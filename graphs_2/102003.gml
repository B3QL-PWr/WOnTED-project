graph [
  node [
    id 0
    label "rozmowa"
    origin "text"
  ]
  node [
    id 1
    label "j&#225;nosem"
    origin "text"
  ]
  node [
    id 2
    label "h&#225;yem"
    origin "text"
  ]
  node [
    id 3
    label "przed"
    origin "text"
  ]
  node [
    id 4
    label "niedzielny"
    origin "text"
  ]
  node [
    id 5
    label "premiera"
    origin "text"
  ]
  node [
    id 6
    label "sztuka"
    origin "text"
  ]
  node [
    id 7
    label "g&#233;za"
    origin "text"
  ]
  node [
    id 8
    label "dzieciak"
    origin "text"
  ]
  node [
    id 9
    label "studio"
    origin "text"
  ]
  node [
    id 10
    label "teatralny"
    origin "text"
  ]
  node [
    id 11
    label "tvp"
    origin "text"
  ]
  node [
    id 12
    label "pesymista"
    origin "text"
  ]
  node [
    id 13
    label "pogodzi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "los"
    origin "text"
  ]
  node [
    id 15
    label "pa&#324;ski"
    origin "text"
  ]
  node [
    id 16
    label "przet&#322;umaczy&#263;"
    origin "text"
  ]
  node [
    id 17
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 18
    label "kilkana&#347;cie"
    origin "text"
  ]
  node [
    id 19
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 20
    label "taki"
    origin "text"
  ]
  node [
    id 21
    label "sukces"
    origin "text"
  ]
  node [
    id 22
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 23
    label "pierwsze"
    origin "text"
  ]
  node [
    id 24
    label "dramat"
    origin "text"
  ]
  node [
    id 25
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 26
    label "by&#263;"
    origin "text"
  ]
  node [
    id 27
    label "niebezpieczny"
    origin "text"
  ]
  node [
    id 28
    label "j&#225;nos"
    origin "text"
  ]
  node [
    id 29
    label "h&#225;y"
    origin "text"
  ]
  node [
    id 30
    label "moi"
    origin "text"
  ]
  node [
    id 31
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 32
    label "nic"
    origin "text"
  ]
  node [
    id 33
    label "si&#281;"
    origin "text"
  ]
  node [
    id 34
    label "zmieni&#263;"
    origin "text"
  ]
  node [
    id 35
    label "sta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 36
    label "ani"
    origin "text"
  ]
  node [
    id 37
    label "bogaty"
    origin "text"
  ]
  node [
    id 38
    label "m&#261;dry"
    origin "text"
  ]
  node [
    id 39
    label "pi&#281;kny"
    origin "text"
  ]
  node [
    id 40
    label "wci&#261;&#380;"
    origin "text"
  ]
  node [
    id 41
    label "upad&#322;y"
    origin "text"
  ]
  node [
    id 42
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 43
    label "jaki"
    origin "text"
  ]
  node [
    id 44
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 45
    label "wczesno"
    origin "text"
  ]
  node [
    id 46
    label "ten"
    origin "text"
  ]
  node [
    id 47
    label "nim"
    origin "text"
  ]
  node [
    id 48
    label "g&#233;z&#281;"
    origin "text"
  ]
  node [
    id 49
    label "w&#281;gry"
    origin "text"
  ]
  node [
    id 50
    label "ceni&#263;"
    origin "text"
  ]
  node [
    id 51
    label "pisarz"
    origin "text"
  ]
  node [
    id 52
    label "poeta"
    origin "text"
  ]
  node [
    id 53
    label "dopiero"
    origin "text"
  ]
  node [
    id 54
    label "przynie&#347;&#263;"
    origin "text"
  ]
  node [
    id 55
    label "pan"
    origin "text"
  ]
  node [
    id 56
    label "s&#322;awa"
    origin "text"
  ]
  node [
    id 57
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 58
    label "cisza"
  ]
  node [
    id 59
    label "odpowied&#378;"
  ]
  node [
    id 60
    label "rozhowor"
  ]
  node [
    id 61
    label "discussion"
  ]
  node [
    id 62
    label "czynno&#347;&#263;"
  ]
  node [
    id 63
    label "activity"
  ]
  node [
    id 64
    label "bezproblemowy"
  ]
  node [
    id 65
    label "wydarzenie"
  ]
  node [
    id 66
    label "g&#322;adki"
  ]
  node [
    id 67
    label "cicha_praca"
  ]
  node [
    id 68
    label "przerwa"
  ]
  node [
    id 69
    label "cicha_msza"
  ]
  node [
    id 70
    label "pok&#243;j"
  ]
  node [
    id 71
    label "motionlessness"
  ]
  node [
    id 72
    label "spok&#243;j"
  ]
  node [
    id 73
    label "zjawisko"
  ]
  node [
    id 74
    label "cecha"
  ]
  node [
    id 75
    label "czas"
  ]
  node [
    id 76
    label "ci&#261;g"
  ]
  node [
    id 77
    label "tajemno&#347;&#263;"
  ]
  node [
    id 78
    label "peace"
  ]
  node [
    id 79
    label "cicha_modlitwa"
  ]
  node [
    id 80
    label "react"
  ]
  node [
    id 81
    label "replica"
  ]
  node [
    id 82
    label "wyj&#347;cie"
  ]
  node [
    id 83
    label "respondent"
  ]
  node [
    id 84
    label "dokument"
  ]
  node [
    id 85
    label "reakcja"
  ]
  node [
    id 86
    label "&#347;wi&#261;teczny"
  ]
  node [
    id 87
    label "od&#347;wi&#281;tny"
  ]
  node [
    id 88
    label "obrz&#281;dowy"
  ]
  node [
    id 89
    label "&#347;wi&#261;tecznie"
  ]
  node [
    id 90
    label "dzie&#324;_wolny"
  ]
  node [
    id 91
    label "wyj&#261;tkowy"
  ]
  node [
    id 92
    label "&#347;wi&#281;tny"
  ]
  node [
    id 93
    label "uroczysty"
  ]
  node [
    id 94
    label "od&#347;wi&#281;tnie"
  ]
  node [
    id 95
    label "premiere"
  ]
  node [
    id 96
    label "przedstawienie"
  ]
  node [
    id 97
    label "pr&#243;bowanie"
  ]
  node [
    id 98
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 99
    label "zademonstrowanie"
  ]
  node [
    id 100
    label "report"
  ]
  node [
    id 101
    label "obgadanie"
  ]
  node [
    id 102
    label "realizacja"
  ]
  node [
    id 103
    label "scena"
  ]
  node [
    id 104
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 105
    label "narration"
  ]
  node [
    id 106
    label "cyrk"
  ]
  node [
    id 107
    label "wytw&#243;r"
  ]
  node [
    id 108
    label "posta&#263;"
  ]
  node [
    id 109
    label "theatrical_performance"
  ]
  node [
    id 110
    label "opisanie"
  ]
  node [
    id 111
    label "malarstwo"
  ]
  node [
    id 112
    label "scenografia"
  ]
  node [
    id 113
    label "teatr"
  ]
  node [
    id 114
    label "ukazanie"
  ]
  node [
    id 115
    label "zapoznanie"
  ]
  node [
    id 116
    label "pokaz"
  ]
  node [
    id 117
    label "podanie"
  ]
  node [
    id 118
    label "spos&#243;b"
  ]
  node [
    id 119
    label "ods&#322;ona"
  ]
  node [
    id 120
    label "exhibit"
  ]
  node [
    id 121
    label "pokazanie"
  ]
  node [
    id 122
    label "wyst&#261;pienie"
  ]
  node [
    id 123
    label "przedstawi&#263;"
  ]
  node [
    id 124
    label "przedstawianie"
  ]
  node [
    id 125
    label "przedstawia&#263;"
  ]
  node [
    id 126
    label "rola"
  ]
  node [
    id 127
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 128
    label "przedmiot"
  ]
  node [
    id 129
    label "didaskalia"
  ]
  node [
    id 130
    label "czyn"
  ]
  node [
    id 131
    label "environment"
  ]
  node [
    id 132
    label "head"
  ]
  node [
    id 133
    label "scenariusz"
  ]
  node [
    id 134
    label "egzemplarz"
  ]
  node [
    id 135
    label "jednostka"
  ]
  node [
    id 136
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 137
    label "utw&#243;r"
  ]
  node [
    id 138
    label "kultura_duchowa"
  ]
  node [
    id 139
    label "fortel"
  ]
  node [
    id 140
    label "ambala&#380;"
  ]
  node [
    id 141
    label "sprawno&#347;&#263;"
  ]
  node [
    id 142
    label "kobieta"
  ]
  node [
    id 143
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 144
    label "Faust"
  ]
  node [
    id 145
    label "turn"
  ]
  node [
    id 146
    label "ilo&#347;&#263;"
  ]
  node [
    id 147
    label "Apollo"
  ]
  node [
    id 148
    label "kultura"
  ]
  node [
    id 149
    label "towar"
  ]
  node [
    id 150
    label "przyswoi&#263;"
  ]
  node [
    id 151
    label "ludzko&#347;&#263;"
  ]
  node [
    id 152
    label "one"
  ]
  node [
    id 153
    label "poj&#281;cie"
  ]
  node [
    id 154
    label "ewoluowanie"
  ]
  node [
    id 155
    label "supremum"
  ]
  node [
    id 156
    label "skala"
  ]
  node [
    id 157
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 158
    label "przyswajanie"
  ]
  node [
    id 159
    label "wyewoluowanie"
  ]
  node [
    id 160
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 161
    label "przeliczy&#263;"
  ]
  node [
    id 162
    label "wyewoluowa&#263;"
  ]
  node [
    id 163
    label "ewoluowa&#263;"
  ]
  node [
    id 164
    label "matematyka"
  ]
  node [
    id 165
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 166
    label "rzut"
  ]
  node [
    id 167
    label "liczba_naturalna"
  ]
  node [
    id 168
    label "czynnik_biotyczny"
  ]
  node [
    id 169
    label "g&#322;owa"
  ]
  node [
    id 170
    label "figura"
  ]
  node [
    id 171
    label "individual"
  ]
  node [
    id 172
    label "portrecista"
  ]
  node [
    id 173
    label "obiekt"
  ]
  node [
    id 174
    label "przyswaja&#263;"
  ]
  node [
    id 175
    label "przyswojenie"
  ]
  node [
    id 176
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 177
    label "profanum"
  ]
  node [
    id 178
    label "mikrokosmos"
  ]
  node [
    id 179
    label "starzenie_si&#281;"
  ]
  node [
    id 180
    label "duch"
  ]
  node [
    id 181
    label "przeliczanie"
  ]
  node [
    id 182
    label "osoba"
  ]
  node [
    id 183
    label "oddzia&#322;ywanie"
  ]
  node [
    id 184
    label "antropochoria"
  ]
  node [
    id 185
    label "funkcja"
  ]
  node [
    id 186
    label "homo_sapiens"
  ]
  node [
    id 187
    label "przelicza&#263;"
  ]
  node [
    id 188
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 189
    label "infimum"
  ]
  node [
    id 190
    label "przeliczenie"
  ]
  node [
    id 191
    label "zbi&#243;r"
  ]
  node [
    id 192
    label "dorobek"
  ]
  node [
    id 193
    label "tworzenie"
  ]
  node [
    id 194
    label "kreacja"
  ]
  node [
    id 195
    label "creation"
  ]
  node [
    id 196
    label "asymilowanie"
  ]
  node [
    id 197
    label "wapniak"
  ]
  node [
    id 198
    label "asymilowa&#263;"
  ]
  node [
    id 199
    label "os&#322;abia&#263;"
  ]
  node [
    id 200
    label "hominid"
  ]
  node [
    id 201
    label "podw&#322;adny"
  ]
  node [
    id 202
    label "os&#322;abianie"
  ]
  node [
    id 203
    label "dwun&#243;g"
  ]
  node [
    id 204
    label "nasada"
  ]
  node [
    id 205
    label "wz&#243;r"
  ]
  node [
    id 206
    label "senior"
  ]
  node [
    id 207
    label "Adam"
  ]
  node [
    id 208
    label "polifag"
  ]
  node [
    id 209
    label "act"
  ]
  node [
    id 210
    label "rozmiar"
  ]
  node [
    id 211
    label "part"
  ]
  node [
    id 212
    label "fabrication"
  ]
  node [
    id 213
    label "scheduling"
  ]
  node [
    id 214
    label "operacja"
  ]
  node [
    id 215
    label "proces"
  ]
  node [
    id 216
    label "dzie&#322;o"
  ]
  node [
    id 217
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 218
    label "monta&#380;"
  ]
  node [
    id 219
    label "postprodukcja"
  ]
  node [
    id 220
    label "performance"
  ]
  node [
    id 221
    label "pokaz&#243;wka"
  ]
  node [
    id 222
    label "prezenter"
  ]
  node [
    id 223
    label "wyraz"
  ]
  node [
    id 224
    label "impreza"
  ]
  node [
    id 225
    label "show"
  ]
  node [
    id 226
    label "zboczenie"
  ]
  node [
    id 227
    label "om&#243;wienie"
  ]
  node [
    id 228
    label "sponiewieranie"
  ]
  node [
    id 229
    label "discipline"
  ]
  node [
    id 230
    label "rzecz"
  ]
  node [
    id 231
    label "omawia&#263;"
  ]
  node [
    id 232
    label "kr&#261;&#380;enie"
  ]
  node [
    id 233
    label "tre&#347;&#263;"
  ]
  node [
    id 234
    label "robienie"
  ]
  node [
    id 235
    label "sponiewiera&#263;"
  ]
  node [
    id 236
    label "element"
  ]
  node [
    id 237
    label "entity"
  ]
  node [
    id 238
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 239
    label "tematyka"
  ]
  node [
    id 240
    label "w&#261;tek"
  ]
  node [
    id 241
    label "charakter"
  ]
  node [
    id 242
    label "zbaczanie"
  ]
  node [
    id 243
    label "program_nauczania"
  ]
  node [
    id 244
    label "om&#243;wi&#263;"
  ]
  node [
    id 245
    label "omawianie"
  ]
  node [
    id 246
    label "thing"
  ]
  node [
    id 247
    label "istota"
  ]
  node [
    id 248
    label "zbacza&#263;"
  ]
  node [
    id 249
    label "zboczy&#263;"
  ]
  node [
    id 250
    label "metka"
  ]
  node [
    id 251
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 252
    label "szprycowa&#263;"
  ]
  node [
    id 253
    label "naszprycowa&#263;"
  ]
  node [
    id 254
    label "rzuca&#263;"
  ]
  node [
    id 255
    label "tandeta"
  ]
  node [
    id 256
    label "obr&#243;t_handlowy"
  ]
  node [
    id 257
    label "wyr&#243;b"
  ]
  node [
    id 258
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 259
    label "rzuci&#263;"
  ]
  node [
    id 260
    label "naszprycowanie"
  ]
  node [
    id 261
    label "tkanina"
  ]
  node [
    id 262
    label "szprycowanie"
  ]
  node [
    id 263
    label "za&#322;adownia"
  ]
  node [
    id 264
    label "asortyment"
  ]
  node [
    id 265
    label "&#322;&#243;dzki"
  ]
  node [
    id 266
    label "narkobiznes"
  ]
  node [
    id 267
    label "rzucenie"
  ]
  node [
    id 268
    label "rzucanie"
  ]
  node [
    id 269
    label "doros&#322;y"
  ]
  node [
    id 270
    label "&#380;ona"
  ]
  node [
    id 271
    label "samica"
  ]
  node [
    id 272
    label "uleganie"
  ]
  node [
    id 273
    label "ulec"
  ]
  node [
    id 274
    label "m&#281;&#380;yna"
  ]
  node [
    id 275
    label "partnerka"
  ]
  node [
    id 276
    label "ulegni&#281;cie"
  ]
  node [
    id 277
    label "pa&#324;stwo"
  ]
  node [
    id 278
    label "&#322;ono"
  ]
  node [
    id 279
    label "menopauza"
  ]
  node [
    id 280
    label "przekwitanie"
  ]
  node [
    id 281
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 282
    label "babka"
  ]
  node [
    id 283
    label "ulega&#263;"
  ]
  node [
    id 284
    label "jako&#347;&#263;"
  ]
  node [
    id 285
    label "szybko&#347;&#263;"
  ]
  node [
    id 286
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 287
    label "kondycja_fizyczna"
  ]
  node [
    id 288
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 289
    label "zdrowie"
  ]
  node [
    id 290
    label "stan"
  ]
  node [
    id 291
    label "harcerski"
  ]
  node [
    id 292
    label "odznaka"
  ]
  node [
    id 293
    label "obrazowanie"
  ]
  node [
    id 294
    label "organ"
  ]
  node [
    id 295
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 296
    label "element_anatomiczny"
  ]
  node [
    id 297
    label "tekst"
  ]
  node [
    id 298
    label "komunikat"
  ]
  node [
    id 299
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 300
    label "okaz"
  ]
  node [
    id 301
    label "agent"
  ]
  node [
    id 302
    label "nicpo&#324;"
  ]
  node [
    id 303
    label "pean"
  ]
  node [
    id 304
    label "ukaza&#263;"
  ]
  node [
    id 305
    label "pokaza&#263;"
  ]
  node [
    id 306
    label "poda&#263;"
  ]
  node [
    id 307
    label "zapozna&#263;"
  ]
  node [
    id 308
    label "express"
  ]
  node [
    id 309
    label "represent"
  ]
  node [
    id 310
    label "zaproponowa&#263;"
  ]
  node [
    id 311
    label "zademonstrowa&#263;"
  ]
  node [
    id 312
    label "typify"
  ]
  node [
    id 313
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 314
    label "opisa&#263;"
  ]
  node [
    id 315
    label "opisywanie"
  ]
  node [
    id 316
    label "bycie"
  ]
  node [
    id 317
    label "representation"
  ]
  node [
    id 318
    label "obgadywanie"
  ]
  node [
    id 319
    label "zapoznawanie"
  ]
  node [
    id 320
    label "wyst&#281;powanie"
  ]
  node [
    id 321
    label "ukazywanie"
  ]
  node [
    id 322
    label "pokazywanie"
  ]
  node [
    id 323
    label "display"
  ]
  node [
    id 324
    label "podawanie"
  ]
  node [
    id 325
    label "demonstrowanie"
  ]
  node [
    id 326
    label "presentation"
  ]
  node [
    id 327
    label "medialno&#347;&#263;"
  ]
  node [
    id 328
    label "podawa&#263;"
  ]
  node [
    id 329
    label "pokazywa&#263;"
  ]
  node [
    id 330
    label "demonstrowa&#263;"
  ]
  node [
    id 331
    label "zapoznawa&#263;"
  ]
  node [
    id 332
    label "opisywa&#263;"
  ]
  node [
    id 333
    label "ukazywa&#263;"
  ]
  node [
    id 334
    label "zg&#322;asza&#263;"
  ]
  node [
    id 335
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 336
    label "attest"
  ]
  node [
    id 337
    label "stanowi&#263;"
  ]
  node [
    id 338
    label "badanie"
  ]
  node [
    id 339
    label "jedzenie"
  ]
  node [
    id 340
    label "podejmowanie"
  ]
  node [
    id 341
    label "usi&#322;owanie"
  ]
  node [
    id 342
    label "tasting"
  ]
  node [
    id 343
    label "kiperstwo"
  ]
  node [
    id 344
    label "staranie_si&#281;"
  ]
  node [
    id 345
    label "zaznawanie"
  ]
  node [
    id 346
    label "przygotowywanie_si&#281;"
  ]
  node [
    id 347
    label "essay"
  ]
  node [
    id 348
    label "stara&#263;_si&#281;"
  ]
  node [
    id 349
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 350
    label "sprawdza&#263;"
  ]
  node [
    id 351
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 352
    label "feel"
  ]
  node [
    id 353
    label "try"
  ]
  node [
    id 354
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 355
    label "kosztowa&#263;"
  ]
  node [
    id 356
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 357
    label "plan"
  ]
  node [
    id 358
    label "prognoza"
  ]
  node [
    id 359
    label "scenario"
  ]
  node [
    id 360
    label "uprawienie"
  ]
  node [
    id 361
    label "kszta&#322;t"
  ]
  node [
    id 362
    label "dialog"
  ]
  node [
    id 363
    label "p&#322;osa"
  ]
  node [
    id 364
    label "wykonywanie"
  ]
  node [
    id 365
    label "plik"
  ]
  node [
    id 366
    label "ziemia"
  ]
  node [
    id 367
    label "wykonywa&#263;"
  ]
  node [
    id 368
    label "ustawienie"
  ]
  node [
    id 369
    label "pole"
  ]
  node [
    id 370
    label "gospodarstwo"
  ]
  node [
    id 371
    label "uprawi&#263;"
  ]
  node [
    id 372
    label "function"
  ]
  node [
    id 373
    label "zreinterpretowa&#263;"
  ]
  node [
    id 374
    label "zastosowanie"
  ]
  node [
    id 375
    label "reinterpretowa&#263;"
  ]
  node [
    id 376
    label "wrench"
  ]
  node [
    id 377
    label "irygowanie"
  ]
  node [
    id 378
    label "ustawi&#263;"
  ]
  node [
    id 379
    label "irygowa&#263;"
  ]
  node [
    id 380
    label "zreinterpretowanie"
  ]
  node [
    id 381
    label "cel"
  ]
  node [
    id 382
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 383
    label "gra&#263;"
  ]
  node [
    id 384
    label "aktorstwo"
  ]
  node [
    id 385
    label "kostium"
  ]
  node [
    id 386
    label "zagon"
  ]
  node [
    id 387
    label "znaczenie"
  ]
  node [
    id 388
    label "zagra&#263;"
  ]
  node [
    id 389
    label "reinterpretowanie"
  ]
  node [
    id 390
    label "sk&#322;ad"
  ]
  node [
    id 391
    label "zagranie"
  ]
  node [
    id 392
    label "radlina"
  ]
  node [
    id 393
    label "granie"
  ]
  node [
    id 394
    label "mansjon"
  ]
  node [
    id 395
    label "modelatornia"
  ]
  node [
    id 396
    label "dekoracja"
  ]
  node [
    id 397
    label "podwy&#380;szenie"
  ]
  node [
    id 398
    label "kurtyna"
  ]
  node [
    id 399
    label "akt"
  ]
  node [
    id 400
    label "widzownia"
  ]
  node [
    id 401
    label "sznurownia"
  ]
  node [
    id 402
    label "dramaturgy"
  ]
  node [
    id 403
    label "sphere"
  ]
  node [
    id 404
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 405
    label "budka_suflera"
  ]
  node [
    id 406
    label "epizod"
  ]
  node [
    id 407
    label "film"
  ]
  node [
    id 408
    label "fragment"
  ]
  node [
    id 409
    label "k&#322;&#243;tnia"
  ]
  node [
    id 410
    label "kiesze&#324;"
  ]
  node [
    id 411
    label "stadium"
  ]
  node [
    id 412
    label "podest"
  ]
  node [
    id 413
    label "horyzont"
  ]
  node [
    id 414
    label "teren"
  ]
  node [
    id 415
    label "instytucja"
  ]
  node [
    id 416
    label "proscenium"
  ]
  node [
    id 417
    label "nadscenie"
  ]
  node [
    id 418
    label "antyteatr"
  ]
  node [
    id 419
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 420
    label "stage_direction"
  ]
  node [
    id 421
    label "obja&#347;nienie"
  ]
  node [
    id 422
    label "asymilowanie_si&#281;"
  ]
  node [
    id 423
    label "Wsch&#243;d"
  ]
  node [
    id 424
    label "praca_rolnicza"
  ]
  node [
    id 425
    label "przejmowanie"
  ]
  node [
    id 426
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 427
    label "makrokosmos"
  ]
  node [
    id 428
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 429
    label "konwencja"
  ]
  node [
    id 430
    label "propriety"
  ]
  node [
    id 431
    label "przejmowa&#263;"
  ]
  node [
    id 432
    label "brzoskwiniarnia"
  ]
  node [
    id 433
    label "zwyczaj"
  ]
  node [
    id 434
    label "kuchnia"
  ]
  node [
    id 435
    label "tradycja"
  ]
  node [
    id 436
    label "populace"
  ]
  node [
    id 437
    label "hodowla"
  ]
  node [
    id 438
    label "religia"
  ]
  node [
    id 439
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 440
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 441
    label "przej&#281;cie"
  ]
  node [
    id 442
    label "przej&#261;&#263;"
  ]
  node [
    id 443
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 444
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 445
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 446
    label "&#347;rodek"
  ]
  node [
    id 447
    label "manewr"
  ]
  node [
    id 448
    label "chwyt"
  ]
  node [
    id 449
    label "game"
  ]
  node [
    id 450
    label "podchwyt"
  ]
  node [
    id 451
    label "bech"
  ]
  node [
    id 452
    label "dziecinny"
  ]
  node [
    id 453
    label "naiwniak"
  ]
  node [
    id 454
    label "dziecko"
  ]
  node [
    id 455
    label "utulenie"
  ]
  node [
    id 456
    label "pediatra"
  ]
  node [
    id 457
    label "utulanie"
  ]
  node [
    id 458
    label "dzieciarnia"
  ]
  node [
    id 459
    label "niepe&#322;noletni"
  ]
  node [
    id 460
    label "organizm"
  ]
  node [
    id 461
    label "utula&#263;"
  ]
  node [
    id 462
    label "cz&#322;owieczek"
  ]
  node [
    id 463
    label "fledgling"
  ]
  node [
    id 464
    label "zwierz&#281;"
  ]
  node [
    id 465
    label "utuli&#263;"
  ]
  node [
    id 466
    label "m&#322;odzik"
  ]
  node [
    id 467
    label "pedofil"
  ]
  node [
    id 468
    label "m&#322;odziak"
  ]
  node [
    id 469
    label "potomek"
  ]
  node [
    id 470
    label "entliczek-pentliczek"
  ]
  node [
    id 471
    label "potomstwo"
  ]
  node [
    id 472
    label "sraluch"
  ]
  node [
    id 473
    label "wa&#322;"
  ]
  node [
    id 474
    label "g&#322;upiec"
  ]
  node [
    id 475
    label "oferma"
  ]
  node [
    id 476
    label "dziecinnie"
  ]
  node [
    id 477
    label "pocz&#261;tkowy"
  ]
  node [
    id 478
    label "typowy"
  ]
  node [
    id 479
    label "dzieci&#281;co"
  ]
  node [
    id 480
    label "dziecinnienie"
  ]
  node [
    id 481
    label "dzieci&#324;ski"
  ]
  node [
    id 482
    label "zdziecinnienie"
  ]
  node [
    id 483
    label "niedojrza&#322;y"
  ]
  node [
    id 484
    label "telewizja"
  ]
  node [
    id 485
    label "radio"
  ]
  node [
    id 486
    label "pomieszczenie"
  ]
  node [
    id 487
    label "amfilada"
  ]
  node [
    id 488
    label "front"
  ]
  node [
    id 489
    label "apartment"
  ]
  node [
    id 490
    label "pod&#322;oga"
  ]
  node [
    id 491
    label "udost&#281;pnienie"
  ]
  node [
    id 492
    label "miejsce"
  ]
  node [
    id 493
    label "sklepienie"
  ]
  node [
    id 494
    label "sufit"
  ]
  node [
    id 495
    label "umieszczenie"
  ]
  node [
    id 496
    label "zakamarek"
  ]
  node [
    id 497
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 498
    label "paj&#281;czarz"
  ]
  node [
    id 499
    label "radiola"
  ]
  node [
    id 500
    label "programowiec"
  ]
  node [
    id 501
    label "redakcja"
  ]
  node [
    id 502
    label "spot"
  ]
  node [
    id 503
    label "stacja"
  ]
  node [
    id 504
    label "uk&#322;ad"
  ]
  node [
    id 505
    label "odbiornik"
  ]
  node [
    id 506
    label "eliminator"
  ]
  node [
    id 507
    label "radiolinia"
  ]
  node [
    id 508
    label "media"
  ]
  node [
    id 509
    label "fala_radiowa"
  ]
  node [
    id 510
    label "radiofonia"
  ]
  node [
    id 511
    label "odbieranie"
  ]
  node [
    id 512
    label "dyskryminator"
  ]
  node [
    id 513
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 514
    label "odbiera&#263;"
  ]
  node [
    id 515
    label "telekomunikacja"
  ]
  node [
    id 516
    label "ekran"
  ]
  node [
    id 517
    label "Interwizja"
  ]
  node [
    id 518
    label "BBC"
  ]
  node [
    id 519
    label "Polsat"
  ]
  node [
    id 520
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 521
    label "muza"
  ]
  node [
    id 522
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 523
    label "technologia"
  ]
  node [
    id 524
    label "nienaturalny"
  ]
  node [
    id 525
    label "teatralnie"
  ]
  node [
    id 526
    label "nadmierny"
  ]
  node [
    id 527
    label "nadmiernie"
  ]
  node [
    id 528
    label "artystycznie"
  ]
  node [
    id 529
    label "stagily"
  ]
  node [
    id 530
    label "theatrically"
  ]
  node [
    id 531
    label "nienaturalnie"
  ]
  node [
    id 532
    label "niespotykany"
  ]
  node [
    id 533
    label "sztucznie"
  ]
  node [
    id 534
    label "niepodobny"
  ]
  node [
    id 535
    label "nieprzekonuj&#261;cy"
  ]
  node [
    id 536
    label "nienormalny"
  ]
  node [
    id 537
    label "nietypowy"
  ]
  node [
    id 538
    label "dziwny"
  ]
  node [
    id 539
    label "smutas"
  ]
  node [
    id 540
    label "sceptyk"
  ]
  node [
    id 541
    label "Schopenhauer"
  ]
  node [
    id 542
    label "nudziarz"
  ]
  node [
    id 543
    label "filozof_staro&#380;ytny"
  ]
  node [
    id 544
    label "teoria_poznania"
  ]
  node [
    id 545
    label "filozofia"
  ]
  node [
    id 546
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 547
    label "accommodate"
  ]
  node [
    id 548
    label "spowodowa&#263;"
  ]
  node [
    id 549
    label "permit"
  ]
  node [
    id 550
    label "destiny"
  ]
  node [
    id 551
    label "si&#322;a"
  ]
  node [
    id 552
    label "przymus"
  ]
  node [
    id 553
    label "hazard"
  ]
  node [
    id 554
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 555
    label "przebieg_&#380;ycia"
  ]
  node [
    id 556
    label "bilet"
  ]
  node [
    id 557
    label "karta_wst&#281;pu"
  ]
  node [
    id 558
    label "konik"
  ]
  node [
    id 559
    label "passe-partout"
  ]
  node [
    id 560
    label "cedu&#322;a"
  ]
  node [
    id 561
    label "energia"
  ]
  node [
    id 562
    label "parametr"
  ]
  node [
    id 563
    label "rozwi&#261;zanie"
  ]
  node [
    id 564
    label "wojsko"
  ]
  node [
    id 565
    label "wuchta"
  ]
  node [
    id 566
    label "zaleta"
  ]
  node [
    id 567
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 568
    label "moment_si&#322;y"
  ]
  node [
    id 569
    label "mn&#243;stwo"
  ]
  node [
    id 570
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 571
    label "zdolno&#347;&#263;"
  ]
  node [
    id 572
    label "capacity"
  ]
  node [
    id 573
    label "magnitude"
  ]
  node [
    id 574
    label "potencja"
  ]
  node [
    id 575
    label "przemoc"
  ]
  node [
    id 576
    label "potrzeba"
  ]
  node [
    id 577
    label "presja"
  ]
  node [
    id 578
    label "raj_utracony"
  ]
  node [
    id 579
    label "umieranie"
  ]
  node [
    id 580
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 581
    label "prze&#380;ywanie"
  ]
  node [
    id 582
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 583
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 584
    label "po&#322;&#243;g"
  ]
  node [
    id 585
    label "umarcie"
  ]
  node [
    id 586
    label "subsistence"
  ]
  node [
    id 587
    label "power"
  ]
  node [
    id 588
    label "okres_noworodkowy"
  ]
  node [
    id 589
    label "prze&#380;ycie"
  ]
  node [
    id 590
    label "wiek_matuzalemowy"
  ]
  node [
    id 591
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 592
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 593
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 594
    label "do&#380;ywanie"
  ]
  node [
    id 595
    label "byt"
  ]
  node [
    id 596
    label "andropauza"
  ]
  node [
    id 597
    label "dzieci&#324;stwo"
  ]
  node [
    id 598
    label "rozw&#243;j"
  ]
  node [
    id 599
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 600
    label "&#347;mier&#263;"
  ]
  node [
    id 601
    label "koleje_losu"
  ]
  node [
    id 602
    label "zegar_biologiczny"
  ]
  node [
    id 603
    label "szwung"
  ]
  node [
    id 604
    label "przebywanie"
  ]
  node [
    id 605
    label "warunki"
  ]
  node [
    id 606
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 607
    label "niemowl&#281;ctwo"
  ]
  node [
    id 608
    label "&#380;ywy"
  ]
  node [
    id 609
    label "life"
  ]
  node [
    id 610
    label "staro&#347;&#263;"
  ]
  node [
    id 611
    label "energy"
  ]
  node [
    id 612
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 613
    label "konwulsja"
  ]
  node [
    id 614
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 615
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 616
    label "ruszy&#263;"
  ]
  node [
    id 617
    label "powiedzie&#263;"
  ]
  node [
    id 618
    label "majdn&#261;&#263;"
  ]
  node [
    id 619
    label "most"
  ]
  node [
    id 620
    label "poruszy&#263;"
  ]
  node [
    id 621
    label "wyzwanie"
  ]
  node [
    id 622
    label "da&#263;"
  ]
  node [
    id 623
    label "peddle"
  ]
  node [
    id 624
    label "rush"
  ]
  node [
    id 625
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 626
    label "bewilder"
  ]
  node [
    id 627
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 628
    label "przeznaczenie"
  ]
  node [
    id 629
    label "skonstruowa&#263;"
  ]
  node [
    id 630
    label "sygn&#261;&#263;"
  ]
  node [
    id 631
    label "&#347;wiat&#322;o"
  ]
  node [
    id 632
    label "wywo&#322;a&#263;"
  ]
  node [
    id 633
    label "frame"
  ]
  node [
    id 634
    label "podejrzenie"
  ]
  node [
    id 635
    label "czar"
  ]
  node [
    id 636
    label "project"
  ]
  node [
    id 637
    label "odej&#347;&#263;"
  ]
  node [
    id 638
    label "zdecydowa&#263;"
  ]
  node [
    id 639
    label "cie&#324;"
  ]
  node [
    id 640
    label "opu&#347;ci&#263;"
  ]
  node [
    id 641
    label "atak"
  ]
  node [
    id 642
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 643
    label "ruszenie"
  ]
  node [
    id 644
    label "pierdolni&#281;cie"
  ]
  node [
    id 645
    label "poruszenie"
  ]
  node [
    id 646
    label "opuszczenie"
  ]
  node [
    id 647
    label "wywo&#322;anie"
  ]
  node [
    id 648
    label "odej&#347;cie"
  ]
  node [
    id 649
    label "przewr&#243;cenie"
  ]
  node [
    id 650
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 651
    label "skonstruowanie"
  ]
  node [
    id 652
    label "spowodowanie"
  ]
  node [
    id 653
    label "grzmotni&#281;cie"
  ]
  node [
    id 654
    label "zdecydowanie"
  ]
  node [
    id 655
    label "przemieszczenie"
  ]
  node [
    id 656
    label "wyposa&#380;enie"
  ]
  node [
    id 657
    label "shy"
  ]
  node [
    id 658
    label "oddzia&#322;anie"
  ]
  node [
    id 659
    label "zrezygnowanie"
  ]
  node [
    id 660
    label "porzucenie"
  ]
  node [
    id 661
    label "powiedzenie"
  ]
  node [
    id 662
    label "play"
  ]
  node [
    id 663
    label "prawdopodobie&#324;stwo"
  ]
  node [
    id 664
    label "rozrywka"
  ]
  node [
    id 665
    label "wideoloteria"
  ]
  node [
    id 666
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 667
    label "charakterystyczny"
  ]
  node [
    id 668
    label "pa&#324;sko"
  ]
  node [
    id 669
    label "charakterystycznie"
  ]
  node [
    id 670
    label "szczeg&#243;lny"
  ]
  node [
    id 671
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 672
    label "podobny"
  ]
  node [
    id 673
    label "arrogantly"
  ]
  node [
    id 674
    label "zinterpretowa&#263;"
  ]
  node [
    id 675
    label "put"
  ]
  node [
    id 676
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 677
    label "zrobi&#263;"
  ]
  node [
    id 678
    label "przekona&#263;"
  ]
  node [
    id 679
    label "deepen"
  ]
  node [
    id 680
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 681
    label "transfer"
  ]
  node [
    id 682
    label "translate"
  ]
  node [
    id 683
    label "give"
  ]
  node [
    id 684
    label "picture"
  ]
  node [
    id 685
    label "uzna&#263;"
  ]
  node [
    id 686
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 687
    label "przenie&#347;&#263;"
  ]
  node [
    id 688
    label "prym"
  ]
  node [
    id 689
    label "post&#261;pi&#263;"
  ]
  node [
    id 690
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 691
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 692
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 693
    label "zorganizowa&#263;"
  ]
  node [
    id 694
    label "appoint"
  ]
  node [
    id 695
    label "wystylizowa&#263;"
  ]
  node [
    id 696
    label "cause"
  ]
  node [
    id 697
    label "przerobi&#263;"
  ]
  node [
    id 698
    label "nabra&#263;"
  ]
  node [
    id 699
    label "make"
  ]
  node [
    id 700
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 701
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 702
    label "wydali&#263;"
  ]
  node [
    id 703
    label "oceni&#263;"
  ]
  node [
    id 704
    label "illustrate"
  ]
  node [
    id 705
    label "zanalizowa&#263;"
  ]
  node [
    id 706
    label "read"
  ]
  node [
    id 707
    label "think"
  ]
  node [
    id 708
    label "get"
  ]
  node [
    id 709
    label "nak&#322;oni&#263;"
  ]
  node [
    id 710
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 711
    label "artykulator"
  ]
  node [
    id 712
    label "kod"
  ]
  node [
    id 713
    label "kawa&#322;ek"
  ]
  node [
    id 714
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 715
    label "gramatyka"
  ]
  node [
    id 716
    label "stylik"
  ]
  node [
    id 717
    label "przet&#322;umaczenie"
  ]
  node [
    id 718
    label "formalizowanie"
  ]
  node [
    id 719
    label "ssanie"
  ]
  node [
    id 720
    label "ssa&#263;"
  ]
  node [
    id 721
    label "language"
  ]
  node [
    id 722
    label "liza&#263;"
  ]
  node [
    id 723
    label "konsonantyzm"
  ]
  node [
    id 724
    label "wokalizm"
  ]
  node [
    id 725
    label "pisa&#263;"
  ]
  node [
    id 726
    label "fonetyka"
  ]
  node [
    id 727
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 728
    label "jeniec"
  ]
  node [
    id 729
    label "but"
  ]
  node [
    id 730
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 731
    label "po_koroniarsku"
  ]
  node [
    id 732
    label "t&#322;umaczenie"
  ]
  node [
    id 733
    label "m&#243;wienie"
  ]
  node [
    id 734
    label "pype&#263;"
  ]
  node [
    id 735
    label "lizanie"
  ]
  node [
    id 736
    label "pismo"
  ]
  node [
    id 737
    label "formalizowa&#263;"
  ]
  node [
    id 738
    label "rozumie&#263;"
  ]
  node [
    id 739
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 740
    label "rozumienie"
  ]
  node [
    id 741
    label "makroglosja"
  ]
  node [
    id 742
    label "m&#243;wi&#263;"
  ]
  node [
    id 743
    label "jama_ustna"
  ]
  node [
    id 744
    label "formacja_geologiczna"
  ]
  node [
    id 745
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 746
    label "natural_language"
  ]
  node [
    id 747
    label "s&#322;ownictwo"
  ]
  node [
    id 748
    label "urz&#261;dzenie"
  ]
  node [
    id 749
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 750
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 751
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 752
    label "osta&#263;_si&#281;"
  ]
  node [
    id 753
    label "change"
  ]
  node [
    id 754
    label "pozosta&#263;"
  ]
  node [
    id 755
    label "catch"
  ]
  node [
    id 756
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 757
    label "proceed"
  ]
  node [
    id 758
    label "support"
  ]
  node [
    id 759
    label "prze&#380;y&#263;"
  ]
  node [
    id 760
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 761
    label "kawa&#322;"
  ]
  node [
    id 762
    label "plot"
  ]
  node [
    id 763
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 764
    label "piece"
  ]
  node [
    id 765
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 766
    label "podp&#322;ywanie"
  ]
  node [
    id 767
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 768
    label "model"
  ]
  node [
    id 769
    label "narz&#281;dzie"
  ]
  node [
    id 770
    label "tryb"
  ]
  node [
    id 771
    label "nature"
  ]
  node [
    id 772
    label "struktura"
  ]
  node [
    id 773
    label "code"
  ]
  node [
    id 774
    label "szyfrowanie"
  ]
  node [
    id 775
    label "szablon"
  ]
  node [
    id 776
    label "&#380;o&#322;nierz"
  ]
  node [
    id 777
    label "internowanie"
  ]
  node [
    id 778
    label "ojczyc"
  ]
  node [
    id 779
    label "pojmaniec"
  ]
  node [
    id 780
    label "niewolnik"
  ]
  node [
    id 781
    label "internowa&#263;"
  ]
  node [
    id 782
    label "tkanka"
  ]
  node [
    id 783
    label "jednostka_organizacyjna"
  ]
  node [
    id 784
    label "budowa"
  ]
  node [
    id 785
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 786
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 787
    label "tw&#243;r"
  ]
  node [
    id 788
    label "organogeneza"
  ]
  node [
    id 789
    label "zesp&#243;&#322;"
  ]
  node [
    id 790
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 791
    label "struktura_anatomiczna"
  ]
  node [
    id 792
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 793
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 794
    label "Izba_Konsyliarska"
  ]
  node [
    id 795
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 796
    label "stomia"
  ]
  node [
    id 797
    label "dekortykacja"
  ]
  node [
    id 798
    label "okolica"
  ]
  node [
    id 799
    label "Komitet_Region&#243;w"
  ]
  node [
    id 800
    label "aparat_artykulacyjny"
  ]
  node [
    id 801
    label "zapi&#281;tek"
  ]
  node [
    id 802
    label "sznurowad&#322;o"
  ]
  node [
    id 803
    label "rozbijarka"
  ]
  node [
    id 804
    label "podeszwa"
  ]
  node [
    id 805
    label "obcas"
  ]
  node [
    id 806
    label "wzuwanie"
  ]
  node [
    id 807
    label "wzu&#263;"
  ]
  node [
    id 808
    label "przyszwa"
  ]
  node [
    id 809
    label "raki"
  ]
  node [
    id 810
    label "cholewa"
  ]
  node [
    id 811
    label "cholewka"
  ]
  node [
    id 812
    label "zel&#243;wka"
  ]
  node [
    id 813
    label "obuwie"
  ]
  node [
    id 814
    label "napi&#281;tek"
  ]
  node [
    id 815
    label "wzucie"
  ]
  node [
    id 816
    label "kom&#243;rka"
  ]
  node [
    id 817
    label "furnishing"
  ]
  node [
    id 818
    label "zabezpieczenie"
  ]
  node [
    id 819
    label "zrobienie"
  ]
  node [
    id 820
    label "wyrz&#261;dzenie"
  ]
  node [
    id 821
    label "zagospodarowanie"
  ]
  node [
    id 822
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 823
    label "ig&#322;a"
  ]
  node [
    id 824
    label "wirnik"
  ]
  node [
    id 825
    label "aparatura"
  ]
  node [
    id 826
    label "system_energetyczny"
  ]
  node [
    id 827
    label "impulsator"
  ]
  node [
    id 828
    label "mechanizm"
  ]
  node [
    id 829
    label "sprz&#281;t"
  ]
  node [
    id 830
    label "blokowanie"
  ]
  node [
    id 831
    label "set"
  ]
  node [
    id 832
    label "zablokowanie"
  ]
  node [
    id 833
    label "przygotowanie"
  ]
  node [
    id 834
    label "komora"
  ]
  node [
    id 835
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 836
    label "public_speaking"
  ]
  node [
    id 837
    label "powiadanie"
  ]
  node [
    id 838
    label "przepowiadanie"
  ]
  node [
    id 839
    label "wyznawanie"
  ]
  node [
    id 840
    label "wypowiadanie"
  ]
  node [
    id 841
    label "wydobywanie"
  ]
  node [
    id 842
    label "gaworzenie"
  ]
  node [
    id 843
    label "stosowanie"
  ]
  node [
    id 844
    label "wyra&#380;anie"
  ]
  node [
    id 845
    label "formu&#322;owanie"
  ]
  node [
    id 846
    label "dowalenie"
  ]
  node [
    id 847
    label "przerywanie"
  ]
  node [
    id 848
    label "wydawanie"
  ]
  node [
    id 849
    label "dogadywanie_si&#281;"
  ]
  node [
    id 850
    label "dodawanie"
  ]
  node [
    id 851
    label "prawienie"
  ]
  node [
    id 852
    label "opowiadanie"
  ]
  node [
    id 853
    label "ozywanie_si&#281;"
  ]
  node [
    id 854
    label "zapeszanie"
  ]
  node [
    id 855
    label "zwracanie_si&#281;"
  ]
  node [
    id 856
    label "dysfonia"
  ]
  node [
    id 857
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 858
    label "speaking"
  ]
  node [
    id 859
    label "zauwa&#380;enie"
  ]
  node [
    id 860
    label "mawianie"
  ]
  node [
    id 861
    label "opowiedzenie"
  ]
  node [
    id 862
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 863
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 864
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 865
    label "informowanie"
  ]
  node [
    id 866
    label "dogadanie_si&#281;"
  ]
  node [
    id 867
    label "wygadanie"
  ]
  node [
    id 868
    label "terminology"
  ]
  node [
    id 869
    label "termin"
  ]
  node [
    id 870
    label "g&#322;osownia"
  ]
  node [
    id 871
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 872
    label "zasymilowa&#263;"
  ]
  node [
    id 873
    label "phonetics"
  ]
  node [
    id 874
    label "palatogram"
  ]
  node [
    id 875
    label "transkrypcja"
  ]
  node [
    id 876
    label "zasymilowanie"
  ]
  node [
    id 877
    label "psychotest"
  ]
  node [
    id 878
    label "wk&#322;ad"
  ]
  node [
    id 879
    label "handwriting"
  ]
  node [
    id 880
    label "przekaz"
  ]
  node [
    id 881
    label "paleograf"
  ]
  node [
    id 882
    label "interpunkcja"
  ]
  node [
    id 883
    label "dzia&#322;"
  ]
  node [
    id 884
    label "grafia"
  ]
  node [
    id 885
    label "communication"
  ]
  node [
    id 886
    label "script"
  ]
  node [
    id 887
    label "zajawka"
  ]
  node [
    id 888
    label "list"
  ]
  node [
    id 889
    label "adres"
  ]
  node [
    id 890
    label "Zwrotnica"
  ]
  node [
    id 891
    label "czasopismo"
  ]
  node [
    id 892
    label "ok&#322;adka"
  ]
  node [
    id 893
    label "ortografia"
  ]
  node [
    id 894
    label "letter"
  ]
  node [
    id 895
    label "komunikacja"
  ]
  node [
    id 896
    label "paleografia"
  ]
  node [
    id 897
    label "prasa"
  ]
  node [
    id 898
    label "fleksja"
  ]
  node [
    id 899
    label "sk&#322;adnia"
  ]
  node [
    id 900
    label "kategoria_gramatyczna"
  ]
  node [
    id 901
    label "morfologia"
  ]
  node [
    id 902
    label "styl"
  ]
  node [
    id 903
    label "stworzy&#263;"
  ]
  node [
    id 904
    label "postawi&#263;"
  ]
  node [
    id 905
    label "write"
  ]
  node [
    id 906
    label "donie&#347;&#263;"
  ]
  node [
    id 907
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 908
    label "formu&#322;owa&#263;"
  ]
  node [
    id 909
    label "ozdabia&#263;"
  ]
  node [
    id 910
    label "stawia&#263;"
  ]
  node [
    id 911
    label "spell"
  ]
  node [
    id 912
    label "skryba"
  ]
  node [
    id 913
    label "donosi&#263;"
  ]
  node [
    id 914
    label "dysgrafia"
  ]
  node [
    id 915
    label "dysortografia"
  ]
  node [
    id 916
    label "tworzy&#263;"
  ]
  node [
    id 917
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 918
    label "rendition"
  ]
  node [
    id 919
    label "explanation"
  ]
  node [
    id 920
    label "bronienie"
  ]
  node [
    id 921
    label "remark"
  ]
  node [
    id 922
    label "przek&#322;adanie"
  ]
  node [
    id 923
    label "zrozumia&#322;y"
  ]
  node [
    id 924
    label "przekonywanie"
  ]
  node [
    id 925
    label "uzasadnianie"
  ]
  node [
    id 926
    label "rozwianie"
  ]
  node [
    id 927
    label "rozwiewanie"
  ]
  node [
    id 928
    label "gossip"
  ]
  node [
    id 929
    label "kr&#281;ty"
  ]
  node [
    id 930
    label "poja&#347;nia&#263;"
  ]
  node [
    id 931
    label "robi&#263;"
  ]
  node [
    id 932
    label "u&#322;atwia&#263;"
  ]
  node [
    id 933
    label "elaborate"
  ]
  node [
    id 934
    label "suplikowa&#263;"
  ]
  node [
    id 935
    label "przek&#322;ada&#263;"
  ]
  node [
    id 936
    label "przekonywa&#263;"
  ]
  node [
    id 937
    label "interpretowa&#263;"
  ]
  node [
    id 938
    label "broni&#263;"
  ]
  node [
    id 939
    label "explain"
  ]
  node [
    id 940
    label "sprawowa&#263;"
  ]
  node [
    id 941
    label "uzasadnia&#263;"
  ]
  node [
    id 942
    label "wiedzie&#263;"
  ]
  node [
    id 943
    label "kuma&#263;"
  ]
  node [
    id 944
    label "czu&#263;"
  ]
  node [
    id 945
    label "dziama&#263;"
  ]
  node [
    id 946
    label "match"
  ]
  node [
    id 947
    label "empatia"
  ]
  node [
    id 948
    label "see"
  ]
  node [
    id 949
    label "zna&#263;"
  ]
  node [
    id 950
    label "gaworzy&#263;"
  ]
  node [
    id 951
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 952
    label "rozmawia&#263;"
  ]
  node [
    id 953
    label "wyra&#380;a&#263;"
  ]
  node [
    id 954
    label "umie&#263;"
  ]
  node [
    id 955
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 956
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 957
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 958
    label "talk"
  ]
  node [
    id 959
    label "u&#380;ywa&#263;"
  ]
  node [
    id 960
    label "prawi&#263;"
  ]
  node [
    id 961
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 962
    label "powiada&#263;"
  ]
  node [
    id 963
    label "tell"
  ]
  node [
    id 964
    label "chew_the_fat"
  ]
  node [
    id 965
    label "say"
  ]
  node [
    id 966
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 967
    label "informowa&#263;"
  ]
  node [
    id 968
    label "wydobywa&#263;"
  ]
  node [
    id 969
    label "okre&#347;la&#263;"
  ]
  node [
    id 970
    label "hermeneutyka"
  ]
  node [
    id 971
    label "kontekst"
  ]
  node [
    id 972
    label "apprehension"
  ]
  node [
    id 973
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 974
    label "interpretation"
  ]
  node [
    id 975
    label "czucie"
  ]
  node [
    id 976
    label "realization"
  ]
  node [
    id 977
    label "kumanie"
  ]
  node [
    id 978
    label "wnioskowanie"
  ]
  node [
    id 979
    label "nadawanie"
  ]
  node [
    id 980
    label "precyzowanie"
  ]
  node [
    id 981
    label "formalny"
  ]
  node [
    id 982
    label "validate"
  ]
  node [
    id 983
    label "nadawa&#263;"
  ]
  node [
    id 984
    label "precyzowa&#263;"
  ]
  node [
    id 985
    label "salt_lick"
  ]
  node [
    id 986
    label "dotyka&#263;"
  ]
  node [
    id 987
    label "muska&#263;"
  ]
  node [
    id 988
    label "wada_wrodzona"
  ]
  node [
    id 989
    label "dotykanie"
  ]
  node [
    id 990
    label "przesuwanie"
  ]
  node [
    id 991
    label "g&#322;askanie"
  ]
  node [
    id 992
    label "zlizanie"
  ]
  node [
    id 993
    label "wylizywanie"
  ]
  node [
    id 994
    label "zlizywanie"
  ]
  node [
    id 995
    label "wylizanie"
  ]
  node [
    id 996
    label "usta"
  ]
  node [
    id 997
    label "&#347;lina"
  ]
  node [
    id 998
    label "pi&#263;"
  ]
  node [
    id 999
    label "sponge"
  ]
  node [
    id 1000
    label "mleko"
  ]
  node [
    id 1001
    label "rozpuszcza&#263;"
  ]
  node [
    id 1002
    label "wci&#261;ga&#263;"
  ]
  node [
    id 1003
    label "rusza&#263;"
  ]
  node [
    id 1004
    label "sucking"
  ]
  node [
    id 1005
    label "smoczek"
  ]
  node [
    id 1006
    label "znami&#281;"
  ]
  node [
    id 1007
    label "krosta"
  ]
  node [
    id 1008
    label "schorzenie"
  ]
  node [
    id 1009
    label "brodawka"
  ]
  node [
    id 1010
    label "pip"
  ]
  node [
    id 1011
    label "picie"
  ]
  node [
    id 1012
    label "ruszanie"
  ]
  node [
    id 1013
    label "consumption"
  ]
  node [
    id 1014
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 1015
    label "rozpuszczanie"
  ]
  node [
    id 1016
    label "aspiration"
  ]
  node [
    id 1017
    label "wci&#261;ganie"
  ]
  node [
    id 1018
    label "odci&#261;ganie"
  ]
  node [
    id 1019
    label "wessanie"
  ]
  node [
    id 1020
    label "ga&#378;nik"
  ]
  node [
    id 1021
    label "wysysanie"
  ]
  node [
    id 1022
    label "wyssanie"
  ]
  node [
    id 1023
    label "okre&#347;lony"
  ]
  node [
    id 1024
    label "jaki&#347;"
  ]
  node [
    id 1025
    label "przyzwoity"
  ]
  node [
    id 1026
    label "ciekawy"
  ]
  node [
    id 1027
    label "jako&#347;"
  ]
  node [
    id 1028
    label "jako_tako"
  ]
  node [
    id 1029
    label "niez&#322;y"
  ]
  node [
    id 1030
    label "wiadomy"
  ]
  node [
    id 1031
    label "kobieta_sukcesu"
  ]
  node [
    id 1032
    label "success"
  ]
  node [
    id 1033
    label "rezultat"
  ]
  node [
    id 1034
    label "passa"
  ]
  node [
    id 1035
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1036
    label "uzyskanie"
  ]
  node [
    id 1037
    label "dochrapanie_si&#281;"
  ]
  node [
    id 1038
    label "skill"
  ]
  node [
    id 1039
    label "accomplishment"
  ]
  node [
    id 1040
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1041
    label "zaawansowanie"
  ]
  node [
    id 1042
    label "dotarcie"
  ]
  node [
    id 1043
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 1044
    label "dzia&#322;anie"
  ]
  node [
    id 1045
    label "typ"
  ]
  node [
    id 1046
    label "event"
  ]
  node [
    id 1047
    label "przyczyna"
  ]
  node [
    id 1048
    label "przebieg"
  ]
  node [
    id 1049
    label "pora&#380;ka"
  ]
  node [
    id 1050
    label "continuum"
  ]
  node [
    id 1051
    label "ci&#261;g&#322;o&#347;&#263;"
  ]
  node [
    id 1052
    label "zafundowa&#263;"
  ]
  node [
    id 1053
    label "budowla"
  ]
  node [
    id 1054
    label "wyda&#263;"
  ]
  node [
    id 1055
    label "plant"
  ]
  node [
    id 1056
    label "uruchomi&#263;"
  ]
  node [
    id 1057
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 1058
    label "pozostawi&#263;"
  ]
  node [
    id 1059
    label "obra&#263;"
  ]
  node [
    id 1060
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 1061
    label "obstawi&#263;"
  ]
  node [
    id 1062
    label "post"
  ]
  node [
    id 1063
    label "wyznaczy&#263;"
  ]
  node [
    id 1064
    label "stanowisko"
  ]
  node [
    id 1065
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 1066
    label "uczyni&#263;"
  ]
  node [
    id 1067
    label "znak"
  ]
  node [
    id 1068
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 1069
    label "wytworzy&#263;"
  ]
  node [
    id 1070
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 1071
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1072
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 1073
    label "wskaza&#263;"
  ]
  node [
    id 1074
    label "przyzna&#263;"
  ]
  node [
    id 1075
    label "wydoby&#263;"
  ]
  node [
    id 1076
    label "establish"
  ]
  node [
    id 1077
    label "stawi&#263;"
  ]
  node [
    id 1078
    label "create"
  ]
  node [
    id 1079
    label "specjalista_od_public_relations"
  ]
  node [
    id 1080
    label "wizerunek"
  ]
  node [
    id 1081
    label "przygotowa&#263;"
  ]
  node [
    id 1082
    label "zakomunikowa&#263;"
  ]
  node [
    id 1083
    label "testify"
  ]
  node [
    id 1084
    label "przytacha&#263;"
  ]
  node [
    id 1085
    label "yield"
  ]
  node [
    id 1086
    label "zanie&#347;&#263;"
  ]
  node [
    id 1087
    label "inform"
  ]
  node [
    id 1088
    label "poinformowa&#263;"
  ]
  node [
    id 1089
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 1090
    label "denounce"
  ]
  node [
    id 1091
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 1092
    label "serve"
  ]
  node [
    id 1093
    label "trzonek"
  ]
  node [
    id 1094
    label "zachowanie"
  ]
  node [
    id 1095
    label "dyscyplina_sportowa"
  ]
  node [
    id 1096
    label "handle"
  ]
  node [
    id 1097
    label "stroke"
  ]
  node [
    id 1098
    label "line"
  ]
  node [
    id 1099
    label "kanon"
  ]
  node [
    id 1100
    label "behawior"
  ]
  node [
    id 1101
    label "t&#322;oczysko"
  ]
  node [
    id 1102
    label "depesza"
  ]
  node [
    id 1103
    label "maszyna"
  ]
  node [
    id 1104
    label "dziennikarz_prasowy"
  ]
  node [
    id 1105
    label "kiosk"
  ]
  node [
    id 1106
    label "maszyna_rolnicza"
  ]
  node [
    id 1107
    label "gazeta"
  ]
  node [
    id 1108
    label "rodzaj_literacki"
  ]
  node [
    id 1109
    label "drama"
  ]
  node [
    id 1110
    label "cios"
  ]
  node [
    id 1111
    label "literatura"
  ]
  node [
    id 1112
    label "animatronika"
  ]
  node [
    id 1113
    label "odczulenie"
  ]
  node [
    id 1114
    label "odczula&#263;"
  ]
  node [
    id 1115
    label "blik"
  ]
  node [
    id 1116
    label "odczuli&#263;"
  ]
  node [
    id 1117
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 1118
    label "block"
  ]
  node [
    id 1119
    label "trawiarnia"
  ]
  node [
    id 1120
    label "sklejarka"
  ]
  node [
    id 1121
    label "uj&#281;cie"
  ]
  node [
    id 1122
    label "filmoteka"
  ]
  node [
    id 1123
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 1124
    label "klatka"
  ]
  node [
    id 1125
    label "rozbieg&#243;wka"
  ]
  node [
    id 1126
    label "napisy"
  ]
  node [
    id 1127
    label "ta&#347;ma"
  ]
  node [
    id 1128
    label "odczulanie"
  ]
  node [
    id 1129
    label "anamorfoza"
  ]
  node [
    id 1130
    label "ty&#322;&#243;wka"
  ]
  node [
    id 1131
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 1132
    label "b&#322;ona"
  ]
  node [
    id 1133
    label "emulsja_fotograficzna"
  ]
  node [
    id 1134
    label "photograph"
  ]
  node [
    id 1135
    label "czo&#322;&#243;wka"
  ]
  node [
    id 1136
    label "blok"
  ]
  node [
    id 1137
    label "time"
  ]
  node [
    id 1138
    label "shot"
  ]
  node [
    id 1139
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 1140
    label "uderzenie"
  ]
  node [
    id 1141
    label "struktura_geologiczna"
  ]
  node [
    id 1142
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 1143
    label "pr&#243;ba"
  ]
  node [
    id 1144
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 1145
    label "coup"
  ]
  node [
    id 1146
    label "siekacz"
  ]
  node [
    id 1147
    label "larp"
  ]
  node [
    id 1148
    label "gra_RPG"
  ]
  node [
    id 1149
    label "rozgrywka"
  ]
  node [
    id 1150
    label "psychoterapia_Gestalt"
  ]
  node [
    id 1151
    label "psychoterapia"
  ]
  node [
    id 1152
    label "metoda_edukacyjna"
  ]
  node [
    id 1153
    label "pisarstwo"
  ]
  node [
    id 1154
    label "liryka"
  ]
  node [
    id 1155
    label "amorfizm"
  ]
  node [
    id 1156
    label "bibliografia"
  ]
  node [
    id 1157
    label "epika"
  ]
  node [
    id 1158
    label "translator"
  ]
  node [
    id 1159
    label "pi&#347;miennictwo"
  ]
  node [
    id 1160
    label "zoologia_fantastyczna"
  ]
  node [
    id 1161
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1162
    label "mie&#263;_miejsce"
  ]
  node [
    id 1163
    label "equal"
  ]
  node [
    id 1164
    label "trwa&#263;"
  ]
  node [
    id 1165
    label "chodzi&#263;"
  ]
  node [
    id 1166
    label "si&#281;ga&#263;"
  ]
  node [
    id 1167
    label "obecno&#347;&#263;"
  ]
  node [
    id 1168
    label "stand"
  ]
  node [
    id 1169
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1170
    label "uczestniczy&#263;"
  ]
  node [
    id 1171
    label "participate"
  ]
  node [
    id 1172
    label "istnie&#263;"
  ]
  node [
    id 1173
    label "pozostawa&#263;"
  ]
  node [
    id 1174
    label "zostawa&#263;"
  ]
  node [
    id 1175
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1176
    label "adhere"
  ]
  node [
    id 1177
    label "compass"
  ]
  node [
    id 1178
    label "korzysta&#263;"
  ]
  node [
    id 1179
    label "appreciation"
  ]
  node [
    id 1180
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1181
    label "dociera&#263;"
  ]
  node [
    id 1182
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1183
    label "mierzy&#263;"
  ]
  node [
    id 1184
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1185
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1186
    label "exsert"
  ]
  node [
    id 1187
    label "being"
  ]
  node [
    id 1188
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1189
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1190
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1191
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1192
    label "run"
  ]
  node [
    id 1193
    label "bangla&#263;"
  ]
  node [
    id 1194
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1195
    label "przebiega&#263;"
  ]
  node [
    id 1196
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1197
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1198
    label "carry"
  ]
  node [
    id 1199
    label "bywa&#263;"
  ]
  node [
    id 1200
    label "para"
  ]
  node [
    id 1201
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1202
    label "str&#243;j"
  ]
  node [
    id 1203
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1204
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1205
    label "krok"
  ]
  node [
    id 1206
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1207
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1208
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1209
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1210
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1211
    label "continue"
  ]
  node [
    id 1212
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1213
    label "Ohio"
  ]
  node [
    id 1214
    label "wci&#281;cie"
  ]
  node [
    id 1215
    label "Nowy_York"
  ]
  node [
    id 1216
    label "warstwa"
  ]
  node [
    id 1217
    label "samopoczucie"
  ]
  node [
    id 1218
    label "Illinois"
  ]
  node [
    id 1219
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1220
    label "state"
  ]
  node [
    id 1221
    label "Jukatan"
  ]
  node [
    id 1222
    label "Kalifornia"
  ]
  node [
    id 1223
    label "Wirginia"
  ]
  node [
    id 1224
    label "wektor"
  ]
  node [
    id 1225
    label "Teksas"
  ]
  node [
    id 1226
    label "Goa"
  ]
  node [
    id 1227
    label "Waszyngton"
  ]
  node [
    id 1228
    label "Massachusetts"
  ]
  node [
    id 1229
    label "Alaska"
  ]
  node [
    id 1230
    label "Arakan"
  ]
  node [
    id 1231
    label "Hawaje"
  ]
  node [
    id 1232
    label "Maryland"
  ]
  node [
    id 1233
    label "punkt"
  ]
  node [
    id 1234
    label "Michigan"
  ]
  node [
    id 1235
    label "Arizona"
  ]
  node [
    id 1236
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1237
    label "Georgia"
  ]
  node [
    id 1238
    label "poziom"
  ]
  node [
    id 1239
    label "Pensylwania"
  ]
  node [
    id 1240
    label "shape"
  ]
  node [
    id 1241
    label "Luizjana"
  ]
  node [
    id 1242
    label "Nowy_Meksyk"
  ]
  node [
    id 1243
    label "Alabama"
  ]
  node [
    id 1244
    label "Kansas"
  ]
  node [
    id 1245
    label "Oregon"
  ]
  node [
    id 1246
    label "Floryda"
  ]
  node [
    id 1247
    label "Oklahoma"
  ]
  node [
    id 1248
    label "jednostka_administracyjna"
  ]
  node [
    id 1249
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1250
    label "niebezpiecznie"
  ]
  node [
    id 1251
    label "gro&#378;ny"
  ]
  node [
    id 1252
    label "k&#322;opotliwy"
  ]
  node [
    id 1253
    label "k&#322;opotliwie"
  ]
  node [
    id 1254
    label "gro&#378;nie"
  ]
  node [
    id 1255
    label "nieprzyjemny"
  ]
  node [
    id 1256
    label "niewygodny"
  ]
  node [
    id 1257
    label "nad&#261;sany"
  ]
  node [
    id 1258
    label "trwanie"
  ]
  node [
    id 1259
    label "wra&#380;enie"
  ]
  node [
    id 1260
    label "przej&#347;cie"
  ]
  node [
    id 1261
    label "doznanie"
  ]
  node [
    id 1262
    label "poradzenie_sobie"
  ]
  node [
    id 1263
    label "przetrwanie"
  ]
  node [
    id 1264
    label "survival"
  ]
  node [
    id 1265
    label "przechodzenie"
  ]
  node [
    id 1266
    label "wytrzymywanie"
  ]
  node [
    id 1267
    label "obejrzenie"
  ]
  node [
    id 1268
    label "widzenie"
  ]
  node [
    id 1269
    label "urzeczywistnianie"
  ]
  node [
    id 1270
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1271
    label "przeszkodzenie"
  ]
  node [
    id 1272
    label "produkowanie"
  ]
  node [
    id 1273
    label "znikni&#281;cie"
  ]
  node [
    id 1274
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 1275
    label "przeszkadzanie"
  ]
  node [
    id 1276
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1277
    label "wyprodukowanie"
  ]
  node [
    id 1278
    label "utrzymywanie"
  ]
  node [
    id 1279
    label "subsystencja"
  ]
  node [
    id 1280
    label "utrzyma&#263;"
  ]
  node [
    id 1281
    label "egzystencja"
  ]
  node [
    id 1282
    label "wy&#380;ywienie"
  ]
  node [
    id 1283
    label "ontologicznie"
  ]
  node [
    id 1284
    label "utrzymanie"
  ]
  node [
    id 1285
    label "utrzymywa&#263;"
  ]
  node [
    id 1286
    label "status"
  ]
  node [
    id 1287
    label "sytuacja"
  ]
  node [
    id 1288
    label "poprzedzanie"
  ]
  node [
    id 1289
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1290
    label "laba"
  ]
  node [
    id 1291
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1292
    label "chronometria"
  ]
  node [
    id 1293
    label "rachuba_czasu"
  ]
  node [
    id 1294
    label "przep&#322;ywanie"
  ]
  node [
    id 1295
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1296
    label "czasokres"
  ]
  node [
    id 1297
    label "odczyt"
  ]
  node [
    id 1298
    label "chwila"
  ]
  node [
    id 1299
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1300
    label "dzieje"
  ]
  node [
    id 1301
    label "poprzedzenie"
  ]
  node [
    id 1302
    label "trawienie"
  ]
  node [
    id 1303
    label "pochodzi&#263;"
  ]
  node [
    id 1304
    label "period"
  ]
  node [
    id 1305
    label "okres_czasu"
  ]
  node [
    id 1306
    label "poprzedza&#263;"
  ]
  node [
    id 1307
    label "schy&#322;ek"
  ]
  node [
    id 1308
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1309
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1310
    label "zegar"
  ]
  node [
    id 1311
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1312
    label "czwarty_wymiar"
  ]
  node [
    id 1313
    label "pochodzenie"
  ]
  node [
    id 1314
    label "koniugacja"
  ]
  node [
    id 1315
    label "Zeitgeist"
  ]
  node [
    id 1316
    label "trawi&#263;"
  ]
  node [
    id 1317
    label "pogoda"
  ]
  node [
    id 1318
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1319
    label "poprzedzi&#263;"
  ]
  node [
    id 1320
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1321
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1322
    label "time_period"
  ]
  node [
    id 1323
    label "ocieranie_si&#281;"
  ]
  node [
    id 1324
    label "otoczenie_si&#281;"
  ]
  node [
    id 1325
    label "posiedzenie"
  ]
  node [
    id 1326
    label "otarcie_si&#281;"
  ]
  node [
    id 1327
    label "atakowanie"
  ]
  node [
    id 1328
    label "otaczanie_si&#281;"
  ]
  node [
    id 1329
    label "zmierzanie"
  ]
  node [
    id 1330
    label "residency"
  ]
  node [
    id 1331
    label "sojourn"
  ]
  node [
    id 1332
    label "wychodzenie"
  ]
  node [
    id 1333
    label "tkwienie"
  ]
  node [
    id 1334
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1335
    label "absolutorium"
  ]
  node [
    id 1336
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1337
    label "ton"
  ]
  node [
    id 1338
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 1339
    label "korkowanie"
  ]
  node [
    id 1340
    label "death"
  ]
  node [
    id 1341
    label "zabijanie"
  ]
  node [
    id 1342
    label "martwy"
  ]
  node [
    id 1343
    label "przestawanie"
  ]
  node [
    id 1344
    label "odumieranie"
  ]
  node [
    id 1345
    label "zdychanie"
  ]
  node [
    id 1346
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 1347
    label "zanikanie"
  ]
  node [
    id 1348
    label "ko&#324;czenie"
  ]
  node [
    id 1349
    label "nieuleczalnie_chory"
  ]
  node [
    id 1350
    label "szybki"
  ]
  node [
    id 1351
    label "&#380;ywotny"
  ]
  node [
    id 1352
    label "naturalny"
  ]
  node [
    id 1353
    label "&#380;ywo"
  ]
  node [
    id 1354
    label "o&#380;ywianie"
  ]
  node [
    id 1355
    label "silny"
  ]
  node [
    id 1356
    label "g&#322;&#281;boki"
  ]
  node [
    id 1357
    label "wyra&#378;ny"
  ]
  node [
    id 1358
    label "czynny"
  ]
  node [
    id 1359
    label "aktualny"
  ]
  node [
    id 1360
    label "zgrabny"
  ]
  node [
    id 1361
    label "prawdziwy"
  ]
  node [
    id 1362
    label "realistyczny"
  ]
  node [
    id 1363
    label "energiczny"
  ]
  node [
    id 1364
    label "odumarcie"
  ]
  node [
    id 1365
    label "przestanie"
  ]
  node [
    id 1366
    label "dysponowanie_si&#281;"
  ]
  node [
    id 1367
    label "pomarcie"
  ]
  node [
    id 1368
    label "die"
  ]
  node [
    id 1369
    label "sko&#324;czenie"
  ]
  node [
    id 1370
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 1371
    label "zdechni&#281;cie"
  ]
  node [
    id 1372
    label "zabicie"
  ]
  node [
    id 1373
    label "procedura"
  ]
  node [
    id 1374
    label "proces_biologiczny"
  ]
  node [
    id 1375
    label "z&#322;ote_czasy"
  ]
  node [
    id 1376
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 1377
    label "process"
  ]
  node [
    id 1378
    label "cycle"
  ]
  node [
    id 1379
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 1380
    label "adolescence"
  ]
  node [
    id 1381
    label "wiek"
  ]
  node [
    id 1382
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 1383
    label "zielone_lata"
  ]
  node [
    id 1384
    label "zlec"
  ]
  node [
    id 1385
    label "zlegni&#281;cie"
  ]
  node [
    id 1386
    label "defenestracja"
  ]
  node [
    id 1387
    label "agonia"
  ]
  node [
    id 1388
    label "kres"
  ]
  node [
    id 1389
    label "mogi&#322;a"
  ]
  node [
    id 1390
    label "kres_&#380;ycia"
  ]
  node [
    id 1391
    label "upadek"
  ]
  node [
    id 1392
    label "szeol"
  ]
  node [
    id 1393
    label "pogrzebanie"
  ]
  node [
    id 1394
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1395
    label "&#380;a&#322;oba"
  ]
  node [
    id 1396
    label "pogrzeb"
  ]
  node [
    id 1397
    label "majority"
  ]
  node [
    id 1398
    label "osiemnastoletni"
  ]
  node [
    id 1399
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1400
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1401
    label "age"
  ]
  node [
    id 1402
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 1403
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1404
    label "zapa&#322;"
  ]
  node [
    id 1405
    label "ciura"
  ]
  node [
    id 1406
    label "miernota"
  ]
  node [
    id 1407
    label "g&#243;wno"
  ]
  node [
    id 1408
    label "love"
  ]
  node [
    id 1409
    label "brak"
  ]
  node [
    id 1410
    label "nieistnienie"
  ]
  node [
    id 1411
    label "defect"
  ]
  node [
    id 1412
    label "gap"
  ]
  node [
    id 1413
    label "kr&#243;tki"
  ]
  node [
    id 1414
    label "wada"
  ]
  node [
    id 1415
    label "odchodzi&#263;"
  ]
  node [
    id 1416
    label "odchodzenie"
  ]
  node [
    id 1417
    label "prywatywny"
  ]
  node [
    id 1418
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 1419
    label "tandetno&#347;&#263;"
  ]
  node [
    id 1420
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 1421
    label "ka&#322;"
  ]
  node [
    id 1422
    label "zero"
  ]
  node [
    id 1423
    label "drobiazg"
  ]
  node [
    id 1424
    label "chor&#261;&#380;y"
  ]
  node [
    id 1425
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 1426
    label "sprawi&#263;"
  ]
  node [
    id 1427
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1428
    label "come_up"
  ]
  node [
    id 1429
    label "przej&#347;&#263;"
  ]
  node [
    id 1430
    label "straci&#263;"
  ]
  node [
    id 1431
    label "zyska&#263;"
  ]
  node [
    id 1432
    label "bomber"
  ]
  node [
    id 1433
    label "wyrobi&#263;"
  ]
  node [
    id 1434
    label "wzi&#261;&#263;"
  ]
  node [
    id 1435
    label "ustawa"
  ]
  node [
    id 1436
    label "podlec"
  ]
  node [
    id 1437
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 1438
    label "min&#261;&#263;"
  ]
  node [
    id 1439
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 1440
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 1441
    label "zaliczy&#263;"
  ]
  node [
    id 1442
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1443
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 1444
    label "przeby&#263;"
  ]
  node [
    id 1445
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1446
    label "dozna&#263;"
  ]
  node [
    id 1447
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1448
    label "zacz&#261;&#263;"
  ]
  node [
    id 1449
    label "happen"
  ]
  node [
    id 1450
    label "pass"
  ]
  node [
    id 1451
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 1452
    label "beat"
  ]
  node [
    id 1453
    label "mienie"
  ]
  node [
    id 1454
    label "absorb"
  ]
  node [
    id 1455
    label "pique"
  ]
  node [
    id 1456
    label "przesta&#263;"
  ]
  node [
    id 1457
    label "stracenie"
  ]
  node [
    id 1458
    label "leave_office"
  ]
  node [
    id 1459
    label "zabi&#263;"
  ]
  node [
    id 1460
    label "forfeit"
  ]
  node [
    id 1461
    label "wytraci&#263;"
  ]
  node [
    id 1462
    label "waste"
  ]
  node [
    id 1463
    label "przegra&#263;"
  ]
  node [
    id 1464
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 1465
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 1466
    label "execute"
  ]
  node [
    id 1467
    label "omin&#261;&#263;"
  ]
  node [
    id 1468
    label "pozyska&#263;"
  ]
  node [
    id 1469
    label "utilize"
  ]
  node [
    id 1470
    label "naby&#263;"
  ]
  node [
    id 1471
    label "uzyska&#263;"
  ]
  node [
    id 1472
    label "poprawi&#263;_si&#281;"
  ]
  node [
    id 1473
    label "receive"
  ]
  node [
    id 1474
    label "obfituj&#261;cy"
  ]
  node [
    id 1475
    label "nabab"
  ]
  node [
    id 1476
    label "r&#243;&#380;norodny"
  ]
  node [
    id 1477
    label "spania&#322;y"
  ]
  node [
    id 1478
    label "obficie"
  ]
  node [
    id 1479
    label "sytuowany"
  ]
  node [
    id 1480
    label "och&#281;do&#380;ny"
  ]
  node [
    id 1481
    label "forsiasty"
  ]
  node [
    id 1482
    label "zapa&#347;ny"
  ]
  node [
    id 1483
    label "bogato"
  ]
  node [
    id 1484
    label "och&#281;do&#380;nie"
  ]
  node [
    id 1485
    label "porz&#261;dny"
  ]
  node [
    id 1486
    label "ch&#281;dogo"
  ]
  node [
    id 1487
    label "smaczny"
  ]
  node [
    id 1488
    label "&#347;wietny"
  ]
  node [
    id 1489
    label "wspania&#322;y"
  ]
  node [
    id 1490
    label "rezerwowy"
  ]
  node [
    id 1491
    label "zapa&#347;nie"
  ]
  node [
    id 1492
    label "zapasowy"
  ]
  node [
    id 1493
    label "urz&#281;dnik"
  ]
  node [
    id 1494
    label "bogacz"
  ]
  node [
    id 1495
    label "zarz&#261;dca"
  ]
  node [
    id 1496
    label "dostojnik"
  ]
  node [
    id 1497
    label "obfito"
  ]
  node [
    id 1498
    label "obfity"
  ]
  node [
    id 1499
    label "pe&#322;no"
  ]
  node [
    id 1500
    label "r&#243;&#380;norodnie"
  ]
  node [
    id 1501
    label "intensywnie"
  ]
  node [
    id 1502
    label "poka&#378;ny"
  ]
  node [
    id 1503
    label "r&#243;&#380;ny"
  ]
  node [
    id 1504
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 1505
    label "pe&#322;ny"
  ]
  node [
    id 1506
    label "zm&#261;drzenie"
  ]
  node [
    id 1507
    label "m&#261;drzenie"
  ]
  node [
    id 1508
    label "m&#261;drze"
  ]
  node [
    id 1509
    label "skomplikowany"
  ]
  node [
    id 1510
    label "m&#261;drzenie_si&#281;"
  ]
  node [
    id 1511
    label "pyszny"
  ]
  node [
    id 1512
    label "inteligentny"
  ]
  node [
    id 1513
    label "dobry"
  ]
  node [
    id 1514
    label "stawanie_si&#281;"
  ]
  node [
    id 1515
    label "stanie_si&#281;"
  ]
  node [
    id 1516
    label "dobrze"
  ]
  node [
    id 1517
    label "skomplikowanie"
  ]
  node [
    id 1518
    label "inteligentnie"
  ]
  node [
    id 1519
    label "my&#347;l&#261;cy"
  ]
  node [
    id 1520
    label "zmy&#347;lny"
  ]
  node [
    id 1521
    label "wysokich_lot&#243;w"
  ]
  node [
    id 1522
    label "wynios&#322;y"
  ]
  node [
    id 1523
    label "dufny"
  ]
  node [
    id 1524
    label "rozd&#281;cie_si&#281;"
  ]
  node [
    id 1525
    label "napuszanie_si&#281;"
  ]
  node [
    id 1526
    label "pysznie"
  ]
  node [
    id 1527
    label "podufa&#322;y"
  ]
  node [
    id 1528
    label "rozdymanie_si&#281;"
  ]
  node [
    id 1529
    label "udany"
  ]
  node [
    id 1530
    label "przesmaczny"
  ]
  node [
    id 1531
    label "napuszenie_si&#281;"
  ]
  node [
    id 1532
    label "przedni"
  ]
  node [
    id 1533
    label "dobroczynny"
  ]
  node [
    id 1534
    label "czw&#243;rka"
  ]
  node [
    id 1535
    label "spokojny"
  ]
  node [
    id 1536
    label "skuteczny"
  ]
  node [
    id 1537
    label "&#347;mieszny"
  ]
  node [
    id 1538
    label "mi&#322;y"
  ]
  node [
    id 1539
    label "grzeczny"
  ]
  node [
    id 1540
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1541
    label "powitanie"
  ]
  node [
    id 1542
    label "ca&#322;y"
  ]
  node [
    id 1543
    label "zwrot"
  ]
  node [
    id 1544
    label "pomy&#347;lny"
  ]
  node [
    id 1545
    label "moralny"
  ]
  node [
    id 1546
    label "drogi"
  ]
  node [
    id 1547
    label "pozytywny"
  ]
  node [
    id 1548
    label "odpowiedni"
  ]
  node [
    id 1549
    label "korzystny"
  ]
  node [
    id 1550
    label "pos&#322;uszny"
  ]
  node [
    id 1551
    label "trudny"
  ]
  node [
    id 1552
    label "wypi&#281;knienie"
  ]
  node [
    id 1553
    label "skandaliczny"
  ]
  node [
    id 1554
    label "szlachetnie"
  ]
  node [
    id 1555
    label "z&#322;y"
  ]
  node [
    id 1556
    label "gor&#261;cy"
  ]
  node [
    id 1557
    label "pi&#281;knie"
  ]
  node [
    id 1558
    label "pi&#281;knienie"
  ]
  node [
    id 1559
    label "wzruszaj&#261;cy"
  ]
  node [
    id 1560
    label "po&#380;&#261;dany"
  ]
  node [
    id 1561
    label "cudowny"
  ]
  node [
    id 1562
    label "okaza&#322;y"
  ]
  node [
    id 1563
    label "stresogenny"
  ]
  node [
    id 1564
    label "szczery"
  ]
  node [
    id 1565
    label "rozpalenie_si&#281;"
  ]
  node [
    id 1566
    label "zdecydowany"
  ]
  node [
    id 1567
    label "sensacyjny"
  ]
  node [
    id 1568
    label "rozpalanie_si&#281;"
  ]
  node [
    id 1569
    label "na_gor&#261;co"
  ]
  node [
    id 1570
    label "&#380;arki"
  ]
  node [
    id 1571
    label "serdeczny"
  ]
  node [
    id 1572
    label "ciep&#322;y"
  ]
  node [
    id 1573
    label "gor&#261;co"
  ]
  node [
    id 1574
    label "seksowny"
  ]
  node [
    id 1575
    label "&#347;wie&#380;y"
  ]
  node [
    id 1576
    label "wzruszaj&#261;co"
  ]
  node [
    id 1577
    label "przejmuj&#261;cy"
  ]
  node [
    id 1578
    label "wspaniale"
  ]
  node [
    id 1579
    label "&#347;wietnie"
  ]
  node [
    id 1580
    label "warto&#347;ciowy"
  ]
  node [
    id 1581
    label "zajebisty"
  ]
  node [
    id 1582
    label "okazale"
  ]
  node [
    id 1583
    label "imponuj&#261;cy"
  ]
  node [
    id 1584
    label "cudownie"
  ]
  node [
    id 1585
    label "fantastyczny"
  ]
  node [
    id 1586
    label "cudnie"
  ]
  node [
    id 1587
    label "przewspania&#322;y"
  ]
  node [
    id 1588
    label "niezwyk&#322;y"
  ]
  node [
    id 1589
    label "pieski"
  ]
  node [
    id 1590
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 1591
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 1592
    label "niekorzystny"
  ]
  node [
    id 1593
    label "z&#322;oszczenie"
  ]
  node [
    id 1594
    label "sierdzisty"
  ]
  node [
    id 1595
    label "niegrzeczny"
  ]
  node [
    id 1596
    label "zez&#322;oszczenie"
  ]
  node [
    id 1597
    label "zdenerwowany"
  ]
  node [
    id 1598
    label "negatywny"
  ]
  node [
    id 1599
    label "rozgniewanie"
  ]
  node [
    id 1600
    label "gniewanie"
  ]
  node [
    id 1601
    label "niemoralny"
  ]
  node [
    id 1602
    label "&#378;le"
  ]
  node [
    id 1603
    label "niepomy&#347;lny"
  ]
  node [
    id 1604
    label "syf"
  ]
  node [
    id 1605
    label "straszny"
  ]
  node [
    id 1606
    label "skandalicznie"
  ]
  node [
    id 1607
    label "gorsz&#261;cy"
  ]
  node [
    id 1608
    label "szlachetny"
  ]
  node [
    id 1609
    label "beautifully"
  ]
  node [
    id 1610
    label "zacnie"
  ]
  node [
    id 1611
    label "estetycznie"
  ]
  node [
    id 1612
    label "stylowy"
  ]
  node [
    id 1613
    label "harmonijnie"
  ]
  node [
    id 1614
    label "gatunkowo"
  ]
  node [
    id 1615
    label "stale"
  ]
  node [
    id 1616
    label "ci&#261;g&#322;y"
  ]
  node [
    id 1617
    label "sta&#322;y"
  ]
  node [
    id 1618
    label "ci&#261;gle"
  ]
  node [
    id 1619
    label "nieprzerwany"
  ]
  node [
    id 1620
    label "nieustanny"
  ]
  node [
    id 1621
    label "zawsze"
  ]
  node [
    id 1622
    label "zwykle"
  ]
  node [
    id 1623
    label "jednakowo"
  ]
  node [
    id 1624
    label "konsument"
  ]
  node [
    id 1625
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 1626
    label "cz&#322;owiekowate"
  ]
  node [
    id 1627
    label "istota_&#380;ywa"
  ]
  node [
    id 1628
    label "pracownik"
  ]
  node [
    id 1629
    label "Chocho&#322;"
  ]
  node [
    id 1630
    label "Herkules_Poirot"
  ]
  node [
    id 1631
    label "Edyp"
  ]
  node [
    id 1632
    label "parali&#380;owa&#263;"
  ]
  node [
    id 1633
    label "Harry_Potter"
  ]
  node [
    id 1634
    label "Casanova"
  ]
  node [
    id 1635
    label "Zgredek"
  ]
  node [
    id 1636
    label "Gargantua"
  ]
  node [
    id 1637
    label "Winnetou"
  ]
  node [
    id 1638
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 1639
    label "Dulcynea"
  ]
  node [
    id 1640
    label "person"
  ]
  node [
    id 1641
    label "Plastu&#347;"
  ]
  node [
    id 1642
    label "Quasimodo"
  ]
  node [
    id 1643
    label "Sherlock_Holmes"
  ]
  node [
    id 1644
    label "Wallenrod"
  ]
  node [
    id 1645
    label "Dwukwiat"
  ]
  node [
    id 1646
    label "Don_Juan"
  ]
  node [
    id 1647
    label "Don_Kiszot"
  ]
  node [
    id 1648
    label "Hamlet"
  ]
  node [
    id 1649
    label "Werter"
  ]
  node [
    id 1650
    label "Szwejk"
  ]
  node [
    id 1651
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 1652
    label "jajko"
  ]
  node [
    id 1653
    label "rodzic"
  ]
  node [
    id 1654
    label "wapniaki"
  ]
  node [
    id 1655
    label "zwierzchnik"
  ]
  node [
    id 1656
    label "feuda&#322;"
  ]
  node [
    id 1657
    label "starzec"
  ]
  node [
    id 1658
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 1659
    label "zawodnik"
  ]
  node [
    id 1660
    label "komendancja"
  ]
  node [
    id 1661
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 1662
    label "absorption"
  ]
  node [
    id 1663
    label "pobieranie"
  ]
  node [
    id 1664
    label "czerpanie"
  ]
  node [
    id 1665
    label "acquisition"
  ]
  node [
    id 1666
    label "zmienianie"
  ]
  node [
    id 1667
    label "assimilation"
  ]
  node [
    id 1668
    label "upodabnianie"
  ]
  node [
    id 1669
    label "g&#322;oska"
  ]
  node [
    id 1670
    label "grupa"
  ]
  node [
    id 1671
    label "suppress"
  ]
  node [
    id 1672
    label "os&#322;abienie"
  ]
  node [
    id 1673
    label "os&#322;abi&#263;"
  ]
  node [
    id 1674
    label "powodowa&#263;"
  ]
  node [
    id 1675
    label "zmniejsza&#263;"
  ]
  node [
    id 1676
    label "bate"
  ]
  node [
    id 1677
    label "de-escalation"
  ]
  node [
    id 1678
    label "powodowanie"
  ]
  node [
    id 1679
    label "debilitation"
  ]
  node [
    id 1680
    label "zmniejszanie"
  ]
  node [
    id 1681
    label "s&#322;abszy"
  ]
  node [
    id 1682
    label "pogarszanie"
  ]
  node [
    id 1683
    label "assimilate"
  ]
  node [
    id 1684
    label "dostosowywa&#263;"
  ]
  node [
    id 1685
    label "dostosowa&#263;"
  ]
  node [
    id 1686
    label "upodobni&#263;"
  ]
  node [
    id 1687
    label "upodabnia&#263;"
  ]
  node [
    id 1688
    label "pobiera&#263;"
  ]
  node [
    id 1689
    label "pobra&#263;"
  ]
  node [
    id 1690
    label "zapis"
  ]
  node [
    id 1691
    label "figure"
  ]
  node [
    id 1692
    label "mildew"
  ]
  node [
    id 1693
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 1694
    label "ideal"
  ]
  node [
    id 1695
    label "rule"
  ]
  node [
    id 1696
    label "ruch"
  ]
  node [
    id 1697
    label "dekal"
  ]
  node [
    id 1698
    label "motyw"
  ]
  node [
    id 1699
    label "projekt"
  ]
  node [
    id 1700
    label "charakterystyka"
  ]
  node [
    id 1701
    label "zaistnie&#263;"
  ]
  node [
    id 1702
    label "Osjan"
  ]
  node [
    id 1703
    label "kto&#347;"
  ]
  node [
    id 1704
    label "wygl&#261;d"
  ]
  node [
    id 1705
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1706
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1707
    label "trim"
  ]
  node [
    id 1708
    label "poby&#263;"
  ]
  node [
    id 1709
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1710
    label "Aspazja"
  ]
  node [
    id 1711
    label "punkt_widzenia"
  ]
  node [
    id 1712
    label "kompleksja"
  ]
  node [
    id 1713
    label "wytrzyma&#263;"
  ]
  node [
    id 1714
    label "formacja"
  ]
  node [
    id 1715
    label "point"
  ]
  node [
    id 1716
    label "go&#347;&#263;"
  ]
  node [
    id 1717
    label "fotograf"
  ]
  node [
    id 1718
    label "malarz"
  ]
  node [
    id 1719
    label "artysta"
  ]
  node [
    id 1720
    label "hipnotyzowanie"
  ]
  node [
    id 1721
    label "&#347;lad"
  ]
  node [
    id 1722
    label "docieranie"
  ]
  node [
    id 1723
    label "natural_process"
  ]
  node [
    id 1724
    label "reakcja_chemiczna"
  ]
  node [
    id 1725
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1726
    label "lobbysta"
  ]
  node [
    id 1727
    label "pryncypa&#322;"
  ]
  node [
    id 1728
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1729
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1730
    label "wiedza"
  ]
  node [
    id 1731
    label "kierowa&#263;"
  ]
  node [
    id 1732
    label "alkohol"
  ]
  node [
    id 1733
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1734
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1735
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1736
    label "dekiel"
  ]
  node [
    id 1737
    label "ro&#347;lina"
  ]
  node [
    id 1738
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1739
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1740
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1741
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1742
    label "noosfera"
  ]
  node [
    id 1743
    label "byd&#322;o"
  ]
  node [
    id 1744
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1745
    label "makrocefalia"
  ]
  node [
    id 1746
    label "ucho"
  ]
  node [
    id 1747
    label "g&#243;ra"
  ]
  node [
    id 1748
    label "m&#243;zg"
  ]
  node [
    id 1749
    label "kierownictwo"
  ]
  node [
    id 1750
    label "fryzura"
  ]
  node [
    id 1751
    label "umys&#322;"
  ]
  node [
    id 1752
    label "cia&#322;o"
  ]
  node [
    id 1753
    label "cz&#322;onek"
  ]
  node [
    id 1754
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1755
    label "czaszka"
  ]
  node [
    id 1756
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1757
    label "allochoria"
  ]
  node [
    id 1758
    label "p&#322;aszczyzna"
  ]
  node [
    id 1759
    label "bierka_szachowa"
  ]
  node [
    id 1760
    label "obiekt_matematyczny"
  ]
  node [
    id 1761
    label "gestaltyzm"
  ]
  node [
    id 1762
    label "obraz"
  ]
  node [
    id 1763
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1764
    label "character"
  ]
  node [
    id 1765
    label "rze&#378;ba"
  ]
  node [
    id 1766
    label "stylistyka"
  ]
  node [
    id 1767
    label "antycypacja"
  ]
  node [
    id 1768
    label "ornamentyka"
  ]
  node [
    id 1769
    label "informacja"
  ]
  node [
    id 1770
    label "facet"
  ]
  node [
    id 1771
    label "popis"
  ]
  node [
    id 1772
    label "wiersz"
  ]
  node [
    id 1773
    label "symetria"
  ]
  node [
    id 1774
    label "lingwistyka_kognitywna"
  ]
  node [
    id 1775
    label "karta"
  ]
  node [
    id 1776
    label "podzbi&#243;r"
  ]
  node [
    id 1777
    label "perspektywa"
  ]
  node [
    id 1778
    label "dziedzina"
  ]
  node [
    id 1779
    label "nak&#322;adka"
  ]
  node [
    id 1780
    label "li&#347;&#263;"
  ]
  node [
    id 1781
    label "jama_gard&#322;owa"
  ]
  node [
    id 1782
    label "rezonator"
  ]
  node [
    id 1783
    label "podstawa"
  ]
  node [
    id 1784
    label "base"
  ]
  node [
    id 1785
    label "piek&#322;o"
  ]
  node [
    id 1786
    label "human_body"
  ]
  node [
    id 1787
    label "ofiarowywanie"
  ]
  node [
    id 1788
    label "sfera_afektywna"
  ]
  node [
    id 1789
    label "nekromancja"
  ]
  node [
    id 1790
    label "Po&#347;wist"
  ]
  node [
    id 1791
    label "podekscytowanie"
  ]
  node [
    id 1792
    label "deformowanie"
  ]
  node [
    id 1793
    label "sumienie"
  ]
  node [
    id 1794
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1795
    label "deformowa&#263;"
  ]
  node [
    id 1796
    label "psychika"
  ]
  node [
    id 1797
    label "zjawa"
  ]
  node [
    id 1798
    label "zmar&#322;y"
  ]
  node [
    id 1799
    label "ofiarowywa&#263;"
  ]
  node [
    id 1800
    label "oddech"
  ]
  node [
    id 1801
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1802
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1803
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1804
    label "ego"
  ]
  node [
    id 1805
    label "ofiarowanie"
  ]
  node [
    id 1806
    label "fizjonomia"
  ]
  node [
    id 1807
    label "kompleks"
  ]
  node [
    id 1808
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1809
    label "T&#281;sknica"
  ]
  node [
    id 1810
    label "ofiarowa&#263;"
  ]
  node [
    id 1811
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1812
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1813
    label "passion"
  ]
  node [
    id 1814
    label "atom"
  ]
  node [
    id 1815
    label "odbicie"
  ]
  node [
    id 1816
    label "przyroda"
  ]
  node [
    id 1817
    label "Ziemia"
  ]
  node [
    id 1818
    label "kosmos"
  ]
  node [
    id 1819
    label "miniatura"
  ]
  node [
    id 1820
    label "wcze&#347;nie"
  ]
  node [
    id 1821
    label "wczesny"
  ]
  node [
    id 1822
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1823
    label "gra_planszowa"
  ]
  node [
    id 1824
    label "liczy&#263;"
  ]
  node [
    id 1825
    label "prize"
  ]
  node [
    id 1826
    label "appreciate"
  ]
  node [
    id 1827
    label "szanowa&#263;"
  ]
  node [
    id 1828
    label "ustala&#263;"
  ]
  node [
    id 1829
    label "uznawa&#263;"
  ]
  node [
    id 1830
    label "unwrap"
  ]
  node [
    id 1831
    label "decydowa&#263;"
  ]
  node [
    id 1832
    label "zmienia&#263;"
  ]
  node [
    id 1833
    label "umacnia&#263;"
  ]
  node [
    id 1834
    label "arrange"
  ]
  node [
    id 1835
    label "os&#261;dza&#263;"
  ]
  node [
    id 1836
    label "consider"
  ]
  node [
    id 1837
    label "notice"
  ]
  node [
    id 1838
    label "stwierdza&#263;"
  ]
  node [
    id 1839
    label "przyznawa&#263;"
  ]
  node [
    id 1840
    label "powa&#380;anie"
  ]
  node [
    id 1841
    label "treasure"
  ]
  node [
    id 1842
    label "respektowa&#263;"
  ]
  node [
    id 1843
    label "chowa&#263;"
  ]
  node [
    id 1844
    label "dyskalkulia"
  ]
  node [
    id 1845
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1846
    label "wynagrodzenie"
  ]
  node [
    id 1847
    label "wymienia&#263;"
  ]
  node [
    id 1848
    label "posiada&#263;"
  ]
  node [
    id 1849
    label "wycenia&#263;"
  ]
  node [
    id 1850
    label "bra&#263;"
  ]
  node [
    id 1851
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 1852
    label "rachowa&#263;"
  ]
  node [
    id 1853
    label "count"
  ]
  node [
    id 1854
    label "odlicza&#263;"
  ]
  node [
    id 1855
    label "dodawa&#263;"
  ]
  node [
    id 1856
    label "wyznacza&#263;"
  ]
  node [
    id 1857
    label "admit"
  ]
  node [
    id 1858
    label "policza&#263;"
  ]
  node [
    id 1859
    label "surogator"
  ]
  node [
    id 1860
    label "Machiavelli"
  ]
  node [
    id 1861
    label "Sienkiewicz"
  ]
  node [
    id 1862
    label "Andersen"
  ]
  node [
    id 1863
    label "Katon"
  ]
  node [
    id 1864
    label "Gogol"
  ]
  node [
    id 1865
    label "pisarczyk"
  ]
  node [
    id 1866
    label "Iwaszkiewicz"
  ]
  node [
    id 1867
    label "Gombrowicz"
  ]
  node [
    id 1868
    label "Proust"
  ]
  node [
    id 1869
    label "Boy-&#379;ele&#324;ski"
  ]
  node [
    id 1870
    label "Thomas_Mann"
  ]
  node [
    id 1871
    label "Balzak"
  ]
  node [
    id 1872
    label "Reymont"
  ]
  node [
    id 1873
    label "Walter_Scott"
  ]
  node [
    id 1874
    label "Stendhal"
  ]
  node [
    id 1875
    label "Juliusz_Cezar"
  ]
  node [
    id 1876
    label "Flaubert"
  ]
  node [
    id 1877
    label "Tolkien"
  ]
  node [
    id 1878
    label "Brecht"
  ]
  node [
    id 1879
    label "To&#322;stoj"
  ]
  node [
    id 1880
    label "Lem"
  ]
  node [
    id 1881
    label "Orwell"
  ]
  node [
    id 1882
    label "Kafka"
  ]
  node [
    id 1883
    label "Conrad"
  ]
  node [
    id 1884
    label "Voltaire"
  ]
  node [
    id 1885
    label "Bergson"
  ]
  node [
    id 1886
    label "zamilkn&#261;&#263;"
  ]
  node [
    id 1887
    label "mistrz"
  ]
  node [
    id 1888
    label "autor"
  ]
  node [
    id 1889
    label "zamilkni&#281;cie"
  ]
  node [
    id 1890
    label "korpus_urz&#281;dniczy"
  ]
  node [
    id 1891
    label "pragmatyka"
  ]
  node [
    id 1892
    label "Wilko"
  ]
  node [
    id 1893
    label "renesans"
  ]
  node [
    id 1894
    label "bergsonista"
  ]
  node [
    id 1895
    label "p&#281;d_&#380;yciowy"
  ]
  node [
    id 1896
    label "fantastyka"
  ]
  node [
    id 1897
    label "Anglia"
  ]
  node [
    id 1898
    label "ch&#322;opiec"
  ]
  node [
    id 1899
    label "gryzipi&#243;rek"
  ]
  node [
    id 1900
    label "starosta_grodowy"
  ]
  node [
    id 1901
    label "zast&#281;pca"
  ]
  node [
    id 1902
    label "Homer"
  ]
  node [
    id 1903
    label "Puszkin"
  ]
  node [
    id 1904
    label "Horacy"
  ]
  node [
    id 1905
    label "Le&#347;mian"
  ]
  node [
    id 1906
    label "Rej"
  ]
  node [
    id 1907
    label "Schiller"
  ]
  node [
    id 1908
    label "Norwid"
  ]
  node [
    id 1909
    label "R&#243;&#380;ewicz"
  ]
  node [
    id 1910
    label "Herbert"
  ]
  node [
    id 1911
    label "Tuwim"
  ]
  node [
    id 1912
    label "Zagajewski"
  ]
  node [
    id 1913
    label "Bara&#324;czak"
  ]
  node [
    id 1914
    label "Pindar"
  ]
  node [
    id 1915
    label "Jesienin"
  ]
  node [
    id 1916
    label "Jan_Czeczot"
  ]
  node [
    id 1917
    label "Byron"
  ]
  node [
    id 1918
    label "Asnyk"
  ]
  node [
    id 1919
    label "Bia&#322;oszewski"
  ]
  node [
    id 1920
    label "wierszopis"
  ]
  node [
    id 1921
    label "Mi&#322;osz"
  ]
  node [
    id 1922
    label "Dawid"
  ]
  node [
    id 1923
    label "Dante"
  ]
  node [
    id 1924
    label "Peiper"
  ]
  node [
    id 1925
    label "grafoman"
  ]
  node [
    id 1926
    label "poecina"
  ]
  node [
    id 1927
    label "szko&#322;a"
  ]
  node [
    id 1928
    label "zwrotnicowy"
  ]
  node [
    id 1929
    label "ima&#380;ynizm"
  ]
  node [
    id 1930
    label "poezja"
  ]
  node [
    id 1931
    label "epos"
  ]
  node [
    id 1932
    label "ponie&#347;&#263;"
  ]
  node [
    id 1933
    label "doda&#263;"
  ]
  node [
    id 1934
    label "increase"
  ]
  node [
    id 1935
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 1936
    label "pokry&#263;"
  ]
  node [
    id 1937
    label "zakry&#263;"
  ]
  node [
    id 1938
    label "convey"
  ]
  node [
    id 1939
    label "dostarczy&#263;"
  ]
  node [
    id 1940
    label "powierzy&#263;"
  ]
  node [
    id 1941
    label "obieca&#263;"
  ]
  node [
    id 1942
    label "pozwoli&#263;"
  ]
  node [
    id 1943
    label "odst&#261;pi&#263;"
  ]
  node [
    id 1944
    label "przywali&#263;"
  ]
  node [
    id 1945
    label "wyrzec_si&#281;"
  ]
  node [
    id 1946
    label "sztachn&#261;&#263;"
  ]
  node [
    id 1947
    label "rap"
  ]
  node [
    id 1948
    label "feed"
  ]
  node [
    id 1949
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 1950
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 1951
    label "udost&#281;pni&#263;"
  ]
  node [
    id 1952
    label "przeznaczy&#263;"
  ]
  node [
    id 1953
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 1954
    label "zada&#263;"
  ]
  node [
    id 1955
    label "dress"
  ]
  node [
    id 1956
    label "przekaza&#263;"
  ]
  node [
    id 1957
    label "supply"
  ]
  node [
    id 1958
    label "zap&#322;aci&#263;"
  ]
  node [
    id 1959
    label "riot"
  ]
  node [
    id 1960
    label "wst&#261;pi&#263;"
  ]
  node [
    id 1961
    label "porwa&#263;"
  ]
  node [
    id 1962
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1963
    label "tenis"
  ]
  node [
    id 1964
    label "siatk&#243;wka"
  ]
  node [
    id 1965
    label "introduce"
  ]
  node [
    id 1966
    label "nafaszerowa&#263;"
  ]
  node [
    id 1967
    label "zaserwowa&#263;"
  ]
  node [
    id 1968
    label "ascend"
  ]
  node [
    id 1969
    label "nada&#263;"
  ]
  node [
    id 1970
    label "policzy&#263;"
  ]
  node [
    id 1971
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 1972
    label "complete"
  ]
  node [
    id 1973
    label "belfer"
  ]
  node [
    id 1974
    label "murza"
  ]
  node [
    id 1975
    label "ojciec"
  ]
  node [
    id 1976
    label "samiec"
  ]
  node [
    id 1977
    label "androlog"
  ]
  node [
    id 1978
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 1979
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 1980
    label "efendi"
  ]
  node [
    id 1981
    label "opiekun"
  ]
  node [
    id 1982
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 1983
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 1984
    label "bratek"
  ]
  node [
    id 1985
    label "Mieszko_I"
  ]
  node [
    id 1986
    label "Midas"
  ]
  node [
    id 1987
    label "m&#261;&#380;"
  ]
  node [
    id 1988
    label "popularyzator"
  ]
  node [
    id 1989
    label "pracodawca"
  ]
  node [
    id 1990
    label "kszta&#322;ciciel"
  ]
  node [
    id 1991
    label "preceptor"
  ]
  node [
    id 1992
    label "pupil"
  ]
  node [
    id 1993
    label "przyw&#243;dca"
  ]
  node [
    id 1994
    label "pedagog"
  ]
  node [
    id 1995
    label "rz&#261;dzenie"
  ]
  node [
    id 1996
    label "jegomo&#347;&#263;"
  ]
  node [
    id 1997
    label "szkolnik"
  ]
  node [
    id 1998
    label "ch&#322;opina"
  ]
  node [
    id 1999
    label "w&#322;odarz"
  ]
  node [
    id 2000
    label "profesor"
  ]
  node [
    id 2001
    label "gra_w_karty"
  ]
  node [
    id 2002
    label "w&#322;adza"
  ]
  node [
    id 2003
    label "Fidel_Castro"
  ]
  node [
    id 2004
    label "Anders"
  ]
  node [
    id 2005
    label "Ko&#347;ciuszko"
  ]
  node [
    id 2006
    label "Tito"
  ]
  node [
    id 2007
    label "Miko&#322;ajczyk"
  ]
  node [
    id 2008
    label "Sabataj_Cwi"
  ]
  node [
    id 2009
    label "lider"
  ]
  node [
    id 2010
    label "Mao"
  ]
  node [
    id 2011
    label "p&#322;atnik"
  ]
  node [
    id 2012
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 2013
    label "nadzorca"
  ]
  node [
    id 2014
    label "funkcjonariusz"
  ]
  node [
    id 2015
    label "podmiot"
  ]
  node [
    id 2016
    label "wykupienie"
  ]
  node [
    id 2017
    label "bycie_w_posiadaniu"
  ]
  node [
    id 2018
    label "wykupywanie"
  ]
  node [
    id 2019
    label "rozszerzyciel"
  ]
  node [
    id 2020
    label "wydoro&#347;lenie"
  ]
  node [
    id 2021
    label "du&#380;y"
  ]
  node [
    id 2022
    label "doro&#347;lenie"
  ]
  node [
    id 2023
    label "&#378;ra&#322;y"
  ]
  node [
    id 2024
    label "doro&#347;le"
  ]
  node [
    id 2025
    label "dojrzale"
  ]
  node [
    id 2026
    label "dojrza&#322;y"
  ]
  node [
    id 2027
    label "doletni"
  ]
  node [
    id 2028
    label "turning"
  ]
  node [
    id 2029
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 2030
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 2031
    label "skr&#281;t"
  ]
  node [
    id 2032
    label "obr&#243;t"
  ]
  node [
    id 2033
    label "fraza_czasownikowa"
  ]
  node [
    id 2034
    label "jednostka_leksykalna"
  ]
  node [
    id 2035
    label "zmiana"
  ]
  node [
    id 2036
    label "wyra&#380;enie"
  ]
  node [
    id 2037
    label "starosta"
  ]
  node [
    id 2038
    label "w&#322;adca"
  ]
  node [
    id 2039
    label "nauczyciel"
  ]
  node [
    id 2040
    label "wyprawka"
  ]
  node [
    id 2041
    label "mundurek"
  ]
  node [
    id 2042
    label "tarcza"
  ]
  node [
    id 2043
    label "elew"
  ]
  node [
    id 2044
    label "absolwent"
  ]
  node [
    id 2045
    label "klasa"
  ]
  node [
    id 2046
    label "stopie&#324;_naukowy"
  ]
  node [
    id 2047
    label "nauczyciel_akademicki"
  ]
  node [
    id 2048
    label "tytu&#322;"
  ]
  node [
    id 2049
    label "profesura"
  ]
  node [
    id 2050
    label "konsulent"
  ]
  node [
    id 2051
    label "wirtuoz"
  ]
  node [
    id 2052
    label "ekspert"
  ]
  node [
    id 2053
    label "ochotnik"
  ]
  node [
    id 2054
    label "pomocnik"
  ]
  node [
    id 2055
    label "student"
  ]
  node [
    id 2056
    label "nauczyciel_muzyki"
  ]
  node [
    id 2057
    label "zakonnik"
  ]
  node [
    id 2058
    label "mo&#347;&#263;"
  ]
  node [
    id 2059
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 2060
    label "kuwada"
  ]
  node [
    id 2061
    label "tworzyciel"
  ]
  node [
    id 2062
    label "rodzice"
  ]
  node [
    id 2063
    label "&#347;w"
  ]
  node [
    id 2064
    label "pomys&#322;odawca"
  ]
  node [
    id 2065
    label "wykonawca"
  ]
  node [
    id 2066
    label "ojczym"
  ]
  node [
    id 2067
    label "przodek"
  ]
  node [
    id 2068
    label "papa"
  ]
  node [
    id 2069
    label "stary"
  ]
  node [
    id 2070
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 2071
    label "kochanek"
  ]
  node [
    id 2072
    label "fio&#322;ek"
  ]
  node [
    id 2073
    label "brat"
  ]
  node [
    id 2074
    label "ma&#322;&#380;onek"
  ]
  node [
    id 2075
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 2076
    label "m&#243;j"
  ]
  node [
    id 2077
    label "ch&#322;op"
  ]
  node [
    id 2078
    label "pan_m&#322;ody"
  ]
  node [
    id 2079
    label "&#347;lubny"
  ]
  node [
    id 2080
    label "pan_domu"
  ]
  node [
    id 2081
    label "pan_i_w&#322;adca"
  ]
  node [
    id 2082
    label "Frygia"
  ]
  node [
    id 2083
    label "sprawowanie"
  ]
  node [
    id 2084
    label "dominion"
  ]
  node [
    id 2085
    label "dominowanie"
  ]
  node [
    id 2086
    label "reign"
  ]
  node [
    id 2087
    label "zwierz&#281;_domowe"
  ]
  node [
    id 2088
    label "J&#281;drzejewicz"
  ]
  node [
    id 2089
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 2090
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 2091
    label "John_Dewey"
  ]
  node [
    id 2092
    label "specjalista"
  ]
  node [
    id 2093
    label "Turek"
  ]
  node [
    id 2094
    label "effendi"
  ]
  node [
    id 2095
    label "Katar"
  ]
  node [
    id 2096
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 2097
    label "Libia"
  ]
  node [
    id 2098
    label "Gwatemala"
  ]
  node [
    id 2099
    label "Afganistan"
  ]
  node [
    id 2100
    label "Ekwador"
  ]
  node [
    id 2101
    label "Tad&#380;ykistan"
  ]
  node [
    id 2102
    label "Bhutan"
  ]
  node [
    id 2103
    label "Argentyna"
  ]
  node [
    id 2104
    label "D&#380;ibuti"
  ]
  node [
    id 2105
    label "Wenezuela"
  ]
  node [
    id 2106
    label "Ukraina"
  ]
  node [
    id 2107
    label "Gabon"
  ]
  node [
    id 2108
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 2109
    label "Rwanda"
  ]
  node [
    id 2110
    label "Liechtenstein"
  ]
  node [
    id 2111
    label "organizacja"
  ]
  node [
    id 2112
    label "Sri_Lanka"
  ]
  node [
    id 2113
    label "Madagaskar"
  ]
  node [
    id 2114
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 2115
    label "Tonga"
  ]
  node [
    id 2116
    label "Kongo"
  ]
  node [
    id 2117
    label "Bangladesz"
  ]
  node [
    id 2118
    label "Kanada"
  ]
  node [
    id 2119
    label "Wehrlen"
  ]
  node [
    id 2120
    label "Algieria"
  ]
  node [
    id 2121
    label "Surinam"
  ]
  node [
    id 2122
    label "Chile"
  ]
  node [
    id 2123
    label "Sahara_Zachodnia"
  ]
  node [
    id 2124
    label "Uganda"
  ]
  node [
    id 2125
    label "W&#281;gry"
  ]
  node [
    id 2126
    label "Birma"
  ]
  node [
    id 2127
    label "Kazachstan"
  ]
  node [
    id 2128
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 2129
    label "Armenia"
  ]
  node [
    id 2130
    label "Tuwalu"
  ]
  node [
    id 2131
    label "Timor_Wschodni"
  ]
  node [
    id 2132
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 2133
    label "Izrael"
  ]
  node [
    id 2134
    label "Estonia"
  ]
  node [
    id 2135
    label "Komory"
  ]
  node [
    id 2136
    label "Kamerun"
  ]
  node [
    id 2137
    label "Haiti"
  ]
  node [
    id 2138
    label "Belize"
  ]
  node [
    id 2139
    label "Sierra_Leone"
  ]
  node [
    id 2140
    label "Luksemburg"
  ]
  node [
    id 2141
    label "USA"
  ]
  node [
    id 2142
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 2143
    label "Barbados"
  ]
  node [
    id 2144
    label "San_Marino"
  ]
  node [
    id 2145
    label "Bu&#322;garia"
  ]
  node [
    id 2146
    label "Wietnam"
  ]
  node [
    id 2147
    label "Indonezja"
  ]
  node [
    id 2148
    label "Malawi"
  ]
  node [
    id 2149
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 2150
    label "Francja"
  ]
  node [
    id 2151
    label "partia"
  ]
  node [
    id 2152
    label "Zambia"
  ]
  node [
    id 2153
    label "Angola"
  ]
  node [
    id 2154
    label "Grenada"
  ]
  node [
    id 2155
    label "Nepal"
  ]
  node [
    id 2156
    label "Panama"
  ]
  node [
    id 2157
    label "Rumunia"
  ]
  node [
    id 2158
    label "Czarnog&#243;ra"
  ]
  node [
    id 2159
    label "Malediwy"
  ]
  node [
    id 2160
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 2161
    label "S&#322;owacja"
  ]
  node [
    id 2162
    label "Egipt"
  ]
  node [
    id 2163
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 2164
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 2165
    label "Kolumbia"
  ]
  node [
    id 2166
    label "Mozambik"
  ]
  node [
    id 2167
    label "Laos"
  ]
  node [
    id 2168
    label "Burundi"
  ]
  node [
    id 2169
    label "Suazi"
  ]
  node [
    id 2170
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 2171
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 2172
    label "Czechy"
  ]
  node [
    id 2173
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 2174
    label "Wyspy_Marshalla"
  ]
  node [
    id 2175
    label "Trynidad_i_Tobago"
  ]
  node [
    id 2176
    label "Dominika"
  ]
  node [
    id 2177
    label "Palau"
  ]
  node [
    id 2178
    label "Syria"
  ]
  node [
    id 2179
    label "Gwinea_Bissau"
  ]
  node [
    id 2180
    label "Liberia"
  ]
  node [
    id 2181
    label "Zimbabwe"
  ]
  node [
    id 2182
    label "Polska"
  ]
  node [
    id 2183
    label "Jamajka"
  ]
  node [
    id 2184
    label "Dominikana"
  ]
  node [
    id 2185
    label "Senegal"
  ]
  node [
    id 2186
    label "Gruzja"
  ]
  node [
    id 2187
    label "Togo"
  ]
  node [
    id 2188
    label "Chorwacja"
  ]
  node [
    id 2189
    label "Meksyk"
  ]
  node [
    id 2190
    label "Macedonia"
  ]
  node [
    id 2191
    label "Gujana"
  ]
  node [
    id 2192
    label "Zair"
  ]
  node [
    id 2193
    label "Albania"
  ]
  node [
    id 2194
    label "Kambod&#380;a"
  ]
  node [
    id 2195
    label "Mauritius"
  ]
  node [
    id 2196
    label "Monako"
  ]
  node [
    id 2197
    label "Gwinea"
  ]
  node [
    id 2198
    label "Mali"
  ]
  node [
    id 2199
    label "Nigeria"
  ]
  node [
    id 2200
    label "Kostaryka"
  ]
  node [
    id 2201
    label "Hanower"
  ]
  node [
    id 2202
    label "Paragwaj"
  ]
  node [
    id 2203
    label "W&#322;ochy"
  ]
  node [
    id 2204
    label "Wyspy_Salomona"
  ]
  node [
    id 2205
    label "Seszele"
  ]
  node [
    id 2206
    label "Hiszpania"
  ]
  node [
    id 2207
    label "Boliwia"
  ]
  node [
    id 2208
    label "Kirgistan"
  ]
  node [
    id 2209
    label "Irlandia"
  ]
  node [
    id 2210
    label "Czad"
  ]
  node [
    id 2211
    label "Irak"
  ]
  node [
    id 2212
    label "Lesoto"
  ]
  node [
    id 2213
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 2214
    label "Malta"
  ]
  node [
    id 2215
    label "Andora"
  ]
  node [
    id 2216
    label "Chiny"
  ]
  node [
    id 2217
    label "Filipiny"
  ]
  node [
    id 2218
    label "Antarktis"
  ]
  node [
    id 2219
    label "Niemcy"
  ]
  node [
    id 2220
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 2221
    label "Brazylia"
  ]
  node [
    id 2222
    label "terytorium"
  ]
  node [
    id 2223
    label "Nikaragua"
  ]
  node [
    id 2224
    label "Pakistan"
  ]
  node [
    id 2225
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 2226
    label "Kenia"
  ]
  node [
    id 2227
    label "Niger"
  ]
  node [
    id 2228
    label "Tunezja"
  ]
  node [
    id 2229
    label "Portugalia"
  ]
  node [
    id 2230
    label "Fid&#380;i"
  ]
  node [
    id 2231
    label "Maroko"
  ]
  node [
    id 2232
    label "Botswana"
  ]
  node [
    id 2233
    label "Tajlandia"
  ]
  node [
    id 2234
    label "Australia"
  ]
  node [
    id 2235
    label "Burkina_Faso"
  ]
  node [
    id 2236
    label "interior"
  ]
  node [
    id 2237
    label "Benin"
  ]
  node [
    id 2238
    label "Tanzania"
  ]
  node [
    id 2239
    label "Indie"
  ]
  node [
    id 2240
    label "&#321;otwa"
  ]
  node [
    id 2241
    label "Kiribati"
  ]
  node [
    id 2242
    label "Antigua_i_Barbuda"
  ]
  node [
    id 2243
    label "Rodezja"
  ]
  node [
    id 2244
    label "Cypr"
  ]
  node [
    id 2245
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 2246
    label "Peru"
  ]
  node [
    id 2247
    label "Austria"
  ]
  node [
    id 2248
    label "Urugwaj"
  ]
  node [
    id 2249
    label "Jordania"
  ]
  node [
    id 2250
    label "Grecja"
  ]
  node [
    id 2251
    label "Azerbejd&#380;an"
  ]
  node [
    id 2252
    label "Turcja"
  ]
  node [
    id 2253
    label "Samoa"
  ]
  node [
    id 2254
    label "Sudan"
  ]
  node [
    id 2255
    label "Oman"
  ]
  node [
    id 2256
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 2257
    label "Uzbekistan"
  ]
  node [
    id 2258
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 2259
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 2260
    label "Honduras"
  ]
  node [
    id 2261
    label "Mongolia"
  ]
  node [
    id 2262
    label "Portoryko"
  ]
  node [
    id 2263
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 2264
    label "Serbia"
  ]
  node [
    id 2265
    label "Tajwan"
  ]
  node [
    id 2266
    label "Wielka_Brytania"
  ]
  node [
    id 2267
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 2268
    label "Liban"
  ]
  node [
    id 2269
    label "Japonia"
  ]
  node [
    id 2270
    label "Ghana"
  ]
  node [
    id 2271
    label "Bahrajn"
  ]
  node [
    id 2272
    label "Belgia"
  ]
  node [
    id 2273
    label "Etiopia"
  ]
  node [
    id 2274
    label "Mikronezja"
  ]
  node [
    id 2275
    label "Kuwejt"
  ]
  node [
    id 2276
    label "Bahamy"
  ]
  node [
    id 2277
    label "Rosja"
  ]
  node [
    id 2278
    label "Mo&#322;dawia"
  ]
  node [
    id 2279
    label "Litwa"
  ]
  node [
    id 2280
    label "S&#322;owenia"
  ]
  node [
    id 2281
    label "Szwajcaria"
  ]
  node [
    id 2282
    label "Erytrea"
  ]
  node [
    id 2283
    label "Kuba"
  ]
  node [
    id 2284
    label "Arabia_Saudyjska"
  ]
  node [
    id 2285
    label "granica_pa&#324;stwa"
  ]
  node [
    id 2286
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 2287
    label "Malezja"
  ]
  node [
    id 2288
    label "Korea"
  ]
  node [
    id 2289
    label "Jemen"
  ]
  node [
    id 2290
    label "Nowa_Zelandia"
  ]
  node [
    id 2291
    label "Namibia"
  ]
  node [
    id 2292
    label "Nauru"
  ]
  node [
    id 2293
    label "holoarktyka"
  ]
  node [
    id 2294
    label "Brunei"
  ]
  node [
    id 2295
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 2296
    label "Khitai"
  ]
  node [
    id 2297
    label "Mauretania"
  ]
  node [
    id 2298
    label "Iran"
  ]
  node [
    id 2299
    label "Gambia"
  ]
  node [
    id 2300
    label "Somalia"
  ]
  node [
    id 2301
    label "Holandia"
  ]
  node [
    id 2302
    label "Turkmenistan"
  ]
  node [
    id 2303
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 2304
    label "Salwador"
  ]
  node [
    id 2305
    label "renoma"
  ]
  node [
    id 2306
    label "rozg&#322;os"
  ]
  node [
    id 2307
    label "sensacja"
  ]
  node [
    id 2308
    label "popularno&#347;&#263;"
  ]
  node [
    id 2309
    label "opinia"
  ]
  node [
    id 2310
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 2311
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 2312
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 2313
    label "internationalization"
  ]
  node [
    id 2314
    label "transgraniczny"
  ]
  node [
    id 2315
    label "uwsp&#243;lnienie"
  ]
  node [
    id 2316
    label "zbiorowo"
  ]
  node [
    id 2317
    label "udost&#281;pnianie"
  ]
  node [
    id 2318
    label "J&#225;nosem"
  ]
  node [
    id 2319
    label "H&#225;yem"
  ]
  node [
    id 2320
    label "G&#233;za"
  ]
  node [
    id 2321
    label "TVP"
  ]
  node [
    id 2322
    label "2"
  ]
  node [
    id 2323
    label "J&#193;NOS"
  ]
  node [
    id 2324
    label "H&#193;Y"
  ]
  node [
    id 2325
    label "G&#233;z&#281;"
  ]
  node [
    id 2326
    label "Sara"
  ]
  node [
    id 2327
    label "Kane"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 2320
  ]
  edge [
    source 8
    target 2325
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 2321
  ]
  edge [
    source 9
    target 2322
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 2321
  ]
  edge [
    source 10
    target 2322
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 42
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 200
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 203
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 204
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 205
  ]
  edge [
    source 12
    target 206
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 208
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 259
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 267
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 570
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 571
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 574
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 14
    target 577
  ]
  edge [
    source 14
    target 578
  ]
  edge [
    source 14
    target 579
  ]
  edge [
    source 14
    target 580
  ]
  edge [
    source 14
    target 581
  ]
  edge [
    source 14
    target 582
  ]
  edge [
    source 14
    target 583
  ]
  edge [
    source 14
    target 584
  ]
  edge [
    source 14
    target 585
  ]
  edge [
    source 14
    target 404
  ]
  edge [
    source 14
    target 586
  ]
  edge [
    source 14
    target 587
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 589
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 591
  ]
  edge [
    source 14
    target 237
  ]
  edge [
    source 14
    target 592
  ]
  edge [
    source 14
    target 593
  ]
  edge [
    source 14
    target 594
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 596
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 14
    target 75
  ]
  edge [
    source 14
    target 279
  ]
  edge [
    source 14
    target 600
  ]
  edge [
    source 14
    target 601
  ]
  edge [
    source 14
    target 316
  ]
  edge [
    source 14
    target 602
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 34
  ]
  edge [
    source 14
    target 626
  ]
  edge [
    source 14
    target 627
  ]
  edge [
    source 14
    target 628
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 14
    target 630
  ]
  edge [
    source 14
    target 631
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 632
  ]
  edge [
    source 14
    target 633
  ]
  edge [
    source 14
    target 634
  ]
  edge [
    source 14
    target 635
  ]
  edge [
    source 14
    target 636
  ]
  edge [
    source 14
    target 637
  ]
  edge [
    source 14
    target 638
  ]
  edge [
    source 14
    target 639
  ]
  edge [
    source 14
    target 640
  ]
  edge [
    source 14
    target 641
  ]
  edge [
    source 14
    target 642
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 643
  ]
  edge [
    source 14
    target 644
  ]
  edge [
    source 14
    target 645
  ]
  edge [
    source 14
    target 646
  ]
  edge [
    source 14
    target 647
  ]
  edge [
    source 14
    target 648
  ]
  edge [
    source 14
    target 649
  ]
  edge [
    source 14
    target 650
  ]
  edge [
    source 14
    target 651
  ]
  edge [
    source 14
    target 652
  ]
  edge [
    source 14
    target 653
  ]
  edge [
    source 14
    target 654
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 14
    target 657
  ]
  edge [
    source 14
    target 658
  ]
  edge [
    source 14
    target 659
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 661
  ]
  edge [
    source 14
    target 268
  ]
  edge [
    source 14
    target 662
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 478
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 633
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 614
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 627
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 294
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 710
  ]
  edge [
    source 19
    target 711
  ]
  edge [
    source 19
    target 712
  ]
  edge [
    source 19
    target 713
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 714
  ]
  edge [
    source 19
    target 715
  ]
  edge [
    source 19
    target 716
  ]
  edge [
    source 19
    target 717
  ]
  edge [
    source 19
    target 718
  ]
  edge [
    source 19
    target 720
  ]
  edge [
    source 19
    target 719
  ]
  edge [
    source 19
    target 721
  ]
  edge [
    source 19
    target 722
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 723
  ]
  edge [
    source 19
    target 724
  ]
  edge [
    source 19
    target 725
  ]
  edge [
    source 19
    target 726
  ]
  edge [
    source 19
    target 727
  ]
  edge [
    source 19
    target 728
  ]
  edge [
    source 19
    target 729
  ]
  edge [
    source 19
    target 730
  ]
  edge [
    source 19
    target 731
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 732
  ]
  edge [
    source 19
    target 733
  ]
  edge [
    source 19
    target 734
  ]
  edge [
    source 19
    target 735
  ]
  edge [
    source 19
    target 736
  ]
  edge [
    source 19
    target 737
  ]
  edge [
    source 19
    target 738
  ]
  edge [
    source 19
    target 294
  ]
  edge [
    source 19
    target 739
  ]
  edge [
    source 19
    target 740
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 741
  ]
  edge [
    source 19
    target 742
  ]
  edge [
    source 19
    target 743
  ]
  edge [
    source 19
    target 744
  ]
  edge [
    source 19
    target 745
  ]
  edge [
    source 19
    target 746
  ]
  edge [
    source 19
    target 747
  ]
  edge [
    source 19
    target 748
  ]
  edge [
    source 19
    target 761
  ]
  edge [
    source 19
    target 762
  ]
  edge [
    source 19
    target 763
  ]
  edge [
    source 19
    target 137
  ]
  edge [
    source 19
    target 764
  ]
  edge [
    source 19
    target 765
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 766
  ]
  edge [
    source 19
    target 767
  ]
  edge [
    source 19
    target 768
  ]
  edge [
    source 19
    target 769
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 770
  ]
  edge [
    source 19
    target 771
  ]
  edge [
    source 19
    target 772
  ]
  edge [
    source 19
    target 773
  ]
  edge [
    source 19
    target 774
  ]
  edge [
    source 19
    target 76
  ]
  edge [
    source 19
    target 775
  ]
  edge [
    source 19
    target 776
  ]
  edge [
    source 19
    target 777
  ]
  edge [
    source 19
    target 778
  ]
  edge [
    source 19
    target 779
  ]
  edge [
    source 19
    target 780
  ]
  edge [
    source 19
    target 781
  ]
  edge [
    source 19
    target 782
  ]
  edge [
    source 19
    target 783
  ]
  edge [
    source 19
    target 784
  ]
  edge [
    source 19
    target 785
  ]
  edge [
    source 19
    target 786
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 788
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 504
  ]
  edge [
    source 19
    target 792
  ]
  edge [
    source 19
    target 793
  ]
  edge [
    source 19
    target 794
  ]
  edge [
    source 19
    target 795
  ]
  edge [
    source 19
    target 796
  ]
  edge [
    source 19
    target 797
  ]
  edge [
    source 19
    target 798
  ]
  edge [
    source 19
    target 799
  ]
  edge [
    source 19
    target 800
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 232
  ]
  edge [
    source 19
    target 233
  ]
  edge [
    source 19
    target 234
  ]
  edge [
    source 19
    target 235
  ]
  edge [
    source 19
    target 236
  ]
  edge [
    source 19
    target 237
  ]
  edge [
    source 19
    target 238
  ]
  edge [
    source 19
    target 239
  ]
  edge [
    source 19
    target 240
  ]
  edge [
    source 19
    target 241
  ]
  edge [
    source 19
    target 242
  ]
  edge [
    source 19
    target 243
  ]
  edge [
    source 19
    target 244
  ]
  edge [
    source 19
    target 245
  ]
  edge [
    source 19
    target 246
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 247
  ]
  edge [
    source 19
    target 248
  ]
  edge [
    source 19
    target 249
  ]
  edge [
    source 19
    target 801
  ]
  edge [
    source 19
    target 802
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 19
    target 804
  ]
  edge [
    source 19
    target 805
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 807
  ]
  edge [
    source 19
    target 808
  ]
  edge [
    source 19
    target 809
  ]
  edge [
    source 19
    target 810
  ]
  edge [
    source 19
    target 811
  ]
  edge [
    source 19
    target 812
  ]
  edge [
    source 19
    target 813
  ]
  edge [
    source 19
    target 814
  ]
  edge [
    source 19
    target 815
  ]
  edge [
    source 19
    target 816
  ]
  edge [
    source 19
    target 817
  ]
  edge [
    source 19
    target 818
  ]
  edge [
    source 19
    target 819
  ]
  edge [
    source 19
    target 820
  ]
  edge [
    source 19
    target 821
  ]
  edge [
    source 19
    target 822
  ]
  edge [
    source 19
    target 823
  ]
  edge [
    source 19
    target 824
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 19
    target 826
  ]
  edge [
    source 19
    target 827
  ]
  edge [
    source 19
    target 828
  ]
  edge [
    source 19
    target 829
  ]
  edge [
    source 19
    target 62
  ]
  edge [
    source 19
    target 830
  ]
  edge [
    source 19
    target 831
  ]
  edge [
    source 19
    target 832
  ]
  edge [
    source 19
    target 833
  ]
  edge [
    source 19
    target 834
  ]
  edge [
    source 19
    target 835
  ]
  edge [
    source 19
    target 836
  ]
  edge [
    source 19
    target 837
  ]
  edge [
    source 19
    target 838
  ]
  edge [
    source 19
    target 839
  ]
  edge [
    source 19
    target 840
  ]
  edge [
    source 19
    target 841
  ]
  edge [
    source 19
    target 842
  ]
  edge [
    source 19
    target 843
  ]
  edge [
    source 19
    target 844
  ]
  edge [
    source 19
    target 845
  ]
  edge [
    source 19
    target 846
  ]
  edge [
    source 19
    target 847
  ]
  edge [
    source 19
    target 848
  ]
  edge [
    source 19
    target 849
  ]
  edge [
    source 19
    target 850
  ]
  edge [
    source 19
    target 851
  ]
  edge [
    source 19
    target 852
  ]
  edge [
    source 19
    target 853
  ]
  edge [
    source 19
    target 854
  ]
  edge [
    source 19
    target 855
  ]
  edge [
    source 19
    target 856
  ]
  edge [
    source 19
    target 857
  ]
  edge [
    source 19
    target 858
  ]
  edge [
    source 19
    target 859
  ]
  edge [
    source 19
    target 860
  ]
  edge [
    source 19
    target 861
  ]
  edge [
    source 19
    target 862
  ]
  edge [
    source 19
    target 863
  ]
  edge [
    source 19
    target 864
  ]
  edge [
    source 19
    target 865
  ]
  edge [
    source 19
    target 866
  ]
  edge [
    source 19
    target 867
  ]
  edge [
    source 19
    target 868
  ]
  edge [
    source 19
    target 869
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 870
  ]
  edge [
    source 19
    target 871
  ]
  edge [
    source 19
    target 872
  ]
  edge [
    source 19
    target 873
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 874
  ]
  edge [
    source 19
    target 875
  ]
  edge [
    source 19
    target 876
  ]
  edge [
    source 19
    target 877
  ]
  edge [
    source 19
    target 878
  ]
  edge [
    source 19
    target 879
  ]
  edge [
    source 19
    target 880
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 881
  ]
  edge [
    source 19
    target 882
  ]
  edge [
    source 19
    target 74
  ]
  edge [
    source 19
    target 883
  ]
  edge [
    source 19
    target 884
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 19
    target 885
  ]
  edge [
    source 19
    target 886
  ]
  edge [
    source 19
    target 887
  ]
  edge [
    source 19
    target 888
  ]
  edge [
    source 19
    target 889
  ]
  edge [
    source 19
    target 890
  ]
  edge [
    source 19
    target 891
  ]
  edge [
    source 19
    target 892
  ]
  edge [
    source 19
    target 893
  ]
  edge [
    source 19
    target 894
  ]
  edge [
    source 19
    target 895
  ]
  edge [
    source 19
    target 896
  ]
  edge [
    source 19
    target 84
  ]
  edge [
    source 19
    target 897
  ]
  edge [
    source 19
    target 898
  ]
  edge [
    source 19
    target 899
  ]
  edge [
    source 19
    target 900
  ]
  edge [
    source 19
    target 901
  ]
  edge [
    source 19
    target 902
  ]
  edge [
    source 19
    target 903
  ]
  edge [
    source 19
    target 706
  ]
  edge [
    source 19
    target 904
  ]
  edge [
    source 19
    target 905
  ]
  edge [
    source 19
    target 906
  ]
  edge [
    source 19
    target 907
  ]
  edge [
    source 19
    target 908
  ]
  edge [
    source 19
    target 909
  ]
  edge [
    source 19
    target 910
  ]
  edge [
    source 19
    target 911
  ]
  edge [
    source 19
    target 912
  ]
  edge [
    source 19
    target 913
  ]
  edge [
    source 19
    target 297
  ]
  edge [
    source 19
    target 914
  ]
  edge [
    source 19
    target 915
  ]
  edge [
    source 19
    target 916
  ]
  edge [
    source 19
    target 917
  ]
  edge [
    source 19
    target 918
  ]
  edge [
    source 19
    target 919
  ]
  edge [
    source 19
    target 920
  ]
  edge [
    source 19
    target 921
  ]
  edge [
    source 19
    target 922
  ]
  edge [
    source 19
    target 923
  ]
  edge [
    source 19
    target 924
  ]
  edge [
    source 19
    target 925
  ]
  edge [
    source 19
    target 926
  ]
  edge [
    source 19
    target 927
  ]
  edge [
    source 19
    target 928
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 929
  ]
  edge [
    source 19
    target 930
  ]
  edge [
    source 19
    target 931
  ]
  edge [
    source 19
    target 932
  ]
  edge [
    source 19
    target 933
  ]
  edge [
    source 19
    target 683
  ]
  edge [
    source 19
    target 934
  ]
  edge [
    source 19
    target 935
  ]
  edge [
    source 19
    target 936
  ]
  edge [
    source 19
    target 937
  ]
  edge [
    source 19
    target 938
  ]
  edge [
    source 19
    target 939
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 940
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 674
  ]
  edge [
    source 19
    target 675
  ]
  edge [
    source 19
    target 676
  ]
  edge [
    source 19
    target 677
  ]
  edge [
    source 19
    target 678
  ]
  edge [
    source 19
    target 633
  ]
  edge [
    source 19
    target 942
  ]
  edge [
    source 19
    target 943
  ]
  edge [
    source 19
    target 944
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 946
  ]
  edge [
    source 19
    target 947
  ]
  edge [
    source 19
    target 514
  ]
  edge [
    source 19
    target 948
  ]
  edge [
    source 19
    target 949
  ]
  edge [
    source 19
    target 950
  ]
  edge [
    source 19
    target 951
  ]
  edge [
    source 19
    target 952
  ]
  edge [
    source 19
    target 953
  ]
  edge [
    source 19
    target 954
  ]
  edge [
    source 19
    target 955
  ]
  edge [
    source 19
    target 956
  ]
  edge [
    source 19
    target 308
  ]
  edge [
    source 19
    target 957
  ]
  edge [
    source 19
    target 958
  ]
  edge [
    source 19
    target 959
  ]
  edge [
    source 19
    target 960
  ]
  edge [
    source 19
    target 961
  ]
  edge [
    source 19
    target 962
  ]
  edge [
    source 19
    target 963
  ]
  edge [
    source 19
    target 964
  ]
  edge [
    source 19
    target 965
  ]
  edge [
    source 19
    target 966
  ]
  edge [
    source 19
    target 967
  ]
  edge [
    source 19
    target 968
  ]
  edge [
    source 19
    target 969
  ]
  edge [
    source 19
    target 970
  ]
  edge [
    source 19
    target 316
  ]
  edge [
    source 19
    target 971
  ]
  edge [
    source 19
    target 972
  ]
  edge [
    source 19
    target 973
  ]
  edge [
    source 19
    target 974
  ]
  edge [
    source 19
    target 421
  ]
  edge [
    source 19
    target 975
  ]
  edge [
    source 19
    target 976
  ]
  edge [
    source 19
    target 977
  ]
  edge [
    source 19
    target 978
  ]
  edge [
    source 19
    target 979
  ]
  edge [
    source 19
    target 980
  ]
  edge [
    source 19
    target 981
  ]
  edge [
    source 19
    target 982
  ]
  edge [
    source 19
    target 983
  ]
  edge [
    source 19
    target 984
  ]
  edge [
    source 19
    target 985
  ]
  edge [
    source 19
    target 986
  ]
  edge [
    source 19
    target 987
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 502
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 20
    target 41
  ]
  edge [
    source 20
    target 1023
  ]
  edge [
    source 20
    target 1024
  ]
  edge [
    source 20
    target 1025
  ]
  edge [
    source 20
    target 1026
  ]
  edge [
    source 20
    target 1027
  ]
  edge [
    source 20
    target 1028
  ]
  edge [
    source 20
    target 1029
  ]
  edge [
    source 20
    target 538
  ]
  edge [
    source 20
    target 667
  ]
  edge [
    source 20
    target 1030
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 21
    target 1032
  ]
  edge [
    source 21
    target 1033
  ]
  edge [
    source 21
    target 1034
  ]
  edge [
    source 21
    target 1035
  ]
  edge [
    source 21
    target 1036
  ]
  edge [
    source 21
    target 1037
  ]
  edge [
    source 21
    target 1038
  ]
  edge [
    source 21
    target 1039
  ]
  edge [
    source 21
    target 1040
  ]
  edge [
    source 21
    target 1041
  ]
  edge [
    source 21
    target 1042
  ]
  edge [
    source 21
    target 209
  ]
  edge [
    source 21
    target 1043
  ]
  edge [
    source 21
    target 1044
  ]
  edge [
    source 21
    target 1045
  ]
  edge [
    source 21
    target 1046
  ]
  edge [
    source 21
    target 1047
  ]
  edge [
    source 21
    target 1048
  ]
  edge [
    source 21
    target 1049
  ]
  edge [
    source 21
    target 1050
  ]
  edge [
    source 21
    target 1051
  ]
  edge [
    source 21
    target 76
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 47
  ]
  edge [
    source 22
    target 48
  ]
  edge [
    source 22
    target 903
  ]
  edge [
    source 22
    target 706
  ]
  edge [
    source 22
    target 902
  ]
  edge [
    source 22
    target 904
  ]
  edge [
    source 22
    target 905
  ]
  edge [
    source 22
    target 906
  ]
  edge [
    source 22
    target 907
  ]
  edge [
    source 22
    target 897
  ]
  edge [
    source 22
    target 1052
  ]
  edge [
    source 22
    target 1053
  ]
  edge [
    source 22
    target 1054
  ]
  edge [
    source 22
    target 313
  ]
  edge [
    source 22
    target 1055
  ]
  edge [
    source 22
    target 1056
  ]
  edge [
    source 22
    target 1057
  ]
  edge [
    source 22
    target 1058
  ]
  edge [
    source 22
    target 1059
  ]
  edge [
    source 22
    target 623
  ]
  edge [
    source 22
    target 1060
  ]
  edge [
    source 22
    target 1061
  ]
  edge [
    source 22
    target 34
  ]
  edge [
    source 22
    target 1062
  ]
  edge [
    source 22
    target 1063
  ]
  edge [
    source 22
    target 703
  ]
  edge [
    source 22
    target 1064
  ]
  edge [
    source 22
    target 1065
  ]
  edge [
    source 22
    target 1066
  ]
  edge [
    source 22
    target 1067
  ]
  edge [
    source 22
    target 548
  ]
  edge [
    source 22
    target 1068
  ]
  edge [
    source 22
    target 1069
  ]
  edge [
    source 22
    target 1070
  ]
  edge [
    source 22
    target 1071
  ]
  edge [
    source 22
    target 1072
  ]
  edge [
    source 22
    target 831
  ]
  edge [
    source 22
    target 1073
  ]
  edge [
    source 22
    target 1074
  ]
  edge [
    source 22
    target 1075
  ]
  edge [
    source 22
    target 123
  ]
  edge [
    source 22
    target 1076
  ]
  edge [
    source 22
    target 1077
  ]
  edge [
    source 22
    target 1078
  ]
  edge [
    source 22
    target 1079
  ]
  edge [
    source 22
    target 677
  ]
  edge [
    source 22
    target 1080
  ]
  edge [
    source 22
    target 1081
  ]
  edge [
    source 22
    target 1082
  ]
  edge [
    source 22
    target 1083
  ]
  edge [
    source 22
    target 1084
  ]
  edge [
    source 22
    target 1085
  ]
  edge [
    source 22
    target 1086
  ]
  edge [
    source 22
    target 1087
  ]
  edge [
    source 22
    target 1088
  ]
  edge [
    source 22
    target 708
  ]
  edge [
    source 22
    target 1089
  ]
  edge [
    source 22
    target 1090
  ]
  edge [
    source 22
    target 1091
  ]
  edge [
    source 22
    target 1092
  ]
  edge [
    source 22
    target 1093
  ]
  edge [
    source 22
    target 85
  ]
  edge [
    source 22
    target 769
  ]
  edge [
    source 22
    target 118
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 714
  ]
  edge [
    source 22
    target 1094
  ]
  edge [
    source 22
    target 716
  ]
  edge [
    source 22
    target 1095
  ]
  edge [
    source 22
    target 1096
  ]
  edge [
    source 22
    target 1097
  ]
  edge [
    source 22
    target 1098
  ]
  edge [
    source 22
    target 241
  ]
  edge [
    source 22
    target 746
  ]
  edge [
    source 22
    target 725
  ]
  edge [
    source 22
    target 1099
  ]
  edge [
    source 22
    target 1100
  ]
  edge [
    source 22
    target 789
  ]
  edge [
    source 22
    target 1101
  ]
  edge [
    source 22
    target 1102
  ]
  edge [
    source 22
    target 1103
  ]
  edge [
    source 22
    target 508
  ]
  edge [
    source 22
    target 891
  ]
  edge [
    source 22
    target 1104
  ]
  edge [
    source 22
    target 1105
  ]
  edge [
    source 22
    target 1106
  ]
  edge [
    source 22
    target 1107
  ]
  edge [
    source 22
    target 38
  ]
  edge [
    source 22
    target 54
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1108
  ]
  edge [
    source 24
    target 1109
  ]
  edge [
    source 24
    target 133
  ]
  edge [
    source 24
    target 1110
  ]
  edge [
    source 24
    target 407
  ]
  edge [
    source 24
    target 129
  ]
  edge [
    source 24
    target 137
  ]
  edge [
    source 24
    target 1111
  ]
  edge [
    source 24
    target 144
  ]
  edge [
    source 24
    target 1112
  ]
  edge [
    source 24
    target 1113
  ]
  edge [
    source 24
    target 1114
  ]
  edge [
    source 24
    target 1115
  ]
  edge [
    source 24
    target 1116
  ]
  edge [
    source 24
    target 103
  ]
  edge [
    source 24
    target 1117
  ]
  edge [
    source 24
    target 521
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 1118
  ]
  edge [
    source 24
    target 1119
  ]
  edge [
    source 24
    target 1120
  ]
  edge [
    source 24
    target 1121
  ]
  edge [
    source 24
    target 1122
  ]
  edge [
    source 24
    target 1123
  ]
  edge [
    source 24
    target 1124
  ]
  edge [
    source 24
    target 1125
  ]
  edge [
    source 24
    target 1126
  ]
  edge [
    source 24
    target 1127
  ]
  edge [
    source 24
    target 1128
  ]
  edge [
    source 24
    target 1129
  ]
  edge [
    source 24
    target 192
  ]
  edge [
    source 24
    target 1130
  ]
  edge [
    source 24
    target 1131
  ]
  edge [
    source 24
    target 1132
  ]
  edge [
    source 24
    target 1133
  ]
  edge [
    source 24
    target 1134
  ]
  edge [
    source 24
    target 1135
  ]
  edge [
    source 24
    target 126
  ]
  edge [
    source 24
    target 293
  ]
  edge [
    source 24
    target 127
  ]
  edge [
    source 24
    target 294
  ]
  edge [
    source 24
    target 233
  ]
  edge [
    source 24
    target 295
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 296
  ]
  edge [
    source 24
    target 297
  ]
  edge [
    source 24
    target 298
  ]
  edge [
    source 24
    target 299
  ]
  edge [
    source 24
    target 1136
  ]
  edge [
    source 24
    target 1137
  ]
  edge [
    source 24
    target 1138
  ]
  edge [
    source 24
    target 1139
  ]
  edge [
    source 24
    target 1140
  ]
  edge [
    source 24
    target 1141
  ]
  edge [
    source 24
    target 1142
  ]
  edge [
    source 24
    target 1143
  ]
  edge [
    source 24
    target 1144
  ]
  edge [
    source 24
    target 1145
  ]
  edge [
    source 24
    target 1146
  ]
  edge [
    source 24
    target 1147
  ]
  edge [
    source 24
    target 1148
  ]
  edge [
    source 24
    target 1149
  ]
  edge [
    source 24
    target 1150
  ]
  edge [
    source 24
    target 1151
  ]
  edge [
    source 24
    target 1152
  ]
  edge [
    source 24
    target 1153
  ]
  edge [
    source 24
    target 1154
  ]
  edge [
    source 24
    target 1155
  ]
  edge [
    source 24
    target 1156
  ]
  edge [
    source 24
    target 1157
  ]
  edge [
    source 24
    target 1158
  ]
  edge [
    source 24
    target 1159
  ]
  edge [
    source 24
    target 1160
  ]
  edge [
    source 24
    target 84
  ]
  edge [
    source 24
    target 420
  ]
  edge [
    source 24
    target 421
  ]
  edge [
    source 24
    target 357
  ]
  edge [
    source 24
    target 358
  ]
  edge [
    source 24
    target 359
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 1161
  ]
  edge [
    source 26
    target 1162
  ]
  edge [
    source 26
    target 1163
  ]
  edge [
    source 26
    target 1164
  ]
  edge [
    source 26
    target 1165
  ]
  edge [
    source 26
    target 1166
  ]
  edge [
    source 26
    target 290
  ]
  edge [
    source 26
    target 1167
  ]
  edge [
    source 26
    target 1168
  ]
  edge [
    source 26
    target 1169
  ]
  edge [
    source 26
    target 1170
  ]
  edge [
    source 26
    target 1171
  ]
  edge [
    source 26
    target 931
  ]
  edge [
    source 26
    target 1172
  ]
  edge [
    source 26
    target 1173
  ]
  edge [
    source 26
    target 1174
  ]
  edge [
    source 26
    target 1175
  ]
  edge [
    source 26
    target 1176
  ]
  edge [
    source 26
    target 1177
  ]
  edge [
    source 26
    target 1178
  ]
  edge [
    source 26
    target 1179
  ]
  edge [
    source 26
    target 1180
  ]
  edge [
    source 26
    target 1181
  ]
  edge [
    source 26
    target 708
  ]
  edge [
    source 26
    target 1182
  ]
  edge [
    source 26
    target 1183
  ]
  edge [
    source 26
    target 959
  ]
  edge [
    source 26
    target 1184
  ]
  edge [
    source 26
    target 1185
  ]
  edge [
    source 26
    target 1186
  ]
  edge [
    source 26
    target 1187
  ]
  edge [
    source 26
    target 1188
  ]
  edge [
    source 26
    target 74
  ]
  edge [
    source 26
    target 1189
  ]
  edge [
    source 26
    target 1190
  ]
  edge [
    source 26
    target 1191
  ]
  edge [
    source 26
    target 1192
  ]
  edge [
    source 26
    target 1193
  ]
  edge [
    source 26
    target 1194
  ]
  edge [
    source 26
    target 1195
  ]
  edge [
    source 26
    target 1196
  ]
  edge [
    source 26
    target 757
  ]
  edge [
    source 26
    target 1197
  ]
  edge [
    source 26
    target 1198
  ]
  edge [
    source 26
    target 1199
  ]
  edge [
    source 26
    target 945
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 348
  ]
  edge [
    source 26
    target 1200
  ]
  edge [
    source 26
    target 1201
  ]
  edge [
    source 26
    target 1202
  ]
  edge [
    source 26
    target 1203
  ]
  edge [
    source 26
    target 1204
  ]
  edge [
    source 26
    target 1205
  ]
  edge [
    source 26
    target 770
  ]
  edge [
    source 26
    target 1206
  ]
  edge [
    source 26
    target 1207
  ]
  edge [
    source 26
    target 1208
  ]
  edge [
    source 26
    target 1209
  ]
  edge [
    source 26
    target 1210
  ]
  edge [
    source 26
    target 1211
  ]
  edge [
    source 26
    target 1212
  ]
  edge [
    source 26
    target 1213
  ]
  edge [
    source 26
    target 1214
  ]
  edge [
    source 26
    target 1215
  ]
  edge [
    source 26
    target 1216
  ]
  edge [
    source 26
    target 1217
  ]
  edge [
    source 26
    target 1218
  ]
  edge [
    source 26
    target 1219
  ]
  edge [
    source 26
    target 1220
  ]
  edge [
    source 26
    target 1221
  ]
  edge [
    source 26
    target 1222
  ]
  edge [
    source 26
    target 1223
  ]
  edge [
    source 26
    target 1224
  ]
  edge [
    source 26
    target 1225
  ]
  edge [
    source 26
    target 1226
  ]
  edge [
    source 26
    target 1227
  ]
  edge [
    source 26
    target 492
  ]
  edge [
    source 26
    target 1228
  ]
  edge [
    source 26
    target 1229
  ]
  edge [
    source 26
    target 1230
  ]
  edge [
    source 26
    target 1231
  ]
  edge [
    source 26
    target 1232
  ]
  edge [
    source 26
    target 1233
  ]
  edge [
    source 26
    target 1234
  ]
  edge [
    source 26
    target 1235
  ]
  edge [
    source 26
    target 1236
  ]
  edge [
    source 26
    target 1237
  ]
  edge [
    source 26
    target 1238
  ]
  edge [
    source 26
    target 1239
  ]
  edge [
    source 26
    target 1240
  ]
  edge [
    source 26
    target 1241
  ]
  edge [
    source 26
    target 1242
  ]
  edge [
    source 26
    target 1243
  ]
  edge [
    source 26
    target 146
  ]
  edge [
    source 26
    target 1244
  ]
  edge [
    source 26
    target 1245
  ]
  edge [
    source 26
    target 1246
  ]
  edge [
    source 26
    target 1247
  ]
  edge [
    source 26
    target 1248
  ]
  edge [
    source 26
    target 1249
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1250
  ]
  edge [
    source 27
    target 1251
  ]
  edge [
    source 27
    target 1252
  ]
  edge [
    source 27
    target 1253
  ]
  edge [
    source 27
    target 1254
  ]
  edge [
    source 27
    target 1255
  ]
  edge [
    source 27
    target 1256
  ]
  edge [
    source 27
    target 1257
  ]
  edge [
    source 27
    target 56
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 578
  ]
  edge [
    source 31
    target 579
  ]
  edge [
    source 31
    target 580
  ]
  edge [
    source 31
    target 581
  ]
  edge [
    source 31
    target 582
  ]
  edge [
    source 31
    target 583
  ]
  edge [
    source 31
    target 584
  ]
  edge [
    source 31
    target 585
  ]
  edge [
    source 31
    target 404
  ]
  edge [
    source 31
    target 586
  ]
  edge [
    source 31
    target 587
  ]
  edge [
    source 31
    target 588
  ]
  edge [
    source 31
    target 589
  ]
  edge [
    source 31
    target 590
  ]
  edge [
    source 31
    target 591
  ]
  edge [
    source 31
    target 237
  ]
  edge [
    source 31
    target 592
  ]
  edge [
    source 31
    target 593
  ]
  edge [
    source 31
    target 594
  ]
  edge [
    source 31
    target 595
  ]
  edge [
    source 31
    target 596
  ]
  edge [
    source 31
    target 597
  ]
  edge [
    source 31
    target 554
  ]
  edge [
    source 31
    target 598
  ]
  edge [
    source 31
    target 599
  ]
  edge [
    source 31
    target 75
  ]
  edge [
    source 31
    target 279
  ]
  edge [
    source 31
    target 600
  ]
  edge [
    source 31
    target 601
  ]
  edge [
    source 31
    target 316
  ]
  edge [
    source 31
    target 602
  ]
  edge [
    source 31
    target 603
  ]
  edge [
    source 31
    target 604
  ]
  edge [
    source 31
    target 605
  ]
  edge [
    source 31
    target 606
  ]
  edge [
    source 31
    target 607
  ]
  edge [
    source 31
    target 608
  ]
  edge [
    source 31
    target 609
  ]
  edge [
    source 31
    target 610
  ]
  edge [
    source 31
    target 611
  ]
  edge [
    source 31
    target 1258
  ]
  edge [
    source 31
    target 1259
  ]
  edge [
    source 31
    target 1260
  ]
  edge [
    source 31
    target 1261
  ]
  edge [
    source 31
    target 1262
  ]
  edge [
    source 31
    target 1263
  ]
  edge [
    source 31
    target 1264
  ]
  edge [
    source 31
    target 1265
  ]
  edge [
    source 31
    target 1266
  ]
  edge [
    source 31
    target 345
  ]
  edge [
    source 31
    target 1267
  ]
  edge [
    source 31
    target 1268
  ]
  edge [
    source 31
    target 1269
  ]
  edge [
    source 31
    target 1270
  ]
  edge [
    source 31
    target 1271
  ]
  edge [
    source 31
    target 1272
  ]
  edge [
    source 31
    target 1187
  ]
  edge [
    source 31
    target 1273
  ]
  edge [
    source 31
    target 234
  ]
  edge [
    source 31
    target 1274
  ]
  edge [
    source 31
    target 1275
  ]
  edge [
    source 31
    target 1276
  ]
  edge [
    source 31
    target 1277
  ]
  edge [
    source 31
    target 1278
  ]
  edge [
    source 31
    target 1279
  ]
  edge [
    source 31
    target 1280
  ]
  edge [
    source 31
    target 1281
  ]
  edge [
    source 31
    target 1282
  ]
  edge [
    source 31
    target 1283
  ]
  edge [
    source 31
    target 1284
  ]
  edge [
    source 31
    target 157
  ]
  edge [
    source 31
    target 574
  ]
  edge [
    source 31
    target 1285
  ]
  edge [
    source 31
    target 1286
  ]
  edge [
    source 31
    target 1287
  ]
  edge [
    source 31
    target 1288
  ]
  edge [
    source 31
    target 1289
  ]
  edge [
    source 31
    target 1290
  ]
  edge [
    source 31
    target 1291
  ]
  edge [
    source 31
    target 1292
  ]
  edge [
    source 31
    target 1184
  ]
  edge [
    source 31
    target 1293
  ]
  edge [
    source 31
    target 1294
  ]
  edge [
    source 31
    target 1295
  ]
  edge [
    source 31
    target 1296
  ]
  edge [
    source 31
    target 1297
  ]
  edge [
    source 31
    target 1298
  ]
  edge [
    source 31
    target 1299
  ]
  edge [
    source 31
    target 1300
  ]
  edge [
    source 31
    target 900
  ]
  edge [
    source 31
    target 1301
  ]
  edge [
    source 31
    target 1302
  ]
  edge [
    source 31
    target 1303
  ]
  edge [
    source 31
    target 1304
  ]
  edge [
    source 31
    target 1305
  ]
  edge [
    source 31
    target 1306
  ]
  edge [
    source 31
    target 1307
  ]
  edge [
    source 31
    target 1308
  ]
  edge [
    source 31
    target 1309
  ]
  edge [
    source 31
    target 1310
  ]
  edge [
    source 31
    target 1311
  ]
  edge [
    source 31
    target 1312
  ]
  edge [
    source 31
    target 1313
  ]
  edge [
    source 31
    target 1314
  ]
  edge [
    source 31
    target 1315
  ]
  edge [
    source 31
    target 1316
  ]
  edge [
    source 31
    target 1317
  ]
  edge [
    source 31
    target 1318
  ]
  edge [
    source 31
    target 1319
  ]
  edge [
    source 31
    target 1320
  ]
  edge [
    source 31
    target 1321
  ]
  edge [
    source 31
    target 1322
  ]
  edge [
    source 31
    target 1323
  ]
  edge [
    source 31
    target 1324
  ]
  edge [
    source 31
    target 1325
  ]
  edge [
    source 31
    target 1326
  ]
  edge [
    source 31
    target 1327
  ]
  edge [
    source 31
    target 1328
  ]
  edge [
    source 31
    target 82
  ]
  edge [
    source 31
    target 1329
  ]
  edge [
    source 31
    target 1330
  ]
  edge [
    source 31
    target 1331
  ]
  edge [
    source 31
    target 1332
  ]
  edge [
    source 31
    target 1333
  ]
  edge [
    source 31
    target 1334
  ]
  edge [
    source 31
    target 1335
  ]
  edge [
    source 31
    target 1336
  ]
  edge [
    source 31
    target 1044
  ]
  edge [
    source 31
    target 63
  ]
  edge [
    source 31
    target 1337
  ]
  edge [
    source 31
    target 1338
  ]
  edge [
    source 31
    target 74
  ]
  edge [
    source 31
    target 1339
  ]
  edge [
    source 31
    target 1340
  ]
  edge [
    source 31
    target 1341
  ]
  edge [
    source 31
    target 1342
  ]
  edge [
    source 31
    target 1343
  ]
  edge [
    source 31
    target 1344
  ]
  edge [
    source 31
    target 1345
  ]
  edge [
    source 31
    target 290
  ]
  edge [
    source 31
    target 1346
  ]
  edge [
    source 31
    target 1347
  ]
  edge [
    source 31
    target 1348
  ]
  edge [
    source 31
    target 1349
  ]
  edge [
    source 31
    target 1026
  ]
  edge [
    source 31
    target 1350
  ]
  edge [
    source 31
    target 1351
  ]
  edge [
    source 31
    target 1352
  ]
  edge [
    source 31
    target 1353
  ]
  edge [
    source 31
    target 42
  ]
  edge [
    source 31
    target 1354
  ]
  edge [
    source 31
    target 1355
  ]
  edge [
    source 31
    target 1356
  ]
  edge [
    source 31
    target 1357
  ]
  edge [
    source 31
    target 1358
  ]
  edge [
    source 31
    target 1359
  ]
  edge [
    source 31
    target 1360
  ]
  edge [
    source 31
    target 1361
  ]
  edge [
    source 31
    target 1362
  ]
  edge [
    source 31
    target 1363
  ]
  edge [
    source 31
    target 1364
  ]
  edge [
    source 31
    target 1365
  ]
  edge [
    source 31
    target 1366
  ]
  edge [
    source 31
    target 1367
  ]
  edge [
    source 31
    target 1368
  ]
  edge [
    source 31
    target 1369
  ]
  edge [
    source 31
    target 1370
  ]
  edge [
    source 31
    target 1371
  ]
  edge [
    source 31
    target 1372
  ]
  edge [
    source 31
    target 1373
  ]
  edge [
    source 31
    target 215
  ]
  edge [
    source 31
    target 1374
  ]
  edge [
    source 31
    target 1375
  ]
  edge [
    source 31
    target 1376
  ]
  edge [
    source 31
    target 1377
  ]
  edge [
    source 31
    target 1378
  ]
  edge [
    source 31
    target 1379
  ]
  edge [
    source 31
    target 1380
  ]
  edge [
    source 31
    target 1381
  ]
  edge [
    source 31
    target 1382
  ]
  edge [
    source 31
    target 1383
  ]
  edge [
    source 31
    target 563
  ]
  edge [
    source 31
    target 1384
  ]
  edge [
    source 31
    target 1385
  ]
  edge [
    source 31
    target 1386
  ]
  edge [
    source 31
    target 1387
  ]
  edge [
    source 31
    target 1388
  ]
  edge [
    source 31
    target 1389
  ]
  edge [
    source 31
    target 1390
  ]
  edge [
    source 31
    target 1391
  ]
  edge [
    source 31
    target 1392
  ]
  edge [
    source 31
    target 1393
  ]
  edge [
    source 31
    target 1394
  ]
  edge [
    source 31
    target 1395
  ]
  edge [
    source 31
    target 1396
  ]
  edge [
    source 31
    target 1397
  ]
  edge [
    source 31
    target 1398
  ]
  edge [
    source 31
    target 1399
  ]
  edge [
    source 31
    target 1400
  ]
  edge [
    source 31
    target 1401
  ]
  edge [
    source 31
    target 142
  ]
  edge [
    source 31
    target 280
  ]
  edge [
    source 31
    target 1402
  ]
  edge [
    source 31
    target 1403
  ]
  edge [
    source 31
    target 561
  ]
  edge [
    source 31
    target 1404
  ]
  edge [
    source 31
    target 55
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 146
  ]
  edge [
    source 32
    target 1405
  ]
  edge [
    source 32
    target 1406
  ]
  edge [
    source 32
    target 1407
  ]
  edge [
    source 32
    target 1408
  ]
  edge [
    source 32
    target 1409
  ]
  edge [
    source 32
    target 1410
  ]
  edge [
    source 32
    target 648
  ]
  edge [
    source 32
    target 1411
  ]
  edge [
    source 32
    target 1412
  ]
  edge [
    source 32
    target 637
  ]
  edge [
    source 32
    target 1413
  ]
  edge [
    source 32
    target 1414
  ]
  edge [
    source 32
    target 1415
  ]
  edge [
    source 32
    target 257
  ]
  edge [
    source 32
    target 1416
  ]
  edge [
    source 32
    target 1417
  ]
  edge [
    source 32
    target 157
  ]
  edge [
    source 32
    target 210
  ]
  edge [
    source 32
    target 211
  ]
  edge [
    source 32
    target 284
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 32
    target 1418
  ]
  edge [
    source 32
    target 1419
  ]
  edge [
    source 32
    target 1420
  ]
  edge [
    source 32
    target 1421
  ]
  edge [
    source 32
    target 255
  ]
  edge [
    source 32
    target 1422
  ]
  edge [
    source 32
    target 1423
  ]
  edge [
    source 32
    target 1424
  ]
  edge [
    source 32
    target 1425
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1426
  ]
  edge [
    source 34
    target 753
  ]
  edge [
    source 34
    target 677
  ]
  edge [
    source 34
    target 1427
  ]
  edge [
    source 34
    target 1428
  ]
  edge [
    source 34
    target 1429
  ]
  edge [
    source 34
    target 1430
  ]
  edge [
    source 34
    target 1431
  ]
  edge [
    source 34
    target 750
  ]
  edge [
    source 34
    target 1432
  ]
  edge [
    source 34
    target 638
  ]
  edge [
    source 34
    target 1433
  ]
  edge [
    source 34
    target 1434
  ]
  edge [
    source 34
    target 755
  ]
  edge [
    source 34
    target 548
  ]
  edge [
    source 34
    target 633
  ]
  edge [
    source 34
    target 1081
  ]
  edge [
    source 34
    target 1435
  ]
  edge [
    source 34
    target 1436
  ]
  edge [
    source 34
    target 1437
  ]
  edge [
    source 34
    target 1438
  ]
  edge [
    source 34
    target 1439
  ]
  edge [
    source 34
    target 1440
  ]
  edge [
    source 34
    target 1441
  ]
  edge [
    source 34
    target 1442
  ]
  edge [
    source 34
    target 1443
  ]
  edge [
    source 34
    target 1444
  ]
  edge [
    source 34
    target 1445
  ]
  edge [
    source 34
    target 1368
  ]
  edge [
    source 34
    target 1446
  ]
  edge [
    source 34
    target 1447
  ]
  edge [
    source 34
    target 1448
  ]
  edge [
    source 34
    target 1449
  ]
  edge [
    source 34
    target 1450
  ]
  edge [
    source 34
    target 749
  ]
  edge [
    source 34
    target 1451
  ]
  edge [
    source 34
    target 1452
  ]
  edge [
    source 34
    target 1453
  ]
  edge [
    source 34
    target 1454
  ]
  edge [
    source 34
    target 697
  ]
  edge [
    source 34
    target 1455
  ]
  edge [
    source 34
    target 1456
  ]
  edge [
    source 34
    target 1457
  ]
  edge [
    source 34
    target 1458
  ]
  edge [
    source 34
    target 1459
  ]
  edge [
    source 34
    target 1460
  ]
  edge [
    source 34
    target 1461
  ]
  edge [
    source 34
    target 1462
  ]
  edge [
    source 34
    target 1463
  ]
  edge [
    source 34
    target 1464
  ]
  edge [
    source 34
    target 1465
  ]
  edge [
    source 34
    target 1466
  ]
  edge [
    source 34
    target 1467
  ]
  edge [
    source 34
    target 1468
  ]
  edge [
    source 34
    target 1469
  ]
  edge [
    source 34
    target 1470
  ]
  edge [
    source 34
    target 1471
  ]
  edge [
    source 34
    target 1472
  ]
  edge [
    source 34
    target 1473
  ]
  edge [
    source 34
    target 689
  ]
  edge [
    source 34
    target 690
  ]
  edge [
    source 34
    target 691
  ]
  edge [
    source 34
    target 692
  ]
  edge [
    source 34
    target 693
  ]
  edge [
    source 34
    target 694
  ]
  edge [
    source 34
    target 695
  ]
  edge [
    source 34
    target 696
  ]
  edge [
    source 34
    target 698
  ]
  edge [
    source 34
    target 699
  ]
  edge [
    source 34
    target 700
  ]
  edge [
    source 34
    target 701
  ]
  edge [
    source 34
    target 702
  ]
  edge [
    source 34
    target 54
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1474
  ]
  edge [
    source 37
    target 1475
  ]
  edge [
    source 37
    target 42
  ]
  edge [
    source 37
    target 1476
  ]
  edge [
    source 37
    target 1477
  ]
  edge [
    source 37
    target 1478
  ]
  edge [
    source 37
    target 1479
  ]
  edge [
    source 37
    target 1480
  ]
  edge [
    source 37
    target 1481
  ]
  edge [
    source 37
    target 1482
  ]
  edge [
    source 37
    target 1483
  ]
  edge [
    source 37
    target 1484
  ]
  edge [
    source 37
    target 1485
  ]
  edge [
    source 37
    target 1486
  ]
  edge [
    source 37
    target 1487
  ]
  edge [
    source 37
    target 1488
  ]
  edge [
    source 37
    target 1489
  ]
  edge [
    source 37
    target 1490
  ]
  edge [
    source 37
    target 1491
  ]
  edge [
    source 37
    target 1492
  ]
  edge [
    source 37
    target 1493
  ]
  edge [
    source 37
    target 1494
  ]
  edge [
    source 37
    target 1495
  ]
  edge [
    source 37
    target 1496
  ]
  edge [
    source 37
    target 1497
  ]
  edge [
    source 37
    target 1498
  ]
  edge [
    source 37
    target 1499
  ]
  edge [
    source 37
    target 1500
  ]
  edge [
    source 37
    target 1501
  ]
  edge [
    source 37
    target 1502
  ]
  edge [
    source 37
    target 1503
  ]
  edge [
    source 37
    target 1504
  ]
  edge [
    source 37
    target 151
  ]
  edge [
    source 37
    target 196
  ]
  edge [
    source 37
    target 197
  ]
  edge [
    source 37
    target 198
  ]
  edge [
    source 37
    target 199
  ]
  edge [
    source 37
    target 108
  ]
  edge [
    source 37
    target 200
  ]
  edge [
    source 37
    target 201
  ]
  edge [
    source 37
    target 202
  ]
  edge [
    source 37
    target 169
  ]
  edge [
    source 37
    target 170
  ]
  edge [
    source 37
    target 172
  ]
  edge [
    source 37
    target 203
  ]
  edge [
    source 37
    target 177
  ]
  edge [
    source 37
    target 178
  ]
  edge [
    source 37
    target 204
  ]
  edge [
    source 37
    target 180
  ]
  edge [
    source 37
    target 184
  ]
  edge [
    source 37
    target 182
  ]
  edge [
    source 37
    target 205
  ]
  edge [
    source 37
    target 206
  ]
  edge [
    source 37
    target 183
  ]
  edge [
    source 37
    target 207
  ]
  edge [
    source 37
    target 186
  ]
  edge [
    source 37
    target 208
  ]
  edge [
    source 37
    target 1505
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 55
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1506
  ]
  edge [
    source 38
    target 1507
  ]
  edge [
    source 38
    target 1508
  ]
  edge [
    source 38
    target 1509
  ]
  edge [
    source 38
    target 1510
  ]
  edge [
    source 38
    target 1511
  ]
  edge [
    source 38
    target 1512
  ]
  edge [
    source 38
    target 1513
  ]
  edge [
    source 38
    target 1514
  ]
  edge [
    source 38
    target 1515
  ]
  edge [
    source 38
    target 1516
  ]
  edge [
    source 38
    target 1517
  ]
  edge [
    source 38
    target 1518
  ]
  edge [
    source 38
    target 1519
  ]
  edge [
    source 38
    target 1520
  ]
  edge [
    source 38
    target 1521
  ]
  edge [
    source 38
    target 1522
  ]
  edge [
    source 38
    target 1523
  ]
  edge [
    source 38
    target 1489
  ]
  edge [
    source 38
    target 1524
  ]
  edge [
    source 38
    target 1525
  ]
  edge [
    source 38
    target 1526
  ]
  edge [
    source 38
    target 1527
  ]
  edge [
    source 38
    target 1528
  ]
  edge [
    source 38
    target 1529
  ]
  edge [
    source 38
    target 1530
  ]
  edge [
    source 38
    target 1531
  ]
  edge [
    source 38
    target 1532
  ]
  edge [
    source 38
    target 1533
  ]
  edge [
    source 38
    target 1534
  ]
  edge [
    source 38
    target 1535
  ]
  edge [
    source 38
    target 1536
  ]
  edge [
    source 38
    target 1537
  ]
  edge [
    source 38
    target 1538
  ]
  edge [
    source 38
    target 1539
  ]
  edge [
    source 38
    target 1540
  ]
  edge [
    source 38
    target 1541
  ]
  edge [
    source 38
    target 1542
  ]
  edge [
    source 38
    target 1543
  ]
  edge [
    source 38
    target 1544
  ]
  edge [
    source 38
    target 1545
  ]
  edge [
    source 38
    target 1546
  ]
  edge [
    source 38
    target 1547
  ]
  edge [
    source 38
    target 1548
  ]
  edge [
    source 38
    target 1549
  ]
  edge [
    source 38
    target 1550
  ]
  edge [
    source 38
    target 1551
  ]
  edge [
    source 38
    target 55
  ]
  edge [
    source 38
    target 54
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1552
  ]
  edge [
    source 39
    target 1553
  ]
  edge [
    source 39
    target 1489
  ]
  edge [
    source 39
    target 1554
  ]
  edge [
    source 39
    target 1555
  ]
  edge [
    source 39
    target 1556
  ]
  edge [
    source 39
    target 1557
  ]
  edge [
    source 39
    target 1558
  ]
  edge [
    source 39
    target 1559
  ]
  edge [
    source 39
    target 1560
  ]
  edge [
    source 39
    target 1561
  ]
  edge [
    source 39
    target 1562
  ]
  edge [
    source 39
    target 1513
  ]
  edge [
    source 39
    target 1563
  ]
  edge [
    source 39
    target 1564
  ]
  edge [
    source 39
    target 1565
  ]
  edge [
    source 39
    target 1566
  ]
  edge [
    source 39
    target 1567
  ]
  edge [
    source 39
    target 1568
  ]
  edge [
    source 39
    target 1569
  ]
  edge [
    source 39
    target 1570
  ]
  edge [
    source 39
    target 1571
  ]
  edge [
    source 39
    target 1572
  ]
  edge [
    source 39
    target 1356
  ]
  edge [
    source 39
    target 1573
  ]
  edge [
    source 39
    target 1574
  ]
  edge [
    source 39
    target 1575
  ]
  edge [
    source 39
    target 1533
  ]
  edge [
    source 39
    target 1534
  ]
  edge [
    source 39
    target 1535
  ]
  edge [
    source 39
    target 1536
  ]
  edge [
    source 39
    target 1537
  ]
  edge [
    source 39
    target 1538
  ]
  edge [
    source 39
    target 1539
  ]
  edge [
    source 39
    target 1540
  ]
  edge [
    source 39
    target 1541
  ]
  edge [
    source 39
    target 1516
  ]
  edge [
    source 39
    target 1542
  ]
  edge [
    source 39
    target 1543
  ]
  edge [
    source 39
    target 1544
  ]
  edge [
    source 39
    target 1545
  ]
  edge [
    source 39
    target 1546
  ]
  edge [
    source 39
    target 1547
  ]
  edge [
    source 39
    target 1548
  ]
  edge [
    source 39
    target 1549
  ]
  edge [
    source 39
    target 1550
  ]
  edge [
    source 39
    target 1576
  ]
  edge [
    source 39
    target 1577
  ]
  edge [
    source 39
    target 1578
  ]
  edge [
    source 39
    target 1579
  ]
  edge [
    source 39
    target 1477
  ]
  edge [
    source 39
    target 1480
  ]
  edge [
    source 39
    target 1580
  ]
  edge [
    source 39
    target 1581
  ]
  edge [
    source 39
    target 1483
  ]
  edge [
    source 39
    target 1582
  ]
  edge [
    source 39
    target 1583
  ]
  edge [
    source 39
    target 1502
  ]
  edge [
    source 39
    target 1584
  ]
  edge [
    source 39
    target 91
  ]
  edge [
    source 39
    target 1585
  ]
  edge [
    source 39
    target 1586
  ]
  edge [
    source 39
    target 1488
  ]
  edge [
    source 39
    target 1587
  ]
  edge [
    source 39
    target 1588
  ]
  edge [
    source 39
    target 1589
  ]
  edge [
    source 39
    target 1590
  ]
  edge [
    source 39
    target 1591
  ]
  edge [
    source 39
    target 1592
  ]
  edge [
    source 39
    target 1593
  ]
  edge [
    source 39
    target 1594
  ]
  edge [
    source 39
    target 1595
  ]
  edge [
    source 39
    target 1596
  ]
  edge [
    source 39
    target 1597
  ]
  edge [
    source 39
    target 1598
  ]
  edge [
    source 39
    target 1599
  ]
  edge [
    source 39
    target 1600
  ]
  edge [
    source 39
    target 1601
  ]
  edge [
    source 39
    target 1602
  ]
  edge [
    source 39
    target 1603
  ]
  edge [
    source 39
    target 1604
  ]
  edge [
    source 39
    target 1605
  ]
  edge [
    source 39
    target 1606
  ]
  edge [
    source 39
    target 1607
  ]
  edge [
    source 39
    target 1608
  ]
  edge [
    source 39
    target 1609
  ]
  edge [
    source 39
    target 1514
  ]
  edge [
    source 39
    target 1515
  ]
  edge [
    source 39
    target 1610
  ]
  edge [
    source 39
    target 1611
  ]
  edge [
    source 39
    target 1612
  ]
  edge [
    source 39
    target 1613
  ]
  edge [
    source 39
    target 1614
  ]
  edge [
    source 40
    target 1615
  ]
  edge [
    source 40
    target 1616
  ]
  edge [
    source 40
    target 1617
  ]
  edge [
    source 40
    target 1618
  ]
  edge [
    source 40
    target 1619
  ]
  edge [
    source 40
    target 1620
  ]
  edge [
    source 40
    target 1621
  ]
  edge [
    source 40
    target 1622
  ]
  edge [
    source 40
    target 1623
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 42
    target 151
  ]
  edge [
    source 42
    target 196
  ]
  edge [
    source 42
    target 197
  ]
  edge [
    source 42
    target 198
  ]
  edge [
    source 42
    target 199
  ]
  edge [
    source 42
    target 108
  ]
  edge [
    source 42
    target 200
  ]
  edge [
    source 42
    target 201
  ]
  edge [
    source 42
    target 202
  ]
  edge [
    source 42
    target 169
  ]
  edge [
    source 42
    target 170
  ]
  edge [
    source 42
    target 172
  ]
  edge [
    source 42
    target 203
  ]
  edge [
    source 42
    target 177
  ]
  edge [
    source 42
    target 178
  ]
  edge [
    source 42
    target 204
  ]
  edge [
    source 42
    target 180
  ]
  edge [
    source 42
    target 184
  ]
  edge [
    source 42
    target 182
  ]
  edge [
    source 42
    target 205
  ]
  edge [
    source 42
    target 206
  ]
  edge [
    source 42
    target 183
  ]
  edge [
    source 42
    target 207
  ]
  edge [
    source 42
    target 186
  ]
  edge [
    source 42
    target 208
  ]
  edge [
    source 42
    target 1624
  ]
  edge [
    source 42
    target 1625
  ]
  edge [
    source 42
    target 1626
  ]
  edge [
    source 42
    target 1627
  ]
  edge [
    source 42
    target 1628
  ]
  edge [
    source 42
    target 1629
  ]
  edge [
    source 42
    target 1630
  ]
  edge [
    source 42
    target 1631
  ]
  edge [
    source 42
    target 1632
  ]
  edge [
    source 42
    target 1633
  ]
  edge [
    source 42
    target 1634
  ]
  edge [
    source 42
    target 1635
  ]
  edge [
    source 42
    target 1636
  ]
  edge [
    source 42
    target 1637
  ]
  edge [
    source 42
    target 1638
  ]
  edge [
    source 42
    target 1639
  ]
  edge [
    source 42
    target 900
  ]
  edge [
    source 42
    target 1640
  ]
  edge [
    source 42
    target 1641
  ]
  edge [
    source 42
    target 1642
  ]
  edge [
    source 42
    target 1643
  ]
  edge [
    source 42
    target 144
  ]
  edge [
    source 42
    target 1644
  ]
  edge [
    source 42
    target 1645
  ]
  edge [
    source 42
    target 1646
  ]
  edge [
    source 42
    target 1314
  ]
  edge [
    source 42
    target 1647
  ]
  edge [
    source 42
    target 1648
  ]
  edge [
    source 42
    target 1649
  ]
  edge [
    source 42
    target 247
  ]
  edge [
    source 42
    target 1650
  ]
  edge [
    source 42
    target 269
  ]
  edge [
    source 42
    target 1651
  ]
  edge [
    source 42
    target 1652
  ]
  edge [
    source 42
    target 1653
  ]
  edge [
    source 42
    target 1654
  ]
  edge [
    source 42
    target 1655
  ]
  edge [
    source 42
    target 1656
  ]
  edge [
    source 42
    target 1657
  ]
  edge [
    source 42
    target 1658
  ]
  edge [
    source 42
    target 1659
  ]
  edge [
    source 42
    target 1660
  ]
  edge [
    source 42
    target 1661
  ]
  edge [
    source 42
    target 422
  ]
  edge [
    source 42
    target 1662
  ]
  edge [
    source 42
    target 1663
  ]
  edge [
    source 42
    target 1664
  ]
  edge [
    source 42
    target 1665
  ]
  edge [
    source 42
    target 1666
  ]
  edge [
    source 42
    target 460
  ]
  edge [
    source 42
    target 1667
  ]
  edge [
    source 42
    target 1668
  ]
  edge [
    source 42
    target 1669
  ]
  edge [
    source 42
    target 148
  ]
  edge [
    source 42
    target 672
  ]
  edge [
    source 42
    target 1670
  ]
  edge [
    source 42
    target 726
  ]
  edge [
    source 42
    target 1671
  ]
  edge [
    source 42
    target 931
  ]
  edge [
    source 42
    target 1672
  ]
  edge [
    source 42
    target 287
  ]
  edge [
    source 42
    target 1673
  ]
  edge [
    source 42
    target 289
  ]
  edge [
    source 42
    target 1674
  ]
  edge [
    source 42
    target 1675
  ]
  edge [
    source 42
    target 1676
  ]
  edge [
    source 42
    target 1677
  ]
  edge [
    source 42
    target 1678
  ]
  edge [
    source 42
    target 1679
  ]
  edge [
    source 42
    target 1680
  ]
  edge [
    source 42
    target 1681
  ]
  edge [
    source 42
    target 1682
  ]
  edge [
    source 42
    target 1683
  ]
  edge [
    source 42
    target 1684
  ]
  edge [
    source 42
    target 1685
  ]
  edge [
    source 42
    target 431
  ]
  edge [
    source 42
    target 1686
  ]
  edge [
    source 42
    target 442
  ]
  edge [
    source 42
    target 1687
  ]
  edge [
    source 42
    target 1688
  ]
  edge [
    source 42
    target 1689
  ]
  edge [
    source 42
    target 1690
  ]
  edge [
    source 42
    target 1691
  ]
  edge [
    source 42
    target 1045
  ]
  edge [
    source 42
    target 118
  ]
  edge [
    source 42
    target 1692
  ]
  edge [
    source 42
    target 1693
  ]
  edge [
    source 42
    target 1694
  ]
  edge [
    source 42
    target 1695
  ]
  edge [
    source 42
    target 1696
  ]
  edge [
    source 42
    target 1697
  ]
  edge [
    source 42
    target 1698
  ]
  edge [
    source 42
    target 1699
  ]
  edge [
    source 42
    target 1700
  ]
  edge [
    source 42
    target 1701
  ]
  edge [
    source 42
    target 74
  ]
  edge [
    source 42
    target 1702
  ]
  edge [
    source 42
    target 1703
  ]
  edge [
    source 42
    target 1704
  ]
  edge [
    source 42
    target 1705
  ]
  edge [
    source 42
    target 1706
  ]
  edge [
    source 42
    target 107
  ]
  edge [
    source 42
    target 1707
  ]
  edge [
    source 42
    target 1708
  ]
  edge [
    source 42
    target 1709
  ]
  edge [
    source 42
    target 1710
  ]
  edge [
    source 42
    target 1711
  ]
  edge [
    source 42
    target 1712
  ]
  edge [
    source 42
    target 1713
  ]
  edge [
    source 42
    target 784
  ]
  edge [
    source 42
    target 1714
  ]
  edge [
    source 42
    target 754
  ]
  edge [
    source 42
    target 1715
  ]
  edge [
    source 42
    target 96
  ]
  edge [
    source 42
    target 1716
  ]
  edge [
    source 42
    target 1717
  ]
  edge [
    source 42
    target 1718
  ]
  edge [
    source 42
    target 1719
  ]
  edge [
    source 42
    target 1720
  ]
  edge [
    source 42
    target 1721
  ]
  edge [
    source 42
    target 1722
  ]
  edge [
    source 42
    target 1723
  ]
  edge [
    source 42
    target 1724
  ]
  edge [
    source 42
    target 1725
  ]
  edge [
    source 42
    target 73
  ]
  edge [
    source 42
    target 209
  ]
  edge [
    source 42
    target 1033
  ]
  edge [
    source 42
    target 1726
  ]
  edge [
    source 42
    target 1727
  ]
  edge [
    source 42
    target 1728
  ]
  edge [
    source 42
    target 361
  ]
  edge [
    source 42
    target 1729
  ]
  edge [
    source 42
    target 1730
  ]
  edge [
    source 42
    target 1731
  ]
  edge [
    source 42
    target 1732
  ]
  edge [
    source 42
    target 571
  ]
  edge [
    source 42
    target 1733
  ]
  edge [
    source 42
    target 1734
  ]
  edge [
    source 42
    target 1735
  ]
  edge [
    source 42
    target 1736
  ]
  edge [
    source 42
    target 1737
  ]
  edge [
    source 42
    target 1738
  ]
  edge [
    source 42
    target 1739
  ]
  edge [
    source 42
    target 1740
  ]
  edge [
    source 42
    target 1741
  ]
  edge [
    source 42
    target 1742
  ]
  edge [
    source 42
    target 1743
  ]
  edge [
    source 42
    target 1744
  ]
  edge [
    source 42
    target 1745
  ]
  edge [
    source 42
    target 173
  ]
  edge [
    source 42
    target 1746
  ]
  edge [
    source 42
    target 1747
  ]
  edge [
    source 42
    target 1748
  ]
  edge [
    source 42
    target 1749
  ]
  edge [
    source 42
    target 1750
  ]
  edge [
    source 42
    target 1751
  ]
  edge [
    source 42
    target 1752
  ]
  edge [
    source 42
    target 1753
  ]
  edge [
    source 42
    target 1754
  ]
  edge [
    source 42
    target 1755
  ]
  edge [
    source 42
    target 1756
  ]
  edge [
    source 42
    target 1757
  ]
  edge [
    source 42
    target 1758
  ]
  edge [
    source 42
    target 128
  ]
  edge [
    source 42
    target 714
  ]
  edge [
    source 42
    target 1759
  ]
  edge [
    source 42
    target 1760
  ]
  edge [
    source 42
    target 1761
  ]
  edge [
    source 42
    target 902
  ]
  edge [
    source 42
    target 1762
  ]
  edge [
    source 42
    target 230
  ]
  edge [
    source 42
    target 1763
  ]
  edge [
    source 42
    target 1764
  ]
  edge [
    source 42
    target 1765
  ]
  edge [
    source 42
    target 1766
  ]
  edge [
    source 42
    target 492
  ]
  edge [
    source 42
    target 1767
  ]
  edge [
    source 42
    target 1768
  ]
  edge [
    source 42
    target 1769
  ]
  edge [
    source 42
    target 1770
  ]
  edge [
    source 42
    target 1771
  ]
  edge [
    source 42
    target 1772
  ]
  edge [
    source 42
    target 1773
  ]
  edge [
    source 42
    target 1774
  ]
  edge [
    source 42
    target 1775
  ]
  edge [
    source 42
    target 1240
  ]
  edge [
    source 42
    target 1776
  ]
  edge [
    source 42
    target 1777
  ]
  edge [
    source 42
    target 1778
  ]
  edge [
    source 42
    target 1779
  ]
  edge [
    source 42
    target 1780
  ]
  edge [
    source 42
    target 1781
  ]
  edge [
    source 42
    target 1782
  ]
  edge [
    source 42
    target 1783
  ]
  edge [
    source 42
    target 1784
  ]
  edge [
    source 42
    target 1785
  ]
  edge [
    source 42
    target 1786
  ]
  edge [
    source 42
    target 1787
  ]
  edge [
    source 42
    target 1788
  ]
  edge [
    source 42
    target 1789
  ]
  edge [
    source 42
    target 1790
  ]
  edge [
    source 42
    target 1791
  ]
  edge [
    source 42
    target 1792
  ]
  edge [
    source 42
    target 1793
  ]
  edge [
    source 42
    target 1794
  ]
  edge [
    source 42
    target 1795
  ]
  edge [
    source 42
    target 1796
  ]
  edge [
    source 42
    target 1797
  ]
  edge [
    source 42
    target 1798
  ]
  edge [
    source 42
    target 1394
  ]
  edge [
    source 42
    target 587
  ]
  edge [
    source 42
    target 237
  ]
  edge [
    source 42
    target 1799
  ]
  edge [
    source 42
    target 1800
  ]
  edge [
    source 42
    target 1801
  ]
  edge [
    source 42
    target 1802
  ]
  edge [
    source 42
    target 595
  ]
  edge [
    source 42
    target 551
  ]
  edge [
    source 42
    target 1803
  ]
  edge [
    source 42
    target 1804
  ]
  edge [
    source 42
    target 1805
  ]
  edge [
    source 42
    target 241
  ]
  edge [
    source 42
    target 1806
  ]
  edge [
    source 42
    target 1807
  ]
  edge [
    source 42
    target 1808
  ]
  edge [
    source 42
    target 1809
  ]
  edge [
    source 42
    target 1810
  ]
  edge [
    source 42
    target 1811
  ]
  edge [
    source 42
    target 1812
  ]
  edge [
    source 42
    target 1813
  ]
  edge [
    source 42
    target 567
  ]
  edge [
    source 42
    target 1814
  ]
  edge [
    source 42
    target 1815
  ]
  edge [
    source 42
    target 1816
  ]
  edge [
    source 42
    target 1817
  ]
  edge [
    source 42
    target 1818
  ]
  edge [
    source 42
    target 1819
  ]
  edge [
    source 42
    target 51
  ]
  edge [
    source 42
    target 52
  ]
  edge [
    source 42
    target 55
  ]
  edge [
    source 42
    target 56
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1820
  ]
  edge [
    source 45
    target 1821
  ]
  edge [
    source 46
    target 1023
  ]
  edge [
    source 46
    target 1822
  ]
  edge [
    source 46
    target 1030
  ]
  edge [
    source 47
    target 1823
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 1824
  ]
  edge [
    source 50
    target 1825
  ]
  edge [
    source 50
    target 1826
  ]
  edge [
    source 50
    target 1827
  ]
  edge [
    source 50
    target 1828
  ]
  edge [
    source 50
    target 1829
  ]
  edge [
    source 50
    target 931
  ]
  edge [
    source 50
    target 623
  ]
  edge [
    source 50
    target 1830
  ]
  edge [
    source 50
    target 1831
  ]
  edge [
    source 50
    target 1832
  ]
  edge [
    source 50
    target 1833
  ]
  edge [
    source 50
    target 1674
  ]
  edge [
    source 50
    target 1834
  ]
  edge [
    source 50
    target 1835
  ]
  edge [
    source 50
    target 1836
  ]
  edge [
    source 50
    target 1837
  ]
  edge [
    source 50
    target 1838
  ]
  edge [
    source 50
    target 1839
  ]
  edge [
    source 50
    target 1840
  ]
  edge [
    source 50
    target 1841
  ]
  edge [
    source 50
    target 944
  ]
  edge [
    source 50
    target 1842
  ]
  edge [
    source 50
    target 953
  ]
  edge [
    source 50
    target 1843
  ]
  edge [
    source 50
    target 100
  ]
  edge [
    source 50
    target 1844
  ]
  edge [
    source 50
    target 1845
  ]
  edge [
    source 50
    target 1846
  ]
  edge [
    source 50
    target 1180
  ]
  edge [
    source 50
    target 1847
  ]
  edge [
    source 50
    target 1848
  ]
  edge [
    source 50
    target 160
  ]
  edge [
    source 50
    target 1849
  ]
  edge [
    source 50
    target 1850
  ]
  edge [
    source 50
    target 1851
  ]
  edge [
    source 50
    target 1183
  ]
  edge [
    source 50
    target 1852
  ]
  edge [
    source 50
    target 1853
  ]
  edge [
    source 50
    target 963
  ]
  edge [
    source 50
    target 1854
  ]
  edge [
    source 50
    target 1855
  ]
  edge [
    source 50
    target 1856
  ]
  edge [
    source 50
    target 1857
  ]
  edge [
    source 50
    target 1858
  ]
  edge [
    source 50
    target 969
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 1859
  ]
  edge [
    source 51
    target 1860
  ]
  edge [
    source 51
    target 1861
  ]
  edge [
    source 51
    target 1862
  ]
  edge [
    source 51
    target 1863
  ]
  edge [
    source 51
    target 1864
  ]
  edge [
    source 51
    target 1865
  ]
  edge [
    source 51
    target 1866
  ]
  edge [
    source 51
    target 1867
  ]
  edge [
    source 51
    target 1868
  ]
  edge [
    source 51
    target 1869
  ]
  edge [
    source 51
    target 1870
  ]
  edge [
    source 51
    target 1871
  ]
  edge [
    source 51
    target 1493
  ]
  edge [
    source 51
    target 1872
  ]
  edge [
    source 51
    target 1873
  ]
  edge [
    source 51
    target 1874
  ]
  edge [
    source 51
    target 1875
  ]
  edge [
    source 51
    target 1876
  ]
  edge [
    source 51
    target 1877
  ]
  edge [
    source 51
    target 1878
  ]
  edge [
    source 51
    target 1879
  ]
  edge [
    source 51
    target 1880
  ]
  edge [
    source 51
    target 1881
  ]
  edge [
    source 51
    target 1882
  ]
  edge [
    source 51
    target 1883
  ]
  edge [
    source 51
    target 1884
  ]
  edge [
    source 51
    target 1719
  ]
  edge [
    source 51
    target 1885
  ]
  edge [
    source 51
    target 151
  ]
  edge [
    source 51
    target 196
  ]
  edge [
    source 51
    target 197
  ]
  edge [
    source 51
    target 198
  ]
  edge [
    source 51
    target 199
  ]
  edge [
    source 51
    target 108
  ]
  edge [
    source 51
    target 200
  ]
  edge [
    source 51
    target 201
  ]
  edge [
    source 51
    target 202
  ]
  edge [
    source 51
    target 169
  ]
  edge [
    source 51
    target 170
  ]
  edge [
    source 51
    target 172
  ]
  edge [
    source 51
    target 203
  ]
  edge [
    source 51
    target 177
  ]
  edge [
    source 51
    target 178
  ]
  edge [
    source 51
    target 204
  ]
  edge [
    source 51
    target 180
  ]
  edge [
    source 51
    target 184
  ]
  edge [
    source 51
    target 182
  ]
  edge [
    source 51
    target 205
  ]
  edge [
    source 51
    target 206
  ]
  edge [
    source 51
    target 183
  ]
  edge [
    source 51
    target 207
  ]
  edge [
    source 51
    target 186
  ]
  edge [
    source 51
    target 208
  ]
  edge [
    source 51
    target 1886
  ]
  edge [
    source 51
    target 301
  ]
  edge [
    source 51
    target 1887
  ]
  edge [
    source 51
    target 1888
  ]
  edge [
    source 51
    target 1889
  ]
  edge [
    source 51
    target 302
  ]
  edge [
    source 51
    target 1628
  ]
  edge [
    source 51
    target 1890
  ]
  edge [
    source 51
    target 1891
  ]
  edge [
    source 51
    target 127
  ]
  edge [
    source 51
    target 1892
  ]
  edge [
    source 51
    target 1893
  ]
  edge [
    source 51
    target 545
  ]
  edge [
    source 51
    target 1894
  ]
  edge [
    source 51
    target 1895
  ]
  edge [
    source 51
    target 1896
  ]
  edge [
    source 51
    target 1287
  ]
  edge [
    source 51
    target 1897
  ]
  edge [
    source 51
    target 1898
  ]
  edge [
    source 51
    target 1899
  ]
  edge [
    source 51
    target 1900
  ]
  edge [
    source 51
    target 1901
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 1902
  ]
  edge [
    source 52
    target 1903
  ]
  edge [
    source 52
    target 1702
  ]
  edge [
    source 52
    target 1904
  ]
  edge [
    source 52
    target 1905
  ]
  edge [
    source 52
    target 1906
  ]
  edge [
    source 52
    target 1907
  ]
  edge [
    source 52
    target 1908
  ]
  edge [
    source 52
    target 1909
  ]
  edge [
    source 52
    target 1910
  ]
  edge [
    source 52
    target 1911
  ]
  edge [
    source 52
    target 1912
  ]
  edge [
    source 52
    target 1913
  ]
  edge [
    source 52
    target 1914
  ]
  edge [
    source 52
    target 1915
  ]
  edge [
    source 52
    target 1916
  ]
  edge [
    source 52
    target 1917
  ]
  edge [
    source 52
    target 1918
  ]
  edge [
    source 52
    target 1919
  ]
  edge [
    source 52
    target 1920
  ]
  edge [
    source 52
    target 1921
  ]
  edge [
    source 52
    target 1922
  ]
  edge [
    source 52
    target 1923
  ]
  edge [
    source 52
    target 1924
  ]
  edge [
    source 52
    target 1859
  ]
  edge [
    source 52
    target 1860
  ]
  edge [
    source 52
    target 1861
  ]
  edge [
    source 52
    target 1862
  ]
  edge [
    source 52
    target 1863
  ]
  edge [
    source 52
    target 1864
  ]
  edge [
    source 52
    target 1865
  ]
  edge [
    source 52
    target 1866
  ]
  edge [
    source 52
    target 1867
  ]
  edge [
    source 52
    target 1868
  ]
  edge [
    source 52
    target 1869
  ]
  edge [
    source 52
    target 1870
  ]
  edge [
    source 52
    target 1871
  ]
  edge [
    source 52
    target 1493
  ]
  edge [
    source 52
    target 1872
  ]
  edge [
    source 52
    target 1873
  ]
  edge [
    source 52
    target 1874
  ]
  edge [
    source 52
    target 1875
  ]
  edge [
    source 52
    target 1876
  ]
  edge [
    source 52
    target 1877
  ]
  edge [
    source 52
    target 1878
  ]
  edge [
    source 52
    target 1879
  ]
  edge [
    source 52
    target 1880
  ]
  edge [
    source 52
    target 1881
  ]
  edge [
    source 52
    target 1882
  ]
  edge [
    source 52
    target 1883
  ]
  edge [
    source 52
    target 1884
  ]
  edge [
    source 52
    target 1719
  ]
  edge [
    source 52
    target 1885
  ]
  edge [
    source 52
    target 1925
  ]
  edge [
    source 52
    target 1926
  ]
  edge [
    source 52
    target 1927
  ]
  edge [
    source 52
    target 127
  ]
  edge [
    source 52
    target 1928
  ]
  edge [
    source 52
    target 1929
  ]
  edge [
    source 52
    target 1930
  ]
  edge [
    source 52
    target 1931
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 1932
  ]
  edge [
    source 54
    target 1084
  ]
  edge [
    source 54
    target 622
  ]
  edge [
    source 54
    target 1086
  ]
  edge [
    source 54
    target 1198
  ]
  edge [
    source 54
    target 1933
  ]
  edge [
    source 54
    target 1934
  ]
  edge [
    source 54
    target 1935
  ]
  edge [
    source 54
    target 306
  ]
  edge [
    source 54
    target 708
  ]
  edge [
    source 54
    target 1936
  ]
  edge [
    source 54
    target 1937
  ]
  edge [
    source 54
    target 1068
  ]
  edge [
    source 54
    target 687
  ]
  edge [
    source 54
    target 1938
  ]
  edge [
    source 54
    target 1939
  ]
  edge [
    source 54
    target 1940
  ]
  edge [
    source 54
    target 612
  ]
  edge [
    source 54
    target 683
  ]
  edge [
    source 54
    target 1941
  ]
  edge [
    source 54
    target 1942
  ]
  edge [
    source 54
    target 1943
  ]
  edge [
    source 54
    target 313
  ]
  edge [
    source 54
    target 1944
  ]
  edge [
    source 54
    target 1945
  ]
  edge [
    source 54
    target 1946
  ]
  edge [
    source 54
    target 1947
  ]
  edge [
    source 54
    target 1948
  ]
  edge [
    source 54
    target 677
  ]
  edge [
    source 54
    target 1949
  ]
  edge [
    source 54
    target 1950
  ]
  edge [
    source 54
    target 1083
  ]
  edge [
    source 54
    target 1951
  ]
  edge [
    source 54
    target 1952
  ]
  edge [
    source 54
    target 1953
  ]
  edge [
    source 54
    target 684
  ]
  edge [
    source 54
    target 1954
  ]
  edge [
    source 54
    target 1955
  ]
  edge [
    source 54
    target 750
  ]
  edge [
    source 54
    target 1956
  ]
  edge [
    source 54
    target 1957
  ]
  edge [
    source 54
    target 1958
  ]
  edge [
    source 54
    target 1959
  ]
  edge [
    source 54
    target 1446
  ]
  edge [
    source 54
    target 709
  ]
  edge [
    source 54
    target 1960
  ]
  edge [
    source 54
    target 1961
  ]
  edge [
    source 54
    target 1962
  ]
  edge [
    source 54
    target 1963
  ]
  edge [
    source 54
    target 378
  ]
  edge [
    source 54
    target 1964
  ]
  edge [
    source 54
    target 388
  ]
  edge [
    source 54
    target 339
  ]
  edge [
    source 54
    target 1088
  ]
  edge [
    source 54
    target 1965
  ]
  edge [
    source 54
    target 1966
  ]
  edge [
    source 54
    target 1967
  ]
  edge [
    source 54
    target 1968
  ]
  edge [
    source 54
    target 831
  ]
  edge [
    source 54
    target 1969
  ]
  edge [
    source 54
    target 1970
  ]
  edge [
    source 54
    target 1089
  ]
  edge [
    source 54
    target 1971
  ]
  edge [
    source 54
    target 1972
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 1973
  ]
  edge [
    source 55
    target 1974
  ]
  edge [
    source 55
    target 1975
  ]
  edge [
    source 55
    target 1976
  ]
  edge [
    source 55
    target 1977
  ]
  edge [
    source 55
    target 1978
  ]
  edge [
    source 55
    target 1979
  ]
  edge [
    source 55
    target 1980
  ]
  edge [
    source 55
    target 1981
  ]
  edge [
    source 55
    target 1982
  ]
  edge [
    source 55
    target 277
  ]
  edge [
    source 55
    target 1983
  ]
  edge [
    source 55
    target 1984
  ]
  edge [
    source 55
    target 1985
  ]
  edge [
    source 55
    target 1986
  ]
  edge [
    source 55
    target 1987
  ]
  edge [
    source 55
    target 1988
  ]
  edge [
    source 55
    target 1989
  ]
  edge [
    source 55
    target 1990
  ]
  edge [
    source 55
    target 1991
  ]
  edge [
    source 55
    target 1475
  ]
  edge [
    source 55
    target 1992
  ]
  edge [
    source 55
    target 596
  ]
  edge [
    source 55
    target 1543
  ]
  edge [
    source 55
    target 1993
  ]
  edge [
    source 55
    target 269
  ]
  edge [
    source 55
    target 1994
  ]
  edge [
    source 55
    target 1995
  ]
  edge [
    source 55
    target 1996
  ]
  edge [
    source 55
    target 1997
  ]
  edge [
    source 55
    target 1998
  ]
  edge [
    source 55
    target 1999
  ]
  edge [
    source 55
    target 2000
  ]
  edge [
    source 55
    target 2001
  ]
  edge [
    source 55
    target 2002
  ]
  edge [
    source 55
    target 2003
  ]
  edge [
    source 55
    target 2004
  ]
  edge [
    source 55
    target 2005
  ]
  edge [
    source 55
    target 2006
  ]
  edge [
    source 55
    target 2007
  ]
  edge [
    source 55
    target 2008
  ]
  edge [
    source 55
    target 2009
  ]
  edge [
    source 55
    target 2010
  ]
  edge [
    source 55
    target 2011
  ]
  edge [
    source 55
    target 1655
  ]
  edge [
    source 55
    target 2012
  ]
  edge [
    source 55
    target 2013
  ]
  edge [
    source 55
    target 2014
  ]
  edge [
    source 55
    target 2015
  ]
  edge [
    source 55
    target 2016
  ]
  edge [
    source 55
    target 2017
  ]
  edge [
    source 55
    target 2018
  ]
  edge [
    source 55
    target 2019
  ]
  edge [
    source 55
    target 151
  ]
  edge [
    source 55
    target 196
  ]
  edge [
    source 55
    target 197
  ]
  edge [
    source 55
    target 198
  ]
  edge [
    source 55
    target 199
  ]
  edge [
    source 55
    target 108
  ]
  edge [
    source 55
    target 200
  ]
  edge [
    source 55
    target 201
  ]
  edge [
    source 55
    target 202
  ]
  edge [
    source 55
    target 169
  ]
  edge [
    source 55
    target 170
  ]
  edge [
    source 55
    target 172
  ]
  edge [
    source 55
    target 203
  ]
  edge [
    source 55
    target 177
  ]
  edge [
    source 55
    target 178
  ]
  edge [
    source 55
    target 204
  ]
  edge [
    source 55
    target 180
  ]
  edge [
    source 55
    target 184
  ]
  edge [
    source 55
    target 182
  ]
  edge [
    source 55
    target 205
  ]
  edge [
    source 55
    target 206
  ]
  edge [
    source 55
    target 183
  ]
  edge [
    source 55
    target 207
  ]
  edge [
    source 55
    target 186
  ]
  edge [
    source 55
    target 208
  ]
  edge [
    source 55
    target 2020
  ]
  edge [
    source 55
    target 2021
  ]
  edge [
    source 55
    target 1540
  ]
  edge [
    source 55
    target 2022
  ]
  edge [
    source 55
    target 2023
  ]
  edge [
    source 55
    target 2024
  ]
  edge [
    source 55
    target 2025
  ]
  edge [
    source 55
    target 2026
  ]
  edge [
    source 55
    target 2027
  ]
  edge [
    source 55
    target 1233
  ]
  edge [
    source 55
    target 145
  ]
  edge [
    source 55
    target 2028
  ]
  edge [
    source 55
    target 2029
  ]
  edge [
    source 55
    target 2030
  ]
  edge [
    source 55
    target 2031
  ]
  edge [
    source 55
    target 2032
  ]
  edge [
    source 55
    target 2033
  ]
  edge [
    source 55
    target 2034
  ]
  edge [
    source 55
    target 2035
  ]
  edge [
    source 55
    target 2036
  ]
  edge [
    source 55
    target 2037
  ]
  edge [
    source 55
    target 1495
  ]
  edge [
    source 55
    target 2038
  ]
  edge [
    source 55
    target 2039
  ]
  edge [
    source 55
    target 1888
  ]
  edge [
    source 55
    target 2040
  ]
  edge [
    source 55
    target 2041
  ]
  edge [
    source 55
    target 1927
  ]
  edge [
    source 55
    target 2042
  ]
  edge [
    source 55
    target 2043
  ]
  edge [
    source 55
    target 2044
  ]
  edge [
    source 55
    target 2045
  ]
  edge [
    source 55
    target 2046
  ]
  edge [
    source 55
    target 2047
  ]
  edge [
    source 55
    target 2048
  ]
  edge [
    source 55
    target 2049
  ]
  edge [
    source 55
    target 2050
  ]
  edge [
    source 55
    target 2051
  ]
  edge [
    source 55
    target 2052
  ]
  edge [
    source 55
    target 2053
  ]
  edge [
    source 55
    target 2054
  ]
  edge [
    source 55
    target 2055
  ]
  edge [
    source 55
    target 2056
  ]
  edge [
    source 55
    target 2057
  ]
  edge [
    source 55
    target 1493
  ]
  edge [
    source 55
    target 1494
  ]
  edge [
    source 55
    target 1496
  ]
  edge [
    source 55
    target 2058
  ]
  edge [
    source 55
    target 1400
  ]
  edge [
    source 55
    target 2059
  ]
  edge [
    source 55
    target 2060
  ]
  edge [
    source 55
    target 2061
  ]
  edge [
    source 55
    target 2062
  ]
  edge [
    source 55
    target 2063
  ]
  edge [
    source 55
    target 2064
  ]
  edge [
    source 55
    target 1653
  ]
  edge [
    source 55
    target 2065
  ]
  edge [
    source 55
    target 2066
  ]
  edge [
    source 55
    target 2067
  ]
  edge [
    source 55
    target 2068
  ]
  edge [
    source 55
    target 2069
  ]
  edge [
    source 55
    target 464
  ]
  edge [
    source 55
    target 2070
  ]
  edge [
    source 55
    target 2071
  ]
  edge [
    source 55
    target 2072
  ]
  edge [
    source 55
    target 1770
  ]
  edge [
    source 55
    target 2073
  ]
  edge [
    source 55
    target 2074
  ]
  edge [
    source 55
    target 2075
  ]
  edge [
    source 55
    target 2076
  ]
  edge [
    source 55
    target 2077
  ]
  edge [
    source 55
    target 2078
  ]
  edge [
    source 55
    target 2079
  ]
  edge [
    source 55
    target 2080
  ]
  edge [
    source 55
    target 2081
  ]
  edge [
    source 55
    target 2082
  ]
  edge [
    source 55
    target 2083
  ]
  edge [
    source 55
    target 2084
  ]
  edge [
    source 55
    target 2085
  ]
  edge [
    source 55
    target 2086
  ]
  edge [
    source 55
    target 1695
  ]
  edge [
    source 55
    target 2087
  ]
  edge [
    source 55
    target 2088
  ]
  edge [
    source 55
    target 2089
  ]
  edge [
    source 55
    target 2090
  ]
  edge [
    source 55
    target 2091
  ]
  edge [
    source 55
    target 2092
  ]
  edge [
    source 55
    target 1399
  ]
  edge [
    source 55
    target 2093
  ]
  edge [
    source 55
    target 2094
  ]
  edge [
    source 55
    target 1474
  ]
  edge [
    source 55
    target 1476
  ]
  edge [
    source 55
    target 1477
  ]
  edge [
    source 55
    target 1478
  ]
  edge [
    source 55
    target 1479
  ]
  edge [
    source 55
    target 1480
  ]
  edge [
    source 55
    target 1481
  ]
  edge [
    source 55
    target 1482
  ]
  edge [
    source 55
    target 1483
  ]
  edge [
    source 55
    target 2095
  ]
  edge [
    source 55
    target 2096
  ]
  edge [
    source 55
    target 2097
  ]
  edge [
    source 55
    target 2098
  ]
  edge [
    source 55
    target 2099
  ]
  edge [
    source 55
    target 2100
  ]
  edge [
    source 55
    target 2101
  ]
  edge [
    source 55
    target 2102
  ]
  edge [
    source 55
    target 2103
  ]
  edge [
    source 55
    target 2104
  ]
  edge [
    source 55
    target 2105
  ]
  edge [
    source 55
    target 2106
  ]
  edge [
    source 55
    target 2107
  ]
  edge [
    source 55
    target 2108
  ]
  edge [
    source 55
    target 2109
  ]
  edge [
    source 55
    target 2110
  ]
  edge [
    source 55
    target 2111
  ]
  edge [
    source 55
    target 2112
  ]
  edge [
    source 55
    target 2113
  ]
  edge [
    source 55
    target 2114
  ]
  edge [
    source 55
    target 2115
  ]
  edge [
    source 55
    target 2116
  ]
  edge [
    source 55
    target 2117
  ]
  edge [
    source 55
    target 2118
  ]
  edge [
    source 55
    target 2119
  ]
  edge [
    source 55
    target 2120
  ]
  edge [
    source 55
    target 2121
  ]
  edge [
    source 55
    target 2122
  ]
  edge [
    source 55
    target 2123
  ]
  edge [
    source 55
    target 2124
  ]
  edge [
    source 55
    target 2125
  ]
  edge [
    source 55
    target 2126
  ]
  edge [
    source 55
    target 2127
  ]
  edge [
    source 55
    target 2128
  ]
  edge [
    source 55
    target 2129
  ]
  edge [
    source 55
    target 2130
  ]
  edge [
    source 55
    target 2131
  ]
  edge [
    source 55
    target 2132
  ]
  edge [
    source 55
    target 2133
  ]
  edge [
    source 55
    target 2134
  ]
  edge [
    source 55
    target 2135
  ]
  edge [
    source 55
    target 2136
  ]
  edge [
    source 55
    target 2137
  ]
  edge [
    source 55
    target 2138
  ]
  edge [
    source 55
    target 2139
  ]
  edge [
    source 55
    target 2140
  ]
  edge [
    source 55
    target 2141
  ]
  edge [
    source 55
    target 2142
  ]
  edge [
    source 55
    target 2143
  ]
  edge [
    source 55
    target 2144
  ]
  edge [
    source 55
    target 2145
  ]
  edge [
    source 55
    target 2146
  ]
  edge [
    source 55
    target 2147
  ]
  edge [
    source 55
    target 2148
  ]
  edge [
    source 55
    target 2149
  ]
  edge [
    source 55
    target 2150
  ]
  edge [
    source 55
    target 426
  ]
  edge [
    source 55
    target 2151
  ]
  edge [
    source 55
    target 2152
  ]
  edge [
    source 55
    target 2153
  ]
  edge [
    source 55
    target 2154
  ]
  edge [
    source 55
    target 2155
  ]
  edge [
    source 55
    target 2156
  ]
  edge [
    source 55
    target 2157
  ]
  edge [
    source 55
    target 2158
  ]
  edge [
    source 55
    target 2159
  ]
  edge [
    source 55
    target 2160
  ]
  edge [
    source 55
    target 2161
  ]
  edge [
    source 55
    target 1200
  ]
  edge [
    source 55
    target 2162
  ]
  edge [
    source 55
    target 2163
  ]
  edge [
    source 55
    target 2164
  ]
  edge [
    source 55
    target 2165
  ]
  edge [
    source 55
    target 2166
  ]
  edge [
    source 55
    target 2167
  ]
  edge [
    source 55
    target 2168
  ]
  edge [
    source 55
    target 2169
  ]
  edge [
    source 55
    target 2170
  ]
  edge [
    source 55
    target 2171
  ]
  edge [
    source 55
    target 2172
  ]
  edge [
    source 55
    target 2173
  ]
  edge [
    source 55
    target 2174
  ]
  edge [
    source 55
    target 2175
  ]
  edge [
    source 55
    target 2176
  ]
  edge [
    source 55
    target 2177
  ]
  edge [
    source 55
    target 2178
  ]
  edge [
    source 55
    target 2179
  ]
  edge [
    source 55
    target 2180
  ]
  edge [
    source 55
    target 2181
  ]
  edge [
    source 55
    target 2182
  ]
  edge [
    source 55
    target 2183
  ]
  edge [
    source 55
    target 2184
  ]
  edge [
    source 55
    target 2185
  ]
  edge [
    source 55
    target 2186
  ]
  edge [
    source 55
    target 2187
  ]
  edge [
    source 55
    target 2188
  ]
  edge [
    source 55
    target 2189
  ]
  edge [
    source 55
    target 2190
  ]
  edge [
    source 55
    target 2191
  ]
  edge [
    source 55
    target 2192
  ]
  edge [
    source 55
    target 2193
  ]
  edge [
    source 55
    target 2194
  ]
  edge [
    source 55
    target 2195
  ]
  edge [
    source 55
    target 2196
  ]
  edge [
    source 55
    target 2197
  ]
  edge [
    source 55
    target 2198
  ]
  edge [
    source 55
    target 2199
  ]
  edge [
    source 55
    target 2200
  ]
  edge [
    source 55
    target 2201
  ]
  edge [
    source 55
    target 2202
  ]
  edge [
    source 55
    target 2203
  ]
  edge [
    source 55
    target 2204
  ]
  edge [
    source 55
    target 2205
  ]
  edge [
    source 55
    target 2206
  ]
  edge [
    source 55
    target 2207
  ]
  edge [
    source 55
    target 2208
  ]
  edge [
    source 55
    target 2209
  ]
  edge [
    source 55
    target 2210
  ]
  edge [
    source 55
    target 2211
  ]
  edge [
    source 55
    target 2212
  ]
  edge [
    source 55
    target 2213
  ]
  edge [
    source 55
    target 2214
  ]
  edge [
    source 55
    target 2215
  ]
  edge [
    source 55
    target 2216
  ]
  edge [
    source 55
    target 2217
  ]
  edge [
    source 55
    target 2218
  ]
  edge [
    source 55
    target 2219
  ]
  edge [
    source 55
    target 2220
  ]
  edge [
    source 55
    target 2221
  ]
  edge [
    source 55
    target 2222
  ]
  edge [
    source 55
    target 2223
  ]
  edge [
    source 55
    target 2224
  ]
  edge [
    source 55
    target 2225
  ]
  edge [
    source 55
    target 2226
  ]
  edge [
    source 55
    target 2227
  ]
  edge [
    source 55
    target 2228
  ]
  edge [
    source 55
    target 2229
  ]
  edge [
    source 55
    target 2230
  ]
  edge [
    source 55
    target 2231
  ]
  edge [
    source 55
    target 2232
  ]
  edge [
    source 55
    target 2233
  ]
  edge [
    source 55
    target 2234
  ]
  edge [
    source 55
    target 2235
  ]
  edge [
    source 55
    target 2236
  ]
  edge [
    source 55
    target 2237
  ]
  edge [
    source 55
    target 2238
  ]
  edge [
    source 55
    target 2239
  ]
  edge [
    source 55
    target 2240
  ]
  edge [
    source 55
    target 2241
  ]
  edge [
    source 55
    target 2242
  ]
  edge [
    source 55
    target 2243
  ]
  edge [
    source 55
    target 2244
  ]
  edge [
    source 55
    target 2245
  ]
  edge [
    source 55
    target 2246
  ]
  edge [
    source 55
    target 2247
  ]
  edge [
    source 55
    target 2248
  ]
  edge [
    source 55
    target 2249
  ]
  edge [
    source 55
    target 2250
  ]
  edge [
    source 55
    target 2251
  ]
  edge [
    source 55
    target 2252
  ]
  edge [
    source 55
    target 2253
  ]
  edge [
    source 55
    target 2254
  ]
  edge [
    source 55
    target 2255
  ]
  edge [
    source 55
    target 366
  ]
  edge [
    source 55
    target 2256
  ]
  edge [
    source 55
    target 2257
  ]
  edge [
    source 55
    target 2258
  ]
  edge [
    source 55
    target 2259
  ]
  edge [
    source 55
    target 2260
  ]
  edge [
    source 55
    target 2261
  ]
  edge [
    source 55
    target 2262
  ]
  edge [
    source 55
    target 2263
  ]
  edge [
    source 55
    target 2264
  ]
  edge [
    source 55
    target 2265
  ]
  edge [
    source 55
    target 2266
  ]
  edge [
    source 55
    target 2267
  ]
  edge [
    source 55
    target 2268
  ]
  edge [
    source 55
    target 2269
  ]
  edge [
    source 55
    target 2270
  ]
  edge [
    source 55
    target 2271
  ]
  edge [
    source 55
    target 2272
  ]
  edge [
    source 55
    target 2273
  ]
  edge [
    source 55
    target 2274
  ]
  edge [
    source 55
    target 2275
  ]
  edge [
    source 55
    target 1670
  ]
  edge [
    source 55
    target 2276
  ]
  edge [
    source 55
    target 2277
  ]
  edge [
    source 55
    target 2278
  ]
  edge [
    source 55
    target 2279
  ]
  edge [
    source 55
    target 2280
  ]
  edge [
    source 55
    target 2281
  ]
  edge [
    source 55
    target 2282
  ]
  edge [
    source 55
    target 2283
  ]
  edge [
    source 55
    target 2284
  ]
  edge [
    source 55
    target 2285
  ]
  edge [
    source 55
    target 2286
  ]
  edge [
    source 55
    target 2287
  ]
  edge [
    source 55
    target 2288
  ]
  edge [
    source 55
    target 2289
  ]
  edge [
    source 55
    target 2290
  ]
  edge [
    source 55
    target 2291
  ]
  edge [
    source 55
    target 2292
  ]
  edge [
    source 55
    target 2293
  ]
  edge [
    source 55
    target 2294
  ]
  edge [
    source 55
    target 2295
  ]
  edge [
    source 55
    target 2296
  ]
  edge [
    source 55
    target 2297
  ]
  edge [
    source 55
    target 2298
  ]
  edge [
    source 55
    target 2299
  ]
  edge [
    source 55
    target 2300
  ]
  edge [
    source 55
    target 2301
  ]
  edge [
    source 55
    target 2302
  ]
  edge [
    source 55
    target 2303
  ]
  edge [
    source 55
    target 2304
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 1703
  ]
  edge [
    source 56
    target 2305
  ]
  edge [
    source 56
    target 2306
  ]
  edge [
    source 56
    target 2307
  ]
  edge [
    source 56
    target 2308
  ]
  edge [
    source 56
    target 2309
  ]
  edge [
    source 56
    target 108
  ]
  edge [
    source 56
    target 182
  ]
  edge [
    source 56
    target 387
  ]
  edge [
    source 56
    target 1716
  ]
  edge [
    source 57
    target 2310
  ]
  edge [
    source 57
    target 2311
  ]
  edge [
    source 57
    target 2312
  ]
  edge [
    source 57
    target 2313
  ]
  edge [
    source 57
    target 2314
  ]
  edge [
    source 57
    target 2315
  ]
  edge [
    source 57
    target 491
  ]
  edge [
    source 57
    target 2316
  ]
  edge [
    source 57
    target 2317
  ]
  edge [
    source 2318
    target 2319
  ]
  edge [
    source 2321
    target 2322
  ]
  edge [
    source 2323
    target 2324
  ]
  edge [
    source 2326
    target 2327
  ]
]
