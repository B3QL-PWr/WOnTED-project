graph [
  node [
    id 0
    label "ocena"
    origin "text"
  ]
  node [
    id 1
    label "ratingowy"
    origin "text"
  ]
  node [
    id 2
    label "pozna&#324;"
    origin "text"
  ]
  node [
    id 3
    label "potwierdzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "kryterium"
  ]
  node [
    id 5
    label "sofcik"
  ]
  node [
    id 6
    label "informacja"
  ]
  node [
    id 7
    label "pogl&#261;d"
  ]
  node [
    id 8
    label "decyzja"
  ]
  node [
    id 9
    label "appraisal"
  ]
  node [
    id 10
    label "zdecydowanie"
  ]
  node [
    id 11
    label "management"
  ]
  node [
    id 12
    label "resolution"
  ]
  node [
    id 13
    label "dokument"
  ]
  node [
    id 14
    label "wytw&#243;r"
  ]
  node [
    id 15
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 16
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 17
    label "teoria_Arrheniusa"
  ]
  node [
    id 18
    label "belief"
  ]
  node [
    id 19
    label "zderzenie_si&#281;"
  ]
  node [
    id 20
    label "s&#261;d"
  ]
  node [
    id 21
    label "teologicznie"
  ]
  node [
    id 22
    label "punkt"
  ]
  node [
    id 23
    label "powzi&#281;cie"
  ]
  node [
    id 24
    label "obieganie"
  ]
  node [
    id 25
    label "sygna&#322;"
  ]
  node [
    id 26
    label "obiec"
  ]
  node [
    id 27
    label "doj&#347;&#263;"
  ]
  node [
    id 28
    label "wiedza"
  ]
  node [
    id 29
    label "publikacja"
  ]
  node [
    id 30
    label "powzi&#261;&#263;"
  ]
  node [
    id 31
    label "doj&#347;cie"
  ]
  node [
    id 32
    label "obiega&#263;"
  ]
  node [
    id 33
    label "obiegni&#281;cie"
  ]
  node [
    id 34
    label "dane"
  ]
  node [
    id 35
    label "czynnik"
  ]
  node [
    id 36
    label "pornografia"
  ]
  node [
    id 37
    label "pornografia_mi&#281;kka"
  ]
  node [
    id 38
    label "poziom"
  ]
  node [
    id 39
    label "drobiazg"
  ]
  node [
    id 40
    label "przy&#347;wiadczy&#263;"
  ]
  node [
    id 41
    label "stwierdzi&#263;"
  ]
  node [
    id 42
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 43
    label "acknowledge"
  ]
  node [
    id 44
    label "attest"
  ]
  node [
    id 45
    label "testify"
  ]
  node [
    id 46
    label "declare"
  ]
  node [
    id 47
    label "oznajmi&#263;"
  ]
  node [
    id 48
    label "uzna&#263;"
  ]
  node [
    id 49
    label "powiedzie&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
]
