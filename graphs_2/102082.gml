graph [
  node [
    id 0
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 1
    label "czas"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "blog"
    origin "text"
  ]
  node [
    id 4
    label "learning"
    origin "text"
  ]
  node [
    id 5
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "zestawienie"
    origin "text"
  ]
  node [
    id 8
    label "dobry"
    origin "text"
  ]
  node [
    id 9
    label "zdanie"
    origin "text"
  ]
  node [
    id 10
    label "autor"
    origin "text"
  ]
  node [
    id 11
    label "strona"
    origin "text"
  ]
  node [
    id 12
    label "darmowy"
    origin "text"
  ]
  node [
    id 13
    label "kurs"
    origin "text"
  ]
  node [
    id 14
    label "przyzwoity"
  ]
  node [
    id 15
    label "ciekawy"
  ]
  node [
    id 16
    label "jako&#347;"
  ]
  node [
    id 17
    label "jako_tako"
  ]
  node [
    id 18
    label "niez&#322;y"
  ]
  node [
    id 19
    label "dziwny"
  ]
  node [
    id 20
    label "charakterystyczny"
  ]
  node [
    id 21
    label "intensywny"
  ]
  node [
    id 22
    label "udolny"
  ]
  node [
    id 23
    label "skuteczny"
  ]
  node [
    id 24
    label "&#347;mieszny"
  ]
  node [
    id 25
    label "niczegowaty"
  ]
  node [
    id 26
    label "dobrze"
  ]
  node [
    id 27
    label "nieszpetny"
  ]
  node [
    id 28
    label "spory"
  ]
  node [
    id 29
    label "pozytywny"
  ]
  node [
    id 30
    label "korzystny"
  ]
  node [
    id 31
    label "nie&#378;le"
  ]
  node [
    id 32
    label "kulturalny"
  ]
  node [
    id 33
    label "skromny"
  ]
  node [
    id 34
    label "grzeczny"
  ]
  node [
    id 35
    label "stosowny"
  ]
  node [
    id 36
    label "przystojny"
  ]
  node [
    id 37
    label "nale&#380;yty"
  ]
  node [
    id 38
    label "moralny"
  ]
  node [
    id 39
    label "przyzwoicie"
  ]
  node [
    id 40
    label "wystarczaj&#261;cy"
  ]
  node [
    id 41
    label "nietuzinkowy"
  ]
  node [
    id 42
    label "cz&#322;owiek"
  ]
  node [
    id 43
    label "intryguj&#261;cy"
  ]
  node [
    id 44
    label "ch&#281;tny"
  ]
  node [
    id 45
    label "swoisty"
  ]
  node [
    id 46
    label "interesowanie"
  ]
  node [
    id 47
    label "interesuj&#261;cy"
  ]
  node [
    id 48
    label "ciekawie"
  ]
  node [
    id 49
    label "indagator"
  ]
  node [
    id 50
    label "charakterystycznie"
  ]
  node [
    id 51
    label "szczeg&#243;lny"
  ]
  node [
    id 52
    label "wyj&#261;tkowy"
  ]
  node [
    id 53
    label "typowy"
  ]
  node [
    id 54
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 55
    label "podobny"
  ]
  node [
    id 56
    label "dziwnie"
  ]
  node [
    id 57
    label "dziwy"
  ]
  node [
    id 58
    label "inny"
  ]
  node [
    id 59
    label "w_miar&#281;"
  ]
  node [
    id 60
    label "jako_taki"
  ]
  node [
    id 61
    label "poprzedzanie"
  ]
  node [
    id 62
    label "czasoprzestrze&#324;"
  ]
  node [
    id 63
    label "laba"
  ]
  node [
    id 64
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 65
    label "chronometria"
  ]
  node [
    id 66
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 67
    label "rachuba_czasu"
  ]
  node [
    id 68
    label "przep&#322;ywanie"
  ]
  node [
    id 69
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 70
    label "czasokres"
  ]
  node [
    id 71
    label "odczyt"
  ]
  node [
    id 72
    label "chwila"
  ]
  node [
    id 73
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 74
    label "dzieje"
  ]
  node [
    id 75
    label "kategoria_gramatyczna"
  ]
  node [
    id 76
    label "poprzedzenie"
  ]
  node [
    id 77
    label "trawienie"
  ]
  node [
    id 78
    label "pochodzi&#263;"
  ]
  node [
    id 79
    label "period"
  ]
  node [
    id 80
    label "okres_czasu"
  ]
  node [
    id 81
    label "poprzedza&#263;"
  ]
  node [
    id 82
    label "schy&#322;ek"
  ]
  node [
    id 83
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 84
    label "odwlekanie_si&#281;"
  ]
  node [
    id 85
    label "zegar"
  ]
  node [
    id 86
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 87
    label "czwarty_wymiar"
  ]
  node [
    id 88
    label "pochodzenie"
  ]
  node [
    id 89
    label "koniugacja"
  ]
  node [
    id 90
    label "Zeitgeist"
  ]
  node [
    id 91
    label "trawi&#263;"
  ]
  node [
    id 92
    label "pogoda"
  ]
  node [
    id 93
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 94
    label "poprzedzi&#263;"
  ]
  node [
    id 95
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 96
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 97
    label "time_period"
  ]
  node [
    id 98
    label "time"
  ]
  node [
    id 99
    label "blok"
  ]
  node [
    id 100
    label "handout"
  ]
  node [
    id 101
    label "pomiar"
  ]
  node [
    id 102
    label "lecture"
  ]
  node [
    id 103
    label "reading"
  ]
  node [
    id 104
    label "podawanie"
  ]
  node [
    id 105
    label "wyk&#322;ad"
  ]
  node [
    id 106
    label "potrzyma&#263;"
  ]
  node [
    id 107
    label "warunki"
  ]
  node [
    id 108
    label "pok&#243;j"
  ]
  node [
    id 109
    label "atak"
  ]
  node [
    id 110
    label "program"
  ]
  node [
    id 111
    label "zjawisko"
  ]
  node [
    id 112
    label "meteorology"
  ]
  node [
    id 113
    label "weather"
  ]
  node [
    id 114
    label "prognoza_meteorologiczna"
  ]
  node [
    id 115
    label "czas_wolny"
  ]
  node [
    id 116
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 117
    label "metrologia"
  ]
  node [
    id 118
    label "godzinnik"
  ]
  node [
    id 119
    label "bicie"
  ]
  node [
    id 120
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 121
    label "wahad&#322;o"
  ]
  node [
    id 122
    label "kurant"
  ]
  node [
    id 123
    label "cyferblat"
  ]
  node [
    id 124
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 125
    label "nabicie"
  ]
  node [
    id 126
    label "werk"
  ]
  node [
    id 127
    label "czasomierz"
  ]
  node [
    id 128
    label "tyka&#263;"
  ]
  node [
    id 129
    label "tykn&#261;&#263;"
  ]
  node [
    id 130
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 131
    label "urz&#261;dzenie"
  ]
  node [
    id 132
    label "kotwica"
  ]
  node [
    id 133
    label "fleksja"
  ]
  node [
    id 134
    label "liczba"
  ]
  node [
    id 135
    label "coupling"
  ]
  node [
    id 136
    label "osoba"
  ]
  node [
    id 137
    label "tryb"
  ]
  node [
    id 138
    label "czasownik"
  ]
  node [
    id 139
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 140
    label "orz&#281;sek"
  ]
  node [
    id 141
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 142
    label "zaczynanie_si&#281;"
  ]
  node [
    id 143
    label "str&#243;j"
  ]
  node [
    id 144
    label "wynikanie"
  ]
  node [
    id 145
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 146
    label "origin"
  ]
  node [
    id 147
    label "background"
  ]
  node [
    id 148
    label "geneza"
  ]
  node [
    id 149
    label "beginning"
  ]
  node [
    id 150
    label "digestion"
  ]
  node [
    id 151
    label "unicestwianie"
  ]
  node [
    id 152
    label "sp&#281;dzanie"
  ]
  node [
    id 153
    label "contemplation"
  ]
  node [
    id 154
    label "rozk&#322;adanie"
  ]
  node [
    id 155
    label "marnowanie"
  ]
  node [
    id 156
    label "proces_fizjologiczny"
  ]
  node [
    id 157
    label "przetrawianie"
  ]
  node [
    id 158
    label "perystaltyka"
  ]
  node [
    id 159
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 160
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 161
    label "przebywa&#263;"
  ]
  node [
    id 162
    label "pour"
  ]
  node [
    id 163
    label "carry"
  ]
  node [
    id 164
    label "sail"
  ]
  node [
    id 165
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 166
    label "go&#347;ci&#263;"
  ]
  node [
    id 167
    label "mija&#263;"
  ]
  node [
    id 168
    label "proceed"
  ]
  node [
    id 169
    label "odej&#347;cie"
  ]
  node [
    id 170
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 171
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 172
    label "zanikni&#281;cie"
  ]
  node [
    id 173
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 174
    label "ciecz"
  ]
  node [
    id 175
    label "opuszczenie"
  ]
  node [
    id 176
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 177
    label "departure"
  ]
  node [
    id 178
    label "oddalenie_si&#281;"
  ]
  node [
    id 179
    label "przeby&#263;"
  ]
  node [
    id 180
    label "min&#261;&#263;"
  ]
  node [
    id 181
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 182
    label "swimming"
  ]
  node [
    id 183
    label "zago&#347;ci&#263;"
  ]
  node [
    id 184
    label "cross"
  ]
  node [
    id 185
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 186
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 187
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 188
    label "zrobi&#263;"
  ]
  node [
    id 189
    label "opatrzy&#263;"
  ]
  node [
    id 190
    label "overwhelm"
  ]
  node [
    id 191
    label "opatrywa&#263;"
  ]
  node [
    id 192
    label "date"
  ]
  node [
    id 193
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 194
    label "wynika&#263;"
  ]
  node [
    id 195
    label "fall"
  ]
  node [
    id 196
    label "poby&#263;"
  ]
  node [
    id 197
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 198
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 199
    label "bolt"
  ]
  node [
    id 200
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 201
    label "spowodowa&#263;"
  ]
  node [
    id 202
    label "uda&#263;_si&#281;"
  ]
  node [
    id 203
    label "opatrzenie"
  ]
  node [
    id 204
    label "zdarzenie_si&#281;"
  ]
  node [
    id 205
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 206
    label "progress"
  ]
  node [
    id 207
    label "opatrywanie"
  ]
  node [
    id 208
    label "mini&#281;cie"
  ]
  node [
    id 209
    label "doznanie"
  ]
  node [
    id 210
    label "zaistnienie"
  ]
  node [
    id 211
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 212
    label "przebycie"
  ]
  node [
    id 213
    label "cruise"
  ]
  node [
    id 214
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 215
    label "usuwa&#263;"
  ]
  node [
    id 216
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 217
    label "lutowa&#263;"
  ]
  node [
    id 218
    label "marnowa&#263;"
  ]
  node [
    id 219
    label "przetrawia&#263;"
  ]
  node [
    id 220
    label "poch&#322;ania&#263;"
  ]
  node [
    id 221
    label "digest"
  ]
  node [
    id 222
    label "metal"
  ]
  node [
    id 223
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 224
    label "sp&#281;dza&#263;"
  ]
  node [
    id 225
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 226
    label "zjawianie_si&#281;"
  ]
  node [
    id 227
    label "przebywanie"
  ]
  node [
    id 228
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 229
    label "mijanie"
  ]
  node [
    id 230
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 231
    label "zaznawanie"
  ]
  node [
    id 232
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 233
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 234
    label "flux"
  ]
  node [
    id 235
    label "epoka"
  ]
  node [
    id 236
    label "charakter"
  ]
  node [
    id 237
    label "flow"
  ]
  node [
    id 238
    label "choroba_przyrodzona"
  ]
  node [
    id 239
    label "ciota"
  ]
  node [
    id 240
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 241
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 242
    label "kres"
  ]
  node [
    id 243
    label "przestrze&#324;"
  ]
  node [
    id 244
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 245
    label "komcio"
  ]
  node [
    id 246
    label "blogosfera"
  ]
  node [
    id 247
    label "pami&#281;tnik"
  ]
  node [
    id 248
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 249
    label "pami&#261;tka"
  ]
  node [
    id 250
    label "notes"
  ]
  node [
    id 251
    label "zapiski"
  ]
  node [
    id 252
    label "raptularz"
  ]
  node [
    id 253
    label "album"
  ]
  node [
    id 254
    label "utw&#243;r_epicki"
  ]
  node [
    id 255
    label "kartka"
  ]
  node [
    id 256
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 257
    label "logowanie"
  ]
  node [
    id 258
    label "plik"
  ]
  node [
    id 259
    label "s&#261;d"
  ]
  node [
    id 260
    label "adres_internetowy"
  ]
  node [
    id 261
    label "linia"
  ]
  node [
    id 262
    label "serwis_internetowy"
  ]
  node [
    id 263
    label "posta&#263;"
  ]
  node [
    id 264
    label "bok"
  ]
  node [
    id 265
    label "skr&#281;canie"
  ]
  node [
    id 266
    label "skr&#281;ca&#263;"
  ]
  node [
    id 267
    label "orientowanie"
  ]
  node [
    id 268
    label "skr&#281;ci&#263;"
  ]
  node [
    id 269
    label "uj&#281;cie"
  ]
  node [
    id 270
    label "zorientowanie"
  ]
  node [
    id 271
    label "ty&#322;"
  ]
  node [
    id 272
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 273
    label "fragment"
  ]
  node [
    id 274
    label "layout"
  ]
  node [
    id 275
    label "obiekt"
  ]
  node [
    id 276
    label "zorientowa&#263;"
  ]
  node [
    id 277
    label "pagina"
  ]
  node [
    id 278
    label "podmiot"
  ]
  node [
    id 279
    label "g&#243;ra"
  ]
  node [
    id 280
    label "orientowa&#263;"
  ]
  node [
    id 281
    label "voice"
  ]
  node [
    id 282
    label "orientacja"
  ]
  node [
    id 283
    label "prz&#243;d"
  ]
  node [
    id 284
    label "internet"
  ]
  node [
    id 285
    label "powierzchnia"
  ]
  node [
    id 286
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 287
    label "forma"
  ]
  node [
    id 288
    label "skr&#281;cenie"
  ]
  node [
    id 289
    label "zbi&#243;r"
  ]
  node [
    id 290
    label "komentarz"
  ]
  node [
    id 291
    label "sumariusz"
  ]
  node [
    id 292
    label "ustawienie"
  ]
  node [
    id 293
    label "z&#322;amanie"
  ]
  node [
    id 294
    label "kompozycja"
  ]
  node [
    id 295
    label "strata"
  ]
  node [
    id 296
    label "composition"
  ]
  node [
    id 297
    label "book"
  ]
  node [
    id 298
    label "informacja"
  ]
  node [
    id 299
    label "stock"
  ]
  node [
    id 300
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 301
    label "catalog"
  ]
  node [
    id 302
    label "z&#322;o&#380;enie"
  ]
  node [
    id 303
    label "sprawozdanie_finansowe"
  ]
  node [
    id 304
    label "figurowa&#263;"
  ]
  node [
    id 305
    label "z&#322;&#261;czenie"
  ]
  node [
    id 306
    label "count"
  ]
  node [
    id 307
    label "wyra&#380;enie"
  ]
  node [
    id 308
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 309
    label "wyliczanka"
  ]
  node [
    id 310
    label "set"
  ]
  node [
    id 311
    label "analiza"
  ]
  node [
    id 312
    label "deficyt"
  ]
  node [
    id 313
    label "obrot&#243;wka"
  ]
  node [
    id 314
    label "przedstawienie"
  ]
  node [
    id 315
    label "pozycja"
  ]
  node [
    id 316
    label "tekst"
  ]
  node [
    id 317
    label "comparison"
  ]
  node [
    id 318
    label "zanalizowanie"
  ]
  node [
    id 319
    label "przybycie"
  ]
  node [
    id 320
    label "zmuszenie"
  ]
  node [
    id 321
    label "ukradzenie"
  ]
  node [
    id 322
    label "shoplifting"
  ]
  node [
    id 323
    label "zgromadzenie"
  ]
  node [
    id 324
    label "contract"
  ]
  node [
    id 325
    label "levy"
  ]
  node [
    id 326
    label "zmniejszenie"
  ]
  node [
    id 327
    label "spowodowanie"
  ]
  node [
    id 328
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 329
    label "contraction"
  ]
  node [
    id 330
    label "dostanie_si&#281;"
  ]
  node [
    id 331
    label "zniesienie"
  ]
  node [
    id 332
    label "przepisanie"
  ]
  node [
    id 333
    label "przewi&#261;zanie"
  ]
  node [
    id 334
    label "czynno&#347;&#263;"
  ]
  node [
    id 335
    label "odprowadzenie"
  ]
  node [
    id 336
    label "przebieranie"
  ]
  node [
    id 337
    label "zgromadzenie_si&#281;"
  ]
  node [
    id 338
    label "zdj&#281;cie"
  ]
  node [
    id 339
    label "u&#322;o&#380;enie"
  ]
  node [
    id 340
    label "ustalenie"
  ]
  node [
    id 341
    label "erection"
  ]
  node [
    id 342
    label "setup"
  ]
  node [
    id 343
    label "erecting"
  ]
  node [
    id 344
    label "rozmieszczenie"
  ]
  node [
    id 345
    label "poustawianie"
  ]
  node [
    id 346
    label "zinterpretowanie"
  ]
  node [
    id 347
    label "porozstawianie"
  ]
  node [
    id 348
    label "rola"
  ]
  node [
    id 349
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 350
    label "danie"
  ]
  node [
    id 351
    label "blend"
  ]
  node [
    id 352
    label "zgi&#281;cie"
  ]
  node [
    id 353
    label "fold"
  ]
  node [
    id 354
    label "opracowanie"
  ]
  node [
    id 355
    label "posk&#322;adanie"
  ]
  node [
    id 356
    label "przekazanie"
  ]
  node [
    id 357
    label "stage_set"
  ]
  node [
    id 358
    label "powiedzenie"
  ]
  node [
    id 359
    label "leksem"
  ]
  node [
    id 360
    label "przy&#322;o&#380;enie"
  ]
  node [
    id 361
    label "derywat_z&#322;o&#380;ony"
  ]
  node [
    id 362
    label "lodging"
  ]
  node [
    id 363
    label "removal"
  ]
  node [
    id 364
    label "pay"
  ]
  node [
    id 365
    label "composing"
  ]
  node [
    id 366
    label "zespolenie"
  ]
  node [
    id 367
    label "zjednoczenie"
  ]
  node [
    id 368
    label "element"
  ]
  node [
    id 369
    label "junction"
  ]
  node [
    id 370
    label "zgrzeina"
  ]
  node [
    id 371
    label "akt_p&#322;ciowy"
  ]
  node [
    id 372
    label "joining"
  ]
  node [
    id 373
    label "zrobienie"
  ]
  node [
    id 374
    label "rozwa&#380;enie"
  ]
  node [
    id 375
    label "udowodnienie"
  ]
  node [
    id 376
    label "przebadanie"
  ]
  node [
    id 377
    label "badanie"
  ]
  node [
    id 378
    label "opis"
  ]
  node [
    id 379
    label "analysis"
  ]
  node [
    id 380
    label "dissection"
  ]
  node [
    id 381
    label "metoda"
  ]
  node [
    id 382
    label "reakcja_chemiczna"
  ]
  node [
    id 383
    label "struktura"
  ]
  node [
    id 384
    label "prawo_karne"
  ]
  node [
    id 385
    label "dzie&#322;o"
  ]
  node [
    id 386
    label "figuracja"
  ]
  node [
    id 387
    label "chwyt"
  ]
  node [
    id 388
    label "okup"
  ]
  node [
    id 389
    label "muzykologia"
  ]
  node [
    id 390
    label "&#347;redniowiecze"
  ]
  node [
    id 391
    label "punkt"
  ]
  node [
    id 392
    label "publikacja"
  ]
  node [
    id 393
    label "wiedza"
  ]
  node [
    id 394
    label "doj&#347;cie"
  ]
  node [
    id 395
    label "obiega&#263;"
  ]
  node [
    id 396
    label "powzi&#281;cie"
  ]
  node [
    id 397
    label "dane"
  ]
  node [
    id 398
    label "obiegni&#281;cie"
  ]
  node [
    id 399
    label "sygna&#322;"
  ]
  node [
    id 400
    label "obieganie"
  ]
  node [
    id 401
    label "powzi&#261;&#263;"
  ]
  node [
    id 402
    label "obiec"
  ]
  node [
    id 403
    label "doj&#347;&#263;"
  ]
  node [
    id 404
    label "pr&#243;bowanie"
  ]
  node [
    id 405
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 406
    label "zademonstrowanie"
  ]
  node [
    id 407
    label "report"
  ]
  node [
    id 408
    label "obgadanie"
  ]
  node [
    id 409
    label "realizacja"
  ]
  node [
    id 410
    label "scena"
  ]
  node [
    id 411
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 412
    label "narration"
  ]
  node [
    id 413
    label "cyrk"
  ]
  node [
    id 414
    label "wytw&#243;r"
  ]
  node [
    id 415
    label "theatrical_performance"
  ]
  node [
    id 416
    label "opisanie"
  ]
  node [
    id 417
    label "malarstwo"
  ]
  node [
    id 418
    label "scenografia"
  ]
  node [
    id 419
    label "teatr"
  ]
  node [
    id 420
    label "ukazanie"
  ]
  node [
    id 421
    label "zapoznanie"
  ]
  node [
    id 422
    label "pokaz"
  ]
  node [
    id 423
    label "podanie"
  ]
  node [
    id 424
    label "spos&#243;b"
  ]
  node [
    id 425
    label "ods&#322;ona"
  ]
  node [
    id 426
    label "exhibit"
  ]
  node [
    id 427
    label "pokazanie"
  ]
  node [
    id 428
    label "wyst&#261;pienie"
  ]
  node [
    id 429
    label "przedstawi&#263;"
  ]
  node [
    id 430
    label "przedstawianie"
  ]
  node [
    id 431
    label "przedstawia&#263;"
  ]
  node [
    id 432
    label "ekscerpcja"
  ]
  node [
    id 433
    label "j&#281;zykowo"
  ]
  node [
    id 434
    label "wypowied&#378;"
  ]
  node [
    id 435
    label "redakcja"
  ]
  node [
    id 436
    label "pomini&#281;cie"
  ]
  node [
    id 437
    label "preparacja"
  ]
  node [
    id 438
    label "odmianka"
  ]
  node [
    id 439
    label "opu&#347;ci&#263;"
  ]
  node [
    id 440
    label "koniektura"
  ]
  node [
    id 441
    label "pisa&#263;"
  ]
  node [
    id 442
    label "obelga"
  ]
  node [
    id 443
    label "egzemplarz"
  ]
  node [
    id 444
    label "series"
  ]
  node [
    id 445
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 446
    label "uprawianie"
  ]
  node [
    id 447
    label "praca_rolnicza"
  ]
  node [
    id 448
    label "collection"
  ]
  node [
    id 449
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 450
    label "pakiet_klimatyczny"
  ]
  node [
    id 451
    label "poj&#281;cie"
  ]
  node [
    id 452
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 453
    label "sum"
  ]
  node [
    id 454
    label "gathering"
  ]
  node [
    id 455
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 456
    label "sformu&#322;owanie"
  ]
  node [
    id 457
    label "poinformowanie"
  ]
  node [
    id 458
    label "wording"
  ]
  node [
    id 459
    label "oznaczenie"
  ]
  node [
    id 460
    label "znak_j&#281;zykowy"
  ]
  node [
    id 461
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 462
    label "ozdobnik"
  ]
  node [
    id 463
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 464
    label "grupa_imienna"
  ]
  node [
    id 465
    label "jednostka_leksykalna"
  ]
  node [
    id 466
    label "term"
  ]
  node [
    id 467
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 468
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 469
    label "ujawnienie"
  ]
  node [
    id 470
    label "affirmation"
  ]
  node [
    id 471
    label "zapisanie"
  ]
  node [
    id 472
    label "rzucenie"
  ]
  node [
    id 473
    label "gem"
  ]
  node [
    id 474
    label "runda"
  ]
  node [
    id 475
    label "muzyka"
  ]
  node [
    id 476
    label "zestaw"
  ]
  node [
    id 477
    label "za&#322;amanie_si&#281;"
  ]
  node [
    id 478
    label "nastawi&#263;"
  ]
  node [
    id 479
    label "szyjka_udowa"
  ]
  node [
    id 480
    label "wy&#322;amanie"
  ]
  node [
    id 481
    label "discourtesy"
  ]
  node [
    id 482
    label "gips"
  ]
  node [
    id 483
    label "fracture"
  ]
  node [
    id 484
    label "wygranie"
  ]
  node [
    id 485
    label "dislocation"
  ]
  node [
    id 486
    label "nastawia&#263;"
  ]
  node [
    id 487
    label "nastawianie"
  ]
  node [
    id 488
    label "uraz"
  ]
  node [
    id 489
    label "nastawienie"
  ]
  node [
    id 490
    label "interruption"
  ]
  node [
    id 491
    label "przygn&#281;bienie"
  ]
  node [
    id 492
    label "transgresja"
  ]
  node [
    id 493
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 494
    label "podzielenie"
  ]
  node [
    id 495
    label "bilans"
  ]
  node [
    id 496
    label "b&#322;ystka"
  ]
  node [
    id 497
    label "ksi&#281;ga"
  ]
  node [
    id 498
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 499
    label "spis"
  ]
  node [
    id 500
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 501
    label "discrimination"
  ]
  node [
    id 502
    label "diverseness"
  ]
  node [
    id 503
    label "eklektyk"
  ]
  node [
    id 504
    label "rozproszenie_si&#281;"
  ]
  node [
    id 505
    label "differentiation"
  ]
  node [
    id 506
    label "bogactwo"
  ]
  node [
    id 507
    label "cecha"
  ]
  node [
    id 508
    label "multikulturalizm"
  ]
  node [
    id 509
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 510
    label "rozdzielenie"
  ]
  node [
    id 511
    label "nadanie"
  ]
  node [
    id 512
    label "zniszczenie"
  ]
  node [
    id 513
    label "ubytek"
  ]
  node [
    id 514
    label "szwank"
  ]
  node [
    id 515
    label "niepowodzenie"
  ]
  node [
    id 516
    label "r&#243;&#380;nica"
  ]
  node [
    id 517
    label "kwota"
  ]
  node [
    id 518
    label "ilo&#347;&#263;"
  ]
  node [
    id 519
    label "saldo"
  ]
  node [
    id 520
    label "failure"
  ]
  node [
    id 521
    label "brak"
  ]
  node [
    id 522
    label "niedob&#243;r"
  ]
  node [
    id 523
    label "po&#322;o&#380;enie"
  ]
  node [
    id 524
    label "debit"
  ]
  node [
    id 525
    label "druk"
  ]
  node [
    id 526
    label "szata_graficzna"
  ]
  node [
    id 527
    label "wydawa&#263;"
  ]
  node [
    id 528
    label "szermierka"
  ]
  node [
    id 529
    label "wyda&#263;"
  ]
  node [
    id 530
    label "status"
  ]
  node [
    id 531
    label "miejsce"
  ]
  node [
    id 532
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 533
    label "adres"
  ]
  node [
    id 534
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 535
    label "sytuacja"
  ]
  node [
    id 536
    label "rz&#261;d"
  ]
  node [
    id 537
    label "redaktor"
  ]
  node [
    id 538
    label "awansowa&#263;"
  ]
  node [
    id 539
    label "wojsko"
  ]
  node [
    id 540
    label "bearing"
  ]
  node [
    id 541
    label "znaczenie"
  ]
  node [
    id 542
    label "awans"
  ]
  node [
    id 543
    label "awansowanie"
  ]
  node [
    id 544
    label "poster"
  ]
  node [
    id 545
    label "le&#380;e&#263;"
  ]
  node [
    id 546
    label "entliczek"
  ]
  node [
    id 547
    label "zabawa"
  ]
  node [
    id 548
    label "wiersz"
  ]
  node [
    id 549
    label "pentliczek"
  ]
  node [
    id 550
    label "dobroczynny"
  ]
  node [
    id 551
    label "czw&#243;rka"
  ]
  node [
    id 552
    label "spokojny"
  ]
  node [
    id 553
    label "mi&#322;y"
  ]
  node [
    id 554
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 555
    label "powitanie"
  ]
  node [
    id 556
    label "ca&#322;y"
  ]
  node [
    id 557
    label "zwrot"
  ]
  node [
    id 558
    label "pomy&#347;lny"
  ]
  node [
    id 559
    label "drogi"
  ]
  node [
    id 560
    label "odpowiedni"
  ]
  node [
    id 561
    label "pos&#322;uszny"
  ]
  node [
    id 562
    label "moralnie"
  ]
  node [
    id 563
    label "warto&#347;ciowy"
  ]
  node [
    id 564
    label "etycznie"
  ]
  node [
    id 565
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 566
    label "nale&#380;ny"
  ]
  node [
    id 567
    label "uprawniony"
  ]
  node [
    id 568
    label "zasadniczy"
  ]
  node [
    id 569
    label "stosownie"
  ]
  node [
    id 570
    label "taki"
  ]
  node [
    id 571
    label "prawdziwy"
  ]
  node [
    id 572
    label "ten"
  ]
  node [
    id 573
    label "pozytywnie"
  ]
  node [
    id 574
    label "fajny"
  ]
  node [
    id 575
    label "dodatnio"
  ]
  node [
    id 576
    label "przyjemny"
  ]
  node [
    id 577
    label "po&#380;&#261;dany"
  ]
  node [
    id 578
    label "niepowa&#380;ny"
  ]
  node [
    id 579
    label "o&#347;mieszanie"
  ]
  node [
    id 580
    label "&#347;miesznie"
  ]
  node [
    id 581
    label "bawny"
  ]
  node [
    id 582
    label "o&#347;mieszenie"
  ]
  node [
    id 583
    label "nieadekwatny"
  ]
  node [
    id 584
    label "zale&#380;ny"
  ]
  node [
    id 585
    label "uleg&#322;y"
  ]
  node [
    id 586
    label "pos&#322;usznie"
  ]
  node [
    id 587
    label "grzecznie"
  ]
  node [
    id 588
    label "niewinny"
  ]
  node [
    id 589
    label "konserwatywny"
  ]
  node [
    id 590
    label "nijaki"
  ]
  node [
    id 591
    label "wolny"
  ]
  node [
    id 592
    label "uspokajanie_si&#281;"
  ]
  node [
    id 593
    label "bezproblemowy"
  ]
  node [
    id 594
    label "spokojnie"
  ]
  node [
    id 595
    label "uspokojenie_si&#281;"
  ]
  node [
    id 596
    label "cicho"
  ]
  node [
    id 597
    label "uspokojenie"
  ]
  node [
    id 598
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 599
    label "nietrudny"
  ]
  node [
    id 600
    label "uspokajanie"
  ]
  node [
    id 601
    label "korzystnie"
  ]
  node [
    id 602
    label "drogo"
  ]
  node [
    id 603
    label "bliski"
  ]
  node [
    id 604
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 605
    label "przyjaciel"
  ]
  node [
    id 606
    label "jedyny"
  ]
  node [
    id 607
    label "du&#380;y"
  ]
  node [
    id 608
    label "zdr&#243;w"
  ]
  node [
    id 609
    label "calu&#347;ko"
  ]
  node [
    id 610
    label "kompletny"
  ]
  node [
    id 611
    label "&#380;ywy"
  ]
  node [
    id 612
    label "pe&#322;ny"
  ]
  node [
    id 613
    label "ca&#322;o"
  ]
  node [
    id 614
    label "poskutkowanie"
  ]
  node [
    id 615
    label "sprawny"
  ]
  node [
    id 616
    label "skutecznie"
  ]
  node [
    id 617
    label "skutkowanie"
  ]
  node [
    id 618
    label "pomy&#347;lnie"
  ]
  node [
    id 619
    label "toto-lotek"
  ]
  node [
    id 620
    label "trafienie"
  ]
  node [
    id 621
    label "arkusz_drukarski"
  ]
  node [
    id 622
    label "&#322;&#243;dka"
  ]
  node [
    id 623
    label "four"
  ]
  node [
    id 624
    label "&#263;wiartka"
  ]
  node [
    id 625
    label "hotel"
  ]
  node [
    id 626
    label "cyfra"
  ]
  node [
    id 627
    label "stopie&#324;"
  ]
  node [
    id 628
    label "minialbum"
  ]
  node [
    id 629
    label "osada"
  ]
  node [
    id 630
    label "p&#322;yta_winylowa"
  ]
  node [
    id 631
    label "blotka"
  ]
  node [
    id 632
    label "zaprz&#281;g"
  ]
  node [
    id 633
    label "przedtrzonowiec"
  ]
  node [
    id 634
    label "turn"
  ]
  node [
    id 635
    label "turning"
  ]
  node [
    id 636
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 637
    label "skr&#281;t"
  ]
  node [
    id 638
    label "obr&#243;t"
  ]
  node [
    id 639
    label "fraza_czasownikowa"
  ]
  node [
    id 640
    label "zmiana"
  ]
  node [
    id 641
    label "welcome"
  ]
  node [
    id 642
    label "spotkanie"
  ]
  node [
    id 643
    label "pozdrowienie"
  ]
  node [
    id 644
    label "zwyczaj"
  ]
  node [
    id 645
    label "greeting"
  ]
  node [
    id 646
    label "zdarzony"
  ]
  node [
    id 647
    label "odpowiednio"
  ]
  node [
    id 648
    label "odpowiadanie"
  ]
  node [
    id 649
    label "specjalny"
  ]
  node [
    id 650
    label "kochanek"
  ]
  node [
    id 651
    label "sk&#322;onny"
  ]
  node [
    id 652
    label "wybranek"
  ]
  node [
    id 653
    label "umi&#322;owany"
  ]
  node [
    id 654
    label "przyjemnie"
  ]
  node [
    id 655
    label "mi&#322;o"
  ]
  node [
    id 656
    label "kochanie"
  ]
  node [
    id 657
    label "dyplomata"
  ]
  node [
    id 658
    label "dobroczynnie"
  ]
  node [
    id 659
    label "lepiej"
  ]
  node [
    id 660
    label "wiele"
  ]
  node [
    id 661
    label "spo&#322;eczny"
  ]
  node [
    id 662
    label "szko&#322;a"
  ]
  node [
    id 663
    label "fraza"
  ]
  node [
    id 664
    label "stanowisko"
  ]
  node [
    id 665
    label "wypowiedzenie"
  ]
  node [
    id 666
    label "prison_term"
  ]
  node [
    id 667
    label "system"
  ]
  node [
    id 668
    label "okres"
  ]
  node [
    id 669
    label "zaliczenie"
  ]
  node [
    id 670
    label "antylogizm"
  ]
  node [
    id 671
    label "konektyw"
  ]
  node [
    id 672
    label "attitude"
  ]
  node [
    id 673
    label "powierzenie"
  ]
  node [
    id 674
    label "adjudication"
  ]
  node [
    id 675
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 676
    label "pass"
  ]
  node [
    id 677
    label "spe&#322;nienie"
  ]
  node [
    id 678
    label "wliczenie"
  ]
  node [
    id 679
    label "zaliczanie"
  ]
  node [
    id 680
    label "zaklasyfikowanie_si&#281;"
  ]
  node [
    id 681
    label "crack"
  ]
  node [
    id 682
    label "zadanie"
  ]
  node [
    id 683
    label "odb&#281;bnienie"
  ]
  node [
    id 684
    label "ocenienie"
  ]
  node [
    id 685
    label "number"
  ]
  node [
    id 686
    label "policzenie"
  ]
  node [
    id 687
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 688
    label "przeklasyfikowanie"
  ]
  node [
    id 689
    label "zaliczanie_si&#281;"
  ]
  node [
    id 690
    label "wzi&#281;cie"
  ]
  node [
    id 691
    label "dor&#281;czenie"
  ]
  node [
    id 692
    label "wys&#322;anie"
  ]
  node [
    id 693
    label "delivery"
  ]
  node [
    id 694
    label "transfer"
  ]
  node [
    id 695
    label "wp&#322;acenie"
  ]
  node [
    id 696
    label "constraint"
  ]
  node [
    id 697
    label "zadanie_gwa&#322;tu"
  ]
  node [
    id 698
    label "oddzia&#322;anie"
  ]
  node [
    id 699
    label "force"
  ]
  node [
    id 700
    label "pop&#281;dzenie_"
  ]
  node [
    id 701
    label "konwersja"
  ]
  node [
    id 702
    label "notice"
  ]
  node [
    id 703
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 704
    label "przepowiedzenie"
  ]
  node [
    id 705
    label "rozwi&#261;zanie"
  ]
  node [
    id 706
    label "generowa&#263;"
  ]
  node [
    id 707
    label "wydanie"
  ]
  node [
    id 708
    label "message"
  ]
  node [
    id 709
    label "generowanie"
  ]
  node [
    id 710
    label "wydobycie"
  ]
  node [
    id 711
    label "zwerbalizowanie"
  ]
  node [
    id 712
    label "szyk"
  ]
  node [
    id 713
    label "notification"
  ]
  node [
    id 714
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 715
    label "denunciation"
  ]
  node [
    id 716
    label "pogl&#261;d"
  ]
  node [
    id 717
    label "stawia&#263;"
  ]
  node [
    id 718
    label "wakowa&#263;"
  ]
  node [
    id 719
    label "powierzanie"
  ]
  node [
    id 720
    label "postawi&#263;"
  ]
  node [
    id 721
    label "praca"
  ]
  node [
    id 722
    label "wyznanie"
  ]
  node [
    id 723
    label "zlecenie"
  ]
  node [
    id 724
    label "ufanie"
  ]
  node [
    id 725
    label "commitment"
  ]
  node [
    id 726
    label "perpetration"
  ]
  node [
    id 727
    label "oddanie"
  ]
  node [
    id 728
    label "do&#347;wiadczenie"
  ]
  node [
    id 729
    label "teren_szko&#322;y"
  ]
  node [
    id 730
    label "Mickiewicz"
  ]
  node [
    id 731
    label "kwalifikacje"
  ]
  node [
    id 732
    label "podr&#281;cznik"
  ]
  node [
    id 733
    label "absolwent"
  ]
  node [
    id 734
    label "praktyka"
  ]
  node [
    id 735
    label "school"
  ]
  node [
    id 736
    label "zda&#263;"
  ]
  node [
    id 737
    label "gabinet"
  ]
  node [
    id 738
    label "urszulanki"
  ]
  node [
    id 739
    label "sztuba"
  ]
  node [
    id 740
    label "&#322;awa_szkolna"
  ]
  node [
    id 741
    label "nauka"
  ]
  node [
    id 742
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 743
    label "przepisa&#263;"
  ]
  node [
    id 744
    label "grupa"
  ]
  node [
    id 745
    label "form"
  ]
  node [
    id 746
    label "klasa"
  ]
  node [
    id 747
    label "lekcja"
  ]
  node [
    id 748
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 749
    label "skolaryzacja"
  ]
  node [
    id 750
    label "stopek"
  ]
  node [
    id 751
    label "sekretariat"
  ]
  node [
    id 752
    label "ideologia"
  ]
  node [
    id 753
    label "lesson"
  ]
  node [
    id 754
    label "instytucja"
  ]
  node [
    id 755
    label "niepokalanki"
  ]
  node [
    id 756
    label "siedziba"
  ]
  node [
    id 757
    label "szkolenie"
  ]
  node [
    id 758
    label "kara"
  ]
  node [
    id 759
    label "tablica"
  ]
  node [
    id 760
    label "funktor"
  ]
  node [
    id 761
    label "j&#261;dro"
  ]
  node [
    id 762
    label "systemik"
  ]
  node [
    id 763
    label "rozprz&#261;c"
  ]
  node [
    id 764
    label "oprogramowanie"
  ]
  node [
    id 765
    label "systemat"
  ]
  node [
    id 766
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 767
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 768
    label "model"
  ]
  node [
    id 769
    label "usenet"
  ]
  node [
    id 770
    label "porz&#261;dek"
  ]
  node [
    id 771
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 772
    label "przyn&#281;ta"
  ]
  node [
    id 773
    label "p&#322;&#243;d"
  ]
  node [
    id 774
    label "net"
  ]
  node [
    id 775
    label "w&#281;dkarstwo"
  ]
  node [
    id 776
    label "eratem"
  ]
  node [
    id 777
    label "oddzia&#322;"
  ]
  node [
    id 778
    label "doktryna"
  ]
  node [
    id 779
    label "pulpit"
  ]
  node [
    id 780
    label "konstelacja"
  ]
  node [
    id 781
    label "jednostka_geologiczna"
  ]
  node [
    id 782
    label "o&#347;"
  ]
  node [
    id 783
    label "podsystem"
  ]
  node [
    id 784
    label "ryba"
  ]
  node [
    id 785
    label "Leopard"
  ]
  node [
    id 786
    label "Android"
  ]
  node [
    id 787
    label "zachowanie"
  ]
  node [
    id 788
    label "cybernetyk"
  ]
  node [
    id 789
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 790
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 791
    label "method"
  ]
  node [
    id 792
    label "sk&#322;ad"
  ]
  node [
    id 793
    label "podstawa"
  ]
  node [
    id 794
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 795
    label "relacja_logiczna"
  ]
  node [
    id 796
    label "okres_amazo&#324;ski"
  ]
  node [
    id 797
    label "stater"
  ]
  node [
    id 798
    label "postglacja&#322;"
  ]
  node [
    id 799
    label "sylur"
  ]
  node [
    id 800
    label "kreda"
  ]
  node [
    id 801
    label "ordowik"
  ]
  node [
    id 802
    label "okres_hesperyjski"
  ]
  node [
    id 803
    label "paleogen"
  ]
  node [
    id 804
    label "okres_halsztacki"
  ]
  node [
    id 805
    label "riak"
  ]
  node [
    id 806
    label "czwartorz&#281;d"
  ]
  node [
    id 807
    label "podokres"
  ]
  node [
    id 808
    label "trzeciorz&#281;d"
  ]
  node [
    id 809
    label "kalim"
  ]
  node [
    id 810
    label "fala"
  ]
  node [
    id 811
    label "perm"
  ]
  node [
    id 812
    label "retoryka"
  ]
  node [
    id 813
    label "prekambr"
  ]
  node [
    id 814
    label "faza"
  ]
  node [
    id 815
    label "neogen"
  ]
  node [
    id 816
    label "pulsacja"
  ]
  node [
    id 817
    label "kambr"
  ]
  node [
    id 818
    label "kriogen"
  ]
  node [
    id 819
    label "ton"
  ]
  node [
    id 820
    label "orosir"
  ]
  node [
    id 821
    label "poprzednik"
  ]
  node [
    id 822
    label "spell"
  ]
  node [
    id 823
    label "interstadia&#322;"
  ]
  node [
    id 824
    label "ektas"
  ]
  node [
    id 825
    label "sider"
  ]
  node [
    id 826
    label "rok_akademicki"
  ]
  node [
    id 827
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 828
    label "cykl"
  ]
  node [
    id 829
    label "pierwszorz&#281;d"
  ]
  node [
    id 830
    label "okres_noachijski"
  ]
  node [
    id 831
    label "ediakar"
  ]
  node [
    id 832
    label "nast&#281;pnik"
  ]
  node [
    id 833
    label "condition"
  ]
  node [
    id 834
    label "jura"
  ]
  node [
    id 835
    label "glacja&#322;"
  ]
  node [
    id 836
    label "sten"
  ]
  node [
    id 837
    label "era"
  ]
  node [
    id 838
    label "trias"
  ]
  node [
    id 839
    label "p&#243;&#322;okres"
  ]
  node [
    id 840
    label "rok_szkolny"
  ]
  node [
    id 841
    label "dewon"
  ]
  node [
    id 842
    label "karbon"
  ]
  node [
    id 843
    label "izochronizm"
  ]
  node [
    id 844
    label "preglacja&#322;"
  ]
  node [
    id 845
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 846
    label "drugorz&#281;d"
  ]
  node [
    id 847
    label "semester"
  ]
  node [
    id 848
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 849
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 850
    label "motyw"
  ]
  node [
    id 851
    label "kszta&#322;ciciel"
  ]
  node [
    id 852
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 853
    label "tworzyciel"
  ]
  node [
    id 854
    label "wykonawca"
  ]
  node [
    id 855
    label "pomys&#322;odawca"
  ]
  node [
    id 856
    label "&#347;w"
  ]
  node [
    id 857
    label "inicjator"
  ]
  node [
    id 858
    label "podmiot_gospodarczy"
  ]
  node [
    id 859
    label "artysta"
  ]
  node [
    id 860
    label "muzyk"
  ]
  node [
    id 861
    label "nauczyciel"
  ]
  node [
    id 862
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 863
    label "byt"
  ]
  node [
    id 864
    label "osobowo&#347;&#263;"
  ]
  node [
    id 865
    label "organizacja"
  ]
  node [
    id 866
    label "prawo"
  ]
  node [
    id 867
    label "nauka_prawa"
  ]
  node [
    id 868
    label "utw&#243;r"
  ]
  node [
    id 869
    label "charakterystyka"
  ]
  node [
    id 870
    label "zaistnie&#263;"
  ]
  node [
    id 871
    label "Osjan"
  ]
  node [
    id 872
    label "kto&#347;"
  ]
  node [
    id 873
    label "wygl&#261;d"
  ]
  node [
    id 874
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 875
    label "trim"
  ]
  node [
    id 876
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 877
    label "Aspazja"
  ]
  node [
    id 878
    label "punkt_widzenia"
  ]
  node [
    id 879
    label "kompleksja"
  ]
  node [
    id 880
    label "wytrzyma&#263;"
  ]
  node [
    id 881
    label "budowa"
  ]
  node [
    id 882
    label "formacja"
  ]
  node [
    id 883
    label "pozosta&#263;"
  ]
  node [
    id 884
    label "point"
  ]
  node [
    id 885
    label "go&#347;&#263;"
  ]
  node [
    id 886
    label "kszta&#322;t"
  ]
  node [
    id 887
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 888
    label "armia"
  ]
  node [
    id 889
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 890
    label "poprowadzi&#263;"
  ]
  node [
    id 891
    label "cord"
  ]
  node [
    id 892
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 893
    label "trasa"
  ]
  node [
    id 894
    label "po&#322;&#261;czenie"
  ]
  node [
    id 895
    label "tract"
  ]
  node [
    id 896
    label "materia&#322;_zecerski"
  ]
  node [
    id 897
    label "przeorientowywanie"
  ]
  node [
    id 898
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 899
    label "curve"
  ]
  node [
    id 900
    label "figura_geometryczna"
  ]
  node [
    id 901
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 902
    label "jard"
  ]
  node [
    id 903
    label "szczep"
  ]
  node [
    id 904
    label "phreaker"
  ]
  node [
    id 905
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 906
    label "grupa_organizm&#243;w"
  ]
  node [
    id 907
    label "prowadzi&#263;"
  ]
  node [
    id 908
    label "przeorientowywa&#263;"
  ]
  node [
    id 909
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 910
    label "access"
  ]
  node [
    id 911
    label "przeorientowanie"
  ]
  node [
    id 912
    label "przeorientowa&#263;"
  ]
  node [
    id 913
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 914
    label "billing"
  ]
  node [
    id 915
    label "granica"
  ]
  node [
    id 916
    label "szpaler"
  ]
  node [
    id 917
    label "sztrych"
  ]
  node [
    id 918
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 919
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 920
    label "drzewo_genealogiczne"
  ]
  node [
    id 921
    label "transporter"
  ]
  node [
    id 922
    label "line"
  ]
  node [
    id 923
    label "przew&#243;d"
  ]
  node [
    id 924
    label "granice"
  ]
  node [
    id 925
    label "kontakt"
  ]
  node [
    id 926
    label "przewo&#378;nik"
  ]
  node [
    id 927
    label "przystanek"
  ]
  node [
    id 928
    label "linijka"
  ]
  node [
    id 929
    label "uporz&#261;dkowanie"
  ]
  node [
    id 930
    label "coalescence"
  ]
  node [
    id 931
    label "Ural"
  ]
  node [
    id 932
    label "prowadzenie"
  ]
  node [
    id 933
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 934
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 935
    label "koniec"
  ]
  node [
    id 936
    label "podkatalog"
  ]
  node [
    id 937
    label "nadpisa&#263;"
  ]
  node [
    id 938
    label "nadpisanie"
  ]
  node [
    id 939
    label "bundle"
  ]
  node [
    id 940
    label "folder"
  ]
  node [
    id 941
    label "nadpisywanie"
  ]
  node [
    id 942
    label "paczka"
  ]
  node [
    id 943
    label "nadpisywa&#263;"
  ]
  node [
    id 944
    label "dokument"
  ]
  node [
    id 945
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 946
    label "Rzym_Zachodni"
  ]
  node [
    id 947
    label "whole"
  ]
  node [
    id 948
    label "Rzym_Wschodni"
  ]
  node [
    id 949
    label "rozmiar"
  ]
  node [
    id 950
    label "obszar"
  ]
  node [
    id 951
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 952
    label "zwierciad&#322;o"
  ]
  node [
    id 953
    label "capacity"
  ]
  node [
    id 954
    label "plane"
  ]
  node [
    id 955
    label "temat"
  ]
  node [
    id 956
    label "jednostka_systematyczna"
  ]
  node [
    id 957
    label "poznanie"
  ]
  node [
    id 958
    label "stan"
  ]
  node [
    id 959
    label "blaszka"
  ]
  node [
    id 960
    label "kantyzm"
  ]
  node [
    id 961
    label "zdolno&#347;&#263;"
  ]
  node [
    id 962
    label "do&#322;ek"
  ]
  node [
    id 963
    label "zawarto&#347;&#263;"
  ]
  node [
    id 964
    label "gwiazda"
  ]
  node [
    id 965
    label "formality"
  ]
  node [
    id 966
    label "mode"
  ]
  node [
    id 967
    label "morfem"
  ]
  node [
    id 968
    label "rdze&#324;"
  ]
  node [
    id 969
    label "kielich"
  ]
  node [
    id 970
    label "ornamentyka"
  ]
  node [
    id 971
    label "pasmo"
  ]
  node [
    id 972
    label "g&#322;owa"
  ]
  node [
    id 973
    label "naczynie"
  ]
  node [
    id 974
    label "p&#322;at"
  ]
  node [
    id 975
    label "maszyna_drukarska"
  ]
  node [
    id 976
    label "style"
  ]
  node [
    id 977
    label "linearno&#347;&#263;"
  ]
  node [
    id 978
    label "spirala"
  ]
  node [
    id 979
    label "dyspozycja"
  ]
  node [
    id 980
    label "odmiana"
  ]
  node [
    id 981
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 982
    label "wz&#243;r"
  ]
  node [
    id 983
    label "October"
  ]
  node [
    id 984
    label "creation"
  ]
  node [
    id 985
    label "p&#281;tla"
  ]
  node [
    id 986
    label "arystotelizm"
  ]
  node [
    id 987
    label "szablon"
  ]
  node [
    id 988
    label "miniatura"
  ]
  node [
    id 989
    label "zesp&#243;&#322;"
  ]
  node [
    id 990
    label "podejrzany"
  ]
  node [
    id 991
    label "s&#261;downictwo"
  ]
  node [
    id 992
    label "biuro"
  ]
  node [
    id 993
    label "court"
  ]
  node [
    id 994
    label "forum"
  ]
  node [
    id 995
    label "bronienie"
  ]
  node [
    id 996
    label "urz&#261;d"
  ]
  node [
    id 997
    label "wydarzenie"
  ]
  node [
    id 998
    label "oskar&#380;yciel"
  ]
  node [
    id 999
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1000
    label "skazany"
  ]
  node [
    id 1001
    label "post&#281;powanie"
  ]
  node [
    id 1002
    label "broni&#263;"
  ]
  node [
    id 1003
    label "my&#347;l"
  ]
  node [
    id 1004
    label "pods&#261;dny"
  ]
  node [
    id 1005
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1006
    label "obrona"
  ]
  node [
    id 1007
    label "&#347;wiadek"
  ]
  node [
    id 1008
    label "procesowicz"
  ]
  node [
    id 1009
    label "pochwytanie"
  ]
  node [
    id 1010
    label "wzbudzenie"
  ]
  node [
    id 1011
    label "withdrawal"
  ]
  node [
    id 1012
    label "capture"
  ]
  node [
    id 1013
    label "podniesienie"
  ]
  node [
    id 1014
    label "film"
  ]
  node [
    id 1015
    label "prezentacja"
  ]
  node [
    id 1016
    label "zamkni&#281;cie"
  ]
  node [
    id 1017
    label "zabranie"
  ]
  node [
    id 1018
    label "zaaresztowanie"
  ]
  node [
    id 1019
    label "eastern_hemisphere"
  ]
  node [
    id 1020
    label "kierunek"
  ]
  node [
    id 1021
    label "kierowa&#263;"
  ]
  node [
    id 1022
    label "inform"
  ]
  node [
    id 1023
    label "marshal"
  ]
  node [
    id 1024
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1025
    label "wyznacza&#263;"
  ]
  node [
    id 1026
    label "pomaga&#263;"
  ]
  node [
    id 1027
    label "tu&#322;&#243;w"
  ]
  node [
    id 1028
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 1029
    label "wielok&#261;t"
  ]
  node [
    id 1030
    label "odcinek"
  ]
  node [
    id 1031
    label "strzelba"
  ]
  node [
    id 1032
    label "lufa"
  ]
  node [
    id 1033
    label "&#347;ciana"
  ]
  node [
    id 1034
    label "wyznaczenie"
  ]
  node [
    id 1035
    label "przyczynienie_si&#281;"
  ]
  node [
    id 1036
    label "zwr&#243;cenie"
  ]
  node [
    id 1037
    label "zrozumienie"
  ]
  node [
    id 1038
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1039
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 1040
    label "zorientowanie_si&#281;"
  ]
  node [
    id 1041
    label "pogubienie_si&#281;"
  ]
  node [
    id 1042
    label "orientation"
  ]
  node [
    id 1043
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 1044
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 1045
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1046
    label "gubienie_si&#281;"
  ]
  node [
    id 1047
    label "wrench"
  ]
  node [
    id 1048
    label "nawini&#281;cie"
  ]
  node [
    id 1049
    label "os&#322;abienie"
  ]
  node [
    id 1050
    label "uszkodzenie"
  ]
  node [
    id 1051
    label "odbicie"
  ]
  node [
    id 1052
    label "poskr&#281;canie"
  ]
  node [
    id 1053
    label "odchylenie_si&#281;"
  ]
  node [
    id 1054
    label "splecenie"
  ]
  node [
    id 1055
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 1056
    label "sple&#347;&#263;"
  ]
  node [
    id 1057
    label "os&#322;abi&#263;"
  ]
  node [
    id 1058
    label "nawin&#261;&#263;"
  ]
  node [
    id 1059
    label "scali&#263;"
  ]
  node [
    id 1060
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 1061
    label "twist"
  ]
  node [
    id 1062
    label "splay"
  ]
  node [
    id 1063
    label "uszkodzi&#263;"
  ]
  node [
    id 1064
    label "break"
  ]
  node [
    id 1065
    label "flex"
  ]
  node [
    id 1066
    label "zaty&#322;"
  ]
  node [
    id 1067
    label "pupa"
  ]
  node [
    id 1068
    label "cia&#322;o"
  ]
  node [
    id 1069
    label "os&#322;abia&#263;"
  ]
  node [
    id 1070
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 1071
    label "splata&#263;"
  ]
  node [
    id 1072
    label "throw"
  ]
  node [
    id 1073
    label "screw"
  ]
  node [
    id 1074
    label "scala&#263;"
  ]
  node [
    id 1075
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 1076
    label "przedmiot"
  ]
  node [
    id 1077
    label "przelezienie"
  ]
  node [
    id 1078
    label "&#347;piew"
  ]
  node [
    id 1079
    label "Synaj"
  ]
  node [
    id 1080
    label "Kreml"
  ]
  node [
    id 1081
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1082
    label "wysoki"
  ]
  node [
    id 1083
    label "wzniesienie"
  ]
  node [
    id 1084
    label "pi&#281;tro"
  ]
  node [
    id 1085
    label "Ropa"
  ]
  node [
    id 1086
    label "kupa"
  ]
  node [
    id 1087
    label "przele&#378;&#263;"
  ]
  node [
    id 1088
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 1089
    label "karczek"
  ]
  node [
    id 1090
    label "rami&#261;czko"
  ]
  node [
    id 1091
    label "Jaworze"
  ]
  node [
    id 1092
    label "orient"
  ]
  node [
    id 1093
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 1094
    label "aim"
  ]
  node [
    id 1095
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 1096
    label "wyznaczy&#263;"
  ]
  node [
    id 1097
    label "pomaganie"
  ]
  node [
    id 1098
    label "przyczynianie_si&#281;"
  ]
  node [
    id 1099
    label "zwracanie"
  ]
  node [
    id 1100
    label "rozeznawanie"
  ]
  node [
    id 1101
    label "oznaczanie"
  ]
  node [
    id 1102
    label "odchylanie_si&#281;"
  ]
  node [
    id 1103
    label "kszta&#322;towanie"
  ]
  node [
    id 1104
    label "os&#322;abianie"
  ]
  node [
    id 1105
    label "uprz&#281;dzenie"
  ]
  node [
    id 1106
    label "scalanie"
  ]
  node [
    id 1107
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 1108
    label "snucie"
  ]
  node [
    id 1109
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 1110
    label "tortuosity"
  ]
  node [
    id 1111
    label "odbijanie"
  ]
  node [
    id 1112
    label "contortion"
  ]
  node [
    id 1113
    label "splatanie"
  ]
  node [
    id 1114
    label "figura"
  ]
  node [
    id 1115
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 1116
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 1117
    label "uwierzytelnienie"
  ]
  node [
    id 1118
    label "circumference"
  ]
  node [
    id 1119
    label "cyrkumferencja"
  ]
  node [
    id 1120
    label "provider"
  ]
  node [
    id 1121
    label "hipertekst"
  ]
  node [
    id 1122
    label "cyberprzestrze&#324;"
  ]
  node [
    id 1123
    label "mem"
  ]
  node [
    id 1124
    label "gra_sieciowa"
  ]
  node [
    id 1125
    label "grooming"
  ]
  node [
    id 1126
    label "media"
  ]
  node [
    id 1127
    label "biznes_elektroniczny"
  ]
  node [
    id 1128
    label "sie&#263;_komputerowa"
  ]
  node [
    id 1129
    label "punkt_dost&#281;pu"
  ]
  node [
    id 1130
    label "us&#322;uga_internetowa"
  ]
  node [
    id 1131
    label "netbook"
  ]
  node [
    id 1132
    label "e-hazard"
  ]
  node [
    id 1133
    label "podcast"
  ]
  node [
    id 1134
    label "co&#347;"
  ]
  node [
    id 1135
    label "budynek"
  ]
  node [
    id 1136
    label "thing"
  ]
  node [
    id 1137
    label "rzecz"
  ]
  node [
    id 1138
    label "faul"
  ]
  node [
    id 1139
    label "wk&#322;ad"
  ]
  node [
    id 1140
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 1141
    label "s&#281;dzia"
  ]
  node [
    id 1142
    label "bon"
  ]
  node [
    id 1143
    label "ticket"
  ]
  node [
    id 1144
    label "arkusz"
  ]
  node [
    id 1145
    label "kartonik"
  ]
  node [
    id 1146
    label "pagination"
  ]
  node [
    id 1147
    label "numer"
  ]
  node [
    id 1148
    label "darmowo"
  ]
  node [
    id 1149
    label "zwy&#380;kowanie"
  ]
  node [
    id 1150
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1151
    label "zaj&#281;cia"
  ]
  node [
    id 1152
    label "rok"
  ]
  node [
    id 1153
    label "przejazd"
  ]
  node [
    id 1154
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 1155
    label "manner"
  ]
  node [
    id 1156
    label "course"
  ]
  node [
    id 1157
    label "passage"
  ]
  node [
    id 1158
    label "zni&#380;kowanie"
  ]
  node [
    id 1159
    label "seria"
  ]
  node [
    id 1160
    label "stawka"
  ]
  node [
    id 1161
    label "way"
  ]
  node [
    id 1162
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1163
    label "deprecjacja"
  ]
  node [
    id 1164
    label "cedu&#322;a"
  ]
  node [
    id 1165
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 1166
    label "drive"
  ]
  node [
    id 1167
    label "Lira"
  ]
  node [
    id 1168
    label "podr&#243;&#380;"
  ]
  node [
    id 1169
    label "migracja"
  ]
  node [
    id 1170
    label "hike"
  ]
  node [
    id 1171
    label "pensum"
  ]
  node [
    id 1172
    label "enroll"
  ]
  node [
    id 1173
    label "przebieg"
  ]
  node [
    id 1174
    label "jednostka"
  ]
  node [
    id 1175
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 1176
    label "komplet"
  ]
  node [
    id 1177
    label "sekwencja"
  ]
  node [
    id 1178
    label "partia"
  ]
  node [
    id 1179
    label "produkcja"
  ]
  node [
    id 1180
    label "narz&#281;dzie"
  ]
  node [
    id 1181
    label "nature"
  ]
  node [
    id 1182
    label "intencja"
  ]
  node [
    id 1183
    label "leaning"
  ]
  node [
    id 1184
    label "studia"
  ]
  node [
    id 1185
    label "jazda"
  ]
  node [
    id 1186
    label "droga"
  ]
  node [
    id 1187
    label "infrastruktura"
  ]
  node [
    id 1188
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 1189
    label "w&#281;ze&#322;"
  ]
  node [
    id 1190
    label "marszrutyzacja"
  ]
  node [
    id 1191
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1192
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 1193
    label "podbieg"
  ]
  node [
    id 1194
    label "cena"
  ]
  node [
    id 1195
    label "pace"
  ]
  node [
    id 1196
    label "odm&#322;adzanie"
  ]
  node [
    id 1197
    label "liga"
  ]
  node [
    id 1198
    label "asymilowanie"
  ]
  node [
    id 1199
    label "gromada"
  ]
  node [
    id 1200
    label "asymilowa&#263;"
  ]
  node [
    id 1201
    label "Entuzjastki"
  ]
  node [
    id 1202
    label "Terranie"
  ]
  node [
    id 1203
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1204
    label "category"
  ]
  node [
    id 1205
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1206
    label "cz&#261;steczka"
  ]
  node [
    id 1207
    label "type"
  ]
  node [
    id 1208
    label "specgrupa"
  ]
  node [
    id 1209
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1210
    label "&#346;wietliki"
  ]
  node [
    id 1211
    label "odm&#322;odzenie"
  ]
  node [
    id 1212
    label "Eurogrupa"
  ]
  node [
    id 1213
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1214
    label "formacja_geologiczna"
  ]
  node [
    id 1215
    label "harcerze_starsi"
  ]
  node [
    id 1216
    label "miasteczko_rowerowe"
  ]
  node [
    id 1217
    label "porada"
  ]
  node [
    id 1218
    label "fotowoltaika"
  ]
  node [
    id 1219
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 1220
    label "przem&#243;wienie"
  ]
  node [
    id 1221
    label "nauki_o_poznaniu"
  ]
  node [
    id 1222
    label "nomotetyczny"
  ]
  node [
    id 1223
    label "systematyka"
  ]
  node [
    id 1224
    label "proces"
  ]
  node [
    id 1225
    label "typologia"
  ]
  node [
    id 1226
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 1227
    label "kultura_duchowa"
  ]
  node [
    id 1228
    label "nauki_penalne"
  ]
  node [
    id 1229
    label "dziedzina"
  ]
  node [
    id 1230
    label "imagineskopia"
  ]
  node [
    id 1231
    label "teoria_naukowa"
  ]
  node [
    id 1232
    label "inwentyka"
  ]
  node [
    id 1233
    label "metodologia"
  ]
  node [
    id 1234
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 1235
    label "nauki_o_Ziemi"
  ]
  node [
    id 1236
    label "zmienienie"
  ]
  node [
    id 1237
    label "zmienia&#263;"
  ]
  node [
    id 1238
    label "zmienianie"
  ]
  node [
    id 1239
    label "zmieni&#263;"
  ]
  node [
    id 1240
    label "podnosi&#263;_si&#281;"
  ]
  node [
    id 1241
    label "heighten"
  ]
  node [
    id 1242
    label "spadek"
  ]
  node [
    id 1243
    label "kurs_p&#322;ynny"
  ]
  node [
    id 1244
    label "spada&#263;"
  ]
  node [
    id 1245
    label "decline"
  ]
  node [
    id 1246
    label "spadanie"
  ]
  node [
    id 1247
    label "kolej"
  ]
  node [
    id 1248
    label "raport"
  ]
  node [
    id 1249
    label "transport"
  ]
  node [
    id 1250
    label "bilet"
  ]
  node [
    id 1251
    label "podnoszenie_si&#281;"
  ]
  node [
    id 1252
    label "wagon"
  ]
  node [
    id 1253
    label "mecz_mistrzowski"
  ]
  node [
    id 1254
    label "arrangement"
  ]
  node [
    id 1255
    label "class"
  ]
  node [
    id 1256
    label "&#322;awka"
  ]
  node [
    id 1257
    label "wykrzyknik"
  ]
  node [
    id 1258
    label "zaleta"
  ]
  node [
    id 1259
    label "programowanie_obiektowe"
  ]
  node [
    id 1260
    label "warstwa"
  ]
  node [
    id 1261
    label "rezerwa"
  ]
  node [
    id 1262
    label "Ekwici"
  ]
  node [
    id 1263
    label "&#347;rodowisko"
  ]
  node [
    id 1264
    label "sala"
  ]
  node [
    id 1265
    label "pomoc"
  ]
  node [
    id 1266
    label "jako&#347;&#263;"
  ]
  node [
    id 1267
    label "znak_jako&#347;ci"
  ]
  node [
    id 1268
    label "poziom"
  ]
  node [
    id 1269
    label "promocja"
  ]
  node [
    id 1270
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 1271
    label "dziennik_lekcyjny"
  ]
  node [
    id 1272
    label "typ"
  ]
  node [
    id 1273
    label "fakcja"
  ]
  node [
    id 1274
    label "botanika"
  ]
  node [
    id 1275
    label "p&#243;&#322;rocze"
  ]
  node [
    id 1276
    label "martwy_sezon"
  ]
  node [
    id 1277
    label "kalendarz"
  ]
  node [
    id 1278
    label "cykl_astronomiczny"
  ]
  node [
    id 1279
    label "lata"
  ]
  node [
    id 1280
    label "pora_roku"
  ]
  node [
    id 1281
    label "stulecie"
  ]
  node [
    id 1282
    label "jubileusz"
  ]
  node [
    id 1283
    label "kwarta&#322;"
  ]
  node [
    id 1284
    label "miesi&#261;c"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 808
  ]
  edge [
    source 9
    target 809
  ]
  edge [
    source 9
    target 810
  ]
  edge [
    source 9
    target 811
  ]
  edge [
    source 9
    target 812
  ]
  edge [
    source 9
    target 813
  ]
  edge [
    source 9
    target 814
  ]
  edge [
    source 9
    target 815
  ]
  edge [
    source 9
    target 816
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 817
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 818
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 819
  ]
  edge [
    source 9
    target 820
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 821
  ]
  edge [
    source 9
    target 822
  ]
  edge [
    source 9
    target 823
  ]
  edge [
    source 9
    target 824
  ]
  edge [
    source 9
    target 825
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 826
  ]
  edge [
    source 9
    target 827
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 828
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 829
  ]
  edge [
    source 9
    target 830
  ]
  edge [
    source 9
    target 831
  ]
  edge [
    source 9
    target 832
  ]
  edge [
    source 9
    target 833
  ]
  edge [
    source 9
    target 834
  ]
  edge [
    source 9
    target 835
  ]
  edge [
    source 9
    target 836
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 837
  ]
  edge [
    source 9
    target 838
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 9
    target 840
  ]
  edge [
    source 9
    target 841
  ]
  edge [
    source 9
    target 842
  ]
  edge [
    source 9
    target 843
  ]
  edge [
    source 9
    target 844
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 845
  ]
  edge [
    source 9
    target 846
  ]
  edge [
    source 9
    target 847
  ]
  edge [
    source 9
    target 848
  ]
  edge [
    source 9
    target 849
  ]
  edge [
    source 9
    target 850
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 851
  ]
  edge [
    source 10
    target 852
  ]
  edge [
    source 10
    target 853
  ]
  edge [
    source 10
    target 854
  ]
  edge [
    source 10
    target 855
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 42
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 894
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 897
  ]
  edge [
    source 11
    target 898
  ]
  edge [
    source 11
    target 899
  ]
  edge [
    source 11
    target 900
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 901
  ]
  edge [
    source 11
    target 902
  ]
  edge [
    source 11
    target 903
  ]
  edge [
    source 11
    target 904
  ]
  edge [
    source 11
    target 905
  ]
  edge [
    source 11
    target 906
  ]
  edge [
    source 11
    target 907
  ]
  edge [
    source 11
    target 908
  ]
  edge [
    source 11
    target 909
  ]
  edge [
    source 11
    target 910
  ]
  edge [
    source 11
    target 911
  ]
  edge [
    source 11
    target 912
  ]
  edge [
    source 11
    target 913
  ]
  edge [
    source 11
    target 914
  ]
  edge [
    source 11
    target 915
  ]
  edge [
    source 11
    target 916
  ]
  edge [
    source 11
    target 917
  ]
  edge [
    source 11
    target 918
  ]
  edge [
    source 11
    target 919
  ]
  edge [
    source 11
    target 920
  ]
  edge [
    source 11
    target 921
  ]
  edge [
    source 11
    target 922
  ]
  edge [
    source 11
    target 923
  ]
  edge [
    source 11
    target 924
  ]
  edge [
    source 11
    target 925
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 926
  ]
  edge [
    source 11
    target 927
  ]
  edge [
    source 11
    target 928
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 929
  ]
  edge [
    source 11
    target 930
  ]
  edge [
    source 11
    target 931
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 932
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 933
  ]
  edge [
    source 11
    target 934
  ]
  edge [
    source 11
    target 935
  ]
  edge [
    source 11
    target 936
  ]
  edge [
    source 11
    target 937
  ]
  edge [
    source 11
    target 938
  ]
  edge [
    source 11
    target 939
  ]
  edge [
    source 11
    target 940
  ]
  edge [
    source 11
    target 941
  ]
  edge [
    source 11
    target 942
  ]
  edge [
    source 11
    target 943
  ]
  edge [
    source 11
    target 944
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 945
  ]
  edge [
    source 11
    target 946
  ]
  edge [
    source 11
    target 947
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 948
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 949
  ]
  edge [
    source 11
    target 950
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 951
  ]
  edge [
    source 11
    target 952
  ]
  edge [
    source 11
    target 953
  ]
  edge [
    source 11
    target 954
  ]
  edge [
    source 11
    target 955
  ]
  edge [
    source 11
    target 956
  ]
  edge [
    source 11
    target 957
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 958
  ]
  edge [
    source 11
    target 959
  ]
  edge [
    source 11
    target 960
  ]
  edge [
    source 11
    target 961
  ]
  edge [
    source 11
    target 962
  ]
  edge [
    source 11
    target 963
  ]
  edge [
    source 11
    target 964
  ]
  edge [
    source 11
    target 965
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 966
  ]
  edge [
    source 11
    target 967
  ]
  edge [
    source 11
    target 968
  ]
  edge [
    source 11
    target 969
  ]
  edge [
    source 11
    target 970
  ]
  edge [
    source 11
    target 971
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 972
  ]
  edge [
    source 11
    target 973
  ]
  edge [
    source 11
    target 974
  ]
  edge [
    source 11
    target 975
  ]
  edge [
    source 11
    target 976
  ]
  edge [
    source 11
    target 977
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 978
  ]
  edge [
    source 11
    target 979
  ]
  edge [
    source 11
    target 980
  ]
  edge [
    source 11
    target 981
  ]
  edge [
    source 11
    target 982
  ]
  edge [
    source 11
    target 983
  ]
  edge [
    source 11
    target 984
  ]
  edge [
    source 11
    target 985
  ]
  edge [
    source 11
    target 986
  ]
  edge [
    source 11
    target 987
  ]
  edge [
    source 11
    target 988
  ]
  edge [
    source 11
    target 989
  ]
  edge [
    source 11
    target 990
  ]
  edge [
    source 11
    target 991
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 992
  ]
  edge [
    source 11
    target 993
  ]
  edge [
    source 11
    target 994
  ]
  edge [
    source 11
    target 995
  ]
  edge [
    source 11
    target 996
  ]
  edge [
    source 11
    target 997
  ]
  edge [
    source 11
    target 998
  ]
  edge [
    source 11
    target 999
  ]
  edge [
    source 11
    target 1000
  ]
  edge [
    source 11
    target 1001
  ]
  edge [
    source 11
    target 1002
  ]
  edge [
    source 11
    target 1003
  ]
  edge [
    source 11
    target 1004
  ]
  edge [
    source 11
    target 1005
  ]
  edge [
    source 11
    target 1006
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 1007
  ]
  edge [
    source 11
    target 1008
  ]
  edge [
    source 11
    target 1009
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 1010
  ]
  edge [
    source 11
    target 1011
  ]
  edge [
    source 11
    target 1012
  ]
  edge [
    source 11
    target 1013
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 1014
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 1015
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 1016
  ]
  edge [
    source 11
    target 1017
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 1018
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 1019
  ]
  edge [
    source 11
    target 1020
  ]
  edge [
    source 11
    target 1021
  ]
  edge [
    source 11
    target 1022
  ]
  edge [
    source 11
    target 1023
  ]
  edge [
    source 11
    target 1024
  ]
  edge [
    source 11
    target 1025
  ]
  edge [
    source 11
    target 1026
  ]
  edge [
    source 11
    target 1027
  ]
  edge [
    source 11
    target 1028
  ]
  edge [
    source 11
    target 1029
  ]
  edge [
    source 11
    target 1030
  ]
  edge [
    source 11
    target 1031
  ]
  edge [
    source 11
    target 1032
  ]
  edge [
    source 11
    target 1033
  ]
  edge [
    source 11
    target 1034
  ]
  edge [
    source 11
    target 1035
  ]
  edge [
    source 11
    target 1036
  ]
  edge [
    source 11
    target 1037
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 1038
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 1039
  ]
  edge [
    source 11
    target 1040
  ]
  edge [
    source 11
    target 1041
  ]
  edge [
    source 11
    target 1042
  ]
  edge [
    source 11
    target 1043
  ]
  edge [
    source 11
    target 1044
  ]
  edge [
    source 11
    target 1045
  ]
  edge [
    source 11
    target 1046
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 1047
  ]
  edge [
    source 11
    target 1048
  ]
  edge [
    source 11
    target 1049
  ]
  edge [
    source 11
    target 1050
  ]
  edge [
    source 11
    target 1051
  ]
  edge [
    source 11
    target 1052
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 1053
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 1054
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 1055
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 1056
  ]
  edge [
    source 11
    target 1057
  ]
  edge [
    source 11
    target 1058
  ]
  edge [
    source 11
    target 1059
  ]
  edge [
    source 11
    target 1060
  ]
  edge [
    source 11
    target 1061
  ]
  edge [
    source 11
    target 1062
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 1063
  ]
  edge [
    source 11
    target 1064
  ]
  edge [
    source 11
    target 1065
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 1066
  ]
  edge [
    source 11
    target 1067
  ]
  edge [
    source 11
    target 1068
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 1069
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 1070
  ]
  edge [
    source 11
    target 1071
  ]
  edge [
    source 11
    target 1072
  ]
  edge [
    source 11
    target 1073
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 1074
  ]
  edge [
    source 11
    target 1075
  ]
  edge [
    source 11
    target 1076
  ]
  edge [
    source 11
    target 1077
  ]
  edge [
    source 11
    target 1078
  ]
  edge [
    source 11
    target 1079
  ]
  edge [
    source 11
    target 1080
  ]
  edge [
    source 11
    target 1081
  ]
  edge [
    source 11
    target 1082
  ]
  edge [
    source 11
    target 1083
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 1084
  ]
  edge [
    source 11
    target 1085
  ]
  edge [
    source 11
    target 1086
  ]
  edge [
    source 11
    target 1087
  ]
  edge [
    source 11
    target 1088
  ]
  edge [
    source 11
    target 1089
  ]
  edge [
    source 11
    target 1090
  ]
  edge [
    source 11
    target 1091
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 1092
  ]
  edge [
    source 11
    target 1093
  ]
  edge [
    source 11
    target 1094
  ]
  edge [
    source 11
    target 1095
  ]
  edge [
    source 11
    target 1096
  ]
  edge [
    source 11
    target 1097
  ]
  edge [
    source 11
    target 1098
  ]
  edge [
    source 11
    target 1099
  ]
  edge [
    source 11
    target 1100
  ]
  edge [
    source 11
    target 1101
  ]
  edge [
    source 11
    target 1102
  ]
  edge [
    source 11
    target 1103
  ]
  edge [
    source 11
    target 1104
  ]
  edge [
    source 11
    target 1105
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 1106
  ]
  edge [
    source 11
    target 1107
  ]
  edge [
    source 11
    target 1108
  ]
  edge [
    source 11
    target 1109
  ]
  edge [
    source 11
    target 1110
  ]
  edge [
    source 11
    target 1111
  ]
  edge [
    source 11
    target 1112
  ]
  edge [
    source 11
    target 1113
  ]
  edge [
    source 11
    target 1114
  ]
  edge [
    source 11
    target 1115
  ]
  edge [
    source 11
    target 1116
  ]
  edge [
    source 11
    target 1117
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 1118
  ]
  edge [
    source 11
    target 1119
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 1120
  ]
  edge [
    source 11
    target 1121
  ]
  edge [
    source 11
    target 1122
  ]
  edge [
    source 11
    target 1123
  ]
  edge [
    source 11
    target 1124
  ]
  edge [
    source 11
    target 1125
  ]
  edge [
    source 11
    target 1126
  ]
  edge [
    source 11
    target 1127
  ]
  edge [
    source 11
    target 1128
  ]
  edge [
    source 11
    target 1129
  ]
  edge [
    source 11
    target 1130
  ]
  edge [
    source 11
    target 1131
  ]
  edge [
    source 11
    target 1132
  ]
  edge [
    source 11
    target 1133
  ]
  edge [
    source 11
    target 1134
  ]
  edge [
    source 11
    target 1135
  ]
  edge [
    source 11
    target 1136
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 1137
  ]
  edge [
    source 11
    target 1138
  ]
  edge [
    source 11
    target 1139
  ]
  edge [
    source 11
    target 1140
  ]
  edge [
    source 11
    target 1141
  ]
  edge [
    source 11
    target 1142
  ]
  edge [
    source 11
    target 1143
  ]
  edge [
    source 11
    target 1144
  ]
  edge [
    source 11
    target 1145
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 1146
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 1147
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1148
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 1149
  ]
  edge [
    source 13
    target 1150
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 1151
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 1152
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 1153
  ]
  edge [
    source 13
    target 1020
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 1154
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 1155
  ]
  edge [
    source 13
    target 1156
  ]
  edge [
    source 13
    target 1157
  ]
  edge [
    source 13
    target 1158
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 1159
  ]
  edge [
    source 13
    target 1160
  ]
  edge [
    source 13
    target 1161
  ]
  edge [
    source 13
    target 1162
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 1163
  ]
  edge [
    source 13
    target 1164
  ]
  edge [
    source 13
    target 1165
  ]
  edge [
    source 13
    target 1166
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 1167
  ]
  edge [
    source 13
    target 1168
  ]
  edge [
    source 13
    target 1169
  ]
  edge [
    source 13
    target 1170
  ]
  edge [
    source 13
    target 1171
  ]
  edge [
    source 13
    target 1172
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 1173
  ]
  edge [
    source 13
    target 289
  ]
  edge [
    source 13
    target 1174
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 1175
  ]
  edge [
    source 13
    target 1081
  ]
  edge [
    source 13
    target 1176
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 1177
  ]
  edge [
    source 13
    target 1178
  ]
  edge [
    source 13
    target 1179
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 1180
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 1181
  ]
  edge [
    source 13
    target 1182
  ]
  edge [
    source 13
    target 1045
  ]
  edge [
    source 13
    target 1183
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 1184
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 264
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 268
  ]
  edge [
    source 13
    target 270
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 271
  ]
  edge [
    source 13
    target 276
  ]
  edge [
    source 13
    target 279
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 282
  ]
  edge [
    source 13
    target 283
  ]
  edge [
    source 13
    target 288
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 1185
  ]
  edge [
    source 13
    target 1186
  ]
  edge [
    source 13
    target 1187
  ]
  edge [
    source 13
    target 1188
  ]
  edge [
    source 13
    target 1189
  ]
  edge [
    source 13
    target 1190
  ]
  edge [
    source 13
    target 1191
  ]
  edge [
    source 13
    target 1192
  ]
  edge [
    source 13
    target 1193
  ]
  edge [
    source 13
    target 1194
  ]
  edge [
    source 13
    target 1195
  ]
  edge [
    source 13
    target 1196
  ]
  edge [
    source 13
    target 1197
  ]
  edge [
    source 13
    target 1198
  ]
  edge [
    source 13
    target 1199
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 1200
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 1201
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 1202
  ]
  edge [
    source 13
    target 1203
  ]
  edge [
    source 13
    target 1204
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 1205
  ]
  edge [
    source 13
    target 1206
  ]
  edge [
    source 13
    target 1207
  ]
  edge [
    source 13
    target 1208
  ]
  edge [
    source 13
    target 1209
  ]
  edge [
    source 13
    target 1210
  ]
  edge [
    source 13
    target 1211
  ]
  edge [
    source 13
    target 1212
  ]
  edge [
    source 13
    target 1213
  ]
  edge [
    source 13
    target 1214
  ]
  edge [
    source 13
    target 1215
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 1216
  ]
  edge [
    source 13
    target 1217
  ]
  edge [
    source 13
    target 1218
  ]
  edge [
    source 13
    target 1219
  ]
  edge [
    source 13
    target 1220
  ]
  edge [
    source 13
    target 1221
  ]
  edge [
    source 13
    target 1222
  ]
  edge [
    source 13
    target 1223
  ]
  edge [
    source 13
    target 1224
  ]
  edge [
    source 13
    target 1225
  ]
  edge [
    source 13
    target 1226
  ]
  edge [
    source 13
    target 1227
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 1228
  ]
  edge [
    source 13
    target 1229
  ]
  edge [
    source 13
    target 1230
  ]
  edge [
    source 13
    target 1231
  ]
  edge [
    source 13
    target 1232
  ]
  edge [
    source 13
    target 1233
  ]
  edge [
    source 13
    target 1234
  ]
  edge [
    source 13
    target 1235
  ]
  edge [
    source 13
    target 1236
  ]
  edge [
    source 13
    target 1237
  ]
  edge [
    source 13
    target 1238
  ]
  edge [
    source 13
    target 1239
  ]
  edge [
    source 13
    target 1240
  ]
  edge [
    source 13
    target 1241
  ]
  edge [
    source 13
    target 1242
  ]
  edge [
    source 13
    target 1243
  ]
  edge [
    source 13
    target 1244
  ]
  edge [
    source 13
    target 1245
  ]
  edge [
    source 13
    target 1246
  ]
  edge [
    source 13
    target 1247
  ]
  edge [
    source 13
    target 1248
  ]
  edge [
    source 13
    target 1249
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 1250
  ]
  edge [
    source 13
    target 1251
  ]
  edge [
    source 13
    target 1252
  ]
  edge [
    source 13
    target 1253
  ]
  edge [
    source 13
    target 1076
  ]
  edge [
    source 13
    target 1254
  ]
  edge [
    source 13
    target 1255
  ]
  edge [
    source 13
    target 1256
  ]
  edge [
    source 13
    target 1257
  ]
  edge [
    source 13
    target 1258
  ]
  edge [
    source 13
    target 1259
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 1260
  ]
  edge [
    source 13
    target 1261
  ]
  edge [
    source 13
    target 1262
  ]
  edge [
    source 13
    target 1263
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 1264
  ]
  edge [
    source 13
    target 1265
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 1266
  ]
  edge [
    source 13
    target 1267
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 1268
  ]
  edge [
    source 13
    target 1269
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 1270
  ]
  edge [
    source 13
    target 1271
  ]
  edge [
    source 13
    target 1272
  ]
  edge [
    source 13
    target 1273
  ]
  edge [
    source 13
    target 1006
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 1274
  ]
  edge [
    source 13
    target 1275
  ]
  edge [
    source 13
    target 1276
  ]
  edge [
    source 13
    target 1277
  ]
  edge [
    source 13
    target 1278
  ]
  edge [
    source 13
    target 1279
  ]
  edge [
    source 13
    target 1280
  ]
  edge [
    source 13
    target 1281
  ]
  edge [
    source 13
    target 1282
  ]
  edge [
    source 13
    target 1283
  ]
  edge [
    source 13
    target 1284
  ]
]
