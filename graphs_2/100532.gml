graph [
  node [
    id 0
    label "marina"
    origin "text"
  ]
  node [
    id 1
    label "premeru"
    origin "text"
  ]
  node [
    id 2
    label "dzie&#322;o"
  ]
  node [
    id 3
    label "przysta&#324;"
  ]
  node [
    id 4
    label "morze"
  ]
  node [
    id 5
    label "obraz"
  ]
  node [
    id 6
    label "projekcja"
  ]
  node [
    id 7
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 8
    label "wypunktowa&#263;"
  ]
  node [
    id 9
    label "opinion"
  ]
  node [
    id 10
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 11
    label "inning"
  ]
  node [
    id 12
    label "zaj&#347;cie"
  ]
  node [
    id 13
    label "representation"
  ]
  node [
    id 14
    label "filmoteka"
  ]
  node [
    id 15
    label "zbi&#243;r"
  ]
  node [
    id 16
    label "widok"
  ]
  node [
    id 17
    label "podobrazie"
  ]
  node [
    id 18
    label "przeplot"
  ]
  node [
    id 19
    label "napisy"
  ]
  node [
    id 20
    label "pogl&#261;d"
  ]
  node [
    id 21
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 22
    label "oprawia&#263;"
  ]
  node [
    id 23
    label "ziarno"
  ]
  node [
    id 24
    label "human_body"
  ]
  node [
    id 25
    label "ostro&#347;&#263;"
  ]
  node [
    id 26
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 27
    label "pulment"
  ]
  node [
    id 28
    label "sztafa&#380;"
  ]
  node [
    id 29
    label "punktowa&#263;"
  ]
  node [
    id 30
    label "picture"
  ]
  node [
    id 31
    label "scena"
  ]
  node [
    id 32
    label "przedstawienie"
  ]
  node [
    id 33
    label "persona"
  ]
  node [
    id 34
    label "zjawisko"
  ]
  node [
    id 35
    label "oprawianie"
  ]
  node [
    id 36
    label "malarz"
  ]
  node [
    id 37
    label "uj&#281;cie"
  ]
  node [
    id 38
    label "parkiet"
  ]
  node [
    id 39
    label "t&#322;o"
  ]
  node [
    id 40
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 41
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 42
    label "effigy"
  ]
  node [
    id 43
    label "anamorfoza"
  ]
  node [
    id 44
    label "perspektywa"
  ]
  node [
    id 45
    label "czo&#322;&#243;wka"
  ]
  node [
    id 46
    label "plama_barwna"
  ]
  node [
    id 47
    label "postprodukcja"
  ]
  node [
    id 48
    label "wytw&#243;r"
  ]
  node [
    id 49
    label "rola"
  ]
  node [
    id 50
    label "ty&#322;&#243;wka"
  ]
  node [
    id 51
    label "komunikat"
  ]
  node [
    id 52
    label "tekst"
  ]
  node [
    id 53
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 54
    label "dorobek"
  ]
  node [
    id 55
    label "praca"
  ]
  node [
    id 56
    label "tre&#347;&#263;"
  ]
  node [
    id 57
    label "works"
  ]
  node [
    id 58
    label "obrazowanie"
  ]
  node [
    id 59
    label "retrospektywa"
  ]
  node [
    id 60
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 61
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 62
    label "forma"
  ]
  node [
    id 63
    label "creation"
  ]
  node [
    id 64
    label "tetralogia"
  ]
  node [
    id 65
    label "slip"
  ]
  node [
    id 66
    label "port"
  ]
  node [
    id 67
    label "reda"
  ]
  node [
    id 68
    label "pe&#322;ne_morze"
  ]
  node [
    id 69
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 70
    label "Morze_Bia&#322;e"
  ]
  node [
    id 71
    label "przymorze"
  ]
  node [
    id 72
    label "Morze_Adriatyckie"
  ]
  node [
    id 73
    label "paliszcze"
  ]
  node [
    id 74
    label "talasoterapia"
  ]
  node [
    id 75
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 76
    label "bezmiar"
  ]
  node [
    id 77
    label "Morze_Egejskie"
  ]
  node [
    id 78
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 79
    label "latarnia_morska"
  ]
  node [
    id 80
    label "Neptun"
  ]
  node [
    id 81
    label "zbiornik_wodny"
  ]
  node [
    id 82
    label "Morze_Czarne"
  ]
  node [
    id 83
    label "nereida"
  ]
  node [
    id 84
    label "Ziemia"
  ]
  node [
    id 85
    label "laguna"
  ]
  node [
    id 86
    label "okeanida"
  ]
  node [
    id 87
    label "Morze_Czerwone"
  ]
  node [
    id 88
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 89
    label "Premeru"
  ]
  node [
    id 90
    label "nowy"
  ]
  node [
    id 91
    label "sad"
  ]
  node [
    id 92
    label "europejski"
  ]
  node [
    id 93
    label "festiwal"
  ]
  node [
    id 94
    label "m&#322;odzie&#380;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 94
  ]
  edge [
    source 93
    target 94
  ]
]
