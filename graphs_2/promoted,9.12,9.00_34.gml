graph [
  node [
    id 0
    label "kart"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 3
    label "kupi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "bilet"
    origin "text"
  ]
  node [
    id 5
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 6
    label "automat"
    origin "text"
  ]
  node [
    id 7
    label "wy&#347;cig&#243;wka"
  ]
  node [
    id 8
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 9
    label "rower"
  ]
  node [
    id 10
    label "gra"
  ]
  node [
    id 11
    label "program"
  ]
  node [
    id 12
    label "&#322;y&#380;wa"
  ]
  node [
    id 13
    label "rajd&#243;wka"
  ]
  node [
    id 14
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 15
    label "mie&#263;_miejsce"
  ]
  node [
    id 16
    label "equal"
  ]
  node [
    id 17
    label "trwa&#263;"
  ]
  node [
    id 18
    label "chodzi&#263;"
  ]
  node [
    id 19
    label "si&#281;ga&#263;"
  ]
  node [
    id 20
    label "stan"
  ]
  node [
    id 21
    label "obecno&#347;&#263;"
  ]
  node [
    id 22
    label "stand"
  ]
  node [
    id 23
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 24
    label "uczestniczy&#263;"
  ]
  node [
    id 25
    label "participate"
  ]
  node [
    id 26
    label "robi&#263;"
  ]
  node [
    id 27
    label "istnie&#263;"
  ]
  node [
    id 28
    label "pozostawa&#263;"
  ]
  node [
    id 29
    label "zostawa&#263;"
  ]
  node [
    id 30
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 31
    label "adhere"
  ]
  node [
    id 32
    label "compass"
  ]
  node [
    id 33
    label "korzysta&#263;"
  ]
  node [
    id 34
    label "appreciation"
  ]
  node [
    id 35
    label "osi&#261;ga&#263;"
  ]
  node [
    id 36
    label "dociera&#263;"
  ]
  node [
    id 37
    label "get"
  ]
  node [
    id 38
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 39
    label "mierzy&#263;"
  ]
  node [
    id 40
    label "u&#380;ywa&#263;"
  ]
  node [
    id 41
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 42
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 43
    label "exsert"
  ]
  node [
    id 44
    label "being"
  ]
  node [
    id 45
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 46
    label "cecha"
  ]
  node [
    id 47
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 48
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 49
    label "p&#322;ywa&#263;"
  ]
  node [
    id 50
    label "run"
  ]
  node [
    id 51
    label "bangla&#263;"
  ]
  node [
    id 52
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 53
    label "przebiega&#263;"
  ]
  node [
    id 54
    label "wk&#322;ada&#263;"
  ]
  node [
    id 55
    label "proceed"
  ]
  node [
    id 56
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 57
    label "carry"
  ]
  node [
    id 58
    label "bywa&#263;"
  ]
  node [
    id 59
    label "dziama&#263;"
  ]
  node [
    id 60
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 61
    label "stara&#263;_si&#281;"
  ]
  node [
    id 62
    label "para"
  ]
  node [
    id 63
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 64
    label "str&#243;j"
  ]
  node [
    id 65
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 66
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 67
    label "krok"
  ]
  node [
    id 68
    label "tryb"
  ]
  node [
    id 69
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 70
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 71
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 72
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 73
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 74
    label "continue"
  ]
  node [
    id 75
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 76
    label "Ohio"
  ]
  node [
    id 77
    label "wci&#281;cie"
  ]
  node [
    id 78
    label "Nowy_York"
  ]
  node [
    id 79
    label "warstwa"
  ]
  node [
    id 80
    label "samopoczucie"
  ]
  node [
    id 81
    label "Illinois"
  ]
  node [
    id 82
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 83
    label "state"
  ]
  node [
    id 84
    label "Jukatan"
  ]
  node [
    id 85
    label "Kalifornia"
  ]
  node [
    id 86
    label "Wirginia"
  ]
  node [
    id 87
    label "wektor"
  ]
  node [
    id 88
    label "Teksas"
  ]
  node [
    id 89
    label "Goa"
  ]
  node [
    id 90
    label "Waszyngton"
  ]
  node [
    id 91
    label "miejsce"
  ]
  node [
    id 92
    label "Massachusetts"
  ]
  node [
    id 93
    label "Alaska"
  ]
  node [
    id 94
    label "Arakan"
  ]
  node [
    id 95
    label "Hawaje"
  ]
  node [
    id 96
    label "Maryland"
  ]
  node [
    id 97
    label "punkt"
  ]
  node [
    id 98
    label "Michigan"
  ]
  node [
    id 99
    label "Arizona"
  ]
  node [
    id 100
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 101
    label "Georgia"
  ]
  node [
    id 102
    label "poziom"
  ]
  node [
    id 103
    label "Pensylwania"
  ]
  node [
    id 104
    label "shape"
  ]
  node [
    id 105
    label "Luizjana"
  ]
  node [
    id 106
    label "Nowy_Meksyk"
  ]
  node [
    id 107
    label "Alabama"
  ]
  node [
    id 108
    label "ilo&#347;&#263;"
  ]
  node [
    id 109
    label "Kansas"
  ]
  node [
    id 110
    label "Oregon"
  ]
  node [
    id 111
    label "Floryda"
  ]
  node [
    id 112
    label "Oklahoma"
  ]
  node [
    id 113
    label "jednostka_administracyjna"
  ]
  node [
    id 114
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 115
    label "free"
  ]
  node [
    id 116
    label "pozyska&#263;"
  ]
  node [
    id 117
    label "ustawi&#263;"
  ]
  node [
    id 118
    label "wzi&#261;&#263;"
  ]
  node [
    id 119
    label "uwierzy&#263;"
  ]
  node [
    id 120
    label "catch"
  ]
  node [
    id 121
    label "zagra&#263;"
  ]
  node [
    id 122
    label "beget"
  ]
  node [
    id 123
    label "uzna&#263;"
  ]
  node [
    id 124
    label "przyj&#261;&#263;"
  ]
  node [
    id 125
    label "play"
  ]
  node [
    id 126
    label "zabrzmie&#263;"
  ]
  node [
    id 127
    label "leave"
  ]
  node [
    id 128
    label "instrument_muzyczny"
  ]
  node [
    id 129
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 130
    label "flare"
  ]
  node [
    id 131
    label "rozegra&#263;"
  ]
  node [
    id 132
    label "zrobi&#263;"
  ]
  node [
    id 133
    label "zaszczeka&#263;"
  ]
  node [
    id 134
    label "sound"
  ]
  node [
    id 135
    label "represent"
  ]
  node [
    id 136
    label "wykorzysta&#263;"
  ]
  node [
    id 137
    label "zatokowa&#263;"
  ]
  node [
    id 138
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 139
    label "uda&#263;_si&#281;"
  ]
  node [
    id 140
    label "zacz&#261;&#263;"
  ]
  node [
    id 141
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 142
    label "wykona&#263;"
  ]
  node [
    id 143
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 144
    label "typify"
  ]
  node [
    id 145
    label "rola"
  ]
  node [
    id 146
    label "poprawi&#263;"
  ]
  node [
    id 147
    label "nada&#263;"
  ]
  node [
    id 148
    label "peddle"
  ]
  node [
    id 149
    label "marshal"
  ]
  node [
    id 150
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 151
    label "wyznaczy&#263;"
  ]
  node [
    id 152
    label "stanowisko"
  ]
  node [
    id 153
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 154
    label "spowodowa&#263;"
  ]
  node [
    id 155
    label "zabezpieczy&#263;"
  ]
  node [
    id 156
    label "umie&#347;ci&#263;"
  ]
  node [
    id 157
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 158
    label "zinterpretowa&#263;"
  ]
  node [
    id 159
    label "wskaza&#263;"
  ]
  node [
    id 160
    label "set"
  ]
  node [
    id 161
    label "przyzna&#263;"
  ]
  node [
    id 162
    label "sk&#322;oni&#263;"
  ]
  node [
    id 163
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 164
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 165
    label "zdecydowa&#263;"
  ]
  node [
    id 166
    label "accommodate"
  ]
  node [
    id 167
    label "ustali&#263;"
  ]
  node [
    id 168
    label "situate"
  ]
  node [
    id 169
    label "odziedziczy&#263;"
  ]
  node [
    id 170
    label "ruszy&#263;"
  ]
  node [
    id 171
    label "take"
  ]
  node [
    id 172
    label "zaatakowa&#263;"
  ]
  node [
    id 173
    label "skorzysta&#263;"
  ]
  node [
    id 174
    label "uciec"
  ]
  node [
    id 175
    label "receive"
  ]
  node [
    id 176
    label "nakaza&#263;"
  ]
  node [
    id 177
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 178
    label "obskoczy&#263;"
  ]
  node [
    id 179
    label "bra&#263;"
  ]
  node [
    id 180
    label "u&#380;y&#263;"
  ]
  node [
    id 181
    label "wyrucha&#263;"
  ]
  node [
    id 182
    label "World_Health_Organization"
  ]
  node [
    id 183
    label "wyciupcia&#263;"
  ]
  node [
    id 184
    label "wygra&#263;"
  ]
  node [
    id 185
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 186
    label "withdraw"
  ]
  node [
    id 187
    label "wzi&#281;cie"
  ]
  node [
    id 188
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 189
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 190
    label "poczyta&#263;"
  ]
  node [
    id 191
    label "obj&#261;&#263;"
  ]
  node [
    id 192
    label "seize"
  ]
  node [
    id 193
    label "aim"
  ]
  node [
    id 194
    label "chwyci&#263;"
  ]
  node [
    id 195
    label "pokona&#263;"
  ]
  node [
    id 196
    label "arise"
  ]
  node [
    id 197
    label "otrzyma&#263;"
  ]
  node [
    id 198
    label "wej&#347;&#263;"
  ]
  node [
    id 199
    label "poruszy&#263;"
  ]
  node [
    id 200
    label "dosta&#263;"
  ]
  node [
    id 201
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 202
    label "stage"
  ]
  node [
    id 203
    label "uzyska&#263;"
  ]
  node [
    id 204
    label "wytworzy&#263;"
  ]
  node [
    id 205
    label "give_birth"
  ]
  node [
    id 206
    label "przybra&#263;"
  ]
  node [
    id 207
    label "strike"
  ]
  node [
    id 208
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 209
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 210
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 211
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 212
    label "obra&#263;"
  ]
  node [
    id 213
    label "draw"
  ]
  node [
    id 214
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 215
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 216
    label "przyj&#281;cie"
  ]
  node [
    id 217
    label "fall"
  ]
  node [
    id 218
    label "swallow"
  ]
  node [
    id 219
    label "odebra&#263;"
  ]
  node [
    id 220
    label "dostarczy&#263;"
  ]
  node [
    id 221
    label "absorb"
  ]
  node [
    id 222
    label "undertake"
  ]
  node [
    id 223
    label "oceni&#263;"
  ]
  node [
    id 224
    label "stwierdzi&#263;"
  ]
  node [
    id 225
    label "assent"
  ]
  node [
    id 226
    label "rede"
  ]
  node [
    id 227
    label "see"
  ]
  node [
    id 228
    label "trust"
  ]
  node [
    id 229
    label "karta_wst&#281;pu"
  ]
  node [
    id 230
    label "konik"
  ]
  node [
    id 231
    label "passe-partout"
  ]
  node [
    id 232
    label "cedu&#322;a"
  ]
  node [
    id 233
    label "bordiura"
  ]
  node [
    id 234
    label "kolej"
  ]
  node [
    id 235
    label "raport"
  ]
  node [
    id 236
    label "transport"
  ]
  node [
    id 237
    label "kurs"
  ]
  node [
    id 238
    label "spis"
  ]
  node [
    id 239
    label "zaj&#281;cie"
  ]
  node [
    id 240
    label "cyka&#263;"
  ]
  node [
    id 241
    label "zabawka"
  ]
  node [
    id 242
    label "figura"
  ]
  node [
    id 243
    label "szara&#324;czak"
  ]
  node [
    id 244
    label "grasshopper"
  ]
  node [
    id 245
    label "fondness"
  ]
  node [
    id 246
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 247
    label "mechanizm"
  ]
  node [
    id 248
    label "za&#322;atwiacz"
  ]
  node [
    id 249
    label "nijaki"
  ]
  node [
    id 250
    label "nijak"
  ]
  node [
    id 251
    label "niezabawny"
  ]
  node [
    id 252
    label "zwyczajny"
  ]
  node [
    id 253
    label "oboj&#281;tny"
  ]
  node [
    id 254
    label "poszarzenie"
  ]
  node [
    id 255
    label "neutralny"
  ]
  node [
    id 256
    label "szarzenie"
  ]
  node [
    id 257
    label "bezbarwnie"
  ]
  node [
    id 258
    label "nieciekawy"
  ]
  node [
    id 259
    label "cz&#322;owiek"
  ]
  node [
    id 260
    label "automatyczna_skrzynia_bieg&#243;w"
  ]
  node [
    id 261
    label "wrzutnik_monet"
  ]
  node [
    id 262
    label "samoch&#243;d"
  ]
  node [
    id 263
    label "telefon"
  ]
  node [
    id 264
    label "dehumanizacja"
  ]
  node [
    id 265
    label "pistolet"
  ]
  node [
    id 266
    label "bro&#324;_samoczynno-samopowtarzalna"
  ]
  node [
    id 267
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 268
    label "pralka"
  ]
  node [
    id 269
    label "urz&#261;dzenie"
  ]
  node [
    id 270
    label "lody_w&#322;oskie"
  ]
  node [
    id 271
    label "ludzko&#347;&#263;"
  ]
  node [
    id 272
    label "asymilowanie"
  ]
  node [
    id 273
    label "wapniak"
  ]
  node [
    id 274
    label "asymilowa&#263;"
  ]
  node [
    id 275
    label "os&#322;abia&#263;"
  ]
  node [
    id 276
    label "posta&#263;"
  ]
  node [
    id 277
    label "hominid"
  ]
  node [
    id 278
    label "podw&#322;adny"
  ]
  node [
    id 279
    label "os&#322;abianie"
  ]
  node [
    id 280
    label "g&#322;owa"
  ]
  node [
    id 281
    label "portrecista"
  ]
  node [
    id 282
    label "dwun&#243;g"
  ]
  node [
    id 283
    label "profanum"
  ]
  node [
    id 284
    label "mikrokosmos"
  ]
  node [
    id 285
    label "nasada"
  ]
  node [
    id 286
    label "duch"
  ]
  node [
    id 287
    label "antropochoria"
  ]
  node [
    id 288
    label "osoba"
  ]
  node [
    id 289
    label "wz&#243;r"
  ]
  node [
    id 290
    label "senior"
  ]
  node [
    id 291
    label "oddzia&#322;ywanie"
  ]
  node [
    id 292
    label "Adam"
  ]
  node [
    id 293
    label "homo_sapiens"
  ]
  node [
    id 294
    label "polifag"
  ]
  node [
    id 295
    label "pojazd_drogowy"
  ]
  node [
    id 296
    label "spryskiwacz"
  ]
  node [
    id 297
    label "most"
  ]
  node [
    id 298
    label "baga&#380;nik"
  ]
  node [
    id 299
    label "silnik"
  ]
  node [
    id 300
    label "dachowanie"
  ]
  node [
    id 301
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 302
    label "pompa_wodna"
  ]
  node [
    id 303
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 304
    label "poduszka_powietrzna"
  ]
  node [
    id 305
    label "tempomat"
  ]
  node [
    id 306
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 307
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 308
    label "deska_rozdzielcza"
  ]
  node [
    id 309
    label "immobilizer"
  ]
  node [
    id 310
    label "t&#322;umik"
  ]
  node [
    id 311
    label "ABS"
  ]
  node [
    id 312
    label "kierownica"
  ]
  node [
    id 313
    label "bak"
  ]
  node [
    id 314
    label "dwu&#347;lad"
  ]
  node [
    id 315
    label "poci&#261;g_drogowy"
  ]
  node [
    id 316
    label "wycieraczka"
  ]
  node [
    id 317
    label "sprz&#281;t_AGD"
  ]
  node [
    id 318
    label "zawarto&#347;&#263;"
  ]
  node [
    id 319
    label "urz&#261;dzenie_domowe"
  ]
  node [
    id 320
    label "przedmiot"
  ]
  node [
    id 321
    label "kom&#243;rka"
  ]
  node [
    id 322
    label "furnishing"
  ]
  node [
    id 323
    label "zabezpieczenie"
  ]
  node [
    id 324
    label "zrobienie"
  ]
  node [
    id 325
    label "wyrz&#261;dzenie"
  ]
  node [
    id 326
    label "zagospodarowanie"
  ]
  node [
    id 327
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 328
    label "ig&#322;a"
  ]
  node [
    id 329
    label "narz&#281;dzie"
  ]
  node [
    id 330
    label "wirnik"
  ]
  node [
    id 331
    label "aparatura"
  ]
  node [
    id 332
    label "system_energetyczny"
  ]
  node [
    id 333
    label "impulsator"
  ]
  node [
    id 334
    label "sprz&#281;t"
  ]
  node [
    id 335
    label "czynno&#347;&#263;"
  ]
  node [
    id 336
    label "blokowanie"
  ]
  node [
    id 337
    label "zablokowanie"
  ]
  node [
    id 338
    label "przygotowanie"
  ]
  node [
    id 339
    label "komora"
  ]
  node [
    id 340
    label "j&#281;zyk"
  ]
  node [
    id 341
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 342
    label "kurcz&#281;"
  ]
  node [
    id 343
    label "bro&#324;_palna"
  ]
  node [
    id 344
    label "bro&#324;"
  ]
  node [
    id 345
    label "bro&#324;_osobista"
  ]
  node [
    id 346
    label "rajtar"
  ]
  node [
    id 347
    label "spluwa"
  ]
  node [
    id 348
    label "bro&#324;_kr&#243;tka"
  ]
  node [
    id 349
    label "odbezpieczy&#263;"
  ]
  node [
    id 350
    label "tejsak"
  ]
  node [
    id 351
    label "odbezpieczenie"
  ]
  node [
    id 352
    label "kolba"
  ]
  node [
    id 353
    label "odbezpieczanie"
  ]
  node [
    id 354
    label "zabezpiecza&#263;"
  ]
  node [
    id 355
    label "bro&#324;_strzelecka"
  ]
  node [
    id 356
    label "zabezpieczanie"
  ]
  node [
    id 357
    label "giwera"
  ]
  node [
    id 358
    label "odbezpiecza&#263;"
  ]
  node [
    id 359
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 360
    label "zadzwoni&#263;"
  ]
  node [
    id 361
    label "provider"
  ]
  node [
    id 362
    label "infrastruktura"
  ]
  node [
    id 363
    label "numer"
  ]
  node [
    id 364
    label "po&#322;&#261;czenie"
  ]
  node [
    id 365
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 366
    label "phreaker"
  ]
  node [
    id 367
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 368
    label "mikrotelefon"
  ]
  node [
    id 369
    label "billing"
  ]
  node [
    id 370
    label "dzwoni&#263;"
  ]
  node [
    id 371
    label "instalacja"
  ]
  node [
    id 372
    label "kontakt"
  ]
  node [
    id 373
    label "coalescence"
  ]
  node [
    id 374
    label "wy&#347;wietlacz"
  ]
  node [
    id 375
    label "dzwonienie"
  ]
  node [
    id 376
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 377
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 378
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 379
    label "zepsucie"
  ]
  node [
    id 380
    label "proces"
  ]
  node [
    id 381
    label "zjawisko"
  ]
  node [
    id 382
    label "intelektualizacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
]
