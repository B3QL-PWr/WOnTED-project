graph [
  node [
    id 0
    label "kr&#243;tki"
    origin "text"
  ]
  node [
    id 1
    label "opis"
    origin "text"
  ]
  node [
    id 2
    label "szybki"
  ]
  node [
    id 3
    label "jednowyrazowy"
  ]
  node [
    id 4
    label "bliski"
  ]
  node [
    id 5
    label "s&#322;aby"
  ]
  node [
    id 6
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 7
    label "kr&#243;tko"
  ]
  node [
    id 8
    label "drobny"
  ]
  node [
    id 9
    label "ruch"
  ]
  node [
    id 10
    label "brak"
  ]
  node [
    id 11
    label "z&#322;y"
  ]
  node [
    id 12
    label "pieski"
  ]
  node [
    id 13
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 14
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 15
    label "niekorzystny"
  ]
  node [
    id 16
    label "z&#322;oszczenie"
  ]
  node [
    id 17
    label "sierdzisty"
  ]
  node [
    id 18
    label "niegrzeczny"
  ]
  node [
    id 19
    label "zez&#322;oszczenie"
  ]
  node [
    id 20
    label "zdenerwowany"
  ]
  node [
    id 21
    label "negatywny"
  ]
  node [
    id 22
    label "rozgniewanie"
  ]
  node [
    id 23
    label "gniewanie"
  ]
  node [
    id 24
    label "niemoralny"
  ]
  node [
    id 25
    label "&#378;le"
  ]
  node [
    id 26
    label "niepomy&#347;lny"
  ]
  node [
    id 27
    label "syf"
  ]
  node [
    id 28
    label "sprawny"
  ]
  node [
    id 29
    label "efektywny"
  ]
  node [
    id 30
    label "zwi&#281;&#378;le"
  ]
  node [
    id 31
    label "oszcz&#281;dny"
  ]
  node [
    id 32
    label "nietrwa&#322;y"
  ]
  node [
    id 33
    label "mizerny"
  ]
  node [
    id 34
    label "marnie"
  ]
  node [
    id 35
    label "delikatny"
  ]
  node [
    id 36
    label "po&#347;ledni"
  ]
  node [
    id 37
    label "niezdrowy"
  ]
  node [
    id 38
    label "nieumiej&#281;tny"
  ]
  node [
    id 39
    label "s&#322;abo"
  ]
  node [
    id 40
    label "nieznaczny"
  ]
  node [
    id 41
    label "lura"
  ]
  node [
    id 42
    label "nieudany"
  ]
  node [
    id 43
    label "s&#322;abowity"
  ]
  node [
    id 44
    label "zawodny"
  ]
  node [
    id 45
    label "&#322;agodny"
  ]
  node [
    id 46
    label "md&#322;y"
  ]
  node [
    id 47
    label "niedoskona&#322;y"
  ]
  node [
    id 48
    label "przemijaj&#261;cy"
  ]
  node [
    id 49
    label "niemocny"
  ]
  node [
    id 50
    label "niefajny"
  ]
  node [
    id 51
    label "kiepsko"
  ]
  node [
    id 52
    label "blisko"
  ]
  node [
    id 53
    label "cz&#322;owiek"
  ]
  node [
    id 54
    label "znajomy"
  ]
  node [
    id 55
    label "zwi&#261;zany"
  ]
  node [
    id 56
    label "przesz&#322;y"
  ]
  node [
    id 57
    label "silny"
  ]
  node [
    id 58
    label "zbli&#380;enie"
  ]
  node [
    id 59
    label "oddalony"
  ]
  node [
    id 60
    label "dok&#322;adny"
  ]
  node [
    id 61
    label "nieodleg&#322;y"
  ]
  node [
    id 62
    label "przysz&#322;y"
  ]
  node [
    id 63
    label "gotowy"
  ]
  node [
    id 64
    label "ma&#322;y"
  ]
  node [
    id 65
    label "intensywny"
  ]
  node [
    id 66
    label "prosty"
  ]
  node [
    id 67
    label "temperamentny"
  ]
  node [
    id 68
    label "bystrolotny"
  ]
  node [
    id 69
    label "dynamiczny"
  ]
  node [
    id 70
    label "szybko"
  ]
  node [
    id 71
    label "bezpo&#347;redni"
  ]
  node [
    id 72
    label "energiczny"
  ]
  node [
    id 73
    label "skromny"
  ]
  node [
    id 74
    label "niesamodzielny"
  ]
  node [
    id 75
    label "niewa&#380;ny"
  ]
  node [
    id 76
    label "podhala&#324;ski"
  ]
  node [
    id 77
    label "taniec_ludowy"
  ]
  node [
    id 78
    label "szczup&#322;y"
  ]
  node [
    id 79
    label "drobno"
  ]
  node [
    id 80
    label "ma&#322;oletni"
  ]
  node [
    id 81
    label "nieistnienie"
  ]
  node [
    id 82
    label "odej&#347;cie"
  ]
  node [
    id 83
    label "defect"
  ]
  node [
    id 84
    label "gap"
  ]
  node [
    id 85
    label "odej&#347;&#263;"
  ]
  node [
    id 86
    label "wada"
  ]
  node [
    id 87
    label "odchodzi&#263;"
  ]
  node [
    id 88
    label "wyr&#243;b"
  ]
  node [
    id 89
    label "odchodzenie"
  ]
  node [
    id 90
    label "prywatywny"
  ]
  node [
    id 91
    label "jednocz&#322;onowy"
  ]
  node [
    id 92
    label "mechanika"
  ]
  node [
    id 93
    label "utrzymywanie"
  ]
  node [
    id 94
    label "move"
  ]
  node [
    id 95
    label "poruszenie"
  ]
  node [
    id 96
    label "movement"
  ]
  node [
    id 97
    label "myk"
  ]
  node [
    id 98
    label "utrzyma&#263;"
  ]
  node [
    id 99
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 100
    label "zjawisko"
  ]
  node [
    id 101
    label "utrzymanie"
  ]
  node [
    id 102
    label "travel"
  ]
  node [
    id 103
    label "kanciasty"
  ]
  node [
    id 104
    label "commercial_enterprise"
  ]
  node [
    id 105
    label "model"
  ]
  node [
    id 106
    label "strumie&#324;"
  ]
  node [
    id 107
    label "proces"
  ]
  node [
    id 108
    label "aktywno&#347;&#263;"
  ]
  node [
    id 109
    label "taktyka"
  ]
  node [
    id 110
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 111
    label "apraksja"
  ]
  node [
    id 112
    label "natural_process"
  ]
  node [
    id 113
    label "utrzymywa&#263;"
  ]
  node [
    id 114
    label "d&#322;ugi"
  ]
  node [
    id 115
    label "wydarzenie"
  ]
  node [
    id 116
    label "dyssypacja_energii"
  ]
  node [
    id 117
    label "tumult"
  ]
  node [
    id 118
    label "stopek"
  ]
  node [
    id 119
    label "czynno&#347;&#263;"
  ]
  node [
    id 120
    label "zmiana"
  ]
  node [
    id 121
    label "manewr"
  ]
  node [
    id 122
    label "lokomocja"
  ]
  node [
    id 123
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 124
    label "komunikacja"
  ]
  node [
    id 125
    label "drift"
  ]
  node [
    id 126
    label "wypowied&#378;"
  ]
  node [
    id 127
    label "exposition"
  ]
  node [
    id 128
    label "obja&#347;nienie"
  ]
  node [
    id 129
    label "activity"
  ]
  node [
    id 130
    label "bezproblemowy"
  ]
  node [
    id 131
    label "explanation"
  ]
  node [
    id 132
    label "remark"
  ]
  node [
    id 133
    label "report"
  ]
  node [
    id 134
    label "zrozumia&#322;y"
  ]
  node [
    id 135
    label "przedstawienie"
  ]
  node [
    id 136
    label "informacja"
  ]
  node [
    id 137
    label "poinformowanie"
  ]
  node [
    id 138
    label "pos&#322;uchanie"
  ]
  node [
    id 139
    label "s&#261;d"
  ]
  node [
    id 140
    label "sparafrazowanie"
  ]
  node [
    id 141
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 142
    label "strawestowa&#263;"
  ]
  node [
    id 143
    label "sparafrazowa&#263;"
  ]
  node [
    id 144
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 145
    label "trawestowa&#263;"
  ]
  node [
    id 146
    label "sformu&#322;owanie"
  ]
  node [
    id 147
    label "parafrazowanie"
  ]
  node [
    id 148
    label "ozdobnik"
  ]
  node [
    id 149
    label "delimitacja"
  ]
  node [
    id 150
    label "parafrazowa&#263;"
  ]
  node [
    id 151
    label "stylizacja"
  ]
  node [
    id 152
    label "komunikat"
  ]
  node [
    id 153
    label "trawestowanie"
  ]
  node [
    id 154
    label "strawestowanie"
  ]
  node [
    id 155
    label "rezultat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
]
