graph [
  node [
    id 0
    label "metoda"
    origin "text"
  ]
  node [
    id 1
    label "pomiarowy"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 4
    label "por&#243;wnywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wielko&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "mierzy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wzorcowy"
    origin "text"
  ]
  node [
    id 8
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 9
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 10
    label "niezale&#380;ny"
    origin "text"
  ]
  node [
    id 11
    label "rodzaj"
    origin "text"
  ]
  node [
    id 12
    label "zjawisko"
    origin "text"
  ]
  node [
    id 13
    label "fizyczny"
    origin "text"
  ]
  node [
    id 14
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "przyrz&#261;d"
    origin "text"
  ]
  node [
    id 16
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "zasada"
    origin "text"
  ]
  node [
    id 18
    label "nazwa"
    origin "text"
  ]
  node [
    id 19
    label "przedstawienie"
    origin "text"
  ]
  node [
    id 20
    label "spotyka&#263;"
    origin "text"
  ]
  node [
    id 21
    label "literatura"
    origin "text"
  ]
  node [
    id 22
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 23
    label "r&#243;&#380;ni&#263;"
    origin "text"
  ]
  node [
    id 24
    label "si&#281;"
    origin "text"
  ]
  node [
    id 25
    label "w&#322;a&#347;ciwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 26
    label "u&#322;atwia&#263;"
    origin "text"
  ]
  node [
    id 27
    label "pozna&#324;"
    origin "text"
  ]
  node [
    id 28
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 29
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 30
    label "dana"
    origin "text"
  ]
  node [
    id 31
    label "zastosowa&#263;"
    origin "text"
  ]
  node [
    id 32
    label "okre&#347;lony"
    origin "text"
  ]
  node [
    id 33
    label "&#322;atwo"
    origin "text"
  ]
  node [
    id 34
    label "zidentyfikowa&#263;"
    origin "text"
  ]
  node [
    id 35
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "opisa&#263;"
    origin "text"
  ]
  node [
    id 37
    label "dzia&#322;anie"
    origin "text"
  ]
  node [
    id 38
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 39
    label "schemat"
    origin "text"
  ]
  node [
    id 40
    label "w&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 41
    label "dla"
    origin "text"
  ]
  node [
    id 42
    label "ten"
    origin "text"
  ]
  node [
    id 43
    label "metody"
    origin "text"
  ]
  node [
    id 44
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 45
    label "method"
  ]
  node [
    id 46
    label "doktryna"
  ]
  node [
    id 47
    label "model"
  ]
  node [
    id 48
    label "narz&#281;dzie"
  ]
  node [
    id 49
    label "zbi&#243;r"
  ]
  node [
    id 50
    label "tryb"
  ]
  node [
    id 51
    label "nature"
  ]
  node [
    id 52
    label "strategia"
  ]
  node [
    id 53
    label "teoria"
  ]
  node [
    id 54
    label "doctrine"
  ]
  node [
    id 55
    label "pomierny"
  ]
  node [
    id 56
    label "mierny"
  ]
  node [
    id 57
    label "&#347;redni"
  ]
  node [
    id 58
    label "lichy"
  ]
  node [
    id 59
    label "pomiernie"
  ]
  node [
    id 60
    label "niski"
  ]
  node [
    id 61
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 62
    label "mie&#263;_miejsce"
  ]
  node [
    id 63
    label "equal"
  ]
  node [
    id 64
    label "trwa&#263;"
  ]
  node [
    id 65
    label "chodzi&#263;"
  ]
  node [
    id 66
    label "si&#281;ga&#263;"
  ]
  node [
    id 67
    label "stan"
  ]
  node [
    id 68
    label "obecno&#347;&#263;"
  ]
  node [
    id 69
    label "stand"
  ]
  node [
    id 70
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 71
    label "uczestniczy&#263;"
  ]
  node [
    id 72
    label "participate"
  ]
  node [
    id 73
    label "robi&#263;"
  ]
  node [
    id 74
    label "istnie&#263;"
  ]
  node [
    id 75
    label "pozostawa&#263;"
  ]
  node [
    id 76
    label "zostawa&#263;"
  ]
  node [
    id 77
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 78
    label "adhere"
  ]
  node [
    id 79
    label "compass"
  ]
  node [
    id 80
    label "korzysta&#263;"
  ]
  node [
    id 81
    label "appreciation"
  ]
  node [
    id 82
    label "osi&#261;ga&#263;"
  ]
  node [
    id 83
    label "dociera&#263;"
  ]
  node [
    id 84
    label "get"
  ]
  node [
    id 85
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 86
    label "u&#380;ywa&#263;"
  ]
  node [
    id 87
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 88
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 89
    label "exsert"
  ]
  node [
    id 90
    label "being"
  ]
  node [
    id 91
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 92
    label "cecha"
  ]
  node [
    id 93
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 94
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 95
    label "p&#322;ywa&#263;"
  ]
  node [
    id 96
    label "run"
  ]
  node [
    id 97
    label "bangla&#263;"
  ]
  node [
    id 98
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 99
    label "przebiega&#263;"
  ]
  node [
    id 100
    label "wk&#322;ada&#263;"
  ]
  node [
    id 101
    label "proceed"
  ]
  node [
    id 102
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 103
    label "carry"
  ]
  node [
    id 104
    label "bywa&#263;"
  ]
  node [
    id 105
    label "dziama&#263;"
  ]
  node [
    id 106
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 107
    label "stara&#263;_si&#281;"
  ]
  node [
    id 108
    label "para"
  ]
  node [
    id 109
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 110
    label "str&#243;j"
  ]
  node [
    id 111
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 112
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 113
    label "krok"
  ]
  node [
    id 114
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 115
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 116
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 117
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 118
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 119
    label "continue"
  ]
  node [
    id 120
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 121
    label "Ohio"
  ]
  node [
    id 122
    label "wci&#281;cie"
  ]
  node [
    id 123
    label "Nowy_York"
  ]
  node [
    id 124
    label "warstwa"
  ]
  node [
    id 125
    label "samopoczucie"
  ]
  node [
    id 126
    label "Illinois"
  ]
  node [
    id 127
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 128
    label "state"
  ]
  node [
    id 129
    label "Jukatan"
  ]
  node [
    id 130
    label "Kalifornia"
  ]
  node [
    id 131
    label "Wirginia"
  ]
  node [
    id 132
    label "wektor"
  ]
  node [
    id 133
    label "Teksas"
  ]
  node [
    id 134
    label "Goa"
  ]
  node [
    id 135
    label "Waszyngton"
  ]
  node [
    id 136
    label "miejsce"
  ]
  node [
    id 137
    label "Massachusetts"
  ]
  node [
    id 138
    label "Alaska"
  ]
  node [
    id 139
    label "Arakan"
  ]
  node [
    id 140
    label "Hawaje"
  ]
  node [
    id 141
    label "Maryland"
  ]
  node [
    id 142
    label "punkt"
  ]
  node [
    id 143
    label "Michigan"
  ]
  node [
    id 144
    label "Arizona"
  ]
  node [
    id 145
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 146
    label "Georgia"
  ]
  node [
    id 147
    label "poziom"
  ]
  node [
    id 148
    label "Pensylwania"
  ]
  node [
    id 149
    label "shape"
  ]
  node [
    id 150
    label "Luizjana"
  ]
  node [
    id 151
    label "Nowy_Meksyk"
  ]
  node [
    id 152
    label "Alabama"
  ]
  node [
    id 153
    label "ilo&#347;&#263;"
  ]
  node [
    id 154
    label "Kansas"
  ]
  node [
    id 155
    label "Oregon"
  ]
  node [
    id 156
    label "Floryda"
  ]
  node [
    id 157
    label "Oklahoma"
  ]
  node [
    id 158
    label "jednostka_administracyjna"
  ]
  node [
    id 159
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 160
    label "egzemplarz"
  ]
  node [
    id 161
    label "series"
  ]
  node [
    id 162
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 163
    label "uprawianie"
  ]
  node [
    id 164
    label "praca_rolnicza"
  ]
  node [
    id 165
    label "collection"
  ]
  node [
    id 166
    label "dane"
  ]
  node [
    id 167
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 168
    label "pakiet_klimatyczny"
  ]
  node [
    id 169
    label "poj&#281;cie"
  ]
  node [
    id 170
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 171
    label "sum"
  ]
  node [
    id 172
    label "gathering"
  ]
  node [
    id 173
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 174
    label "album"
  ]
  node [
    id 175
    label "&#347;rodek"
  ]
  node [
    id 176
    label "niezb&#281;dnik"
  ]
  node [
    id 177
    label "przedmiot"
  ]
  node [
    id 178
    label "cz&#322;owiek"
  ]
  node [
    id 179
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 180
    label "tylec"
  ]
  node [
    id 181
    label "urz&#261;dzenie"
  ]
  node [
    id 182
    label "ko&#322;o"
  ]
  node [
    id 183
    label "modalno&#347;&#263;"
  ]
  node [
    id 184
    label "z&#261;b"
  ]
  node [
    id 185
    label "kategoria_gramatyczna"
  ]
  node [
    id 186
    label "skala"
  ]
  node [
    id 187
    label "funkcjonowa&#263;"
  ]
  node [
    id 188
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 189
    label "koniugacja"
  ]
  node [
    id 190
    label "prezenter"
  ]
  node [
    id 191
    label "typ"
  ]
  node [
    id 192
    label "mildew"
  ]
  node [
    id 193
    label "zi&#243;&#322;ko"
  ]
  node [
    id 194
    label "motif"
  ]
  node [
    id 195
    label "pozowanie"
  ]
  node [
    id 196
    label "ideal"
  ]
  node [
    id 197
    label "wz&#243;r"
  ]
  node [
    id 198
    label "matryca"
  ]
  node [
    id 199
    label "adaptation"
  ]
  node [
    id 200
    label "ruch"
  ]
  node [
    id 201
    label "pozowa&#263;"
  ]
  node [
    id 202
    label "imitacja"
  ]
  node [
    id 203
    label "orygina&#322;"
  ]
  node [
    id 204
    label "facet"
  ]
  node [
    id 205
    label "miniatura"
  ]
  node [
    id 206
    label "analizowa&#263;"
  ]
  node [
    id 207
    label "szacowa&#263;"
  ]
  node [
    id 208
    label "consider"
  ]
  node [
    id 209
    label "badany"
  ]
  node [
    id 210
    label "poddawa&#263;"
  ]
  node [
    id 211
    label "bada&#263;"
  ]
  node [
    id 212
    label "rozpatrywa&#263;"
  ]
  node [
    id 213
    label "gauge"
  ]
  node [
    id 214
    label "okre&#347;la&#263;"
  ]
  node [
    id 215
    label "liczy&#263;"
  ]
  node [
    id 216
    label "warunek_lokalowy"
  ]
  node [
    id 217
    label "rozmiar"
  ]
  node [
    id 218
    label "liczba"
  ]
  node [
    id 219
    label "rzadko&#347;&#263;"
  ]
  node [
    id 220
    label "zaleta"
  ]
  node [
    id 221
    label "measure"
  ]
  node [
    id 222
    label "znaczenie"
  ]
  node [
    id 223
    label "opinia"
  ]
  node [
    id 224
    label "dymensja"
  ]
  node [
    id 225
    label "zdolno&#347;&#263;"
  ]
  node [
    id 226
    label "potencja"
  ]
  node [
    id 227
    label "property"
  ]
  node [
    id 228
    label "pos&#322;uchanie"
  ]
  node [
    id 229
    label "skumanie"
  ]
  node [
    id 230
    label "orientacja"
  ]
  node [
    id 231
    label "wytw&#243;r"
  ]
  node [
    id 232
    label "zorientowanie"
  ]
  node [
    id 233
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 234
    label "clasp"
  ]
  node [
    id 235
    label "forma"
  ]
  node [
    id 236
    label "przem&#243;wienie"
  ]
  node [
    id 237
    label "g&#281;sto&#347;&#263;"
  ]
  node [
    id 238
    label "struktura"
  ]
  node [
    id 239
    label "frekwencja"
  ]
  node [
    id 240
    label "niezwyk&#322;o&#347;&#263;"
  ]
  node [
    id 241
    label "rozmieszczenie"
  ]
  node [
    id 242
    label "warto&#347;&#263;"
  ]
  node [
    id 243
    label "charakterystyka"
  ]
  node [
    id 244
    label "feature"
  ]
  node [
    id 245
    label "wyregulowanie"
  ]
  node [
    id 246
    label "kompetencja"
  ]
  node [
    id 247
    label "wyregulowa&#263;"
  ]
  node [
    id 248
    label "regulowanie"
  ]
  node [
    id 249
    label "regulowa&#263;"
  ]
  node [
    id 250
    label "attribute"
  ]
  node [
    id 251
    label "standard"
  ]
  node [
    id 252
    label "posiada&#263;"
  ]
  node [
    id 253
    label "potencja&#322;"
  ]
  node [
    id 254
    label "zapomnienie"
  ]
  node [
    id 255
    label "zapomina&#263;"
  ]
  node [
    id 256
    label "zapominanie"
  ]
  node [
    id 257
    label "ability"
  ]
  node [
    id 258
    label "obliczeniowo"
  ]
  node [
    id 259
    label "zapomnie&#263;"
  ]
  node [
    id 260
    label "circumference"
  ]
  node [
    id 261
    label "odzie&#380;"
  ]
  node [
    id 262
    label "zrewaluowa&#263;"
  ]
  node [
    id 263
    label "rewaluowanie"
  ]
  node [
    id 264
    label "korzy&#347;&#263;"
  ]
  node [
    id 265
    label "zrewaluowanie"
  ]
  node [
    id 266
    label "rewaluowa&#263;"
  ]
  node [
    id 267
    label "wabik"
  ]
  node [
    id 268
    label "strona"
  ]
  node [
    id 269
    label "m&#322;ot"
  ]
  node [
    id 270
    label "znak"
  ]
  node [
    id 271
    label "drzewo"
  ]
  node [
    id 272
    label "pr&#243;ba"
  ]
  node [
    id 273
    label "marka"
  ]
  node [
    id 274
    label "odk&#322;adanie"
  ]
  node [
    id 275
    label "condition"
  ]
  node [
    id 276
    label "liczenie"
  ]
  node [
    id 277
    label "stawianie"
  ]
  node [
    id 278
    label "bycie"
  ]
  node [
    id 279
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 280
    label "assay"
  ]
  node [
    id 281
    label "wskazywanie"
  ]
  node [
    id 282
    label "wyraz"
  ]
  node [
    id 283
    label "gravity"
  ]
  node [
    id 284
    label "weight"
  ]
  node [
    id 285
    label "command"
  ]
  node [
    id 286
    label "odgrywanie_roli"
  ]
  node [
    id 287
    label "istota"
  ]
  node [
    id 288
    label "informacja"
  ]
  node [
    id 289
    label "okre&#347;lanie"
  ]
  node [
    id 290
    label "kto&#347;"
  ]
  node [
    id 291
    label "wyra&#380;enie"
  ]
  node [
    id 292
    label "wojsko"
  ]
  node [
    id 293
    label "potency"
  ]
  node [
    id 294
    label "byt"
  ]
  node [
    id 295
    label "si&#322;a"
  ]
  node [
    id 296
    label "tomizm"
  ]
  node [
    id 297
    label "wydolno&#347;&#263;"
  ]
  node [
    id 298
    label "arystotelizm"
  ]
  node [
    id 299
    label "gotowo&#347;&#263;"
  ]
  node [
    id 300
    label "parametr"
  ]
  node [
    id 301
    label "reputacja"
  ]
  node [
    id 302
    label "pogl&#261;d"
  ]
  node [
    id 303
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 304
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 305
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 306
    label "sofcik"
  ]
  node [
    id 307
    label "kryterium"
  ]
  node [
    id 308
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 309
    label "ekspertyza"
  ]
  node [
    id 310
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 311
    label "dokument"
  ]
  node [
    id 312
    label "appraisal"
  ]
  node [
    id 313
    label "kategoria"
  ]
  node [
    id 314
    label "pierwiastek"
  ]
  node [
    id 315
    label "number"
  ]
  node [
    id 316
    label "grupa"
  ]
  node [
    id 317
    label "kwadrat_magiczny"
  ]
  node [
    id 318
    label "part"
  ]
  node [
    id 319
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 320
    label "take"
  ]
  node [
    id 321
    label "decydowa&#263;"
  ]
  node [
    id 322
    label "signify"
  ]
  node [
    id 323
    label "style"
  ]
  node [
    id 324
    label "powodowa&#263;"
  ]
  node [
    id 325
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 326
    label "sprawdza&#263;"
  ]
  node [
    id 327
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 328
    label "feel"
  ]
  node [
    id 329
    label "try"
  ]
  node [
    id 330
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 331
    label "kosztowa&#263;"
  ]
  node [
    id 332
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 333
    label "modelowy"
  ]
  node [
    id 334
    label "wzorcowo"
  ]
  node [
    id 335
    label "typowy"
  ]
  node [
    id 336
    label "doskona&#322;y"
  ]
  node [
    id 337
    label "pr&#243;bny"
  ]
  node [
    id 338
    label "modelowo"
  ]
  node [
    id 339
    label "specjalny"
  ]
  node [
    id 340
    label "follow-up"
  ]
  node [
    id 341
    label "term"
  ]
  node [
    id 342
    label "ustalenie"
  ]
  node [
    id 343
    label "appointment"
  ]
  node [
    id 344
    label "localization"
  ]
  node [
    id 345
    label "ozdobnik"
  ]
  node [
    id 346
    label "denomination"
  ]
  node [
    id 347
    label "zdecydowanie"
  ]
  node [
    id 348
    label "przewidzenie"
  ]
  node [
    id 349
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 350
    label "decyzja"
  ]
  node [
    id 351
    label "pewnie"
  ]
  node [
    id 352
    label "zdecydowany"
  ]
  node [
    id 353
    label "zauwa&#380;alnie"
  ]
  node [
    id 354
    label "oddzia&#322;anie"
  ]
  node [
    id 355
    label "podj&#281;cie"
  ]
  node [
    id 356
    label "resoluteness"
  ]
  node [
    id 357
    label "judgment"
  ]
  node [
    id 358
    label "zrobienie"
  ]
  node [
    id 359
    label "leksem"
  ]
  node [
    id 360
    label "sformu&#322;owanie"
  ]
  node [
    id 361
    label "zdarzenie_si&#281;"
  ]
  node [
    id 362
    label "poinformowanie"
  ]
  node [
    id 363
    label "wording"
  ]
  node [
    id 364
    label "kompozycja"
  ]
  node [
    id 365
    label "oznaczenie"
  ]
  node [
    id 366
    label "znak_j&#281;zykowy"
  ]
  node [
    id 367
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 368
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 369
    label "grupa_imienna"
  ]
  node [
    id 370
    label "jednostka_leksykalna"
  ]
  node [
    id 371
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 372
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 373
    label "ujawnienie"
  ]
  node [
    id 374
    label "affirmation"
  ]
  node [
    id 375
    label "zapisanie"
  ]
  node [
    id 376
    label "rzucenie"
  ]
  node [
    id 377
    label "umocnienie"
  ]
  node [
    id 378
    label "spowodowanie"
  ]
  node [
    id 379
    label "czynno&#347;&#263;"
  ]
  node [
    id 380
    label "obliczenie"
  ]
  node [
    id 381
    label "spodziewanie_si&#281;"
  ]
  node [
    id 382
    label "zaplanowanie"
  ]
  node [
    id 383
    label "vision"
  ]
  node [
    id 384
    label "dekor"
  ]
  node [
    id 385
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 386
    label "wypowied&#378;"
  ]
  node [
    id 387
    label "ornamentyka"
  ]
  node [
    id 388
    label "ilustracja"
  ]
  node [
    id 389
    label "d&#378;wi&#281;k"
  ]
  node [
    id 390
    label "dekoracja"
  ]
  node [
    id 391
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 392
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 393
    label "appear"
  ]
  node [
    id 394
    label "rise"
  ]
  node [
    id 395
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 396
    label "usamodzielnienie"
  ]
  node [
    id 397
    label "usamodzielnianie"
  ]
  node [
    id 398
    label "niezale&#380;nie"
  ]
  node [
    id 399
    label "uwolnienie"
  ]
  node [
    id 400
    label "emancipation"
  ]
  node [
    id 401
    label "uwalnianie"
  ]
  node [
    id 402
    label "rodzina"
  ]
  node [
    id 403
    label "fashion"
  ]
  node [
    id 404
    label "jednostka_systematyczna"
  ]
  node [
    id 405
    label "autorament"
  ]
  node [
    id 406
    label "variety"
  ]
  node [
    id 407
    label "pob&#243;r"
  ]
  node [
    id 408
    label "type"
  ]
  node [
    id 409
    label "powinowaci"
  ]
  node [
    id 410
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 411
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 412
    label "rodze&#324;stwo"
  ]
  node [
    id 413
    label "krewni"
  ]
  node [
    id 414
    label "Ossoli&#324;scy"
  ]
  node [
    id 415
    label "potomstwo"
  ]
  node [
    id 416
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 417
    label "theater"
  ]
  node [
    id 418
    label "Soplicowie"
  ]
  node [
    id 419
    label "kin"
  ]
  node [
    id 420
    label "family"
  ]
  node [
    id 421
    label "rodzice"
  ]
  node [
    id 422
    label "ordynacja"
  ]
  node [
    id 423
    label "dom_rodzinny"
  ]
  node [
    id 424
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 425
    label "Ostrogscy"
  ]
  node [
    id 426
    label "bliscy"
  ]
  node [
    id 427
    label "przyjaciel_domu"
  ]
  node [
    id 428
    label "dom"
  ]
  node [
    id 429
    label "rz&#261;d"
  ]
  node [
    id 430
    label "Firlejowie"
  ]
  node [
    id 431
    label "Kossakowie"
  ]
  node [
    id 432
    label "Czartoryscy"
  ]
  node [
    id 433
    label "Sapiehowie"
  ]
  node [
    id 434
    label "proces"
  ]
  node [
    id 435
    label "boski"
  ]
  node [
    id 436
    label "krajobraz"
  ]
  node [
    id 437
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 438
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 439
    label "przywidzenie"
  ]
  node [
    id 440
    label "presence"
  ]
  node [
    id 441
    label "charakter"
  ]
  node [
    id 442
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 443
    label "jako&#347;&#263;"
  ]
  node [
    id 444
    label "wygl&#261;d"
  ]
  node [
    id 445
    label "gust"
  ]
  node [
    id 446
    label "drobiazg"
  ]
  node [
    id 447
    label "kobieta"
  ]
  node [
    id 448
    label "beauty"
  ]
  node [
    id 449
    label "kalokagatia"
  ]
  node [
    id 450
    label "prettiness"
  ]
  node [
    id 451
    label "ozdoba"
  ]
  node [
    id 452
    label "nadprzyrodzony"
  ]
  node [
    id 453
    label "&#347;mieszny"
  ]
  node [
    id 454
    label "wspania&#322;y"
  ]
  node [
    id 455
    label "bezpretensjonalny"
  ]
  node [
    id 456
    label "bosko"
  ]
  node [
    id 457
    label "nale&#380;ny"
  ]
  node [
    id 458
    label "wyj&#261;tkowy"
  ]
  node [
    id 459
    label "fantastyczny"
  ]
  node [
    id 460
    label "arcypi&#281;kny"
  ]
  node [
    id 461
    label "cudnie"
  ]
  node [
    id 462
    label "cudowny"
  ]
  node [
    id 463
    label "udany"
  ]
  node [
    id 464
    label "wielki"
  ]
  node [
    id 465
    label "teren"
  ]
  node [
    id 466
    label "przestrze&#324;"
  ]
  node [
    id 467
    label "human_body"
  ]
  node [
    id 468
    label "obszar"
  ]
  node [
    id 469
    label "dzie&#322;o"
  ]
  node [
    id 470
    label "przyroda"
  ]
  node [
    id 471
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 472
    label "obraz"
  ]
  node [
    id 473
    label "widok"
  ]
  node [
    id 474
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 475
    label "zaj&#347;cie"
  ]
  node [
    id 476
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 477
    label "kontekst"
  ]
  node [
    id 478
    label "message"
  ]
  node [
    id 479
    label "&#347;wiat"
  ]
  node [
    id 480
    label "dar"
  ]
  node [
    id 481
    label "real"
  ]
  node [
    id 482
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 483
    label "wydarzenie"
  ]
  node [
    id 484
    label "osobowo&#347;&#263;"
  ]
  node [
    id 485
    label "psychika"
  ]
  node [
    id 486
    label "posta&#263;"
  ]
  node [
    id 487
    label "kompleksja"
  ]
  node [
    id 488
    label "fizjonomia"
  ]
  node [
    id 489
    label "entity"
  ]
  node [
    id 490
    label "kognicja"
  ]
  node [
    id 491
    label "przebieg"
  ]
  node [
    id 492
    label "rozprawa"
  ]
  node [
    id 493
    label "legislacyjnie"
  ]
  node [
    id 494
    label "przes&#322;anka"
  ]
  node [
    id 495
    label "nast&#281;pstwo"
  ]
  node [
    id 496
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 497
    label "z&#322;uda"
  ]
  node [
    id 498
    label "sojourn"
  ]
  node [
    id 499
    label "z&#322;udzenie"
  ]
  node [
    id 500
    label "pracownik"
  ]
  node [
    id 501
    label "fizykalnie"
  ]
  node [
    id 502
    label "materializowanie"
  ]
  node [
    id 503
    label "fizycznie"
  ]
  node [
    id 504
    label "namacalny"
  ]
  node [
    id 505
    label "widoczny"
  ]
  node [
    id 506
    label "zmaterializowanie"
  ]
  node [
    id 507
    label "organiczny"
  ]
  node [
    id 508
    label "materjalny"
  ]
  node [
    id 509
    label "gimnastyczny"
  ]
  node [
    id 510
    label "po_newtonowsku"
  ]
  node [
    id 511
    label "forcibly"
  ]
  node [
    id 512
    label "fizykalny"
  ]
  node [
    id 513
    label "physically"
  ]
  node [
    id 514
    label "namacalnie"
  ]
  node [
    id 515
    label "powodowanie"
  ]
  node [
    id 516
    label "wyjrzenie"
  ]
  node [
    id 517
    label "wygl&#261;danie"
  ]
  node [
    id 518
    label "widny"
  ]
  node [
    id 519
    label "widomy"
  ]
  node [
    id 520
    label "pojawianie_si&#281;"
  ]
  node [
    id 521
    label "widocznie"
  ]
  node [
    id 522
    label "wyra&#378;ny"
  ]
  node [
    id 523
    label "widzialny"
  ]
  node [
    id 524
    label "wystawienie_si&#281;"
  ]
  node [
    id 525
    label "widnienie"
  ]
  node [
    id 526
    label "ods&#322;oni&#281;cie"
  ]
  node [
    id 527
    label "ods&#322;anianie"
  ]
  node [
    id 528
    label "zarysowanie_si&#281;"
  ]
  node [
    id 529
    label "dostrzegalny"
  ]
  node [
    id 530
    label "wystawianie_si&#281;"
  ]
  node [
    id 531
    label "finansowy"
  ]
  node [
    id 532
    label "materialny"
  ]
  node [
    id 533
    label "naturalny"
  ]
  node [
    id 534
    label "nieodparty"
  ]
  node [
    id 535
    label "na&#347;ladowczy"
  ]
  node [
    id 536
    label "organicznie"
  ]
  node [
    id 537
    label "podobny"
  ]
  node [
    id 538
    label "trwa&#322;y"
  ]
  node [
    id 539
    label "salariat"
  ]
  node [
    id 540
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 541
    label "delegowanie"
  ]
  node [
    id 542
    label "pracu&#347;"
  ]
  node [
    id 543
    label "r&#281;ka"
  ]
  node [
    id 544
    label "delegowa&#263;"
  ]
  node [
    id 545
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 546
    label "postrzegalny"
  ]
  node [
    id 547
    label "konkretny"
  ]
  node [
    id 548
    label "wiarygodny"
  ]
  node [
    id 549
    label "liga&#263;"
  ]
  node [
    id 550
    label "give"
  ]
  node [
    id 551
    label "distribute"
  ]
  node [
    id 552
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 553
    label "use"
  ]
  node [
    id 554
    label "krzywdzi&#263;"
  ]
  node [
    id 555
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 556
    label "bash"
  ]
  node [
    id 557
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 558
    label "doznawa&#263;"
  ]
  node [
    id 559
    label "uzyskiwa&#263;"
  ]
  node [
    id 560
    label "copulate"
  ]
  node [
    id 561
    label "&#380;y&#263;"
  ]
  node [
    id 562
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 563
    label "ukrzywdza&#263;"
  ]
  node [
    id 564
    label "niesprawiedliwy"
  ]
  node [
    id 565
    label "szkodzi&#263;"
  ]
  node [
    id 566
    label "wrong"
  ]
  node [
    id 567
    label "wierzga&#263;"
  ]
  node [
    id 568
    label "utensylia"
  ]
  node [
    id 569
    label "equipment"
  ]
  node [
    id 570
    label "zapoznawa&#263;"
  ]
  node [
    id 571
    label "represent"
  ]
  node [
    id 572
    label "zawiera&#263;"
  ]
  node [
    id 573
    label "poznawa&#263;"
  ]
  node [
    id 574
    label "obznajamia&#263;"
  ]
  node [
    id 575
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 576
    label "go_steady"
  ]
  node [
    id 577
    label "informowa&#263;"
  ]
  node [
    id 578
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 579
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 580
    label "regu&#322;a_Allena"
  ]
  node [
    id 581
    label "base"
  ]
  node [
    id 582
    label "umowa"
  ]
  node [
    id 583
    label "obserwacja"
  ]
  node [
    id 584
    label "zasada_d'Alemberta"
  ]
  node [
    id 585
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 586
    label "normalizacja"
  ]
  node [
    id 587
    label "moralno&#347;&#263;"
  ]
  node [
    id 588
    label "criterion"
  ]
  node [
    id 589
    label "opis"
  ]
  node [
    id 590
    label "regu&#322;a_Glogera"
  ]
  node [
    id 591
    label "prawo_Mendla"
  ]
  node [
    id 592
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 593
    label "twierdzenie"
  ]
  node [
    id 594
    label "prawo"
  ]
  node [
    id 595
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 596
    label "qualification"
  ]
  node [
    id 597
    label "dominion"
  ]
  node [
    id 598
    label "occupation"
  ]
  node [
    id 599
    label "podstawa"
  ]
  node [
    id 600
    label "substancja"
  ]
  node [
    id 601
    label "prawid&#322;o"
  ]
  node [
    id 602
    label "dobro&#263;"
  ]
  node [
    id 603
    label "aretologia"
  ]
  node [
    id 604
    label "zesp&#243;&#322;"
  ]
  node [
    id 605
    label "morality"
  ]
  node [
    id 606
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 607
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 608
    label "honesty"
  ]
  node [
    id 609
    label "organizowa&#263;"
  ]
  node [
    id 610
    label "ordinariness"
  ]
  node [
    id 611
    label "instytucja"
  ]
  node [
    id 612
    label "zorganizowa&#263;"
  ]
  node [
    id 613
    label "taniec_towarzyski"
  ]
  node [
    id 614
    label "organizowanie"
  ]
  node [
    id 615
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 616
    label "zorganizowanie"
  ]
  node [
    id 617
    label "exposition"
  ]
  node [
    id 618
    label "obja&#347;nienie"
  ]
  node [
    id 619
    label "zawarcie"
  ]
  node [
    id 620
    label "zawrze&#263;"
  ]
  node [
    id 621
    label "czyn"
  ]
  node [
    id 622
    label "warunek"
  ]
  node [
    id 623
    label "gestia_transportowa"
  ]
  node [
    id 624
    label "contract"
  ]
  node [
    id 625
    label "porozumienie"
  ]
  node [
    id 626
    label "klauzula"
  ]
  node [
    id 627
    label "przenikanie"
  ]
  node [
    id 628
    label "materia"
  ]
  node [
    id 629
    label "cz&#261;steczka"
  ]
  node [
    id 630
    label "temperatura_krytyczna"
  ]
  node [
    id 631
    label "przenika&#263;"
  ]
  node [
    id 632
    label "smolisty"
  ]
  node [
    id 633
    label "pot&#281;ga"
  ]
  node [
    id 634
    label "documentation"
  ]
  node [
    id 635
    label "column"
  ]
  node [
    id 636
    label "zasadzi&#263;"
  ]
  node [
    id 637
    label "za&#322;o&#380;enie"
  ]
  node [
    id 638
    label "punkt_odniesienia"
  ]
  node [
    id 639
    label "zasadzenie"
  ]
  node [
    id 640
    label "bok"
  ]
  node [
    id 641
    label "d&#243;&#322;"
  ]
  node [
    id 642
    label "dzieci&#281;ctwo"
  ]
  node [
    id 643
    label "background"
  ]
  node [
    id 644
    label "podstawowy"
  ]
  node [
    id 645
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 646
    label "pomys&#322;"
  ]
  node [
    id 647
    label "&#347;ciana"
  ]
  node [
    id 648
    label "shoetree"
  ]
  node [
    id 649
    label "badanie"
  ]
  node [
    id 650
    label "proces_my&#347;lowy"
  ]
  node [
    id 651
    label "remark"
  ]
  node [
    id 652
    label "stwierdzenie"
  ]
  node [
    id 653
    label "observation"
  ]
  node [
    id 654
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 655
    label "alternatywa_Fredholma"
  ]
  node [
    id 656
    label "oznajmianie"
  ]
  node [
    id 657
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 658
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 659
    label "paradoks_Leontiefa"
  ]
  node [
    id 660
    label "s&#261;d"
  ]
  node [
    id 661
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 662
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 663
    label "teza"
  ]
  node [
    id 664
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 665
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 666
    label "twierdzenie_Pettisa"
  ]
  node [
    id 667
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 668
    label "twierdzenie_Maya"
  ]
  node [
    id 669
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 670
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 671
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 672
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 673
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 674
    label "zapewnianie"
  ]
  node [
    id 675
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 676
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 677
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 678
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 679
    label "twierdzenie_Stokesa"
  ]
  node [
    id 680
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 681
    label "twierdzenie_Cevy"
  ]
  node [
    id 682
    label "twierdzenie_Pascala"
  ]
  node [
    id 683
    label "proposition"
  ]
  node [
    id 684
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 685
    label "komunikowanie"
  ]
  node [
    id 686
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 687
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 688
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 689
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 690
    label "relacja"
  ]
  node [
    id 691
    label "calibration"
  ]
  node [
    id 692
    label "operacja"
  ]
  node [
    id 693
    label "porz&#261;dek"
  ]
  node [
    id 694
    label "dominance"
  ]
  node [
    id 695
    label "zabieg"
  ]
  node [
    id 696
    label "standardization"
  ]
  node [
    id 697
    label "zmiana"
  ]
  node [
    id 698
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 699
    label "umocowa&#263;"
  ]
  node [
    id 700
    label "procesualistyka"
  ]
  node [
    id 701
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 702
    label "kryminalistyka"
  ]
  node [
    id 703
    label "szko&#322;a"
  ]
  node [
    id 704
    label "kierunek"
  ]
  node [
    id 705
    label "normatywizm"
  ]
  node [
    id 706
    label "jurisprudence"
  ]
  node [
    id 707
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 708
    label "kultura_duchowa"
  ]
  node [
    id 709
    label "przepis"
  ]
  node [
    id 710
    label "prawo_karne_procesowe"
  ]
  node [
    id 711
    label "kazuistyka"
  ]
  node [
    id 712
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 713
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 714
    label "kryminologia"
  ]
  node [
    id 715
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 716
    label "prawo_karne"
  ]
  node [
    id 717
    label "cywilistyka"
  ]
  node [
    id 718
    label "judykatura"
  ]
  node [
    id 719
    label "kanonistyka"
  ]
  node [
    id 720
    label "nauka_prawa"
  ]
  node [
    id 721
    label "podmiot"
  ]
  node [
    id 722
    label "law"
  ]
  node [
    id 723
    label "wykonawczy"
  ]
  node [
    id 724
    label "wezwanie"
  ]
  node [
    id 725
    label "patron"
  ]
  node [
    id 726
    label "wordnet"
  ]
  node [
    id 727
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 728
    label "wypowiedzenie"
  ]
  node [
    id 729
    label "morfem"
  ]
  node [
    id 730
    label "s&#322;ownictwo"
  ]
  node [
    id 731
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 732
    label "wykrzyknik"
  ]
  node [
    id 733
    label "pole_semantyczne"
  ]
  node [
    id 734
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 735
    label "pisanie_si&#281;"
  ]
  node [
    id 736
    label "nag&#322;os"
  ]
  node [
    id 737
    label "wyg&#322;os"
  ]
  node [
    id 738
    label "nakaz"
  ]
  node [
    id 739
    label "pismo_urz&#281;dowe"
  ]
  node [
    id 740
    label "zwo&#322;anie_si&#281;"
  ]
  node [
    id 741
    label "wst&#281;p"
  ]
  node [
    id 742
    label "pro&#347;ba"
  ]
  node [
    id 743
    label "nakazanie"
  ]
  node [
    id 744
    label "admonition"
  ]
  node [
    id 745
    label "summons"
  ]
  node [
    id 746
    label "poproszenie"
  ]
  node [
    id 747
    label "bid"
  ]
  node [
    id 748
    label "apostrofa"
  ]
  node [
    id 749
    label "zach&#281;cenie"
  ]
  node [
    id 750
    label "&#322;uska"
  ]
  node [
    id 751
    label "opiekun"
  ]
  node [
    id 752
    label "patrycjusz"
  ]
  node [
    id 753
    label "prawnik"
  ]
  node [
    id 754
    label "nab&#243;j"
  ]
  node [
    id 755
    label "&#347;wi&#281;ty"
  ]
  node [
    id 756
    label "zmar&#322;y"
  ]
  node [
    id 757
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 758
    label "&#347;w"
  ]
  node [
    id 759
    label "szablon"
  ]
  node [
    id 760
    label "pr&#243;bowanie"
  ]
  node [
    id 761
    label "zademonstrowanie"
  ]
  node [
    id 762
    label "report"
  ]
  node [
    id 763
    label "obgadanie"
  ]
  node [
    id 764
    label "realizacja"
  ]
  node [
    id 765
    label "scena"
  ]
  node [
    id 766
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 767
    label "narration"
  ]
  node [
    id 768
    label "cyrk"
  ]
  node [
    id 769
    label "theatrical_performance"
  ]
  node [
    id 770
    label "opisanie"
  ]
  node [
    id 771
    label "malarstwo"
  ]
  node [
    id 772
    label "scenografia"
  ]
  node [
    id 773
    label "teatr"
  ]
  node [
    id 774
    label "ukazanie"
  ]
  node [
    id 775
    label "zapoznanie"
  ]
  node [
    id 776
    label "pokaz"
  ]
  node [
    id 777
    label "podanie"
  ]
  node [
    id 778
    label "ods&#322;ona"
  ]
  node [
    id 779
    label "exhibit"
  ]
  node [
    id 780
    label "pokazanie"
  ]
  node [
    id 781
    label "wyst&#261;pienie"
  ]
  node [
    id 782
    label "przedstawi&#263;"
  ]
  node [
    id 783
    label "przedstawianie"
  ]
  node [
    id 784
    label "przedstawia&#263;"
  ]
  node [
    id 785
    label "rola"
  ]
  node [
    id 786
    label "wolty&#380;erka"
  ]
  node [
    id 787
    label "repryza"
  ]
  node [
    id 788
    label "ekwilibrystyka"
  ]
  node [
    id 789
    label "tresura"
  ]
  node [
    id 790
    label "nied&#378;wiednik"
  ]
  node [
    id 791
    label "skandal"
  ]
  node [
    id 792
    label "hipodrom"
  ]
  node [
    id 793
    label "namiot"
  ]
  node [
    id 794
    label "budynek"
  ]
  node [
    id 795
    label "circus"
  ]
  node [
    id 796
    label "heca"
  ]
  node [
    id 797
    label "arena"
  ]
  node [
    id 798
    label "klownada"
  ]
  node [
    id 799
    label "akrobacja"
  ]
  node [
    id 800
    label "amfiteatr"
  ]
  node [
    id 801
    label "trybuna"
  ]
  node [
    id 802
    label "portrayal"
  ]
  node [
    id 803
    label "figura_my&#347;li"
  ]
  node [
    id 804
    label "zinterpretowanie"
  ]
  node [
    id 805
    label "portrait"
  ]
  node [
    id 806
    label "p&#322;&#243;d"
  ]
  node [
    id 807
    label "work"
  ]
  node [
    id 808
    label "rezultat"
  ]
  node [
    id 809
    label "ustawienie"
  ]
  node [
    id 810
    label "danie"
  ]
  node [
    id 811
    label "narrative"
  ]
  node [
    id 812
    label "pismo"
  ]
  node [
    id 813
    label "nafaszerowanie"
  ]
  node [
    id 814
    label "tenis"
  ]
  node [
    id 815
    label "prayer"
  ]
  node [
    id 816
    label "siatk&#243;wka"
  ]
  node [
    id 817
    label "pi&#322;ka"
  ]
  node [
    id 818
    label "myth"
  ]
  node [
    id 819
    label "service"
  ]
  node [
    id 820
    label "jedzenie"
  ]
  node [
    id 821
    label "zagranie"
  ]
  node [
    id 822
    label "zaserwowanie"
  ]
  node [
    id 823
    label "opowie&#347;&#263;"
  ]
  node [
    id 824
    label "pass"
  ]
  node [
    id 825
    label "udowodnienie"
  ]
  node [
    id 826
    label "obniesienie"
  ]
  node [
    id 827
    label "meaning"
  ]
  node [
    id 828
    label "nauczenie"
  ]
  node [
    id 829
    label "wczytanie"
  ]
  node [
    id 830
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 831
    label "representation"
  ]
  node [
    id 832
    label "znajomy"
  ]
  node [
    id 833
    label "obznajomienie"
  ]
  node [
    id 834
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 835
    label "umo&#380;liwienie"
  ]
  node [
    id 836
    label "knowing"
  ]
  node [
    id 837
    label "nak&#322;onienie"
  ]
  node [
    id 838
    label "case"
  ]
  node [
    id 839
    label "odst&#261;pienie"
  ]
  node [
    id 840
    label "naznaczenie"
  ]
  node [
    id 841
    label "wyst&#281;p"
  ]
  node [
    id 842
    label "happening"
  ]
  node [
    id 843
    label "porobienie_si&#281;"
  ]
  node [
    id 844
    label "wyj&#347;cie"
  ]
  node [
    id 845
    label "zrezygnowanie"
  ]
  node [
    id 846
    label "pojawienie_si&#281;"
  ]
  node [
    id 847
    label "appearance"
  ]
  node [
    id 848
    label "egress"
  ]
  node [
    id 849
    label "zacz&#281;cie"
  ]
  node [
    id 850
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 851
    label "event"
  ]
  node [
    id 852
    label "performance"
  ]
  node [
    id 853
    label "exit"
  ]
  node [
    id 854
    label "przepisanie_si&#281;"
  ]
  node [
    id 855
    label "fabrication"
  ]
  node [
    id 856
    label "scheduling"
  ]
  node [
    id 857
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 858
    label "kreacja"
  ]
  node [
    id 859
    label "monta&#380;"
  ]
  node [
    id 860
    label "postprodukcja"
  ]
  node [
    id 861
    label "pokaz&#243;wka"
  ]
  node [
    id 862
    label "impreza"
  ]
  node [
    id 863
    label "show"
  ]
  node [
    id 864
    label "zapoznanie_si&#281;"
  ]
  node [
    id 865
    label "podwy&#380;szenie"
  ]
  node [
    id 866
    label "kurtyna"
  ]
  node [
    id 867
    label "akt"
  ]
  node [
    id 868
    label "widzownia"
  ]
  node [
    id 869
    label "sznurownia"
  ]
  node [
    id 870
    label "dramaturgy"
  ]
  node [
    id 871
    label "sphere"
  ]
  node [
    id 872
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 873
    label "sztuka"
  ]
  node [
    id 874
    label "budka_suflera"
  ]
  node [
    id 875
    label "epizod"
  ]
  node [
    id 876
    label "film"
  ]
  node [
    id 877
    label "fragment"
  ]
  node [
    id 878
    label "k&#322;&#243;tnia"
  ]
  node [
    id 879
    label "kiesze&#324;"
  ]
  node [
    id 880
    label "stadium"
  ]
  node [
    id 881
    label "podest"
  ]
  node [
    id 882
    label "horyzont"
  ]
  node [
    id 883
    label "proscenium"
  ]
  node [
    id 884
    label "nadscenie"
  ]
  node [
    id 885
    label "antyteatr"
  ]
  node [
    id 886
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 887
    label "uprawienie"
  ]
  node [
    id 888
    label "kszta&#322;t"
  ]
  node [
    id 889
    label "dialog"
  ]
  node [
    id 890
    label "p&#322;osa"
  ]
  node [
    id 891
    label "wykonywanie"
  ]
  node [
    id 892
    label "plik"
  ]
  node [
    id 893
    label "ziemia"
  ]
  node [
    id 894
    label "wykonywa&#263;"
  ]
  node [
    id 895
    label "scenariusz"
  ]
  node [
    id 896
    label "pole"
  ]
  node [
    id 897
    label "gospodarstwo"
  ]
  node [
    id 898
    label "uprawi&#263;"
  ]
  node [
    id 899
    label "function"
  ]
  node [
    id 900
    label "zreinterpretowa&#263;"
  ]
  node [
    id 901
    label "zastosowanie"
  ]
  node [
    id 902
    label "reinterpretowa&#263;"
  ]
  node [
    id 903
    label "wrench"
  ]
  node [
    id 904
    label "irygowanie"
  ]
  node [
    id 905
    label "ustawi&#263;"
  ]
  node [
    id 906
    label "irygowa&#263;"
  ]
  node [
    id 907
    label "zreinterpretowanie"
  ]
  node [
    id 908
    label "cel"
  ]
  node [
    id 909
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 910
    label "gra&#263;"
  ]
  node [
    id 911
    label "aktorstwo"
  ]
  node [
    id 912
    label "kostium"
  ]
  node [
    id 913
    label "zagon"
  ]
  node [
    id 914
    label "zagra&#263;"
  ]
  node [
    id 915
    label "reinterpretowanie"
  ]
  node [
    id 916
    label "sk&#322;ad"
  ]
  node [
    id 917
    label "tekst"
  ]
  node [
    id 918
    label "radlina"
  ]
  node [
    id 919
    label "granie"
  ]
  node [
    id 920
    label "mansjon"
  ]
  node [
    id 921
    label "modelatornia"
  ]
  node [
    id 922
    label "zaistnie&#263;"
  ]
  node [
    id 923
    label "Osjan"
  ]
  node [
    id 924
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 925
    label "trim"
  ]
  node [
    id 926
    label "poby&#263;"
  ]
  node [
    id 927
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 928
    label "Aspazja"
  ]
  node [
    id 929
    label "punkt_widzenia"
  ]
  node [
    id 930
    label "wytrzyma&#263;"
  ]
  node [
    id 931
    label "budowa"
  ]
  node [
    id 932
    label "formacja"
  ]
  node [
    id 933
    label "pozosta&#263;"
  ]
  node [
    id 934
    label "point"
  ]
  node [
    id 935
    label "go&#347;&#263;"
  ]
  node [
    id 936
    label "play"
  ]
  node [
    id 937
    label "gra"
  ]
  node [
    id 938
    label "deski"
  ]
  node [
    id 939
    label "sala"
  ]
  node [
    id 940
    label "dekoratornia"
  ]
  node [
    id 941
    label "ukaza&#263;"
  ]
  node [
    id 942
    label "pokaza&#263;"
  ]
  node [
    id 943
    label "poda&#263;"
  ]
  node [
    id 944
    label "zapozna&#263;"
  ]
  node [
    id 945
    label "express"
  ]
  node [
    id 946
    label "zaproponowa&#263;"
  ]
  node [
    id 947
    label "zademonstrowa&#263;"
  ]
  node [
    id 948
    label "typify"
  ]
  node [
    id 949
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 950
    label "opisywanie"
  ]
  node [
    id 951
    label "obgadywanie"
  ]
  node [
    id 952
    label "zapoznawanie"
  ]
  node [
    id 953
    label "wyst&#281;powanie"
  ]
  node [
    id 954
    label "ukazywanie"
  ]
  node [
    id 955
    label "pokazywanie"
  ]
  node [
    id 956
    label "display"
  ]
  node [
    id 957
    label "podawanie"
  ]
  node [
    id 958
    label "demonstrowanie"
  ]
  node [
    id 959
    label "presentation"
  ]
  node [
    id 960
    label "medialno&#347;&#263;"
  ]
  node [
    id 961
    label "podawa&#263;"
  ]
  node [
    id 962
    label "pokazywa&#263;"
  ]
  node [
    id 963
    label "demonstrowa&#263;"
  ]
  node [
    id 964
    label "ukazywa&#263;"
  ]
  node [
    id 965
    label "zg&#322;asza&#263;"
  ]
  node [
    id 966
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 967
    label "attest"
  ]
  node [
    id 968
    label "stanowi&#263;"
  ]
  node [
    id 969
    label "podejmowanie"
  ]
  node [
    id 970
    label "usi&#322;owanie"
  ]
  node [
    id 971
    label "tasting"
  ]
  node [
    id 972
    label "kiperstwo"
  ]
  node [
    id 973
    label "staranie_si&#281;"
  ]
  node [
    id 974
    label "zaznawanie"
  ]
  node [
    id 975
    label "przygotowywanie_si&#281;"
  ]
  node [
    id 976
    label "essay"
  ]
  node [
    id 977
    label "cover"
  ]
  node [
    id 978
    label "syntetyzm"
  ]
  node [
    id 979
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 980
    label "skr&#243;t_perspektywiczny"
  ]
  node [
    id 981
    label "linearyzm"
  ]
  node [
    id 982
    label "tempera"
  ]
  node [
    id 983
    label "gwasz"
  ]
  node [
    id 984
    label "plastyka"
  ]
  node [
    id 985
    label "skrytykowanie"
  ]
  node [
    id 986
    label "temat"
  ]
  node [
    id 987
    label "smear"
  ]
  node [
    id 988
    label "discussion"
  ]
  node [
    id 989
    label "strike"
  ]
  node [
    id 990
    label "fall"
  ]
  node [
    id 991
    label "znajdowa&#263;"
  ]
  node [
    id 992
    label "happen"
  ]
  node [
    id 993
    label "styka&#263;_si&#281;"
  ]
  node [
    id 994
    label "odzyskiwa&#263;"
  ]
  node [
    id 995
    label "znachodzi&#263;"
  ]
  node [
    id 996
    label "pozyskiwa&#263;"
  ]
  node [
    id 997
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 998
    label "detect"
  ]
  node [
    id 999
    label "unwrap"
  ]
  node [
    id 1000
    label "wykrywa&#263;"
  ]
  node [
    id 1001
    label "os&#261;dza&#263;"
  ]
  node [
    id 1002
    label "wymy&#347;la&#263;"
  ]
  node [
    id 1003
    label "cognizance"
  ]
  node [
    id 1004
    label "szkoli&#263;_si&#281;"
  ]
  node [
    id 1005
    label "make"
  ]
  node [
    id 1006
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 1007
    label "hurt"
  ]
  node [
    id 1008
    label "dramat"
  ]
  node [
    id 1009
    label "pisarstwo"
  ]
  node [
    id 1010
    label "liryka"
  ]
  node [
    id 1011
    label "amorfizm"
  ]
  node [
    id 1012
    label "bibliografia"
  ]
  node [
    id 1013
    label "epika"
  ]
  node [
    id 1014
    label "translator"
  ]
  node [
    id 1015
    label "pi&#347;miennictwo"
  ]
  node [
    id 1016
    label "zoologia_fantastyczna"
  ]
  node [
    id 1017
    label "zapis"
  ]
  node [
    id 1018
    label "&#347;wiadectwo"
  ]
  node [
    id 1019
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1020
    label "parafa"
  ]
  node [
    id 1021
    label "raport&#243;wka"
  ]
  node [
    id 1022
    label "utw&#243;r"
  ]
  node [
    id 1023
    label "record"
  ]
  node [
    id 1024
    label "fascyku&#322;"
  ]
  node [
    id 1025
    label "dokumentacja"
  ]
  node [
    id 1026
    label "registratura"
  ]
  node [
    id 1027
    label "artyku&#322;"
  ]
  node [
    id 1028
    label "writing"
  ]
  node [
    id 1029
    label "sygnatariusz"
  ]
  node [
    id 1030
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1031
    label "rocznikarstwo"
  ]
  node [
    id 1032
    label "monografistyka"
  ]
  node [
    id 1033
    label "czasopi&#347;miennictwo"
  ]
  node [
    id 1034
    label "didaskalia"
  ]
  node [
    id 1035
    label "environment"
  ]
  node [
    id 1036
    label "head"
  ]
  node [
    id 1037
    label "jednostka"
  ]
  node [
    id 1038
    label "fortel"
  ]
  node [
    id 1039
    label "ambala&#380;"
  ]
  node [
    id 1040
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1041
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1042
    label "Faust"
  ]
  node [
    id 1043
    label "turn"
  ]
  node [
    id 1044
    label "Apollo"
  ]
  node [
    id 1045
    label "kultura"
  ]
  node [
    id 1046
    label "towar"
  ]
  node [
    id 1047
    label "spis"
  ]
  node [
    id 1048
    label "bibliologia"
  ]
  node [
    id 1049
    label "rodzaj_literacki"
  ]
  node [
    id 1050
    label "sielanka"
  ]
  node [
    id 1051
    label "wra&#380;liwo&#347;&#263;"
  ]
  node [
    id 1052
    label "nastrojowo&#347;&#263;"
  ]
  node [
    id 1053
    label "uczuciowo&#347;&#263;"
  ]
  node [
    id 1054
    label "waka"
  ]
  node [
    id 1055
    label "dzie&#322;o_poetyckie"
  ]
  node [
    id 1056
    label "epos"
  ]
  node [
    id 1057
    label "fantastyka"
  ]
  node [
    id 1058
    label "romans"
  ]
  node [
    id 1059
    label "nowelistyka"
  ]
  node [
    id 1060
    label "bajka"
  ]
  node [
    id 1061
    label "przypowie&#347;&#263;"
  ]
  node [
    id 1062
    label "utw&#243;r_epicki"
  ]
  node [
    id 1063
    label "drama"
  ]
  node [
    id 1064
    label "cios"
  ]
  node [
    id 1065
    label "cia&#322;o_sta&#322;e"
  ]
  node [
    id 1066
    label "organizacja"
  ]
  node [
    id 1067
    label "aplikacja"
  ]
  node [
    id 1068
    label "t&#322;umacz"
  ]
  node [
    id 1069
    label "cz&#281;sty"
  ]
  node [
    id 1070
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 1071
    label "wielokrotnie"
  ]
  node [
    id 1072
    label "sprawowa&#263;"
  ]
  node [
    id 1073
    label "prosecute"
  ]
  node [
    id 1074
    label "gestia"
  ]
  node [
    id 1075
    label "znawstwo"
  ]
  node [
    id 1076
    label "authority"
  ]
  node [
    id 1077
    label "zmienna"
  ]
  node [
    id 1078
    label "wskazywa&#263;"
  ]
  node [
    id 1079
    label "worth"
  ]
  node [
    id 1080
    label "analiza"
  ]
  node [
    id 1081
    label "specyfikacja"
  ]
  node [
    id 1082
    label "wykres"
  ]
  node [
    id 1083
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 1084
    label "interpretacja"
  ]
  node [
    id 1085
    label "nastawi&#263;"
  ]
  node [
    id 1086
    label "ulepszy&#263;"
  ]
  node [
    id 1087
    label "determine"
  ]
  node [
    id 1088
    label "proces_fizjologiczny"
  ]
  node [
    id 1089
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1090
    label "usprawni&#263;"
  ]
  node [
    id 1091
    label "align"
  ]
  node [
    id 1092
    label "manipulate"
  ]
  node [
    id 1093
    label "op&#322;aca&#263;"
  ]
  node [
    id 1094
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1095
    label "tune"
  ]
  node [
    id 1096
    label "ustawia&#263;"
  ]
  node [
    id 1097
    label "nastawia&#263;"
  ]
  node [
    id 1098
    label "ulepsza&#263;"
  ]
  node [
    id 1099
    label "usprawnia&#263;"
  ]
  node [
    id 1100
    label "normalize"
  ]
  node [
    id 1101
    label "kszta&#322;towanie"
  ]
  node [
    id 1102
    label "alteration"
  ]
  node [
    id 1103
    label "nastawianie"
  ]
  node [
    id 1104
    label "control"
  ]
  node [
    id 1105
    label "op&#322;acanie"
  ]
  node [
    id 1106
    label "ustawianie"
  ]
  node [
    id 1107
    label "usprawnianie"
  ]
  node [
    id 1108
    label "ulepszanie"
  ]
  node [
    id 1109
    label "usprawnienie"
  ]
  node [
    id 1110
    label "nastawienie"
  ]
  node [
    id 1111
    label "ulepszenie"
  ]
  node [
    id 1112
    label "ukszta&#322;towanie"
  ]
  node [
    id 1113
    label "adjustment"
  ]
  node [
    id 1114
    label "&#322;atwi&#263;"
  ]
  node [
    id 1115
    label "ease"
  ]
  node [
    id 1116
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1117
    label "motywowa&#263;"
  ]
  node [
    id 1118
    label "act"
  ]
  node [
    id 1119
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1120
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1121
    label "czyni&#263;"
  ]
  node [
    id 1122
    label "stylizowa&#263;"
  ]
  node [
    id 1123
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 1124
    label "falowa&#263;"
  ]
  node [
    id 1125
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 1126
    label "peddle"
  ]
  node [
    id 1127
    label "praca"
  ]
  node [
    id 1128
    label "wydala&#263;"
  ]
  node [
    id 1129
    label "tentegowa&#263;"
  ]
  node [
    id 1130
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 1131
    label "urz&#261;dza&#263;"
  ]
  node [
    id 1132
    label "oszukiwa&#263;"
  ]
  node [
    id 1133
    label "przerabia&#263;"
  ]
  node [
    id 1134
    label "post&#281;powa&#263;"
  ]
  node [
    id 1135
    label "buddyzm"
  ]
  node [
    id 1136
    label "cnota"
  ]
  node [
    id 1137
    label "dyspozycja"
  ]
  node [
    id 1138
    label "da&#324;"
  ]
  node [
    id 1139
    label "faculty"
  ]
  node [
    id 1140
    label "stygmat"
  ]
  node [
    id 1141
    label "dobro"
  ]
  node [
    id 1142
    label "rzecz"
  ]
  node [
    id 1143
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 1144
    label "panie&#324;stwo"
  ]
  node [
    id 1145
    label "kalpa"
  ]
  node [
    id 1146
    label "lampka_ma&#347;lana"
  ]
  node [
    id 1147
    label "Buddhism"
  ]
  node [
    id 1148
    label "mahajana"
  ]
  node [
    id 1149
    label "bardo"
  ]
  node [
    id 1150
    label "wad&#378;rajana"
  ]
  node [
    id 1151
    label "arahant"
  ]
  node [
    id 1152
    label "therawada"
  ]
  node [
    id 1153
    label "tantryzm"
  ]
  node [
    id 1154
    label "ahinsa"
  ]
  node [
    id 1155
    label "hinajana"
  ]
  node [
    id 1156
    label "bonzo"
  ]
  node [
    id 1157
    label "asura"
  ]
  node [
    id 1158
    label "religia"
  ]
  node [
    id 1159
    label "maja"
  ]
  node [
    id 1160
    label "li"
  ]
  node [
    id 1161
    label "u&#380;y&#263;"
  ]
  node [
    id 1162
    label "utilize"
  ]
  node [
    id 1163
    label "seize"
  ]
  node [
    id 1164
    label "zrobi&#263;"
  ]
  node [
    id 1165
    label "dozna&#263;"
  ]
  node [
    id 1166
    label "employment"
  ]
  node [
    id 1167
    label "skorzysta&#263;"
  ]
  node [
    id 1168
    label "wykorzysta&#263;"
  ]
  node [
    id 1169
    label "wiadomy"
  ]
  node [
    id 1170
    label "znany"
  ]
  node [
    id 1171
    label "wiadomie"
  ]
  node [
    id 1172
    label "&#322;acno"
  ]
  node [
    id 1173
    label "prosto"
  ]
  node [
    id 1174
    label "snadnie"
  ]
  node [
    id 1175
    label "przyjemnie"
  ]
  node [
    id 1176
    label "&#322;atwie"
  ]
  node [
    id 1177
    label "&#322;atwy"
  ]
  node [
    id 1178
    label "szybko"
  ]
  node [
    id 1179
    label "prosty"
  ]
  node [
    id 1180
    label "skromnie"
  ]
  node [
    id 1181
    label "bezpo&#347;rednio"
  ]
  node [
    id 1182
    label "elementarily"
  ]
  node [
    id 1183
    label "niepozornie"
  ]
  node [
    id 1184
    label "naturalnie"
  ]
  node [
    id 1185
    label "dobrze"
  ]
  node [
    id 1186
    label "pleasantly"
  ]
  node [
    id 1187
    label "deliciously"
  ]
  node [
    id 1188
    label "przyjemny"
  ]
  node [
    id 1189
    label "gratifyingly"
  ]
  node [
    id 1190
    label "quickest"
  ]
  node [
    id 1191
    label "szybki"
  ]
  node [
    id 1192
    label "szybciochem"
  ]
  node [
    id 1193
    label "quicker"
  ]
  node [
    id 1194
    label "szybciej"
  ]
  node [
    id 1195
    label "promptly"
  ]
  node [
    id 1196
    label "dynamicznie"
  ]
  node [
    id 1197
    label "sprawnie"
  ]
  node [
    id 1198
    label "&#322;acny"
  ]
  node [
    id 1199
    label "snadny"
  ]
  node [
    id 1200
    label "letki"
  ]
  node [
    id 1201
    label "ujednolici&#263;"
  ]
  node [
    id 1202
    label "rozpozna&#263;"
  ]
  node [
    id 1203
    label "tag"
  ]
  node [
    id 1204
    label "nada&#263;"
  ]
  node [
    id 1205
    label "dopasowa&#263;"
  ]
  node [
    id 1206
    label "set"
  ]
  node [
    id 1207
    label "rozwa&#380;y&#263;"
  ]
  node [
    id 1208
    label "topographic_point"
  ]
  node [
    id 1209
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 1210
    label "zorientowa&#263;_si&#281;"
  ]
  node [
    id 1211
    label "napis"
  ]
  node [
    id 1212
    label "nerd"
  ]
  node [
    id 1213
    label "znacznik"
  ]
  node [
    id 1214
    label "komnatowy"
  ]
  node [
    id 1215
    label "sport_elektroniczny"
  ]
  node [
    id 1216
    label "identyfikator"
  ]
  node [
    id 1217
    label "Rzym_Zachodni"
  ]
  node [
    id 1218
    label "whole"
  ]
  node [
    id 1219
    label "element"
  ]
  node [
    id 1220
    label "Rzym_Wschodni"
  ]
  node [
    id 1221
    label "r&#243;&#380;niczka"
  ]
  node [
    id 1222
    label "&#347;rodowisko"
  ]
  node [
    id 1223
    label "szambo"
  ]
  node [
    id 1224
    label "aspo&#322;eczny"
  ]
  node [
    id 1225
    label "component"
  ]
  node [
    id 1226
    label "szkodnik"
  ]
  node [
    id 1227
    label "gangsterski"
  ]
  node [
    id 1228
    label "underworld"
  ]
  node [
    id 1229
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 1230
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 1231
    label "kom&#243;rka"
  ]
  node [
    id 1232
    label "furnishing"
  ]
  node [
    id 1233
    label "zabezpieczenie"
  ]
  node [
    id 1234
    label "wyrz&#261;dzenie"
  ]
  node [
    id 1235
    label "zagospodarowanie"
  ]
  node [
    id 1236
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 1237
    label "ig&#322;a"
  ]
  node [
    id 1238
    label "wirnik"
  ]
  node [
    id 1239
    label "aparatura"
  ]
  node [
    id 1240
    label "system_energetyczny"
  ]
  node [
    id 1241
    label "impulsator"
  ]
  node [
    id 1242
    label "mechanizm"
  ]
  node [
    id 1243
    label "sprz&#281;t"
  ]
  node [
    id 1244
    label "blokowanie"
  ]
  node [
    id 1245
    label "zablokowanie"
  ]
  node [
    id 1246
    label "przygotowanie"
  ]
  node [
    id 1247
    label "komora"
  ]
  node [
    id 1248
    label "j&#281;zyk"
  ]
  node [
    id 1249
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 1250
    label "relate"
  ]
  node [
    id 1251
    label "zinterpretowa&#263;"
  ]
  node [
    id 1252
    label "delineate"
  ]
  node [
    id 1253
    label "insert"
  ]
  node [
    id 1254
    label "obznajomi&#263;"
  ]
  node [
    id 1255
    label "pozna&#263;"
  ]
  node [
    id 1256
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1257
    label "poinformowa&#263;"
  ]
  node [
    id 1258
    label "teach"
  ]
  node [
    id 1259
    label "oceni&#263;"
  ]
  node [
    id 1260
    label "illustrate"
  ]
  node [
    id 1261
    label "zanalizowa&#263;"
  ]
  node [
    id 1262
    label "read"
  ]
  node [
    id 1263
    label "think"
  ]
  node [
    id 1264
    label "infimum"
  ]
  node [
    id 1265
    label "skutek"
  ]
  node [
    id 1266
    label "podzia&#322;anie"
  ]
  node [
    id 1267
    label "supremum"
  ]
  node [
    id 1268
    label "kampania"
  ]
  node [
    id 1269
    label "uruchamianie"
  ]
  node [
    id 1270
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1271
    label "hipnotyzowanie"
  ]
  node [
    id 1272
    label "robienie"
  ]
  node [
    id 1273
    label "uruchomienie"
  ]
  node [
    id 1274
    label "nakr&#281;canie"
  ]
  node [
    id 1275
    label "matematyka"
  ]
  node [
    id 1276
    label "reakcja_chemiczna"
  ]
  node [
    id 1277
    label "tr&#243;jstronny"
  ]
  node [
    id 1278
    label "natural_process"
  ]
  node [
    id 1279
    label "nakr&#281;cenie"
  ]
  node [
    id 1280
    label "zatrzymanie"
  ]
  node [
    id 1281
    label "wp&#322;yw"
  ]
  node [
    id 1282
    label "rzut"
  ]
  node [
    id 1283
    label "podtrzymywanie"
  ]
  node [
    id 1284
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1285
    label "operation"
  ]
  node [
    id 1286
    label "dzianie_si&#281;"
  ]
  node [
    id 1287
    label "zadzia&#322;anie"
  ]
  node [
    id 1288
    label "priorytet"
  ]
  node [
    id 1289
    label "kres"
  ]
  node [
    id 1290
    label "rozpocz&#281;cie"
  ]
  node [
    id 1291
    label "docieranie"
  ]
  node [
    id 1292
    label "funkcja"
  ]
  node [
    id 1293
    label "czynny"
  ]
  node [
    id 1294
    label "impact"
  ]
  node [
    id 1295
    label "oferta"
  ]
  node [
    id 1296
    label "zako&#324;czenie"
  ]
  node [
    id 1297
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1298
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1299
    label "cause"
  ]
  node [
    id 1300
    label "causal_agent"
  ]
  node [
    id 1301
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 1302
    label "laparotomia"
  ]
  node [
    id 1303
    label "torakotomia"
  ]
  node [
    id 1304
    label "chirurg"
  ]
  node [
    id 1305
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 1306
    label "szew"
  ]
  node [
    id 1307
    label "mathematical_process"
  ]
  node [
    id 1308
    label "kwota"
  ]
  node [
    id 1309
    label "&#347;lad"
  ]
  node [
    id 1310
    label "lobbysta"
  ]
  node [
    id 1311
    label "doch&#243;d_narodowy"
  ]
  node [
    id 1312
    label "offer"
  ]
  node [
    id 1313
    label "propozycja"
  ]
  node [
    id 1314
    label "obejrzenie"
  ]
  node [
    id 1315
    label "widzenie"
  ]
  node [
    id 1316
    label "urzeczywistnianie"
  ]
  node [
    id 1317
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1318
    label "produkowanie"
  ]
  node [
    id 1319
    label "przeszkodzenie"
  ]
  node [
    id 1320
    label "znikni&#281;cie"
  ]
  node [
    id 1321
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 1322
    label "przeszkadzanie"
  ]
  node [
    id 1323
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1324
    label "wyprodukowanie"
  ]
  node [
    id 1325
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1326
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1327
    label "creation"
  ]
  node [
    id 1328
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 1329
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 1330
    label "porobienie"
  ]
  node [
    id 1331
    label "tentegowanie"
  ]
  node [
    id 1332
    label "activity"
  ]
  node [
    id 1333
    label "bezproblemowy"
  ]
  node [
    id 1334
    label "addytywno&#347;&#263;"
  ]
  node [
    id 1335
    label "funkcjonowanie"
  ]
  node [
    id 1336
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 1337
    label "powierzanie"
  ]
  node [
    id 1338
    label "dziedzina"
  ]
  node [
    id 1339
    label "przeciwdziedzina"
  ]
  node [
    id 1340
    label "awansowa&#263;"
  ]
  node [
    id 1341
    label "stawia&#263;"
  ]
  node [
    id 1342
    label "wakowa&#263;"
  ]
  node [
    id 1343
    label "postawi&#263;"
  ]
  node [
    id 1344
    label "awansowanie"
  ]
  node [
    id 1345
    label "ostatnie_podrygi"
  ]
  node [
    id 1346
    label "chwila"
  ]
  node [
    id 1347
    label "koniec"
  ]
  node [
    id 1348
    label "przyczyna"
  ]
  node [
    id 1349
    label "closing"
  ]
  node [
    id 1350
    label "termination"
  ]
  node [
    id 1351
    label "closure"
  ]
  node [
    id 1352
    label "conclusion"
  ]
  node [
    id 1353
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 1354
    label "opening"
  ]
  node [
    id 1355
    label "start"
  ]
  node [
    id 1356
    label "pocz&#261;tek"
  ]
  node [
    id 1357
    label "znalezienie_si&#281;"
  ]
  node [
    id 1358
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 1359
    label "wy&#322;&#261;czanie"
  ]
  node [
    id 1360
    label "incorporation"
  ]
  node [
    id 1361
    label "attachment"
  ]
  node [
    id 1362
    label "zaczynanie"
  ]
  node [
    id 1363
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 1364
    label "zapalanie"
  ]
  node [
    id 1365
    label "inclusion"
  ]
  node [
    id 1366
    label "przes&#322;uchiwanie"
  ]
  node [
    id 1367
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 1368
    label "uczestniczenie"
  ]
  node [
    id 1369
    label "przefiltrowanie"
  ]
  node [
    id 1370
    label "zamkni&#281;cie"
  ]
  node [
    id 1371
    label "career"
  ]
  node [
    id 1372
    label "zaaresztowanie"
  ]
  node [
    id 1373
    label "przechowanie"
  ]
  node [
    id 1374
    label "uniemo&#380;liwienie"
  ]
  node [
    id 1375
    label "pochowanie"
  ]
  node [
    id 1376
    label "discontinuance"
  ]
  node [
    id 1377
    label "przerwanie"
  ]
  node [
    id 1378
    label "zaczepienie"
  ]
  node [
    id 1379
    label "pozajmowanie"
  ]
  node [
    id 1380
    label "hipostaza"
  ]
  node [
    id 1381
    label "capture"
  ]
  node [
    id 1382
    label "przetrzymanie"
  ]
  node [
    id 1383
    label "&#322;apanie"
  ]
  node [
    id 1384
    label "z&#322;apanie"
  ]
  node [
    id 1385
    label "check"
  ]
  node [
    id 1386
    label "unieruchomienie"
  ]
  node [
    id 1387
    label "zabranie"
  ]
  node [
    id 1388
    label "przestanie"
  ]
  node [
    id 1389
    label "kapita&#322;"
  ]
  node [
    id 1390
    label "propulsion"
  ]
  node [
    id 1391
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 1392
    label "involvement"
  ]
  node [
    id 1393
    label "za&#347;wiecenie"
  ]
  node [
    id 1394
    label "wy&#322;&#261;czenie"
  ]
  node [
    id 1395
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1396
    label "ukr&#281;cenie"
  ]
  node [
    id 1397
    label "gyration"
  ]
  node [
    id 1398
    label "ruszanie"
  ]
  node [
    id 1399
    label "dokr&#281;cenie"
  ]
  node [
    id 1400
    label "kr&#281;cenie"
  ]
  node [
    id 1401
    label "zakr&#281;canie"
  ]
  node [
    id 1402
    label "tworzenie"
  ]
  node [
    id 1403
    label "nagrywanie"
  ]
  node [
    id 1404
    label "wind"
  ]
  node [
    id 1405
    label "nak&#322;adanie"
  ]
  node [
    id 1406
    label "okr&#281;canie"
  ]
  node [
    id 1407
    label "wzmaganie"
  ]
  node [
    id 1408
    label "wzbudzanie"
  ]
  node [
    id 1409
    label "dokr&#281;canie"
  ]
  node [
    id 1410
    label "pozawijanie"
  ]
  node [
    id 1411
    label "stworzenie"
  ]
  node [
    id 1412
    label "nagranie"
  ]
  node [
    id 1413
    label "wzbudzenie"
  ]
  node [
    id 1414
    label "ruszenie"
  ]
  node [
    id 1415
    label "zakr&#281;cenie"
  ]
  node [
    id 1416
    label "naniesienie"
  ]
  node [
    id 1417
    label "suppression"
  ]
  node [
    id 1418
    label "wzmo&#380;enie"
  ]
  node [
    id 1419
    label "okr&#281;cenie"
  ]
  node [
    id 1420
    label "nak&#322;amanie"
  ]
  node [
    id 1421
    label "utrzymywanie"
  ]
  node [
    id 1422
    label "obstawanie"
  ]
  node [
    id 1423
    label "preservation"
  ]
  node [
    id 1424
    label "boost"
  ]
  node [
    id 1425
    label "continuance"
  ]
  node [
    id 1426
    label "pocieszanie"
  ]
  node [
    id 1427
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1428
    label "asymilowanie"
  ]
  node [
    id 1429
    label "wapniak"
  ]
  node [
    id 1430
    label "asymilowa&#263;"
  ]
  node [
    id 1431
    label "os&#322;abia&#263;"
  ]
  node [
    id 1432
    label "hominid"
  ]
  node [
    id 1433
    label "podw&#322;adny"
  ]
  node [
    id 1434
    label "os&#322;abianie"
  ]
  node [
    id 1435
    label "g&#322;owa"
  ]
  node [
    id 1436
    label "figura"
  ]
  node [
    id 1437
    label "portrecista"
  ]
  node [
    id 1438
    label "dwun&#243;g"
  ]
  node [
    id 1439
    label "profanum"
  ]
  node [
    id 1440
    label "mikrokosmos"
  ]
  node [
    id 1441
    label "nasada"
  ]
  node [
    id 1442
    label "duch"
  ]
  node [
    id 1443
    label "antropochoria"
  ]
  node [
    id 1444
    label "osoba"
  ]
  node [
    id 1445
    label "senior"
  ]
  node [
    id 1446
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1447
    label "Adam"
  ]
  node [
    id 1448
    label "homo_sapiens"
  ]
  node [
    id 1449
    label "polifag"
  ]
  node [
    id 1450
    label "rachowanie"
  ]
  node [
    id 1451
    label "dyskalkulia"
  ]
  node [
    id 1452
    label "wynagrodzenie"
  ]
  node [
    id 1453
    label "rozliczanie"
  ]
  node [
    id 1454
    label "wymienianie"
  ]
  node [
    id 1455
    label "oznaczanie"
  ]
  node [
    id 1456
    label "wychodzenie"
  ]
  node [
    id 1457
    label "naliczenie_si&#281;"
  ]
  node [
    id 1458
    label "wyznaczanie"
  ]
  node [
    id 1459
    label "dodawanie"
  ]
  node [
    id 1460
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 1461
    label "bang"
  ]
  node [
    id 1462
    label "rozliczenie"
  ]
  node [
    id 1463
    label "kwotowanie"
  ]
  node [
    id 1464
    label "mierzenie"
  ]
  node [
    id 1465
    label "count"
  ]
  node [
    id 1466
    label "wycenianie"
  ]
  node [
    id 1467
    label "branie"
  ]
  node [
    id 1468
    label "sprowadzanie"
  ]
  node [
    id 1469
    label "przeliczanie"
  ]
  node [
    id 1470
    label "uwzgl&#281;dnianie"
  ]
  node [
    id 1471
    label "odliczanie"
  ]
  node [
    id 1472
    label "przeliczenie"
  ]
  node [
    id 1473
    label "przyswoi&#263;"
  ]
  node [
    id 1474
    label "one"
  ]
  node [
    id 1475
    label "ewoluowanie"
  ]
  node [
    id 1476
    label "przyswajanie"
  ]
  node [
    id 1477
    label "wyewoluowanie"
  ]
  node [
    id 1478
    label "reakcja"
  ]
  node [
    id 1479
    label "przeliczy&#263;"
  ]
  node [
    id 1480
    label "wyewoluowa&#263;"
  ]
  node [
    id 1481
    label "ewoluowa&#263;"
  ]
  node [
    id 1482
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1483
    label "liczba_naturalna"
  ]
  node [
    id 1484
    label "czynnik_biotyczny"
  ]
  node [
    id 1485
    label "individual"
  ]
  node [
    id 1486
    label "obiekt"
  ]
  node [
    id 1487
    label "przyswaja&#263;"
  ]
  node [
    id 1488
    label "przyswojenie"
  ]
  node [
    id 1489
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 1490
    label "starzenie_si&#281;"
  ]
  node [
    id 1491
    label "przelicza&#263;"
  ]
  node [
    id 1492
    label "ograniczenie"
  ]
  node [
    id 1493
    label "armia"
  ]
  node [
    id 1494
    label "nawr&#243;t_choroby"
  ]
  node [
    id 1495
    label "odwzorowanie"
  ]
  node [
    id 1496
    label "rysunek"
  ]
  node [
    id 1497
    label "scene"
  ]
  node [
    id 1498
    label "throw"
  ]
  node [
    id 1499
    label "float"
  ]
  node [
    id 1500
    label "projection"
  ]
  node [
    id 1501
    label "injection"
  ]
  node [
    id 1502
    label "blow"
  ]
  node [
    id 1503
    label "k&#322;ad"
  ]
  node [
    id 1504
    label "mold"
  ]
  node [
    id 1505
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1506
    label "wymienia&#263;"
  ]
  node [
    id 1507
    label "wycenia&#263;"
  ]
  node [
    id 1508
    label "bra&#263;"
  ]
  node [
    id 1509
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 1510
    label "rachowa&#263;"
  ]
  node [
    id 1511
    label "tell"
  ]
  node [
    id 1512
    label "odlicza&#263;"
  ]
  node [
    id 1513
    label "dodawa&#263;"
  ]
  node [
    id 1514
    label "wyznacza&#263;"
  ]
  node [
    id 1515
    label "admit"
  ]
  node [
    id 1516
    label "policza&#263;"
  ]
  node [
    id 1517
    label "rachunek_operatorowy"
  ]
  node [
    id 1518
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1519
    label "kryptologia"
  ]
  node [
    id 1520
    label "logicyzm"
  ]
  node [
    id 1521
    label "logika"
  ]
  node [
    id 1522
    label "matematyka_czysta"
  ]
  node [
    id 1523
    label "forsing"
  ]
  node [
    id 1524
    label "modelowanie_matematyczne"
  ]
  node [
    id 1525
    label "matma"
  ]
  node [
    id 1526
    label "teoria_katastrof"
  ]
  node [
    id 1527
    label "fizyka_matematyczna"
  ]
  node [
    id 1528
    label "teoria_graf&#243;w"
  ]
  node [
    id 1529
    label "rachunki"
  ]
  node [
    id 1530
    label "topologia_algebraiczna"
  ]
  node [
    id 1531
    label "matematyka_stosowana"
  ]
  node [
    id 1532
    label "reply"
  ]
  node [
    id 1533
    label "chemia"
  ]
  node [
    id 1534
    label "response"
  ]
  node [
    id 1535
    label "intensywny"
  ]
  node [
    id 1536
    label "ciekawy"
  ]
  node [
    id 1537
    label "realny"
  ]
  node [
    id 1538
    label "dzia&#322;alny"
  ]
  node [
    id 1539
    label "faktyczny"
  ]
  node [
    id 1540
    label "zdolny"
  ]
  node [
    id 1541
    label "czynnie"
  ]
  node [
    id 1542
    label "uczynnianie"
  ]
  node [
    id 1543
    label "aktywnie"
  ]
  node [
    id 1544
    label "zaanga&#380;owany"
  ]
  node [
    id 1545
    label "wa&#380;ny"
  ]
  node [
    id 1546
    label "istotny"
  ]
  node [
    id 1547
    label "zaj&#281;ty"
  ]
  node [
    id 1548
    label "uczynnienie"
  ]
  node [
    id 1549
    label "dobry"
  ]
  node [
    id 1550
    label "hypnotism"
  ]
  node [
    id 1551
    label "zachwycanie"
  ]
  node [
    id 1552
    label "usypianie"
  ]
  node [
    id 1553
    label "magnetyzowanie"
  ]
  node [
    id 1554
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 1555
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 1556
    label "silnik"
  ]
  node [
    id 1557
    label "dorabianie"
  ]
  node [
    id 1558
    label "tarcie"
  ]
  node [
    id 1559
    label "dopasowywanie"
  ]
  node [
    id 1560
    label "g&#322;adzenie"
  ]
  node [
    id 1561
    label "dostawanie_si&#281;"
  ]
  node [
    id 1562
    label "zahipnotyzowanie"
  ]
  node [
    id 1563
    label "wdarcie_si&#281;"
  ]
  node [
    id 1564
    label "dotarcie"
  ]
  node [
    id 1565
    label "sprawa"
  ]
  node [
    id 1566
    label "us&#322;uga"
  ]
  node [
    id 1567
    label "pierwszy_plan"
  ]
  node [
    id 1568
    label "przesy&#322;ka"
  ]
  node [
    id 1569
    label "absolutorium"
  ]
  node [
    id 1570
    label "campaign"
  ]
  node [
    id 1571
    label "akcja"
  ]
  node [
    id 1572
    label "Kampania_Wrze&#347;niowa"
  ]
  node [
    id 1573
    label "drabina_analgetyczna"
  ]
  node [
    id 1574
    label "radiation_pattern"
  ]
  node [
    id 1575
    label "exemplar"
  ]
  node [
    id 1576
    label "kreska"
  ]
  node [
    id 1577
    label "picture"
  ]
  node [
    id 1578
    label "teka"
  ]
  node [
    id 1579
    label "photograph"
  ]
  node [
    id 1580
    label "grafika"
  ]
  node [
    id 1581
    label "pocz&#261;tki"
  ]
  node [
    id 1582
    label "ukra&#347;&#263;"
  ]
  node [
    id 1583
    label "ukradzenie"
  ]
  node [
    id 1584
    label "idea"
  ]
  node [
    id 1585
    label "system"
  ]
  node [
    id 1586
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1587
    label "nale&#380;yty"
  ]
  node [
    id 1588
    label "uprawniony"
  ]
  node [
    id 1589
    label "zasadniczy"
  ]
  node [
    id 1590
    label "stosownie"
  ]
  node [
    id 1591
    label "taki"
  ]
  node [
    id 1592
    label "charakterystyczny"
  ]
  node [
    id 1593
    label "prawdziwy"
  ]
  node [
    id 1594
    label "powinny"
  ]
  node [
    id 1595
    label "nale&#380;nie"
  ]
  node [
    id 1596
    label "godny"
  ]
  node [
    id 1597
    label "przynale&#380;ny"
  ]
  node [
    id 1598
    label "zwyczajny"
  ]
  node [
    id 1599
    label "typowo"
  ]
  node [
    id 1600
    label "zwyk&#322;y"
  ]
  node [
    id 1601
    label "charakterystycznie"
  ]
  node [
    id 1602
    label "szczeg&#243;lny"
  ]
  node [
    id 1603
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1604
    label "&#380;ywny"
  ]
  node [
    id 1605
    label "szczery"
  ]
  node [
    id 1606
    label "naprawd&#281;"
  ]
  node [
    id 1607
    label "realnie"
  ]
  node [
    id 1608
    label "zgodny"
  ]
  node [
    id 1609
    label "m&#261;dry"
  ]
  node [
    id 1610
    label "prawdziwie"
  ]
  node [
    id 1611
    label "w&#322;ady"
  ]
  node [
    id 1612
    label "g&#322;&#243;wny"
  ]
  node [
    id 1613
    label "og&#243;lny"
  ]
  node [
    id 1614
    label "zasadniczo"
  ]
  node [
    id 1615
    label "surowy"
  ]
  node [
    id 1616
    label "zadowalaj&#261;cy"
  ]
  node [
    id 1617
    label "nale&#380;ycie"
  ]
  node [
    id 1618
    label "przystojny"
  ]
  node [
    id 1619
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1620
    label "dobroczynny"
  ]
  node [
    id 1621
    label "czw&#243;rka"
  ]
  node [
    id 1622
    label "spokojny"
  ]
  node [
    id 1623
    label "skuteczny"
  ]
  node [
    id 1624
    label "mi&#322;y"
  ]
  node [
    id 1625
    label "grzeczny"
  ]
  node [
    id 1626
    label "powitanie"
  ]
  node [
    id 1627
    label "ca&#322;y"
  ]
  node [
    id 1628
    label "zwrot"
  ]
  node [
    id 1629
    label "pomy&#347;lny"
  ]
  node [
    id 1630
    label "moralny"
  ]
  node [
    id 1631
    label "drogi"
  ]
  node [
    id 1632
    label "pozytywny"
  ]
  node [
    id 1633
    label "odpowiedni"
  ]
  node [
    id 1634
    label "korzystny"
  ]
  node [
    id 1635
    label "pos&#322;uszny"
  ]
  node [
    id 1636
    label "jaki&#347;"
  ]
  node [
    id 1637
    label "stosowny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 27
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 292
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 49
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 231
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 37
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 324
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 30
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 35
  ]
  edge [
    source 15
    target 36
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 48
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 569
  ]
  edge [
    source 15
    target 49
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 40
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 570
  ]
  edge [
    source 16
    target 571
  ]
  edge [
    source 16
    target 572
  ]
  edge [
    source 16
    target 573
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 17
    target 578
  ]
  edge [
    source 17
    target 579
  ]
  edge [
    source 17
    target 580
  ]
  edge [
    source 17
    target 581
  ]
  edge [
    source 17
    target 582
  ]
  edge [
    source 17
    target 583
  ]
  edge [
    source 17
    target 584
  ]
  edge [
    source 17
    target 585
  ]
  edge [
    source 17
    target 586
  ]
  edge [
    source 17
    target 587
  ]
  edge [
    source 17
    target 588
  ]
  edge [
    source 17
    target 589
  ]
  edge [
    source 17
    target 590
  ]
  edge [
    source 17
    target 591
  ]
  edge [
    source 17
    target 592
  ]
  edge [
    source 17
    target 593
  ]
  edge [
    source 17
    target 594
  ]
  edge [
    source 17
    target 251
  ]
  edge [
    source 17
    target 595
  ]
  edge [
    source 17
    target 596
  ]
  edge [
    source 17
    target 597
  ]
  edge [
    source 17
    target 598
  ]
  edge [
    source 17
    target 599
  ]
  edge [
    source 17
    target 600
  ]
  edge [
    source 17
    target 601
  ]
  edge [
    source 17
    target 602
  ]
  edge [
    source 17
    target 603
  ]
  edge [
    source 17
    target 604
  ]
  edge [
    source 17
    target 605
  ]
  edge [
    source 17
    target 606
  ]
  edge [
    source 17
    target 607
  ]
  edge [
    source 17
    target 608
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 17
    target 47
  ]
  edge [
    source 17
    target 609
  ]
  edge [
    source 17
    target 610
  ]
  edge [
    source 17
    target 611
  ]
  edge [
    source 17
    target 612
  ]
  edge [
    source 17
    target 613
  ]
  edge [
    source 17
    target 614
  ]
  edge [
    source 17
    target 615
  ]
  edge [
    source 17
    target 616
  ]
  edge [
    source 17
    target 386
  ]
  edge [
    source 17
    target 617
  ]
  edge [
    source 17
    target 379
  ]
  edge [
    source 17
    target 618
  ]
  edge [
    source 17
    target 619
  ]
  edge [
    source 17
    target 620
  ]
  edge [
    source 17
    target 621
  ]
  edge [
    source 17
    target 622
  ]
  edge [
    source 17
    target 623
  ]
  edge [
    source 17
    target 624
  ]
  edge [
    source 17
    target 625
  ]
  edge [
    source 17
    target 626
  ]
  edge [
    source 17
    target 627
  ]
  edge [
    source 17
    target 294
  ]
  edge [
    source 17
    target 628
  ]
  edge [
    source 17
    target 629
  ]
  edge [
    source 17
    target 630
  ]
  edge [
    source 17
    target 631
  ]
  edge [
    source 17
    target 632
  ]
  edge [
    source 17
    target 48
  ]
  edge [
    source 17
    target 49
  ]
  edge [
    source 17
    target 50
  ]
  edge [
    source 17
    target 51
  ]
  edge [
    source 17
    target 633
  ]
  edge [
    source 17
    target 634
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 635
  ]
  edge [
    source 17
    target 636
  ]
  edge [
    source 17
    target 637
  ]
  edge [
    source 17
    target 638
  ]
  edge [
    source 17
    target 639
  ]
  edge [
    source 17
    target 640
  ]
  edge [
    source 17
    target 641
  ]
  edge [
    source 17
    target 642
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 645
  ]
  edge [
    source 17
    target 52
  ]
  edge [
    source 17
    target 646
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 649
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 651
  ]
  edge [
    source 17
    target 652
  ]
  edge [
    source 17
    target 653
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 656
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 53
  ]
  edge [
    source 17
    target 658
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 661
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 667
  ]
  edge [
    source 17
    target 668
  ]
  edge [
    source 17
    target 669
  ]
  edge [
    source 17
    target 670
  ]
  edge [
    source 17
    target 671
  ]
  edge [
    source 17
    target 672
  ]
  edge [
    source 17
    target 673
  ]
  edge [
    source 17
    target 674
  ]
  edge [
    source 17
    target 675
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 678
  ]
  edge [
    source 17
    target 679
  ]
  edge [
    source 17
    target 680
  ]
  edge [
    source 17
    target 681
  ]
  edge [
    source 17
    target 682
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 684
  ]
  edge [
    source 17
    target 685
  ]
  edge [
    source 17
    target 686
  ]
  edge [
    source 17
    target 687
  ]
  edge [
    source 17
    target 688
  ]
  edge [
    source 17
    target 689
  ]
  edge [
    source 17
    target 690
  ]
  edge [
    source 17
    target 691
  ]
  edge [
    source 17
    target 692
  ]
  edge [
    source 17
    target 434
  ]
  edge [
    source 17
    target 693
  ]
  edge [
    source 17
    target 694
  ]
  edge [
    source 17
    target 695
  ]
  edge [
    source 17
    target 696
  ]
  edge [
    source 17
    target 697
  ]
  edge [
    source 17
    target 698
  ]
  edge [
    source 17
    target 699
  ]
  edge [
    source 17
    target 700
  ]
  edge [
    source 17
    target 701
  ]
  edge [
    source 17
    target 702
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 703
  ]
  edge [
    source 17
    target 704
  ]
  edge [
    source 17
    target 705
  ]
  edge [
    source 17
    target 706
  ]
  edge [
    source 17
    target 707
  ]
  edge [
    source 17
    target 708
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 710
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 714
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 493
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 18
    target 341
  ]
  edge [
    source 18
    target 724
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 359
  ]
  edge [
    source 18
    target 726
  ]
  edge [
    source 18
    target 727
  ]
  edge [
    source 18
    target 728
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 730
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 732
  ]
  edge [
    source 18
    target 733
  ]
  edge [
    source 18
    target 734
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 736
  ]
  edge [
    source 18
    target 737
  ]
  edge [
    source 18
    target 370
  ]
  edge [
    source 18
    target 738
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 742
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 362
  ]
  edge [
    source 18
    target 750
  ]
  edge [
    source 18
    target 751
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 18
    target 756
  ]
  edge [
    source 18
    target 757
  ]
  edge [
    source 18
    target 758
  ]
  edge [
    source 18
    target 759
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 760
  ]
  edge [
    source 19
    target 319
  ]
  edge [
    source 19
    target 761
  ]
  edge [
    source 19
    target 762
  ]
  edge [
    source 19
    target 763
  ]
  edge [
    source 19
    target 764
  ]
  edge [
    source 19
    target 765
  ]
  edge [
    source 19
    target 766
  ]
  edge [
    source 19
    target 767
  ]
  edge [
    source 19
    target 768
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 486
  ]
  edge [
    source 19
    target 769
  ]
  edge [
    source 19
    target 770
  ]
  edge [
    source 19
    target 771
  ]
  edge [
    source 19
    target 772
  ]
  edge [
    source 19
    target 773
  ]
  edge [
    source 19
    target 774
  ]
  edge [
    source 19
    target 775
  ]
  edge [
    source 19
    target 776
  ]
  edge [
    source 19
    target 777
  ]
  edge [
    source 19
    target 778
  ]
  edge [
    source 19
    target 779
  ]
  edge [
    source 19
    target 780
  ]
  edge [
    source 19
    target 781
  ]
  edge [
    source 19
    target 782
  ]
  edge [
    source 19
    target 783
  ]
  edge [
    source 19
    target 784
  ]
  edge [
    source 19
    target 785
  ]
  edge [
    source 19
    target 786
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 788
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 611
  ]
  edge [
    source 19
    target 792
  ]
  edge [
    source 19
    target 793
  ]
  edge [
    source 19
    target 794
  ]
  edge [
    source 19
    target 795
  ]
  edge [
    source 19
    target 796
  ]
  edge [
    source 19
    target 797
  ]
  edge [
    source 19
    target 798
  ]
  edge [
    source 19
    target 799
  ]
  edge [
    source 19
    target 316
  ]
  edge [
    source 19
    target 800
  ]
  edge [
    source 19
    target 801
  ]
  edge [
    source 19
    target 802
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 19
    target 804
  ]
  edge [
    source 19
    target 805
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 807
  ]
  edge [
    source 19
    target 808
  ]
  edge [
    source 19
    target 809
  ]
  edge [
    source 19
    target 810
  ]
  edge [
    source 19
    target 811
  ]
  edge [
    source 19
    target 812
  ]
  edge [
    source 19
    target 813
  ]
  edge [
    source 19
    target 814
  ]
  edge [
    source 19
    target 815
  ]
  edge [
    source 19
    target 816
  ]
  edge [
    source 19
    target 817
  ]
  edge [
    source 19
    target 550
  ]
  edge [
    source 19
    target 818
  ]
  edge [
    source 19
    target 819
  ]
  edge [
    source 19
    target 820
  ]
  edge [
    source 19
    target 821
  ]
  edge [
    source 19
    target 362
  ]
  edge [
    source 19
    target 822
  ]
  edge [
    source 19
    target 823
  ]
  edge [
    source 19
    target 824
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 19
    target 826
  ]
  edge [
    source 19
    target 378
  ]
  edge [
    source 19
    target 827
  ]
  edge [
    source 19
    target 828
  ]
  edge [
    source 19
    target 829
  ]
  edge [
    source 19
    target 291
  ]
  edge [
    source 19
    target 358
  ]
  edge [
    source 19
    target 47
  ]
  edge [
    source 19
    target 48
  ]
  edge [
    source 19
    target 49
  ]
  edge [
    source 19
    target 50
  ]
  edge [
    source 19
    target 51
  ]
  edge [
    source 19
    target 830
  ]
  edge [
    source 19
    target 831
  ]
  edge [
    source 19
    target 619
  ]
  edge [
    source 19
    target 832
  ]
  edge [
    source 19
    target 833
  ]
  edge [
    source 19
    target 834
  ]
  edge [
    source 19
    target 835
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 836
  ]
  edge [
    source 19
    target 837
  ]
  edge [
    source 19
    target 838
  ]
  edge [
    source 19
    target 839
  ]
  edge [
    source 19
    target 840
  ]
  edge [
    source 19
    target 841
  ]
  edge [
    source 19
    target 842
  ]
  edge [
    source 19
    target 372
  ]
  edge [
    source 19
    target 843
  ]
  edge [
    source 19
    target 844
  ]
  edge [
    source 19
    target 845
  ]
  edge [
    source 19
    target 361
  ]
  edge [
    source 19
    target 846
  ]
  edge [
    source 19
    target 847
  ]
  edge [
    source 19
    target 848
  ]
  edge [
    source 19
    target 849
  ]
  edge [
    source 19
    target 850
  ]
  edge [
    source 19
    target 851
  ]
  edge [
    source 19
    target 852
  ]
  edge [
    source 19
    target 853
  ]
  edge [
    source 19
    target 854
  ]
  edge [
    source 19
    target 855
  ]
  edge [
    source 19
    target 856
  ]
  edge [
    source 19
    target 692
  ]
  edge [
    source 19
    target 434
  ]
  edge [
    source 19
    target 469
  ]
  edge [
    source 19
    target 857
  ]
  edge [
    source 19
    target 858
  ]
  edge [
    source 19
    target 859
  ]
  edge [
    source 19
    target 860
  ]
  edge [
    source 19
    target 861
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 483
  ]
  edge [
    source 19
    target 282
  ]
  edge [
    source 19
    target 862
  ]
  edge [
    source 19
    target 863
  ]
  edge [
    source 19
    target 864
  ]
  edge [
    source 19
    target 865
  ]
  edge [
    source 19
    target 866
  ]
  edge [
    source 19
    target 867
  ]
  edge [
    source 19
    target 868
  ]
  edge [
    source 19
    target 869
  ]
  edge [
    source 19
    target 870
  ]
  edge [
    source 19
    target 871
  ]
  edge [
    source 19
    target 872
  ]
  edge [
    source 19
    target 873
  ]
  edge [
    source 19
    target 874
  ]
  edge [
    source 19
    target 875
  ]
  edge [
    source 19
    target 876
  ]
  edge [
    source 19
    target 877
  ]
  edge [
    source 19
    target 878
  ]
  edge [
    source 19
    target 879
  ]
  edge [
    source 19
    target 880
  ]
  edge [
    source 19
    target 881
  ]
  edge [
    source 19
    target 882
  ]
  edge [
    source 19
    target 465
  ]
  edge [
    source 19
    target 883
  ]
  edge [
    source 19
    target 884
  ]
  edge [
    source 19
    target 885
  ]
  edge [
    source 19
    target 886
  ]
  edge [
    source 19
    target 887
  ]
  edge [
    source 19
    target 888
  ]
  edge [
    source 19
    target 889
  ]
  edge [
    source 19
    target 890
  ]
  edge [
    source 19
    target 891
  ]
  edge [
    source 19
    target 892
  ]
  edge [
    source 19
    target 893
  ]
  edge [
    source 19
    target 894
  ]
  edge [
    source 19
    target 621
  ]
  edge [
    source 19
    target 895
  ]
  edge [
    source 19
    target 896
  ]
  edge [
    source 19
    target 897
  ]
  edge [
    source 19
    target 898
  ]
  edge [
    source 19
    target 899
  ]
  edge [
    source 19
    target 900
  ]
  edge [
    source 19
    target 901
  ]
  edge [
    source 19
    target 902
  ]
  edge [
    source 19
    target 903
  ]
  edge [
    source 19
    target 904
  ]
  edge [
    source 19
    target 905
  ]
  edge [
    source 19
    target 906
  ]
  edge [
    source 19
    target 907
  ]
  edge [
    source 19
    target 908
  ]
  edge [
    source 19
    target 909
  ]
  edge [
    source 19
    target 910
  ]
  edge [
    source 19
    target 911
  ]
  edge [
    source 19
    target 912
  ]
  edge [
    source 19
    target 913
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 914
  ]
  edge [
    source 19
    target 915
  ]
  edge [
    source 19
    target 916
  ]
  edge [
    source 19
    target 917
  ]
  edge [
    source 19
    target 918
  ]
  edge [
    source 19
    target 919
  ]
  edge [
    source 19
    target 920
  ]
  edge [
    source 19
    target 921
  ]
  edge [
    source 19
    target 390
  ]
  edge [
    source 19
    target 243
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 922
  ]
  edge [
    source 19
    target 92
  ]
  edge [
    source 19
    target 923
  ]
  edge [
    source 19
    target 290
  ]
  edge [
    source 19
    target 444
  ]
  edge [
    source 19
    target 924
  ]
  edge [
    source 19
    target 484
  ]
  edge [
    source 19
    target 925
  ]
  edge [
    source 19
    target 926
  ]
  edge [
    source 19
    target 927
  ]
  edge [
    source 19
    target 928
  ]
  edge [
    source 19
    target 929
  ]
  edge [
    source 19
    target 487
  ]
  edge [
    source 19
    target 930
  ]
  edge [
    source 19
    target 931
  ]
  edge [
    source 19
    target 932
  ]
  edge [
    source 19
    target 933
  ]
  edge [
    source 19
    target 934
  ]
  edge [
    source 19
    target 935
  ]
  edge [
    source 19
    target 936
  ]
  edge [
    source 19
    target 937
  ]
  edge [
    source 19
    target 938
  ]
  edge [
    source 19
    target 939
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 940
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 942
  ]
  edge [
    source 19
    target 943
  ]
  edge [
    source 19
    target 944
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 571
  ]
  edge [
    source 19
    target 946
  ]
  edge [
    source 19
    target 947
  ]
  edge [
    source 19
    target 948
  ]
  edge [
    source 19
    target 949
  ]
  edge [
    source 19
    target 36
  ]
  edge [
    source 19
    target 950
  ]
  edge [
    source 19
    target 278
  ]
  edge [
    source 19
    target 951
  ]
  edge [
    source 19
    target 952
  ]
  edge [
    source 19
    target 953
  ]
  edge [
    source 19
    target 954
  ]
  edge [
    source 19
    target 955
  ]
  edge [
    source 19
    target 956
  ]
  edge [
    source 19
    target 957
  ]
  edge [
    source 19
    target 958
  ]
  edge [
    source 19
    target 959
  ]
  edge [
    source 19
    target 960
  ]
  edge [
    source 19
    target 961
  ]
  edge [
    source 19
    target 962
  ]
  edge [
    source 19
    target 963
  ]
  edge [
    source 19
    target 570
  ]
  edge [
    source 19
    target 964
  ]
  edge [
    source 19
    target 965
  ]
  edge [
    source 19
    target 966
  ]
  edge [
    source 19
    target 967
  ]
  edge [
    source 19
    target 968
  ]
  edge [
    source 19
    target 649
  ]
  edge [
    source 19
    target 969
  ]
  edge [
    source 19
    target 970
  ]
  edge [
    source 19
    target 971
  ]
  edge [
    source 19
    target 972
  ]
  edge [
    source 19
    target 973
  ]
  edge [
    source 19
    target 974
  ]
  edge [
    source 19
    target 975
  ]
  edge [
    source 19
    target 976
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 325
  ]
  edge [
    source 19
    target 326
  ]
  edge [
    source 19
    target 327
  ]
  edge [
    source 19
    target 328
  ]
  edge [
    source 19
    target 329
  ]
  edge [
    source 19
    target 330
  ]
  edge [
    source 19
    target 331
  ]
  edge [
    source 19
    target 332
  ]
  edge [
    source 19
    target 582
  ]
  edge [
    source 19
    target 977
  ]
  edge [
    source 19
    target 978
  ]
  edge [
    source 19
    target 364
  ]
  edge [
    source 19
    target 979
  ]
  edge [
    source 19
    target 980
  ]
  edge [
    source 19
    target 981
  ]
  edge [
    source 19
    target 982
  ]
  edge [
    source 19
    target 983
  ]
  edge [
    source 19
    target 984
  ]
  edge [
    source 19
    target 985
  ]
  edge [
    source 19
    target 986
  ]
  edge [
    source 19
    target 987
  ]
  edge [
    source 19
    target 360
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 573
  ]
  edge [
    source 20
    target 989
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 20
    target 94
  ]
  edge [
    source 20
    target 990
  ]
  edge [
    source 20
    target 991
  ]
  edge [
    source 20
    target 992
  ]
  edge [
    source 20
    target 993
  ]
  edge [
    source 20
    target 994
  ]
  edge [
    source 20
    target 995
  ]
  edge [
    source 20
    target 996
  ]
  edge [
    source 20
    target 997
  ]
  edge [
    source 20
    target 998
  ]
  edge [
    source 20
    target 324
  ]
  edge [
    source 20
    target 999
  ]
  edge [
    source 20
    target 1000
  ]
  edge [
    source 20
    target 1001
  ]
  edge [
    source 20
    target 558
  ]
  edge [
    source 20
    target 1002
  ]
  edge [
    source 20
    target 572
  ]
  edge [
    source 20
    target 1003
  ]
  edge [
    source 20
    target 1004
  ]
  edge [
    source 20
    target 575
  ]
  edge [
    source 20
    target 576
  ]
  edge [
    source 20
    target 1005
  ]
  edge [
    source 20
    target 1006
  ]
  edge [
    source 20
    target 1007
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1008
  ]
  edge [
    source 21
    target 1009
  ]
  edge [
    source 21
    target 1010
  ]
  edge [
    source 21
    target 1011
  ]
  edge [
    source 21
    target 1012
  ]
  edge [
    source 21
    target 1013
  ]
  edge [
    source 21
    target 1014
  ]
  edge [
    source 21
    target 1015
  ]
  edge [
    source 21
    target 873
  ]
  edge [
    source 21
    target 1016
  ]
  edge [
    source 21
    target 311
  ]
  edge [
    source 21
    target 1017
  ]
  edge [
    source 21
    target 1018
  ]
  edge [
    source 21
    target 1019
  ]
  edge [
    source 21
    target 231
  ]
  edge [
    source 21
    target 1020
  ]
  edge [
    source 21
    target 892
  ]
  edge [
    source 21
    target 1021
  ]
  edge [
    source 21
    target 1022
  ]
  edge [
    source 21
    target 1023
  ]
  edge [
    source 21
    target 1024
  ]
  edge [
    source 21
    target 1025
  ]
  edge [
    source 21
    target 1026
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 1029
  ]
  edge [
    source 21
    target 1030
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 21
    target 1032
  ]
  edge [
    source 21
    target 1033
  ]
  edge [
    source 21
    target 760
  ]
  edge [
    source 21
    target 785
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 319
  ]
  edge [
    source 21
    target 764
  ]
  edge [
    source 21
    target 765
  ]
  edge [
    source 21
    target 1034
  ]
  edge [
    source 21
    target 621
  ]
  edge [
    source 21
    target 766
  ]
  edge [
    source 21
    target 1035
  ]
  edge [
    source 21
    target 1036
  ]
  edge [
    source 21
    target 895
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 21
    target 1037
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 708
  ]
  edge [
    source 21
    target 1038
  ]
  edge [
    source 21
    target 769
  ]
  edge [
    source 21
    target 1039
  ]
  edge [
    source 21
    target 1040
  ]
  edge [
    source 21
    target 447
  ]
  edge [
    source 21
    target 1041
  ]
  edge [
    source 21
    target 1042
  ]
  edge [
    source 21
    target 772
  ]
  edge [
    source 21
    target 778
  ]
  edge [
    source 21
    target 1043
  ]
  edge [
    source 21
    target 776
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 782
  ]
  edge [
    source 21
    target 1044
  ]
  edge [
    source 21
    target 1045
  ]
  edge [
    source 21
    target 783
  ]
  edge [
    source 21
    target 784
  ]
  edge [
    source 21
    target 1046
  ]
  edge [
    source 21
    target 1047
  ]
  edge [
    source 21
    target 1048
  ]
  edge [
    source 21
    target 1049
  ]
  edge [
    source 21
    target 1050
  ]
  edge [
    source 21
    target 1051
  ]
  edge [
    source 21
    target 1052
  ]
  edge [
    source 21
    target 1053
  ]
  edge [
    source 21
    target 1054
  ]
  edge [
    source 21
    target 1055
  ]
  edge [
    source 21
    target 1056
  ]
  edge [
    source 21
    target 1057
  ]
  edge [
    source 21
    target 1058
  ]
  edge [
    source 21
    target 1059
  ]
  edge [
    source 21
    target 1060
  ]
  edge [
    source 21
    target 1061
  ]
  edge [
    source 21
    target 1062
  ]
  edge [
    source 21
    target 1063
  ]
  edge [
    source 21
    target 1064
  ]
  edge [
    source 21
    target 876
  ]
  edge [
    source 21
    target 1065
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 441
  ]
  edge [
    source 21
    target 92
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1069
  ]
  edge [
    source 22
    target 1070
  ]
  edge [
    source 22
    target 1071
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1072
  ]
  edge [
    source 23
    target 1073
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 242
  ]
  edge [
    source 25
    target 243
  ]
  edge [
    source 25
    target 244
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 245
  ]
  edge [
    source 25
    target 246
  ]
  edge [
    source 25
    target 247
  ]
  edge [
    source 25
    target 248
  ]
  edge [
    source 25
    target 249
  ]
  edge [
    source 25
    target 92
  ]
  edge [
    source 25
    target 250
  ]
  edge [
    source 25
    target 251
  ]
  edge [
    source 25
    target 1074
  ]
  edge [
    source 25
    target 1040
  ]
  edge [
    source 25
    target 1075
  ]
  edge [
    source 25
    target 257
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 1076
  ]
  edge [
    source 25
    target 594
  ]
  edge [
    source 25
    target 262
  ]
  edge [
    source 25
    target 263
  ]
  edge [
    source 25
    target 264
  ]
  edge [
    source 25
    target 268
  ]
  edge [
    source 25
    target 266
  ]
  edge [
    source 25
    target 267
  ]
  edge [
    source 25
    target 265
  ]
  edge [
    source 25
    target 269
  ]
  edge [
    source 25
    target 270
  ]
  edge [
    source 25
    target 271
  ]
  edge [
    source 25
    target 272
  ]
  edge [
    source 25
    target 273
  ]
  edge [
    source 25
    target 47
  ]
  edge [
    source 25
    target 609
  ]
  edge [
    source 25
    target 610
  ]
  edge [
    source 25
    target 611
  ]
  edge [
    source 25
    target 612
  ]
  edge [
    source 25
    target 613
  ]
  edge [
    source 25
    target 614
  ]
  edge [
    source 25
    target 615
  ]
  edge [
    source 25
    target 588
  ]
  edge [
    source 25
    target 616
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 1077
  ]
  edge [
    source 25
    target 281
  ]
  edge [
    source 25
    target 908
  ]
  edge [
    source 25
    target 1078
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 1079
  ]
  edge [
    source 25
    target 589
  ]
  edge [
    source 25
    target 300
  ]
  edge [
    source 25
    target 1080
  ]
  edge [
    source 25
    target 1081
  ]
  edge [
    source 25
    target 1082
  ]
  edge [
    source 25
    target 1083
  ]
  edge [
    source 25
    target 486
  ]
  edge [
    source 25
    target 1084
  ]
  edge [
    source 25
    target 441
  ]
  edge [
    source 25
    target 1085
  ]
  edge [
    source 25
    target 1086
  ]
  edge [
    source 25
    target 905
  ]
  edge [
    source 25
    target 1087
  ]
  edge [
    source 25
    target 1088
  ]
  edge [
    source 25
    target 1089
  ]
  edge [
    source 25
    target 1090
  ]
  edge [
    source 25
    target 1091
  ]
  edge [
    source 25
    target 1092
  ]
  edge [
    source 25
    target 1093
  ]
  edge [
    source 25
    target 1094
  ]
  edge [
    source 25
    target 1095
  ]
  edge [
    source 25
    target 1096
  ]
  edge [
    source 25
    target 1097
  ]
  edge [
    source 25
    target 1098
  ]
  edge [
    source 25
    target 1099
  ]
  edge [
    source 25
    target 1100
  ]
  edge [
    source 25
    target 1101
  ]
  edge [
    source 25
    target 1102
  ]
  edge [
    source 25
    target 1103
  ]
  edge [
    source 25
    target 1104
  ]
  edge [
    source 25
    target 1105
  ]
  edge [
    source 25
    target 1106
  ]
  edge [
    source 25
    target 1107
  ]
  edge [
    source 25
    target 1108
  ]
  edge [
    source 25
    target 696
  ]
  edge [
    source 25
    target 809
  ]
  edge [
    source 25
    target 1109
  ]
  edge [
    source 25
    target 1110
  ]
  edge [
    source 25
    target 1111
  ]
  edge [
    source 25
    target 1112
  ]
  edge [
    source 25
    target 1113
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1114
  ]
  edge [
    source 26
    target 1115
  ]
  edge [
    source 26
    target 324
  ]
  edge [
    source 26
    target 73
  ]
  edge [
    source 26
    target 62
  ]
  edge [
    source 26
    target 1116
  ]
  edge [
    source 26
    target 1117
  ]
  edge [
    source 26
    target 1118
  ]
  edge [
    source 26
    target 1119
  ]
  edge [
    source 26
    target 609
  ]
  edge [
    source 26
    target 1120
  ]
  edge [
    source 26
    target 1121
  ]
  edge [
    source 26
    target 550
  ]
  edge [
    source 26
    target 1122
  ]
  edge [
    source 26
    target 1123
  ]
  edge [
    source 26
    target 1124
  ]
  edge [
    source 26
    target 1125
  ]
  edge [
    source 26
    target 1126
  ]
  edge [
    source 26
    target 1127
  ]
  edge [
    source 26
    target 1128
  ]
  edge [
    source 26
    target 327
  ]
  edge [
    source 26
    target 1129
  ]
  edge [
    source 26
    target 1130
  ]
  edge [
    source 26
    target 1131
  ]
  edge [
    source 26
    target 1132
  ]
  edge [
    source 26
    target 807
  ]
  edge [
    source 26
    target 964
  ]
  edge [
    source 26
    target 1133
  ]
  edge [
    source 26
    target 1134
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1003
  ]
  edge [
    source 30
    target 1135
  ]
  edge [
    source 30
    target 1136
  ]
  edge [
    source 30
    target 480
  ]
  edge [
    source 30
    target 1137
  ]
  edge [
    source 30
    target 437
  ]
  edge [
    source 30
    target 1138
  ]
  edge [
    source 30
    target 1139
  ]
  edge [
    source 30
    target 1140
  ]
  edge [
    source 30
    target 1141
  ]
  edge [
    source 30
    target 1142
  ]
  edge [
    source 30
    target 1143
  ]
  edge [
    source 30
    target 602
  ]
  edge [
    source 30
    target 603
  ]
  edge [
    source 30
    target 220
  ]
  edge [
    source 30
    target 606
  ]
  edge [
    source 30
    target 67
  ]
  edge [
    source 30
    target 608
  ]
  edge [
    source 30
    target 92
  ]
  edge [
    source 30
    target 1144
  ]
  edge [
    source 30
    target 1145
  ]
  edge [
    source 30
    target 1146
  ]
  edge [
    source 30
    target 1147
  ]
  edge [
    source 30
    target 1148
  ]
  edge [
    source 30
    target 1149
  ]
  edge [
    source 30
    target 1150
  ]
  edge [
    source 30
    target 1151
  ]
  edge [
    source 30
    target 1152
  ]
  edge [
    source 30
    target 1153
  ]
  edge [
    source 30
    target 1154
  ]
  edge [
    source 30
    target 1155
  ]
  edge [
    source 30
    target 1156
  ]
  edge [
    source 30
    target 1157
  ]
  edge [
    source 30
    target 1158
  ]
  edge [
    source 30
    target 1159
  ]
  edge [
    source 30
    target 1160
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1161
  ]
  edge [
    source 31
    target 1162
  ]
  edge [
    source 31
    target 1163
  ]
  edge [
    source 31
    target 1164
  ]
  edge [
    source 31
    target 1165
  ]
  edge [
    source 31
    target 557
  ]
  edge [
    source 31
    target 1166
  ]
  edge [
    source 31
    target 1167
  ]
  edge [
    source 31
    target 1168
  ]
  edge [
    source 32
    target 1169
  ]
  edge [
    source 32
    target 547
  ]
  edge [
    source 32
    target 1170
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 32
    target 1171
  ]
  edge [
    source 32
    target 40
  ]
  edge [
    source 33
    target 1172
  ]
  edge [
    source 33
    target 1173
  ]
  edge [
    source 33
    target 1174
  ]
  edge [
    source 33
    target 1175
  ]
  edge [
    source 33
    target 1176
  ]
  edge [
    source 33
    target 1177
  ]
  edge [
    source 33
    target 1178
  ]
  edge [
    source 33
    target 1179
  ]
  edge [
    source 33
    target 1180
  ]
  edge [
    source 33
    target 1181
  ]
  edge [
    source 33
    target 1182
  ]
  edge [
    source 33
    target 1183
  ]
  edge [
    source 33
    target 1184
  ]
  edge [
    source 33
    target 1185
  ]
  edge [
    source 33
    target 1186
  ]
  edge [
    source 33
    target 1187
  ]
  edge [
    source 33
    target 1188
  ]
  edge [
    source 33
    target 1189
  ]
  edge [
    source 33
    target 1190
  ]
  edge [
    source 33
    target 1191
  ]
  edge [
    source 33
    target 1192
  ]
  edge [
    source 33
    target 1193
  ]
  edge [
    source 33
    target 1194
  ]
  edge [
    source 33
    target 1195
  ]
  edge [
    source 33
    target 1196
  ]
  edge [
    source 33
    target 1197
  ]
  edge [
    source 33
    target 1198
  ]
  edge [
    source 33
    target 1199
  ]
  edge [
    source 33
    target 1200
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1201
  ]
  edge [
    source 34
    target 1202
  ]
  edge [
    source 34
    target 1203
  ]
  edge [
    source 34
    target 1204
  ]
  edge [
    source 34
    target 1100
  ]
  edge [
    source 34
    target 1205
  ]
  edge [
    source 34
    target 1206
  ]
  edge [
    source 34
    target 1207
  ]
  edge [
    source 34
    target 1208
  ]
  edge [
    source 34
    target 1209
  ]
  edge [
    source 34
    target 1210
  ]
  edge [
    source 34
    target 1211
  ]
  edge [
    source 34
    target 1212
  ]
  edge [
    source 34
    target 1213
  ]
  edge [
    source 34
    target 1214
  ]
  edge [
    source 34
    target 1215
  ]
  edge [
    source 34
    target 1216
  ]
  edge [
    source 35
    target 1217
  ]
  edge [
    source 35
    target 1218
  ]
  edge [
    source 35
    target 153
  ]
  edge [
    source 35
    target 1219
  ]
  edge [
    source 35
    target 1220
  ]
  edge [
    source 35
    target 181
  ]
  edge [
    source 35
    target 1221
  ]
  edge [
    source 35
    target 1222
  ]
  edge [
    source 35
    target 177
  ]
  edge [
    source 35
    target 628
  ]
  edge [
    source 35
    target 1223
  ]
  edge [
    source 35
    target 1224
  ]
  edge [
    source 35
    target 1225
  ]
  edge [
    source 35
    target 1226
  ]
  edge [
    source 35
    target 1227
  ]
  edge [
    source 35
    target 169
  ]
  edge [
    source 35
    target 1228
  ]
  edge [
    source 35
    target 1229
  ]
  edge [
    source 35
    target 1230
  ]
  edge [
    source 35
    target 173
  ]
  edge [
    source 35
    target 217
  ]
  edge [
    source 35
    target 318
  ]
  edge [
    source 35
    target 1231
  ]
  edge [
    source 35
    target 1232
  ]
  edge [
    source 35
    target 1233
  ]
  edge [
    source 35
    target 358
  ]
  edge [
    source 35
    target 1234
  ]
  edge [
    source 35
    target 1235
  ]
  edge [
    source 35
    target 1236
  ]
  edge [
    source 35
    target 1237
  ]
  edge [
    source 35
    target 48
  ]
  edge [
    source 35
    target 1238
  ]
  edge [
    source 35
    target 1239
  ]
  edge [
    source 35
    target 1240
  ]
  edge [
    source 35
    target 1241
  ]
  edge [
    source 35
    target 1242
  ]
  edge [
    source 35
    target 1243
  ]
  edge [
    source 35
    target 379
  ]
  edge [
    source 35
    target 1244
  ]
  edge [
    source 35
    target 1206
  ]
  edge [
    source 35
    target 1245
  ]
  edge [
    source 35
    target 1246
  ]
  edge [
    source 35
    target 1247
  ]
  edge [
    source 35
    target 1248
  ]
  edge [
    source 35
    target 1249
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1250
  ]
  edge [
    source 36
    target 1251
  ]
  edge [
    source 36
    target 1252
  ]
  edge [
    source 36
    target 944
  ]
  edge [
    source 36
    target 1253
  ]
  edge [
    source 36
    target 1254
  ]
  edge [
    source 36
    target 620
  ]
  edge [
    source 36
    target 1255
  ]
  edge [
    source 36
    target 1256
  ]
  edge [
    source 36
    target 1257
  ]
  edge [
    source 36
    target 1258
  ]
  edge [
    source 36
    target 1259
  ]
  edge [
    source 36
    target 914
  ]
  edge [
    source 36
    target 1260
  ]
  edge [
    source 36
    target 1261
  ]
  edge [
    source 36
    target 1262
  ]
  edge [
    source 36
    target 1263
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1264
  ]
  edge [
    source 37
    target 515
  ]
  edge [
    source 37
    target 276
  ]
  edge [
    source 37
    target 178
  ]
  edge [
    source 37
    target 1265
  ]
  edge [
    source 37
    target 1266
  ]
  edge [
    source 37
    target 1267
  ]
  edge [
    source 37
    target 1268
  ]
  edge [
    source 37
    target 1269
  ]
  edge [
    source 37
    target 1270
  ]
  edge [
    source 37
    target 692
  ]
  edge [
    source 37
    target 1037
  ]
  edge [
    source 37
    target 1271
  ]
  edge [
    source 37
    target 1272
  ]
  edge [
    source 37
    target 1273
  ]
  edge [
    source 37
    target 1274
  ]
  edge [
    source 37
    target 872
  ]
  edge [
    source 37
    target 1275
  ]
  edge [
    source 37
    target 1276
  ]
  edge [
    source 37
    target 1277
  ]
  edge [
    source 37
    target 1278
  ]
  edge [
    source 37
    target 1279
  ]
  edge [
    source 37
    target 1280
  ]
  edge [
    source 37
    target 1281
  ]
  edge [
    source 37
    target 1282
  ]
  edge [
    source 37
    target 1283
  ]
  edge [
    source 37
    target 1284
  ]
  edge [
    source 37
    target 215
  ]
  edge [
    source 37
    target 1285
  ]
  edge [
    source 37
    target 808
  ]
  edge [
    source 37
    target 379
  ]
  edge [
    source 37
    target 1286
  ]
  edge [
    source 37
    target 1287
  ]
  edge [
    source 37
    target 1288
  ]
  edge [
    source 37
    target 278
  ]
  edge [
    source 37
    target 1289
  ]
  edge [
    source 37
    target 1290
  ]
  edge [
    source 37
    target 1291
  ]
  edge [
    source 37
    target 1292
  ]
  edge [
    source 37
    target 1293
  ]
  edge [
    source 37
    target 1294
  ]
  edge [
    source 37
    target 1295
  ]
  edge [
    source 37
    target 1296
  ]
  edge [
    source 37
    target 1118
  ]
  edge [
    source 37
    target 1297
  ]
  edge [
    source 37
    target 1298
  ]
  edge [
    source 37
    target 1299
  ]
  edge [
    source 37
    target 1300
  ]
  edge [
    source 37
    target 650
  ]
  edge [
    source 37
    target 621
  ]
  edge [
    source 37
    target 1301
  ]
  edge [
    source 37
    target 1302
  ]
  edge [
    source 37
    target 52
  ]
  edge [
    source 37
    target 1303
  ]
  edge [
    source 37
    target 1304
  ]
  edge [
    source 37
    target 1305
  ]
  edge [
    source 37
    target 695
  ]
  edge [
    source 37
    target 1306
  ]
  edge [
    source 37
    target 1307
  ]
  edge [
    source 37
    target 1308
  ]
  edge [
    source 37
    target 1309
  ]
  edge [
    source 37
    target 1310
  ]
  edge [
    source 37
    target 1311
  ]
  edge [
    source 37
    target 1312
  ]
  edge [
    source 37
    target 1313
  ]
  edge [
    source 37
    target 1314
  ]
  edge [
    source 37
    target 1315
  ]
  edge [
    source 37
    target 1316
  ]
  edge [
    source 37
    target 1317
  ]
  edge [
    source 37
    target 1318
  ]
  edge [
    source 37
    target 1319
  ]
  edge [
    source 37
    target 294
  ]
  edge [
    source 37
    target 90
  ]
  edge [
    source 37
    target 1320
  ]
  edge [
    source 37
    target 1321
  ]
  edge [
    source 37
    target 1322
  ]
  edge [
    source 37
    target 1323
  ]
  edge [
    source 37
    target 1324
  ]
  edge [
    source 37
    target 855
  ]
  edge [
    source 37
    target 177
  ]
  edge [
    source 37
    target 1325
  ]
  edge [
    source 37
    target 1326
  ]
  edge [
    source 37
    target 1327
  ]
  edge [
    source 37
    target 1328
  ]
  edge [
    source 37
    target 1329
  ]
  edge [
    source 37
    target 1330
  ]
  edge [
    source 37
    target 1331
  ]
  edge [
    source 37
    target 1332
  ]
  edge [
    source 37
    target 1333
  ]
  edge [
    source 37
    target 483
  ]
  edge [
    source 37
    target 1334
  ]
  edge [
    source 37
    target 899
  ]
  edge [
    source 37
    target 901
  ]
  edge [
    source 37
    target 1335
  ]
  edge [
    source 37
    target 1127
  ]
  edge [
    source 37
    target 1336
  ]
  edge [
    source 37
    target 1337
  ]
  edge [
    source 37
    target 908
  ]
  edge [
    source 37
    target 1338
  ]
  edge [
    source 37
    target 1339
  ]
  edge [
    source 37
    target 1340
  ]
  edge [
    source 37
    target 1341
  ]
  edge [
    source 37
    target 1342
  ]
  edge [
    source 37
    target 222
  ]
  edge [
    source 37
    target 1343
  ]
  edge [
    source 37
    target 1344
  ]
  edge [
    source 37
    target 1345
  ]
  edge [
    source 37
    target 142
  ]
  edge [
    source 37
    target 1346
  ]
  edge [
    source 37
    target 1347
  ]
  edge [
    source 37
    target 191
  ]
  edge [
    source 37
    target 851
  ]
  edge [
    source 37
    target 1348
  ]
  edge [
    source 37
    target 1349
  ]
  edge [
    source 37
    target 1350
  ]
  edge [
    source 37
    target 845
  ]
  edge [
    source 37
    target 1351
  ]
  edge [
    source 37
    target 1112
  ]
  edge [
    source 37
    target 1352
  ]
  edge [
    source 37
    target 1353
  ]
  edge [
    source 37
    target 1113
  ]
  edge [
    source 37
    target 358
  ]
  edge [
    source 37
    target 1354
  ]
  edge [
    source 37
    target 1355
  ]
  edge [
    source 37
    target 1356
  ]
  edge [
    source 37
    target 1357
  ]
  edge [
    source 37
    target 849
  ]
  edge [
    source 37
    target 1358
  ]
  edge [
    source 37
    target 1359
  ]
  edge [
    source 37
    target 1360
  ]
  edge [
    source 37
    target 1361
  ]
  edge [
    source 37
    target 1362
  ]
  edge [
    source 37
    target 1103
  ]
  edge [
    source 37
    target 1363
  ]
  edge [
    source 37
    target 1364
  ]
  edge [
    source 37
    target 1365
  ]
  edge [
    source 37
    target 1366
  ]
  edge [
    source 37
    target 1367
  ]
  edge [
    source 37
    target 1368
  ]
  edge [
    source 37
    target 1369
  ]
  edge [
    source 37
    target 1370
  ]
  edge [
    source 37
    target 1371
  ]
  edge [
    source 37
    target 1372
  ]
  edge [
    source 37
    target 1373
  ]
  edge [
    source 37
    target 378
  ]
  edge [
    source 37
    target 653
  ]
  edge [
    source 37
    target 1374
  ]
  edge [
    source 37
    target 1375
  ]
  edge [
    source 37
    target 1376
  ]
  edge [
    source 37
    target 1377
  ]
  edge [
    source 37
    target 1378
  ]
  edge [
    source 37
    target 1379
  ]
  edge [
    source 37
    target 1380
  ]
  edge [
    source 37
    target 1381
  ]
  edge [
    source 37
    target 1382
  ]
  edge [
    source 37
    target 354
  ]
  edge [
    source 37
    target 1383
  ]
  edge [
    source 37
    target 1384
  ]
  edge [
    source 37
    target 1385
  ]
  edge [
    source 37
    target 1386
  ]
  edge [
    source 37
    target 1387
  ]
  edge [
    source 37
    target 1388
  ]
  edge [
    source 37
    target 1389
  ]
  edge [
    source 37
    target 1390
  ]
  edge [
    source 37
    target 1391
  ]
  edge [
    source 37
    target 228
  ]
  edge [
    source 37
    target 1392
  ]
  edge [
    source 37
    target 850
  ]
  edge [
    source 37
    target 1393
  ]
  edge [
    source 37
    target 1110
  ]
  edge [
    source 37
    target 1394
  ]
  edge [
    source 37
    target 1395
  ]
  edge [
    source 37
    target 1396
  ]
  edge [
    source 37
    target 1397
  ]
  edge [
    source 37
    target 1398
  ]
  edge [
    source 37
    target 1399
  ]
  edge [
    source 37
    target 1400
  ]
  edge [
    source 37
    target 1401
  ]
  edge [
    source 37
    target 1402
  ]
  edge [
    source 37
    target 1403
  ]
  edge [
    source 37
    target 1404
  ]
  edge [
    source 37
    target 1405
  ]
  edge [
    source 37
    target 1406
  ]
  edge [
    source 37
    target 1407
  ]
  edge [
    source 37
    target 1408
  ]
  edge [
    source 37
    target 1409
  ]
  edge [
    source 37
    target 1410
  ]
  edge [
    source 37
    target 1411
  ]
  edge [
    source 37
    target 1412
  ]
  edge [
    source 37
    target 1413
  ]
  edge [
    source 37
    target 1414
  ]
  edge [
    source 37
    target 1415
  ]
  edge [
    source 37
    target 1416
  ]
  edge [
    source 37
    target 1417
  ]
  edge [
    source 37
    target 1418
  ]
  edge [
    source 37
    target 1419
  ]
  edge [
    source 37
    target 1420
  ]
  edge [
    source 37
    target 1421
  ]
  edge [
    source 37
    target 1422
  ]
  edge [
    source 37
    target 1423
  ]
  edge [
    source 37
    target 1424
  ]
  edge [
    source 37
    target 1425
  ]
  edge [
    source 37
    target 1426
  ]
  edge [
    source 37
    target 1427
  ]
  edge [
    source 37
    target 1428
  ]
  edge [
    source 37
    target 1429
  ]
  edge [
    source 37
    target 1430
  ]
  edge [
    source 37
    target 1431
  ]
  edge [
    source 37
    target 486
  ]
  edge [
    source 37
    target 1432
  ]
  edge [
    source 37
    target 1433
  ]
  edge [
    source 37
    target 1434
  ]
  edge [
    source 37
    target 1435
  ]
  edge [
    source 37
    target 1436
  ]
  edge [
    source 37
    target 1437
  ]
  edge [
    source 37
    target 1438
  ]
  edge [
    source 37
    target 1439
  ]
  edge [
    source 37
    target 1440
  ]
  edge [
    source 37
    target 1441
  ]
  edge [
    source 37
    target 1442
  ]
  edge [
    source 37
    target 1443
  ]
  edge [
    source 37
    target 1444
  ]
  edge [
    source 37
    target 197
  ]
  edge [
    source 37
    target 1445
  ]
  edge [
    source 37
    target 1446
  ]
  edge [
    source 37
    target 1447
  ]
  edge [
    source 37
    target 1448
  ]
  edge [
    source 37
    target 1449
  ]
  edge [
    source 37
    target 649
  ]
  edge [
    source 37
    target 1450
  ]
  edge [
    source 37
    target 1451
  ]
  edge [
    source 37
    target 1452
  ]
  edge [
    source 37
    target 1453
  ]
  edge [
    source 37
    target 1454
  ]
  edge [
    source 37
    target 1455
  ]
  edge [
    source 37
    target 1456
  ]
  edge [
    source 37
    target 1457
  ]
  edge [
    source 37
    target 1458
  ]
  edge [
    source 37
    target 1459
  ]
  edge [
    source 37
    target 1460
  ]
  edge [
    source 37
    target 1461
  ]
  edge [
    source 37
    target 381
  ]
  edge [
    source 37
    target 1462
  ]
  edge [
    source 37
    target 1463
  ]
  edge [
    source 37
    target 1464
  ]
  edge [
    source 37
    target 1465
  ]
  edge [
    source 37
    target 1466
  ]
  edge [
    source 37
    target 1467
  ]
  edge [
    source 37
    target 1468
  ]
  edge [
    source 37
    target 1469
  ]
  edge [
    source 37
    target 1470
  ]
  edge [
    source 37
    target 1471
  ]
  edge [
    source 37
    target 1472
  ]
  edge [
    source 37
    target 1473
  ]
  edge [
    source 37
    target 1474
  ]
  edge [
    source 37
    target 169
  ]
  edge [
    source 37
    target 1475
  ]
  edge [
    source 37
    target 186
  ]
  edge [
    source 37
    target 173
  ]
  edge [
    source 37
    target 1476
  ]
  edge [
    source 37
    target 1477
  ]
  edge [
    source 37
    target 1478
  ]
  edge [
    source 37
    target 1479
  ]
  edge [
    source 37
    target 1480
  ]
  edge [
    source 37
    target 1481
  ]
  edge [
    source 37
    target 1482
  ]
  edge [
    source 37
    target 1483
  ]
  edge [
    source 37
    target 1484
  ]
  edge [
    source 37
    target 1485
  ]
  edge [
    source 37
    target 1486
  ]
  edge [
    source 37
    target 1487
  ]
  edge [
    source 37
    target 1488
  ]
  edge [
    source 37
    target 1489
  ]
  edge [
    source 37
    target 1490
  ]
  edge [
    source 37
    target 1491
  ]
  edge [
    source 37
    target 1492
  ]
  edge [
    source 37
    target 1493
  ]
  edge [
    source 37
    target 1494
  ]
  edge [
    source 37
    target 415
  ]
  edge [
    source 37
    target 1495
  ]
  edge [
    source 37
    target 1496
  ]
  edge [
    source 37
    target 1497
  ]
  edge [
    source 37
    target 1498
  ]
  edge [
    source 37
    target 1499
  ]
  edge [
    source 37
    target 1500
  ]
  edge [
    source 37
    target 1501
  ]
  edge [
    source 37
    target 1502
  ]
  edge [
    source 37
    target 646
  ]
  edge [
    source 37
    target 200
  ]
  edge [
    source 37
    target 1503
  ]
  edge [
    source 37
    target 1504
  ]
  edge [
    source 37
    target 762
  ]
  edge [
    source 37
    target 1505
  ]
  edge [
    source 37
    target 82
  ]
  edge [
    source 37
    target 1506
  ]
  edge [
    source 37
    target 252
  ]
  edge [
    source 37
    target 1507
  ]
  edge [
    source 37
    target 1508
  ]
  edge [
    source 37
    target 1509
  ]
  edge [
    source 37
    target 1510
  ]
  edge [
    source 37
    target 1511
  ]
  edge [
    source 37
    target 1512
  ]
  edge [
    source 37
    target 1513
  ]
  edge [
    source 37
    target 1514
  ]
  edge [
    source 37
    target 1515
  ]
  edge [
    source 37
    target 1516
  ]
  edge [
    source 37
    target 214
  ]
  edge [
    source 37
    target 1517
  ]
  edge [
    source 37
    target 1518
  ]
  edge [
    source 37
    target 1519
  ]
  edge [
    source 37
    target 1520
  ]
  edge [
    source 37
    target 1521
  ]
  edge [
    source 37
    target 1522
  ]
  edge [
    source 37
    target 1523
  ]
  edge [
    source 37
    target 1524
  ]
  edge [
    source 37
    target 1525
  ]
  edge [
    source 37
    target 1526
  ]
  edge [
    source 37
    target 704
  ]
  edge [
    source 37
    target 1527
  ]
  edge [
    source 37
    target 1528
  ]
  edge [
    source 37
    target 1529
  ]
  edge [
    source 37
    target 1530
  ]
  edge [
    source 37
    target 1531
  ]
  edge [
    source 37
    target 1532
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 1533
  ]
  edge [
    source 37
    target 1534
  ]
  edge [
    source 37
    target 233
  ]
  edge [
    source 37
    target 1535
  ]
  edge [
    source 37
    target 1536
  ]
  edge [
    source 37
    target 1537
  ]
  edge [
    source 37
    target 1538
  ]
  edge [
    source 37
    target 1539
  ]
  edge [
    source 37
    target 1540
  ]
  edge [
    source 37
    target 1541
  ]
  edge [
    source 37
    target 1542
  ]
  edge [
    source 37
    target 1543
  ]
  edge [
    source 37
    target 1544
  ]
  edge [
    source 37
    target 1545
  ]
  edge [
    source 37
    target 1546
  ]
  edge [
    source 37
    target 1547
  ]
  edge [
    source 37
    target 1548
  ]
  edge [
    source 37
    target 1549
  ]
  edge [
    source 37
    target 1550
  ]
  edge [
    source 37
    target 1551
  ]
  edge [
    source 37
    target 1552
  ]
  edge [
    source 37
    target 1553
  ]
  edge [
    source 37
    target 1554
  ]
  edge [
    source 37
    target 1555
  ]
  edge [
    source 37
    target 1556
  ]
  edge [
    source 37
    target 1557
  ]
  edge [
    source 37
    target 1558
  ]
  edge [
    source 37
    target 1559
  ]
  edge [
    source 37
    target 1560
  ]
  edge [
    source 37
    target 1561
  ]
  edge [
    source 37
    target 1562
  ]
  edge [
    source 37
    target 1563
  ]
  edge [
    source 37
    target 1564
  ]
  edge [
    source 37
    target 1565
  ]
  edge [
    source 37
    target 1566
  ]
  edge [
    source 37
    target 1567
  ]
  edge [
    source 37
    target 1568
  ]
  edge [
    source 37
    target 1569
  ]
  edge [
    source 37
    target 476
  ]
  edge [
    source 37
    target 1570
  ]
  edge [
    source 37
    target 1571
  ]
  edge [
    source 37
    target 1572
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 47
  ]
  edge [
    source 39
    target 1496
  ]
  edge [
    source 39
    target 192
  ]
  edge [
    source 39
    target 1573
  ]
  edge [
    source 39
    target 1574
  ]
  edge [
    source 39
    target 646
  ]
  edge [
    source 39
    target 1575
  ]
  edge [
    source 39
    target 1576
  ]
  edge [
    source 39
    target 888
  ]
  edge [
    source 39
    target 1577
  ]
  edge [
    source 39
    target 1578
  ]
  edge [
    source 39
    target 1579
  ]
  edge [
    source 39
    target 388
  ]
  edge [
    source 39
    target 1580
  ]
  edge [
    source 39
    target 984
  ]
  edge [
    source 39
    target 149
  ]
  edge [
    source 39
    target 231
  ]
  edge [
    source 39
    target 1581
  ]
  edge [
    source 39
    target 1582
  ]
  edge [
    source 39
    target 1583
  ]
  edge [
    source 39
    target 1584
  ]
  edge [
    source 39
    target 1585
  ]
  edge [
    source 39
    target 178
  ]
  edge [
    source 39
    target 190
  ]
  edge [
    source 39
    target 191
  ]
  edge [
    source 39
    target 193
  ]
  edge [
    source 39
    target 194
  ]
  edge [
    source 39
    target 195
  ]
  edge [
    source 39
    target 196
  ]
  edge [
    source 39
    target 197
  ]
  edge [
    source 39
    target 198
  ]
  edge [
    source 39
    target 199
  ]
  edge [
    source 39
    target 200
  ]
  edge [
    source 39
    target 201
  ]
  edge [
    source 39
    target 202
  ]
  edge [
    source 39
    target 203
  ]
  edge [
    source 39
    target 204
  ]
  edge [
    source 39
    target 205
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1586
  ]
  edge [
    source 40
    target 457
  ]
  edge [
    source 40
    target 1587
  ]
  edge [
    source 40
    target 335
  ]
  edge [
    source 40
    target 1588
  ]
  edge [
    source 40
    target 1589
  ]
  edge [
    source 40
    target 1590
  ]
  edge [
    source 40
    target 1591
  ]
  edge [
    source 40
    target 1592
  ]
  edge [
    source 40
    target 1593
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 1549
  ]
  edge [
    source 40
    target 1594
  ]
  edge [
    source 40
    target 1595
  ]
  edge [
    source 40
    target 1596
  ]
  edge [
    source 40
    target 1597
  ]
  edge [
    source 40
    target 1598
  ]
  edge [
    source 40
    target 1599
  ]
  edge [
    source 40
    target 1069
  ]
  edge [
    source 40
    target 1600
  ]
  edge [
    source 40
    target 1601
  ]
  edge [
    source 40
    target 1602
  ]
  edge [
    source 40
    target 458
  ]
  edge [
    source 40
    target 1603
  ]
  edge [
    source 40
    target 537
  ]
  edge [
    source 40
    target 1604
  ]
  edge [
    source 40
    target 1605
  ]
  edge [
    source 40
    target 533
  ]
  edge [
    source 40
    target 1606
  ]
  edge [
    source 40
    target 1607
  ]
  edge [
    source 40
    target 1608
  ]
  edge [
    source 40
    target 1609
  ]
  edge [
    source 40
    target 1610
  ]
  edge [
    source 40
    target 1611
  ]
  edge [
    source 40
    target 1612
  ]
  edge [
    source 40
    target 1613
  ]
  edge [
    source 40
    target 1614
  ]
  edge [
    source 40
    target 1615
  ]
  edge [
    source 40
    target 1616
  ]
  edge [
    source 40
    target 1617
  ]
  edge [
    source 40
    target 1618
  ]
  edge [
    source 40
    target 1619
  ]
  edge [
    source 40
    target 1620
  ]
  edge [
    source 40
    target 1621
  ]
  edge [
    source 40
    target 1622
  ]
  edge [
    source 40
    target 1623
  ]
  edge [
    source 40
    target 453
  ]
  edge [
    source 40
    target 1624
  ]
  edge [
    source 40
    target 1625
  ]
  edge [
    source 40
    target 1626
  ]
  edge [
    source 40
    target 1185
  ]
  edge [
    source 40
    target 1627
  ]
  edge [
    source 40
    target 1628
  ]
  edge [
    source 40
    target 1629
  ]
  edge [
    source 40
    target 1630
  ]
  edge [
    source 40
    target 1631
  ]
  edge [
    source 40
    target 1632
  ]
  edge [
    source 40
    target 1633
  ]
  edge [
    source 40
    target 1634
  ]
  edge [
    source 40
    target 1635
  ]
  edge [
    source 40
    target 1636
  ]
  edge [
    source 40
    target 1637
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1619
  ]
  edge [
    source 42
    target 1169
  ]
]
