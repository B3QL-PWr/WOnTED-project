graph [
  node [
    id 0
    label "dzienia"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pan"
    origin "text"
  ]
  node [
    id 4
    label "karolin"
    origin "text"
  ]
  node [
    id 5
    label "tutaj"
    origin "text"
  ]
  node [
    id 6
    label "&#347;wi&#281;tokrzyski"
    origin "text"
  ]
  node [
    id 7
    label "sto"
    origin "text"
  ]
  node [
    id 8
    label "pi&#281;&#263;dziesi&#261;t"
    origin "text"
  ]
  node [
    id 9
    label "pi&#281;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "jecha&#263;"
    origin "text"
  ]
  node [
    id 11
    label "prawda"
    origin "text"
  ]
  node [
    id 12
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 13
    label "skuteczny"
  ]
  node [
    id 14
    label "ca&#322;y"
  ]
  node [
    id 15
    label "czw&#243;rka"
  ]
  node [
    id 16
    label "spokojny"
  ]
  node [
    id 17
    label "pos&#322;uszny"
  ]
  node [
    id 18
    label "korzystny"
  ]
  node [
    id 19
    label "drogi"
  ]
  node [
    id 20
    label "pozytywny"
  ]
  node [
    id 21
    label "moralny"
  ]
  node [
    id 22
    label "pomy&#347;lny"
  ]
  node [
    id 23
    label "powitanie"
  ]
  node [
    id 24
    label "grzeczny"
  ]
  node [
    id 25
    label "&#347;mieszny"
  ]
  node [
    id 26
    label "odpowiedni"
  ]
  node [
    id 27
    label "zwrot"
  ]
  node [
    id 28
    label "dobrze"
  ]
  node [
    id 29
    label "dobroczynny"
  ]
  node [
    id 30
    label "mi&#322;y"
  ]
  node [
    id 31
    label "etycznie"
  ]
  node [
    id 32
    label "moralnie"
  ]
  node [
    id 33
    label "warto&#347;ciowy"
  ]
  node [
    id 34
    label "taki"
  ]
  node [
    id 35
    label "stosownie"
  ]
  node [
    id 36
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 37
    label "prawdziwy"
  ]
  node [
    id 38
    label "typowy"
  ]
  node [
    id 39
    label "zasadniczy"
  ]
  node [
    id 40
    label "charakterystyczny"
  ]
  node [
    id 41
    label "uprawniony"
  ]
  node [
    id 42
    label "nale&#380;yty"
  ]
  node [
    id 43
    label "ten"
  ]
  node [
    id 44
    label "nale&#380;ny"
  ]
  node [
    id 45
    label "pozytywnie"
  ]
  node [
    id 46
    label "fajny"
  ]
  node [
    id 47
    label "przyjemny"
  ]
  node [
    id 48
    label "po&#380;&#261;dany"
  ]
  node [
    id 49
    label "dodatnio"
  ]
  node [
    id 50
    label "o&#347;mieszenie"
  ]
  node [
    id 51
    label "o&#347;mieszanie"
  ]
  node [
    id 52
    label "&#347;miesznie"
  ]
  node [
    id 53
    label "nieadekwatny"
  ]
  node [
    id 54
    label "bawny"
  ]
  node [
    id 55
    label "niepowa&#380;ny"
  ]
  node [
    id 56
    label "dziwny"
  ]
  node [
    id 57
    label "uspokojenie_si&#281;"
  ]
  node [
    id 58
    label "wolny"
  ]
  node [
    id 59
    label "bezproblemowy"
  ]
  node [
    id 60
    label "uspokajanie_si&#281;"
  ]
  node [
    id 61
    label "spokojnie"
  ]
  node [
    id 62
    label "uspokojenie"
  ]
  node [
    id 63
    label "nietrudny"
  ]
  node [
    id 64
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 65
    label "cicho"
  ]
  node [
    id 66
    label "uspokajanie"
  ]
  node [
    id 67
    label "pos&#322;usznie"
  ]
  node [
    id 68
    label "zale&#380;ny"
  ]
  node [
    id 69
    label "uleg&#322;y"
  ]
  node [
    id 70
    label "konserwatywny"
  ]
  node [
    id 71
    label "stosowny"
  ]
  node [
    id 72
    label "grzecznie"
  ]
  node [
    id 73
    label "nijaki"
  ]
  node [
    id 74
    label "niewinny"
  ]
  node [
    id 75
    label "korzystnie"
  ]
  node [
    id 76
    label "cz&#322;owiek"
  ]
  node [
    id 77
    label "przyjaciel"
  ]
  node [
    id 78
    label "bliski"
  ]
  node [
    id 79
    label "drogo"
  ]
  node [
    id 80
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 81
    label "kompletny"
  ]
  node [
    id 82
    label "zdr&#243;w"
  ]
  node [
    id 83
    label "ca&#322;o"
  ]
  node [
    id 84
    label "du&#380;y"
  ]
  node [
    id 85
    label "calu&#347;ko"
  ]
  node [
    id 86
    label "podobny"
  ]
  node [
    id 87
    label "&#380;ywy"
  ]
  node [
    id 88
    label "pe&#322;ny"
  ]
  node [
    id 89
    label "jedyny"
  ]
  node [
    id 90
    label "sprawny"
  ]
  node [
    id 91
    label "skutkowanie"
  ]
  node [
    id 92
    label "poskutkowanie"
  ]
  node [
    id 93
    label "skutecznie"
  ]
  node [
    id 94
    label "pomy&#347;lnie"
  ]
  node [
    id 95
    label "zbi&#243;r"
  ]
  node [
    id 96
    label "przedtrzonowiec"
  ]
  node [
    id 97
    label "trafienie"
  ]
  node [
    id 98
    label "osada"
  ]
  node [
    id 99
    label "blotka"
  ]
  node [
    id 100
    label "p&#322;yta_winylowa"
  ]
  node [
    id 101
    label "cyfra"
  ]
  node [
    id 102
    label "pok&#243;j"
  ]
  node [
    id 103
    label "obiekt"
  ]
  node [
    id 104
    label "stopie&#324;"
  ]
  node [
    id 105
    label "arkusz_drukarski"
  ]
  node [
    id 106
    label "zaprz&#281;g"
  ]
  node [
    id 107
    label "toto-lotek"
  ]
  node [
    id 108
    label "&#263;wiartka"
  ]
  node [
    id 109
    label "&#322;&#243;dka"
  ]
  node [
    id 110
    label "four"
  ]
  node [
    id 111
    label "minialbum"
  ]
  node [
    id 112
    label "hotel"
  ]
  node [
    id 113
    label "punkt"
  ]
  node [
    id 114
    label "zmiana"
  ]
  node [
    id 115
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 116
    label "turn"
  ]
  node [
    id 117
    label "wyra&#380;enie"
  ]
  node [
    id 118
    label "fraza_czasownikowa"
  ]
  node [
    id 119
    label "turning"
  ]
  node [
    id 120
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 121
    label "skr&#281;t"
  ]
  node [
    id 122
    label "jednostka_leksykalna"
  ]
  node [
    id 123
    label "obr&#243;t"
  ]
  node [
    id 124
    label "spotkanie"
  ]
  node [
    id 125
    label "pozdrowienie"
  ]
  node [
    id 126
    label "welcome"
  ]
  node [
    id 127
    label "zwyczaj"
  ]
  node [
    id 128
    label "greeting"
  ]
  node [
    id 129
    label "zdarzony"
  ]
  node [
    id 130
    label "odpowiednio"
  ]
  node [
    id 131
    label "specjalny"
  ]
  node [
    id 132
    label "odpowiadanie"
  ]
  node [
    id 133
    label "wybranek"
  ]
  node [
    id 134
    label "sk&#322;onny"
  ]
  node [
    id 135
    label "kochanek"
  ]
  node [
    id 136
    label "mi&#322;o"
  ]
  node [
    id 137
    label "dyplomata"
  ]
  node [
    id 138
    label "umi&#322;owany"
  ]
  node [
    id 139
    label "kochanie"
  ]
  node [
    id 140
    label "przyjemnie"
  ]
  node [
    id 141
    label "wiele"
  ]
  node [
    id 142
    label "lepiej"
  ]
  node [
    id 143
    label "dobroczynnie"
  ]
  node [
    id 144
    label "spo&#322;eczny"
  ]
  node [
    id 145
    label "pies"
  ]
  node [
    id 146
    label "suffice"
  ]
  node [
    id 147
    label "invite"
  ]
  node [
    id 148
    label "trwa&#263;"
  ]
  node [
    id 149
    label "zach&#281;ca&#263;"
  ]
  node [
    id 150
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 151
    label "ask"
  ]
  node [
    id 152
    label "preach"
  ]
  node [
    id 153
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 154
    label "zaprasza&#263;"
  ]
  node [
    id 155
    label "poleca&#263;"
  ]
  node [
    id 156
    label "zezwala&#263;"
  ]
  node [
    id 157
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 158
    label "oferowa&#263;"
  ]
  node [
    id 159
    label "adhere"
  ]
  node [
    id 160
    label "pozostawa&#263;"
  ]
  node [
    id 161
    label "stand"
  ]
  node [
    id 162
    label "zostawa&#263;"
  ]
  node [
    id 163
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 164
    label "istnie&#263;"
  ]
  node [
    id 165
    label "placard"
  ]
  node [
    id 166
    label "wydawa&#263;"
  ]
  node [
    id 167
    label "doradza&#263;"
  ]
  node [
    id 168
    label "m&#243;wi&#263;"
  ]
  node [
    id 169
    label "zadawa&#263;"
  ]
  node [
    id 170
    label "control"
  ]
  node [
    id 171
    label "ordynowa&#263;"
  ]
  node [
    id 172
    label "powierza&#263;"
  ]
  node [
    id 173
    label "charge"
  ]
  node [
    id 174
    label "pozyskiwa&#263;"
  ]
  node [
    id 175
    label "act"
  ]
  node [
    id 176
    label "uznawa&#263;"
  ]
  node [
    id 177
    label "authorize"
  ]
  node [
    id 178
    label "dogoterapia"
  ]
  node [
    id 179
    label "wycie"
  ]
  node [
    id 180
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 181
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 182
    label "sobaka"
  ]
  node [
    id 183
    label "Cerber"
  ]
  node [
    id 184
    label "trufla"
  ]
  node [
    id 185
    label "istota_&#380;ywa"
  ]
  node [
    id 186
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 187
    label "rakarz"
  ]
  node [
    id 188
    label "wyzwisko"
  ]
  node [
    id 189
    label "spragniony"
  ]
  node [
    id 190
    label "szczucie"
  ]
  node [
    id 191
    label "policjant"
  ]
  node [
    id 192
    label "piese&#322;"
  ]
  node [
    id 193
    label "samiec"
  ]
  node [
    id 194
    label "s&#322;u&#380;enie"
  ]
  node [
    id 195
    label "czworon&#243;g"
  ]
  node [
    id 196
    label "kabanos"
  ]
  node [
    id 197
    label "psowate"
  ]
  node [
    id 198
    label "szczeka&#263;"
  ]
  node [
    id 199
    label "wy&#263;"
  ]
  node [
    id 200
    label "szczu&#263;"
  ]
  node [
    id 201
    label "&#322;ajdak"
  ]
  node [
    id 202
    label "zawy&#263;"
  ]
  node [
    id 203
    label "murza"
  ]
  node [
    id 204
    label "belfer"
  ]
  node [
    id 205
    label "szkolnik"
  ]
  node [
    id 206
    label "pupil"
  ]
  node [
    id 207
    label "ojciec"
  ]
  node [
    id 208
    label "kszta&#322;ciciel"
  ]
  node [
    id 209
    label "Midas"
  ]
  node [
    id 210
    label "przyw&#243;dca"
  ]
  node [
    id 211
    label "opiekun"
  ]
  node [
    id 212
    label "Mieszko_I"
  ]
  node [
    id 213
    label "doros&#322;y"
  ]
  node [
    id 214
    label "pracodawca"
  ]
  node [
    id 215
    label "profesor"
  ]
  node [
    id 216
    label "m&#261;&#380;"
  ]
  node [
    id 217
    label "rz&#261;dzenie"
  ]
  node [
    id 218
    label "bogaty"
  ]
  node [
    id 219
    label "pa&#324;stwo"
  ]
  node [
    id 220
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 221
    label "w&#322;odarz"
  ]
  node [
    id 222
    label "nabab"
  ]
  node [
    id 223
    label "preceptor"
  ]
  node [
    id 224
    label "pedagog"
  ]
  node [
    id 225
    label "efendi"
  ]
  node [
    id 226
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 227
    label "popularyzator"
  ]
  node [
    id 228
    label "gra_w_karty"
  ]
  node [
    id 229
    label "jegomo&#347;&#263;"
  ]
  node [
    id 230
    label "androlog"
  ]
  node [
    id 231
    label "bratek"
  ]
  node [
    id 232
    label "andropauza"
  ]
  node [
    id 233
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 234
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 235
    label "ch&#322;opina"
  ]
  node [
    id 236
    label "w&#322;adza"
  ]
  node [
    id 237
    label "Sabataj_Cwi"
  ]
  node [
    id 238
    label "lider"
  ]
  node [
    id 239
    label "Mao"
  ]
  node [
    id 240
    label "Anders"
  ]
  node [
    id 241
    label "Fidel_Castro"
  ]
  node [
    id 242
    label "Miko&#322;ajczyk"
  ]
  node [
    id 243
    label "Tito"
  ]
  node [
    id 244
    label "Ko&#347;ciuszko"
  ]
  node [
    id 245
    label "p&#322;atnik"
  ]
  node [
    id 246
    label "zwierzchnik"
  ]
  node [
    id 247
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 248
    label "nadzorca"
  ]
  node [
    id 249
    label "funkcjonariusz"
  ]
  node [
    id 250
    label "podmiot"
  ]
  node [
    id 251
    label "wykupywanie"
  ]
  node [
    id 252
    label "wykupienie"
  ]
  node [
    id 253
    label "bycie_w_posiadaniu"
  ]
  node [
    id 254
    label "rozszerzyciel"
  ]
  node [
    id 255
    label "asymilowa&#263;"
  ]
  node [
    id 256
    label "nasada"
  ]
  node [
    id 257
    label "profanum"
  ]
  node [
    id 258
    label "wz&#243;r"
  ]
  node [
    id 259
    label "senior"
  ]
  node [
    id 260
    label "asymilowanie"
  ]
  node [
    id 261
    label "os&#322;abia&#263;"
  ]
  node [
    id 262
    label "homo_sapiens"
  ]
  node [
    id 263
    label "osoba"
  ]
  node [
    id 264
    label "ludzko&#347;&#263;"
  ]
  node [
    id 265
    label "Adam"
  ]
  node [
    id 266
    label "hominid"
  ]
  node [
    id 267
    label "posta&#263;"
  ]
  node [
    id 268
    label "portrecista"
  ]
  node [
    id 269
    label "polifag"
  ]
  node [
    id 270
    label "podw&#322;adny"
  ]
  node [
    id 271
    label "dwun&#243;g"
  ]
  node [
    id 272
    label "wapniak"
  ]
  node [
    id 273
    label "duch"
  ]
  node [
    id 274
    label "os&#322;abianie"
  ]
  node [
    id 275
    label "antropochoria"
  ]
  node [
    id 276
    label "figura"
  ]
  node [
    id 277
    label "g&#322;owa"
  ]
  node [
    id 278
    label "mikrokosmos"
  ]
  node [
    id 279
    label "oddzia&#322;ywanie"
  ]
  node [
    id 280
    label "dojrza&#322;y"
  ]
  node [
    id 281
    label "dojrzale"
  ]
  node [
    id 282
    label "doro&#347;lenie"
  ]
  node [
    id 283
    label "wydoro&#347;lenie"
  ]
  node [
    id 284
    label "m&#261;dry"
  ]
  node [
    id 285
    label "&#378;ra&#322;y"
  ]
  node [
    id 286
    label "doletni"
  ]
  node [
    id 287
    label "doro&#347;le"
  ]
  node [
    id 288
    label "starosta"
  ]
  node [
    id 289
    label "w&#322;adca"
  ]
  node [
    id 290
    label "zarz&#261;dca"
  ]
  node [
    id 291
    label "nauczyciel"
  ]
  node [
    id 292
    label "autor"
  ]
  node [
    id 293
    label "szko&#322;a"
  ]
  node [
    id 294
    label "tarcza"
  ]
  node [
    id 295
    label "klasa"
  ]
  node [
    id 296
    label "elew"
  ]
  node [
    id 297
    label "wyprawka"
  ]
  node [
    id 298
    label "mundurek"
  ]
  node [
    id 299
    label "absolwent"
  ]
  node [
    id 300
    label "nauczyciel_akademicki"
  ]
  node [
    id 301
    label "tytu&#322;"
  ]
  node [
    id 302
    label "stopie&#324;_naukowy"
  ]
  node [
    id 303
    label "konsulent"
  ]
  node [
    id 304
    label "profesura"
  ]
  node [
    id 305
    label "wirtuoz"
  ]
  node [
    id 306
    label "ochotnik"
  ]
  node [
    id 307
    label "nauczyciel_muzyki"
  ]
  node [
    id 308
    label "pomocnik"
  ]
  node [
    id 309
    label "zakonnik"
  ]
  node [
    id 310
    label "student"
  ]
  node [
    id 311
    label "ekspert"
  ]
  node [
    id 312
    label "bogacz"
  ]
  node [
    id 313
    label "dostojnik"
  ]
  node [
    id 314
    label "urz&#281;dnik"
  ]
  node [
    id 315
    label "mo&#347;&#263;"
  ]
  node [
    id 316
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 317
    label "&#347;w"
  ]
  node [
    id 318
    label "rodzic"
  ]
  node [
    id 319
    label "pomys&#322;odawca"
  ]
  node [
    id 320
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 321
    label "rodzice"
  ]
  node [
    id 322
    label "wykonawca"
  ]
  node [
    id 323
    label "stary"
  ]
  node [
    id 324
    label "kuwada"
  ]
  node [
    id 325
    label "ojczym"
  ]
  node [
    id 326
    label "papa"
  ]
  node [
    id 327
    label "przodek"
  ]
  node [
    id 328
    label "tworzyciel"
  ]
  node [
    id 329
    label "zwierz&#281;"
  ]
  node [
    id 330
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 331
    label "facet"
  ]
  node [
    id 332
    label "fio&#322;ek"
  ]
  node [
    id 333
    label "brat"
  ]
  node [
    id 334
    label "pan_i_w&#322;adca"
  ]
  node [
    id 335
    label "pan_m&#322;ody"
  ]
  node [
    id 336
    label "ch&#322;op"
  ]
  node [
    id 337
    label "&#347;lubny"
  ]
  node [
    id 338
    label "m&#243;j"
  ]
  node [
    id 339
    label "pan_domu"
  ]
  node [
    id 340
    label "ma&#322;&#380;onek"
  ]
  node [
    id 341
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 342
    label "Frygia"
  ]
  node [
    id 343
    label "dominowanie"
  ]
  node [
    id 344
    label "reign"
  ]
  node [
    id 345
    label "sprawowanie"
  ]
  node [
    id 346
    label "dominion"
  ]
  node [
    id 347
    label "rule"
  ]
  node [
    id 348
    label "zwierz&#281;_domowe"
  ]
  node [
    id 349
    label "John_Dewey"
  ]
  node [
    id 350
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 351
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 352
    label "J&#281;drzejewicz"
  ]
  node [
    id 353
    label "specjalista"
  ]
  node [
    id 354
    label "&#380;ycie"
  ]
  node [
    id 355
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 356
    label "Turek"
  ]
  node [
    id 357
    label "effendi"
  ]
  node [
    id 358
    label "och&#281;do&#380;ny"
  ]
  node [
    id 359
    label "zapa&#347;ny"
  ]
  node [
    id 360
    label "sytuowany"
  ]
  node [
    id 361
    label "obfituj&#261;cy"
  ]
  node [
    id 362
    label "forsiasty"
  ]
  node [
    id 363
    label "spania&#322;y"
  ]
  node [
    id 364
    label "obficie"
  ]
  node [
    id 365
    label "r&#243;&#380;norodny"
  ]
  node [
    id 366
    label "bogato"
  ]
  node [
    id 367
    label "Japonia"
  ]
  node [
    id 368
    label "Zair"
  ]
  node [
    id 369
    label "Belize"
  ]
  node [
    id 370
    label "San_Marino"
  ]
  node [
    id 371
    label "Tanzania"
  ]
  node [
    id 372
    label "Antigua_i_Barbuda"
  ]
  node [
    id 373
    label "granica_pa&#324;stwa"
  ]
  node [
    id 374
    label "Senegal"
  ]
  node [
    id 375
    label "Indie"
  ]
  node [
    id 376
    label "Seszele"
  ]
  node [
    id 377
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 378
    label "Zimbabwe"
  ]
  node [
    id 379
    label "Filipiny"
  ]
  node [
    id 380
    label "Mauretania"
  ]
  node [
    id 381
    label "Malezja"
  ]
  node [
    id 382
    label "Rumunia"
  ]
  node [
    id 383
    label "Surinam"
  ]
  node [
    id 384
    label "Ukraina"
  ]
  node [
    id 385
    label "Syria"
  ]
  node [
    id 386
    label "Wyspy_Marshalla"
  ]
  node [
    id 387
    label "Burkina_Faso"
  ]
  node [
    id 388
    label "Grecja"
  ]
  node [
    id 389
    label "Polska"
  ]
  node [
    id 390
    label "Wenezuela"
  ]
  node [
    id 391
    label "Nepal"
  ]
  node [
    id 392
    label "Suazi"
  ]
  node [
    id 393
    label "S&#322;owacja"
  ]
  node [
    id 394
    label "Algieria"
  ]
  node [
    id 395
    label "Chiny"
  ]
  node [
    id 396
    label "Grenada"
  ]
  node [
    id 397
    label "Barbados"
  ]
  node [
    id 398
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 399
    label "Pakistan"
  ]
  node [
    id 400
    label "Niemcy"
  ]
  node [
    id 401
    label "Bahrajn"
  ]
  node [
    id 402
    label "Komory"
  ]
  node [
    id 403
    label "Australia"
  ]
  node [
    id 404
    label "Rodezja"
  ]
  node [
    id 405
    label "Malawi"
  ]
  node [
    id 406
    label "Gwinea"
  ]
  node [
    id 407
    label "Wehrlen"
  ]
  node [
    id 408
    label "Meksyk"
  ]
  node [
    id 409
    label "Liechtenstein"
  ]
  node [
    id 410
    label "Czarnog&#243;ra"
  ]
  node [
    id 411
    label "Wielka_Brytania"
  ]
  node [
    id 412
    label "Kuwejt"
  ]
  node [
    id 413
    label "Angola"
  ]
  node [
    id 414
    label "Monako"
  ]
  node [
    id 415
    label "Jemen"
  ]
  node [
    id 416
    label "Etiopia"
  ]
  node [
    id 417
    label "Madagaskar"
  ]
  node [
    id 418
    label "terytorium"
  ]
  node [
    id 419
    label "Kolumbia"
  ]
  node [
    id 420
    label "Portoryko"
  ]
  node [
    id 421
    label "Mauritius"
  ]
  node [
    id 422
    label "Kostaryka"
  ]
  node [
    id 423
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 424
    label "Tajlandia"
  ]
  node [
    id 425
    label "Argentyna"
  ]
  node [
    id 426
    label "Zambia"
  ]
  node [
    id 427
    label "Sri_Lanka"
  ]
  node [
    id 428
    label "Gwatemala"
  ]
  node [
    id 429
    label "Kirgistan"
  ]
  node [
    id 430
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 431
    label "Hiszpania"
  ]
  node [
    id 432
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 433
    label "Salwador"
  ]
  node [
    id 434
    label "Korea"
  ]
  node [
    id 435
    label "Macedonia"
  ]
  node [
    id 436
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 437
    label "Brunei"
  ]
  node [
    id 438
    label "Mozambik"
  ]
  node [
    id 439
    label "Turcja"
  ]
  node [
    id 440
    label "Kambod&#380;a"
  ]
  node [
    id 441
    label "Benin"
  ]
  node [
    id 442
    label "Bhutan"
  ]
  node [
    id 443
    label "Tunezja"
  ]
  node [
    id 444
    label "Austria"
  ]
  node [
    id 445
    label "Izrael"
  ]
  node [
    id 446
    label "Sierra_Leone"
  ]
  node [
    id 447
    label "Jamajka"
  ]
  node [
    id 448
    label "Rosja"
  ]
  node [
    id 449
    label "Rwanda"
  ]
  node [
    id 450
    label "holoarktyka"
  ]
  node [
    id 451
    label "Nigeria"
  ]
  node [
    id 452
    label "USA"
  ]
  node [
    id 453
    label "Oman"
  ]
  node [
    id 454
    label "Luksemburg"
  ]
  node [
    id 455
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 456
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 457
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 458
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 459
    label "Dominikana"
  ]
  node [
    id 460
    label "Irlandia"
  ]
  node [
    id 461
    label "Liban"
  ]
  node [
    id 462
    label "Hanower"
  ]
  node [
    id 463
    label "Estonia"
  ]
  node [
    id 464
    label "Samoa"
  ]
  node [
    id 465
    label "Nowa_Zelandia"
  ]
  node [
    id 466
    label "Gabon"
  ]
  node [
    id 467
    label "Iran"
  ]
  node [
    id 468
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 469
    label "S&#322;owenia"
  ]
  node [
    id 470
    label "Egipt"
  ]
  node [
    id 471
    label "Kiribati"
  ]
  node [
    id 472
    label "Togo"
  ]
  node [
    id 473
    label "Mongolia"
  ]
  node [
    id 474
    label "Sudan"
  ]
  node [
    id 475
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 476
    label "Bahamy"
  ]
  node [
    id 477
    label "Bangladesz"
  ]
  node [
    id 478
    label "partia"
  ]
  node [
    id 479
    label "Serbia"
  ]
  node [
    id 480
    label "Czechy"
  ]
  node [
    id 481
    label "Holandia"
  ]
  node [
    id 482
    label "Birma"
  ]
  node [
    id 483
    label "Albania"
  ]
  node [
    id 484
    label "Mikronezja"
  ]
  node [
    id 485
    label "Gambia"
  ]
  node [
    id 486
    label "Kazachstan"
  ]
  node [
    id 487
    label "interior"
  ]
  node [
    id 488
    label "Uzbekistan"
  ]
  node [
    id 489
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 490
    label "Malta"
  ]
  node [
    id 491
    label "Lesoto"
  ]
  node [
    id 492
    label "para"
  ]
  node [
    id 493
    label "Antarktis"
  ]
  node [
    id 494
    label "Andora"
  ]
  node [
    id 495
    label "Nauru"
  ]
  node [
    id 496
    label "Kuba"
  ]
  node [
    id 497
    label "Wietnam"
  ]
  node [
    id 498
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 499
    label "ziemia"
  ]
  node [
    id 500
    label "Chorwacja"
  ]
  node [
    id 501
    label "Kamerun"
  ]
  node [
    id 502
    label "Urugwaj"
  ]
  node [
    id 503
    label "Niger"
  ]
  node [
    id 504
    label "Turkmenistan"
  ]
  node [
    id 505
    label "Szwajcaria"
  ]
  node [
    id 506
    label "organizacja"
  ]
  node [
    id 507
    label "grupa"
  ]
  node [
    id 508
    label "Litwa"
  ]
  node [
    id 509
    label "Palau"
  ]
  node [
    id 510
    label "Gruzja"
  ]
  node [
    id 511
    label "Kongo"
  ]
  node [
    id 512
    label "Tajwan"
  ]
  node [
    id 513
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 514
    label "Honduras"
  ]
  node [
    id 515
    label "Boliwia"
  ]
  node [
    id 516
    label "Uganda"
  ]
  node [
    id 517
    label "Namibia"
  ]
  node [
    id 518
    label "Erytrea"
  ]
  node [
    id 519
    label "Azerbejd&#380;an"
  ]
  node [
    id 520
    label "Panama"
  ]
  node [
    id 521
    label "Gujana"
  ]
  node [
    id 522
    label "Somalia"
  ]
  node [
    id 523
    label "Burundi"
  ]
  node [
    id 524
    label "Tuwalu"
  ]
  node [
    id 525
    label "Libia"
  ]
  node [
    id 526
    label "Katar"
  ]
  node [
    id 527
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 528
    label "Trynidad_i_Tobago"
  ]
  node [
    id 529
    label "Sahara_Zachodnia"
  ]
  node [
    id 530
    label "Gwinea_Bissau"
  ]
  node [
    id 531
    label "Bu&#322;garia"
  ]
  node [
    id 532
    label "Tonga"
  ]
  node [
    id 533
    label "Nikaragua"
  ]
  node [
    id 534
    label "Fid&#380;i"
  ]
  node [
    id 535
    label "Timor_Wschodni"
  ]
  node [
    id 536
    label "Laos"
  ]
  node [
    id 537
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 538
    label "Ghana"
  ]
  node [
    id 539
    label "Brazylia"
  ]
  node [
    id 540
    label "Belgia"
  ]
  node [
    id 541
    label "Irak"
  ]
  node [
    id 542
    label "Peru"
  ]
  node [
    id 543
    label "Arabia_Saudyjska"
  ]
  node [
    id 544
    label "Indonezja"
  ]
  node [
    id 545
    label "Malediwy"
  ]
  node [
    id 546
    label "Afganistan"
  ]
  node [
    id 547
    label "Jordania"
  ]
  node [
    id 548
    label "Kenia"
  ]
  node [
    id 549
    label "Czad"
  ]
  node [
    id 550
    label "Liberia"
  ]
  node [
    id 551
    label "Mali"
  ]
  node [
    id 552
    label "Armenia"
  ]
  node [
    id 553
    label "W&#281;gry"
  ]
  node [
    id 554
    label "Chile"
  ]
  node [
    id 555
    label "Kanada"
  ]
  node [
    id 556
    label "Cypr"
  ]
  node [
    id 557
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 558
    label "Ekwador"
  ]
  node [
    id 559
    label "Mo&#322;dawia"
  ]
  node [
    id 560
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 561
    label "W&#322;ochy"
  ]
  node [
    id 562
    label "Wyspy_Salomona"
  ]
  node [
    id 563
    label "&#321;otwa"
  ]
  node [
    id 564
    label "D&#380;ibuti"
  ]
  node [
    id 565
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 566
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 567
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 568
    label "Portugalia"
  ]
  node [
    id 569
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 570
    label "Maroko"
  ]
  node [
    id 571
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 572
    label "Francja"
  ]
  node [
    id 573
    label "Botswana"
  ]
  node [
    id 574
    label "Dominika"
  ]
  node [
    id 575
    label "Paragwaj"
  ]
  node [
    id 576
    label "Tad&#380;ykistan"
  ]
  node [
    id 577
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 578
    label "Haiti"
  ]
  node [
    id 579
    label "Khitai"
  ]
  node [
    id 580
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 581
    label "tam"
  ]
  node [
    id 582
    label "tu"
  ]
  node [
    id 583
    label "famu&#322;a"
  ]
  node [
    id 584
    label "zupa_owocowa"
  ]
  node [
    id 585
    label "&#322;&#243;dzki"
  ]
  node [
    id 586
    label "&#380;ywiecki"
  ]
  node [
    id 587
    label "dom_wielorodzinny"
  ]
  node [
    id 588
    label "zupa"
  ]
  node [
    id 589
    label "proceed"
  ]
  node [
    id 590
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 591
    label "kontynuowa&#263;"
  ]
  node [
    id 592
    label "wykonywa&#263;"
  ]
  node [
    id 593
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 594
    label "ride"
  ]
  node [
    id 595
    label "korzysta&#263;"
  ]
  node [
    id 596
    label "continue"
  ]
  node [
    id 597
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 598
    label "prowadzi&#263;"
  ]
  node [
    id 599
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 600
    label "drive"
  ]
  node [
    id 601
    label "go"
  ]
  node [
    id 602
    label "carry"
  ]
  node [
    id 603
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 604
    label "napada&#263;"
  ]
  node [
    id 605
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 606
    label "odbywa&#263;"
  ]
  node [
    id 607
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 608
    label "overdrive"
  ]
  node [
    id 609
    label "czu&#263;"
  ]
  node [
    id 610
    label "wali&#263;"
  ]
  node [
    id 611
    label "pachnie&#263;"
  ]
  node [
    id 612
    label "jeba&#263;"
  ]
  node [
    id 613
    label "talerz_perkusyjny"
  ]
  node [
    id 614
    label "cover"
  ]
  node [
    id 615
    label "chi&#324;ski"
  ]
  node [
    id 616
    label "goban"
  ]
  node [
    id 617
    label "gra_planszowa"
  ]
  node [
    id 618
    label "sport_umys&#322;owy"
  ]
  node [
    id 619
    label "robi&#263;"
  ]
  node [
    id 620
    label "prosecute"
  ]
  node [
    id 621
    label "przechodzi&#263;"
  ]
  node [
    id 622
    label "hold"
  ]
  node [
    id 623
    label "uczestniczy&#263;"
  ]
  node [
    id 624
    label "muzyka"
  ]
  node [
    id 625
    label "praca"
  ]
  node [
    id 626
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 627
    label "wytwarza&#263;"
  ]
  node [
    id 628
    label "rola"
  ]
  node [
    id 629
    label "work"
  ]
  node [
    id 630
    label "create"
  ]
  node [
    id 631
    label "eksponowa&#263;"
  ]
  node [
    id 632
    label "g&#243;rowa&#263;"
  ]
  node [
    id 633
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 634
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 635
    label "sterowa&#263;"
  ]
  node [
    id 636
    label "kierowa&#263;"
  ]
  node [
    id 637
    label "string"
  ]
  node [
    id 638
    label "kre&#347;li&#263;"
  ]
  node [
    id 639
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 640
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 641
    label "&#380;y&#263;"
  ]
  node [
    id 642
    label "partner"
  ]
  node [
    id 643
    label "linia_melodyczna"
  ]
  node [
    id 644
    label "prowadzenie"
  ]
  node [
    id 645
    label "ukierunkowywa&#263;"
  ]
  node [
    id 646
    label "przesuwa&#263;"
  ]
  node [
    id 647
    label "tworzy&#263;"
  ]
  node [
    id 648
    label "powodowa&#263;"
  ]
  node [
    id 649
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 650
    label "message"
  ]
  node [
    id 651
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 652
    label "navigate"
  ]
  node [
    id 653
    label "krzywa"
  ]
  node [
    id 654
    label "manipulate"
  ]
  node [
    id 655
    label "jeer"
  ]
  node [
    id 656
    label "traktowa&#263;"
  ]
  node [
    id 657
    label "dopada&#263;"
  ]
  node [
    id 658
    label "atakowa&#263;"
  ]
  node [
    id 659
    label "piratowa&#263;"
  ]
  node [
    id 660
    label "krytykowa&#263;"
  ]
  node [
    id 661
    label "attack"
  ]
  node [
    id 662
    label "u&#380;ywa&#263;"
  ]
  node [
    id 663
    label "use"
  ]
  node [
    id 664
    label "uzyskiwa&#263;"
  ]
  node [
    id 665
    label "uczuwa&#263;"
  ]
  node [
    id 666
    label "smell"
  ]
  node [
    id 667
    label "doznawa&#263;"
  ]
  node [
    id 668
    label "przewidywa&#263;"
  ]
  node [
    id 669
    label "anticipate"
  ]
  node [
    id 670
    label "postrzega&#263;"
  ]
  node [
    id 671
    label "by&#263;"
  ]
  node [
    id 672
    label "spirit"
  ]
  node [
    id 673
    label "za&#322;o&#380;enie"
  ]
  node [
    id 674
    label "truth"
  ]
  node [
    id 675
    label "nieprawdziwy"
  ]
  node [
    id 676
    label "s&#261;d"
  ]
  node [
    id 677
    label "realia"
  ]
  node [
    id 678
    label "kontekst"
  ]
  node [
    id 679
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 680
    label "infliction"
  ]
  node [
    id 681
    label "spowodowanie"
  ]
  node [
    id 682
    label "proposition"
  ]
  node [
    id 683
    label "przygotowanie"
  ]
  node [
    id 684
    label "pozak&#322;adanie"
  ]
  node [
    id 685
    label "point"
  ]
  node [
    id 686
    label "program"
  ]
  node [
    id 687
    label "poubieranie"
  ]
  node [
    id 688
    label "rozebranie"
  ]
  node [
    id 689
    label "str&#243;j"
  ]
  node [
    id 690
    label "budowla"
  ]
  node [
    id 691
    label "przewidzenie"
  ]
  node [
    id 692
    label "zak&#322;adka"
  ]
  node [
    id 693
    label "czynno&#347;&#263;"
  ]
  node [
    id 694
    label "twierdzenie"
  ]
  node [
    id 695
    label "przygotowywanie"
  ]
  node [
    id 696
    label "podwini&#281;cie"
  ]
  node [
    id 697
    label "zap&#322;acenie"
  ]
  node [
    id 698
    label "wyko&#324;czenie"
  ]
  node [
    id 699
    label "struktura"
  ]
  node [
    id 700
    label "utworzenie"
  ]
  node [
    id 701
    label "przebranie"
  ]
  node [
    id 702
    label "obleczenie"
  ]
  node [
    id 703
    label "przymierzenie"
  ]
  node [
    id 704
    label "obleczenie_si&#281;"
  ]
  node [
    id 705
    label "przywdzianie"
  ]
  node [
    id 706
    label "umieszczenie"
  ]
  node [
    id 707
    label "zrobienie"
  ]
  node [
    id 708
    label "przyodzianie"
  ]
  node [
    id 709
    label "pokrycie"
  ]
  node [
    id 710
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 711
    label "forum"
  ]
  node [
    id 712
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 713
    label "s&#261;downictwo"
  ]
  node [
    id 714
    label "wydarzenie"
  ]
  node [
    id 715
    label "podejrzany"
  ]
  node [
    id 716
    label "&#347;wiadek"
  ]
  node [
    id 717
    label "instytucja"
  ]
  node [
    id 718
    label "biuro"
  ]
  node [
    id 719
    label "post&#281;powanie"
  ]
  node [
    id 720
    label "court"
  ]
  node [
    id 721
    label "my&#347;l"
  ]
  node [
    id 722
    label "obrona"
  ]
  node [
    id 723
    label "system"
  ]
  node [
    id 724
    label "broni&#263;"
  ]
  node [
    id 725
    label "antylogizm"
  ]
  node [
    id 726
    label "strona"
  ]
  node [
    id 727
    label "oskar&#380;yciel"
  ]
  node [
    id 728
    label "urz&#261;d"
  ]
  node [
    id 729
    label "skazany"
  ]
  node [
    id 730
    label "konektyw"
  ]
  node [
    id 731
    label "wypowied&#378;"
  ]
  node [
    id 732
    label "bronienie"
  ]
  node [
    id 733
    label "wytw&#243;r"
  ]
  node [
    id 734
    label "pods&#261;dny"
  ]
  node [
    id 735
    label "zesp&#243;&#322;"
  ]
  node [
    id 736
    label "procesowicz"
  ]
  node [
    id 737
    label "zgodny"
  ]
  node [
    id 738
    label "prawdziwie"
  ]
  node [
    id 739
    label "szczery"
  ]
  node [
    id 740
    label "naprawd&#281;"
  ]
  node [
    id 741
    label "naturalny"
  ]
  node [
    id 742
    label "&#380;ywny"
  ]
  node [
    id 743
    label "realnie"
  ]
  node [
    id 744
    label "nieszczery"
  ]
  node [
    id 745
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 746
    label "niehistoryczny"
  ]
  node [
    id 747
    label "nieprawdziwie"
  ]
  node [
    id 748
    label "udawany"
  ]
  node [
    id 749
    label "niezgodny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 582
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
]
