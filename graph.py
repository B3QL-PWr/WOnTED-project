from glob import glob
from pathlib import Path
from itertools import chain, combinations
from statistics import mean
from tqdm import tqdm
from multiprocessing import Pool
import networkx as nx
from networkx.algorithms.clique import graph_clique_number
import numpy as np
import matplotlib.pyplot as plt
from typing import List

from WordNet import getmultipleall
from fasttext import WordComparer
from tfidf import TF_IDF
from xml_dataset import XMLDataset


WORDNET_SHORTCUTS = {
    'hipo': 'hiponimia',
    'meron_element': 'meronimia/element taksonomiczny',
    'meron_part': 'meronimia/część',
    'mero_collection': 'meronimia/element kolekcji',
    'synset': 'fuzzynimia_synsetów',
    'instance': 'egzemplarz',
    'closeness':'bliskoznaczność',
    'def': 'cecha_definicyjna',
    'similar': 'potencjalny_odpowiednik/pot_odp_plWN-PWN',
    'holo': 'holonimia/część'
}


class GraphBuilder:

    def __init__(self, record: XMLDataset.Record = None, filename: str = None, graph_dir: str = 'graphs/',
                 depth: int = 2):
        self.depth = depth
        if record is not None:
            self.record = record
            self.graph_path = f'{graph_dir}{record.filename}.gml'
        elif filename is not None:
            self.record = None
            self.graph_path = f'{graph_dir}{filename}.gml'

        if Path(self.graph_path).is_file():
            graph = nx.read_gml(self.graph_path)
            self.graph = nx.relabel_nodes(graph, {'_id': 'id'})
            self.was_loaded = True
        else:
            if self.record is None:
                raise AttributeError('Neither record nor correct filename provided')
            self.was_loaded = False
            self.graph = nx.Graph()
            self.graph.add_path(record.lemmas)
            nx.set_node_attributes(self.graph, 'text', 'origin')

    def annotate_ttidf(self):
        lemmas = self.record.lemmas
        model = TF_IDF(filename='tfidf.json')
        for lemma, tfidf in zip(lemmas, model.tf_idf(lemmas).values()):
            self.graph.node[lemma]['tfidf'] = tfidf

    def annotate_nouns(self):
        def is_noun(pos):
            parts = set(pos.split())
            return 'subst' in parts or 'depr' in parts

        for lemma, part in zip(self.record.lemmas, self.record.pos):
            self.graph.node[lemma]['noun'] = is_noun(part)

    def expand_wordnet(self):
        word_types = WORDNET_SHORTCUTS.values()
        for lemma in self.record.lemmas:
            for results in getmultipleall(lemma, self.depth, word_types).values():
                words = set(chain.from_iterable(results.values()))
                self.graph.add_edges_from((lemma, word) for word in words if word != lemma)

    def expand_word2vec(self):
        model = WordComparer()
        self.graph.add_edges_from(model.get_similar(self.record.lemmas))

    def expand_proper(self):
        for lexems in self.record.proper['multi'].values():
            self.graph.add_edges_from(combinations(lexems, 2))

    def fetch_metrics(self, lemmas: List[str]):
        pagerank = nx.pagerank(self.graph)
        betweenness = nx.betweenness_centrality(self.graph)
        degree = nx.degree(self.graph)
        pr = [[pagerank[key] for key, value in dict(pagerank).items() if k in key] for k in lemmas]
        pr_out = [np.mean(p) if len(p) > 0 else 0. for p in pr]
        bc = [[betweenness[key] for key, value in dict(betweenness).items() if k in key] for k in lemmas]
        bc_out = [np.mean(b) if len(b) > 0 else 0. for b in bc]
        dg = [[degree[k] for key, value in dict(degree).items() if k in key] for k in lemmas]
        dg_out = [np.mean(d) if len(d) > 0 else 0. for d in dg]
        return pr_out, bc_out, dg_out

    def build(self, rebuild=False):
        if not self.was_loaded or rebuild:
            #self.annotate_nouns()
            #self.annotate_ttidf()
            self.expand_wordnet()
            self.expand_word2vec()
            self.expand_proper()
            #self.extend_metrics()

    def save(self):
        relabeled = nx.relabel_nodes(self.graph, {'id': '_id'})
        nx.write_gml(relabeled, self.graph_path)

    @property
    def characteristics(self):
        metrics = ['maxDegree', 'minDegree', 'meanDegree', 'density', 'graphCliqueNumber']
        return [self.graph.graph[m] for m in metrics]


def vizualize_graph(graph, labels=True, figsize=(10, 10), scale=5):
    layout = nx.drawing.kamada_kawai_layout(graph, scale=scale)
    plt.figure(figsize=figsize)
    nx.draw(graph, pos=layout, with_labels=labels)
    plt.show()

    
def work(filename, depth=1):
    keywords = 'wykop' not in filename
    ds = XMLDataset(filename, keywords=keywords)
    for record in ds.records:
        builder = GraphBuilder(record, depth=depth)
        if not builder.was_loaded:
            builder.build()
            builder.save()
    return True


if __name__ == '__main__':
    with Pool(16) as p:
        results = [p.apply_async(work, args=(filename,)) for filename in glob('processed_dataset/*.csv')]
        [ret for ret in [res.get(timeout=None) for res in tqdm(results)]]
