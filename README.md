# WykOp TrEnds Detector
Wykop.pl is a widely known news aggregator and popular social medium in Poland, which hasn’t been explored so
far. Discovering growing trends in early stages whenever they are positive or negative may leads to different business
decisions from detecting and preventing social media brand crisis to starting new companies or products.

## Requirements
Project uses Python 3.7 and `pipenv` for managing requirements.

Install it with `pip install pipenv`.
Then install project requirements with `pipenv install --python 3.7`.

If you want to add new requirement just execute `pipenv install <your_library>`.

### Word Embeddings
`WordComparer` class from `fasttext.py` uses fastText python binding: https://github.com/facebookresearch/fastText/tree/master/python.
It requires a pretrained model in root directory (`model.bin` file). Polish pretrained FastText models are available at http://tools.clarin-pl.eu/share/embeddings/.

## External documentation
http://moria.umcs.lublin.pl/sphinx/src/machine_learning/natural_language_processing/clarin.html

http://nlp.pwr.wroc.pl/redmine/projects/nlprest2/wiki
