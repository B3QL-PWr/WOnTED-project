import numpy as np
import pandas as pd
from glob import glob
from json import dump, load
from sklearn.model_selection import train_test_split
from tqdm import tqdm
from typing import Dict, List, Tuple

from graph import GraphBuilder
from tfidf import TF_IDF
from xml_dataset import XMLDataset


def fetch_wykop_stats(filename: str, data_dir: str = 'data/'):
    file, row = filename.split('_')
    df = pd.read_csv(f'{data_dir}{file}.csv', usecols=['vote_count'])
    return df.iloc[[int(row)]].values[0][0]


class WontedDataset:
    """
    Dataset for classifier training
    """

    def __init__(self, datasets_dir: str = 'processed_dataset', train_dataset_substrings: List[str] = ('wykop',),
                 use_network: bool = True, use_pos: bool = True, use_proper: bool = True, use_tfidf: bool = True,
                 ignore_chars: int = 2, min_keyword_length: int = 5, graph_depth: int = 2,
                 graph_builder: GraphBuilder = None):
        self.ignore_chars = ignore_chars
        self.min_keyword_length = min_keyword_length
        self.graph_depth = graph_depth
        self.gb = graph_builder
        files = glob(f'{datasets_dir}/*.csv')
        validation = [f for f in files if any([sub in f for sub in train_dataset_substrings])]
        train = [f for f in files if f not in validation]
        self.train_datasets = {csv: XMLDataset(csv=csv, keywords=True) for csv in train}
        self.val_datasets = {csv: XMLDataset(csv=csv, keywords=False) for csv in validation}
        self.train_data = {}
        self.val_data = {}
        self.tfidf = TF_IDF(filename='tfidf.json')
        self.process = {}
        self.parameters = 1
        if use_network and graph_depth != 0:
            self.parameters += 3
            self.process['network'] = self._get_network
        if use_pos:
            self.parameters += 3
            self.process['pos'] = self._get_pos
        if use_proper:
            self.parameters += 2
            self.process['proper'] = self._get_proper
        if use_tfidf:
            self.parameters += 3
            self.process['tfidf'] = self._get_tfidf

    def _get_network(self, record: XMLDataset.Record) -> np.ndarray:
        if self.gb is None:
            self.gb = GraphBuilder(filename=record.filename, depth=self.graph_depth)
        lemmas = record.lemmas
        ret = np.stack(self.gb.fetch_metrics(lemmas), axis=1)
        return ret

    @staticmethod
    def _get_pos(record: XMLDataset.Record) -> np.ndarray:
        lemmas = record.lemmas
        pos = record.pos
        ret = np.zeros((len(lemmas), 3))
        subst_str = ('subst',)
        verb_str = ('fin', 'imperf', 'praet', 'perf')
        adj_str = ('adj',)
        params = [(1 if any(s in p for s in subst_str) else 0,
                   1 if any(v in p for v in verb_str) else 0,
                   1 if any(a in p for a in adj_str) else 0) for p in pos]
        ret[:, 0] = [param[0] for param in params]
        ret[:, 1] = [param[1] for param in params]
        ret[:, 2] = [param[2] for param in params]
        return ret

    @staticmethod
    def _get_proper(record: XMLDataset.Record) -> np.ndarray:
        lemmas = record.lemmas
        ret = np.zeros((len(lemmas), 2))
        proper = record.proper
        params = [(1 if lem in proper['single'] else 0,
                   1 if any(lem in mul for mul in proper['multi'].values()) else 0) for lem in lemmas]
        ret[:, 0] = [param[0] for param in params]
        ret[:, 1] = [param[1] for param in params]
        return ret

    def _get_tfidf(self, record: XMLDataset.Record) -> np.ndarray:
        lemmas = record.lemmas
        ret = np.zeros((len(lemmas), 3))
        tf_dict = self.tfidf.tf(lemmas)
        idf_dict = self.tfidf.idf(lemmas)
        tfidf_dict = self.tfidf.tf_idf(lemmas, tf=tf_dict, idf=idf_dict)
        ret[:, 0] = [tf_dict[l] for l in lemmas]
        ret[:, 1] = [idf_dict[l] for l in lemmas]
        ret[:, 2] = [tfidf_dict[l] for l in lemmas]
        return ret

    def _analyze_row(self, record: XMLDataset.Record) -> np.ndarray:
        lemmas = record.lemmas
        ret = np.zeros((len(lemmas), self.parameters), dtype=object)
        ret[:, 0] = lemmas
        current_pos = 1
        for k, fn in self.process.items():
            param = fn(record=record)
            ret[:, current_pos:current_pos + param.shape[1]] = param
            current_pos += param.shape[1]
        return ret

    def _get_labels(self, record: XMLDataset.Record) -> np.ndarray:
        lemmas = record.lemmas
        keywords = record.keywords
        ret = np.array([1. if any(l[:-self.ignore_chars] in kw.lower() for kw in keywords) and len(
            l) > self.min_keyword_length else 0. for l in lemmas])
        return ret

    def save_data(self, filename: str = 'wonted.json'):
        train_save = {key: {k: (v[0].tolist(), v[1].tolist()) for k, v in subdict.items()} for key, subdict in self.train_data.items()}
        val_save = {key: {k: v.tolist() for k, v in subdict.items()} for key, subdict in self.val_data.items()}
        with open(filename, 'w') as f:
            dump((train_save, val_save), f)

    def load_data(self, filename: str = 'wonted.json'):
        with open(filename, 'r') as f:
            train, val = load(f)
            self.train_data = {key: {k: (np.array(v[0]), np.array(v[1])) for k, v in subdict.items()} for key, subdict in train.items()}
            self.val_data = {key: {k: np.array(v) for k, v in subdict.items()} for key, subdict in val.items()}

    def generate_datasets(self):
        if not self.train_data:
            for csv, ds in tqdm(self.train_datasets.items()):
                self.train_data[csv] = {record.filename: (self._analyze_row(record), self._get_labels(record)) for
                                        record in ds.records}
        if not self.val_data:
            for csv, ds in tqdm(self.val_datasets.items()):
                self.val_data[csv] = {record.filename: self._analyze_row(record) for record in ds.records}

    def get_train_test(self, shuffled: bool = False, test_size: float = 0.2) -> Tuple[
        np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        if not self.train_data:
            try:
                self.load_data()
            except FileNotFoundError:
                self.generate_datasets()
        X = None
        y = None
        for _, filename in self.train_data.items():
            for _, ds in filename.items():
                if X is None and y is None:
                    X = ds[0]
                    y = ds[1]
                else:
                    X = np.concatenate((X, ds[0]), axis=0)
                    y = np.concatenate((y, ds[1]), axis=0)
        if shuffled:
            stratify = y
        else:
            stratify = None
        return train_test_split(X, y, test_size=test_size, shuffle=shuffled, stratify=stratify)

    def get_train_dictionary(self) -> Dict[str, Tuple[np.ndarray, np.ndarray]]:
        if not self.train_data:
            try:
                self.load_data()
            except FileNotFoundError:
                self.generate_datasets()
        return self.train_data

    def get_val_dictionary(self) -> Dict[str, np.ndarray]:
        if not self.val_data:
            try:
                self.load_data()
            except FileNotFoundError:
                self.generate_datasets()
        return self.val_data['processed_dataset/dataset_wykop.csv']

    def get_custom(self, record) -> np.ndarray:
        return self._analyze_row(record)


if __name__ == '__main__':
    wd = WontedDataset(use_network=True, use_pos=True, use_proper=True, use_tfidf=True)
    wd.generate_datasets()
    wd.save_data()
