import re
import string
import subprocess
from configparser import ConfigParser
from json import dumps, loads
from os import listdir
from typing import List, Tuple

import pandas as pd
from tqdm import tqdm

from preprocess import fetch_keywords
from proper import get_proper


def execute_bash_command(command: str) -> List[str]:
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    if error is not None:
        raise OSError(f'{error} occurred')
    return [result for result in output.decode('UTF-8').split('\n') if string != '']


class XMLDataset:
    class Record:
        filename = None
        text = None
        lemmas = None
        pos = None
        proper = None
        keywords = None

    def __init__(self, csv: str = '', load=True, source_dir: str = 'dataset/blogi/',
                 save_dir: str = 'processed_dataset/', docker: bool = False, keywords: bool = True,
                 extension: str = 'ini'):
        self.records = []
        self.source_dir = source_dir
        self.files = [f[:-4] for f in listdir(self.source_dir) if extension in f]
        self.save_dir = save_dir
        self.kws = keywords
        if csv == '':
            self.csv = self.source_dir[:-1].replace('/', '_') + '.csv'
        else:
            self.csv = csv
        if load:
            self.load_csv()
        if docker:
            execute_bash_command('docker cp _docker/fetch_lemmas.py wcrft2:/home/install/fetch_lemmas.py')
            execute_bash_command('docker cp _docker/fetch_text.py wcrft2:/home/install/fetch_text.py')
            execute_bash_command('docker cp _docker/fetch_all.py wcrft2:/home/install/fetch_all.py')

    def _get_keywords(self, path: str) -> List[str]:
        ini = ConfigParser()
        ini.read(path)
        keywords = ini['metadata']['keywords'].lower()
        multi = [kw for kw in keywords.split(', ') if len(kw.split()) > 1]
        keywords.translate(str.maketrans('', '', string.punctuation))
        with open("tmp.xml", "w+") as xml_file:
            xml_file.write(list(fetch_keywords([keywords]).values())[0])
        lems = self._get_lemmas("tmp.xml")
        return [word.lower() for word in multi + lems[0]]

    @staticmethod
    def _get_lemmas(path: str) -> List[List[str]]:
        target = '/tmp/2proc.xml'
        execute_bash_command('docker start wcrft2')
        execute_bash_command(f'docker cp {path} wcrft2:{target}')
        sentences = execute_bash_command(f'docker exec wcrft2 python fetch_lemmas.py {target}')
        return [list(filter(None, [''.join(filter(str.isalpha, lem.lower())) for lem in sent.split(' ')])) for sent in
                sentences]

    @staticmethod
    def _get_text(path: str) -> str:
        target = '/tmp/2proc.xml'
        execute_bash_command('docker start wcrft2')
        execute_bash_command(f'docker cp {path} wcrft2:{target}')
        sentences = execute_bash_command(f'docker exec wcrft2 python fetch_text.py {target}')
        return ' '.join(sentences)

    @staticmethod
    def _get_all(path: str):
        target = '/tmp/2proc.xml'
        execute_bash_command('docker start wcrft2')
        execute_bash_command(f'docker cp {path} wcrft2:{target}')
        data = loads(execute_bash_command(f'docker exec wcrft2 python fetch_all.py {target}')[0])
        return data

    @staticmethod
    def _filter_lemmas(lemmas: List[str]) -> List[List[str]]:
        return [re.findall(fr"[\w']+|[{string.punctuation}]", sent) for sent in lemmas]

    @staticmethod
    def _clean_dataset(lemmas: List[List[str]], pos: List[List[str]]) -> Tuple[List[List[str]], List[List[str]]]:
        ret_lemmas = []
        ret_pos = []
        for sent, desc in zip(lemmas, pos):
            ret_sent = []
            ret_desc = []
            for l, p in zip(sent, desc):
                if len(l) > 2 and 'interp' not in p and 'conj' not in p and not any(i.isdigit() for i in l):
                    ret_sent.append(l.lower())
                    ret_desc.append(p)
            if ret_sent and ret_desc:
                ret_lemmas += ret_sent
                ret_pos += ret_desc
            if len(ret_sent) != len(ret_desc):
                print('Sentence and POS vectors lengths not same')
        if len(ret_lemmas) != len(ret_pos):
            print('Return lists lengths not same')
        return ret_lemmas, ret_pos

    @staticmethod
    def _convert_pos(pos: List[List[str]]) -> List[List[str]]:
        return [[p.replace(',', ' ') for p in poss] for poss in pos]

    @staticmethod
    def _encode(words) -> str:
        return dumps(words)

    @staticmethod
    def _decode(encoded: str) -> List[List[str]]:
        return loads(encoded)

    def generate_records(self):
        for f in tqdm(self.files):
            try:
                file = f'{self.source_dir}{f}.xml'
                data = self._get_all(file)
                pos = self._convert_pos([d[0] for d in data['pos']])
                lemmas = self._filter_lemmas([d[0] for d in data['lemma']])
                lemmas, pos = self._clean_dataset(lemmas, pos)
                proper_single, proper_multi = get_proper(file)
                record = self.Record()
                record.filename = f
                record.text = '. '.join([' '.join(d) for d in data['text']])
                record.pos = pos
                record.lemmas = lemmas
                record.proper = {'single': proper_single, 'multi': proper_multi}
                keywords_correct = True
                if self.kws:
                    record.keywords = self._get_keywords(f'{self.source_dir}{f}.ini')
                    if not record.keywords:
                        keywords_correct = False
                if record.text.lower() != 'nan' and record.lemmas and record.pos and keywords_correct:
                    self.records.append(record)
                else:
                    print('Row is nan.')
            except KeyError:
                print('No keywords in ini file.')
            except IndexError:
                print('No words.')

    def _encode_record(self, record: Record) -> pd.DataFrame:
        if self.kws:
            encoded = {'filename': record.filename,
                       'text': record.text,
                       'lemmas': self._encode(record.lemmas),
                       'pos': self._encode(record.pos),
                       'proper': self._encode(record.proper),
                       'keywords': self._encode([record.keywords])}
        else:
            encoded = {'filename': record.filename,
                       'text': record.text,
                       'lemmas': self._encode(record.lemmas),
                       'pos': self._encode(record.pos),
                       'proper': dumps(record.proper)}
        return pd.DataFrame([encoded], columns=encoded.keys())

    def _decode_record(self, record: pd.Series) -> Record:
        decoded = self.Record()
        decoded.filename = record['filename']
        decoded.text = record['text']
        decoded.lemmas = self._decode(record['lemmas'])
        decoded.pos = self._decode(record['pos'])
        decoded.proper = loads(record['proper'])
        if self.kws:
            decoded.keywords = self._decode(record['keywords'])[0]
        return decoded

    def _encode_dataset(self):
        if self.kws:
            columns = ['filename', 'text', 'lemmas', 'pos', 'proper', 'keywords']
        else:
            columns = ['filename', 'text', 'lemmas', 'pos', 'proper']
        self.df = pd.DataFrame(columns=columns)
        for record in self.records:
            to_concat = self._encode_record(record)
            self.df = pd.concat([self.df, to_concat], axis=0, sort=False, ignore_index=True)

    def _decode_dataset(self):
        self.records = []
        for index, row in self.df.iterrows():
            self.records.append(self._decode_record(row))

    def save_csv(self):
        self._encode_dataset()
        self.df.to_csv(self.save_dir + self.csv)

    def load_csv(self):
        self.df = pd.read_csv(self.csv)
        self._decode_dataset()

    @property
    def filenames(self):
        return [r.filename for r in self.records]

    @property
    def text(self):
        return [r.text for r in self.records]

    @property
    def lemmas(self):
        return [r.lemmas for r in self.records]

    @property
    def keywords(self):
        return [r.keywords for r in self.records]

    @property
    def pos(self):
        return [r.pos for r in self.records]

    @property
    def proper(self):
        return [r.proper for r in self.records]


if __name__ == '__main__':
    kwargs = [{'source_dir': 'output/', 'save_dir': 'processed_dataset/', 'csv': 'dataset_wykop.csv', 'keywords': False,
               'extension': 'xml', 'load': False},
              {'source_dir': 'dataset/blogi/', 'load': False},
              {'source_dir': 'dataset/dap/', 'load': False},
              {'source_dir': 'dataset/dialog/', 'load': False},
              {'source_dir': 'dataset/kap/', 'load': False},
              {'source_dir': 'dataset/popularno_naukowe_i_podręczniki/', 'load': False},
              {'source_dir': 'dataset/proza_dawna/', 'load': False},
              {'source_dir': 'dataset/proza_wspolczesna/', 'load': False},
              {'source_dir': 'dataset/stenogramy/', 'load': False},
              {'source_dir': 'dataset/techniczne/', 'load': False},
              {'source_dir': 'dataset/urzędowe/', 'load': False},
              {'source_dir': 'dataset/ustawy/', 'load': False},
              {'source_dir': 'dataset/wikipedia/', 'load': False}]
    for kw in kwargs:
        print(f'Current dir: {kw["source_dir"]}')
        ds = XMLDataset(docker=True, **kw)
        ds.generate_records()
        ds.save_csv()
        f = ds.save_dir + ds.csv
        print(f"Trying to load {f}")
        if 'wykop' in f:
            XMLDataset(csv=f, keywords=False)
        else:
            XMLDataset(csv=f, keywords=True)
