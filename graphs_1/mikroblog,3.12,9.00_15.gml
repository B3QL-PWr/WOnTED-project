graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9534883720930232
  density 0.046511627906976744
  graphCliqueNumber 2
  node [
    id 0
    label "zaplusuj"
    origin "text"
  ]
  node [
    id 1
    label "pyszny"
    origin "text"
  ]
  node [
    id 2
    label "drwal"
    origin "text"
  ]
  node [
    id 3
    label "obudzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "jutro"
    origin "text"
  ]
  node [
    id 5
    label "nasyci&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pelen"
    origin "text"
  ]
  node [
    id 7
    label "sil"
    origin "text"
  ]
  node [
    id 8
    label "napuszenie_si&#281;"
  ]
  node [
    id 9
    label "pysznie"
  ]
  node [
    id 10
    label "przesmaczny"
  ]
  node [
    id 11
    label "udany"
  ]
  node [
    id 12
    label "przedni"
  ]
  node [
    id 13
    label "rozd&#281;cie_si&#281;"
  ]
  node [
    id 14
    label "rozdymanie_si&#281;"
  ]
  node [
    id 15
    label "podufa&#322;y"
  ]
  node [
    id 16
    label "wspania&#322;y"
  ]
  node [
    id 17
    label "dufny"
  ]
  node [
    id 18
    label "wynios&#322;y"
  ]
  node [
    id 19
    label "napuszanie_si&#281;"
  ]
  node [
    id 20
    label "kantak"
  ]
  node [
    id 21
    label "robotnik"
  ]
  node [
    id 22
    label "drzewiarz"
  ]
  node [
    id 23
    label "r&#261;ba&#263;"
  ]
  node [
    id 24
    label "wyrwa&#263;"
  ]
  node [
    id 25
    label "wywo&#322;a&#263;"
  ]
  node [
    id 26
    label "arouse"
  ]
  node [
    id 27
    label "prompt"
  ]
  node [
    id 28
    label "blisko"
  ]
  node [
    id 29
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 30
    label "dzie&#324;_jutrzejszy"
  ]
  node [
    id 31
    label "jutrzejszy"
  ]
  node [
    id 32
    label "dzie&#324;"
  ]
  node [
    id 33
    label "wzmocni&#263;"
  ]
  node [
    id 34
    label "dostarczy&#263;"
  ]
  node [
    id 35
    label "soak"
  ]
  node [
    id 36
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 37
    label "przenikn&#261;&#263;"
  ]
  node [
    id 38
    label "nakarmi&#263;"
  ]
  node [
    id 39
    label "wype&#322;ni&#263;"
  ]
  node [
    id 40
    label "overcharge"
  ]
  node [
    id 41
    label "zaspokoi&#263;"
  ]
  node [
    id 42
    label "wprowadzi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 6
    target 7
  ]
]
