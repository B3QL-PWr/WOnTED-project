graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "lenno"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;ochy"
    origin "text"
  ]
  node [
    id 2
    label "feudalizm"
  ]
  node [
    id 3
    label "podleg&#322;o&#347;&#263;"
  ]
  node [
    id 4
    label "maj&#261;tek"
  ]
  node [
    id 5
    label "serfdom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
]
