graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.96875
  density 0.03125
  graphCliqueNumber 2
  node [
    id 0
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 1
    label "gryz"
    origin "text"
  ]
  node [
    id 2
    label "luty"
    origin "text"
  ]
  node [
    id 3
    label "godz"
    origin "text"
  ]
  node [
    id 4
    label "filia"
    origin "text"
  ]
  node [
    id 5
    label "dla"
    origin "text"
  ]
  node [
    id 6
    label "doros&#322;a"
    origin "text"
  ]
  node [
    id 7
    label "przy"
    origin "text"
  ]
  node [
    id 8
    label "ula"
    origin "text"
  ]
  node [
    id 9
    label "mielczarski"
    origin "text"
  ]
  node [
    id 10
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "spotkanie"
    origin "text"
  ]
  node [
    id 13
    label "organizacyjny"
    origin "text"
  ]
  node [
    id 14
    label "dyskusyjny"
    origin "text"
  ]
  node [
    id 15
    label "klub"
    origin "text"
  ]
  node [
    id 16
    label "ksi&#261;&#380;kowy"
    origin "text"
  ]
  node [
    id 17
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 18
    label "miesi&#261;c"
  ]
  node [
    id 19
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 20
    label "walentynki"
  ]
  node [
    id 21
    label "dzia&#322;"
  ]
  node [
    id 22
    label "agencja"
  ]
  node [
    id 23
    label "reserve"
  ]
  node [
    id 24
    label "przej&#347;&#263;"
  ]
  node [
    id 25
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 26
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 27
    label "po&#380;egnanie"
  ]
  node [
    id 28
    label "spowodowanie"
  ]
  node [
    id 29
    label "znalezienie"
  ]
  node [
    id 30
    label "znajomy"
  ]
  node [
    id 31
    label "doznanie"
  ]
  node [
    id 32
    label "employment"
  ]
  node [
    id 33
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 34
    label "gather"
  ]
  node [
    id 35
    label "powitanie"
  ]
  node [
    id 36
    label "spotykanie"
  ]
  node [
    id 37
    label "wydarzenie"
  ]
  node [
    id 38
    label "gathering"
  ]
  node [
    id 39
    label "spotkanie_si&#281;"
  ]
  node [
    id 40
    label "zdarzenie_si&#281;"
  ]
  node [
    id 41
    label "match"
  ]
  node [
    id 42
    label "zawarcie"
  ]
  node [
    id 43
    label "kontrowersyjnie"
  ]
  node [
    id 44
    label "w&#261;tpliwy"
  ]
  node [
    id 45
    label "dyskusyjnie"
  ]
  node [
    id 46
    label "society"
  ]
  node [
    id 47
    label "jakobini"
  ]
  node [
    id 48
    label "klubista"
  ]
  node [
    id 49
    label "stowarzyszenie"
  ]
  node [
    id 50
    label "lokal"
  ]
  node [
    id 51
    label "od&#322;am"
  ]
  node [
    id 52
    label "siedziba"
  ]
  node [
    id 53
    label "bar"
  ]
  node [
    id 54
    label "ksi&#261;&#380;kowo"
  ]
  node [
    id 55
    label "podr&#281;cznikowo"
  ]
  node [
    id 56
    label "klasyczny"
  ]
  node [
    id 57
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 58
    label "wzorowy"
  ]
  node [
    id 59
    label "wzorcowy"
  ]
  node [
    id 60
    label "invite"
  ]
  node [
    id 61
    label "ask"
  ]
  node [
    id 62
    label "oferowa&#263;"
  ]
  node [
    id 63
    label "zwraca&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 35
  ]
  edge [
    source 12
    target 36
  ]
  edge [
    source 12
    target 37
  ]
  edge [
    source 12
    target 38
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 12
    target 41
  ]
  edge [
    source 12
    target 42
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 43
  ]
  edge [
    source 14
    target 44
  ]
  edge [
    source 14
    target 45
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 46
  ]
  edge [
    source 15
    target 47
  ]
  edge [
    source 15
    target 48
  ]
  edge [
    source 15
    target 49
  ]
  edge [
    source 15
    target 50
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 15
    target 52
  ]
  edge [
    source 15
    target 53
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 54
  ]
  edge [
    source 16
    target 55
  ]
  edge [
    source 16
    target 56
  ]
  edge [
    source 16
    target 57
  ]
  edge [
    source 16
    target 58
  ]
  edge [
    source 16
    target 59
  ]
  edge [
    source 17
    target 60
  ]
  edge [
    source 17
    target 61
  ]
  edge [
    source 17
    target 62
  ]
  edge [
    source 17
    target 63
  ]
]
