graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9726027397260273
  density 0.0273972602739726
  graphCliqueNumber 3
  node [
    id 0
    label "liczba"
    origin "text"
  ]
  node [
    id 1
    label "fotoradar"
    origin "text"
  ]
  node [
    id 2
    label "przy"
    origin "text"
  ]
  node [
    id 3
    label "polski"
    origin "text"
  ]
  node [
    id 4
    label "droga"
    origin "text"
  ]
  node [
    id 5
    label "wkr&#243;tce"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "podwoj"
    origin "text"
  ]
  node [
    id 8
    label "kategoria"
  ]
  node [
    id 9
    label "kategoria_gramatyczna"
  ]
  node [
    id 10
    label "kwadrat_magiczny"
  ]
  node [
    id 11
    label "cecha"
  ]
  node [
    id 12
    label "grupa"
  ]
  node [
    id 13
    label "wyra&#380;enie"
  ]
  node [
    id 14
    label "pierwiastek"
  ]
  node [
    id 15
    label "rozmiar"
  ]
  node [
    id 16
    label "number"
  ]
  node [
    id 17
    label "poj&#281;cie"
  ]
  node [
    id 18
    label "koniugacja"
  ]
  node [
    id 19
    label "radar"
  ]
  node [
    id 20
    label "lacki"
  ]
  node [
    id 21
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 22
    label "przedmiot"
  ]
  node [
    id 23
    label "sztajer"
  ]
  node [
    id 24
    label "drabant"
  ]
  node [
    id 25
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 26
    label "polak"
  ]
  node [
    id 27
    label "pierogi_ruskie"
  ]
  node [
    id 28
    label "krakowiak"
  ]
  node [
    id 29
    label "Polish"
  ]
  node [
    id 30
    label "j&#281;zyk"
  ]
  node [
    id 31
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 32
    label "oberek"
  ]
  node [
    id 33
    label "po_polsku"
  ]
  node [
    id 34
    label "mazur"
  ]
  node [
    id 35
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 36
    label "chodzony"
  ]
  node [
    id 37
    label "skoczny"
  ]
  node [
    id 38
    label "ryba_po_grecku"
  ]
  node [
    id 39
    label "goniony"
  ]
  node [
    id 40
    label "polsko"
  ]
  node [
    id 41
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 42
    label "journey"
  ]
  node [
    id 43
    label "podbieg"
  ]
  node [
    id 44
    label "bezsilnikowy"
  ]
  node [
    id 45
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 46
    label "wylot"
  ]
  node [
    id 47
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 48
    label "drogowskaz"
  ]
  node [
    id 49
    label "nawierzchnia"
  ]
  node [
    id 50
    label "turystyka"
  ]
  node [
    id 51
    label "budowla"
  ]
  node [
    id 52
    label "spos&#243;b"
  ]
  node [
    id 53
    label "passage"
  ]
  node [
    id 54
    label "marszrutyzacja"
  ]
  node [
    id 55
    label "zbior&#243;wka"
  ]
  node [
    id 56
    label "ekskursja"
  ]
  node [
    id 57
    label "rajza"
  ]
  node [
    id 58
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 59
    label "ruch"
  ]
  node [
    id 60
    label "trasa"
  ]
  node [
    id 61
    label "wyb&#243;j"
  ]
  node [
    id 62
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 63
    label "ekwipunek"
  ]
  node [
    id 64
    label "korona_drogi"
  ]
  node [
    id 65
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 66
    label "pobocze"
  ]
  node [
    id 67
    label "wpr&#281;dce"
  ]
  node [
    id 68
    label "blisko"
  ]
  node [
    id 69
    label "nied&#322;ugi"
  ]
  node [
    id 70
    label "inspekcja"
  ]
  node [
    id 71
    label "transport"
  ]
  node [
    id 72
    label "drogowe"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 71
    target 72
  ]
]
