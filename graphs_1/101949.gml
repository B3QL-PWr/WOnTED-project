graph [
  maxDegree 18
  minDegree 1
  meanDegree 2.684931506849315
  density 0.03729071537290715
  graphCliqueNumber 6
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "urz&#281;dowy"
    origin "text"
  ]
  node [
    id 2
    label "urz&#261;d"
    origin "text"
  ]
  node [
    id 3
    label "ochrona"
    origin "text"
  ]
  node [
    id 4
    label "konkurencja"
    origin "text"
  ]
  node [
    id 5
    label "konsument"
    origin "text"
  ]
  node [
    id 6
    label "marco"
    origin "text"
  ]
  node [
    id 7
    label "poz"
    origin "text"
  ]
  node [
    id 8
    label "spis"
  ]
  node [
    id 9
    label "sheet"
  ]
  node [
    id 10
    label "gazeta"
  ]
  node [
    id 11
    label "diariusz"
  ]
  node [
    id 12
    label "pami&#281;tnik"
  ]
  node [
    id 13
    label "journal"
  ]
  node [
    id 14
    label "ksi&#281;ga"
  ]
  node [
    id 15
    label "program_informacyjny"
  ]
  node [
    id 16
    label "urz&#281;dowo"
  ]
  node [
    id 17
    label "oficjalny"
  ]
  node [
    id 18
    label "formalny"
  ]
  node [
    id 19
    label "organ"
  ]
  node [
    id 20
    label "w&#322;adza"
  ]
  node [
    id 21
    label "instytucja"
  ]
  node [
    id 22
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 23
    label "mianowaniec"
  ]
  node [
    id 24
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 25
    label "stanowisko"
  ]
  node [
    id 26
    label "position"
  ]
  node [
    id 27
    label "dzia&#322;"
  ]
  node [
    id 28
    label "siedziba"
  ]
  node [
    id 29
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 30
    label "okienko"
  ]
  node [
    id 31
    label "chemical_bond"
  ]
  node [
    id 32
    label "tarcza"
  ]
  node [
    id 33
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 34
    label "obiekt"
  ]
  node [
    id 35
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 36
    label "borowiec"
  ]
  node [
    id 37
    label "obstawienie"
  ]
  node [
    id 38
    label "formacja"
  ]
  node [
    id 39
    label "ubezpieczenie"
  ]
  node [
    id 40
    label "obstawia&#263;"
  ]
  node [
    id 41
    label "obstawianie"
  ]
  node [
    id 42
    label "transportacja"
  ]
  node [
    id 43
    label "dob&#243;r_naturalny"
  ]
  node [
    id 44
    label "firma"
  ]
  node [
    id 45
    label "dyscyplina_sportowa"
  ]
  node [
    id 46
    label "interakcja"
  ]
  node [
    id 47
    label "wydarzenie"
  ]
  node [
    id 48
    label "rywalizacja"
  ]
  node [
    id 49
    label "uczestnik"
  ]
  node [
    id 50
    label "contest"
  ]
  node [
    id 51
    label "rynek"
  ]
  node [
    id 52
    label "odbiorca"
  ]
  node [
    id 53
    label "go&#347;&#263;"
  ]
  node [
    id 54
    label "zjadacz"
  ]
  node [
    id 55
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 56
    label "restauracja"
  ]
  node [
    id 57
    label "heterotrof"
  ]
  node [
    id 58
    label "klient"
  ]
  node [
    id 59
    label "u&#380;ytkownik"
  ]
  node [
    id 60
    label "i"
  ]
  node [
    id 61
    label "polski"
  ]
  node [
    id 62
    label "telefonia"
  ]
  node [
    id 63
    label "cyfrowy"
  ]
  node [
    id 64
    label "sp&#243;&#322;ka"
  ]
  node [
    id 65
    label "zeszyt"
  ]
  node [
    id 66
    label "ojciec"
  ]
  node [
    id 67
    label "prezes"
  ]
  node [
    id 68
    label "UOKiK"
  ]
  node [
    id 69
    label "s&#261;d"
  ]
  node [
    id 70
    label "okr&#281;gowy"
  ]
  node [
    id 71
    label "wyspa"
  ]
  node [
    id 72
    label "warszawa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 60
    target 67
  ]
  edge [
    source 60
    target 69
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 61
    target 64
  ]
  edge [
    source 61
    target 65
  ]
  edge [
    source 61
    target 66
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 62
    target 66
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 63
    target 66
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 66
    target 66
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 69
    target 72
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 71
    target 72
  ]
]
