graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "niestety"
    origin "text"
  ]
  node [
    id 1
    label "jeszcze"
    origin "text"
  ]
  node [
    id 2
    label "&#380;y&#263;"
    origin "text"
  ]
  node [
    id 3
    label "xdd"
    origin "text"
  ]
  node [
    id 4
    label "pokazmorde"
    origin "text"
  ]
  node [
    id 5
    label "ci&#261;gle"
  ]
  node [
    id 6
    label "pause"
  ]
  node [
    id 7
    label "stay"
  ]
  node [
    id 8
    label "consist"
  ]
  node [
    id 9
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 10
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 11
    label "istnie&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
]
