graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.3181818181818183
  density 0.013246753246753246
  graphCliqueNumber 5
  node [
    id 0
    label "przypadek"
    origin "text"
  ]
  node [
    id 1
    label "uchyli&#263;"
    origin "text"
  ]
  node [
    id 2
    label "prawomocny"
    origin "text"
  ]
  node [
    id 3
    label "wyrok"
    origin "text"
  ]
  node [
    id 4
    label "skazywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "lub"
    origin "text"
  ]
  node [
    id 6
    label "postanowienie"
    origin "text"
  ]
  node [
    id 7
    label "warunkowy"
    origin "text"
  ]
  node [
    id 8
    label "umorzy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "post&#281;powanie"
    origin "text"
  ]
  node [
    id 10
    label "karny"
    origin "text"
  ]
  node [
    id 11
    label "albo"
    origin "text"
  ]
  node [
    id 12
    label "wydanie"
    origin "text"
  ]
  node [
    id 13
    label "uniewinnia&#263;"
    origin "text"
  ]
  node [
    id 14
    label "ulega&#263;"
    origin "text"
  ]
  node [
    id 15
    label "wszystek"
    origin "text"
  ]
  node [
    id 16
    label "skutek"
    origin "text"
  ]
  node [
    id 17
    label "jaki"
    origin "text"
  ]
  node [
    id 18
    label "wynik&#322;y"
    origin "text"
  ]
  node [
    id 19
    label "dla"
    origin "text"
  ]
  node [
    id 20
    label "funkcjonariusz"
    origin "text"
  ]
  node [
    id 21
    label "dyscyplinarny"
    origin "text"
  ]
  node [
    id 22
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 24
    label "orzeczenie"
    origin "text"
  ]
  node [
    id 25
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 26
    label "prokurator"
    origin "text"
  ]
  node [
    id 27
    label "pacjent"
  ]
  node [
    id 28
    label "kategoria_gramatyczna"
  ]
  node [
    id 29
    label "schorzenie"
  ]
  node [
    id 30
    label "przeznaczenie"
  ]
  node [
    id 31
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 32
    label "wydarzenie"
  ]
  node [
    id 33
    label "happening"
  ]
  node [
    id 34
    label "przyk&#322;ad"
  ]
  node [
    id 35
    label "rise"
  ]
  node [
    id 36
    label "spowodowa&#263;"
  ]
  node [
    id 37
    label "zniwelowa&#263;"
  ]
  node [
    id 38
    label "przesun&#261;&#263;"
  ]
  node [
    id 39
    label "retract"
  ]
  node [
    id 40
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 41
    label "prawomocnie"
  ]
  node [
    id 42
    label "wa&#380;ny"
  ]
  node [
    id 43
    label "order"
  ]
  node [
    id 44
    label "kara"
  ]
  node [
    id 45
    label "judgment"
  ]
  node [
    id 46
    label "sentencja"
  ]
  node [
    id 47
    label "condemn"
  ]
  node [
    id 48
    label "okre&#347;la&#263;"
  ]
  node [
    id 49
    label "wydawa&#263;_wyrok"
  ]
  node [
    id 50
    label "zrobienie"
  ]
  node [
    id 51
    label "wypowied&#378;"
  ]
  node [
    id 52
    label "decyzja"
  ]
  node [
    id 53
    label "podj&#281;cie"
  ]
  node [
    id 54
    label "zale&#380;ny"
  ]
  node [
    id 55
    label "warunkowo"
  ]
  node [
    id 56
    label "remit"
  ]
  node [
    id 57
    label "obni&#380;y&#263;"
  ]
  node [
    id 58
    label "break"
  ]
  node [
    id 59
    label "sko&#324;czy&#263;"
  ]
  node [
    id 60
    label "zabi&#263;"
  ]
  node [
    id 61
    label "robienie"
  ]
  node [
    id 62
    label "czynno&#347;&#263;"
  ]
  node [
    id 63
    label "zachowanie"
  ]
  node [
    id 64
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 65
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 66
    label "kognicja"
  ]
  node [
    id 67
    label "rozprawa"
  ]
  node [
    id 68
    label "kazanie"
  ]
  node [
    id 69
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 70
    label "campaign"
  ]
  node [
    id 71
    label "fashion"
  ]
  node [
    id 72
    label "przes&#322;anka"
  ]
  node [
    id 73
    label "zmierzanie"
  ]
  node [
    id 74
    label "karnie"
  ]
  node [
    id 75
    label "zdyscyplinowany"
  ]
  node [
    id 76
    label "strza&#322;"
  ]
  node [
    id 77
    label "pos&#322;uszny"
  ]
  node [
    id 78
    label "wzorowy"
  ]
  node [
    id 79
    label "delivery"
  ]
  node [
    id 80
    label "podanie"
  ]
  node [
    id 81
    label "issue"
  ]
  node [
    id 82
    label "danie"
  ]
  node [
    id 83
    label "rendition"
  ]
  node [
    id 84
    label "egzemplarz"
  ]
  node [
    id 85
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 86
    label "impression"
  ]
  node [
    id 87
    label "odmiana"
  ]
  node [
    id 88
    label "zapach"
  ]
  node [
    id 89
    label "wytworzenie"
  ]
  node [
    id 90
    label "wprowadzenie"
  ]
  node [
    id 91
    label "zdarzenie_si&#281;"
  ]
  node [
    id 92
    label "ujawnienie"
  ]
  node [
    id 93
    label "reszta"
  ]
  node [
    id 94
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 95
    label "zadenuncjowanie"
  ]
  node [
    id 96
    label "czasopismo"
  ]
  node [
    id 97
    label "urz&#261;dzenie"
  ]
  node [
    id 98
    label "d&#378;wi&#281;k"
  ]
  node [
    id 99
    label "publikacja"
  ]
  node [
    id 100
    label "niewinny"
  ]
  node [
    id 101
    label "traktowa&#263;"
  ]
  node [
    id 102
    label "zezwala&#263;"
  ]
  node [
    id 103
    label "render"
  ]
  node [
    id 104
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 105
    label "postpone"
  ]
  node [
    id 106
    label "kobieta"
  ]
  node [
    id 107
    label "poddawa&#263;"
  ]
  node [
    id 108
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 109
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 110
    label "przywo&#322;a&#263;"
  ]
  node [
    id 111
    label "subject"
  ]
  node [
    id 112
    label "ca&#322;y"
  ]
  node [
    id 113
    label "rezultat"
  ]
  node [
    id 114
    label "pracownik"
  ]
  node [
    id 115
    label "cz&#322;owiek"
  ]
  node [
    id 116
    label "dyscyplinarnie"
  ]
  node [
    id 117
    label "pom&#243;c"
  ]
  node [
    id 118
    label "zbudowa&#263;"
  ]
  node [
    id 119
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 120
    label "leave"
  ]
  node [
    id 121
    label "przewie&#347;&#263;"
  ]
  node [
    id 122
    label "wykona&#263;"
  ]
  node [
    id 123
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 124
    label "draw"
  ]
  node [
    id 125
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 126
    label "carry"
  ]
  node [
    id 127
    label "odwodnienie"
  ]
  node [
    id 128
    label "konstytucja"
  ]
  node [
    id 129
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 130
    label "substancja_chemiczna"
  ]
  node [
    id 131
    label "bratnia_dusza"
  ]
  node [
    id 132
    label "zwi&#261;zanie"
  ]
  node [
    id 133
    label "lokant"
  ]
  node [
    id 134
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 135
    label "zwi&#261;za&#263;"
  ]
  node [
    id 136
    label "organizacja"
  ]
  node [
    id 137
    label "odwadnia&#263;"
  ]
  node [
    id 138
    label "marriage"
  ]
  node [
    id 139
    label "marketing_afiliacyjny"
  ]
  node [
    id 140
    label "bearing"
  ]
  node [
    id 141
    label "wi&#261;zanie"
  ]
  node [
    id 142
    label "odwadnianie"
  ]
  node [
    id 143
    label "koligacja"
  ]
  node [
    id 144
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 145
    label "odwodni&#263;"
  ]
  node [
    id 146
    label "azeotrop"
  ]
  node [
    id 147
    label "powi&#261;zanie"
  ]
  node [
    id 148
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 149
    label "predykatywno&#347;&#263;"
  ]
  node [
    id 150
    label "procesowicz"
  ]
  node [
    id 151
    label "pods&#261;dny"
  ]
  node [
    id 152
    label "podejrzany"
  ]
  node [
    id 153
    label "broni&#263;"
  ]
  node [
    id 154
    label "bronienie"
  ]
  node [
    id 155
    label "system"
  ]
  node [
    id 156
    label "my&#347;l"
  ]
  node [
    id 157
    label "wytw&#243;r"
  ]
  node [
    id 158
    label "urz&#261;d"
  ]
  node [
    id 159
    label "konektyw"
  ]
  node [
    id 160
    label "court"
  ]
  node [
    id 161
    label "obrona"
  ]
  node [
    id 162
    label "s&#261;downictwo"
  ]
  node [
    id 163
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 164
    label "forum"
  ]
  node [
    id 165
    label "zesp&#243;&#322;"
  ]
  node [
    id 166
    label "skazany"
  ]
  node [
    id 167
    label "&#347;wiadek"
  ]
  node [
    id 168
    label "antylogizm"
  ]
  node [
    id 169
    label "strona"
  ]
  node [
    id 170
    label "oskar&#380;yciel"
  ]
  node [
    id 171
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 172
    label "biuro"
  ]
  node [
    id 173
    label "instytucja"
  ]
  node [
    id 174
    label "prawnik"
  ]
  node [
    id 175
    label "oskar&#380;yciel_publiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 50
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 116
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 117
  ]
  edge [
    source 22
    target 118
  ]
  edge [
    source 22
    target 119
  ]
  edge [
    source 22
    target 120
  ]
  edge [
    source 22
    target 121
  ]
  edge [
    source 22
    target 122
  ]
  edge [
    source 22
    target 123
  ]
  edge [
    source 22
    target 124
  ]
  edge [
    source 22
    target 125
  ]
  edge [
    source 22
    target 126
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 127
  ]
  edge [
    source 23
    target 128
  ]
  edge [
    source 23
    target 129
  ]
  edge [
    source 23
    target 130
  ]
  edge [
    source 23
    target 131
  ]
  edge [
    source 23
    target 132
  ]
  edge [
    source 23
    target 133
  ]
  edge [
    source 23
    target 134
  ]
  edge [
    source 23
    target 135
  ]
  edge [
    source 23
    target 136
  ]
  edge [
    source 23
    target 137
  ]
  edge [
    source 23
    target 138
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 23
    target 140
  ]
  edge [
    source 23
    target 141
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 148
  ]
  edge [
    source 24
    target 51
  ]
  edge [
    source 24
    target 149
  ]
  edge [
    source 24
    target 52
  ]
  edge [
    source 25
    target 150
  ]
  edge [
    source 25
    target 51
  ]
  edge [
    source 25
    target 151
  ]
  edge [
    source 25
    target 152
  ]
  edge [
    source 25
    target 153
  ]
  edge [
    source 25
    target 154
  ]
  edge [
    source 25
    target 155
  ]
  edge [
    source 25
    target 156
  ]
  edge [
    source 25
    target 157
  ]
  edge [
    source 25
    target 158
  ]
  edge [
    source 25
    target 159
  ]
  edge [
    source 25
    target 160
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 162
  ]
  edge [
    source 25
    target 163
  ]
  edge [
    source 25
    target 164
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 32
  ]
  edge [
    source 25
    target 167
  ]
  edge [
    source 25
    target 168
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 25
    target 172
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 26
    target 114
  ]
  edge [
    source 26
    target 174
  ]
  edge [
    source 26
    target 175
  ]
]
