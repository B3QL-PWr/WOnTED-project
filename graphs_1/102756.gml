graph [
  maxDegree 26
  minDegree 1
  meanDegree 1.9636363636363636
  density 0.03636363636363636
  graphCliqueNumber 2
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "urz&#281;dowy"
    origin "text"
  ]
  node [
    id 2
    label "minister"
    origin "text"
  ]
  node [
    id 3
    label "praca"
    origin "text"
  ]
  node [
    id 4
    label "polityka"
    origin "text"
  ]
  node [
    id 5
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 6
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "spis"
  ]
  node [
    id 8
    label "sheet"
  ]
  node [
    id 9
    label "gazeta"
  ]
  node [
    id 10
    label "diariusz"
  ]
  node [
    id 11
    label "pami&#281;tnik"
  ]
  node [
    id 12
    label "journal"
  ]
  node [
    id 13
    label "ksi&#281;ga"
  ]
  node [
    id 14
    label "program_informacyjny"
  ]
  node [
    id 15
    label "urz&#281;dowo"
  ]
  node [
    id 16
    label "oficjalny"
  ]
  node [
    id 17
    label "formalny"
  ]
  node [
    id 18
    label "Goebbels"
  ]
  node [
    id 19
    label "Sto&#322;ypin"
  ]
  node [
    id 20
    label "rz&#261;d"
  ]
  node [
    id 21
    label "dostojnik"
  ]
  node [
    id 22
    label "stosunek_pracy"
  ]
  node [
    id 23
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 24
    label "benedykty&#324;ski"
  ]
  node [
    id 25
    label "pracowanie"
  ]
  node [
    id 26
    label "zaw&#243;d"
  ]
  node [
    id 27
    label "kierownictwo"
  ]
  node [
    id 28
    label "zmiana"
  ]
  node [
    id 29
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 30
    label "wytw&#243;r"
  ]
  node [
    id 31
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 32
    label "tynkarski"
  ]
  node [
    id 33
    label "czynnik_produkcji"
  ]
  node [
    id 34
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 35
    label "zobowi&#261;zanie"
  ]
  node [
    id 36
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 37
    label "czynno&#347;&#263;"
  ]
  node [
    id 38
    label "tyrka"
  ]
  node [
    id 39
    label "pracowa&#263;"
  ]
  node [
    id 40
    label "siedziba"
  ]
  node [
    id 41
    label "poda&#380;_pracy"
  ]
  node [
    id 42
    label "miejsce"
  ]
  node [
    id 43
    label "zak&#322;ad"
  ]
  node [
    id 44
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 45
    label "najem"
  ]
  node [
    id 46
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 47
    label "policy"
  ]
  node [
    id 48
    label "dyplomacja"
  ]
  node [
    id 49
    label "Europejska_Polityka_S&#261;siedztwa"
  ]
  node [
    id 50
    label "metoda"
  ]
  node [
    id 51
    label "niepubliczny"
  ]
  node [
    id 52
    label "spo&#322;ecznie"
  ]
  node [
    id 53
    label "publiczny"
  ]
  node [
    id 54
    label "miesi&#261;c"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 54
  ]
]
