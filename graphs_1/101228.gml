graph [
  maxDegree 4
  minDegree 1
  meanDegree 2.0952380952380953
  density 0.10476190476190476
  graphCliqueNumber 5
  node [
    id 0
    label "szpic"
    origin "text"
  ]
  node [
    id 1
    label "miniaturowy"
    origin "text"
  ]
  node [
    id 2
    label "pies_obro&#324;czy"
  ]
  node [
    id 3
    label "zako&#324;czenie"
  ]
  node [
    id 4
    label "miniaturowo"
  ]
  node [
    id 5
    label "male&#324;ki"
  ]
  node [
    id 6
    label "ma&#322;y"
  ]
  node [
    id 7
    label "Nicole"
  ]
  node [
    id 8
    label "Richie"
  ]
  node [
    id 9
    label "da&#263;"
  ]
  node [
    id 10
    label "Scully"
  ]
  node [
    id 11
    label "zeszyt"
  ]
  node [
    id 12
    label "archiwum"
  ]
  node [
    id 13
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 14
    label "London"
  ]
  node [
    id 15
    label "Tipton"
  ]
  node [
    id 16
    label "on"
  ]
  node [
    id 17
    label "m&#243;j"
  ]
  node [
    id 18
    label "to"
  ]
  node [
    id 19
    label "jaka"
  ]
  node [
    id 20
    label "hotel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
]
