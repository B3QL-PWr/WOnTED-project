graph [
  maxDegree 26
  minDegree 1
  meanDegree 1.9661016949152543
  density 0.03389830508474576
  graphCliqueNumber 3
  node [
    id 0
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 1
    label "stary"
    origin "text"
  ]
  node [
    id 2
    label "kolejka"
    origin "text"
  ]
  node [
    id 3
    label "azja"
    origin "text"
  ]
  node [
    id 4
    label "pi&#281;trowy"
    origin "text"
  ]
  node [
    id 5
    label "tramwaj"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
  ]
  node [
    id 7
    label "czeka&#263;"
  ]
  node [
    id 8
    label "lookout"
  ]
  node [
    id 9
    label "wyziera&#263;"
  ]
  node [
    id 10
    label "peep"
  ]
  node [
    id 11
    label "look"
  ]
  node [
    id 12
    label "patrze&#263;"
  ]
  node [
    id 13
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 14
    label "nienowoczesny"
  ]
  node [
    id 15
    label "dojrza&#322;y"
  ]
  node [
    id 16
    label "starzenie_si&#281;"
  ]
  node [
    id 17
    label "m&#261;&#380;"
  ]
  node [
    id 18
    label "starczo"
  ]
  node [
    id 19
    label "zestarzenie_si&#281;"
  ]
  node [
    id 20
    label "po_staro&#347;wiecku"
  ]
  node [
    id 21
    label "d&#322;ugoletni"
  ]
  node [
    id 22
    label "dawno"
  ]
  node [
    id 23
    label "dawniej"
  ]
  node [
    id 24
    label "znajomy"
  ]
  node [
    id 25
    label "gruba_ryba"
  ]
  node [
    id 26
    label "ojciec"
  ]
  node [
    id 27
    label "niegdysiejszy"
  ]
  node [
    id 28
    label "brat"
  ]
  node [
    id 29
    label "staro"
  ]
  node [
    id 30
    label "charakterystyczny"
  ]
  node [
    id 31
    label "starzy"
  ]
  node [
    id 32
    label "dotychczasowy"
  ]
  node [
    id 33
    label "p&#243;&#378;ny"
  ]
  node [
    id 34
    label "odleg&#322;y"
  ]
  node [
    id 35
    label "poprzedni"
  ]
  node [
    id 36
    label "zwierzchnik"
  ]
  node [
    id 37
    label "seria"
  ]
  node [
    id 38
    label "kolej"
  ]
  node [
    id 39
    label "nagromadzenie"
  ]
  node [
    id 40
    label "zabawka"
  ]
  node [
    id 41
    label "line"
  ]
  node [
    id 42
    label "rz&#261;d"
  ]
  node [
    id 43
    label "porcja"
  ]
  node [
    id 44
    label "struktura"
  ]
  node [
    id 45
    label "zagmatwany"
  ]
  node [
    id 46
    label "pi&#281;trowo"
  ]
  node [
    id 47
    label "du&#380;y"
  ]
  node [
    id 48
    label "rozbudowany"
  ]
  node [
    id 49
    label "g&#243;rny"
  ]
  node [
    id 50
    label "pojazd_szynowy"
  ]
  node [
    id 51
    label "odbierak"
  ]
  node [
    id 52
    label "bimba"
  ]
  node [
    id 53
    label "miasto"
  ]
  node [
    id 54
    label "&#347;rodek_transportu_publicznego"
  ]
  node [
    id 55
    label "wagon"
  ]
  node [
    id 56
    label "Hong"
  ]
  node [
    id 57
    label "Kongo"
  ]
  node [
    id 58
    label "zwa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 57
    target 58
  ]
]
