graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.050632911392405
  density 0.008689122505900022
  graphCliqueNumber 3
  node [
    id 0
    label "teoria"
    origin "text"
  ]
  node [
    id 1
    label "autorstwo"
    origin "text"
  ]
  node [
    id 2
    label "nosi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ukry&#263;"
    origin "text"
  ]
  node [
    id 5
    label "egotyzm"
    origin "text"
  ]
  node [
    id 6
    label "implicit"
    origin "text"
  ]
  node [
    id 7
    label "egotism"
    origin "text"
  ]
  node [
    id 8
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "rok"
    origin "text"
  ]
  node [
    id 10
    label "badan"
    origin "text"
  ]
  node [
    id 11
    label "charakter"
    origin "text"
  ]
  node [
    id 12
    label "analiza"
    origin "text"
  ]
  node [
    id 13
    label "dana"
    origin "text"
  ]
  node [
    id 14
    label "statystyczny"
    origin "text"
  ]
  node [
    id 15
    label "dowie&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "istotnie"
    origin "text"
  ]
  node [
    id 17
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 18
    label "charakteryzowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "ci&#261;gota"
    origin "text"
  ]
  node [
    id 21
    label "miejsce"
    origin "text"
  ]
  node [
    id 22
    label "rzecz"
    origin "text"
  ]
  node [
    id 23
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 24
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 25
    label "stopie&#324;"
    origin "text"
  ]
  node [
    id 26
    label "podobny"
    origin "text"
  ]
  node [
    id 27
    label "by&#263;"
    origin "text"
  ]
  node [
    id 28
    label "kojarzy&#263;"
    origin "text"
  ]
  node [
    id 29
    label "siebie"
    origin "text"
  ]
  node [
    id 30
    label "w&#322;a&#347;ciwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 31
    label "system"
  ]
  node [
    id 32
    label "zderzenie_si&#281;"
  ]
  node [
    id 33
    label "s&#261;d"
  ]
  node [
    id 34
    label "twierdzenie"
  ]
  node [
    id 35
    label "teoria_Dowa"
  ]
  node [
    id 36
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 37
    label "teologicznie"
  ]
  node [
    id 38
    label "przypuszczenie"
  ]
  node [
    id 39
    label "belief"
  ]
  node [
    id 40
    label "wiedza"
  ]
  node [
    id 41
    label "teoria_Fishera"
  ]
  node [
    id 42
    label "teoria_Arrheniusa"
  ]
  node [
    id 43
    label "pochodzenie"
  ]
  node [
    id 44
    label "patent"
  ]
  node [
    id 45
    label "str&#243;j"
  ]
  node [
    id 46
    label "wk&#322;ada&#263;"
  ]
  node [
    id 47
    label "przemieszcza&#263;"
  ]
  node [
    id 48
    label "posiada&#263;"
  ]
  node [
    id 49
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 50
    label "wear"
  ]
  node [
    id 51
    label "carry"
  ]
  node [
    id 52
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 53
    label "czu&#263;"
  ]
  node [
    id 54
    label "need"
  ]
  node [
    id 55
    label "hide"
  ]
  node [
    id 56
    label "support"
  ]
  node [
    id 57
    label "zachowa&#263;"
  ]
  node [
    id 58
    label "ensconce"
  ]
  node [
    id 59
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 60
    label "umie&#347;ci&#263;"
  ]
  node [
    id 61
    label "przytai&#263;"
  ]
  node [
    id 62
    label "egocentryzm"
  ]
  node [
    id 63
    label "upubliczni&#263;"
  ]
  node [
    id 64
    label "wydawnictwo"
  ]
  node [
    id 65
    label "wprowadzi&#263;"
  ]
  node [
    id 66
    label "picture"
  ]
  node [
    id 67
    label "stulecie"
  ]
  node [
    id 68
    label "kalendarz"
  ]
  node [
    id 69
    label "czas"
  ]
  node [
    id 70
    label "pora_roku"
  ]
  node [
    id 71
    label "cykl_astronomiczny"
  ]
  node [
    id 72
    label "p&#243;&#322;rocze"
  ]
  node [
    id 73
    label "grupa"
  ]
  node [
    id 74
    label "kwarta&#322;"
  ]
  node [
    id 75
    label "kurs"
  ]
  node [
    id 76
    label "jubileusz"
  ]
  node [
    id 77
    label "miesi&#261;c"
  ]
  node [
    id 78
    label "lata"
  ]
  node [
    id 79
    label "martwy_sezon"
  ]
  node [
    id 80
    label "zjawisko"
  ]
  node [
    id 81
    label "psychika"
  ]
  node [
    id 82
    label "przedmiot"
  ]
  node [
    id 83
    label "cecha"
  ]
  node [
    id 84
    label "entity"
  ]
  node [
    id 85
    label "zbi&#243;r"
  ]
  node [
    id 86
    label "wydarzenie"
  ]
  node [
    id 87
    label "kompleksja"
  ]
  node [
    id 88
    label "osobowo&#347;&#263;"
  ]
  node [
    id 89
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 90
    label "posta&#263;"
  ]
  node [
    id 91
    label "fizjonomia"
  ]
  node [
    id 92
    label "opis"
  ]
  node [
    id 93
    label "analysis"
  ]
  node [
    id 94
    label "reakcja_chemiczna"
  ]
  node [
    id 95
    label "dissection"
  ]
  node [
    id 96
    label "badanie"
  ]
  node [
    id 97
    label "metoda"
  ]
  node [
    id 98
    label "dar"
  ]
  node [
    id 99
    label "cnota"
  ]
  node [
    id 100
    label "buddyzm"
  ]
  node [
    id 101
    label "statystycznie"
  ]
  node [
    id 102
    label "przeci&#281;tny"
  ]
  node [
    id 103
    label "moderate"
  ]
  node [
    id 104
    label "uzasadni&#263;"
  ]
  node [
    id 105
    label "leave"
  ]
  node [
    id 106
    label "testify"
  ]
  node [
    id 107
    label "zrobi&#263;"
  ]
  node [
    id 108
    label "doprowadzi&#263;"
  ]
  node [
    id 109
    label "realnie"
  ]
  node [
    id 110
    label "importantly"
  ]
  node [
    id 111
    label "istotny"
  ]
  node [
    id 112
    label "wa&#380;ny"
  ]
  node [
    id 113
    label "asymilowa&#263;"
  ]
  node [
    id 114
    label "wapniak"
  ]
  node [
    id 115
    label "dwun&#243;g"
  ]
  node [
    id 116
    label "polifag"
  ]
  node [
    id 117
    label "wz&#243;r"
  ]
  node [
    id 118
    label "profanum"
  ]
  node [
    id 119
    label "hominid"
  ]
  node [
    id 120
    label "homo_sapiens"
  ]
  node [
    id 121
    label "nasada"
  ]
  node [
    id 122
    label "podw&#322;adny"
  ]
  node [
    id 123
    label "ludzko&#347;&#263;"
  ]
  node [
    id 124
    label "os&#322;abianie"
  ]
  node [
    id 125
    label "mikrokosmos"
  ]
  node [
    id 126
    label "portrecista"
  ]
  node [
    id 127
    label "duch"
  ]
  node [
    id 128
    label "oddzia&#322;ywanie"
  ]
  node [
    id 129
    label "g&#322;owa"
  ]
  node [
    id 130
    label "asymilowanie"
  ]
  node [
    id 131
    label "osoba"
  ]
  node [
    id 132
    label "os&#322;abia&#263;"
  ]
  node [
    id 133
    label "figura"
  ]
  node [
    id 134
    label "Adam"
  ]
  node [
    id 135
    label "senior"
  ]
  node [
    id 136
    label "antropochoria"
  ]
  node [
    id 137
    label "report"
  ]
  node [
    id 138
    label "opisywa&#263;"
  ]
  node [
    id 139
    label "mark"
  ]
  node [
    id 140
    label "przygotowywa&#263;"
  ]
  node [
    id 141
    label "cechowa&#263;"
  ]
  node [
    id 142
    label "cia&#322;o"
  ]
  node [
    id 143
    label "plac"
  ]
  node [
    id 144
    label "uwaga"
  ]
  node [
    id 145
    label "przestrze&#324;"
  ]
  node [
    id 146
    label "status"
  ]
  node [
    id 147
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 148
    label "chwila"
  ]
  node [
    id 149
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 150
    label "rz&#261;d"
  ]
  node [
    id 151
    label "praca"
  ]
  node [
    id 152
    label "location"
  ]
  node [
    id 153
    label "warunek_lokalowy"
  ]
  node [
    id 154
    label "obiekt"
  ]
  node [
    id 155
    label "temat"
  ]
  node [
    id 156
    label "istota"
  ]
  node [
    id 157
    label "wpa&#347;&#263;"
  ]
  node [
    id 158
    label "wpadanie"
  ]
  node [
    id 159
    label "wpada&#263;"
  ]
  node [
    id 160
    label "kultura"
  ]
  node [
    id 161
    label "przyroda"
  ]
  node [
    id 162
    label "mienie"
  ]
  node [
    id 163
    label "object"
  ]
  node [
    id 164
    label "wpadni&#281;cie"
  ]
  node [
    id 165
    label "jako&#347;"
  ]
  node [
    id 166
    label "charakterystyczny"
  ]
  node [
    id 167
    label "ciekawy"
  ]
  node [
    id 168
    label "jako_tako"
  ]
  node [
    id 169
    label "dziwny"
  ]
  node [
    id 170
    label "niez&#322;y"
  ]
  node [
    id 171
    label "przyzwoity"
  ]
  node [
    id 172
    label "minuta"
  ]
  node [
    id 173
    label "forma"
  ]
  node [
    id 174
    label "znaczenie"
  ]
  node [
    id 175
    label "kategoria_gramatyczna"
  ]
  node [
    id 176
    label "przys&#322;&#243;wek"
  ]
  node [
    id 177
    label "szczebel"
  ]
  node [
    id 178
    label "element"
  ]
  node [
    id 179
    label "poziom"
  ]
  node [
    id 180
    label "degree"
  ]
  node [
    id 181
    label "podn&#243;&#380;ek"
  ]
  node [
    id 182
    label "rank"
  ]
  node [
    id 183
    label "przymiotnik"
  ]
  node [
    id 184
    label "podzia&#322;"
  ]
  node [
    id 185
    label "ocena"
  ]
  node [
    id 186
    label "kszta&#322;t"
  ]
  node [
    id 187
    label "wschodek"
  ]
  node [
    id 188
    label "schody"
  ]
  node [
    id 189
    label "gama"
  ]
  node [
    id 190
    label "podstopie&#324;"
  ]
  node [
    id 191
    label "d&#378;wi&#281;k"
  ]
  node [
    id 192
    label "wielko&#347;&#263;"
  ]
  node [
    id 193
    label "jednostka"
  ]
  node [
    id 194
    label "podobnie"
  ]
  node [
    id 195
    label "upodabnianie_si&#281;"
  ]
  node [
    id 196
    label "zasymilowanie"
  ]
  node [
    id 197
    label "drugi"
  ]
  node [
    id 198
    label "taki"
  ]
  node [
    id 199
    label "upodobnienie"
  ]
  node [
    id 200
    label "przypominanie"
  ]
  node [
    id 201
    label "upodobnienie_si&#281;"
  ]
  node [
    id 202
    label "si&#281;ga&#263;"
  ]
  node [
    id 203
    label "trwa&#263;"
  ]
  node [
    id 204
    label "obecno&#347;&#263;"
  ]
  node [
    id 205
    label "stan"
  ]
  node [
    id 206
    label "stand"
  ]
  node [
    id 207
    label "mie&#263;_miejsce"
  ]
  node [
    id 208
    label "uczestniczy&#263;"
  ]
  node [
    id 209
    label "chodzi&#263;"
  ]
  node [
    id 210
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 211
    label "equal"
  ]
  node [
    id 212
    label "cover"
  ]
  node [
    id 213
    label "rozumie&#263;"
  ]
  node [
    id 214
    label "zaskakiwa&#263;"
  ]
  node [
    id 215
    label "swat"
  ]
  node [
    id 216
    label "relate"
  ]
  node [
    id 217
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 218
    label "wyregulowanie"
  ]
  node [
    id 219
    label "warto&#347;&#263;"
  ]
  node [
    id 220
    label "charakterystyka"
  ]
  node [
    id 221
    label "kompetencja"
  ]
  node [
    id 222
    label "regulowanie"
  ]
  node [
    id 223
    label "feature"
  ]
  node [
    id 224
    label "wyregulowa&#263;"
  ]
  node [
    id 225
    label "regulowa&#263;"
  ]
  node [
    id 226
    label "standard"
  ]
  node [
    id 227
    label "attribute"
  ]
  node [
    id 228
    label "zaleta"
  ]
  node [
    id 229
    label "Virginia"
  ]
  node [
    id 230
    label "Beach"
  ]
  node [
    id 231
    label "Two"
  ]
  node [
    id 232
    label "Harbors"
  ]
  node [
    id 233
    label "Three"
  ]
  node [
    id 234
    label "Oaks"
  ]
  node [
    id 235
    label "Six"
  ]
  node [
    id 236
    label "mile"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 142
  ]
  edge [
    source 21
    target 143
  ]
  edge [
    source 21
    target 83
  ]
  edge [
    source 21
    target 144
  ]
  edge [
    source 21
    target 145
  ]
  edge [
    source 21
    target 146
  ]
  edge [
    source 21
    target 147
  ]
  edge [
    source 21
    target 148
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 21
    target 150
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 22
    target 155
  ]
  edge [
    source 22
    target 156
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 82
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 24
    target 167
  ]
  edge [
    source 24
    target 168
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 170
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 172
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 25
    target 181
  ]
  edge [
    source 25
    target 182
  ]
  edge [
    source 25
    target 183
  ]
  edge [
    source 25
    target 184
  ]
  edge [
    source 25
    target 185
  ]
  edge [
    source 25
    target 186
  ]
  edge [
    source 25
    target 187
  ]
  edge [
    source 25
    target 188
  ]
  edge [
    source 25
    target 189
  ]
  edge [
    source 25
    target 190
  ]
  edge [
    source 25
    target 191
  ]
  edge [
    source 25
    target 192
  ]
  edge [
    source 25
    target 193
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 194
  ]
  edge [
    source 26
    target 195
  ]
  edge [
    source 26
    target 196
  ]
  edge [
    source 26
    target 197
  ]
  edge [
    source 26
    target 198
  ]
  edge [
    source 26
    target 199
  ]
  edge [
    source 26
    target 166
  ]
  edge [
    source 26
    target 200
  ]
  edge [
    source 26
    target 130
  ]
  edge [
    source 26
    target 201
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 202
  ]
  edge [
    source 27
    target 203
  ]
  edge [
    source 27
    target 204
  ]
  edge [
    source 27
    target 205
  ]
  edge [
    source 27
    target 52
  ]
  edge [
    source 27
    target 206
  ]
  edge [
    source 27
    target 207
  ]
  edge [
    source 27
    target 208
  ]
  edge [
    source 27
    target 209
  ]
  edge [
    source 27
    target 210
  ]
  edge [
    source 27
    target 211
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 212
  ]
  edge [
    source 28
    target 213
  ]
  edge [
    source 28
    target 214
  ]
  edge [
    source 28
    target 215
  ]
  edge [
    source 28
    target 216
  ]
  edge [
    source 28
    target 217
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 218
  ]
  edge [
    source 30
    target 219
  ]
  edge [
    source 30
    target 220
  ]
  edge [
    source 30
    target 221
  ]
  edge [
    source 30
    target 83
  ]
  edge [
    source 30
    target 222
  ]
  edge [
    source 30
    target 223
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 30
    target 227
  ]
  edge [
    source 30
    target 228
  ]
  edge [
    source 229
    target 230
  ]
  edge [
    source 231
    target 232
  ]
  edge [
    source 233
    target 234
  ]
  edge [
    source 235
    target 236
  ]
]
