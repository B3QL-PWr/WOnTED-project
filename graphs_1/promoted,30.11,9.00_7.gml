graph [
  maxDegree 34
  minDegree 1
  meanDegree 1.9759036144578312
  density 0.024096385542168676
  graphCliqueNumber 2
  node [
    id 0
    label "nagranie"
    origin "text"
  ]
  node [
    id 1
    label "telefon"
    origin "text"
  ]
  node [
    id 2
    label "pokazowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kierowca"
    origin "text"
  ]
  node [
    id 4
    label "bmw"
    origin "text"
  ]
  node [
    id 5
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 6
    label "brak"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;zg"
    origin "text"
  ]
  node [
    id 8
    label "wys&#322;uchanie"
  ]
  node [
    id 9
    label "wytw&#243;r"
  ]
  node [
    id 10
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 11
    label "recording"
  ]
  node [
    id 12
    label "utrwalenie"
  ]
  node [
    id 13
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 14
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 15
    label "coalescence"
  ]
  node [
    id 16
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 17
    label "phreaker"
  ]
  node [
    id 18
    label "infrastruktura"
  ]
  node [
    id 19
    label "wy&#347;wietlacz"
  ]
  node [
    id 20
    label "provider"
  ]
  node [
    id 21
    label "dzwonienie"
  ]
  node [
    id 22
    label "zadzwoni&#263;"
  ]
  node [
    id 23
    label "dzwoni&#263;"
  ]
  node [
    id 24
    label "kontakt"
  ]
  node [
    id 25
    label "mikrotelefon"
  ]
  node [
    id 26
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 27
    label "po&#322;&#261;czenie"
  ]
  node [
    id 28
    label "numer"
  ]
  node [
    id 29
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 30
    label "instalacja"
  ]
  node [
    id 31
    label "billing"
  ]
  node [
    id 32
    label "urz&#261;dzenie"
  ]
  node [
    id 33
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "transportowiec"
  ]
  node [
    id 36
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 37
    label "BMW"
  ]
  node [
    id 38
    label "samoch&#243;d"
  ]
  node [
    id 39
    label "prywatywny"
  ]
  node [
    id 40
    label "defect"
  ]
  node [
    id 41
    label "odej&#347;cie"
  ]
  node [
    id 42
    label "gap"
  ]
  node [
    id 43
    label "kr&#243;tki"
  ]
  node [
    id 44
    label "wyr&#243;b"
  ]
  node [
    id 45
    label "nieistnienie"
  ]
  node [
    id 46
    label "wada"
  ]
  node [
    id 47
    label "odej&#347;&#263;"
  ]
  node [
    id 48
    label "odchodzenie"
  ]
  node [
    id 49
    label "odchodzi&#263;"
  ]
  node [
    id 50
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 51
    label "elektroencefalogram"
  ]
  node [
    id 52
    label "substancja_szara"
  ]
  node [
    id 53
    label "przodom&#243;zgowie"
  ]
  node [
    id 54
    label "bruzda"
  ]
  node [
    id 55
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 56
    label "wzg&#243;rze"
  ]
  node [
    id 57
    label "umys&#322;"
  ]
  node [
    id 58
    label "zw&#243;j"
  ]
  node [
    id 59
    label "kresom&#243;zgowie"
  ]
  node [
    id 60
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 61
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 62
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 63
    label "przysadka"
  ]
  node [
    id 64
    label "wiedza"
  ]
  node [
    id 65
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 66
    label "przedmurze"
  ]
  node [
    id 67
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 68
    label "projektodawca"
  ]
  node [
    id 69
    label "noosfera"
  ]
  node [
    id 70
    label "cecha"
  ]
  node [
    id 71
    label "g&#322;owa"
  ]
  node [
    id 72
    label "organ"
  ]
  node [
    id 73
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 74
    label "most"
  ]
  node [
    id 75
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 76
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 77
    label "encefalografia"
  ]
  node [
    id 78
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 79
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 80
    label "kora_m&#243;zgowa"
  ]
  node [
    id 81
    label "podwzg&#243;rze"
  ]
  node [
    id 82
    label "poduszka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
]
