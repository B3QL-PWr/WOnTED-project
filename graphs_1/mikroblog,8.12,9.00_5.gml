graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;ga&#263;"
  ]
  node [
    id 2
    label "trwa&#263;"
  ]
  node [
    id 3
    label "obecno&#347;&#263;"
  ]
  node [
    id 4
    label "stan"
  ]
  node [
    id 5
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 6
    label "stand"
  ]
  node [
    id 7
    label "mie&#263;_miejsce"
  ]
  node [
    id 8
    label "uczestniczy&#263;"
  ]
  node [
    id 9
    label "chodzi&#263;"
  ]
  node [
    id 10
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 11
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
]
