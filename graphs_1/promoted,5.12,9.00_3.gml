graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.9523809523809523
  density 0.047619047619047616
  graphCliqueNumber 3
  node [
    id 0
    label "ubieg&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "sp&#322;on&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kolejny"
    origin "text"
  ]
  node [
    id 4
    label "wysypisko"
    origin "text"
  ]
  node [
    id 5
    label "odpad"
    origin "text"
  ]
  node [
    id 6
    label "stara"
    origin "text"
  ]
  node [
    id 7
    label "opona"
    origin "text"
  ]
  node [
    id 8
    label "ostatni"
  ]
  node [
    id 9
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 10
    label "doba"
  ]
  node [
    id 11
    label "czas"
  ]
  node [
    id 12
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 13
    label "weekend"
  ]
  node [
    id 14
    label "miesi&#261;c"
  ]
  node [
    id 15
    label "zjara&#263;_si&#281;"
  ]
  node [
    id 16
    label "przegorze&#263;"
  ]
  node [
    id 17
    label "erupt"
  ]
  node [
    id 18
    label "zgorze&#263;"
  ]
  node [
    id 19
    label "ulec"
  ]
  node [
    id 20
    label "inny"
  ]
  node [
    id 21
    label "nast&#281;pnie"
  ]
  node [
    id 22
    label "kt&#243;ry&#347;"
  ]
  node [
    id 23
    label "kolejno"
  ]
  node [
    id 24
    label "nastopny"
  ]
  node [
    id 25
    label "bomba_ekologiczna"
  ]
  node [
    id 26
    label "sk&#322;adowisko"
  ]
  node [
    id 27
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 28
    label "matka"
  ]
  node [
    id 29
    label "kobieta"
  ]
  node [
    id 30
    label "partnerka"
  ]
  node [
    id 31
    label "&#380;ona"
  ]
  node [
    id 32
    label "starzy"
  ]
  node [
    id 33
    label "wa&#322;ek"
  ]
  node [
    id 34
    label "ogumienie"
  ]
  node [
    id 35
    label "meninx"
  ]
  node [
    id 36
    label "b&#322;ona"
  ]
  node [
    id 37
    label "ko&#322;o"
  ]
  node [
    id 38
    label "bie&#380;nik"
  ]
  node [
    id 39
    label "rybnicki"
  ]
  node [
    id 40
    label "alarm"
  ]
  node [
    id 41
    label "smogowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
]
