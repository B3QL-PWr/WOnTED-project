graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.7142857142857142
  density 0.2857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "kanter"
    origin "text"
  ]
  node [
    id 1
    label "uorkiestra"
    origin "text"
  ]
  node [
    id 2
    label "wina"
    origin "text"
  ]
  node [
    id 3
    label "wstyd"
  ]
  node [
    id 4
    label "konsekwencja"
  ]
  node [
    id 5
    label "guilt"
  ]
  node [
    id 6
    label "lutnia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
]
