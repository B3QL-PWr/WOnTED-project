graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9692307692307693
  density 0.03076923076923077
  graphCliqueNumber 2
  node [
    id 0
    label "aspirant"
    origin "text"
  ]
  node [
    id 1
    label "wojciech"
    origin "text"
  ]
  node [
    id 2
    label "gram"
    origin "text"
  ]
  node [
    id 3
    label "naczelnik"
    origin "text"
  ]
  node [
    id 4
    label "wydzia&#322;"
    origin "text"
  ]
  node [
    id 5
    label "prewencja"
    origin "text"
  ]
  node [
    id 6
    label "&#347;wiecie"
    origin "text"
  ]
  node [
    id 7
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 8
    label "katowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "podejrzana"
    origin "text"
  ]
  node [
    id 10
    label "podczas"
    origin "text"
  ]
  node [
    id 11
    label "przes&#322;uchanie"
    origin "text"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "ministrant"
  ]
  node [
    id 14
    label "kandydat"
  ]
  node [
    id 15
    label "materia&#322;"
  ]
  node [
    id 16
    label "lista_wyborcza"
  ]
  node [
    id 17
    label "aspirantura"
  ]
  node [
    id 18
    label "oficer_stra&#380;y_po&#380;arnej"
  ]
  node [
    id 19
    label "pracownik_naukowy"
  ]
  node [
    id 20
    label "bierne_prawo_wyborcze"
  ]
  node [
    id 21
    label "oficer_policji"
  ]
  node [
    id 22
    label "aspirowanie"
  ]
  node [
    id 23
    label "drzewo"
  ]
  node [
    id 24
    label "dekagram"
  ]
  node [
    id 25
    label "megagram"
  ]
  node [
    id 26
    label "centygram"
  ]
  node [
    id 27
    label "miligram"
  ]
  node [
    id 28
    label "metryczna_jednostka_masy"
  ]
  node [
    id 29
    label "naczelnikostwo"
  ]
  node [
    id 30
    label "zwierzchnik"
  ]
  node [
    id 31
    label "dow&#243;dca"
  ]
  node [
    id 32
    label "podsekcja"
  ]
  node [
    id 33
    label "whole"
  ]
  node [
    id 34
    label "relation"
  ]
  node [
    id 35
    label "politechnika"
  ]
  node [
    id 36
    label "katedra"
  ]
  node [
    id 37
    label "urz&#261;d"
  ]
  node [
    id 38
    label "uniwersytet"
  ]
  node [
    id 39
    label "jednostka_organizacyjna"
  ]
  node [
    id 40
    label "insourcing"
  ]
  node [
    id 41
    label "ministerstwo"
  ]
  node [
    id 42
    label "miejsce_pracy"
  ]
  node [
    id 43
    label "dzia&#322;"
  ]
  node [
    id 44
    label "ochrona"
  ]
  node [
    id 45
    label "restraint"
  ]
  node [
    id 46
    label "proszek"
  ]
  node [
    id 47
    label "nudzi&#263;"
  ]
  node [
    id 48
    label "extort"
  ]
  node [
    id 49
    label "bi&#263;"
  ]
  node [
    id 50
    label "m&#281;czy&#263;"
  ]
  node [
    id 51
    label "mistreat"
  ]
  node [
    id 52
    label "czynno&#347;&#263;"
  ]
  node [
    id 53
    label "technika_operacyjna"
  ]
  node [
    id 54
    label "wys&#322;uchanie"
  ]
  node [
    id 55
    label "inquisition"
  ]
  node [
    id 56
    label "magiel"
  ]
  node [
    id 57
    label "odpytanie"
  ]
  node [
    id 58
    label "spotkanie"
  ]
  node [
    id 59
    label "wypytanie"
  ]
  node [
    id 60
    label "tortury"
  ]
  node [
    id 61
    label "Inquisition"
  ]
  node [
    id 62
    label "skontrolowanie"
  ]
  node [
    id 63
    label "test"
  ]
  node [
    id 64
    label "Wojciech"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
]
