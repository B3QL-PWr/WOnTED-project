graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "symbol"
    origin "text"
  ]
  node [
    id 1
    label "kanada"
    origin "text"
  ]
  node [
    id 2
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 3
    label "symbolizowanie"
  ]
  node [
    id 4
    label "wcielenie"
  ]
  node [
    id 5
    label "notacja"
  ]
  node [
    id 6
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 7
    label "znak_pisarski"
  ]
  node [
    id 8
    label "znak"
  ]
  node [
    id 9
    label "character"
  ]
  node [
    id 10
    label "miejsce"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 10
  ]
]
