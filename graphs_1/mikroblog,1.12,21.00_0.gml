graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9642857142857142
  density 0.03571428571428571
  graphCliqueNumber 2
  node [
    id 0
    label "kiedy"
    origin "text"
  ]
  node [
    id 1
    label "zrobi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "tak"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;aby"
    origin "text"
  ]
  node [
    id 4
    label "gra"
    origin "text"
  ]
  node [
    id 5
    label "nawet"
    origin "text"
  ]
  node [
    id 6
    label "wyborczy"
    origin "text"
  ]
  node [
    id 7
    label "szkalowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "fallout"
    origin "text"
  ]
  node [
    id 9
    label "nieznaczny"
  ]
  node [
    id 10
    label "nieumiej&#281;tny"
  ]
  node [
    id 11
    label "marnie"
  ]
  node [
    id 12
    label "md&#322;y"
  ]
  node [
    id 13
    label "przemijaj&#261;cy"
  ]
  node [
    id 14
    label "zawodny"
  ]
  node [
    id 15
    label "delikatny"
  ]
  node [
    id 16
    label "&#322;agodny"
  ]
  node [
    id 17
    label "niedoskona&#322;y"
  ]
  node [
    id 18
    label "nietrwa&#322;y"
  ]
  node [
    id 19
    label "po&#347;ledni"
  ]
  node [
    id 20
    label "s&#322;abowity"
  ]
  node [
    id 21
    label "niefajny"
  ]
  node [
    id 22
    label "z&#322;y"
  ]
  node [
    id 23
    label "niemocny"
  ]
  node [
    id 24
    label "kiepsko"
  ]
  node [
    id 25
    label "niezdrowy"
  ]
  node [
    id 26
    label "lura"
  ]
  node [
    id 27
    label "s&#322;abo"
  ]
  node [
    id 28
    label "nieudany"
  ]
  node [
    id 29
    label "mizerny"
  ]
  node [
    id 30
    label "zabawa"
  ]
  node [
    id 31
    label "rywalizacja"
  ]
  node [
    id 32
    label "czynno&#347;&#263;"
  ]
  node [
    id 33
    label "Pok&#233;mon"
  ]
  node [
    id 34
    label "synteza"
  ]
  node [
    id 35
    label "odtworzenie"
  ]
  node [
    id 36
    label "komplet"
  ]
  node [
    id 37
    label "rekwizyt_do_gry"
  ]
  node [
    id 38
    label "odg&#322;os"
  ]
  node [
    id 39
    label "rozgrywka"
  ]
  node [
    id 40
    label "post&#281;powanie"
  ]
  node [
    id 41
    label "wydarzenie"
  ]
  node [
    id 42
    label "apparent_motion"
  ]
  node [
    id 43
    label "game"
  ]
  node [
    id 44
    label "zmienno&#347;&#263;"
  ]
  node [
    id 45
    label "zasada"
  ]
  node [
    id 46
    label "akcja"
  ]
  node [
    id 47
    label "play"
  ]
  node [
    id 48
    label "contest"
  ]
  node [
    id 49
    label "zbijany"
  ]
  node [
    id 50
    label "obrabia&#263;_dup&#281;"
  ]
  node [
    id 51
    label "&#322;oi&#263;"
  ]
  node [
    id 52
    label "slur"
  ]
  node [
    id 53
    label "plotkowa&#263;"
  ]
  node [
    id 54
    label "krytykowa&#263;"
  ]
  node [
    id 55
    label "ty"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
]
