graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.0684931506849313
  density 0.014265470004723665
  graphCliqueNumber 3
  node [
    id 0
    label "droga"
    origin "text"
  ]
  node [
    id 1
    label "vectra"
    origin "text"
  ]
  node [
    id 2
    label "naprawd&#281;"
    origin "text"
  ]
  node [
    id 3
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 5
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 6
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "otrzyma&#263;by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ulotka"
    origin "text"
  ]
  node [
    id 9
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 10
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "nawet"
    origin "text"
  ]
  node [
    id 13
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 14
    label "tylko"
    origin "text"
  ]
  node [
    id 15
    label "wy&#322;apa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 16
    label "ch&#281;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "skorzysta&#263;"
    origin "text"
  ]
  node [
    id 18
    label "wasz"
    origin "text"
  ]
  node [
    id 19
    label "oferta"
    origin "text"
  ]
  node [
    id 20
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 21
    label "journey"
  ]
  node [
    id 22
    label "podbieg"
  ]
  node [
    id 23
    label "bezsilnikowy"
  ]
  node [
    id 24
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 25
    label "wylot"
  ]
  node [
    id 26
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 27
    label "drogowskaz"
  ]
  node [
    id 28
    label "nawierzchnia"
  ]
  node [
    id 29
    label "turystyka"
  ]
  node [
    id 30
    label "budowla"
  ]
  node [
    id 31
    label "spos&#243;b"
  ]
  node [
    id 32
    label "passage"
  ]
  node [
    id 33
    label "marszrutyzacja"
  ]
  node [
    id 34
    label "zbior&#243;wka"
  ]
  node [
    id 35
    label "rajza"
  ]
  node [
    id 36
    label "ekskursja"
  ]
  node [
    id 37
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 38
    label "ruch"
  ]
  node [
    id 39
    label "trasa"
  ]
  node [
    id 40
    label "wyb&#243;j"
  ]
  node [
    id 41
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 42
    label "ekwipunek"
  ]
  node [
    id 43
    label "korona_drogi"
  ]
  node [
    id 44
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 45
    label "pobocze"
  ]
  node [
    id 46
    label "liftback"
  ]
  node [
    id 47
    label "opel"
  ]
  node [
    id 48
    label "samoch&#243;d"
  ]
  node [
    id 49
    label "Vectra"
  ]
  node [
    id 50
    label "prawdziwy"
  ]
  node [
    id 51
    label "continue"
  ]
  node [
    id 52
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 53
    label "consider"
  ]
  node [
    id 54
    label "my&#347;le&#263;"
  ]
  node [
    id 55
    label "pilnowa&#263;"
  ]
  node [
    id 56
    label "robi&#263;"
  ]
  node [
    id 57
    label "uznawa&#263;"
  ]
  node [
    id 58
    label "obserwowa&#263;"
  ]
  node [
    id 59
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 60
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 61
    label "deliver"
  ]
  node [
    id 62
    label "si&#322;a"
  ]
  node [
    id 63
    label "stan"
  ]
  node [
    id 64
    label "lina"
  ]
  node [
    id 65
    label "way"
  ]
  node [
    id 66
    label "cable"
  ]
  node [
    id 67
    label "przebieg"
  ]
  node [
    id 68
    label "zbi&#243;r"
  ]
  node [
    id 69
    label "ch&#243;d"
  ]
  node [
    id 70
    label "rz&#261;d"
  ]
  node [
    id 71
    label "k&#322;us"
  ]
  node [
    id 72
    label "progression"
  ]
  node [
    id 73
    label "current"
  ]
  node [
    id 74
    label "pr&#261;d"
  ]
  node [
    id 75
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 76
    label "wydarzenie"
  ]
  node [
    id 77
    label "lot"
  ]
  node [
    id 78
    label "s&#322;o&#324;ce"
  ]
  node [
    id 79
    label "czynienie_si&#281;"
  ]
  node [
    id 80
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 81
    label "czas"
  ]
  node [
    id 82
    label "long_time"
  ]
  node [
    id 83
    label "przedpo&#322;udnie"
  ]
  node [
    id 84
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 85
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 86
    label "godzina"
  ]
  node [
    id 87
    label "t&#322;usty_czwartek"
  ]
  node [
    id 88
    label "wsta&#263;"
  ]
  node [
    id 89
    label "day"
  ]
  node [
    id 90
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 91
    label "przedwiecz&#243;r"
  ]
  node [
    id 92
    label "Sylwester"
  ]
  node [
    id 93
    label "po&#322;udnie"
  ]
  node [
    id 94
    label "wzej&#347;cie"
  ]
  node [
    id 95
    label "podwiecz&#243;r"
  ]
  node [
    id 96
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 97
    label "rano"
  ]
  node [
    id 98
    label "termin"
  ]
  node [
    id 99
    label "ranek"
  ]
  node [
    id 100
    label "doba"
  ]
  node [
    id 101
    label "wiecz&#243;r"
  ]
  node [
    id 102
    label "walentynki"
  ]
  node [
    id 103
    label "popo&#322;udnie"
  ]
  node [
    id 104
    label "noc"
  ]
  node [
    id 105
    label "wstanie"
  ]
  node [
    id 106
    label "booklet"
  ]
  node [
    id 107
    label "druk_ulotny"
  ]
  node [
    id 108
    label "reklama"
  ]
  node [
    id 109
    label "poziom"
  ]
  node [
    id 110
    label "faza"
  ]
  node [
    id 111
    label "depression"
  ]
  node [
    id 112
    label "zjawisko"
  ]
  node [
    id 113
    label "nizina"
  ]
  node [
    id 114
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 115
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 116
    label "weekend"
  ]
  node [
    id 117
    label "miesi&#261;c"
  ]
  node [
    id 118
    label "si&#281;ga&#263;"
  ]
  node [
    id 119
    label "trwa&#263;"
  ]
  node [
    id 120
    label "obecno&#347;&#263;"
  ]
  node [
    id 121
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 122
    label "stand"
  ]
  node [
    id 123
    label "mie&#263;_miejsce"
  ]
  node [
    id 124
    label "uczestniczy&#263;"
  ]
  node [
    id 125
    label "chodzi&#263;"
  ]
  node [
    id 126
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 127
    label "equal"
  ]
  node [
    id 128
    label "du&#380;y"
  ]
  node [
    id 129
    label "cz&#281;sto"
  ]
  node [
    id 130
    label "bardzo"
  ]
  node [
    id 131
    label "mocno"
  ]
  node [
    id 132
    label "wiela"
  ]
  node [
    id 133
    label "oskoma"
  ]
  node [
    id 134
    label "wytw&#243;r"
  ]
  node [
    id 135
    label "thinking"
  ]
  node [
    id 136
    label "emocja"
  ]
  node [
    id 137
    label "inclination"
  ]
  node [
    id 138
    label "zajawka"
  ]
  node [
    id 139
    label "zrobi&#263;"
  ]
  node [
    id 140
    label "u&#380;y&#263;"
  ]
  node [
    id 141
    label "uzyska&#263;"
  ]
  node [
    id 142
    label "utilize"
  ]
  node [
    id 143
    label "czyj&#347;"
  ]
  node [
    id 144
    label "propozycja"
  ]
  node [
    id 145
    label "offer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 145
  ]
]
