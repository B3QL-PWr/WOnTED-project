graph [
  maxDegree 31
  minDegree 1
  meanDegree 1.9615384615384615
  density 0.038461538461538464
  graphCliqueNumber 2
  node [
    id 0
    label "dominik"
    origin "text"
  ]
  node [
    id 1
    label "batorski"
    origin "text"
  ]
  node [
    id 2
    label "model"
    origin "text"
  ]
  node [
    id 3
    label "uk&#322;ad"
    origin "text"
  ]
  node [
    id 4
    label "typ"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "pozowa&#263;"
  ]
  node [
    id 7
    label "ideal"
  ]
  node [
    id 8
    label "matryca"
  ]
  node [
    id 9
    label "imitacja"
  ]
  node [
    id 10
    label "ruch"
  ]
  node [
    id 11
    label "motif"
  ]
  node [
    id 12
    label "pozowanie"
  ]
  node [
    id 13
    label "wz&#243;r"
  ]
  node [
    id 14
    label "miniatura"
  ]
  node [
    id 15
    label "prezenter"
  ]
  node [
    id 16
    label "facet"
  ]
  node [
    id 17
    label "orygina&#322;"
  ]
  node [
    id 18
    label "mildew"
  ]
  node [
    id 19
    label "spos&#243;b"
  ]
  node [
    id 20
    label "zi&#243;&#322;ko"
  ]
  node [
    id 21
    label "adaptation"
  ]
  node [
    id 22
    label "sk&#322;ad"
  ]
  node [
    id 23
    label "zachowanie"
  ]
  node [
    id 24
    label "umowa"
  ]
  node [
    id 25
    label "podsystem"
  ]
  node [
    id 26
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 27
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 28
    label "system"
  ]
  node [
    id 29
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 30
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 31
    label "struktura"
  ]
  node [
    id 32
    label "wi&#281;&#378;"
  ]
  node [
    id 33
    label "zawarcie"
  ]
  node [
    id 34
    label "systemat"
  ]
  node [
    id 35
    label "usenet"
  ]
  node [
    id 36
    label "ONZ"
  ]
  node [
    id 37
    label "o&#347;"
  ]
  node [
    id 38
    label "organ"
  ]
  node [
    id 39
    label "przestawi&#263;"
  ]
  node [
    id 40
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 41
    label "traktat_wersalski"
  ]
  node [
    id 42
    label "rozprz&#261;c"
  ]
  node [
    id 43
    label "cybernetyk"
  ]
  node [
    id 44
    label "cia&#322;o"
  ]
  node [
    id 45
    label "zawrze&#263;"
  ]
  node [
    id 46
    label "konstelacja"
  ]
  node [
    id 47
    label "alliance"
  ]
  node [
    id 48
    label "zbi&#243;r"
  ]
  node [
    id 49
    label "NATO"
  ]
  node [
    id 50
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 51
    label "treaty"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
]
