graph [
  maxDegree 20
  minDegree 1
  meanDegree 2
  density 0.023809523809523808
  graphCliqueNumber 2
  node [
    id 0
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 1
    label "beke"
    origin "text"
  ]
  node [
    id 2
    label "si&#322;ka"
    origin "text"
  ]
  node [
    id 3
    label "kumpel"
    origin "text"
  ]
  node [
    id 4
    label "poprosi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "&#380;ebyby&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 7
    label "jak"
    origin "text"
  ]
  node [
    id 8
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "ten"
    origin "text"
  ]
  node [
    id 10
    label "caly"
    origin "text"
  ]
  node [
    id 11
    label "mostek"
    origin "text"
  ]
  node [
    id 12
    label "drive"
    origin "text"
  ]
  node [
    id 13
    label "nim"
    origin "text"
  ]
  node [
    id 14
    label "wyciska&#263;"
    origin "text"
  ]
  node [
    id 15
    label "proszek"
  ]
  node [
    id 16
    label "pakowa&#263;"
  ]
  node [
    id 17
    label "kulturysta"
  ]
  node [
    id 18
    label "obiekt"
  ]
  node [
    id 19
    label "budowla"
  ]
  node [
    id 20
    label "pakernia"
  ]
  node [
    id 21
    label "kumplowanie_si&#281;"
  ]
  node [
    id 22
    label "znajomy"
  ]
  node [
    id 23
    label "konfrater"
  ]
  node [
    id 24
    label "towarzysz"
  ]
  node [
    id 25
    label "zakolegowanie_si&#281;"
  ]
  node [
    id 26
    label "partner"
  ]
  node [
    id 27
    label "ziom"
  ]
  node [
    id 28
    label "ask"
  ]
  node [
    id 29
    label "invite"
  ]
  node [
    id 30
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 31
    label "zaproponowa&#263;"
  ]
  node [
    id 32
    label "spowodowa&#263;"
  ]
  node [
    id 33
    label "wyrazi&#263;"
  ]
  node [
    id 34
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 35
    label "przedstawi&#263;"
  ]
  node [
    id 36
    label "testify"
  ]
  node [
    id 37
    label "indicate"
  ]
  node [
    id 38
    label "przeszkoli&#263;"
  ]
  node [
    id 39
    label "udowodni&#263;"
  ]
  node [
    id 40
    label "poinformowa&#263;"
  ]
  node [
    id 41
    label "poda&#263;"
  ]
  node [
    id 42
    label "point"
  ]
  node [
    id 43
    label "byd&#322;o"
  ]
  node [
    id 44
    label "zobo"
  ]
  node [
    id 45
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 46
    label "yakalo"
  ]
  node [
    id 47
    label "dzo"
  ]
  node [
    id 48
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 49
    label "work"
  ]
  node [
    id 50
    label "robi&#263;"
  ]
  node [
    id 51
    label "muzyka"
  ]
  node [
    id 52
    label "rola"
  ]
  node [
    id 53
    label "create"
  ]
  node [
    id 54
    label "wytwarza&#263;"
  ]
  node [
    id 55
    label "praca"
  ]
  node [
    id 56
    label "okre&#347;lony"
  ]
  node [
    id 57
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 58
    label "r&#281;koje&#347;&#263;_mostka"
  ]
  node [
    id 59
    label "trzon_mostka"
  ]
  node [
    id 60
    label "ko&#347;&#263;"
  ]
  node [
    id 61
    label "instrument_strunowy"
  ]
  node [
    id 62
    label "wyrostek_mieczykowaty"
  ]
  node [
    id 63
    label "sternum"
  ]
  node [
    id 64
    label "okulary"
  ]
  node [
    id 65
    label "sieciowanie"
  ]
  node [
    id 66
    label "proteza_dentystyczna"
  ]
  node [
    id 67
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 68
    label "rower"
  ]
  node [
    id 69
    label "z&#261;b"
  ]
  node [
    id 70
    label "element"
  ]
  node [
    id 71
    label "&#263;wiczenie"
  ]
  node [
    id 72
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 73
    label "bridge"
  ]
  node [
    id 74
    label "klatka_piersiowa"
  ]
  node [
    id 75
    label "obw&#243;d_elektroniczny"
  ]
  node [
    id 76
    label "gra_planszowa"
  ]
  node [
    id 77
    label "zmusza&#263;"
  ]
  node [
    id 78
    label "uzyskiwa&#263;"
  ]
  node [
    id 79
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 80
    label "wydostawa&#263;"
  ]
  node [
    id 81
    label "wyzyskiwa&#263;"
  ]
  node [
    id 82
    label "extort"
  ]
  node [
    id 83
    label "odciska&#263;"
  ]
  node [
    id 84
    label "carry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 14
    target 78
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 84
  ]
]
