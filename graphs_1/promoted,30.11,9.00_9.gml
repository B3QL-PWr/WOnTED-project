graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.0570409982174688
  graphCliqueNumber 3
  node [
    id 0
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 1
    label "luby"
    origin "text"
  ]
  node [
    id 2
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "sta&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "klient"
    origin "text"
  ]
  node [
    id 5
    label "kochanek"
  ]
  node [
    id 6
    label "wybranek"
  ]
  node [
    id 7
    label "kochanie"
  ]
  node [
    id 8
    label "umi&#322;owany"
  ]
  node [
    id 9
    label "drogi"
  ]
  node [
    id 10
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 11
    label "czu&#263;"
  ]
  node [
    id 12
    label "need"
  ]
  node [
    id 13
    label "hide"
  ]
  node [
    id 14
    label "support"
  ]
  node [
    id 15
    label "wielko&#347;&#263;"
  ]
  node [
    id 16
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 17
    label "element"
  ]
  node [
    id 18
    label "constant"
  ]
  node [
    id 19
    label "cz&#322;owiek"
  ]
  node [
    id 20
    label "bratek"
  ]
  node [
    id 21
    label "klientela"
  ]
  node [
    id 22
    label "szlachcic"
  ]
  node [
    id 23
    label "agent_rozliczeniowy"
  ]
  node [
    id 24
    label "komputer_cyfrowy"
  ]
  node [
    id 25
    label "program"
  ]
  node [
    id 26
    label "us&#322;ugobiorca"
  ]
  node [
    id 27
    label "Rzymianin"
  ]
  node [
    id 28
    label "obywatel"
  ]
  node [
    id 29
    label "Samsungu"
  ]
  node [
    id 30
    label "Galaxy"
  ]
  node [
    id 31
    label "S8"
  ]
  node [
    id 32
    label "medium"
  ]
  node [
    id 33
    label "Markt"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
]
