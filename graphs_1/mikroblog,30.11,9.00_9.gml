graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "mame"
    origin "text"
  ]
  node [
    id 1
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 2
    label "obiad"
    origin "text"
  ]
  node [
    id 3
    label "doba"
  ]
  node [
    id 4
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 5
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 6
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 7
    label "posi&#322;ek"
  ]
  node [
    id 8
    label "meal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
]
