graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "ousmane"
    origin "text"
  ]
  node [
    id 1
    label "dembele"
    origin "text"
  ]
  node [
    id 2
    label "espanyol"
    origin "text"
  ]
  node [
    id 3
    label "barcelona"
    origin "text"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
