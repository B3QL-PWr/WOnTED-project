graph [
  maxDegree 24
  minDegree 1
  meanDegree 2.0444444444444443
  density 0.046464646464646465
  graphCliqueNumber 3
  node [
    id 0
    label "gwiazda"
    origin "text"
  ]
  node [
    id 1
    label "pozosta&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 3
    label "inny"
    origin "text"
  ]
  node [
    id 4
    label "S&#322;o&#324;ce"
  ]
  node [
    id 5
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 6
    label "Arktur"
  ]
  node [
    id 7
    label "star"
  ]
  node [
    id 8
    label "delta_Scuti"
  ]
  node [
    id 9
    label "agregatka"
  ]
  node [
    id 10
    label "s&#322;awa"
  ]
  node [
    id 11
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 12
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 13
    label "gwiazdosz"
  ]
  node [
    id 14
    label "&#347;wiat&#322;o"
  ]
  node [
    id 15
    label "Nibiru"
  ]
  node [
    id 16
    label "ornament"
  ]
  node [
    id 17
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 18
    label "Gwiazda_Polarna"
  ]
  node [
    id 19
    label "kszta&#322;t"
  ]
  node [
    id 20
    label "supergrupa"
  ]
  node [
    id 21
    label "gromada"
  ]
  node [
    id 22
    label "obiekt"
  ]
  node [
    id 23
    label "promie&#324;"
  ]
  node [
    id 24
    label "konstelacja"
  ]
  node [
    id 25
    label "asocjacja_gwiazd"
  ]
  node [
    id 26
    label "&#380;ywy"
  ]
  node [
    id 27
    label "defenestracja"
  ]
  node [
    id 28
    label "kres"
  ]
  node [
    id 29
    label "agonia"
  ]
  node [
    id 30
    label "&#380;ycie"
  ]
  node [
    id 31
    label "szeol"
  ]
  node [
    id 32
    label "mogi&#322;a"
  ]
  node [
    id 33
    label "pogrzeb"
  ]
  node [
    id 34
    label "istota_nadprzyrodzona"
  ]
  node [
    id 35
    label "&#380;a&#322;oba"
  ]
  node [
    id 36
    label "pogrzebanie"
  ]
  node [
    id 37
    label "upadek"
  ]
  node [
    id 38
    label "zabicie"
  ]
  node [
    id 39
    label "kres_&#380;ycia"
  ]
  node [
    id 40
    label "kolejny"
  ]
  node [
    id 41
    label "inaczej"
  ]
  node [
    id 42
    label "r&#243;&#380;ny"
  ]
  node [
    id 43
    label "inszy"
  ]
  node [
    id 44
    label "osobno"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
]
