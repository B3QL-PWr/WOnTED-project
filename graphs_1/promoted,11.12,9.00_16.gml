graph [
  maxDegree 32
  minDegree 1
  meanDegree 2
  density 0.02247191011235955
  graphCliqueNumber 2
  node [
    id 0
    label "zdarzenie"
    origin "text"
  ]
  node [
    id 1
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "sobota"
    origin "text"
  ]
  node [
    id 3
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 4
    label "godz"
    origin "text"
  ]
  node [
    id 5
    label "teren"
    origin "text"
  ]
  node [
    id 6
    label "stacja"
    origin "text"
  ]
  node [
    id 7
    label "paliwo"
    origin "text"
  ]
  node [
    id 8
    label "miejscowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "zboiska"
    origin "text"
  ]
  node [
    id 10
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 11
    label "czynno&#347;&#263;"
  ]
  node [
    id 12
    label "motyw"
  ]
  node [
    id 13
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 14
    label "fabu&#322;a"
  ]
  node [
    id 15
    label "przebiec"
  ]
  node [
    id 16
    label "przebiegni&#281;cie"
  ]
  node [
    id 17
    label "charakter"
  ]
  node [
    id 18
    label "get"
  ]
  node [
    id 19
    label "zaj&#347;&#263;"
  ]
  node [
    id 20
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 21
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 22
    label "dop&#322;ata"
  ]
  node [
    id 23
    label "supervene"
  ]
  node [
    id 24
    label "heed"
  ]
  node [
    id 25
    label "dodatek"
  ]
  node [
    id 26
    label "catch"
  ]
  node [
    id 27
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 28
    label "uzyska&#263;"
  ]
  node [
    id 29
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 30
    label "orgazm"
  ]
  node [
    id 31
    label "dozna&#263;"
  ]
  node [
    id 32
    label "sta&#263;_si&#281;"
  ]
  node [
    id 33
    label "bodziec"
  ]
  node [
    id 34
    label "drive"
  ]
  node [
    id 35
    label "informacja"
  ]
  node [
    id 36
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 37
    label "spowodowa&#263;"
  ]
  node [
    id 38
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 39
    label "dotrze&#263;"
  ]
  node [
    id 40
    label "postrzega&#263;"
  ]
  node [
    id 41
    label "become"
  ]
  node [
    id 42
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 43
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 44
    label "przesy&#322;ka"
  ]
  node [
    id 45
    label "dolecie&#263;"
  ]
  node [
    id 46
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 47
    label "dokoptowa&#263;"
  ]
  node [
    id 48
    label "weekend"
  ]
  node [
    id 49
    label "dzie&#324;_powszedni"
  ]
  node [
    id 50
    label "Wielka_Sobota"
  ]
  node [
    id 51
    label "zakres"
  ]
  node [
    id 52
    label "kontekst"
  ]
  node [
    id 53
    label "wymiar"
  ]
  node [
    id 54
    label "obszar"
  ]
  node [
    id 55
    label "krajobraz"
  ]
  node [
    id 56
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 57
    label "w&#322;adza"
  ]
  node [
    id 58
    label "nation"
  ]
  node [
    id 59
    label "przyroda"
  ]
  node [
    id 60
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 61
    label "miejsce_pracy"
  ]
  node [
    id 62
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 63
    label "miejsce"
  ]
  node [
    id 64
    label "instytucja"
  ]
  node [
    id 65
    label "droga_krzy&#380;owa"
  ]
  node [
    id 66
    label "punkt"
  ]
  node [
    id 67
    label "urz&#261;dzenie"
  ]
  node [
    id 68
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 69
    label "siedziba"
  ]
  node [
    id 70
    label "spalenie"
  ]
  node [
    id 71
    label "spali&#263;"
  ]
  node [
    id 72
    label "fuel"
  ]
  node [
    id 73
    label "tankowanie"
  ]
  node [
    id 74
    label "zgazowa&#263;"
  ]
  node [
    id 75
    label "pompa_wtryskowa"
  ]
  node [
    id 76
    label "tankowa&#263;"
  ]
  node [
    id 77
    label "antydetonator"
  ]
  node [
    id 78
    label "Orlen"
  ]
  node [
    id 79
    label "spalanie"
  ]
  node [
    id 80
    label "spala&#263;"
  ]
  node [
    id 81
    label "substancja"
  ]
  node [
    id 82
    label "Aurignac"
  ]
  node [
    id 83
    label "osiedle"
  ]
  node [
    id 84
    label "Levallois-Perret"
  ]
  node [
    id 85
    label "Sabaudia"
  ]
  node [
    id 86
    label "Saint-Acheul"
  ]
  node [
    id 87
    label "Opat&#243;wek"
  ]
  node [
    id 88
    label "Boulogne"
  ]
  node [
    id 89
    label "Cecora"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
]
