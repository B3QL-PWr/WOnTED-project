graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.566585956416465
  density 0.006229577564117633
  graphCliqueNumber 12
  node [
    id 0
    label "tym"
    origin "text"
  ]
  node [
    id 1
    label "sam"
    origin "text"
  ]
  node [
    id 2
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "jeszcze"
    origin "text"
  ]
  node [
    id 4
    label "przed"
    origin "text"
  ]
  node [
    id 5
    label "przem&#243;wienie"
    origin "text"
  ]
  node [
    id 6
    label "larry"
    origin "text"
  ]
  node [
    id 7
    label "lessig"
    origin "text"
  ]
  node [
    id 8
    label "benjamin"
    origin "text"
  ]
  node [
    id 9
    label "mako"
    origin "text"
  ]
  node [
    id 10
    label "hill"
    origin "text"
  ]
  node [
    id 11
    label "wzi&#261;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 13
    label "warsztatowy"
    origin "text"
  ]
  node [
    id 14
    label "dyskusja"
    origin "text"
  ]
  node [
    id 15
    label "problem"
    origin "text"
  ]
  node [
    id 16
    label "licencjonowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "praca"
    origin "text"
  ]
  node [
    id 18
    label "by&#263;"
    origin "text"
  ]
  node [
    id 19
    label "autor"
    origin "text"
  ]
  node [
    id 20
    label "tekst"
    origin "text"
  ]
  node [
    id 21
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 22
    label "freedom"
    origin "text"
  ]
  node [
    id 23
    label "commons"
    origin "text"
  ]
  node [
    id 24
    label "anda"
    origin "text"
  ]
  node [
    id 25
    label "the"
    origin "text"
  ]
  node [
    id 26
    label "free"
    origin "text"
  ]
  node [
    id 27
    label "software"
    origin "text"
  ]
  node [
    id 28
    label "movement"
    origin "text"
  ]
  node [
    id 29
    label "standard"
    origin "text"
  ]
  node [
    id 30
    label "wolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 31
    label "creative"
    origin "text"
  ]
  node [
    id 32
    label "ruch"
    origin "text"
  ]
  node [
    id 33
    label "oprogramowanie"
    origin "text"
  ]
  node [
    id 34
    label "oskar&#380;y&#263;"
    origin "text"
  ]
  node [
    id 35
    label "brak"
    origin "text"
  ]
  node [
    id 36
    label "definicja"
    origin "text"
  ]
  node [
    id 37
    label "propagowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 39
    label "jasno"
    origin "text"
  ]
  node [
    id 40
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 41
    label "czy"
    origin "text"
  ]
  node [
    id 42
    label "dan"
    origin "text"
  ]
  node [
    id 43
    label "dla"
    origin "text"
  ]
  node [
    id 44
    label "wystarczaj&#261;cy"
    origin "text"
  ]
  node [
    id 45
    label "otwarty"
    origin "text"
  ]
  node [
    id 46
    label "nie"
    origin "text"
  ]
  node [
    id 47
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 48
    label "wyra&#378;ny"
    origin "text"
  ]
  node [
    id 49
    label "granica"
    origin "text"
  ]
  node [
    id 50
    label "etyczny"
    origin "text"
  ]
  node [
    id 51
    label "stoa"
    origin "text"
  ]
  node [
    id 52
    label "przegrana"
    origin "text"
  ]
  node [
    id 53
    label "pozycja"
    origin "text"
  ]
  node [
    id 54
    label "pisz"
    origin "text"
  ]
  node [
    id 55
    label "sklep"
  ]
  node [
    id 56
    label "s&#322;o&#324;ce"
  ]
  node [
    id 57
    label "czynienie_si&#281;"
  ]
  node [
    id 58
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 59
    label "czas"
  ]
  node [
    id 60
    label "long_time"
  ]
  node [
    id 61
    label "przedpo&#322;udnie"
  ]
  node [
    id 62
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 63
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 64
    label "tydzie&#324;"
  ]
  node [
    id 65
    label "godzina"
  ]
  node [
    id 66
    label "t&#322;usty_czwartek"
  ]
  node [
    id 67
    label "wsta&#263;"
  ]
  node [
    id 68
    label "day"
  ]
  node [
    id 69
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 70
    label "przedwiecz&#243;r"
  ]
  node [
    id 71
    label "Sylwester"
  ]
  node [
    id 72
    label "po&#322;udnie"
  ]
  node [
    id 73
    label "wzej&#347;cie"
  ]
  node [
    id 74
    label "podwiecz&#243;r"
  ]
  node [
    id 75
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 76
    label "rano"
  ]
  node [
    id 77
    label "termin"
  ]
  node [
    id 78
    label "ranek"
  ]
  node [
    id 79
    label "doba"
  ]
  node [
    id 80
    label "wiecz&#243;r"
  ]
  node [
    id 81
    label "walentynki"
  ]
  node [
    id 82
    label "popo&#322;udnie"
  ]
  node [
    id 83
    label "noc"
  ]
  node [
    id 84
    label "wstanie"
  ]
  node [
    id 85
    label "ci&#261;gle"
  ]
  node [
    id 86
    label "sermon"
  ]
  node [
    id 87
    label "wypowied&#378;"
  ]
  node [
    id 88
    label "obronienie"
  ]
  node [
    id 89
    label "wydanie"
  ]
  node [
    id 90
    label "oddzia&#322;anie"
  ]
  node [
    id 91
    label "odzyskanie"
  ]
  node [
    id 92
    label "wyst&#261;pienie"
  ]
  node [
    id 93
    label "wyg&#322;oszenie"
  ]
  node [
    id 94
    label "zrozumienie"
  ]
  node [
    id 95
    label "talk"
  ]
  node [
    id 96
    label "wydobycie"
  ]
  node [
    id 97
    label "address"
  ]
  node [
    id 98
    label "wej&#347;&#263;"
  ]
  node [
    id 99
    label "get"
  ]
  node [
    id 100
    label "wzi&#281;cie"
  ]
  node [
    id 101
    label "wyrucha&#263;"
  ]
  node [
    id 102
    label "uciec"
  ]
  node [
    id 103
    label "ruszy&#263;"
  ]
  node [
    id 104
    label "wygra&#263;"
  ]
  node [
    id 105
    label "obj&#261;&#263;"
  ]
  node [
    id 106
    label "zacz&#261;&#263;"
  ]
  node [
    id 107
    label "wyciupcia&#263;"
  ]
  node [
    id 108
    label "World_Health_Organization"
  ]
  node [
    id 109
    label "skorzysta&#263;"
  ]
  node [
    id 110
    label "pokona&#263;"
  ]
  node [
    id 111
    label "poczyta&#263;"
  ]
  node [
    id 112
    label "poruszy&#263;"
  ]
  node [
    id 113
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 114
    label "take"
  ]
  node [
    id 115
    label "aim"
  ]
  node [
    id 116
    label "arise"
  ]
  node [
    id 117
    label "u&#380;y&#263;"
  ]
  node [
    id 118
    label "zaatakowa&#263;"
  ]
  node [
    id 119
    label "receive"
  ]
  node [
    id 120
    label "uda&#263;_si&#281;"
  ]
  node [
    id 121
    label "dosta&#263;"
  ]
  node [
    id 122
    label "otrzyma&#263;"
  ]
  node [
    id 123
    label "obskoczy&#263;"
  ]
  node [
    id 124
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 125
    label "zrobi&#263;"
  ]
  node [
    id 126
    label "bra&#263;"
  ]
  node [
    id 127
    label "nakaza&#263;"
  ]
  node [
    id 128
    label "chwyci&#263;"
  ]
  node [
    id 129
    label "przyj&#261;&#263;"
  ]
  node [
    id 130
    label "seize"
  ]
  node [
    id 131
    label "odziedziczy&#263;"
  ]
  node [
    id 132
    label "withdraw"
  ]
  node [
    id 133
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 134
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 135
    label "obecno&#347;&#263;"
  ]
  node [
    id 136
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 137
    label "kwota"
  ]
  node [
    id 138
    label "ilo&#347;&#263;"
  ]
  node [
    id 139
    label "technicznie"
  ]
  node [
    id 140
    label "rozmowa"
  ]
  node [
    id 141
    label "sympozjon"
  ]
  node [
    id 142
    label "conference"
  ]
  node [
    id 143
    label "trudno&#347;&#263;"
  ]
  node [
    id 144
    label "sprawa"
  ]
  node [
    id 145
    label "ambaras"
  ]
  node [
    id 146
    label "problemat"
  ]
  node [
    id 147
    label "pierepa&#322;ka"
  ]
  node [
    id 148
    label "obstruction"
  ]
  node [
    id 149
    label "problematyka"
  ]
  node [
    id 150
    label "jajko_Kolumba"
  ]
  node [
    id 151
    label "subiekcja"
  ]
  node [
    id 152
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 153
    label "akceptowa&#263;"
  ]
  node [
    id 154
    label "udzieli&#263;"
  ]
  node [
    id 155
    label "udziela&#263;"
  ]
  node [
    id 156
    label "hodowla"
  ]
  node [
    id 157
    label "rasowy"
  ]
  node [
    id 158
    label "license"
  ]
  node [
    id 159
    label "licencja"
  ]
  node [
    id 160
    label "stosunek_pracy"
  ]
  node [
    id 161
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 162
    label "benedykty&#324;ski"
  ]
  node [
    id 163
    label "pracowanie"
  ]
  node [
    id 164
    label "zaw&#243;d"
  ]
  node [
    id 165
    label "kierownictwo"
  ]
  node [
    id 166
    label "zmiana"
  ]
  node [
    id 167
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 168
    label "wytw&#243;r"
  ]
  node [
    id 169
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 170
    label "tynkarski"
  ]
  node [
    id 171
    label "czynnik_produkcji"
  ]
  node [
    id 172
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 173
    label "zobowi&#261;zanie"
  ]
  node [
    id 174
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 175
    label "czynno&#347;&#263;"
  ]
  node [
    id 176
    label "tyrka"
  ]
  node [
    id 177
    label "pracowa&#263;"
  ]
  node [
    id 178
    label "siedziba"
  ]
  node [
    id 179
    label "poda&#380;_pracy"
  ]
  node [
    id 180
    label "miejsce"
  ]
  node [
    id 181
    label "zak&#322;ad"
  ]
  node [
    id 182
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 183
    label "najem"
  ]
  node [
    id 184
    label "si&#281;ga&#263;"
  ]
  node [
    id 185
    label "trwa&#263;"
  ]
  node [
    id 186
    label "stan"
  ]
  node [
    id 187
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 188
    label "stand"
  ]
  node [
    id 189
    label "mie&#263;_miejsce"
  ]
  node [
    id 190
    label "uczestniczy&#263;"
  ]
  node [
    id 191
    label "chodzi&#263;"
  ]
  node [
    id 192
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 193
    label "equal"
  ]
  node [
    id 194
    label "pomys&#322;odawca"
  ]
  node [
    id 195
    label "kszta&#322;ciciel"
  ]
  node [
    id 196
    label "tworzyciel"
  ]
  node [
    id 197
    label "&#347;w"
  ]
  node [
    id 198
    label "wykonawca"
  ]
  node [
    id 199
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 200
    label "pisa&#263;"
  ]
  node [
    id 201
    label "odmianka"
  ]
  node [
    id 202
    label "opu&#347;ci&#263;"
  ]
  node [
    id 203
    label "koniektura"
  ]
  node [
    id 204
    label "preparacja"
  ]
  node [
    id 205
    label "ekscerpcja"
  ]
  node [
    id 206
    label "j&#281;zykowo"
  ]
  node [
    id 207
    label "obelga"
  ]
  node [
    id 208
    label "dzie&#322;o"
  ]
  node [
    id 209
    label "redakcja"
  ]
  node [
    id 210
    label "pomini&#281;cie"
  ]
  node [
    id 211
    label "dzie&#324;_powszedni"
  ]
  node [
    id 212
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 213
    label "program"
  ]
  node [
    id 214
    label "zbi&#243;r"
  ]
  node [
    id 215
    label "reengineering"
  ]
  node [
    id 216
    label "zorganizowa&#263;"
  ]
  node [
    id 217
    label "model"
  ]
  node [
    id 218
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 219
    label "taniec_towarzyski"
  ]
  node [
    id 220
    label "ordinariness"
  ]
  node [
    id 221
    label "organizowanie"
  ]
  node [
    id 222
    label "criterion"
  ]
  node [
    id 223
    label "zorganizowanie"
  ]
  node [
    id 224
    label "instytucja"
  ]
  node [
    id 225
    label "organizowa&#263;"
  ]
  node [
    id 226
    label "absolutno&#347;&#263;"
  ]
  node [
    id 227
    label "uwi&#281;zi&#263;"
  ]
  node [
    id 228
    label "cecha"
  ]
  node [
    id 229
    label "niezale&#380;no&#347;&#263;"
  ]
  node [
    id 230
    label "uwi&#281;zienie"
  ]
  node [
    id 231
    label "nieokre&#347;lono&#347;&#263;"
  ]
  node [
    id 232
    label "manewr"
  ]
  node [
    id 233
    label "apraksja"
  ]
  node [
    id 234
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 235
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 236
    label "poruszenie"
  ]
  node [
    id 237
    label "commercial_enterprise"
  ]
  node [
    id 238
    label "dyssypacja_energii"
  ]
  node [
    id 239
    label "utrzymanie"
  ]
  node [
    id 240
    label "utrzyma&#263;"
  ]
  node [
    id 241
    label "komunikacja"
  ]
  node [
    id 242
    label "tumult"
  ]
  node [
    id 243
    label "kr&#243;tki"
  ]
  node [
    id 244
    label "drift"
  ]
  node [
    id 245
    label "utrzymywa&#263;"
  ]
  node [
    id 246
    label "stopek"
  ]
  node [
    id 247
    label "kanciasty"
  ]
  node [
    id 248
    label "d&#322;ugi"
  ]
  node [
    id 249
    label "zjawisko"
  ]
  node [
    id 250
    label "utrzymywanie"
  ]
  node [
    id 251
    label "myk"
  ]
  node [
    id 252
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 253
    label "wydarzenie"
  ]
  node [
    id 254
    label "taktyka"
  ]
  node [
    id 255
    label "move"
  ]
  node [
    id 256
    label "natural_process"
  ]
  node [
    id 257
    label "lokomocja"
  ]
  node [
    id 258
    label "mechanika"
  ]
  node [
    id 259
    label "proces"
  ]
  node [
    id 260
    label "strumie&#324;"
  ]
  node [
    id 261
    label "aktywno&#347;&#263;"
  ]
  node [
    id 262
    label "travel"
  ]
  node [
    id 263
    label "disapprove"
  ]
  node [
    id 264
    label "zakomunikowa&#263;"
  ]
  node [
    id 265
    label "prywatywny"
  ]
  node [
    id 266
    label "defect"
  ]
  node [
    id 267
    label "odej&#347;cie"
  ]
  node [
    id 268
    label "gap"
  ]
  node [
    id 269
    label "wyr&#243;b"
  ]
  node [
    id 270
    label "nieistnienie"
  ]
  node [
    id 271
    label "wada"
  ]
  node [
    id 272
    label "odej&#347;&#263;"
  ]
  node [
    id 273
    label "odchodzenie"
  ]
  node [
    id 274
    label "odchodzi&#263;"
  ]
  node [
    id 275
    label "definiens"
  ]
  node [
    id 276
    label "definiendum"
  ]
  node [
    id 277
    label "obja&#347;nienie"
  ]
  node [
    id 278
    label "definition"
  ]
  node [
    id 279
    label "rozpowszechnia&#263;"
  ]
  node [
    id 280
    label "rumor"
  ]
  node [
    id 281
    label "jednoznacznie"
  ]
  node [
    id 282
    label "ja&#347;nie"
  ]
  node [
    id 283
    label "sprawnie"
  ]
  node [
    id 284
    label "zrozumiale"
  ]
  node [
    id 285
    label "pogodnie"
  ]
  node [
    id 286
    label "dobrze"
  ]
  node [
    id 287
    label "jasny"
  ]
  node [
    id 288
    label "ja&#347;niej"
  ]
  node [
    id 289
    label "klarownie"
  ]
  node [
    id 290
    label "szczerze"
  ]
  node [
    id 291
    label "skutecznie"
  ]
  node [
    id 292
    label "powiedzie&#263;"
  ]
  node [
    id 293
    label "testify"
  ]
  node [
    id 294
    label "uzna&#263;"
  ]
  node [
    id 295
    label "oznajmi&#263;"
  ]
  node [
    id 296
    label "declare"
  ]
  node [
    id 297
    label "wiek"
  ]
  node [
    id 298
    label "paleocen"
  ]
  node [
    id 299
    label "odpowiedni"
  ]
  node [
    id 300
    label "niez&#322;y"
  ]
  node [
    id 301
    label "wystarczaj&#261;co"
  ]
  node [
    id 302
    label "ewidentny"
  ]
  node [
    id 303
    label "bezpo&#347;redni"
  ]
  node [
    id 304
    label "otwarcie"
  ]
  node [
    id 305
    label "nieograniczony"
  ]
  node [
    id 306
    label "zdecydowany"
  ]
  node [
    id 307
    label "gotowy"
  ]
  node [
    id 308
    label "aktualny"
  ]
  node [
    id 309
    label "prostoduszny"
  ]
  node [
    id 310
    label "jawnie"
  ]
  node [
    id 311
    label "otworzysty"
  ]
  node [
    id 312
    label "dost&#281;pny"
  ]
  node [
    id 313
    label "publiczny"
  ]
  node [
    id 314
    label "aktywny"
  ]
  node [
    id 315
    label "kontaktowy"
  ]
  node [
    id 316
    label "sprzeciw"
  ]
  node [
    id 317
    label "strona"
  ]
  node [
    id 318
    label "przyczyna"
  ]
  node [
    id 319
    label "matuszka"
  ]
  node [
    id 320
    label "geneza"
  ]
  node [
    id 321
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 322
    label "czynnik"
  ]
  node [
    id 323
    label "poci&#261;ganie"
  ]
  node [
    id 324
    label "rezultat"
  ]
  node [
    id 325
    label "uprz&#261;&#380;"
  ]
  node [
    id 326
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 327
    label "subject"
  ]
  node [
    id 328
    label "nieoboj&#281;tny"
  ]
  node [
    id 329
    label "zauwa&#380;alny"
  ]
  node [
    id 330
    label "wyra&#378;nie"
  ]
  node [
    id 331
    label "zakres"
  ]
  node [
    id 332
    label "Ural"
  ]
  node [
    id 333
    label "koniec"
  ]
  node [
    id 334
    label "kres"
  ]
  node [
    id 335
    label "granice"
  ]
  node [
    id 336
    label "granica_pa&#324;stwa"
  ]
  node [
    id 337
    label "pu&#322;ap"
  ]
  node [
    id 338
    label "frontier"
  ]
  node [
    id 339
    label "end"
  ]
  node [
    id 340
    label "miara"
  ]
  node [
    id 341
    label "poj&#281;cie"
  ]
  node [
    id 342
    label "przej&#347;cie"
  ]
  node [
    id 343
    label "etycznie"
  ]
  node [
    id 344
    label "dobry"
  ]
  node [
    id 345
    label "przyzwoity"
  ]
  node [
    id 346
    label "moralnie"
  ]
  node [
    id 347
    label "kolumna"
  ]
  node [
    id 348
    label "budowla"
  ]
  node [
    id 349
    label "gorzka_pigu&#322;ka"
  ]
  node [
    id 350
    label "przegra"
  ]
  node [
    id 351
    label "wysiadka"
  ]
  node [
    id 352
    label "passa"
  ]
  node [
    id 353
    label "niefortunno&#347;&#263;"
  ]
  node [
    id 354
    label "po&#322;o&#380;enie"
  ]
  node [
    id 355
    label "niepowodzenie"
  ]
  node [
    id 356
    label "strata"
  ]
  node [
    id 357
    label "reverse"
  ]
  node [
    id 358
    label "k&#322;adzenie"
  ]
  node [
    id 359
    label "spis"
  ]
  node [
    id 360
    label "znaczenie"
  ]
  node [
    id 361
    label "awansowanie"
  ]
  node [
    id 362
    label "rz&#261;d"
  ]
  node [
    id 363
    label "wydawa&#263;"
  ]
  node [
    id 364
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 365
    label "szermierka"
  ]
  node [
    id 366
    label "debit"
  ]
  node [
    id 367
    label "status"
  ]
  node [
    id 368
    label "adres"
  ]
  node [
    id 369
    label "redaktor"
  ]
  node [
    id 370
    label "poster"
  ]
  node [
    id 371
    label "le&#380;e&#263;"
  ]
  node [
    id 372
    label "wyda&#263;"
  ]
  node [
    id 373
    label "bearing"
  ]
  node [
    id 374
    label "wojsko"
  ]
  node [
    id 375
    label "druk"
  ]
  node [
    id 376
    label "awans"
  ]
  node [
    id 377
    label "ustawienie"
  ]
  node [
    id 378
    label "sytuacja"
  ]
  node [
    id 379
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 380
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 381
    label "szata_graficzna"
  ]
  node [
    id 382
    label "awansowa&#263;"
  ]
  node [
    id 383
    label "rozmieszczenie"
  ]
  node [
    id 384
    label "publikacja"
  ]
  node [
    id 385
    label "Larry"
  ]
  node [
    id 386
    label "Lessig"
  ]
  node [
    id 387
    label "Benjamin"
  ]
  node [
    id 388
    label "Mako"
  ]
  node [
    id 389
    label "Hill"
  ]
  node [
    id 390
    label "Towards"
  ]
  node [
    id 391
    label "albo"
  ]
  node [
    id 392
    label "of"
  ]
  node [
    id 393
    label "Freedom"
  ]
  node [
    id 394
    label "Creative"
  ]
  node [
    id 395
    label "Commons"
  ]
  node [
    id 396
    label "Anda"
  ]
  node [
    id 397
    label "Free"
  ]
  node [
    id 398
    label "Movement"
  ]
  node [
    id 399
    label "wyspa"
  ]
  node [
    id 400
    label "kierunek"
  ]
  node [
    id 401
    label "i"
  ]
  node [
    id 402
    label "rucho"
  ]
  node [
    id 403
    label "wolny"
  ]
  node [
    id 404
    label "oprogramowa&#263;"
  ]
  node [
    id 405
    label "WOS"
  ]
  node [
    id 406
    label "4"
  ]
  node [
    id 407
    label "Lessiga"
  ]
  node [
    id 408
    label "Benjamina"
  ]
  node [
    id 409
    label "Richard"
  ]
  node [
    id 410
    label "Stallmanem"
  ]
  node [
    id 411
    label "Jaros&#322;awa"
  ]
  node [
    id 412
    label "Lipszyc"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 43
  ]
  edge [
    source 19
    target 44
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 47
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 87
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 211
  ]
  edge [
    source 21
    target 212
  ]
  edge [
    source 21
    target 64
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 390
  ]
  edge [
    source 25
    target 391
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 392
  ]
  edge [
    source 25
    target 393
  ]
  edge [
    source 25
    target 394
  ]
  edge [
    source 25
    target 395
  ]
  edge [
    source 25
    target 396
  ]
  edge [
    source 25
    target 397
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 398
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 213
  ]
  edge [
    source 27
    target 214
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 390
  ]
  edge [
    source 27
    target 391
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 392
  ]
  edge [
    source 27
    target 393
  ]
  edge [
    source 27
    target 394
  ]
  edge [
    source 27
    target 395
  ]
  edge [
    source 27
    target 396
  ]
  edge [
    source 27
    target 397
  ]
  edge [
    source 27
    target 398
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 216
  ]
  edge [
    source 29
    target 217
  ]
  edge [
    source 29
    target 218
  ]
  edge [
    source 29
    target 219
  ]
  edge [
    source 29
    target 220
  ]
  edge [
    source 29
    target 221
  ]
  edge [
    source 29
    target 222
  ]
  edge [
    source 29
    target 223
  ]
  edge [
    source 29
    target 224
  ]
  edge [
    source 29
    target 225
  ]
  edge [
    source 29
    target 390
  ]
  edge [
    source 29
    target 391
  ]
  edge [
    source 29
    target 392
  ]
  edge [
    source 29
    target 393
  ]
  edge [
    source 29
    target 394
  ]
  edge [
    source 29
    target 395
  ]
  edge [
    source 29
    target 396
  ]
  edge [
    source 29
    target 397
  ]
  edge [
    source 29
    target 398
  ]
  edge [
    source 29
    target 399
  ]
  edge [
    source 29
    target 400
  ]
  edge [
    source 29
    target 401
  ]
  edge [
    source 29
    target 402
  ]
  edge [
    source 29
    target 403
  ]
  edge [
    source 29
    target 404
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 30
    target 135
  ]
  edge [
    source 30
    target 227
  ]
  edge [
    source 30
    target 228
  ]
  edge [
    source 30
    target 229
  ]
  edge [
    source 30
    target 230
  ]
  edge [
    source 30
    target 231
  ]
  edge [
    source 30
    target 399
  ]
  edge [
    source 30
    target 400
  ]
  edge [
    source 30
    target 394
  ]
  edge [
    source 30
    target 395
  ]
  edge [
    source 30
    target 401
  ]
  edge [
    source 30
    target 402
  ]
  edge [
    source 30
    target 403
  ]
  edge [
    source 30
    target 404
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 232
  ]
  edge [
    source 32
    target 217
  ]
  edge [
    source 32
    target 233
  ]
  edge [
    source 32
    target 234
  ]
  edge [
    source 32
    target 235
  ]
  edge [
    source 32
    target 236
  ]
  edge [
    source 32
    target 237
  ]
  edge [
    source 32
    target 238
  ]
  edge [
    source 32
    target 166
  ]
  edge [
    source 32
    target 239
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 32
    target 241
  ]
  edge [
    source 32
    target 242
  ]
  edge [
    source 32
    target 243
  ]
  edge [
    source 32
    target 244
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 246
  ]
  edge [
    source 32
    target 247
  ]
  edge [
    source 32
    target 248
  ]
  edge [
    source 32
    target 249
  ]
  edge [
    source 32
    target 250
  ]
  edge [
    source 32
    target 175
  ]
  edge [
    source 32
    target 251
  ]
  edge [
    source 32
    target 252
  ]
  edge [
    source 32
    target 253
  ]
  edge [
    source 32
    target 254
  ]
  edge [
    source 32
    target 255
  ]
  edge [
    source 32
    target 256
  ]
  edge [
    source 32
    target 257
  ]
  edge [
    source 32
    target 258
  ]
  edge [
    source 32
    target 259
  ]
  edge [
    source 32
    target 260
  ]
  edge [
    source 32
    target 261
  ]
  edge [
    source 32
    target 262
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 213
  ]
  edge [
    source 33
    target 214
  ]
  edge [
    source 33
    target 215
  ]
  edge [
    source 34
    target 263
  ]
  edge [
    source 34
    target 264
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 47
  ]
  edge [
    source 35
    target 48
  ]
  edge [
    source 35
    target 265
  ]
  edge [
    source 35
    target 266
  ]
  edge [
    source 35
    target 267
  ]
  edge [
    source 35
    target 268
  ]
  edge [
    source 35
    target 243
  ]
  edge [
    source 35
    target 269
  ]
  edge [
    source 35
    target 270
  ]
  edge [
    source 35
    target 271
  ]
  edge [
    source 35
    target 272
  ]
  edge [
    source 35
    target 273
  ]
  edge [
    source 35
    target 274
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 275
  ]
  edge [
    source 36
    target 276
  ]
  edge [
    source 36
    target 277
  ]
  edge [
    source 36
    target 278
  ]
  edge [
    source 37
    target 279
  ]
  edge [
    source 37
    target 280
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 281
  ]
  edge [
    source 39
    target 282
  ]
  edge [
    source 39
    target 283
  ]
  edge [
    source 39
    target 284
  ]
  edge [
    source 39
    target 285
  ]
  edge [
    source 39
    target 286
  ]
  edge [
    source 39
    target 287
  ]
  edge [
    source 39
    target 288
  ]
  edge [
    source 39
    target 289
  ]
  edge [
    source 39
    target 290
  ]
  edge [
    source 39
    target 291
  ]
  edge [
    source 39
    target 52
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 292
  ]
  edge [
    source 40
    target 293
  ]
  edge [
    source 40
    target 294
  ]
  edge [
    source 40
    target 295
  ]
  edge [
    source 40
    target 296
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 45
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 297
  ]
  edge [
    source 42
    target 298
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 299
  ]
  edge [
    source 44
    target 300
  ]
  edge [
    source 44
    target 301
  ]
  edge [
    source 45
    target 302
  ]
  edge [
    source 45
    target 303
  ]
  edge [
    source 45
    target 304
  ]
  edge [
    source 45
    target 305
  ]
  edge [
    source 45
    target 306
  ]
  edge [
    source 45
    target 307
  ]
  edge [
    source 45
    target 308
  ]
  edge [
    source 45
    target 309
  ]
  edge [
    source 45
    target 310
  ]
  edge [
    source 45
    target 311
  ]
  edge [
    source 45
    target 312
  ]
  edge [
    source 45
    target 313
  ]
  edge [
    source 45
    target 314
  ]
  edge [
    source 45
    target 315
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 316
  ]
  edge [
    source 47
    target 317
  ]
  edge [
    source 47
    target 318
  ]
  edge [
    source 47
    target 319
  ]
  edge [
    source 47
    target 320
  ]
  edge [
    source 47
    target 321
  ]
  edge [
    source 47
    target 322
  ]
  edge [
    source 47
    target 323
  ]
  edge [
    source 47
    target 324
  ]
  edge [
    source 47
    target 325
  ]
  edge [
    source 47
    target 326
  ]
  edge [
    source 47
    target 327
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 328
  ]
  edge [
    source 48
    target 329
  ]
  edge [
    source 48
    target 330
  ]
  edge [
    source 48
    target 306
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 331
  ]
  edge [
    source 49
    target 332
  ]
  edge [
    source 49
    target 333
  ]
  edge [
    source 49
    target 334
  ]
  edge [
    source 49
    target 335
  ]
  edge [
    source 49
    target 336
  ]
  edge [
    source 49
    target 337
  ]
  edge [
    source 49
    target 338
  ]
  edge [
    source 49
    target 339
  ]
  edge [
    source 49
    target 340
  ]
  edge [
    source 49
    target 341
  ]
  edge [
    source 49
    target 342
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 343
  ]
  edge [
    source 50
    target 344
  ]
  edge [
    source 50
    target 345
  ]
  edge [
    source 50
    target 346
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 347
  ]
  edge [
    source 51
    target 348
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 349
  ]
  edge [
    source 52
    target 350
  ]
  edge [
    source 52
    target 351
  ]
  edge [
    source 52
    target 352
  ]
  edge [
    source 52
    target 353
  ]
  edge [
    source 52
    target 354
  ]
  edge [
    source 52
    target 355
  ]
  edge [
    source 52
    target 356
  ]
  edge [
    source 52
    target 357
  ]
  edge [
    source 52
    target 324
  ]
  edge [
    source 52
    target 137
  ]
  edge [
    source 52
    target 358
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 359
  ]
  edge [
    source 53
    target 360
  ]
  edge [
    source 53
    target 361
  ]
  edge [
    source 53
    target 354
  ]
  edge [
    source 53
    target 362
  ]
  edge [
    source 53
    target 363
  ]
  edge [
    source 53
    target 364
  ]
  edge [
    source 53
    target 365
  ]
  edge [
    source 53
    target 366
  ]
  edge [
    source 53
    target 367
  ]
  edge [
    source 53
    target 368
  ]
  edge [
    source 53
    target 369
  ]
  edge [
    source 53
    target 370
  ]
  edge [
    source 53
    target 371
  ]
  edge [
    source 53
    target 372
  ]
  edge [
    source 53
    target 373
  ]
  edge [
    source 53
    target 374
  ]
  edge [
    source 53
    target 375
  ]
  edge [
    source 53
    target 376
  ]
  edge [
    source 53
    target 377
  ]
  edge [
    source 53
    target 378
  ]
  edge [
    source 53
    target 379
  ]
  edge [
    source 53
    target 180
  ]
  edge [
    source 53
    target 380
  ]
  edge [
    source 53
    target 381
  ]
  edge [
    source 53
    target 382
  ]
  edge [
    source 53
    target 383
  ]
  edge [
    source 53
    target 384
  ]
  edge [
    source 385
    target 386
  ]
  edge [
    source 385
    target 407
  ]
  edge [
    source 387
    target 388
  ]
  edge [
    source 387
    target 389
  ]
  edge [
    source 388
    target 389
  ]
  edge [
    source 388
    target 408
  ]
  edge [
    source 389
    target 408
  ]
  edge [
    source 390
    target 391
  ]
  edge [
    source 390
    target 392
  ]
  edge [
    source 390
    target 393
  ]
  edge [
    source 390
    target 394
  ]
  edge [
    source 390
    target 395
  ]
  edge [
    source 390
    target 396
  ]
  edge [
    source 390
    target 397
  ]
  edge [
    source 390
    target 398
  ]
  edge [
    source 391
    target 392
  ]
  edge [
    source 391
    target 393
  ]
  edge [
    source 391
    target 394
  ]
  edge [
    source 391
    target 395
  ]
  edge [
    source 391
    target 396
  ]
  edge [
    source 391
    target 397
  ]
  edge [
    source 391
    target 398
  ]
  edge [
    source 392
    target 393
  ]
  edge [
    source 392
    target 394
  ]
  edge [
    source 392
    target 395
  ]
  edge [
    source 392
    target 396
  ]
  edge [
    source 392
    target 397
  ]
  edge [
    source 392
    target 398
  ]
  edge [
    source 393
    target 394
  ]
  edge [
    source 393
    target 395
  ]
  edge [
    source 393
    target 396
  ]
  edge [
    source 393
    target 397
  ]
  edge [
    source 393
    target 398
  ]
  edge [
    source 394
    target 395
  ]
  edge [
    source 394
    target 396
  ]
  edge [
    source 394
    target 397
  ]
  edge [
    source 394
    target 398
  ]
  edge [
    source 394
    target 399
  ]
  edge [
    source 394
    target 400
  ]
  edge [
    source 394
    target 401
  ]
  edge [
    source 394
    target 402
  ]
  edge [
    source 394
    target 403
  ]
  edge [
    source 394
    target 404
  ]
  edge [
    source 395
    target 396
  ]
  edge [
    source 395
    target 397
  ]
  edge [
    source 395
    target 398
  ]
  edge [
    source 395
    target 399
  ]
  edge [
    source 395
    target 400
  ]
  edge [
    source 395
    target 401
  ]
  edge [
    source 395
    target 402
  ]
  edge [
    source 395
    target 403
  ]
  edge [
    source 395
    target 404
  ]
  edge [
    source 396
    target 397
  ]
  edge [
    source 396
    target 398
  ]
  edge [
    source 397
    target 398
  ]
  edge [
    source 399
    target 400
  ]
  edge [
    source 399
    target 401
  ]
  edge [
    source 399
    target 402
  ]
  edge [
    source 399
    target 403
  ]
  edge [
    source 399
    target 404
  ]
  edge [
    source 400
    target 401
  ]
  edge [
    source 400
    target 402
  ]
  edge [
    source 400
    target 403
  ]
  edge [
    source 400
    target 404
  ]
  edge [
    source 401
    target 402
  ]
  edge [
    source 401
    target 403
  ]
  edge [
    source 401
    target 404
  ]
  edge [
    source 402
    target 403
  ]
  edge [
    source 402
    target 404
  ]
  edge [
    source 403
    target 404
  ]
  edge [
    source 405
    target 406
  ]
  edge [
    source 409
    target 410
  ]
  edge [
    source 411
    target 412
  ]
]
