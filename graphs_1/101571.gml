graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.0306122448979593
  density 0.010413396127681843
  graphCliqueNumber 3
  node [
    id 0
    label "pomieniony"
    origin "text"
  ]
  node [
    id 1
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 2
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 3
    label "swoje"
    origin "text"
  ]
  node [
    id 4
    label "godzina"
    origin "text"
  ]
  node [
    id 5
    label "wolne"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "dopomaga&#263;"
    origin "text"
  ]
  node [
    id 8
    label "rzemie&#347;lnik"
    origin "text"
  ]
  node [
    id 9
    label "nasi"
    origin "text"
  ]
  node [
    id 10
    label "d&#378;wiga&#263;"
    origin "text"
  ]
  node [
    id 11
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "wielki"
    origin "text"
  ]
  node [
    id 13
    label "kamie&#324;"
    origin "text"
  ]
  node [
    id 14
    label "dla"
    origin "text"
  ]
  node [
    id 15
    label "doko&#324;czenie"
    origin "text"
  ]
  node [
    id 16
    label "mur"
    origin "text"
  ]
  node [
    id 17
    label "zamek"
    origin "text"
  ]
  node [
    id 18
    label "inny"
    origin "text"
  ]
  node [
    id 19
    label "budowla"
    origin "text"
  ]
  node [
    id 20
    label "cesarski"
    origin "text"
  ]
  node [
    id 21
    label "asymilowa&#263;"
  ]
  node [
    id 22
    label "wapniak"
  ]
  node [
    id 23
    label "dwun&#243;g"
  ]
  node [
    id 24
    label "polifag"
  ]
  node [
    id 25
    label "wz&#243;r"
  ]
  node [
    id 26
    label "profanum"
  ]
  node [
    id 27
    label "hominid"
  ]
  node [
    id 28
    label "homo_sapiens"
  ]
  node [
    id 29
    label "nasada"
  ]
  node [
    id 30
    label "podw&#322;adny"
  ]
  node [
    id 31
    label "ludzko&#347;&#263;"
  ]
  node [
    id 32
    label "os&#322;abianie"
  ]
  node [
    id 33
    label "mikrokosmos"
  ]
  node [
    id 34
    label "portrecista"
  ]
  node [
    id 35
    label "duch"
  ]
  node [
    id 36
    label "oddzia&#322;ywanie"
  ]
  node [
    id 37
    label "g&#322;owa"
  ]
  node [
    id 38
    label "asymilowanie"
  ]
  node [
    id 39
    label "osoba"
  ]
  node [
    id 40
    label "os&#322;abia&#263;"
  ]
  node [
    id 41
    label "figura"
  ]
  node [
    id 42
    label "Adam"
  ]
  node [
    id 43
    label "senior"
  ]
  node [
    id 44
    label "antropochoria"
  ]
  node [
    id 45
    label "posta&#263;"
  ]
  node [
    id 46
    label "przedmiot"
  ]
  node [
    id 47
    label "grupa"
  ]
  node [
    id 48
    label "element"
  ]
  node [
    id 49
    label "przele&#378;&#263;"
  ]
  node [
    id 50
    label "pi&#281;tro"
  ]
  node [
    id 51
    label "karczek"
  ]
  node [
    id 52
    label "wysoki"
  ]
  node [
    id 53
    label "rami&#261;czko"
  ]
  node [
    id 54
    label "Ropa"
  ]
  node [
    id 55
    label "Jaworze"
  ]
  node [
    id 56
    label "Synaj"
  ]
  node [
    id 57
    label "wzniesienie"
  ]
  node [
    id 58
    label "przelezienie"
  ]
  node [
    id 59
    label "&#347;piew"
  ]
  node [
    id 60
    label "kupa"
  ]
  node [
    id 61
    label "kierunek"
  ]
  node [
    id 62
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 63
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 64
    label "d&#378;wi&#281;k"
  ]
  node [
    id 65
    label "Kreml"
  ]
  node [
    id 66
    label "minuta"
  ]
  node [
    id 67
    label "doba"
  ]
  node [
    id 68
    label "czas"
  ]
  node [
    id 69
    label "p&#243;&#322;godzina"
  ]
  node [
    id 70
    label "kwadrans"
  ]
  node [
    id 71
    label "time"
  ]
  node [
    id 72
    label "jednostka_czasu"
  ]
  node [
    id 73
    label "czas_wolny"
  ]
  node [
    id 74
    label "si&#281;ga&#263;"
  ]
  node [
    id 75
    label "trwa&#263;"
  ]
  node [
    id 76
    label "obecno&#347;&#263;"
  ]
  node [
    id 77
    label "stan"
  ]
  node [
    id 78
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 79
    label "stand"
  ]
  node [
    id 80
    label "mie&#263;_miejsce"
  ]
  node [
    id 81
    label "uczestniczy&#263;"
  ]
  node [
    id 82
    label "chodzi&#263;"
  ]
  node [
    id 83
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 84
    label "equal"
  ]
  node [
    id 85
    label "u&#322;atwia&#263;"
  ]
  node [
    id 86
    label "sprzyja&#263;"
  ]
  node [
    id 87
    label "Warszawa"
  ]
  node [
    id 88
    label "back"
  ]
  node [
    id 89
    label "remiecha"
  ]
  node [
    id 90
    label "pryczy&#263;"
  ]
  node [
    id 91
    label "ulepsza&#263;"
  ]
  node [
    id 92
    label "znosi&#263;"
  ]
  node [
    id 93
    label "zadawa&#263;"
  ]
  node [
    id 94
    label "nie&#347;&#263;"
  ]
  node [
    id 95
    label "enhance"
  ]
  node [
    id 96
    label "tire"
  ]
  node [
    id 97
    label "raise"
  ]
  node [
    id 98
    label "podnosi&#263;"
  ]
  node [
    id 99
    label "odbudowywa&#263;"
  ]
  node [
    id 100
    label "jaki&#347;"
  ]
  node [
    id 101
    label "dupny"
  ]
  node [
    id 102
    label "wysoce"
  ]
  node [
    id 103
    label "wyj&#261;tkowy"
  ]
  node [
    id 104
    label "wybitny"
  ]
  node [
    id 105
    label "znaczny"
  ]
  node [
    id 106
    label "prawdziwy"
  ]
  node [
    id 107
    label "wa&#380;ny"
  ]
  node [
    id 108
    label "nieprzeci&#281;tny"
  ]
  node [
    id 109
    label "skamienienie"
  ]
  node [
    id 110
    label "z&#322;&#243;g"
  ]
  node [
    id 111
    label "jednostka_avoirdupois"
  ]
  node [
    id 112
    label "tworzywo"
  ]
  node [
    id 113
    label "rock"
  ]
  node [
    id 114
    label "lapidarium"
  ]
  node [
    id 115
    label "minera&#322;_barwny"
  ]
  node [
    id 116
    label "Had&#380;ar"
  ]
  node [
    id 117
    label "autografia"
  ]
  node [
    id 118
    label "rekwizyt_do_gry"
  ]
  node [
    id 119
    label "osad"
  ]
  node [
    id 120
    label "funt"
  ]
  node [
    id 121
    label "mad&#380;ong"
  ]
  node [
    id 122
    label "oczko"
  ]
  node [
    id 123
    label "cube"
  ]
  node [
    id 124
    label "p&#322;ytka"
  ]
  node [
    id 125
    label "domino"
  ]
  node [
    id 126
    label "ci&#281;&#380;ar"
  ]
  node [
    id 127
    label "ska&#322;a"
  ]
  node [
    id 128
    label "kamienienie"
  ]
  node [
    id 129
    label "dzia&#322;anie"
  ]
  node [
    id 130
    label "zrobienie"
  ]
  node [
    id 131
    label "termination"
  ]
  node [
    id 132
    label "trudno&#347;&#263;"
  ]
  node [
    id 133
    label "konstrukcja"
  ]
  node [
    id 134
    label "tynk"
  ]
  node [
    id 135
    label "belkowanie"
  ]
  node [
    id 136
    label "&#347;ciana"
  ]
  node [
    id 137
    label "fola"
  ]
  node [
    id 138
    label "futr&#243;wka"
  ]
  node [
    id 139
    label "rz&#261;d"
  ]
  node [
    id 140
    label "obstruction"
  ]
  node [
    id 141
    label "futbolista"
  ]
  node [
    id 142
    label "gzyms"
  ]
  node [
    id 143
    label "ceg&#322;a"
  ]
  node [
    id 144
    label "Windsor"
  ]
  node [
    id 145
    label "budynek"
  ]
  node [
    id 146
    label "bramka"
  ]
  node [
    id 147
    label "fortyfikacja"
  ]
  node [
    id 148
    label "blokada"
  ]
  node [
    id 149
    label "iglica"
  ]
  node [
    id 150
    label "fastener"
  ]
  node [
    id 151
    label "baszta"
  ]
  node [
    id 152
    label "zagrywka"
  ]
  node [
    id 153
    label "hokej"
  ]
  node [
    id 154
    label "wjazd"
  ]
  node [
    id 155
    label "mechanizm"
  ]
  node [
    id 156
    label "bro&#324;_palna"
  ]
  node [
    id 157
    label "stra&#380;nica"
  ]
  node [
    id 158
    label "blockage"
  ]
  node [
    id 159
    label "informatyka"
  ]
  node [
    id 160
    label "Wawel"
  ]
  node [
    id 161
    label "brama"
  ]
  node [
    id 162
    label "ekspres"
  ]
  node [
    id 163
    label "rezydencja"
  ]
  node [
    id 164
    label "Zamek_Ogrodzieniec"
  ]
  node [
    id 165
    label "drzwi"
  ]
  node [
    id 166
    label "komora_zamkowa"
  ]
  node [
    id 167
    label "zamkni&#281;cie"
  ]
  node [
    id 168
    label "tercja"
  ]
  node [
    id 169
    label "zapi&#281;cie"
  ]
  node [
    id 170
    label "kolejny"
  ]
  node [
    id 171
    label "inaczej"
  ]
  node [
    id 172
    label "r&#243;&#380;ny"
  ]
  node [
    id 173
    label "inszy"
  ]
  node [
    id 174
    label "osobno"
  ]
  node [
    id 175
    label "kolumnada"
  ]
  node [
    id 176
    label "obudowywa&#263;"
  ]
  node [
    id 177
    label "zbudowa&#263;"
  ]
  node [
    id 178
    label "obudowanie"
  ]
  node [
    id 179
    label "obudowywanie"
  ]
  node [
    id 180
    label "Sukiennice"
  ]
  node [
    id 181
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 182
    label "fundament"
  ]
  node [
    id 183
    label "stan_surowy"
  ]
  node [
    id 184
    label "rzecz"
  ]
  node [
    id 185
    label "obudowa&#263;"
  ]
  node [
    id 186
    label "postanie"
  ]
  node [
    id 187
    label "zbudowanie"
  ]
  node [
    id 188
    label "korpus"
  ]
  node [
    id 189
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 190
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 191
    label "po_kr&#243;lewsku"
  ]
  node [
    id 192
    label "wspania&#322;y"
  ]
  node [
    id 193
    label "po_cesarsku"
  ]
  node [
    id 194
    label "Skyresha"
  ]
  node [
    id 195
    label "Bolgolama"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 194
    target 195
  ]
]
