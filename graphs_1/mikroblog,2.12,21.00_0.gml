graph [
  maxDegree 43
  minDegree 1
  meanDegree 3.0625
  density 0.019261006289308175
  graphCliqueNumber 12
  node [
    id 0
    label "tldr"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "oskar&#380;ona"
    origin "text"
  ]
  node [
    id 3
    label "pr&#243;ba"
    origin "text"
  ]
  node [
    id 4
    label "przemyt"
    origin "text"
  ]
  node [
    id 5
    label "ostry"
    origin "text"
  ]
  node [
    id 6
    label "amunicja"
    origin "text"
  ]
  node [
    id 7
    label "pok&#322;ad"
    origin "text"
  ]
  node [
    id 8
    label "samolot"
    origin "text"
  ]
  node [
    id 9
    label "tym"
    origin "text"
  ]
  node [
    id 10
    label "przemyci&#263;by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "raz"
    origin "text"
  ]
  node [
    id 12
    label "usi&#322;owanie"
  ]
  node [
    id 13
    label "pobiera&#263;"
  ]
  node [
    id 14
    label "spotkanie"
  ]
  node [
    id 15
    label "analiza_chemiczna"
  ]
  node [
    id 16
    label "test"
  ]
  node [
    id 17
    label "znak"
  ]
  node [
    id 18
    label "item"
  ]
  node [
    id 19
    label "ilo&#347;&#263;"
  ]
  node [
    id 20
    label "effort"
  ]
  node [
    id 21
    label "czynno&#347;&#263;"
  ]
  node [
    id 22
    label "metal_szlachetny"
  ]
  node [
    id 23
    label "pobranie"
  ]
  node [
    id 24
    label "pobieranie"
  ]
  node [
    id 25
    label "sytuacja"
  ]
  node [
    id 26
    label "do&#347;wiadczenie"
  ]
  node [
    id 27
    label "probiernictwo"
  ]
  node [
    id 28
    label "zbi&#243;r"
  ]
  node [
    id 29
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 30
    label "pobra&#263;"
  ]
  node [
    id 31
    label "rezultat"
  ]
  node [
    id 32
    label "przest&#281;pstwo"
  ]
  node [
    id 33
    label "towar"
  ]
  node [
    id 34
    label "silny"
  ]
  node [
    id 35
    label "jednoznaczny"
  ]
  node [
    id 36
    label "widoczny"
  ]
  node [
    id 37
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 38
    label "ostro"
  ]
  node [
    id 39
    label "skuteczny"
  ]
  node [
    id 40
    label "dynamiczny"
  ]
  node [
    id 41
    label "k&#322;uj&#261;cy"
  ]
  node [
    id 42
    label "gro&#378;ny"
  ]
  node [
    id 43
    label "trudny"
  ]
  node [
    id 44
    label "raptowny"
  ]
  node [
    id 45
    label "za&#380;arcie"
  ]
  node [
    id 46
    label "ostrzenie"
  ]
  node [
    id 47
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 48
    label "niebezpieczny"
  ]
  node [
    id 49
    label "naostrzenie"
  ]
  node [
    id 50
    label "nieoboj&#281;tny"
  ]
  node [
    id 51
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 52
    label "bystro"
  ]
  node [
    id 53
    label "podniecaj&#261;cy"
  ]
  node [
    id 54
    label "porywczy"
  ]
  node [
    id 55
    label "agresywny"
  ]
  node [
    id 56
    label "szorstki"
  ]
  node [
    id 57
    label "nieneutralny"
  ]
  node [
    id 58
    label "kategoryczny"
  ]
  node [
    id 59
    label "nieprzyjazny"
  ]
  node [
    id 60
    label "dotkliwy"
  ]
  node [
    id 61
    label "mocny"
  ]
  node [
    id 62
    label "energiczny"
  ]
  node [
    id 63
    label "dramatyczny"
  ]
  node [
    id 64
    label "dokuczliwy"
  ]
  node [
    id 65
    label "zdecydowany"
  ]
  node [
    id 66
    label "gryz&#261;cy"
  ]
  node [
    id 67
    label "nieobyczajny"
  ]
  node [
    id 68
    label "powa&#380;ny"
  ]
  node [
    id 69
    label "intensywny"
  ]
  node [
    id 70
    label "osch&#322;y"
  ]
  node [
    id 71
    label "dziki"
  ]
  node [
    id 72
    label "wyra&#378;ny"
  ]
  node [
    id 73
    label "ci&#281;&#380;ki"
  ]
  node [
    id 74
    label "surowy"
  ]
  node [
    id 75
    label "pocisk"
  ]
  node [
    id 76
    label "bro&#324;"
  ]
  node [
    id 77
    label "uzbrojenie"
  ]
  node [
    id 78
    label "&#380;agl&#243;wka"
  ]
  node [
    id 79
    label "warstwa"
  ]
  node [
    id 80
    label "jut"
  ]
  node [
    id 81
    label "statek"
  ]
  node [
    id 82
    label "p&#243;&#322;pok&#322;ad"
  ]
  node [
    id 83
    label "kipa"
  ]
  node [
    id 84
    label "przestrze&#324;"
  ]
  node [
    id 85
    label "powierzchnia"
  ]
  node [
    id 86
    label "sp&#261;g"
  ]
  node [
    id 87
    label "&#322;&#243;d&#378;"
  ]
  node [
    id 88
    label "poszycie_pok&#322;adowe"
  ]
  node [
    id 89
    label "kabina"
  ]
  node [
    id 90
    label "pok&#322;adnik"
  ]
  node [
    id 91
    label "strop"
  ]
  node [
    id 92
    label "z&#322;o&#380;e"
  ]
  node [
    id 93
    label "p&#322;aszczyzna"
  ]
  node [
    id 94
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 95
    label "p&#322;atowiec"
  ]
  node [
    id 96
    label "lecenie"
  ]
  node [
    id 97
    label "gondola"
  ]
  node [
    id 98
    label "wylecie&#263;"
  ]
  node [
    id 99
    label "kapotowanie"
  ]
  node [
    id 100
    label "wylatywa&#263;"
  ]
  node [
    id 101
    label "katapulta"
  ]
  node [
    id 102
    label "dzi&#243;b"
  ]
  node [
    id 103
    label "sterownica"
  ]
  node [
    id 104
    label "kad&#322;ub"
  ]
  node [
    id 105
    label "kapotowa&#263;"
  ]
  node [
    id 106
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 107
    label "fotel_lotniczy"
  ]
  node [
    id 108
    label "wylatywanie"
  ]
  node [
    id 109
    label "pilot_automatyczny"
  ]
  node [
    id 110
    label "inhalator_tlenowy"
  ]
  node [
    id 111
    label "kapota&#380;"
  ]
  node [
    id 112
    label "sta&#322;op&#322;at"
  ]
  node [
    id 113
    label "&#380;yroskop"
  ]
  node [
    id 114
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 115
    label "wy&#347;lizg"
  ]
  node [
    id 116
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 117
    label "skrzyd&#322;o"
  ]
  node [
    id 118
    label "wiatrochron"
  ]
  node [
    id 119
    label "spalin&#243;wka"
  ]
  node [
    id 120
    label "czarna_skrzynka"
  ]
  node [
    id 121
    label "kapot"
  ]
  node [
    id 122
    label "wylecenie"
  ]
  node [
    id 123
    label "kabinka"
  ]
  node [
    id 124
    label "chwila"
  ]
  node [
    id 125
    label "uderzenie"
  ]
  node [
    id 126
    label "cios"
  ]
  node [
    id 127
    label "time"
  ]
  node [
    id 128
    label "san"
  ]
  node [
    id 129
    label "Francisno"
  ]
  node [
    id 130
    label "nowa"
  ]
  node [
    id 131
    label "York"
  ]
  node [
    id 132
    label "XD"
  ]
  node [
    id 133
    label "pan"
  ]
  node [
    id 134
    label "bo"
  ]
  node [
    id 135
    label "przecie&#380;"
  ]
  node [
    id 136
    label "tylko"
  ]
  node [
    id 137
    label "jeden"
  ]
  node [
    id 138
    label "nab&#243;j"
  ]
  node [
    id 139
    label "a"
  ]
  node [
    id 140
    label "nie"
  ]
  node [
    id 141
    label "karabin"
  ]
  node [
    id 142
    label "XDXDXD"
  ]
  node [
    id 143
    label "nast&#281;pny"
  ]
  node [
    id 144
    label "dzie&#324;"
  ]
  node [
    id 145
    label "policja"
  ]
  node [
    id 146
    label "przysz&#322;y"
  ]
  node [
    id 147
    label "do"
  ]
  node [
    id 148
    label "moje"
  ]
  node [
    id 149
    label "dom"
  ]
  node [
    id 150
    label "z"
  ]
  node [
    id 151
    label "nakaz"
  ]
  node [
    id 152
    label "przeszukanie"
  ]
  node [
    id 153
    label "pok&#243;j"
  ]
  node [
    id 154
    label "nasz"
  ]
  node [
    id 155
    label "stra&#380;a"
  ]
  node [
    id 156
    label "graniczny"
  ]
  node [
    id 157
    label "teraz"
  ]
  node [
    id 158
    label "w"
  ]
  node [
    id 159
    label "gda&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 132
    target 143
  ]
  edge [
    source 132
    target 144
  ]
  edge [
    source 132
    target 145
  ]
  edge [
    source 132
    target 146
  ]
  edge [
    source 132
    target 147
  ]
  edge [
    source 132
    target 148
  ]
  edge [
    source 132
    target 149
  ]
  edge [
    source 132
    target 150
  ]
  edge [
    source 132
    target 151
  ]
  edge [
    source 132
    target 152
  ]
  edge [
    source 132
    target 153
  ]
  edge [
    source 132
    target 157
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 134
    target 136
  ]
  edge [
    source 134
    target 137
  ]
  edge [
    source 134
    target 138
  ]
  edge [
    source 134
    target 139
  ]
  edge [
    source 134
    target 140
  ]
  edge [
    source 134
    target 141
  ]
  edge [
    source 134
    target 142
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 135
    target 137
  ]
  edge [
    source 135
    target 138
  ]
  edge [
    source 135
    target 139
  ]
  edge [
    source 135
    target 140
  ]
  edge [
    source 135
    target 141
  ]
  edge [
    source 135
    target 142
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 136
    target 138
  ]
  edge [
    source 136
    target 139
  ]
  edge [
    source 136
    target 140
  ]
  edge [
    source 136
    target 141
  ]
  edge [
    source 136
    target 142
  ]
  edge [
    source 137
    target 138
  ]
  edge [
    source 137
    target 139
  ]
  edge [
    source 137
    target 140
  ]
  edge [
    source 137
    target 141
  ]
  edge [
    source 137
    target 142
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 138
    target 140
  ]
  edge [
    source 138
    target 141
  ]
  edge [
    source 138
    target 142
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 139
    target 141
  ]
  edge [
    source 139
    target 142
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 140
    target 142
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 143
    target 145
  ]
  edge [
    source 143
    target 146
  ]
  edge [
    source 143
    target 147
  ]
  edge [
    source 143
    target 148
  ]
  edge [
    source 143
    target 149
  ]
  edge [
    source 143
    target 150
  ]
  edge [
    source 143
    target 151
  ]
  edge [
    source 143
    target 152
  ]
  edge [
    source 143
    target 153
  ]
  edge [
    source 144
    target 145
  ]
  edge [
    source 144
    target 146
  ]
  edge [
    source 144
    target 147
  ]
  edge [
    source 144
    target 148
  ]
  edge [
    source 144
    target 149
  ]
  edge [
    source 144
    target 150
  ]
  edge [
    source 144
    target 151
  ]
  edge [
    source 144
    target 152
  ]
  edge [
    source 144
    target 153
  ]
  edge [
    source 145
    target 146
  ]
  edge [
    source 145
    target 147
  ]
  edge [
    source 145
    target 148
  ]
  edge [
    source 145
    target 149
  ]
  edge [
    source 145
    target 150
  ]
  edge [
    source 145
    target 151
  ]
  edge [
    source 145
    target 152
  ]
  edge [
    source 145
    target 153
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 146
    target 148
  ]
  edge [
    source 146
    target 149
  ]
  edge [
    source 146
    target 150
  ]
  edge [
    source 146
    target 151
  ]
  edge [
    source 146
    target 152
  ]
  edge [
    source 146
    target 153
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 147
    target 149
  ]
  edge [
    source 147
    target 150
  ]
  edge [
    source 147
    target 151
  ]
  edge [
    source 147
    target 152
  ]
  edge [
    source 147
    target 153
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 148
    target 150
  ]
  edge [
    source 148
    target 151
  ]
  edge [
    source 148
    target 152
  ]
  edge [
    source 148
    target 148
  ]
  edge [
    source 148
    target 153
  ]
  edge [
    source 149
    target 150
  ]
  edge [
    source 149
    target 151
  ]
  edge [
    source 149
    target 152
  ]
  edge [
    source 149
    target 153
  ]
  edge [
    source 150
    target 151
  ]
  edge [
    source 150
    target 152
  ]
  edge [
    source 150
    target 153
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 151
    target 153
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 154
    target 156
  ]
  edge [
    source 154
    target 158
  ]
  edge [
    source 154
    target 159
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 155
    target 158
  ]
  edge [
    source 155
    target 159
  ]
  edge [
    source 156
    target 158
  ]
  edge [
    source 156
    target 159
  ]
  edge [
    source 158
    target 159
  ]
]
