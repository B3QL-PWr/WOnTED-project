graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.894736842105263
  density 0.10526315789473684
  graphCliqueNumber 2
  node [
    id 0
    label "&#322;adny"
    origin "text"
  ]
  node [
    id 1
    label "p&#322;ot"
    origin "text"
  ]
  node [
    id 2
    label "wycialem"
    origin "text"
  ]
  node [
    id 3
    label "g&#322;adki"
  ]
  node [
    id 4
    label "przyjemny"
  ]
  node [
    id 5
    label "ch&#281;dogi"
  ]
  node [
    id 6
    label "harny"
  ]
  node [
    id 7
    label "po&#380;&#261;dany"
  ]
  node [
    id 8
    label "&#322;adnie"
  ]
  node [
    id 9
    label "obyczajny"
  ]
  node [
    id 10
    label "dobrze"
  ]
  node [
    id 11
    label "z&#322;y"
  ]
  node [
    id 12
    label "dobry"
  ]
  node [
    id 13
    label "ca&#322;y"
  ]
  node [
    id 14
    label "niez&#322;y"
  ]
  node [
    id 15
    label "&#347;warny"
  ]
  node [
    id 16
    label "przyzwoity"
  ]
  node [
    id 17
    label "ogrodzenie"
  ]
  node [
    id 18
    label "bramka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
]
