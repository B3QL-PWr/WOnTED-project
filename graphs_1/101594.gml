graph [
  maxDegree 37
  minDegree 1
  meanDegree 2.161943319838057
  density 0.008788387479016491
  graphCliqueNumber 5
  node [
    id 0
    label "przyst&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "rozpatrzenie"
    origin "text"
  ]
  node [
    id 2
    label "punkt"
    origin "text"
  ]
  node [
    id 3
    label "porz&#261;dek"
    origin "text"
  ]
  node [
    id 4
    label "dzienny"
    origin "text"
  ]
  node [
    id 5
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 6
    label "komisja"
    origin "text"
  ]
  node [
    id 7
    label "infrastruktura"
    origin "text"
  ]
  node [
    id 8
    label "kultura"
    origin "text"
  ]
  node [
    id 9
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 10
    label "przekaz"
    origin "text"
  ]
  node [
    id 11
    label "poselski"
    origin "text"
  ]
  node [
    id 12
    label "projekt"
    origin "text"
  ]
  node [
    id 13
    label "uchwa&#322;a"
    origin "text"
  ]
  node [
    id 14
    label "sprawa"
    origin "text"
  ]
  node [
    id 15
    label "podj&#281;cie"
    origin "text"
  ]
  node [
    id 16
    label "przez"
    origin "text"
  ]
  node [
    id 17
    label "rada"
    origin "text"
  ]
  node [
    id 18
    label "minister"
    origin "text"
  ]
  node [
    id 19
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 20
    label "zmierza&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 22
    label "naziemny"
    origin "text"
  ]
  node [
    id 23
    label "telewizja"
    origin "text"
  ]
  node [
    id 24
    label "cyfrowy"
    origin "text"
  ]
  node [
    id 25
    label "polska"
    origin "text"
  ]
  node [
    id 26
    label "druk"
    origin "text"
  ]
  node [
    id 27
    label "wchodzi&#263;"
  ]
  node [
    id 28
    label "set_about"
  ]
  node [
    id 29
    label "submit"
  ]
  node [
    id 30
    label "zaczyna&#263;"
  ]
  node [
    id 31
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 32
    label "probe"
  ]
  node [
    id 33
    label "przemy&#347;lenie"
  ]
  node [
    id 34
    label "prosta"
  ]
  node [
    id 35
    label "po&#322;o&#380;enie"
  ]
  node [
    id 36
    label "chwila"
  ]
  node [
    id 37
    label "ust&#281;p"
  ]
  node [
    id 38
    label "problemat"
  ]
  node [
    id 39
    label "kres"
  ]
  node [
    id 40
    label "mark"
  ]
  node [
    id 41
    label "pozycja"
  ]
  node [
    id 42
    label "point"
  ]
  node [
    id 43
    label "stopie&#324;_pisma"
  ]
  node [
    id 44
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 45
    label "przestrze&#324;"
  ]
  node [
    id 46
    label "wojsko"
  ]
  node [
    id 47
    label "problematyka"
  ]
  node [
    id 48
    label "zapunktowa&#263;"
  ]
  node [
    id 49
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 50
    label "obiekt_matematyczny"
  ]
  node [
    id 51
    label "plamka"
  ]
  node [
    id 52
    label "miejsce"
  ]
  node [
    id 53
    label "obiekt"
  ]
  node [
    id 54
    label "plan"
  ]
  node [
    id 55
    label "podpunkt"
  ]
  node [
    id 56
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 57
    label "jednostka"
  ]
  node [
    id 58
    label "uk&#322;ad"
  ]
  node [
    id 59
    label "styl_architektoniczny"
  ]
  node [
    id 60
    label "stan"
  ]
  node [
    id 61
    label "normalizacja"
  ]
  node [
    id 62
    label "cecha"
  ]
  node [
    id 63
    label "relacja"
  ]
  node [
    id 64
    label "zasada"
  ]
  node [
    id 65
    label "struktura"
  ]
  node [
    id 66
    label "specjalny"
  ]
  node [
    id 67
    label "stacjonarnie"
  ]
  node [
    id 68
    label "kilkudziesi&#281;ciogodzinny"
  ]
  node [
    id 69
    label "student"
  ]
  node [
    id 70
    label "typowy"
  ]
  node [
    id 71
    label "message"
  ]
  node [
    id 72
    label "korespondent"
  ]
  node [
    id 73
    label "wypowied&#378;"
  ]
  node [
    id 74
    label "sprawko"
  ]
  node [
    id 75
    label "obrady"
  ]
  node [
    id 76
    label "zesp&#243;&#322;"
  ]
  node [
    id 77
    label "organ"
  ]
  node [
    id 78
    label "Komisja_Europejska"
  ]
  node [
    id 79
    label "podkomisja"
  ]
  node [
    id 80
    label "telefonia"
  ]
  node [
    id 81
    label "trasa"
  ]
  node [
    id 82
    label "zaplecze"
  ]
  node [
    id 83
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 84
    label "radiofonia"
  ]
  node [
    id 85
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 86
    label "przedmiot"
  ]
  node [
    id 87
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 88
    label "Wsch&#243;d"
  ]
  node [
    id 89
    label "rzecz"
  ]
  node [
    id 90
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 91
    label "sztuka"
  ]
  node [
    id 92
    label "religia"
  ]
  node [
    id 93
    label "przejmowa&#263;"
  ]
  node [
    id 94
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 95
    label "makrokosmos"
  ]
  node [
    id 96
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 97
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 98
    label "zjawisko"
  ]
  node [
    id 99
    label "praca_rolnicza"
  ]
  node [
    id 100
    label "tradycja"
  ]
  node [
    id 101
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 102
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 103
    label "przejmowanie"
  ]
  node [
    id 104
    label "asymilowanie_si&#281;"
  ]
  node [
    id 105
    label "przej&#261;&#263;"
  ]
  node [
    id 106
    label "hodowla"
  ]
  node [
    id 107
    label "brzoskwiniarnia"
  ]
  node [
    id 108
    label "populace"
  ]
  node [
    id 109
    label "konwencja"
  ]
  node [
    id 110
    label "propriety"
  ]
  node [
    id 111
    label "jako&#347;&#263;"
  ]
  node [
    id 112
    label "kuchnia"
  ]
  node [
    id 113
    label "zwyczaj"
  ]
  node [
    id 114
    label "przej&#281;cie"
  ]
  node [
    id 115
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 116
    label "czas"
  ]
  node [
    id 117
    label "abstrakcja"
  ]
  node [
    id 118
    label "substancja"
  ]
  node [
    id 119
    label "spos&#243;b"
  ]
  node [
    id 120
    label "chemikalia"
  ]
  node [
    id 121
    label "dokument"
  ]
  node [
    id 122
    label "znaczenie"
  ]
  node [
    id 123
    label "implicite"
  ]
  node [
    id 124
    label "transakcja"
  ]
  node [
    id 125
    label "order"
  ]
  node [
    id 126
    label "explicite"
  ]
  node [
    id 127
    label "draft"
  ]
  node [
    id 128
    label "proces"
  ]
  node [
    id 129
    label "kwota"
  ]
  node [
    id 130
    label "blankiet"
  ]
  node [
    id 131
    label "tekst"
  ]
  node [
    id 132
    label "device"
  ]
  node [
    id 133
    label "program_u&#380;ytkowy"
  ]
  node [
    id 134
    label "intencja"
  ]
  node [
    id 135
    label "agreement"
  ]
  node [
    id 136
    label "pomys&#322;"
  ]
  node [
    id 137
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 138
    label "dokumentacja"
  ]
  node [
    id 139
    label "resolution"
  ]
  node [
    id 140
    label "akt"
  ]
  node [
    id 141
    label "temat"
  ]
  node [
    id 142
    label "kognicja"
  ]
  node [
    id 143
    label "idea"
  ]
  node [
    id 144
    label "szczeg&#243;&#322;"
  ]
  node [
    id 145
    label "wydarzenie"
  ]
  node [
    id 146
    label "przes&#322;anka"
  ]
  node [
    id 147
    label "rozprawa"
  ]
  node [
    id 148
    label "object"
  ]
  node [
    id 149
    label "proposition"
  ]
  node [
    id 150
    label "zrobienie"
  ]
  node [
    id 151
    label "consumption"
  ]
  node [
    id 152
    label "czynno&#347;&#263;"
  ]
  node [
    id 153
    label "spowodowanie"
  ]
  node [
    id 154
    label "zareagowanie"
  ]
  node [
    id 155
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 156
    label "erecting"
  ]
  node [
    id 157
    label "movement"
  ]
  node [
    id 158
    label "entertainment"
  ]
  node [
    id 159
    label "zacz&#281;cie"
  ]
  node [
    id 160
    label "dyskusja"
  ]
  node [
    id 161
    label "grupa"
  ]
  node [
    id 162
    label "Rada_Unii_Europejskiej"
  ]
  node [
    id 163
    label "conference"
  ]
  node [
    id 164
    label "zgromadzenie"
  ]
  node [
    id 165
    label "wskaz&#243;wka"
  ]
  node [
    id 166
    label "konsylium"
  ]
  node [
    id 167
    label "Rada_Europy"
  ]
  node [
    id 168
    label "Rada_Europejska"
  ]
  node [
    id 169
    label "posiedzenie"
  ]
  node [
    id 170
    label "Goebbels"
  ]
  node [
    id 171
    label "Sto&#322;ypin"
  ]
  node [
    id 172
    label "rz&#261;d"
  ]
  node [
    id 173
    label "dostojnik"
  ]
  node [
    id 174
    label "strategia"
  ]
  node [
    id 175
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 176
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 177
    label "try"
  ]
  node [
    id 178
    label "post&#281;powa&#263;"
  ]
  node [
    id 179
    label "describe"
  ]
  node [
    id 180
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 181
    label "rynek"
  ]
  node [
    id 182
    label "issue"
  ]
  node [
    id 183
    label "evocation"
  ]
  node [
    id 184
    label "wst&#281;p"
  ]
  node [
    id 185
    label "nuklearyzacja"
  ]
  node [
    id 186
    label "umo&#380;liwienie"
  ]
  node [
    id 187
    label "wpisanie"
  ]
  node [
    id 188
    label "zapoznanie"
  ]
  node [
    id 189
    label "entrance"
  ]
  node [
    id 190
    label "wej&#347;cie"
  ]
  node [
    id 191
    label "podstawy"
  ]
  node [
    id 192
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 193
    label "w&#322;&#261;czenie"
  ]
  node [
    id 194
    label "doprowadzenie"
  ]
  node [
    id 195
    label "przewietrzenie"
  ]
  node [
    id 196
    label "deduction"
  ]
  node [
    id 197
    label "umieszczenie"
  ]
  node [
    id 198
    label "naziemnie"
  ]
  node [
    id 199
    label "Polsat"
  ]
  node [
    id 200
    label "paj&#281;czarz"
  ]
  node [
    id 201
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 202
    label "programowiec"
  ]
  node [
    id 203
    label "technologia"
  ]
  node [
    id 204
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 205
    label "Interwizja"
  ]
  node [
    id 206
    label "BBC"
  ]
  node [
    id 207
    label "ekran"
  ]
  node [
    id 208
    label "redakcja"
  ]
  node [
    id 209
    label "media"
  ]
  node [
    id 210
    label "odbieranie"
  ]
  node [
    id 211
    label "odbiera&#263;"
  ]
  node [
    id 212
    label "odbiornik"
  ]
  node [
    id 213
    label "instytucja"
  ]
  node [
    id 214
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 215
    label "studio"
  ]
  node [
    id 216
    label "telekomunikacja"
  ]
  node [
    id 217
    label "muza"
  ]
  node [
    id 218
    label "cyfrowo"
  ]
  node [
    id 219
    label "sygna&#322;_cyfrowy"
  ]
  node [
    id 220
    label "elektroniczny"
  ]
  node [
    id 221
    label "cyfryzacja"
  ]
  node [
    id 222
    label "prohibita"
  ]
  node [
    id 223
    label "impression"
  ]
  node [
    id 224
    label "wytw&#243;r"
  ]
  node [
    id 225
    label "tkanina"
  ]
  node [
    id 226
    label "glif"
  ]
  node [
    id 227
    label "formatowanie"
  ]
  node [
    id 228
    label "printing"
  ]
  node [
    id 229
    label "technika"
  ]
  node [
    id 230
    label "formatowa&#263;"
  ]
  node [
    id 231
    label "pismo"
  ]
  node [
    id 232
    label "cymelium"
  ]
  node [
    id 233
    label "zdobnik"
  ]
  node [
    id 234
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 235
    label "publikacja"
  ]
  node [
    id 236
    label "zaproszenie"
  ]
  node [
    id 237
    label "dese&#324;"
  ]
  node [
    id 238
    label "character"
  ]
  node [
    id 239
    label "i"
  ]
  node [
    id 240
    label "&#347;rodki"
  ]
  node [
    id 241
    label "El&#380;bieta"
  ]
  node [
    id 242
    label "kruk"
  ]
  node [
    id 243
    label "prawo"
  ]
  node [
    id 244
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 245
    label "rzeczpospolita"
  ]
  node [
    id 246
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 6
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 54
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 77
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 87
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 150
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 23
    target 211
  ]
  edge [
    source 23
    target 212
  ]
  edge [
    source 23
    target 213
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 222
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 26
    target 224
  ]
  edge [
    source 26
    target 225
  ]
  edge [
    source 26
    target 226
  ]
  edge [
    source 26
    target 227
  ]
  edge [
    source 26
    target 228
  ]
  edge [
    source 26
    target 229
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 131
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 239
    target 240
  ]
  edge [
    source 239
    target 243
  ]
  edge [
    source 239
    target 244
  ]
  edge [
    source 241
    target 242
  ]
  edge [
    source 243
    target 244
  ]
  edge [
    source 245
    target 246
  ]
]
