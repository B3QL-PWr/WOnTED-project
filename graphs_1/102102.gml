graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.2258953168044076
  density 0.0061488820906199105
  graphCliqueNumber 4
  node [
    id 0
    label "ucze&#324;"
    origin "text"
  ]
  node [
    id 1
    label "ozorkowski"
    origin "text"
  ]
  node [
    id 2
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "otrzyma&#263;"
    origin "text"
  ]
  node [
    id 4
    label "stypendium"
    origin "text"
  ]
  node [
    id 5
    label "socjalna"
    origin "text"
  ]
  node [
    id 6
    label "przeznaczy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zakup"
    origin "text"
  ]
  node [
    id 8
    label "pomoc"
    origin "text"
  ]
  node [
    id 9
    label "naukowy"
    origin "text"
  ]
  node [
    id 10
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 11
    label "pochodzi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "bud&#380;et"
    origin "text"
  ]
  node [
    id 13
    label "wojewoda"
    origin "text"
  ]
  node [
    id 14
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 15
    label "rozdziela&#263;"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "tutejszy"
    origin "text"
  ]
  node [
    id 18
    label "magistrat"
    origin "text"
  ]
  node [
    id 19
    label "pierwsza"
    origin "text"
  ]
  node [
    id 20
    label "wyp&#322;at"
    origin "text"
  ]
  node [
    id 21
    label "nast&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 22
    label "niestety"
    origin "text"
  ]
  node [
    id 23
    label "dopiero"
    origin "text"
  ]
  node [
    id 24
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 25
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 26
    label "tym"
    origin "text"
  ]
  node [
    id 27
    label "rok"
    origin "text"
  ]
  node [
    id 28
    label "stara&#263;"
    origin "text"
  ]
  node [
    id 29
    label "si&#281;"
    origin "text"
  ]
  node [
    id 30
    label "osoba"
    origin "text"
  ]
  node [
    id 31
    label "spe&#322;nia&#263;"
    origin "text"
  ]
  node [
    id 32
    label "kryterium"
    origin "text"
  ]
  node [
    id 33
    label "pozosta&#322;y"
    origin "text"
  ]
  node [
    id 34
    label "przyzna&#263;"
    origin "text"
  ]
  node [
    id 35
    label "wsparcie"
    origin "text"
  ]
  node [
    id 36
    label "granica"
    origin "text"
  ]
  node [
    id 37
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 38
    label "miesi&#281;cznie"
    origin "text"
  ]
  node [
    id 39
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 40
    label "wyp&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 41
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 42
    label "wyr&#243;wnanie"
    origin "text"
  ]
  node [
    id 43
    label "cztery"
    origin "text"
  ]
  node [
    id 44
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 45
    label "rocznik"
    origin "text"
  ]
  node [
    id 46
    label "raz"
    origin "text"
  ]
  node [
    id 47
    label "drugi"
    origin "text"
  ]
  node [
    id 48
    label "czerwiec"
    origin "text"
  ]
  node [
    id 49
    label "sze&#347;&#263;"
    origin "text"
  ]
  node [
    id 50
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 51
    label "cz&#322;owiek"
  ]
  node [
    id 52
    label "zwolennik"
  ]
  node [
    id 53
    label "tarcza"
  ]
  node [
    id 54
    label "czeladnik"
  ]
  node [
    id 55
    label "elew"
  ]
  node [
    id 56
    label "rzemie&#347;lnik"
  ]
  node [
    id 57
    label "kontynuator"
  ]
  node [
    id 58
    label "klasa"
  ]
  node [
    id 59
    label "wyprawka"
  ]
  node [
    id 60
    label "mundurek"
  ]
  node [
    id 61
    label "absolwent"
  ]
  node [
    id 62
    label "praktykant"
  ]
  node [
    id 63
    label "Mickiewicz"
  ]
  node [
    id 64
    label "czas"
  ]
  node [
    id 65
    label "szkolenie"
  ]
  node [
    id 66
    label "przepisa&#263;"
  ]
  node [
    id 67
    label "lesson"
  ]
  node [
    id 68
    label "grupa"
  ]
  node [
    id 69
    label "praktyka"
  ]
  node [
    id 70
    label "metoda"
  ]
  node [
    id 71
    label "niepokalanki"
  ]
  node [
    id 72
    label "kara"
  ]
  node [
    id 73
    label "zda&#263;"
  ]
  node [
    id 74
    label "form"
  ]
  node [
    id 75
    label "kwalifikacje"
  ]
  node [
    id 76
    label "system"
  ]
  node [
    id 77
    label "przepisanie"
  ]
  node [
    id 78
    label "sztuba"
  ]
  node [
    id 79
    label "wiedza"
  ]
  node [
    id 80
    label "stopek"
  ]
  node [
    id 81
    label "school"
  ]
  node [
    id 82
    label "urszulanki"
  ]
  node [
    id 83
    label "gabinet"
  ]
  node [
    id 84
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 85
    label "ideologia"
  ]
  node [
    id 86
    label "lekcja"
  ]
  node [
    id 87
    label "muzyka"
  ]
  node [
    id 88
    label "podr&#281;cznik"
  ]
  node [
    id 89
    label "zdanie"
  ]
  node [
    id 90
    label "siedziba"
  ]
  node [
    id 91
    label "sekretariat"
  ]
  node [
    id 92
    label "nauka"
  ]
  node [
    id 93
    label "do&#347;wiadczenie"
  ]
  node [
    id 94
    label "tablica"
  ]
  node [
    id 95
    label "teren_szko&#322;y"
  ]
  node [
    id 96
    label "instytucja"
  ]
  node [
    id 97
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 98
    label "skolaryzacja"
  ]
  node [
    id 99
    label "&#322;awa_szkolna"
  ]
  node [
    id 100
    label "wytworzy&#263;"
  ]
  node [
    id 101
    label "return"
  ]
  node [
    id 102
    label "give_birth"
  ]
  node [
    id 103
    label "dosta&#263;"
  ]
  node [
    id 104
    label "&#347;wiadczenie"
  ]
  node [
    id 105
    label "sta&#263;_si&#281;"
  ]
  node [
    id 106
    label "appoint"
  ]
  node [
    id 107
    label "zrobi&#263;"
  ]
  node [
    id 108
    label "ustali&#263;"
  ]
  node [
    id 109
    label "oblat"
  ]
  node [
    id 110
    label "sprzedaj&#261;cy"
  ]
  node [
    id 111
    label "dobro"
  ]
  node [
    id 112
    label "transakcja"
  ]
  node [
    id 113
    label "zgodzi&#263;"
  ]
  node [
    id 114
    label "pomocnik"
  ]
  node [
    id 115
    label "doch&#243;d"
  ]
  node [
    id 116
    label "property"
  ]
  node [
    id 117
    label "przedmiot"
  ]
  node [
    id 118
    label "telefon_zaufania"
  ]
  node [
    id 119
    label "darowizna"
  ]
  node [
    id 120
    label "&#347;rodek"
  ]
  node [
    id 121
    label "liga"
  ]
  node [
    id 122
    label "specjalny"
  ]
  node [
    id 123
    label "edukacyjnie"
  ]
  node [
    id 124
    label "intelektualny"
  ]
  node [
    id 125
    label "skomplikowany"
  ]
  node [
    id 126
    label "zgodny"
  ]
  node [
    id 127
    label "naukowo"
  ]
  node [
    id 128
    label "scjentyficzny"
  ]
  node [
    id 129
    label "teoretyczny"
  ]
  node [
    id 130
    label "specjalistyczny"
  ]
  node [
    id 131
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 132
    label "zdewaluowa&#263;"
  ]
  node [
    id 133
    label "moniak"
  ]
  node [
    id 134
    label "wytw&#243;r"
  ]
  node [
    id 135
    label "zdewaluowanie"
  ]
  node [
    id 136
    label "jednostka_monetarna"
  ]
  node [
    id 137
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 138
    label "numizmat"
  ]
  node [
    id 139
    label "rozmieni&#263;"
  ]
  node [
    id 140
    label "rozmienienie"
  ]
  node [
    id 141
    label "rozmienianie"
  ]
  node [
    id 142
    label "dewaluowanie"
  ]
  node [
    id 143
    label "nomina&#322;"
  ]
  node [
    id 144
    label "coin"
  ]
  node [
    id 145
    label "dewaluowa&#263;"
  ]
  node [
    id 146
    label "pieni&#261;dze"
  ]
  node [
    id 147
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 148
    label "rozmienia&#263;"
  ]
  node [
    id 149
    label "date"
  ]
  node [
    id 150
    label "str&#243;j"
  ]
  node [
    id 151
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 152
    label "spowodowa&#263;"
  ]
  node [
    id 153
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 154
    label "uda&#263;_si&#281;"
  ]
  node [
    id 155
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 156
    label "poby&#263;"
  ]
  node [
    id 157
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 158
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 159
    label "wynika&#263;"
  ]
  node [
    id 160
    label "fall"
  ]
  node [
    id 161
    label "bolt"
  ]
  node [
    id 162
    label "wydatki_bud&#380;etowe"
  ]
  node [
    id 163
    label "etat"
  ]
  node [
    id 164
    label "portfel"
  ]
  node [
    id 165
    label "rezerwa_rewaluacyjna"
  ]
  node [
    id 166
    label "kwota"
  ]
  node [
    id 167
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 168
    label "dostojnik"
  ]
  node [
    id 169
    label "urz&#281;dnik"
  ]
  node [
    id 170
    label "organ"
  ]
  node [
    id 171
    label "palatyn"
  ]
  node [
    id 172
    label "urz&#281;dnik_ziemski"
  ]
  node [
    id 173
    label "cover"
  ]
  node [
    id 174
    label "sk&#322;&#243;ca&#263;"
  ]
  node [
    id 175
    label "share"
  ]
  node [
    id 176
    label "przestrze&#324;"
  ]
  node [
    id 177
    label "deal"
  ]
  node [
    id 178
    label "dzieli&#263;"
  ]
  node [
    id 179
    label "oddziela&#263;"
  ]
  node [
    id 180
    label "odr&#243;&#380;nia&#263;"
  ]
  node [
    id 181
    label "rozdawa&#263;"
  ]
  node [
    id 182
    label "si&#281;ga&#263;"
  ]
  node [
    id 183
    label "trwa&#263;"
  ]
  node [
    id 184
    label "obecno&#347;&#263;"
  ]
  node [
    id 185
    label "stan"
  ]
  node [
    id 186
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 187
    label "stand"
  ]
  node [
    id 188
    label "mie&#263;_miejsce"
  ]
  node [
    id 189
    label "uczestniczy&#263;"
  ]
  node [
    id 190
    label "chodzi&#263;"
  ]
  node [
    id 191
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 192
    label "equal"
  ]
  node [
    id 193
    label "tuteczny"
  ]
  node [
    id 194
    label "lokalny"
  ]
  node [
    id 195
    label "rynek"
  ]
  node [
    id 196
    label "plac_ratuszowy"
  ]
  node [
    id 197
    label "urz&#261;d"
  ]
  node [
    id 198
    label "urz&#281;dnik_miejski"
  ]
  node [
    id 199
    label "zwierzchnik"
  ]
  node [
    id 200
    label "godzina"
  ]
  node [
    id 201
    label "nacisn&#261;&#263;"
  ]
  node [
    id 202
    label "zaatakowa&#263;"
  ]
  node [
    id 203
    label "gamble"
  ]
  node [
    id 204
    label "supervene"
  ]
  node [
    id 205
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 206
    label "kolejny"
  ]
  node [
    id 207
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 208
    label "miech"
  ]
  node [
    id 209
    label "kalendy"
  ]
  node [
    id 210
    label "tydzie&#324;"
  ]
  node [
    id 211
    label "stulecie"
  ]
  node [
    id 212
    label "kalendarz"
  ]
  node [
    id 213
    label "pora_roku"
  ]
  node [
    id 214
    label "cykl_astronomiczny"
  ]
  node [
    id 215
    label "p&#243;&#322;rocze"
  ]
  node [
    id 216
    label "kwarta&#322;"
  ]
  node [
    id 217
    label "kurs"
  ]
  node [
    id 218
    label "jubileusz"
  ]
  node [
    id 219
    label "lata"
  ]
  node [
    id 220
    label "martwy_sezon"
  ]
  node [
    id 221
    label "Zgredek"
  ]
  node [
    id 222
    label "kategoria_gramatyczna"
  ]
  node [
    id 223
    label "Casanova"
  ]
  node [
    id 224
    label "Don_Juan"
  ]
  node [
    id 225
    label "Gargantua"
  ]
  node [
    id 226
    label "Faust"
  ]
  node [
    id 227
    label "profanum"
  ]
  node [
    id 228
    label "Chocho&#322;"
  ]
  node [
    id 229
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 230
    label "koniugacja"
  ]
  node [
    id 231
    label "Winnetou"
  ]
  node [
    id 232
    label "Dwukwiat"
  ]
  node [
    id 233
    label "homo_sapiens"
  ]
  node [
    id 234
    label "Edyp"
  ]
  node [
    id 235
    label "Herkules_Poirot"
  ]
  node [
    id 236
    label "ludzko&#347;&#263;"
  ]
  node [
    id 237
    label "mikrokosmos"
  ]
  node [
    id 238
    label "person"
  ]
  node [
    id 239
    label "Sherlock_Holmes"
  ]
  node [
    id 240
    label "portrecista"
  ]
  node [
    id 241
    label "Szwejk"
  ]
  node [
    id 242
    label "Hamlet"
  ]
  node [
    id 243
    label "duch"
  ]
  node [
    id 244
    label "g&#322;owa"
  ]
  node [
    id 245
    label "oddzia&#322;ywanie"
  ]
  node [
    id 246
    label "Quasimodo"
  ]
  node [
    id 247
    label "Dulcynea"
  ]
  node [
    id 248
    label "Don_Kiszot"
  ]
  node [
    id 249
    label "Wallenrod"
  ]
  node [
    id 250
    label "Plastu&#347;"
  ]
  node [
    id 251
    label "Harry_Potter"
  ]
  node [
    id 252
    label "figura"
  ]
  node [
    id 253
    label "parali&#380;owa&#263;"
  ]
  node [
    id 254
    label "istota"
  ]
  node [
    id 255
    label "Werter"
  ]
  node [
    id 256
    label "antropochoria"
  ]
  node [
    id 257
    label "posta&#263;"
  ]
  node [
    id 258
    label "robi&#263;"
  ]
  node [
    id 259
    label "close"
  ]
  node [
    id 260
    label "perform"
  ]
  node [
    id 261
    label "urzeczywistnia&#263;"
  ]
  node [
    id 262
    label "ocena"
  ]
  node [
    id 263
    label "czynnik"
  ]
  node [
    id 264
    label "inny"
  ]
  node [
    id 265
    label "&#380;ywy"
  ]
  node [
    id 266
    label "da&#263;"
  ]
  node [
    id 267
    label "nada&#263;"
  ]
  node [
    id 268
    label "pozwoli&#263;"
  ]
  node [
    id 269
    label "give"
  ]
  node [
    id 270
    label "stwierdzi&#263;"
  ]
  node [
    id 271
    label "comfort"
  ]
  node [
    id 272
    label "u&#322;atwienie"
  ]
  node [
    id 273
    label "oparcie"
  ]
  node [
    id 274
    label "dar"
  ]
  node [
    id 275
    label "zapomoga"
  ]
  node [
    id 276
    label "pocieszenie"
  ]
  node [
    id 277
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 278
    label "support"
  ]
  node [
    id 279
    label "zakres"
  ]
  node [
    id 280
    label "Ural"
  ]
  node [
    id 281
    label "koniec"
  ]
  node [
    id 282
    label "kres"
  ]
  node [
    id 283
    label "granice"
  ]
  node [
    id 284
    label "granica_pa&#324;stwa"
  ]
  node [
    id 285
    label "pu&#322;ap"
  ]
  node [
    id 286
    label "frontier"
  ]
  node [
    id 287
    label "end"
  ]
  node [
    id 288
    label "miara"
  ]
  node [
    id 289
    label "poj&#281;cie"
  ]
  node [
    id 290
    label "przej&#347;cie"
  ]
  node [
    id 291
    label "szlachetny"
  ]
  node [
    id 292
    label "metaliczny"
  ]
  node [
    id 293
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 294
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 295
    label "grosz"
  ]
  node [
    id 296
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 297
    label "utytu&#322;owany"
  ]
  node [
    id 298
    label "poz&#322;ocenie"
  ]
  node [
    id 299
    label "Polska"
  ]
  node [
    id 300
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 301
    label "wspania&#322;y"
  ]
  node [
    id 302
    label "doskona&#322;y"
  ]
  node [
    id 303
    label "kochany"
  ]
  node [
    id 304
    label "z&#322;ocenie"
  ]
  node [
    id 305
    label "miesi&#281;czny"
  ]
  node [
    id 306
    label "proceed"
  ]
  node [
    id 307
    label "catch"
  ]
  node [
    id 308
    label "pozosta&#263;"
  ]
  node [
    id 309
    label "osta&#263;_si&#281;"
  ]
  node [
    id 310
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 311
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 312
    label "change"
  ]
  node [
    id 313
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 314
    label "zap&#322;aci&#263;"
  ]
  node [
    id 315
    label "disburse"
  ]
  node [
    id 316
    label "wage"
  ]
  node [
    id 317
    label "wyda&#263;"
  ]
  node [
    id 318
    label "odwzajemni&#263;_si&#281;"
  ]
  node [
    id 319
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 320
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 321
    label "Barb&#243;rka"
  ]
  node [
    id 322
    label "Sylwester"
  ]
  node [
    id 323
    label "zdobycie"
  ]
  node [
    id 324
    label "akrobacja_lotnicza"
  ]
  node [
    id 325
    label "equalizer"
  ]
  node [
    id 326
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 327
    label "uporz&#261;dkowanie"
  ]
  node [
    id 328
    label "prosty"
  ]
  node [
    id 329
    label "ukszta&#322;towanie"
  ]
  node [
    id 330
    label "arrangement"
  ]
  node [
    id 331
    label "erecting"
  ]
  node [
    id 332
    label "wynagrodzenie"
  ]
  node [
    id 333
    label "alteration"
  ]
  node [
    id 334
    label "zjawisko"
  ]
  node [
    id 335
    label "zrobienie"
  ]
  node [
    id 336
    label "czynno&#347;&#263;"
  ]
  node [
    id 337
    label "zap&#322;ata"
  ]
  node [
    id 338
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 339
    label "ujednolicenie"
  ]
  node [
    id 340
    label "g&#322;adki"
  ]
  node [
    id 341
    label "grade"
  ]
  node [
    id 342
    label "reform"
  ]
  node [
    id 343
    label "refund"
  ]
  node [
    id 344
    label "formacja"
  ]
  node [
    id 345
    label "kronika"
  ]
  node [
    id 346
    label "czasopismo"
  ]
  node [
    id 347
    label "yearbook"
  ]
  node [
    id 348
    label "chwila"
  ]
  node [
    id 349
    label "uderzenie"
  ]
  node [
    id 350
    label "cios"
  ]
  node [
    id 351
    label "time"
  ]
  node [
    id 352
    label "przeciwny"
  ]
  node [
    id 353
    label "sw&#243;j"
  ]
  node [
    id 354
    label "odwrotnie"
  ]
  node [
    id 355
    label "dzie&#324;"
  ]
  node [
    id 356
    label "podobny"
  ]
  node [
    id 357
    label "wt&#243;ry"
  ]
  node [
    id 358
    label "ro&#347;lina_zielna"
  ]
  node [
    id 359
    label "go&#378;dzikowate"
  ]
  node [
    id 360
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 361
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 362
    label "Nowy_Rok"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 43
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 195
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 90
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 201
  ]
  edge [
    source 21
    target 202
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 48
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 43
  ]
  edge [
    source 25
    target 44
  ]
  edge [
    source 25
    target 49
  ]
  edge [
    source 25
    target 50
  ]
  edge [
    source 25
    target 64
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 41
  ]
  edge [
    source 25
    target 48
  ]
  edge [
    source 25
    target 45
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 49
  ]
  edge [
    source 27
    target 211
  ]
  edge [
    source 27
    target 212
  ]
  edge [
    source 27
    target 64
  ]
  edge [
    source 27
    target 213
  ]
  edge [
    source 27
    target 214
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 68
  ]
  edge [
    source 27
    target 216
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 221
  ]
  edge [
    source 30
    target 222
  ]
  edge [
    source 30
    target 223
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 30
    target 227
  ]
  edge [
    source 30
    target 228
  ]
  edge [
    source 30
    target 229
  ]
  edge [
    source 30
    target 230
  ]
  edge [
    source 30
    target 231
  ]
  edge [
    source 30
    target 232
  ]
  edge [
    source 30
    target 233
  ]
  edge [
    source 30
    target 234
  ]
  edge [
    source 30
    target 235
  ]
  edge [
    source 30
    target 236
  ]
  edge [
    source 30
    target 237
  ]
  edge [
    source 30
    target 238
  ]
  edge [
    source 30
    target 239
  ]
  edge [
    source 30
    target 240
  ]
  edge [
    source 30
    target 241
  ]
  edge [
    source 30
    target 242
  ]
  edge [
    source 30
    target 243
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 30
    target 245
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 247
  ]
  edge [
    source 30
    target 248
  ]
  edge [
    source 30
    target 249
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 258
  ]
  edge [
    source 31
    target 259
  ]
  edge [
    source 31
    target 260
  ]
  edge [
    source 31
    target 261
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 262
  ]
  edge [
    source 32
    target 263
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 264
  ]
  edge [
    source 33
    target 265
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 266
  ]
  edge [
    source 34
    target 267
  ]
  edge [
    source 34
    target 268
  ]
  edge [
    source 34
    target 269
  ]
  edge [
    source 34
    target 270
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 271
  ]
  edge [
    source 35
    target 272
  ]
  edge [
    source 35
    target 115
  ]
  edge [
    source 35
    target 273
  ]
  edge [
    source 35
    target 118
  ]
  edge [
    source 35
    target 274
  ]
  edge [
    source 35
    target 275
  ]
  edge [
    source 35
    target 276
  ]
  edge [
    source 35
    target 119
  ]
  edge [
    source 35
    target 120
  ]
  edge [
    source 35
    target 277
  ]
  edge [
    source 35
    target 278
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 279
  ]
  edge [
    source 36
    target 280
  ]
  edge [
    source 36
    target 281
  ]
  edge [
    source 36
    target 282
  ]
  edge [
    source 36
    target 283
  ]
  edge [
    source 36
    target 284
  ]
  edge [
    source 36
    target 285
  ]
  edge [
    source 36
    target 286
  ]
  edge [
    source 36
    target 287
  ]
  edge [
    source 36
    target 288
  ]
  edge [
    source 36
    target 289
  ]
  edge [
    source 36
    target 290
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 291
  ]
  edge [
    source 37
    target 292
  ]
  edge [
    source 37
    target 293
  ]
  edge [
    source 37
    target 294
  ]
  edge [
    source 37
    target 295
  ]
  edge [
    source 37
    target 296
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 37
    target 298
  ]
  edge [
    source 37
    target 299
  ]
  edge [
    source 37
    target 300
  ]
  edge [
    source 37
    target 301
  ]
  edge [
    source 37
    target 302
  ]
  edge [
    source 37
    target 303
  ]
  edge [
    source 37
    target 136
  ]
  edge [
    source 37
    target 304
  ]
  edge [
    source 38
    target 305
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 306
  ]
  edge [
    source 39
    target 307
  ]
  edge [
    source 39
    target 308
  ]
  edge [
    source 39
    target 309
  ]
  edge [
    source 39
    target 310
  ]
  edge [
    source 39
    target 311
  ]
  edge [
    source 39
    target 205
  ]
  edge [
    source 39
    target 312
  ]
  edge [
    source 39
    target 313
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 314
  ]
  edge [
    source 40
    target 315
  ]
  edge [
    source 40
    target 316
  ]
  edge [
    source 40
    target 317
  ]
  edge [
    source 40
    target 318
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 45
  ]
  edge [
    source 41
    target 319
  ]
  edge [
    source 41
    target 320
  ]
  edge [
    source 41
    target 321
  ]
  edge [
    source 41
    target 322
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 323
  ]
  edge [
    source 42
    target 324
  ]
  edge [
    source 42
    target 325
  ]
  edge [
    source 42
    target 326
  ]
  edge [
    source 42
    target 327
  ]
  edge [
    source 42
    target 328
  ]
  edge [
    source 42
    target 329
  ]
  edge [
    source 42
    target 330
  ]
  edge [
    source 42
    target 331
  ]
  edge [
    source 42
    target 332
  ]
  edge [
    source 42
    target 333
  ]
  edge [
    source 42
    target 334
  ]
  edge [
    source 42
    target 335
  ]
  edge [
    source 42
    target 336
  ]
  edge [
    source 42
    target 337
  ]
  edge [
    source 42
    target 338
  ]
  edge [
    source 42
    target 339
  ]
  edge [
    source 42
    target 340
  ]
  edge [
    source 42
    target 341
  ]
  edge [
    source 42
    target 342
  ]
  edge [
    source 42
    target 343
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 45
    target 344
  ]
  edge [
    source 45
    target 345
  ]
  edge [
    source 45
    target 346
  ]
  edge [
    source 45
    target 347
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 348
  ]
  edge [
    source 46
    target 349
  ]
  edge [
    source 46
    target 350
  ]
  edge [
    source 46
    target 351
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 47
    target 264
  ]
  edge [
    source 47
    target 206
  ]
  edge [
    source 47
    target 352
  ]
  edge [
    source 47
    target 353
  ]
  edge [
    source 47
    target 354
  ]
  edge [
    source 47
    target 355
  ]
  edge [
    source 47
    target 356
  ]
  edge [
    source 47
    target 357
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 358
  ]
  edge [
    source 48
    target 359
  ]
  edge [
    source 48
    target 360
  ]
  edge [
    source 48
    target 361
  ]
  edge [
    source 48
    target 48
  ]
  edge [
    source 50
    target 362
  ]
]
