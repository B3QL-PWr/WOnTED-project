graph [
  maxDegree 36
  minDegree 1
  meanDegree 2.3807228915662653
  density 0.005750538385425761
  graphCliqueNumber 5
  node [
    id 0
    label "podstawa"
    origin "text"
  ]
  node [
    id 1
    label "usta"
    origin "text"
  ]
  node [
    id 2
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 3
    label "minister"
    origin "text"
  ]
  node [
    id 4
    label "transport"
    origin "text"
  ]
  node [
    id 5
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 6
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 7
    label "rocznik"
    origin "text"
  ]
  node [
    id 8
    label "sprawa"
    origin "text"
  ]
  node [
    id 9
    label "wypadek"
    origin "text"
  ]
  node [
    id 10
    label "lotniczy"
    origin "text"
  ]
  node [
    id 11
    label "poz"
    origin "text"
  ]
  node [
    id 12
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 13
    label "zarz&#261;dzenie"
    origin "text"
  ]
  node [
    id 14
    label "prezes"
    origin "text"
  ]
  node [
    id 15
    label "urz&#261;d"
    origin "text"
  ]
  node [
    id 16
    label "lotnictwo"
    origin "text"
  ]
  node [
    id 17
    label "cywilny"
    origin "text"
  ]
  node [
    id 18
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 19
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 20
    label "klasyfikacja"
    origin "text"
  ]
  node [
    id 21
    label "grupa"
    origin "text"
  ]
  node [
    id 22
    label "przyczynowy"
    origin "text"
  ]
  node [
    id 23
    label "zdarzenie"
    origin "text"
  ]
  node [
    id 24
    label "dziennik"
    origin "text"
  ]
  node [
    id 25
    label "urz"
    origin "text"
  ]
  node [
    id 26
    label "ulc"
    origin "text"
  ]
  node [
    id 27
    label "og&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 28
    label "si&#281;"
    origin "text"
  ]
  node [
    id 29
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "powa&#380;ny"
    origin "text"
  ]
  node [
    id 31
    label "incydent"
    origin "text"
  ]
  node [
    id 32
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 33
    label "wydarzy&#263;"
    origin "text"
  ]
  node [
    id 34
    label "maj"
    origin "text"
  ]
  node [
    id 35
    label "spadochron"
    origin "text"
  ]
  node [
    id 36
    label "mars"
    origin "text"
  ]
  node [
    id 37
    label "uczenie"
    origin "text"
  ]
  node [
    id 38
    label "skoczek"
    origin "text"
  ]
  node [
    id 39
    label "spadochronowy"
    origin "text"
  ]
  node [
    id 40
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 41
    label "drugi"
    origin "text"
  ]
  node [
    id 42
    label "skok"
    origin "text"
  ]
  node [
    id 43
    label "klasyfikowa&#263;"
    origin "text"
  ]
  node [
    id 44
    label "kategoria"
    origin "text"
  ]
  node [
    id 45
    label "brak"
    origin "text"
  ]
  node [
    id 46
    label "kwalifikacja"
    origin "text"
  ]
  node [
    id 47
    label "czynnik"
    origin "text"
  ]
  node [
    id 48
    label "techniczny"
    origin "text"
  ]
  node [
    id 49
    label "b&#322;&#261;d"
    origin "text"
  ]
  node [
    id 50
    label "konstrukcyjny"
    origin "text"
  ]
  node [
    id 51
    label "produkcyjny"
    origin "text"
  ]
  node [
    id 52
    label "opis"
    origin "text"
  ]
  node [
    id 53
    label "okoliczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 54
    label "podstawowy"
  ]
  node [
    id 55
    label "strategia"
  ]
  node [
    id 56
    label "pot&#281;ga"
  ]
  node [
    id 57
    label "zasadzenie"
  ]
  node [
    id 58
    label "za&#322;o&#380;enie"
  ]
  node [
    id 59
    label "&#347;ciana"
  ]
  node [
    id 60
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 61
    label "przedmiot"
  ]
  node [
    id 62
    label "documentation"
  ]
  node [
    id 63
    label "dzieci&#281;ctwo"
  ]
  node [
    id 64
    label "pomys&#322;"
  ]
  node [
    id 65
    label "bok"
  ]
  node [
    id 66
    label "d&#243;&#322;"
  ]
  node [
    id 67
    label "punkt_odniesienia"
  ]
  node [
    id 68
    label "column"
  ]
  node [
    id 69
    label "zasadzi&#263;"
  ]
  node [
    id 70
    label "background"
  ]
  node [
    id 71
    label "warga_dolna"
  ]
  node [
    id 72
    label "ssa&#263;"
  ]
  node [
    id 73
    label "zaci&#261;&#263;"
  ]
  node [
    id 74
    label "ryjek"
  ]
  node [
    id 75
    label "twarz"
  ]
  node [
    id 76
    label "dzi&#243;b"
  ]
  node [
    id 77
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 78
    label "ssanie"
  ]
  node [
    id 79
    label "zaci&#281;cie"
  ]
  node [
    id 80
    label "jadaczka"
  ]
  node [
    id 81
    label "zacinanie"
  ]
  node [
    id 82
    label "organ"
  ]
  node [
    id 83
    label "jama_ustna"
  ]
  node [
    id 84
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 85
    label "warga_g&#243;rna"
  ]
  node [
    id 86
    label "zacina&#263;"
  ]
  node [
    id 87
    label "rule"
  ]
  node [
    id 88
    label "polecenie"
  ]
  node [
    id 89
    label "akt"
  ]
  node [
    id 90
    label "arrangement"
  ]
  node [
    id 91
    label "commission"
  ]
  node [
    id 92
    label "ordonans"
  ]
  node [
    id 93
    label "Goebbels"
  ]
  node [
    id 94
    label "Sto&#322;ypin"
  ]
  node [
    id 95
    label "rz&#261;d"
  ]
  node [
    id 96
    label "dostojnik"
  ]
  node [
    id 97
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 98
    label "zawarto&#347;&#263;"
  ]
  node [
    id 99
    label "unos"
  ]
  node [
    id 100
    label "traffic"
  ]
  node [
    id 101
    label "za&#322;adunek"
  ]
  node [
    id 102
    label "gospodarka"
  ]
  node [
    id 103
    label "roz&#322;adunek"
  ]
  node [
    id 104
    label "sprz&#281;t"
  ]
  node [
    id 105
    label "jednoszynowy"
  ]
  node [
    id 106
    label "cedu&#322;a"
  ]
  node [
    id 107
    label "tyfon"
  ]
  node [
    id 108
    label "us&#322;uga"
  ]
  node [
    id 109
    label "prze&#322;adunek"
  ]
  node [
    id 110
    label "komunikacja"
  ]
  node [
    id 111
    label "towar"
  ]
  node [
    id 112
    label "s&#322;o&#324;ce"
  ]
  node [
    id 113
    label "czynienie_si&#281;"
  ]
  node [
    id 114
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 115
    label "czas"
  ]
  node [
    id 116
    label "long_time"
  ]
  node [
    id 117
    label "przedpo&#322;udnie"
  ]
  node [
    id 118
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 119
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 120
    label "tydzie&#324;"
  ]
  node [
    id 121
    label "godzina"
  ]
  node [
    id 122
    label "t&#322;usty_czwartek"
  ]
  node [
    id 123
    label "wsta&#263;"
  ]
  node [
    id 124
    label "day"
  ]
  node [
    id 125
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 126
    label "przedwiecz&#243;r"
  ]
  node [
    id 127
    label "Sylwester"
  ]
  node [
    id 128
    label "po&#322;udnie"
  ]
  node [
    id 129
    label "wzej&#347;cie"
  ]
  node [
    id 130
    label "podwiecz&#243;r"
  ]
  node [
    id 131
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 132
    label "rano"
  ]
  node [
    id 133
    label "termin"
  ]
  node [
    id 134
    label "ranek"
  ]
  node [
    id 135
    label "doba"
  ]
  node [
    id 136
    label "wiecz&#243;r"
  ]
  node [
    id 137
    label "walentynki"
  ]
  node [
    id 138
    label "popo&#322;udnie"
  ]
  node [
    id 139
    label "noc"
  ]
  node [
    id 140
    label "wstanie"
  ]
  node [
    id 141
    label "Nowy_Rok"
  ]
  node [
    id 142
    label "miesi&#261;c"
  ]
  node [
    id 143
    label "formacja"
  ]
  node [
    id 144
    label "kronika"
  ]
  node [
    id 145
    label "czasopismo"
  ]
  node [
    id 146
    label "yearbook"
  ]
  node [
    id 147
    label "temat"
  ]
  node [
    id 148
    label "kognicja"
  ]
  node [
    id 149
    label "idea"
  ]
  node [
    id 150
    label "szczeg&#243;&#322;"
  ]
  node [
    id 151
    label "rzecz"
  ]
  node [
    id 152
    label "wydarzenie"
  ]
  node [
    id 153
    label "przes&#322;anka"
  ]
  node [
    id 154
    label "rozprawa"
  ]
  node [
    id 155
    label "object"
  ]
  node [
    id 156
    label "proposition"
  ]
  node [
    id 157
    label "czynno&#347;&#263;"
  ]
  node [
    id 158
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 159
    label "motyw"
  ]
  node [
    id 160
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 161
    label "fabu&#322;a"
  ]
  node [
    id 162
    label "przebiec"
  ]
  node [
    id 163
    label "happening"
  ]
  node [
    id 164
    label "przebiegni&#281;cie"
  ]
  node [
    id 165
    label "event"
  ]
  node [
    id 166
    label "charakter"
  ]
  node [
    id 167
    label "odwodnienie"
  ]
  node [
    id 168
    label "konstytucja"
  ]
  node [
    id 169
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 170
    label "substancja_chemiczna"
  ]
  node [
    id 171
    label "bratnia_dusza"
  ]
  node [
    id 172
    label "zwi&#261;zanie"
  ]
  node [
    id 173
    label "lokant"
  ]
  node [
    id 174
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 175
    label "zwi&#261;za&#263;"
  ]
  node [
    id 176
    label "organizacja"
  ]
  node [
    id 177
    label "odwadnia&#263;"
  ]
  node [
    id 178
    label "marriage"
  ]
  node [
    id 179
    label "marketing_afiliacyjny"
  ]
  node [
    id 180
    label "bearing"
  ]
  node [
    id 181
    label "wi&#261;zanie"
  ]
  node [
    id 182
    label "odwadnianie"
  ]
  node [
    id 183
    label "koligacja"
  ]
  node [
    id 184
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 185
    label "odwodni&#263;"
  ]
  node [
    id 186
    label "azeotrop"
  ]
  node [
    id 187
    label "powi&#261;zanie"
  ]
  node [
    id 188
    label "stipulation"
  ]
  node [
    id 189
    label "danie"
  ]
  node [
    id 190
    label "gruba_ryba"
  ]
  node [
    id 191
    label "zwierzchnik"
  ]
  node [
    id 192
    label "w&#322;adza"
  ]
  node [
    id 193
    label "instytucja"
  ]
  node [
    id 194
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 195
    label "mianowaniec"
  ]
  node [
    id 196
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 197
    label "stanowisko"
  ]
  node [
    id 198
    label "position"
  ]
  node [
    id 199
    label "dzia&#322;"
  ]
  node [
    id 200
    label "siedziba"
  ]
  node [
    id 201
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 202
    label "okienko"
  ]
  node [
    id 203
    label "konwojer"
  ]
  node [
    id 204
    label "skrzyd&#322;o"
  ]
  node [
    id 205
    label "awiacja"
  ]
  node [
    id 206
    label "skr&#281;tomierz"
  ]
  node [
    id 207
    label "nauka"
  ]
  node [
    id 208
    label "wojsko"
  ]
  node [
    id 209
    label "nawigacja"
  ]
  node [
    id 210
    label "balenit"
  ]
  node [
    id 211
    label "Si&#322;y_Powietrzne"
  ]
  node [
    id 212
    label "cywilnie"
  ]
  node [
    id 213
    label "nieoficjalny"
  ]
  node [
    id 214
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 215
    label "Barb&#243;rka"
  ]
  node [
    id 216
    label "rynek"
  ]
  node [
    id 217
    label "issue"
  ]
  node [
    id 218
    label "evocation"
  ]
  node [
    id 219
    label "wst&#281;p"
  ]
  node [
    id 220
    label "nuklearyzacja"
  ]
  node [
    id 221
    label "umo&#380;liwienie"
  ]
  node [
    id 222
    label "zacz&#281;cie"
  ]
  node [
    id 223
    label "wpisanie"
  ]
  node [
    id 224
    label "zapoznanie"
  ]
  node [
    id 225
    label "zrobienie"
  ]
  node [
    id 226
    label "entrance"
  ]
  node [
    id 227
    label "wej&#347;cie"
  ]
  node [
    id 228
    label "podstawy"
  ]
  node [
    id 229
    label "spowodowanie"
  ]
  node [
    id 230
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 231
    label "w&#322;&#261;czenie"
  ]
  node [
    id 232
    label "doprowadzenie"
  ]
  node [
    id 233
    label "przewietrzenie"
  ]
  node [
    id 234
    label "deduction"
  ]
  node [
    id 235
    label "umieszczenie"
  ]
  node [
    id 236
    label "wytw&#243;r"
  ]
  node [
    id 237
    label "uplasowa&#263;_si&#281;"
  ]
  node [
    id 238
    label "podzia&#322;"
  ]
  node [
    id 239
    label "competence"
  ]
  node [
    id 240
    label "stopie&#324;"
  ]
  node [
    id 241
    label "ocena"
  ]
  node [
    id 242
    label "kolejno&#347;&#263;"
  ]
  node [
    id 243
    label "plasowanie_si&#281;"
  ]
  node [
    id 244
    label "poj&#281;cie"
  ]
  node [
    id 245
    label "uplasowanie_si&#281;"
  ]
  node [
    id 246
    label "plasowa&#263;_si&#281;"
  ]
  node [
    id 247
    label "distribution"
  ]
  node [
    id 248
    label "division"
  ]
  node [
    id 249
    label "odm&#322;adza&#263;"
  ]
  node [
    id 250
    label "asymilowa&#263;"
  ]
  node [
    id 251
    label "cz&#261;steczka"
  ]
  node [
    id 252
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 253
    label "egzemplarz"
  ]
  node [
    id 254
    label "formacja_geologiczna"
  ]
  node [
    id 255
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 256
    label "harcerze_starsi"
  ]
  node [
    id 257
    label "liga"
  ]
  node [
    id 258
    label "Terranie"
  ]
  node [
    id 259
    label "&#346;wietliki"
  ]
  node [
    id 260
    label "pakiet_klimatyczny"
  ]
  node [
    id 261
    label "oddzia&#322;"
  ]
  node [
    id 262
    label "stage_set"
  ]
  node [
    id 263
    label "Entuzjastki"
  ]
  node [
    id 264
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 265
    label "odm&#322;odzenie"
  ]
  node [
    id 266
    label "type"
  ]
  node [
    id 267
    label "category"
  ]
  node [
    id 268
    label "asymilowanie"
  ]
  node [
    id 269
    label "specgrupa"
  ]
  node [
    id 270
    label "odm&#322;adzanie"
  ]
  node [
    id 271
    label "gromada"
  ]
  node [
    id 272
    label "Eurogrupa"
  ]
  node [
    id 273
    label "jednostka_systematyczna"
  ]
  node [
    id 274
    label "kompozycja"
  ]
  node [
    id 275
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 276
    label "zbi&#243;r"
  ]
  node [
    id 277
    label "przyczynowo"
  ]
  node [
    id 278
    label "spis"
  ]
  node [
    id 279
    label "sheet"
  ]
  node [
    id 280
    label "gazeta"
  ]
  node [
    id 281
    label "diariusz"
  ]
  node [
    id 282
    label "pami&#281;tnik"
  ]
  node [
    id 283
    label "journal"
  ]
  node [
    id 284
    label "ksi&#281;ga"
  ]
  node [
    id 285
    label "program_informacyjny"
  ]
  node [
    id 286
    label "announce"
  ]
  node [
    id 287
    label "publikowa&#263;"
  ]
  node [
    id 288
    label "obwo&#322;ywa&#263;"
  ]
  node [
    id 289
    label "post"
  ]
  node [
    id 290
    label "podawa&#263;"
  ]
  node [
    id 291
    label "mie&#263;_miejsce"
  ]
  node [
    id 292
    label "chance"
  ]
  node [
    id 293
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 294
    label "alternate"
  ]
  node [
    id 295
    label "naciska&#263;"
  ]
  node [
    id 296
    label "atakowa&#263;"
  ]
  node [
    id 297
    label "gro&#378;ny"
  ]
  node [
    id 298
    label "trudny"
  ]
  node [
    id 299
    label "du&#380;y"
  ]
  node [
    id 300
    label "spowa&#380;nienie"
  ]
  node [
    id 301
    label "prawdziwy"
  ]
  node [
    id 302
    label "powa&#380;nienie"
  ]
  node [
    id 303
    label "powa&#380;nie"
  ]
  node [
    id 304
    label "ci&#281;&#380;ko"
  ]
  node [
    id 305
    label "ci&#281;&#380;ki"
  ]
  node [
    id 306
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 307
    label "wyzwalacz"
  ]
  node [
    id 308
    label "czasza"
  ]
  node [
    id 309
    label "spadochronik_pilotowy"
  ]
  node [
    id 310
    label "sprz&#281;t_ratowniczy"
  ]
  node [
    id 311
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 312
    label "maszt"
  ]
  node [
    id 313
    label "mina"
  ]
  node [
    id 314
    label "platforma"
  ]
  node [
    id 315
    label "zapoznawanie"
  ]
  node [
    id 316
    label "teaching"
  ]
  node [
    id 317
    label "uczony"
  ]
  node [
    id 318
    label "pouczenie"
  ]
  node [
    id 319
    label "education"
  ]
  node [
    id 320
    label "rozwijanie"
  ]
  node [
    id 321
    label "training"
  ]
  node [
    id 322
    label "oddzia&#322;ywanie"
  ]
  node [
    id 323
    label "przyuczenie"
  ]
  node [
    id 324
    label "pracowanie"
  ]
  node [
    id 325
    label "pomaganie"
  ]
  node [
    id 326
    label "kliker"
  ]
  node [
    id 327
    label "przyuczanie"
  ]
  node [
    id 328
    label "o&#347;wiecanie"
  ]
  node [
    id 329
    label "m&#261;drze"
  ]
  node [
    id 330
    label "wychowywanie"
  ]
  node [
    id 331
    label "figura"
  ]
  node [
    id 332
    label "szara&#324;czowate"
  ]
  node [
    id 333
    label "szara&#324;czak"
  ]
  node [
    id 334
    label "sportowiec"
  ]
  node [
    id 335
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 336
    label "work"
  ]
  node [
    id 337
    label "robi&#263;"
  ]
  node [
    id 338
    label "muzyka"
  ]
  node [
    id 339
    label "rola"
  ]
  node [
    id 340
    label "create"
  ]
  node [
    id 341
    label "wytwarza&#263;"
  ]
  node [
    id 342
    label "praca"
  ]
  node [
    id 343
    label "cz&#322;owiek"
  ]
  node [
    id 344
    label "inny"
  ]
  node [
    id 345
    label "kolejny"
  ]
  node [
    id 346
    label "przeciwny"
  ]
  node [
    id 347
    label "sw&#243;j"
  ]
  node [
    id 348
    label "odwrotnie"
  ]
  node [
    id 349
    label "podobny"
  ]
  node [
    id 350
    label "wt&#243;ry"
  ]
  node [
    id 351
    label "wybicie"
  ]
  node [
    id 352
    label "konkurencja"
  ]
  node [
    id 353
    label "derail"
  ]
  node [
    id 354
    label "ptak"
  ]
  node [
    id 355
    label "ruch"
  ]
  node [
    id 356
    label "l&#261;dowanie"
  ]
  node [
    id 357
    label "&#322;apa"
  ]
  node [
    id 358
    label "struktura_anatomiczna"
  ]
  node [
    id 359
    label "stroke"
  ]
  node [
    id 360
    label "gimnastyka_przyrz&#261;dowa"
  ]
  node [
    id 361
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 362
    label "zmiana"
  ]
  node [
    id 363
    label "caper"
  ]
  node [
    id 364
    label "zaj&#261;c"
  ]
  node [
    id 365
    label "naskok"
  ]
  node [
    id 366
    label "napad"
  ]
  node [
    id 367
    label "ko&#347;&#263;_skokowa"
  ]
  node [
    id 368
    label "noga"
  ]
  node [
    id 369
    label "zalicza&#263;"
  ]
  node [
    id 370
    label "ocenia&#263;"
  ]
  node [
    id 371
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 372
    label "digest"
  ]
  node [
    id 373
    label "forma"
  ]
  node [
    id 374
    label "teoria"
  ]
  node [
    id 375
    label "klasa"
  ]
  node [
    id 376
    label "prywatywny"
  ]
  node [
    id 377
    label "defect"
  ]
  node [
    id 378
    label "odej&#347;cie"
  ]
  node [
    id 379
    label "gap"
  ]
  node [
    id 380
    label "kr&#243;tki"
  ]
  node [
    id 381
    label "wyr&#243;b"
  ]
  node [
    id 382
    label "nieistnienie"
  ]
  node [
    id 383
    label "wada"
  ]
  node [
    id 384
    label "odej&#347;&#263;"
  ]
  node [
    id 385
    label "odchodzenie"
  ]
  node [
    id 386
    label "odchodzi&#263;"
  ]
  node [
    id 387
    label "iloczyn"
  ]
  node [
    id 388
    label "ekspozycja"
  ]
  node [
    id 389
    label "agent"
  ]
  node [
    id 390
    label "divisor"
  ]
  node [
    id 391
    label "faktor"
  ]
  node [
    id 392
    label "specjalny"
  ]
  node [
    id 393
    label "nieznaczny"
  ]
  node [
    id 394
    label "suchy"
  ]
  node [
    id 395
    label "pozamerytoryczny"
  ]
  node [
    id 396
    label "ambitny"
  ]
  node [
    id 397
    label "technicznie"
  ]
  node [
    id 398
    label "baseball"
  ]
  node [
    id 399
    label "czyn"
  ]
  node [
    id 400
    label "error"
  ]
  node [
    id 401
    label "pomyli&#263;_si&#281;"
  ]
  node [
    id 402
    label "pomylenie_si&#281;"
  ]
  node [
    id 403
    label "mniemanie"
  ]
  node [
    id 404
    label "byk"
  ]
  node [
    id 405
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 406
    label "rezultat"
  ]
  node [
    id 407
    label "konstrukcyjnie"
  ]
  node [
    id 408
    label "produkcyjnie"
  ]
  node [
    id 409
    label "exposition"
  ]
  node [
    id 410
    label "wypowied&#378;"
  ]
  node [
    id 411
    label "obja&#347;nienie"
  ]
  node [
    id 412
    label "sk&#322;adnik"
  ]
  node [
    id 413
    label "warunki"
  ]
  node [
    id 414
    label "sytuacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 7
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 10
    target 31
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 27
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 197
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 203
  ]
  edge [
    source 16
    target 204
  ]
  edge [
    source 16
    target 205
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 207
  ]
  edge [
    source 16
    target 208
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 17
    target 212
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 223
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 232
  ]
  edge [
    source 19
    target 233
  ]
  edge [
    source 19
    target 234
  ]
  edge [
    source 19
    target 235
  ]
  edge [
    source 19
    target 31
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 236
  ]
  edge [
    source 20
    target 237
  ]
  edge [
    source 20
    target 238
  ]
  edge [
    source 20
    target 239
  ]
  edge [
    source 20
    target 240
  ]
  edge [
    source 20
    target 241
  ]
  edge [
    source 20
    target 242
  ]
  edge [
    source 20
    target 243
  ]
  edge [
    source 20
    target 244
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 44
  ]
  edge [
    source 21
    target 48
  ]
  edge [
    source 21
    target 249
  ]
  edge [
    source 21
    target 250
  ]
  edge [
    source 21
    target 251
  ]
  edge [
    source 21
    target 252
  ]
  edge [
    source 21
    target 253
  ]
  edge [
    source 21
    target 254
  ]
  edge [
    source 21
    target 255
  ]
  edge [
    source 21
    target 256
  ]
  edge [
    source 21
    target 257
  ]
  edge [
    source 21
    target 258
  ]
  edge [
    source 21
    target 259
  ]
  edge [
    source 21
    target 260
  ]
  edge [
    source 21
    target 261
  ]
  edge [
    source 21
    target 262
  ]
  edge [
    source 21
    target 263
  ]
  edge [
    source 21
    target 264
  ]
  edge [
    source 21
    target 265
  ]
  edge [
    source 21
    target 266
  ]
  edge [
    source 21
    target 267
  ]
  edge [
    source 21
    target 268
  ]
  edge [
    source 21
    target 269
  ]
  edge [
    source 21
    target 270
  ]
  edge [
    source 21
    target 271
  ]
  edge [
    source 21
    target 272
  ]
  edge [
    source 21
    target 273
  ]
  edge [
    source 21
    target 274
  ]
  edge [
    source 21
    target 275
  ]
  edge [
    source 21
    target 276
  ]
  edge [
    source 21
    target 37
  ]
  edge [
    source 21
    target 43
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 45
  ]
  edge [
    source 22
    target 49
  ]
  edge [
    source 22
    target 277
  ]
  edge [
    source 23
    target 158
  ]
  edge [
    source 23
    target 157
  ]
  edge [
    source 23
    target 159
  ]
  edge [
    source 23
    target 53
  ]
  edge [
    source 23
    target 161
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 164
  ]
  edge [
    source 23
    target 166
  ]
  edge [
    source 23
    target 51
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 278
  ]
  edge [
    source 24
    target 279
  ]
  edge [
    source 24
    target 280
  ]
  edge [
    source 24
    target 281
  ]
  edge [
    source 24
    target 282
  ]
  edge [
    source 24
    target 283
  ]
  edge [
    source 24
    target 284
  ]
  edge [
    source 24
    target 285
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 286
  ]
  edge [
    source 27
    target 287
  ]
  edge [
    source 27
    target 288
  ]
  edge [
    source 27
    target 289
  ]
  edge [
    source 27
    target 290
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 291
  ]
  edge [
    source 29
    target 292
  ]
  edge [
    source 29
    target 293
  ]
  edge [
    source 29
    target 294
  ]
  edge [
    source 29
    target 295
  ]
  edge [
    source 29
    target 296
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 53
  ]
  edge [
    source 30
    target 297
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 31
    target 165
  ]
  edge [
    source 31
    target 152
  ]
  edge [
    source 31
    target 160
  ]
  edge [
    source 31
    target 39
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 34
    target 306
  ]
  edge [
    source 34
    target 142
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 307
  ]
  edge [
    source 35
    target 308
  ]
  edge [
    source 35
    target 309
  ]
  edge [
    source 35
    target 310
  ]
  edge [
    source 35
    target 311
  ]
  edge [
    source 36
    target 312
  ]
  edge [
    source 36
    target 313
  ]
  edge [
    source 36
    target 314
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 37
    target 317
  ]
  edge [
    source 37
    target 318
  ]
  edge [
    source 37
    target 319
  ]
  edge [
    source 37
    target 320
  ]
  edge [
    source 37
    target 321
  ]
  edge [
    source 37
    target 322
  ]
  edge [
    source 37
    target 323
  ]
  edge [
    source 37
    target 324
  ]
  edge [
    source 37
    target 325
  ]
  edge [
    source 37
    target 326
  ]
  edge [
    source 37
    target 327
  ]
  edge [
    source 37
    target 328
  ]
  edge [
    source 37
    target 329
  ]
  edge [
    source 37
    target 330
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 331
  ]
  edge [
    source 38
    target 332
  ]
  edge [
    source 38
    target 333
  ]
  edge [
    source 38
    target 334
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 39
    target 43
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 335
  ]
  edge [
    source 40
    target 336
  ]
  edge [
    source 40
    target 337
  ]
  edge [
    source 40
    target 338
  ]
  edge [
    source 40
    target 339
  ]
  edge [
    source 40
    target 340
  ]
  edge [
    source 40
    target 341
  ]
  edge [
    source 40
    target 342
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 343
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 345
  ]
  edge [
    source 41
    target 346
  ]
  edge [
    source 41
    target 347
  ]
  edge [
    source 41
    target 348
  ]
  edge [
    source 41
    target 349
  ]
  edge [
    source 41
    target 350
  ]
  edge [
    source 42
    target 351
  ]
  edge [
    source 42
    target 352
  ]
  edge [
    source 42
    target 353
  ]
  edge [
    source 42
    target 354
  ]
  edge [
    source 42
    target 355
  ]
  edge [
    source 42
    target 356
  ]
  edge [
    source 42
    target 357
  ]
  edge [
    source 42
    target 358
  ]
  edge [
    source 42
    target 359
  ]
  edge [
    source 42
    target 360
  ]
  edge [
    source 42
    target 361
  ]
  edge [
    source 42
    target 362
  ]
  edge [
    source 42
    target 363
  ]
  edge [
    source 42
    target 364
  ]
  edge [
    source 42
    target 365
  ]
  edge [
    source 42
    target 366
  ]
  edge [
    source 42
    target 367
  ]
  edge [
    source 42
    target 368
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 43
    target 369
  ]
  edge [
    source 43
    target 370
  ]
  edge [
    source 43
    target 371
  ]
  edge [
    source 43
    target 372
  ]
  edge [
    source 43
    target 43
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 44
    target 373
  ]
  edge [
    source 44
    target 236
  ]
  edge [
    source 44
    target 266
  ]
  edge [
    source 44
    target 374
  ]
  edge [
    source 44
    target 276
  ]
  edge [
    source 44
    target 244
  ]
  edge [
    source 44
    target 375
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 376
  ]
  edge [
    source 45
    target 377
  ]
  edge [
    source 45
    target 378
  ]
  edge [
    source 45
    target 379
  ]
  edge [
    source 45
    target 380
  ]
  edge [
    source 45
    target 381
  ]
  edge [
    source 45
    target 382
  ]
  edge [
    source 45
    target 383
  ]
  edge [
    source 45
    target 384
  ]
  edge [
    source 45
    target 385
  ]
  edge [
    source 45
    target 386
  ]
  edge [
    source 46
    target 236
  ]
  edge [
    source 46
    target 238
  ]
  edge [
    source 46
    target 239
  ]
  edge [
    source 46
    target 240
  ]
  edge [
    source 46
    target 241
  ]
  edge [
    source 46
    target 247
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 387
  ]
  edge [
    source 47
    target 60
  ]
  edge [
    source 47
    target 53
  ]
  edge [
    source 47
    target 388
  ]
  edge [
    source 47
    target 389
  ]
  edge [
    source 47
    target 390
  ]
  edge [
    source 47
    target 391
  ]
  edge [
    source 48
    target 392
  ]
  edge [
    source 48
    target 393
  ]
  edge [
    source 48
    target 394
  ]
  edge [
    source 48
    target 395
  ]
  edge [
    source 48
    target 396
  ]
  edge [
    source 48
    target 397
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 398
  ]
  edge [
    source 49
    target 399
  ]
  edge [
    source 49
    target 400
  ]
  edge [
    source 49
    target 401
  ]
  edge [
    source 49
    target 402
  ]
  edge [
    source 49
    target 403
  ]
  edge [
    source 49
    target 404
  ]
  edge [
    source 49
    target 405
  ]
  edge [
    source 49
    target 406
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 407
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 408
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 409
  ]
  edge [
    source 52
    target 157
  ]
  edge [
    source 52
    target 410
  ]
  edge [
    source 52
    target 411
  ]
  edge [
    source 53
    target 152
  ]
  edge [
    source 53
    target 412
  ]
  edge [
    source 53
    target 413
  ]
  edge [
    source 53
    target 414
  ]
]
