graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "rozdajo"
    origin "text"
  ]
  node [
    id 1
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 2
    label "daleki"
    origin "text"
  ]
  node [
    id 3
    label "si&#322;a"
  ]
  node [
    id 4
    label "stan"
  ]
  node [
    id 5
    label "lina"
  ]
  node [
    id 6
    label "way"
  ]
  node [
    id 7
    label "cable"
  ]
  node [
    id 8
    label "przebieg"
  ]
  node [
    id 9
    label "zbi&#243;r"
  ]
  node [
    id 10
    label "ch&#243;d"
  ]
  node [
    id 11
    label "trasa"
  ]
  node [
    id 12
    label "rz&#261;d"
  ]
  node [
    id 13
    label "k&#322;us"
  ]
  node [
    id 14
    label "progression"
  ]
  node [
    id 15
    label "current"
  ]
  node [
    id 16
    label "pr&#261;d"
  ]
  node [
    id 17
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "lot"
  ]
  node [
    id 20
    label "dawny"
  ]
  node [
    id 21
    label "du&#380;y"
  ]
  node [
    id 22
    label "s&#322;aby"
  ]
  node [
    id 23
    label "oddalony"
  ]
  node [
    id 24
    label "daleko"
  ]
  node [
    id 25
    label "przysz&#322;y"
  ]
  node [
    id 26
    label "ogl&#281;dny"
  ]
  node [
    id 27
    label "r&#243;&#380;ny"
  ]
  node [
    id 28
    label "g&#322;&#281;boki"
  ]
  node [
    id 29
    label "odlegle"
  ]
  node [
    id 30
    label "nieobecny"
  ]
  node [
    id 31
    label "odleg&#322;y"
  ]
  node [
    id 32
    label "d&#322;ugi"
  ]
  node [
    id 33
    label "zwi&#261;zany"
  ]
  node [
    id 34
    label "obcy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
]
