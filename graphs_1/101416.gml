graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.5384615384615385
  density 0.1282051282051282
  graphCliqueNumber 3
  node [
    id 0
    label "joshua"
    origin "text"
  ]
  node [
    id 1
    label "van"
    origin "text"
  ]
  node [
    id 2
    label "sant"
    origin "text"
  ]
  node [
    id 3
    label "samoch&#243;d"
  ]
  node [
    id 4
    label "nadwozie"
  ]
  node [
    id 5
    label "Joshua"
  ]
  node [
    id 6
    label "Sant"
  ]
  node [
    id 7
    label "partia"
  ]
  node [
    id 8
    label "demokratyczny"
  ]
  node [
    id 9
    label "izba"
  ]
  node [
    id 10
    label "reprezentant"
  ]
  node [
    id 11
    label "stanowi&#263;"
  ]
  node [
    id 12
    label "zjednoczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
]
