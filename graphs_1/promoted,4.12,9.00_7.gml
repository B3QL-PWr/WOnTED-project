graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "pocisk"
    origin "text"
  ]
  node [
    id 1
    label "miota&#263;"
    origin "text"
  ]
  node [
    id 2
    label "przez"
    origin "text"
  ]
  node [
    id 3
    label "katapulta"
    origin "text"
  ]
  node [
    id 4
    label "trebusze"
    origin "text"
  ]
  node [
    id 5
    label "g&#322;owica"
  ]
  node [
    id 6
    label "kulka"
  ]
  node [
    id 7
    label "amunicja"
  ]
  node [
    id 8
    label "przenie&#347;&#263;"
  ]
  node [
    id 9
    label "trafianie"
  ]
  node [
    id 10
    label "przenosi&#263;"
  ]
  node [
    id 11
    label "trafia&#263;"
  ]
  node [
    id 12
    label "trafienie"
  ]
  node [
    id 13
    label "przeniesienie"
  ]
  node [
    id 14
    label "&#322;adunek_bojowy"
  ]
  node [
    id 15
    label "trafi&#263;"
  ]
  node [
    id 16
    label "bro&#324;"
  ]
  node [
    id 17
    label "prochownia"
  ]
  node [
    id 18
    label "rdze&#324;"
  ]
  node [
    id 19
    label "przenoszenie"
  ]
  node [
    id 20
    label "tug"
  ]
  node [
    id 21
    label "syga&#263;"
  ]
  node [
    id 22
    label "zmienia&#263;"
  ]
  node [
    id 23
    label "grzmoci&#263;"
  ]
  node [
    id 24
    label "samolot"
  ]
  node [
    id 25
    label "machina_miotaj&#261;ca"
  ]
  node [
    id 26
    label "urz&#261;dzenie"
  ]
  node [
    id 27
    label "artyleria_przedogniowa"
  ]
  node [
    id 28
    label "machina_obl&#281;&#380;nicza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
]
