graph [
  maxDegree 11
  minDegree 1
  meanDegree 2.3703703703703702
  density 0.09116809116809117
  graphCliqueNumber 5
  node [
    id 0
    label "prosze"
    origin "text"
  ]
  node [
    id 1
    label "wyplusowanie"
    origin "text"
  ]
  node [
    id 2
    label "wpis"
    origin "text"
  ]
  node [
    id 3
    label "gorace"
    origin "text"
  ]
  node [
    id 4
    label "slusznej"
    origin "text"
  ]
  node [
    id 5
    label "sprawa"
    origin "text"
  ]
  node [
    id 6
    label "czynno&#347;&#263;"
  ]
  node [
    id 7
    label "entrance"
  ]
  node [
    id 8
    label "inscription"
  ]
  node [
    id 9
    label "akt"
  ]
  node [
    id 10
    label "op&#322;ata"
  ]
  node [
    id 11
    label "tekst"
  ]
  node [
    id 12
    label "temat"
  ]
  node [
    id 13
    label "kognicja"
  ]
  node [
    id 14
    label "idea"
  ]
  node [
    id 15
    label "szczeg&#243;&#322;"
  ]
  node [
    id 16
    label "rzecz"
  ]
  node [
    id 17
    label "wydarzenie"
  ]
  node [
    id 18
    label "przes&#322;anka"
  ]
  node [
    id 19
    label "rozprawa"
  ]
  node [
    id 20
    label "object"
  ]
  node [
    id 21
    label "proposition"
  ]
  node [
    id 22
    label "obw&#243;d"
  ]
  node [
    id 23
    label "w"
  ]
  node [
    id 24
    label "biceps"
  ]
  node [
    id 25
    label "32"
  ]
  node [
    id 26
    label "5cm"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 22
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 25
    target 26
  ]
]
