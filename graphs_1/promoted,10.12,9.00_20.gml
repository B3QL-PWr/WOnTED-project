graph [
  maxDegree 7
  minDegree 1
  meanDegree 2.054054054054054
  density 0.057057057057057055
  graphCliqueNumber 3
  node [
    id 0
    label "proca"
    origin "text"
  ]
  node [
    id 1
    label "wegetarianin"
    origin "text"
  ]
  node [
    id 2
    label "weganin"
    origin "text"
  ]
  node [
    id 3
    label "przyzna&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "pijak"
    origin "text"
  ]
  node [
    id 6
    label "zdarza&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zje&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "co&#347;"
    origin "text"
  ]
  node [
    id 9
    label "mi&#281;sny"
    origin "text"
  ]
  node [
    id 10
    label "bro&#324;_miotaj&#261;ca"
  ]
  node [
    id 11
    label "zabawka"
  ]
  node [
    id 12
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 13
    label "bro&#324;"
  ]
  node [
    id 14
    label "catapult"
  ]
  node [
    id 15
    label "urwis"
  ]
  node [
    id 16
    label "zjadacz"
  ]
  node [
    id 17
    label "da&#263;"
  ]
  node [
    id 18
    label "nada&#263;"
  ]
  node [
    id 19
    label "pozwoli&#263;"
  ]
  node [
    id 20
    label "give"
  ]
  node [
    id 21
    label "stwierdzi&#263;"
  ]
  node [
    id 22
    label "bibu&#322;a"
  ]
  node [
    id 23
    label "pij&#261;cy"
  ]
  node [
    id 24
    label "na&#322;ogowiec"
  ]
  node [
    id 25
    label "ciasto"
  ]
  node [
    id 26
    label "ze&#380;re&#263;"
  ]
  node [
    id 27
    label "spo&#380;y&#263;"
  ]
  node [
    id 28
    label "zrobi&#263;"
  ]
  node [
    id 29
    label "absorb"
  ]
  node [
    id 30
    label "thing"
  ]
  node [
    id 31
    label "cosik"
  ]
  node [
    id 32
    label "specjalny"
  ]
  node [
    id 33
    label "bia&#322;kowy"
  ]
  node [
    id 34
    label "sklep"
  ]
  node [
    id 35
    label "naturalny"
  ]
  node [
    id 36
    label "hodowlany"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 36
  ]
]
