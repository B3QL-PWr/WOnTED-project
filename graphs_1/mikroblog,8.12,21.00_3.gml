graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "kur"
    origin "text"
  ]
  node [
    id 1
    label "maca"
    origin "text"
  ]
  node [
    id 2
    label "zapia&#263;"
  ]
  node [
    id 3
    label "r&#243;&#380;yczka"
  ]
  node [
    id 4
    label "zapianie"
  ]
  node [
    id 5
    label "samiec"
  ]
  node [
    id 6
    label "kura"
  ]
  node [
    id 7
    label "pia&#263;"
  ]
  node [
    id 8
    label "pianie"
  ]
  node [
    id 9
    label "m&#261;czny"
  ]
  node [
    id 10
    label "pieprzyca"
  ]
  node [
    id 11
    label "korzec"
  ]
  node [
    id 12
    label "surowiec"
  ]
  node [
    id 13
    label "jednostka_obj&#281;to&#347;ci_cia&#322;_sypkich"
  ]
  node [
    id 14
    label "maka"
  ]
  node [
    id 15
    label "pieczywo_chrupkie"
  ]
  node [
    id 16
    label "korze&#324;"
  ]
  node [
    id 17
    label "placek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
]
