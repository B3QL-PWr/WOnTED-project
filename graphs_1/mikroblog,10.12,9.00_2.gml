graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.094915254237288
  density 0.0071255620892424765
  graphCliqueNumber 3
  node [
    id 0
    label "pies"
    origin "text"
  ]
  node [
    id 1
    label "zwierzeta"
    origin "text"
  ]
  node [
    id 2
    label "cytowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "informacja"
    origin "text"
  ]
  node [
    id 4
    label "oficjalny"
    origin "text"
  ]
  node [
    id 5
    label "doniesienie"
    origin "text"
  ]
  node [
    id 6
    label "bie&#380;&#261;co"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "niezmiernie"
    origin "text"
  ]
  node [
    id 9
    label "przykro"
    origin "text"
  ]
  node [
    id 10
    label "skutek"
    origin "text"
  ]
  node [
    id 11
    label "szarpanina"
    origin "text"
  ]
  node [
    id 12
    label "dziecko"
    origin "text"
  ]
  node [
    id 13
    label "ojciec"
    origin "text"
  ]
  node [
    id 14
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 16
    label "szok"
    origin "text"
  ]
  node [
    id 17
    label "ratowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "c&#243;rka"
    origin "text"
  ]
  node [
    id 19
    label "syn"
    origin "text"
  ]
  node [
    id 20
    label "uderzy&#263;"
    origin "text"
  ]
  node [
    id 21
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 22
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 23
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 24
    label "butelka"
    origin "text"
  ]
  node [
    id 25
    label "piwo"
    origin "text"
  ]
  node [
    id 26
    label "cz&#322;owiek"
  ]
  node [
    id 27
    label "wy&#263;"
  ]
  node [
    id 28
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 29
    label "spragniony"
  ]
  node [
    id 30
    label "rakarz"
  ]
  node [
    id 31
    label "psowate"
  ]
  node [
    id 32
    label "istota_&#380;ywa"
  ]
  node [
    id 33
    label "kabanos"
  ]
  node [
    id 34
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 35
    label "&#322;ajdak"
  ]
  node [
    id 36
    label "czworon&#243;g"
  ]
  node [
    id 37
    label "policjant"
  ]
  node [
    id 38
    label "szczucie"
  ]
  node [
    id 39
    label "s&#322;u&#380;enie"
  ]
  node [
    id 40
    label "sobaka"
  ]
  node [
    id 41
    label "dogoterapia"
  ]
  node [
    id 42
    label "Cerber"
  ]
  node [
    id 43
    label "wyzwisko"
  ]
  node [
    id 44
    label "szczu&#263;"
  ]
  node [
    id 45
    label "wycie"
  ]
  node [
    id 46
    label "szczeka&#263;"
  ]
  node [
    id 47
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 48
    label "trufla"
  ]
  node [
    id 49
    label "samiec"
  ]
  node [
    id 50
    label "piese&#322;"
  ]
  node [
    id 51
    label "zawy&#263;"
  ]
  node [
    id 52
    label "wymienia&#263;"
  ]
  node [
    id 53
    label "quote"
  ]
  node [
    id 54
    label "przytacza&#263;"
  ]
  node [
    id 55
    label "doj&#347;cie"
  ]
  node [
    id 56
    label "doj&#347;&#263;"
  ]
  node [
    id 57
    label "powzi&#261;&#263;"
  ]
  node [
    id 58
    label "wiedza"
  ]
  node [
    id 59
    label "sygna&#322;"
  ]
  node [
    id 60
    label "obiegni&#281;cie"
  ]
  node [
    id 61
    label "obieganie"
  ]
  node [
    id 62
    label "obiec"
  ]
  node [
    id 63
    label "dane"
  ]
  node [
    id 64
    label "obiega&#263;"
  ]
  node [
    id 65
    label "punkt"
  ]
  node [
    id 66
    label "publikacja"
  ]
  node [
    id 67
    label "powzi&#281;cie"
  ]
  node [
    id 68
    label "jawny"
  ]
  node [
    id 69
    label "oficjalnie"
  ]
  node [
    id 70
    label "legalny"
  ]
  node [
    id 71
    label "sformalizowanie"
  ]
  node [
    id 72
    label "formalizowanie"
  ]
  node [
    id 73
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 74
    label "formalnie"
  ]
  node [
    id 75
    label "naznoszenie"
  ]
  node [
    id 76
    label "poinformowanie"
  ]
  node [
    id 77
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 78
    label "announcement"
  ]
  node [
    id 79
    label "fetch"
  ]
  node [
    id 80
    label "do&#322;&#261;czenie"
  ]
  node [
    id 81
    label "zniesienie"
  ]
  node [
    id 82
    label "zaniesienie"
  ]
  node [
    id 83
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 84
    label "message"
  ]
  node [
    id 85
    label "zawiadomienie"
  ]
  node [
    id 86
    label "ci&#261;gle"
  ]
  node [
    id 87
    label "aktualnie"
  ]
  node [
    id 88
    label "bie&#380;&#261;cy"
  ]
  node [
    id 89
    label "si&#281;ga&#263;"
  ]
  node [
    id 90
    label "trwa&#263;"
  ]
  node [
    id 91
    label "obecno&#347;&#263;"
  ]
  node [
    id 92
    label "stan"
  ]
  node [
    id 93
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 94
    label "stand"
  ]
  node [
    id 95
    label "mie&#263;_miejsce"
  ]
  node [
    id 96
    label "uczestniczy&#263;"
  ]
  node [
    id 97
    label "chodzi&#263;"
  ]
  node [
    id 98
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 99
    label "equal"
  ]
  node [
    id 100
    label "szczytnie"
  ]
  node [
    id 101
    label "niezmierny"
  ]
  node [
    id 102
    label "ogromnie"
  ]
  node [
    id 103
    label "nieprzyjemnie"
  ]
  node [
    id 104
    label "niepo&#380;&#261;danie"
  ]
  node [
    id 105
    label "przykry"
  ]
  node [
    id 106
    label "rezultat"
  ]
  node [
    id 107
    label "poszarpanie"
  ]
  node [
    id 108
    label "zaj&#347;cie"
  ]
  node [
    id 109
    label "problem"
  ]
  node [
    id 110
    label "wyszarpywanie"
  ]
  node [
    id 111
    label "targanina"
  ]
  node [
    id 112
    label "zmagania"
  ]
  node [
    id 113
    label "bust"
  ]
  node [
    id 114
    label "struggle"
  ]
  node [
    id 115
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 116
    label "sytuacja"
  ]
  node [
    id 117
    label "scramble"
  ]
  node [
    id 118
    label "wyszarpanie"
  ]
  node [
    id 119
    label "potomstwo"
  ]
  node [
    id 120
    label "organizm"
  ]
  node [
    id 121
    label "sraluch"
  ]
  node [
    id 122
    label "utulanie"
  ]
  node [
    id 123
    label "pediatra"
  ]
  node [
    id 124
    label "dzieciarnia"
  ]
  node [
    id 125
    label "m&#322;odziak"
  ]
  node [
    id 126
    label "dzieciak"
  ]
  node [
    id 127
    label "utula&#263;"
  ]
  node [
    id 128
    label "potomek"
  ]
  node [
    id 129
    label "pedofil"
  ]
  node [
    id 130
    label "entliczek-pentliczek"
  ]
  node [
    id 131
    label "m&#322;odzik"
  ]
  node [
    id 132
    label "cz&#322;owieczek"
  ]
  node [
    id 133
    label "zwierz&#281;"
  ]
  node [
    id 134
    label "niepe&#322;noletni"
  ]
  node [
    id 135
    label "fledgling"
  ]
  node [
    id 136
    label "utuli&#263;"
  ]
  node [
    id 137
    label "utulenie"
  ]
  node [
    id 138
    label "pomys&#322;odawca"
  ]
  node [
    id 139
    label "kszta&#322;ciciel"
  ]
  node [
    id 140
    label "tworzyciel"
  ]
  node [
    id 141
    label "ojczym"
  ]
  node [
    id 142
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 143
    label "stary"
  ]
  node [
    id 144
    label "papa"
  ]
  node [
    id 145
    label "&#347;w"
  ]
  node [
    id 146
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 147
    label "zakonnik"
  ]
  node [
    id 148
    label "kuwada"
  ]
  node [
    id 149
    label "przodek"
  ]
  node [
    id 150
    label "wykonawca"
  ]
  node [
    id 151
    label "rodzice"
  ]
  node [
    id 152
    label "rodzic"
  ]
  node [
    id 153
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 154
    label "feel"
  ]
  node [
    id 155
    label "przedstawienie"
  ]
  node [
    id 156
    label "try"
  ]
  node [
    id 157
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 158
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 159
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 160
    label "sprawdza&#263;"
  ]
  node [
    id 161
    label "stara&#263;_si&#281;"
  ]
  node [
    id 162
    label "kosztowa&#263;"
  ]
  node [
    id 163
    label "shock_absorber"
  ]
  node [
    id 164
    label "wstrz&#261;s"
  ]
  node [
    id 165
    label "zaskoczenie"
  ]
  node [
    id 166
    label "prze&#380;ycie"
  ]
  node [
    id 167
    label "zapobiega&#263;"
  ]
  node [
    id 168
    label "oddala&#263;"
  ]
  node [
    id 169
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 170
    label "wybawia&#263;"
  ]
  node [
    id 171
    label "stanowi&#263;"
  ]
  node [
    id 172
    label "deliver"
  ]
  node [
    id 173
    label "dziewka"
  ]
  node [
    id 174
    label "potomkini"
  ]
  node [
    id 175
    label "dziewoja"
  ]
  node [
    id 176
    label "siksa"
  ]
  node [
    id 177
    label "dziewczynina"
  ]
  node [
    id 178
    label "dziunia"
  ]
  node [
    id 179
    label "dziewcz&#281;"
  ]
  node [
    id 180
    label "kora"
  ]
  node [
    id 181
    label "m&#322;&#243;dka"
  ]
  node [
    id 182
    label "dziecina"
  ]
  node [
    id 183
    label "sikorka"
  ]
  node [
    id 184
    label "usynowienie"
  ]
  node [
    id 185
    label "usynawianie"
  ]
  node [
    id 186
    label "skrytykowa&#263;"
  ]
  node [
    id 187
    label "lumber"
  ]
  node [
    id 188
    label "strike"
  ]
  node [
    id 189
    label "nast&#261;pi&#263;"
  ]
  node [
    id 190
    label "postara&#263;_si&#281;"
  ]
  node [
    id 191
    label "dotkn&#261;&#263;"
  ]
  node [
    id 192
    label "uderzy&#263;_do_panny"
  ]
  node [
    id 193
    label "zada&#263;"
  ]
  node [
    id 194
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 195
    label "hopn&#261;&#263;"
  ]
  node [
    id 196
    label "fall"
  ]
  node [
    id 197
    label "transgress"
  ]
  node [
    id 198
    label "uda&#263;_si&#281;"
  ]
  node [
    id 199
    label "sztachn&#261;&#263;"
  ]
  node [
    id 200
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 201
    label "jebn&#261;&#263;"
  ]
  node [
    id 202
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 203
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 204
    label "zrobi&#263;"
  ]
  node [
    id 205
    label "anoint"
  ]
  node [
    id 206
    label "przywali&#263;"
  ]
  node [
    id 207
    label "wstrzeli&#263;_si&#281;"
  ]
  node [
    id 208
    label "urazi&#263;"
  ]
  node [
    id 209
    label "rap"
  ]
  node [
    id 210
    label "spowodowa&#263;"
  ]
  node [
    id 211
    label "dupn&#261;&#263;"
  ]
  node [
    id 212
    label "wystartowa&#263;"
  ]
  node [
    id 213
    label "chop"
  ]
  node [
    id 214
    label "crush"
  ]
  node [
    id 215
    label "du&#380;y"
  ]
  node [
    id 216
    label "jedyny"
  ]
  node [
    id 217
    label "kompletny"
  ]
  node [
    id 218
    label "zdr&#243;w"
  ]
  node [
    id 219
    label "&#380;ywy"
  ]
  node [
    id 220
    label "ca&#322;o"
  ]
  node [
    id 221
    label "pe&#322;ny"
  ]
  node [
    id 222
    label "calu&#347;ko"
  ]
  node [
    id 223
    label "podobny"
  ]
  node [
    id 224
    label "wojsko"
  ]
  node [
    id 225
    label "magnitude"
  ]
  node [
    id 226
    label "energia"
  ]
  node [
    id 227
    label "capacity"
  ]
  node [
    id 228
    label "wuchta"
  ]
  node [
    id 229
    label "cecha"
  ]
  node [
    id 230
    label "parametr"
  ]
  node [
    id 231
    label "moment_si&#322;y"
  ]
  node [
    id 232
    label "przemoc"
  ]
  node [
    id 233
    label "zdolno&#347;&#263;"
  ]
  node [
    id 234
    label "mn&#243;stwo"
  ]
  node [
    id 235
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 236
    label "rozwi&#261;zanie"
  ]
  node [
    id 237
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 238
    label "potencja"
  ]
  node [
    id 239
    label "zjawisko"
  ]
  node [
    id 240
    label "zaleta"
  ]
  node [
    id 241
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 242
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 243
    label "ucho"
  ]
  node [
    id 244
    label "makrocefalia"
  ]
  node [
    id 245
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 246
    label "m&#243;zg"
  ]
  node [
    id 247
    label "kierownictwo"
  ]
  node [
    id 248
    label "czaszka"
  ]
  node [
    id 249
    label "dekiel"
  ]
  node [
    id 250
    label "umys&#322;"
  ]
  node [
    id 251
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 252
    label "&#347;ci&#281;cie"
  ]
  node [
    id 253
    label "sztuka"
  ]
  node [
    id 254
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 255
    label "g&#243;ra"
  ]
  node [
    id 256
    label "byd&#322;o"
  ]
  node [
    id 257
    label "alkohol"
  ]
  node [
    id 258
    label "ro&#347;lina"
  ]
  node [
    id 259
    label "&#347;ci&#281;gno"
  ]
  node [
    id 260
    label "&#380;ycie"
  ]
  node [
    id 261
    label "pryncypa&#322;"
  ]
  node [
    id 262
    label "fryzura"
  ]
  node [
    id 263
    label "noosfera"
  ]
  node [
    id 264
    label "kierowa&#263;"
  ]
  node [
    id 265
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 266
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 267
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 268
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 269
    label "kszta&#322;t"
  ]
  node [
    id 270
    label "cz&#322;onek"
  ]
  node [
    id 271
    label "cia&#322;o"
  ]
  node [
    id 272
    label "obiekt"
  ]
  node [
    id 273
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 274
    label "zawarto&#347;&#263;"
  ]
  node [
    id 275
    label "glass"
  ]
  node [
    id 276
    label "naczynie"
  ]
  node [
    id 277
    label "niemowl&#281;"
  ]
  node [
    id 278
    label "zabawa"
  ]
  node [
    id 279
    label "korkownica"
  ]
  node [
    id 280
    label "szyjka"
  ]
  node [
    id 281
    label "browarnia"
  ]
  node [
    id 282
    label "anta&#322;"
  ]
  node [
    id 283
    label "wyj&#347;cie"
  ]
  node [
    id 284
    label "warzy&#263;"
  ]
  node [
    id 285
    label "warzenie"
  ]
  node [
    id 286
    label "uwarzenie"
  ]
  node [
    id 287
    label "nap&#243;j"
  ]
  node [
    id 288
    label "nawarzy&#263;"
  ]
  node [
    id 289
    label "bacik"
  ]
  node [
    id 290
    label "uwarzy&#263;"
  ]
  node [
    id 291
    label "nawarzenie"
  ]
  node [
    id 292
    label "birofilia"
  ]
  node [
    id 293
    label "XD"
  ]
  node [
    id 294
    label "&#378;le"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 26
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 215
  ]
  edge [
    source 21
    target 216
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 219
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 223
  ]
  edge [
    source 22
    target 224
  ]
  edge [
    source 22
    target 225
  ]
  edge [
    source 22
    target 226
  ]
  edge [
    source 22
    target 227
  ]
  edge [
    source 22
    target 228
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 22
    target 231
  ]
  edge [
    source 22
    target 232
  ]
  edge [
    source 22
    target 233
  ]
  edge [
    source 22
    target 234
  ]
  edge [
    source 22
    target 235
  ]
  edge [
    source 22
    target 236
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 22
    target 238
  ]
  edge [
    source 22
    target 239
  ]
  edge [
    source 22
    target 240
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 243
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 23
    target 250
  ]
  edge [
    source 23
    target 251
  ]
  edge [
    source 23
    target 252
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 254
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 256
  ]
  edge [
    source 23
    target 257
  ]
  edge [
    source 23
    target 58
  ]
  edge [
    source 23
    target 258
  ]
  edge [
    source 23
    target 259
  ]
  edge [
    source 23
    target 260
  ]
  edge [
    source 23
    target 261
  ]
  edge [
    source 23
    target 262
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 229
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 233
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 274
  ]
  edge [
    source 24
    target 275
  ]
  edge [
    source 24
    target 276
  ]
  edge [
    source 24
    target 277
  ]
  edge [
    source 24
    target 278
  ]
  edge [
    source 24
    target 279
  ]
  edge [
    source 24
    target 280
  ]
  edge [
    source 25
    target 281
  ]
  edge [
    source 25
    target 282
  ]
  edge [
    source 25
    target 283
  ]
  edge [
    source 25
    target 284
  ]
  edge [
    source 25
    target 285
  ]
  edge [
    source 25
    target 286
  ]
  edge [
    source 25
    target 257
  ]
  edge [
    source 25
    target 287
  ]
  edge [
    source 25
    target 288
  ]
  edge [
    source 25
    target 289
  ]
  edge [
    source 25
    target 290
  ]
  edge [
    source 25
    target 291
  ]
  edge [
    source 25
    target 292
  ]
  edge [
    source 293
    target 294
  ]
]
