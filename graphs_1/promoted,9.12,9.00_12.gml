graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9764705882352942
  density 0.023529411764705882
  graphCliqueNumber 2
  node [
    id 0
    label "sytuacja"
    origin "text"
  ]
  node [
    id 1
    label "bez"
    origin "text"
  ]
  node [
    id 2
    label "precedens"
    origin "text"
  ]
  node [
    id 3
    label "w&#322;adza"
    origin "text"
  ]
  node [
    id 4
    label "wystawi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pojazd"
    origin "text"
  ]
  node [
    id 6
    label "opancerzy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przeciwko"
    origin "text"
  ]
  node [
    id 8
    label "protestuj&#261;cy"
    origin "text"
  ]
  node [
    id 9
    label "szczeg&#243;&#322;"
  ]
  node [
    id 10
    label "motyw"
  ]
  node [
    id 11
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 12
    label "state"
  ]
  node [
    id 13
    label "realia"
  ]
  node [
    id 14
    label "warunki"
  ]
  node [
    id 15
    label "ki&#347;&#263;"
  ]
  node [
    id 16
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 17
    label "krzew"
  ]
  node [
    id 18
    label "pi&#380;maczkowate"
  ]
  node [
    id 19
    label "pestkowiec"
  ]
  node [
    id 20
    label "kwiat"
  ]
  node [
    id 21
    label "owoc"
  ]
  node [
    id 22
    label "oliwkowate"
  ]
  node [
    id 23
    label "ro&#347;lina"
  ]
  node [
    id 24
    label "hy&#263;ka"
  ]
  node [
    id 25
    label "lilac"
  ]
  node [
    id 26
    label "delfinidyna"
  ]
  node [
    id 27
    label "precedent"
  ]
  node [
    id 28
    label "orzeczenie"
  ]
  node [
    id 29
    label "wydarzenie"
  ]
  node [
    id 30
    label "przypadek"
  ]
  node [
    id 31
    label "cz&#322;owiek"
  ]
  node [
    id 32
    label "panowanie"
  ]
  node [
    id 33
    label "Kreml"
  ]
  node [
    id 34
    label "wydolno&#347;&#263;"
  ]
  node [
    id 35
    label "grupa"
  ]
  node [
    id 36
    label "prawo"
  ]
  node [
    id 37
    label "rz&#261;dzenie"
  ]
  node [
    id 38
    label "rz&#261;d"
  ]
  node [
    id 39
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 40
    label "struktura"
  ]
  node [
    id 41
    label "wysun&#261;&#263;"
  ]
  node [
    id 42
    label "zbudowa&#263;"
  ]
  node [
    id 43
    label "wyrazi&#263;"
  ]
  node [
    id 44
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 45
    label "przedstawi&#263;"
  ]
  node [
    id 46
    label "wypisa&#263;"
  ]
  node [
    id 47
    label "wskaza&#263;"
  ]
  node [
    id 48
    label "indicate"
  ]
  node [
    id 49
    label "wynie&#347;&#263;"
  ]
  node [
    id 50
    label "pies_my&#347;liwski"
  ]
  node [
    id 51
    label "zaproponowa&#263;"
  ]
  node [
    id 52
    label "wyeksponowa&#263;"
  ]
  node [
    id 53
    label "wychyli&#263;"
  ]
  node [
    id 54
    label "set"
  ]
  node [
    id 55
    label "wyj&#261;&#263;"
  ]
  node [
    id 56
    label "fukanie"
  ]
  node [
    id 57
    label "przeszklenie"
  ]
  node [
    id 58
    label "pod&#322;oga"
  ]
  node [
    id 59
    label "odzywka"
  ]
  node [
    id 60
    label "powietrze"
  ]
  node [
    id 61
    label "przyholowanie"
  ]
  node [
    id 62
    label "fukni&#281;cie"
  ]
  node [
    id 63
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 64
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 65
    label "hamulec"
  ]
  node [
    id 66
    label "nadwozie"
  ]
  node [
    id 67
    label "zielona_karta"
  ]
  node [
    id 68
    label "przyholowywa&#263;"
  ]
  node [
    id 69
    label "test_zderzeniowy"
  ]
  node [
    id 70
    label "odholowanie"
  ]
  node [
    id 71
    label "tabor"
  ]
  node [
    id 72
    label "odholowywanie"
  ]
  node [
    id 73
    label "przyholowywanie"
  ]
  node [
    id 74
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 75
    label "l&#261;d"
  ]
  node [
    id 76
    label "przyholowa&#263;"
  ]
  node [
    id 77
    label "odholowa&#263;"
  ]
  node [
    id 78
    label "podwozie"
  ]
  node [
    id 79
    label "woda"
  ]
  node [
    id 80
    label "odholowywa&#263;"
  ]
  node [
    id 81
    label "prowadzenie_si&#281;"
  ]
  node [
    id 82
    label "zabezpieczy&#263;"
  ]
  node [
    id 83
    label "demonstrant"
  ]
  node [
    id 84
    label "pikieta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
]
