graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.010989010989011
  density 0.011110436524801166
  graphCliqueNumber 3
  node [
    id 0
    label "miesi&#281;cznik"
    origin "text"
  ]
  node [
    id 1
    label "komputerowy"
    origin "text"
  ]
  node [
    id 2
    label "elektroniczny"
    origin "text"
  ]
  node [
    id 3
    label "czasopismo"
    origin "text"
  ]
  node [
    id 4
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "licencja"
    origin "text"
  ]
  node [
    id 6
    label "creative"
    origin "text"
  ]
  node [
    id 7
    label "commons"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "tematyka"
    origin "text"
  ]
  node [
    id 10
    label "skupiony"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "wok&#243;&#322;"
    origin "text"
  ]
  node [
    id 13
    label "otwarty"
    origin "text"
  ]
  node [
    id 14
    label "oprogramowanie"
    origin "text"
  ]
  node [
    id 15
    label "standard"
    origin "text"
  ]
  node [
    id 16
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 17
    label "ita"
    origin "text"
  ]
  node [
    id 18
    label "Supe&#322;ek"
  ]
  node [
    id 19
    label "pn&#261;cze"
  ]
  node [
    id 20
    label "miesi&#281;cznikowate"
  ]
  node [
    id 21
    label "komputerowo"
  ]
  node [
    id 22
    label "elektronicznie"
  ]
  node [
    id 23
    label "elektrycznie"
  ]
  node [
    id 24
    label "psychotest"
  ]
  node [
    id 25
    label "ok&#322;adka"
  ]
  node [
    id 26
    label "Zwrotnica"
  ]
  node [
    id 27
    label "prasa"
  ]
  node [
    id 28
    label "wk&#322;ad"
  ]
  node [
    id 29
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 30
    label "pismo"
  ]
  node [
    id 31
    label "communication"
  ]
  node [
    id 32
    label "dzia&#322;"
  ]
  node [
    id 33
    label "egzemplarz"
  ]
  node [
    id 34
    label "zajawka"
  ]
  node [
    id 35
    label "impart"
  ]
  node [
    id 36
    label "panna_na_wydaniu"
  ]
  node [
    id 37
    label "surrender"
  ]
  node [
    id 38
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 39
    label "train"
  ]
  node [
    id 40
    label "give"
  ]
  node [
    id 41
    label "wytwarza&#263;"
  ]
  node [
    id 42
    label "dawa&#263;"
  ]
  node [
    id 43
    label "zapach"
  ]
  node [
    id 44
    label "wprowadza&#263;"
  ]
  node [
    id 45
    label "ujawnia&#263;"
  ]
  node [
    id 46
    label "wydawnictwo"
  ]
  node [
    id 47
    label "powierza&#263;"
  ]
  node [
    id 48
    label "produkcja"
  ]
  node [
    id 49
    label "denuncjowa&#263;"
  ]
  node [
    id 50
    label "mie&#263;_miejsce"
  ]
  node [
    id 51
    label "plon"
  ]
  node [
    id 52
    label "reszta"
  ]
  node [
    id 53
    label "robi&#263;"
  ]
  node [
    id 54
    label "placard"
  ]
  node [
    id 55
    label "tajemnica"
  ]
  node [
    id 56
    label "wiano"
  ]
  node [
    id 57
    label "kojarzy&#263;"
  ]
  node [
    id 58
    label "d&#378;wi&#281;k"
  ]
  node [
    id 59
    label "podawa&#263;"
  ]
  node [
    id 60
    label "prawo"
  ]
  node [
    id 61
    label "licencjonowa&#263;"
  ]
  node [
    id 62
    label "pozwolenie"
  ]
  node [
    id 63
    label "hodowla"
  ]
  node [
    id 64
    label "rasowy"
  ]
  node [
    id 65
    label "license"
  ]
  node [
    id 66
    label "zezwolenie"
  ]
  node [
    id 67
    label "za&#347;wiadczenie"
  ]
  node [
    id 68
    label "temat"
  ]
  node [
    id 69
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 70
    label "uwa&#380;ny"
  ]
  node [
    id 71
    label "si&#281;ga&#263;"
  ]
  node [
    id 72
    label "trwa&#263;"
  ]
  node [
    id 73
    label "obecno&#347;&#263;"
  ]
  node [
    id 74
    label "stan"
  ]
  node [
    id 75
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 76
    label "stand"
  ]
  node [
    id 77
    label "uczestniczy&#263;"
  ]
  node [
    id 78
    label "chodzi&#263;"
  ]
  node [
    id 79
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 80
    label "equal"
  ]
  node [
    id 81
    label "blisko"
  ]
  node [
    id 82
    label "wsz&#281;dzie"
  ]
  node [
    id 83
    label "ewidentny"
  ]
  node [
    id 84
    label "bezpo&#347;redni"
  ]
  node [
    id 85
    label "otwarcie"
  ]
  node [
    id 86
    label "nieograniczony"
  ]
  node [
    id 87
    label "zdecydowany"
  ]
  node [
    id 88
    label "gotowy"
  ]
  node [
    id 89
    label "aktualny"
  ]
  node [
    id 90
    label "prostoduszny"
  ]
  node [
    id 91
    label "jawnie"
  ]
  node [
    id 92
    label "otworzysty"
  ]
  node [
    id 93
    label "dost&#281;pny"
  ]
  node [
    id 94
    label "publiczny"
  ]
  node [
    id 95
    label "aktywny"
  ]
  node [
    id 96
    label "kontaktowy"
  ]
  node [
    id 97
    label "program"
  ]
  node [
    id 98
    label "zbi&#243;r"
  ]
  node [
    id 99
    label "reengineering"
  ]
  node [
    id 100
    label "zorganizowa&#263;"
  ]
  node [
    id 101
    label "model"
  ]
  node [
    id 102
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 103
    label "taniec_towarzyski"
  ]
  node [
    id 104
    label "ordinariness"
  ]
  node [
    id 105
    label "organizowanie"
  ]
  node [
    id 106
    label "criterion"
  ]
  node [
    id 107
    label "zorganizowanie"
  ]
  node [
    id 108
    label "instytucja"
  ]
  node [
    id 109
    label "organizowa&#263;"
  ]
  node [
    id 110
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 111
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 112
    label "obszar"
  ]
  node [
    id 113
    label "obiekt_naturalny"
  ]
  node [
    id 114
    label "przedmiot"
  ]
  node [
    id 115
    label "biosfera"
  ]
  node [
    id 116
    label "grupa"
  ]
  node [
    id 117
    label "stw&#243;r"
  ]
  node [
    id 118
    label "Stary_&#346;wiat"
  ]
  node [
    id 119
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 120
    label "rzecz"
  ]
  node [
    id 121
    label "magnetosfera"
  ]
  node [
    id 122
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 123
    label "environment"
  ]
  node [
    id 124
    label "Nowy_&#346;wiat"
  ]
  node [
    id 125
    label "geosfera"
  ]
  node [
    id 126
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 127
    label "planeta"
  ]
  node [
    id 128
    label "przejmowa&#263;"
  ]
  node [
    id 129
    label "litosfera"
  ]
  node [
    id 130
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 131
    label "makrokosmos"
  ]
  node [
    id 132
    label "barysfera"
  ]
  node [
    id 133
    label "biota"
  ]
  node [
    id 134
    label "p&#243;&#322;noc"
  ]
  node [
    id 135
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 136
    label "fauna"
  ]
  node [
    id 137
    label "wszechstworzenie"
  ]
  node [
    id 138
    label "geotermia"
  ]
  node [
    id 139
    label "biegun"
  ]
  node [
    id 140
    label "ekosystem"
  ]
  node [
    id 141
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 142
    label "teren"
  ]
  node [
    id 143
    label "zjawisko"
  ]
  node [
    id 144
    label "p&#243;&#322;kula"
  ]
  node [
    id 145
    label "atmosfera"
  ]
  node [
    id 146
    label "mikrokosmos"
  ]
  node [
    id 147
    label "class"
  ]
  node [
    id 148
    label "po&#322;udnie"
  ]
  node [
    id 149
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 150
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 151
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 152
    label "przejmowanie"
  ]
  node [
    id 153
    label "przestrze&#324;"
  ]
  node [
    id 154
    label "asymilowanie_si&#281;"
  ]
  node [
    id 155
    label "przej&#261;&#263;"
  ]
  node [
    id 156
    label "ekosfera"
  ]
  node [
    id 157
    label "przyroda"
  ]
  node [
    id 158
    label "ciemna_materia"
  ]
  node [
    id 159
    label "geoida"
  ]
  node [
    id 160
    label "Wsch&#243;d"
  ]
  node [
    id 161
    label "populace"
  ]
  node [
    id 162
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 163
    label "huczek"
  ]
  node [
    id 164
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 165
    label "Ziemia"
  ]
  node [
    id 166
    label "universe"
  ]
  node [
    id 167
    label "ozonosfera"
  ]
  node [
    id 168
    label "rze&#378;ba"
  ]
  node [
    id 169
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 170
    label "zagranica"
  ]
  node [
    id 171
    label "hydrosfera"
  ]
  node [
    id 172
    label "woda"
  ]
  node [
    id 173
    label "kuchnia"
  ]
  node [
    id 174
    label "przej&#281;cie"
  ]
  node [
    id 175
    label "czarna_dziura"
  ]
  node [
    id 176
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 177
    label "morze"
  ]
  node [
    id 178
    label "Creative"
  ]
  node [
    id 179
    label "Commons"
  ]
  node [
    id 180
    label "Social"
  ]
  node [
    id 181
    label "Slider"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 180
    target 181
  ]
]
