graph [
  maxDegree 37
  minDegree 1
  meanDegree 2.0517241379310347
  density 0.017841079460269867
  graphCliqueNumber 4
  node [
    id 0
    label "komunistyczny"
    origin "text"
  ]
  node [
    id 1
    label "partia"
    origin "text"
  ]
  node [
    id 2
    label "narzuca&#263;"
    origin "text"
  ]
  node [
    id 3
    label "polityka"
    origin "text"
  ]
  node [
    id 4
    label "jeden"
    origin "text"
  ]
  node [
    id 5
    label "dziecko"
    origin "text"
  ]
  node [
    id 6
    label "kobieta"
    origin "text"
  ]
  node [
    id 7
    label "dokonywa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "aborcja"
    origin "text"
  ]
  node [
    id 9
    label "dziewczynka"
    origin "text"
  ]
  node [
    id 10
    label "lewicowy"
  ]
  node [
    id 11
    label "skomunizowanie"
  ]
  node [
    id 12
    label "czerwono"
  ]
  node [
    id 13
    label "komunizowanie"
  ]
  node [
    id 14
    label "SLD"
  ]
  node [
    id 15
    label "niedoczas"
  ]
  node [
    id 16
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 17
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 18
    label "grupa"
  ]
  node [
    id 19
    label "game"
  ]
  node [
    id 20
    label "ZChN"
  ]
  node [
    id 21
    label "wybranka"
  ]
  node [
    id 22
    label "Wigowie"
  ]
  node [
    id 23
    label "egzekutywa"
  ]
  node [
    id 24
    label "unit"
  ]
  node [
    id 25
    label "blok"
  ]
  node [
    id 26
    label "Razem"
  ]
  node [
    id 27
    label "si&#322;a"
  ]
  node [
    id 28
    label "organizacja"
  ]
  node [
    id 29
    label "wybranek"
  ]
  node [
    id 30
    label "materia&#322;"
  ]
  node [
    id 31
    label "PiS"
  ]
  node [
    id 32
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 33
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 34
    label "AWS"
  ]
  node [
    id 35
    label "package"
  ]
  node [
    id 36
    label "Bund"
  ]
  node [
    id 37
    label "Kuomintang"
  ]
  node [
    id 38
    label "aktyw"
  ]
  node [
    id 39
    label "Jakobici"
  ]
  node [
    id 40
    label "PSL"
  ]
  node [
    id 41
    label "Federali&#347;ci"
  ]
  node [
    id 42
    label "gra"
  ]
  node [
    id 43
    label "ZSL"
  ]
  node [
    id 44
    label "PPR"
  ]
  node [
    id 45
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 46
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 47
    label "PO"
  ]
  node [
    id 48
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 49
    label "zmusza&#263;"
  ]
  node [
    id 50
    label "aplikowa&#263;"
  ]
  node [
    id 51
    label "intrude"
  ]
  node [
    id 52
    label "trespass"
  ]
  node [
    id 53
    label "force"
  ]
  node [
    id 54
    label "umieszcza&#263;"
  ]
  node [
    id 55
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 56
    label "policy"
  ]
  node [
    id 57
    label "dyplomacja"
  ]
  node [
    id 58
    label "Europejska_Polityka_S&#261;siedztwa"
  ]
  node [
    id 59
    label "metoda"
  ]
  node [
    id 60
    label "kieliszek"
  ]
  node [
    id 61
    label "shot"
  ]
  node [
    id 62
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 63
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 64
    label "jaki&#347;"
  ]
  node [
    id 65
    label "jednolicie"
  ]
  node [
    id 66
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 67
    label "w&#243;dka"
  ]
  node [
    id 68
    label "ten"
  ]
  node [
    id 69
    label "ujednolicenie"
  ]
  node [
    id 70
    label "jednakowy"
  ]
  node [
    id 71
    label "cz&#322;owiek"
  ]
  node [
    id 72
    label "potomstwo"
  ]
  node [
    id 73
    label "organizm"
  ]
  node [
    id 74
    label "sraluch"
  ]
  node [
    id 75
    label "utulanie"
  ]
  node [
    id 76
    label "pediatra"
  ]
  node [
    id 77
    label "dzieciarnia"
  ]
  node [
    id 78
    label "m&#322;odziak"
  ]
  node [
    id 79
    label "dzieciak"
  ]
  node [
    id 80
    label "utula&#263;"
  ]
  node [
    id 81
    label "potomek"
  ]
  node [
    id 82
    label "entliczek-pentliczek"
  ]
  node [
    id 83
    label "pedofil"
  ]
  node [
    id 84
    label "m&#322;odzik"
  ]
  node [
    id 85
    label "cz&#322;owieczek"
  ]
  node [
    id 86
    label "zwierz&#281;"
  ]
  node [
    id 87
    label "niepe&#322;noletni"
  ]
  node [
    id 88
    label "fledgling"
  ]
  node [
    id 89
    label "utuli&#263;"
  ]
  node [
    id 90
    label "utulenie"
  ]
  node [
    id 91
    label "przekwitanie"
  ]
  node [
    id 92
    label "m&#281;&#380;yna"
  ]
  node [
    id 93
    label "babka"
  ]
  node [
    id 94
    label "samica"
  ]
  node [
    id 95
    label "doros&#322;y"
  ]
  node [
    id 96
    label "ulec"
  ]
  node [
    id 97
    label "uleganie"
  ]
  node [
    id 98
    label "partnerka"
  ]
  node [
    id 99
    label "&#380;ona"
  ]
  node [
    id 100
    label "ulega&#263;"
  ]
  node [
    id 101
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 102
    label "pa&#324;stwo"
  ]
  node [
    id 103
    label "ulegni&#281;cie"
  ]
  node [
    id 104
    label "menopauza"
  ]
  node [
    id 105
    label "&#322;ono"
  ]
  node [
    id 106
    label "robi&#263;"
  ]
  node [
    id 107
    label "determine"
  ]
  node [
    id 108
    label "przestawa&#263;"
  ]
  node [
    id 109
    label "make"
  ]
  node [
    id 110
    label "zabieg"
  ]
  node [
    id 111
    label "termination"
  ]
  node [
    id 112
    label "zygotarianin"
  ]
  node [
    id 113
    label "abrazja"
  ]
  node [
    id 114
    label "potomkini"
  ]
  node [
    id 115
    label "prostytutka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 115
  ]
]
