graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.5
  density 0.07894736842105263
  graphCliqueNumber 2
  node [
    id 0
    label "gmina"
    origin "text"
  ]
  node [
    id 1
    label "bov"
    origin "text"
  ]
  node [
    id 2
    label "rada_gminy"
  ]
  node [
    id 3
    label "Wielka_Wie&#347;"
  ]
  node [
    id 4
    label "jednostka_administracyjna"
  ]
  node [
    id 5
    label "powiat"
  ]
  node [
    id 6
    label "Dobro&#324;"
  ]
  node [
    id 7
    label "Karlsbad"
  ]
  node [
    id 8
    label "urz&#261;d"
  ]
  node [
    id 9
    label "Biskupice"
  ]
  node [
    id 10
    label "radny"
  ]
  node [
    id 11
    label "organizacja_religijna"
  ]
  node [
    id 12
    label "po&#322;udniowy"
  ]
  node [
    id 13
    label "Jutlandia"
  ]
  node [
    id 14
    label "S&#248;nderjyllands"
  ]
  node [
    id 15
    label "Amt"
  ]
  node [
    id 16
    label "Bov"
  ]
  node [
    id 17
    label "Kommune"
  ]
  node [
    id 18
    label "trzeci"
  ]
  node [
    id 19
    label "&#347;wiat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
]
