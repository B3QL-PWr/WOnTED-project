graph [
  maxDegree 36
  minDegree 1
  meanDegree 2
  density 0.011627906976744186
  graphCliqueNumber 2
  node [
    id 0
    label "zach&#281;ci&#263;"
    origin "text"
  ]
  node [
    id 1
    label "prze&#322;omowy"
    origin "text"
  ]
  node [
    id 2
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 3
    label "dystrybutor"
    origin "text"
  ]
  node [
    id 4
    label "dvd"
    origin "text"
  ]
  node [
    id 5
    label "niekonwencjonalny"
    origin "text"
  ]
  node [
    id 6
    label "wreszcie"
    origin "text"
  ]
  node [
    id 7
    label "skuteczny"
    origin "text"
  ]
  node [
    id 8
    label "walka"
    origin "text"
  ]
  node [
    id 9
    label "piractwo"
    origin "text"
  ]
  node [
    id 10
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "w&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 14
    label "inny"
    origin "text"
  ]
  node [
    id 15
    label "posiadacz"
    origin "text"
  ]
  node [
    id 16
    label "prawy"
    origin "text"
  ]
  node [
    id 17
    label "autorski"
    origin "text"
  ]
  node [
    id 18
    label "&#347;lad"
    origin "text"
  ]
  node [
    id 19
    label "filip"
    origin "text"
  ]
  node [
    id 20
    label "rzuci&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wszystek"
    origin "text"
  ]
  node [
    id 22
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 23
    label "pozyska&#263;"
  ]
  node [
    id 24
    label "invite"
  ]
  node [
    id 25
    label "donios&#322;y"
  ]
  node [
    id 26
    label "innowacyjny"
  ]
  node [
    id 27
    label "prze&#322;omowo"
  ]
  node [
    id 28
    label "wa&#380;ny"
  ]
  node [
    id 29
    label "system"
  ]
  node [
    id 30
    label "wytw&#243;r"
  ]
  node [
    id 31
    label "idea"
  ]
  node [
    id 32
    label "ukra&#347;&#263;"
  ]
  node [
    id 33
    label "ukradzenie"
  ]
  node [
    id 34
    label "pocz&#261;tki"
  ]
  node [
    id 35
    label "po&#347;rednik"
  ]
  node [
    id 36
    label "dostawca"
  ]
  node [
    id 37
    label "dozownik"
  ]
  node [
    id 38
    label "urz&#261;dzenie"
  ]
  node [
    id 39
    label "niepospolity"
  ]
  node [
    id 40
    label "bezpretensjonalny"
  ]
  node [
    id 41
    label "niestandardowy"
  ]
  node [
    id 42
    label "oryginalny"
  ]
  node [
    id 43
    label "niekonwencjonalnie"
  ]
  node [
    id 44
    label "w&#380;dy"
  ]
  node [
    id 45
    label "skutkowanie"
  ]
  node [
    id 46
    label "poskutkowanie"
  ]
  node [
    id 47
    label "dobry"
  ]
  node [
    id 48
    label "sprawny"
  ]
  node [
    id 49
    label "skutecznie"
  ]
  node [
    id 50
    label "czyn"
  ]
  node [
    id 51
    label "trudno&#347;&#263;"
  ]
  node [
    id 52
    label "zaatakowanie"
  ]
  node [
    id 53
    label "obrona"
  ]
  node [
    id 54
    label "konfrontacyjny"
  ]
  node [
    id 55
    label "military_action"
  ]
  node [
    id 56
    label "wrestle"
  ]
  node [
    id 57
    label "action"
  ]
  node [
    id 58
    label "wydarzenie"
  ]
  node [
    id 59
    label "rywalizacja"
  ]
  node [
    id 60
    label "sambo"
  ]
  node [
    id 61
    label "contest"
  ]
  node [
    id 62
    label "sp&#243;r"
  ]
  node [
    id 63
    label "bootleg"
  ]
  node [
    id 64
    label "przest&#281;pstwo"
  ]
  node [
    id 65
    label "plagiarism"
  ]
  node [
    id 66
    label "nielegalno&#347;&#263;"
  ]
  node [
    id 67
    label "bandytyzm"
  ]
  node [
    id 68
    label "terroryzm"
  ]
  node [
    id 69
    label "transmisja"
  ]
  node [
    id 70
    label "reprodukcja"
  ]
  node [
    id 71
    label "sta&#263;_si&#281;"
  ]
  node [
    id 72
    label "zrobi&#263;"
  ]
  node [
    id 73
    label "podj&#261;&#263;"
  ]
  node [
    id 74
    label "determine"
  ]
  node [
    id 75
    label "zacz&#261;&#263;"
  ]
  node [
    id 76
    label "nastawi&#263;"
  ]
  node [
    id 77
    label "prosecute"
  ]
  node [
    id 78
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 79
    label "impersonate"
  ]
  node [
    id 80
    label "umie&#347;ci&#263;"
  ]
  node [
    id 81
    label "obejrze&#263;"
  ]
  node [
    id 82
    label "incorporate"
  ]
  node [
    id 83
    label "draw"
  ]
  node [
    id 84
    label "uruchomi&#263;"
  ]
  node [
    id 85
    label "dokoptowa&#263;"
  ]
  node [
    id 86
    label "kolejny"
  ]
  node [
    id 87
    label "inaczej"
  ]
  node [
    id 88
    label "r&#243;&#380;ny"
  ]
  node [
    id 89
    label "inszy"
  ]
  node [
    id 90
    label "osobno"
  ]
  node [
    id 91
    label "wykupienie"
  ]
  node [
    id 92
    label "bycie_w_posiadaniu"
  ]
  node [
    id 93
    label "wykupywanie"
  ]
  node [
    id 94
    label "podmiot"
  ]
  node [
    id 95
    label "z_prawa"
  ]
  node [
    id 96
    label "cnotliwy"
  ]
  node [
    id 97
    label "moralny"
  ]
  node [
    id 98
    label "zgodnie_z_prawem"
  ]
  node [
    id 99
    label "naturalny"
  ]
  node [
    id 100
    label "legalny"
  ]
  node [
    id 101
    label "na_prawo"
  ]
  node [
    id 102
    label "s&#322;uszny"
  ]
  node [
    id 103
    label "w_prawo"
  ]
  node [
    id 104
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 105
    label "prawicowy"
  ]
  node [
    id 106
    label "chwalebny"
  ]
  node [
    id 107
    label "zacny"
  ]
  node [
    id 108
    label "w&#322;asny"
  ]
  node [
    id 109
    label "autorsko"
  ]
  node [
    id 110
    label "sznurowanie"
  ]
  node [
    id 111
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 112
    label "odrobina"
  ]
  node [
    id 113
    label "sznurowa&#263;"
  ]
  node [
    id 114
    label "attribute"
  ]
  node [
    id 115
    label "wp&#322;yw"
  ]
  node [
    id 116
    label "odcisk"
  ]
  node [
    id 117
    label "skutek"
  ]
  node [
    id 118
    label "wyzwanie"
  ]
  node [
    id 119
    label "majdn&#261;&#263;"
  ]
  node [
    id 120
    label "opu&#347;ci&#263;"
  ]
  node [
    id 121
    label "cie&#324;"
  ]
  node [
    id 122
    label "konwulsja"
  ]
  node [
    id 123
    label "podejrzenie"
  ]
  node [
    id 124
    label "wywo&#322;a&#263;"
  ]
  node [
    id 125
    label "ruszy&#263;"
  ]
  node [
    id 126
    label "odej&#347;&#263;"
  ]
  node [
    id 127
    label "project"
  ]
  node [
    id 128
    label "da&#263;"
  ]
  node [
    id 129
    label "czar"
  ]
  node [
    id 130
    label "atak"
  ]
  node [
    id 131
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 132
    label "zdecydowa&#263;"
  ]
  node [
    id 133
    label "rush"
  ]
  node [
    id 134
    label "bewilder"
  ]
  node [
    id 135
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 136
    label "sygn&#261;&#263;"
  ]
  node [
    id 137
    label "zmieni&#263;"
  ]
  node [
    id 138
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 139
    label "poruszy&#263;"
  ]
  node [
    id 140
    label "&#347;wiat&#322;o"
  ]
  node [
    id 141
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 142
    label "most"
  ]
  node [
    id 143
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 144
    label "spowodowa&#263;"
  ]
  node [
    id 145
    label "frame"
  ]
  node [
    id 146
    label "powiedzie&#263;"
  ]
  node [
    id 147
    label "przeznaczenie"
  ]
  node [
    id 148
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 149
    label "peddle"
  ]
  node [
    id 150
    label "skonstruowa&#263;"
  ]
  node [
    id 151
    label "towar"
  ]
  node [
    id 152
    label "ca&#322;y"
  ]
  node [
    id 153
    label "wojsko"
  ]
  node [
    id 154
    label "magnitude"
  ]
  node [
    id 155
    label "energia"
  ]
  node [
    id 156
    label "capacity"
  ]
  node [
    id 157
    label "wuchta"
  ]
  node [
    id 158
    label "cecha"
  ]
  node [
    id 159
    label "parametr"
  ]
  node [
    id 160
    label "moment_si&#322;y"
  ]
  node [
    id 161
    label "przemoc"
  ]
  node [
    id 162
    label "zdolno&#347;&#263;"
  ]
  node [
    id 163
    label "mn&#243;stwo"
  ]
  node [
    id 164
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 165
    label "rozwi&#261;zanie"
  ]
  node [
    id 166
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 167
    label "potencja"
  ]
  node [
    id 168
    label "zjawisko"
  ]
  node [
    id 169
    label "zaleta"
  ]
  node [
    id 170
    label "ii"
  ]
  node [
    id 171
    label "tv"
  ]
  node [
    id 172
    label "trwa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 108
  ]
  edge [
    source 17
    target 42
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 22
    target 155
  ]
  edge [
    source 22
    target 156
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 171
    target 172
  ]
]
