graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.878787878787879
  density 0.058712121212121215
  graphCliqueNumber 4
  node [
    id 0
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 2
    label "perceive"
  ]
  node [
    id 3
    label "reagowa&#263;"
  ]
  node [
    id 4
    label "spowodowa&#263;"
  ]
  node [
    id 5
    label "male&#263;"
  ]
  node [
    id 6
    label "zmale&#263;"
  ]
  node [
    id 7
    label "spotka&#263;"
  ]
  node [
    id 8
    label "go_steady"
  ]
  node [
    id 9
    label "dostrzega&#263;"
  ]
  node [
    id 10
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 11
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 12
    label "ogl&#261;da&#263;"
  ]
  node [
    id 13
    label "os&#261;dza&#263;"
  ]
  node [
    id 14
    label "aprobowa&#263;"
  ]
  node [
    id 15
    label "punkt_widzenia"
  ]
  node [
    id 16
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 17
    label "wzrok"
  ]
  node [
    id 18
    label "postrzega&#263;"
  ]
  node [
    id 19
    label "notice"
  ]
  node [
    id 20
    label "arkada"
  ]
  node [
    id 21
    label "Fiedler"
  ]
  node [
    id 22
    label "platforma"
  ]
  node [
    id 23
    label "obywatelski"
  ]
  node [
    id 24
    label "ameryk"
  ]
  node [
    id 25
    label "p&#243;&#322;nocny"
  ]
  node [
    id 26
    label "stan"
  ]
  node [
    id 27
    label "zjednoczy&#263;"
  ]
  node [
    id 28
    label "drugi"
  ]
  node [
    id 29
    label "wojna"
  ]
  node [
    id 30
    label "&#347;wiatowy"
  ]
  node [
    id 31
    label "Tomasz"
  ]
  node [
    id 32
    label "mannowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
]
