graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.972972972972973
  density 0.02702702702702703
  graphCliqueNumber 2
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "coraz"
    origin "text"
  ]
  node [
    id 3
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 4
    label "zamo&#380;ny"
    origin "text"
  ]
  node [
    id 5
    label "osoba"
    origin "text"
  ]
  node [
    id 6
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 7
    label "raport"
    origin "text"
  ]
  node [
    id 8
    label "kpmg"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;ga&#263;"
  ]
  node [
    id 10
    label "trwa&#263;"
  ]
  node [
    id 11
    label "obecno&#347;&#263;"
  ]
  node [
    id 12
    label "stan"
  ]
  node [
    id 13
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 14
    label "stand"
  ]
  node [
    id 15
    label "mie&#263;_miejsce"
  ]
  node [
    id 16
    label "uczestniczy&#263;"
  ]
  node [
    id 17
    label "chodzi&#263;"
  ]
  node [
    id 18
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 19
    label "equal"
  ]
  node [
    id 20
    label "du&#380;y"
  ]
  node [
    id 21
    label "cz&#281;sto"
  ]
  node [
    id 22
    label "bardzo"
  ]
  node [
    id 23
    label "mocno"
  ]
  node [
    id 24
    label "wiela"
  ]
  node [
    id 25
    label "bogato"
  ]
  node [
    id 26
    label "sytuowany"
  ]
  node [
    id 27
    label "zapa&#347;ny"
  ]
  node [
    id 28
    label "forsiasty"
  ]
  node [
    id 29
    label "Zgredek"
  ]
  node [
    id 30
    label "kategoria_gramatyczna"
  ]
  node [
    id 31
    label "Casanova"
  ]
  node [
    id 32
    label "Don_Juan"
  ]
  node [
    id 33
    label "Gargantua"
  ]
  node [
    id 34
    label "Faust"
  ]
  node [
    id 35
    label "profanum"
  ]
  node [
    id 36
    label "Chocho&#322;"
  ]
  node [
    id 37
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 38
    label "koniugacja"
  ]
  node [
    id 39
    label "Winnetou"
  ]
  node [
    id 40
    label "Dwukwiat"
  ]
  node [
    id 41
    label "homo_sapiens"
  ]
  node [
    id 42
    label "Edyp"
  ]
  node [
    id 43
    label "Herkules_Poirot"
  ]
  node [
    id 44
    label "ludzko&#347;&#263;"
  ]
  node [
    id 45
    label "mikrokosmos"
  ]
  node [
    id 46
    label "person"
  ]
  node [
    id 47
    label "Sherlock_Holmes"
  ]
  node [
    id 48
    label "portrecista"
  ]
  node [
    id 49
    label "Szwejk"
  ]
  node [
    id 50
    label "Hamlet"
  ]
  node [
    id 51
    label "duch"
  ]
  node [
    id 52
    label "g&#322;owa"
  ]
  node [
    id 53
    label "oddzia&#322;ywanie"
  ]
  node [
    id 54
    label "Quasimodo"
  ]
  node [
    id 55
    label "Dulcynea"
  ]
  node [
    id 56
    label "Don_Kiszot"
  ]
  node [
    id 57
    label "Wallenrod"
  ]
  node [
    id 58
    label "Plastu&#347;"
  ]
  node [
    id 59
    label "Harry_Potter"
  ]
  node [
    id 60
    label "figura"
  ]
  node [
    id 61
    label "parali&#380;owa&#263;"
  ]
  node [
    id 62
    label "istota"
  ]
  node [
    id 63
    label "Werter"
  ]
  node [
    id 64
    label "antropochoria"
  ]
  node [
    id 65
    label "posta&#263;"
  ]
  node [
    id 66
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 67
    label "rise"
  ]
  node [
    id 68
    label "appear"
  ]
  node [
    id 69
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 70
    label "raport_Beveridge'a"
  ]
  node [
    id 71
    label "relacja"
  ]
  node [
    id 72
    label "raport_Fischlera"
  ]
  node [
    id 73
    label "statement"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
]
