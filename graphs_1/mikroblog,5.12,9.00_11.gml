graph [
  maxDegree 12
  minDegree 1
  meanDegree 2
  density 0.08333333333333333
  graphCliqueNumber 3
  node [
    id 0
    label "&#322;ukasz"
    origin "text"
  ]
  node [
    id 1
    label "fabia&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "bro&#324;"
    origin "text"
  ]
  node [
    id 3
    label "karny"
    origin "text"
  ]
  node [
    id 4
    label "westa"
    origin "text"
  ]
  node [
    id 5
    label "ham"
    origin "text"
  ]
  node [
    id 6
    label "cardiff"
    origin "text"
  ]
  node [
    id 7
    label "amunicja"
  ]
  node [
    id 8
    label "rozbroi&#263;"
  ]
  node [
    id 9
    label "osprz&#281;t"
  ]
  node [
    id 10
    label "przyrz&#261;d"
  ]
  node [
    id 11
    label "uzbrojenie"
  ]
  node [
    id 12
    label "karta_przetargowa"
  ]
  node [
    id 13
    label "or&#281;&#380;"
  ]
  node [
    id 14
    label "rozbraja&#263;"
  ]
  node [
    id 15
    label "rozbrajanie"
  ]
  node [
    id 16
    label "rozbrojenie"
  ]
  node [
    id 17
    label "karnie"
  ]
  node [
    id 18
    label "zdyscyplinowany"
  ]
  node [
    id 19
    label "strza&#322;"
  ]
  node [
    id 20
    label "pos&#322;uszny"
  ]
  node [
    id 21
    label "wzorowy"
  ]
  node [
    id 22
    label "kamizelka"
  ]
  node [
    id 23
    label "&#321;ukasz"
  ]
  node [
    id 24
    label "Cardiff"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
]
