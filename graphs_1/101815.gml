graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.1896024464831805
  density 0.006716571921727548
  graphCliqueNumber 6
  node [
    id 0
    label "trzeci"
    origin "text"
  ]
  node [
    id 1
    label "typ"
    origin "text"
  ]
  node [
    id 2
    label "zaciera&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "granica"
    origin "text"
  ]
  node [
    id 5
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 6
    label "popkultur&#261;"
    origin "text"
  ]
  node [
    id 7
    label "religia"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 9
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "w&#243;wczas"
    origin "text"
  ]
  node [
    id 11
    label "gdy"
    origin "text"
  ]
  node [
    id 12
    label "ostatni"
    origin "text"
  ]
  node [
    id 13
    label "staj"
    origin "text"
  ]
  node [
    id 14
    label "kultura"
    origin "text"
  ]
  node [
    id 15
    label "popularny"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "stan"
    origin "text"
  ]
  node [
    id 18
    label "stworzy&#263;"
    origin "text"
  ]
  node [
    id 19
    label "nowa"
    origin "text"
  ]
  node [
    id 20
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 21
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 22
    label "zawrze&#263;"
    origin "text"
  ]
  node [
    id 23
    label "druga"
    origin "text"
  ]
  node [
    id 24
    label "numer"
    origin "text"
  ]
  node [
    id 25
    label "rok"
    origin "text"
  ]
  node [
    id 26
    label "jak"
    origin "text"
  ]
  node [
    id 27
    label "bardzo"
    origin "text"
  ]
  node [
    id 28
    label "mo&#380;liwy"
    origin "text"
  ]
  node [
    id 29
    label "krzysztof"
    origin "text"
  ]
  node [
    id 30
    label "p&#281;dziszewski"
    origin "text"
  ]
  node [
    id 31
    label "rycerz"
    origin "text"
  ]
  node [
    id 32
    label "jedi"
    origin "text"
  ]
  node [
    id 33
    label "spis"
    origin "text"
  ]
  node [
    id 34
    label "powszechny"
    origin "text"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "neutralny"
  ]
  node [
    id 37
    label "przypadkowy"
  ]
  node [
    id 38
    label "dzie&#324;"
  ]
  node [
    id 39
    label "postronnie"
  ]
  node [
    id 40
    label "gromada"
  ]
  node [
    id 41
    label "autorament"
  ]
  node [
    id 42
    label "przypuszczenie"
  ]
  node [
    id 43
    label "cynk"
  ]
  node [
    id 44
    label "rezultat"
  ]
  node [
    id 45
    label "jednostka_systematyczna"
  ]
  node [
    id 46
    label "kr&#243;lestwo"
  ]
  node [
    id 47
    label "obstawia&#263;"
  ]
  node [
    id 48
    label "design"
  ]
  node [
    id 49
    label "facet"
  ]
  node [
    id 50
    label "variety"
  ]
  node [
    id 51
    label "sztuka"
  ]
  node [
    id 52
    label "antycypacja"
  ]
  node [
    id 53
    label "zataja&#263;"
  ]
  node [
    id 54
    label "usuwa&#263;"
  ]
  node [
    id 55
    label "chafe"
  ]
  node [
    id 56
    label "uszkadza&#263;"
  ]
  node [
    id 57
    label "r&#243;wna&#263;"
  ]
  node [
    id 58
    label "powodowa&#263;"
  ]
  node [
    id 59
    label "smear"
  ]
  node [
    id 60
    label "zakres"
  ]
  node [
    id 61
    label "Ural"
  ]
  node [
    id 62
    label "koniec"
  ]
  node [
    id 63
    label "kres"
  ]
  node [
    id 64
    label "granice"
  ]
  node [
    id 65
    label "granica_pa&#324;stwa"
  ]
  node [
    id 66
    label "pu&#322;ap"
  ]
  node [
    id 67
    label "frontier"
  ]
  node [
    id 68
    label "end"
  ]
  node [
    id 69
    label "miara"
  ]
  node [
    id 70
    label "poj&#281;cie"
  ]
  node [
    id 71
    label "przej&#347;cie"
  ]
  node [
    id 72
    label "wyznanie"
  ]
  node [
    id 73
    label "mitologia"
  ]
  node [
    id 74
    label "przedmiot"
  ]
  node [
    id 75
    label "ideologia"
  ]
  node [
    id 76
    label "kosmogonia"
  ]
  node [
    id 77
    label "mistyka"
  ]
  node [
    id 78
    label "nawraca&#263;"
  ]
  node [
    id 79
    label "nawracanie_si&#281;"
  ]
  node [
    id 80
    label "duchowny"
  ]
  node [
    id 81
    label "kultura_duchowa"
  ]
  node [
    id 82
    label "kosmologia"
  ]
  node [
    id 83
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 84
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 85
    label "kult"
  ]
  node [
    id 86
    label "rela"
  ]
  node [
    id 87
    label "uprawi&#263;"
  ]
  node [
    id 88
    label "gotowy"
  ]
  node [
    id 89
    label "might"
  ]
  node [
    id 90
    label "remark"
  ]
  node [
    id 91
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 92
    label "u&#380;ywa&#263;"
  ]
  node [
    id 93
    label "okre&#347;la&#263;"
  ]
  node [
    id 94
    label "j&#281;zyk"
  ]
  node [
    id 95
    label "say"
  ]
  node [
    id 96
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 97
    label "formu&#322;owa&#263;"
  ]
  node [
    id 98
    label "talk"
  ]
  node [
    id 99
    label "powiada&#263;"
  ]
  node [
    id 100
    label "informowa&#263;"
  ]
  node [
    id 101
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 102
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 103
    label "wydobywa&#263;"
  ]
  node [
    id 104
    label "express"
  ]
  node [
    id 105
    label "chew_the_fat"
  ]
  node [
    id 106
    label "dysfonia"
  ]
  node [
    id 107
    label "umie&#263;"
  ]
  node [
    id 108
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 109
    label "tell"
  ]
  node [
    id 110
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 111
    label "wyra&#380;a&#263;"
  ]
  node [
    id 112
    label "gaworzy&#263;"
  ]
  node [
    id 113
    label "rozmawia&#263;"
  ]
  node [
    id 114
    label "dziama&#263;"
  ]
  node [
    id 115
    label "prawi&#263;"
  ]
  node [
    id 116
    label "na&#243;wczas"
  ]
  node [
    id 117
    label "wtedy"
  ]
  node [
    id 118
    label "kolejny"
  ]
  node [
    id 119
    label "istota_&#380;ywa"
  ]
  node [
    id 120
    label "najgorszy"
  ]
  node [
    id 121
    label "aktualny"
  ]
  node [
    id 122
    label "ostatnio"
  ]
  node [
    id 123
    label "niedawno"
  ]
  node [
    id 124
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 125
    label "sko&#324;czony"
  ]
  node [
    id 126
    label "poprzedni"
  ]
  node [
    id 127
    label "pozosta&#322;y"
  ]
  node [
    id 128
    label "w&#261;tpliwy"
  ]
  node [
    id 129
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 130
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 131
    label "Wsch&#243;d"
  ]
  node [
    id 132
    label "rzecz"
  ]
  node [
    id 133
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 134
    label "przejmowa&#263;"
  ]
  node [
    id 135
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 136
    label "makrokosmos"
  ]
  node [
    id 137
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 138
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 139
    label "zjawisko"
  ]
  node [
    id 140
    label "praca_rolnicza"
  ]
  node [
    id 141
    label "tradycja"
  ]
  node [
    id 142
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 143
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 144
    label "przejmowanie"
  ]
  node [
    id 145
    label "cecha"
  ]
  node [
    id 146
    label "asymilowanie_si&#281;"
  ]
  node [
    id 147
    label "przej&#261;&#263;"
  ]
  node [
    id 148
    label "hodowla"
  ]
  node [
    id 149
    label "brzoskwiniarnia"
  ]
  node [
    id 150
    label "populace"
  ]
  node [
    id 151
    label "konwencja"
  ]
  node [
    id 152
    label "propriety"
  ]
  node [
    id 153
    label "jako&#347;&#263;"
  ]
  node [
    id 154
    label "kuchnia"
  ]
  node [
    id 155
    label "zwyczaj"
  ]
  node [
    id 156
    label "przej&#281;cie"
  ]
  node [
    id 157
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 158
    label "przyst&#281;pny"
  ]
  node [
    id 159
    label "&#322;atwy"
  ]
  node [
    id 160
    label "popularnie"
  ]
  node [
    id 161
    label "znany"
  ]
  node [
    id 162
    label "si&#281;ga&#263;"
  ]
  node [
    id 163
    label "trwa&#263;"
  ]
  node [
    id 164
    label "obecno&#347;&#263;"
  ]
  node [
    id 165
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 166
    label "stand"
  ]
  node [
    id 167
    label "mie&#263;_miejsce"
  ]
  node [
    id 168
    label "uczestniczy&#263;"
  ]
  node [
    id 169
    label "chodzi&#263;"
  ]
  node [
    id 170
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 171
    label "equal"
  ]
  node [
    id 172
    label "Arizona"
  ]
  node [
    id 173
    label "Georgia"
  ]
  node [
    id 174
    label "warstwa"
  ]
  node [
    id 175
    label "jednostka_administracyjna"
  ]
  node [
    id 176
    label "Hawaje"
  ]
  node [
    id 177
    label "Goa"
  ]
  node [
    id 178
    label "Floryda"
  ]
  node [
    id 179
    label "Oklahoma"
  ]
  node [
    id 180
    label "punkt"
  ]
  node [
    id 181
    label "Alaska"
  ]
  node [
    id 182
    label "wci&#281;cie"
  ]
  node [
    id 183
    label "Alabama"
  ]
  node [
    id 184
    label "Oregon"
  ]
  node [
    id 185
    label "poziom"
  ]
  node [
    id 186
    label "Teksas"
  ]
  node [
    id 187
    label "Illinois"
  ]
  node [
    id 188
    label "Waszyngton"
  ]
  node [
    id 189
    label "Jukatan"
  ]
  node [
    id 190
    label "shape"
  ]
  node [
    id 191
    label "Nowy_Meksyk"
  ]
  node [
    id 192
    label "ilo&#347;&#263;"
  ]
  node [
    id 193
    label "state"
  ]
  node [
    id 194
    label "Nowy_York"
  ]
  node [
    id 195
    label "Arakan"
  ]
  node [
    id 196
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 197
    label "Kalifornia"
  ]
  node [
    id 198
    label "wektor"
  ]
  node [
    id 199
    label "Massachusetts"
  ]
  node [
    id 200
    label "miejsce"
  ]
  node [
    id 201
    label "Pensylwania"
  ]
  node [
    id 202
    label "Michigan"
  ]
  node [
    id 203
    label "Maryland"
  ]
  node [
    id 204
    label "Ohio"
  ]
  node [
    id 205
    label "Kansas"
  ]
  node [
    id 206
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 207
    label "Luizjana"
  ]
  node [
    id 208
    label "samopoczucie"
  ]
  node [
    id 209
    label "Wirginia"
  ]
  node [
    id 210
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 211
    label "przygotowa&#263;"
  ]
  node [
    id 212
    label "specjalista_od_public_relations"
  ]
  node [
    id 213
    label "create"
  ]
  node [
    id 214
    label "zrobi&#263;"
  ]
  node [
    id 215
    label "wizerunek"
  ]
  node [
    id 216
    label "gwiazda"
  ]
  node [
    id 217
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 218
    label "dokument"
  ]
  node [
    id 219
    label "towar"
  ]
  node [
    id 220
    label "nag&#322;&#243;wek"
  ]
  node [
    id 221
    label "znak_j&#281;zykowy"
  ]
  node [
    id 222
    label "wyr&#243;b"
  ]
  node [
    id 223
    label "blok"
  ]
  node [
    id 224
    label "line"
  ]
  node [
    id 225
    label "paragraf"
  ]
  node [
    id 226
    label "rodzajnik"
  ]
  node [
    id 227
    label "prawda"
  ]
  node [
    id 228
    label "szkic"
  ]
  node [
    id 229
    label "tekst"
  ]
  node [
    id 230
    label "fragment"
  ]
  node [
    id 231
    label "uk&#322;ad"
  ]
  node [
    id 232
    label "sta&#263;_si&#281;"
  ]
  node [
    id 233
    label "raptowny"
  ]
  node [
    id 234
    label "embrace"
  ]
  node [
    id 235
    label "pozna&#263;"
  ]
  node [
    id 236
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 237
    label "insert"
  ]
  node [
    id 238
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 239
    label "admit"
  ]
  node [
    id 240
    label "boil"
  ]
  node [
    id 241
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 242
    label "umowa"
  ]
  node [
    id 243
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 244
    label "incorporate"
  ]
  node [
    id 245
    label "wezbra&#263;"
  ]
  node [
    id 246
    label "ustali&#263;"
  ]
  node [
    id 247
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 248
    label "zamkn&#261;&#263;"
  ]
  node [
    id 249
    label "godzina"
  ]
  node [
    id 250
    label "manewr"
  ]
  node [
    id 251
    label "sztos"
  ]
  node [
    id 252
    label "pok&#243;j"
  ]
  node [
    id 253
    label "wyst&#281;p"
  ]
  node [
    id 254
    label "turn"
  ]
  node [
    id 255
    label "impression"
  ]
  node [
    id 256
    label "hotel"
  ]
  node [
    id 257
    label "liczba"
  ]
  node [
    id 258
    label "czasopismo"
  ]
  node [
    id 259
    label "&#380;art"
  ]
  node [
    id 260
    label "orygina&#322;"
  ]
  node [
    id 261
    label "oznaczenie"
  ]
  node [
    id 262
    label "zi&#243;&#322;ko"
  ]
  node [
    id 263
    label "akt_p&#322;ciowy"
  ]
  node [
    id 264
    label "publikacja"
  ]
  node [
    id 265
    label "stulecie"
  ]
  node [
    id 266
    label "kalendarz"
  ]
  node [
    id 267
    label "czas"
  ]
  node [
    id 268
    label "pora_roku"
  ]
  node [
    id 269
    label "cykl_astronomiczny"
  ]
  node [
    id 270
    label "p&#243;&#322;rocze"
  ]
  node [
    id 271
    label "grupa"
  ]
  node [
    id 272
    label "kwarta&#322;"
  ]
  node [
    id 273
    label "kurs"
  ]
  node [
    id 274
    label "jubileusz"
  ]
  node [
    id 275
    label "miesi&#261;c"
  ]
  node [
    id 276
    label "lata"
  ]
  node [
    id 277
    label "martwy_sezon"
  ]
  node [
    id 278
    label "byd&#322;o"
  ]
  node [
    id 279
    label "zobo"
  ]
  node [
    id 280
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 281
    label "yakalo"
  ]
  node [
    id 282
    label "dzo"
  ]
  node [
    id 283
    label "w_chuj"
  ]
  node [
    id 284
    label "zno&#347;ny"
  ]
  node [
    id 285
    label "mo&#380;liwie"
  ]
  node [
    id 286
    label "urealnianie"
  ]
  node [
    id 287
    label "umo&#380;liwienie"
  ]
  node [
    id 288
    label "mo&#380;ebny"
  ]
  node [
    id 289
    label "umo&#380;liwianie"
  ]
  node [
    id 290
    label "dost&#281;pny"
  ]
  node [
    id 291
    label "urealnienie"
  ]
  node [
    id 292
    label "&#380;o&#322;nierz"
  ]
  node [
    id 293
    label "rycerstwo"
  ]
  node [
    id 294
    label "wojownik"
  ]
  node [
    id 295
    label "wyliczanka"
  ]
  node [
    id 296
    label "czynno&#347;&#263;"
  ]
  node [
    id 297
    label "catalog"
  ]
  node [
    id 298
    label "stock"
  ]
  node [
    id 299
    label "akt"
  ]
  node [
    id 300
    label "figurowa&#263;"
  ]
  node [
    id 301
    label "zbi&#243;r"
  ]
  node [
    id 302
    label "book"
  ]
  node [
    id 303
    label "pozycja"
  ]
  node [
    id 304
    label "sumariusz"
  ]
  node [
    id 305
    label "zbiorowy"
  ]
  node [
    id 306
    label "generalny"
  ]
  node [
    id 307
    label "cz&#281;sty"
  ]
  node [
    id 308
    label "powszechnie"
  ]
  node [
    id 309
    label "Krzysztofa"
  ]
  node [
    id 310
    label "P&#281;dziszewski"
  ]
  node [
    id 311
    label "Jedi"
  ]
  node [
    id 312
    label "i"
  ]
  node [
    id 313
    label "spisa"
  ]
  node [
    id 314
    label "wielki"
  ]
  node [
    id 315
    label "brytania"
  ]
  node [
    id 316
    label "gwiezdny"
  ]
  node [
    id 317
    label "wojna"
  ]
  node [
    id 318
    label "kr&#243;l"
  ]
  node [
    id 319
    label "rock"
  ]
  node [
    id 320
    label "Elvis"
  ]
  node [
    id 321
    label "Presley"
  ]
  node [
    id 322
    label "Twenty"
  ]
  node [
    id 323
    label "four"
  ]
  node [
    id 324
    label "Hour"
  ]
  node [
    id 325
    label "Church"
  ]
  node [
    id 326
    label "of"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 35
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 51
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 34
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 201
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 207
  ]
  edge [
    source 17
    target 208
  ]
  edge [
    source 17
    target 209
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 18
    target 213
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 219
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 223
  ]
  edge [
    source 21
    target 224
  ]
  edge [
    source 21
    target 225
  ]
  edge [
    source 21
    target 226
  ]
  edge [
    source 21
    target 227
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 21
    target 229
  ]
  edge [
    source 21
    target 230
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 231
  ]
  edge [
    source 22
    target 232
  ]
  edge [
    source 22
    target 233
  ]
  edge [
    source 22
    target 234
  ]
  edge [
    source 22
    target 235
  ]
  edge [
    source 22
    target 236
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 22
    target 238
  ]
  edge [
    source 22
    target 239
  ]
  edge [
    source 22
    target 240
  ]
  edge [
    source 22
    target 241
  ]
  edge [
    source 22
    target 242
  ]
  edge [
    source 22
    target 243
  ]
  edge [
    source 22
    target 244
  ]
  edge [
    source 22
    target 245
  ]
  edge [
    source 22
    target 246
  ]
  edge [
    source 22
    target 247
  ]
  edge [
    source 22
    target 248
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 24
    target 250
  ]
  edge [
    source 24
    target 251
  ]
  edge [
    source 24
    target 252
  ]
  edge [
    source 24
    target 49
  ]
  edge [
    source 24
    target 253
  ]
  edge [
    source 24
    target 254
  ]
  edge [
    source 24
    target 255
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 24
    target 257
  ]
  edge [
    source 24
    target 180
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 259
  ]
  edge [
    source 24
    target 260
  ]
  edge [
    source 24
    target 261
  ]
  edge [
    source 24
    target 262
  ]
  edge [
    source 24
    target 263
  ]
  edge [
    source 24
    target 264
  ]
  edge [
    source 25
    target 265
  ]
  edge [
    source 25
    target 266
  ]
  edge [
    source 25
    target 267
  ]
  edge [
    source 25
    target 268
  ]
  edge [
    source 25
    target 269
  ]
  edge [
    source 25
    target 270
  ]
  edge [
    source 25
    target 271
  ]
  edge [
    source 25
    target 272
  ]
  edge [
    source 25
    target 273
  ]
  edge [
    source 25
    target 274
  ]
  edge [
    source 25
    target 275
  ]
  edge [
    source 25
    target 276
  ]
  edge [
    source 25
    target 277
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 278
  ]
  edge [
    source 26
    target 279
  ]
  edge [
    source 26
    target 280
  ]
  edge [
    source 26
    target 281
  ]
  edge [
    source 26
    target 282
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 283
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 284
  ]
  edge [
    source 28
    target 285
  ]
  edge [
    source 28
    target 286
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 28
    target 288
  ]
  edge [
    source 28
    target 289
  ]
  edge [
    source 28
    target 290
  ]
  edge [
    source 28
    target 291
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 292
  ]
  edge [
    source 31
    target 293
  ]
  edge [
    source 31
    target 294
  ]
  edge [
    source 31
    target 311
  ]
  edge [
    source 31
    target 312
  ]
  edge [
    source 31
    target 313
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 295
  ]
  edge [
    source 33
    target 296
  ]
  edge [
    source 33
    target 297
  ]
  edge [
    source 33
    target 298
  ]
  edge [
    source 33
    target 299
  ]
  edge [
    source 33
    target 300
  ]
  edge [
    source 33
    target 301
  ]
  edge [
    source 33
    target 302
  ]
  edge [
    source 33
    target 303
  ]
  edge [
    source 33
    target 229
  ]
  edge [
    source 33
    target 304
  ]
  edge [
    source 34
    target 305
  ]
  edge [
    source 34
    target 306
  ]
  edge [
    source 34
    target 307
  ]
  edge [
    source 34
    target 161
  ]
  edge [
    source 34
    target 308
  ]
  edge [
    source 34
    target 311
  ]
  edge [
    source 34
    target 312
  ]
  edge [
    source 34
    target 313
  ]
  edge [
    source 309
    target 310
  ]
  edge [
    source 311
    target 312
  ]
  edge [
    source 311
    target 313
  ]
  edge [
    source 312
    target 313
  ]
  edge [
    source 314
    target 315
  ]
  edge [
    source 316
    target 317
  ]
  edge [
    source 318
    target 319
  ]
  edge [
    source 320
    target 321
  ]
  edge [
    source 320
    target 322
  ]
  edge [
    source 320
    target 323
  ]
  edge [
    source 320
    target 324
  ]
  edge [
    source 320
    target 325
  ]
  edge [
    source 320
    target 326
  ]
  edge [
    source 322
    target 323
  ]
  edge [
    source 322
    target 324
  ]
  edge [
    source 322
    target 325
  ]
  edge [
    source 322
    target 326
  ]
  edge [
    source 323
    target 324
  ]
  edge [
    source 323
    target 325
  ]
  edge [
    source 323
    target 326
  ]
  edge [
    source 324
    target 325
  ]
  edge [
    source 324
    target 326
  ]
  edge [
    source 325
    target 326
  ]
]
