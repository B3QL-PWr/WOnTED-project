graph [
  maxDegree 9
  minDegree 1
  meanDegree 2.1379310344827585
  density 0.07635467980295567
  graphCliqueNumber 5
  node [
    id 0
    label "run"
    origin "text"
  ]
  node [
    id 1
    label "singel"
    origin "text"
  ]
  node [
    id 2
    label "leona"
    origin "text"
  ]
  node [
    id 3
    label "lewis"
    origin "text"
  ]
  node [
    id 4
    label "popyt"
  ]
  node [
    id 5
    label "nie&#380;onaty"
  ]
  node [
    id 6
    label "singiel"
  ]
  node [
    id 7
    label "p&#322;yta"
  ]
  node [
    id 8
    label "stan_wolny"
  ]
  node [
    id 9
    label "piosenka"
  ]
  node [
    id 10
    label "mecz"
  ]
  node [
    id 11
    label "karta"
  ]
  node [
    id 12
    label "Leona"
  ]
  node [
    id 13
    label "Lewis"
  ]
  node [
    id 14
    label "BBC"
  ]
  node [
    id 15
    label "radio"
  ]
  node [
    id 16
    label "1s"
  ]
  node [
    id 17
    label "Live"
  ]
  node [
    id 18
    label "Lounge"
  ]
  node [
    id 19
    label "Snow"
  ]
  node [
    id 20
    label "patrol"
  ]
  node [
    id 21
    label "Spirit"
  ]
  node [
    id 22
    label "The"
  ]
  node [
    id 23
    label "Deluxe"
  ]
  node [
    id 24
    label "Edition"
  ]
  node [
    id 25
    label "wielki"
  ]
  node [
    id 26
    label "brytania"
  ]
  node [
    id 27
    label "stan"
  ]
  node [
    id 28
    label "zjednoczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
]
