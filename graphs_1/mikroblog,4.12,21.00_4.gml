graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.0136054421768708
  density 0.013791818097101855
  graphCliqueNumber 2
  node [
    id 0
    label "osoba"
    origin "text"
  ]
  node [
    id 1
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "profil"
    origin "text"
  ]
  node [
    id 4
    label "firmowy"
    origin "text"
  ]
  node [
    id 5
    label "daleko"
    origin "text"
  ]
  node [
    id 6
    label "forma"
    origin "text"
  ]
  node [
    id 7
    label "Zgredek"
  ]
  node [
    id 8
    label "kategoria_gramatyczna"
  ]
  node [
    id 9
    label "Casanova"
  ]
  node [
    id 10
    label "Don_Juan"
  ]
  node [
    id 11
    label "Gargantua"
  ]
  node [
    id 12
    label "Faust"
  ]
  node [
    id 13
    label "profanum"
  ]
  node [
    id 14
    label "Chocho&#322;"
  ]
  node [
    id 15
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 16
    label "koniugacja"
  ]
  node [
    id 17
    label "Winnetou"
  ]
  node [
    id 18
    label "Dwukwiat"
  ]
  node [
    id 19
    label "homo_sapiens"
  ]
  node [
    id 20
    label "Edyp"
  ]
  node [
    id 21
    label "Herkules_Poirot"
  ]
  node [
    id 22
    label "ludzko&#347;&#263;"
  ]
  node [
    id 23
    label "mikrokosmos"
  ]
  node [
    id 24
    label "person"
  ]
  node [
    id 25
    label "Sherlock_Holmes"
  ]
  node [
    id 26
    label "portrecista"
  ]
  node [
    id 27
    label "Szwejk"
  ]
  node [
    id 28
    label "Hamlet"
  ]
  node [
    id 29
    label "duch"
  ]
  node [
    id 30
    label "g&#322;owa"
  ]
  node [
    id 31
    label "oddzia&#322;ywanie"
  ]
  node [
    id 32
    label "Quasimodo"
  ]
  node [
    id 33
    label "Dulcynea"
  ]
  node [
    id 34
    label "Don_Kiszot"
  ]
  node [
    id 35
    label "Wallenrod"
  ]
  node [
    id 36
    label "Plastu&#347;"
  ]
  node [
    id 37
    label "Harry_Potter"
  ]
  node [
    id 38
    label "figura"
  ]
  node [
    id 39
    label "parali&#380;owa&#263;"
  ]
  node [
    id 40
    label "istota"
  ]
  node [
    id 41
    label "Werter"
  ]
  node [
    id 42
    label "antropochoria"
  ]
  node [
    id 43
    label "posta&#263;"
  ]
  node [
    id 44
    label "control"
  ]
  node [
    id 45
    label "eksponowa&#263;"
  ]
  node [
    id 46
    label "kre&#347;li&#263;"
  ]
  node [
    id 47
    label "g&#243;rowa&#263;"
  ]
  node [
    id 48
    label "message"
  ]
  node [
    id 49
    label "partner"
  ]
  node [
    id 50
    label "string"
  ]
  node [
    id 51
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 52
    label "przesuwa&#263;"
  ]
  node [
    id 53
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 54
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 55
    label "powodowa&#263;"
  ]
  node [
    id 56
    label "kierowa&#263;"
  ]
  node [
    id 57
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 58
    label "robi&#263;"
  ]
  node [
    id 59
    label "manipulate"
  ]
  node [
    id 60
    label "&#380;y&#263;"
  ]
  node [
    id 61
    label "navigate"
  ]
  node [
    id 62
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 63
    label "ukierunkowywa&#263;"
  ]
  node [
    id 64
    label "linia_melodyczna"
  ]
  node [
    id 65
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 66
    label "prowadzenie"
  ]
  node [
    id 67
    label "tworzy&#263;"
  ]
  node [
    id 68
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 69
    label "sterowa&#263;"
  ]
  node [
    id 70
    label "krzywa"
  ]
  node [
    id 71
    label "rzeczywisto&#347;&#263;_wirtualna"
  ]
  node [
    id 72
    label "seria"
  ]
  node [
    id 73
    label "twarz"
  ]
  node [
    id 74
    label "section"
  ]
  node [
    id 75
    label "listwa"
  ]
  node [
    id 76
    label "podgl&#261;d"
  ]
  node [
    id 77
    label "ozdoba"
  ]
  node [
    id 78
    label "dominanta"
  ]
  node [
    id 79
    label "obw&#243;dka"
  ]
  node [
    id 80
    label "faseta"
  ]
  node [
    id 81
    label "kontur"
  ]
  node [
    id 82
    label "profile"
  ]
  node [
    id 83
    label "konto"
  ]
  node [
    id 84
    label "przekr&#243;j"
  ]
  node [
    id 85
    label "awatar"
  ]
  node [
    id 86
    label "charakter"
  ]
  node [
    id 87
    label "element_konstrukcyjny"
  ]
  node [
    id 88
    label "sylwetka"
  ]
  node [
    id 89
    label "markowy"
  ]
  node [
    id 90
    label "oryginalny"
  ]
  node [
    id 91
    label "firmowo"
  ]
  node [
    id 92
    label "dawno"
  ]
  node [
    id 93
    label "nisko"
  ]
  node [
    id 94
    label "nieobecnie"
  ]
  node [
    id 95
    label "daleki"
  ]
  node [
    id 96
    label "het"
  ]
  node [
    id 97
    label "wysoko"
  ]
  node [
    id 98
    label "du&#380;o"
  ]
  node [
    id 99
    label "znacznie"
  ]
  node [
    id 100
    label "g&#322;&#281;boko"
  ]
  node [
    id 101
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 102
    label "punkt_widzenia"
  ]
  node [
    id 103
    label "do&#322;ek"
  ]
  node [
    id 104
    label "formality"
  ]
  node [
    id 105
    label "wz&#243;r"
  ]
  node [
    id 106
    label "kantyzm"
  ]
  node [
    id 107
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 108
    label "ornamentyka"
  ]
  node [
    id 109
    label "odmiana"
  ]
  node [
    id 110
    label "mode"
  ]
  node [
    id 111
    label "style"
  ]
  node [
    id 112
    label "formacja"
  ]
  node [
    id 113
    label "maszyna_drukarska"
  ]
  node [
    id 114
    label "poznanie"
  ]
  node [
    id 115
    label "szablon"
  ]
  node [
    id 116
    label "struktura"
  ]
  node [
    id 117
    label "spirala"
  ]
  node [
    id 118
    label "blaszka"
  ]
  node [
    id 119
    label "linearno&#347;&#263;"
  ]
  node [
    id 120
    label "stan"
  ]
  node [
    id 121
    label "temat"
  ]
  node [
    id 122
    label "cecha"
  ]
  node [
    id 123
    label "kielich"
  ]
  node [
    id 124
    label "zdolno&#347;&#263;"
  ]
  node [
    id 125
    label "poj&#281;cie"
  ]
  node [
    id 126
    label "kszta&#322;t"
  ]
  node [
    id 127
    label "pasmo"
  ]
  node [
    id 128
    label "rdze&#324;"
  ]
  node [
    id 129
    label "leksem"
  ]
  node [
    id 130
    label "dyspozycja"
  ]
  node [
    id 131
    label "wygl&#261;d"
  ]
  node [
    id 132
    label "October"
  ]
  node [
    id 133
    label "zawarto&#347;&#263;"
  ]
  node [
    id 134
    label "creation"
  ]
  node [
    id 135
    label "p&#281;tla"
  ]
  node [
    id 136
    label "p&#322;at"
  ]
  node [
    id 137
    label "gwiazda"
  ]
  node [
    id 138
    label "arystotelizm"
  ]
  node [
    id 139
    label "obiekt"
  ]
  node [
    id 140
    label "dzie&#322;o"
  ]
  node [
    id 141
    label "naczynie"
  ]
  node [
    id 142
    label "wyra&#380;enie"
  ]
  node [
    id 143
    label "jednostka_systematyczna"
  ]
  node [
    id 144
    label "miniatura"
  ]
  node [
    id 145
    label "zwyczaj"
  ]
  node [
    id 146
    label "morfem"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 43
  ]
]
