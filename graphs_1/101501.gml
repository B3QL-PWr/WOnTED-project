graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "kamienny"
    origin "text"
  ]
  node [
    id 1
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 2
    label "powiat"
    origin "text"
  ]
  node [
    id 3
    label "sztumski"
    origin "text"
  ]
  node [
    id 4
    label "niewzruszony"
  ]
  node [
    id 5
    label "twardy"
  ]
  node [
    id 6
    label "kamiennie"
  ]
  node [
    id 7
    label "naturalny"
  ]
  node [
    id 8
    label "g&#322;&#281;boki"
  ]
  node [
    id 9
    label "mineralny"
  ]
  node [
    id 10
    label "ch&#322;odny"
  ]
  node [
    id 11
    label "ghaty"
  ]
  node [
    id 12
    label "przedmiot"
  ]
  node [
    id 13
    label "grupa"
  ]
  node [
    id 14
    label "element"
  ]
  node [
    id 15
    label "przele&#378;&#263;"
  ]
  node [
    id 16
    label "pi&#281;tro"
  ]
  node [
    id 17
    label "karczek"
  ]
  node [
    id 18
    label "wysoki"
  ]
  node [
    id 19
    label "rami&#261;czko"
  ]
  node [
    id 20
    label "Ropa"
  ]
  node [
    id 21
    label "Jaworze"
  ]
  node [
    id 22
    label "Synaj"
  ]
  node [
    id 23
    label "wzniesienie"
  ]
  node [
    id 24
    label "przelezienie"
  ]
  node [
    id 25
    label "&#347;piew"
  ]
  node [
    id 26
    label "kupa"
  ]
  node [
    id 27
    label "kierunek"
  ]
  node [
    id 28
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 29
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 30
    label "d&#378;wi&#281;k"
  ]
  node [
    id 31
    label "Kreml"
  ]
  node [
    id 32
    label "gmina"
  ]
  node [
    id 33
    label "jednostka_administracyjna"
  ]
  node [
    id 34
    label "wojew&#243;dztwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
]
