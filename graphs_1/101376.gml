graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8095238095238095
  density 0.09047619047619047
  graphCliqueNumber 3
  node [
    id 0
    label "chlebowo"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "pomorski"
    origin "text"
  ]
  node [
    id 3
    label "jednostka_administracyjna"
  ]
  node [
    id 4
    label "makroregion"
  ]
  node [
    id 5
    label "powiat"
  ]
  node [
    id 6
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 7
    label "mikroregion"
  ]
  node [
    id 8
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 9
    label "pa&#324;stwo"
  ]
  node [
    id 10
    label "polski"
  ]
  node [
    id 11
    label "po_pomorsku"
  ]
  node [
    id 12
    label "etnolekt"
  ]
  node [
    id 13
    label "regionalny"
  ]
  node [
    id 14
    label "PGR"
  ]
  node [
    id 15
    label "miastko"
  ]
  node [
    id 16
    label "zak&#322;ad"
  ]
  node [
    id 17
    label "rolny"
  ]
  node [
    id 18
    label "skarb"
  ]
  node [
    id 19
    label "agencja"
  ]
  node [
    id 20
    label "w&#322;asno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 19
    target 20
  ]
]
