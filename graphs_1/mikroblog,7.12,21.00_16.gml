graph [
  maxDegree 17
  minDegree 1
  meanDegree 2
  density 0.03225806451612903
  graphCliqueNumber 2
  node [
    id 0
    label "kole&#380;anka"
    origin "text"
  ]
  node [
    id 1
    label "plastyk"
    origin "text"
  ]
  node [
    id 2
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "d&#322;ugopis"
    origin "text"
  ]
  node [
    id 4
    label "wz&#243;r"
    origin "text"
  ]
  node [
    id 5
    label "tatua&#380;"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "planowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "przysz&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "siebie"
    origin "text"
  ]
  node [
    id 10
    label "kuma"
  ]
  node [
    id 11
    label "tworzywo"
  ]
  node [
    id 12
    label "przeciwutleniacz"
  ]
  node [
    id 13
    label "plastic"
  ]
  node [
    id 14
    label "sztuczny"
  ]
  node [
    id 15
    label "nauczyciel"
  ]
  node [
    id 16
    label "artysta"
  ]
  node [
    id 17
    label "zorganizowa&#263;"
  ]
  node [
    id 18
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 19
    label "przerobi&#263;"
  ]
  node [
    id 20
    label "wystylizowa&#263;"
  ]
  node [
    id 21
    label "cause"
  ]
  node [
    id 22
    label "wydali&#263;"
  ]
  node [
    id 23
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 24
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 25
    label "post&#261;pi&#263;"
  ]
  node [
    id 26
    label "appoint"
  ]
  node [
    id 27
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 28
    label "nabra&#263;"
  ]
  node [
    id 29
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 30
    label "make"
  ]
  node [
    id 31
    label "przybory_do_pisania"
  ]
  node [
    id 32
    label "wypisa&#263;"
  ]
  node [
    id 33
    label "artyku&#322;"
  ]
  node [
    id 34
    label "sztyft"
  ]
  node [
    id 35
    label "wypisanie"
  ]
  node [
    id 36
    label "typ"
  ]
  node [
    id 37
    label "cz&#322;owiek"
  ]
  node [
    id 38
    label "rule"
  ]
  node [
    id 39
    label "projekt"
  ]
  node [
    id 40
    label "zapis"
  ]
  node [
    id 41
    label "motyw"
  ]
  node [
    id 42
    label "ruch"
  ]
  node [
    id 43
    label "figure"
  ]
  node [
    id 44
    label "dekal"
  ]
  node [
    id 45
    label "ideal"
  ]
  node [
    id 46
    label "mildew"
  ]
  node [
    id 47
    label "spos&#243;b"
  ]
  node [
    id 48
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 49
    label "tattoo"
  ]
  node [
    id 50
    label "ozdoba"
  ]
  node [
    id 51
    label "technika"
  ]
  node [
    id 52
    label "volunteer"
  ]
  node [
    id 53
    label "opuszcza&#263;_si&#281;"
  ]
  node [
    id 54
    label "lot_&#347;lizgowy"
  ]
  node [
    id 55
    label "organize"
  ]
  node [
    id 56
    label "my&#347;le&#263;"
  ]
  node [
    id 57
    label "opracowywa&#263;"
  ]
  node [
    id 58
    label "project"
  ]
  node [
    id 59
    label "mean"
  ]
  node [
    id 60
    label "jutro"
  ]
  node [
    id 61
    label "cel"
  ]
  node [
    id 62
    label "czas"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
]
