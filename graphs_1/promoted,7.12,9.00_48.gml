graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.021978021978022
  density 0.022466422466422466
  graphCliqueNumber 2
  node [
    id 0
    label "dwa"
    origin "text"
  ]
  node [
    id 1
    label "kobieta"
    origin "text"
  ]
  node [
    id 2
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kradzie&#380;"
    origin "text"
  ]
  node [
    id 4
    label "dow&#243;d"
    origin "text"
  ]
  node [
    id 5
    label "osobisty"
    origin "text"
  ]
  node [
    id 6
    label "torebka"
    origin "text"
  ]
  node [
    id 7
    label "pokrzywdzona"
    origin "text"
  ]
  node [
    id 8
    label "nast&#281;pnie"
    origin "text"
  ]
  node [
    id 9
    label "ten"
    origin "text"
  ]
  node [
    id 10
    label "zawrze&#263;"
    origin "text"
  ]
  node [
    id 11
    label "wiele"
    origin "text"
  ]
  node [
    id 12
    label "po&#380;yczka"
    origin "text"
  ]
  node [
    id 13
    label "got&#243;wkowy"
    origin "text"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "przekwitanie"
  ]
  node [
    id 16
    label "m&#281;&#380;yna"
  ]
  node [
    id 17
    label "babka"
  ]
  node [
    id 18
    label "samica"
  ]
  node [
    id 19
    label "doros&#322;y"
  ]
  node [
    id 20
    label "ulec"
  ]
  node [
    id 21
    label "uleganie"
  ]
  node [
    id 22
    label "partnerka"
  ]
  node [
    id 23
    label "&#380;ona"
  ]
  node [
    id 24
    label "ulega&#263;"
  ]
  node [
    id 25
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 26
    label "pa&#324;stwo"
  ]
  node [
    id 27
    label "ulegni&#281;cie"
  ]
  node [
    id 28
    label "menopauza"
  ]
  node [
    id 29
    label "&#322;ono"
  ]
  node [
    id 30
    label "przesta&#263;"
  ]
  node [
    id 31
    label "zrobi&#263;"
  ]
  node [
    id 32
    label "cause"
  ]
  node [
    id 33
    label "communicate"
  ]
  node [
    id 34
    label "plundering"
  ]
  node [
    id 35
    label "wydarzenie"
  ]
  node [
    id 36
    label "przest&#281;pstwo"
  ]
  node [
    id 37
    label "dokument"
  ]
  node [
    id 38
    label "forsing"
  ]
  node [
    id 39
    label "certificate"
  ]
  node [
    id 40
    label "rewizja"
  ]
  node [
    id 41
    label "argument"
  ]
  node [
    id 42
    label "act"
  ]
  node [
    id 43
    label "rzecz"
  ]
  node [
    id 44
    label "&#347;rodek"
  ]
  node [
    id 45
    label "uzasadnienie"
  ]
  node [
    id 46
    label "osobi&#347;cie"
  ]
  node [
    id 47
    label "bezpo&#347;redni"
  ]
  node [
    id 48
    label "szczery"
  ]
  node [
    id 49
    label "czyj&#347;"
  ]
  node [
    id 50
    label "intymny"
  ]
  node [
    id 51
    label "prywatny"
  ]
  node [
    id 52
    label "personalny"
  ]
  node [
    id 53
    label "w&#322;asny"
  ]
  node [
    id 54
    label "prywatnie"
  ]
  node [
    id 55
    label "emocjonalny"
  ]
  node [
    id 56
    label "dodatek"
  ]
  node [
    id 57
    label "torba"
  ]
  node [
    id 58
    label "owoc"
  ]
  node [
    id 59
    label "paczka"
  ]
  node [
    id 60
    label "otoczka"
  ]
  node [
    id 61
    label "kolejny"
  ]
  node [
    id 62
    label "okre&#347;lony"
  ]
  node [
    id 63
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 64
    label "uk&#322;ad"
  ]
  node [
    id 65
    label "sta&#263;_si&#281;"
  ]
  node [
    id 66
    label "raptowny"
  ]
  node [
    id 67
    label "embrace"
  ]
  node [
    id 68
    label "pozna&#263;"
  ]
  node [
    id 69
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 70
    label "insert"
  ]
  node [
    id 71
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 72
    label "admit"
  ]
  node [
    id 73
    label "boil"
  ]
  node [
    id 74
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 75
    label "umowa"
  ]
  node [
    id 76
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 77
    label "incorporate"
  ]
  node [
    id 78
    label "wezbra&#263;"
  ]
  node [
    id 79
    label "ustali&#263;"
  ]
  node [
    id 80
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 81
    label "zamkn&#261;&#263;"
  ]
  node [
    id 82
    label "wiela"
  ]
  node [
    id 83
    label "du&#380;y"
  ]
  node [
    id 84
    label "d&#322;ug"
  ]
  node [
    id 85
    label "&#322;ysina"
  ]
  node [
    id 86
    label "nalecia&#322;o&#347;&#263;"
  ]
  node [
    id 87
    label "loanword"
  ]
  node [
    id 88
    label "zobowi&#261;zanie"
  ]
  node [
    id 89
    label "fryzura"
  ]
  node [
    id 90
    label "fizyczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 13
    target 90
  ]
]
