graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.014388489208633
  density 0.014597018037743718
  graphCliqueNumber 2
  node [
    id 0
    label "anna"
    origin "text"
  ]
  node [
    id 1
    label "kiwa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "przecz&#261;co"
    origin "text"
  ]
  node [
    id 3
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 4
    label "nalewa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "whisky"
    origin "text"
  ]
  node [
    id 6
    label "dwa"
    origin "text"
  ]
  node [
    id 7
    label "szklaneczka"
    origin "text"
  ]
  node [
    id 8
    label "dodawa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "woda"
    origin "text"
  ]
  node [
    id 10
    label "loda"
    origin "text"
  ]
  node [
    id 11
    label "jeden"
    origin "text"
  ]
  node [
    id 12
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "irena"
    origin "text"
  ]
  node [
    id 14
    label "przechyla&#263;"
  ]
  node [
    id 15
    label "zwodzi&#263;"
  ]
  node [
    id 16
    label "fool"
  ]
  node [
    id 17
    label "dryblowa&#263;"
  ]
  node [
    id 18
    label "nod"
  ]
  node [
    id 19
    label "macha&#263;"
  ]
  node [
    id 20
    label "rusza&#263;"
  ]
  node [
    id 21
    label "negatywnie"
  ]
  node [
    id 22
    label "przecz&#261;cy"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 25
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 26
    label "ucho"
  ]
  node [
    id 27
    label "makrocefalia"
  ]
  node [
    id 28
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 29
    label "m&#243;zg"
  ]
  node [
    id 30
    label "kierownictwo"
  ]
  node [
    id 31
    label "czaszka"
  ]
  node [
    id 32
    label "dekiel"
  ]
  node [
    id 33
    label "umys&#322;"
  ]
  node [
    id 34
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 35
    label "&#347;ci&#281;cie"
  ]
  node [
    id 36
    label "sztuka"
  ]
  node [
    id 37
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 38
    label "g&#243;ra"
  ]
  node [
    id 39
    label "byd&#322;o"
  ]
  node [
    id 40
    label "alkohol"
  ]
  node [
    id 41
    label "wiedza"
  ]
  node [
    id 42
    label "ro&#347;lina"
  ]
  node [
    id 43
    label "&#347;ci&#281;gno"
  ]
  node [
    id 44
    label "&#380;ycie"
  ]
  node [
    id 45
    label "pryncypa&#322;"
  ]
  node [
    id 46
    label "fryzura"
  ]
  node [
    id 47
    label "noosfera"
  ]
  node [
    id 48
    label "kierowa&#263;"
  ]
  node [
    id 49
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 50
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 51
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 52
    label "cecha"
  ]
  node [
    id 53
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 54
    label "zdolno&#347;&#263;"
  ]
  node [
    id 55
    label "kszta&#322;t"
  ]
  node [
    id 56
    label "cz&#322;onek"
  ]
  node [
    id 57
    label "cia&#322;o"
  ]
  node [
    id 58
    label "obiekt"
  ]
  node [
    id 59
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 60
    label "la&#263;"
  ]
  node [
    id 61
    label "inculcate"
  ]
  node [
    id 62
    label "zalewa&#263;"
  ]
  node [
    id 63
    label "bi&#263;"
  ]
  node [
    id 64
    label "wype&#322;nia&#263;"
  ]
  node [
    id 65
    label "pour"
  ]
  node [
    id 66
    label "&#322;ycha"
  ]
  node [
    id 67
    label "w&#243;dka_gatunkowa"
  ]
  node [
    id 68
    label "szklanka"
  ]
  node [
    id 69
    label "dawa&#263;"
  ]
  node [
    id 70
    label "uzupe&#322;nia&#263;"
  ]
  node [
    id 71
    label "bind"
  ]
  node [
    id 72
    label "suma"
  ]
  node [
    id 73
    label "liczy&#263;"
  ]
  node [
    id 74
    label "nadawa&#263;"
  ]
  node [
    id 75
    label "wypowied&#378;"
  ]
  node [
    id 76
    label "obiekt_naturalny"
  ]
  node [
    id 77
    label "bicie"
  ]
  node [
    id 78
    label "wysi&#281;k"
  ]
  node [
    id 79
    label "pustka"
  ]
  node [
    id 80
    label "woda_s&#322;odka"
  ]
  node [
    id 81
    label "p&#322;ycizna"
  ]
  node [
    id 82
    label "ciecz"
  ]
  node [
    id 83
    label "spi&#281;trza&#263;"
  ]
  node [
    id 84
    label "uj&#281;cie_wody"
  ]
  node [
    id 85
    label "chlasta&#263;"
  ]
  node [
    id 86
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 87
    label "nap&#243;j"
  ]
  node [
    id 88
    label "bombast"
  ]
  node [
    id 89
    label "water"
  ]
  node [
    id 90
    label "kryptodepresja"
  ]
  node [
    id 91
    label "wodnik"
  ]
  node [
    id 92
    label "pojazd"
  ]
  node [
    id 93
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 94
    label "fala"
  ]
  node [
    id 95
    label "Waruna"
  ]
  node [
    id 96
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 97
    label "zrzut"
  ]
  node [
    id 98
    label "dotleni&#263;"
  ]
  node [
    id 99
    label "utylizator"
  ]
  node [
    id 100
    label "przyroda"
  ]
  node [
    id 101
    label "uci&#261;g"
  ]
  node [
    id 102
    label "wybrze&#380;e"
  ]
  node [
    id 103
    label "nabranie"
  ]
  node [
    id 104
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 105
    label "chlastanie"
  ]
  node [
    id 106
    label "klarownik"
  ]
  node [
    id 107
    label "przybrze&#380;e"
  ]
  node [
    id 108
    label "deklamacja"
  ]
  node [
    id 109
    label "spi&#281;trzenie"
  ]
  node [
    id 110
    label "przybieranie"
  ]
  node [
    id 111
    label "nabra&#263;"
  ]
  node [
    id 112
    label "tlenek"
  ]
  node [
    id 113
    label "spi&#281;trzanie"
  ]
  node [
    id 114
    label "l&#243;d"
  ]
  node [
    id 115
    label "kieliszek"
  ]
  node [
    id 116
    label "shot"
  ]
  node [
    id 117
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 118
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 119
    label "jaki&#347;"
  ]
  node [
    id 120
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 121
    label "jednolicie"
  ]
  node [
    id 122
    label "w&#243;dka"
  ]
  node [
    id 123
    label "ten"
  ]
  node [
    id 124
    label "ujednolicenie"
  ]
  node [
    id 125
    label "jednakowy"
  ]
  node [
    id 126
    label "tenis"
  ]
  node [
    id 127
    label "cover"
  ]
  node [
    id 128
    label "siatk&#243;wka"
  ]
  node [
    id 129
    label "faszerowa&#263;"
  ]
  node [
    id 130
    label "informowa&#263;"
  ]
  node [
    id 131
    label "introduce"
  ]
  node [
    id 132
    label "jedzenie"
  ]
  node [
    id 133
    label "tender"
  ]
  node [
    id 134
    label "deal"
  ]
  node [
    id 135
    label "kelner"
  ]
  node [
    id 136
    label "serwowa&#263;"
  ]
  node [
    id 137
    label "rozgrywa&#263;"
  ]
  node [
    id 138
    label "stawia&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
]
