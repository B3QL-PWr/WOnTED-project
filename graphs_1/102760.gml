graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.099585062240664
  density 0.008748271092669433
  graphCliqueNumber 3
  node [
    id 0
    label "podstawa"
    origin "text"
  ]
  node [
    id 1
    label "art"
    origin "text"
  ]
  node [
    id 2
    label "usta"
    origin "text"
  ]
  node [
    id 3
    label "ustawa"
    origin "text"
  ]
  node [
    id 4
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "czerwiec"
    origin "text"
  ]
  node [
    id 6
    label "rocznik"
    origin "text"
  ]
  node [
    id 7
    label "finanse"
    origin "text"
  ]
  node [
    id 8
    label "publiczny"
    origin "text"
  ]
  node [
    id 9
    label "dziennik"
    origin "text"
  ]
  node [
    id 10
    label "poz"
    origin "text"
  ]
  node [
    id 11
    label "p&#243;&#378;n"
    origin "text"
  ]
  node [
    id 12
    label "zmar&#322;"
    origin "text"
  ]
  node [
    id 13
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 14
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 15
    label "rad"
    origin "text"
  ]
  node [
    id 16
    label "minister"
    origin "text"
  ]
  node [
    id 17
    label "pa&#378;dziernik"
    origin "text"
  ]
  node [
    id 18
    label "sprawa"
    origin "text"
  ]
  node [
    id 19
    label "utworzy&#263;"
    origin "text"
  ]
  node [
    id 20
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 21
    label "gospodarka"
    origin "text"
  ]
  node [
    id 22
    label "oraz"
    origin "text"
  ]
  node [
    id 23
    label "si&#281;"
    origin "text"
  ]
  node [
    id 24
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "podstawowy"
  ]
  node [
    id 26
    label "strategia"
  ]
  node [
    id 27
    label "pot&#281;ga"
  ]
  node [
    id 28
    label "zasadzenie"
  ]
  node [
    id 29
    label "przedmiot"
  ]
  node [
    id 30
    label "za&#322;o&#380;enie"
  ]
  node [
    id 31
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 32
    label "&#347;ciana"
  ]
  node [
    id 33
    label "documentation"
  ]
  node [
    id 34
    label "dzieci&#281;ctwo"
  ]
  node [
    id 35
    label "pomys&#322;"
  ]
  node [
    id 36
    label "bok"
  ]
  node [
    id 37
    label "d&#243;&#322;"
  ]
  node [
    id 38
    label "punkt_odniesienia"
  ]
  node [
    id 39
    label "column"
  ]
  node [
    id 40
    label "zasadzi&#263;"
  ]
  node [
    id 41
    label "background"
  ]
  node [
    id 42
    label "warga_dolna"
  ]
  node [
    id 43
    label "ryjek"
  ]
  node [
    id 44
    label "zaci&#261;&#263;"
  ]
  node [
    id 45
    label "ssa&#263;"
  ]
  node [
    id 46
    label "twarz"
  ]
  node [
    id 47
    label "dzi&#243;b"
  ]
  node [
    id 48
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 49
    label "ssanie"
  ]
  node [
    id 50
    label "zaci&#281;cie"
  ]
  node [
    id 51
    label "jadaczka"
  ]
  node [
    id 52
    label "zacinanie"
  ]
  node [
    id 53
    label "organ"
  ]
  node [
    id 54
    label "jama_ustna"
  ]
  node [
    id 55
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 56
    label "warga_g&#243;rna"
  ]
  node [
    id 57
    label "zacina&#263;"
  ]
  node [
    id 58
    label "Karta_Nauczyciela"
  ]
  node [
    id 59
    label "marc&#243;wka"
  ]
  node [
    id 60
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 61
    label "akt"
  ]
  node [
    id 62
    label "przej&#347;&#263;"
  ]
  node [
    id 63
    label "charter"
  ]
  node [
    id 64
    label "przej&#347;cie"
  ]
  node [
    id 65
    label "s&#322;o&#324;ce"
  ]
  node [
    id 66
    label "czynienie_si&#281;"
  ]
  node [
    id 67
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 68
    label "czas"
  ]
  node [
    id 69
    label "long_time"
  ]
  node [
    id 70
    label "przedpo&#322;udnie"
  ]
  node [
    id 71
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 72
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 73
    label "tydzie&#324;"
  ]
  node [
    id 74
    label "godzina"
  ]
  node [
    id 75
    label "t&#322;usty_czwartek"
  ]
  node [
    id 76
    label "wsta&#263;"
  ]
  node [
    id 77
    label "day"
  ]
  node [
    id 78
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 79
    label "przedwiecz&#243;r"
  ]
  node [
    id 80
    label "Sylwester"
  ]
  node [
    id 81
    label "po&#322;udnie"
  ]
  node [
    id 82
    label "wzej&#347;cie"
  ]
  node [
    id 83
    label "podwiecz&#243;r"
  ]
  node [
    id 84
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 85
    label "rano"
  ]
  node [
    id 86
    label "termin"
  ]
  node [
    id 87
    label "ranek"
  ]
  node [
    id 88
    label "doba"
  ]
  node [
    id 89
    label "wiecz&#243;r"
  ]
  node [
    id 90
    label "walentynki"
  ]
  node [
    id 91
    label "popo&#322;udnie"
  ]
  node [
    id 92
    label "noc"
  ]
  node [
    id 93
    label "wstanie"
  ]
  node [
    id 94
    label "ro&#347;lina_zielna"
  ]
  node [
    id 95
    label "go&#378;dzikowate"
  ]
  node [
    id 96
    label "miesi&#261;c"
  ]
  node [
    id 97
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 98
    label "formacja"
  ]
  node [
    id 99
    label "kronika"
  ]
  node [
    id 100
    label "czasopismo"
  ]
  node [
    id 101
    label "yearbook"
  ]
  node [
    id 102
    label "uruchomienie"
  ]
  node [
    id 103
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 104
    label "nauka_ekonomiczna"
  ]
  node [
    id 105
    label "supernadz&#243;r"
  ]
  node [
    id 106
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 107
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 108
    label "absolutorium"
  ]
  node [
    id 109
    label "podupada&#263;"
  ]
  node [
    id 110
    label "nap&#322;ywanie"
  ]
  node [
    id 111
    label "podupadanie"
  ]
  node [
    id 112
    label "kwestor"
  ]
  node [
    id 113
    label "uruchamia&#263;"
  ]
  node [
    id 114
    label "mienie"
  ]
  node [
    id 115
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 116
    label "uruchamianie"
  ]
  node [
    id 117
    label "czynnik_produkcji"
  ]
  node [
    id 118
    label "jawny"
  ]
  node [
    id 119
    label "upublicznienie"
  ]
  node [
    id 120
    label "upublicznianie"
  ]
  node [
    id 121
    label "publicznie"
  ]
  node [
    id 122
    label "spis"
  ]
  node [
    id 123
    label "sheet"
  ]
  node [
    id 124
    label "gazeta"
  ]
  node [
    id 125
    label "diariusz"
  ]
  node [
    id 126
    label "pami&#281;tnik"
  ]
  node [
    id 127
    label "journal"
  ]
  node [
    id 128
    label "ksi&#281;ga"
  ]
  node [
    id 129
    label "program_informacyjny"
  ]
  node [
    id 130
    label "odwodnienie"
  ]
  node [
    id 131
    label "konstytucja"
  ]
  node [
    id 132
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 133
    label "substancja_chemiczna"
  ]
  node [
    id 134
    label "bratnia_dusza"
  ]
  node [
    id 135
    label "zwi&#261;zanie"
  ]
  node [
    id 136
    label "lokant"
  ]
  node [
    id 137
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 138
    label "zwi&#261;za&#263;"
  ]
  node [
    id 139
    label "organizacja"
  ]
  node [
    id 140
    label "odwadnia&#263;"
  ]
  node [
    id 141
    label "marriage"
  ]
  node [
    id 142
    label "marketing_afiliacyjny"
  ]
  node [
    id 143
    label "bearing"
  ]
  node [
    id 144
    label "wi&#261;zanie"
  ]
  node [
    id 145
    label "odwadnianie"
  ]
  node [
    id 146
    label "koligacja"
  ]
  node [
    id 147
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 148
    label "odwodni&#263;"
  ]
  node [
    id 149
    label "azeotrop"
  ]
  node [
    id 150
    label "powi&#261;zanie"
  ]
  node [
    id 151
    label "rule"
  ]
  node [
    id 152
    label "polecenie"
  ]
  node [
    id 153
    label "arrangement"
  ]
  node [
    id 154
    label "zarz&#261;dzenie"
  ]
  node [
    id 155
    label "commission"
  ]
  node [
    id 156
    label "ordonans"
  ]
  node [
    id 157
    label "berylowiec"
  ]
  node [
    id 158
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 159
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 160
    label "mikroradian"
  ]
  node [
    id 161
    label "zadowolenie_si&#281;"
  ]
  node [
    id 162
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 163
    label "content"
  ]
  node [
    id 164
    label "jednostka_promieniowania"
  ]
  node [
    id 165
    label "miliradian"
  ]
  node [
    id 166
    label "jednostka"
  ]
  node [
    id 167
    label "Goebbels"
  ]
  node [
    id 168
    label "Sto&#322;ypin"
  ]
  node [
    id 169
    label "rz&#261;d"
  ]
  node [
    id 170
    label "dostojnik"
  ]
  node [
    id 171
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 172
    label "oktober"
  ]
  node [
    id 173
    label "temat"
  ]
  node [
    id 174
    label "kognicja"
  ]
  node [
    id 175
    label "idea"
  ]
  node [
    id 176
    label "szczeg&#243;&#322;"
  ]
  node [
    id 177
    label "rzecz"
  ]
  node [
    id 178
    label "wydarzenie"
  ]
  node [
    id 179
    label "przes&#322;anka"
  ]
  node [
    id 180
    label "rozprawa"
  ]
  node [
    id 181
    label "object"
  ]
  node [
    id 182
    label "proposition"
  ]
  node [
    id 183
    label "zorganizowa&#263;"
  ]
  node [
    id 184
    label "sta&#263;_si&#281;"
  ]
  node [
    id 185
    label "compose"
  ]
  node [
    id 186
    label "przygotowa&#263;"
  ]
  node [
    id 187
    label "cause"
  ]
  node [
    id 188
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 189
    label "create"
  ]
  node [
    id 190
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 191
    label "ministerium"
  ]
  node [
    id 192
    label "resort"
  ]
  node [
    id 193
    label "urz&#261;d"
  ]
  node [
    id 194
    label "MSW"
  ]
  node [
    id 195
    label "departament"
  ]
  node [
    id 196
    label "NKWD"
  ]
  node [
    id 197
    label "rynek"
  ]
  node [
    id 198
    label "pole"
  ]
  node [
    id 199
    label "szkolnictwo"
  ]
  node [
    id 200
    label "przemys&#322;"
  ]
  node [
    id 201
    label "gospodarka_wodna"
  ]
  node [
    id 202
    label "fabryka"
  ]
  node [
    id 203
    label "rolnictwo"
  ]
  node [
    id 204
    label "gospodarka_le&#347;na"
  ]
  node [
    id 205
    label "gospodarowa&#263;"
  ]
  node [
    id 206
    label "sektor_prywatny"
  ]
  node [
    id 207
    label "obronno&#347;&#263;"
  ]
  node [
    id 208
    label "obora"
  ]
  node [
    id 209
    label "mieszkalnictwo"
  ]
  node [
    id 210
    label "sektor_publiczny"
  ]
  node [
    id 211
    label "czerwona_strefa"
  ]
  node [
    id 212
    label "struktura"
  ]
  node [
    id 213
    label "stodo&#322;a"
  ]
  node [
    id 214
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 215
    label "produkowanie"
  ]
  node [
    id 216
    label "gospodarowanie"
  ]
  node [
    id 217
    label "agregat_ekonomiczny"
  ]
  node [
    id 218
    label "sch&#322;adza&#263;"
  ]
  node [
    id 219
    label "spichlerz"
  ]
  node [
    id 220
    label "inwentarz"
  ]
  node [
    id 221
    label "transport"
  ]
  node [
    id 222
    label "sch&#322;odzenie"
  ]
  node [
    id 223
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 224
    label "miejsce_pracy"
  ]
  node [
    id 225
    label "wytw&#243;rnia"
  ]
  node [
    id 226
    label "farmaceutyka"
  ]
  node [
    id 227
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 228
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 229
    label "administracja"
  ]
  node [
    id 230
    label "sch&#322;adzanie"
  ]
  node [
    id 231
    label "bankowo&#347;&#263;"
  ]
  node [
    id 232
    label "zasada"
  ]
  node [
    id 233
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 234
    label "regulacja_cen"
  ]
  node [
    id 235
    label "mie&#263;_miejsce"
  ]
  node [
    id 236
    label "chance"
  ]
  node [
    id 237
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 238
    label "alternate"
  ]
  node [
    id 239
    label "naciska&#263;"
  ]
  node [
    id 240
    label "atakowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 61
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 201
  ]
  edge [
    source 21
    target 202
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 21
    target 206
  ]
  edge [
    source 21
    target 207
  ]
  edge [
    source 21
    target 208
  ]
  edge [
    source 21
    target 209
  ]
  edge [
    source 21
    target 210
  ]
  edge [
    source 21
    target 211
  ]
  edge [
    source 21
    target 212
  ]
  edge [
    source 21
    target 213
  ]
  edge [
    source 21
    target 214
  ]
  edge [
    source 21
    target 215
  ]
  edge [
    source 21
    target 216
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 219
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 223
  ]
  edge [
    source 21
    target 224
  ]
  edge [
    source 21
    target 225
  ]
  edge [
    source 21
    target 226
  ]
  edge [
    source 21
    target 227
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 21
    target 229
  ]
  edge [
    source 21
    target 230
  ]
  edge [
    source 21
    target 231
  ]
  edge [
    source 21
    target 232
  ]
  edge [
    source 21
    target 233
  ]
  edge [
    source 21
    target 234
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 235
  ]
  edge [
    source 24
    target 236
  ]
  edge [
    source 24
    target 237
  ]
  edge [
    source 24
    target 238
  ]
  edge [
    source 24
    target 239
  ]
  edge [
    source 24
    target 240
  ]
]
