graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.1004566210045663
  density 0.009635122114699845
  graphCliqueNumber 3
  node [
    id 0
    label "ten"
    origin "text"
  ]
  node [
    id 1
    label "por"
    origin "text"
  ]
  node [
    id 2
    label "energia"
    origin "text"
  ]
  node [
    id 3
    label "elektryczny"
    origin "text"
  ]
  node [
    id 4
    label "pozyskiwa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ziemniak"
    origin "text"
  ]
  node [
    id 6
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 7
    label "przedmiot"
    origin "text"
  ]
  node [
    id 8
    label "jedynie"
    origin "text"
  ]
  node [
    id 9
    label "szkolny"
    origin "text"
  ]
  node [
    id 10
    label "lub"
    origin "text"
  ]
  node [
    id 11
    label "domowy"
    origin "text"
  ]
  node [
    id 12
    label "eksperyment"
    origin "text"
  ]
  node [
    id 13
    label "naukowiec"
    origin "text"
  ]
  node [
    id 14
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 15
    label "zbada&#263;"
    origin "text"
  ]
  node [
    id 16
    label "ukry&#263;"
    origin "text"
  ]
  node [
    id 17
    label "potencja&#322;"
    origin "text"
  ]
  node [
    id 18
    label "powa&#380;nie"
    origin "text"
  ]
  node [
    id 19
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "prosta"
    origin "text"
  ]
  node [
    id 21
    label "ogniwo"
    origin "text"
  ]
  node [
    id 22
    label "galwaniczny"
    origin "text"
  ]
  node [
    id 23
    label "jerozolima"
    origin "text"
  ]
  node [
    id 24
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 25
    label "si&#281;"
    origin "text"
  ]
  node [
    id 26
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 27
    label "gotowany"
    origin "text"
  ]
  node [
    id 28
    label "ziemniaczek"
    origin "text"
  ]
  node [
    id 29
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 30
    label "by&#263;"
    origin "text"
  ]
  node [
    id 31
    label "ca&#322;kiem"
    origin "text"
  ]
  node [
    id 32
    label "wydajny"
    origin "text"
  ]
  node [
    id 33
    label "&#378;r&#243;d&#322;o"
    origin "text"
  ]
  node [
    id 34
    label "pr&#261;d"
    origin "text"
  ]
  node [
    id 35
    label "okre&#347;lony"
  ]
  node [
    id 36
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 37
    label "otw&#243;r"
  ]
  node [
    id 38
    label "w&#322;oszczyzna"
  ]
  node [
    id 39
    label "warzywo"
  ]
  node [
    id 40
    label "czosnek"
  ]
  node [
    id 41
    label "kapelusz"
  ]
  node [
    id 42
    label "uj&#347;cie"
  ]
  node [
    id 43
    label "egzergia"
  ]
  node [
    id 44
    label "kwant_energii"
  ]
  node [
    id 45
    label "szwung"
  ]
  node [
    id 46
    label "energy"
  ]
  node [
    id 47
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 48
    label "cecha"
  ]
  node [
    id 49
    label "emitowanie"
  ]
  node [
    id 50
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 51
    label "power"
  ]
  node [
    id 52
    label "zjawisko"
  ]
  node [
    id 53
    label "emitowa&#263;"
  ]
  node [
    id 54
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 55
    label "elektrycznie"
  ]
  node [
    id 56
    label "uzyskiwa&#263;"
  ]
  node [
    id 57
    label "tease"
  ]
  node [
    id 58
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 59
    label "wytwarza&#263;"
  ]
  node [
    id 60
    label "take"
  ]
  node [
    id 61
    label "bylina"
  ]
  node [
    id 62
    label "psianka"
  ]
  node [
    id 63
    label "m&#261;ka_ziemniaczana"
  ]
  node [
    id 64
    label "potato"
  ]
  node [
    id 65
    label "ro&#347;lina"
  ]
  node [
    id 66
    label "ba&#322;aban"
  ]
  node [
    id 67
    label "ro&#347;lina_bulwiasta"
  ]
  node [
    id 68
    label "p&#281;t&#243;wka"
  ]
  node [
    id 69
    label "ro&#347;lina_truj&#261;ca"
  ]
  node [
    id 70
    label "grula"
  ]
  node [
    id 71
    label "bulwa_p&#281;dowa"
  ]
  node [
    id 72
    label "partnerka"
  ]
  node [
    id 73
    label "robienie"
  ]
  node [
    id 74
    label "kr&#261;&#380;enie"
  ]
  node [
    id 75
    label "rzecz"
  ]
  node [
    id 76
    label "zbacza&#263;"
  ]
  node [
    id 77
    label "entity"
  ]
  node [
    id 78
    label "element"
  ]
  node [
    id 79
    label "omawia&#263;"
  ]
  node [
    id 80
    label "om&#243;wi&#263;"
  ]
  node [
    id 81
    label "sponiewiera&#263;"
  ]
  node [
    id 82
    label "sponiewieranie"
  ]
  node [
    id 83
    label "omawianie"
  ]
  node [
    id 84
    label "charakter"
  ]
  node [
    id 85
    label "program_nauczania"
  ]
  node [
    id 86
    label "w&#261;tek"
  ]
  node [
    id 87
    label "thing"
  ]
  node [
    id 88
    label "zboczenie"
  ]
  node [
    id 89
    label "zbaczanie"
  ]
  node [
    id 90
    label "tre&#347;&#263;"
  ]
  node [
    id 91
    label "tematyka"
  ]
  node [
    id 92
    label "istota"
  ]
  node [
    id 93
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 94
    label "kultura"
  ]
  node [
    id 95
    label "zboczy&#263;"
  ]
  node [
    id 96
    label "discipline"
  ]
  node [
    id 97
    label "om&#243;wienie"
  ]
  node [
    id 98
    label "szkolnie"
  ]
  node [
    id 99
    label "szkoleniowy"
  ]
  node [
    id 100
    label "podstawowy"
  ]
  node [
    id 101
    label "prosty"
  ]
  node [
    id 102
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 103
    label "domowo"
  ]
  node [
    id 104
    label "budynkowy"
  ]
  node [
    id 105
    label "innowacja"
  ]
  node [
    id 106
    label "badanie"
  ]
  node [
    id 107
    label "assay"
  ]
  node [
    id 108
    label "obserwowanie"
  ]
  node [
    id 109
    label "Miczurin"
  ]
  node [
    id 110
    label "&#347;ledziciel"
  ]
  node [
    id 111
    label "uczony"
  ]
  node [
    id 112
    label "sta&#263;_si&#281;"
  ]
  node [
    id 113
    label "zrobi&#263;"
  ]
  node [
    id 114
    label "podj&#261;&#263;"
  ]
  node [
    id 115
    label "determine"
  ]
  node [
    id 116
    label "sprawdzi&#263;"
  ]
  node [
    id 117
    label "wybada&#263;"
  ]
  node [
    id 118
    label "examine"
  ]
  node [
    id 119
    label "pozna&#263;"
  ]
  node [
    id 120
    label "zdecydowa&#263;"
  ]
  node [
    id 121
    label "lekarz"
  ]
  node [
    id 122
    label "zachowa&#263;"
  ]
  node [
    id 123
    label "ensconce"
  ]
  node [
    id 124
    label "hide"
  ]
  node [
    id 125
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 126
    label "umie&#347;ci&#263;"
  ]
  node [
    id 127
    label "przytai&#263;"
  ]
  node [
    id 128
    label "wielko&#347;&#263;"
  ]
  node [
    id 129
    label "powa&#380;ny"
  ]
  node [
    id 130
    label "bardzo"
  ]
  node [
    id 131
    label "niema&#322;o"
  ]
  node [
    id 132
    label "gro&#378;nie"
  ]
  node [
    id 133
    label "ci&#281;&#380;ko"
  ]
  node [
    id 134
    label "ci&#281;&#380;ki"
  ]
  node [
    id 135
    label "use"
  ]
  node [
    id 136
    label "krzywdzi&#263;"
  ]
  node [
    id 137
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 138
    label "distribute"
  ]
  node [
    id 139
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 140
    label "give"
  ]
  node [
    id 141
    label "liga&#263;"
  ]
  node [
    id 142
    label "u&#380;ywa&#263;"
  ]
  node [
    id 143
    label "korzysta&#263;"
  ]
  node [
    id 144
    label "czas"
  ]
  node [
    id 145
    label "odcinek"
  ]
  node [
    id 146
    label "proste_sko&#347;ne"
  ]
  node [
    id 147
    label "straight_line"
  ]
  node [
    id 148
    label "punkt"
  ]
  node [
    id 149
    label "trasa"
  ]
  node [
    id 150
    label "krzywa"
  ]
  node [
    id 151
    label "k&#243;&#322;ko"
  ]
  node [
    id 152
    label "&#322;a&#324;cuch"
  ]
  node [
    id 153
    label "czynnik"
  ]
  node [
    id 154
    label "cell"
  ]
  node [
    id 155
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 156
    label "kontakt"
  ]
  node [
    id 157
    label "filia"
  ]
  node [
    id 158
    label "specjalny"
  ]
  node [
    id 159
    label "galwanicznie"
  ]
  node [
    id 160
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 161
    label "represent"
  ]
  node [
    id 162
    label "spowodowa&#263;"
  ]
  node [
    id 163
    label "wyrazi&#263;"
  ]
  node [
    id 164
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 165
    label "przedstawi&#263;"
  ]
  node [
    id 166
    label "testify"
  ]
  node [
    id 167
    label "indicate"
  ]
  node [
    id 168
    label "przeszkoli&#263;"
  ]
  node [
    id 169
    label "udowodni&#263;"
  ]
  node [
    id 170
    label "poinformowa&#263;"
  ]
  node [
    id 171
    label "poda&#263;"
  ]
  node [
    id 172
    label "point"
  ]
  node [
    id 173
    label "ciastko"
  ]
  node [
    id 174
    label "uprawi&#263;"
  ]
  node [
    id 175
    label "gotowy"
  ]
  node [
    id 176
    label "might"
  ]
  node [
    id 177
    label "si&#281;ga&#263;"
  ]
  node [
    id 178
    label "trwa&#263;"
  ]
  node [
    id 179
    label "obecno&#347;&#263;"
  ]
  node [
    id 180
    label "stan"
  ]
  node [
    id 181
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 182
    label "stand"
  ]
  node [
    id 183
    label "mie&#263;_miejsce"
  ]
  node [
    id 184
    label "uczestniczy&#263;"
  ]
  node [
    id 185
    label "chodzi&#263;"
  ]
  node [
    id 186
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 187
    label "equal"
  ]
  node [
    id 188
    label "wniwecz"
  ]
  node [
    id 189
    label "zupe&#322;ny"
  ]
  node [
    id 190
    label "nasilony"
  ]
  node [
    id 191
    label "wydajnie"
  ]
  node [
    id 192
    label "dobry"
  ]
  node [
    id 193
    label "efektywny"
  ]
  node [
    id 194
    label "bogaty"
  ]
  node [
    id 195
    label "bra&#263;_si&#281;"
  ]
  node [
    id 196
    label "&#347;wiadectwo"
  ]
  node [
    id 197
    label "przyczyna"
  ]
  node [
    id 198
    label "matuszka"
  ]
  node [
    id 199
    label "geneza"
  ]
  node [
    id 200
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 201
    label "kamena"
  ]
  node [
    id 202
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 203
    label "pocz&#261;tek"
  ]
  node [
    id 204
    label "poci&#261;ganie"
  ]
  node [
    id 205
    label "rezultat"
  ]
  node [
    id 206
    label "ciek_wodny"
  ]
  node [
    id 207
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 208
    label "subject"
  ]
  node [
    id 209
    label "system"
  ]
  node [
    id 210
    label "przep&#322;yw"
  ]
  node [
    id 211
    label "ideologia"
  ]
  node [
    id 212
    label "ruch"
  ]
  node [
    id 213
    label "dreszcz"
  ]
  node [
    id 214
    label "praktyka"
  ]
  node [
    id 215
    label "metoda"
  ]
  node [
    id 216
    label "apparent_motion"
  ]
  node [
    id 217
    label "electricity"
  ]
  node [
    id 218
    label "przyp&#322;yw"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 17
    target 48
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 135
  ]
  edge [
    source 19
    target 136
  ]
  edge [
    source 19
    target 137
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 24
    target 161
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 162
  ]
  edge [
    source 26
    target 163
  ]
  edge [
    source 26
    target 164
  ]
  edge [
    source 26
    target 165
  ]
  edge [
    source 26
    target 166
  ]
  edge [
    source 26
    target 167
  ]
  edge [
    source 26
    target 168
  ]
  edge [
    source 26
    target 169
  ]
  edge [
    source 26
    target 170
  ]
  edge [
    source 26
    target 171
  ]
  edge [
    source 26
    target 172
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 173
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 174
  ]
  edge [
    source 29
    target 175
  ]
  edge [
    source 29
    target 176
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 177
  ]
  edge [
    source 30
    target 178
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 30
    target 181
  ]
  edge [
    source 30
    target 182
  ]
  edge [
    source 30
    target 183
  ]
  edge [
    source 30
    target 184
  ]
  edge [
    source 30
    target 185
  ]
  edge [
    source 30
    target 186
  ]
  edge [
    source 30
    target 187
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 188
  ]
  edge [
    source 31
    target 189
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 190
  ]
  edge [
    source 32
    target 191
  ]
  edge [
    source 32
    target 192
  ]
  edge [
    source 32
    target 193
  ]
  edge [
    source 32
    target 194
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 195
  ]
  edge [
    source 33
    target 196
  ]
  edge [
    source 33
    target 197
  ]
  edge [
    source 33
    target 198
  ]
  edge [
    source 33
    target 199
  ]
  edge [
    source 33
    target 200
  ]
  edge [
    source 33
    target 201
  ]
  edge [
    source 33
    target 202
  ]
  edge [
    source 33
    target 153
  ]
  edge [
    source 33
    target 203
  ]
  edge [
    source 33
    target 204
  ]
  edge [
    source 33
    target 205
  ]
  edge [
    source 33
    target 206
  ]
  edge [
    source 33
    target 207
  ]
  edge [
    source 33
    target 208
  ]
  edge [
    source 34
    target 209
  ]
  edge [
    source 34
    target 210
  ]
  edge [
    source 34
    target 47
  ]
  edge [
    source 34
    target 211
  ]
  edge [
    source 34
    target 212
  ]
  edge [
    source 34
    target 213
  ]
  edge [
    source 34
    target 214
  ]
  edge [
    source 34
    target 215
  ]
  edge [
    source 34
    target 216
  ]
  edge [
    source 34
    target 217
  ]
  edge [
    source 34
    target 52
  ]
  edge [
    source 34
    target 218
  ]
]
