graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.0217391304347827
  density 0.011047754811119031
  graphCliqueNumber 2
  node [
    id 0
    label "unia"
    origin "text"
  ]
  node [
    id 1
    label "europejski"
    origin "text"
  ]
  node [
    id 2
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "obecnie"
    origin "text"
  ]
  node [
    id 4
    label "nad"
    origin "text"
  ]
  node [
    id 5
    label "nowa"
    origin "text"
  ]
  node [
    id 6
    label "strategia"
    origin "text"
  ]
  node [
    id 7
    label "rozw&#243;j"
    origin "text"
  ]
  node [
    id 8
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 9
    label "informacyjny"
    origin "text"
  ]
  node [
    id 10
    label "lata"
    origin "text"
  ]
  node [
    id 11
    label "rama"
    origin "text"
  ]
  node [
    id 12
    label "praca"
    origin "text"
  ]
  node [
    id 13
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "konsultacja"
    origin "text"
  ]
  node [
    id 15
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 16
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 17
    label "wynik"
    origin "text"
  ]
  node [
    id 18
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 19
    label "uwzgl&#281;dni&#263;"
    origin "text"
  ]
  node [
    id 20
    label "podczas"
    origin "text"
  ]
  node [
    id 21
    label "formu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "uk&#322;ad"
  ]
  node [
    id 23
    label "partia"
  ]
  node [
    id 24
    label "organizacja"
  ]
  node [
    id 25
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 26
    label "Unia_Europejska"
  ]
  node [
    id 27
    label "combination"
  ]
  node [
    id 28
    label "union"
  ]
  node [
    id 29
    label "Unia"
  ]
  node [
    id 30
    label "European"
  ]
  node [
    id 31
    label "po_europejsku"
  ]
  node [
    id 32
    label "charakterystyczny"
  ]
  node [
    id 33
    label "europejsko"
  ]
  node [
    id 34
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 35
    label "typowy"
  ]
  node [
    id 36
    label "endeavor"
  ]
  node [
    id 37
    label "funkcjonowa&#263;"
  ]
  node [
    id 38
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 39
    label "mie&#263;_miejsce"
  ]
  node [
    id 40
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 41
    label "dzia&#322;a&#263;"
  ]
  node [
    id 42
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 43
    label "work"
  ]
  node [
    id 44
    label "bangla&#263;"
  ]
  node [
    id 45
    label "do"
  ]
  node [
    id 46
    label "maszyna"
  ]
  node [
    id 47
    label "tryb"
  ]
  node [
    id 48
    label "dziama&#263;"
  ]
  node [
    id 49
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 50
    label "podejmowa&#263;"
  ]
  node [
    id 51
    label "ninie"
  ]
  node [
    id 52
    label "aktualny"
  ]
  node [
    id 53
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 54
    label "gwiazda"
  ]
  node [
    id 55
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 56
    label "dokument"
  ]
  node [
    id 57
    label "gra"
  ]
  node [
    id 58
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 59
    label "wzorzec_projektowy"
  ]
  node [
    id 60
    label "pocz&#261;tki"
  ]
  node [
    id 61
    label "doktryna"
  ]
  node [
    id 62
    label "program"
  ]
  node [
    id 63
    label "plan"
  ]
  node [
    id 64
    label "metoda"
  ]
  node [
    id 65
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 66
    label "operacja"
  ]
  node [
    id 67
    label "dziedzina"
  ]
  node [
    id 68
    label "wrinkle"
  ]
  node [
    id 69
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 70
    label "procedura"
  ]
  node [
    id 71
    label "process"
  ]
  node [
    id 72
    label "cycle"
  ]
  node [
    id 73
    label "proces"
  ]
  node [
    id 74
    label "&#380;ycie"
  ]
  node [
    id 75
    label "z&#322;ote_czasy"
  ]
  node [
    id 76
    label "proces_biologiczny"
  ]
  node [
    id 77
    label "pole"
  ]
  node [
    id 78
    label "kastowo&#347;&#263;"
  ]
  node [
    id 79
    label "ludzie_pracy"
  ]
  node [
    id 80
    label "community"
  ]
  node [
    id 81
    label "status"
  ]
  node [
    id 82
    label "cywilizacja"
  ]
  node [
    id 83
    label "pozaklasowy"
  ]
  node [
    id 84
    label "aspo&#322;eczny"
  ]
  node [
    id 85
    label "uwarstwienie"
  ]
  node [
    id 86
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 87
    label "elita"
  ]
  node [
    id 88
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 89
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 90
    label "klasa"
  ]
  node [
    id 91
    label "informacyjnie"
  ]
  node [
    id 92
    label "summer"
  ]
  node [
    id 93
    label "czas"
  ]
  node [
    id 94
    label "zakres"
  ]
  node [
    id 95
    label "dodatek"
  ]
  node [
    id 96
    label "struktura"
  ]
  node [
    id 97
    label "stela&#380;"
  ]
  node [
    id 98
    label "za&#322;o&#380;enie"
  ]
  node [
    id 99
    label "human_body"
  ]
  node [
    id 100
    label "szablon"
  ]
  node [
    id 101
    label "oprawa"
  ]
  node [
    id 102
    label "paczka"
  ]
  node [
    id 103
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 104
    label "obramowanie"
  ]
  node [
    id 105
    label "pojazd"
  ]
  node [
    id 106
    label "postawa"
  ]
  node [
    id 107
    label "element_konstrukcyjny"
  ]
  node [
    id 108
    label "stosunek_pracy"
  ]
  node [
    id 109
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 110
    label "benedykty&#324;ski"
  ]
  node [
    id 111
    label "pracowanie"
  ]
  node [
    id 112
    label "zaw&#243;d"
  ]
  node [
    id 113
    label "kierownictwo"
  ]
  node [
    id 114
    label "zmiana"
  ]
  node [
    id 115
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 116
    label "wytw&#243;r"
  ]
  node [
    id 117
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 118
    label "tynkarski"
  ]
  node [
    id 119
    label "czynnik_produkcji"
  ]
  node [
    id 120
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 121
    label "zobowi&#261;zanie"
  ]
  node [
    id 122
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 123
    label "czynno&#347;&#263;"
  ]
  node [
    id 124
    label "tyrka"
  ]
  node [
    id 125
    label "siedziba"
  ]
  node [
    id 126
    label "poda&#380;_pracy"
  ]
  node [
    id 127
    label "miejsce"
  ]
  node [
    id 128
    label "zak&#322;ad"
  ]
  node [
    id 129
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 130
    label "najem"
  ]
  node [
    id 131
    label "control"
  ]
  node [
    id 132
    label "eksponowa&#263;"
  ]
  node [
    id 133
    label "kre&#347;li&#263;"
  ]
  node [
    id 134
    label "g&#243;rowa&#263;"
  ]
  node [
    id 135
    label "message"
  ]
  node [
    id 136
    label "partner"
  ]
  node [
    id 137
    label "string"
  ]
  node [
    id 138
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 139
    label "przesuwa&#263;"
  ]
  node [
    id 140
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 141
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 142
    label "powodowa&#263;"
  ]
  node [
    id 143
    label "kierowa&#263;"
  ]
  node [
    id 144
    label "robi&#263;"
  ]
  node [
    id 145
    label "manipulate"
  ]
  node [
    id 146
    label "&#380;y&#263;"
  ]
  node [
    id 147
    label "navigate"
  ]
  node [
    id 148
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 149
    label "ukierunkowywa&#263;"
  ]
  node [
    id 150
    label "linia_melodyczna"
  ]
  node [
    id 151
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 152
    label "prowadzenie"
  ]
  node [
    id 153
    label "tworzy&#263;"
  ]
  node [
    id 154
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 155
    label "sterowa&#263;"
  ]
  node [
    id 156
    label "krzywa"
  ]
  node [
    id 157
    label "ocena"
  ]
  node [
    id 158
    label "narada"
  ]
  node [
    id 159
    label "porada"
  ]
  node [
    id 160
    label "niepubliczny"
  ]
  node [
    id 161
    label "spo&#322;ecznie"
  ]
  node [
    id 162
    label "publiczny"
  ]
  node [
    id 163
    label "typ"
  ]
  node [
    id 164
    label "dzia&#322;anie"
  ]
  node [
    id 165
    label "przyczyna"
  ]
  node [
    id 166
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 167
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 168
    label "zaokr&#261;glenie"
  ]
  node [
    id 169
    label "event"
  ]
  node [
    id 170
    label "rezultat"
  ]
  node [
    id 171
    label "proceed"
  ]
  node [
    id 172
    label "catch"
  ]
  node [
    id 173
    label "pozosta&#263;"
  ]
  node [
    id 174
    label "osta&#263;_si&#281;"
  ]
  node [
    id 175
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 176
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 177
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 178
    label "change"
  ]
  node [
    id 179
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 180
    label "wzi&#261;&#263;"
  ]
  node [
    id 181
    label "include"
  ]
  node [
    id 182
    label "komunikowa&#263;"
  ]
  node [
    id 183
    label "convey"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 42
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
]
