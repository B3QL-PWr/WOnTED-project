graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9534883720930232
  density 0.046511627906976744
  graphCliqueNumber 2
  node [
    id 0
    label "dobry"
    origin "text"
  ]
  node [
    id 1
    label "wiecz&#243;r"
    origin "text"
  ]
  node [
    id 2
    label "xxx"
    origin "text"
  ]
  node [
    id 3
    label "czym"
    origin "text"
  ]
  node [
    id 4
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 5
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 6
    label "pomy&#347;lny"
  ]
  node [
    id 7
    label "skuteczny"
  ]
  node [
    id 8
    label "moralny"
  ]
  node [
    id 9
    label "korzystny"
  ]
  node [
    id 10
    label "odpowiedni"
  ]
  node [
    id 11
    label "zwrot"
  ]
  node [
    id 12
    label "dobrze"
  ]
  node [
    id 13
    label "pozytywny"
  ]
  node [
    id 14
    label "grzeczny"
  ]
  node [
    id 15
    label "powitanie"
  ]
  node [
    id 16
    label "mi&#322;y"
  ]
  node [
    id 17
    label "dobroczynny"
  ]
  node [
    id 18
    label "pos&#322;uszny"
  ]
  node [
    id 19
    label "ca&#322;y"
  ]
  node [
    id 20
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 21
    label "czw&#243;rka"
  ]
  node [
    id 22
    label "spokojny"
  ]
  node [
    id 23
    label "&#347;mieszny"
  ]
  node [
    id 24
    label "drogi"
  ]
  node [
    id 25
    label "zach&#243;d"
  ]
  node [
    id 26
    label "vesper"
  ]
  node [
    id 27
    label "spotkanie"
  ]
  node [
    id 28
    label "przyj&#281;cie"
  ]
  node [
    id 29
    label "pora"
  ]
  node [
    id 30
    label "dzie&#324;"
  ]
  node [
    id 31
    label "night"
  ]
  node [
    id 32
    label "by&#263;"
  ]
  node [
    id 33
    label "uprawi&#263;"
  ]
  node [
    id 34
    label "gotowy"
  ]
  node [
    id 35
    label "might"
  ]
  node [
    id 36
    label "help"
  ]
  node [
    id 37
    label "aid"
  ]
  node [
    id 38
    label "u&#322;atwi&#263;"
  ]
  node [
    id 39
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 40
    label "concur"
  ]
  node [
    id 41
    label "zrobi&#263;"
  ]
  node [
    id 42
    label "zaskutkowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
]
