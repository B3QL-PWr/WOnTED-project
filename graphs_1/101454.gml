graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.646153846153846
  density 0.041346153846153845
  graphCliqueNumber 8
  node [
    id 0
    label "ustawa"
    origin "text"
  ]
  node [
    id 1
    label "ustalenie"
    origin "text"
  ]
  node [
    id 2
    label "oszacowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 4
    label "strata"
    origin "text"
  ]
  node [
    id 5
    label "wojenny"
    origin "text"
  ]
  node [
    id 6
    label "Karta_Nauczyciela"
  ]
  node [
    id 7
    label "marc&#243;wka"
  ]
  node [
    id 8
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 9
    label "akt"
  ]
  node [
    id 10
    label "przej&#347;&#263;"
  ]
  node [
    id 11
    label "charter"
  ]
  node [
    id 12
    label "przej&#347;cie"
  ]
  node [
    id 13
    label "zrobienie"
  ]
  node [
    id 14
    label "zdecydowanie"
  ]
  node [
    id 15
    label "czynno&#347;&#263;"
  ]
  node [
    id 16
    label "spowodowanie"
  ]
  node [
    id 17
    label "appointment"
  ]
  node [
    id 18
    label "localization"
  ]
  node [
    id 19
    label "informacja"
  ]
  node [
    id 20
    label "decyzja"
  ]
  node [
    id 21
    label "umocnienie"
  ]
  node [
    id 22
    label "visualize"
  ]
  node [
    id 23
    label "policzy&#263;"
  ]
  node [
    id 24
    label "okre&#347;li&#263;"
  ]
  node [
    id 25
    label "p&#322;acenie"
  ]
  node [
    id 26
    label "znaczenie"
  ]
  node [
    id 27
    label "sk&#322;adanie"
  ]
  node [
    id 28
    label "service"
  ]
  node [
    id 29
    label "czynienie_dobra"
  ]
  node [
    id 30
    label "informowanie"
  ]
  node [
    id 31
    label "command"
  ]
  node [
    id 32
    label "opowiadanie"
  ]
  node [
    id 33
    label "koszt_rodzajowy"
  ]
  node [
    id 34
    label "pracowanie"
  ]
  node [
    id 35
    label "przekonywanie"
  ]
  node [
    id 36
    label "wyraz"
  ]
  node [
    id 37
    label "us&#322;uga"
  ]
  node [
    id 38
    label "performance"
  ]
  node [
    id 39
    label "zobowi&#261;zanie"
  ]
  node [
    id 40
    label "zniszczenie"
  ]
  node [
    id 41
    label "niepowodzenie"
  ]
  node [
    id 42
    label "ubytek"
  ]
  node [
    id 43
    label "bilans"
  ]
  node [
    id 44
    label "szwank"
  ]
  node [
    id 45
    label "bojowo"
  ]
  node [
    id 46
    label "typowy"
  ]
  node [
    id 47
    label "ojciec"
  ]
  node [
    id 48
    label "ustali&#263;"
  ]
  node [
    id 49
    label "i"
  ]
  node [
    id 50
    label "sejm"
  ]
  node [
    id 51
    label "ustawodawczy"
  ]
  node [
    id 52
    label "dziennik"
  ]
  node [
    id 53
    label "prawy"
  ]
  node [
    id 54
    label "g&#322;&#243;wny"
  ]
  node [
    id 55
    label "urz&#261;d"
  ]
  node [
    id 56
    label "likwidacyjny"
  ]
  node [
    id 57
    label "komisja"
  ]
  node [
    id 58
    label "szacunkowy"
  ]
  node [
    id 59
    label "miejscowy"
  ]
  node [
    id 60
    label "miejski"
  ]
  node [
    id 61
    label "ministerstwo"
  ]
  node [
    id 62
    label "skarb"
  ]
  node [
    id 63
    label "kodeks"
  ]
  node [
    id 64
    label "karny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 63
    target 64
  ]
]
