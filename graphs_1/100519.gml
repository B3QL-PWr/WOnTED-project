graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.06436781609195402
  graphCliqueNumber 3
  node [
    id 0
    label "obelisk"
    origin "text"
  ]
  node [
    id 1
    label "przy"
    origin "text"
  ]
  node [
    id 2
    label "ulica"
    origin "text"
  ]
  node [
    id 3
    label "lindley"
    origin "text"
  ]
  node [
    id 4
    label "pomnik"
  ]
  node [
    id 5
    label "&#347;rodowisko"
  ]
  node [
    id 6
    label "miasteczko"
  ]
  node [
    id 7
    label "streetball"
  ]
  node [
    id 8
    label "pierzeja"
  ]
  node [
    id 9
    label "grupa"
  ]
  node [
    id 10
    label "pas_ruchu"
  ]
  node [
    id 11
    label "pas_rozdzielczy"
  ]
  node [
    id 12
    label "jezdnia"
  ]
  node [
    id 13
    label "droga"
  ]
  node [
    id 14
    label "korona_drogi"
  ]
  node [
    id 15
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 16
    label "chodnik"
  ]
  node [
    id 17
    label "arteria"
  ]
  node [
    id 18
    label "Broadway"
  ]
  node [
    id 19
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 20
    label "wysepka"
  ]
  node [
    id 21
    label "autostrada"
  ]
  node [
    id 22
    label "szpital"
  ]
  node [
    id 23
    label "dzieci&#261;tko"
  ]
  node [
    id 24
    label "Jezus"
  ]
  node [
    id 25
    label "plac"
  ]
  node [
    id 26
    label "powstaniec"
  ]
  node [
    id 27
    label "warszawa"
  ]
  node [
    id 28
    label "&#347;wi&#281;ty"
  ]
  node [
    id 29
    label "Wincenty"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
]
