graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.014925373134328
  density 0.015149814835596454
  graphCliqueNumber 3
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "uczelnia"
    origin "text"
  ]
  node [
    id 2
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 3
    label "czas"
    origin "text"
  ]
  node [
    id 4
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 5
    label "zaobserwowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "niepokoj&#261;cy"
    origin "text"
  ]
  node [
    id 7
    label "zjawisko"
    origin "text"
  ]
  node [
    id 8
    label "jaki"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "coraz"
    origin "text"
  ]
  node [
    id 11
    label "cz&#281;sty"
    origin "text"
  ]
  node [
    id 12
    label "obecno&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "tak_zwany"
    origin "text"
  ]
  node [
    id 14
    label "pseudonauka"
    origin "text"
  ]
  node [
    id 15
    label "uczelniany"
    origin "text"
  ]
  node [
    id 16
    label "mur"
    origin "text"
  ]
  node [
    id 17
    label "lacki"
  ]
  node [
    id 18
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 19
    label "przedmiot"
  ]
  node [
    id 20
    label "sztajer"
  ]
  node [
    id 21
    label "drabant"
  ]
  node [
    id 22
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 23
    label "polak"
  ]
  node [
    id 24
    label "pierogi_ruskie"
  ]
  node [
    id 25
    label "krakowiak"
  ]
  node [
    id 26
    label "Polish"
  ]
  node [
    id 27
    label "j&#281;zyk"
  ]
  node [
    id 28
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 29
    label "oberek"
  ]
  node [
    id 30
    label "po_polsku"
  ]
  node [
    id 31
    label "mazur"
  ]
  node [
    id 32
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 33
    label "chodzony"
  ]
  node [
    id 34
    label "skoczny"
  ]
  node [
    id 35
    label "ryba_po_grecku"
  ]
  node [
    id 36
    label "goniony"
  ]
  node [
    id 37
    label "polsko"
  ]
  node [
    id 38
    label "rektorat"
  ]
  node [
    id 39
    label "podkanclerz"
  ]
  node [
    id 40
    label "kanclerz"
  ]
  node [
    id 41
    label "kwestura"
  ]
  node [
    id 42
    label "miasteczko_studenckie"
  ]
  node [
    id 43
    label "school"
  ]
  node [
    id 44
    label "senat"
  ]
  node [
    id 45
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 46
    label "wyk&#322;adanie"
  ]
  node [
    id 47
    label "promotorstwo"
  ]
  node [
    id 48
    label "szko&#322;a"
  ]
  node [
    id 49
    label "daleki"
  ]
  node [
    id 50
    label "d&#322;ugo"
  ]
  node [
    id 51
    label "ruch"
  ]
  node [
    id 52
    label "czasokres"
  ]
  node [
    id 53
    label "trawienie"
  ]
  node [
    id 54
    label "kategoria_gramatyczna"
  ]
  node [
    id 55
    label "period"
  ]
  node [
    id 56
    label "odczyt"
  ]
  node [
    id 57
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 58
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 59
    label "chwila"
  ]
  node [
    id 60
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 61
    label "poprzedzenie"
  ]
  node [
    id 62
    label "koniugacja"
  ]
  node [
    id 63
    label "dzieje"
  ]
  node [
    id 64
    label "poprzedzi&#263;"
  ]
  node [
    id 65
    label "przep&#322;ywanie"
  ]
  node [
    id 66
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 67
    label "odwlekanie_si&#281;"
  ]
  node [
    id 68
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 69
    label "Zeitgeist"
  ]
  node [
    id 70
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 71
    label "okres_czasu"
  ]
  node [
    id 72
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 73
    label "pochodzi&#263;"
  ]
  node [
    id 74
    label "schy&#322;ek"
  ]
  node [
    id 75
    label "czwarty_wymiar"
  ]
  node [
    id 76
    label "chronometria"
  ]
  node [
    id 77
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 78
    label "poprzedzanie"
  ]
  node [
    id 79
    label "pogoda"
  ]
  node [
    id 80
    label "zegar"
  ]
  node [
    id 81
    label "pochodzenie"
  ]
  node [
    id 82
    label "poprzedza&#263;"
  ]
  node [
    id 83
    label "trawi&#263;"
  ]
  node [
    id 84
    label "time_period"
  ]
  node [
    id 85
    label "rachuba_czasu"
  ]
  node [
    id 86
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 87
    label "czasoprzestrze&#324;"
  ]
  node [
    id 88
    label "laba"
  ]
  node [
    id 89
    label "free"
  ]
  node [
    id 90
    label "zobaczy&#263;"
  ]
  node [
    id 91
    label "watch"
  ]
  node [
    id 92
    label "niepokoj&#261;co"
  ]
  node [
    id 93
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 94
    label "krajobraz"
  ]
  node [
    id 95
    label "przywidzenie"
  ]
  node [
    id 96
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 97
    label "boski"
  ]
  node [
    id 98
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 99
    label "proces"
  ]
  node [
    id 100
    label "presence"
  ]
  node [
    id 101
    label "charakter"
  ]
  node [
    id 102
    label "si&#281;ga&#263;"
  ]
  node [
    id 103
    label "trwa&#263;"
  ]
  node [
    id 104
    label "stan"
  ]
  node [
    id 105
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 106
    label "stand"
  ]
  node [
    id 107
    label "mie&#263;_miejsce"
  ]
  node [
    id 108
    label "uczestniczy&#263;"
  ]
  node [
    id 109
    label "chodzi&#263;"
  ]
  node [
    id 110
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 111
    label "equal"
  ]
  node [
    id 112
    label "cz&#281;sto"
  ]
  node [
    id 113
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 114
    label "cecha"
  ]
  node [
    id 115
    label "being"
  ]
  node [
    id 116
    label "dziedzina"
  ]
  node [
    id 117
    label "akademicki"
  ]
  node [
    id 118
    label "trudno&#347;&#263;"
  ]
  node [
    id 119
    label "konstrukcja"
  ]
  node [
    id 120
    label "tynk"
  ]
  node [
    id 121
    label "belkowanie"
  ]
  node [
    id 122
    label "&#347;ciana"
  ]
  node [
    id 123
    label "fola"
  ]
  node [
    id 124
    label "futr&#243;wka"
  ]
  node [
    id 125
    label "rz&#261;d"
  ]
  node [
    id 126
    label "obstruction"
  ]
  node [
    id 127
    label "futbolista"
  ]
  node [
    id 128
    label "budowla"
  ]
  node [
    id 129
    label "gzyms"
  ]
  node [
    id 130
    label "ceg&#322;a"
  ]
  node [
    id 131
    label "niezale&#380;ny"
  ]
  node [
    id 132
    label "zrzeszenie"
  ]
  node [
    id 133
    label "student"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 133
  ]
  edge [
    source 132
    target 133
  ]
]
