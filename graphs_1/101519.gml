graph [
  maxDegree 37
  minDegree 1
  meanDegree 1.975609756097561
  density 0.024390243902439025
  graphCliqueNumber 2
  node [
    id 0
    label "wtedy"
    origin "text"
  ]
  node [
    id 1
    label "dusza"
    origin "text"
  ]
  node [
    id 2
    label "strach"
    origin "text"
  ]
  node [
    id 3
    label "zakrzep&#322;y"
    origin "text"
  ]
  node [
    id 4
    label "ten"
    origin "text"
  ]
  node [
    id 5
    label "lit"
    origin "text"
  ]
  node [
    id 6
    label "kamie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "kiedy&#347;"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "kompleks"
  ]
  node [
    id 10
    label "sfera_afektywna"
  ]
  node [
    id 11
    label "sumienie"
  ]
  node [
    id 12
    label "odwaga"
  ]
  node [
    id 13
    label "pupa"
  ]
  node [
    id 14
    label "sztuka"
  ]
  node [
    id 15
    label "core"
  ]
  node [
    id 16
    label "piek&#322;o"
  ]
  node [
    id 17
    label "pi&#243;ro"
  ]
  node [
    id 18
    label "shape"
  ]
  node [
    id 19
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 20
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 21
    label "charakter"
  ]
  node [
    id 22
    label "mind"
  ]
  node [
    id 23
    label "marrow"
  ]
  node [
    id 24
    label "mikrokosmos"
  ]
  node [
    id 25
    label "byt"
  ]
  node [
    id 26
    label "przestrze&#324;"
  ]
  node [
    id 27
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 28
    label "mi&#281;kisz"
  ]
  node [
    id 29
    label "ego"
  ]
  node [
    id 30
    label "motor"
  ]
  node [
    id 31
    label "osobowo&#347;&#263;"
  ]
  node [
    id 32
    label "rdze&#324;"
  ]
  node [
    id 33
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 34
    label "sztabka"
  ]
  node [
    id 35
    label "lina"
  ]
  node [
    id 36
    label "deformowa&#263;"
  ]
  node [
    id 37
    label "schody"
  ]
  node [
    id 38
    label "seksualno&#347;&#263;"
  ]
  node [
    id 39
    label "deformowanie"
  ]
  node [
    id 40
    label "&#380;elazko"
  ]
  node [
    id 41
    label "instrument_smyczkowy"
  ]
  node [
    id 42
    label "klocek"
  ]
  node [
    id 43
    label "akatyzja"
  ]
  node [
    id 44
    label "phobia"
  ]
  node [
    id 45
    label "ba&#263;_si&#281;"
  ]
  node [
    id 46
    label "emocja"
  ]
  node [
    id 47
    label "zastraszenie"
  ]
  node [
    id 48
    label "zjawa"
  ]
  node [
    id 49
    label "straszyd&#322;o"
  ]
  node [
    id 50
    label "zastraszanie"
  ]
  node [
    id 51
    label "spirit"
  ]
  node [
    id 52
    label "nieruchomy"
  ]
  node [
    id 53
    label "sta&#322;y"
  ]
  node [
    id 54
    label "stwardnia&#322;y"
  ]
  node [
    id 55
    label "okre&#347;lony"
  ]
  node [
    id 56
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 57
    label "litowiec"
  ]
  node [
    id 58
    label "cent"
  ]
  node [
    id 59
    label "Litwa"
  ]
  node [
    id 60
    label "lithium"
  ]
  node [
    id 61
    label "jednostka_monetarna"
  ]
  node [
    id 62
    label "skamienienie"
  ]
  node [
    id 63
    label "z&#322;&#243;g"
  ]
  node [
    id 64
    label "jednostka_avoirdupois"
  ]
  node [
    id 65
    label "tworzywo"
  ]
  node [
    id 66
    label "rock"
  ]
  node [
    id 67
    label "lapidarium"
  ]
  node [
    id 68
    label "minera&#322;_barwny"
  ]
  node [
    id 69
    label "Had&#380;ar"
  ]
  node [
    id 70
    label "autografia"
  ]
  node [
    id 71
    label "rekwizyt_do_gry"
  ]
  node [
    id 72
    label "osad"
  ]
  node [
    id 73
    label "funt"
  ]
  node [
    id 74
    label "mad&#380;ong"
  ]
  node [
    id 75
    label "oczko"
  ]
  node [
    id 76
    label "cube"
  ]
  node [
    id 77
    label "p&#322;ytka"
  ]
  node [
    id 78
    label "domino"
  ]
  node [
    id 79
    label "ci&#281;&#380;ar"
  ]
  node [
    id 80
    label "ska&#322;a"
  ]
  node [
    id 81
    label "kamienienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
]
