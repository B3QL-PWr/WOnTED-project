graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.029126213592233
  density 0.00989817665166943
  graphCliqueNumber 2
  node [
    id 0
    label "strona"
    origin "text"
  ]
  node [
    id 1
    label "internetowy"
    origin "text"
  ]
  node [
    id 2
    label "popularny"
    origin "text"
  ]
  node [
    id 3
    label "pasek"
    origin "text"
  ]
  node [
    id 4
    label "komiksowy"
    origin "text"
  ]
  node [
    id 5
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 6
    label "biurowy"
    origin "text"
  ]
  node [
    id 7
    label "doczeka&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "nowa"
    origin "text"
  ]
  node [
    id 10
    label "ods&#322;ona"
    origin "text"
  ]
  node [
    id 11
    label "otwarty"
    origin "text"
  ]
  node [
    id 12
    label "generowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "przez"
    origin "text"
  ]
  node [
    id 14
    label "u&#380;ytkownik"
    origin "text"
  ]
  node [
    id 15
    label "by&#263;"
    origin "text"
  ]
  node [
    id 16
    label "wielki"
    origin "text"
  ]
  node [
    id 17
    label "fan"
    origin "text"
  ]
  node [
    id 18
    label "dilberta"
    origin "text"
  ]
  node [
    id 19
    label "ale"
    origin "text"
  ]
  node [
    id 20
    label "wspomina&#263;"
    origin "text"
  ]
  node [
    id 21
    label "tym"
    origin "text"
  ]
  node [
    id 22
    label "jeszcze"
    origin "text"
  ]
  node [
    id 23
    label "listopad"
    origin "text"
  ]
  node [
    id 24
    label "autor"
    origin "text"
  ]
  node [
    id 25
    label "komiks"
    origin "text"
  ]
  node [
    id 26
    label "scott"
    origin "text"
  ]
  node [
    id 27
    label "adams"
    origin "text"
  ]
  node [
    id 28
    label "deklarowa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "jako"
    origin "text"
  ]
  node [
    id 30
    label "zagorza&#322;y"
    origin "text"
  ]
  node [
    id 31
    label "przeciwnik"
    origin "text"
  ]
  node [
    id 32
    label "web"
    origin "text"
  ]
  node [
    id 33
    label "skr&#281;canie"
  ]
  node [
    id 34
    label "voice"
  ]
  node [
    id 35
    label "forma"
  ]
  node [
    id 36
    label "internet"
  ]
  node [
    id 37
    label "skr&#281;ci&#263;"
  ]
  node [
    id 38
    label "kartka"
  ]
  node [
    id 39
    label "orientowa&#263;"
  ]
  node [
    id 40
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 41
    label "powierzchnia"
  ]
  node [
    id 42
    label "plik"
  ]
  node [
    id 43
    label "bok"
  ]
  node [
    id 44
    label "pagina"
  ]
  node [
    id 45
    label "orientowanie"
  ]
  node [
    id 46
    label "fragment"
  ]
  node [
    id 47
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 48
    label "s&#261;d"
  ]
  node [
    id 49
    label "skr&#281;ca&#263;"
  ]
  node [
    id 50
    label "g&#243;ra"
  ]
  node [
    id 51
    label "serwis_internetowy"
  ]
  node [
    id 52
    label "orientacja"
  ]
  node [
    id 53
    label "linia"
  ]
  node [
    id 54
    label "skr&#281;cenie"
  ]
  node [
    id 55
    label "layout"
  ]
  node [
    id 56
    label "zorientowa&#263;"
  ]
  node [
    id 57
    label "zorientowanie"
  ]
  node [
    id 58
    label "obiekt"
  ]
  node [
    id 59
    label "podmiot"
  ]
  node [
    id 60
    label "ty&#322;"
  ]
  node [
    id 61
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 62
    label "logowanie"
  ]
  node [
    id 63
    label "adres_internetowy"
  ]
  node [
    id 64
    label "uj&#281;cie"
  ]
  node [
    id 65
    label "prz&#243;d"
  ]
  node [
    id 66
    label "posta&#263;"
  ]
  node [
    id 67
    label "nowoczesny"
  ]
  node [
    id 68
    label "elektroniczny"
  ]
  node [
    id 69
    label "sieciowo"
  ]
  node [
    id 70
    label "netowy"
  ]
  node [
    id 71
    label "internetowo"
  ]
  node [
    id 72
    label "przyst&#281;pny"
  ]
  node [
    id 73
    label "&#322;atwy"
  ]
  node [
    id 74
    label "popularnie"
  ]
  node [
    id 75
    label "znany"
  ]
  node [
    id 76
    label "zwi&#261;zek"
  ]
  node [
    id 77
    label "dyktando"
  ]
  node [
    id 78
    label "dodatek"
  ]
  node [
    id 79
    label "oznaka"
  ]
  node [
    id 80
    label "prevention"
  ]
  node [
    id 81
    label "spekulacja"
  ]
  node [
    id 82
    label "handel"
  ]
  node [
    id 83
    label "zone"
  ]
  node [
    id 84
    label "naszywka"
  ]
  node [
    id 85
    label "us&#322;uga"
  ]
  node [
    id 86
    label "przewi&#261;zka"
  ]
  node [
    id 87
    label "komiksowo"
  ]
  node [
    id 88
    label "energy"
  ]
  node [
    id 89
    label "czas"
  ]
  node [
    id 90
    label "bycie"
  ]
  node [
    id 91
    label "zegar_biologiczny"
  ]
  node [
    id 92
    label "okres_noworodkowy"
  ]
  node [
    id 93
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 94
    label "entity"
  ]
  node [
    id 95
    label "prze&#380;ywanie"
  ]
  node [
    id 96
    label "prze&#380;ycie"
  ]
  node [
    id 97
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 98
    label "wiek_matuzalemowy"
  ]
  node [
    id 99
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 100
    label "dzieci&#324;stwo"
  ]
  node [
    id 101
    label "power"
  ]
  node [
    id 102
    label "szwung"
  ]
  node [
    id 103
    label "menopauza"
  ]
  node [
    id 104
    label "umarcie"
  ]
  node [
    id 105
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 106
    label "life"
  ]
  node [
    id 107
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 108
    label "&#380;ywy"
  ]
  node [
    id 109
    label "rozw&#243;j"
  ]
  node [
    id 110
    label "po&#322;&#243;g"
  ]
  node [
    id 111
    label "byt"
  ]
  node [
    id 112
    label "przebywanie"
  ]
  node [
    id 113
    label "subsistence"
  ]
  node [
    id 114
    label "koleje_losu"
  ]
  node [
    id 115
    label "raj_utracony"
  ]
  node [
    id 116
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 117
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 118
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 119
    label "andropauza"
  ]
  node [
    id 120
    label "warunki"
  ]
  node [
    id 121
    label "do&#380;ywanie"
  ]
  node [
    id 122
    label "niemowl&#281;ctwo"
  ]
  node [
    id 123
    label "umieranie"
  ]
  node [
    id 124
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 125
    label "staro&#347;&#263;"
  ]
  node [
    id 126
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 127
    label "&#347;mier&#263;"
  ]
  node [
    id 128
    label "poczeka&#263;"
  ]
  node [
    id 129
    label "pozosta&#263;"
  ]
  node [
    id 130
    label "draw"
  ]
  node [
    id 131
    label "gwiazda"
  ]
  node [
    id 132
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 133
    label "przedstawienie"
  ]
  node [
    id 134
    label "ewidentny"
  ]
  node [
    id 135
    label "bezpo&#347;redni"
  ]
  node [
    id 136
    label "otwarcie"
  ]
  node [
    id 137
    label "nieograniczony"
  ]
  node [
    id 138
    label "zdecydowany"
  ]
  node [
    id 139
    label "gotowy"
  ]
  node [
    id 140
    label "aktualny"
  ]
  node [
    id 141
    label "prostoduszny"
  ]
  node [
    id 142
    label "jawnie"
  ]
  node [
    id 143
    label "otworzysty"
  ]
  node [
    id 144
    label "dost&#281;pny"
  ]
  node [
    id 145
    label "publiczny"
  ]
  node [
    id 146
    label "aktywny"
  ]
  node [
    id 147
    label "kontaktowy"
  ]
  node [
    id 148
    label "wytwarza&#263;"
  ]
  node [
    id 149
    label "wypowiedzenie"
  ]
  node [
    id 150
    label "tworzy&#263;"
  ]
  node [
    id 151
    label "j&#281;zykowo"
  ]
  node [
    id 152
    label "si&#281;ga&#263;"
  ]
  node [
    id 153
    label "trwa&#263;"
  ]
  node [
    id 154
    label "obecno&#347;&#263;"
  ]
  node [
    id 155
    label "stan"
  ]
  node [
    id 156
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 157
    label "stand"
  ]
  node [
    id 158
    label "mie&#263;_miejsce"
  ]
  node [
    id 159
    label "uczestniczy&#263;"
  ]
  node [
    id 160
    label "chodzi&#263;"
  ]
  node [
    id 161
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 162
    label "equal"
  ]
  node [
    id 163
    label "dupny"
  ]
  node [
    id 164
    label "wysoce"
  ]
  node [
    id 165
    label "wyj&#261;tkowy"
  ]
  node [
    id 166
    label "wybitny"
  ]
  node [
    id 167
    label "znaczny"
  ]
  node [
    id 168
    label "prawdziwy"
  ]
  node [
    id 169
    label "wa&#380;ny"
  ]
  node [
    id 170
    label "nieprzeci&#281;tny"
  ]
  node [
    id 171
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 172
    label "fan_club"
  ]
  node [
    id 173
    label "fandom"
  ]
  node [
    id 174
    label "piwo"
  ]
  node [
    id 175
    label "mention"
  ]
  node [
    id 176
    label "dodawa&#263;"
  ]
  node [
    id 177
    label "my&#347;le&#263;"
  ]
  node [
    id 178
    label "hint"
  ]
  node [
    id 179
    label "ci&#261;gle"
  ]
  node [
    id 180
    label "miesi&#261;c"
  ]
  node [
    id 181
    label "pomys&#322;odawca"
  ]
  node [
    id 182
    label "kszta&#322;ciciel"
  ]
  node [
    id 183
    label "tworzyciel"
  ]
  node [
    id 184
    label "&#347;w"
  ]
  node [
    id 185
    label "wykonawca"
  ]
  node [
    id 186
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 187
    label "wydawnictwo"
  ]
  node [
    id 188
    label "literatura_popularna"
  ]
  node [
    id 189
    label "scenorys"
  ]
  node [
    id 190
    label "zapewnia&#263;"
  ]
  node [
    id 191
    label "poda&#263;"
  ]
  node [
    id 192
    label "obiecywa&#263;"
  ]
  node [
    id 193
    label "obieca&#263;"
  ]
  node [
    id 194
    label "bespeak"
  ]
  node [
    id 195
    label "sign"
  ]
  node [
    id 196
    label "podawa&#263;"
  ]
  node [
    id 197
    label "zapewni&#263;"
  ]
  node [
    id 198
    label "gor&#261;cy"
  ]
  node [
    id 199
    label "gorliwy"
  ]
  node [
    id 200
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 201
    label "za&#380;arty"
  ]
  node [
    id 202
    label "cz&#322;owiek"
  ]
  node [
    id 203
    label "konkurencja"
  ]
  node [
    id 204
    label "wojna"
  ]
  node [
    id 205
    label "sp&#243;&#322;zawodnik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 59
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 179
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 180
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 181
  ]
  edge [
    source 24
    target 182
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 187
  ]
  edge [
    source 25
    target 188
  ]
  edge [
    source 25
    target 189
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 190
  ]
  edge [
    source 28
    target 191
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 28
    target 193
  ]
  edge [
    source 28
    target 194
  ]
  edge [
    source 28
    target 195
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 197
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 198
  ]
  edge [
    source 30
    target 199
  ]
  edge [
    source 30
    target 200
  ]
  edge [
    source 30
    target 201
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 202
  ]
  edge [
    source 31
    target 203
  ]
  edge [
    source 31
    target 204
  ]
  edge [
    source 31
    target 205
  ]
  edge [
    source 31
    target 66
  ]
]
