graph [
  maxDegree 20
  minDegree 1
  meanDegree 2
  density 0.024096385542168676
  graphCliqueNumber 2
  node [
    id 0
    label "dosta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "odpowied&#378;"
    origin "text"
  ]
  node [
    id 2
    label "prokuratura"
    origin "text"
  ]
  node [
    id 3
    label "odno&#347;nie"
    origin "text"
  ]
  node [
    id 4
    label "wpis"
    origin "text"
  ]
  node [
    id 5
    label "wykop"
    origin "text"
  ]
  node [
    id 6
    label "nawo&#322;uj&#261;cy"
    origin "text"
  ]
  node [
    id 7
    label "pod&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 8
    label "bomba"
    origin "text"
  ]
  node [
    id 9
    label "pod"
    origin "text"
  ]
  node [
    id 10
    label "m&#243;wnica"
    origin "text"
  ]
  node [
    id 11
    label "prezydent"
    origin "text"
  ]
  node [
    id 12
    label "listopad"
    origin "text"
  ]
  node [
    id 13
    label "wi&#281;c"
    origin "text"
  ]
  node [
    id 14
    label "zainteresowana"
    origin "text"
  ]
  node [
    id 15
    label "poprzedni"
    origin "text"
  ]
  node [
    id 16
    label "dokument"
  ]
  node [
    id 17
    label "rozmowa"
  ]
  node [
    id 18
    label "reakcja"
  ]
  node [
    id 19
    label "wyj&#347;cie"
  ]
  node [
    id 20
    label "react"
  ]
  node [
    id 21
    label "respondent"
  ]
  node [
    id 22
    label "replica"
  ]
  node [
    id 23
    label "siedziba"
  ]
  node [
    id 24
    label "urz&#261;d"
  ]
  node [
    id 25
    label "organ"
  ]
  node [
    id 26
    label "czynno&#347;&#263;"
  ]
  node [
    id 27
    label "entrance"
  ]
  node [
    id 28
    label "inscription"
  ]
  node [
    id 29
    label "akt"
  ]
  node [
    id 30
    label "op&#322;ata"
  ]
  node [
    id 31
    label "tekst"
  ]
  node [
    id 32
    label "odwa&#322;"
  ]
  node [
    id 33
    label "chody"
  ]
  node [
    id 34
    label "grodzisko"
  ]
  node [
    id 35
    label "budowa"
  ]
  node [
    id 36
    label "kopniak"
  ]
  node [
    id 37
    label "wyrobisko"
  ]
  node [
    id 38
    label "zrzutowy"
  ]
  node [
    id 39
    label "szaniec"
  ]
  node [
    id 40
    label "odk&#322;ad"
  ]
  node [
    id 41
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 42
    label "jell"
  ]
  node [
    id 43
    label "podwin&#261;&#263;"
  ]
  node [
    id 44
    label "doda&#263;"
  ]
  node [
    id 45
    label "plant"
  ]
  node [
    id 46
    label "zebra&#263;"
  ]
  node [
    id 47
    label "umie&#347;ci&#263;"
  ]
  node [
    id 48
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 49
    label "set"
  ]
  node [
    id 50
    label "niedostateczny"
  ]
  node [
    id 51
    label "zapalnik"
  ]
  node [
    id 52
    label "novum"
  ]
  node [
    id 53
    label "czerep"
  ]
  node [
    id 54
    label "strza&#322;"
  ]
  node [
    id 55
    label "pocisk"
  ]
  node [
    id 56
    label "materia&#322;_piroklastyczny"
  ]
  node [
    id 57
    label "bombowiec"
  ]
  node [
    id 58
    label "nab&#243;j"
  ]
  node [
    id 59
    label "podwy&#380;szenie"
  ]
  node [
    id 60
    label "forum"
  ]
  node [
    id 61
    label "platform"
  ]
  node [
    id 62
    label "Jelcyn"
  ]
  node [
    id 63
    label "Roosevelt"
  ]
  node [
    id 64
    label "Clinton"
  ]
  node [
    id 65
    label "dostojnik"
  ]
  node [
    id 66
    label "Tito"
  ]
  node [
    id 67
    label "de_Gaulle"
  ]
  node [
    id 68
    label "Nixon"
  ]
  node [
    id 69
    label "gruba_ryba"
  ]
  node [
    id 70
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 71
    label "Putin"
  ]
  node [
    id 72
    label "Gorbaczow"
  ]
  node [
    id 73
    label "Naser"
  ]
  node [
    id 74
    label "samorz&#261;dowiec"
  ]
  node [
    id 75
    label "Kemal"
  ]
  node [
    id 76
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 77
    label "zwierzchnik"
  ]
  node [
    id 78
    label "Bierut"
  ]
  node [
    id 79
    label "miesi&#261;c"
  ]
  node [
    id 80
    label "poprzednio"
  ]
  node [
    id 81
    label "przesz&#322;y"
  ]
  node [
    id 82
    label "wcze&#347;niejszy"
  ]
  node [
    id 83
    label "RP"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 15
    target 81
  ]
  edge [
    source 15
    target 82
  ]
]
