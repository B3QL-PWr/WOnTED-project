graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.1176470588235294
  density 0.020966802562609202
  graphCliqueNumber 3
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 2
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rozedrze&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "dan"
    origin "text"
  ]
  node [
    id 6
    label "rozumie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "nikt"
    origin "text"
  ]
  node [
    id 8
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "ani"
    origin "text"
  ]
  node [
    id 13
    label "matczysko"
  ]
  node [
    id 14
    label "macierz"
  ]
  node [
    id 15
    label "przodkini"
  ]
  node [
    id 16
    label "Matka_Boska"
  ]
  node [
    id 17
    label "macocha"
  ]
  node [
    id 18
    label "matka_zast&#281;pcza"
  ]
  node [
    id 19
    label "stara"
  ]
  node [
    id 20
    label "rodzice"
  ]
  node [
    id 21
    label "rodzic"
  ]
  node [
    id 22
    label "przeszy&#263;"
  ]
  node [
    id 23
    label "lacerate"
  ]
  node [
    id 24
    label "podrze&#263;"
  ]
  node [
    id 25
    label "zm&#261;ci&#263;"
  ]
  node [
    id 26
    label "tear"
  ]
  node [
    id 27
    label "wiek"
  ]
  node [
    id 28
    label "paleocen"
  ]
  node [
    id 29
    label "wiedzie&#263;"
  ]
  node [
    id 30
    label "zna&#263;"
  ]
  node [
    id 31
    label "czu&#263;"
  ]
  node [
    id 32
    label "j&#281;zyk"
  ]
  node [
    id 33
    label "kuma&#263;"
  ]
  node [
    id 34
    label "give"
  ]
  node [
    id 35
    label "odbiera&#263;"
  ]
  node [
    id 36
    label "empatia"
  ]
  node [
    id 37
    label "see"
  ]
  node [
    id 38
    label "match"
  ]
  node [
    id 39
    label "dziama&#263;"
  ]
  node [
    id 40
    label "miernota"
  ]
  node [
    id 41
    label "ciura"
  ]
  node [
    id 42
    label "du&#380;y"
  ]
  node [
    id 43
    label "cz&#281;sto"
  ]
  node [
    id 44
    label "bardzo"
  ]
  node [
    id 45
    label "mocno"
  ]
  node [
    id 46
    label "wiela"
  ]
  node [
    id 47
    label "si&#281;ga&#263;"
  ]
  node [
    id 48
    label "trwa&#263;"
  ]
  node [
    id 49
    label "obecno&#347;&#263;"
  ]
  node [
    id 50
    label "stan"
  ]
  node [
    id 51
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 52
    label "stand"
  ]
  node [
    id 53
    label "mie&#263;_miejsce"
  ]
  node [
    id 54
    label "uczestniczy&#263;"
  ]
  node [
    id 55
    label "chodzi&#263;"
  ]
  node [
    id 56
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 57
    label "equal"
  ]
  node [
    id 58
    label "remark"
  ]
  node [
    id 59
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 60
    label "u&#380;ywa&#263;"
  ]
  node [
    id 61
    label "okre&#347;la&#263;"
  ]
  node [
    id 62
    label "say"
  ]
  node [
    id 63
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 64
    label "formu&#322;owa&#263;"
  ]
  node [
    id 65
    label "talk"
  ]
  node [
    id 66
    label "powiada&#263;"
  ]
  node [
    id 67
    label "informowa&#263;"
  ]
  node [
    id 68
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 69
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 70
    label "wydobywa&#263;"
  ]
  node [
    id 71
    label "express"
  ]
  node [
    id 72
    label "chew_the_fat"
  ]
  node [
    id 73
    label "dysfonia"
  ]
  node [
    id 74
    label "umie&#263;"
  ]
  node [
    id 75
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 76
    label "tell"
  ]
  node [
    id 77
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 78
    label "wyra&#380;a&#263;"
  ]
  node [
    id 79
    label "gaworzy&#263;"
  ]
  node [
    id 80
    label "rozmawia&#263;"
  ]
  node [
    id 81
    label "prawi&#263;"
  ]
  node [
    id 82
    label "tentegowa&#263;"
  ]
  node [
    id 83
    label "urz&#261;dza&#263;"
  ]
  node [
    id 84
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 85
    label "czyni&#263;"
  ]
  node [
    id 86
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 87
    label "post&#281;powa&#263;"
  ]
  node [
    id 88
    label "wydala&#263;"
  ]
  node [
    id 89
    label "oszukiwa&#263;"
  ]
  node [
    id 90
    label "organizowa&#263;"
  ]
  node [
    id 91
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 92
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 93
    label "work"
  ]
  node [
    id 94
    label "przerabia&#263;"
  ]
  node [
    id 95
    label "stylizowa&#263;"
  ]
  node [
    id 96
    label "falowa&#263;"
  ]
  node [
    id 97
    label "act"
  ]
  node [
    id 98
    label "peddle"
  ]
  node [
    id 99
    label "ukazywa&#263;"
  ]
  node [
    id 100
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 101
    label "praca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 34
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 12
    target 12
  ]
]
