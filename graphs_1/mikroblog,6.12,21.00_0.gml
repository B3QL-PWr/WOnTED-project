graph [
  maxDegree 12
  minDegree 1
  meanDegree 2
  density 0.07407407407407407
  graphCliqueNumber 3
  node [
    id 0
    label "nadej&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ten"
    origin "text"
  ]
  node [
    id 2
    label "dzienia"
    origin "text"
  ]
  node [
    id 3
    label "rozdajo"
    origin "text"
  ]
  node [
    id 4
    label "xboxa"
    origin "text"
  ]
  node [
    id 5
    label "sekunda"
    origin "text"
  ]
  node [
    id 6
    label "sta&#263;_si&#281;"
  ]
  node [
    id 7
    label "czas"
  ]
  node [
    id 8
    label "catch"
  ]
  node [
    id 9
    label "become"
  ]
  node [
    id 10
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 11
    label "line_up"
  ]
  node [
    id 12
    label "przyby&#263;"
  ]
  node [
    id 13
    label "okre&#347;lony"
  ]
  node [
    id 14
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 15
    label "minuta"
  ]
  node [
    id 16
    label "tercja"
  ]
  node [
    id 17
    label "milisekunda"
  ]
  node [
    id 18
    label "nanosekunda"
  ]
  node [
    id 19
    label "uk&#322;ad_SI"
  ]
  node [
    id 20
    label "mikrosekunda"
  ]
  node [
    id 21
    label "time"
  ]
  node [
    id 22
    label "jednostka_czasu"
  ]
  node [
    id 23
    label "jednostka"
  ]
  node [
    id 24
    label "Xboxa"
  ]
  node [
    id 25
    label "on"
  ]
  node [
    id 26
    label "planet"
  ]
  node [
    id 27
    label "plus"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
]
