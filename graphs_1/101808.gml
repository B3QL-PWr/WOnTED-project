graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9795918367346939
  density 0.02040816326530612
  graphCliqueNumber 2
  node [
    id 0
    label "kino"
    origin "text"
  ]
  node [
    id 1
    label "czas"
    origin "text"
  ]
  node [
    id 2
    label "bezkr&#243;lewie"
    origin "text"
  ]
  node [
    id 3
    label "relacja"
    origin "text"
  ]
  node [
    id 4
    label "spotkanie"
    origin "text"
  ]
  node [
    id 5
    label "animatronika"
  ]
  node [
    id 6
    label "cyrk"
  ]
  node [
    id 7
    label "seans"
  ]
  node [
    id 8
    label "dorobek"
  ]
  node [
    id 9
    label "ekran"
  ]
  node [
    id 10
    label "budynek"
  ]
  node [
    id 11
    label "kinoteatr"
  ]
  node [
    id 12
    label "picture"
  ]
  node [
    id 13
    label "bioskop"
  ]
  node [
    id 14
    label "sztuka"
  ]
  node [
    id 15
    label "muza"
  ]
  node [
    id 16
    label "czasokres"
  ]
  node [
    id 17
    label "trawienie"
  ]
  node [
    id 18
    label "kategoria_gramatyczna"
  ]
  node [
    id 19
    label "period"
  ]
  node [
    id 20
    label "odczyt"
  ]
  node [
    id 21
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 22
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 23
    label "chwila"
  ]
  node [
    id 24
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 25
    label "poprzedzenie"
  ]
  node [
    id 26
    label "koniugacja"
  ]
  node [
    id 27
    label "dzieje"
  ]
  node [
    id 28
    label "poprzedzi&#263;"
  ]
  node [
    id 29
    label "przep&#322;ywanie"
  ]
  node [
    id 30
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 31
    label "odwlekanie_si&#281;"
  ]
  node [
    id 32
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 33
    label "Zeitgeist"
  ]
  node [
    id 34
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 35
    label "okres_czasu"
  ]
  node [
    id 36
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 37
    label "pochodzi&#263;"
  ]
  node [
    id 38
    label "schy&#322;ek"
  ]
  node [
    id 39
    label "czwarty_wymiar"
  ]
  node [
    id 40
    label "chronometria"
  ]
  node [
    id 41
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 42
    label "poprzedzanie"
  ]
  node [
    id 43
    label "pogoda"
  ]
  node [
    id 44
    label "zegar"
  ]
  node [
    id 45
    label "pochodzenie"
  ]
  node [
    id 46
    label "poprzedza&#263;"
  ]
  node [
    id 47
    label "trawi&#263;"
  ]
  node [
    id 48
    label "time_period"
  ]
  node [
    id 49
    label "rachuba_czasu"
  ]
  node [
    id 50
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 51
    label "czasoprzestrze&#324;"
  ]
  node [
    id 52
    label "laba"
  ]
  node [
    id 53
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 54
    label "nie&#322;ad"
  ]
  node [
    id 55
    label "interrex"
  ]
  node [
    id 56
    label "konwokacja"
  ]
  node [
    id 57
    label "sejm_konwokacyjny"
  ]
  node [
    id 58
    label "woluntaryzm"
  ]
  node [
    id 59
    label "wypowied&#378;"
  ]
  node [
    id 60
    label "message"
  ]
  node [
    id 61
    label "podzbi&#243;r"
  ]
  node [
    id 62
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 63
    label "ustosunkowywa&#263;"
  ]
  node [
    id 64
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 65
    label "bratnia_dusza"
  ]
  node [
    id 66
    label "zwi&#261;zanie"
  ]
  node [
    id 67
    label "ustosunkowanie"
  ]
  node [
    id 68
    label "ustosunkowywanie"
  ]
  node [
    id 69
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 70
    label "zwi&#261;za&#263;"
  ]
  node [
    id 71
    label "ustosunkowa&#263;"
  ]
  node [
    id 72
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 73
    label "korespondent"
  ]
  node [
    id 74
    label "marriage"
  ]
  node [
    id 75
    label "wi&#261;zanie"
  ]
  node [
    id 76
    label "trasa"
  ]
  node [
    id 77
    label "zwi&#261;zek"
  ]
  node [
    id 78
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 79
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 80
    label "sprawko"
  ]
  node [
    id 81
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 82
    label "po&#380;egnanie"
  ]
  node [
    id 83
    label "spowodowanie"
  ]
  node [
    id 84
    label "znalezienie"
  ]
  node [
    id 85
    label "znajomy"
  ]
  node [
    id 86
    label "doznanie"
  ]
  node [
    id 87
    label "employment"
  ]
  node [
    id 88
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 89
    label "gather"
  ]
  node [
    id 90
    label "powitanie"
  ]
  node [
    id 91
    label "spotykanie"
  ]
  node [
    id 92
    label "wydarzenie"
  ]
  node [
    id 93
    label "gathering"
  ]
  node [
    id 94
    label "spotkanie_si&#281;"
  ]
  node [
    id 95
    label "zdarzenie_si&#281;"
  ]
  node [
    id 96
    label "match"
  ]
  node [
    id 97
    label "zawarcie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
]
