graph [
  maxDegree 14
  minDegree 1
  meanDegree 2.0689655172413794
  density 0.036297640653357534
  graphCliqueNumber 4
  node [
    id 0
    label "pu&#322;k"
    origin "text"
  ]
  node [
    id 1
    label "piechota"
    origin "text"
  ]
  node [
    id 2
    label "liniowy"
    origin "text"
  ]
  node [
    id 3
    label "kr&#243;lestwo"
    origin "text"
  ]
  node [
    id 4
    label "kongresowy"
    origin "text"
  ]
  node [
    id 5
    label "regiment"
  ]
  node [
    id 6
    label "formacja"
  ]
  node [
    id 7
    label "pododdzia&#322;"
  ]
  node [
    id 8
    label "dywizjon_artylerii"
  ]
  node [
    id 9
    label "brygada"
  ]
  node [
    id 10
    label "batalion"
  ]
  node [
    id 11
    label "dywizjon_lotniczy"
  ]
  node [
    id 12
    label "armia"
  ]
  node [
    id 13
    label "kompania_honorowa"
  ]
  node [
    id 14
    label "falanga"
  ]
  node [
    id 15
    label "infantry"
  ]
  node [
    id 16
    label "liniowo"
  ]
  node [
    id 17
    label "prosty"
  ]
  node [
    id 18
    label "Arktogea"
  ]
  node [
    id 19
    label "kategoria_systematyczna"
  ]
  node [
    id 20
    label "domena"
  ]
  node [
    id 21
    label "protisty"
  ]
  node [
    id 22
    label "grzyby"
  ]
  node [
    id 23
    label "prokarioty"
  ]
  node [
    id 24
    label "ro&#347;liny"
  ]
  node [
    id 25
    label "terytorium"
  ]
  node [
    id 26
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 27
    label "pa&#324;stwo"
  ]
  node [
    id 28
    label "zwierz&#281;ta"
  ]
  node [
    id 29
    label "11"
  ]
  node [
    id 30
    label "polskie"
  ]
  node [
    id 31
    label "powsta&#263;"
  ]
  node [
    id 32
    label "listopadowy"
  ]
  node [
    id 33
    label "J&#243;zefa"
  ]
  node [
    id 34
    label "Ch&#322;opicki"
  ]
  node [
    id 35
    label "1"
  ]
  node [
    id 36
    label "Franciszka"
  ]
  node [
    id 37
    label "M&#322;okosiewicz"
  ]
  node [
    id 38
    label "Odolski"
  ]
  node [
    id 39
    label "komisja"
  ]
  node [
    id 40
    label "rz&#261;dowy"
  ]
  node [
    id 41
    label "wojna"
  ]
  node [
    id 42
    label "12"
  ]
  node [
    id 43
    label "PPL"
  ]
  node [
    id 44
    label "gwardia"
  ]
  node [
    id 45
    label "ruchomy"
  ]
  node [
    id 46
    label "regimentarz"
  ]
  node [
    id 47
    label "lewy"
  ]
  node [
    id 48
    label "brzeg"
  ]
  node [
    id 49
    label "Wis&#322;a"
  ]
  node [
    id 50
    label "wojskowy"
  ]
  node [
    id 51
    label "rz&#261;d"
  ]
  node [
    id 52
    label "stan"
  ]
  node [
    id 53
    label "obecny"
  ]
  node [
    id 54
    label "do"
  ]
  node [
    id 55
    label "b&#243;j"
  ]
  node [
    id 56
    label "VI"
  ]
  node [
    id 57
    label "DP"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 50
  ]
  edge [
    source 39
    target 51
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
]
