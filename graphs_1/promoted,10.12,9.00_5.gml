graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.0155038759689923
  density 0.015746124031007752
  graphCliqueNumber 2
  node [
    id 0
    label "pocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "czech"
    origin "text"
  ]
  node [
    id 4
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 6
    label "zawiera&#263;"
    origin "text"
  ]
  node [
    id 7
    label "glifosat"
    origin "text"
  ]
  node [
    id 8
    label "ulec"
    origin "text"
  ]
  node [
    id 9
    label "ograniczenie"
    origin "text"
  ]
  node [
    id 10
    label "poda&#263;"
    origin "text"
  ]
  node [
    id 11
    label "poniedzia&#322;ek"
    origin "text"
  ]
  node [
    id 12
    label "komunikat"
    origin "text"
  ]
  node [
    id 13
    label "czeski"
    origin "text"
  ]
  node [
    id 14
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 15
    label "rolnictwo"
    origin "text"
  ]
  node [
    id 16
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 17
    label "do"
  ]
  node [
    id 18
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 19
    label "zrobi&#263;"
  ]
  node [
    id 20
    label "kolejny"
  ]
  node [
    id 21
    label "stulecie"
  ]
  node [
    id 22
    label "kalendarz"
  ]
  node [
    id 23
    label "czas"
  ]
  node [
    id 24
    label "pora_roku"
  ]
  node [
    id 25
    label "cykl_astronomiczny"
  ]
  node [
    id 26
    label "p&#243;&#322;rocze"
  ]
  node [
    id 27
    label "grupa"
  ]
  node [
    id 28
    label "kwarta&#322;"
  ]
  node [
    id 29
    label "kurs"
  ]
  node [
    id 30
    label "jubileusz"
  ]
  node [
    id 31
    label "miesi&#261;c"
  ]
  node [
    id 32
    label "lata"
  ]
  node [
    id 33
    label "martwy_sezon"
  ]
  node [
    id 34
    label "u&#380;ywa&#263;"
  ]
  node [
    id 35
    label "miejsce"
  ]
  node [
    id 36
    label "abstrakcja"
  ]
  node [
    id 37
    label "punkt"
  ]
  node [
    id 38
    label "substancja"
  ]
  node [
    id 39
    label "spos&#243;b"
  ]
  node [
    id 40
    label "chemikalia"
  ]
  node [
    id 41
    label "obejmowa&#263;"
  ]
  node [
    id 42
    label "mie&#263;"
  ]
  node [
    id 43
    label "zamyka&#263;"
  ]
  node [
    id 44
    label "lock"
  ]
  node [
    id 45
    label "poznawa&#263;"
  ]
  node [
    id 46
    label "fold"
  ]
  node [
    id 47
    label "make"
  ]
  node [
    id 48
    label "ustala&#263;"
  ]
  node [
    id 49
    label "sta&#263;_si&#281;"
  ]
  node [
    id 50
    label "put_in"
  ]
  node [
    id 51
    label "podda&#263;"
  ]
  node [
    id 52
    label "kobieta"
  ]
  node [
    id 53
    label "pozwoli&#263;"
  ]
  node [
    id 54
    label "give"
  ]
  node [
    id 55
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 56
    label "fall"
  ]
  node [
    id 57
    label "podda&#263;_si&#281;"
  ]
  node [
    id 58
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 59
    label "prevention"
  ]
  node [
    id 60
    label "zdyskryminowanie"
  ]
  node [
    id 61
    label "barrier"
  ]
  node [
    id 62
    label "zmniejszenie"
  ]
  node [
    id 63
    label "otoczenie"
  ]
  node [
    id 64
    label "przekracza&#263;"
  ]
  node [
    id 65
    label "cecha"
  ]
  node [
    id 66
    label "przekraczanie"
  ]
  node [
    id 67
    label "przekroczenie"
  ]
  node [
    id 68
    label "pomiarkowanie"
  ]
  node [
    id 69
    label "limitation"
  ]
  node [
    id 70
    label "warunek"
  ]
  node [
    id 71
    label "przekroczy&#263;"
  ]
  node [
    id 72
    label "g&#322;upstwo"
  ]
  node [
    id 73
    label "osielstwo"
  ]
  node [
    id 74
    label "przeszkoda"
  ]
  node [
    id 75
    label "reservation"
  ]
  node [
    id 76
    label "intelekt"
  ]
  node [
    id 77
    label "finlandyzacja"
  ]
  node [
    id 78
    label "tenis"
  ]
  node [
    id 79
    label "da&#263;"
  ]
  node [
    id 80
    label "siatk&#243;wka"
  ]
  node [
    id 81
    label "introduce"
  ]
  node [
    id 82
    label "jedzenie"
  ]
  node [
    id 83
    label "zaserwowa&#263;"
  ]
  node [
    id 84
    label "ustawi&#263;"
  ]
  node [
    id 85
    label "zagra&#263;"
  ]
  node [
    id 86
    label "supply"
  ]
  node [
    id 87
    label "nafaszerowa&#263;"
  ]
  node [
    id 88
    label "poinformowa&#263;"
  ]
  node [
    id 89
    label "dzie&#324;_powszedni"
  ]
  node [
    id 90
    label "Poniedzia&#322;ek_Wielkonocny"
  ]
  node [
    id 91
    label "tydzie&#324;"
  ]
  node [
    id 92
    label "kreacjonista"
  ]
  node [
    id 93
    label "wytw&#243;r"
  ]
  node [
    id 94
    label "roi&#263;_si&#281;"
  ]
  node [
    id 95
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 96
    label "communication"
  ]
  node [
    id 97
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 98
    label "j&#281;zyk_zachodnios&#322;owia&#324;ski"
  ]
  node [
    id 99
    label "j&#281;zyk"
  ]
  node [
    id 100
    label "Czech"
  ]
  node [
    id 101
    label "po_czesku"
  ]
  node [
    id 102
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 103
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 104
    label "ministerium"
  ]
  node [
    id 105
    label "resort"
  ]
  node [
    id 106
    label "urz&#261;d"
  ]
  node [
    id 107
    label "MSW"
  ]
  node [
    id 108
    label "departament"
  ]
  node [
    id 109
    label "NKWD"
  ]
  node [
    id 110
    label "intensyfikacja"
  ]
  node [
    id 111
    label "agronomia"
  ]
  node [
    id 112
    label "gleboznawstwo"
  ]
  node [
    id 113
    label "&#322;&#261;karstwo"
  ]
  node [
    id 114
    label "sadownictwo"
  ]
  node [
    id 115
    label "&#322;owiectwo"
  ]
  node [
    id 116
    label "agrobiznes"
  ]
  node [
    id 117
    label "nasiennictwo"
  ]
  node [
    id 118
    label "agroekologia"
  ]
  node [
    id 119
    label "uprawianie"
  ]
  node [
    id 120
    label "gospodarka"
  ]
  node [
    id 121
    label "zgarniacz"
  ]
  node [
    id 122
    label "hodowla"
  ]
  node [
    id 123
    label "agrochemia"
  ]
  node [
    id 124
    label "farmerstwo"
  ]
  node [
    id 125
    label "zootechnika"
  ]
  node [
    id 126
    label "agrotechnika"
  ]
  node [
    id 127
    label "ogrodnictwo"
  ]
  node [
    id 128
    label "nauka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
]
