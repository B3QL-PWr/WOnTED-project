graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 1
    label "kasa"
    origin "text"
  ]
  node [
    id 2
    label "kafelek"
    origin "text"
  ]
  node [
    id 3
    label "czyj&#347;"
  ]
  node [
    id 4
    label "m&#261;&#380;"
  ]
  node [
    id 5
    label "skrzynia"
  ]
  node [
    id 6
    label "miejsce"
  ]
  node [
    id 7
    label "instytucja"
  ]
  node [
    id 8
    label "szafa"
  ]
  node [
    id 9
    label "pieni&#261;dze"
  ]
  node [
    id 10
    label "urz&#261;dzenie"
  ]
  node [
    id 11
    label "glazura"
  ]
  node [
    id 12
    label "p&#322;ytka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
]
