graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "suka"
    origin "text"
  ]
  node [
    id 1
    label "puszczalska"
  ]
  node [
    id 2
    label "zo&#322;za"
  ]
  node [
    id 3
    label "rozpustnica"
  ]
  node [
    id 4
    label "samica"
  ]
  node [
    id 5
    label "shed"
  ]
  node [
    id 6
    label "skurwienie_si&#281;"
  ]
  node [
    id 7
    label "pies"
  ]
  node [
    id 8
    label "wyzwisko"
  ]
  node [
    id 9
    label "oszczenienie_si&#281;"
  ]
  node [
    id 10
    label "szczenienie_si&#281;"
  ]
  node [
    id 11
    label "radiow&#243;z"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
]
