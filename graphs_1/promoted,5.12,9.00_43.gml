graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9545454545454546
  density 0.02246603970741902
  graphCliqueNumber 2
  node [
    id 0
    label "publikacja"
    origin "text"
  ]
  node [
    id 1
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "autor"
    origin "text"
  ]
  node [
    id 3
    label "zarzuca&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 5
    label "henryk"
    origin "text"
  ]
  node [
    id 6
    label "jankowski"
    origin "text"
  ]
  node [
    id 7
    label "pedofilia"
    origin "text"
  ]
  node [
    id 8
    label "wzbudzi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wielki"
    origin "text"
  ]
  node [
    id 10
    label "emocja"
    origin "text"
  ]
  node [
    id 11
    label "opinia"
    origin "text"
  ]
  node [
    id 12
    label "publiczny"
    origin "text"
  ]
  node [
    id 13
    label "produkcja"
  ]
  node [
    id 14
    label "druk"
  ]
  node [
    id 15
    label "notification"
  ]
  node [
    id 16
    label "tekst"
  ]
  node [
    id 17
    label "pomys&#322;odawca"
  ]
  node [
    id 18
    label "kszta&#322;ciciel"
  ]
  node [
    id 19
    label "tworzyciel"
  ]
  node [
    id 20
    label "&#347;w"
  ]
  node [
    id 21
    label "wykonawca"
  ]
  node [
    id 22
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 23
    label "twierdzi&#263;"
  ]
  node [
    id 24
    label "intrude"
  ]
  node [
    id 25
    label "przestawa&#263;"
  ]
  node [
    id 26
    label "prosecute"
  ]
  node [
    id 27
    label "umieszcza&#263;"
  ]
  node [
    id 28
    label "przeznacza&#263;"
  ]
  node [
    id 29
    label "bequeath"
  ]
  node [
    id 30
    label "inflict"
  ]
  node [
    id 31
    label "odchodzi&#263;"
  ]
  node [
    id 32
    label "klecha"
  ]
  node [
    id 33
    label "eklezjasta"
  ]
  node [
    id 34
    label "rozgrzeszanie"
  ]
  node [
    id 35
    label "duszpasterstwo"
  ]
  node [
    id 36
    label "rozgrzesza&#263;"
  ]
  node [
    id 37
    label "kap&#322;an"
  ]
  node [
    id 38
    label "ksi&#281;&#380;a"
  ]
  node [
    id 39
    label "duchowny"
  ]
  node [
    id 40
    label "kol&#281;da"
  ]
  node [
    id 41
    label "seminarzysta"
  ]
  node [
    id 42
    label "pasterz"
  ]
  node [
    id 43
    label "pedophilia"
  ]
  node [
    id 44
    label "przest&#281;pstwo"
  ]
  node [
    id 45
    label "parafilia"
  ]
  node [
    id 46
    label "wywo&#322;a&#263;"
  ]
  node [
    id 47
    label "arouse"
  ]
  node [
    id 48
    label "dupny"
  ]
  node [
    id 49
    label "wysoce"
  ]
  node [
    id 50
    label "wyj&#261;tkowy"
  ]
  node [
    id 51
    label "wybitny"
  ]
  node [
    id 52
    label "znaczny"
  ]
  node [
    id 53
    label "prawdziwy"
  ]
  node [
    id 54
    label "wa&#380;ny"
  ]
  node [
    id 55
    label "nieprzeci&#281;tny"
  ]
  node [
    id 56
    label "ostygn&#261;&#263;"
  ]
  node [
    id 57
    label "afekt"
  ]
  node [
    id 58
    label "stan"
  ]
  node [
    id 59
    label "iskrzy&#263;"
  ]
  node [
    id 60
    label "wpa&#347;&#263;"
  ]
  node [
    id 61
    label "wpada&#263;"
  ]
  node [
    id 62
    label "d&#322;awi&#263;"
  ]
  node [
    id 63
    label "ogrom"
  ]
  node [
    id 64
    label "stygn&#261;&#263;"
  ]
  node [
    id 65
    label "temperatura"
  ]
  node [
    id 66
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 67
    label "dokument"
  ]
  node [
    id 68
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 69
    label "reputacja"
  ]
  node [
    id 70
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 71
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 72
    label "cecha"
  ]
  node [
    id 73
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 74
    label "informacja"
  ]
  node [
    id 75
    label "sofcik"
  ]
  node [
    id 76
    label "appraisal"
  ]
  node [
    id 77
    label "ekspertyza"
  ]
  node [
    id 78
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 79
    label "pogl&#261;d"
  ]
  node [
    id 80
    label "kryterium"
  ]
  node [
    id 81
    label "wielko&#347;&#263;"
  ]
  node [
    id 82
    label "jawny"
  ]
  node [
    id 83
    label "upublicznienie"
  ]
  node [
    id 84
    label "upublicznianie"
  ]
  node [
    id 85
    label "publicznie"
  ]
  node [
    id 86
    label "Henryk"
  ]
  node [
    id 87
    label "Jankowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 86
    target 87
  ]
]
