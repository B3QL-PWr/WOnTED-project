graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 3
  node [
    id 0
    label "cerkiew"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiadek"
    origin "text"
  ]
  node [
    id 2
    label "miko&#322;aj"
    origin "text"
  ]
  node [
    id 3
    label "brze&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 5
    label "Ko&#347;ci&#243;&#322;_greckokatolicki"
  ]
  node [
    id 6
    label "carskie_wrota"
  ]
  node [
    id 7
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 8
    label "kliros"
  ]
  node [
    id 9
    label "babiniec"
  ]
  node [
    id 10
    label "Ko&#347;ci&#243;&#322;_prawos&#322;awny"
  ]
  node [
    id 11
    label "ikonostas"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "s&#261;d"
  ]
  node [
    id 14
    label "osoba_fizyczna"
  ]
  node [
    id 15
    label "uczestnik"
  ]
  node [
    id 16
    label "obserwator"
  ]
  node [
    id 17
    label "dru&#380;ba"
  ]
  node [
    id 18
    label "stand"
  ]
  node [
    id 19
    label "&#347;wi&#281;ty"
  ]
  node [
    id 20
    label "wielki"
  ]
  node [
    id 21
    label "ksi&#281;stwo"
  ]
  node [
    id 22
    label "litewski"
  ]
  node [
    id 23
    label "twierdza"
  ]
  node [
    id 24
    label "brzeski"
  ]
  node [
    id 25
    label "sob&#243;r"
  ]
  node [
    id 26
    label "unia"
  ]
  node [
    id 27
    label "ziemia"
  ]
  node [
    id 28
    label "zabra&#263;"
  ]
  node [
    id 29
    label "greckokatolicki"
  ]
  node [
    id 30
    label "rosyjski"
  ]
  node [
    id 31
    label "prawos&#322;awny"
  ]
  node [
    id 32
    label "stary"
  ]
  node [
    id 33
    label "miasto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
]
