graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.7142857142857142
  density 0.13186813186813187
  graphCliqueNumber 2
  node [
    id 0
    label "gmina"
    origin "text"
  ]
  node [
    id 1
    label "o&#322;yka"
    origin "text"
  ]
  node [
    id 2
    label "rada_gminy"
  ]
  node [
    id 3
    label "Wielka_Wie&#347;"
  ]
  node [
    id 4
    label "jednostka_administracyjna"
  ]
  node [
    id 5
    label "powiat"
  ]
  node [
    id 6
    label "Dobro&#324;"
  ]
  node [
    id 7
    label "Karlsbad"
  ]
  node [
    id 8
    label "urz&#261;d"
  ]
  node [
    id 9
    label "Biskupice"
  ]
  node [
    id 10
    label "radny"
  ]
  node [
    id 11
    label "organizacja_religijna"
  ]
  node [
    id 12
    label "zwi&#261;zek"
  ]
  node [
    id 13
    label "radziecki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 12
    target 13
  ]
]
