graph [
  maxDegree 52
  minDegree 1
  meanDegree 2.016
  density 0.01625806451612903
  graphCliqueNumber 3
  node [
    id 0
    label "rzecznik"
    origin "text"
  ]
  node [
    id 1
    label "prawy"
    origin "text"
  ]
  node [
    id 2
    label "obywatelski"
    origin "text"
  ]
  node [
    id 3
    label "zorganizowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "spotkanie"
    origin "text"
  ]
  node [
    id 5
    label "po&#347;wi&#281;cony"
    origin "text"
  ]
  node [
    id 6
    label "problematyka"
    origin "text"
  ]
  node [
    id 7
    label "patologia"
    origin "text"
  ]
  node [
    id 8
    label "internet"
    origin "text"
  ]
  node [
    id 9
    label "doradca"
  ]
  node [
    id 10
    label "przyjaciel"
  ]
  node [
    id 11
    label "przedstawiciel"
  ]
  node [
    id 12
    label "z_prawa"
  ]
  node [
    id 13
    label "cnotliwy"
  ]
  node [
    id 14
    label "moralny"
  ]
  node [
    id 15
    label "naturalny"
  ]
  node [
    id 16
    label "zgodnie_z_prawem"
  ]
  node [
    id 17
    label "legalny"
  ]
  node [
    id 18
    label "na_prawo"
  ]
  node [
    id 19
    label "s&#322;uszny"
  ]
  node [
    id 20
    label "w_prawo"
  ]
  node [
    id 21
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 22
    label "prawicowy"
  ]
  node [
    id 23
    label "chwalebny"
  ]
  node [
    id 24
    label "zacny"
  ]
  node [
    id 25
    label "odpowiedzialny"
  ]
  node [
    id 26
    label "obywatelsko"
  ]
  node [
    id 27
    label "oddolny"
  ]
  node [
    id 28
    label "zaplanowa&#263;"
  ]
  node [
    id 29
    label "dostosowa&#263;"
  ]
  node [
    id 30
    label "przygotowa&#263;"
  ]
  node [
    id 31
    label "stworzy&#263;"
  ]
  node [
    id 32
    label "stage"
  ]
  node [
    id 33
    label "pozyska&#263;"
  ]
  node [
    id 34
    label "urobi&#263;"
  ]
  node [
    id 35
    label "plan"
  ]
  node [
    id 36
    label "ensnare"
  ]
  node [
    id 37
    label "standard"
  ]
  node [
    id 38
    label "skupi&#263;"
  ]
  node [
    id 39
    label "wprowadzi&#263;"
  ]
  node [
    id 40
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 41
    label "po&#380;egnanie"
  ]
  node [
    id 42
    label "spowodowanie"
  ]
  node [
    id 43
    label "znalezienie"
  ]
  node [
    id 44
    label "znajomy"
  ]
  node [
    id 45
    label "doznanie"
  ]
  node [
    id 46
    label "employment"
  ]
  node [
    id 47
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 48
    label "gather"
  ]
  node [
    id 49
    label "powitanie"
  ]
  node [
    id 50
    label "spotykanie"
  ]
  node [
    id 51
    label "wydarzenie"
  ]
  node [
    id 52
    label "gathering"
  ]
  node [
    id 53
    label "spotkanie_si&#281;"
  ]
  node [
    id 54
    label "zdarzenie_si&#281;"
  ]
  node [
    id 55
    label "match"
  ]
  node [
    id 56
    label "zawarcie"
  ]
  node [
    id 57
    label "oddany"
  ]
  node [
    id 58
    label "zbi&#243;r"
  ]
  node [
    id 59
    label "problem"
  ]
  node [
    id 60
    label "histopatologia"
  ]
  node [
    id 61
    label "logopatologia"
  ]
  node [
    id 62
    label "ognisko"
  ]
  node [
    id 63
    label "osteopatologia"
  ]
  node [
    id 64
    label "odezwanie_si&#281;"
  ]
  node [
    id 65
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 66
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 67
    label "psychopatologia"
  ]
  node [
    id 68
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 69
    label "przypadek"
  ]
  node [
    id 70
    label "zajmowa&#263;"
  ]
  node [
    id 71
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 72
    label "&#347;rodowisko"
  ]
  node [
    id 73
    label "zajmowanie"
  ]
  node [
    id 74
    label "badanie_histopatologiczne"
  ]
  node [
    id 75
    label "immunopatologia"
  ]
  node [
    id 76
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 77
    label "atakowanie"
  ]
  node [
    id 78
    label "gangsterski"
  ]
  node [
    id 79
    label "paleopatologia"
  ]
  node [
    id 80
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 81
    label "remisja"
  ]
  node [
    id 82
    label "grupa_ryzyka"
  ]
  node [
    id 83
    label "atakowa&#263;"
  ]
  node [
    id 84
    label "kryzys"
  ]
  node [
    id 85
    label "nabawienie_si&#281;"
  ]
  node [
    id 86
    label "abnormality"
  ]
  node [
    id 87
    label "przemoc"
  ]
  node [
    id 88
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 89
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 90
    label "inkubacja"
  ]
  node [
    id 91
    label "meteoropatologia"
  ]
  node [
    id 92
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 93
    label "patolnia"
  ]
  node [
    id 94
    label "powalenie"
  ]
  node [
    id 95
    label "fizjologia_patologiczna"
  ]
  node [
    id 96
    label "nabawianie_si&#281;"
  ]
  node [
    id 97
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 98
    label "medycyna"
  ]
  node [
    id 99
    label "szambo"
  ]
  node [
    id 100
    label "odzywanie_si&#281;"
  ]
  node [
    id 101
    label "diagnoza"
  ]
  node [
    id 102
    label "patomorfologia"
  ]
  node [
    id 103
    label "patogeneza"
  ]
  node [
    id 104
    label "aspo&#322;eczny"
  ]
  node [
    id 105
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 106
    label "underworld"
  ]
  node [
    id 107
    label "neuropatologia"
  ]
  node [
    id 108
    label "powali&#263;"
  ]
  node [
    id 109
    label "zaburzenie"
  ]
  node [
    id 110
    label "us&#322;uga_internetowa"
  ]
  node [
    id 111
    label "biznes_elektroniczny"
  ]
  node [
    id 112
    label "punkt_dost&#281;pu"
  ]
  node [
    id 113
    label "hipertekst"
  ]
  node [
    id 114
    label "gra_sieciowa"
  ]
  node [
    id 115
    label "mem"
  ]
  node [
    id 116
    label "e-hazard"
  ]
  node [
    id 117
    label "sie&#263;_komputerowa"
  ]
  node [
    id 118
    label "media"
  ]
  node [
    id 119
    label "podcast"
  ]
  node [
    id 120
    label "netbook"
  ]
  node [
    id 121
    label "provider"
  ]
  node [
    id 122
    label "cyberprzestrze&#324;"
  ]
  node [
    id 123
    label "grooming"
  ]
  node [
    id 124
    label "strona"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
]
