graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9285714285714286
  density 0.07142857142857142
  graphCliqueNumber 2
  node [
    id 0
    label "dublet"
    origin "text"
  ]
  node [
    id 1
    label "pi&#322;ka"
    origin "text"
  ]
  node [
    id 2
    label "no&#380;ny"
    origin "text"
  ]
  node [
    id 3
    label "kaftan"
  ]
  node [
    id 4
    label "zwyci&#281;stwo"
  ]
  node [
    id 5
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 6
    label "kamie&#324;_jubilerski"
  ]
  node [
    id 7
    label "dru&#380;yna"
  ]
  node [
    id 8
    label "egzemplarz"
  ]
  node [
    id 9
    label "uk&#322;ad_optyczny"
  ]
  node [
    id 10
    label "zagrywka"
  ]
  node [
    id 11
    label "serwowanie"
  ]
  node [
    id 12
    label "sport"
  ]
  node [
    id 13
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 14
    label "sport_zespo&#322;owy"
  ]
  node [
    id 15
    label "odbicie"
  ]
  node [
    id 16
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 17
    label "rzucanka"
  ]
  node [
    id 18
    label "gra"
  ]
  node [
    id 19
    label "aut"
  ]
  node [
    id 20
    label "orb"
  ]
  node [
    id 21
    label "do&#347;rodkowywanie"
  ]
  node [
    id 22
    label "zaserwowanie"
  ]
  node [
    id 23
    label "zaserwowa&#263;"
  ]
  node [
    id 24
    label "kula"
  ]
  node [
    id 25
    label "&#347;wieca"
  ]
  node [
    id 26
    label "serwowa&#263;"
  ]
  node [
    id 27
    label "musket_ball"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
]
