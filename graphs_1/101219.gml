graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.9166666666666667
  density 0.08333333333333333
  graphCliqueNumber 3
  node [
    id 0
    label "bitwa"
    origin "text"
  ]
  node [
    id 1
    label "pod"
    origin "text"
  ]
  node [
    id 2
    label "suchodo&#322;em"
    origin "text"
  ]
  node [
    id 3
    label "zaj&#347;cie"
  ]
  node [
    id 4
    label "batalista"
  ]
  node [
    id 5
    label "walka"
  ]
  node [
    id 6
    label "action"
  ]
  node [
    id 7
    label "bitwa_pod_Pi&#322;awcami"
  ]
  node [
    id 8
    label "Suchodo&#322;em"
  ]
  node [
    id 9
    label "Klemens"
  ]
  node [
    id 10
    label "zeszyt"
  ]
  node [
    id 11
    label "ruszczy&#263;"
  ]
  node [
    id 12
    label "Przemys&#322;"
  ]
  node [
    id 13
    label "i"
  ]
  node [
    id 14
    label "Boles&#322;awa"
  ]
  node [
    id 15
    label "wstydliwy"
  ]
  node [
    id 16
    label "mieszka&#263;"
  ]
  node [
    id 17
    label "ii"
  ]
  node [
    id 18
    label "oty&#322;y"
  ]
  node [
    id 19
    label "Konrada"
  ]
  node [
    id 20
    label "mazowiecki"
  ]
  node [
    id 21
    label "Henryka"
  ]
  node [
    id 22
    label "pobo&#380;ny"
  ]
  node [
    id 23
    label "Mieszko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
]
