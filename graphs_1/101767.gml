graph [
  maxDegree 36
  minDegree 1
  meanDegree 2.0088105726872247
  density 0.008888542357023119
  graphCliqueNumber 3
  node [
    id 0
    label "odst&#281;p"
    origin "text"
  ]
  node [
    id 1
    label "kilka"
    origin "text"
  ]
  node [
    id 2
    label "dni"
    origin "text"
  ]
  node [
    id 3
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "dwa"
    origin "text"
  ]
  node [
    id 6
    label "zupe&#322;nie"
    origin "text"
  ]
  node [
    id 7
    label "sprzeczny"
    origin "text"
  ]
  node [
    id 8
    label "diagnoza"
    origin "text"
  ]
  node [
    id 9
    label "wp&#322;yw"
    origin "text"
  ]
  node [
    id 10
    label "kryzys"
    origin "text"
  ]
  node [
    id 11
    label "wolne"
    origin "text"
  ]
  node [
    id 12
    label "oprogramowanie"
    origin "text"
  ]
  node [
    id 13
    label "wolny"
    origin "text"
  ]
  node [
    id 14
    label "kultura"
    origin "text"
  ]
  node [
    id 15
    label "naro&#380;nik"
    origin "text"
  ]
  node [
    id 16
    label "czerwona"
    origin "text"
  ]
  node [
    id 17
    label "strona"
    origin "text"
  ]
  node [
    id 18
    label "popyt"
    origin "text"
  ]
  node [
    id 19
    label "szef"
    origin "text"
  ]
  node [
    id 20
    label "reda"
    origin "text"
  ]
  node [
    id 21
    label "hata"
    origin "text"
  ]
  node [
    id 22
    label "jim"
    origin "text"
  ]
  node [
    id 23
    label "whitehurst"
    origin "text"
  ]
  node [
    id 24
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 25
    label "wszystek"
    origin "text"
  ]
  node [
    id 26
    label "zam&#243;wienie"
    origin "text"
  ]
  node [
    id 27
    label "bez"
    origin "text"
  ]
  node [
    id 28
    label "op&#322;ata"
    origin "text"
  ]
  node [
    id 29
    label "licencyjny"
    origin "text"
  ]
  node [
    id 30
    label "abcug"
  ]
  node [
    id 31
    label "miejsce"
  ]
  node [
    id 32
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 33
    label "&#347;ledziowate"
  ]
  node [
    id 34
    label "ryba"
  ]
  node [
    id 35
    label "czas"
  ]
  node [
    id 36
    label "kompletny"
  ]
  node [
    id 37
    label "wniwecz"
  ]
  node [
    id 38
    label "zupe&#322;ny"
  ]
  node [
    id 39
    label "sprzecznie"
  ]
  node [
    id 40
    label "niezgodny"
  ]
  node [
    id 41
    label "dokument"
  ]
  node [
    id 42
    label "diagnosis"
  ]
  node [
    id 43
    label "sprawdzian"
  ]
  node [
    id 44
    label "schorzenie"
  ]
  node [
    id 45
    label "rozpoznanie"
  ]
  node [
    id 46
    label "ocena"
  ]
  node [
    id 47
    label "&#347;lad"
  ]
  node [
    id 48
    label "doch&#243;d_narodowy"
  ]
  node [
    id 49
    label "zjawisko"
  ]
  node [
    id 50
    label "rezultat"
  ]
  node [
    id 51
    label "kwota"
  ]
  node [
    id 52
    label "lobbysta"
  ]
  node [
    id 53
    label "Marzec_'68"
  ]
  node [
    id 54
    label "cykl_koniunkturalny"
  ]
  node [
    id 55
    label "zwrot"
  ]
  node [
    id 56
    label "k&#322;opot"
  ]
  node [
    id 57
    label "head"
  ]
  node [
    id 58
    label "July"
  ]
  node [
    id 59
    label "pogorszenie"
  ]
  node [
    id 60
    label "sytuacja"
  ]
  node [
    id 61
    label "czas_wolny"
  ]
  node [
    id 62
    label "program"
  ]
  node [
    id 63
    label "zbi&#243;r"
  ]
  node [
    id 64
    label "reengineering"
  ]
  node [
    id 65
    label "niezale&#380;ny"
  ]
  node [
    id 66
    label "swobodnie"
  ]
  node [
    id 67
    label "niespieszny"
  ]
  node [
    id 68
    label "rozrzedzanie"
  ]
  node [
    id 69
    label "zwolnienie_si&#281;"
  ]
  node [
    id 70
    label "wolno"
  ]
  node [
    id 71
    label "rozrzedzenie"
  ]
  node [
    id 72
    label "lu&#378;no"
  ]
  node [
    id 73
    label "zwalnianie_si&#281;"
  ]
  node [
    id 74
    label "wolnie"
  ]
  node [
    id 75
    label "strza&#322;"
  ]
  node [
    id 76
    label "rozwodnienie"
  ]
  node [
    id 77
    label "wakowa&#263;"
  ]
  node [
    id 78
    label "rozwadnianie"
  ]
  node [
    id 79
    label "rzedni&#281;cie"
  ]
  node [
    id 80
    label "zrzedni&#281;cie"
  ]
  node [
    id 81
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 82
    label "przedmiot"
  ]
  node [
    id 83
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 84
    label "Wsch&#243;d"
  ]
  node [
    id 85
    label "rzecz"
  ]
  node [
    id 86
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 87
    label "sztuka"
  ]
  node [
    id 88
    label "religia"
  ]
  node [
    id 89
    label "przejmowa&#263;"
  ]
  node [
    id 90
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 91
    label "makrokosmos"
  ]
  node [
    id 92
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 93
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 94
    label "praca_rolnicza"
  ]
  node [
    id 95
    label "tradycja"
  ]
  node [
    id 96
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 97
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 98
    label "przejmowanie"
  ]
  node [
    id 99
    label "cecha"
  ]
  node [
    id 100
    label "asymilowanie_si&#281;"
  ]
  node [
    id 101
    label "przej&#261;&#263;"
  ]
  node [
    id 102
    label "hodowla"
  ]
  node [
    id 103
    label "brzoskwiniarnia"
  ]
  node [
    id 104
    label "populace"
  ]
  node [
    id 105
    label "konwencja"
  ]
  node [
    id 106
    label "propriety"
  ]
  node [
    id 107
    label "jako&#347;&#263;"
  ]
  node [
    id 108
    label "kuchnia"
  ]
  node [
    id 109
    label "zwyczaj"
  ]
  node [
    id 110
    label "przej&#281;cie"
  ]
  node [
    id 111
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 112
    label "okucie"
  ]
  node [
    id 113
    label "figura_zaszczytna"
  ]
  node [
    id 114
    label "kraw&#281;d&#378;"
  ]
  node [
    id 115
    label "corner"
  ]
  node [
    id 116
    label "mebel"
  ]
  node [
    id 117
    label "skr&#281;canie"
  ]
  node [
    id 118
    label "voice"
  ]
  node [
    id 119
    label "forma"
  ]
  node [
    id 120
    label "internet"
  ]
  node [
    id 121
    label "skr&#281;ci&#263;"
  ]
  node [
    id 122
    label "kartka"
  ]
  node [
    id 123
    label "orientowa&#263;"
  ]
  node [
    id 124
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 125
    label "powierzchnia"
  ]
  node [
    id 126
    label "plik"
  ]
  node [
    id 127
    label "bok"
  ]
  node [
    id 128
    label "pagina"
  ]
  node [
    id 129
    label "orientowanie"
  ]
  node [
    id 130
    label "fragment"
  ]
  node [
    id 131
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 132
    label "s&#261;d"
  ]
  node [
    id 133
    label "skr&#281;ca&#263;"
  ]
  node [
    id 134
    label "g&#243;ra"
  ]
  node [
    id 135
    label "serwis_internetowy"
  ]
  node [
    id 136
    label "orientacja"
  ]
  node [
    id 137
    label "linia"
  ]
  node [
    id 138
    label "skr&#281;cenie"
  ]
  node [
    id 139
    label "layout"
  ]
  node [
    id 140
    label "zorientowa&#263;"
  ]
  node [
    id 141
    label "zorientowanie"
  ]
  node [
    id 142
    label "obiekt"
  ]
  node [
    id 143
    label "podmiot"
  ]
  node [
    id 144
    label "ty&#322;"
  ]
  node [
    id 145
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 146
    label "logowanie"
  ]
  node [
    id 147
    label "adres_internetowy"
  ]
  node [
    id 148
    label "uj&#281;cie"
  ]
  node [
    id 149
    label "prz&#243;d"
  ]
  node [
    id 150
    label "posta&#263;"
  ]
  node [
    id 151
    label "elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 152
    label "krzywa_popytu"
  ]
  node [
    id 153
    label "popyt_zagregowany"
  ]
  node [
    id 154
    label "mechanizm_mno&#380;nikowy"
  ]
  node [
    id 155
    label "proces_ekonomiczny"
  ]
  node [
    id 156
    label "handel"
  ]
  node [
    id 157
    label "nawis_inflacyjny"
  ]
  node [
    id 158
    label "lepko&#347;&#263;_cen"
  ]
  node [
    id 159
    label "cena_r&#243;wnowagi_rynkowej"
  ]
  node [
    id 160
    label "czynnik_niecenowy"
  ]
  node [
    id 161
    label "cz&#322;owiek"
  ]
  node [
    id 162
    label "kierowa&#263;"
  ]
  node [
    id 163
    label "kierownictwo"
  ]
  node [
    id 164
    label "pryncypa&#322;"
  ]
  node [
    id 165
    label "akwatorium"
  ]
  node [
    id 166
    label "morze"
  ]
  node [
    id 167
    label "falochron"
  ]
  node [
    id 168
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 169
    label "perceive"
  ]
  node [
    id 170
    label "reagowa&#263;"
  ]
  node [
    id 171
    label "spowodowa&#263;"
  ]
  node [
    id 172
    label "male&#263;"
  ]
  node [
    id 173
    label "zmale&#263;"
  ]
  node [
    id 174
    label "spotka&#263;"
  ]
  node [
    id 175
    label "go_steady"
  ]
  node [
    id 176
    label "dostrzega&#263;"
  ]
  node [
    id 177
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 178
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 179
    label "ogl&#261;da&#263;"
  ]
  node [
    id 180
    label "os&#261;dza&#263;"
  ]
  node [
    id 181
    label "aprobowa&#263;"
  ]
  node [
    id 182
    label "punkt_widzenia"
  ]
  node [
    id 183
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 184
    label "wzrok"
  ]
  node [
    id 185
    label "postrzega&#263;"
  ]
  node [
    id 186
    label "notice"
  ]
  node [
    id 187
    label "ca&#322;y"
  ]
  node [
    id 188
    label "zg&#322;oszenie"
  ]
  node [
    id 189
    label "zaczarowanie"
  ]
  node [
    id 190
    label "zamawianie"
  ]
  node [
    id 191
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 192
    label "rozdysponowanie"
  ]
  node [
    id 193
    label "polecenie"
  ]
  node [
    id 194
    label "transakcja"
  ]
  node [
    id 195
    label "order"
  ]
  node [
    id 196
    label "zamawia&#263;"
  ]
  node [
    id 197
    label "indent"
  ]
  node [
    id 198
    label "perpetration"
  ]
  node [
    id 199
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 200
    label "zarezerwowanie"
  ]
  node [
    id 201
    label "zam&#243;wi&#263;"
  ]
  node [
    id 202
    label "zlecenie"
  ]
  node [
    id 203
    label "ki&#347;&#263;"
  ]
  node [
    id 204
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 205
    label "krzew"
  ]
  node [
    id 206
    label "pi&#380;maczkowate"
  ]
  node [
    id 207
    label "pestkowiec"
  ]
  node [
    id 208
    label "kwiat"
  ]
  node [
    id 209
    label "owoc"
  ]
  node [
    id 210
    label "oliwkowate"
  ]
  node [
    id 211
    label "ro&#347;lina"
  ]
  node [
    id 212
    label "hy&#263;ka"
  ]
  node [
    id 213
    label "lilac"
  ]
  node [
    id 214
    label "delfinidyna"
  ]
  node [
    id 215
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 216
    label "Hata"
  ]
  node [
    id 217
    label "Jim"
  ]
  node [
    id 218
    label "Whitehurst"
  ]
  node [
    id 219
    label "Andrew"
  ]
  node [
    id 220
    label "Keen"
  ]
  node [
    id 221
    label "weba"
  ]
  node [
    id 222
    label "2"
  ]
  node [
    id 223
    label "0"
  ]
  node [
    id 224
    label "Hat"
  ]
  node [
    id 225
    label "bill"
  ]
  node [
    id 226
    label "Gates"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 49
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 55
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 168
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 170
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 24
    target 178
  ]
  edge [
    source 24
    target 179
  ]
  edge [
    source 24
    target 180
  ]
  edge [
    source 24
    target 181
  ]
  edge [
    source 24
    target 182
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 187
  ]
  edge [
    source 26
    target 188
  ]
  edge [
    source 26
    target 189
  ]
  edge [
    source 26
    target 190
  ]
  edge [
    source 26
    target 191
  ]
  edge [
    source 26
    target 192
  ]
  edge [
    source 26
    target 193
  ]
  edge [
    source 26
    target 194
  ]
  edge [
    source 26
    target 195
  ]
  edge [
    source 26
    target 196
  ]
  edge [
    source 26
    target 197
  ]
  edge [
    source 26
    target 198
  ]
  edge [
    source 26
    target 199
  ]
  edge [
    source 26
    target 200
  ]
  edge [
    source 26
    target 201
  ]
  edge [
    source 26
    target 202
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 203
  ]
  edge [
    source 27
    target 204
  ]
  edge [
    source 27
    target 205
  ]
  edge [
    source 27
    target 206
  ]
  edge [
    source 27
    target 207
  ]
  edge [
    source 27
    target 208
  ]
  edge [
    source 27
    target 209
  ]
  edge [
    source 27
    target 210
  ]
  edge [
    source 27
    target 211
  ]
  edge [
    source 27
    target 212
  ]
  edge [
    source 27
    target 213
  ]
  edge [
    source 27
    target 214
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 215
  ]
  edge [
    source 28
    target 51
  ]
  edge [
    source 217
    target 218
  ]
  edge [
    source 219
    target 220
  ]
  edge [
    source 221
    target 222
  ]
  edge [
    source 221
    target 223
  ]
  edge [
    source 222
    target 223
  ]
  edge [
    source 225
    target 226
  ]
]
