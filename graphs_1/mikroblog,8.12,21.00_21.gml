graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.9692307692307693
  density 0.03076923076923077
  graphCliqueNumber 2
  node [
    id 0
    label "mireczka"
    origin "text"
  ]
  node [
    id 1
    label "m&#281;ski"
    origin "text"
  ]
  node [
    id 2
    label "getr"
    origin "text"
  ]
  node [
    id 3
    label "na_przyk&#322;ad"
    origin "text"
  ]
  node [
    id 4
    label "si&#322;ownia"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "oko"
    origin "text"
  ]
  node [
    id 7
    label "pedalski"
    origin "text"
  ]
  node [
    id 8
    label "toaleta"
  ]
  node [
    id 9
    label "zdecydowany"
  ]
  node [
    id 10
    label "doros&#322;y"
  ]
  node [
    id 11
    label "stosowny"
  ]
  node [
    id 12
    label "prawdziwy"
  ]
  node [
    id 13
    label "odr&#281;bny"
  ]
  node [
    id 14
    label "m&#281;sko"
  ]
  node [
    id 15
    label "typowy"
  ]
  node [
    id 16
    label "podobny"
  ]
  node [
    id 17
    label "po_m&#281;sku"
  ]
  node [
    id 18
    label "pakowa&#263;"
  ]
  node [
    id 19
    label "kulturysta"
  ]
  node [
    id 20
    label "obiekt"
  ]
  node [
    id 21
    label "budowla"
  ]
  node [
    id 22
    label "zak&#322;ad_komunalny"
  ]
  node [
    id 23
    label "pakernia"
  ]
  node [
    id 24
    label "si&#281;ga&#263;"
  ]
  node [
    id 25
    label "trwa&#263;"
  ]
  node [
    id 26
    label "obecno&#347;&#263;"
  ]
  node [
    id 27
    label "stan"
  ]
  node [
    id 28
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 29
    label "stand"
  ]
  node [
    id 30
    label "mie&#263;_miejsce"
  ]
  node [
    id 31
    label "uczestniczy&#263;"
  ]
  node [
    id 32
    label "chodzi&#263;"
  ]
  node [
    id 33
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 34
    label "equal"
  ]
  node [
    id 35
    label "wypowied&#378;"
  ]
  node [
    id 36
    label "siniec"
  ]
  node [
    id 37
    label "uwaga"
  ]
  node [
    id 38
    label "rzecz"
  ]
  node [
    id 39
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 40
    label "powieka"
  ]
  node [
    id 41
    label "oczy"
  ]
  node [
    id 42
    label "&#347;lepko"
  ]
  node [
    id 43
    label "ga&#322;ka_oczna"
  ]
  node [
    id 44
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 45
    label "&#347;lepie"
  ]
  node [
    id 46
    label "twarz"
  ]
  node [
    id 47
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 48
    label "organ"
  ]
  node [
    id 49
    label "nerw_wzrokowy"
  ]
  node [
    id 50
    label "wzrok"
  ]
  node [
    id 51
    label "spoj&#243;wka"
  ]
  node [
    id 52
    label "&#378;renica"
  ]
  node [
    id 53
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 54
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 55
    label "kaprawie&#263;"
  ]
  node [
    id 56
    label "kaprawienie"
  ]
  node [
    id 57
    label "spojrzenie"
  ]
  node [
    id 58
    label "net"
  ]
  node [
    id 59
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 60
    label "coloboma"
  ]
  node [
    id 61
    label "ros&#243;&#322;"
  ]
  node [
    id 62
    label "niem&#281;ski"
  ]
  node [
    id 63
    label "gejowski"
  ]
  node [
    id 64
    label "pedalsko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
]
