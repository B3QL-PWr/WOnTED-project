graph [
  maxDegree 51
  minDegree 1
  meanDegree 2.1986143187066975
  density 0.005089384997006244
  graphCliqueNumber 3
  node [
    id 0
    label "zmiana"
    origin "text"
  ]
  node [
    id 1
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "pr&#243;ba"
    origin "text"
  ]
  node [
    id 3
    label "opis"
    origin "text"
  ]
  node [
    id 4
    label "konceptualizacja"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "obszar"
    origin "text"
  ]
  node [
    id 7
    label "badanie"
    origin "text"
  ]
  node [
    id 8
    label "kultura"
    origin "text"
  ]
  node [
    id 9
    label "dlatego"
    origin "text"
  ]
  node [
    id 10
    label "uzna&#263;by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "udost&#281;pnia&#263;"
    origin "text"
  ]
  node [
    id 12
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 13
    label "wolny"
    origin "text"
  ]
  node [
    id 14
    label "licencja"
    origin "text"
  ]
  node [
    id 15
    label "raport"
    origin "text"
  ]
  node [
    id 16
    label "powinien"
    origin "text"
  ]
  node [
    id 17
    label "otwarty"
    origin "text"
  ]
  node [
    id 18
    label "nie"
    origin "text"
  ]
  node [
    id 19
    label "poziom"
    origin "text"
  ]
  node [
    id 20
    label "dystrybucja"
    origin "text"
  ]
  node [
    id 21
    label "wybiera&#263;"
    origin "text"
  ]
  node [
    id 22
    label "taka"
    origin "text"
  ]
  node [
    id 23
    label "forma"
    origin "text"
  ]
  node [
    id 24
    label "publikacja"
    origin "text"
  ]
  node [
    id 25
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 26
    label "strona"
    origin "text"
  ]
  node [
    id 27
    label "stan"
    origin "text"
  ]
  node [
    id 28
    label "si&#281;"
    origin "text"
  ]
  node [
    id 29
    label "miejsce"
    origin "text"
  ]
  node [
    id 30
    label "dyskusja"
    origin "text"
  ]
  node [
    id 31
    label "temat"
    origin "text"
  ]
  node [
    id 32
    label "kulturowy"
    origin "text"
  ]
  node [
    id 33
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 34
    label "znaczenie"
    origin "text"
  ]
  node [
    id 35
    label "nowa"
    origin "text"
  ]
  node [
    id 36
    label "technologia"
    origin "text"
  ]
  node [
    id 37
    label "komunikowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "anatomopatolog"
  ]
  node [
    id 39
    label "rewizja"
  ]
  node [
    id 40
    label "oznaka"
  ]
  node [
    id 41
    label "czas"
  ]
  node [
    id 42
    label "ferment"
  ]
  node [
    id 43
    label "komplet"
  ]
  node [
    id 44
    label "tura"
  ]
  node [
    id 45
    label "amendment"
  ]
  node [
    id 46
    label "zmianka"
  ]
  node [
    id 47
    label "odmienianie"
  ]
  node [
    id 48
    label "passage"
  ]
  node [
    id 49
    label "zjawisko"
  ]
  node [
    id 50
    label "change"
  ]
  node [
    id 51
    label "praca"
  ]
  node [
    id 52
    label "usi&#322;owanie"
  ]
  node [
    id 53
    label "pobiera&#263;"
  ]
  node [
    id 54
    label "spotkanie"
  ]
  node [
    id 55
    label "analiza_chemiczna"
  ]
  node [
    id 56
    label "test"
  ]
  node [
    id 57
    label "znak"
  ]
  node [
    id 58
    label "item"
  ]
  node [
    id 59
    label "ilo&#347;&#263;"
  ]
  node [
    id 60
    label "effort"
  ]
  node [
    id 61
    label "czynno&#347;&#263;"
  ]
  node [
    id 62
    label "metal_szlachetny"
  ]
  node [
    id 63
    label "pobranie"
  ]
  node [
    id 64
    label "pobieranie"
  ]
  node [
    id 65
    label "sytuacja"
  ]
  node [
    id 66
    label "do&#347;wiadczenie"
  ]
  node [
    id 67
    label "probiernictwo"
  ]
  node [
    id 68
    label "zbi&#243;r"
  ]
  node [
    id 69
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 70
    label "pobra&#263;"
  ]
  node [
    id 71
    label "rezultat"
  ]
  node [
    id 72
    label "exposition"
  ]
  node [
    id 73
    label "wypowied&#378;"
  ]
  node [
    id 74
    label "obja&#347;nienie"
  ]
  node [
    id 75
    label "szko&#322;a"
  ]
  node [
    id 76
    label "proces"
  ]
  node [
    id 77
    label "si&#281;ga&#263;"
  ]
  node [
    id 78
    label "trwa&#263;"
  ]
  node [
    id 79
    label "obecno&#347;&#263;"
  ]
  node [
    id 80
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 81
    label "stand"
  ]
  node [
    id 82
    label "mie&#263;_miejsce"
  ]
  node [
    id 83
    label "uczestniczy&#263;"
  ]
  node [
    id 84
    label "chodzi&#263;"
  ]
  node [
    id 85
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 86
    label "equal"
  ]
  node [
    id 87
    label "pas_planetoid"
  ]
  node [
    id 88
    label "wsch&#243;d"
  ]
  node [
    id 89
    label "Neogea"
  ]
  node [
    id 90
    label "holarktyka"
  ]
  node [
    id 91
    label "Rakowice"
  ]
  node [
    id 92
    label "Kosowo"
  ]
  node [
    id 93
    label "Syberia_Wschodnia"
  ]
  node [
    id 94
    label "wymiar"
  ]
  node [
    id 95
    label "p&#243;&#322;noc"
  ]
  node [
    id 96
    label "akrecja"
  ]
  node [
    id 97
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 98
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 99
    label "terytorium"
  ]
  node [
    id 100
    label "antroposfera"
  ]
  node [
    id 101
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 102
    label "po&#322;udnie"
  ]
  node [
    id 103
    label "zach&#243;d"
  ]
  node [
    id 104
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 105
    label "Olszanica"
  ]
  node [
    id 106
    label "Syberia_Zachodnia"
  ]
  node [
    id 107
    label "przestrze&#324;"
  ]
  node [
    id 108
    label "Notogea"
  ]
  node [
    id 109
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 110
    label "Ruda_Pabianicka"
  ]
  node [
    id 111
    label "&#321;&#281;g"
  ]
  node [
    id 112
    label "Antarktyka"
  ]
  node [
    id 113
    label "Piotrowo"
  ]
  node [
    id 114
    label "Zab&#322;ocie"
  ]
  node [
    id 115
    label "zakres"
  ]
  node [
    id 116
    label "Pow&#261;zki"
  ]
  node [
    id 117
    label "Arktyka"
  ]
  node [
    id 118
    label "Ludwin&#243;w"
  ]
  node [
    id 119
    label "Zabu&#380;e"
  ]
  node [
    id 120
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 121
    label "Kresy_Zachodnie"
  ]
  node [
    id 122
    label "examination"
  ]
  node [
    id 123
    label "investigation"
  ]
  node [
    id 124
    label "ustalenie"
  ]
  node [
    id 125
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 126
    label "ustalanie"
  ]
  node [
    id 127
    label "bia&#322;a_niedziela"
  ]
  node [
    id 128
    label "analysis"
  ]
  node [
    id 129
    label "rozpatrywanie"
  ]
  node [
    id 130
    label "wziernikowanie"
  ]
  node [
    id 131
    label "obserwowanie"
  ]
  node [
    id 132
    label "omawianie"
  ]
  node [
    id 133
    label "sprawdzanie"
  ]
  node [
    id 134
    label "udowadnianie"
  ]
  node [
    id 135
    label "diagnostyka"
  ]
  node [
    id 136
    label "macanie"
  ]
  node [
    id 137
    label "rektalny"
  ]
  node [
    id 138
    label "penetrowanie"
  ]
  node [
    id 139
    label "krytykowanie"
  ]
  node [
    id 140
    label "kontrola"
  ]
  node [
    id 141
    label "dociekanie"
  ]
  node [
    id 142
    label "zrecenzowanie"
  ]
  node [
    id 143
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 144
    label "przedmiot"
  ]
  node [
    id 145
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 146
    label "Wsch&#243;d"
  ]
  node [
    id 147
    label "rzecz"
  ]
  node [
    id 148
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 149
    label "sztuka"
  ]
  node [
    id 150
    label "religia"
  ]
  node [
    id 151
    label "przejmowa&#263;"
  ]
  node [
    id 152
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 153
    label "makrokosmos"
  ]
  node [
    id 154
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 155
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 156
    label "praca_rolnicza"
  ]
  node [
    id 157
    label "tradycja"
  ]
  node [
    id 158
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 159
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 160
    label "przejmowanie"
  ]
  node [
    id 161
    label "cecha"
  ]
  node [
    id 162
    label "asymilowanie_si&#281;"
  ]
  node [
    id 163
    label "przej&#261;&#263;"
  ]
  node [
    id 164
    label "hodowla"
  ]
  node [
    id 165
    label "brzoskwiniarnia"
  ]
  node [
    id 166
    label "populace"
  ]
  node [
    id 167
    label "konwencja"
  ]
  node [
    id 168
    label "propriety"
  ]
  node [
    id 169
    label "jako&#347;&#263;"
  ]
  node [
    id 170
    label "kuchnia"
  ]
  node [
    id 171
    label "zwyczaj"
  ]
  node [
    id 172
    label "przej&#281;cie"
  ]
  node [
    id 173
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 174
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 175
    label "faza"
  ]
  node [
    id 176
    label "upgrade"
  ]
  node [
    id 177
    label "pierworodztwo"
  ]
  node [
    id 178
    label "nast&#281;pstwo"
  ]
  node [
    id 179
    label "niezale&#380;ny"
  ]
  node [
    id 180
    label "swobodnie"
  ]
  node [
    id 181
    label "niespieszny"
  ]
  node [
    id 182
    label "rozrzedzanie"
  ]
  node [
    id 183
    label "zwolnienie_si&#281;"
  ]
  node [
    id 184
    label "wolno"
  ]
  node [
    id 185
    label "rozrzedzenie"
  ]
  node [
    id 186
    label "lu&#378;no"
  ]
  node [
    id 187
    label "zwalnianie_si&#281;"
  ]
  node [
    id 188
    label "wolnie"
  ]
  node [
    id 189
    label "strza&#322;"
  ]
  node [
    id 190
    label "rozwodnienie"
  ]
  node [
    id 191
    label "wakowa&#263;"
  ]
  node [
    id 192
    label "rozwadnianie"
  ]
  node [
    id 193
    label "rzedni&#281;cie"
  ]
  node [
    id 194
    label "zrzedni&#281;cie"
  ]
  node [
    id 195
    label "prawo"
  ]
  node [
    id 196
    label "licencjonowa&#263;"
  ]
  node [
    id 197
    label "pozwolenie"
  ]
  node [
    id 198
    label "rasowy"
  ]
  node [
    id 199
    label "license"
  ]
  node [
    id 200
    label "zezwolenie"
  ]
  node [
    id 201
    label "za&#347;wiadczenie"
  ]
  node [
    id 202
    label "raport_Beveridge'a"
  ]
  node [
    id 203
    label "relacja"
  ]
  node [
    id 204
    label "raport_Fischlera"
  ]
  node [
    id 205
    label "statement"
  ]
  node [
    id 206
    label "musie&#263;"
  ]
  node [
    id 207
    label "due"
  ]
  node [
    id 208
    label "ewidentny"
  ]
  node [
    id 209
    label "bezpo&#347;redni"
  ]
  node [
    id 210
    label "otwarcie"
  ]
  node [
    id 211
    label "nieograniczony"
  ]
  node [
    id 212
    label "zdecydowany"
  ]
  node [
    id 213
    label "gotowy"
  ]
  node [
    id 214
    label "aktualny"
  ]
  node [
    id 215
    label "prostoduszny"
  ]
  node [
    id 216
    label "jawnie"
  ]
  node [
    id 217
    label "otworzysty"
  ]
  node [
    id 218
    label "dost&#281;pny"
  ]
  node [
    id 219
    label "publiczny"
  ]
  node [
    id 220
    label "aktywny"
  ]
  node [
    id 221
    label "kontaktowy"
  ]
  node [
    id 222
    label "sprzeciw"
  ]
  node [
    id 223
    label "wysoko&#347;&#263;"
  ]
  node [
    id 224
    label "szczebel"
  ]
  node [
    id 225
    label "po&#322;o&#380;enie"
  ]
  node [
    id 226
    label "kierunek"
  ]
  node [
    id 227
    label "wyk&#322;adnik"
  ]
  node [
    id 228
    label "budynek"
  ]
  node [
    id 229
    label "punkt_widzenia"
  ]
  node [
    id 230
    label "ranga"
  ]
  node [
    id 231
    label "p&#322;aszczyzna"
  ]
  node [
    id 232
    label "podzia&#322;"
  ]
  node [
    id 233
    label "sie&#263;_rybacka"
  ]
  node [
    id 234
    label "wyjmowa&#263;"
  ]
  node [
    id 235
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 236
    label "powo&#322;ywa&#263;"
  ]
  node [
    id 237
    label "kotwica"
  ]
  node [
    id 238
    label "take"
  ]
  node [
    id 239
    label "ustala&#263;"
  ]
  node [
    id 240
    label "Bangladesz"
  ]
  node [
    id 241
    label "jednostka_monetarna"
  ]
  node [
    id 242
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 243
    label "do&#322;ek"
  ]
  node [
    id 244
    label "formality"
  ]
  node [
    id 245
    label "wz&#243;r"
  ]
  node [
    id 246
    label "kantyzm"
  ]
  node [
    id 247
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 248
    label "ornamentyka"
  ]
  node [
    id 249
    label "odmiana"
  ]
  node [
    id 250
    label "mode"
  ]
  node [
    id 251
    label "style"
  ]
  node [
    id 252
    label "formacja"
  ]
  node [
    id 253
    label "maszyna_drukarska"
  ]
  node [
    id 254
    label "poznanie"
  ]
  node [
    id 255
    label "szablon"
  ]
  node [
    id 256
    label "struktura"
  ]
  node [
    id 257
    label "spirala"
  ]
  node [
    id 258
    label "blaszka"
  ]
  node [
    id 259
    label "linearno&#347;&#263;"
  ]
  node [
    id 260
    label "g&#322;owa"
  ]
  node [
    id 261
    label "kielich"
  ]
  node [
    id 262
    label "zdolno&#347;&#263;"
  ]
  node [
    id 263
    label "poj&#281;cie"
  ]
  node [
    id 264
    label "kszta&#322;t"
  ]
  node [
    id 265
    label "pasmo"
  ]
  node [
    id 266
    label "rdze&#324;"
  ]
  node [
    id 267
    label "leksem"
  ]
  node [
    id 268
    label "dyspozycja"
  ]
  node [
    id 269
    label "wygl&#261;d"
  ]
  node [
    id 270
    label "October"
  ]
  node [
    id 271
    label "zawarto&#347;&#263;"
  ]
  node [
    id 272
    label "creation"
  ]
  node [
    id 273
    label "gwiazda"
  ]
  node [
    id 274
    label "p&#281;tla"
  ]
  node [
    id 275
    label "p&#322;at"
  ]
  node [
    id 276
    label "arystotelizm"
  ]
  node [
    id 277
    label "obiekt"
  ]
  node [
    id 278
    label "dzie&#322;o"
  ]
  node [
    id 279
    label "naczynie"
  ]
  node [
    id 280
    label "wyra&#380;enie"
  ]
  node [
    id 281
    label "jednostka_systematyczna"
  ]
  node [
    id 282
    label "miniatura"
  ]
  node [
    id 283
    label "morfem"
  ]
  node [
    id 284
    label "posta&#263;"
  ]
  node [
    id 285
    label "produkcja"
  ]
  node [
    id 286
    label "druk"
  ]
  node [
    id 287
    label "notification"
  ]
  node [
    id 288
    label "tekst"
  ]
  node [
    id 289
    label "report"
  ]
  node [
    id 290
    label "dodawa&#263;"
  ]
  node [
    id 291
    label "wymienia&#263;"
  ]
  node [
    id 292
    label "okre&#347;la&#263;"
  ]
  node [
    id 293
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 294
    label "dyskalkulia"
  ]
  node [
    id 295
    label "wynagrodzenie"
  ]
  node [
    id 296
    label "admit"
  ]
  node [
    id 297
    label "osi&#261;ga&#263;"
  ]
  node [
    id 298
    label "wyznacza&#263;"
  ]
  node [
    id 299
    label "posiada&#263;"
  ]
  node [
    id 300
    label "mierzy&#263;"
  ]
  node [
    id 301
    label "odlicza&#263;"
  ]
  node [
    id 302
    label "bra&#263;"
  ]
  node [
    id 303
    label "wycenia&#263;"
  ]
  node [
    id 304
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 305
    label "rachowa&#263;"
  ]
  node [
    id 306
    label "tell"
  ]
  node [
    id 307
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 308
    label "policza&#263;"
  ]
  node [
    id 309
    label "count"
  ]
  node [
    id 310
    label "skr&#281;canie"
  ]
  node [
    id 311
    label "voice"
  ]
  node [
    id 312
    label "internet"
  ]
  node [
    id 313
    label "skr&#281;ci&#263;"
  ]
  node [
    id 314
    label "kartka"
  ]
  node [
    id 315
    label "orientowa&#263;"
  ]
  node [
    id 316
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 317
    label "powierzchnia"
  ]
  node [
    id 318
    label "plik"
  ]
  node [
    id 319
    label "bok"
  ]
  node [
    id 320
    label "pagina"
  ]
  node [
    id 321
    label "orientowanie"
  ]
  node [
    id 322
    label "fragment"
  ]
  node [
    id 323
    label "s&#261;d"
  ]
  node [
    id 324
    label "skr&#281;ca&#263;"
  ]
  node [
    id 325
    label "g&#243;ra"
  ]
  node [
    id 326
    label "serwis_internetowy"
  ]
  node [
    id 327
    label "orientacja"
  ]
  node [
    id 328
    label "linia"
  ]
  node [
    id 329
    label "skr&#281;cenie"
  ]
  node [
    id 330
    label "layout"
  ]
  node [
    id 331
    label "zorientowa&#263;"
  ]
  node [
    id 332
    label "zorientowanie"
  ]
  node [
    id 333
    label "podmiot"
  ]
  node [
    id 334
    label "ty&#322;"
  ]
  node [
    id 335
    label "logowanie"
  ]
  node [
    id 336
    label "adres_internetowy"
  ]
  node [
    id 337
    label "uj&#281;cie"
  ]
  node [
    id 338
    label "prz&#243;d"
  ]
  node [
    id 339
    label "Arizona"
  ]
  node [
    id 340
    label "Georgia"
  ]
  node [
    id 341
    label "warstwa"
  ]
  node [
    id 342
    label "jednostka_administracyjna"
  ]
  node [
    id 343
    label "Hawaje"
  ]
  node [
    id 344
    label "Goa"
  ]
  node [
    id 345
    label "Floryda"
  ]
  node [
    id 346
    label "Oklahoma"
  ]
  node [
    id 347
    label "punkt"
  ]
  node [
    id 348
    label "Alaska"
  ]
  node [
    id 349
    label "wci&#281;cie"
  ]
  node [
    id 350
    label "Alabama"
  ]
  node [
    id 351
    label "Oregon"
  ]
  node [
    id 352
    label "Teksas"
  ]
  node [
    id 353
    label "Illinois"
  ]
  node [
    id 354
    label "Waszyngton"
  ]
  node [
    id 355
    label "Jukatan"
  ]
  node [
    id 356
    label "shape"
  ]
  node [
    id 357
    label "Nowy_Meksyk"
  ]
  node [
    id 358
    label "state"
  ]
  node [
    id 359
    label "Nowy_York"
  ]
  node [
    id 360
    label "Arakan"
  ]
  node [
    id 361
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 362
    label "Kalifornia"
  ]
  node [
    id 363
    label "wektor"
  ]
  node [
    id 364
    label "Massachusetts"
  ]
  node [
    id 365
    label "Pensylwania"
  ]
  node [
    id 366
    label "Michigan"
  ]
  node [
    id 367
    label "Maryland"
  ]
  node [
    id 368
    label "Ohio"
  ]
  node [
    id 369
    label "Kansas"
  ]
  node [
    id 370
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 371
    label "Luizjana"
  ]
  node [
    id 372
    label "samopoczucie"
  ]
  node [
    id 373
    label "Wirginia"
  ]
  node [
    id 374
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 375
    label "cia&#322;o"
  ]
  node [
    id 376
    label "plac"
  ]
  node [
    id 377
    label "uwaga"
  ]
  node [
    id 378
    label "status"
  ]
  node [
    id 379
    label "chwila"
  ]
  node [
    id 380
    label "rz&#261;d"
  ]
  node [
    id 381
    label "location"
  ]
  node [
    id 382
    label "warunek_lokalowy"
  ]
  node [
    id 383
    label "rozmowa"
  ]
  node [
    id 384
    label "sympozjon"
  ]
  node [
    id 385
    label "conference"
  ]
  node [
    id 386
    label "fraza"
  ]
  node [
    id 387
    label "melodia"
  ]
  node [
    id 388
    label "zbacza&#263;"
  ]
  node [
    id 389
    label "entity"
  ]
  node [
    id 390
    label "omawia&#263;"
  ]
  node [
    id 391
    label "topik"
  ]
  node [
    id 392
    label "wyraz_pochodny"
  ]
  node [
    id 393
    label "om&#243;wi&#263;"
  ]
  node [
    id 394
    label "w&#261;tek"
  ]
  node [
    id 395
    label "forum"
  ]
  node [
    id 396
    label "zboczenie"
  ]
  node [
    id 397
    label "zbaczanie"
  ]
  node [
    id 398
    label "tre&#347;&#263;"
  ]
  node [
    id 399
    label "tematyka"
  ]
  node [
    id 400
    label "sprawa"
  ]
  node [
    id 401
    label "istota"
  ]
  node [
    id 402
    label "otoczka"
  ]
  node [
    id 403
    label "zboczy&#263;"
  ]
  node [
    id 404
    label "om&#243;wienie"
  ]
  node [
    id 405
    label "kulturowo"
  ]
  node [
    id 406
    label "niepubliczny"
  ]
  node [
    id 407
    label "spo&#322;ecznie"
  ]
  node [
    id 408
    label "gravity"
  ]
  node [
    id 409
    label "okre&#347;lanie"
  ]
  node [
    id 410
    label "liczenie"
  ]
  node [
    id 411
    label "odgrywanie_roli"
  ]
  node [
    id 412
    label "wskazywanie"
  ]
  node [
    id 413
    label "bycie"
  ]
  node [
    id 414
    label "weight"
  ]
  node [
    id 415
    label "command"
  ]
  node [
    id 416
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 417
    label "informacja"
  ]
  node [
    id 418
    label "odk&#322;adanie"
  ]
  node [
    id 419
    label "wyraz"
  ]
  node [
    id 420
    label "assay"
  ]
  node [
    id 421
    label "condition"
  ]
  node [
    id 422
    label "kto&#347;"
  ]
  node [
    id 423
    label "stawianie"
  ]
  node [
    id 424
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 425
    label "engineering"
  ]
  node [
    id 426
    label "technika"
  ]
  node [
    id 427
    label "mikrotechnologia"
  ]
  node [
    id 428
    label "technologia_nieorganiczna"
  ]
  node [
    id 429
    label "biotechnologia"
  ]
  node [
    id 430
    label "spos&#243;b"
  ]
  node [
    id 431
    label "powodowa&#263;"
  ]
  node [
    id 432
    label "communicate"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 180
  ]
  edge [
    source 13
    target 181
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 204
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 207
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 208
  ]
  edge [
    source 17
    target 209
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 212
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 17
    target 214
  ]
  edge [
    source 17
    target 215
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 217
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 219
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 222
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 223
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 69
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 232
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 233
  ]
  edge [
    source 21
    target 234
  ]
  edge [
    source 21
    target 235
  ]
  edge [
    source 21
    target 236
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 21
    target 239
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 240
  ]
  edge [
    source 22
    target 241
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 229
  ]
  edge [
    source 23
    target 243
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 23
    target 250
  ]
  edge [
    source 23
    target 251
  ]
  edge [
    source 23
    target 252
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 254
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 256
  ]
  edge [
    source 23
    target 257
  ]
  edge [
    source 23
    target 258
  ]
  edge [
    source 23
    target 259
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 161
  ]
  edge [
    source 23
    target 260
  ]
  edge [
    source 23
    target 261
  ]
  edge [
    source 23
    target 262
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 23
    target 281
  ]
  edge [
    source 23
    target 282
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 283
  ]
  edge [
    source 23
    target 284
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 285
  ]
  edge [
    source 24
    target 286
  ]
  edge [
    source 24
    target 287
  ]
  edge [
    source 24
    target 288
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 289
  ]
  edge [
    source 25
    target 290
  ]
  edge [
    source 25
    target 291
  ]
  edge [
    source 25
    target 292
  ]
  edge [
    source 25
    target 293
  ]
  edge [
    source 25
    target 294
  ]
  edge [
    source 25
    target 295
  ]
  edge [
    source 25
    target 296
  ]
  edge [
    source 25
    target 297
  ]
  edge [
    source 25
    target 298
  ]
  edge [
    source 25
    target 299
  ]
  edge [
    source 25
    target 300
  ]
  edge [
    source 25
    target 301
  ]
  edge [
    source 25
    target 302
  ]
  edge [
    source 25
    target 303
  ]
  edge [
    source 25
    target 304
  ]
  edge [
    source 25
    target 305
  ]
  edge [
    source 25
    target 306
  ]
  edge [
    source 25
    target 307
  ]
  edge [
    source 25
    target 308
  ]
  edge [
    source 25
    target 309
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 310
  ]
  edge [
    source 26
    target 311
  ]
  edge [
    source 26
    target 312
  ]
  edge [
    source 26
    target 313
  ]
  edge [
    source 26
    target 314
  ]
  edge [
    source 26
    target 315
  ]
  edge [
    source 26
    target 316
  ]
  edge [
    source 26
    target 317
  ]
  edge [
    source 26
    target 318
  ]
  edge [
    source 26
    target 319
  ]
  edge [
    source 26
    target 320
  ]
  edge [
    source 26
    target 321
  ]
  edge [
    source 26
    target 322
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 26
    target 323
  ]
  edge [
    source 26
    target 324
  ]
  edge [
    source 26
    target 325
  ]
  edge [
    source 26
    target 326
  ]
  edge [
    source 26
    target 327
  ]
  edge [
    source 26
    target 328
  ]
  edge [
    source 26
    target 329
  ]
  edge [
    source 26
    target 330
  ]
  edge [
    source 26
    target 331
  ]
  edge [
    source 26
    target 332
  ]
  edge [
    source 26
    target 277
  ]
  edge [
    source 26
    target 333
  ]
  edge [
    source 26
    target 334
  ]
  edge [
    source 26
    target 69
  ]
  edge [
    source 26
    target 335
  ]
  edge [
    source 26
    target 336
  ]
  edge [
    source 26
    target 337
  ]
  edge [
    source 26
    target 338
  ]
  edge [
    source 26
    target 284
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 339
  ]
  edge [
    source 27
    target 340
  ]
  edge [
    source 27
    target 341
  ]
  edge [
    source 27
    target 342
  ]
  edge [
    source 27
    target 343
  ]
  edge [
    source 27
    target 344
  ]
  edge [
    source 27
    target 345
  ]
  edge [
    source 27
    target 346
  ]
  edge [
    source 27
    target 347
  ]
  edge [
    source 27
    target 348
  ]
  edge [
    source 27
    target 349
  ]
  edge [
    source 27
    target 350
  ]
  edge [
    source 27
    target 351
  ]
  edge [
    source 27
    target 352
  ]
  edge [
    source 27
    target 353
  ]
  edge [
    source 27
    target 354
  ]
  edge [
    source 27
    target 355
  ]
  edge [
    source 27
    target 356
  ]
  edge [
    source 27
    target 357
  ]
  edge [
    source 27
    target 59
  ]
  edge [
    source 27
    target 358
  ]
  edge [
    source 27
    target 359
  ]
  edge [
    source 27
    target 360
  ]
  edge [
    source 27
    target 361
  ]
  edge [
    source 27
    target 362
  ]
  edge [
    source 27
    target 363
  ]
  edge [
    source 27
    target 364
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 365
  ]
  edge [
    source 27
    target 366
  ]
  edge [
    source 27
    target 367
  ]
  edge [
    source 27
    target 368
  ]
  edge [
    source 27
    target 369
  ]
  edge [
    source 27
    target 370
  ]
  edge [
    source 27
    target 371
  ]
  edge [
    source 27
    target 372
  ]
  edge [
    source 27
    target 373
  ]
  edge [
    source 27
    target 374
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 375
  ]
  edge [
    source 29
    target 376
  ]
  edge [
    source 29
    target 161
  ]
  edge [
    source 29
    target 377
  ]
  edge [
    source 29
    target 107
  ]
  edge [
    source 29
    target 378
  ]
  edge [
    source 29
    target 316
  ]
  edge [
    source 29
    target 379
  ]
  edge [
    source 29
    target 69
  ]
  edge [
    source 29
    target 380
  ]
  edge [
    source 29
    target 51
  ]
  edge [
    source 29
    target 381
  ]
  edge [
    source 29
    target 382
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 383
  ]
  edge [
    source 30
    target 384
  ]
  edge [
    source 30
    target 385
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 386
  ]
  edge [
    source 31
    target 387
  ]
  edge [
    source 31
    target 147
  ]
  edge [
    source 31
    target 388
  ]
  edge [
    source 31
    target 389
  ]
  edge [
    source 31
    target 390
  ]
  edge [
    source 31
    target 391
  ]
  edge [
    source 31
    target 392
  ]
  edge [
    source 31
    target 393
  ]
  edge [
    source 31
    target 132
  ]
  edge [
    source 31
    target 394
  ]
  edge [
    source 31
    target 395
  ]
  edge [
    source 31
    target 161
  ]
  edge [
    source 31
    target 396
  ]
  edge [
    source 31
    target 397
  ]
  edge [
    source 31
    target 398
  ]
  edge [
    source 31
    target 399
  ]
  edge [
    source 31
    target 400
  ]
  edge [
    source 31
    target 401
  ]
  edge [
    source 31
    target 402
  ]
  edge [
    source 31
    target 403
  ]
  edge [
    source 31
    target 404
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 405
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 406
  ]
  edge [
    source 33
    target 407
  ]
  edge [
    source 33
    target 219
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 408
  ]
  edge [
    source 34
    target 409
  ]
  edge [
    source 34
    target 410
  ]
  edge [
    source 34
    target 411
  ]
  edge [
    source 34
    target 412
  ]
  edge [
    source 34
    target 413
  ]
  edge [
    source 34
    target 414
  ]
  edge [
    source 34
    target 415
  ]
  edge [
    source 34
    target 401
  ]
  edge [
    source 34
    target 161
  ]
  edge [
    source 34
    target 416
  ]
  edge [
    source 34
    target 417
  ]
  edge [
    source 34
    target 418
  ]
  edge [
    source 34
    target 280
  ]
  edge [
    source 34
    target 419
  ]
  edge [
    source 34
    target 420
  ]
  edge [
    source 34
    target 421
  ]
  edge [
    source 34
    target 422
  ]
  edge [
    source 34
    target 423
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 273
  ]
  edge [
    source 35
    target 424
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 425
  ]
  edge [
    source 36
    target 426
  ]
  edge [
    source 36
    target 427
  ]
  edge [
    source 36
    target 428
  ]
  edge [
    source 36
    target 429
  ]
  edge [
    source 36
    target 430
  ]
  edge [
    source 37
    target 431
  ]
  edge [
    source 37
    target 432
  ]
]
