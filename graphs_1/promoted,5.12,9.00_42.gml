graph [
  maxDegree 11
  minDegree 1
  meanDegree 3.066666666666667
  density 0.21904761904761905
  graphCliqueNumber 2
  node [
    id 0
    label "taka"
    origin "text"
  ]
  node [
    id 1
    label "drobnostka"
    origin "text"
  ]
  node [
    id 2
    label "wr&#281;cz"
    origin "text"
  ]
  node [
    id 3
    label "b&#322;ahostka"
    origin "text"
  ]
  node [
    id 4
    label "Bangladesz"
  ]
  node [
    id 5
    label "jednostka_monetarna"
  ]
  node [
    id 6
    label "banalny"
  ]
  node [
    id 7
    label "fraszka"
  ]
  node [
    id 8
    label "fidryga&#322;ki"
  ]
  node [
    id 9
    label "szczeg&#243;&#322;"
  ]
  node [
    id 10
    label "g&#243;wno"
  ]
  node [
    id 11
    label "furda"
  ]
  node [
    id 12
    label "ma&#322;y_przedmiot"
  ]
  node [
    id 13
    label "sofcik"
  ]
  node [
    id 14
    label "triviality"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
]
