graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.1379310344827585
  density 0.03750756200846945
  graphCliqueNumber 5
  node [
    id 0
    label "pojazd"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "przyspiesza&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 4
    label "sekunda"
    origin "text"
  ]
  node [
    id 5
    label "fukanie"
  ]
  node [
    id 6
    label "przeszklenie"
  ]
  node [
    id 7
    label "pod&#322;oga"
  ]
  node [
    id 8
    label "odzywka"
  ]
  node [
    id 9
    label "powietrze"
  ]
  node [
    id 10
    label "przyholowanie"
  ]
  node [
    id 11
    label "fukni&#281;cie"
  ]
  node [
    id 12
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 13
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 14
    label "hamulec"
  ]
  node [
    id 15
    label "nadwozie"
  ]
  node [
    id 16
    label "zielona_karta"
  ]
  node [
    id 17
    label "przyholowywa&#263;"
  ]
  node [
    id 18
    label "test_zderzeniowy"
  ]
  node [
    id 19
    label "odholowanie"
  ]
  node [
    id 20
    label "tabor"
  ]
  node [
    id 21
    label "odholowywanie"
  ]
  node [
    id 22
    label "przyholowywanie"
  ]
  node [
    id 23
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 24
    label "l&#261;d"
  ]
  node [
    id 25
    label "przyholowa&#263;"
  ]
  node [
    id 26
    label "odholowa&#263;"
  ]
  node [
    id 27
    label "podwozie"
  ]
  node [
    id 28
    label "woda"
  ]
  node [
    id 29
    label "odholowywa&#263;"
  ]
  node [
    id 30
    label "prowadzenie_si&#281;"
  ]
  node [
    id 31
    label "czyj&#347;"
  ]
  node [
    id 32
    label "m&#261;&#380;"
  ]
  node [
    id 33
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 34
    label "przek&#322;ada&#263;"
  ]
  node [
    id 35
    label "boost"
  ]
  node [
    id 36
    label "mo&#380;liwie"
  ]
  node [
    id 37
    label "nieznaczny"
  ]
  node [
    id 38
    label "kr&#243;tko"
  ]
  node [
    id 39
    label "nieistotnie"
  ]
  node [
    id 40
    label "nieliczny"
  ]
  node [
    id 41
    label "mikroskopijnie"
  ]
  node [
    id 42
    label "pomiernie"
  ]
  node [
    id 43
    label "ma&#322;y"
  ]
  node [
    id 44
    label "minuta"
  ]
  node [
    id 45
    label "tercja"
  ]
  node [
    id 46
    label "milisekunda"
  ]
  node [
    id 47
    label "nanosekunda"
  ]
  node [
    id 48
    label "uk&#322;ad_SI"
  ]
  node [
    id 49
    label "mikrosekunda"
  ]
  node [
    id 50
    label "time"
  ]
  node [
    id 51
    label "jednostka_czasu"
  ]
  node [
    id 52
    label "jednostka"
  ]
  node [
    id 53
    label "Chorwat"
  ]
  node [
    id 54
    label "mate"
  ]
  node [
    id 55
    label "&#8217;"
  ]
  node [
    id 56
    label "a"
  ]
  node [
    id 57
    label "Rimaca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 56
    target 57
  ]
]
