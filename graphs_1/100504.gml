graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.066666666666667
  density 0.07126436781609195
  graphCliqueNumber 4
  node [
    id 0
    label "park"
    origin "text"
  ]
  node [
    id 1
    label "tadeusz"
    origin "text"
  ]
  node [
    id 2
    label "rejtan"
    origin "text"
  ]
  node [
    id 3
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 4
    label "teren_przemys&#322;owy"
  ]
  node [
    id 5
    label "miejsce"
  ]
  node [
    id 6
    label "ballpark"
  ]
  node [
    id 7
    label "obszar"
  ]
  node [
    id 8
    label "sprz&#281;t"
  ]
  node [
    id 9
    label "tabor"
  ]
  node [
    id 10
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 11
    label "teren_zielony"
  ]
  node [
    id 12
    label "regaty"
  ]
  node [
    id 13
    label "statek"
  ]
  node [
    id 14
    label "spalin&#243;wka"
  ]
  node [
    id 15
    label "pok&#322;ad"
  ]
  node [
    id 16
    label "ster"
  ]
  node [
    id 17
    label "kratownica"
  ]
  node [
    id 18
    label "pojazd_niemechaniczny"
  ]
  node [
    id 19
    label "drzewce"
  ]
  node [
    id 20
    label "on"
  ]
  node [
    id 21
    label "Tadeusz"
  ]
  node [
    id 22
    label "Rejtan"
  ]
  node [
    id 23
    label "Skrzywana"
  ]
  node [
    id 24
    label "nowy"
  ]
  node [
    id 25
    label "rokita"
  ]
  node [
    id 26
    label "aleja"
  ]
  node [
    id 27
    label "politechnika"
  ]
  node [
    id 28
    label "rada"
  ]
  node [
    id 29
    label "miejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
]
