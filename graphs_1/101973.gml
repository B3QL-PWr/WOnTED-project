graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.227703984819734
  density 0.004235178678364514
  graphCliqueNumber 4
  node [
    id 0
    label "poza"
    origin "text"
  ]
  node [
    id 1
    label "tym"
    origin "text"
  ]
  node [
    id 2
    label "myli&#263;"
    origin "text"
  ]
  node [
    id 3
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "tco"
    origin "text"
  ]
  node [
    id 5
    label "mak&#243;w"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "niski"
    origin "text"
  ]
  node [
    id 8
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 9
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 10
    label "nisza"
    origin "text"
  ]
  node [
    id 11
    label "wr&#281;cz"
    origin "text"
  ]
  node [
    id 12
    label "przeciwnie"
    origin "text"
  ]
  node [
    id 13
    label "apple"
    origin "text"
  ]
  node [
    id 14
    label "tak"
    origin "text"
  ]
  node [
    id 15
    label "sam"
    origin "text"
  ]
  node [
    id 16
    label "jak"
    origin "text"
  ]
  node [
    id 17
    label "microsoft"
    origin "text"
  ]
  node [
    id 18
    label "bardzo"
    origin "text"
  ]
  node [
    id 19
    label "innowacyjny"
    origin "text"
  ]
  node [
    id 20
    label "biznesowo"
    origin "text"
  ]
  node [
    id 21
    label "marketingowo"
    origin "text"
  ]
  node [
    id 22
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 23
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 24
    label "inaczej"
    origin "text"
  ]
  node [
    id 25
    label "prosty"
    origin "text"
  ]
  node [
    id 26
    label "potrafi&#263;"
    origin "text"
  ]
  node [
    id 27
    label "dobrze"
    origin "text"
  ]
  node [
    id 28
    label "po&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 29
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 30
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 31
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 32
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 33
    label "sprzeda&#263;"
    origin "text"
  ]
  node [
    id 34
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 35
    label "inny"
    origin "text"
  ]
  node [
    id 36
    label "kolej"
    origin "text"
  ]
  node [
    id 37
    label "pompowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "got&#243;wka"
    origin "text"
  ]
  node [
    id 39
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 40
    label "bezrefleksyjne"
    origin "text"
  ]
  node [
    id 41
    label "kopiowa&#263;"
    origin "text"
  ]
  node [
    id 42
    label "nu&#380;"
    origin "text"
  ]
  node [
    id 43
    label "si&#281;"
    origin "text"
  ]
  node [
    id 44
    label "udo"
    origin "text"
  ]
  node [
    id 45
    label "przycisn&#261;&#263;"
    origin "text"
  ]
  node [
    id 46
    label "producent"
    origin "text"
  ]
  node [
    id 47
    label "sprz&#281;t"
    origin "text"
  ]
  node [
    id 48
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 49
    label "maja"
    origin "text"
  ]
  node [
    id 50
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 51
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 52
    label "ile"
    origin "text"
  ]
  node [
    id 53
    label "p&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 54
    label "konsument"
    origin "text"
  ]
  node [
    id 55
    label "polega&#263;"
    origin "text"
  ]
  node [
    id 56
    label "korzy&#347;&#263;"
    origin "text"
  ]
  node [
    id 57
    label "skala"
    origin "text"
  ]
  node [
    id 58
    label "jednostkowy"
    origin "text"
  ]
  node [
    id 59
    label "cena"
    origin "text"
  ]
  node [
    id 60
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 61
    label "jeszcze"
    origin "text"
  ]
  node [
    id 62
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 63
    label "kop"
    origin "text"
  ]
  node [
    id 64
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 65
    label "lepsze"
    origin "text"
  ]
  node [
    id 66
    label "ale"
    origin "text"
  ]
  node [
    id 67
    label "zwykle"
    origin "text"
  ]
  node [
    id 68
    label "trzeba"
    origin "text"
  ]
  node [
    id 69
    label "kilka"
    origin "text"
  ]
  node [
    id 70
    label "lata"
    origin "text"
  ]
  node [
    id 71
    label "dotrze&#263;"
    origin "text"
  ]
  node [
    id 72
    label "proces"
    origin "text"
  ]
  node [
    id 73
    label "naruszenie"
    origin "text"
  ]
  node [
    id 74
    label "zasada"
    origin "text"
  ]
  node [
    id 75
    label "konkurencja"
    origin "text"
  ]
  node [
    id 76
    label "zanim"
    origin "text"
  ]
  node [
    id 77
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 78
    label "co&#347;"
    origin "text"
  ]
  node [
    id 79
    label "nadawa&#263;"
    origin "text"
  ]
  node [
    id 80
    label "word"
    origin "text"
  ]
  node [
    id 81
    label "for"
    origin "text"
  ]
  node [
    id 82
    label "windows"
    origin "text"
  ]
  node [
    id 83
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 84
    label "sans"
    origin "text"
  ]
  node [
    id 85
    label "tablet"
    origin "text"
  ]
  node [
    id 86
    label "internet"
    origin "text"
  ]
  node [
    id 87
    label "explorer"
    origin "text"
  ]
  node [
    id 88
    label "mode"
  ]
  node [
    id 89
    label "gra"
  ]
  node [
    id 90
    label "przesada"
  ]
  node [
    id 91
    label "ustawienie"
  ]
  node [
    id 92
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 93
    label "fool"
  ]
  node [
    id 94
    label "misinform"
  ]
  node [
    id 95
    label "mi&#281;sza&#263;"
  ]
  node [
    id 96
    label "mani&#263;"
  ]
  node [
    id 97
    label "oszukiwa&#263;"
  ]
  node [
    id 98
    label "ba&#322;amuci&#263;"
  ]
  node [
    id 99
    label "continue"
  ]
  node [
    id 100
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 101
    label "consider"
  ]
  node [
    id 102
    label "my&#347;le&#263;"
  ]
  node [
    id 103
    label "pilnowa&#263;"
  ]
  node [
    id 104
    label "robi&#263;"
  ]
  node [
    id 105
    label "uznawa&#263;"
  ]
  node [
    id 106
    label "obserwowa&#263;"
  ]
  node [
    id 107
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 108
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 109
    label "deliver"
  ]
  node [
    id 110
    label "si&#281;ga&#263;"
  ]
  node [
    id 111
    label "trwa&#263;"
  ]
  node [
    id 112
    label "obecno&#347;&#263;"
  ]
  node [
    id 113
    label "stan"
  ]
  node [
    id 114
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 115
    label "stand"
  ]
  node [
    id 116
    label "mie&#263;_miejsce"
  ]
  node [
    id 117
    label "uczestniczy&#263;"
  ]
  node [
    id 118
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 119
    label "equal"
  ]
  node [
    id 120
    label "marny"
  ]
  node [
    id 121
    label "wstydliwy"
  ]
  node [
    id 122
    label "nieznaczny"
  ]
  node [
    id 123
    label "gorszy"
  ]
  node [
    id 124
    label "bliski"
  ]
  node [
    id 125
    label "s&#322;aby"
  ]
  node [
    id 126
    label "obni&#380;enie"
  ]
  node [
    id 127
    label "nisko"
  ]
  node [
    id 128
    label "n&#281;dznie"
  ]
  node [
    id 129
    label "po&#347;ledni"
  ]
  node [
    id 130
    label "uni&#380;ony"
  ]
  node [
    id 131
    label "pospolity"
  ]
  node [
    id 132
    label "obni&#380;anie"
  ]
  node [
    id 133
    label "pomierny"
  ]
  node [
    id 134
    label "ma&#322;y"
  ]
  node [
    id 135
    label "jako&#347;"
  ]
  node [
    id 136
    label "charakterystyczny"
  ]
  node [
    id 137
    label "ciekawy"
  ]
  node [
    id 138
    label "jako_tako"
  ]
  node [
    id 139
    label "dziwny"
  ]
  node [
    id 140
    label "niez&#322;y"
  ]
  node [
    id 141
    label "przyzwoity"
  ]
  node [
    id 142
    label "dziedzina"
  ]
  node [
    id 143
    label "element_konstrukcyjny"
  ]
  node [
    id 144
    label "wn&#281;ka"
  ]
  node [
    id 145
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 146
    label "odmiennie"
  ]
  node [
    id 147
    label "spornie"
  ]
  node [
    id 148
    label "na_abarot"
  ]
  node [
    id 149
    label "przeciwny"
  ]
  node [
    id 150
    label "odwrotny"
  ]
  node [
    id 151
    label "sklep"
  ]
  node [
    id 152
    label "byd&#322;o"
  ]
  node [
    id 153
    label "zobo"
  ]
  node [
    id 154
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 155
    label "yakalo"
  ]
  node [
    id 156
    label "dzo"
  ]
  node [
    id 157
    label "w_chuj"
  ]
  node [
    id 158
    label "innowacyjnie"
  ]
  node [
    id 159
    label "niekonwencjonalny"
  ]
  node [
    id 160
    label "nowatorski"
  ]
  node [
    id 161
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 162
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 163
    label "biznesowy"
  ]
  node [
    id 164
    label "marketingowy"
  ]
  node [
    id 165
    label "handlowo"
  ]
  node [
    id 166
    label "niestandardowo"
  ]
  node [
    id 167
    label "&#322;atwy"
  ]
  node [
    id 168
    label "prostowanie_si&#281;"
  ]
  node [
    id 169
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 170
    label "rozprostowanie"
  ]
  node [
    id 171
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 172
    label "prostoduszny"
  ]
  node [
    id 173
    label "naturalny"
  ]
  node [
    id 174
    label "naiwny"
  ]
  node [
    id 175
    label "cios"
  ]
  node [
    id 176
    label "prostowanie"
  ]
  node [
    id 177
    label "niepozorny"
  ]
  node [
    id 178
    label "zwyk&#322;y"
  ]
  node [
    id 179
    label "prosto"
  ]
  node [
    id 180
    label "po_prostu"
  ]
  node [
    id 181
    label "skromny"
  ]
  node [
    id 182
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 183
    label "umie&#263;"
  ]
  node [
    id 184
    label "cope"
  ]
  node [
    id 185
    label "potrafia&#263;"
  ]
  node [
    id 186
    label "posuwa&#263;_si&#281;"
  ]
  node [
    id 187
    label "posun&#261;&#263;_si&#281;"
  ]
  node [
    id 188
    label "can"
  ]
  node [
    id 189
    label "moralnie"
  ]
  node [
    id 190
    label "wiele"
  ]
  node [
    id 191
    label "lepiej"
  ]
  node [
    id 192
    label "korzystnie"
  ]
  node [
    id 193
    label "pomy&#347;lnie"
  ]
  node [
    id 194
    label "pozytywnie"
  ]
  node [
    id 195
    label "dobry"
  ]
  node [
    id 196
    label "dobroczynnie"
  ]
  node [
    id 197
    label "odpowiednio"
  ]
  node [
    id 198
    label "skutecznie"
  ]
  node [
    id 199
    label "relate"
  ]
  node [
    id 200
    label "spowodowa&#263;"
  ]
  node [
    id 201
    label "stworzy&#263;"
  ]
  node [
    id 202
    label "po&#322;&#261;czenie"
  ]
  node [
    id 203
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 204
    label "connect"
  ]
  node [
    id 205
    label "zjednoczy&#263;"
  ]
  node [
    id 206
    label "incorporate"
  ]
  node [
    id 207
    label "zrobi&#263;"
  ]
  node [
    id 208
    label "du&#380;y"
  ]
  node [
    id 209
    label "cz&#281;sto"
  ]
  node [
    id 210
    label "mocno"
  ]
  node [
    id 211
    label "wiela"
  ]
  node [
    id 212
    label "poziom"
  ]
  node [
    id 213
    label "faza"
  ]
  node [
    id 214
    label "depression"
  ]
  node [
    id 215
    label "zjawisko"
  ]
  node [
    id 216
    label "nizina"
  ]
  node [
    id 217
    label "zhandlowa&#263;"
  ]
  node [
    id 218
    label "odda&#263;"
  ]
  node [
    id 219
    label "zach&#281;ci&#263;"
  ]
  node [
    id 220
    label "give_birth"
  ]
  node [
    id 221
    label "zdradzi&#263;"
  ]
  node [
    id 222
    label "op&#281;dzi&#263;"
  ]
  node [
    id 223
    label "sell"
  ]
  node [
    id 224
    label "system"
  ]
  node [
    id 225
    label "wytw&#243;r"
  ]
  node [
    id 226
    label "idea"
  ]
  node [
    id 227
    label "ukra&#347;&#263;"
  ]
  node [
    id 228
    label "ukradzenie"
  ]
  node [
    id 229
    label "pocz&#261;tki"
  ]
  node [
    id 230
    label "kolejny"
  ]
  node [
    id 231
    label "r&#243;&#380;ny"
  ]
  node [
    id 232
    label "inszy"
  ]
  node [
    id 233
    label "osobno"
  ]
  node [
    id 234
    label "pojazd_kolejowy"
  ]
  node [
    id 235
    label "czas"
  ]
  node [
    id 236
    label "tor"
  ]
  node [
    id 237
    label "tender"
  ]
  node [
    id 238
    label "droga"
  ]
  node [
    id 239
    label "blokada"
  ]
  node [
    id 240
    label "wagon"
  ]
  node [
    id 241
    label "run"
  ]
  node [
    id 242
    label "cedu&#322;a"
  ]
  node [
    id 243
    label "kolejno&#347;&#263;"
  ]
  node [
    id 244
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 245
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 246
    label "trakcja"
  ]
  node [
    id 247
    label "linia"
  ]
  node [
    id 248
    label "cug"
  ]
  node [
    id 249
    label "poci&#261;g"
  ]
  node [
    id 250
    label "pocz&#261;tek"
  ]
  node [
    id 251
    label "nast&#281;pstwo"
  ]
  node [
    id 252
    label "lokomotywa"
  ]
  node [
    id 253
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 254
    label "throng"
  ]
  node [
    id 255
    label "rozdyma&#263;"
  ]
  node [
    id 256
    label "kierowa&#263;"
  ]
  node [
    id 257
    label "wk&#322;ada&#263;"
  ]
  node [
    id 258
    label "przemieszcza&#263;"
  ]
  node [
    id 259
    label "trenowa&#263;"
  ]
  node [
    id 260
    label "si&#322;ownia"
  ]
  node [
    id 261
    label "wype&#322;nia&#263;"
  ]
  node [
    id 262
    label "pump"
  ]
  node [
    id 263
    label "bra&#263;"
  ]
  node [
    id 264
    label "wt&#322;acza&#263;"
  ]
  node [
    id 265
    label "inflate"
  ]
  node [
    id 266
    label "podnosi&#263;"
  ]
  node [
    id 267
    label "pieni&#261;dze"
  ]
  node [
    id 268
    label "brz&#281;cz&#261;ca_moneta"
  ]
  node [
    id 269
    label "money"
  ]
  node [
    id 270
    label "pirat"
  ]
  node [
    id 271
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 272
    label "mock"
  ]
  node [
    id 273
    label "transcribe"
  ]
  node [
    id 274
    label "wytwarza&#263;"
  ]
  node [
    id 275
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 276
    label "kr&#281;tarz"
  ]
  node [
    id 277
    label "mi&#281;sie&#324;_czworog&#322;owy"
  ]
  node [
    id 278
    label "mi&#281;sie&#324;_dwug&#322;owy_uda"
  ]
  node [
    id 279
    label "struktura_anatomiczna"
  ]
  node [
    id 280
    label "t&#281;tnica_udowa"
  ]
  node [
    id 281
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 282
    label "noga"
  ]
  node [
    id 283
    label "zmusi&#263;"
  ]
  node [
    id 284
    label "push"
  ]
  node [
    id 285
    label "nadusi&#263;"
  ]
  node [
    id 286
    label "tug"
  ]
  node [
    id 287
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 288
    label "press"
  ]
  node [
    id 289
    label "przy&#322;o&#380;y&#263;"
  ]
  node [
    id 290
    label "rynek"
  ]
  node [
    id 291
    label "podmiot"
  ]
  node [
    id 292
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 293
    label "manufacturer"
  ]
  node [
    id 294
    label "bran&#380;owiec"
  ]
  node [
    id 295
    label "artel"
  ]
  node [
    id 296
    label "filmowiec"
  ]
  node [
    id 297
    label "muzyk"
  ]
  node [
    id 298
    label "Canon"
  ]
  node [
    id 299
    label "wykonawca"
  ]
  node [
    id 300
    label "autotrof"
  ]
  node [
    id 301
    label "Wedel"
  ]
  node [
    id 302
    label "przedmiot"
  ]
  node [
    id 303
    label "kolekcja"
  ]
  node [
    id 304
    label "sprz&#281;cior"
  ]
  node [
    id 305
    label "furniture"
  ]
  node [
    id 306
    label "equipment"
  ]
  node [
    id 307
    label "penis"
  ]
  node [
    id 308
    label "zbi&#243;r"
  ]
  node [
    id 309
    label "sprz&#281;cik"
  ]
  node [
    id 310
    label "wedyzm"
  ]
  node [
    id 311
    label "energia"
  ]
  node [
    id 312
    label "buddyzm"
  ]
  node [
    id 313
    label "czynno&#347;&#263;"
  ]
  node [
    id 314
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 315
    label "decyzja"
  ]
  node [
    id 316
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 317
    label "pick"
  ]
  node [
    id 318
    label "proceed"
  ]
  node [
    id 319
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 320
    label "bangla&#263;"
  ]
  node [
    id 321
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 322
    label "tryb"
  ]
  node [
    id 323
    label "p&#322;ywa&#263;"
  ]
  node [
    id 324
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 325
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 326
    label "przebiega&#263;"
  ]
  node [
    id 327
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 328
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 329
    label "para"
  ]
  node [
    id 330
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 331
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 332
    label "krok"
  ]
  node [
    id 333
    label "str&#243;j"
  ]
  node [
    id 334
    label "bywa&#263;"
  ]
  node [
    id 335
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 336
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 337
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 338
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 339
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 340
    label "dziama&#263;"
  ]
  node [
    id 341
    label "stara&#263;_si&#281;"
  ]
  node [
    id 342
    label "carry"
  ]
  node [
    id 343
    label "buli&#263;"
  ]
  node [
    id 344
    label "osi&#261;ga&#263;"
  ]
  node [
    id 345
    label "give"
  ]
  node [
    id 346
    label "wydawa&#263;"
  ]
  node [
    id 347
    label "pay"
  ]
  node [
    id 348
    label "odbiorca"
  ]
  node [
    id 349
    label "go&#347;&#263;"
  ]
  node [
    id 350
    label "zjadacz"
  ]
  node [
    id 351
    label "restauracja"
  ]
  node [
    id 352
    label "heterotrof"
  ]
  node [
    id 353
    label "klient"
  ]
  node [
    id 354
    label "u&#380;ytkownik"
  ]
  node [
    id 355
    label "ufa&#263;"
  ]
  node [
    id 356
    label "consist"
  ]
  node [
    id 357
    label "trust"
  ]
  node [
    id 358
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 359
    label "dobro"
  ]
  node [
    id 360
    label "zaleta"
  ]
  node [
    id 361
    label "przedzia&#322;"
  ]
  node [
    id 362
    label "przymiar"
  ]
  node [
    id 363
    label "podzia&#322;ka"
  ]
  node [
    id 364
    label "proporcja"
  ]
  node [
    id 365
    label "tetrachord"
  ]
  node [
    id 366
    label "scale"
  ]
  node [
    id 367
    label "dominanta"
  ]
  node [
    id 368
    label "rejestr"
  ]
  node [
    id 369
    label "sfera"
  ]
  node [
    id 370
    label "struktura"
  ]
  node [
    id 371
    label "kreska"
  ]
  node [
    id 372
    label "zero"
  ]
  node [
    id 373
    label "interwa&#322;"
  ]
  node [
    id 374
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 375
    label "subdominanta"
  ]
  node [
    id 376
    label "masztab"
  ]
  node [
    id 377
    label "part"
  ]
  node [
    id 378
    label "podzakres"
  ]
  node [
    id 379
    label "wielko&#347;&#263;"
  ]
  node [
    id 380
    label "jednostka"
  ]
  node [
    id 381
    label "indywidualny"
  ]
  node [
    id 382
    label "jednostkowo"
  ]
  node [
    id 383
    label "jednokrotny"
  ]
  node [
    id 384
    label "unikatowy"
  ]
  node [
    id 385
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 386
    label "warto&#347;&#263;"
  ]
  node [
    id 387
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 388
    label "dyskryminacja_cenowa"
  ]
  node [
    id 389
    label "inflacja"
  ]
  node [
    id 390
    label "kupowanie"
  ]
  node [
    id 391
    label "kosztowa&#263;"
  ]
  node [
    id 392
    label "kosztowanie"
  ]
  node [
    id 393
    label "worth"
  ]
  node [
    id 394
    label "wyceni&#263;"
  ]
  node [
    id 395
    label "wycenienie"
  ]
  node [
    id 396
    label "uprawi&#263;"
  ]
  node [
    id 397
    label "gotowy"
  ]
  node [
    id 398
    label "might"
  ]
  node [
    id 399
    label "ci&#261;gle"
  ]
  node [
    id 400
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 401
    label "reakcja"
  ]
  node [
    id 402
    label "kick"
  ]
  node [
    id 403
    label "recoil"
  ]
  node [
    id 404
    label "odjazd"
  ]
  node [
    id 405
    label "kopni&#281;cie"
  ]
  node [
    id 406
    label "dawny"
  ]
  node [
    id 407
    label "rozw&#243;d"
  ]
  node [
    id 408
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 409
    label "eksprezydent"
  ]
  node [
    id 410
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 411
    label "partner"
  ]
  node [
    id 412
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 413
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 414
    label "wcze&#347;niejszy"
  ]
  node [
    id 415
    label "piwo"
  ]
  node [
    id 416
    label "trza"
  ]
  node [
    id 417
    label "necessity"
  ]
  node [
    id 418
    label "&#347;ledziowate"
  ]
  node [
    id 419
    label "ryba"
  ]
  node [
    id 420
    label "summer"
  ]
  node [
    id 421
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 422
    label "get"
  ]
  node [
    id 423
    label "utrze&#263;"
  ]
  node [
    id 424
    label "catch"
  ]
  node [
    id 425
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 426
    label "become"
  ]
  node [
    id 427
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 428
    label "znale&#378;&#263;"
  ]
  node [
    id 429
    label "dorobi&#263;"
  ]
  node [
    id 430
    label "advance"
  ]
  node [
    id 431
    label "dopasowa&#263;"
  ]
  node [
    id 432
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 433
    label "silnik"
  ]
  node [
    id 434
    label "legislacyjnie"
  ]
  node [
    id 435
    label "kognicja"
  ]
  node [
    id 436
    label "przebieg"
  ]
  node [
    id 437
    label "wydarzenie"
  ]
  node [
    id 438
    label "przes&#322;anka"
  ]
  node [
    id 439
    label "rozprawa"
  ]
  node [
    id 440
    label "zrobienie"
  ]
  node [
    id 441
    label "transgresja"
  ]
  node [
    id 442
    label "zacz&#281;cie"
  ]
  node [
    id 443
    label "zepsucie"
  ]
  node [
    id 444
    label "discourtesy"
  ]
  node [
    id 445
    label "odj&#281;cie"
  ]
  node [
    id 446
    label "obserwacja"
  ]
  node [
    id 447
    label "moralno&#347;&#263;"
  ]
  node [
    id 448
    label "podstawa"
  ]
  node [
    id 449
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 450
    label "umowa"
  ]
  node [
    id 451
    label "dominion"
  ]
  node [
    id 452
    label "qualification"
  ]
  node [
    id 453
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 454
    label "opis"
  ]
  node [
    id 455
    label "regu&#322;a_Allena"
  ]
  node [
    id 456
    label "normalizacja"
  ]
  node [
    id 457
    label "regu&#322;a_Glogera"
  ]
  node [
    id 458
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 459
    label "standard"
  ]
  node [
    id 460
    label "base"
  ]
  node [
    id 461
    label "substancja"
  ]
  node [
    id 462
    label "spos&#243;b"
  ]
  node [
    id 463
    label "prawid&#322;o"
  ]
  node [
    id 464
    label "prawo_Mendla"
  ]
  node [
    id 465
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 466
    label "criterion"
  ]
  node [
    id 467
    label "twierdzenie"
  ]
  node [
    id 468
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 469
    label "prawo"
  ]
  node [
    id 470
    label "occupation"
  ]
  node [
    id 471
    label "zasada_d'Alemberta"
  ]
  node [
    id 472
    label "dob&#243;r_naturalny"
  ]
  node [
    id 473
    label "firma"
  ]
  node [
    id 474
    label "dyscyplina_sportowa"
  ]
  node [
    id 475
    label "interakcja"
  ]
  node [
    id 476
    label "rywalizacja"
  ]
  node [
    id 477
    label "uczestnik"
  ]
  node [
    id 478
    label "contest"
  ]
  node [
    id 479
    label "cause"
  ]
  node [
    id 480
    label "introduce"
  ]
  node [
    id 481
    label "begin"
  ]
  node [
    id 482
    label "odj&#261;&#263;"
  ]
  node [
    id 483
    label "post&#261;pi&#263;"
  ]
  node [
    id 484
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 485
    label "do"
  ]
  node [
    id 486
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 487
    label "thing"
  ]
  node [
    id 488
    label "cosik"
  ]
  node [
    id 489
    label "gada&#263;"
  ]
  node [
    id 490
    label "za&#322;atwia&#263;"
  ]
  node [
    id 491
    label "dawa&#263;"
  ]
  node [
    id 492
    label "assign"
  ]
  node [
    id 493
    label "rekomendowa&#263;"
  ]
  node [
    id 494
    label "obgadywa&#263;"
  ]
  node [
    id 495
    label "donosi&#263;"
  ]
  node [
    id 496
    label "sprawia&#263;"
  ]
  node [
    id 497
    label "przesy&#322;a&#263;"
  ]
  node [
    id 498
    label "podanie"
  ]
  node [
    id 499
    label "czyj&#347;"
  ]
  node [
    id 500
    label "m&#261;&#380;"
  ]
  node [
    id 501
    label "ekran_dotykowy"
  ]
  node [
    id 502
    label "klawiatura_ekranowa"
  ]
  node [
    id 503
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 504
    label "us&#322;uga_internetowa"
  ]
  node [
    id 505
    label "biznes_elektroniczny"
  ]
  node [
    id 506
    label "punkt_dost&#281;pu"
  ]
  node [
    id 507
    label "hipertekst"
  ]
  node [
    id 508
    label "gra_sieciowa"
  ]
  node [
    id 509
    label "mem"
  ]
  node [
    id 510
    label "e-hazard"
  ]
  node [
    id 511
    label "sie&#263;_komputerowa"
  ]
  node [
    id 512
    label "media"
  ]
  node [
    id 513
    label "podcast"
  ]
  node [
    id 514
    label "netbook"
  ]
  node [
    id 515
    label "provider"
  ]
  node [
    id 516
    label "cyberprzestrze&#324;"
  ]
  node [
    id 517
    label "grooming"
  ]
  node [
    id 518
    label "strona"
  ]
  node [
    id 519
    label "Word"
  ]
  node [
    id 520
    label "2"
  ]
  node [
    id 521
    label "0"
  ]
  node [
    id 522
    label "Windows"
  ]
  node [
    id 523
    label "3"
  ]
  node [
    id 524
    label "95"
  ]
  node [
    id 525
    label "XP"
  ]
  node [
    id 526
    label "Explorer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 61
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 48
  ]
  edge [
    source 14
    target 49
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 44
  ]
  edge [
    source 16
    target 45
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 17
    target 32
  ]
  edge [
    source 17
    target 33
  ]
  edge [
    source 17
    target 35
  ]
  edge [
    source 17
    target 36
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 31
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 71
  ]
  edge [
    source 22
    target 75
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 71
  ]
  edge [
    source 24
    target 75
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 167
  ]
  edge [
    source 25
    target 168
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 25
    target 172
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 25
    target 181
  ]
  edge [
    source 25
    target 182
  ]
  edge [
    source 25
    target 50
  ]
  edge [
    source 25
    target 55
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 183
  ]
  edge [
    source 26
    target 184
  ]
  edge [
    source 26
    target 185
  ]
  edge [
    source 26
    target 186
  ]
  edge [
    source 26
    target 187
  ]
  edge [
    source 26
    target 188
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 189
  ]
  edge [
    source 27
    target 190
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 27
    target 192
  ]
  edge [
    source 27
    target 193
  ]
  edge [
    source 27
    target 194
  ]
  edge [
    source 27
    target 195
  ]
  edge [
    source 27
    target 196
  ]
  edge [
    source 27
    target 197
  ]
  edge [
    source 27
    target 161
  ]
  edge [
    source 27
    target 198
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 200
  ]
  edge [
    source 28
    target 201
  ]
  edge [
    source 28
    target 202
  ]
  edge [
    source 28
    target 203
  ]
  edge [
    source 28
    target 204
  ]
  edge [
    source 28
    target 205
  ]
  edge [
    source 28
    target 206
  ]
  edge [
    source 28
    target 207
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 208
  ]
  edge [
    source 31
    target 209
  ]
  edge [
    source 31
    target 210
  ]
  edge [
    source 31
    target 211
  ]
  edge [
    source 32
    target 212
  ]
  edge [
    source 32
    target 213
  ]
  edge [
    source 32
    target 214
  ]
  edge [
    source 32
    target 215
  ]
  edge [
    source 32
    target 216
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 217
  ]
  edge [
    source 33
    target 218
  ]
  edge [
    source 33
    target 219
  ]
  edge [
    source 33
    target 220
  ]
  edge [
    source 33
    target 221
  ]
  edge [
    source 33
    target 222
  ]
  edge [
    source 33
    target 223
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 34
    target 42
  ]
  edge [
    source 34
    target 224
  ]
  edge [
    source 34
    target 225
  ]
  edge [
    source 34
    target 226
  ]
  edge [
    source 34
    target 227
  ]
  edge [
    source 34
    target 228
  ]
  edge [
    source 34
    target 229
  ]
  edge [
    source 34
    target 45
  ]
  edge [
    source 35
    target 230
  ]
  edge [
    source 35
    target 231
  ]
  edge [
    source 35
    target 232
  ]
  edge [
    source 35
    target 233
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 234
  ]
  edge [
    source 36
    target 235
  ]
  edge [
    source 36
    target 236
  ]
  edge [
    source 36
    target 237
  ]
  edge [
    source 36
    target 238
  ]
  edge [
    source 36
    target 239
  ]
  edge [
    source 36
    target 240
  ]
  edge [
    source 36
    target 241
  ]
  edge [
    source 36
    target 242
  ]
  edge [
    source 36
    target 243
  ]
  edge [
    source 36
    target 244
  ]
  edge [
    source 36
    target 245
  ]
  edge [
    source 36
    target 246
  ]
  edge [
    source 36
    target 247
  ]
  edge [
    source 36
    target 248
  ]
  edge [
    source 36
    target 249
  ]
  edge [
    source 36
    target 250
  ]
  edge [
    source 36
    target 251
  ]
  edge [
    source 36
    target 252
  ]
  edge [
    source 36
    target 253
  ]
  edge [
    source 36
    target 72
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 254
  ]
  edge [
    source 37
    target 255
  ]
  edge [
    source 37
    target 256
  ]
  edge [
    source 37
    target 257
  ]
  edge [
    source 37
    target 258
  ]
  edge [
    source 37
    target 259
  ]
  edge [
    source 37
    target 260
  ]
  edge [
    source 37
    target 261
  ]
  edge [
    source 37
    target 262
  ]
  edge [
    source 37
    target 263
  ]
  edge [
    source 37
    target 264
  ]
  edge [
    source 37
    target 265
  ]
  edge [
    source 37
    target 266
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 267
  ]
  edge [
    source 38
    target 268
  ]
  edge [
    source 38
    target 269
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 270
  ]
  edge [
    source 41
    target 271
  ]
  edge [
    source 41
    target 272
  ]
  edge [
    source 41
    target 104
  ]
  edge [
    source 41
    target 273
  ]
  edge [
    source 41
    target 274
  ]
  edge [
    source 41
    target 275
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 77
  ]
  edge [
    source 43
    target 78
  ]
  edge [
    source 44
    target 276
  ]
  edge [
    source 44
    target 277
  ]
  edge [
    source 44
    target 278
  ]
  edge [
    source 44
    target 279
  ]
  edge [
    source 44
    target 280
  ]
  edge [
    source 44
    target 281
  ]
  edge [
    source 44
    target 282
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 283
  ]
  edge [
    source 45
    target 200
  ]
  edge [
    source 45
    target 284
  ]
  edge [
    source 45
    target 285
  ]
  edge [
    source 45
    target 286
  ]
  edge [
    source 45
    target 287
  ]
  edge [
    source 45
    target 288
  ]
  edge [
    source 45
    target 289
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 290
  ]
  edge [
    source 46
    target 291
  ]
  edge [
    source 46
    target 292
  ]
  edge [
    source 46
    target 293
  ]
  edge [
    source 46
    target 294
  ]
  edge [
    source 46
    target 295
  ]
  edge [
    source 46
    target 296
  ]
  edge [
    source 46
    target 297
  ]
  edge [
    source 46
    target 298
  ]
  edge [
    source 46
    target 299
  ]
  edge [
    source 46
    target 300
  ]
  edge [
    source 46
    target 301
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 302
  ]
  edge [
    source 47
    target 303
  ]
  edge [
    source 47
    target 304
  ]
  edge [
    source 47
    target 305
  ]
  edge [
    source 47
    target 306
  ]
  edge [
    source 47
    target 307
  ]
  edge [
    source 47
    target 308
  ]
  edge [
    source 47
    target 309
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 310
  ]
  edge [
    source 49
    target 311
  ]
  edge [
    source 49
    target 312
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 313
  ]
  edge [
    source 50
    target 314
  ]
  edge [
    source 50
    target 315
  ]
  edge [
    source 50
    target 316
  ]
  edge [
    source 50
    target 317
  ]
  edge [
    source 50
    target 55
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 318
  ]
  edge [
    source 51
    target 319
  ]
  edge [
    source 51
    target 320
  ]
  edge [
    source 51
    target 92
  ]
  edge [
    source 51
    target 321
  ]
  edge [
    source 51
    target 241
  ]
  edge [
    source 51
    target 322
  ]
  edge [
    source 51
    target 323
  ]
  edge [
    source 51
    target 99
  ]
  edge [
    source 51
    target 324
  ]
  edge [
    source 51
    target 325
  ]
  edge [
    source 51
    target 326
  ]
  edge [
    source 51
    target 116
  ]
  edge [
    source 51
    target 257
  ]
  edge [
    source 51
    target 327
  ]
  edge [
    source 51
    target 328
  ]
  edge [
    source 51
    target 329
  ]
  edge [
    source 51
    target 330
  ]
  edge [
    source 51
    target 331
  ]
  edge [
    source 51
    target 332
  ]
  edge [
    source 51
    target 333
  ]
  edge [
    source 51
    target 334
  ]
  edge [
    source 51
    target 335
  ]
  edge [
    source 51
    target 336
  ]
  edge [
    source 51
    target 337
  ]
  edge [
    source 51
    target 338
  ]
  edge [
    source 51
    target 339
  ]
  edge [
    source 51
    target 340
  ]
  edge [
    source 51
    target 341
  ]
  edge [
    source 51
    target 342
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 343
  ]
  edge [
    source 53
    target 344
  ]
  edge [
    source 53
    target 345
  ]
  edge [
    source 53
    target 346
  ]
  edge [
    source 53
    target 347
  ]
  edge [
    source 53
    target 65
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 290
  ]
  edge [
    source 54
    target 348
  ]
  edge [
    source 54
    target 349
  ]
  edge [
    source 54
    target 350
  ]
  edge [
    source 54
    target 292
  ]
  edge [
    source 54
    target 351
  ]
  edge [
    source 54
    target 352
  ]
  edge [
    source 54
    target 353
  ]
  edge [
    source 54
    target 354
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 355
  ]
  edge [
    source 55
    target 356
  ]
  edge [
    source 55
    target 357
  ]
  edge [
    source 55
    target 358
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 359
  ]
  edge [
    source 56
    target 360
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 361
  ]
  edge [
    source 57
    target 362
  ]
  edge [
    source 57
    target 363
  ]
  edge [
    source 57
    target 364
  ]
  edge [
    source 57
    target 365
  ]
  edge [
    source 57
    target 366
  ]
  edge [
    source 57
    target 367
  ]
  edge [
    source 57
    target 368
  ]
  edge [
    source 57
    target 369
  ]
  edge [
    source 57
    target 370
  ]
  edge [
    source 57
    target 371
  ]
  edge [
    source 57
    target 372
  ]
  edge [
    source 57
    target 373
  ]
  edge [
    source 57
    target 374
  ]
  edge [
    source 57
    target 375
  ]
  edge [
    source 57
    target 142
  ]
  edge [
    source 57
    target 376
  ]
  edge [
    source 57
    target 377
  ]
  edge [
    source 57
    target 378
  ]
  edge [
    source 57
    target 308
  ]
  edge [
    source 57
    target 379
  ]
  edge [
    source 57
    target 380
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 381
  ]
  edge [
    source 58
    target 382
  ]
  edge [
    source 58
    target 383
  ]
  edge [
    source 58
    target 384
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 385
  ]
  edge [
    source 59
    target 386
  ]
  edge [
    source 59
    target 387
  ]
  edge [
    source 59
    target 388
  ]
  edge [
    source 59
    target 389
  ]
  edge [
    source 59
    target 390
  ]
  edge [
    source 59
    target 391
  ]
  edge [
    source 59
    target 392
  ]
  edge [
    source 59
    target 393
  ]
  edge [
    source 59
    target 394
  ]
  edge [
    source 59
    target 395
  ]
  edge [
    source 60
    target 396
  ]
  edge [
    source 60
    target 397
  ]
  edge [
    source 60
    target 398
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 399
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 400
  ]
  edge [
    source 63
    target 401
  ]
  edge [
    source 63
    target 402
  ]
  edge [
    source 63
    target 403
  ]
  edge [
    source 63
    target 175
  ]
  edge [
    source 63
    target 404
  ]
  edge [
    source 63
    target 405
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 406
  ]
  edge [
    source 64
    target 407
  ]
  edge [
    source 64
    target 408
  ]
  edge [
    source 64
    target 409
  ]
  edge [
    source 64
    target 410
  ]
  edge [
    source 64
    target 411
  ]
  edge [
    source 64
    target 412
  ]
  edge [
    source 64
    target 413
  ]
  edge [
    source 64
    target 414
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 415
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 178
  ]
  edge [
    source 67
    target 209
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 416
  ]
  edge [
    source 68
    target 417
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 69
    target 72
  ]
  edge [
    source 69
    target 418
  ]
  edge [
    source 69
    target 419
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 420
  ]
  edge [
    source 70
    target 235
  ]
  edge [
    source 71
    target 421
  ]
  edge [
    source 71
    target 422
  ]
  edge [
    source 71
    target 423
  ]
  edge [
    source 71
    target 200
  ]
  edge [
    source 71
    target 424
  ]
  edge [
    source 71
    target 425
  ]
  edge [
    source 71
    target 426
  ]
  edge [
    source 71
    target 427
  ]
  edge [
    source 71
    target 428
  ]
  edge [
    source 71
    target 429
  ]
  edge [
    source 71
    target 430
  ]
  edge [
    source 71
    target 431
  ]
  edge [
    source 71
    target 432
  ]
  edge [
    source 71
    target 433
  ]
  edge [
    source 71
    target 75
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 434
  ]
  edge [
    source 72
    target 435
  ]
  edge [
    source 72
    target 436
  ]
  edge [
    source 72
    target 244
  ]
  edge [
    source 72
    target 437
  ]
  edge [
    source 72
    target 438
  ]
  edge [
    source 72
    target 439
  ]
  edge [
    source 72
    target 215
  ]
  edge [
    source 72
    target 251
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 440
  ]
  edge [
    source 73
    target 441
  ]
  edge [
    source 73
    target 442
  ]
  edge [
    source 73
    target 443
  ]
  edge [
    source 73
    target 444
  ]
  edge [
    source 73
    target 445
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 446
  ]
  edge [
    source 74
    target 447
  ]
  edge [
    source 74
    target 448
  ]
  edge [
    source 74
    target 449
  ]
  edge [
    source 74
    target 450
  ]
  edge [
    source 74
    target 451
  ]
  edge [
    source 74
    target 452
  ]
  edge [
    source 74
    target 453
  ]
  edge [
    source 74
    target 454
  ]
  edge [
    source 74
    target 455
  ]
  edge [
    source 74
    target 456
  ]
  edge [
    source 74
    target 457
  ]
  edge [
    source 74
    target 458
  ]
  edge [
    source 74
    target 459
  ]
  edge [
    source 74
    target 460
  ]
  edge [
    source 74
    target 461
  ]
  edge [
    source 74
    target 462
  ]
  edge [
    source 74
    target 463
  ]
  edge [
    source 74
    target 464
  ]
  edge [
    source 74
    target 465
  ]
  edge [
    source 74
    target 466
  ]
  edge [
    source 74
    target 467
  ]
  edge [
    source 74
    target 468
  ]
  edge [
    source 74
    target 469
  ]
  edge [
    source 74
    target 470
  ]
  edge [
    source 74
    target 471
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 472
  ]
  edge [
    source 75
    target 473
  ]
  edge [
    source 75
    target 474
  ]
  edge [
    source 75
    target 475
  ]
  edge [
    source 75
    target 437
  ]
  edge [
    source 75
    target 476
  ]
  edge [
    source 75
    target 477
  ]
  edge [
    source 75
    target 478
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 77
    target 479
  ]
  edge [
    source 77
    target 480
  ]
  edge [
    source 77
    target 481
  ]
  edge [
    source 77
    target 482
  ]
  edge [
    source 77
    target 483
  ]
  edge [
    source 77
    target 484
  ]
  edge [
    source 77
    target 485
  ]
  edge [
    source 77
    target 486
  ]
  edge [
    source 77
    target 207
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 487
  ]
  edge [
    source 78
    target 488
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 489
  ]
  edge [
    source 79
    target 490
  ]
  edge [
    source 79
    target 491
  ]
  edge [
    source 79
    target 492
  ]
  edge [
    source 79
    target 493
  ]
  edge [
    source 79
    target 494
  ]
  edge [
    source 79
    target 495
  ]
  edge [
    source 79
    target 496
  ]
  edge [
    source 79
    target 345
  ]
  edge [
    source 79
    target 497
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 498
  ]
  edge [
    source 82
    target 82
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 84
  ]
  edge [
    source 83
    target 499
  ]
  edge [
    source 83
    target 500
  ]
  edge [
    source 83
    target 522
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 501
  ]
  edge [
    source 85
    target 502
  ]
  edge [
    source 85
    target 503
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 504
  ]
  edge [
    source 86
    target 505
  ]
  edge [
    source 86
    target 506
  ]
  edge [
    source 86
    target 507
  ]
  edge [
    source 86
    target 508
  ]
  edge [
    source 86
    target 509
  ]
  edge [
    source 86
    target 510
  ]
  edge [
    source 86
    target 511
  ]
  edge [
    source 86
    target 512
  ]
  edge [
    source 86
    target 513
  ]
  edge [
    source 86
    target 514
  ]
  edge [
    source 86
    target 515
  ]
  edge [
    source 86
    target 516
  ]
  edge [
    source 86
    target 517
  ]
  edge [
    source 86
    target 518
  ]
  edge [
    source 86
    target 526
  ]
  edge [
    source 86
    target 523
  ]
  edge [
    source 519
    target 520
  ]
  edge [
    source 519
    target 521
  ]
  edge [
    source 520
    target 521
  ]
  edge [
    source 521
    target 522
  ]
  edge [
    source 521
    target 523
  ]
  edge [
    source 522
    target 523
  ]
  edge [
    source 522
    target 524
  ]
  edge [
    source 522
    target 525
  ]
  edge [
    source 523
    target 526
  ]
]
