graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.631578947368421
  density 0.044096728307254626
  graphCliqueNumber 4
  node [
    id 0
    label "michai&#322;"
    origin "text"
  ]
  node [
    id 1
    label "frunze"
    origin "text"
  ]
  node [
    id 2
    label "Michai&#322;"
  ]
  node [
    id 3
    label "Frunze"
  ]
  node [
    id 4
    label "Wasiljewicz"
  ]
  node [
    id 5
    label "Mihail"
  ]
  node [
    id 6
    label "Frunz&#259;"
  ]
  node [
    id 7
    label "&#1052;&#1080;&#1093;&#1072;&#1080;&#1083;"
  ]
  node [
    id 8
    label "&#1042;&#1072;&#1089;&#1080;&#1083;&#1100;&#1077;&#1074;&#1080;&#1095;"
  ]
  node [
    id 9
    label "&#1060;&#1088;&#1091;&#1085;&#1079;&#1077;"
  ]
  node [
    id 10
    label "rewolucja"
  ]
  node [
    id 11
    label "1905"
  ]
  node [
    id 12
    label "rok"
  ]
  node [
    id 13
    label "pa&#378;dziernikowy"
  ]
  node [
    id 14
    label "wojna"
  ]
  node [
    id 15
    label "domowy"
  ]
  node [
    id 16
    label "po&#322;udniowy"
  ]
  node [
    id 17
    label "grupa"
  ]
  node [
    id 18
    label "armia"
  ]
  node [
    id 19
    label "czerwony"
  ]
  node [
    id 20
    label "front"
  ]
  node [
    id 21
    label "wschodni"
  ]
  node [
    id 22
    label "rada"
  ]
  node [
    id 23
    label "wojskowo"
  ]
  node [
    id 24
    label "rewolucyjny"
  ]
  node [
    id 25
    label "&#1056;&#1077;&#1074;&#1074;&#1086;&#1077;&#1085;&#1089;&#1086;&#1074;&#1077;&#1090;"
  ]
  node [
    id 26
    label "&#1057;&#1057;&#1057;&#1056;"
  ]
  node [
    id 27
    label "Borys"
  ]
  node [
    id 28
    label "Pilniak"
  ]
  node [
    id 29
    label "opowie&#347;&#263;"
  ]
  node [
    id 30
    label "zgasi&#263;"
  ]
  node [
    id 31
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 32
    label "akademia"
  ]
  node [
    id 33
    label "wojskowy"
  ]
  node [
    id 34
    label "szko&#322;a"
  ]
  node [
    id 35
    label "morski"
  ]
  node [
    id 36
    label "Kliment"
  ]
  node [
    id 37
    label "Woroszy&#322;ow"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
]
