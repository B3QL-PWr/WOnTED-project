graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9375
  density 0.0625
  graphCliqueNumber 2
  node [
    id 0
    label "filler"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "wiecz&#243;r"
    origin "text"
  ]
  node [
    id 3
    label "xxx"
    origin "text"
  ]
  node [
    id 4
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 5
    label "drzewo_owocowe"
  ]
  node [
    id 6
    label "pomy&#347;lny"
  ]
  node [
    id 7
    label "skuteczny"
  ]
  node [
    id 8
    label "moralny"
  ]
  node [
    id 9
    label "korzystny"
  ]
  node [
    id 10
    label "odpowiedni"
  ]
  node [
    id 11
    label "zwrot"
  ]
  node [
    id 12
    label "dobrze"
  ]
  node [
    id 13
    label "pozytywny"
  ]
  node [
    id 14
    label "grzeczny"
  ]
  node [
    id 15
    label "powitanie"
  ]
  node [
    id 16
    label "mi&#322;y"
  ]
  node [
    id 17
    label "dobroczynny"
  ]
  node [
    id 18
    label "pos&#322;uszny"
  ]
  node [
    id 19
    label "ca&#322;y"
  ]
  node [
    id 20
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 21
    label "czw&#243;rka"
  ]
  node [
    id 22
    label "spokojny"
  ]
  node [
    id 23
    label "&#347;mieszny"
  ]
  node [
    id 24
    label "drogi"
  ]
  node [
    id 25
    label "zach&#243;d"
  ]
  node [
    id 26
    label "vesper"
  ]
  node [
    id 27
    label "spotkanie"
  ]
  node [
    id 28
    label "przyj&#281;cie"
  ]
  node [
    id 29
    label "pora"
  ]
  node [
    id 30
    label "dzie&#324;"
  ]
  node [
    id 31
    label "night"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
]
