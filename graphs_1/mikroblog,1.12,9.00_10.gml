graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.935483870967742
  density 0.06451612903225806
  graphCliqueNumber 2
  node [
    id 0
    label "kurrr"
    origin "text"
  ]
  node [
    id 1
    label "jak"
    origin "text"
  ]
  node [
    id 2
    label "&#347;wietny"
    origin "text"
  ]
  node [
    id 3
    label "serial"
    origin "text"
  ]
  node [
    id 4
    label "bardzo"
    origin "text"
  ]
  node [
    id 5
    label "pocieszny"
    origin "text"
  ]
  node [
    id 6
    label "byd&#322;o"
  ]
  node [
    id 7
    label "zobo"
  ]
  node [
    id 8
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 9
    label "yakalo"
  ]
  node [
    id 10
    label "dzo"
  ]
  node [
    id 11
    label "pomy&#347;lny"
  ]
  node [
    id 12
    label "pozytywny"
  ]
  node [
    id 13
    label "wspaniale"
  ]
  node [
    id 14
    label "dobry"
  ]
  node [
    id 15
    label "superancki"
  ]
  node [
    id 16
    label "arcydzielny"
  ]
  node [
    id 17
    label "zajebisty"
  ]
  node [
    id 18
    label "wa&#380;ny"
  ]
  node [
    id 19
    label "&#347;wietnie"
  ]
  node [
    id 20
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 21
    label "skuteczny"
  ]
  node [
    id 22
    label "spania&#322;y"
  ]
  node [
    id 23
    label "seria"
  ]
  node [
    id 24
    label "Klan"
  ]
  node [
    id 25
    label "film"
  ]
  node [
    id 26
    label "Ranczo"
  ]
  node [
    id 27
    label "program_telewizyjny"
  ]
  node [
    id 28
    label "w_chuj"
  ]
  node [
    id 29
    label "pociesznie"
  ]
  node [
    id 30
    label "&#347;mieszny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
]
