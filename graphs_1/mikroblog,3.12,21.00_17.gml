graph [
  maxDegree 42
  minDegree 1
  meanDegree 2
  density 0.014814814814814815
  graphCliqueNumber 2
  node [
    id 0
    label "alkohol"
    origin "text"
  ]
  node [
    id 1
    label "zwiazki"
    origin "text"
  ]
  node [
    id 2
    label "logikaniebieskichpaskow"
    origin "text"
  ]
  node [
    id 3
    label "moja"
    origin "text"
  ]
  node [
    id 4
    label "super"
    origin "text"
  ]
  node [
    id 5
    label "&#380;ona"
    origin "text"
  ]
  node [
    id 6
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 8
    label "spotkanie"
    origin "text"
  ]
  node [
    id 9
    label "kole&#380;anka"
    origin "text"
  ]
  node [
    id 10
    label "akurat"
    origin "text"
  ]
  node [
    id 11
    label "wtedy"
    origin "text"
  ]
  node [
    id 12
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 13
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 14
    label "gorzelnia_rolnicza"
  ]
  node [
    id 15
    label "upija&#263;"
  ]
  node [
    id 16
    label "szk&#322;o"
  ]
  node [
    id 17
    label "spirytualia"
  ]
  node [
    id 18
    label "nap&#243;j"
  ]
  node [
    id 19
    label "wypicie"
  ]
  node [
    id 20
    label "poniewierca"
  ]
  node [
    id 21
    label "rozgrzewacz"
  ]
  node [
    id 22
    label "upajanie"
  ]
  node [
    id 23
    label "piwniczka"
  ]
  node [
    id 24
    label "najebka"
  ]
  node [
    id 25
    label "grupa_hydroksylowa"
  ]
  node [
    id 26
    label "le&#380;akownia"
  ]
  node [
    id 27
    label "g&#322;owa"
  ]
  node [
    id 28
    label "upi&#263;"
  ]
  node [
    id 29
    label "upojenie"
  ]
  node [
    id 30
    label "likwor"
  ]
  node [
    id 31
    label "u&#380;ywka"
  ]
  node [
    id 32
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 33
    label "alko"
  ]
  node [
    id 34
    label "picie"
  ]
  node [
    id 35
    label "wspaniale"
  ]
  node [
    id 36
    label "dobry"
  ]
  node [
    id 37
    label "wspania&#322;y"
  ]
  node [
    id 38
    label "superancko"
  ]
  node [
    id 39
    label "superancki"
  ]
  node [
    id 40
    label "kapitalny"
  ]
  node [
    id 41
    label "kapitalnie"
  ]
  node [
    id 42
    label "ma&#322;&#380;onek"
  ]
  node [
    id 43
    label "panna_m&#322;oda"
  ]
  node [
    id 44
    label "partnerka"
  ]
  node [
    id 45
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 46
    label "&#347;lubna"
  ]
  node [
    id 47
    label "kobita"
  ]
  node [
    id 48
    label "zorganizowa&#263;"
  ]
  node [
    id 49
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 50
    label "przerobi&#263;"
  ]
  node [
    id 51
    label "wystylizowa&#263;"
  ]
  node [
    id 52
    label "cause"
  ]
  node [
    id 53
    label "wydali&#263;"
  ]
  node [
    id 54
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 55
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 56
    label "post&#261;pi&#263;"
  ]
  node [
    id 57
    label "appoint"
  ]
  node [
    id 58
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 59
    label "nabra&#263;"
  ]
  node [
    id 60
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 61
    label "make"
  ]
  node [
    id 62
    label "stanie"
  ]
  node [
    id 63
    label "przebywanie"
  ]
  node [
    id 64
    label "panowanie"
  ]
  node [
    id 65
    label "zajmowanie"
  ]
  node [
    id 66
    label "pomieszkanie"
  ]
  node [
    id 67
    label "adjustment"
  ]
  node [
    id 68
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 69
    label "lokal"
  ]
  node [
    id 70
    label "kwadrat"
  ]
  node [
    id 71
    label "animation"
  ]
  node [
    id 72
    label "dom"
  ]
  node [
    id 73
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 74
    label "po&#380;egnanie"
  ]
  node [
    id 75
    label "spowodowanie"
  ]
  node [
    id 76
    label "znalezienie"
  ]
  node [
    id 77
    label "znajomy"
  ]
  node [
    id 78
    label "doznanie"
  ]
  node [
    id 79
    label "employment"
  ]
  node [
    id 80
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 81
    label "gather"
  ]
  node [
    id 82
    label "powitanie"
  ]
  node [
    id 83
    label "spotykanie"
  ]
  node [
    id 84
    label "wydarzenie"
  ]
  node [
    id 85
    label "gathering"
  ]
  node [
    id 86
    label "spotkanie_si&#281;"
  ]
  node [
    id 87
    label "zdarzenie_si&#281;"
  ]
  node [
    id 88
    label "match"
  ]
  node [
    id 89
    label "zawarcie"
  ]
  node [
    id 90
    label "kuma"
  ]
  node [
    id 91
    label "odpowiedni"
  ]
  node [
    id 92
    label "dok&#322;adnie"
  ]
  node [
    id 93
    label "akuratny"
  ]
  node [
    id 94
    label "kiedy&#347;"
  ]
  node [
    id 95
    label "Mickiewicz"
  ]
  node [
    id 96
    label "czas"
  ]
  node [
    id 97
    label "szkolenie"
  ]
  node [
    id 98
    label "przepisa&#263;"
  ]
  node [
    id 99
    label "lesson"
  ]
  node [
    id 100
    label "grupa"
  ]
  node [
    id 101
    label "praktyka"
  ]
  node [
    id 102
    label "metoda"
  ]
  node [
    id 103
    label "niepokalanki"
  ]
  node [
    id 104
    label "kara"
  ]
  node [
    id 105
    label "zda&#263;"
  ]
  node [
    id 106
    label "form"
  ]
  node [
    id 107
    label "kwalifikacje"
  ]
  node [
    id 108
    label "system"
  ]
  node [
    id 109
    label "przepisanie"
  ]
  node [
    id 110
    label "sztuba"
  ]
  node [
    id 111
    label "wiedza"
  ]
  node [
    id 112
    label "stopek"
  ]
  node [
    id 113
    label "school"
  ]
  node [
    id 114
    label "absolwent"
  ]
  node [
    id 115
    label "urszulanki"
  ]
  node [
    id 116
    label "gabinet"
  ]
  node [
    id 117
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 118
    label "ideologia"
  ]
  node [
    id 119
    label "lekcja"
  ]
  node [
    id 120
    label "muzyka"
  ]
  node [
    id 121
    label "podr&#281;cznik"
  ]
  node [
    id 122
    label "zdanie"
  ]
  node [
    id 123
    label "siedziba"
  ]
  node [
    id 124
    label "sekretariat"
  ]
  node [
    id 125
    label "nauka"
  ]
  node [
    id 126
    label "do&#347;wiadczenie"
  ]
  node [
    id 127
    label "tablica"
  ]
  node [
    id 128
    label "teren_szko&#322;y"
  ]
  node [
    id 129
    label "instytucja"
  ]
  node [
    id 130
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 131
    label "skolaryzacja"
  ]
  node [
    id 132
    label "&#322;awa_szkolna"
  ]
  node [
    id 133
    label "klasa"
  ]
  node [
    id 134
    label "Royal"
  ]
  node [
    id 135
    label "Salute"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 13
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 134
    target 135
  ]
]
