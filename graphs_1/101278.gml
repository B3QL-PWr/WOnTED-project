graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "geofit"
    origin "text"
  ]
  node [
    id 1
    label "cebulkowy"
    origin "text"
  ]
  node [
    id 2
    label "kryptofit"
  ]
  node [
    id 3
    label "cebulowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 3
  ]
]
