graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 3
  node [
    id 0
    label "policja"
    origin "text"
  ]
  node [
    id 1
    label "poszukiwa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "letni"
    origin "text"
  ]
  node [
    id 3
    label "kamila"
    origin "text"
  ]
  node [
    id 4
    label "siedlecki"
    origin "text"
  ]
  node [
    id 5
    label "&#347;widnik"
    origin "text"
  ]
  node [
    id 6
    label "komisariat"
  ]
  node [
    id 7
    label "psiarnia"
  ]
  node [
    id 8
    label "posterunek"
  ]
  node [
    id 9
    label "grupa"
  ]
  node [
    id 10
    label "organ"
  ]
  node [
    id 11
    label "s&#322;u&#380;ba"
  ]
  node [
    id 12
    label "ask"
  ]
  node [
    id 13
    label "stara&#263;_si&#281;"
  ]
  node [
    id 14
    label "szuka&#263;"
  ]
  node [
    id 15
    label "look"
  ]
  node [
    id 16
    label "nijaki"
  ]
  node [
    id 17
    label "sezonowy"
  ]
  node [
    id 18
    label "letnio"
  ]
  node [
    id 19
    label "s&#322;oneczny"
  ]
  node [
    id 20
    label "weso&#322;y"
  ]
  node [
    id 21
    label "oboj&#281;tny"
  ]
  node [
    id 22
    label "latowy"
  ]
  node [
    id 23
    label "ciep&#322;y"
  ]
  node [
    id 24
    label "typowy"
  ]
  node [
    id 25
    label "Kamila"
  ]
  node [
    id 26
    label "pierwsza"
  ]
  node [
    id 27
    label "kategoria"
  ]
  node [
    id 28
    label "zagini&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
]
