graph [
  maxDegree 9
  minDegree 1
  meanDegree 3.35
  density 0.0858974358974359
  graphCliqueNumber 7
  node [
    id 0
    label "william"
    origin "text"
  ]
  node [
    id 1
    label "thomas"
    origin "text"
  ]
  node [
    id 2
    label "William"
  ]
  node [
    id 3
    label "Thomas"
  ]
  node [
    id 4
    label "Isaac"
  ]
  node [
    id 5
    label "nowy"
  ]
  node [
    id 6
    label "Jork"
  ]
  node [
    id 7
    label "uniwersytet"
  ]
  node [
    id 8
    label "wyspa"
  ]
  node [
    id 9
    label "Chicago"
  ]
  node [
    id 10
    label "Harvard"
  ]
  node [
    id 11
    label "University"
  ]
  node [
    id 12
    label "Newa"
  ]
  node [
    id 13
    label "School"
  ]
  node [
    id 14
    label "for"
  ]
  node [
    id 15
    label "Social"
  ]
  node [
    id 16
    label "Research"
  ]
  node [
    id 17
    label "stan"
  ]
  node [
    id 18
    label "zjednoczy&#263;"
  ]
  node [
    id 19
    label "Florian"
  ]
  node [
    id 20
    label "Znaniecki"
  ]
  node [
    id 21
    label "The"
  ]
  node [
    id 22
    label "Polish"
  ]
  node [
    id 23
    label "Peasant"
  ]
  node [
    id 24
    label "inny"
  ]
  node [
    id 25
    label "Europe"
  ]
  node [
    id 26
    label "Anda"
  ]
  node [
    id 27
    label "America"
  ]
  node [
    id 28
    label "ch&#322;op"
  ]
  node [
    id 29
    label "polski"
  ]
  node [
    id 30
    label "europ"
  ]
  node [
    id 31
    label "i"
  ]
  node [
    id 32
    label "Ameryka"
  ]
  node [
    id 33
    label "sex"
  ]
  node [
    id 34
    label "Society"
  ]
  node [
    id 35
    label "Unadjusted"
  ]
  node [
    id 36
    label "Girl"
  ]
  node [
    id 37
    label "Child"
  ]
  node [
    id 38
    label "Primitive"
  ]
  node [
    id 39
    label "Behaviour"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 21
    target 37
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 37
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 27
    target 37
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 38
    target 39
  ]
]
