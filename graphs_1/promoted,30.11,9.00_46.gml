graph [
  maxDegree 21
  minDegree 1
  meanDegree 2.033333333333333
  density 0.03446327683615819
  graphCliqueNumber 2
  node [
    id 0
    label "ofiara"
    origin "text"
  ]
  node [
    id 1
    label "pada&#263;"
    origin "text"
  ]
  node [
    id 2
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 3
    label "kamienna"
    origin "text"
  ]
  node [
    id 4
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "gamo&#324;"
  ]
  node [
    id 7
    label "istota_&#380;ywa"
  ]
  node [
    id 8
    label "dupa_wo&#322;owa"
  ]
  node [
    id 9
    label "przedmiot"
  ]
  node [
    id 10
    label "pastwa"
  ]
  node [
    id 11
    label "rzecz"
  ]
  node [
    id 12
    label "zbi&#243;rka"
  ]
  node [
    id 13
    label "dar"
  ]
  node [
    id 14
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 15
    label "nastawienie"
  ]
  node [
    id 16
    label "crack"
  ]
  node [
    id 17
    label "zwierz&#281;"
  ]
  node [
    id 18
    label "ko&#347;cielny"
  ]
  node [
    id 19
    label "po&#347;miewisko"
  ]
  node [
    id 20
    label "zdycha&#263;"
  ]
  node [
    id 21
    label "by&#263;"
  ]
  node [
    id 22
    label "mie&#263;_miejsce"
  ]
  node [
    id 23
    label "die"
  ]
  node [
    id 24
    label "przestawa&#263;"
  ]
  node [
    id 25
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 26
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 27
    label "przelecie&#263;"
  ]
  node [
    id 28
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 29
    label "robi&#263;"
  ]
  node [
    id 30
    label "gin&#261;&#263;"
  ]
  node [
    id 31
    label "przypada&#263;"
  ]
  node [
    id 32
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 33
    label "czu&#263;_si&#281;"
  ]
  node [
    id 34
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 35
    label "fall"
  ]
  node [
    id 36
    label "spada&#263;"
  ]
  node [
    id 37
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 38
    label "ludno&#347;&#263;"
  ]
  node [
    id 39
    label "grupa"
  ]
  node [
    id 40
    label "element"
  ]
  node [
    id 41
    label "przele&#378;&#263;"
  ]
  node [
    id 42
    label "pi&#281;tro"
  ]
  node [
    id 43
    label "karczek"
  ]
  node [
    id 44
    label "wysoki"
  ]
  node [
    id 45
    label "rami&#261;czko"
  ]
  node [
    id 46
    label "Ropa"
  ]
  node [
    id 47
    label "Jaworze"
  ]
  node [
    id 48
    label "Synaj"
  ]
  node [
    id 49
    label "wzniesienie"
  ]
  node [
    id 50
    label "przelezienie"
  ]
  node [
    id 51
    label "&#347;piew"
  ]
  node [
    id 52
    label "kupa"
  ]
  node [
    id 53
    label "kierunek"
  ]
  node [
    id 54
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 55
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 56
    label "d&#378;wi&#281;k"
  ]
  node [
    id 57
    label "Kreml"
  ]
  node [
    id 58
    label "Kamienna"
  ]
  node [
    id 59
    label "G&#243;ra"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 58
    target 59
  ]
]
