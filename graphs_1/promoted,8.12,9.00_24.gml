graph [
  maxDegree 22
  minDegree 1
  meanDegree 4.848484848484849
  density 0.15151515151515152
  graphCliqueNumber 11
  node [
    id 0
    label "part"
    origin "text"
  ]
  node [
    id 1
    label "the"
    origin "text"
  ]
  node [
    id 2
    label "journey"
    origin "text"
  ]
  node [
    id 3
    label "end"
    origin "text"
  ]
  node [
    id 4
    label "pierwszy"
    origin "text"
  ]
  node [
    id 5
    label "zwiastun"
    origin "text"
  ]
  node [
    id 6
    label "avengers"
    origin "text"
  ]
  node [
    id 7
    label "parciak"
  ]
  node [
    id 8
    label "ta&#347;ma"
  ]
  node [
    id 9
    label "plecionka"
  ]
  node [
    id 10
    label "p&#322;&#243;tno"
  ]
  node [
    id 11
    label "najwa&#380;niejszy"
  ]
  node [
    id 12
    label "pocz&#261;tkowy"
  ]
  node [
    id 13
    label "dobry"
  ]
  node [
    id 14
    label "ch&#281;tny"
  ]
  node [
    id 15
    label "dzie&#324;"
  ]
  node [
    id 16
    label "pr&#281;dki"
  ]
  node [
    id 17
    label "nabawianie_si&#281;"
  ]
  node [
    id 18
    label "harbinger"
  ]
  node [
    id 19
    label "oznaka"
  ]
  node [
    id 20
    label "zapowied&#378;"
  ]
  node [
    id 21
    label "nabawienie_si&#281;"
  ]
  node [
    id 22
    label "obwie&#347;ciciel"
  ]
  node [
    id 23
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 24
    label "przewidywanie"
  ]
  node [
    id 25
    label "declaration"
  ]
  node [
    id 26
    label "reklama"
  ]
  node [
    id 27
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 28
    label "of"
  ]
  node [
    id 29
    label "IS"
  ]
  node [
    id 30
    label "do"
  ]
  node [
    id 31
    label "Avengers"
  ]
  node [
    id 32
    label "4"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 31
    target 32
  ]
]
