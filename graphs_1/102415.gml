graph [
  maxDegree 50
  minDegree 1
  meanDegree 2.0427807486631018
  density 0.01098269219711345
  graphCliqueNumber 3
  node [
    id 0
    label "premier"
    origin "text"
  ]
  node [
    id 1
    label "moja"
    origin "text"
  ]
  node [
    id 2
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 3
    label "forma"
    origin "text"
  ]
  node [
    id 4
    label "papierowy"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "op&#243;&#378;nia&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ale"
    origin "text"
  ]
  node [
    id 8
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 9
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 10
    label "czyta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "wiki&#378;r&#243;d&#322;ach"
    origin "text"
  ]
  node [
    id 12
    label "liryk"
    origin "text"
  ]
  node [
    id 13
    label "polityk"
    origin "text"
  ]
  node [
    id 14
    label "mi&#322;a"
    origin "text"
  ]
  node [
    id 15
    label "lektura"
    origin "text"
  ]
  node [
    id 16
    label "Jelcyn"
  ]
  node [
    id 17
    label "Sto&#322;ypin"
  ]
  node [
    id 18
    label "dostojnik"
  ]
  node [
    id 19
    label "Chruszczow"
  ]
  node [
    id 20
    label "Miko&#322;ajczyk"
  ]
  node [
    id 21
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 22
    label "Bismarck"
  ]
  node [
    id 23
    label "rz&#261;d"
  ]
  node [
    id 24
    label "zwierzchnik"
  ]
  node [
    id 25
    label "ok&#322;adka"
  ]
  node [
    id 26
    label "zak&#322;adka"
  ]
  node [
    id 27
    label "ekslibris"
  ]
  node [
    id 28
    label "wk&#322;ad"
  ]
  node [
    id 29
    label "przek&#322;adacz"
  ]
  node [
    id 30
    label "wydawnictwo"
  ]
  node [
    id 31
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 32
    label "tytu&#322;"
  ]
  node [
    id 33
    label "bibliofilstwo"
  ]
  node [
    id 34
    label "falc"
  ]
  node [
    id 35
    label "nomina&#322;"
  ]
  node [
    id 36
    label "pagina"
  ]
  node [
    id 37
    label "rozdzia&#322;"
  ]
  node [
    id 38
    label "egzemplarz"
  ]
  node [
    id 39
    label "zw&#243;j"
  ]
  node [
    id 40
    label "tekst"
  ]
  node [
    id 41
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 42
    label "punkt_widzenia"
  ]
  node [
    id 43
    label "do&#322;ek"
  ]
  node [
    id 44
    label "formality"
  ]
  node [
    id 45
    label "wz&#243;r"
  ]
  node [
    id 46
    label "kantyzm"
  ]
  node [
    id 47
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 48
    label "ornamentyka"
  ]
  node [
    id 49
    label "odmiana"
  ]
  node [
    id 50
    label "mode"
  ]
  node [
    id 51
    label "style"
  ]
  node [
    id 52
    label "formacja"
  ]
  node [
    id 53
    label "maszyna_drukarska"
  ]
  node [
    id 54
    label "poznanie"
  ]
  node [
    id 55
    label "szablon"
  ]
  node [
    id 56
    label "struktura"
  ]
  node [
    id 57
    label "spirala"
  ]
  node [
    id 58
    label "blaszka"
  ]
  node [
    id 59
    label "linearno&#347;&#263;"
  ]
  node [
    id 60
    label "stan"
  ]
  node [
    id 61
    label "temat"
  ]
  node [
    id 62
    label "cecha"
  ]
  node [
    id 63
    label "g&#322;owa"
  ]
  node [
    id 64
    label "kielich"
  ]
  node [
    id 65
    label "zdolno&#347;&#263;"
  ]
  node [
    id 66
    label "poj&#281;cie"
  ]
  node [
    id 67
    label "kszta&#322;t"
  ]
  node [
    id 68
    label "pasmo"
  ]
  node [
    id 69
    label "rdze&#324;"
  ]
  node [
    id 70
    label "leksem"
  ]
  node [
    id 71
    label "dyspozycja"
  ]
  node [
    id 72
    label "wygl&#261;d"
  ]
  node [
    id 73
    label "October"
  ]
  node [
    id 74
    label "zawarto&#347;&#263;"
  ]
  node [
    id 75
    label "creation"
  ]
  node [
    id 76
    label "p&#281;tla"
  ]
  node [
    id 77
    label "p&#322;at"
  ]
  node [
    id 78
    label "gwiazda"
  ]
  node [
    id 79
    label "arystotelizm"
  ]
  node [
    id 80
    label "obiekt"
  ]
  node [
    id 81
    label "dzie&#322;o"
  ]
  node [
    id 82
    label "naczynie"
  ]
  node [
    id 83
    label "wyra&#380;enie"
  ]
  node [
    id 84
    label "jednostka_systematyczna"
  ]
  node [
    id 85
    label "miniatura"
  ]
  node [
    id 86
    label "zwyczaj"
  ]
  node [
    id 87
    label "morfem"
  ]
  node [
    id 88
    label "posta&#263;"
  ]
  node [
    id 89
    label "specjalny"
  ]
  node [
    id 90
    label "analogowy"
  ]
  node [
    id 91
    label "nierealistyczny"
  ]
  node [
    id 92
    label "papierowo"
  ]
  node [
    id 93
    label "w&#261;tpliwy"
  ]
  node [
    id 94
    label "podobny"
  ]
  node [
    id 95
    label "martwy"
  ]
  node [
    id 96
    label "delay"
  ]
  node [
    id 97
    label "sprawia&#263;"
  ]
  node [
    id 98
    label "piwo"
  ]
  node [
    id 99
    label "doba"
  ]
  node [
    id 100
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 101
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 102
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 103
    label "free"
  ]
  node [
    id 104
    label "dysleksja"
  ]
  node [
    id 105
    label "umie&#263;"
  ]
  node [
    id 106
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 107
    label "przetwarza&#263;"
  ]
  node [
    id 108
    label "read"
  ]
  node [
    id 109
    label "poznawa&#263;"
  ]
  node [
    id 110
    label "obserwowa&#263;"
  ]
  node [
    id 111
    label "odczytywa&#263;"
  ]
  node [
    id 112
    label "Zagajewski"
  ]
  node [
    id 113
    label "Osjan"
  ]
  node [
    id 114
    label "Horacy"
  ]
  node [
    id 115
    label "Mi&#322;osz"
  ]
  node [
    id 116
    label "Schiller"
  ]
  node [
    id 117
    label "Rej"
  ]
  node [
    id 118
    label "Byron"
  ]
  node [
    id 119
    label "dzie&#322;o_poetyckie"
  ]
  node [
    id 120
    label "Jan_Czeczot"
  ]
  node [
    id 121
    label "Peiper"
  ]
  node [
    id 122
    label "Bara&#324;czak"
  ]
  node [
    id 123
    label "Asnyk"
  ]
  node [
    id 124
    label "Bia&#322;oszewski"
  ]
  node [
    id 125
    label "Herbert"
  ]
  node [
    id 126
    label "Dante"
  ]
  node [
    id 127
    label "Puszkin"
  ]
  node [
    id 128
    label "Le&#347;mian"
  ]
  node [
    id 129
    label "R&#243;&#380;ewicz"
  ]
  node [
    id 130
    label "lyric"
  ]
  node [
    id 131
    label "Jesienin"
  ]
  node [
    id 132
    label "pisarz"
  ]
  node [
    id 133
    label "wierszopis"
  ]
  node [
    id 134
    label "Tuwim"
  ]
  node [
    id 135
    label "Norwid"
  ]
  node [
    id 136
    label "Homer"
  ]
  node [
    id 137
    label "Pindar"
  ]
  node [
    id 138
    label "Dawid"
  ]
  node [
    id 139
    label "J&#281;drzejewicz"
  ]
  node [
    id 140
    label "Nixon"
  ]
  node [
    id 141
    label "Perykles"
  ]
  node [
    id 142
    label "bezpartyjny"
  ]
  node [
    id 143
    label "Gomu&#322;ka"
  ]
  node [
    id 144
    label "Gorbaczow"
  ]
  node [
    id 145
    label "Borel"
  ]
  node [
    id 146
    label "Katon"
  ]
  node [
    id 147
    label "McCarthy"
  ]
  node [
    id 148
    label "Naser"
  ]
  node [
    id 149
    label "Gierek"
  ]
  node [
    id 150
    label "Goebbels"
  ]
  node [
    id 151
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 152
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 153
    label "de_Gaulle"
  ]
  node [
    id 154
    label "Vladimir_Me&#269;iar"
  ]
  node [
    id 155
    label "Bre&#380;niew"
  ]
  node [
    id 156
    label "Juliusz_Cezar"
  ]
  node [
    id 157
    label "Bierut"
  ]
  node [
    id 158
    label "Kuro&#324;"
  ]
  node [
    id 159
    label "Arafat"
  ]
  node [
    id 160
    label "Fidel_Castro"
  ]
  node [
    id 161
    label "Moczar"
  ]
  node [
    id 162
    label "Korwin"
  ]
  node [
    id 163
    label "dzia&#322;acz"
  ]
  node [
    id 164
    label "Winston_Churchill"
  ]
  node [
    id 165
    label "Leszek_Miller"
  ]
  node [
    id 166
    label "Ziobro"
  ]
  node [
    id 167
    label "Putin"
  ]
  node [
    id 168
    label "Falandysz"
  ]
  node [
    id 169
    label "Mao"
  ]
  node [
    id 170
    label "Metternich"
  ]
  node [
    id 171
    label "ptaszyna"
  ]
  node [
    id 172
    label "umi&#322;owana"
  ]
  node [
    id 173
    label "kochanka"
  ]
  node [
    id 174
    label "kochanie"
  ]
  node [
    id 175
    label "Dulcynea"
  ]
  node [
    id 176
    label "wybranka"
  ]
  node [
    id 177
    label "wyczytywanie"
  ]
  node [
    id 178
    label "doczytywanie"
  ]
  node [
    id 179
    label "oczytywanie_si&#281;"
  ]
  node [
    id 180
    label "zaczytanie_si&#281;"
  ]
  node [
    id 181
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 182
    label "czytywanie"
  ]
  node [
    id 183
    label "recitation"
  ]
  node [
    id 184
    label "poznawanie"
  ]
  node [
    id 185
    label "poczytanie"
  ]
  node [
    id 186
    label "wczytywanie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 40
  ]
]
