graph [
  maxDegree 38
  minDegree 1
  meanDegree 1.9574468085106382
  density 0.0425531914893617
  graphCliqueNumber 2
  node [
    id 0
    label "pokazowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "moja"
    origin "text"
  ]
  node [
    id 2
    label "stracona"
    origin "text"
  ]
  node [
    id 3
    label "marzenia"
    origin "text"
  ]
  node [
    id 4
    label "naiwny"
    origin "text"
  ]
  node [
    id 5
    label "czas"
    origin "text"
  ]
  node [
    id 6
    label "naiwnie"
  ]
  node [
    id 7
    label "g&#322;upi"
  ]
  node [
    id 8
    label "prostoduszny"
  ]
  node [
    id 9
    label "poczciwy"
  ]
  node [
    id 10
    label "czasokres"
  ]
  node [
    id 11
    label "trawienie"
  ]
  node [
    id 12
    label "kategoria_gramatyczna"
  ]
  node [
    id 13
    label "period"
  ]
  node [
    id 14
    label "odczyt"
  ]
  node [
    id 15
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 16
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 17
    label "chwila"
  ]
  node [
    id 18
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 19
    label "poprzedzenie"
  ]
  node [
    id 20
    label "koniugacja"
  ]
  node [
    id 21
    label "dzieje"
  ]
  node [
    id 22
    label "poprzedzi&#263;"
  ]
  node [
    id 23
    label "przep&#322;ywanie"
  ]
  node [
    id 24
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 25
    label "odwlekanie_si&#281;"
  ]
  node [
    id 26
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 27
    label "Zeitgeist"
  ]
  node [
    id 28
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 29
    label "okres_czasu"
  ]
  node [
    id 30
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 31
    label "pochodzi&#263;"
  ]
  node [
    id 32
    label "schy&#322;ek"
  ]
  node [
    id 33
    label "czwarty_wymiar"
  ]
  node [
    id 34
    label "chronometria"
  ]
  node [
    id 35
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 36
    label "poprzedzanie"
  ]
  node [
    id 37
    label "pogoda"
  ]
  node [
    id 38
    label "zegar"
  ]
  node [
    id 39
    label "pochodzenie"
  ]
  node [
    id 40
    label "poprzedza&#263;"
  ]
  node [
    id 41
    label "trawi&#263;"
  ]
  node [
    id 42
    label "time_period"
  ]
  node [
    id 43
    label "rachuba_czasu"
  ]
  node [
    id 44
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 45
    label "czasoprzestrze&#324;"
  ]
  node [
    id 46
    label "laba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
]
