graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.894736842105263
  density 0.10526315789473684
  graphCliqueNumber 2
  node [
    id 0
    label "czyszczenie"
    origin "text"
  ]
  node [
    id 1
    label "&#322;o&#380;ysko"
    origin "text"
  ]
  node [
    id 2
    label "cleaning"
  ]
  node [
    id 3
    label "uk&#322;adanie"
  ]
  node [
    id 4
    label "czyszczenie_si&#281;"
  ]
  node [
    id 5
    label "przeczyszczenie"
  ]
  node [
    id 6
    label "usuwanie"
  ]
  node [
    id 7
    label "purge"
  ]
  node [
    id 8
    label "klarownia"
  ]
  node [
    id 9
    label "katarakta"
  ]
  node [
    id 10
    label "trofoblast"
  ]
  node [
    id 11
    label "placenta"
  ]
  node [
    id 12
    label "organ"
  ]
  node [
    id 13
    label "dolina"
  ]
  node [
    id 14
    label "zal&#261;&#380;nia"
  ]
  node [
    id 15
    label "layer"
  ]
  node [
    id 16
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 17
    label "panew"
  ]
  node [
    id 18
    label "bariera_&#322;o&#380;yskowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
]
