graph [
  maxDegree 34
  minDegree 1
  meanDegree 1.9803921568627452
  density 0.0196078431372549
  graphCliqueNumber 2
  node [
    id 0
    label "wczorajszy"
    origin "text"
  ]
  node [
    id 1
    label "gazeta"
    origin "text"
  ]
  node [
    id 2
    label "wyborczy"
    origin "text"
  ]
  node [
    id 3
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "o&#380;ywiaj&#261;cy"
    origin "text"
  ]
  node [
    id 5
    label "kultura"
    origin "text"
  ]
  node [
    id 6
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "prewencja"
    origin "text"
  ]
  node [
    id 9
    label "zabijaj&#261;cy"
    origin "text"
  ]
  node [
    id 10
    label "poradzi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "dawny"
  ]
  node [
    id 12
    label "stary"
  ]
  node [
    id 13
    label "archaicznie"
  ]
  node [
    id 14
    label "zgrzybienie"
  ]
  node [
    id 15
    label "przestarzale"
  ]
  node [
    id 16
    label "starzenie_si&#281;"
  ]
  node [
    id 17
    label "zestarzenie_si&#281;"
  ]
  node [
    id 18
    label "niedzisiejszy"
  ]
  node [
    id 19
    label "prasa"
  ]
  node [
    id 20
    label "redakcja"
  ]
  node [
    id 21
    label "tytu&#322;"
  ]
  node [
    id 22
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 23
    label "czasopismo"
  ]
  node [
    id 24
    label "remark"
  ]
  node [
    id 25
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 26
    label "u&#380;ywa&#263;"
  ]
  node [
    id 27
    label "okre&#347;la&#263;"
  ]
  node [
    id 28
    label "j&#281;zyk"
  ]
  node [
    id 29
    label "say"
  ]
  node [
    id 30
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 31
    label "formu&#322;owa&#263;"
  ]
  node [
    id 32
    label "talk"
  ]
  node [
    id 33
    label "powiada&#263;"
  ]
  node [
    id 34
    label "informowa&#263;"
  ]
  node [
    id 35
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 36
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 37
    label "wydobywa&#263;"
  ]
  node [
    id 38
    label "express"
  ]
  node [
    id 39
    label "chew_the_fat"
  ]
  node [
    id 40
    label "dysfonia"
  ]
  node [
    id 41
    label "umie&#263;"
  ]
  node [
    id 42
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 43
    label "tell"
  ]
  node [
    id 44
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 45
    label "wyra&#380;a&#263;"
  ]
  node [
    id 46
    label "gaworzy&#263;"
  ]
  node [
    id 47
    label "rozmawia&#263;"
  ]
  node [
    id 48
    label "dziama&#263;"
  ]
  node [
    id 49
    label "prawi&#263;"
  ]
  node [
    id 50
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 51
    label "przedmiot"
  ]
  node [
    id 52
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 53
    label "Wsch&#243;d"
  ]
  node [
    id 54
    label "rzecz"
  ]
  node [
    id 55
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 56
    label "sztuka"
  ]
  node [
    id 57
    label "religia"
  ]
  node [
    id 58
    label "przejmowa&#263;"
  ]
  node [
    id 59
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 60
    label "makrokosmos"
  ]
  node [
    id 61
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 62
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 63
    label "zjawisko"
  ]
  node [
    id 64
    label "praca_rolnicza"
  ]
  node [
    id 65
    label "tradycja"
  ]
  node [
    id 66
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 67
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 68
    label "przejmowanie"
  ]
  node [
    id 69
    label "cecha"
  ]
  node [
    id 70
    label "asymilowanie_si&#281;"
  ]
  node [
    id 71
    label "przej&#261;&#263;"
  ]
  node [
    id 72
    label "hodowla"
  ]
  node [
    id 73
    label "brzoskwiniarnia"
  ]
  node [
    id 74
    label "populace"
  ]
  node [
    id 75
    label "konwencja"
  ]
  node [
    id 76
    label "propriety"
  ]
  node [
    id 77
    label "jako&#347;&#263;"
  ]
  node [
    id 78
    label "kuchnia"
  ]
  node [
    id 79
    label "zwyczaj"
  ]
  node [
    id 80
    label "przej&#281;cie"
  ]
  node [
    id 81
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 82
    label "zorganizowa&#263;"
  ]
  node [
    id 83
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 84
    label "przerobi&#263;"
  ]
  node [
    id 85
    label "wystylizowa&#263;"
  ]
  node [
    id 86
    label "cause"
  ]
  node [
    id 87
    label "wydali&#263;"
  ]
  node [
    id 88
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 89
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 90
    label "post&#261;pi&#263;"
  ]
  node [
    id 91
    label "appoint"
  ]
  node [
    id 92
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 93
    label "nabra&#263;"
  ]
  node [
    id 94
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 95
    label "make"
  ]
  node [
    id 96
    label "ochrona"
  ]
  node [
    id 97
    label "restraint"
  ]
  node [
    id 98
    label "rede"
  ]
  node [
    id 99
    label "guidance"
  ]
  node [
    id 100
    label "pom&#243;c"
  ]
  node [
    id 101
    label "udzieli&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
]
