graph [
  maxDegree 3
  minDegree 0
  meanDegree 2
  density 0.3333333333333333
  graphCliqueNumber 4
  node [
    id 0
    label "rab&#243;s"
    origin "text"
  ]
  node [
    id 1
    label "alt"
  ]
  node [
    id 2
    label "Empord&#224;"
  ]
  node [
    id 3
    label "Sant"
  ]
  node [
    id 4
    label "Quirze"
  ]
  node [
    id 5
    label "de"
  ]
  node [
    id 6
    label "Colera"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
]
