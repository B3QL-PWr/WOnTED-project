graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9884393063583814
  density 0.011560693641618497
  graphCliqueNumber 2
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "kilogram"
    origin "text"
  ]
  node [
    id 2
    label "krowa"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "dobry"
    origin "text"
  ]
  node [
    id 5
    label "jako&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "mi&#281;so"
    origin "text"
  ]
  node [
    id 7
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "eksperyment"
    origin "text"
  ]
  node [
    id 9
    label "wyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "szkoda"
    origin "text"
  ]
  node [
    id 11
    label "dobre"
    origin "text"
  ]
  node [
    id 12
    label "kawa&#322;ek"
    origin "text"
  ]
  node [
    id 13
    label "oraz"
    origin "text"
  ]
  node [
    id 14
    label "lod&#243;wka"
    origin "text"
  ]
  node [
    id 15
    label "matczysko"
  ]
  node [
    id 16
    label "macierz"
  ]
  node [
    id 17
    label "przodkini"
  ]
  node [
    id 18
    label "Matka_Boska"
  ]
  node [
    id 19
    label "macocha"
  ]
  node [
    id 20
    label "matka_zast&#281;pcza"
  ]
  node [
    id 21
    label "stara"
  ]
  node [
    id 22
    label "rodzice"
  ]
  node [
    id 23
    label "rodzic"
  ]
  node [
    id 24
    label "dekagram"
  ]
  node [
    id 25
    label "uk&#322;ad_SI"
  ]
  node [
    id 26
    label "metryczna_jednostka_masy"
  ]
  node [
    id 27
    label "hektogram"
  ]
  node [
    id 28
    label "tona"
  ]
  node [
    id 29
    label "szkarada"
  ]
  node [
    id 30
    label "wymi&#281;"
  ]
  node [
    id 31
    label "samica"
  ]
  node [
    id 32
    label "baba"
  ]
  node [
    id 33
    label "berek"
  ]
  node [
    id 34
    label "mucze&#263;"
  ]
  node [
    id 35
    label "muczenie"
  ]
  node [
    id 36
    label "mleczno&#347;&#263;"
  ]
  node [
    id 37
    label "krasula"
  ]
  node [
    id 38
    label "miotacz"
  ]
  node [
    id 39
    label "gigant"
  ]
  node [
    id 40
    label "zamucze&#263;"
  ]
  node [
    id 41
    label "bydl&#281;"
  ]
  node [
    id 42
    label "si&#281;ga&#263;"
  ]
  node [
    id 43
    label "trwa&#263;"
  ]
  node [
    id 44
    label "obecno&#347;&#263;"
  ]
  node [
    id 45
    label "stan"
  ]
  node [
    id 46
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 47
    label "stand"
  ]
  node [
    id 48
    label "mie&#263;_miejsce"
  ]
  node [
    id 49
    label "uczestniczy&#263;"
  ]
  node [
    id 50
    label "chodzi&#263;"
  ]
  node [
    id 51
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 52
    label "equal"
  ]
  node [
    id 53
    label "pomy&#347;lny"
  ]
  node [
    id 54
    label "skuteczny"
  ]
  node [
    id 55
    label "moralny"
  ]
  node [
    id 56
    label "korzystny"
  ]
  node [
    id 57
    label "odpowiedni"
  ]
  node [
    id 58
    label "zwrot"
  ]
  node [
    id 59
    label "dobrze"
  ]
  node [
    id 60
    label "pozytywny"
  ]
  node [
    id 61
    label "grzeczny"
  ]
  node [
    id 62
    label "powitanie"
  ]
  node [
    id 63
    label "mi&#322;y"
  ]
  node [
    id 64
    label "dobroczynny"
  ]
  node [
    id 65
    label "pos&#322;uszny"
  ]
  node [
    id 66
    label "ca&#322;y"
  ]
  node [
    id 67
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 68
    label "czw&#243;rka"
  ]
  node [
    id 69
    label "spokojny"
  ]
  node [
    id 70
    label "&#347;mieszny"
  ]
  node [
    id 71
    label "drogi"
  ]
  node [
    id 72
    label "warto&#347;&#263;"
  ]
  node [
    id 73
    label "co&#347;"
  ]
  node [
    id 74
    label "syf"
  ]
  node [
    id 75
    label "state"
  ]
  node [
    id 76
    label "quality"
  ]
  node [
    id 77
    label "wyluzowanie"
  ]
  node [
    id 78
    label "obieralnia"
  ]
  node [
    id 79
    label "cia&#322;o"
  ]
  node [
    id 80
    label "produkt"
  ]
  node [
    id 81
    label "t&#322;uczenie"
  ]
  node [
    id 82
    label "krusze&#263;"
  ]
  node [
    id 83
    label "mi&#281;sie&#324;"
  ]
  node [
    id 84
    label "skrusze&#263;"
  ]
  node [
    id 85
    label "jedzenie"
  ]
  node [
    id 86
    label "marynata"
  ]
  node [
    id 87
    label "luzowa&#263;"
  ]
  node [
    id 88
    label "luzowanie"
  ]
  node [
    id 89
    label "chabanina"
  ]
  node [
    id 90
    label "ut&#322;uczenie"
  ]
  node [
    id 91
    label "panierka"
  ]
  node [
    id 92
    label "potrawa"
  ]
  node [
    id 93
    label "wyluzowa&#263;"
  ]
  node [
    id 94
    label "seitan"
  ]
  node [
    id 95
    label "tempeh"
  ]
  node [
    id 96
    label "tentegowa&#263;"
  ]
  node [
    id 97
    label "urz&#261;dza&#263;"
  ]
  node [
    id 98
    label "give"
  ]
  node [
    id 99
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 100
    label "czyni&#263;"
  ]
  node [
    id 101
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 102
    label "post&#281;powa&#263;"
  ]
  node [
    id 103
    label "wydala&#263;"
  ]
  node [
    id 104
    label "oszukiwa&#263;"
  ]
  node [
    id 105
    label "organizowa&#263;"
  ]
  node [
    id 106
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 107
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 108
    label "work"
  ]
  node [
    id 109
    label "przerabia&#263;"
  ]
  node [
    id 110
    label "stylizowa&#263;"
  ]
  node [
    id 111
    label "falowa&#263;"
  ]
  node [
    id 112
    label "act"
  ]
  node [
    id 113
    label "peddle"
  ]
  node [
    id 114
    label "ukazywa&#263;"
  ]
  node [
    id 115
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 116
    label "praca"
  ]
  node [
    id 117
    label "innowacja"
  ]
  node [
    id 118
    label "badanie"
  ]
  node [
    id 119
    label "assay"
  ]
  node [
    id 120
    label "obserwowanie"
  ]
  node [
    id 121
    label "get"
  ]
  node [
    id 122
    label "opu&#347;ci&#263;"
  ]
  node [
    id 123
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 124
    label "zej&#347;&#263;"
  ]
  node [
    id 125
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 126
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 127
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 128
    label "sko&#324;czy&#263;"
  ]
  node [
    id 129
    label "ograniczenie"
  ]
  node [
    id 130
    label "ruszy&#263;"
  ]
  node [
    id 131
    label "wypa&#347;&#263;"
  ]
  node [
    id 132
    label "uko&#324;czy&#263;"
  ]
  node [
    id 133
    label "open"
  ]
  node [
    id 134
    label "moderate"
  ]
  node [
    id 135
    label "uzyska&#263;"
  ]
  node [
    id 136
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 137
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 138
    label "mount"
  ]
  node [
    id 139
    label "leave"
  ]
  node [
    id 140
    label "drive"
  ]
  node [
    id 141
    label "zagra&#263;"
  ]
  node [
    id 142
    label "zademonstrowa&#263;"
  ]
  node [
    id 143
    label "wystarczy&#263;"
  ]
  node [
    id 144
    label "perform"
  ]
  node [
    id 145
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 146
    label "drop"
  ]
  node [
    id 147
    label "pole"
  ]
  node [
    id 148
    label "commiseration"
  ]
  node [
    id 149
    label "czu&#263;"
  ]
  node [
    id 150
    label "zniszczenie"
  ]
  node [
    id 151
    label "niepowodzenie"
  ]
  node [
    id 152
    label "ubytek"
  ]
  node [
    id 153
    label "&#380;erowisko"
  ]
  node [
    id 154
    label "szwank"
  ]
  node [
    id 155
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 156
    label "podp&#322;ywanie"
  ]
  node [
    id 157
    label "plot"
  ]
  node [
    id 158
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 159
    label "piece"
  ]
  node [
    id 160
    label "kawa&#322;"
  ]
  node [
    id 161
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 162
    label "utw&#243;r"
  ]
  node [
    id 163
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 164
    label "warnik"
  ]
  node [
    id 165
    label "zamra&#380;alnik"
  ]
  node [
    id 166
    label "tracze"
  ]
  node [
    id 167
    label "ch&#322;odnia"
  ]
  node [
    id 168
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 169
    label "zi&#281;biarka"
  ]
  node [
    id 170
    label "sprz&#281;t_AGD"
  ]
  node [
    id 171
    label "kaczka"
  ]
  node [
    id 172
    label "lada_ch&#322;odnicza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
]
