graph [
  maxDegree 2
  minDegree 0
  meanDegree 1.3846153846153846
  density 0.11538461538461539
  graphCliqueNumber 3
  node [
    id 0
    label "culture"
    origin "text"
  ]
  node [
    id 1
    label "instytut"
  ]
  node [
    id 2
    label "Adam"
  ]
  node [
    id 3
    label "Mickiewicz"
  ]
  node [
    id 4
    label "Andrzej"
  ]
  node [
    id 5
    label "Lubomirski"
  ]
  node [
    id 6
    label "Monika"
  ]
  node [
    id 7
    label "Renc&#322;awowicz"
  ]
  node [
    id 8
    label "El&#380;bieta"
  ]
  node [
    id 9
    label "Sawicka"
  ]
  node [
    id 10
    label "marka"
  ]
  node [
    id 11
    label "ko&#322;o"
  ]
  node [
    id 12
    label "Zalejskiego"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
]
