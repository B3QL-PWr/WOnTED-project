graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.0123456790123457
  density 0.012499041484548732
  graphCliqueNumber 2
  node [
    id 0
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 1
    label "usa"
    origin "text"
  ]
  node [
    id 2
    label "today"
    origin "text"
  ]
  node [
    id 3
    label "temat"
    origin "text"
  ]
  node [
    id 4
    label "film"
    origin "text"
  ]
  node [
    id 5
    label "lista"
    origin "text"
  ]
  node [
    id 6
    label "schindler"
    origin "text"
  ]
  node [
    id 7
    label "nim"
    origin "text"
  ]
  node [
    id 8
    label "polski"
    origin "text"
  ]
  node [
    id 9
    label "ob&#243;z"
    origin "text"
  ]
  node [
    id 10
    label "zag&#322;ada"
    origin "text"
  ]
  node [
    id 11
    label "bohater"
    origin "text"
  ]
  node [
    id 12
    label "niemcy"
    origin "text"
  ]
  node [
    id 13
    label "dokument"
  ]
  node [
    id 14
    label "towar"
  ]
  node [
    id 15
    label "nag&#322;&#243;wek"
  ]
  node [
    id 16
    label "znak_j&#281;zykowy"
  ]
  node [
    id 17
    label "wyr&#243;b"
  ]
  node [
    id 18
    label "blok"
  ]
  node [
    id 19
    label "line"
  ]
  node [
    id 20
    label "paragraf"
  ]
  node [
    id 21
    label "rodzajnik"
  ]
  node [
    id 22
    label "prawda"
  ]
  node [
    id 23
    label "szkic"
  ]
  node [
    id 24
    label "tekst"
  ]
  node [
    id 25
    label "fragment"
  ]
  node [
    id 26
    label "fraza"
  ]
  node [
    id 27
    label "forma"
  ]
  node [
    id 28
    label "melodia"
  ]
  node [
    id 29
    label "rzecz"
  ]
  node [
    id 30
    label "zbacza&#263;"
  ]
  node [
    id 31
    label "entity"
  ]
  node [
    id 32
    label "omawia&#263;"
  ]
  node [
    id 33
    label "topik"
  ]
  node [
    id 34
    label "wyraz_pochodny"
  ]
  node [
    id 35
    label "om&#243;wi&#263;"
  ]
  node [
    id 36
    label "omawianie"
  ]
  node [
    id 37
    label "w&#261;tek"
  ]
  node [
    id 38
    label "forum"
  ]
  node [
    id 39
    label "cecha"
  ]
  node [
    id 40
    label "zboczenie"
  ]
  node [
    id 41
    label "zbaczanie"
  ]
  node [
    id 42
    label "tre&#347;&#263;"
  ]
  node [
    id 43
    label "tematyka"
  ]
  node [
    id 44
    label "sprawa"
  ]
  node [
    id 45
    label "istota"
  ]
  node [
    id 46
    label "otoczka"
  ]
  node [
    id 47
    label "zboczy&#263;"
  ]
  node [
    id 48
    label "om&#243;wienie"
  ]
  node [
    id 49
    label "rozbieg&#243;wka"
  ]
  node [
    id 50
    label "block"
  ]
  node [
    id 51
    label "blik"
  ]
  node [
    id 52
    label "odczula&#263;"
  ]
  node [
    id 53
    label "rola"
  ]
  node [
    id 54
    label "trawiarnia"
  ]
  node [
    id 55
    label "b&#322;ona"
  ]
  node [
    id 56
    label "filmoteka"
  ]
  node [
    id 57
    label "sztuka"
  ]
  node [
    id 58
    label "muza"
  ]
  node [
    id 59
    label "odczuli&#263;"
  ]
  node [
    id 60
    label "klatka"
  ]
  node [
    id 61
    label "odczulenie"
  ]
  node [
    id 62
    label "emulsja_fotograficzna"
  ]
  node [
    id 63
    label "animatronika"
  ]
  node [
    id 64
    label "dorobek"
  ]
  node [
    id 65
    label "odczulanie"
  ]
  node [
    id 66
    label "scena"
  ]
  node [
    id 67
    label "czo&#322;&#243;wka"
  ]
  node [
    id 68
    label "ty&#322;&#243;wka"
  ]
  node [
    id 69
    label "napisy"
  ]
  node [
    id 70
    label "photograph"
  ]
  node [
    id 71
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 72
    label "postprodukcja"
  ]
  node [
    id 73
    label "sklejarka"
  ]
  node [
    id 74
    label "anamorfoza"
  ]
  node [
    id 75
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 76
    label "ta&#347;ma"
  ]
  node [
    id 77
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 78
    label "uj&#281;cie"
  ]
  node [
    id 79
    label "wyliczanka"
  ]
  node [
    id 80
    label "catalog"
  ]
  node [
    id 81
    label "stock"
  ]
  node [
    id 82
    label "figurowa&#263;"
  ]
  node [
    id 83
    label "zbi&#243;r"
  ]
  node [
    id 84
    label "book"
  ]
  node [
    id 85
    label "pozycja"
  ]
  node [
    id 86
    label "sumariusz"
  ]
  node [
    id 87
    label "gra_planszowa"
  ]
  node [
    id 88
    label "lacki"
  ]
  node [
    id 89
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 90
    label "przedmiot"
  ]
  node [
    id 91
    label "sztajer"
  ]
  node [
    id 92
    label "drabant"
  ]
  node [
    id 93
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 94
    label "polak"
  ]
  node [
    id 95
    label "pierogi_ruskie"
  ]
  node [
    id 96
    label "krakowiak"
  ]
  node [
    id 97
    label "Polish"
  ]
  node [
    id 98
    label "j&#281;zyk"
  ]
  node [
    id 99
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 100
    label "oberek"
  ]
  node [
    id 101
    label "po_polsku"
  ]
  node [
    id 102
    label "mazur"
  ]
  node [
    id 103
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 104
    label "chodzony"
  ]
  node [
    id 105
    label "skoczny"
  ]
  node [
    id 106
    label "ryba_po_grecku"
  ]
  node [
    id 107
    label "goniony"
  ]
  node [
    id 108
    label "polsko"
  ]
  node [
    id 109
    label "zwi&#261;zek"
  ]
  node [
    id 110
    label "namiot"
  ]
  node [
    id 111
    label "ONZ"
  ]
  node [
    id 112
    label "grupa"
  ]
  node [
    id 113
    label "podob&#243;z"
  ]
  node [
    id 114
    label "odpoczynek"
  ]
  node [
    id 115
    label "alianci"
  ]
  node [
    id 116
    label "Paneuropa"
  ]
  node [
    id 117
    label "NATO"
  ]
  node [
    id 118
    label "miejsce_odosobnienia"
  ]
  node [
    id 119
    label "schronienie"
  ]
  node [
    id 120
    label "confederation"
  ]
  node [
    id 121
    label "obozowisko"
  ]
  node [
    id 122
    label "apokalipsa"
  ]
  node [
    id 123
    label "negacjonizm"
  ]
  node [
    id 124
    label "zniszczenie"
  ]
  node [
    id 125
    label "ludob&#243;jstwo"
  ]
  node [
    id 126
    label "pogr&#261;&#380;y&#263;"
  ]
  node [
    id 127
    label "szmalcownik"
  ]
  node [
    id 128
    label "holocaust"
  ]
  node [
    id 129
    label "kl&#281;ska_&#380;ywio&#322;owa"
  ]
  node [
    id 130
    label "pogr&#261;&#380;enie"
  ]
  node [
    id 131
    label "crash"
  ]
  node [
    id 132
    label "bohaterski"
  ]
  node [
    id 133
    label "cz&#322;owiek"
  ]
  node [
    id 134
    label "Zgredek"
  ]
  node [
    id 135
    label "Herkules"
  ]
  node [
    id 136
    label "Casanova"
  ]
  node [
    id 137
    label "Borewicz"
  ]
  node [
    id 138
    label "Don_Juan"
  ]
  node [
    id 139
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 140
    label "Winnetou"
  ]
  node [
    id 141
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 142
    label "Messi"
  ]
  node [
    id 143
    label "Herkules_Poirot"
  ]
  node [
    id 144
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 145
    label "Szwejk"
  ]
  node [
    id 146
    label "Sherlock_Holmes"
  ]
  node [
    id 147
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 148
    label "Hamlet"
  ]
  node [
    id 149
    label "Asterix"
  ]
  node [
    id 150
    label "Quasimodo"
  ]
  node [
    id 151
    label "Wallenrod"
  ]
  node [
    id 152
    label "Don_Kiszot"
  ]
  node [
    id 153
    label "uczestnik"
  ]
  node [
    id 154
    label "&#347;mia&#322;ek"
  ]
  node [
    id 155
    label "Harry_Potter"
  ]
  node [
    id 156
    label "podmiot"
  ]
  node [
    id 157
    label "Achilles"
  ]
  node [
    id 158
    label "Werter"
  ]
  node [
    id 159
    label "Mario"
  ]
  node [
    id 160
    label "posta&#263;"
  ]
  node [
    id 161
    label "Schindler"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
]
