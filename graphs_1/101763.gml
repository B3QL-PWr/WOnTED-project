graph [
  maxDegree 43
  minDegree 1
  meanDegree 2.206896551724138
  density 0.005449127288207748
  graphCliqueNumber 4
  node [
    id 0
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 1
    label "przeniesienie"
    origin "text"
  ]
  node [
    id 2
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nast&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "tak"
    origin "text"
  ]
  node [
    id 5
    label "jak"
    origin "text"
  ]
  node [
    id 6
    label "komputer"
    origin "text"
  ]
  node [
    id 7
    label "pewne"
    origin "text"
  ]
  node [
    id 8
    label "moment"
    origin "text"
  ]
  node [
    id 9
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 10
    label "znajda"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "mn&#243;stwo"
    origin "text"
  ]
  node [
    id 13
    label "rzecz"
    origin "text"
  ]
  node [
    id 14
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 15
    label "offline"
    origin "text"
  ]
  node [
    id 16
    label "nigdy"
    origin "text"
  ]
  node [
    id 17
    label "prosty"
    origin "text"
  ]
  node [
    id 18
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "u&#380;ywa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "dysk"
    origin "text"
  ]
  node [
    id 21
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 22
    label "teraz"
    origin "text"
  ]
  node [
    id 23
    label "pewnie"
    origin "text"
  ]
  node [
    id 24
    label "co&#347;"
    origin "text"
  ]
  node [
    id 25
    label "tam"
    origin "text"
  ]
  node [
    id 26
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 27
    label "korzysta&#263;"
    origin "text"
  ]
  node [
    id 28
    label "bramka"
    origin "text"
  ]
  node [
    id 29
    label "pocztowy"
    origin "text"
  ]
  node [
    id 30
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 31
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 32
    label "outlook&#243;w"
    origin "text"
  ]
  node [
    id 33
    label "itp"
    origin "text"
  ]
  node [
    id 34
    label "przegl&#261;darka"
    origin "text"
  ]
  node [
    id 35
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 36
    label "dla"
    origin "text"
  ]
  node [
    id 37
    label "zak&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 38
    label "podj&#281;cie"
    origin "text"
  ]
  node [
    id 39
    label "decyzja"
    origin "text"
  ]
  node [
    id 40
    label "zmiana"
    origin "text"
  ]
  node [
    id 41
    label "przej&#347;cie"
    origin "text"
  ]
  node [
    id 42
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 43
    label "system"
    origin "text"
  ]
  node [
    id 44
    label "operacyjny"
    origin "text"
  ]
  node [
    id 45
    label "przypadek"
    origin "text"
  ]
  node [
    id 46
    label "nawet"
    origin "text"
  ]
  node [
    id 47
    label "zauwa&#380;y&#263;"
    origin "text"
  ]
  node [
    id 48
    label "przemieszczenie"
  ]
  node [
    id 49
    label "poprzesuwanie"
  ]
  node [
    id 50
    label "zmienienie"
  ]
  node [
    id 51
    label "move"
  ]
  node [
    id 52
    label "rozpowszechnienie"
  ]
  node [
    id 53
    label "assignment"
  ]
  node [
    id 54
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 55
    label "przesadzenie"
  ]
  node [
    id 56
    label "transfer"
  ]
  node [
    id 57
    label "pocisk"
  ]
  node [
    id 58
    label "mechanizm_obronny"
  ]
  node [
    id 59
    label "przelecenie"
  ]
  node [
    id 60
    label "skopiowanie"
  ]
  node [
    id 61
    label "dostosowanie"
  ]
  node [
    id 62
    label "strzelenie"
  ]
  node [
    id 63
    label "umieszczenie"
  ]
  node [
    id 64
    label "hipertekst"
  ]
  node [
    id 65
    label "gauze"
  ]
  node [
    id 66
    label "nitka"
  ]
  node [
    id 67
    label "mesh"
  ]
  node [
    id 68
    label "e-hazard"
  ]
  node [
    id 69
    label "netbook"
  ]
  node [
    id 70
    label "cyberprzestrze&#324;"
  ]
  node [
    id 71
    label "biznes_elektroniczny"
  ]
  node [
    id 72
    label "snu&#263;"
  ]
  node [
    id 73
    label "organization"
  ]
  node [
    id 74
    label "zasadzka"
  ]
  node [
    id 75
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 76
    label "web"
  ]
  node [
    id 77
    label "provider"
  ]
  node [
    id 78
    label "struktura"
  ]
  node [
    id 79
    label "us&#322;uga_internetowa"
  ]
  node [
    id 80
    label "punkt_dost&#281;pu"
  ]
  node [
    id 81
    label "organizacja"
  ]
  node [
    id 82
    label "mem"
  ]
  node [
    id 83
    label "vane"
  ]
  node [
    id 84
    label "podcast"
  ]
  node [
    id 85
    label "grooming"
  ]
  node [
    id 86
    label "kszta&#322;t"
  ]
  node [
    id 87
    label "strona"
  ]
  node [
    id 88
    label "obiekt"
  ]
  node [
    id 89
    label "wysnu&#263;"
  ]
  node [
    id 90
    label "gra_sieciowa"
  ]
  node [
    id 91
    label "instalacja"
  ]
  node [
    id 92
    label "sie&#263;_komputerowa"
  ]
  node [
    id 93
    label "net"
  ]
  node [
    id 94
    label "plecionka"
  ]
  node [
    id 95
    label "media"
  ]
  node [
    id 96
    label "rozmieszczenie"
  ]
  node [
    id 97
    label "nacisn&#261;&#263;"
  ]
  node [
    id 98
    label "zaatakowa&#263;"
  ]
  node [
    id 99
    label "gamble"
  ]
  node [
    id 100
    label "supervene"
  ]
  node [
    id 101
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 102
    label "byd&#322;o"
  ]
  node [
    id 103
    label "zobo"
  ]
  node [
    id 104
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 105
    label "yakalo"
  ]
  node [
    id 106
    label "dzo"
  ]
  node [
    id 107
    label "pami&#281;&#263;"
  ]
  node [
    id 108
    label "urz&#261;dzenie"
  ]
  node [
    id 109
    label "maszyna_Turinga"
  ]
  node [
    id 110
    label "emulacja"
  ]
  node [
    id 111
    label "botnet"
  ]
  node [
    id 112
    label "moc_obliczeniowa"
  ]
  node [
    id 113
    label "stacja_dysk&#243;w"
  ]
  node [
    id 114
    label "monitor"
  ]
  node [
    id 115
    label "instalowanie"
  ]
  node [
    id 116
    label "karta"
  ]
  node [
    id 117
    label "instalowa&#263;"
  ]
  node [
    id 118
    label "elektroniczna_maszyna_licz&#261;ca"
  ]
  node [
    id 119
    label "mysz"
  ]
  node [
    id 120
    label "pad"
  ]
  node [
    id 121
    label "zainstalowanie"
  ]
  node [
    id 122
    label "twardy_dysk"
  ]
  node [
    id 123
    label "radiator"
  ]
  node [
    id 124
    label "modem"
  ]
  node [
    id 125
    label "zainstalowa&#263;"
  ]
  node [
    id 126
    label "klawiatura"
  ]
  node [
    id 127
    label "p&#322;yta_g&#322;&#243;wna"
  ]
  node [
    id 128
    label "procesor"
  ]
  node [
    id 129
    label "okres_czasu"
  ]
  node [
    id 130
    label "minute"
  ]
  node [
    id 131
    label "jednostka_geologiczna"
  ]
  node [
    id 132
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 133
    label "time"
  ]
  node [
    id 134
    label "chron"
  ]
  node [
    id 135
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 136
    label "fragment"
  ]
  node [
    id 137
    label "asymilowa&#263;"
  ]
  node [
    id 138
    label "wapniak"
  ]
  node [
    id 139
    label "dwun&#243;g"
  ]
  node [
    id 140
    label "polifag"
  ]
  node [
    id 141
    label "wz&#243;r"
  ]
  node [
    id 142
    label "profanum"
  ]
  node [
    id 143
    label "hominid"
  ]
  node [
    id 144
    label "homo_sapiens"
  ]
  node [
    id 145
    label "nasada"
  ]
  node [
    id 146
    label "podw&#322;adny"
  ]
  node [
    id 147
    label "ludzko&#347;&#263;"
  ]
  node [
    id 148
    label "os&#322;abianie"
  ]
  node [
    id 149
    label "mikrokosmos"
  ]
  node [
    id 150
    label "portrecista"
  ]
  node [
    id 151
    label "duch"
  ]
  node [
    id 152
    label "g&#322;owa"
  ]
  node [
    id 153
    label "oddzia&#322;ywanie"
  ]
  node [
    id 154
    label "asymilowanie"
  ]
  node [
    id 155
    label "osoba"
  ]
  node [
    id 156
    label "os&#322;abia&#263;"
  ]
  node [
    id 157
    label "figura"
  ]
  node [
    id 158
    label "Adam"
  ]
  node [
    id 159
    label "senior"
  ]
  node [
    id 160
    label "antropochoria"
  ]
  node [
    id 161
    label "posta&#263;"
  ]
  node [
    id 162
    label "podciep"
  ]
  node [
    id 163
    label "Logan"
  ]
  node [
    id 164
    label "dziecko"
  ]
  node [
    id 165
    label "zwierz&#281;"
  ]
  node [
    id 166
    label "enormousness"
  ]
  node [
    id 167
    label "ilo&#347;&#263;"
  ]
  node [
    id 168
    label "temat"
  ]
  node [
    id 169
    label "istota"
  ]
  node [
    id 170
    label "wpada&#263;"
  ]
  node [
    id 171
    label "wpa&#347;&#263;"
  ]
  node [
    id 172
    label "przedmiot"
  ]
  node [
    id 173
    label "wpadanie"
  ]
  node [
    id 174
    label "kultura"
  ]
  node [
    id 175
    label "przyroda"
  ]
  node [
    id 176
    label "mienie"
  ]
  node [
    id 177
    label "object"
  ]
  node [
    id 178
    label "wpadni&#281;cie"
  ]
  node [
    id 179
    label "proceed"
  ]
  node [
    id 180
    label "catch"
  ]
  node [
    id 181
    label "pozosta&#263;"
  ]
  node [
    id 182
    label "osta&#263;_si&#281;"
  ]
  node [
    id 183
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 184
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 185
    label "change"
  ]
  node [
    id 186
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 187
    label "kompletnie"
  ]
  node [
    id 188
    label "&#322;atwy"
  ]
  node [
    id 189
    label "prostowanie_si&#281;"
  ]
  node [
    id 190
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 191
    label "rozprostowanie"
  ]
  node [
    id 192
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 193
    label "prostoduszny"
  ]
  node [
    id 194
    label "naturalny"
  ]
  node [
    id 195
    label "naiwny"
  ]
  node [
    id 196
    label "cios"
  ]
  node [
    id 197
    label "prostowanie"
  ]
  node [
    id 198
    label "niepozorny"
  ]
  node [
    id 199
    label "zwyk&#322;y"
  ]
  node [
    id 200
    label "prosto"
  ]
  node [
    id 201
    label "po_prostu"
  ]
  node [
    id 202
    label "skromny"
  ]
  node [
    id 203
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 204
    label "cause"
  ]
  node [
    id 205
    label "introduce"
  ]
  node [
    id 206
    label "begin"
  ]
  node [
    id 207
    label "odj&#261;&#263;"
  ]
  node [
    id 208
    label "post&#261;pi&#263;"
  ]
  node [
    id 209
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 210
    label "do"
  ]
  node [
    id 211
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 212
    label "zrobi&#263;"
  ]
  node [
    id 213
    label "bash"
  ]
  node [
    id 214
    label "distribute"
  ]
  node [
    id 215
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 216
    label "give"
  ]
  node [
    id 217
    label "doznawa&#263;"
  ]
  node [
    id 218
    label "pier&#347;cie&#324;_w&#322;&#243;knisty"
  ]
  node [
    id 219
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 220
    label "p&#322;yta"
  ]
  node [
    id 221
    label "hard_disc"
  ]
  node [
    id 222
    label "j&#261;dro_mia&#380;d&#380;yste"
  ]
  node [
    id 223
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 224
    label "chrz&#261;stka"
  ]
  node [
    id 225
    label "ko&#322;o"
  ]
  node [
    id 226
    label "klaster_dyskowy"
  ]
  node [
    id 227
    label "chwila"
  ]
  node [
    id 228
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 229
    label "zwinnie"
  ]
  node [
    id 230
    label "bezpiecznie"
  ]
  node [
    id 231
    label "wiarygodnie"
  ]
  node [
    id 232
    label "pewniej"
  ]
  node [
    id 233
    label "pewny"
  ]
  node [
    id 234
    label "mocno"
  ]
  node [
    id 235
    label "najpewniej"
  ]
  node [
    id 236
    label "thing"
  ]
  node [
    id 237
    label "cosik"
  ]
  node [
    id 238
    label "tu"
  ]
  node [
    id 239
    label "jaki&#347;"
  ]
  node [
    id 240
    label "use"
  ]
  node [
    id 241
    label "uzyskiwa&#263;"
  ]
  node [
    id 242
    label "obstawi&#263;"
  ]
  node [
    id 243
    label "zamek"
  ]
  node [
    id 244
    label "p&#322;ot"
  ]
  node [
    id 245
    label "obstawienie"
  ]
  node [
    id 246
    label "goal"
  ]
  node [
    id 247
    label "trafienie"
  ]
  node [
    id 248
    label "siatka"
  ]
  node [
    id 249
    label "poprzeczka"
  ]
  node [
    id 250
    label "zawiasy"
  ]
  node [
    id 251
    label "obstawia&#263;"
  ]
  node [
    id 252
    label "wej&#347;cie"
  ]
  node [
    id 253
    label "brama"
  ]
  node [
    id 254
    label "s&#322;upek"
  ]
  node [
    id 255
    label "obstawianie"
  ]
  node [
    id 256
    label "boisko"
  ]
  node [
    id 257
    label "ogrodzenie"
  ]
  node [
    id 258
    label "przeszkoda"
  ]
  node [
    id 259
    label "postal"
  ]
  node [
    id 260
    label "majority"
  ]
  node [
    id 261
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 262
    label "viewer"
  ]
  node [
    id 263
    label "przyrz&#261;d"
  ]
  node [
    id 264
    label "program"
  ]
  node [
    id 265
    label "browser"
  ]
  node [
    id 266
    label "projektor"
  ]
  node [
    id 267
    label "volunteer"
  ]
  node [
    id 268
    label "podwija&#263;"
  ]
  node [
    id 269
    label "make_bold"
  ]
  node [
    id 270
    label "umieszcza&#263;"
  ]
  node [
    id 271
    label "supply"
  ]
  node [
    id 272
    label "set"
  ]
  node [
    id 273
    label "ubiera&#263;"
  ]
  node [
    id 274
    label "invest"
  ]
  node [
    id 275
    label "p&#322;aci&#263;"
  ]
  node [
    id 276
    label "przewidywa&#263;"
  ]
  node [
    id 277
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 278
    label "obleka&#263;"
  ]
  node [
    id 279
    label "pokrywa&#263;"
  ]
  node [
    id 280
    label "odziewa&#263;"
  ]
  node [
    id 281
    label "organizowa&#263;"
  ]
  node [
    id 282
    label "wk&#322;ada&#263;"
  ]
  node [
    id 283
    label "robi&#263;"
  ]
  node [
    id 284
    label "nosi&#263;"
  ]
  node [
    id 285
    label "obleka&#263;_si&#281;"
  ]
  node [
    id 286
    label "powodowa&#263;"
  ]
  node [
    id 287
    label "zrobienie"
  ]
  node [
    id 288
    label "consumption"
  ]
  node [
    id 289
    label "czynno&#347;&#263;"
  ]
  node [
    id 290
    label "spowodowanie"
  ]
  node [
    id 291
    label "zareagowanie"
  ]
  node [
    id 292
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 293
    label "erecting"
  ]
  node [
    id 294
    label "movement"
  ]
  node [
    id 295
    label "entertainment"
  ]
  node [
    id 296
    label "zacz&#281;cie"
  ]
  node [
    id 297
    label "dokument"
  ]
  node [
    id 298
    label "resolution"
  ]
  node [
    id 299
    label "zdecydowanie"
  ]
  node [
    id 300
    label "wytw&#243;r"
  ]
  node [
    id 301
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 302
    label "management"
  ]
  node [
    id 303
    label "anatomopatolog"
  ]
  node [
    id 304
    label "rewizja"
  ]
  node [
    id 305
    label "oznaka"
  ]
  node [
    id 306
    label "czas"
  ]
  node [
    id 307
    label "ferment"
  ]
  node [
    id 308
    label "komplet"
  ]
  node [
    id 309
    label "tura"
  ]
  node [
    id 310
    label "amendment"
  ]
  node [
    id 311
    label "zmianka"
  ]
  node [
    id 312
    label "odmienianie"
  ]
  node [
    id 313
    label "passage"
  ]
  node [
    id 314
    label "zjawisko"
  ]
  node [
    id 315
    label "praca"
  ]
  node [
    id 316
    label "nale&#380;enie"
  ]
  node [
    id 317
    label "odmienienie"
  ]
  node [
    id 318
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 319
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 320
    label "mini&#281;cie"
  ]
  node [
    id 321
    label "prze&#380;ycie"
  ]
  node [
    id 322
    label "strain"
  ]
  node [
    id 323
    label "przerobienie"
  ]
  node [
    id 324
    label "stanie_si&#281;"
  ]
  node [
    id 325
    label "dostanie_si&#281;"
  ]
  node [
    id 326
    label "wydeptanie"
  ]
  node [
    id 327
    label "wydeptywanie"
  ]
  node [
    id 328
    label "offense"
  ]
  node [
    id 329
    label "wymienienie"
  ]
  node [
    id 330
    label "trwanie"
  ]
  node [
    id 331
    label "przepojenie"
  ]
  node [
    id 332
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 333
    label "zaliczenie"
  ]
  node [
    id 334
    label "zdarzenie_si&#281;"
  ]
  node [
    id 335
    label "uznanie"
  ]
  node [
    id 336
    label "nasycenie_si&#281;"
  ]
  node [
    id 337
    label "przemokni&#281;cie"
  ]
  node [
    id 338
    label "nas&#261;czenie"
  ]
  node [
    id 339
    label "ustawa"
  ]
  node [
    id 340
    label "experience"
  ]
  node [
    id 341
    label "przewy&#380;szenie"
  ]
  node [
    id 342
    label "miejsce"
  ]
  node [
    id 343
    label "faza"
  ]
  node [
    id 344
    label "doznanie"
  ]
  node [
    id 345
    label "przestanie"
  ]
  node [
    id 346
    label "traversal"
  ]
  node [
    id 347
    label "przebycie"
  ]
  node [
    id 348
    label "przedostanie_si&#281;"
  ]
  node [
    id 349
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 350
    label "wstawka"
  ]
  node [
    id 351
    label "przepuszczenie"
  ]
  node [
    id 352
    label "wytyczenie"
  ]
  node [
    id 353
    label "crack"
  ]
  node [
    id 354
    label "model"
  ]
  node [
    id 355
    label "sk&#322;ad"
  ]
  node [
    id 356
    label "zachowanie"
  ]
  node [
    id 357
    label "podstawa"
  ]
  node [
    id 358
    label "porz&#261;dek"
  ]
  node [
    id 359
    label "Android"
  ]
  node [
    id 360
    label "przyn&#281;ta"
  ]
  node [
    id 361
    label "metoda"
  ]
  node [
    id 362
    label "podsystem"
  ]
  node [
    id 363
    label "p&#322;&#243;d"
  ]
  node [
    id 364
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 365
    label "s&#261;d"
  ]
  node [
    id 366
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 367
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 368
    label "j&#261;dro"
  ]
  node [
    id 369
    label "eratem"
  ]
  node [
    id 370
    label "ryba"
  ]
  node [
    id 371
    label "pulpit"
  ]
  node [
    id 372
    label "spos&#243;b"
  ]
  node [
    id 373
    label "oddzia&#322;"
  ]
  node [
    id 374
    label "usenet"
  ]
  node [
    id 375
    label "o&#347;"
  ]
  node [
    id 376
    label "oprogramowanie"
  ]
  node [
    id 377
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 378
    label "poj&#281;cie"
  ]
  node [
    id 379
    label "w&#281;dkarstwo"
  ]
  node [
    id 380
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 381
    label "Leopard"
  ]
  node [
    id 382
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 383
    label "systemik"
  ]
  node [
    id 384
    label "rozprz&#261;c"
  ]
  node [
    id 385
    label "cybernetyk"
  ]
  node [
    id 386
    label "konstelacja"
  ]
  node [
    id 387
    label "doktryna"
  ]
  node [
    id 388
    label "zbi&#243;r"
  ]
  node [
    id 389
    label "method"
  ]
  node [
    id 390
    label "systemat"
  ]
  node [
    id 391
    label "mo&#380;liwy"
  ]
  node [
    id 392
    label "medyczny"
  ]
  node [
    id 393
    label "operacyjnie"
  ]
  node [
    id 394
    label "pacjent"
  ]
  node [
    id 395
    label "kategoria_gramatyczna"
  ]
  node [
    id 396
    label "schorzenie"
  ]
  node [
    id 397
    label "przeznaczenie"
  ]
  node [
    id 398
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 399
    label "wydarzenie"
  ]
  node [
    id 400
    label "happening"
  ]
  node [
    id 401
    label "przyk&#322;ad"
  ]
  node [
    id 402
    label "zobaczy&#263;"
  ]
  node [
    id 403
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 404
    label "notice"
  ]
  node [
    id 405
    label "cognizance"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 13
    target 177
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 201
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 207
  ]
  edge [
    source 18
    target 208
  ]
  edge [
    source 18
    target 209
  ]
  edge [
    source 18
    target 210
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 107
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 227
  ]
  edge [
    source 22
    target 228
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 229
  ]
  edge [
    source 23
    target 230
  ]
  edge [
    source 23
    target 231
  ]
  edge [
    source 23
    target 232
  ]
  edge [
    source 23
    target 233
  ]
  edge [
    source 23
    target 234
  ]
  edge [
    source 23
    target 235
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 40
  ]
  edge [
    source 24
    target 236
  ]
  edge [
    source 24
    target 237
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 238
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 239
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 27
    target 240
  ]
  edge [
    source 27
    target 241
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 172
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 223
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 259
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 45
  ]
  edge [
    source 31
    target 260
  ]
  edge [
    source 31
    target 261
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 262
  ]
  edge [
    source 34
    target 263
  ]
  edge [
    source 34
    target 264
  ]
  edge [
    source 34
    target 265
  ]
  edge [
    source 34
    target 266
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 267
  ]
  edge [
    source 37
    target 268
  ]
  edge [
    source 37
    target 269
  ]
  edge [
    source 37
    target 270
  ]
  edge [
    source 37
    target 271
  ]
  edge [
    source 37
    target 272
  ]
  edge [
    source 37
    target 273
  ]
  edge [
    source 37
    target 274
  ]
  edge [
    source 37
    target 275
  ]
  edge [
    source 37
    target 276
  ]
  edge [
    source 37
    target 277
  ]
  edge [
    source 37
    target 278
  ]
  edge [
    source 37
    target 279
  ]
  edge [
    source 37
    target 280
  ]
  edge [
    source 37
    target 281
  ]
  edge [
    source 37
    target 282
  ]
  edge [
    source 37
    target 283
  ]
  edge [
    source 37
    target 284
  ]
  edge [
    source 37
    target 205
  ]
  edge [
    source 37
    target 285
  ]
  edge [
    source 37
    target 286
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 287
  ]
  edge [
    source 38
    target 288
  ]
  edge [
    source 38
    target 289
  ]
  edge [
    source 38
    target 290
  ]
  edge [
    source 38
    target 291
  ]
  edge [
    source 38
    target 292
  ]
  edge [
    source 38
    target 293
  ]
  edge [
    source 38
    target 294
  ]
  edge [
    source 38
    target 295
  ]
  edge [
    source 38
    target 296
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 297
  ]
  edge [
    source 39
    target 298
  ]
  edge [
    source 39
    target 299
  ]
  edge [
    source 39
    target 300
  ]
  edge [
    source 39
    target 301
  ]
  edge [
    source 39
    target 302
  ]
  edge [
    source 40
    target 303
  ]
  edge [
    source 40
    target 304
  ]
  edge [
    source 40
    target 305
  ]
  edge [
    source 40
    target 306
  ]
  edge [
    source 40
    target 307
  ]
  edge [
    source 40
    target 308
  ]
  edge [
    source 40
    target 309
  ]
  edge [
    source 40
    target 310
  ]
  edge [
    source 40
    target 311
  ]
  edge [
    source 40
    target 312
  ]
  edge [
    source 40
    target 313
  ]
  edge [
    source 40
    target 314
  ]
  edge [
    source 40
    target 185
  ]
  edge [
    source 40
    target 315
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 316
  ]
  edge [
    source 41
    target 317
  ]
  edge [
    source 41
    target 318
  ]
  edge [
    source 41
    target 319
  ]
  edge [
    source 41
    target 320
  ]
  edge [
    source 41
    target 321
  ]
  edge [
    source 41
    target 322
  ]
  edge [
    source 41
    target 323
  ]
  edge [
    source 41
    target 324
  ]
  edge [
    source 41
    target 325
  ]
  edge [
    source 41
    target 326
  ]
  edge [
    source 41
    target 327
  ]
  edge [
    source 41
    target 328
  ]
  edge [
    source 41
    target 329
  ]
  edge [
    source 41
    target 296
  ]
  edge [
    source 41
    target 330
  ]
  edge [
    source 41
    target 331
  ]
  edge [
    source 41
    target 332
  ]
  edge [
    source 41
    target 333
  ]
  edge [
    source 41
    target 334
  ]
  edge [
    source 41
    target 335
  ]
  edge [
    source 41
    target 336
  ]
  edge [
    source 41
    target 337
  ]
  edge [
    source 41
    target 338
  ]
  edge [
    source 41
    target 176
  ]
  edge [
    source 41
    target 339
  ]
  edge [
    source 41
    target 340
  ]
  edge [
    source 41
    target 341
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 41
    target 343
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 345
  ]
  edge [
    source 41
    target 346
  ]
  edge [
    source 41
    target 347
  ]
  edge [
    source 41
    target 348
  ]
  edge [
    source 41
    target 349
  ]
  edge [
    source 41
    target 350
  ]
  edge [
    source 41
    target 351
  ]
  edge [
    source 41
    target 352
  ]
  edge [
    source 41
    target 353
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 354
  ]
  edge [
    source 43
    target 355
  ]
  edge [
    source 43
    target 356
  ]
  edge [
    source 43
    target 357
  ]
  edge [
    source 43
    target 358
  ]
  edge [
    source 43
    target 359
  ]
  edge [
    source 43
    target 360
  ]
  edge [
    source 43
    target 131
  ]
  edge [
    source 43
    target 361
  ]
  edge [
    source 43
    target 362
  ]
  edge [
    source 43
    target 363
  ]
  edge [
    source 43
    target 364
  ]
  edge [
    source 43
    target 365
  ]
  edge [
    source 43
    target 366
  ]
  edge [
    source 43
    target 367
  ]
  edge [
    source 43
    target 75
  ]
  edge [
    source 43
    target 368
  ]
  edge [
    source 43
    target 369
  ]
  edge [
    source 43
    target 370
  ]
  edge [
    source 43
    target 371
  ]
  edge [
    source 43
    target 78
  ]
  edge [
    source 43
    target 372
  ]
  edge [
    source 43
    target 373
  ]
  edge [
    source 43
    target 374
  ]
  edge [
    source 43
    target 375
  ]
  edge [
    source 43
    target 376
  ]
  edge [
    source 43
    target 377
  ]
  edge [
    source 43
    target 378
  ]
  edge [
    source 43
    target 379
  ]
  edge [
    source 43
    target 380
  ]
  edge [
    source 43
    target 381
  ]
  edge [
    source 43
    target 382
  ]
  edge [
    source 43
    target 383
  ]
  edge [
    source 43
    target 384
  ]
  edge [
    source 43
    target 385
  ]
  edge [
    source 43
    target 386
  ]
  edge [
    source 43
    target 387
  ]
  edge [
    source 43
    target 93
  ]
  edge [
    source 43
    target 388
  ]
  edge [
    source 43
    target 389
  ]
  edge [
    source 43
    target 390
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 391
  ]
  edge [
    source 44
    target 392
  ]
  edge [
    source 44
    target 393
  ]
  edge [
    source 45
    target 394
  ]
  edge [
    source 45
    target 395
  ]
  edge [
    source 45
    target 396
  ]
  edge [
    source 45
    target 397
  ]
  edge [
    source 45
    target 398
  ]
  edge [
    source 45
    target 399
  ]
  edge [
    source 45
    target 400
  ]
  edge [
    source 45
    target 401
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 402
  ]
  edge [
    source 47
    target 403
  ]
  edge [
    source 47
    target 404
  ]
  edge [
    source 47
    target 405
  ]
]
