graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.015748031496063
  density 0.015998000249968752
  graphCliqueNumber 2
  node [
    id 0
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 1
    label "okazja"
    origin "text"
  ]
  node [
    id 2
    label "zbli&#380;a&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "miko&#322;ajek"
    origin "text"
  ]
  node [
    id 5
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 7
    label "rozdajo"
    origin "text"
  ]
  node [
    id 8
    label "sklepik"
    origin "text"
  ]
  node [
    id 9
    label "http"
    origin "text"
  ]
  node [
    id 10
    label "ten"
    origin "text"
  ]
  node [
    id 11
    label "wpis"
    origin "text"
  ]
  node [
    id 12
    label "wylosowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "mirkolos"
    origin "text"
  ]
  node [
    id 14
    label "jeden"
    origin "text"
  ]
  node [
    id 15
    label "osoba"
    origin "text"
  ]
  node [
    id 16
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 17
    label "siebie"
    origin "text"
  ]
  node [
    id 18
    label "koszulka"
    origin "text"
  ]
  node [
    id 19
    label "shirty"
    origin "text"
  ]
  node [
    id 20
    label "com"
    origin "text"
  ]
  node [
    id 21
    label "klubowe"
    origin "text"
  ]
  node [
    id 22
    label "pozdrawia&#263;"
  ]
  node [
    id 23
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 24
    label "greet"
  ]
  node [
    id 25
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 26
    label "welcome"
  ]
  node [
    id 27
    label "atrakcyjny"
  ]
  node [
    id 28
    label "oferta"
  ]
  node [
    id 29
    label "adeptness"
  ]
  node [
    id 30
    label "okazka"
  ]
  node [
    id 31
    label "wydarzenie"
  ]
  node [
    id 32
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 33
    label "podw&#243;zka"
  ]
  node [
    id 34
    label "autostop"
  ]
  node [
    id 35
    label "sytuacja"
  ]
  node [
    id 36
    label "przemieszcza&#263;"
  ]
  node [
    id 37
    label "set_about"
  ]
  node [
    id 38
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 39
    label "bylina"
  ]
  node [
    id 40
    label "selerowate"
  ]
  node [
    id 41
    label "tentegowa&#263;"
  ]
  node [
    id 42
    label "urz&#261;dza&#263;"
  ]
  node [
    id 43
    label "give"
  ]
  node [
    id 44
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 45
    label "czyni&#263;"
  ]
  node [
    id 46
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 47
    label "post&#281;powa&#263;"
  ]
  node [
    id 48
    label "wydala&#263;"
  ]
  node [
    id 49
    label "oszukiwa&#263;"
  ]
  node [
    id 50
    label "organizowa&#263;"
  ]
  node [
    id 51
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 52
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 53
    label "work"
  ]
  node [
    id 54
    label "przerabia&#263;"
  ]
  node [
    id 55
    label "stylizowa&#263;"
  ]
  node [
    id 56
    label "falowa&#263;"
  ]
  node [
    id 57
    label "act"
  ]
  node [
    id 58
    label "peddle"
  ]
  node [
    id 59
    label "ukazywa&#263;"
  ]
  node [
    id 60
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 61
    label "praca"
  ]
  node [
    id 62
    label "dziewczynka"
  ]
  node [
    id 63
    label "dziewczyna"
  ]
  node [
    id 64
    label "sklep"
  ]
  node [
    id 65
    label "okre&#347;lony"
  ]
  node [
    id 66
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 67
    label "czynno&#347;&#263;"
  ]
  node [
    id 68
    label "entrance"
  ]
  node [
    id 69
    label "inscription"
  ]
  node [
    id 70
    label "akt"
  ]
  node [
    id 71
    label "op&#322;ata"
  ]
  node [
    id 72
    label "tekst"
  ]
  node [
    id 73
    label "wybra&#263;"
  ]
  node [
    id 74
    label "wygra&#263;"
  ]
  node [
    id 75
    label "kieliszek"
  ]
  node [
    id 76
    label "shot"
  ]
  node [
    id 77
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 78
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 79
    label "jaki&#347;"
  ]
  node [
    id 80
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 81
    label "jednolicie"
  ]
  node [
    id 82
    label "w&#243;dka"
  ]
  node [
    id 83
    label "ujednolicenie"
  ]
  node [
    id 84
    label "jednakowy"
  ]
  node [
    id 85
    label "Zgredek"
  ]
  node [
    id 86
    label "kategoria_gramatyczna"
  ]
  node [
    id 87
    label "Casanova"
  ]
  node [
    id 88
    label "Don_Juan"
  ]
  node [
    id 89
    label "Gargantua"
  ]
  node [
    id 90
    label "Faust"
  ]
  node [
    id 91
    label "profanum"
  ]
  node [
    id 92
    label "Chocho&#322;"
  ]
  node [
    id 93
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 94
    label "koniugacja"
  ]
  node [
    id 95
    label "Winnetou"
  ]
  node [
    id 96
    label "Dwukwiat"
  ]
  node [
    id 97
    label "homo_sapiens"
  ]
  node [
    id 98
    label "Edyp"
  ]
  node [
    id 99
    label "Herkules_Poirot"
  ]
  node [
    id 100
    label "ludzko&#347;&#263;"
  ]
  node [
    id 101
    label "mikrokosmos"
  ]
  node [
    id 102
    label "person"
  ]
  node [
    id 103
    label "Sherlock_Holmes"
  ]
  node [
    id 104
    label "portrecista"
  ]
  node [
    id 105
    label "Szwejk"
  ]
  node [
    id 106
    label "Hamlet"
  ]
  node [
    id 107
    label "duch"
  ]
  node [
    id 108
    label "g&#322;owa"
  ]
  node [
    id 109
    label "oddzia&#322;ywanie"
  ]
  node [
    id 110
    label "Quasimodo"
  ]
  node [
    id 111
    label "Dulcynea"
  ]
  node [
    id 112
    label "Don_Kiszot"
  ]
  node [
    id 113
    label "Wallenrod"
  ]
  node [
    id 114
    label "Plastu&#347;"
  ]
  node [
    id 115
    label "Harry_Potter"
  ]
  node [
    id 116
    label "figura"
  ]
  node [
    id 117
    label "parali&#380;owa&#263;"
  ]
  node [
    id 118
    label "istota"
  ]
  node [
    id 119
    label "Werter"
  ]
  node [
    id 120
    label "antropochoria"
  ]
  node [
    id 121
    label "posta&#263;"
  ]
  node [
    id 122
    label "ubranko"
  ]
  node [
    id 123
    label "os&#322;ona"
  ]
  node [
    id 124
    label "g&#243;ra"
  ]
  node [
    id 125
    label "artyku&#322;"
  ]
  node [
    id 126
    label "niemowl&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 75
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 14
    target 78
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 85
  ]
  edge [
    source 15
    target 86
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
]
