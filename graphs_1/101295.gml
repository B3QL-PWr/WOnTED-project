graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.3846153846153846
  density 0.11538461538461539
  graphCliqueNumber 2
  node [
    id 0
    label "marina"
    origin "text"
  ]
  node [
    id 1
    label "perani"
    origin "text"
  ]
  node [
    id 2
    label "obraz"
  ]
  node [
    id 3
    label "przysta&#324;"
  ]
  node [
    id 4
    label "morze"
  ]
  node [
    id 5
    label "dzie&#322;o"
  ]
  node [
    id 6
    label "Perani"
  ]
  node [
    id 7
    label "Ponte"
  ]
  node [
    id 8
    label "Nossa"
  ]
  node [
    id 9
    label "Bologna"
  ]
  node [
    id 10
    label "FC"
  ]
  node [
    id 11
    label "mistrzostwo"
  ]
  node [
    id 12
    label "&#347;wiat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
]
