graph [
  maxDegree 33
  minDegree 1
  meanDegree 1.9761904761904763
  density 0.023809523809523808
  graphCliqueNumber 2
  node [
    id 0
    label "wiatr"
    origin "text"
  ]
  node [
    id 1
    label "po&#322;&#261;czenie"
    origin "text"
  ]
  node [
    id 2
    label "l&#243;d"
    origin "text"
  ]
  node [
    id 3
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wygrana"
    origin "text"
  ]
  node [
    id 5
    label "wietrzno&#347;&#263;"
  ]
  node [
    id 6
    label "skala_Beauforta"
  ]
  node [
    id 7
    label "porywisto&#347;&#263;"
  ]
  node [
    id 8
    label "powia&#263;"
  ]
  node [
    id 9
    label "powianie"
  ]
  node [
    id 10
    label "powietrze"
  ]
  node [
    id 11
    label "zjawisko"
  ]
  node [
    id 12
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 13
    label "mention"
  ]
  node [
    id 14
    label "zestawienie"
  ]
  node [
    id 15
    label "zgrzeina"
  ]
  node [
    id 16
    label "coalescence"
  ]
  node [
    id 17
    label "element"
  ]
  node [
    id 18
    label "rzucenie"
  ]
  node [
    id 19
    label "komunikacja"
  ]
  node [
    id 20
    label "akt_p&#322;ciowy"
  ]
  node [
    id 21
    label "umo&#380;liwienie"
  ]
  node [
    id 22
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 23
    label "phreaker"
  ]
  node [
    id 24
    label "pomy&#347;lenie"
  ]
  node [
    id 25
    label "zjednoczenie"
  ]
  node [
    id 26
    label "kontakt"
  ]
  node [
    id 27
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 28
    label "czynno&#347;&#263;"
  ]
  node [
    id 29
    label "stworzenie"
  ]
  node [
    id 30
    label "dressing"
  ]
  node [
    id 31
    label "zwi&#261;zany"
  ]
  node [
    id 32
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 33
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 34
    label "spowodowanie"
  ]
  node [
    id 35
    label "zespolenie"
  ]
  node [
    id 36
    label "billing"
  ]
  node [
    id 37
    label "port"
  ]
  node [
    id 38
    label "alliance"
  ]
  node [
    id 39
    label "joining"
  ]
  node [
    id 40
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 41
    label "lody"
  ]
  node [
    id 42
    label "g&#322;ad&#378;"
  ]
  node [
    id 43
    label "zlodowacenie"
  ]
  node [
    id 44
    label "woda"
  ]
  node [
    id 45
    label "cia&#322;o_sta&#322;e"
  ]
  node [
    id 46
    label "kostkarka"
  ]
  node [
    id 47
    label "lodowacenie"
  ]
  node [
    id 48
    label "render"
  ]
  node [
    id 49
    label "hold"
  ]
  node [
    id 50
    label "surrender"
  ]
  node [
    id 51
    label "traktowa&#263;"
  ]
  node [
    id 52
    label "dostarcza&#263;"
  ]
  node [
    id 53
    label "tender"
  ]
  node [
    id 54
    label "train"
  ]
  node [
    id 55
    label "give"
  ]
  node [
    id 56
    label "umieszcza&#263;"
  ]
  node [
    id 57
    label "nalewa&#263;"
  ]
  node [
    id 58
    label "przeznacza&#263;"
  ]
  node [
    id 59
    label "p&#322;aci&#263;"
  ]
  node [
    id 60
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 61
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 62
    label "powierza&#263;"
  ]
  node [
    id 63
    label "hold_out"
  ]
  node [
    id 64
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 65
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 66
    label "mie&#263;_miejsce"
  ]
  node [
    id 67
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 68
    label "robi&#263;"
  ]
  node [
    id 69
    label "t&#322;uc"
  ]
  node [
    id 70
    label "wpiernicza&#263;"
  ]
  node [
    id 71
    label "przekazywa&#263;"
  ]
  node [
    id 72
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 73
    label "zezwala&#263;"
  ]
  node [
    id 74
    label "rap"
  ]
  node [
    id 75
    label "obiecywa&#263;"
  ]
  node [
    id 76
    label "&#322;adowa&#263;"
  ]
  node [
    id 77
    label "odst&#281;powa&#263;"
  ]
  node [
    id 78
    label "exsert"
  ]
  node [
    id 79
    label "puchar"
  ]
  node [
    id 80
    label "korzy&#347;&#263;"
  ]
  node [
    id 81
    label "sukces"
  ]
  node [
    id 82
    label "przedmiot"
  ]
  node [
    id 83
    label "conquest"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
]
