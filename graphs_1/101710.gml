graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.2512820512820513
  density 0.005787357458308615
  graphCliqueNumber 4
  node [
    id 0
    label "rzecz"
    origin "text"
  ]
  node [
    id 1
    label "sam"
    origin "text"
  ]
  node [
    id 2
    label "disneyowska"
    origin "text"
  ]
  node [
    id 3
    label "interpretacja"
    origin "text"
  ]
  node [
    id 4
    label "proza"
    origin "text"
  ]
  node [
    id 5
    label "milne"
    origin "text"
  ]
  node [
    id 6
    label "pora&#380;a&#263;"
    origin "text"
  ]
  node [
    id 7
    label "intelektualny"
    origin "text"
  ]
  node [
    id 8
    label "p&#322;ycizna"
    origin "text"
  ]
  node [
    id 9
    label "siebie"
    origin "text"
  ]
  node [
    id 10
    label "by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "niebezpieczny"
    origin "text"
  ]
  node [
    id 12
    label "mozart"
    origin "text"
  ]
  node [
    id 13
    label "te&#380;"
    origin "text"
  ]
  node [
    id 14
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 15
    label "zagra&#263;"
    origin "text"
  ]
  node [
    id 16
    label "dobrze"
    origin "text"
  ]
  node [
    id 17
    label "b&#261;d&#378;"
    origin "text"
  ]
  node [
    id 18
    label "&#378;le"
    origin "text"
  ]
  node [
    id 19
    label "staj"
    origin "text"
  ]
  node [
    id 20
    label "si&#281;"
    origin "text"
  ]
  node [
    id 21
    label "dopiero"
    origin "text"
  ]
  node [
    id 22
    label "wtedy"
    origin "text"
  ]
  node [
    id 23
    label "gdy"
    origin "text"
  ]
  node [
    id 24
    label "taka"
    origin "text"
  ]
  node [
    id 25
    label "jedyna"
    origin "text"
  ]
  node [
    id 26
    label "dost&#281;pny"
    origin "text"
  ]
  node [
    id 27
    label "lato"
    origin "text"
  ]
  node [
    id 28
    label "kubu&#347;"
    origin "text"
  ]
  node [
    id 29
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 30
    label "scena"
    origin "text"
  ]
  node [
    id 31
    label "ponownie"
    origin "text"
  ]
  node [
    id 32
    label "lukrowa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "zglajszachtowanym"
    origin "text"
  ]
  node [
    id 34
    label "disneyowskim"
    origin "text"
  ]
  node [
    id 35
    label "musical"
    origin "text"
  ]
  node [
    id 36
    label "tani"
    origin "text"
  ]
  node [
    id 37
    label "bilet"
    origin "text"
  ]
  node [
    id 38
    label "kosztowa&#263;"
    origin "text"
  ]
  node [
    id 39
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 40
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 41
    label "tylko"
    origin "text"
  ]
  node [
    id 42
    label "spekatkle"
    origin "text"
  ]
  node [
    id 43
    label "wielki"
    origin "text"
  ]
  node [
    id 44
    label "wydarzenie"
    origin "text"
  ]
  node [
    id 45
    label "finansowy"
    origin "text"
  ]
  node [
    id 46
    label "marketingowy"
    origin "text"
  ]
  node [
    id 47
    label "trudno"
    origin "text"
  ]
  node [
    id 48
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 49
    label "wydarzenia"
    origin "text"
  ]
  node [
    id 50
    label "mai&#263;"
    origin "text"
  ]
  node [
    id 51
    label "monopol"
    origin "text"
  ]
  node [
    id 52
    label "nak&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 53
    label "knebel"
    origin "text"
  ]
  node [
    id 54
    label "tw&#243;rczo&#347;&#263;"
    origin "text"
  ]
  node [
    id 55
    label "niezale&#380;ny"
    origin "text"
  ]
  node [
    id 56
    label "tym"
    origin "text"
  ]
  node [
    id 57
    label "razem"
    origin "text"
  ]
  node [
    id 58
    label "sukces"
    origin "text"
  ]
  node [
    id 59
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 60
    label "okaza&#263;"
    origin "text"
  ]
  node [
    id 61
    label "dla"
    origin "text"
  ]
  node [
    id 62
    label "disney"
    origin "text"
  ]
  node [
    id 63
    label "gorzki"
    origin "text"
  ]
  node [
    id 64
    label "wielbiciel"
    origin "text"
  ]
  node [
    id 65
    label "teatr"
    origin "text"
  ]
  node [
    id 66
    label "zorganizowa&#263;"
    origin "text"
  ]
  node [
    id 67
    label "inicjatywa"
    origin "text"
  ]
  node [
    id 68
    label "indeks"
    origin "text"
  ]
  node [
    id 69
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 70
    label "zamierza&#263;"
    origin "text"
  ]
  node [
    id 71
    label "praktyka"
    origin "text"
  ]
  node [
    id 72
    label "koncern"
    origin "text"
  ]
  node [
    id 73
    label "obna&#380;y&#263;"
    origin "text"
  ]
  node [
    id 74
    label "napi&#281;tnowa&#263;"
    origin "text"
  ]
  node [
    id 75
    label "obiekt"
  ]
  node [
    id 76
    label "temat"
  ]
  node [
    id 77
    label "istota"
  ]
  node [
    id 78
    label "wpada&#263;"
  ]
  node [
    id 79
    label "wpa&#347;&#263;"
  ]
  node [
    id 80
    label "przedmiot"
  ]
  node [
    id 81
    label "wpadanie"
  ]
  node [
    id 82
    label "kultura"
  ]
  node [
    id 83
    label "przyroda"
  ]
  node [
    id 84
    label "mienie"
  ]
  node [
    id 85
    label "object"
  ]
  node [
    id 86
    label "wpadni&#281;cie"
  ]
  node [
    id 87
    label "sklep"
  ]
  node [
    id 88
    label "wypowied&#378;"
  ]
  node [
    id 89
    label "wytw&#243;r"
  ]
  node [
    id 90
    label "kontekst"
  ]
  node [
    id 91
    label "obja&#347;nienie"
  ]
  node [
    id 92
    label "wypracowanie"
  ]
  node [
    id 93
    label "hermeneutyka"
  ]
  node [
    id 94
    label "interpretation"
  ]
  node [
    id 95
    label "spos&#243;b"
  ]
  node [
    id 96
    label "explanation"
  ]
  node [
    id 97
    label "realizacja"
  ]
  node [
    id 98
    label "akmeizm"
  ]
  node [
    id 99
    label "literatura"
  ]
  node [
    id 100
    label "fiction"
  ]
  node [
    id 101
    label "utw&#243;r"
  ]
  node [
    id 102
    label "paralyze"
  ]
  node [
    id 103
    label "ro&#347;lina"
  ]
  node [
    id 104
    label "strike"
  ]
  node [
    id 105
    label "zachwyca&#263;"
  ]
  node [
    id 106
    label "porusza&#263;"
  ]
  node [
    id 107
    label "uszkadza&#263;"
  ]
  node [
    id 108
    label "zaskakiwa&#263;"
  ]
  node [
    id 109
    label "uderza&#263;"
  ]
  node [
    id 110
    label "atakowa&#263;"
  ]
  node [
    id 111
    label "g&#322;&#281;boki"
  ]
  node [
    id 112
    label "naukowy"
  ]
  node [
    id 113
    label "inteligentny"
  ]
  node [
    id 114
    label "wznios&#322;y"
  ]
  node [
    id 115
    label "umys&#322;owy"
  ]
  node [
    id 116
    label "intelektualnie"
  ]
  node [
    id 117
    label "my&#347;l&#261;cy"
  ]
  node [
    id 118
    label "woda"
  ]
  node [
    id 119
    label "si&#281;ga&#263;"
  ]
  node [
    id 120
    label "trwa&#263;"
  ]
  node [
    id 121
    label "obecno&#347;&#263;"
  ]
  node [
    id 122
    label "stan"
  ]
  node [
    id 123
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 124
    label "stand"
  ]
  node [
    id 125
    label "mie&#263;_miejsce"
  ]
  node [
    id 126
    label "uczestniczy&#263;"
  ]
  node [
    id 127
    label "chodzi&#263;"
  ]
  node [
    id 128
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 129
    label "equal"
  ]
  node [
    id 130
    label "gro&#378;ny"
  ]
  node [
    id 131
    label "k&#322;opotliwy"
  ]
  node [
    id 132
    label "niebezpiecznie"
  ]
  node [
    id 133
    label "free"
  ]
  node [
    id 134
    label "typify"
  ]
  node [
    id 135
    label "flare"
  ]
  node [
    id 136
    label "zaszczeka&#263;"
  ]
  node [
    id 137
    label "rola"
  ]
  node [
    id 138
    label "wykona&#263;"
  ]
  node [
    id 139
    label "wykorzysta&#263;"
  ]
  node [
    id 140
    label "zacz&#261;&#263;"
  ]
  node [
    id 141
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 142
    label "zatokowa&#263;"
  ]
  node [
    id 143
    label "zabrzmie&#263;"
  ]
  node [
    id 144
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 145
    label "leave"
  ]
  node [
    id 146
    label "uda&#263;_si&#281;"
  ]
  node [
    id 147
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 148
    label "instrument_muzyczny"
  ]
  node [
    id 149
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 150
    label "zrobi&#263;"
  ]
  node [
    id 151
    label "play"
  ]
  node [
    id 152
    label "sound"
  ]
  node [
    id 153
    label "represent"
  ]
  node [
    id 154
    label "rozegra&#263;"
  ]
  node [
    id 155
    label "moralnie"
  ]
  node [
    id 156
    label "wiele"
  ]
  node [
    id 157
    label "lepiej"
  ]
  node [
    id 158
    label "korzystnie"
  ]
  node [
    id 159
    label "pomy&#347;lnie"
  ]
  node [
    id 160
    label "pozytywnie"
  ]
  node [
    id 161
    label "dobry"
  ]
  node [
    id 162
    label "dobroczynnie"
  ]
  node [
    id 163
    label "odpowiednio"
  ]
  node [
    id 164
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 165
    label "skutecznie"
  ]
  node [
    id 166
    label "niezgodnie"
  ]
  node [
    id 167
    label "niepomy&#347;lnie"
  ]
  node [
    id 168
    label "negatywnie"
  ]
  node [
    id 169
    label "piesko"
  ]
  node [
    id 170
    label "z&#322;y"
  ]
  node [
    id 171
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 172
    label "gorzej"
  ]
  node [
    id 173
    label "niekorzystnie"
  ]
  node [
    id 174
    label "kiedy&#347;"
  ]
  node [
    id 175
    label "Bangladesz"
  ]
  node [
    id 176
    label "jednostka_monetarna"
  ]
  node [
    id 177
    label "kobieta"
  ]
  node [
    id 178
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 179
    label "&#322;atwy"
  ]
  node [
    id 180
    label "mo&#380;liwy"
  ]
  node [
    id 181
    label "dost&#281;pnie"
  ]
  node [
    id 182
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 183
    label "przyst&#281;pnie"
  ]
  node [
    id 184
    label "zrozumia&#322;y"
  ]
  node [
    id 185
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 186
    label "odblokowanie_si&#281;"
  ]
  node [
    id 187
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 188
    label "pora_roku"
  ]
  node [
    id 189
    label "proscenium"
  ]
  node [
    id 190
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 191
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 192
    label "stadium"
  ]
  node [
    id 193
    label "antyteatr"
  ]
  node [
    id 194
    label "sztuka"
  ]
  node [
    id 195
    label "fragment"
  ]
  node [
    id 196
    label "sznurownia"
  ]
  node [
    id 197
    label "widzownia"
  ]
  node [
    id 198
    label "kiesze&#324;"
  ]
  node [
    id 199
    label "teren"
  ]
  node [
    id 200
    label "horyzont"
  ]
  node [
    id 201
    label "podest"
  ]
  node [
    id 202
    label "przedstawia&#263;"
  ]
  node [
    id 203
    label "podwy&#380;szenie"
  ]
  node [
    id 204
    label "budka_suflera"
  ]
  node [
    id 205
    label "akt"
  ]
  node [
    id 206
    label "przedstawianie"
  ]
  node [
    id 207
    label "kurtyna"
  ]
  node [
    id 208
    label "epizod"
  ]
  node [
    id 209
    label "sphere"
  ]
  node [
    id 210
    label "przedstawienie"
  ]
  node [
    id 211
    label "nadscenie"
  ]
  node [
    id 212
    label "film"
  ]
  node [
    id 213
    label "k&#322;&#243;tnia"
  ]
  node [
    id 214
    label "instytucja"
  ]
  node [
    id 215
    label "dramaturgy"
  ]
  node [
    id 216
    label "znowu"
  ]
  node [
    id 217
    label "ponowny"
  ]
  node [
    id 218
    label "glaze"
  ]
  node [
    id 219
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 220
    label "glazurowa&#263;"
  ]
  node [
    id 221
    label "sugarcoat"
  ]
  node [
    id 222
    label "oblewa&#263;"
  ]
  node [
    id 223
    label "kinematografia"
  ]
  node [
    id 224
    label "niedrogo"
  ]
  node [
    id 225
    label "tanio"
  ]
  node [
    id 226
    label "tandetny"
  ]
  node [
    id 227
    label "karta_wst&#281;pu"
  ]
  node [
    id 228
    label "cedu&#322;a"
  ]
  node [
    id 229
    label "passe-partout"
  ]
  node [
    id 230
    label "konik"
  ]
  node [
    id 231
    label "cena"
  ]
  node [
    id 232
    label "try"
  ]
  node [
    id 233
    label "essay"
  ]
  node [
    id 234
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 235
    label "doznawa&#263;"
  ]
  node [
    id 236
    label "savor"
  ]
  node [
    id 237
    label "szlachetny"
  ]
  node [
    id 238
    label "metaliczny"
  ]
  node [
    id 239
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 240
    label "z&#322;ocenie"
  ]
  node [
    id 241
    label "grosz"
  ]
  node [
    id 242
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 243
    label "utytu&#322;owany"
  ]
  node [
    id 244
    label "poz&#322;ocenie"
  ]
  node [
    id 245
    label "Polska"
  ]
  node [
    id 246
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 247
    label "wspania&#322;y"
  ]
  node [
    id 248
    label "doskona&#322;y"
  ]
  node [
    id 249
    label "kochany"
  ]
  node [
    id 250
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 251
    label "reserve"
  ]
  node [
    id 252
    label "przej&#347;&#263;"
  ]
  node [
    id 253
    label "dupny"
  ]
  node [
    id 254
    label "wysoce"
  ]
  node [
    id 255
    label "wyj&#261;tkowy"
  ]
  node [
    id 256
    label "wybitny"
  ]
  node [
    id 257
    label "znaczny"
  ]
  node [
    id 258
    label "prawdziwy"
  ]
  node [
    id 259
    label "wa&#380;ny"
  ]
  node [
    id 260
    label "nieprzeci&#281;tny"
  ]
  node [
    id 261
    label "czynno&#347;&#263;"
  ]
  node [
    id 262
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 263
    label "motyw"
  ]
  node [
    id 264
    label "fabu&#322;a"
  ]
  node [
    id 265
    label "przebiec"
  ]
  node [
    id 266
    label "przebiegni&#281;cie"
  ]
  node [
    id 267
    label "charakter"
  ]
  node [
    id 268
    label "mi&#281;dzybankowy"
  ]
  node [
    id 269
    label "finansowo"
  ]
  node [
    id 270
    label "fizyczny"
  ]
  node [
    id 271
    label "pozamaterialny"
  ]
  node [
    id 272
    label "materjalny"
  ]
  node [
    id 273
    label "marketingowo"
  ]
  node [
    id 274
    label "handlowy"
  ]
  node [
    id 275
    label "trudny"
  ]
  node [
    id 276
    label "hard"
  ]
  node [
    id 277
    label "tentegowa&#263;"
  ]
  node [
    id 278
    label "urz&#261;dza&#263;"
  ]
  node [
    id 279
    label "give"
  ]
  node [
    id 280
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 281
    label "czyni&#263;"
  ]
  node [
    id 282
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 283
    label "post&#281;powa&#263;"
  ]
  node [
    id 284
    label "wydala&#263;"
  ]
  node [
    id 285
    label "oszukiwa&#263;"
  ]
  node [
    id 286
    label "organizowa&#263;"
  ]
  node [
    id 287
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 288
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 289
    label "work"
  ]
  node [
    id 290
    label "przerabia&#263;"
  ]
  node [
    id 291
    label "stylizowa&#263;"
  ]
  node [
    id 292
    label "falowa&#263;"
  ]
  node [
    id 293
    label "act"
  ]
  node [
    id 294
    label "peddle"
  ]
  node [
    id 295
    label "ukazywa&#263;"
  ]
  node [
    id 296
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 297
    label "praca"
  ]
  node [
    id 298
    label "ozdabia&#263;"
  ]
  node [
    id 299
    label "monopolizowanie"
  ]
  node [
    id 300
    label "podmiot_gospodarczy"
  ]
  node [
    id 301
    label "kostka"
  ]
  node [
    id 302
    label "cenotw&#243;rca"
  ]
  node [
    id 303
    label "zmonopolizowanie"
  ]
  node [
    id 304
    label "gra_planszowa"
  ]
  node [
    id 305
    label "nosi&#263;"
  ]
  node [
    id 306
    label "introduce"
  ]
  node [
    id 307
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 308
    label "wk&#322;ada&#263;"
  ]
  node [
    id 309
    label "umieszcza&#263;"
  ]
  node [
    id 310
    label "obleka&#263;"
  ]
  node [
    id 311
    label "odziewa&#263;"
  ]
  node [
    id 312
    label "powodowa&#263;"
  ]
  node [
    id 313
    label "inflict"
  ]
  node [
    id 314
    label "ubiera&#263;"
  ]
  node [
    id 315
    label "cenzura"
  ]
  node [
    id 316
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 317
    label "zatyczka"
  ]
  node [
    id 318
    label "creation"
  ]
  node [
    id 319
    label "tworzenie"
  ]
  node [
    id 320
    label "kreacja"
  ]
  node [
    id 321
    label "dorobek"
  ]
  node [
    id 322
    label "zbi&#243;r"
  ]
  node [
    id 323
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 324
    label "usamodzielnienie"
  ]
  node [
    id 325
    label "niezale&#380;nie"
  ]
  node [
    id 326
    label "usamodzielnianie"
  ]
  node [
    id 327
    label "&#322;&#261;cznie"
  ]
  node [
    id 328
    label "success"
  ]
  node [
    id 329
    label "passa"
  ]
  node [
    id 330
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 331
    label "kobieta_sukcesu"
  ]
  node [
    id 332
    label "rezultat"
  ]
  node [
    id 333
    label "pokaza&#263;"
  ]
  node [
    id 334
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 335
    label "testify"
  ]
  node [
    id 336
    label "gorzknienie"
  ]
  node [
    id 337
    label "nieprzyjemny"
  ]
  node [
    id 338
    label "gorzko"
  ]
  node [
    id 339
    label "zgorzknienie"
  ]
  node [
    id 340
    label "entuzjasta"
  ]
  node [
    id 341
    label "sympatyk"
  ]
  node [
    id 342
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 343
    label "deski"
  ]
  node [
    id 344
    label "gra"
  ]
  node [
    id 345
    label "modelatornia"
  ]
  node [
    id 346
    label "budynek"
  ]
  node [
    id 347
    label "dekoratornia"
  ]
  node [
    id 348
    label "sala"
  ]
  node [
    id 349
    label "zaplanowa&#263;"
  ]
  node [
    id 350
    label "dostosowa&#263;"
  ]
  node [
    id 351
    label "przygotowa&#263;"
  ]
  node [
    id 352
    label "stworzy&#263;"
  ]
  node [
    id 353
    label "stage"
  ]
  node [
    id 354
    label "pozyska&#263;"
  ]
  node [
    id 355
    label "urobi&#263;"
  ]
  node [
    id 356
    label "plan"
  ]
  node [
    id 357
    label "ensnare"
  ]
  node [
    id 358
    label "standard"
  ]
  node [
    id 359
    label "skupi&#263;"
  ]
  node [
    id 360
    label "wprowadzi&#263;"
  ]
  node [
    id 361
    label "propozycja"
  ]
  node [
    id 362
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 363
    label "spis"
  ]
  node [
    id 364
    label "directory"
  ]
  node [
    id 365
    label "album"
  ]
  node [
    id 366
    label "wska&#378;nik"
  ]
  node [
    id 367
    label "indeks_Lernera"
  ]
  node [
    id 368
    label "student"
  ]
  node [
    id 369
    label "za&#347;wiadczenie"
  ]
  node [
    id 370
    label "znak_pisarski"
  ]
  node [
    id 371
    label "volunteer"
  ]
  node [
    id 372
    label "czyn"
  ]
  node [
    id 373
    label "practice"
  ]
  node [
    id 374
    label "skill"
  ]
  node [
    id 375
    label "znawstwo"
  ]
  node [
    id 376
    label "wiedza"
  ]
  node [
    id 377
    label "zwyczaj"
  ]
  node [
    id 378
    label "eksperiencja"
  ]
  node [
    id 379
    label "nauka"
  ]
  node [
    id 380
    label "firma"
  ]
  node [
    id 381
    label "consortium"
  ]
  node [
    id 382
    label "rozebra&#263;"
  ]
  node [
    id 383
    label "ujawni&#263;"
  ]
  node [
    id 384
    label "unwrap"
  ]
  node [
    id 385
    label "ods&#322;oni&#263;"
  ]
  node [
    id 386
    label "naznaczy&#263;"
  ]
  node [
    id 387
    label "denounce"
  ]
  node [
    id 388
    label "stamp"
  ]
  node [
    id 389
    label "pot&#281;pi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 14
    target 67
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 20
    target 29
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 20
    target 41
  ]
  edge [
    source 20
    target 59
  ]
  edge [
    source 20
    target 60
  ]
  edge [
    source 20
    target 66
  ]
  edge [
    source 20
    target 67
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 174
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 179
  ]
  edge [
    source 26
    target 180
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 26
    target 182
  ]
  edge [
    source 26
    target 183
  ]
  edge [
    source 26
    target 184
  ]
  edge [
    source 26
    target 185
  ]
  edge [
    source 26
    target 186
  ]
  edge [
    source 26
    target 187
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 188
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 37
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 189
  ]
  edge [
    source 30
    target 190
  ]
  edge [
    source 30
    target 191
  ]
  edge [
    source 30
    target 192
  ]
  edge [
    source 30
    target 193
  ]
  edge [
    source 30
    target 194
  ]
  edge [
    source 30
    target 195
  ]
  edge [
    source 30
    target 196
  ]
  edge [
    source 30
    target 197
  ]
  edge [
    source 30
    target 198
  ]
  edge [
    source 30
    target 199
  ]
  edge [
    source 30
    target 200
  ]
  edge [
    source 30
    target 201
  ]
  edge [
    source 30
    target 202
  ]
  edge [
    source 30
    target 203
  ]
  edge [
    source 30
    target 204
  ]
  edge [
    source 30
    target 205
  ]
  edge [
    source 30
    target 206
  ]
  edge [
    source 30
    target 44
  ]
  edge [
    source 30
    target 207
  ]
  edge [
    source 30
    target 208
  ]
  edge [
    source 30
    target 209
  ]
  edge [
    source 30
    target 210
  ]
  edge [
    source 30
    target 211
  ]
  edge [
    source 30
    target 212
  ]
  edge [
    source 30
    target 213
  ]
  edge [
    source 30
    target 214
  ]
  edge [
    source 30
    target 215
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 57
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 216
  ]
  edge [
    source 31
    target 217
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 218
  ]
  edge [
    source 32
    target 219
  ]
  edge [
    source 32
    target 220
  ]
  edge [
    source 32
    target 221
  ]
  edge [
    source 32
    target 222
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 223
  ]
  edge [
    source 35
    target 210
  ]
  edge [
    source 35
    target 65
  ]
  edge [
    source 35
    target 212
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 224
  ]
  edge [
    source 36
    target 225
  ]
  edge [
    source 36
    target 226
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 227
  ]
  edge [
    source 37
    target 228
  ]
  edge [
    source 37
    target 229
  ]
  edge [
    source 37
    target 230
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 231
  ]
  edge [
    source 38
    target 232
  ]
  edge [
    source 38
    target 233
  ]
  edge [
    source 38
    target 234
  ]
  edge [
    source 38
    target 235
  ]
  edge [
    source 38
    target 236
  ]
  edge [
    source 38
    target 44
  ]
  edge [
    source 38
    target 57
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 237
  ]
  edge [
    source 39
    target 238
  ]
  edge [
    source 39
    target 239
  ]
  edge [
    source 39
    target 240
  ]
  edge [
    source 39
    target 241
  ]
  edge [
    source 39
    target 242
  ]
  edge [
    source 39
    target 243
  ]
  edge [
    source 39
    target 244
  ]
  edge [
    source 39
    target 245
  ]
  edge [
    source 39
    target 246
  ]
  edge [
    source 39
    target 247
  ]
  edge [
    source 39
    target 248
  ]
  edge [
    source 39
    target 249
  ]
  edge [
    source 39
    target 176
  ]
  edge [
    source 39
    target 250
  ]
  edge [
    source 40
    target 251
  ]
  edge [
    source 40
    target 252
  ]
  edge [
    source 40
    target 144
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 48
  ]
  edge [
    source 43
    target 49
  ]
  edge [
    source 43
    target 253
  ]
  edge [
    source 43
    target 254
  ]
  edge [
    source 43
    target 255
  ]
  edge [
    source 43
    target 256
  ]
  edge [
    source 43
    target 257
  ]
  edge [
    source 43
    target 258
  ]
  edge [
    source 43
    target 259
  ]
  edge [
    source 43
    target 260
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 261
  ]
  edge [
    source 44
    target 262
  ]
  edge [
    source 44
    target 263
  ]
  edge [
    source 44
    target 264
  ]
  edge [
    source 44
    target 265
  ]
  edge [
    source 44
    target 266
  ]
  edge [
    source 44
    target 267
  ]
  edge [
    source 44
    target 57
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 268
  ]
  edge [
    source 45
    target 269
  ]
  edge [
    source 45
    target 270
  ]
  edge [
    source 45
    target 271
  ]
  edge [
    source 45
    target 272
  ]
  edge [
    source 46
    target 273
  ]
  edge [
    source 46
    target 274
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 275
  ]
  edge [
    source 47
    target 276
  ]
  edge [
    source 48
    target 277
  ]
  edge [
    source 48
    target 278
  ]
  edge [
    source 48
    target 279
  ]
  edge [
    source 48
    target 280
  ]
  edge [
    source 48
    target 281
  ]
  edge [
    source 48
    target 282
  ]
  edge [
    source 48
    target 283
  ]
  edge [
    source 48
    target 284
  ]
  edge [
    source 48
    target 285
  ]
  edge [
    source 48
    target 286
  ]
  edge [
    source 48
    target 287
  ]
  edge [
    source 48
    target 288
  ]
  edge [
    source 48
    target 289
  ]
  edge [
    source 48
    target 290
  ]
  edge [
    source 48
    target 291
  ]
  edge [
    source 48
    target 292
  ]
  edge [
    source 48
    target 293
  ]
  edge [
    source 48
    target 294
  ]
  edge [
    source 48
    target 295
  ]
  edge [
    source 48
    target 296
  ]
  edge [
    source 48
    target 297
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 298
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 299
  ]
  edge [
    source 51
    target 87
  ]
  edge [
    source 51
    target 300
  ]
  edge [
    source 51
    target 301
  ]
  edge [
    source 51
    target 302
  ]
  edge [
    source 51
    target 303
  ]
  edge [
    source 51
    target 304
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 305
  ]
  edge [
    source 52
    target 306
  ]
  edge [
    source 52
    target 307
  ]
  edge [
    source 52
    target 308
  ]
  edge [
    source 52
    target 309
  ]
  edge [
    source 52
    target 310
  ]
  edge [
    source 52
    target 311
  ]
  edge [
    source 52
    target 312
  ]
  edge [
    source 52
    target 313
  ]
  edge [
    source 52
    target 314
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 315
  ]
  edge [
    source 53
    target 316
  ]
  edge [
    source 53
    target 317
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 318
  ]
  edge [
    source 54
    target 319
  ]
  edge [
    source 54
    target 320
  ]
  edge [
    source 54
    target 321
  ]
  edge [
    source 54
    target 82
  ]
  edge [
    source 54
    target 322
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 323
  ]
  edge [
    source 55
    target 324
  ]
  edge [
    source 55
    target 325
  ]
  edge [
    source 55
    target 326
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 327
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 328
  ]
  edge [
    source 58
    target 329
  ]
  edge [
    source 58
    target 330
  ]
  edge [
    source 58
    target 331
  ]
  edge [
    source 58
    target 332
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 333
  ]
  edge [
    source 60
    target 334
  ]
  edge [
    source 60
    target 335
  ]
  edge [
    source 60
    target 279
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 67
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 336
  ]
  edge [
    source 63
    target 337
  ]
  edge [
    source 63
    target 338
  ]
  edge [
    source 63
    target 339
  ]
  edge [
    source 63
    target 70
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 340
  ]
  edge [
    source 64
    target 341
  ]
  edge [
    source 64
    target 342
  ]
  edge [
    source 64
    target 71
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 210
  ]
  edge [
    source 65
    target 343
  ]
  edge [
    source 65
    target 202
  ]
  edge [
    source 65
    target 344
  ]
  edge [
    source 65
    target 345
  ]
  edge [
    source 65
    target 197
  ]
  edge [
    source 65
    target 191
  ]
  edge [
    source 65
    target 346
  ]
  edge [
    source 65
    target 99
  ]
  edge [
    source 65
    target 199
  ]
  edge [
    source 65
    target 206
  ]
  edge [
    source 65
    target 193
  ]
  edge [
    source 65
    target 347
  ]
  edge [
    source 65
    target 214
  ]
  edge [
    source 65
    target 348
  ]
  edge [
    source 65
    target 151
  ]
  edge [
    source 65
    target 194
  ]
  edge [
    source 66
    target 349
  ]
  edge [
    source 66
    target 350
  ]
  edge [
    source 66
    target 351
  ]
  edge [
    source 66
    target 352
  ]
  edge [
    source 66
    target 353
  ]
  edge [
    source 66
    target 354
  ]
  edge [
    source 66
    target 355
  ]
  edge [
    source 66
    target 356
  ]
  edge [
    source 66
    target 357
  ]
  edge [
    source 66
    target 358
  ]
  edge [
    source 66
    target 359
  ]
  edge [
    source 66
    target 360
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 356
  ]
  edge [
    source 67
    target 361
  ]
  edge [
    source 67
    target 362
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 363
  ]
  edge [
    source 68
    target 364
  ]
  edge [
    source 68
    target 365
  ]
  edge [
    source 68
    target 366
  ]
  edge [
    source 68
    target 367
  ]
  edge [
    source 68
    target 368
  ]
  edge [
    source 68
    target 369
  ]
  edge [
    source 68
    target 370
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 371
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 372
  ]
  edge [
    source 71
    target 373
  ]
  edge [
    source 71
    target 374
  ]
  edge [
    source 71
    target 375
  ]
  edge [
    source 71
    target 376
  ]
  edge [
    source 71
    target 377
  ]
  edge [
    source 71
    target 378
  ]
  edge [
    source 71
    target 297
  ]
  edge [
    source 71
    target 379
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 380
  ]
  edge [
    source 72
    target 381
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 382
  ]
  edge [
    source 73
    target 383
  ]
  edge [
    source 73
    target 384
  ]
  edge [
    source 73
    target 385
  ]
  edge [
    source 74
    target 386
  ]
  edge [
    source 74
    target 387
  ]
  edge [
    source 74
    target 388
  ]
  edge [
    source 74
    target 389
  ]
]
