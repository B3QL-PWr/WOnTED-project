graph [
  maxDegree 8
  minDegree 1
  meanDegree 2
  density 0.11764705882352941
  graphCliqueNumber 3
  node [
    id 0
    label "koran"
    origin "text"
  ]
  node [
    id 1
    label "wzajemny"
    origin "text"
  ]
  node [
    id 2
    label "oszukiwa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zajemny"
  ]
  node [
    id 4
    label "wzajemnie"
  ]
  node [
    id 5
    label "zobop&#243;lny"
  ]
  node [
    id 6
    label "wsp&#243;lny"
  ]
  node [
    id 7
    label "orzyna&#263;"
  ]
  node [
    id 8
    label "oszwabia&#263;"
  ]
  node [
    id 9
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 10
    label "cheat"
  ]
  node [
    id 11
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 12
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 13
    label "Koran"
  ]
  node [
    id 14
    label "Surah"
  ]
  node [
    id 15
    label "at"
  ]
  node [
    id 16
    label "Tagh&#226;bun"
  ]
  node [
    id 17
    label "zaw&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
]
