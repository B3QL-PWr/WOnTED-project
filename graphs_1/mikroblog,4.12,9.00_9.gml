graph [
  maxDegree 3
  minDegree 1
  meanDegree 2.25
  density 0.32142857142857145
  graphCliqueNumber 4
  node [
    id 0
    label "arkadiusz"
    origin "text"
  ]
  node [
    id 1
    label "milik"
    origin "text"
  ]
  node [
    id 2
    label "atalanta"
    origin "text"
  ]
  node [
    id 3
    label "napoli"
    origin "text"
  ]
  node [
    id 4
    label "Arkadiusz"
  ]
  node [
    id 5
    label "Milik"
  ]
  node [
    id 6
    label "Atalanta"
  ]
  node [
    id 7
    label "Napoli"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
]
