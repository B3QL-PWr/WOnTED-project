graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.121212121212121
  density 0.01076757421935087
  graphCliqueNumber 3
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "siebie"
    origin "text"
  ]
  node [
    id 2
    label "my&#347;le"
    origin "text"
  ]
  node [
    id 3
    label "jaca"
    origin "text"
  ]
  node [
    id 4
    label "taki"
    origin "text"
  ]
  node [
    id 5
    label "chewbacca"
    origin "text"
  ]
  node [
    id 6
    label "uniwersum"
    origin "text"
  ]
  node [
    id 7
    label "urz&#281;dniczy"
    origin "text"
  ]
  node [
    id 8
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "co&#347;"
    origin "text"
  ]
  node [
    id 10
    label "skrzecze&#263;"
    origin "text"
  ]
  node [
    id 11
    label "rycze&#263;"
    origin "text"
  ]
  node [
    id 12
    label "tam"
    origin "text"
  ]
  node [
    id 13
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 15
    label "odg&#322;os"
    origin "text"
  ]
  node [
    id 16
    label "wiadomo"
    origin "text"
  ]
  node [
    id 17
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 18
    label "trudno"
    origin "text"
  ]
  node [
    id 19
    label "zrozumie&#263;"
    origin "text"
  ]
  node [
    id 20
    label "wszyscy"
    origin "text"
  ]
  node [
    id 21
    label "bohater"
    origin "text"
  ]
  node [
    id 22
    label "rozumie&#263;"
    origin "text"
  ]
  node [
    id 23
    label "jeszcze"
    origin "text"
  ]
  node [
    id 24
    label "odpowiada&#263;"
    origin "text"
  ]
  node [
    id 25
    label "xdd"
    origin "text"
  ]
  node [
    id 26
    label "okre&#347;lony"
  ]
  node [
    id 27
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 28
    label "makrokosmos"
  ]
  node [
    id 29
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 30
    label "czarna_dziura"
  ]
  node [
    id 31
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 32
    label "przestrze&#324;"
  ]
  node [
    id 33
    label "planeta"
  ]
  node [
    id 34
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 35
    label "ekosfera"
  ]
  node [
    id 36
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 37
    label "poj&#281;cie"
  ]
  node [
    id 38
    label "ciemna_materia"
  ]
  node [
    id 39
    label "mikrokosmos"
  ]
  node [
    id 40
    label "urz&#281;dniczo"
  ]
  node [
    id 41
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 42
    label "urz&#281;dowy"
  ]
  node [
    id 43
    label "trwa&#263;"
  ]
  node [
    id 44
    label "sit"
  ]
  node [
    id 45
    label "pause"
  ]
  node [
    id 46
    label "ptak"
  ]
  node [
    id 47
    label "garowa&#263;"
  ]
  node [
    id 48
    label "mieszka&#263;"
  ]
  node [
    id 49
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 50
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 51
    label "przebywa&#263;"
  ]
  node [
    id 52
    label "brood"
  ]
  node [
    id 53
    label "zwierz&#281;"
  ]
  node [
    id 54
    label "doprowadza&#263;"
  ]
  node [
    id 55
    label "spoczywa&#263;"
  ]
  node [
    id 56
    label "tkwi&#263;"
  ]
  node [
    id 57
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 58
    label "thing"
  ]
  node [
    id 59
    label "cosik"
  ]
  node [
    id 60
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 61
    label "squawk"
  ]
  node [
    id 62
    label "&#380;aba"
  ]
  node [
    id 63
    label "hula&#263;"
  ]
  node [
    id 64
    label "p&#322;aka&#263;"
  ]
  node [
    id 65
    label "mika&#263;"
  ]
  node [
    id 66
    label "zia&#263;"
  ]
  node [
    id 67
    label "&#347;mia&#263;_si&#281;"
  ]
  node [
    id 68
    label "bawl"
  ]
  node [
    id 69
    label "wrzeszcze&#263;"
  ]
  node [
    id 70
    label "hucze&#263;"
  ]
  node [
    id 71
    label "tu"
  ]
  node [
    id 72
    label "impart"
  ]
  node [
    id 73
    label "panna_na_wydaniu"
  ]
  node [
    id 74
    label "surrender"
  ]
  node [
    id 75
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 76
    label "train"
  ]
  node [
    id 77
    label "give"
  ]
  node [
    id 78
    label "wytwarza&#263;"
  ]
  node [
    id 79
    label "dawa&#263;"
  ]
  node [
    id 80
    label "zapach"
  ]
  node [
    id 81
    label "wprowadza&#263;"
  ]
  node [
    id 82
    label "ujawnia&#263;"
  ]
  node [
    id 83
    label "wydawnictwo"
  ]
  node [
    id 84
    label "powierza&#263;"
  ]
  node [
    id 85
    label "produkcja"
  ]
  node [
    id 86
    label "denuncjowa&#263;"
  ]
  node [
    id 87
    label "mie&#263;_miejsce"
  ]
  node [
    id 88
    label "plon"
  ]
  node [
    id 89
    label "reszta"
  ]
  node [
    id 90
    label "robi&#263;"
  ]
  node [
    id 91
    label "placard"
  ]
  node [
    id 92
    label "tajemnica"
  ]
  node [
    id 93
    label "wiano"
  ]
  node [
    id 94
    label "kojarzy&#263;"
  ]
  node [
    id 95
    label "d&#378;wi&#281;k"
  ]
  node [
    id 96
    label "podawa&#263;"
  ]
  node [
    id 97
    label "jako&#347;"
  ]
  node [
    id 98
    label "charakterystyczny"
  ]
  node [
    id 99
    label "ciekawy"
  ]
  node [
    id 100
    label "jako_tako"
  ]
  node [
    id 101
    label "dziwny"
  ]
  node [
    id 102
    label "niez&#322;y"
  ]
  node [
    id 103
    label "przyzwoity"
  ]
  node [
    id 104
    label "resonance"
  ]
  node [
    id 105
    label "brzmienie"
  ]
  node [
    id 106
    label "wpada&#263;"
  ]
  node [
    id 107
    label "wpadanie"
  ]
  node [
    id 108
    label "onomatopeja"
  ]
  node [
    id 109
    label "wydanie"
  ]
  node [
    id 110
    label "wyda&#263;"
  ]
  node [
    id 111
    label "wpa&#347;&#263;"
  ]
  node [
    id 112
    label "sound"
  ]
  node [
    id 113
    label "note"
  ]
  node [
    id 114
    label "zjawisko"
  ]
  node [
    id 115
    label "wpadni&#281;cie"
  ]
  node [
    id 116
    label "by&#263;"
  ]
  node [
    id 117
    label "remark"
  ]
  node [
    id 118
    label "u&#380;ywa&#263;"
  ]
  node [
    id 119
    label "okre&#347;la&#263;"
  ]
  node [
    id 120
    label "j&#281;zyk"
  ]
  node [
    id 121
    label "say"
  ]
  node [
    id 122
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 123
    label "formu&#322;owa&#263;"
  ]
  node [
    id 124
    label "talk"
  ]
  node [
    id 125
    label "powiada&#263;"
  ]
  node [
    id 126
    label "informowa&#263;"
  ]
  node [
    id 127
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 128
    label "wydobywa&#263;"
  ]
  node [
    id 129
    label "express"
  ]
  node [
    id 130
    label "chew_the_fat"
  ]
  node [
    id 131
    label "dysfonia"
  ]
  node [
    id 132
    label "umie&#263;"
  ]
  node [
    id 133
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 134
    label "tell"
  ]
  node [
    id 135
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 136
    label "wyra&#380;a&#263;"
  ]
  node [
    id 137
    label "gaworzy&#263;"
  ]
  node [
    id 138
    label "rozmawia&#263;"
  ]
  node [
    id 139
    label "dziama&#263;"
  ]
  node [
    id 140
    label "prawi&#263;"
  ]
  node [
    id 141
    label "trudny"
  ]
  node [
    id 142
    label "hard"
  ]
  node [
    id 143
    label "poczu&#263;"
  ]
  node [
    id 144
    label "zacz&#261;&#263;"
  ]
  node [
    id 145
    label "oceni&#263;"
  ]
  node [
    id 146
    label "think"
  ]
  node [
    id 147
    label "skuma&#263;"
  ]
  node [
    id 148
    label "do"
  ]
  node [
    id 149
    label "bohaterski"
  ]
  node [
    id 150
    label "cz&#322;owiek"
  ]
  node [
    id 151
    label "Zgredek"
  ]
  node [
    id 152
    label "Herkules"
  ]
  node [
    id 153
    label "Casanova"
  ]
  node [
    id 154
    label "Borewicz"
  ]
  node [
    id 155
    label "Don_Juan"
  ]
  node [
    id 156
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 157
    label "Winnetou"
  ]
  node [
    id 158
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 159
    label "Messi"
  ]
  node [
    id 160
    label "Herkules_Poirot"
  ]
  node [
    id 161
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 162
    label "Szwejk"
  ]
  node [
    id 163
    label "Sherlock_Holmes"
  ]
  node [
    id 164
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 165
    label "Hamlet"
  ]
  node [
    id 166
    label "Asterix"
  ]
  node [
    id 167
    label "Quasimodo"
  ]
  node [
    id 168
    label "Don_Kiszot"
  ]
  node [
    id 169
    label "Wallenrod"
  ]
  node [
    id 170
    label "uczestnik"
  ]
  node [
    id 171
    label "&#347;mia&#322;ek"
  ]
  node [
    id 172
    label "Harry_Potter"
  ]
  node [
    id 173
    label "podmiot"
  ]
  node [
    id 174
    label "Achilles"
  ]
  node [
    id 175
    label "Werter"
  ]
  node [
    id 176
    label "Mario"
  ]
  node [
    id 177
    label "posta&#263;"
  ]
  node [
    id 178
    label "wiedzie&#263;"
  ]
  node [
    id 179
    label "zna&#263;"
  ]
  node [
    id 180
    label "czu&#263;"
  ]
  node [
    id 181
    label "kuma&#263;"
  ]
  node [
    id 182
    label "odbiera&#263;"
  ]
  node [
    id 183
    label "empatia"
  ]
  node [
    id 184
    label "see"
  ]
  node [
    id 185
    label "match"
  ]
  node [
    id 186
    label "ci&#261;gle"
  ]
  node [
    id 187
    label "report"
  ]
  node [
    id 188
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 189
    label "reagowa&#263;"
  ]
  node [
    id 190
    label "contend"
  ]
  node [
    id 191
    label "ponosi&#263;"
  ]
  node [
    id 192
    label "react"
  ]
  node [
    id 193
    label "tone"
  ]
  node [
    id 194
    label "equate"
  ]
  node [
    id 195
    label "pytanie"
  ]
  node [
    id 196
    label "powodowa&#263;"
  ]
  node [
    id 197
    label "answer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 75
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 60
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 21
    target 150
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 21
    target 161
  ]
  edge [
    source 21
    target 162
  ]
  edge [
    source 21
    target 163
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 174
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 178
  ]
  edge [
    source 22
    target 179
  ]
  edge [
    source 22
    target 180
  ]
  edge [
    source 22
    target 120
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 77
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 116
  ]
  edge [
    source 24
    target 79
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 24
    target 189
  ]
  edge [
    source 24
    target 190
  ]
  edge [
    source 24
    target 191
  ]
  edge [
    source 24
    target 72
  ]
  edge [
    source 24
    target 192
  ]
  edge [
    source 24
    target 193
  ]
  edge [
    source 24
    target 194
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 196
  ]
  edge [
    source 24
    target 197
  ]
]
