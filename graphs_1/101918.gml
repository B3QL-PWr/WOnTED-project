graph [
  maxDegree 16
  minDegree 1
  meanDegree 2.103448275862069
  density 0.03690260133091349
  graphCliqueNumber 3
  node [
    id 0
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 1
    label "cyfrowy"
    origin "text"
  ]
  node [
    id 2
    label "urz&#261;d"
    origin "text"
  ]
  node [
    id 3
    label "otwarty"
    origin "text"
  ]
  node [
    id 4
    label "godzina"
    origin "text"
  ]
  node [
    id 5
    label "doba"
    origin "text"
  ]
  node [
    id 6
    label "przez"
    origin "text"
  ]
  node [
    id 7
    label "dni"
    origin "text"
  ]
  node [
    id 8
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 9
    label "pozdrawia&#263;"
  ]
  node [
    id 10
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 11
    label "greet"
  ]
  node [
    id 12
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 13
    label "welcome"
  ]
  node [
    id 14
    label "cyfrowo"
  ]
  node [
    id 15
    label "sygna&#322;_cyfrowy"
  ]
  node [
    id 16
    label "elektroniczny"
  ]
  node [
    id 17
    label "cyfryzacja"
  ]
  node [
    id 18
    label "organ"
  ]
  node [
    id 19
    label "w&#322;adza"
  ]
  node [
    id 20
    label "instytucja"
  ]
  node [
    id 21
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 22
    label "mianowaniec"
  ]
  node [
    id 23
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 24
    label "stanowisko"
  ]
  node [
    id 25
    label "position"
  ]
  node [
    id 26
    label "dzia&#322;"
  ]
  node [
    id 27
    label "siedziba"
  ]
  node [
    id 28
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 29
    label "okienko"
  ]
  node [
    id 30
    label "ewidentny"
  ]
  node [
    id 31
    label "bezpo&#347;redni"
  ]
  node [
    id 32
    label "otwarcie"
  ]
  node [
    id 33
    label "nieograniczony"
  ]
  node [
    id 34
    label "zdecydowany"
  ]
  node [
    id 35
    label "gotowy"
  ]
  node [
    id 36
    label "aktualny"
  ]
  node [
    id 37
    label "prostoduszny"
  ]
  node [
    id 38
    label "jawnie"
  ]
  node [
    id 39
    label "otworzysty"
  ]
  node [
    id 40
    label "dost&#281;pny"
  ]
  node [
    id 41
    label "publiczny"
  ]
  node [
    id 42
    label "aktywny"
  ]
  node [
    id 43
    label "kontaktowy"
  ]
  node [
    id 44
    label "minuta"
  ]
  node [
    id 45
    label "czas"
  ]
  node [
    id 46
    label "p&#243;&#322;godzina"
  ]
  node [
    id 47
    label "kwadrans"
  ]
  node [
    id 48
    label "time"
  ]
  node [
    id 49
    label "jednostka_czasu"
  ]
  node [
    id 50
    label "long_time"
  ]
  node [
    id 51
    label "jednostka_geologiczna"
  ]
  node [
    id 52
    label "noc"
  ]
  node [
    id 53
    label "dzie&#324;"
  ]
  node [
    id 54
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 55
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 56
    label "weekend"
  ]
  node [
    id 57
    label "miesi&#261;c"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
]
