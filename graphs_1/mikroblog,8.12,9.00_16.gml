graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.11764705882352941
  graphCliqueNumber 2
  node [
    id 0
    label "czemu"
    origin "text"
  ]
  node [
    id 1
    label "poni&#380;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "zabiega&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "rozowepaski"
    origin "text"
  ]
  node [
    id 6
    label "write_down"
  ]
  node [
    id 7
    label "dyskredytowa&#263;"
  ]
  node [
    id 8
    label "obra&#380;a&#263;"
  ]
  node [
    id 9
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 10
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 11
    label "podejmowa&#263;"
  ]
  node [
    id 12
    label "endeavor"
  ]
  node [
    id 13
    label "przyczyna"
  ]
  node [
    id 14
    label "uwaga"
  ]
  node [
    id 15
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 16
    label "punkt_widzenia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
]
