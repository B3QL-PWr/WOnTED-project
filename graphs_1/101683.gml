graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9615384615384615
  density 0.038461538461538464
  graphCliqueNumber 2
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "pro&#347;ba"
    origin "text"
  ]
  node [
    id 2
    label "aby"
    origin "text"
  ]
  node [
    id 3
    label "&#322;amanie"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "op&#322;atek"
    origin "text"
  ]
  node [
    id 6
    label "w&#322;&#261;cza&#263;"
    origin "text"
  ]
  node [
    id 7
    label "debata"
    origin "text"
  ]
  node [
    id 8
    label "sejmowy"
    origin "text"
  ]
  node [
    id 9
    label "matczysko"
  ]
  node [
    id 10
    label "macierz"
  ]
  node [
    id 11
    label "przodkini"
  ]
  node [
    id 12
    label "Matka_Boska"
  ]
  node [
    id 13
    label "macocha"
  ]
  node [
    id 14
    label "matka_zast&#281;pcza"
  ]
  node [
    id 15
    label "stara"
  ]
  node [
    id 16
    label "rodzice"
  ]
  node [
    id 17
    label "rodzic"
  ]
  node [
    id 18
    label "solicitation"
  ]
  node [
    id 19
    label "wypowied&#378;"
  ]
  node [
    id 20
    label "troch&#281;"
  ]
  node [
    id 21
    label "robienie"
  ]
  node [
    id 22
    label "czynno&#347;&#263;"
  ]
  node [
    id 23
    label "sk&#322;adanie"
  ]
  node [
    id 24
    label "przygn&#281;bianie"
  ]
  node [
    id 25
    label "dzielenie"
  ]
  node [
    id 26
    label "ko&#322;o"
  ]
  node [
    id 27
    label "b&#243;l"
  ]
  node [
    id 28
    label "pokonywanie"
  ]
  node [
    id 29
    label "breakage"
  ]
  node [
    id 30
    label "kszta&#322;towanie"
  ]
  node [
    id 31
    label "za&#322;amywanie_si&#281;"
  ]
  node [
    id 32
    label "wy&#322;amywanie"
  ]
  node [
    id 33
    label "spread"
  ]
  node [
    id 34
    label "powodowanie"
  ]
  node [
    id 35
    label "misdemeanor"
  ]
  node [
    id 36
    label "spotkanie"
  ]
  node [
    id 37
    label "wigilia"
  ]
  node [
    id 38
    label "wafelek"
  ]
  node [
    id 39
    label "nastawia&#263;"
  ]
  node [
    id 40
    label "connect"
  ]
  node [
    id 41
    label "ogl&#261;da&#263;"
  ]
  node [
    id 42
    label "uruchamia&#263;"
  ]
  node [
    id 43
    label "umieszcza&#263;"
  ]
  node [
    id 44
    label "odkr&#281;ca&#263;_kurek"
  ]
  node [
    id 45
    label "dokoptowywa&#263;"
  ]
  node [
    id 46
    label "zaczyna&#263;"
  ]
  node [
    id 47
    label "get_in_touch"
  ]
  node [
    id 48
    label "involve"
  ]
  node [
    id 49
    label "rozmowa"
  ]
  node [
    id 50
    label "sympozjon"
  ]
  node [
    id 51
    label "conference"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
]
