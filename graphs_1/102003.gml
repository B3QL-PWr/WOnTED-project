graph [
  maxDegree 48
  minDegree 1
  meanDegree 2.1797752808988764
  density 0.00490940378580828
  graphCliqueNumber 4
  node [
    id 0
    label "rozmowa"
    origin "text"
  ]
  node [
    id 1
    label "j&#225;nosem"
    origin "text"
  ]
  node [
    id 2
    label "h&#225;yem"
    origin "text"
  ]
  node [
    id 3
    label "przed"
    origin "text"
  ]
  node [
    id 4
    label "niedzielny"
    origin "text"
  ]
  node [
    id 5
    label "premiera"
    origin "text"
  ]
  node [
    id 6
    label "sztuka"
    origin "text"
  ]
  node [
    id 7
    label "g&#233;za"
    origin "text"
  ]
  node [
    id 8
    label "dzieciak"
    origin "text"
  ]
  node [
    id 9
    label "studio"
    origin "text"
  ]
  node [
    id 10
    label "teatralny"
    origin "text"
  ]
  node [
    id 11
    label "tvp"
    origin "text"
  ]
  node [
    id 12
    label "pesymista"
    origin "text"
  ]
  node [
    id 13
    label "pogodzi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "los"
    origin "text"
  ]
  node [
    id 15
    label "pa&#324;ski"
    origin "text"
  ]
  node [
    id 16
    label "przet&#322;umaczy&#263;"
    origin "text"
  ]
  node [
    id 17
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 18
    label "kilkana&#347;cie"
    origin "text"
  ]
  node [
    id 19
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 20
    label "taki"
    origin "text"
  ]
  node [
    id 21
    label "sukces"
    origin "text"
  ]
  node [
    id 22
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 23
    label "pierwsze"
    origin "text"
  ]
  node [
    id 24
    label "dramat"
    origin "text"
  ]
  node [
    id 25
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 26
    label "by&#263;"
    origin "text"
  ]
  node [
    id 27
    label "niebezpieczny"
    origin "text"
  ]
  node [
    id 28
    label "j&#225;nos"
    origin "text"
  ]
  node [
    id 29
    label "h&#225;y"
    origin "text"
  ]
  node [
    id 30
    label "moi"
    origin "text"
  ]
  node [
    id 31
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 32
    label "nic"
    origin "text"
  ]
  node [
    id 33
    label "si&#281;"
    origin "text"
  ]
  node [
    id 34
    label "zmieni&#263;"
    origin "text"
  ]
  node [
    id 35
    label "sta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 36
    label "ani"
    origin "text"
  ]
  node [
    id 37
    label "bogaty"
    origin "text"
  ]
  node [
    id 38
    label "m&#261;dry"
    origin "text"
  ]
  node [
    id 39
    label "pi&#281;kny"
    origin "text"
  ]
  node [
    id 40
    label "wci&#261;&#380;"
    origin "text"
  ]
  node [
    id 41
    label "upad&#322;y"
    origin "text"
  ]
  node [
    id 42
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 43
    label "jaki"
    origin "text"
  ]
  node [
    id 44
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 45
    label "wczesno"
    origin "text"
  ]
  node [
    id 46
    label "ten"
    origin "text"
  ]
  node [
    id 47
    label "nim"
    origin "text"
  ]
  node [
    id 48
    label "g&#233;z&#281;"
    origin "text"
  ]
  node [
    id 49
    label "w&#281;gry"
    origin "text"
  ]
  node [
    id 50
    label "ceni&#263;"
    origin "text"
  ]
  node [
    id 51
    label "pisarz"
    origin "text"
  ]
  node [
    id 52
    label "poeta"
    origin "text"
  ]
  node [
    id 53
    label "dopiero"
    origin "text"
  ]
  node [
    id 54
    label "przynie&#347;&#263;"
    origin "text"
  ]
  node [
    id 55
    label "pan"
    origin "text"
  ]
  node [
    id 56
    label "s&#322;awa"
    origin "text"
  ]
  node [
    id 57
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 58
    label "discussion"
  ]
  node [
    id 59
    label "czynno&#347;&#263;"
  ]
  node [
    id 60
    label "odpowied&#378;"
  ]
  node [
    id 61
    label "rozhowor"
  ]
  node [
    id 62
    label "cisza"
  ]
  node [
    id 63
    label "&#347;wi&#261;teczny"
  ]
  node [
    id 64
    label "przedstawienie"
  ]
  node [
    id 65
    label "premiere"
  ]
  node [
    id 66
    label "theatrical_performance"
  ]
  node [
    id 67
    label "sprawno&#347;&#263;"
  ]
  node [
    id 68
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 69
    label "ods&#322;ona"
  ]
  node [
    id 70
    label "przedmiot"
  ]
  node [
    id 71
    label "Faust"
  ]
  node [
    id 72
    label "jednostka"
  ]
  node [
    id 73
    label "fortel"
  ]
  node [
    id 74
    label "environment"
  ]
  node [
    id 75
    label "rola"
  ]
  node [
    id 76
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 77
    label "utw&#243;r"
  ]
  node [
    id 78
    label "egzemplarz"
  ]
  node [
    id 79
    label "realizacja"
  ]
  node [
    id 80
    label "turn"
  ]
  node [
    id 81
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 82
    label "scenografia"
  ]
  node [
    id 83
    label "kultura_duchowa"
  ]
  node [
    id 84
    label "ilo&#347;&#263;"
  ]
  node [
    id 85
    label "pokaz"
  ]
  node [
    id 86
    label "przedstawia&#263;"
  ]
  node [
    id 87
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 88
    label "pr&#243;bowanie"
  ]
  node [
    id 89
    label "kobieta"
  ]
  node [
    id 90
    label "scena"
  ]
  node [
    id 91
    label "przedstawianie"
  ]
  node [
    id 92
    label "scenariusz"
  ]
  node [
    id 93
    label "didaskalia"
  ]
  node [
    id 94
    label "Apollo"
  ]
  node [
    id 95
    label "czyn"
  ]
  node [
    id 96
    label "head"
  ]
  node [
    id 97
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 98
    label "przedstawi&#263;"
  ]
  node [
    id 99
    label "ambala&#380;"
  ]
  node [
    id 100
    label "kultura"
  ]
  node [
    id 101
    label "towar"
  ]
  node [
    id 102
    label "naiwniak"
  ]
  node [
    id 103
    label "dziecinny"
  ]
  node [
    id 104
    label "dziecko"
  ]
  node [
    id 105
    label "bech"
  ]
  node [
    id 106
    label "telewizja"
  ]
  node [
    id 107
    label "pomieszczenie"
  ]
  node [
    id 108
    label "radio"
  ]
  node [
    id 109
    label "nadmierny"
  ]
  node [
    id 110
    label "nienaturalny"
  ]
  node [
    id 111
    label "teatralnie"
  ]
  node [
    id 112
    label "sceptyk"
  ]
  node [
    id 113
    label "smutas"
  ]
  node [
    id 114
    label "Schopenhauer"
  ]
  node [
    id 115
    label "accommodate"
  ]
  node [
    id 116
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 117
    label "si&#322;a"
  ]
  node [
    id 118
    label "przymus"
  ]
  node [
    id 119
    label "rzuci&#263;"
  ]
  node [
    id 120
    label "hazard"
  ]
  node [
    id 121
    label "destiny"
  ]
  node [
    id 122
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 123
    label "bilet"
  ]
  node [
    id 124
    label "przebieg_&#380;ycia"
  ]
  node [
    id 125
    label "rzucenie"
  ]
  node [
    id 126
    label "charakterystyczny"
  ]
  node [
    id 127
    label "pa&#324;sko"
  ]
  node [
    id 128
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 129
    label "frame"
  ]
  node [
    id 130
    label "zinterpretowa&#263;"
  ]
  node [
    id 131
    label "przekona&#263;"
  ]
  node [
    id 132
    label "zrobi&#263;"
  ]
  node [
    id 133
    label "put"
  ]
  node [
    id 134
    label "proceed"
  ]
  node [
    id 135
    label "catch"
  ]
  node [
    id 136
    label "pozosta&#263;"
  ]
  node [
    id 137
    label "osta&#263;_si&#281;"
  ]
  node [
    id 138
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 139
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 140
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 141
    label "change"
  ]
  node [
    id 142
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 143
    label "pisa&#263;"
  ]
  node [
    id 144
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 145
    label "ssanie"
  ]
  node [
    id 146
    label "po_koroniarsku"
  ]
  node [
    id 147
    label "but"
  ]
  node [
    id 148
    label "m&#243;wienie"
  ]
  node [
    id 149
    label "rozumie&#263;"
  ]
  node [
    id 150
    label "formacja_geologiczna"
  ]
  node [
    id 151
    label "rozumienie"
  ]
  node [
    id 152
    label "m&#243;wi&#263;"
  ]
  node [
    id 153
    label "gramatyka"
  ]
  node [
    id 154
    label "pype&#263;"
  ]
  node [
    id 155
    label "makroglosja"
  ]
  node [
    id 156
    label "kawa&#322;ek"
  ]
  node [
    id 157
    label "artykulator"
  ]
  node [
    id 158
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 159
    label "jama_ustna"
  ]
  node [
    id 160
    label "spos&#243;b"
  ]
  node [
    id 161
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 162
    label "przet&#322;umaczenie"
  ]
  node [
    id 163
    label "t&#322;umaczenie"
  ]
  node [
    id 164
    label "language"
  ]
  node [
    id 165
    label "jeniec"
  ]
  node [
    id 166
    label "organ"
  ]
  node [
    id 167
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 168
    label "pismo"
  ]
  node [
    id 169
    label "formalizowanie"
  ]
  node [
    id 170
    label "fonetyka"
  ]
  node [
    id 171
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 172
    label "wokalizm"
  ]
  node [
    id 173
    label "liza&#263;"
  ]
  node [
    id 174
    label "s&#322;ownictwo"
  ]
  node [
    id 175
    label "formalizowa&#263;"
  ]
  node [
    id 176
    label "natural_language"
  ]
  node [
    id 177
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 178
    label "stylik"
  ]
  node [
    id 179
    label "konsonantyzm"
  ]
  node [
    id 180
    label "urz&#261;dzenie"
  ]
  node [
    id 181
    label "ssa&#263;"
  ]
  node [
    id 182
    label "kod"
  ]
  node [
    id 183
    label "lizanie"
  ]
  node [
    id 184
    label "okre&#347;lony"
  ]
  node [
    id 185
    label "jaki&#347;"
  ]
  node [
    id 186
    label "success"
  ]
  node [
    id 187
    label "passa"
  ]
  node [
    id 188
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 189
    label "kobieta_sukcesu"
  ]
  node [
    id 190
    label "rezultat"
  ]
  node [
    id 191
    label "postawi&#263;"
  ]
  node [
    id 192
    label "prasa"
  ]
  node [
    id 193
    label "stworzy&#263;"
  ]
  node [
    id 194
    label "donie&#347;&#263;"
  ]
  node [
    id 195
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 196
    label "write"
  ]
  node [
    id 197
    label "styl"
  ]
  node [
    id 198
    label "read"
  ]
  node [
    id 199
    label "drama"
  ]
  node [
    id 200
    label "cios"
  ]
  node [
    id 201
    label "film"
  ]
  node [
    id 202
    label "rodzaj_literacki"
  ]
  node [
    id 203
    label "literatura"
  ]
  node [
    id 204
    label "si&#281;ga&#263;"
  ]
  node [
    id 205
    label "trwa&#263;"
  ]
  node [
    id 206
    label "obecno&#347;&#263;"
  ]
  node [
    id 207
    label "stan"
  ]
  node [
    id 208
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 209
    label "stand"
  ]
  node [
    id 210
    label "mie&#263;_miejsce"
  ]
  node [
    id 211
    label "uczestniczy&#263;"
  ]
  node [
    id 212
    label "chodzi&#263;"
  ]
  node [
    id 213
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 214
    label "equal"
  ]
  node [
    id 215
    label "gro&#378;ny"
  ]
  node [
    id 216
    label "k&#322;opotliwy"
  ]
  node [
    id 217
    label "niebezpiecznie"
  ]
  node [
    id 218
    label "energy"
  ]
  node [
    id 219
    label "czas"
  ]
  node [
    id 220
    label "bycie"
  ]
  node [
    id 221
    label "zegar_biologiczny"
  ]
  node [
    id 222
    label "okres_noworodkowy"
  ]
  node [
    id 223
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 224
    label "entity"
  ]
  node [
    id 225
    label "prze&#380;ywanie"
  ]
  node [
    id 226
    label "prze&#380;ycie"
  ]
  node [
    id 227
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 228
    label "wiek_matuzalemowy"
  ]
  node [
    id 229
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 230
    label "dzieci&#324;stwo"
  ]
  node [
    id 231
    label "power"
  ]
  node [
    id 232
    label "szwung"
  ]
  node [
    id 233
    label "menopauza"
  ]
  node [
    id 234
    label "umarcie"
  ]
  node [
    id 235
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 236
    label "life"
  ]
  node [
    id 237
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 238
    label "&#380;ywy"
  ]
  node [
    id 239
    label "rozw&#243;j"
  ]
  node [
    id 240
    label "po&#322;&#243;g"
  ]
  node [
    id 241
    label "byt"
  ]
  node [
    id 242
    label "przebywanie"
  ]
  node [
    id 243
    label "subsistence"
  ]
  node [
    id 244
    label "koleje_losu"
  ]
  node [
    id 245
    label "raj_utracony"
  ]
  node [
    id 246
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 247
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 248
    label "andropauza"
  ]
  node [
    id 249
    label "warunki"
  ]
  node [
    id 250
    label "do&#380;ywanie"
  ]
  node [
    id 251
    label "niemowl&#281;ctwo"
  ]
  node [
    id 252
    label "umieranie"
  ]
  node [
    id 253
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 254
    label "staro&#347;&#263;"
  ]
  node [
    id 255
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 256
    label "&#347;mier&#263;"
  ]
  node [
    id 257
    label "miernota"
  ]
  node [
    id 258
    label "g&#243;wno"
  ]
  node [
    id 259
    label "love"
  ]
  node [
    id 260
    label "brak"
  ]
  node [
    id 261
    label "ciura"
  ]
  node [
    id 262
    label "come_up"
  ]
  node [
    id 263
    label "straci&#263;"
  ]
  node [
    id 264
    label "przej&#347;&#263;"
  ]
  node [
    id 265
    label "zast&#261;pi&#263;"
  ]
  node [
    id 266
    label "sprawi&#263;"
  ]
  node [
    id 267
    label "zyska&#263;"
  ]
  node [
    id 268
    label "nabab"
  ]
  node [
    id 269
    label "forsiasty"
  ]
  node [
    id 270
    label "obfituj&#261;cy"
  ]
  node [
    id 271
    label "r&#243;&#380;norodny"
  ]
  node [
    id 272
    label "och&#281;do&#380;ny"
  ]
  node [
    id 273
    label "zapa&#347;ny"
  ]
  node [
    id 274
    label "obficie"
  ]
  node [
    id 275
    label "sytuowany"
  ]
  node [
    id 276
    label "spania&#322;y"
  ]
  node [
    id 277
    label "bogato"
  ]
  node [
    id 278
    label "zm&#261;drzenie"
  ]
  node [
    id 279
    label "skomplikowany"
  ]
  node [
    id 280
    label "m&#261;drzenie"
  ]
  node [
    id 281
    label "pyszny"
  ]
  node [
    id 282
    label "dobry"
  ]
  node [
    id 283
    label "m&#261;drze"
  ]
  node [
    id 284
    label "inteligentny"
  ]
  node [
    id 285
    label "m&#261;drzenie_si&#281;"
  ]
  node [
    id 286
    label "wypi&#281;knienie"
  ]
  node [
    id 287
    label "pi&#281;knienie"
  ]
  node [
    id 288
    label "szlachetnie"
  ]
  node [
    id 289
    label "po&#380;&#261;dany"
  ]
  node [
    id 290
    label "z&#322;y"
  ]
  node [
    id 291
    label "cudowny"
  ]
  node [
    id 292
    label "wspania&#322;y"
  ]
  node [
    id 293
    label "skandaliczny"
  ]
  node [
    id 294
    label "pi&#281;knie"
  ]
  node [
    id 295
    label "gor&#261;cy"
  ]
  node [
    id 296
    label "okaza&#322;y"
  ]
  node [
    id 297
    label "wzruszaj&#261;cy"
  ]
  node [
    id 298
    label "ci&#261;g&#322;y"
  ]
  node [
    id 299
    label "stale"
  ]
  node [
    id 300
    label "asymilowa&#263;"
  ]
  node [
    id 301
    label "wapniak"
  ]
  node [
    id 302
    label "dwun&#243;g"
  ]
  node [
    id 303
    label "polifag"
  ]
  node [
    id 304
    label "wz&#243;r"
  ]
  node [
    id 305
    label "profanum"
  ]
  node [
    id 306
    label "hominid"
  ]
  node [
    id 307
    label "homo_sapiens"
  ]
  node [
    id 308
    label "nasada"
  ]
  node [
    id 309
    label "podw&#322;adny"
  ]
  node [
    id 310
    label "ludzko&#347;&#263;"
  ]
  node [
    id 311
    label "os&#322;abianie"
  ]
  node [
    id 312
    label "mikrokosmos"
  ]
  node [
    id 313
    label "portrecista"
  ]
  node [
    id 314
    label "duch"
  ]
  node [
    id 315
    label "oddzia&#322;ywanie"
  ]
  node [
    id 316
    label "g&#322;owa"
  ]
  node [
    id 317
    label "asymilowanie"
  ]
  node [
    id 318
    label "osoba"
  ]
  node [
    id 319
    label "os&#322;abia&#263;"
  ]
  node [
    id 320
    label "figura"
  ]
  node [
    id 321
    label "Adam"
  ]
  node [
    id 322
    label "senior"
  ]
  node [
    id 323
    label "antropochoria"
  ]
  node [
    id 324
    label "posta&#263;"
  ]
  node [
    id 325
    label "wcze&#347;nie"
  ]
  node [
    id 326
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 327
    label "gra_planszowa"
  ]
  node [
    id 328
    label "szanowa&#263;"
  ]
  node [
    id 329
    label "prize"
  ]
  node [
    id 330
    label "appreciate"
  ]
  node [
    id 331
    label "uznawa&#263;"
  ]
  node [
    id 332
    label "liczy&#263;"
  ]
  node [
    id 333
    label "ustala&#263;"
  ]
  node [
    id 334
    label "Lem"
  ]
  node [
    id 335
    label "Stendhal"
  ]
  node [
    id 336
    label "Sienkiewicz"
  ]
  node [
    id 337
    label "Katon"
  ]
  node [
    id 338
    label "surogator"
  ]
  node [
    id 339
    label "Andersen"
  ]
  node [
    id 340
    label "Proust"
  ]
  node [
    id 341
    label "Iwaszkiewicz"
  ]
  node [
    id 342
    label "pisarczyk"
  ]
  node [
    id 343
    label "Walter_Scott"
  ]
  node [
    id 344
    label "Tolkien"
  ]
  node [
    id 345
    label "Conrad"
  ]
  node [
    id 346
    label "Juliusz_Cezar"
  ]
  node [
    id 347
    label "To&#322;stoj"
  ]
  node [
    id 348
    label "Bergson"
  ]
  node [
    id 349
    label "Brecht"
  ]
  node [
    id 350
    label "Flaubert"
  ]
  node [
    id 351
    label "Thomas_Mann"
  ]
  node [
    id 352
    label "Orwell"
  ]
  node [
    id 353
    label "Gogol"
  ]
  node [
    id 354
    label "Kafka"
  ]
  node [
    id 355
    label "artysta"
  ]
  node [
    id 356
    label "Machiavelli"
  ]
  node [
    id 357
    label "Reymont"
  ]
  node [
    id 358
    label "urz&#281;dnik"
  ]
  node [
    id 359
    label "Voltaire"
  ]
  node [
    id 360
    label "Boy-&#379;ele&#324;ski"
  ]
  node [
    id 361
    label "Balzak"
  ]
  node [
    id 362
    label "Gombrowicz"
  ]
  node [
    id 363
    label "Zagajewski"
  ]
  node [
    id 364
    label "Osjan"
  ]
  node [
    id 365
    label "Horacy"
  ]
  node [
    id 366
    label "Mi&#322;osz"
  ]
  node [
    id 367
    label "Schiller"
  ]
  node [
    id 368
    label "Rej"
  ]
  node [
    id 369
    label "Byron"
  ]
  node [
    id 370
    label "Jan_Czeczot"
  ]
  node [
    id 371
    label "Peiper"
  ]
  node [
    id 372
    label "Bara&#324;czak"
  ]
  node [
    id 373
    label "Asnyk"
  ]
  node [
    id 374
    label "Bia&#322;oszewski"
  ]
  node [
    id 375
    label "Herbert"
  ]
  node [
    id 376
    label "Dante"
  ]
  node [
    id 377
    label "Puszkin"
  ]
  node [
    id 378
    label "Le&#347;mian"
  ]
  node [
    id 379
    label "R&#243;&#380;ewicz"
  ]
  node [
    id 380
    label "Jesienin"
  ]
  node [
    id 381
    label "wierszopis"
  ]
  node [
    id 382
    label "Tuwim"
  ]
  node [
    id 383
    label "Norwid"
  ]
  node [
    id 384
    label "Homer"
  ]
  node [
    id 385
    label "Pindar"
  ]
  node [
    id 386
    label "Dawid"
  ]
  node [
    id 387
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 388
    label "da&#263;"
  ]
  node [
    id 389
    label "ponie&#347;&#263;"
  ]
  node [
    id 390
    label "get"
  ]
  node [
    id 391
    label "przytacha&#263;"
  ]
  node [
    id 392
    label "increase"
  ]
  node [
    id 393
    label "doda&#263;"
  ]
  node [
    id 394
    label "zanie&#347;&#263;"
  ]
  node [
    id 395
    label "poda&#263;"
  ]
  node [
    id 396
    label "carry"
  ]
  node [
    id 397
    label "profesor"
  ]
  node [
    id 398
    label "kszta&#322;ciciel"
  ]
  node [
    id 399
    label "jegomo&#347;&#263;"
  ]
  node [
    id 400
    label "zwrot"
  ]
  node [
    id 401
    label "pracodawca"
  ]
  node [
    id 402
    label "rz&#261;dzenie"
  ]
  node [
    id 403
    label "m&#261;&#380;"
  ]
  node [
    id 404
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 405
    label "ch&#322;opina"
  ]
  node [
    id 406
    label "bratek"
  ]
  node [
    id 407
    label "opiekun"
  ]
  node [
    id 408
    label "doros&#322;y"
  ]
  node [
    id 409
    label "preceptor"
  ]
  node [
    id 410
    label "Midas"
  ]
  node [
    id 411
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 412
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 413
    label "murza"
  ]
  node [
    id 414
    label "ojciec"
  ]
  node [
    id 415
    label "androlog"
  ]
  node [
    id 416
    label "pupil"
  ]
  node [
    id 417
    label "efendi"
  ]
  node [
    id 418
    label "w&#322;odarz"
  ]
  node [
    id 419
    label "szkolnik"
  ]
  node [
    id 420
    label "pedagog"
  ]
  node [
    id 421
    label "popularyzator"
  ]
  node [
    id 422
    label "gra_w_karty"
  ]
  node [
    id 423
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 424
    label "Mieszko_I"
  ]
  node [
    id 425
    label "samiec"
  ]
  node [
    id 426
    label "przyw&#243;dca"
  ]
  node [
    id 427
    label "pa&#324;stwo"
  ]
  node [
    id 428
    label "belfer"
  ]
  node [
    id 429
    label "rozg&#322;os"
  ]
  node [
    id 430
    label "renoma"
  ]
  node [
    id 431
    label "kto&#347;"
  ]
  node [
    id 432
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 433
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 434
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 435
    label "J&#225;nosem"
  ]
  node [
    id 436
    label "H&#225;yem"
  ]
  node [
    id 437
    label "G&#233;za"
  ]
  node [
    id 438
    label "TVP"
  ]
  node [
    id 439
    label "2"
  ]
  node [
    id 440
    label "J&#193;NOS"
  ]
  node [
    id 441
    label "H&#193;Y"
  ]
  node [
    id 442
    label "G&#233;z&#281;"
  ]
  node [
    id 443
    label "Sara"
  ]
  node [
    id 444
    label "Kane"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 42
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 70
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 83
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 20
    target 41
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 47
  ]
  edge [
    source 22
    target 48
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 38
  ]
  edge [
    source 22
    target 54
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 24
    target 201
  ]
  edge [
    source 24
    target 71
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 92
  ]
  edge [
    source 24
    target 93
  ]
  edge [
    source 24
    target 77
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 204
  ]
  edge [
    source 26
    target 205
  ]
  edge [
    source 26
    target 206
  ]
  edge [
    source 26
    target 207
  ]
  edge [
    source 26
    target 208
  ]
  edge [
    source 26
    target 209
  ]
  edge [
    source 26
    target 210
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 213
  ]
  edge [
    source 26
    target 214
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 216
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 27
    target 56
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 218
  ]
  edge [
    source 31
    target 219
  ]
  edge [
    source 31
    target 220
  ]
  edge [
    source 31
    target 221
  ]
  edge [
    source 31
    target 222
  ]
  edge [
    source 31
    target 223
  ]
  edge [
    source 31
    target 224
  ]
  edge [
    source 31
    target 225
  ]
  edge [
    source 31
    target 226
  ]
  edge [
    source 31
    target 227
  ]
  edge [
    source 31
    target 228
  ]
  edge [
    source 31
    target 229
  ]
  edge [
    source 31
    target 230
  ]
  edge [
    source 31
    target 231
  ]
  edge [
    source 31
    target 232
  ]
  edge [
    source 31
    target 233
  ]
  edge [
    source 31
    target 234
  ]
  edge [
    source 31
    target 235
  ]
  edge [
    source 31
    target 236
  ]
  edge [
    source 31
    target 237
  ]
  edge [
    source 31
    target 238
  ]
  edge [
    source 31
    target 239
  ]
  edge [
    source 31
    target 240
  ]
  edge [
    source 31
    target 241
  ]
  edge [
    source 31
    target 242
  ]
  edge [
    source 31
    target 243
  ]
  edge [
    source 31
    target 244
  ]
  edge [
    source 31
    target 245
  ]
  edge [
    source 31
    target 122
  ]
  edge [
    source 31
    target 246
  ]
  edge [
    source 31
    target 247
  ]
  edge [
    source 31
    target 248
  ]
  edge [
    source 31
    target 249
  ]
  edge [
    source 31
    target 250
  ]
  edge [
    source 31
    target 251
  ]
  edge [
    source 31
    target 252
  ]
  edge [
    source 31
    target 253
  ]
  edge [
    source 31
    target 254
  ]
  edge [
    source 31
    target 255
  ]
  edge [
    source 31
    target 256
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 257
  ]
  edge [
    source 32
    target 258
  ]
  edge [
    source 32
    target 259
  ]
  edge [
    source 32
    target 84
  ]
  edge [
    source 32
    target 260
  ]
  edge [
    source 32
    target 261
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 262
  ]
  edge [
    source 34
    target 263
  ]
  edge [
    source 34
    target 264
  ]
  edge [
    source 34
    target 265
  ]
  edge [
    source 34
    target 266
  ]
  edge [
    source 34
    target 267
  ]
  edge [
    source 34
    target 132
  ]
  edge [
    source 34
    target 141
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 42
  ]
  edge [
    source 37
    target 268
  ]
  edge [
    source 37
    target 269
  ]
  edge [
    source 37
    target 270
  ]
  edge [
    source 37
    target 271
  ]
  edge [
    source 37
    target 272
  ]
  edge [
    source 37
    target 273
  ]
  edge [
    source 37
    target 274
  ]
  edge [
    source 37
    target 275
  ]
  edge [
    source 37
    target 276
  ]
  edge [
    source 37
    target 277
  ]
  edge [
    source 37
    target 55
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 278
  ]
  edge [
    source 38
    target 279
  ]
  edge [
    source 38
    target 280
  ]
  edge [
    source 38
    target 281
  ]
  edge [
    source 38
    target 282
  ]
  edge [
    source 38
    target 283
  ]
  edge [
    source 38
    target 284
  ]
  edge [
    source 38
    target 285
  ]
  edge [
    source 38
    target 54
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 286
  ]
  edge [
    source 39
    target 287
  ]
  edge [
    source 39
    target 288
  ]
  edge [
    source 39
    target 289
  ]
  edge [
    source 39
    target 290
  ]
  edge [
    source 39
    target 291
  ]
  edge [
    source 39
    target 282
  ]
  edge [
    source 39
    target 292
  ]
  edge [
    source 39
    target 293
  ]
  edge [
    source 39
    target 294
  ]
  edge [
    source 39
    target 295
  ]
  edge [
    source 39
    target 296
  ]
  edge [
    source 39
    target 297
  ]
  edge [
    source 40
    target 298
  ]
  edge [
    source 40
    target 299
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 42
    target 300
  ]
  edge [
    source 42
    target 301
  ]
  edge [
    source 42
    target 302
  ]
  edge [
    source 42
    target 303
  ]
  edge [
    source 42
    target 304
  ]
  edge [
    source 42
    target 305
  ]
  edge [
    source 42
    target 306
  ]
  edge [
    source 42
    target 307
  ]
  edge [
    source 42
    target 308
  ]
  edge [
    source 42
    target 309
  ]
  edge [
    source 42
    target 310
  ]
  edge [
    source 42
    target 311
  ]
  edge [
    source 42
    target 312
  ]
  edge [
    source 42
    target 313
  ]
  edge [
    source 42
    target 314
  ]
  edge [
    source 42
    target 315
  ]
  edge [
    source 42
    target 316
  ]
  edge [
    source 42
    target 317
  ]
  edge [
    source 42
    target 318
  ]
  edge [
    source 42
    target 319
  ]
  edge [
    source 42
    target 320
  ]
  edge [
    source 42
    target 321
  ]
  edge [
    source 42
    target 322
  ]
  edge [
    source 42
    target 323
  ]
  edge [
    source 42
    target 324
  ]
  edge [
    source 42
    target 51
  ]
  edge [
    source 42
    target 55
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 325
  ]
  edge [
    source 46
    target 184
  ]
  edge [
    source 46
    target 326
  ]
  edge [
    source 47
    target 327
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 328
  ]
  edge [
    source 50
    target 329
  ]
  edge [
    source 50
    target 330
  ]
  edge [
    source 50
    target 331
  ]
  edge [
    source 50
    target 332
  ]
  edge [
    source 50
    target 333
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 334
  ]
  edge [
    source 51
    target 335
  ]
  edge [
    source 51
    target 336
  ]
  edge [
    source 51
    target 337
  ]
  edge [
    source 51
    target 338
  ]
  edge [
    source 51
    target 339
  ]
  edge [
    source 51
    target 340
  ]
  edge [
    source 51
    target 341
  ]
  edge [
    source 51
    target 342
  ]
  edge [
    source 51
    target 343
  ]
  edge [
    source 51
    target 344
  ]
  edge [
    source 51
    target 345
  ]
  edge [
    source 51
    target 346
  ]
  edge [
    source 51
    target 347
  ]
  edge [
    source 51
    target 348
  ]
  edge [
    source 51
    target 349
  ]
  edge [
    source 51
    target 350
  ]
  edge [
    source 51
    target 351
  ]
  edge [
    source 51
    target 352
  ]
  edge [
    source 51
    target 353
  ]
  edge [
    source 51
    target 354
  ]
  edge [
    source 51
    target 355
  ]
  edge [
    source 51
    target 356
  ]
  edge [
    source 51
    target 357
  ]
  edge [
    source 51
    target 358
  ]
  edge [
    source 51
    target 359
  ]
  edge [
    source 51
    target 360
  ]
  edge [
    source 51
    target 361
  ]
  edge [
    source 51
    target 362
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 363
  ]
  edge [
    source 52
    target 364
  ]
  edge [
    source 52
    target 365
  ]
  edge [
    source 52
    target 366
  ]
  edge [
    source 52
    target 367
  ]
  edge [
    source 52
    target 368
  ]
  edge [
    source 52
    target 369
  ]
  edge [
    source 52
    target 370
  ]
  edge [
    source 52
    target 371
  ]
  edge [
    source 52
    target 372
  ]
  edge [
    source 52
    target 373
  ]
  edge [
    source 52
    target 374
  ]
  edge [
    source 52
    target 375
  ]
  edge [
    source 52
    target 376
  ]
  edge [
    source 52
    target 377
  ]
  edge [
    source 52
    target 378
  ]
  edge [
    source 52
    target 379
  ]
  edge [
    source 52
    target 380
  ]
  edge [
    source 52
    target 381
  ]
  edge [
    source 52
    target 382
  ]
  edge [
    source 52
    target 383
  ]
  edge [
    source 52
    target 384
  ]
  edge [
    source 52
    target 385
  ]
  edge [
    source 52
    target 386
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 387
  ]
  edge [
    source 54
    target 388
  ]
  edge [
    source 54
    target 389
  ]
  edge [
    source 54
    target 390
  ]
  edge [
    source 54
    target 391
  ]
  edge [
    source 54
    target 392
  ]
  edge [
    source 54
    target 393
  ]
  edge [
    source 54
    target 394
  ]
  edge [
    source 54
    target 395
  ]
  edge [
    source 54
    target 396
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 397
  ]
  edge [
    source 55
    target 398
  ]
  edge [
    source 55
    target 399
  ]
  edge [
    source 55
    target 400
  ]
  edge [
    source 55
    target 401
  ]
  edge [
    source 55
    target 402
  ]
  edge [
    source 55
    target 403
  ]
  edge [
    source 55
    target 404
  ]
  edge [
    source 55
    target 405
  ]
  edge [
    source 55
    target 406
  ]
  edge [
    source 55
    target 407
  ]
  edge [
    source 55
    target 408
  ]
  edge [
    source 55
    target 409
  ]
  edge [
    source 55
    target 410
  ]
  edge [
    source 55
    target 411
  ]
  edge [
    source 55
    target 412
  ]
  edge [
    source 55
    target 413
  ]
  edge [
    source 55
    target 414
  ]
  edge [
    source 55
    target 415
  ]
  edge [
    source 55
    target 416
  ]
  edge [
    source 55
    target 417
  ]
  edge [
    source 55
    target 268
  ]
  edge [
    source 55
    target 418
  ]
  edge [
    source 55
    target 419
  ]
  edge [
    source 55
    target 420
  ]
  edge [
    source 55
    target 421
  ]
  edge [
    source 55
    target 248
  ]
  edge [
    source 55
    target 422
  ]
  edge [
    source 55
    target 423
  ]
  edge [
    source 55
    target 424
  ]
  edge [
    source 55
    target 425
  ]
  edge [
    source 55
    target 426
  ]
  edge [
    source 55
    target 427
  ]
  edge [
    source 55
    target 428
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 429
  ]
  edge [
    source 56
    target 430
  ]
  edge [
    source 56
    target 431
  ]
  edge [
    source 57
    target 432
  ]
  edge [
    source 57
    target 433
  ]
  edge [
    source 57
    target 434
  ]
  edge [
    source 435
    target 436
  ]
  edge [
    source 438
    target 439
  ]
  edge [
    source 440
    target 441
  ]
  edge [
    source 443
    target 444
  ]
]
