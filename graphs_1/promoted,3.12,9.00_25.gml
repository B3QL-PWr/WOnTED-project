graph [
  maxDegree 42
  minDegree 1
  meanDegree 2
  density 0.017543859649122806
  graphCliqueNumber 2
  node [
    id 0
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "badanie"
    origin "text"
  ]
  node [
    id 2
    label "por&#243;wnywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "stan"
    origin "text"
  ]
  node [
    id 4
    label "zdrowie"
    origin "text"
  ]
  node [
    id 5
    label "dziecko"
    origin "text"
  ]
  node [
    id 6
    label "szczepi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "stand"
  ]
  node [
    id 8
    label "usi&#322;owanie"
  ]
  node [
    id 9
    label "examination"
  ]
  node [
    id 10
    label "investigation"
  ]
  node [
    id 11
    label "ustalenie"
  ]
  node [
    id 12
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 13
    label "ustalanie"
  ]
  node [
    id 14
    label "bia&#322;a_niedziela"
  ]
  node [
    id 15
    label "analysis"
  ]
  node [
    id 16
    label "rozpatrywanie"
  ]
  node [
    id 17
    label "wziernikowanie"
  ]
  node [
    id 18
    label "obserwowanie"
  ]
  node [
    id 19
    label "omawianie"
  ]
  node [
    id 20
    label "sprawdzanie"
  ]
  node [
    id 21
    label "udowadnianie"
  ]
  node [
    id 22
    label "diagnostyka"
  ]
  node [
    id 23
    label "czynno&#347;&#263;"
  ]
  node [
    id 24
    label "macanie"
  ]
  node [
    id 25
    label "rektalny"
  ]
  node [
    id 26
    label "penetrowanie"
  ]
  node [
    id 27
    label "krytykowanie"
  ]
  node [
    id 28
    label "kontrola"
  ]
  node [
    id 29
    label "dociekanie"
  ]
  node [
    id 30
    label "zrecenzowanie"
  ]
  node [
    id 31
    label "praca"
  ]
  node [
    id 32
    label "rezultat"
  ]
  node [
    id 33
    label "analizowa&#263;"
  ]
  node [
    id 34
    label "szacowa&#263;"
  ]
  node [
    id 35
    label "Arizona"
  ]
  node [
    id 36
    label "Georgia"
  ]
  node [
    id 37
    label "warstwa"
  ]
  node [
    id 38
    label "jednostka_administracyjna"
  ]
  node [
    id 39
    label "Goa"
  ]
  node [
    id 40
    label "Hawaje"
  ]
  node [
    id 41
    label "Floryda"
  ]
  node [
    id 42
    label "Oklahoma"
  ]
  node [
    id 43
    label "punkt"
  ]
  node [
    id 44
    label "Alaska"
  ]
  node [
    id 45
    label "Alabama"
  ]
  node [
    id 46
    label "wci&#281;cie"
  ]
  node [
    id 47
    label "Oregon"
  ]
  node [
    id 48
    label "poziom"
  ]
  node [
    id 49
    label "by&#263;"
  ]
  node [
    id 50
    label "Teksas"
  ]
  node [
    id 51
    label "Illinois"
  ]
  node [
    id 52
    label "Jukatan"
  ]
  node [
    id 53
    label "Waszyngton"
  ]
  node [
    id 54
    label "shape"
  ]
  node [
    id 55
    label "Nowy_Meksyk"
  ]
  node [
    id 56
    label "ilo&#347;&#263;"
  ]
  node [
    id 57
    label "state"
  ]
  node [
    id 58
    label "Nowy_York"
  ]
  node [
    id 59
    label "Arakan"
  ]
  node [
    id 60
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 61
    label "Kalifornia"
  ]
  node [
    id 62
    label "wektor"
  ]
  node [
    id 63
    label "Massachusetts"
  ]
  node [
    id 64
    label "miejsce"
  ]
  node [
    id 65
    label "Pensylwania"
  ]
  node [
    id 66
    label "Maryland"
  ]
  node [
    id 67
    label "Michigan"
  ]
  node [
    id 68
    label "Ohio"
  ]
  node [
    id 69
    label "Kansas"
  ]
  node [
    id 70
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 71
    label "Luizjana"
  ]
  node [
    id 72
    label "samopoczucie"
  ]
  node [
    id 73
    label "Wirginia"
  ]
  node [
    id 74
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 75
    label "os&#322;abia&#263;"
  ]
  node [
    id 76
    label "niszczy&#263;"
  ]
  node [
    id 77
    label "zniszczy&#263;"
  ]
  node [
    id 78
    label "firmness"
  ]
  node [
    id 79
    label "kondycja"
  ]
  node [
    id 80
    label "os&#322;abi&#263;"
  ]
  node [
    id 81
    label "rozsypanie_si&#281;"
  ]
  node [
    id 82
    label "zdarcie"
  ]
  node [
    id 83
    label "cecha"
  ]
  node [
    id 84
    label "zniszczenie"
  ]
  node [
    id 85
    label "zedrze&#263;"
  ]
  node [
    id 86
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 87
    label "niszczenie"
  ]
  node [
    id 88
    label "os&#322;abienie"
  ]
  node [
    id 89
    label "soundness"
  ]
  node [
    id 90
    label "os&#322;abianie"
  ]
  node [
    id 91
    label "cz&#322;owiek"
  ]
  node [
    id 92
    label "potomstwo"
  ]
  node [
    id 93
    label "organizm"
  ]
  node [
    id 94
    label "sraluch"
  ]
  node [
    id 95
    label "utulanie"
  ]
  node [
    id 96
    label "pediatra"
  ]
  node [
    id 97
    label "dzieciarnia"
  ]
  node [
    id 98
    label "m&#322;odziak"
  ]
  node [
    id 99
    label "dzieciak"
  ]
  node [
    id 100
    label "utula&#263;"
  ]
  node [
    id 101
    label "potomek"
  ]
  node [
    id 102
    label "entliczek-pentliczek"
  ]
  node [
    id 103
    label "pedofil"
  ]
  node [
    id 104
    label "m&#322;odzik"
  ]
  node [
    id 105
    label "cz&#322;owieczek"
  ]
  node [
    id 106
    label "zwierz&#281;"
  ]
  node [
    id 107
    label "niepe&#322;noletni"
  ]
  node [
    id 108
    label "fledgling"
  ]
  node [
    id 109
    label "utuli&#263;"
  ]
  node [
    id 110
    label "utulenie"
  ]
  node [
    id 111
    label "inoculate"
  ]
  node [
    id 112
    label "uszlachetnia&#263;"
  ]
  node [
    id 113
    label "przyjmowa&#263;_si&#281;"
  ]
  node [
    id 114
    label "uodpornia&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 6
    target 6
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
]
