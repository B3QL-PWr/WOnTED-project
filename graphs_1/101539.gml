graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.157068062827225
  density 0.011352989804353817
  graphCliqueNumber 4
  node [
    id 0
    label "czas"
    origin "text"
  ]
  node [
    id 1
    label "wakacje"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "dobre"
    origin "text"
  ]
  node [
    id 4
    label "moment"
    origin "text"
  ]
  node [
    id 5
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 6
    label "uaktualnie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "opracowanie"
    origin "text"
  ]
  node [
    id 8
    label "nowa"
    origin "text"
  ]
  node [
    id 9
    label "funkcjonalno&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "uczelniany"
    origin "text"
  ]
  node [
    id 11
    label "platforma"
    origin "text"
  ]
  node [
    id 12
    label "learningowej"
    origin "text"
  ]
  node [
    id 13
    label "jeden"
    origin "text"
  ]
  node [
    id 14
    label "dostosowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "moodle"
    origin "text"
  ]
  node [
    id 16
    label "dla"
    origin "text"
  ]
  node [
    id 17
    label "osoba"
    origin "text"
  ]
  node [
    id 18
    label "s&#322;abowidz&#261;ca"
    origin "text"
  ]
  node [
    id 19
    label "inny"
    origin "text"
  ]
  node [
    id 20
    label "wkr&#243;tce"
    origin "text"
  ]
  node [
    id 21
    label "dostosowywa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "potrzeb"
    origin "text"
  ]
  node [
    id 23
    label "przyj&#261;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 25
    label "za&#322;o&#380;enia"
    origin "text"
  ]
  node [
    id 26
    label "czasokres"
  ]
  node [
    id 27
    label "trawienie"
  ]
  node [
    id 28
    label "kategoria_gramatyczna"
  ]
  node [
    id 29
    label "period"
  ]
  node [
    id 30
    label "odczyt"
  ]
  node [
    id 31
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 32
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 33
    label "chwila"
  ]
  node [
    id 34
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 35
    label "poprzedzenie"
  ]
  node [
    id 36
    label "koniugacja"
  ]
  node [
    id 37
    label "dzieje"
  ]
  node [
    id 38
    label "poprzedzi&#263;"
  ]
  node [
    id 39
    label "przep&#322;ywanie"
  ]
  node [
    id 40
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 41
    label "odwlekanie_si&#281;"
  ]
  node [
    id 42
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 43
    label "Zeitgeist"
  ]
  node [
    id 44
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 45
    label "okres_czasu"
  ]
  node [
    id 46
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 47
    label "pochodzi&#263;"
  ]
  node [
    id 48
    label "schy&#322;ek"
  ]
  node [
    id 49
    label "czwarty_wymiar"
  ]
  node [
    id 50
    label "chronometria"
  ]
  node [
    id 51
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 52
    label "poprzedzanie"
  ]
  node [
    id 53
    label "pogoda"
  ]
  node [
    id 54
    label "zegar"
  ]
  node [
    id 55
    label "pochodzenie"
  ]
  node [
    id 56
    label "poprzedza&#263;"
  ]
  node [
    id 57
    label "trawi&#263;"
  ]
  node [
    id 58
    label "time_period"
  ]
  node [
    id 59
    label "rachuba_czasu"
  ]
  node [
    id 60
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 61
    label "czasoprzestrze&#324;"
  ]
  node [
    id 62
    label "laba"
  ]
  node [
    id 63
    label "urlop"
  ]
  node [
    id 64
    label "czas_wolny"
  ]
  node [
    id 65
    label "rok_akademicki"
  ]
  node [
    id 66
    label "rok_szkolny"
  ]
  node [
    id 67
    label "si&#281;ga&#263;"
  ]
  node [
    id 68
    label "trwa&#263;"
  ]
  node [
    id 69
    label "obecno&#347;&#263;"
  ]
  node [
    id 70
    label "stan"
  ]
  node [
    id 71
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 72
    label "stand"
  ]
  node [
    id 73
    label "mie&#263;_miejsce"
  ]
  node [
    id 74
    label "uczestniczy&#263;"
  ]
  node [
    id 75
    label "chodzi&#263;"
  ]
  node [
    id 76
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 77
    label "equal"
  ]
  node [
    id 78
    label "minute"
  ]
  node [
    id 79
    label "jednostka_geologiczna"
  ]
  node [
    id 80
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 81
    label "time"
  ]
  node [
    id 82
    label "chron"
  ]
  node [
    id 83
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 84
    label "fragment"
  ]
  node [
    id 85
    label "rynek"
  ]
  node [
    id 86
    label "issue"
  ]
  node [
    id 87
    label "evocation"
  ]
  node [
    id 88
    label "wst&#281;p"
  ]
  node [
    id 89
    label "nuklearyzacja"
  ]
  node [
    id 90
    label "umo&#380;liwienie"
  ]
  node [
    id 91
    label "zacz&#281;cie"
  ]
  node [
    id 92
    label "wpisanie"
  ]
  node [
    id 93
    label "zapoznanie"
  ]
  node [
    id 94
    label "zrobienie"
  ]
  node [
    id 95
    label "czynno&#347;&#263;"
  ]
  node [
    id 96
    label "entrance"
  ]
  node [
    id 97
    label "wej&#347;cie"
  ]
  node [
    id 98
    label "podstawy"
  ]
  node [
    id 99
    label "spowodowanie"
  ]
  node [
    id 100
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 101
    label "w&#322;&#261;czenie"
  ]
  node [
    id 102
    label "doprowadzenie"
  ]
  node [
    id 103
    label "przewietrzenie"
  ]
  node [
    id 104
    label "deduction"
  ]
  node [
    id 105
    label "umieszczenie"
  ]
  node [
    id 106
    label "rozprawa"
  ]
  node [
    id 107
    label "paper"
  ]
  node [
    id 108
    label "przygotowanie"
  ]
  node [
    id 109
    label "tekst"
  ]
  node [
    id 110
    label "gwiazda"
  ]
  node [
    id 111
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 112
    label "u&#380;ytkowo&#347;&#263;"
  ]
  node [
    id 113
    label "functionality"
  ]
  node [
    id 114
    label "praktyczno&#347;&#263;"
  ]
  node [
    id 115
    label "akademicki"
  ]
  node [
    id 116
    label "koturn"
  ]
  node [
    id 117
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 118
    label "skorupa_ziemska"
  ]
  node [
    id 119
    label "but"
  ]
  node [
    id 120
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 121
    label "podeszwa"
  ]
  node [
    id 122
    label "nadwozie"
  ]
  node [
    id 123
    label "sfera"
  ]
  node [
    id 124
    label "struktura"
  ]
  node [
    id 125
    label "p&#322;aszczyzna"
  ]
  node [
    id 126
    label "kieliszek"
  ]
  node [
    id 127
    label "shot"
  ]
  node [
    id 128
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 129
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 130
    label "jaki&#347;"
  ]
  node [
    id 131
    label "jednolicie"
  ]
  node [
    id 132
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 133
    label "w&#243;dka"
  ]
  node [
    id 134
    label "ten"
  ]
  node [
    id 135
    label "ujednolicenie"
  ]
  node [
    id 136
    label "jednakowy"
  ]
  node [
    id 137
    label "adjust"
  ]
  node [
    id 138
    label "zmieni&#263;"
  ]
  node [
    id 139
    label "Zgredek"
  ]
  node [
    id 140
    label "Casanova"
  ]
  node [
    id 141
    label "Don_Juan"
  ]
  node [
    id 142
    label "Gargantua"
  ]
  node [
    id 143
    label "Faust"
  ]
  node [
    id 144
    label "profanum"
  ]
  node [
    id 145
    label "Chocho&#322;"
  ]
  node [
    id 146
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 147
    label "Winnetou"
  ]
  node [
    id 148
    label "Dwukwiat"
  ]
  node [
    id 149
    label "homo_sapiens"
  ]
  node [
    id 150
    label "Edyp"
  ]
  node [
    id 151
    label "Herkules_Poirot"
  ]
  node [
    id 152
    label "ludzko&#347;&#263;"
  ]
  node [
    id 153
    label "mikrokosmos"
  ]
  node [
    id 154
    label "person"
  ]
  node [
    id 155
    label "Szwejk"
  ]
  node [
    id 156
    label "portrecista"
  ]
  node [
    id 157
    label "Sherlock_Holmes"
  ]
  node [
    id 158
    label "Hamlet"
  ]
  node [
    id 159
    label "duch"
  ]
  node [
    id 160
    label "oddzia&#322;ywanie"
  ]
  node [
    id 161
    label "g&#322;owa"
  ]
  node [
    id 162
    label "Quasimodo"
  ]
  node [
    id 163
    label "Dulcynea"
  ]
  node [
    id 164
    label "Wallenrod"
  ]
  node [
    id 165
    label "Don_Kiszot"
  ]
  node [
    id 166
    label "Plastu&#347;"
  ]
  node [
    id 167
    label "Harry_Potter"
  ]
  node [
    id 168
    label "figura"
  ]
  node [
    id 169
    label "parali&#380;owa&#263;"
  ]
  node [
    id 170
    label "istota"
  ]
  node [
    id 171
    label "Werter"
  ]
  node [
    id 172
    label "antropochoria"
  ]
  node [
    id 173
    label "posta&#263;"
  ]
  node [
    id 174
    label "kolejny"
  ]
  node [
    id 175
    label "inaczej"
  ]
  node [
    id 176
    label "r&#243;&#380;ny"
  ]
  node [
    id 177
    label "inszy"
  ]
  node [
    id 178
    label "osobno"
  ]
  node [
    id 179
    label "wpr&#281;dce"
  ]
  node [
    id 180
    label "blisko"
  ]
  node [
    id 181
    label "nied&#322;ugi"
  ]
  node [
    id 182
    label "zmienia&#263;"
  ]
  node [
    id 183
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 184
    label "okre&#347;lony"
  ]
  node [
    id 185
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 186
    label "e"
  ]
  node [
    id 187
    label "Learningowej"
  ]
  node [
    id 188
    label "XHTML"
  ]
  node [
    id 189
    label "1"
  ]
  node [
    id 190
    label "0"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 25
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 36
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 77
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 186
    target 187
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 188
    target 190
  ]
  edge [
    source 189
    target 190
  ]
]
