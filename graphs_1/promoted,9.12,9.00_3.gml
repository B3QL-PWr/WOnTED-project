graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 3
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "traktowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "protestuj&#261;cy"
    origin "text"
  ]
  node [
    id 4
    label "obywatel"
    origin "text"
  ]
  node [
    id 5
    label "francja"
    origin "text"
  ]
  node [
    id 6
    label "use"
  ]
  node [
    id 7
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 8
    label "robi&#263;"
  ]
  node [
    id 9
    label "dotyczy&#263;"
  ]
  node [
    id 10
    label "poddawa&#263;"
  ]
  node [
    id 11
    label "si&#281;ga&#263;"
  ]
  node [
    id 12
    label "trwa&#263;"
  ]
  node [
    id 13
    label "obecno&#347;&#263;"
  ]
  node [
    id 14
    label "stan"
  ]
  node [
    id 15
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 16
    label "stand"
  ]
  node [
    id 17
    label "mie&#263;_miejsce"
  ]
  node [
    id 18
    label "uczestniczy&#263;"
  ]
  node [
    id 19
    label "chodzi&#263;"
  ]
  node [
    id 20
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 21
    label "equal"
  ]
  node [
    id 22
    label "demonstrant"
  ]
  node [
    id 23
    label "pikieta"
  ]
  node [
    id 24
    label "cz&#322;owiek"
  ]
  node [
    id 25
    label "przedstawiciel"
  ]
  node [
    id 26
    label "miastowy"
  ]
  node [
    id 27
    label "mieszkaniec"
  ]
  node [
    id 28
    label "pa&#324;stwo"
  ]
  node [
    id 29
    label "unia"
  ]
  node [
    id 30
    label "europejski"
  ]
  node [
    id 31
    label "razem"
  ]
  node [
    id 32
    label "prawa"
  ]
  node [
    id 33
    label "komisja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 33
  ]
]
