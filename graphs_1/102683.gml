graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9622641509433962
  density 0.03773584905660377
  graphCliqueNumber 2
  node [
    id 0
    label "dzienia"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "przy"
    origin "text"
  ]
  node [
    id 3
    label "telefon"
    origin "text"
  ]
  node [
    id 4
    label "xxx"
    origin "text"
  ]
  node [
    id 5
    label "s&#322;ucha&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pomy&#347;lny"
  ]
  node [
    id 7
    label "skuteczny"
  ]
  node [
    id 8
    label "moralny"
  ]
  node [
    id 9
    label "korzystny"
  ]
  node [
    id 10
    label "odpowiedni"
  ]
  node [
    id 11
    label "zwrot"
  ]
  node [
    id 12
    label "dobrze"
  ]
  node [
    id 13
    label "pozytywny"
  ]
  node [
    id 14
    label "grzeczny"
  ]
  node [
    id 15
    label "powitanie"
  ]
  node [
    id 16
    label "mi&#322;y"
  ]
  node [
    id 17
    label "dobroczynny"
  ]
  node [
    id 18
    label "pos&#322;uszny"
  ]
  node [
    id 19
    label "ca&#322;y"
  ]
  node [
    id 20
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 21
    label "czw&#243;rka"
  ]
  node [
    id 22
    label "spokojny"
  ]
  node [
    id 23
    label "&#347;mieszny"
  ]
  node [
    id 24
    label "drogi"
  ]
  node [
    id 25
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 26
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 27
    label "coalescence"
  ]
  node [
    id 28
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 29
    label "phreaker"
  ]
  node [
    id 30
    label "infrastruktura"
  ]
  node [
    id 31
    label "wy&#347;wietlacz"
  ]
  node [
    id 32
    label "provider"
  ]
  node [
    id 33
    label "dzwonienie"
  ]
  node [
    id 34
    label "zadzwoni&#263;"
  ]
  node [
    id 35
    label "dzwoni&#263;"
  ]
  node [
    id 36
    label "kontakt"
  ]
  node [
    id 37
    label "mikrotelefon"
  ]
  node [
    id 38
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 39
    label "po&#322;&#261;czenie"
  ]
  node [
    id 40
    label "numer"
  ]
  node [
    id 41
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 42
    label "instalacja"
  ]
  node [
    id 43
    label "billing"
  ]
  node [
    id 44
    label "urz&#261;dzenie"
  ]
  node [
    id 45
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 46
    label "continue"
  ]
  node [
    id 47
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 48
    label "lubi&#263;"
  ]
  node [
    id 49
    label "odbiera&#263;"
  ]
  node [
    id 50
    label "wybiera&#263;"
  ]
  node [
    id 51
    label "odtwarza&#263;"
  ]
  node [
    id 52
    label "nat&#281;&#380;a&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
]
