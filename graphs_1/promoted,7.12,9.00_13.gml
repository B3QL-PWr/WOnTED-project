graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9642857142857142
  density 0.03571428571428571
  graphCliqueNumber 2
  node [
    id 0
    label "kra&#347;ko"
    origin "text"
  ]
  node [
    id 1
    label "reszta"
    origin "text"
  ]
  node [
    id 2
    label "ekipa"
    origin "text"
  ]
  node [
    id 3
    label "musza"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "bardziej"
    origin "text"
  ]
  node [
    id 6
    label "postara&#263;"
    origin "text"
  ]
  node [
    id 7
    label "komentarz"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "sam"
    origin "text"
  ]
  node [
    id 10
    label "siebie"
    origin "text"
  ]
  node [
    id 11
    label "remainder"
  ]
  node [
    id 12
    label "wydanie"
  ]
  node [
    id 13
    label "wyda&#263;"
  ]
  node [
    id 14
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 15
    label "wydawa&#263;"
  ]
  node [
    id 16
    label "pozosta&#322;y"
  ]
  node [
    id 17
    label "kwota"
  ]
  node [
    id 18
    label "zesp&#243;&#322;"
  ]
  node [
    id 19
    label "grupa"
  ]
  node [
    id 20
    label "dublet"
  ]
  node [
    id 21
    label "force"
  ]
  node [
    id 22
    label "comment"
  ]
  node [
    id 23
    label "artyku&#322;"
  ]
  node [
    id 24
    label "ocena"
  ]
  node [
    id 25
    label "gossip"
  ]
  node [
    id 26
    label "interpretacja"
  ]
  node [
    id 27
    label "audycja"
  ]
  node [
    id 28
    label "tekst"
  ]
  node [
    id 29
    label "remark"
  ]
  node [
    id 30
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 31
    label "u&#380;ywa&#263;"
  ]
  node [
    id 32
    label "okre&#347;la&#263;"
  ]
  node [
    id 33
    label "j&#281;zyk"
  ]
  node [
    id 34
    label "say"
  ]
  node [
    id 35
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 36
    label "formu&#322;owa&#263;"
  ]
  node [
    id 37
    label "talk"
  ]
  node [
    id 38
    label "powiada&#263;"
  ]
  node [
    id 39
    label "informowa&#263;"
  ]
  node [
    id 40
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 41
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 42
    label "wydobywa&#263;"
  ]
  node [
    id 43
    label "express"
  ]
  node [
    id 44
    label "chew_the_fat"
  ]
  node [
    id 45
    label "dysfonia"
  ]
  node [
    id 46
    label "umie&#263;"
  ]
  node [
    id 47
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 48
    label "tell"
  ]
  node [
    id 49
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 50
    label "wyra&#380;a&#263;"
  ]
  node [
    id 51
    label "gaworzy&#263;"
  ]
  node [
    id 52
    label "rozmawia&#263;"
  ]
  node [
    id 53
    label "dziama&#263;"
  ]
  node [
    id 54
    label "prawi&#263;"
  ]
  node [
    id 55
    label "sklep"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 55
  ]
]
