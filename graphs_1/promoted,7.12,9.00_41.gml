graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.018867924528302
  density 0.01922731356693621
  graphCliqueNumber 2
  node [
    id 0
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kampania"
    origin "text"
  ]
  node [
    id 2
    label "rosyjski"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 5
    label "wspomnie&#263;"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 7
    label "bitwa"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 10
    label "miejsce"
    origin "text"
  ]
  node [
    id 11
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 12
    label "pod"
    origin "text"
  ]
  node [
    id 13
    label "borodino"
    origin "text"
  ]
  node [
    id 14
    label "remark"
  ]
  node [
    id 15
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 16
    label "u&#380;ywa&#263;"
  ]
  node [
    id 17
    label "okre&#347;la&#263;"
  ]
  node [
    id 18
    label "j&#281;zyk"
  ]
  node [
    id 19
    label "say"
  ]
  node [
    id 20
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 21
    label "formu&#322;owa&#263;"
  ]
  node [
    id 22
    label "talk"
  ]
  node [
    id 23
    label "powiada&#263;"
  ]
  node [
    id 24
    label "informowa&#263;"
  ]
  node [
    id 25
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 26
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 27
    label "wydobywa&#263;"
  ]
  node [
    id 28
    label "express"
  ]
  node [
    id 29
    label "chew_the_fat"
  ]
  node [
    id 30
    label "dysfonia"
  ]
  node [
    id 31
    label "umie&#263;"
  ]
  node [
    id 32
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 33
    label "tell"
  ]
  node [
    id 34
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 35
    label "wyra&#380;a&#263;"
  ]
  node [
    id 36
    label "gaworzy&#263;"
  ]
  node [
    id 37
    label "rozmawia&#263;"
  ]
  node [
    id 38
    label "dziama&#263;"
  ]
  node [
    id 39
    label "prawi&#263;"
  ]
  node [
    id 40
    label "dzia&#322;anie"
  ]
  node [
    id 41
    label "campaign"
  ]
  node [
    id 42
    label "wydarzenie"
  ]
  node [
    id 43
    label "akcja"
  ]
  node [
    id 44
    label "Kampania_Wrze&#347;niowa"
  ]
  node [
    id 45
    label "po_rosyjsku"
  ]
  node [
    id 46
    label "wielkoruski"
  ]
  node [
    id 47
    label "kacapski"
  ]
  node [
    id 48
    label "Russian"
  ]
  node [
    id 49
    label "rusek"
  ]
  node [
    id 50
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 51
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 52
    label "stulecie"
  ]
  node [
    id 53
    label "kalendarz"
  ]
  node [
    id 54
    label "czas"
  ]
  node [
    id 55
    label "pora_roku"
  ]
  node [
    id 56
    label "cykl_astronomiczny"
  ]
  node [
    id 57
    label "p&#243;&#322;rocze"
  ]
  node [
    id 58
    label "grupa"
  ]
  node [
    id 59
    label "kwarta&#322;"
  ]
  node [
    id 60
    label "kurs"
  ]
  node [
    id 61
    label "jubileusz"
  ]
  node [
    id 62
    label "miesi&#261;c"
  ]
  node [
    id 63
    label "lata"
  ]
  node [
    id 64
    label "martwy_sezon"
  ]
  node [
    id 65
    label "model"
  ]
  node [
    id 66
    label "zbi&#243;r"
  ]
  node [
    id 67
    label "tryb"
  ]
  node [
    id 68
    label "narz&#281;dzie"
  ]
  node [
    id 69
    label "nature"
  ]
  node [
    id 70
    label "doda&#263;"
  ]
  node [
    id 71
    label "pomy&#347;le&#263;"
  ]
  node [
    id 72
    label "mention"
  ]
  node [
    id 73
    label "hint"
  ]
  node [
    id 74
    label "doros&#322;y"
  ]
  node [
    id 75
    label "wiele"
  ]
  node [
    id 76
    label "dorodny"
  ]
  node [
    id 77
    label "znaczny"
  ]
  node [
    id 78
    label "du&#380;o"
  ]
  node [
    id 79
    label "prawdziwy"
  ]
  node [
    id 80
    label "niema&#322;o"
  ]
  node [
    id 81
    label "wa&#380;ny"
  ]
  node [
    id 82
    label "rozwini&#281;ty"
  ]
  node [
    id 83
    label "zaj&#347;cie"
  ]
  node [
    id 84
    label "batalista"
  ]
  node [
    id 85
    label "walka"
  ]
  node [
    id 86
    label "action"
  ]
  node [
    id 87
    label "bitwa_pod_Pi&#322;awcami"
  ]
  node [
    id 88
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 89
    label "czu&#263;"
  ]
  node [
    id 90
    label "need"
  ]
  node [
    id 91
    label "hide"
  ]
  node [
    id 92
    label "support"
  ]
  node [
    id 93
    label "cia&#322;o"
  ]
  node [
    id 94
    label "plac"
  ]
  node [
    id 95
    label "cecha"
  ]
  node [
    id 96
    label "uwaga"
  ]
  node [
    id 97
    label "przestrze&#324;"
  ]
  node [
    id 98
    label "status"
  ]
  node [
    id 99
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 100
    label "chwila"
  ]
  node [
    id 101
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 102
    label "rz&#261;d"
  ]
  node [
    id 103
    label "praca"
  ]
  node [
    id 104
    label "location"
  ]
  node [
    id 105
    label "warunek_lokalowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 12
    target 13
  ]
]
