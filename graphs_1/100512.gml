graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.5
  density 0.13636363636363635
  graphCliqueNumber 3
  node [
    id 0
    label "&#382;arko"
    origin "text"
  ]
  node [
    id 1
    label "&#269;abarkapa"
    origin "text"
  ]
  node [
    id 2
    label "&#381;arko"
  ]
  node [
    id 3
    label "&#268;abarkapa"
  ]
  node [
    id 4
    label "Phoenix"
  ]
  node [
    id 5
    label "Suns"
  ]
  node [
    id 6
    label "Golden"
  ]
  node [
    id 7
    label "State"
  ]
  node [
    id 8
    label "Warriors"
  ]
  node [
    id 9
    label "KK"
  ]
  node [
    id 10
    label "Buducnost"
  ]
  node [
    id 11
    label "Podgorica"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
]
