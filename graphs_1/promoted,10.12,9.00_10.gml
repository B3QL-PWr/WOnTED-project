graph [
  maxDegree 25
  minDegree 1
  meanDegree 2
  density 0.01694915254237288
  graphCliqueNumber 2
  node [
    id 0
    label "ch&#322;opak"
    origin "text"
  ]
  node [
    id 1
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dupa"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ambitny"
    origin "text"
  ]
  node [
    id 5
    label "rozbudowywa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "swoje"
    origin "text"
  ]
  node [
    id 7
    label "portfolio"
    origin "text"
  ]
  node [
    id 8
    label "pogardzi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "dodatkowy"
    origin "text"
  ]
  node [
    id 10
    label "robota"
    origin "text"
  ]
  node [
    id 11
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "pomocnik"
  ]
  node [
    id 14
    label "g&#243;wniarz"
  ]
  node [
    id 15
    label "&#347;l&#261;ski"
  ]
  node [
    id 16
    label "m&#322;odzieniec"
  ]
  node [
    id 17
    label "kajtek"
  ]
  node [
    id 18
    label "kawaler"
  ]
  node [
    id 19
    label "usynawianie"
  ]
  node [
    id 20
    label "dziecko"
  ]
  node [
    id 21
    label "okrzos"
  ]
  node [
    id 22
    label "usynowienie"
  ]
  node [
    id 23
    label "sympatia"
  ]
  node [
    id 24
    label "pederasta"
  ]
  node [
    id 25
    label "synek"
  ]
  node [
    id 26
    label "boyfriend"
  ]
  node [
    id 27
    label "trwa&#263;"
  ]
  node [
    id 28
    label "sit"
  ]
  node [
    id 29
    label "pause"
  ]
  node [
    id 30
    label "ptak"
  ]
  node [
    id 31
    label "garowa&#263;"
  ]
  node [
    id 32
    label "mieszka&#263;"
  ]
  node [
    id 33
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 34
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 35
    label "przebywa&#263;"
  ]
  node [
    id 36
    label "brood"
  ]
  node [
    id 37
    label "zwierz&#281;"
  ]
  node [
    id 38
    label "doprowadza&#263;"
  ]
  node [
    id 39
    label "spoczywa&#263;"
  ]
  node [
    id 40
    label "tkwi&#263;"
  ]
  node [
    id 41
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 42
    label "dusza_wo&#322;owa"
  ]
  node [
    id 43
    label "srom"
  ]
  node [
    id 44
    label "kobieta"
  ]
  node [
    id 45
    label "kochanka"
  ]
  node [
    id 46
    label "oferma"
  ]
  node [
    id 47
    label "pupa"
  ]
  node [
    id 48
    label "sk&#243;ra"
  ]
  node [
    id 49
    label "si&#281;ga&#263;"
  ]
  node [
    id 50
    label "obecno&#347;&#263;"
  ]
  node [
    id 51
    label "stan"
  ]
  node [
    id 52
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 53
    label "stand"
  ]
  node [
    id 54
    label "mie&#263;_miejsce"
  ]
  node [
    id 55
    label "uczestniczy&#263;"
  ]
  node [
    id 56
    label "chodzi&#263;"
  ]
  node [
    id 57
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 58
    label "equal"
  ]
  node [
    id 59
    label "trudny"
  ]
  node [
    id 60
    label "wymagaj&#261;cy"
  ]
  node [
    id 61
    label "wysokich_lot&#243;w"
  ]
  node [
    id 62
    label "zdeterminowany"
  ]
  node [
    id 63
    label "&#347;mia&#322;y"
  ]
  node [
    id 64
    label "ambitnie"
  ]
  node [
    id 65
    label "samodzielny"
  ]
  node [
    id 66
    label "ci&#281;&#380;ki"
  ]
  node [
    id 67
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 68
    label "extend"
  ]
  node [
    id 69
    label "rozwija&#263;"
  ]
  node [
    id 70
    label "rozszerza&#263;"
  ]
  node [
    id 71
    label "enlarge"
  ]
  node [
    id 72
    label "teka"
  ]
  node [
    id 73
    label "zlekcewa&#380;y&#263;"
  ]
  node [
    id 74
    label "odrzuci&#263;"
  ]
  node [
    id 75
    label "potraktowa&#263;"
  ]
  node [
    id 76
    label "wyrzec_si&#281;"
  ]
  node [
    id 77
    label "disregard"
  ]
  node [
    id 78
    label "dismiss"
  ]
  node [
    id 79
    label "poboczny"
  ]
  node [
    id 80
    label "dodatkowo"
  ]
  node [
    id 81
    label "uboczny"
  ]
  node [
    id 82
    label "stosunek_pracy"
  ]
  node [
    id 83
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 84
    label "benedykty&#324;ski"
  ]
  node [
    id 85
    label "pracowanie"
  ]
  node [
    id 86
    label "zaw&#243;d"
  ]
  node [
    id 87
    label "kierownictwo"
  ]
  node [
    id 88
    label "zmiana"
  ]
  node [
    id 89
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 90
    label "tynkarski"
  ]
  node [
    id 91
    label "czynnik_produkcji"
  ]
  node [
    id 92
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 93
    label "zobowi&#261;zanie"
  ]
  node [
    id 94
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 95
    label "czynno&#347;&#263;"
  ]
  node [
    id 96
    label "tyrka"
  ]
  node [
    id 97
    label "pracowa&#263;"
  ]
  node [
    id 98
    label "siedziba"
  ]
  node [
    id 99
    label "poda&#380;_pracy"
  ]
  node [
    id 100
    label "miejsce"
  ]
  node [
    id 101
    label "zak&#322;ad"
  ]
  node [
    id 102
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 103
    label "najem"
  ]
  node [
    id 104
    label "praca"
  ]
  node [
    id 105
    label "zorganizowa&#263;"
  ]
  node [
    id 106
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 107
    label "przerobi&#263;"
  ]
  node [
    id 108
    label "wystylizowa&#263;"
  ]
  node [
    id 109
    label "cause"
  ]
  node [
    id 110
    label "wydali&#263;"
  ]
  node [
    id 111
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 112
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 113
    label "post&#261;pi&#263;"
  ]
  node [
    id 114
    label "appoint"
  ]
  node [
    id 115
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 116
    label "nabra&#263;"
  ]
  node [
    id 117
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 118
    label "make"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
]
