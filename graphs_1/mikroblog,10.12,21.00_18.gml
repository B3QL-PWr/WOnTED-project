graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.1405405405405404
  density 0.01163337250293772
  graphCliqueNumber 4
  node [
    id 0
    label "zlodzieje"
    origin "text"
  ]
  node [
    id 1
    label "kradziez"
    origin "text"
  ]
  node [
    id 2
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 3
    label "wojsko"
    origin "text"
  ]
  node [
    id 4
    label "prokuratura"
    origin "text"
  ]
  node [
    id 5
    label "policja"
    origin "text"
  ]
  node [
    id 6
    label "prawo"
    origin "text"
  ]
  node [
    id 7
    label "pytaniedoeksperta"
    origin "text"
  ]
  node [
    id 8
    label "kradzie&#380;"
    origin "text"
  ]
  node [
    id 9
    label "osobowy"
    origin "text"
  ]
  node [
    id 10
    label "cywilny"
    origin "text"
  ]
  node [
    id 11
    label "osa"
    origin "text"
  ]
  node [
    id 12
    label "fizyczny"
    origin "text"
  ]
  node [
    id 13
    label "jakims"
    origin "text"
  ]
  node [
    id 14
    label "miesiacu"
    origin "text"
  ]
  node [
    id 15
    label "sprawa"
    origin "text"
  ]
  node [
    id 16
    label "le&#380;&#261;ca"
    origin "text"
  ]
  node [
    id 17
    label "trafila"
    origin "text"
  ]
  node [
    id 18
    label "wojskowy"
    origin "text"
  ]
  node [
    id 19
    label "baga&#380;nik"
  ]
  node [
    id 20
    label "immobilizer"
  ]
  node [
    id 21
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 22
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 23
    label "poduszka_powietrzna"
  ]
  node [
    id 24
    label "dachowanie"
  ]
  node [
    id 25
    label "dwu&#347;lad"
  ]
  node [
    id 26
    label "deska_rozdzielcza"
  ]
  node [
    id 27
    label "poci&#261;g_drogowy"
  ]
  node [
    id 28
    label "kierownica"
  ]
  node [
    id 29
    label "pojazd_drogowy"
  ]
  node [
    id 30
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 31
    label "pompa_wodna"
  ]
  node [
    id 32
    label "silnik"
  ]
  node [
    id 33
    label "wycieraczka"
  ]
  node [
    id 34
    label "bak"
  ]
  node [
    id 35
    label "ABS"
  ]
  node [
    id 36
    label "most"
  ]
  node [
    id 37
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 38
    label "spryskiwacz"
  ]
  node [
    id 39
    label "t&#322;umik"
  ]
  node [
    id 40
    label "tempomat"
  ]
  node [
    id 41
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 42
    label "dryl"
  ]
  node [
    id 43
    label "przedmiot"
  ]
  node [
    id 44
    label "zdemobilizowanie"
  ]
  node [
    id 45
    label "ods&#322;ugiwanie"
  ]
  node [
    id 46
    label "korpus"
  ]
  node [
    id 47
    label "s&#322;u&#380;ba"
  ]
  node [
    id 48
    label "wojo"
  ]
  node [
    id 49
    label "zrejterowanie"
  ]
  node [
    id 50
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 51
    label "zmobilizowa&#263;"
  ]
  node [
    id 52
    label "werbowanie_si&#281;"
  ]
  node [
    id 53
    label "struktura"
  ]
  node [
    id 54
    label "mobilizowa&#263;"
  ]
  node [
    id 55
    label "szko&#322;a"
  ]
  node [
    id 56
    label "oddzia&#322;"
  ]
  node [
    id 57
    label "zdemobilizowa&#263;"
  ]
  node [
    id 58
    label "Armia_Krajowa"
  ]
  node [
    id 59
    label "mobilizowanie"
  ]
  node [
    id 60
    label "pozycja"
  ]
  node [
    id 61
    label "si&#322;a"
  ]
  node [
    id 62
    label "fala"
  ]
  node [
    id 63
    label "obrona"
  ]
  node [
    id 64
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 65
    label "pospolite_ruszenie"
  ]
  node [
    id 66
    label "Armia_Czerwona"
  ]
  node [
    id 67
    label "cofni&#281;cie"
  ]
  node [
    id 68
    label "rejterowanie"
  ]
  node [
    id 69
    label "Eurokorpus"
  ]
  node [
    id 70
    label "tabor"
  ]
  node [
    id 71
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 72
    label "petarda"
  ]
  node [
    id 73
    label "zrejterowa&#263;"
  ]
  node [
    id 74
    label "zmobilizowanie"
  ]
  node [
    id 75
    label "rejterowa&#263;"
  ]
  node [
    id 76
    label "Czerwona_Gwardia"
  ]
  node [
    id 77
    label "Legia_Cudzoziemska"
  ]
  node [
    id 78
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 79
    label "wermacht"
  ]
  node [
    id 80
    label "soldateska"
  ]
  node [
    id 81
    label "oddzia&#322;_karny"
  ]
  node [
    id 82
    label "rezerwa"
  ]
  node [
    id 83
    label "or&#281;&#380;"
  ]
  node [
    id 84
    label "dezerter"
  ]
  node [
    id 85
    label "potencja"
  ]
  node [
    id 86
    label "sztabslekarz"
  ]
  node [
    id 87
    label "siedziba"
  ]
  node [
    id 88
    label "urz&#261;d"
  ]
  node [
    id 89
    label "organ"
  ]
  node [
    id 90
    label "komisariat"
  ]
  node [
    id 91
    label "psiarnia"
  ]
  node [
    id 92
    label "posterunek"
  ]
  node [
    id 93
    label "grupa"
  ]
  node [
    id 94
    label "obserwacja"
  ]
  node [
    id 95
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 96
    label "nauka_prawa"
  ]
  node [
    id 97
    label "dominion"
  ]
  node [
    id 98
    label "normatywizm"
  ]
  node [
    id 99
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 100
    label "qualification"
  ]
  node [
    id 101
    label "opis"
  ]
  node [
    id 102
    label "regu&#322;a_Allena"
  ]
  node [
    id 103
    label "normalizacja"
  ]
  node [
    id 104
    label "kazuistyka"
  ]
  node [
    id 105
    label "regu&#322;a_Glogera"
  ]
  node [
    id 106
    label "kultura_duchowa"
  ]
  node [
    id 107
    label "prawo_karne"
  ]
  node [
    id 108
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 109
    label "standard"
  ]
  node [
    id 110
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 111
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 112
    label "prawo_karne_procesowe"
  ]
  node [
    id 113
    label "prawo_Mendla"
  ]
  node [
    id 114
    label "przepis"
  ]
  node [
    id 115
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 116
    label "criterion"
  ]
  node [
    id 117
    label "kanonistyka"
  ]
  node [
    id 118
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 119
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 120
    label "wykonawczy"
  ]
  node [
    id 121
    label "twierdzenie"
  ]
  node [
    id 122
    label "judykatura"
  ]
  node [
    id 123
    label "legislacyjnie"
  ]
  node [
    id 124
    label "umocowa&#263;"
  ]
  node [
    id 125
    label "podmiot"
  ]
  node [
    id 126
    label "procesualistyka"
  ]
  node [
    id 127
    label "kierunek"
  ]
  node [
    id 128
    label "kryminologia"
  ]
  node [
    id 129
    label "kryminalistyka"
  ]
  node [
    id 130
    label "cywilistyka"
  ]
  node [
    id 131
    label "law"
  ]
  node [
    id 132
    label "zasada_d'Alemberta"
  ]
  node [
    id 133
    label "jurisprudence"
  ]
  node [
    id 134
    label "zasada"
  ]
  node [
    id 135
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 136
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 137
    label "plundering"
  ]
  node [
    id 138
    label "wydarzenie"
  ]
  node [
    id 139
    label "przest&#281;pstwo"
  ]
  node [
    id 140
    label "osobowo"
  ]
  node [
    id 141
    label "cywilnie"
  ]
  node [
    id 142
    label "nieoficjalny"
  ]
  node [
    id 143
    label "osowate"
  ]
  node [
    id 144
    label "osy_w&#322;a&#347;ciwe"
  ]
  node [
    id 145
    label "&#380;&#261;d&#322;&#243;wka"
  ]
  node [
    id 146
    label "fizykalnie"
  ]
  node [
    id 147
    label "fizycznie"
  ]
  node [
    id 148
    label "materializowanie"
  ]
  node [
    id 149
    label "pracownik"
  ]
  node [
    id 150
    label "gimnastyczny"
  ]
  node [
    id 151
    label "widoczny"
  ]
  node [
    id 152
    label "namacalny"
  ]
  node [
    id 153
    label "zmaterializowanie"
  ]
  node [
    id 154
    label "organiczny"
  ]
  node [
    id 155
    label "materjalny"
  ]
  node [
    id 156
    label "temat"
  ]
  node [
    id 157
    label "kognicja"
  ]
  node [
    id 158
    label "idea"
  ]
  node [
    id 159
    label "szczeg&#243;&#322;"
  ]
  node [
    id 160
    label "rzecz"
  ]
  node [
    id 161
    label "przes&#322;anka"
  ]
  node [
    id 162
    label "rozprawa"
  ]
  node [
    id 163
    label "object"
  ]
  node [
    id 164
    label "proposition"
  ]
  node [
    id 165
    label "cz&#322;owiek"
  ]
  node [
    id 166
    label "rota"
  ]
  node [
    id 167
    label "militarnie"
  ]
  node [
    id 168
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 169
    label "Gurkha"
  ]
  node [
    id 170
    label "demobilizowanie"
  ]
  node [
    id 171
    label "walcz&#261;cy"
  ]
  node [
    id 172
    label "harcap"
  ]
  node [
    id 173
    label "&#380;o&#322;dowy"
  ]
  node [
    id 174
    label "mundurowy"
  ]
  node [
    id 175
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 176
    label "typowy"
  ]
  node [
    id 177
    label "antybalistyczny"
  ]
  node [
    id 178
    label "specjalny"
  ]
  node [
    id 179
    label "podleg&#322;y"
  ]
  node [
    id 180
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 181
    label "elew"
  ]
  node [
    id 182
    label "so&#322;dat"
  ]
  node [
    id 183
    label "wojskowo"
  ]
  node [
    id 184
    label "demobilizowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 44
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 57
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
]
