graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.7333333333333334
  density 0.12380952380952381
  graphCliqueNumber 2
  node [
    id 0
    label "czai&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 2
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wyrucha&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pedofil"
    origin "text"
  ]
  node [
    id 5
    label "rozumie&#263;"
  ]
  node [
    id 6
    label "czu&#263;"
  ]
  node [
    id 7
    label "desire"
  ]
  node [
    id 8
    label "kcie&#263;"
  ]
  node [
    id 9
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 10
    label "organizm"
  ]
  node [
    id 11
    label "dewiant"
  ]
  node [
    id 12
    label "dziecko"
  ]
  node [
    id 13
    label "XD"
  ]
  node [
    id 14
    label "teraz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 13
    target 14
  ]
]
