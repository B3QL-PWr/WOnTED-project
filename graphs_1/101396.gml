graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.5384615384615385
  density 0.1282051282051282
  graphCliqueNumber 3
  node [
    id 0
    label "pierre"
    origin "text"
  ]
  node [
    id 1
    label "victor"
    origin "text"
  ]
  node [
    id 2
    label "auger"
    origin "text"
  ]
  node [
    id 3
    label "Pierre"
  ]
  node [
    id 4
    label "Victor"
  ]
  node [
    id 5
    label "Auger"
  ]
  node [
    id 6
    label "departament"
  ]
  node [
    id 7
    label "nauka"
  ]
  node [
    id 8
    label "przyrodniczy"
  ]
  node [
    id 9
    label "nowy"
  ]
  node [
    id 10
    label "Jork"
  ]
  node [
    id 11
    label "promie&#324;"
  ]
  node [
    id 12
    label "ksi&#261;&#380;&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
]
