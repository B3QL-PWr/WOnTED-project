graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.9607843137254901
  density 0.0392156862745098
  graphCliqueNumber 2
  node [
    id 0
    label "lato"
    origin "text"
  ]
  node [
    id 1
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "warszawski"
    origin "text"
  ]
  node [
    id 3
    label "wola"
    origin "text"
  ]
  node [
    id 4
    label "restauracja"
    origin "text"
  ]
  node [
    id 5
    label "pora_roku"
  ]
  node [
    id 6
    label "control"
  ]
  node [
    id 7
    label "eksponowa&#263;"
  ]
  node [
    id 8
    label "kre&#347;li&#263;"
  ]
  node [
    id 9
    label "g&#243;rowa&#263;"
  ]
  node [
    id 10
    label "message"
  ]
  node [
    id 11
    label "partner"
  ]
  node [
    id 12
    label "string"
  ]
  node [
    id 13
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 14
    label "przesuwa&#263;"
  ]
  node [
    id 15
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 16
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 17
    label "powodowa&#263;"
  ]
  node [
    id 18
    label "kierowa&#263;"
  ]
  node [
    id 19
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 20
    label "robi&#263;"
  ]
  node [
    id 21
    label "manipulate"
  ]
  node [
    id 22
    label "&#380;y&#263;"
  ]
  node [
    id 23
    label "navigate"
  ]
  node [
    id 24
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 25
    label "ukierunkowywa&#263;"
  ]
  node [
    id 26
    label "linia_melodyczna"
  ]
  node [
    id 27
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 28
    label "prowadzenie"
  ]
  node [
    id 29
    label "tworzy&#263;"
  ]
  node [
    id 30
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 31
    label "sterowa&#263;"
  ]
  node [
    id 32
    label "krzywa"
  ]
  node [
    id 33
    label "po_warszawsku"
  ]
  node [
    id 34
    label "marmuzela"
  ]
  node [
    id 35
    label "mazowiecki"
  ]
  node [
    id 36
    label "oskoma"
  ]
  node [
    id 37
    label "wish"
  ]
  node [
    id 38
    label "emocja"
  ]
  node [
    id 39
    label "mniemanie"
  ]
  node [
    id 40
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 41
    label "inclination"
  ]
  node [
    id 42
    label "zajawka"
  ]
  node [
    id 43
    label "pikolak"
  ]
  node [
    id 44
    label "go&#347;&#263;"
  ]
  node [
    id 45
    label "zak&#322;ad"
  ]
  node [
    id 46
    label "naprawa"
  ]
  node [
    id 47
    label "konsument"
  ]
  node [
    id 48
    label "powr&#243;t"
  ]
  node [
    id 49
    label "karta"
  ]
  node [
    id 50
    label "gastronomia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
]
