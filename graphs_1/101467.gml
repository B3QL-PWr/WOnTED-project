graph [
  maxDegree 13
  minDegree 1
  meanDegree 2.2962962962962963
  density 0.08831908831908832
  graphCliqueNumber 5
  node [
    id 0
    label "turniej"
    origin "text"
  ]
  node [
    id 1
    label "br&#261;zowy"
    origin "text"
  ]
  node [
    id 2
    label "kask"
    origin "text"
  ]
  node [
    id 3
    label "eliminacje"
  ]
  node [
    id 4
    label "zawody"
  ]
  node [
    id 5
    label "Wielki_Szlem"
  ]
  node [
    id 6
    label "drive"
  ]
  node [
    id 7
    label "impreza"
  ]
  node [
    id 8
    label "pojedynek"
  ]
  node [
    id 9
    label "runda"
  ]
  node [
    id 10
    label "tournament"
  ]
  node [
    id 11
    label "rywalizacja"
  ]
  node [
    id 12
    label "opalony"
  ]
  node [
    id 13
    label "metaliczny"
  ]
  node [
    id 14
    label "br&#261;zowo"
  ]
  node [
    id 15
    label "utytu&#322;owany"
  ]
  node [
    id 16
    label "ciemny"
  ]
  node [
    id 17
    label "br&#261;zowienie"
  ]
  node [
    id 18
    label "ciep&#322;y"
  ]
  node [
    id 19
    label "he&#322;m"
  ]
  node [
    id 20
    label "ojciec"
  ]
  node [
    id 21
    label "2010"
  ]
  node [
    id 22
    label "polski"
  ]
  node [
    id 23
    label "zwi&#261;zka"
  ]
  node [
    id 24
    label "motorowy"
  ]
  node [
    id 25
    label "Patryk"
  ]
  node [
    id 26
    label "dudka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
]
