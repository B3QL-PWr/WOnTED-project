graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.9090909090909092
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "sonda"
    origin "text"
  ]
  node [
    id 1
    label "uliczny"
    origin "text"
  ]
  node [
    id 2
    label "nowa"
    origin "text"
  ]
  node [
    id 3
    label "jork"
    origin "text"
  ]
  node [
    id 4
    label "sonda&#380;"
  ]
  node [
    id 5
    label "przyrz&#261;d"
  ]
  node [
    id 6
    label "leash"
  ]
  node [
    id 7
    label "badanie"
  ]
  node [
    id 8
    label "narz&#281;dzie"
  ]
  node [
    id 9
    label "lead"
  ]
  node [
    id 10
    label "statek_kosmiczny"
  ]
  node [
    id 11
    label "jarmarcznie"
  ]
  node [
    id 12
    label "streetowy"
  ]
  node [
    id 13
    label "prostacki"
  ]
  node [
    id 14
    label "gwiazda"
  ]
  node [
    id 15
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 16
    label "pies_do_towarzystwa"
  ]
  node [
    id 17
    label "terier"
  ]
  node [
    id 18
    label "pies_pokojowy"
  ]
  node [
    id 19
    label "pies_miniaturowy"
  ]
  node [
    id 20
    label "pies_ozdobny"
  ]
  node [
    id 21
    label "Jork"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
]
