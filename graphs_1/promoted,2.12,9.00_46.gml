graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9473684210526316
  density 0.05263157894736842
  graphCliqueNumber 2
  node [
    id 0
    label "orangutank&#281;"
    origin "text"
  ]
  node [
    id 1
    label "pony"
    origin "text"
  ]
  node [
    id 2
    label "przez"
    origin "text"
  ]
  node [
    id 3
    label "lato"
    origin "text"
  ]
  node [
    id 4
    label "zmusza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "prostytucja"
    origin "text"
  ]
  node [
    id 6
    label "dom"
    origin "text"
  ]
  node [
    id 7
    label "publiczny"
    origin "text"
  ]
  node [
    id 8
    label "borneo"
    origin "text"
  ]
  node [
    id 9
    label "kuc"
  ]
  node [
    id 10
    label "zabawka"
  ]
  node [
    id 11
    label "pora_roku"
  ]
  node [
    id 12
    label "powodowa&#263;"
  ]
  node [
    id 13
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 14
    label "sandbag"
  ]
  node [
    id 15
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 16
    label "proceder"
  ]
  node [
    id 17
    label "prostitution"
  ]
  node [
    id 18
    label "Sodoma_i_Gomora"
  ]
  node [
    id 19
    label "kurewstwo"
  ]
  node [
    id 20
    label "garderoba"
  ]
  node [
    id 21
    label "wiecha"
  ]
  node [
    id 22
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 23
    label "grupa"
  ]
  node [
    id 24
    label "budynek"
  ]
  node [
    id 25
    label "fratria"
  ]
  node [
    id 26
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 27
    label "poj&#281;cie"
  ]
  node [
    id 28
    label "rodzina"
  ]
  node [
    id 29
    label "substancja_mieszkaniowa"
  ]
  node [
    id 30
    label "instytucja"
  ]
  node [
    id 31
    label "dom_rodzinny"
  ]
  node [
    id 32
    label "stead"
  ]
  node [
    id 33
    label "siedziba"
  ]
  node [
    id 34
    label "jawny"
  ]
  node [
    id 35
    label "upublicznienie"
  ]
  node [
    id 36
    label "upublicznianie"
  ]
  node [
    id 37
    label "publicznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
]
