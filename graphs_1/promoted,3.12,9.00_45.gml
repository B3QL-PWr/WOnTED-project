graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.9545454545454546
  density 0.045454545454545456
  graphCliqueNumber 2
  node [
    id 0
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "pierwsza"
    origin "text"
  ]
  node [
    id 3
    label "fala"
    origin "text"
  ]
  node [
    id 4
    label "migracja"
    origin "text"
  ]
  node [
    id 5
    label "polska"
    origin "text"
  ]
  node [
    id 6
    label "spoza"
    origin "text"
  ]
  node [
    id 7
    label "europa"
    origin "text"
  ]
  node [
    id 8
    label "open"
  ]
  node [
    id 9
    label "odejmowa&#263;"
  ]
  node [
    id 10
    label "mie&#263;_miejsce"
  ]
  node [
    id 11
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 12
    label "set_about"
  ]
  node [
    id 13
    label "begin"
  ]
  node [
    id 14
    label "post&#281;powa&#263;"
  ]
  node [
    id 15
    label "bankrupt"
  ]
  node [
    id 16
    label "godzina"
  ]
  node [
    id 17
    label "reakcja"
  ]
  node [
    id 18
    label "zafalowanie"
  ]
  node [
    id 19
    label "czo&#322;o_fali"
  ]
  node [
    id 20
    label "rozbijanie_si&#281;"
  ]
  node [
    id 21
    label "zjawisko"
  ]
  node [
    id 22
    label "clutter"
  ]
  node [
    id 23
    label "pasemko"
  ]
  node [
    id 24
    label "grzywa_fali"
  ]
  node [
    id 25
    label "t&#322;um"
  ]
  node [
    id 26
    label "przemoc"
  ]
  node [
    id 27
    label "kot"
  ]
  node [
    id 28
    label "mn&#243;stwo"
  ]
  node [
    id 29
    label "wojsko"
  ]
  node [
    id 30
    label "efekt_Dopplera"
  ]
  node [
    id 31
    label "kszta&#322;t"
  ]
  node [
    id 32
    label "obcinka"
  ]
  node [
    id 33
    label "stream"
  ]
  node [
    id 34
    label "fit"
  ]
  node [
    id 35
    label "zafalowa&#263;"
  ]
  node [
    id 36
    label "karb"
  ]
  node [
    id 37
    label "rozbicie_si&#281;"
  ]
  node [
    id 38
    label "znak_diakrytyczny"
  ]
  node [
    id 39
    label "okres"
  ]
  node [
    id 40
    label "woda"
  ]
  node [
    id 41
    label "strumie&#324;"
  ]
  node [
    id 42
    label "exodus"
  ]
  node [
    id 43
    label "ruch"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
]
