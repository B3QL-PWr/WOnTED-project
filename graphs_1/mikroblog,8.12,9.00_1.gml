graph [
  maxDegree 24
  minDegree 1
  meanDegree 2.1379310344827585
  density 0.018590704647676162
  graphCliqueNumber 3
  node [
    id 0
    label "rozdajo"
    origin "text"
  ]
  node [
    id 1
    label "wplace"
    origin "text"
  ]
  node [
    id 2
    label "plusow"
    origin "text"
  ]
  node [
    id 3
    label "zlotowkach"
    origin "text"
  ]
  node [
    id 4
    label "fundacja"
    origin "text"
  ]
  node [
    id 5
    label "charytatywny"
    origin "text"
  ]
  node [
    id 6
    label "ktora"
    origin "text"
  ]
  node [
    id 7
    label "beda"
    origin "text"
  ]
  node [
    id 8
    label "wpisa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "komentarz"
    origin "text"
  ]
  node [
    id 10
    label "ktory"
    origin "text"
  ]
  node [
    id 11
    label "miec"
    origin "text"
  ]
  node [
    id 12
    label "najwiewcej"
    origin "text"
  ]
  node [
    id 13
    label "wplacam"
    origin "text"
  ]
  node [
    id 14
    label "jakies"
    origin "text"
  ]
  node [
    id 15
    label "kupowa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "zubra"
    origin "text"
  ]
  node [
    id 17
    label "dla"
    origin "text"
  ]
  node [
    id 18
    label "daniel"
    origin "text"
  ]
  node [
    id 19
    label "magicala"
    origin "text"
  ]
  node [
    id 20
    label "prawdziwy"
    origin "text"
  ]
  node [
    id 21
    label "dziecko"
    origin "text"
  ]
  node [
    id 22
    label "rak"
    origin "text"
  ]
  node [
    id 23
    label "cos"
    origin "text"
  ]
  node [
    id 24
    label "patusy"
    origin "text"
  ]
  node [
    id 25
    label "foundation"
  ]
  node [
    id 26
    label "instytucja"
  ]
  node [
    id 27
    label "dar"
  ]
  node [
    id 28
    label "darowizna"
  ]
  node [
    id 29
    label "pocz&#261;tek"
  ]
  node [
    id 30
    label "spo&#322;eczny"
  ]
  node [
    id 31
    label "dobroczynnie"
  ]
  node [
    id 32
    label "napisa&#263;"
  ]
  node [
    id 33
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 34
    label "draw"
  ]
  node [
    id 35
    label "write"
  ]
  node [
    id 36
    label "wprowadzi&#263;"
  ]
  node [
    id 37
    label "comment"
  ]
  node [
    id 38
    label "artyku&#322;"
  ]
  node [
    id 39
    label "ocena"
  ]
  node [
    id 40
    label "gossip"
  ]
  node [
    id 41
    label "interpretacja"
  ]
  node [
    id 42
    label "audycja"
  ]
  node [
    id 43
    label "tekst"
  ]
  node [
    id 44
    label "get"
  ]
  node [
    id 45
    label "ustawia&#263;"
  ]
  node [
    id 46
    label "wierzy&#263;"
  ]
  node [
    id 47
    label "przyjmowa&#263;"
  ]
  node [
    id 48
    label "pozyskiwa&#263;"
  ]
  node [
    id 49
    label "gra&#263;"
  ]
  node [
    id 50
    label "kupywa&#263;"
  ]
  node [
    id 51
    label "uznawa&#263;"
  ]
  node [
    id 52
    label "bra&#263;"
  ]
  node [
    id 53
    label "becze&#263;"
  ]
  node [
    id 54
    label "zabecze&#263;"
  ]
  node [
    id 55
    label "&#347;wiece"
  ]
  node [
    id 56
    label "kwiat"
  ]
  node [
    id 57
    label "zwierz&#281;_&#322;owne"
  ]
  node [
    id 58
    label "zwierzyna_p&#322;owa"
  ]
  node [
    id 59
    label "prze&#380;uwacz"
  ]
  node [
    id 60
    label "jeleniowate"
  ]
  node [
    id 61
    label "badyl"
  ]
  node [
    id 62
    label "szczery"
  ]
  node [
    id 63
    label "naprawd&#281;"
  ]
  node [
    id 64
    label "zgodny"
  ]
  node [
    id 65
    label "naturalny"
  ]
  node [
    id 66
    label "realnie"
  ]
  node [
    id 67
    label "prawdziwie"
  ]
  node [
    id 68
    label "m&#261;dry"
  ]
  node [
    id 69
    label "&#380;ywny"
  ]
  node [
    id 70
    label "podobny"
  ]
  node [
    id 71
    label "cz&#322;owiek"
  ]
  node [
    id 72
    label "potomstwo"
  ]
  node [
    id 73
    label "organizm"
  ]
  node [
    id 74
    label "sraluch"
  ]
  node [
    id 75
    label "utulanie"
  ]
  node [
    id 76
    label "pediatra"
  ]
  node [
    id 77
    label "dzieciarnia"
  ]
  node [
    id 78
    label "m&#322;odziak"
  ]
  node [
    id 79
    label "dzieciak"
  ]
  node [
    id 80
    label "utula&#263;"
  ]
  node [
    id 81
    label "potomek"
  ]
  node [
    id 82
    label "entliczek-pentliczek"
  ]
  node [
    id 83
    label "pedofil"
  ]
  node [
    id 84
    label "m&#322;odzik"
  ]
  node [
    id 85
    label "cz&#322;owieczek"
  ]
  node [
    id 86
    label "zwierz&#281;"
  ]
  node [
    id 87
    label "niepe&#322;noletni"
  ]
  node [
    id 88
    label "fledgling"
  ]
  node [
    id 89
    label "utuli&#263;"
  ]
  node [
    id 90
    label "utulenie"
  ]
  node [
    id 91
    label "mozaika"
  ]
  node [
    id 92
    label "przyrz&#261;d"
  ]
  node [
    id 93
    label "guz_z&#322;o&#347;liwy"
  ]
  node [
    id 94
    label "nacieka&#263;"
  ]
  node [
    id 95
    label "naciekanie"
  ]
  node [
    id 96
    label "erwinia"
  ]
  node [
    id 97
    label "rakowate"
  ]
  node [
    id 98
    label "mumia"
  ]
  node [
    id 99
    label "bakteria_brodawkowa"
  ]
  node [
    id 100
    label "dziesi&#281;cion&#243;g"
  ]
  node [
    id 101
    label "pieprzyk"
  ]
  node [
    id 102
    label "czarcia_miot&#322;a"
  ]
  node [
    id 103
    label "naciekn&#261;&#263;"
  ]
  node [
    id 104
    label "fitopatolog"
  ]
  node [
    id 105
    label "choroba_somatyczna"
  ]
  node [
    id 106
    label "schorzenie"
  ]
  node [
    id 107
    label "naciekni&#281;cie"
  ]
  node [
    id 108
    label "stres_oksydacyjny"
  ]
  node [
    id 109
    label "zbi&#243;r"
  ]
  node [
    id 110
    label "wada"
  ]
  node [
    id 111
    label "choroba"
  ]
  node [
    id 112
    label "cosine"
  ]
  node [
    id 113
    label "funkcja_trygonometryczna"
  ]
  node [
    id 114
    label "arcus_cosinus"
  ]
  node [
    id 115
    label "dostawa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 44
  ]
  edge [
    source 15
    target 45
  ]
  edge [
    source 15
    target 46
  ]
  edge [
    source 15
    target 47
  ]
  edge [
    source 15
    target 48
  ]
  edge [
    source 15
    target 49
  ]
  edge [
    source 15
    target 50
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 15
    target 52
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 18
    target 54
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 18
    target 56
  ]
  edge [
    source 18
    target 57
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 59
  ]
  edge [
    source 18
    target 60
  ]
  edge [
    source 18
    target 61
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 20
    target 63
  ]
  edge [
    source 20
    target 64
  ]
  edge [
    source 20
    target 65
  ]
  edge [
    source 20
    target 66
  ]
  edge [
    source 20
    target 67
  ]
  edge [
    source 20
    target 68
  ]
  edge [
    source 20
    target 69
  ]
  edge [
    source 20
    target 70
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 71
  ]
  edge [
    source 21
    target 72
  ]
  edge [
    source 21
    target 73
  ]
  edge [
    source 21
    target 74
  ]
  edge [
    source 21
    target 75
  ]
  edge [
    source 21
    target 76
  ]
  edge [
    source 21
    target 77
  ]
  edge [
    source 21
    target 78
  ]
  edge [
    source 21
    target 79
  ]
  edge [
    source 21
    target 80
  ]
  edge [
    source 21
    target 81
  ]
  edge [
    source 21
    target 82
  ]
  edge [
    source 21
    target 83
  ]
  edge [
    source 21
    target 84
  ]
  edge [
    source 21
    target 85
  ]
  edge [
    source 21
    target 86
  ]
  edge [
    source 21
    target 87
  ]
  edge [
    source 21
    target 88
  ]
  edge [
    source 21
    target 89
  ]
  edge [
    source 21
    target 90
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 91
  ]
  edge [
    source 22
    target 71
  ]
  edge [
    source 22
    target 92
  ]
  edge [
    source 22
    target 93
  ]
  edge [
    source 22
    target 94
  ]
  edge [
    source 22
    target 95
  ]
  edge [
    source 22
    target 96
  ]
  edge [
    source 22
    target 97
  ]
  edge [
    source 22
    target 98
  ]
  edge [
    source 22
    target 99
  ]
  edge [
    source 22
    target 100
  ]
  edge [
    source 22
    target 101
  ]
  edge [
    source 22
    target 102
  ]
  edge [
    source 22
    target 103
  ]
  edge [
    source 22
    target 104
  ]
  edge [
    source 22
    target 105
  ]
  edge [
    source 22
    target 106
  ]
  edge [
    source 22
    target 107
  ]
  edge [
    source 22
    target 108
  ]
  edge [
    source 22
    target 109
  ]
  edge [
    source 22
    target 110
  ]
  edge [
    source 22
    target 111
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 112
  ]
  edge [
    source 23
    target 113
  ]
  edge [
    source 23
    target 114
  ]
  edge [
    source 23
    target 115
  ]
]
