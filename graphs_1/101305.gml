graph [
  maxDegree 40
  minDegree 1
  meanDegree 1.9607843137254901
  density 0.0392156862745098
  graphCliqueNumber 2
  node [
    id 0
    label "pieprz"
    origin "text"
  ]
  node [
    id 1
    label "czerwony"
    origin "text"
  ]
  node [
    id 2
    label "nieprzyzwoito&#347;&#263;"
  ]
  node [
    id 3
    label "ro&#347;lina_przyprawowa"
  ]
  node [
    id 4
    label "przyprawa"
  ]
  node [
    id 5
    label "pestkowiec"
  ]
  node [
    id 6
    label "cecha"
  ]
  node [
    id 7
    label "ro&#347;lina"
  ]
  node [
    id 8
    label "pieprzowate"
  ]
  node [
    id 9
    label "egzotyk"
  ]
  node [
    id 10
    label "przyprawy_korzenne"
  ]
  node [
    id 11
    label "ostry"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "rezerwat"
  ]
  node [
    id 14
    label "Amerykanin"
  ]
  node [
    id 15
    label "zaczerwienienie"
  ]
  node [
    id 16
    label "Tito"
  ]
  node [
    id 17
    label "Gomu&#322;ka"
  ]
  node [
    id 18
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 19
    label "Gierek"
  ]
  node [
    id 20
    label "dojrza&#322;y"
  ]
  node [
    id 21
    label "czerwono"
  ]
  node [
    id 22
    label "rozpalanie_si&#281;"
  ]
  node [
    id 23
    label "kra&#347;ny"
  ]
  node [
    id 24
    label "czerwienienie"
  ]
  node [
    id 25
    label "rozpalenie_si&#281;"
  ]
  node [
    id 26
    label "lewactwo"
  ]
  node [
    id 27
    label "Bre&#380;niew"
  ]
  node [
    id 28
    label "ciep&#322;y"
  ]
  node [
    id 29
    label "Bierut"
  ]
  node [
    id 30
    label "Stalin"
  ]
  node [
    id 31
    label "czerwienienie_si&#281;"
  ]
  node [
    id 32
    label "Fidel_Castro"
  ]
  node [
    id 33
    label "reformator"
  ]
  node [
    id 34
    label "radyka&#322;"
  ]
  node [
    id 35
    label "demokrata"
  ]
  node [
    id 36
    label "dzia&#322;acz"
  ]
  node [
    id 37
    label "komunizowanie"
  ]
  node [
    id 38
    label "rozpalony"
  ]
  node [
    id 39
    label "rumiany"
  ]
  node [
    id 40
    label "lewicowiec"
  ]
  node [
    id 41
    label "Polak"
  ]
  node [
    id 42
    label "komuszek"
  ]
  node [
    id 43
    label "sczerwienienie"
  ]
  node [
    id 44
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 45
    label "Chruszczow"
  ]
  node [
    id 46
    label "Mao"
  ]
  node [
    id 47
    label "lewicowy"
  ]
  node [
    id 48
    label "zaczerwienienie_si&#281;"
  ]
  node [
    id 49
    label "skomunizowanie"
  ]
  node [
    id 50
    label "tubylec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
]
