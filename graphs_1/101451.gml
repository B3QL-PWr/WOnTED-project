graph [
  maxDegree 72
  minDegree 1
  meanDegree 1.8780487804878048
  density 0.02318578741342969
  graphCliqueNumber 2
  node [
    id 0
    label "priesnia"
    origin "text"
  ]
  node [
    id 1
    label "rzeka"
    origin "text"
  ]
  node [
    id 2
    label "Izera"
  ]
  node [
    id 3
    label "Orla"
  ]
  node [
    id 4
    label "Amazonka"
  ]
  node [
    id 5
    label "Kongo"
  ]
  node [
    id 6
    label "Kaczawa"
  ]
  node [
    id 7
    label "Hudson"
  ]
  node [
    id 8
    label "Drina"
  ]
  node [
    id 9
    label "Windawa"
  ]
  node [
    id 10
    label "Wereszyca"
  ]
  node [
    id 11
    label "Wis&#322;a"
  ]
  node [
    id 12
    label "Peczora"
  ]
  node [
    id 13
    label "Pad"
  ]
  node [
    id 14
    label "ciek_wodny"
  ]
  node [
    id 15
    label "Alabama"
  ]
  node [
    id 16
    label "Ren"
  ]
  node [
    id 17
    label "S&#322;upia"
  ]
  node [
    id 18
    label "Dunaj"
  ]
  node [
    id 19
    label "Orinoko"
  ]
  node [
    id 20
    label "Wia&#378;ma"
  ]
  node [
    id 21
    label "Zyrianka"
  ]
  node [
    id 22
    label "Turiec"
  ]
  node [
    id 23
    label "Pia&#347;nica"
  ]
  node [
    id 24
    label "Cisa"
  ]
  node [
    id 25
    label "Dniepr"
  ]
  node [
    id 26
    label "Odra"
  ]
  node [
    id 27
    label "Sekwana"
  ]
  node [
    id 28
    label "Dniestr"
  ]
  node [
    id 29
    label "Supra&#347;l"
  ]
  node [
    id 30
    label "Wieprza"
  ]
  node [
    id 31
    label "Nil"
  ]
  node [
    id 32
    label "D&#378;wina"
  ]
  node [
    id 33
    label "woda_powierzchniowa"
  ]
  node [
    id 34
    label "Anadyr"
  ]
  node [
    id 35
    label "Amur"
  ]
  node [
    id 36
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 37
    label "ilo&#347;&#263;"
  ]
  node [
    id 38
    label "So&#322;a"
  ]
  node [
    id 39
    label "Zi&#281;bina"
  ]
  node [
    id 40
    label "Ropa"
  ]
  node [
    id 41
    label "Styks"
  ]
  node [
    id 42
    label "Mozela"
  ]
  node [
    id 43
    label "Witim"
  ]
  node [
    id 44
    label "Don"
  ]
  node [
    id 45
    label "Sanica"
  ]
  node [
    id 46
    label "potamoplankton"
  ]
  node [
    id 47
    label "Wo&#322;ga"
  ]
  node [
    id 48
    label "Moza"
  ]
  node [
    id 49
    label "Niemen"
  ]
  node [
    id 50
    label "Lena"
  ]
  node [
    id 51
    label "Dwina"
  ]
  node [
    id 52
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 53
    label "Zarycz"
  ]
  node [
    id 54
    label "Brze&#378;niczanka"
  ]
  node [
    id 55
    label "odp&#322;ywanie"
  ]
  node [
    id 56
    label "Jenisej"
  ]
  node [
    id 57
    label "Ussuri"
  ]
  node [
    id 58
    label "wpadni&#281;cie"
  ]
  node [
    id 59
    label "Pr&#261;dnik"
  ]
  node [
    id 60
    label "Lete"
  ]
  node [
    id 61
    label "&#321;aba"
  ]
  node [
    id 62
    label "Ob"
  ]
  node [
    id 63
    label "Berezyna"
  ]
  node [
    id 64
    label "Rega"
  ]
  node [
    id 65
    label "Widawa"
  ]
  node [
    id 66
    label "Newa"
  ]
  node [
    id 67
    label "Ko&#322;yma"
  ]
  node [
    id 68
    label "wpadanie"
  ]
  node [
    id 69
    label "&#321;upawa"
  ]
  node [
    id 70
    label "strumie&#324;"
  ]
  node [
    id 71
    label "Pars&#281;ta"
  ]
  node [
    id 72
    label "ghaty"
  ]
  node [
    id 73
    label "most"
  ]
  node [
    id 74
    label "Nowoarbatskiego"
  ]
  node [
    id 75
    label "park"
  ]
  node [
    id 76
    label "piotrowski"
  ]
  node [
    id 77
    label "bia&#322;y"
  ]
  node [
    id 78
    label "dom"
  ]
  node [
    id 79
    label "garbaty"
  ]
  node [
    id 80
    label "Priesnienskie"
  ]
  node [
    id 81
    label "stawa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 79
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 80
    target 81
  ]
]
