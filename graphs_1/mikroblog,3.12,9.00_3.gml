graph [
  maxDegree 13
  minDegree 1
  meanDegree 2
  density 0.05555555555555555
  graphCliqueNumber 3
  node [
    id 0
    label "znajoma"
    origin "text"
  ]
  node [
    id 1
    label "niedawno"
    origin "text"
  ]
  node [
    id 2
    label "dowiedzie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 6
    label "ci&#261;&#380;a"
    origin "text"
  ]
  node [
    id 7
    label "ostatni"
  ]
  node [
    id 8
    label "aktualnie"
  ]
  node [
    id 9
    label "si&#281;ga&#263;"
  ]
  node [
    id 10
    label "trwa&#263;"
  ]
  node [
    id 11
    label "obecno&#347;&#263;"
  ]
  node [
    id 12
    label "stan"
  ]
  node [
    id 13
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 14
    label "stand"
  ]
  node [
    id 15
    label "mie&#263;_miejsce"
  ]
  node [
    id 16
    label "uczestniczy&#263;"
  ]
  node [
    id 17
    label "chodzi&#263;"
  ]
  node [
    id 18
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 19
    label "equal"
  ]
  node [
    id 20
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 21
    label "doba"
  ]
  node [
    id 22
    label "czas"
  ]
  node [
    id 23
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 24
    label "weekend"
  ]
  node [
    id 25
    label "miesi&#261;c"
  ]
  node [
    id 26
    label "teleangiektazja"
  ]
  node [
    id 27
    label "proces_fizjologiczny"
  ]
  node [
    id 28
    label "donosi&#263;"
  ]
  node [
    id 29
    label "guzek_Montgomery'ego"
  ]
  node [
    id 30
    label "gestoza"
  ]
  node [
    id 31
    label "kuwada"
  ]
  node [
    id 32
    label "rozmna&#380;anie"
  ]
  node [
    id 33
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 34
    label "donoszenie"
  ]
  node [
    id 35
    label "biedny"
  ]
  node [
    id 36
    label "Mirek"
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 35
    target 36
  ]
]
