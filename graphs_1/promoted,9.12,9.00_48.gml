graph [
  maxDegree 18
  minDegree 1
  meanDegree 2.1153846153846154
  density 0.04147812971342383
  graphCliqueNumber 2
  node [
    id 0
    label "zawsze"
    origin "text"
  ]
  node [
    id 1
    label "kobieta"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "polska"
    origin "text"
  ]
  node [
    id 5
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 6
    label "zagranica"
    origin "text"
  ]
  node [
    id 7
    label "zaw&#380;dy"
  ]
  node [
    id 8
    label "ci&#261;gle"
  ]
  node [
    id 9
    label "na_zawsze"
  ]
  node [
    id 10
    label "cz&#281;sto"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "przekwitanie"
  ]
  node [
    id 13
    label "m&#281;&#380;yna"
  ]
  node [
    id 14
    label "babka"
  ]
  node [
    id 15
    label "samica"
  ]
  node [
    id 16
    label "doros&#322;y"
  ]
  node [
    id 17
    label "ulec"
  ]
  node [
    id 18
    label "uleganie"
  ]
  node [
    id 19
    label "partnerka"
  ]
  node [
    id 20
    label "&#380;ona"
  ]
  node [
    id 21
    label "ulega&#263;"
  ]
  node [
    id 22
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 23
    label "pa&#324;stwo"
  ]
  node [
    id 24
    label "ulegni&#281;cie"
  ]
  node [
    id 25
    label "menopauza"
  ]
  node [
    id 26
    label "&#322;ono"
  ]
  node [
    id 27
    label "czyj&#347;"
  ]
  node [
    id 28
    label "m&#261;&#380;"
  ]
  node [
    id 29
    label "si&#281;ga&#263;"
  ]
  node [
    id 30
    label "trwa&#263;"
  ]
  node [
    id 31
    label "obecno&#347;&#263;"
  ]
  node [
    id 32
    label "stan"
  ]
  node [
    id 33
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 34
    label "stand"
  ]
  node [
    id 35
    label "mie&#263;_miejsce"
  ]
  node [
    id 36
    label "uczestniczy&#263;"
  ]
  node [
    id 37
    label "chodzi&#263;"
  ]
  node [
    id 38
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 39
    label "equal"
  ]
  node [
    id 40
    label "ch&#322;opina"
  ]
  node [
    id 41
    label "jegomo&#347;&#263;"
  ]
  node [
    id 42
    label "bratek"
  ]
  node [
    id 43
    label "samiec"
  ]
  node [
    id 44
    label "ojciec"
  ]
  node [
    id 45
    label "twardziel"
  ]
  node [
    id 46
    label "androlog"
  ]
  node [
    id 47
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 48
    label "andropauza"
  ]
  node [
    id 49
    label "obszar"
  ]
  node [
    id 50
    label "granica_pa&#324;stwa"
  ]
  node [
    id 51
    label "&#347;wiat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
]
