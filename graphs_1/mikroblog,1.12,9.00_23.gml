graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9550561797752808
  density 0.022216547497446375
  graphCliqueNumber 3
  node [
    id 0
    label "oko"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pierwsza"
    origin "text"
  ]
  node [
    id 3
    label "odcinek"
    origin "text"
  ]
  node [
    id 4
    label "powoli"
    origin "text"
  ]
  node [
    id 5
    label "ko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "drugi"
    origin "text"
  ]
  node [
    id 7
    label "wypowied&#378;"
  ]
  node [
    id 8
    label "siniec"
  ]
  node [
    id 9
    label "uwaga"
  ]
  node [
    id 10
    label "rzecz"
  ]
  node [
    id 11
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 12
    label "powieka"
  ]
  node [
    id 13
    label "oczy"
  ]
  node [
    id 14
    label "&#347;lepko"
  ]
  node [
    id 15
    label "ga&#322;ka_oczna"
  ]
  node [
    id 16
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 17
    label "&#347;lepie"
  ]
  node [
    id 18
    label "twarz"
  ]
  node [
    id 19
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 20
    label "organ"
  ]
  node [
    id 21
    label "nerw_wzrokowy"
  ]
  node [
    id 22
    label "wzrok"
  ]
  node [
    id 23
    label "spoj&#243;wka"
  ]
  node [
    id 24
    label "&#378;renica"
  ]
  node [
    id 25
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 26
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 27
    label "kaprawie&#263;"
  ]
  node [
    id 28
    label "kaprawienie"
  ]
  node [
    id 29
    label "spojrzenie"
  ]
  node [
    id 30
    label "net"
  ]
  node [
    id 31
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 32
    label "coloboma"
  ]
  node [
    id 33
    label "ros&#243;&#322;"
  ]
  node [
    id 34
    label "si&#281;ga&#263;"
  ]
  node [
    id 35
    label "trwa&#263;"
  ]
  node [
    id 36
    label "obecno&#347;&#263;"
  ]
  node [
    id 37
    label "stan"
  ]
  node [
    id 38
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 39
    label "stand"
  ]
  node [
    id 40
    label "mie&#263;_miejsce"
  ]
  node [
    id 41
    label "uczestniczy&#263;"
  ]
  node [
    id 42
    label "chodzi&#263;"
  ]
  node [
    id 43
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 44
    label "equal"
  ]
  node [
    id 45
    label "godzina"
  ]
  node [
    id 46
    label "pole"
  ]
  node [
    id 47
    label "part"
  ]
  node [
    id 48
    label "pokwitowanie"
  ]
  node [
    id 49
    label "kawa&#322;ek"
  ]
  node [
    id 50
    label "coupon"
  ]
  node [
    id 51
    label "teren"
  ]
  node [
    id 52
    label "line"
  ]
  node [
    id 53
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 54
    label "epizod"
  ]
  node [
    id 55
    label "moneta"
  ]
  node [
    id 56
    label "fragment"
  ]
  node [
    id 57
    label "wolny"
  ]
  node [
    id 58
    label "stopniowo"
  ]
  node [
    id 59
    label "wolniej"
  ]
  node [
    id 60
    label "niespiesznie"
  ]
  node [
    id 61
    label "bezproblemowo"
  ]
  node [
    id 62
    label "spokojny"
  ]
  node [
    id 63
    label "zako&#324;cza&#263;"
  ]
  node [
    id 64
    label "przestawa&#263;"
  ]
  node [
    id 65
    label "robi&#263;"
  ]
  node [
    id 66
    label "satisfy"
  ]
  node [
    id 67
    label "close"
  ]
  node [
    id 68
    label "determine"
  ]
  node [
    id 69
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 70
    label "stanowi&#263;"
  ]
  node [
    id 71
    label "cz&#322;owiek"
  ]
  node [
    id 72
    label "inny"
  ]
  node [
    id 73
    label "kolejny"
  ]
  node [
    id 74
    label "przeciwny"
  ]
  node [
    id 75
    label "sw&#243;j"
  ]
  node [
    id 76
    label "odwrotnie"
  ]
  node [
    id 77
    label "dzie&#324;"
  ]
  node [
    id 78
    label "podobny"
  ]
  node [
    id 79
    label "wt&#243;ry"
  ]
  node [
    id 80
    label "s&#322;u&#380;ba"
  ]
  node [
    id 81
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 82
    label "rzeczpospolita"
  ]
  node [
    id 83
    label "Polska"
  ]
  node [
    id 84
    label "ludowy"
  ]
  node [
    id 85
    label "uniwersytet"
  ]
  node [
    id 86
    label "powszechny"
  ]
  node [
    id 87
    label "a"
  ]
  node [
    id 88
    label "warszawski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 84
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 87
  ]
  edge [
    source 85
    target 88
  ]
  edge [
    source 86
    target 87
  ]
]
