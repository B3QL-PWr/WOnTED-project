graph [
  maxDegree 42
  minDegree 1
  meanDegree 2
  density 0.024390243902439025
  graphCliqueNumber 2
  node [
    id 0
    label "zaplusuj"
    origin "text"
  ]
  node [
    id 1
    label "ten"
    origin "text"
  ]
  node [
    id 2
    label "wpis"
    origin "text"
  ]
  node [
    id 3
    label "nic"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "stan"
    origin "text"
  ]
  node [
    id 6
    label "g&#322;upi"
    origin "text"
  ]
  node [
    id 7
    label "dzban"
    origin "text"
  ]
  node [
    id 8
    label "okre&#347;lony"
  ]
  node [
    id 9
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 10
    label "czynno&#347;&#263;"
  ]
  node [
    id 11
    label "entrance"
  ]
  node [
    id 12
    label "inscription"
  ]
  node [
    id 13
    label "akt"
  ]
  node [
    id 14
    label "op&#322;ata"
  ]
  node [
    id 15
    label "tekst"
  ]
  node [
    id 16
    label "miernota"
  ]
  node [
    id 17
    label "g&#243;wno"
  ]
  node [
    id 18
    label "love"
  ]
  node [
    id 19
    label "ilo&#347;&#263;"
  ]
  node [
    id 20
    label "brak"
  ]
  node [
    id 21
    label "ciura"
  ]
  node [
    id 22
    label "Arizona"
  ]
  node [
    id 23
    label "Georgia"
  ]
  node [
    id 24
    label "warstwa"
  ]
  node [
    id 25
    label "jednostka_administracyjna"
  ]
  node [
    id 26
    label "Goa"
  ]
  node [
    id 27
    label "Hawaje"
  ]
  node [
    id 28
    label "Floryda"
  ]
  node [
    id 29
    label "Oklahoma"
  ]
  node [
    id 30
    label "punkt"
  ]
  node [
    id 31
    label "Alaska"
  ]
  node [
    id 32
    label "Alabama"
  ]
  node [
    id 33
    label "wci&#281;cie"
  ]
  node [
    id 34
    label "Oregon"
  ]
  node [
    id 35
    label "poziom"
  ]
  node [
    id 36
    label "by&#263;"
  ]
  node [
    id 37
    label "Teksas"
  ]
  node [
    id 38
    label "Illinois"
  ]
  node [
    id 39
    label "Jukatan"
  ]
  node [
    id 40
    label "Waszyngton"
  ]
  node [
    id 41
    label "shape"
  ]
  node [
    id 42
    label "Nowy_Meksyk"
  ]
  node [
    id 43
    label "state"
  ]
  node [
    id 44
    label "Nowy_York"
  ]
  node [
    id 45
    label "Arakan"
  ]
  node [
    id 46
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 47
    label "Kalifornia"
  ]
  node [
    id 48
    label "wektor"
  ]
  node [
    id 49
    label "Massachusetts"
  ]
  node [
    id 50
    label "miejsce"
  ]
  node [
    id 51
    label "Pensylwania"
  ]
  node [
    id 52
    label "Maryland"
  ]
  node [
    id 53
    label "Michigan"
  ]
  node [
    id 54
    label "Ohio"
  ]
  node [
    id 55
    label "Kansas"
  ]
  node [
    id 56
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 57
    label "Luizjana"
  ]
  node [
    id 58
    label "samopoczucie"
  ]
  node [
    id 59
    label "Wirginia"
  ]
  node [
    id 60
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 61
    label "g&#322;upiec"
  ]
  node [
    id 62
    label "g&#322;upienie"
  ]
  node [
    id 63
    label "cz&#322;owiek"
  ]
  node [
    id 64
    label "mondzio&#322;"
  ]
  node [
    id 65
    label "istota_&#380;ywa"
  ]
  node [
    id 66
    label "nierozwa&#380;ny"
  ]
  node [
    id 67
    label "niezr&#281;czny"
  ]
  node [
    id 68
    label "nadaremny"
  ]
  node [
    id 69
    label "bezmy&#347;lny"
  ]
  node [
    id 70
    label "bezcelowy"
  ]
  node [
    id 71
    label "uprzykrzony"
  ]
  node [
    id 72
    label "g&#322;upio"
  ]
  node [
    id 73
    label "niem&#261;dry"
  ]
  node [
    id 74
    label "niewa&#380;ny"
  ]
  node [
    id 75
    label "g&#322;uptas"
  ]
  node [
    id 76
    label "ma&#322;y"
  ]
  node [
    id 77
    label "&#347;mieszny"
  ]
  node [
    id 78
    label "zg&#322;upienie"
  ]
  node [
    id 79
    label "bezwolny"
  ]
  node [
    id 80
    label "bezsensowny"
  ]
  node [
    id 81
    label "zawarto&#347;&#263;"
  ]
  node [
    id 82
    label "naczynie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
]
