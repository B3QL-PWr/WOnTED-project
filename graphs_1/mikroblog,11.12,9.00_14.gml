graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "szary"
    origin "text"
  ]
  node [
    id 1
    label "myszka"
    origin "text"
  ]
  node [
    id 2
    label "dla"
    origin "text"
  ]
  node [
    id 3
    label "anonka"
    origin "text"
  ]
  node [
    id 4
    label "p&#322;owy"
  ]
  node [
    id 5
    label "zielono"
  ]
  node [
    id 6
    label "blady"
  ]
  node [
    id 7
    label "pochmurno"
  ]
  node [
    id 8
    label "szaro"
  ]
  node [
    id 9
    label "chmurnienie"
  ]
  node [
    id 10
    label "zwyczajny"
  ]
  node [
    id 11
    label "zwyk&#322;y"
  ]
  node [
    id 12
    label "szarzenie"
  ]
  node [
    id 13
    label "niezabawny"
  ]
  node [
    id 14
    label "brzydki"
  ]
  node [
    id 15
    label "nieciekawy"
  ]
  node [
    id 16
    label "oboj&#281;tny"
  ]
  node [
    id 17
    label "poszarzenie"
  ]
  node [
    id 18
    label "bezbarwnie"
  ]
  node [
    id 19
    label "ch&#322;odny"
  ]
  node [
    id 20
    label "srebrny"
  ]
  node [
    id 21
    label "spochmurnienie"
  ]
  node [
    id 22
    label "srom"
  ]
  node [
    id 23
    label "kobieta"
  ]
  node [
    id 24
    label "urz&#261;dzenie_wskazuj&#261;ce"
  ]
  node [
    id 25
    label "znami&#281;"
  ]
  node [
    id 26
    label "klawisz_myszki"
  ]
  node [
    id 27
    label "bouquet"
  ]
  node [
    id 28
    label "komputer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
]
