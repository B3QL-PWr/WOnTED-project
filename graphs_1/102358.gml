graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.0505050505050506
  density 0.01040865507870584
  graphCliqueNumber 3
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wzgl&#281;dny"
    origin "text"
  ]
  node [
    id 2
    label "wiedza"
    origin "text"
  ]
  node [
    id 3
    label "gazeta"
    origin "text"
  ]
  node [
    id 4
    label "mama"
    origin "text"
  ]
  node [
    id 5
    label "wra&#380;enie"
    origin "text"
  ]
  node [
    id 6
    label "g&#322;&#243;wnie"
    origin "text"
  ]
  node [
    id 7
    label "przebija&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "sensacja"
    origin "text"
  ]
  node [
    id 10
    label "uzyska&#263;"
    origin "text"
  ]
  node [
    id 11
    label "informacja"
    origin "text"
  ]
  node [
    id 12
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 13
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 14
    label "&#380;y&#263;"
    origin "text"
  ]
  node [
    id 15
    label "potrzebny"
    origin "text"
  ]
  node [
    id 16
    label "prasa"
    origin "text"
  ]
  node [
    id 17
    label "si&#281;ga&#263;"
  ]
  node [
    id 18
    label "trwa&#263;"
  ]
  node [
    id 19
    label "obecno&#347;&#263;"
  ]
  node [
    id 20
    label "stan"
  ]
  node [
    id 21
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 22
    label "stand"
  ]
  node [
    id 23
    label "mie&#263;_miejsce"
  ]
  node [
    id 24
    label "uczestniczy&#263;"
  ]
  node [
    id 25
    label "chodzi&#263;"
  ]
  node [
    id 26
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 27
    label "equal"
  ]
  node [
    id 28
    label "akceptowalny"
  ]
  node [
    id 29
    label "zale&#380;ny"
  ]
  node [
    id 30
    label "wzgl&#281;dnie"
  ]
  node [
    id 31
    label "pozwolenie"
  ]
  node [
    id 32
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 33
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 34
    label "wykszta&#322;cenie"
  ]
  node [
    id 35
    label "zaawansowanie"
  ]
  node [
    id 36
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 37
    label "intelekt"
  ]
  node [
    id 38
    label "cognition"
  ]
  node [
    id 39
    label "redakcja"
  ]
  node [
    id 40
    label "tytu&#322;"
  ]
  node [
    id 41
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 42
    label "czasopismo"
  ]
  node [
    id 43
    label "matczysko"
  ]
  node [
    id 44
    label "macierz"
  ]
  node [
    id 45
    label "przodkini"
  ]
  node [
    id 46
    label "Matka_Boska"
  ]
  node [
    id 47
    label "macocha"
  ]
  node [
    id 48
    label "matka_zast&#281;pcza"
  ]
  node [
    id 49
    label "stara"
  ]
  node [
    id 50
    label "rodzice"
  ]
  node [
    id 51
    label "rodzic"
  ]
  node [
    id 52
    label "reakcja"
  ]
  node [
    id 53
    label "odczucia"
  ]
  node [
    id 54
    label "czucie"
  ]
  node [
    id 55
    label "poczucie"
  ]
  node [
    id 56
    label "zmys&#322;"
  ]
  node [
    id 57
    label "przeczulica"
  ]
  node [
    id 58
    label "proces"
  ]
  node [
    id 59
    label "zjawisko"
  ]
  node [
    id 60
    label "g&#322;&#243;wny"
  ]
  node [
    id 61
    label "zmienia&#263;"
  ]
  node [
    id 62
    label "przenika&#263;"
  ]
  node [
    id 63
    label "wybija&#263;"
  ]
  node [
    id 64
    label "pokonywa&#263;"
  ]
  node [
    id 65
    label "bi&#263;"
  ]
  node [
    id 66
    label "rozgramia&#263;"
  ]
  node [
    id 67
    label "dopowiada&#263;"
  ]
  node [
    id 68
    label "przekrzykiwa&#263;"
  ]
  node [
    id 69
    label "tear"
  ]
  node [
    id 70
    label "oferowa&#263;"
  ]
  node [
    id 71
    label "dziurawi&#263;"
  ]
  node [
    id 72
    label "incision"
  ]
  node [
    id 73
    label "break"
  ]
  node [
    id 74
    label "przerzuca&#263;"
  ]
  node [
    id 75
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 76
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 77
    label "transgress"
  ]
  node [
    id 78
    label "trump"
  ]
  node [
    id 79
    label "zarysowywa&#263;_si&#281;"
  ]
  node [
    id 80
    label "m&#261;ci&#263;"
  ]
  node [
    id 81
    label "przeszywa&#263;"
  ]
  node [
    id 82
    label "wierci&#263;"
  ]
  node [
    id 83
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 84
    label "dojmowa&#263;"
  ]
  node [
    id 85
    label "deklasowa&#263;"
  ]
  node [
    id 86
    label "interrupt"
  ]
  node [
    id 87
    label "rozg&#322;os"
  ]
  node [
    id 88
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 89
    label "novum"
  ]
  node [
    id 90
    label "disclosure"
  ]
  node [
    id 91
    label "podekscytowanie"
  ]
  node [
    id 92
    label "zamieszanie"
  ]
  node [
    id 93
    label "niespodzianka"
  ]
  node [
    id 94
    label "promocja"
  ]
  node [
    id 95
    label "give_birth"
  ]
  node [
    id 96
    label "wytworzy&#263;"
  ]
  node [
    id 97
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 98
    label "realize"
  ]
  node [
    id 99
    label "zrobi&#263;"
  ]
  node [
    id 100
    label "make"
  ]
  node [
    id 101
    label "doj&#347;cie"
  ]
  node [
    id 102
    label "doj&#347;&#263;"
  ]
  node [
    id 103
    label "powzi&#261;&#263;"
  ]
  node [
    id 104
    label "sygna&#322;"
  ]
  node [
    id 105
    label "obiegni&#281;cie"
  ]
  node [
    id 106
    label "obieganie"
  ]
  node [
    id 107
    label "obiec"
  ]
  node [
    id 108
    label "dane"
  ]
  node [
    id 109
    label "obiega&#263;"
  ]
  node [
    id 110
    label "punkt"
  ]
  node [
    id 111
    label "publikacja"
  ]
  node [
    id 112
    label "powzi&#281;cie"
  ]
  node [
    id 113
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 114
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 115
    label "obszar"
  ]
  node [
    id 116
    label "obiekt_naturalny"
  ]
  node [
    id 117
    label "przedmiot"
  ]
  node [
    id 118
    label "biosfera"
  ]
  node [
    id 119
    label "grupa"
  ]
  node [
    id 120
    label "stw&#243;r"
  ]
  node [
    id 121
    label "Stary_&#346;wiat"
  ]
  node [
    id 122
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 123
    label "rzecz"
  ]
  node [
    id 124
    label "magnetosfera"
  ]
  node [
    id 125
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 126
    label "environment"
  ]
  node [
    id 127
    label "Nowy_&#346;wiat"
  ]
  node [
    id 128
    label "geosfera"
  ]
  node [
    id 129
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 130
    label "planeta"
  ]
  node [
    id 131
    label "przejmowa&#263;"
  ]
  node [
    id 132
    label "litosfera"
  ]
  node [
    id 133
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 134
    label "makrokosmos"
  ]
  node [
    id 135
    label "barysfera"
  ]
  node [
    id 136
    label "biota"
  ]
  node [
    id 137
    label "p&#243;&#322;noc"
  ]
  node [
    id 138
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 139
    label "fauna"
  ]
  node [
    id 140
    label "wszechstworzenie"
  ]
  node [
    id 141
    label "geotermia"
  ]
  node [
    id 142
    label "biegun"
  ]
  node [
    id 143
    label "ekosystem"
  ]
  node [
    id 144
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 145
    label "teren"
  ]
  node [
    id 146
    label "p&#243;&#322;kula"
  ]
  node [
    id 147
    label "atmosfera"
  ]
  node [
    id 148
    label "mikrokosmos"
  ]
  node [
    id 149
    label "class"
  ]
  node [
    id 150
    label "po&#322;udnie"
  ]
  node [
    id 151
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 152
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 153
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 154
    label "przejmowanie"
  ]
  node [
    id 155
    label "przestrze&#324;"
  ]
  node [
    id 156
    label "asymilowanie_si&#281;"
  ]
  node [
    id 157
    label "przej&#261;&#263;"
  ]
  node [
    id 158
    label "ekosfera"
  ]
  node [
    id 159
    label "przyroda"
  ]
  node [
    id 160
    label "ciemna_materia"
  ]
  node [
    id 161
    label "geoida"
  ]
  node [
    id 162
    label "Wsch&#243;d"
  ]
  node [
    id 163
    label "populace"
  ]
  node [
    id 164
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 165
    label "huczek"
  ]
  node [
    id 166
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 167
    label "Ziemia"
  ]
  node [
    id 168
    label "universe"
  ]
  node [
    id 169
    label "ozonosfera"
  ]
  node [
    id 170
    label "rze&#378;ba"
  ]
  node [
    id 171
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 172
    label "zagranica"
  ]
  node [
    id 173
    label "hydrosfera"
  ]
  node [
    id 174
    label "woda"
  ]
  node [
    id 175
    label "kuchnia"
  ]
  node [
    id 176
    label "przej&#281;cie"
  ]
  node [
    id 177
    label "czarna_dziura"
  ]
  node [
    id 178
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 179
    label "morze"
  ]
  node [
    id 180
    label "pause"
  ]
  node [
    id 181
    label "stay"
  ]
  node [
    id 182
    label "consist"
  ]
  node [
    id 183
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 184
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 185
    label "istnie&#263;"
  ]
  node [
    id 186
    label "potrzebnie"
  ]
  node [
    id 187
    label "przydatny"
  ]
  node [
    id 188
    label "pisa&#263;"
  ]
  node [
    id 189
    label "dziennikarz_prasowy"
  ]
  node [
    id 190
    label "napisa&#263;"
  ]
  node [
    id 191
    label "maszyna_rolnicza"
  ]
  node [
    id 192
    label "zesp&#243;&#322;"
  ]
  node [
    id 193
    label "t&#322;oczysko"
  ]
  node [
    id 194
    label "depesza"
  ]
  node [
    id 195
    label "maszyna"
  ]
  node [
    id 196
    label "media"
  ]
  node [
    id 197
    label "kiosk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 42
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 197
  ]
]
