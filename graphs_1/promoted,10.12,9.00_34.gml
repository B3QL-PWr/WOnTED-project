graph [
  maxDegree 37
  minDegree 1
  meanDegree 1.9523809523809523
  density 0.047619047619047616
  graphCliqueNumber 2
  node [
    id 0
    label "brawa"
    origin "text"
  ]
  node [
    id 1
    label "dla"
    origin "text"
  ]
  node [
    id 2
    label "ten"
    origin "text"
  ]
  node [
    id 3
    label "pan"
    origin "text"
  ]
  node [
    id 4
    label "okre&#347;lony"
  ]
  node [
    id 5
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "profesor"
  ]
  node [
    id 8
    label "kszta&#322;ciciel"
  ]
  node [
    id 9
    label "jegomo&#347;&#263;"
  ]
  node [
    id 10
    label "zwrot"
  ]
  node [
    id 11
    label "pracodawca"
  ]
  node [
    id 12
    label "rz&#261;dzenie"
  ]
  node [
    id 13
    label "m&#261;&#380;"
  ]
  node [
    id 14
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 15
    label "ch&#322;opina"
  ]
  node [
    id 16
    label "bratek"
  ]
  node [
    id 17
    label "opiekun"
  ]
  node [
    id 18
    label "doros&#322;y"
  ]
  node [
    id 19
    label "preceptor"
  ]
  node [
    id 20
    label "Midas"
  ]
  node [
    id 21
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 22
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 23
    label "murza"
  ]
  node [
    id 24
    label "ojciec"
  ]
  node [
    id 25
    label "androlog"
  ]
  node [
    id 26
    label "pupil"
  ]
  node [
    id 27
    label "efendi"
  ]
  node [
    id 28
    label "nabab"
  ]
  node [
    id 29
    label "w&#322;odarz"
  ]
  node [
    id 30
    label "szkolnik"
  ]
  node [
    id 31
    label "pedagog"
  ]
  node [
    id 32
    label "popularyzator"
  ]
  node [
    id 33
    label "andropauza"
  ]
  node [
    id 34
    label "gra_w_karty"
  ]
  node [
    id 35
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 36
    label "Mieszko_I"
  ]
  node [
    id 37
    label "bogaty"
  ]
  node [
    id 38
    label "samiec"
  ]
  node [
    id 39
    label "przyw&#243;dca"
  ]
  node [
    id 40
    label "pa&#324;stwo"
  ]
  node [
    id 41
    label "belfer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
]
