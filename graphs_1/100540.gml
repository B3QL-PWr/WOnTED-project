graph [
  maxDegree 18
  minDegree 1
  meanDegree 2.5714285714285716
  density 0.05357142857142857
  graphCliqueNumber 6
  node [
    id 0
    label "ulica"
    origin "text"
  ]
  node [
    id 1
    label "zacisz"
    origin "text"
  ]
  node [
    id 2
    label "katowice"
    origin "text"
  ]
  node [
    id 3
    label "&#347;rodowisko"
  ]
  node [
    id 4
    label "miasteczko"
  ]
  node [
    id 5
    label "streetball"
  ]
  node [
    id 6
    label "pierzeja"
  ]
  node [
    id 7
    label "grupa"
  ]
  node [
    id 8
    label "pas_ruchu"
  ]
  node [
    id 9
    label "jezdnia"
  ]
  node [
    id 10
    label "pas_rozdzielczy"
  ]
  node [
    id 11
    label "droga"
  ]
  node [
    id 12
    label "korona_drogi"
  ]
  node [
    id 13
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 14
    label "chodnik"
  ]
  node [
    id 15
    label "arteria"
  ]
  node [
    id 16
    label "Broadway"
  ]
  node [
    id 17
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 18
    label "wysepka"
  ]
  node [
    id 19
    label "autostrada"
  ]
  node [
    id 20
    label "akademia"
  ]
  node [
    id 21
    label "muzyczny"
  ]
  node [
    id 22
    label "Stanis&#322;awa"
  ]
  node [
    id 23
    label "Tabe&#324;skiego"
  ]
  node [
    id 24
    label "g&#243;rny"
  ]
  node [
    id 25
    label "&#347;l&#261;ski"
  ]
  node [
    id 26
    label "on"
  ]
  node [
    id 27
    label "Karol"
  ]
  node [
    id 28
    label "szymanowski"
  ]
  node [
    id 29
    label "centrum"
  ]
  node [
    id 30
    label "nauka"
  ]
  node [
    id 31
    label "i"
  ]
  node [
    id 32
    label "edukacja"
  ]
  node [
    id 33
    label "symfonia"
  ]
  node [
    id 34
    label "Tomasz"
  ]
  node [
    id 35
    label "Konior"
  ]
  node [
    id 36
    label "Krzysztofa"
  ]
  node [
    id 37
    label "barysz"
  ]
  node [
    id 38
    label "miejski"
  ]
  node [
    id 39
    label "przedszkole"
  ]
  node [
    id 40
    label "nr"
  ]
  node [
    id 41
    label "36"
  ]
  node [
    id 42
    label "J"
  ]
  node [
    id 43
    label "Lipo&#324;ska"
  ]
  node [
    id 44
    label "sajdak"
  ]
  node [
    id 45
    label "Katowice"
  ]
  node [
    id 46
    label "wczoraj"
  ]
  node [
    id 47
    label "Kattowiz"
  ]
  node [
    id 48
    label "gestern"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
]
