graph [
  maxDegree 30
  minDegree 1
  meanDegree 14.145454545454545
  density 0.26195286195286194
  graphCliqueNumber 27
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "ustawa"
    origin "text"
  ]
  node [
    id 2
    label "luty"
    origin "text"
  ]
  node [
    id 3
    label "poz"
    origin "text"
  ]
  node [
    id 4
    label "spis"
  ]
  node [
    id 5
    label "sheet"
  ]
  node [
    id 6
    label "gazeta"
  ]
  node [
    id 7
    label "diariusz"
  ]
  node [
    id 8
    label "pami&#281;tnik"
  ]
  node [
    id 9
    label "journal"
  ]
  node [
    id 10
    label "ksi&#281;ga"
  ]
  node [
    id 11
    label "program_informacyjny"
  ]
  node [
    id 12
    label "Karta_Nauczyciela"
  ]
  node [
    id 13
    label "marc&#243;wka"
  ]
  node [
    id 14
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 15
    label "akt"
  ]
  node [
    id 16
    label "przej&#347;&#263;"
  ]
  node [
    id 17
    label "charter"
  ]
  node [
    id 18
    label "przej&#347;cie"
  ]
  node [
    id 19
    label "miesi&#261;c"
  ]
  node [
    id 20
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 21
    label "walentynki"
  ]
  node [
    id 22
    label "ustawi&#263;"
  ]
  node [
    id 23
    label "minister"
  ]
  node [
    id 24
    label "gospodarka"
  ]
  node [
    id 25
    label "praca"
  ]
  node [
    id 26
    label "i"
  ]
  node [
    id 27
    label "polityka"
  ]
  node [
    id 28
    label "spo&#322;eczny"
  ]
  node [
    id 29
    label "prawo"
  ]
  node [
    id 30
    label "geologiczny"
  ]
  node [
    id 31
    label "g&#243;rniczy"
  ]
  node [
    id 32
    label "u"
  ]
  node [
    id 33
    label "rozporz&#261;dzi&#263;"
  ]
  node [
    id 34
    label "zeszyt"
  ]
  node [
    id 35
    label "dzie&#324;"
  ]
  node [
    id 36
    label "28"
  ]
  node [
    id 37
    label "czerwiec"
  ]
  node [
    id 38
    label "2002"
  ]
  node [
    id 39
    label "rok"
  ]
  node [
    id 40
    label "wyspa"
  ]
  node [
    id 41
    label "sprawa"
  ]
  node [
    id 42
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 43
    label "higiena"
  ]
  node [
    id 44
    label "prowadzi&#263;"
  ]
  node [
    id 45
    label "rucho"
  ]
  node [
    id 46
    label "oraz"
  ]
  node [
    id 47
    label "specjalistyczny"
  ]
  node [
    id 48
    label "zabezpieczy&#263;"
  ]
  node [
    id 49
    label "przeciwpo&#380;arowy"
  ]
  node [
    id 50
    label "zak&#322;ad"
  ]
  node [
    id 51
    label "wydobywa&#263;"
  ]
  node [
    id 52
    label "kopalina"
  ]
  node [
    id 53
    label "otw&#243;r"
  ]
  node [
    id 54
    label "wiertniczy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 33
  ]
  edge [
    source 23
    target 34
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 23
    target 38
  ]
  edge [
    source 23
    target 39
  ]
  edge [
    source 23
    target 40
  ]
  edge [
    source 23
    target 41
  ]
  edge [
    source 23
    target 42
  ]
  edge [
    source 23
    target 43
  ]
  edge [
    source 23
    target 44
  ]
  edge [
    source 23
    target 45
  ]
  edge [
    source 23
    target 46
  ]
  edge [
    source 23
    target 47
  ]
  edge [
    source 23
    target 48
  ]
  edge [
    source 23
    target 49
  ]
  edge [
    source 23
    target 50
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 51
  ]
  edge [
    source 23
    target 52
  ]
  edge [
    source 23
    target 53
  ]
  edge [
    source 23
    target 54
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 24
    target 37
  ]
  edge [
    source 24
    target 38
  ]
  edge [
    source 24
    target 39
  ]
  edge [
    source 24
    target 40
  ]
  edge [
    source 24
    target 41
  ]
  edge [
    source 24
    target 42
  ]
  edge [
    source 24
    target 43
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 24
    target 45
  ]
  edge [
    source 24
    target 46
  ]
  edge [
    source 24
    target 47
  ]
  edge [
    source 24
    target 48
  ]
  edge [
    source 24
    target 49
  ]
  edge [
    source 24
    target 50
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 51
  ]
  edge [
    source 24
    target 52
  ]
  edge [
    source 24
    target 53
  ]
  edge [
    source 24
    target 54
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 34
  ]
  edge [
    source 25
    target 35
  ]
  edge [
    source 25
    target 36
  ]
  edge [
    source 25
    target 37
  ]
  edge [
    source 25
    target 38
  ]
  edge [
    source 25
    target 39
  ]
  edge [
    source 25
    target 40
  ]
  edge [
    source 25
    target 41
  ]
  edge [
    source 25
    target 42
  ]
  edge [
    source 25
    target 43
  ]
  edge [
    source 25
    target 44
  ]
  edge [
    source 25
    target 45
  ]
  edge [
    source 25
    target 46
  ]
  edge [
    source 25
    target 47
  ]
  edge [
    source 25
    target 48
  ]
  edge [
    source 25
    target 49
  ]
  edge [
    source 25
    target 50
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 25
    target 51
  ]
  edge [
    source 25
    target 52
  ]
  edge [
    source 25
    target 53
  ]
  edge [
    source 25
    target 54
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 26
    target 37
  ]
  edge [
    source 26
    target 38
  ]
  edge [
    source 26
    target 39
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 41
  ]
  edge [
    source 26
    target 42
  ]
  edge [
    source 26
    target 43
  ]
  edge [
    source 26
    target 44
  ]
  edge [
    source 26
    target 45
  ]
  edge [
    source 26
    target 46
  ]
  edge [
    source 26
    target 47
  ]
  edge [
    source 26
    target 48
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 50
  ]
  edge [
    source 26
    target 51
  ]
  edge [
    source 26
    target 52
  ]
  edge [
    source 26
    target 53
  ]
  edge [
    source 26
    target 54
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 38
  ]
  edge [
    source 31
    target 39
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 31
    target 41
  ]
  edge [
    source 31
    target 42
  ]
  edge [
    source 31
    target 43
  ]
  edge [
    source 31
    target 44
  ]
  edge [
    source 31
    target 45
  ]
  edge [
    source 31
    target 46
  ]
  edge [
    source 31
    target 47
  ]
  edge [
    source 31
    target 48
  ]
  edge [
    source 31
    target 49
  ]
  edge [
    source 31
    target 50
  ]
  edge [
    source 31
    target 51
  ]
  edge [
    source 31
    target 52
  ]
  edge [
    source 31
    target 53
  ]
  edge [
    source 31
    target 54
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 33
    target 41
  ]
  edge [
    source 33
    target 42
  ]
  edge [
    source 33
    target 43
  ]
  edge [
    source 33
    target 44
  ]
  edge [
    source 33
    target 45
  ]
  edge [
    source 33
    target 46
  ]
  edge [
    source 33
    target 47
  ]
  edge [
    source 33
    target 48
  ]
  edge [
    source 33
    target 49
  ]
  edge [
    source 33
    target 50
  ]
  edge [
    source 33
    target 51
  ]
  edge [
    source 33
    target 52
  ]
  edge [
    source 33
    target 53
  ]
  edge [
    source 33
    target 54
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 34
    target 42
  ]
  edge [
    source 34
    target 43
  ]
  edge [
    source 34
    target 44
  ]
  edge [
    source 34
    target 45
  ]
  edge [
    source 34
    target 46
  ]
  edge [
    source 34
    target 47
  ]
  edge [
    source 34
    target 48
  ]
  edge [
    source 34
    target 49
  ]
  edge [
    source 34
    target 50
  ]
  edge [
    source 34
    target 51
  ]
  edge [
    source 34
    target 52
  ]
  edge [
    source 34
    target 53
  ]
  edge [
    source 34
    target 54
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 39
  ]
  edge [
    source 35
    target 40
  ]
  edge [
    source 35
    target 41
  ]
  edge [
    source 35
    target 42
  ]
  edge [
    source 35
    target 43
  ]
  edge [
    source 35
    target 44
  ]
  edge [
    source 35
    target 45
  ]
  edge [
    source 35
    target 46
  ]
  edge [
    source 35
    target 47
  ]
  edge [
    source 35
    target 48
  ]
  edge [
    source 35
    target 49
  ]
  edge [
    source 35
    target 50
  ]
  edge [
    source 35
    target 51
  ]
  edge [
    source 35
    target 52
  ]
  edge [
    source 35
    target 53
  ]
  edge [
    source 35
    target 54
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 36
    target 41
  ]
  edge [
    source 36
    target 42
  ]
  edge [
    source 36
    target 43
  ]
  edge [
    source 36
    target 44
  ]
  edge [
    source 36
    target 45
  ]
  edge [
    source 36
    target 46
  ]
  edge [
    source 36
    target 47
  ]
  edge [
    source 36
    target 48
  ]
  edge [
    source 36
    target 49
  ]
  edge [
    source 36
    target 50
  ]
  edge [
    source 36
    target 51
  ]
  edge [
    source 36
    target 52
  ]
  edge [
    source 36
    target 53
  ]
  edge [
    source 36
    target 54
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 37
    target 41
  ]
  edge [
    source 37
    target 42
  ]
  edge [
    source 37
    target 43
  ]
  edge [
    source 37
    target 44
  ]
  edge [
    source 37
    target 45
  ]
  edge [
    source 37
    target 46
  ]
  edge [
    source 37
    target 47
  ]
  edge [
    source 37
    target 48
  ]
  edge [
    source 37
    target 49
  ]
  edge [
    source 37
    target 50
  ]
  edge [
    source 37
    target 51
  ]
  edge [
    source 37
    target 52
  ]
  edge [
    source 37
    target 53
  ]
  edge [
    source 37
    target 54
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 38
    target 42
  ]
  edge [
    source 38
    target 43
  ]
  edge [
    source 38
    target 44
  ]
  edge [
    source 38
    target 45
  ]
  edge [
    source 38
    target 46
  ]
  edge [
    source 38
    target 47
  ]
  edge [
    source 38
    target 48
  ]
  edge [
    source 38
    target 49
  ]
  edge [
    source 38
    target 50
  ]
  edge [
    source 38
    target 51
  ]
  edge [
    source 38
    target 52
  ]
  edge [
    source 38
    target 53
  ]
  edge [
    source 38
    target 54
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 39
    target 43
  ]
  edge [
    source 39
    target 44
  ]
  edge [
    source 39
    target 45
  ]
  edge [
    source 39
    target 46
  ]
  edge [
    source 39
    target 47
  ]
  edge [
    source 39
    target 48
  ]
  edge [
    source 39
    target 49
  ]
  edge [
    source 39
    target 50
  ]
  edge [
    source 39
    target 51
  ]
  edge [
    source 39
    target 52
  ]
  edge [
    source 39
    target 53
  ]
  edge [
    source 39
    target 54
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 40
    target 44
  ]
  edge [
    source 40
    target 45
  ]
  edge [
    source 40
    target 46
  ]
  edge [
    source 40
    target 47
  ]
  edge [
    source 40
    target 48
  ]
  edge [
    source 40
    target 49
  ]
  edge [
    source 40
    target 40
  ]
  edge [
    source 40
    target 50
  ]
  edge [
    source 40
    target 51
  ]
  edge [
    source 40
    target 52
  ]
  edge [
    source 40
    target 53
  ]
  edge [
    source 40
    target 54
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 45
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 48
  ]
  edge [
    source 41
    target 49
  ]
  edge [
    source 41
    target 50
  ]
  edge [
    source 41
    target 51
  ]
  edge [
    source 41
    target 52
  ]
  edge [
    source 41
    target 53
  ]
  edge [
    source 41
    target 54
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 42
    target 48
  ]
  edge [
    source 42
    target 49
  ]
  edge [
    source 42
    target 50
  ]
  edge [
    source 42
    target 51
  ]
  edge [
    source 42
    target 52
  ]
  edge [
    source 42
    target 53
  ]
  edge [
    source 42
    target 54
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 43
    target 47
  ]
  edge [
    source 43
    target 48
  ]
  edge [
    source 43
    target 49
  ]
  edge [
    source 43
    target 50
  ]
  edge [
    source 43
    target 51
  ]
  edge [
    source 43
    target 52
  ]
  edge [
    source 43
    target 53
  ]
  edge [
    source 43
    target 54
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 44
    target 49
  ]
  edge [
    source 44
    target 50
  ]
  edge [
    source 44
    target 51
  ]
  edge [
    source 44
    target 52
  ]
  edge [
    source 44
    target 53
  ]
  edge [
    source 44
    target 54
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 45
    target 49
  ]
  edge [
    source 45
    target 50
  ]
  edge [
    source 45
    target 51
  ]
  edge [
    source 45
    target 52
  ]
  edge [
    source 45
    target 53
  ]
  edge [
    source 45
    target 54
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 46
    target 50
  ]
  edge [
    source 46
    target 51
  ]
  edge [
    source 46
    target 52
  ]
  edge [
    source 46
    target 53
  ]
  edge [
    source 46
    target 54
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 47
    target 52
  ]
  edge [
    source 47
    target 53
  ]
  edge [
    source 47
    target 54
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 48
    target 53
  ]
  edge [
    source 48
    target 54
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 49
    target 52
  ]
  edge [
    source 49
    target 53
  ]
  edge [
    source 49
    target 54
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 53
    target 54
  ]
]
