graph [
  maxDegree 14
  minDegree 1
  meanDegree 2.210526315789474
  density 0.029473684210526315
  graphCliqueNumber 3
  node [
    id 0
    label "cukier"
    origin "text"
  ]
  node [
    id 1
    label "inny"
    origin "text"
  ]
  node [
    id 2
    label "substancja"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;odzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "na_przyk&#322;ad"
    origin "text"
  ]
  node [
    id 5
    label "syrop"
    origin "text"
  ]
  node [
    id 6
    label "glukozowo"
    origin "text"
  ]
  node [
    id 7
    label "fruktozowy"
    origin "text"
  ]
  node [
    id 8
    label "ukry&#263;"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "nieoczywisty"
    origin "text"
  ]
  node [
    id 11
    label "produkt"
    origin "text"
  ]
  node [
    id 12
    label "taki"
    origin "text"
  ]
  node [
    id 13
    label "chleb"
    origin "text"
  ]
  node [
    id 14
    label "kie&#322;basa"
    origin "text"
  ]
  node [
    id 15
    label "w&#281;dlina"
    origin "text"
  ]
  node [
    id 16
    label "crusta"
  ]
  node [
    id 17
    label "s&#322;odki"
  ]
  node [
    id 18
    label "przyprawa"
  ]
  node [
    id 19
    label "sk&#322;adnik_pokarmowy"
  ]
  node [
    id 20
    label "carbohydrate"
  ]
  node [
    id 21
    label "w&#281;giel"
  ]
  node [
    id 22
    label "grupa_hydroksylowa"
  ]
  node [
    id 23
    label "sacharoza"
  ]
  node [
    id 24
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 25
    label "porcja"
  ]
  node [
    id 26
    label "kolejny"
  ]
  node [
    id 27
    label "inaczej"
  ]
  node [
    id 28
    label "r&#243;&#380;ny"
  ]
  node [
    id 29
    label "inszy"
  ]
  node [
    id 30
    label "osobno"
  ]
  node [
    id 31
    label "byt"
  ]
  node [
    id 32
    label "smolisty"
  ]
  node [
    id 33
    label "przenika&#263;"
  ]
  node [
    id 34
    label "cz&#261;steczka"
  ]
  node [
    id 35
    label "materia"
  ]
  node [
    id 36
    label "przenikanie"
  ]
  node [
    id 37
    label "temperatura_krytyczna"
  ]
  node [
    id 38
    label "przyprawia&#263;"
  ]
  node [
    id 39
    label "komplementowa&#263;"
  ]
  node [
    id 40
    label "sugar"
  ]
  node [
    id 41
    label "treacle"
  ]
  node [
    id 42
    label "jedzenie"
  ]
  node [
    id 43
    label "zachowa&#263;"
  ]
  node [
    id 44
    label "ensconce"
  ]
  node [
    id 45
    label "hide"
  ]
  node [
    id 46
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 47
    label "umie&#347;ci&#263;"
  ]
  node [
    id 48
    label "przytai&#263;"
  ]
  node [
    id 49
    label "si&#281;ga&#263;"
  ]
  node [
    id 50
    label "trwa&#263;"
  ]
  node [
    id 51
    label "obecno&#347;&#263;"
  ]
  node [
    id 52
    label "stan"
  ]
  node [
    id 53
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 54
    label "stand"
  ]
  node [
    id 55
    label "mie&#263;_miejsce"
  ]
  node [
    id 56
    label "uczestniczy&#263;"
  ]
  node [
    id 57
    label "chodzi&#263;"
  ]
  node [
    id 58
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 59
    label "equal"
  ]
  node [
    id 60
    label "nieoczywi&#347;cie"
  ]
  node [
    id 61
    label "oryginalny"
  ]
  node [
    id 62
    label "production"
  ]
  node [
    id 63
    label "rezultat"
  ]
  node [
    id 64
    label "wytw&#243;r"
  ]
  node [
    id 65
    label "okre&#347;lony"
  ]
  node [
    id 66
    label "jaki&#347;"
  ]
  node [
    id 67
    label "dar_bo&#380;y"
  ]
  node [
    id 68
    label "bochenek"
  ]
  node [
    id 69
    label "pieczywo"
  ]
  node [
    id 70
    label "wypiek"
  ]
  node [
    id 71
    label "konsubstancjacja"
  ]
  node [
    id 72
    label "utrzymanie"
  ]
  node [
    id 73
    label "kie&#322;bacha"
  ]
  node [
    id 74
    label "kie&#322;ba&#347;nica"
  ]
  node [
    id 75
    label "flak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 42
  ]
]
