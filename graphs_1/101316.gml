graph [
  maxDegree 70
  minDegree 1
  meanDegree 2.267605633802817
  density 0.005335542667771334
  graphCliqueNumber 7
  node [
    id 0
    label "jakub"
    origin "text"
  ]
  node [
    id 1
    label "kazimierz"
    origin "text"
  ]
  node [
    id 2
    label "haur"
    origin "text"
  ]
  node [
    id 3
    label "urodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "mieszcza&#324;ski"
    origin "text"
  ]
  node [
    id 7
    label "rodzina"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "syn"
    origin "text"
  ]
  node [
    id 10
    label "warszawskie"
    origin "text"
  ]
  node [
    id 11
    label "&#322;awnik"
    origin "text"
  ]
  node [
    id 12
    label "haura"
    origin "text"
  ]
  node [
    id 13
    label "pochodzi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "grazu"
    origin "text"
  ]
  node [
    id 15
    label "anna"
    origin "text"
  ]
  node [
    id 16
    label "maria"
    origin "text"
  ]
  node [
    id 17
    label "dom"
    origin "text"
  ]
  node [
    id 18
    label "petard&#243;wny"
    origin "text"
  ]
  node [
    id 19
    label "dw&#243;rka"
    origin "text"
  ]
  node [
    id 20
    label "kr&#243;lowa"
    origin "text"
  ]
  node [
    id 21
    label "konstancja"
    origin "text"
  ]
  node [
    id 22
    label "&#380;ona"
    origin "text"
  ]
  node [
    id 23
    label "zygmunt"
    origin "text"
  ]
  node [
    id 24
    label "iii"
    origin "text"
  ]
  node [
    id 25
    label "waza"
    origin "text"
  ]
  node [
    id 26
    label "studia"
    origin "text"
  ]
  node [
    id 27
    label "odbywa&#263;"
    origin "text"
  ]
  node [
    id 28
    label "akademia"
    origin "text"
  ]
  node [
    id 29
    label "krakowski"
    origin "text"
  ]
  node [
    id 30
    label "owo"
    origin "text"
  ]
  node [
    id 31
    label "czas"
    origin "text"
  ]
  node [
    id 32
    label "wci&#261;&#380;"
    origin "text"
  ]
  node [
    id 33
    label "jedyna"
    origin "text"
  ]
  node [
    id 34
    label "uczelnia"
    origin "text"
  ]
  node [
    id 35
    label "wysoki"
    origin "text"
  ]
  node [
    id 36
    label "polska"
    origin "text"
  ]
  node [
    id 37
    label "lata"
    origin "text"
  ]
  node [
    id 38
    label "okres"
    origin "text"
  ]
  node [
    id 39
    label "o&#380;ywiony"
    origin "text"
  ]
  node [
    id 40
    label "kontakt"
    origin "text"
  ]
  node [
    id 41
    label "bogaty"
    origin "text"
  ]
  node [
    id 42
    label "warszawa"
    origin "text"
  ]
  node [
    id 43
    label "lw&#243;w"
    origin "text"
  ]
  node [
    id 44
    label "g&#322;&#243;wnie"
    origin "text"
  ]
  node [
    id 45
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 46
    label "pokrewie&#324;stwo"
    origin "text"
  ]
  node [
    id 47
    label "dzianottich"
    origin "text"
  ]
  node [
    id 48
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 49
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 50
    label "gibbonich"
    origin "text"
  ]
  node [
    id 51
    label "koneksja"
    origin "text"
  ]
  node [
    id 52
    label "rodzinny"
    origin "text"
  ]
  node [
    id 53
    label "przez"
    origin "text"
  ]
  node [
    id 54
    label "siostra"
    origin "text"
  ]
  node [
    id 55
    label "weronika"
    origin "text"
  ]
  node [
    id 56
    label "lwowskie"
    origin "text"
  ]
  node [
    id 57
    label "mieszczanin"
    origin "text"
  ]
  node [
    id 58
    label "stanis&#322;aw"
    origin "text"
  ]
  node [
    id 59
    label "kralem"
    origin "text"
  ]
  node [
    id 60
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 61
    label "lwowski"
    origin "text"
  ]
  node [
    id 62
    label "morykonich"
    origin "text"
  ]
  node [
    id 63
    label "ma&#347;lak_pstry"
  ]
  node [
    id 64
    label "narodzi&#263;"
  ]
  node [
    id 65
    label "zlec"
  ]
  node [
    id 66
    label "engender"
  ]
  node [
    id 67
    label "powi&#263;"
  ]
  node [
    id 68
    label "zrobi&#263;"
  ]
  node [
    id 69
    label "porodzi&#263;"
  ]
  node [
    id 70
    label "stulecie"
  ]
  node [
    id 71
    label "kalendarz"
  ]
  node [
    id 72
    label "pora_roku"
  ]
  node [
    id 73
    label "cykl_astronomiczny"
  ]
  node [
    id 74
    label "p&#243;&#322;rocze"
  ]
  node [
    id 75
    label "grupa"
  ]
  node [
    id 76
    label "kwarta&#322;"
  ]
  node [
    id 77
    label "kurs"
  ]
  node [
    id 78
    label "jubileusz"
  ]
  node [
    id 79
    label "miesi&#261;c"
  ]
  node [
    id 80
    label "martwy_sezon"
  ]
  node [
    id 81
    label "po_mieszcza&#324;sku"
  ]
  node [
    id 82
    label "miejski"
  ]
  node [
    id 83
    label "miastowy"
  ]
  node [
    id 84
    label "prosty"
  ]
  node [
    id 85
    label "konserwatywny"
  ]
  node [
    id 86
    label "konformistyczny"
  ]
  node [
    id 87
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 88
    label "ograniczony"
  ]
  node [
    id 89
    label "mieszcza&#324;sko"
  ]
  node [
    id 90
    label "krewni"
  ]
  node [
    id 91
    label "Firlejowie"
  ]
  node [
    id 92
    label "Ossoli&#324;scy"
  ]
  node [
    id 93
    label "rodze&#324;stwo"
  ]
  node [
    id 94
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 95
    label "rz&#261;d"
  ]
  node [
    id 96
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 97
    label "przyjaciel_domu"
  ]
  node [
    id 98
    label "Ostrogscy"
  ]
  node [
    id 99
    label "theater"
  ]
  node [
    id 100
    label "dom_rodzinny"
  ]
  node [
    id 101
    label "potomstwo"
  ]
  node [
    id 102
    label "Soplicowie"
  ]
  node [
    id 103
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 104
    label "Czartoryscy"
  ]
  node [
    id 105
    label "family"
  ]
  node [
    id 106
    label "kin"
  ]
  node [
    id 107
    label "bliscy"
  ]
  node [
    id 108
    label "powinowaci"
  ]
  node [
    id 109
    label "Sapiehowie"
  ]
  node [
    id 110
    label "ordynacja"
  ]
  node [
    id 111
    label "jednostka_systematyczna"
  ]
  node [
    id 112
    label "zbi&#243;r"
  ]
  node [
    id 113
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 114
    label "Kossakowie"
  ]
  node [
    id 115
    label "rodzice"
  ]
  node [
    id 116
    label "si&#281;ga&#263;"
  ]
  node [
    id 117
    label "trwa&#263;"
  ]
  node [
    id 118
    label "obecno&#347;&#263;"
  ]
  node [
    id 119
    label "stan"
  ]
  node [
    id 120
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 121
    label "stand"
  ]
  node [
    id 122
    label "mie&#263;_miejsce"
  ]
  node [
    id 123
    label "uczestniczy&#263;"
  ]
  node [
    id 124
    label "chodzi&#263;"
  ]
  node [
    id 125
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 126
    label "equal"
  ]
  node [
    id 127
    label "usynowienie"
  ]
  node [
    id 128
    label "usynawianie"
  ]
  node [
    id 129
    label "dziecko"
  ]
  node [
    id 130
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 131
    label "przysi&#281;g&#322;y"
  ]
  node [
    id 132
    label "s&#261;d_przysi&#281;g&#322;ych"
  ]
  node [
    id 133
    label "samorz&#261;dowiec"
  ]
  node [
    id 134
    label "radny"
  ]
  node [
    id 135
    label "obywatel"
  ]
  node [
    id 136
    label "date"
  ]
  node [
    id 137
    label "str&#243;j"
  ]
  node [
    id 138
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 139
    label "spowodowa&#263;"
  ]
  node [
    id 140
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 141
    label "uda&#263;_si&#281;"
  ]
  node [
    id 142
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 143
    label "poby&#263;"
  ]
  node [
    id 144
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 145
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 146
    label "wynika&#263;"
  ]
  node [
    id 147
    label "fall"
  ]
  node [
    id 148
    label "bolt"
  ]
  node [
    id 149
    label "garderoba"
  ]
  node [
    id 150
    label "wiecha"
  ]
  node [
    id 151
    label "budynek"
  ]
  node [
    id 152
    label "fratria"
  ]
  node [
    id 153
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 154
    label "poj&#281;cie"
  ]
  node [
    id 155
    label "substancja_mieszkaniowa"
  ]
  node [
    id 156
    label "instytucja"
  ]
  node [
    id 157
    label "stead"
  ]
  node [
    id 158
    label "siedziba"
  ]
  node [
    id 159
    label "dw&#243;r"
  ]
  node [
    id 160
    label "fraucymer"
  ]
  node [
    id 161
    label "szlachcianka"
  ]
  node [
    id 162
    label "Izabela_I_Kastylijska"
  ]
  node [
    id 163
    label "pszczo&#322;a"
  ]
  node [
    id 164
    label "Bona"
  ]
  node [
    id 165
    label "Wiktoria"
  ]
  node [
    id 166
    label "kand"
  ]
  node [
    id 167
    label "strzelec"
  ]
  node [
    id 168
    label "El&#380;bieta_I"
  ]
  node [
    id 169
    label "kr&#243;lowa_matka"
  ]
  node [
    id 170
    label "mr&#243;wka"
  ]
  node [
    id 171
    label "ma&#322;&#380;onek"
  ]
  node [
    id 172
    label "panna_m&#322;oda"
  ]
  node [
    id 173
    label "partnerka"
  ]
  node [
    id 174
    label "&#347;lubna"
  ]
  node [
    id 175
    label "kobita"
  ]
  node [
    id 176
    label "serwis"
  ]
  node [
    id 177
    label "zawarto&#347;&#263;"
  ]
  node [
    id 178
    label "vase"
  ]
  node [
    id 179
    label "naczynie"
  ]
  node [
    id 180
    label "badanie"
  ]
  node [
    id 181
    label "nauka"
  ]
  node [
    id 182
    label "przechodzi&#263;"
  ]
  node [
    id 183
    label "hold"
  ]
  node [
    id 184
    label "WAT"
  ]
  node [
    id 185
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 186
    label "plac&#243;wka_naukowa"
  ]
  node [
    id 187
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 188
    label "g&#322;&#243;g"
  ]
  node [
    id 189
    label "ma&#322;opolski"
  ]
  node [
    id 190
    label "po_krakowsku"
  ]
  node [
    id 191
    label "czasokres"
  ]
  node [
    id 192
    label "trawienie"
  ]
  node [
    id 193
    label "kategoria_gramatyczna"
  ]
  node [
    id 194
    label "period"
  ]
  node [
    id 195
    label "odczyt"
  ]
  node [
    id 196
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 197
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 198
    label "chwila"
  ]
  node [
    id 199
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 200
    label "poprzedzenie"
  ]
  node [
    id 201
    label "koniugacja"
  ]
  node [
    id 202
    label "dzieje"
  ]
  node [
    id 203
    label "poprzedzi&#263;"
  ]
  node [
    id 204
    label "przep&#322;ywanie"
  ]
  node [
    id 205
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 206
    label "odwlekanie_si&#281;"
  ]
  node [
    id 207
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 208
    label "Zeitgeist"
  ]
  node [
    id 209
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 210
    label "okres_czasu"
  ]
  node [
    id 211
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 212
    label "schy&#322;ek"
  ]
  node [
    id 213
    label "czwarty_wymiar"
  ]
  node [
    id 214
    label "chronometria"
  ]
  node [
    id 215
    label "poprzedzanie"
  ]
  node [
    id 216
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 217
    label "pogoda"
  ]
  node [
    id 218
    label "zegar"
  ]
  node [
    id 219
    label "trawi&#263;"
  ]
  node [
    id 220
    label "pochodzenie"
  ]
  node [
    id 221
    label "poprzedza&#263;"
  ]
  node [
    id 222
    label "time_period"
  ]
  node [
    id 223
    label "rachuba_czasu"
  ]
  node [
    id 224
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 225
    label "czasoprzestrze&#324;"
  ]
  node [
    id 226
    label "laba"
  ]
  node [
    id 227
    label "ci&#261;g&#322;y"
  ]
  node [
    id 228
    label "stale"
  ]
  node [
    id 229
    label "kobieta"
  ]
  node [
    id 230
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 231
    label "rektorat"
  ]
  node [
    id 232
    label "podkanclerz"
  ]
  node [
    id 233
    label "kanclerz"
  ]
  node [
    id 234
    label "kwestura"
  ]
  node [
    id 235
    label "miasteczko_studenckie"
  ]
  node [
    id 236
    label "school"
  ]
  node [
    id 237
    label "senat"
  ]
  node [
    id 238
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 239
    label "wyk&#322;adanie"
  ]
  node [
    id 240
    label "promotorstwo"
  ]
  node [
    id 241
    label "szko&#322;a"
  ]
  node [
    id 242
    label "warto&#347;ciowy"
  ]
  node [
    id 243
    label "du&#380;y"
  ]
  node [
    id 244
    label "wysoce"
  ]
  node [
    id 245
    label "daleki"
  ]
  node [
    id 246
    label "znaczny"
  ]
  node [
    id 247
    label "wysoko"
  ]
  node [
    id 248
    label "szczytnie"
  ]
  node [
    id 249
    label "wznios&#322;y"
  ]
  node [
    id 250
    label "wyrafinowany"
  ]
  node [
    id 251
    label "z_wysoka"
  ]
  node [
    id 252
    label "chwalebny"
  ]
  node [
    id 253
    label "uprzywilejowany"
  ]
  node [
    id 254
    label "niepo&#347;ledni"
  ]
  node [
    id 255
    label "summer"
  ]
  node [
    id 256
    label "paleogen"
  ]
  node [
    id 257
    label "spell"
  ]
  node [
    id 258
    label "prekambr"
  ]
  node [
    id 259
    label "jura"
  ]
  node [
    id 260
    label "interstadia&#322;"
  ]
  node [
    id 261
    label "jednostka_geologiczna"
  ]
  node [
    id 262
    label "izochronizm"
  ]
  node [
    id 263
    label "okres_noachijski"
  ]
  node [
    id 264
    label "orosir"
  ]
  node [
    id 265
    label "kreda"
  ]
  node [
    id 266
    label "sten"
  ]
  node [
    id 267
    label "drugorz&#281;d"
  ]
  node [
    id 268
    label "semester"
  ]
  node [
    id 269
    label "trzeciorz&#281;d"
  ]
  node [
    id 270
    label "ton"
  ]
  node [
    id 271
    label "poprzednik"
  ]
  node [
    id 272
    label "ordowik"
  ]
  node [
    id 273
    label "karbon"
  ]
  node [
    id 274
    label "trias"
  ]
  node [
    id 275
    label "kalim"
  ]
  node [
    id 276
    label "stater"
  ]
  node [
    id 277
    label "era"
  ]
  node [
    id 278
    label "cykl"
  ]
  node [
    id 279
    label "p&#243;&#322;okres"
  ]
  node [
    id 280
    label "czwartorz&#281;d"
  ]
  node [
    id 281
    label "pulsacja"
  ]
  node [
    id 282
    label "okres_amazo&#324;ski"
  ]
  node [
    id 283
    label "kambr"
  ]
  node [
    id 284
    label "nast&#281;pnik"
  ]
  node [
    id 285
    label "kriogen"
  ]
  node [
    id 286
    label "glacja&#322;"
  ]
  node [
    id 287
    label "fala"
  ]
  node [
    id 288
    label "riak"
  ]
  node [
    id 289
    label "okres_hesperyjski"
  ]
  node [
    id 290
    label "sylur"
  ]
  node [
    id 291
    label "dewon"
  ]
  node [
    id 292
    label "ciota"
  ]
  node [
    id 293
    label "epoka"
  ]
  node [
    id 294
    label "pierwszorz&#281;d"
  ]
  node [
    id 295
    label "okres_halsztacki"
  ]
  node [
    id 296
    label "ektas"
  ]
  node [
    id 297
    label "zdanie"
  ]
  node [
    id 298
    label "condition"
  ]
  node [
    id 299
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 300
    label "rok_akademicki"
  ]
  node [
    id 301
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 302
    label "postglacja&#322;"
  ]
  node [
    id 303
    label "faza"
  ]
  node [
    id 304
    label "proces_fizjologiczny"
  ]
  node [
    id 305
    label "ediakar"
  ]
  node [
    id 306
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 307
    label "perm"
  ]
  node [
    id 308
    label "rok_szkolny"
  ]
  node [
    id 309
    label "neogen"
  ]
  node [
    id 310
    label "sider"
  ]
  node [
    id 311
    label "flow"
  ]
  node [
    id 312
    label "podokres"
  ]
  node [
    id 313
    label "preglacja&#322;"
  ]
  node [
    id 314
    label "retoryka"
  ]
  node [
    id 315
    label "choroba_przyrodzona"
  ]
  node [
    id 316
    label "czynny"
  ]
  node [
    id 317
    label "intensywny"
  ]
  node [
    id 318
    label "&#380;ywy"
  ]
  node [
    id 319
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 320
    label "formacja_geologiczna"
  ]
  node [
    id 321
    label "zwi&#261;zek"
  ]
  node [
    id 322
    label "soczewka"
  ]
  node [
    id 323
    label "contact"
  ]
  node [
    id 324
    label "linkage"
  ]
  node [
    id 325
    label "katalizator"
  ]
  node [
    id 326
    label "z&#322;&#261;czenie"
  ]
  node [
    id 327
    label "&#322;&#261;cznik_mechanizmowy"
  ]
  node [
    id 328
    label "regulator"
  ]
  node [
    id 329
    label "styk"
  ]
  node [
    id 330
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 331
    label "wydarzenie"
  ]
  node [
    id 332
    label "communication"
  ]
  node [
    id 333
    label "instalacja_elektryczna"
  ]
  node [
    id 334
    label "&#322;&#261;cznik"
  ]
  node [
    id 335
    label "socket"
  ]
  node [
    id 336
    label "association"
  ]
  node [
    id 337
    label "cz&#322;owiek"
  ]
  node [
    id 338
    label "nabab"
  ]
  node [
    id 339
    label "forsiasty"
  ]
  node [
    id 340
    label "obfituj&#261;cy"
  ]
  node [
    id 341
    label "r&#243;&#380;norodny"
  ]
  node [
    id 342
    label "sytuowany"
  ]
  node [
    id 343
    label "zapa&#347;ny"
  ]
  node [
    id 344
    label "obficie"
  ]
  node [
    id 345
    label "spania&#322;y"
  ]
  node [
    id 346
    label "och&#281;do&#380;ny"
  ]
  node [
    id 347
    label "bogato"
  ]
  node [
    id 348
    label "Warszawa"
  ]
  node [
    id 349
    label "samoch&#243;d"
  ]
  node [
    id 350
    label "fastback"
  ]
  node [
    id 351
    label "g&#322;&#243;wny"
  ]
  node [
    id 352
    label "alliance"
  ]
  node [
    id 353
    label "koligacja"
  ]
  node [
    id 354
    label "podobie&#324;stwo"
  ]
  node [
    id 355
    label "service"
  ]
  node [
    id 356
    label "ZOMO"
  ]
  node [
    id 357
    label "czworak"
  ]
  node [
    id 358
    label "zesp&#243;&#322;"
  ]
  node [
    id 359
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 360
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 361
    label "praca"
  ]
  node [
    id 362
    label "wys&#322;uga"
  ]
  node [
    id 363
    label "familijnie"
  ]
  node [
    id 364
    label "rodzinnie"
  ]
  node [
    id 365
    label "przyjemny"
  ]
  node [
    id 366
    label "wsp&#243;lny"
  ]
  node [
    id 367
    label "charakterystyczny"
  ]
  node [
    id 368
    label "swobodny"
  ]
  node [
    id 369
    label "zwi&#261;zany"
  ]
  node [
    id 370
    label "towarzyski"
  ]
  node [
    id 371
    label "ciep&#322;y"
  ]
  node [
    id 372
    label "kornet"
  ]
  node [
    id 373
    label "wyznawczyni"
  ]
  node [
    id 374
    label "pigu&#322;a"
  ]
  node [
    id 375
    label "czepek"
  ]
  node [
    id 376
    label "krewna"
  ]
  node [
    id 377
    label "siostrzyca"
  ]
  node [
    id 378
    label "pingwin"
  ]
  node [
    id 379
    label "siora"
  ]
  node [
    id 380
    label "anestetysta"
  ]
  node [
    id 381
    label "ko&#322;tunienie_si&#281;"
  ]
  node [
    id 382
    label "sko&#322;tunienie_si&#281;"
  ]
  node [
    id 383
    label "przedstawiciel"
  ]
  node [
    id 384
    label "gmerk"
  ]
  node [
    id 385
    label "mieszcza&#324;stwo"
  ]
  node [
    id 386
    label "drobnomieszcza&#324;ski"
  ]
  node [
    id 387
    label "prostak"
  ]
  node [
    id 388
    label "p&#243;&#378;ny"
  ]
  node [
    id 389
    label "ukrai&#324;ski"
  ]
  node [
    id 390
    label "po_lwowsku"
  ]
  node [
    id 391
    label "ko&#322;otuszka"
  ]
  node [
    id 392
    label "kresowy"
  ]
  node [
    id 393
    label "Jakub"
  ]
  node [
    id 394
    label "Kazimierz"
  ]
  node [
    id 395
    label "Haur"
  ]
  node [
    id 396
    label "Haura"
  ]
  node [
    id 397
    label "Zygmunt"
  ]
  node [
    id 398
    label "Anna"
  ]
  node [
    id 399
    label "Maria"
  ]
  node [
    id 400
    label "zeszyt"
  ]
  node [
    id 401
    label "Petard&#243;wny"
  ]
  node [
    id 402
    label "Stanis&#322;aw"
  ]
  node [
    id 403
    label "Kralem"
  ]
  node [
    id 404
    label "Stanis&#322;awa"
  ]
  node [
    id 405
    label "Skarszowskiego"
  ]
  node [
    id 406
    label "Jan"
  ]
  node [
    id 407
    label "Sobieski"
  ]
  node [
    id 408
    label "generalny"
  ]
  node [
    id 409
    label "oekonomik&#281;"
  ]
  node [
    id 410
    label "Andrzej"
  ]
  node [
    id 411
    label "Morsztyn"
  ]
  node [
    id 412
    label "Agnieszka"
  ]
  node [
    id 413
    label "Furmankiewicz&#243;wna"
  ]
  node [
    id 414
    label "Wojciech"
  ]
  node [
    id 415
    label "Rezlerem"
  ]
  node [
    id 416
    label "Marcin"
  ]
  node [
    id 417
    label "Winklerem"
  ]
  node [
    id 418
    label "Janowy"
  ]
  node [
    id 419
    label "sk&#322;ad"
  ]
  node [
    id 420
    label "abo"
  ]
  node [
    id 421
    label "skarbiec"
  ]
  node [
    id 422
    label "znakomity"
  ]
  node [
    id 423
    label "sekret"
  ]
  node [
    id 424
    label "oekonomiej"
  ]
  node [
    id 425
    label "ziemia&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 31
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 75
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 398
  ]
  edge [
    source 17
    target 399
  ]
  edge [
    source 17
    target 400
  ]
  edge [
    source 17
    target 401
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 22
    target 113
  ]
  edge [
    source 22
    target 174
  ]
  edge [
    source 22
    target 175
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 397
  ]
  edge [
    source 24
    target 406
  ]
  edge [
    source 24
    target 407
  ]
  edge [
    source 24
    target 418
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 397
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 180
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 123
  ]
  edge [
    source 27
    target 182
  ]
  edge [
    source 27
    target 183
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 184
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 185
  ]
  edge [
    source 28
    target 186
  ]
  edge [
    source 28
    target 187
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 188
  ]
  edge [
    source 29
    target 189
  ]
  edge [
    source 29
    target 190
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 191
  ]
  edge [
    source 31
    target 192
  ]
  edge [
    source 31
    target 193
  ]
  edge [
    source 31
    target 194
  ]
  edge [
    source 31
    target 195
  ]
  edge [
    source 31
    target 196
  ]
  edge [
    source 31
    target 197
  ]
  edge [
    source 31
    target 198
  ]
  edge [
    source 31
    target 199
  ]
  edge [
    source 31
    target 200
  ]
  edge [
    source 31
    target 201
  ]
  edge [
    source 31
    target 202
  ]
  edge [
    source 31
    target 203
  ]
  edge [
    source 31
    target 204
  ]
  edge [
    source 31
    target 205
  ]
  edge [
    source 31
    target 206
  ]
  edge [
    source 31
    target 207
  ]
  edge [
    source 31
    target 208
  ]
  edge [
    source 31
    target 209
  ]
  edge [
    source 31
    target 210
  ]
  edge [
    source 31
    target 211
  ]
  edge [
    source 31
    target 212
  ]
  edge [
    source 31
    target 213
  ]
  edge [
    source 31
    target 214
  ]
  edge [
    source 31
    target 215
  ]
  edge [
    source 31
    target 216
  ]
  edge [
    source 31
    target 217
  ]
  edge [
    source 31
    target 218
  ]
  edge [
    source 31
    target 219
  ]
  edge [
    source 31
    target 220
  ]
  edge [
    source 31
    target 221
  ]
  edge [
    source 31
    target 222
  ]
  edge [
    source 31
    target 223
  ]
  edge [
    source 31
    target 224
  ]
  edge [
    source 31
    target 225
  ]
  edge [
    source 31
    target 226
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 38
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 227
  ]
  edge [
    source 32
    target 228
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 229
  ]
  edge [
    source 33
    target 230
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 231
  ]
  edge [
    source 34
    target 232
  ]
  edge [
    source 34
    target 233
  ]
  edge [
    source 34
    target 234
  ]
  edge [
    source 34
    target 235
  ]
  edge [
    source 34
    target 236
  ]
  edge [
    source 34
    target 237
  ]
  edge [
    source 34
    target 238
  ]
  edge [
    source 34
    target 239
  ]
  edge [
    source 34
    target 240
  ]
  edge [
    source 34
    target 241
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 242
  ]
  edge [
    source 35
    target 243
  ]
  edge [
    source 35
    target 244
  ]
  edge [
    source 35
    target 245
  ]
  edge [
    source 35
    target 246
  ]
  edge [
    source 35
    target 247
  ]
  edge [
    source 35
    target 248
  ]
  edge [
    source 35
    target 249
  ]
  edge [
    source 35
    target 250
  ]
  edge [
    source 35
    target 251
  ]
  edge [
    source 35
    target 252
  ]
  edge [
    source 35
    target 253
  ]
  edge [
    source 35
    target 254
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 255
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 256
  ]
  edge [
    source 38
    target 257
  ]
  edge [
    source 38
    target 194
  ]
  edge [
    source 38
    target 258
  ]
  edge [
    source 38
    target 259
  ]
  edge [
    source 38
    target 260
  ]
  edge [
    source 38
    target 261
  ]
  edge [
    source 38
    target 262
  ]
  edge [
    source 38
    target 199
  ]
  edge [
    source 38
    target 263
  ]
  edge [
    source 38
    target 264
  ]
  edge [
    source 38
    target 265
  ]
  edge [
    source 38
    target 266
  ]
  edge [
    source 38
    target 267
  ]
  edge [
    source 38
    target 268
  ]
  edge [
    source 38
    target 269
  ]
  edge [
    source 38
    target 270
  ]
  edge [
    source 38
    target 202
  ]
  edge [
    source 38
    target 271
  ]
  edge [
    source 38
    target 272
  ]
  edge [
    source 38
    target 273
  ]
  edge [
    source 38
    target 274
  ]
  edge [
    source 38
    target 275
  ]
  edge [
    source 38
    target 276
  ]
  edge [
    source 38
    target 277
  ]
  edge [
    source 38
    target 278
  ]
  edge [
    source 38
    target 279
  ]
  edge [
    source 38
    target 280
  ]
  edge [
    source 38
    target 281
  ]
  edge [
    source 38
    target 282
  ]
  edge [
    source 38
    target 283
  ]
  edge [
    source 38
    target 208
  ]
  edge [
    source 38
    target 284
  ]
  edge [
    source 38
    target 285
  ]
  edge [
    source 38
    target 286
  ]
  edge [
    source 38
    target 287
  ]
  edge [
    source 38
    target 210
  ]
  edge [
    source 38
    target 288
  ]
  edge [
    source 38
    target 212
  ]
  edge [
    source 38
    target 289
  ]
  edge [
    source 38
    target 290
  ]
  edge [
    source 38
    target 291
  ]
  edge [
    source 38
    target 292
  ]
  edge [
    source 38
    target 293
  ]
  edge [
    source 38
    target 294
  ]
  edge [
    source 38
    target 295
  ]
  edge [
    source 38
    target 296
  ]
  edge [
    source 38
    target 297
  ]
  edge [
    source 38
    target 298
  ]
  edge [
    source 38
    target 299
  ]
  edge [
    source 38
    target 300
  ]
  edge [
    source 38
    target 301
  ]
  edge [
    source 38
    target 302
  ]
  edge [
    source 38
    target 303
  ]
  edge [
    source 38
    target 304
  ]
  edge [
    source 38
    target 305
  ]
  edge [
    source 38
    target 222
  ]
  edge [
    source 38
    target 306
  ]
  edge [
    source 38
    target 307
  ]
  edge [
    source 38
    target 308
  ]
  edge [
    source 38
    target 309
  ]
  edge [
    source 38
    target 310
  ]
  edge [
    source 38
    target 311
  ]
  edge [
    source 38
    target 312
  ]
  edge [
    source 38
    target 313
  ]
  edge [
    source 38
    target 314
  ]
  edge [
    source 38
    target 315
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 316
  ]
  edge [
    source 39
    target 317
  ]
  edge [
    source 39
    target 318
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 319
  ]
  edge [
    source 40
    target 320
  ]
  edge [
    source 40
    target 321
  ]
  edge [
    source 40
    target 322
  ]
  edge [
    source 40
    target 323
  ]
  edge [
    source 40
    target 324
  ]
  edge [
    source 40
    target 325
  ]
  edge [
    source 40
    target 326
  ]
  edge [
    source 40
    target 327
  ]
  edge [
    source 40
    target 328
  ]
  edge [
    source 40
    target 329
  ]
  edge [
    source 40
    target 330
  ]
  edge [
    source 40
    target 331
  ]
  edge [
    source 40
    target 332
  ]
  edge [
    source 40
    target 333
  ]
  edge [
    source 40
    target 334
  ]
  edge [
    source 40
    target 335
  ]
  edge [
    source 40
    target 336
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 41
    target 337
  ]
  edge [
    source 41
    target 338
  ]
  edge [
    source 41
    target 339
  ]
  edge [
    source 41
    target 340
  ]
  edge [
    source 41
    target 341
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 41
    target 343
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 345
  ]
  edge [
    source 41
    target 346
  ]
  edge [
    source 41
    target 347
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 348
  ]
  edge [
    source 42
    target 349
  ]
  edge [
    source 42
    target 350
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 351
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 352
  ]
  edge [
    source 46
    target 353
  ]
  edge [
    source 46
    target 354
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 49
    target 61
  ]
  edge [
    source 49
    target 355
  ]
  edge [
    source 49
    target 356
  ]
  edge [
    source 49
    target 357
  ]
  edge [
    source 49
    target 358
  ]
  edge [
    source 49
    target 359
  ]
  edge [
    source 49
    target 156
  ]
  edge [
    source 49
    target 360
  ]
  edge [
    source 49
    target 361
  ]
  edge [
    source 49
    target 362
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 59
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 363
  ]
  edge [
    source 52
    target 364
  ]
  edge [
    source 52
    target 365
  ]
  edge [
    source 52
    target 366
  ]
  edge [
    source 52
    target 367
  ]
  edge [
    source 52
    target 368
  ]
  edge [
    source 52
    target 369
  ]
  edge [
    source 52
    target 370
  ]
  edge [
    source 52
    target 371
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 60
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 372
  ]
  edge [
    source 54
    target 373
  ]
  edge [
    source 54
    target 374
  ]
  edge [
    source 54
    target 93
  ]
  edge [
    source 54
    target 375
  ]
  edge [
    source 54
    target 376
  ]
  edge [
    source 54
    target 377
  ]
  edge [
    source 54
    target 378
  ]
  edge [
    source 54
    target 379
  ]
  edge [
    source 54
    target 380
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 381
  ]
  edge [
    source 57
    target 337
  ]
  edge [
    source 57
    target 382
  ]
  edge [
    source 57
    target 383
  ]
  edge [
    source 57
    target 384
  ]
  edge [
    source 57
    target 83
  ]
  edge [
    source 57
    target 385
  ]
  edge [
    source 57
    target 386
  ]
  edge [
    source 57
    target 387
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 388
  ]
  edge [
    source 61
    target 389
  ]
  edge [
    source 61
    target 390
  ]
  edge [
    source 61
    target 391
  ]
  edge [
    source 61
    target 392
  ]
  edge [
    source 393
    target 394
  ]
  edge [
    source 393
    target 395
  ]
  edge [
    source 393
    target 396
  ]
  edge [
    source 394
    target 395
  ]
  edge [
    source 394
    target 414
  ]
  edge [
    source 394
    target 415
  ]
  edge [
    source 398
    target 399
  ]
  edge [
    source 398
    target 400
  ]
  edge [
    source 398
    target 401
  ]
  edge [
    source 399
    target 400
  ]
  edge [
    source 399
    target 401
  ]
  edge [
    source 400
    target 401
  ]
  edge [
    source 402
    target 403
  ]
  edge [
    source 404
    target 405
  ]
  edge [
    source 406
    target 407
  ]
  edge [
    source 407
    target 418
  ]
  edge [
    source 408
    target 409
  ]
  edge [
    source 410
    target 411
  ]
  edge [
    source 412
    target 413
  ]
  edge [
    source 414
    target 415
  ]
  edge [
    source 416
    target 417
  ]
  edge [
    source 419
    target 420
  ]
  edge [
    source 419
    target 421
  ]
  edge [
    source 419
    target 422
  ]
  edge [
    source 419
    target 423
  ]
  edge [
    source 419
    target 424
  ]
  edge [
    source 419
    target 425
  ]
  edge [
    source 420
    target 421
  ]
  edge [
    source 420
    target 422
  ]
  edge [
    source 420
    target 423
  ]
  edge [
    source 420
    target 424
  ]
  edge [
    source 420
    target 425
  ]
  edge [
    source 421
    target 422
  ]
  edge [
    source 421
    target 423
  ]
  edge [
    source 421
    target 424
  ]
  edge [
    source 421
    target 425
  ]
  edge [
    source 422
    target 423
  ]
  edge [
    source 422
    target 424
  ]
  edge [
    source 422
    target 425
  ]
  edge [
    source 423
    target 424
  ]
  edge [
    source 423
    target 425
  ]
  edge [
    source 424
    target 425
  ]
]
