graph [
  maxDegree 44
  minDegree 1
  meanDegree 2.2240325865580446
  density 0.004538842013383765
  graphCliqueNumber 4
  node [
    id 0
    label "odpowied&#378;"
    origin "text"
  ]
  node [
    id 1
    label "pytanie"
    origin "text"
  ]
  node [
    id 2
    label "pan"
    origin "text"
  ]
  node [
    id 3
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 4
    label "m&#281;&#380;yd&#322;o"
    origin "text"
  ]
  node [
    id 5
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 6
    label "cz&#281;&#347;ciowo"
    origin "text"
  ]
  node [
    id 7
    label "tak"
    origin "text"
  ]
  node [
    id 8
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 9
    label "uregulowanie"
    origin "text"
  ]
  node [
    id 10
    label "prokonsumenckie"
    origin "text"
  ]
  node [
    id 11
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "zar&#243;wno"
    origin "text"
  ]
  node [
    id 14
    label "pierwsza"
    origin "text"
  ]
  node [
    id 15
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "europejski"
    origin "text"
  ]
  node [
    id 17
    label "jak"
    origin "text"
  ]
  node [
    id 18
    label "znajda"
    origin "text"
  ]
  node [
    id 19
    label "druga"
    origin "text"
  ]
  node [
    id 20
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 21
    label "przewidywa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "typ"
    origin "text"
  ]
  node [
    id 23
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 24
    label "prawny"
    origin "text"
  ]
  node [
    id 25
    label "nowelizacja"
    origin "text"
  ]
  node [
    id 26
    label "prawa"
    origin "text"
  ]
  node [
    id 27
    label "telekomunikacyjny"
    origin "text"
  ]
  node [
    id 28
    label "jeden"
    origin "text"
  ]
  node [
    id 29
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 30
    label "projekt"
    origin "text"
  ]
  node [
    id 31
    label "poselski"
    origin "text"
  ]
  node [
    id 32
    label "polega&#263;"
    origin "text"
  ]
  node [
    id 33
    label "metr"
    origin "text"
  ]
  node [
    id 34
    label "mediacja"
    origin "text"
  ]
  node [
    id 35
    label "trudno"
    origin "text"
  ]
  node [
    id 36
    label "by&#263;"
    origin "text"
  ]
  node [
    id 37
    label "zgodzi&#263;"
    origin "text"
  ]
  node [
    id 38
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 39
    label "dobrze"
    origin "text"
  ]
  node [
    id 40
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 41
    label "system"
    origin "text"
  ]
  node [
    id 42
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 43
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 44
    label "miejsce"
    origin "text"
  ]
  node [
    id 45
    label "funkcjonowa&#263;"
    origin "text"
  ]
  node [
    id 46
    label "sprawnie"
    origin "text"
  ]
  node [
    id 47
    label "wystarczy&#263;"
    origin "text"
  ]
  node [
    id 48
    label "chocia&#380;by"
    origin "text"
  ]
  node [
    id 49
    label "zerkn&#261;&#263;"
    origin "text"
  ]
  node [
    id 50
    label "przepis"
    origin "text"
  ]
  node [
    id 51
    label "kodeks"
    origin "text"
  ]
  node [
    id 52
    label "post&#281;powanie"
    origin "text"
  ]
  node [
    id 53
    label "administracyjny"
    origin "text"
  ]
  node [
    id 54
    label "te&#380;"
    origin "text"
  ]
  node [
    id 55
    label "cywilny"
    origin "text"
  ]
  node [
    id 56
    label "tworzenie"
    origin "text"
  ]
  node [
    id 57
    label "kolejny"
    origin "text"
  ]
  node [
    id 58
    label "odnosi&#263;"
    origin "text"
  ]
  node [
    id 59
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 60
    label "zb&#281;dny"
    origin "text"
  ]
  node [
    id 61
    label "dokument"
  ]
  node [
    id 62
    label "rozmowa"
  ]
  node [
    id 63
    label "reakcja"
  ]
  node [
    id 64
    label "wyj&#347;cie"
  ]
  node [
    id 65
    label "react"
  ]
  node [
    id 66
    label "respondent"
  ]
  node [
    id 67
    label "replica"
  ]
  node [
    id 68
    label "sprawa"
  ]
  node [
    id 69
    label "zadanie"
  ]
  node [
    id 70
    label "wypowied&#378;"
  ]
  node [
    id 71
    label "problemat"
  ]
  node [
    id 72
    label "rozpytywanie"
  ]
  node [
    id 73
    label "sprawdzian"
  ]
  node [
    id 74
    label "przes&#322;uchiwanie"
  ]
  node [
    id 75
    label "wypytanie"
  ]
  node [
    id 76
    label "zwracanie_si&#281;"
  ]
  node [
    id 77
    label "wypowiedzenie"
  ]
  node [
    id 78
    label "wywo&#322;ywanie"
  ]
  node [
    id 79
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 80
    label "problematyka"
  ]
  node [
    id 81
    label "question"
  ]
  node [
    id 82
    label "sprawdzanie"
  ]
  node [
    id 83
    label "odpowiadanie"
  ]
  node [
    id 84
    label "survey"
  ]
  node [
    id 85
    label "odpowiada&#263;"
  ]
  node [
    id 86
    label "egzaminowanie"
  ]
  node [
    id 87
    label "cz&#322;owiek"
  ]
  node [
    id 88
    label "profesor"
  ]
  node [
    id 89
    label "kszta&#322;ciciel"
  ]
  node [
    id 90
    label "jegomo&#347;&#263;"
  ]
  node [
    id 91
    label "zwrot"
  ]
  node [
    id 92
    label "pracodawca"
  ]
  node [
    id 93
    label "rz&#261;dzenie"
  ]
  node [
    id 94
    label "m&#261;&#380;"
  ]
  node [
    id 95
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 96
    label "ch&#322;opina"
  ]
  node [
    id 97
    label "bratek"
  ]
  node [
    id 98
    label "opiekun"
  ]
  node [
    id 99
    label "doros&#322;y"
  ]
  node [
    id 100
    label "preceptor"
  ]
  node [
    id 101
    label "Midas"
  ]
  node [
    id 102
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 103
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 104
    label "murza"
  ]
  node [
    id 105
    label "ojciec"
  ]
  node [
    id 106
    label "androlog"
  ]
  node [
    id 107
    label "pupil"
  ]
  node [
    id 108
    label "efendi"
  ]
  node [
    id 109
    label "nabab"
  ]
  node [
    id 110
    label "w&#322;odarz"
  ]
  node [
    id 111
    label "szkolnik"
  ]
  node [
    id 112
    label "pedagog"
  ]
  node [
    id 113
    label "popularyzator"
  ]
  node [
    id 114
    label "andropauza"
  ]
  node [
    id 115
    label "gra_w_karty"
  ]
  node [
    id 116
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 117
    label "Mieszko_I"
  ]
  node [
    id 118
    label "bogaty"
  ]
  node [
    id 119
    label "samiec"
  ]
  node [
    id 120
    label "przyw&#243;dca"
  ]
  node [
    id 121
    label "pa&#324;stwo"
  ]
  node [
    id 122
    label "belfer"
  ]
  node [
    id 123
    label "dyplomata"
  ]
  node [
    id 124
    label "wys&#322;annik"
  ]
  node [
    id 125
    label "przedstawiciel"
  ]
  node [
    id 126
    label "kurier_dyplomatyczny"
  ]
  node [
    id 127
    label "ablegat"
  ]
  node [
    id 128
    label "klubista"
  ]
  node [
    id 129
    label "Miko&#322;ajczyk"
  ]
  node [
    id 130
    label "Korwin"
  ]
  node [
    id 131
    label "parlamentarzysta"
  ]
  node [
    id 132
    label "dyscyplina_partyjna"
  ]
  node [
    id 133
    label "izba_ni&#380;sza"
  ]
  node [
    id 134
    label "poselstwo"
  ]
  node [
    id 135
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 136
    label "express"
  ]
  node [
    id 137
    label "rzekn&#261;&#263;"
  ]
  node [
    id 138
    label "okre&#347;li&#263;"
  ]
  node [
    id 139
    label "wyrazi&#263;"
  ]
  node [
    id 140
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 141
    label "unwrap"
  ]
  node [
    id 142
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 143
    label "convey"
  ]
  node [
    id 144
    label "discover"
  ]
  node [
    id 145
    label "wydoby&#263;"
  ]
  node [
    id 146
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 147
    label "poda&#263;"
  ]
  node [
    id 148
    label "nieca&#322;y"
  ]
  node [
    id 149
    label "control"
  ]
  node [
    id 150
    label "ulepszenie"
  ]
  node [
    id 151
    label "sfinansowanie"
  ]
  node [
    id 152
    label "dominance"
  ]
  node [
    id 153
    label "calibration"
  ]
  node [
    id 154
    label "zasada"
  ]
  node [
    id 155
    label "zmiana"
  ]
  node [
    id 156
    label "doznawa&#263;"
  ]
  node [
    id 157
    label "znachodzi&#263;"
  ]
  node [
    id 158
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 159
    label "pozyskiwa&#263;"
  ]
  node [
    id 160
    label "odzyskiwa&#263;"
  ]
  node [
    id 161
    label "os&#261;dza&#263;"
  ]
  node [
    id 162
    label "wykrywa&#263;"
  ]
  node [
    id 163
    label "detect"
  ]
  node [
    id 164
    label "wymy&#347;la&#263;"
  ]
  node [
    id 165
    label "powodowa&#263;"
  ]
  node [
    id 166
    label "godzina"
  ]
  node [
    id 167
    label "whole"
  ]
  node [
    id 168
    label "Rzym_Zachodni"
  ]
  node [
    id 169
    label "element"
  ]
  node [
    id 170
    label "ilo&#347;&#263;"
  ]
  node [
    id 171
    label "urz&#261;dzenie"
  ]
  node [
    id 172
    label "Rzym_Wschodni"
  ]
  node [
    id 173
    label "European"
  ]
  node [
    id 174
    label "po_europejsku"
  ]
  node [
    id 175
    label "charakterystyczny"
  ]
  node [
    id 176
    label "europejsko"
  ]
  node [
    id 177
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 178
    label "typowy"
  ]
  node [
    id 179
    label "byd&#322;o"
  ]
  node [
    id 180
    label "zobo"
  ]
  node [
    id 181
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 182
    label "yakalo"
  ]
  node [
    id 183
    label "dzo"
  ]
  node [
    id 184
    label "podciep"
  ]
  node [
    id 185
    label "Logan"
  ]
  node [
    id 186
    label "dziecko"
  ]
  node [
    id 187
    label "zwierz&#281;"
  ]
  node [
    id 188
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 189
    label "kategoria"
  ]
  node [
    id 190
    label "egzekutywa"
  ]
  node [
    id 191
    label "gabinet_cieni"
  ]
  node [
    id 192
    label "gromada"
  ]
  node [
    id 193
    label "premier"
  ]
  node [
    id 194
    label "Londyn"
  ]
  node [
    id 195
    label "Konsulat"
  ]
  node [
    id 196
    label "uporz&#261;dkowanie"
  ]
  node [
    id 197
    label "jednostka_systematyczna"
  ]
  node [
    id 198
    label "szpaler"
  ]
  node [
    id 199
    label "przybli&#380;enie"
  ]
  node [
    id 200
    label "tract"
  ]
  node [
    id 201
    label "number"
  ]
  node [
    id 202
    label "lon&#380;a"
  ]
  node [
    id 203
    label "w&#322;adza"
  ]
  node [
    id 204
    label "instytucja"
  ]
  node [
    id 205
    label "klasa"
  ]
  node [
    id 206
    label "zamierza&#263;"
  ]
  node [
    id 207
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 208
    label "anticipate"
  ]
  node [
    id 209
    label "autorament"
  ]
  node [
    id 210
    label "przypuszczenie"
  ]
  node [
    id 211
    label "cynk"
  ]
  node [
    id 212
    label "rezultat"
  ]
  node [
    id 213
    label "kr&#243;lestwo"
  ]
  node [
    id 214
    label "obstawia&#263;"
  ]
  node [
    id 215
    label "design"
  ]
  node [
    id 216
    label "facet"
  ]
  node [
    id 217
    label "variety"
  ]
  node [
    id 218
    label "sztuka"
  ]
  node [
    id 219
    label "antycypacja"
  ]
  node [
    id 220
    label "wynik"
  ]
  node [
    id 221
    label "spe&#322;nienie"
  ]
  node [
    id 222
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 223
    label "po&#322;o&#380;na"
  ]
  node [
    id 224
    label "proces_fizjologiczny"
  ]
  node [
    id 225
    label "przestanie"
  ]
  node [
    id 226
    label "marc&#243;wka"
  ]
  node [
    id 227
    label "usuni&#281;cie"
  ]
  node [
    id 228
    label "uniewa&#380;nienie"
  ]
  node [
    id 229
    label "pomys&#322;"
  ]
  node [
    id 230
    label "birth"
  ]
  node [
    id 231
    label "wymy&#347;lenie"
  ]
  node [
    id 232
    label "po&#322;&#243;g"
  ]
  node [
    id 233
    label "szok_poporodowy"
  ]
  node [
    id 234
    label "event"
  ]
  node [
    id 235
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 236
    label "spos&#243;b"
  ]
  node [
    id 237
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 238
    label "dula"
  ]
  node [
    id 239
    label "prawniczo"
  ]
  node [
    id 240
    label "prawnie"
  ]
  node [
    id 241
    label "konstytucyjnoprawny"
  ]
  node [
    id 242
    label "legalny"
  ]
  node [
    id 243
    label "jurydyczny"
  ]
  node [
    id 244
    label "modyfikacja"
  ]
  node [
    id 245
    label "story"
  ]
  node [
    id 246
    label "ustawa"
  ]
  node [
    id 247
    label "amendment"
  ]
  node [
    id 248
    label "kieliszek"
  ]
  node [
    id 249
    label "shot"
  ]
  node [
    id 250
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 251
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 252
    label "jaki&#347;"
  ]
  node [
    id 253
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 254
    label "jednolicie"
  ]
  node [
    id 255
    label "w&#243;dka"
  ]
  node [
    id 256
    label "ten"
  ]
  node [
    id 257
    label "ujednolicenie"
  ]
  node [
    id 258
    label "jednakowy"
  ]
  node [
    id 259
    label "device"
  ]
  node [
    id 260
    label "program_u&#380;ytkowy"
  ]
  node [
    id 261
    label "intencja"
  ]
  node [
    id 262
    label "agreement"
  ]
  node [
    id 263
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 264
    label "plan"
  ]
  node [
    id 265
    label "dokumentacja"
  ]
  node [
    id 266
    label "ufa&#263;"
  ]
  node [
    id 267
    label "consist"
  ]
  node [
    id 268
    label "trust"
  ]
  node [
    id 269
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 270
    label "meter"
  ]
  node [
    id 271
    label "decymetr"
  ]
  node [
    id 272
    label "megabyte"
  ]
  node [
    id 273
    label "plon"
  ]
  node [
    id 274
    label "metrum"
  ]
  node [
    id 275
    label "dekametr"
  ]
  node [
    id 276
    label "jednostka_powierzchni"
  ]
  node [
    id 277
    label "uk&#322;ad_SI"
  ]
  node [
    id 278
    label "literaturoznawstwo"
  ]
  node [
    id 279
    label "wiersz"
  ]
  node [
    id 280
    label "gigametr"
  ]
  node [
    id 281
    label "miara"
  ]
  node [
    id 282
    label "nauczyciel"
  ]
  node [
    id 283
    label "kilometr_kwadratowy"
  ]
  node [
    id 284
    label "jednostka_metryczna"
  ]
  node [
    id 285
    label "jednostka_masy"
  ]
  node [
    id 286
    label "centymetr_kwadratowy"
  ]
  node [
    id 287
    label "pojednawstwo"
  ]
  node [
    id 288
    label "trudny"
  ]
  node [
    id 289
    label "hard"
  ]
  node [
    id 290
    label "si&#281;ga&#263;"
  ]
  node [
    id 291
    label "trwa&#263;"
  ]
  node [
    id 292
    label "obecno&#347;&#263;"
  ]
  node [
    id 293
    label "stan"
  ]
  node [
    id 294
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 295
    label "stand"
  ]
  node [
    id 296
    label "mie&#263;_miejsce"
  ]
  node [
    id 297
    label "uczestniczy&#263;"
  ]
  node [
    id 298
    label "chodzi&#263;"
  ]
  node [
    id 299
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 300
    label "equal"
  ]
  node [
    id 301
    label "zatrudni&#263;"
  ]
  node [
    id 302
    label "zgodzenie"
  ]
  node [
    id 303
    label "pomoc"
  ]
  node [
    id 304
    label "zgadza&#263;"
  ]
  node [
    id 305
    label "przyczyna"
  ]
  node [
    id 306
    label "uwaga"
  ]
  node [
    id 307
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 308
    label "punkt_widzenia"
  ]
  node [
    id 309
    label "moralnie"
  ]
  node [
    id 310
    label "wiele"
  ]
  node [
    id 311
    label "lepiej"
  ]
  node [
    id 312
    label "korzystnie"
  ]
  node [
    id 313
    label "pomy&#347;lnie"
  ]
  node [
    id 314
    label "pozytywnie"
  ]
  node [
    id 315
    label "dobry"
  ]
  node [
    id 316
    label "dobroczynnie"
  ]
  node [
    id 317
    label "odpowiednio"
  ]
  node [
    id 318
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 319
    label "skutecznie"
  ]
  node [
    id 320
    label "cognizance"
  ]
  node [
    id 321
    label "model"
  ]
  node [
    id 322
    label "sk&#322;ad"
  ]
  node [
    id 323
    label "zachowanie"
  ]
  node [
    id 324
    label "podstawa"
  ]
  node [
    id 325
    label "porz&#261;dek"
  ]
  node [
    id 326
    label "Android"
  ]
  node [
    id 327
    label "przyn&#281;ta"
  ]
  node [
    id 328
    label "jednostka_geologiczna"
  ]
  node [
    id 329
    label "metoda"
  ]
  node [
    id 330
    label "podsystem"
  ]
  node [
    id 331
    label "p&#322;&#243;d"
  ]
  node [
    id 332
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 333
    label "s&#261;d"
  ]
  node [
    id 334
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 335
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 336
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 337
    label "j&#261;dro"
  ]
  node [
    id 338
    label "eratem"
  ]
  node [
    id 339
    label "ryba"
  ]
  node [
    id 340
    label "pulpit"
  ]
  node [
    id 341
    label "struktura"
  ]
  node [
    id 342
    label "oddzia&#322;"
  ]
  node [
    id 343
    label "usenet"
  ]
  node [
    id 344
    label "o&#347;"
  ]
  node [
    id 345
    label "oprogramowanie"
  ]
  node [
    id 346
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 347
    label "poj&#281;cie"
  ]
  node [
    id 348
    label "w&#281;dkarstwo"
  ]
  node [
    id 349
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 350
    label "Leopard"
  ]
  node [
    id 351
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 352
    label "systemik"
  ]
  node [
    id 353
    label "rozprz&#261;c"
  ]
  node [
    id 354
    label "cybernetyk"
  ]
  node [
    id 355
    label "konstelacja"
  ]
  node [
    id 356
    label "doktryna"
  ]
  node [
    id 357
    label "net"
  ]
  node [
    id 358
    label "zbi&#243;r"
  ]
  node [
    id 359
    label "method"
  ]
  node [
    id 360
    label "systemat"
  ]
  node [
    id 361
    label "czyj&#347;"
  ]
  node [
    id 362
    label "cia&#322;o"
  ]
  node [
    id 363
    label "plac"
  ]
  node [
    id 364
    label "cecha"
  ]
  node [
    id 365
    label "przestrze&#324;"
  ]
  node [
    id 366
    label "status"
  ]
  node [
    id 367
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 368
    label "chwila"
  ]
  node [
    id 369
    label "praca"
  ]
  node [
    id 370
    label "location"
  ]
  node [
    id 371
    label "warunek_lokalowy"
  ]
  node [
    id 372
    label "bangla&#263;"
  ]
  node [
    id 373
    label "dziama&#263;"
  ]
  node [
    id 374
    label "tryb"
  ]
  node [
    id 375
    label "zdrowo"
  ]
  node [
    id 376
    label "kompetentnie"
  ]
  node [
    id 377
    label "udanie"
  ]
  node [
    id 378
    label "szybko"
  ]
  node [
    id 379
    label "funkcjonalnie"
  ]
  node [
    id 380
    label "sprawny"
  ]
  node [
    id 381
    label "umiej&#281;tnie"
  ]
  node [
    id 382
    label "spowodowa&#263;"
  ]
  node [
    id 383
    label "stan&#261;&#263;"
  ]
  node [
    id 384
    label "dosta&#263;"
  ]
  node [
    id 385
    label "suffice"
  ]
  node [
    id 386
    label "zaspokoi&#263;"
  ]
  node [
    id 387
    label "spojrze&#263;"
  ]
  node [
    id 388
    label "peek"
  ]
  node [
    id 389
    label "przedawnienie_si&#281;"
  ]
  node [
    id 390
    label "recepta"
  ]
  node [
    id 391
    label "norma_prawna"
  ]
  node [
    id 392
    label "prawo"
  ]
  node [
    id 393
    label "regulation"
  ]
  node [
    id 394
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 395
    label "porada"
  ]
  node [
    id 396
    label "przedawnianie_si&#281;"
  ]
  node [
    id 397
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 398
    label "r&#281;kopis"
  ]
  node [
    id 399
    label "kodeks_morski"
  ]
  node [
    id 400
    label "Justynian"
  ]
  node [
    id 401
    label "code"
  ]
  node [
    id 402
    label "obwiniony"
  ]
  node [
    id 403
    label "kodeks_karny"
  ]
  node [
    id 404
    label "kodeks_drogowy"
  ]
  node [
    id 405
    label "kodeks_pracy"
  ]
  node [
    id 406
    label "kodeks_cywilny"
  ]
  node [
    id 407
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 408
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 409
    label "kodeks_rodzinny"
  ]
  node [
    id 410
    label "robienie"
  ]
  node [
    id 411
    label "czynno&#347;&#263;"
  ]
  node [
    id 412
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 413
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 414
    label "kognicja"
  ]
  node [
    id 415
    label "rozprawa"
  ]
  node [
    id 416
    label "kazanie"
  ]
  node [
    id 417
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 418
    label "campaign"
  ]
  node [
    id 419
    label "fashion"
  ]
  node [
    id 420
    label "wydarzenie"
  ]
  node [
    id 421
    label "przes&#322;anka"
  ]
  node [
    id 422
    label "zmierzanie"
  ]
  node [
    id 423
    label "administracyjnie"
  ]
  node [
    id 424
    label "administrative"
  ]
  node [
    id 425
    label "urz&#281;dowy"
  ]
  node [
    id 426
    label "cywilnie"
  ]
  node [
    id 427
    label "nieoficjalny"
  ]
  node [
    id 428
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 429
    label "pope&#322;nianie"
  ]
  node [
    id 430
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 431
    label "development"
  ]
  node [
    id 432
    label "stanowienie"
  ]
  node [
    id 433
    label "exploitation"
  ]
  node [
    id 434
    label "structure"
  ]
  node [
    id 435
    label "inny"
  ]
  node [
    id 436
    label "nast&#281;pnie"
  ]
  node [
    id 437
    label "kt&#243;ry&#347;"
  ]
  node [
    id 438
    label "kolejno"
  ]
  node [
    id 439
    label "nastopny"
  ]
  node [
    id 440
    label "get"
  ]
  node [
    id 441
    label "catch"
  ]
  node [
    id 442
    label "oddawa&#263;"
  ]
  node [
    id 443
    label "dostarcza&#263;"
  ]
  node [
    id 444
    label "osi&#261;ga&#263;"
  ]
  node [
    id 445
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 446
    label "impart"
  ]
  node [
    id 447
    label "panna_na_wydaniu"
  ]
  node [
    id 448
    label "surrender"
  ]
  node [
    id 449
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 450
    label "train"
  ]
  node [
    id 451
    label "give"
  ]
  node [
    id 452
    label "wytwarza&#263;"
  ]
  node [
    id 453
    label "dawa&#263;"
  ]
  node [
    id 454
    label "zapach"
  ]
  node [
    id 455
    label "wprowadza&#263;"
  ]
  node [
    id 456
    label "ujawnia&#263;"
  ]
  node [
    id 457
    label "wydawnictwo"
  ]
  node [
    id 458
    label "powierza&#263;"
  ]
  node [
    id 459
    label "produkcja"
  ]
  node [
    id 460
    label "denuncjowa&#263;"
  ]
  node [
    id 461
    label "reszta"
  ]
  node [
    id 462
    label "robi&#263;"
  ]
  node [
    id 463
    label "placard"
  ]
  node [
    id 464
    label "tajemnica"
  ]
  node [
    id 465
    label "wiano"
  ]
  node [
    id 466
    label "kojarzy&#263;"
  ]
  node [
    id 467
    label "d&#378;wi&#281;k"
  ]
  node [
    id 468
    label "podawa&#263;"
  ]
  node [
    id 469
    label "odchodzenie"
  ]
  node [
    id 470
    label "odej&#347;cie"
  ]
  node [
    id 471
    label "nadmiarowy"
  ]
  node [
    id 472
    label "zb&#281;dnie"
  ]
  node [
    id 473
    label "prawy"
  ]
  node [
    id 474
    label "post&#281;powa&#263;"
  ]
  node [
    id 475
    label "trybuna&#322;"
  ]
  node [
    id 476
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 477
    label "telekomunikacja"
  ]
  node [
    id 478
    label "polski"
  ]
  node [
    id 479
    label "Stanis&#322;awa"
  ]
  node [
    id 480
    label "pi&#261;tka"
  ]
  node [
    id 481
    label "Antoni"
  ]
  node [
    id 482
    label "M&#281;&#380;yd&#322;o"
  ]
  node [
    id 483
    label "centralny"
  ]
  node [
    id 484
    label "biuro"
  ]
  node [
    id 485
    label "Antykorupcyjne"
  ]
  node [
    id 486
    label "rada"
  ]
  node [
    id 487
    label "Stefan"
  ]
  node [
    id 488
    label "Niesio&#322;owski"
  ]
  node [
    id 489
    label "Jacek"
  ]
  node [
    id 490
    label "Tomczak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 12
    target 36
  ]
  edge [
    source 12
    target 37
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 35
  ]
  edge [
    source 15
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 44
  ]
  edge [
    source 15
    target 37
  ]
  edge [
    source 15
    target 47
  ]
  edge [
    source 15
    target 29
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 486
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 37
  ]
  edge [
    source 19
    target 47
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 44
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 206
  ]
  edge [
    source 21
    target 207
  ]
  edge [
    source 21
    target 208
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 87
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 217
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 219
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 64
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 227
  ]
  edge [
    source 23
    target 228
  ]
  edge [
    source 23
    target 229
  ]
  edge [
    source 23
    target 230
  ]
  edge [
    source 23
    target 231
  ]
  edge [
    source 23
    target 232
  ]
  edge [
    source 23
    target 233
  ]
  edge [
    source 23
    target 234
  ]
  edge [
    source 23
    target 235
  ]
  edge [
    source 23
    target 236
  ]
  edge [
    source 23
    target 237
  ]
  edge [
    source 23
    target 238
  ]
  edge [
    source 23
    target 53
  ]
  edge [
    source 24
    target 41
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 239
  ]
  edge [
    source 24
    target 240
  ]
  edge [
    source 24
    target 241
  ]
  edge [
    source 24
    target 242
  ]
  edge [
    source 24
    target 243
  ]
  edge [
    source 24
    target 53
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 244
  ]
  edge [
    source 25
    target 245
  ]
  edge [
    source 25
    target 246
  ]
  edge [
    source 25
    target 247
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 26
    target 41
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 27
    target 473
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 61
  ]
  edge [
    source 30
    target 259
  ]
  edge [
    source 30
    target 260
  ]
  edge [
    source 30
    target 261
  ]
  edge [
    source 30
    target 262
  ]
  edge [
    source 30
    target 229
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 264
  ]
  edge [
    source 30
    target 265
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 270
  ]
  edge [
    source 33
    target 271
  ]
  edge [
    source 33
    target 272
  ]
  edge [
    source 33
    target 273
  ]
  edge [
    source 33
    target 274
  ]
  edge [
    source 33
    target 275
  ]
  edge [
    source 33
    target 276
  ]
  edge [
    source 33
    target 277
  ]
  edge [
    source 33
    target 278
  ]
  edge [
    source 33
    target 279
  ]
  edge [
    source 33
    target 280
  ]
  edge [
    source 33
    target 281
  ]
  edge [
    source 33
    target 282
  ]
  edge [
    source 33
    target 283
  ]
  edge [
    source 33
    target 284
  ]
  edge [
    source 33
    target 285
  ]
  edge [
    source 33
    target 286
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 42
  ]
  edge [
    source 34
    target 59
  ]
  edge [
    source 34
    target 287
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 288
  ]
  edge [
    source 35
    target 289
  ]
  edge [
    source 36
    target 290
  ]
  edge [
    source 36
    target 291
  ]
  edge [
    source 36
    target 292
  ]
  edge [
    source 36
    target 293
  ]
  edge [
    source 36
    target 294
  ]
  edge [
    source 36
    target 295
  ]
  edge [
    source 36
    target 296
  ]
  edge [
    source 36
    target 297
  ]
  edge [
    source 36
    target 298
  ]
  edge [
    source 36
    target 299
  ]
  edge [
    source 36
    target 300
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 301
  ]
  edge [
    source 37
    target 302
  ]
  edge [
    source 37
    target 303
  ]
  edge [
    source 37
    target 304
  ]
  edge [
    source 37
    target 47
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 305
  ]
  edge [
    source 38
    target 306
  ]
  edge [
    source 38
    target 307
  ]
  edge [
    source 38
    target 308
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 309
  ]
  edge [
    source 39
    target 310
  ]
  edge [
    source 39
    target 311
  ]
  edge [
    source 39
    target 312
  ]
  edge [
    source 39
    target 313
  ]
  edge [
    source 39
    target 314
  ]
  edge [
    source 39
    target 315
  ]
  edge [
    source 39
    target 316
  ]
  edge [
    source 39
    target 317
  ]
  edge [
    source 39
    target 318
  ]
  edge [
    source 39
    target 319
  ]
  edge [
    source 39
    target 46
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 320
  ]
  edge [
    source 41
    target 321
  ]
  edge [
    source 41
    target 322
  ]
  edge [
    source 41
    target 323
  ]
  edge [
    source 41
    target 324
  ]
  edge [
    source 41
    target 325
  ]
  edge [
    source 41
    target 326
  ]
  edge [
    source 41
    target 327
  ]
  edge [
    source 41
    target 328
  ]
  edge [
    source 41
    target 329
  ]
  edge [
    source 41
    target 330
  ]
  edge [
    source 41
    target 331
  ]
  edge [
    source 41
    target 332
  ]
  edge [
    source 41
    target 333
  ]
  edge [
    source 41
    target 334
  ]
  edge [
    source 41
    target 335
  ]
  edge [
    source 41
    target 336
  ]
  edge [
    source 41
    target 337
  ]
  edge [
    source 41
    target 338
  ]
  edge [
    source 41
    target 339
  ]
  edge [
    source 41
    target 340
  ]
  edge [
    source 41
    target 341
  ]
  edge [
    source 41
    target 236
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 41
    target 343
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 345
  ]
  edge [
    source 41
    target 346
  ]
  edge [
    source 41
    target 347
  ]
  edge [
    source 41
    target 348
  ]
  edge [
    source 41
    target 349
  ]
  edge [
    source 41
    target 350
  ]
  edge [
    source 41
    target 351
  ]
  edge [
    source 41
    target 352
  ]
  edge [
    source 41
    target 353
  ]
  edge [
    source 41
    target 354
  ]
  edge [
    source 41
    target 355
  ]
  edge [
    source 41
    target 356
  ]
  edge [
    source 41
    target 357
  ]
  edge [
    source 41
    target 358
  ]
  edge [
    source 41
    target 359
  ]
  edge [
    source 41
    target 360
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 361
  ]
  edge [
    source 43
    target 94
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 362
  ]
  edge [
    source 44
    target 363
  ]
  edge [
    source 44
    target 364
  ]
  edge [
    source 44
    target 306
  ]
  edge [
    source 44
    target 365
  ]
  edge [
    source 44
    target 366
  ]
  edge [
    source 44
    target 367
  ]
  edge [
    source 44
    target 368
  ]
  edge [
    source 44
    target 369
  ]
  edge [
    source 44
    target 370
  ]
  edge [
    source 44
    target 371
  ]
  edge [
    source 44
    target 50
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 296
  ]
  edge [
    source 45
    target 372
  ]
  edge [
    source 45
    target 373
  ]
  edge [
    source 45
    target 374
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 375
  ]
  edge [
    source 46
    target 376
  ]
  edge [
    source 46
    target 377
  ]
  edge [
    source 46
    target 378
  ]
  edge [
    source 46
    target 379
  ]
  edge [
    source 46
    target 380
  ]
  edge [
    source 46
    target 381
  ]
  edge [
    source 46
    target 319
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 382
  ]
  edge [
    source 47
    target 383
  ]
  edge [
    source 47
    target 384
  ]
  edge [
    source 47
    target 385
  ]
  edge [
    source 47
    target 386
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 387
  ]
  edge [
    source 49
    target 388
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 50
    target 58
  ]
  edge [
    source 50
    target 389
  ]
  edge [
    source 50
    target 390
  ]
  edge [
    source 50
    target 391
  ]
  edge [
    source 50
    target 392
  ]
  edge [
    source 50
    target 393
  ]
  edge [
    source 50
    target 394
  ]
  edge [
    source 50
    target 395
  ]
  edge [
    source 50
    target 396
  ]
  edge [
    source 50
    target 236
  ]
  edge [
    source 50
    target 397
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 398
  ]
  edge [
    source 51
    target 399
  ]
  edge [
    source 51
    target 400
  ]
  edge [
    source 51
    target 401
  ]
  edge [
    source 51
    target 402
  ]
  edge [
    source 51
    target 403
  ]
  edge [
    source 51
    target 358
  ]
  edge [
    source 51
    target 404
  ]
  edge [
    source 51
    target 154
  ]
  edge [
    source 51
    target 405
  ]
  edge [
    source 51
    target 406
  ]
  edge [
    source 51
    target 407
  ]
  edge [
    source 51
    target 408
  ]
  edge [
    source 51
    target 409
  ]
  edge [
    source 51
    target 474
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 52
    target 410
  ]
  edge [
    source 52
    target 411
  ]
  edge [
    source 52
    target 323
  ]
  edge [
    source 52
    target 412
  ]
  edge [
    source 52
    target 413
  ]
  edge [
    source 52
    target 414
  ]
  edge [
    source 52
    target 415
  ]
  edge [
    source 52
    target 333
  ]
  edge [
    source 52
    target 416
  ]
  edge [
    source 52
    target 417
  ]
  edge [
    source 52
    target 418
  ]
  edge [
    source 52
    target 419
  ]
  edge [
    source 52
    target 420
  ]
  edge [
    source 52
    target 421
  ]
  edge [
    source 52
    target 422
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 423
  ]
  edge [
    source 53
    target 424
  ]
  edge [
    source 53
    target 425
  ]
  edge [
    source 53
    target 474
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 426
  ]
  edge [
    source 55
    target 427
  ]
  edge [
    source 55
    target 474
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 410
  ]
  edge [
    source 56
    target 428
  ]
  edge [
    source 56
    target 429
  ]
  edge [
    source 56
    target 430
  ]
  edge [
    source 56
    target 431
  ]
  edge [
    source 56
    target 432
  ]
  edge [
    source 56
    target 433
  ]
  edge [
    source 56
    target 434
  ]
  edge [
    source 57
    target 435
  ]
  edge [
    source 57
    target 436
  ]
  edge [
    source 57
    target 437
  ]
  edge [
    source 57
    target 438
  ]
  edge [
    source 57
    target 439
  ]
  edge [
    source 58
    target 440
  ]
  edge [
    source 58
    target 441
  ]
  edge [
    source 58
    target 442
  ]
  edge [
    source 58
    target 443
  ]
  edge [
    source 58
    target 444
  ]
  edge [
    source 58
    target 156
  ]
  edge [
    source 58
    target 445
  ]
  edge [
    source 59
    target 446
  ]
  edge [
    source 59
    target 447
  ]
  edge [
    source 59
    target 448
  ]
  edge [
    source 59
    target 449
  ]
  edge [
    source 59
    target 450
  ]
  edge [
    source 59
    target 451
  ]
  edge [
    source 59
    target 452
  ]
  edge [
    source 59
    target 453
  ]
  edge [
    source 59
    target 454
  ]
  edge [
    source 59
    target 455
  ]
  edge [
    source 59
    target 456
  ]
  edge [
    source 59
    target 457
  ]
  edge [
    source 59
    target 458
  ]
  edge [
    source 59
    target 459
  ]
  edge [
    source 59
    target 460
  ]
  edge [
    source 59
    target 296
  ]
  edge [
    source 59
    target 273
  ]
  edge [
    source 59
    target 461
  ]
  edge [
    source 59
    target 462
  ]
  edge [
    source 59
    target 463
  ]
  edge [
    source 59
    target 464
  ]
  edge [
    source 59
    target 465
  ]
  edge [
    source 59
    target 466
  ]
  edge [
    source 59
    target 467
  ]
  edge [
    source 59
    target 468
  ]
  edge [
    source 60
    target 469
  ]
  edge [
    source 60
    target 470
  ]
  edge [
    source 60
    target 471
  ]
  edge [
    source 60
    target 472
  ]
  edge [
    source 475
    target 476
  ]
  edge [
    source 477
    target 478
  ]
  edge [
    source 479
    target 480
  ]
  edge [
    source 481
    target 482
  ]
  edge [
    source 483
    target 484
  ]
  edge [
    source 483
    target 485
  ]
  edge [
    source 484
    target 485
  ]
  edge [
    source 487
    target 488
  ]
  edge [
    source 489
    target 490
  ]
]
