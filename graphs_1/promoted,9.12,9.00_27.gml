graph [
  maxDegree 25
  minDegree 1
  meanDegree 1.9578947368421054
  density 0.020828667413213885
  graphCliqueNumber 2
  node [
    id 0
    label "obszerny"
    origin "text"
  ]
  node [
    id 1
    label "rozmowa"
    origin "text"
  ]
  node [
    id 2
    label "ksi&#281;ga_habakuka"
    origin "text"
  ]
  node [
    id 3
    label "zenon"
    origin "text"
  ]
  node [
    id 4
    label "piechem"
    origin "text"
  ]
  node [
    id 5
    label "profesor"
    origin "text"
  ]
  node [
    id 6
    label "temat"
    origin "text"
  ]
  node [
    id 7
    label "herb"
    origin "text"
  ]
  node [
    id 8
    label "krak&#243;w"
    origin "text"
  ]
  node [
    id 9
    label "obecna"
    origin "text"
  ]
  node [
    id 10
    label "tendencja"
    origin "text"
  ]
  node [
    id 11
    label "magistrat"
    origin "text"
  ]
  node [
    id 12
    label "miejski"
    origin "text"
  ]
  node [
    id 13
    label "usuwa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "du&#380;y"
  ]
  node [
    id 15
    label "obszernie"
  ]
  node [
    id 16
    label "lu&#378;no"
  ]
  node [
    id 17
    label "rozdeptanie"
  ]
  node [
    id 18
    label "d&#322;ugi"
  ]
  node [
    id 19
    label "rozdeptywanie"
  ]
  node [
    id 20
    label "discussion"
  ]
  node [
    id 21
    label "czynno&#347;&#263;"
  ]
  node [
    id 22
    label "odpowied&#378;"
  ]
  node [
    id 23
    label "rozhowor"
  ]
  node [
    id 24
    label "cisza"
  ]
  node [
    id 25
    label "wirtuoz"
  ]
  node [
    id 26
    label "nauczyciel_akademicki"
  ]
  node [
    id 27
    label "tytu&#322;"
  ]
  node [
    id 28
    label "konsulent"
  ]
  node [
    id 29
    label "nauczyciel"
  ]
  node [
    id 30
    label "stopie&#324;_naukowy"
  ]
  node [
    id 31
    label "profesura"
  ]
  node [
    id 32
    label "fraza"
  ]
  node [
    id 33
    label "forma"
  ]
  node [
    id 34
    label "melodia"
  ]
  node [
    id 35
    label "rzecz"
  ]
  node [
    id 36
    label "zbacza&#263;"
  ]
  node [
    id 37
    label "entity"
  ]
  node [
    id 38
    label "omawia&#263;"
  ]
  node [
    id 39
    label "topik"
  ]
  node [
    id 40
    label "wyraz_pochodny"
  ]
  node [
    id 41
    label "om&#243;wi&#263;"
  ]
  node [
    id 42
    label "omawianie"
  ]
  node [
    id 43
    label "w&#261;tek"
  ]
  node [
    id 44
    label "forum"
  ]
  node [
    id 45
    label "cecha"
  ]
  node [
    id 46
    label "zboczenie"
  ]
  node [
    id 47
    label "zbaczanie"
  ]
  node [
    id 48
    label "tre&#347;&#263;"
  ]
  node [
    id 49
    label "tematyka"
  ]
  node [
    id 50
    label "sprawa"
  ]
  node [
    id 51
    label "istota"
  ]
  node [
    id 52
    label "otoczka"
  ]
  node [
    id 53
    label "zboczy&#263;"
  ]
  node [
    id 54
    label "om&#243;wienie"
  ]
  node [
    id 55
    label "trzymacz"
  ]
  node [
    id 56
    label "symbol"
  ]
  node [
    id 57
    label "tarcza_herbowa"
  ]
  node [
    id 58
    label "barwy"
  ]
  node [
    id 59
    label "heraldyka"
  ]
  node [
    id 60
    label "blazonowa&#263;"
  ]
  node [
    id 61
    label "blazonowanie"
  ]
  node [
    id 62
    label "klejnot_herbowy"
  ]
  node [
    id 63
    label "korona_rangowa"
  ]
  node [
    id 64
    label "znak"
  ]
  node [
    id 65
    label "system"
  ]
  node [
    id 66
    label "podatno&#347;&#263;"
  ]
  node [
    id 67
    label "idea"
  ]
  node [
    id 68
    label "ideologia"
  ]
  node [
    id 69
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 70
    label "praktyka"
  ]
  node [
    id 71
    label "metoda"
  ]
  node [
    id 72
    label "rynek"
  ]
  node [
    id 73
    label "plac_ratuszowy"
  ]
  node [
    id 74
    label "sekretariat"
  ]
  node [
    id 75
    label "urz&#281;dnik"
  ]
  node [
    id 76
    label "urz&#261;d"
  ]
  node [
    id 77
    label "urz&#281;dnik_miejski"
  ]
  node [
    id 78
    label "siedziba"
  ]
  node [
    id 79
    label "zwierzchnik"
  ]
  node [
    id 80
    label "miejsko"
  ]
  node [
    id 81
    label "miastowy"
  ]
  node [
    id 82
    label "typowy"
  ]
  node [
    id 83
    label "publiczny"
  ]
  node [
    id 84
    label "blurt_out"
  ]
  node [
    id 85
    label "przenosi&#263;"
  ]
  node [
    id 86
    label "rugowa&#263;"
  ]
  node [
    id 87
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 88
    label "robi&#263;"
  ]
  node [
    id 89
    label "zabija&#263;"
  ]
  node [
    id 90
    label "przesuwa&#263;"
  ]
  node [
    id 91
    label "undo"
  ]
  node [
    id 92
    label "powodowa&#263;"
  ]
  node [
    id 93
    label "Zenon"
  ]
  node [
    id 94
    label "Piechem"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 93
    target 94
  ]
]
