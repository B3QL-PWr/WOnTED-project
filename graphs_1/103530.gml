graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.9534883720930232
  density 0.046511627906976744
  graphCliqueNumber 2
  node [
    id 0
    label "recepcja"
    origin "text"
  ]
  node [
    id 1
    label "hotelowy"
    origin "text"
  ]
  node [
    id 2
    label "wchodzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "szcz&#281;sna"
    origin "text"
  ]
  node [
    id 4
    label "kupka"
    origin "text"
  ]
  node [
    id 5
    label "przyj&#281;cie"
  ]
  node [
    id 6
    label "reakcja"
  ]
  node [
    id 7
    label "miejsce"
  ]
  node [
    id 8
    label "spos&#243;b"
  ]
  node [
    id 9
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 10
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 11
    label "przenika&#263;"
  ]
  node [
    id 12
    label "przekracza&#263;"
  ]
  node [
    id 13
    label "nast&#281;powa&#263;"
  ]
  node [
    id 14
    label "dochodzi&#263;"
  ]
  node [
    id 15
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 16
    label "intervene"
  ]
  node [
    id 17
    label "scale"
  ]
  node [
    id 18
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 19
    label "&#322;oi&#263;"
  ]
  node [
    id 20
    label "osi&#261;ga&#263;"
  ]
  node [
    id 21
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 22
    label "poznawa&#263;"
  ]
  node [
    id 23
    label "go"
  ]
  node [
    id 24
    label "atakowa&#263;"
  ]
  node [
    id 25
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 26
    label "mount"
  ]
  node [
    id 27
    label "invade"
  ]
  node [
    id 28
    label "bra&#263;"
  ]
  node [
    id 29
    label "wnika&#263;"
  ]
  node [
    id 30
    label "move"
  ]
  node [
    id 31
    label "zaziera&#263;"
  ]
  node [
    id 32
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 33
    label "spotyka&#263;"
  ]
  node [
    id 34
    label "zaczyna&#263;"
  ]
  node [
    id 35
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 36
    label "balas"
  ]
  node [
    id 37
    label "stool"
  ]
  node [
    id 38
    label "g&#243;wno"
  ]
  node [
    id 39
    label "koprofilia"
  ]
  node [
    id 40
    label "wydalina"
  ]
  node [
    id 41
    label "fekalia"
  ]
  node [
    id 42
    label "odchody"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
]
