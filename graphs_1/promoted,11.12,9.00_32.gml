graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.8571428571428572
  density 0.06878306878306878
  graphCliqueNumber 2
  node [
    id 0
    label "cze&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "mirka"
    origin "text"
  ]
  node [
    id 2
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mirabelka"
    origin "text"
  ]
  node [
    id 4
    label "zaimponowanie"
  ]
  node [
    id 5
    label "szanowa&#263;"
  ]
  node [
    id 6
    label "uhonorowa&#263;"
  ]
  node [
    id 7
    label "honorowanie"
  ]
  node [
    id 8
    label "uszanowa&#263;"
  ]
  node [
    id 9
    label "rewerencja"
  ]
  node [
    id 10
    label "uszanowanie"
  ]
  node [
    id 11
    label "imponowanie"
  ]
  node [
    id 12
    label "dobro"
  ]
  node [
    id 13
    label "uhonorowanie"
  ]
  node [
    id 14
    label "respect"
  ]
  node [
    id 15
    label "honorowa&#263;"
  ]
  node [
    id 16
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 17
    label "szacuneczek"
  ]
  node [
    id 18
    label "postawa"
  ]
  node [
    id 19
    label "pozdrawia&#263;"
  ]
  node [
    id 20
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 21
    label "greet"
  ]
  node [
    id 22
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 23
    label "welcome"
  ]
  node [
    id 24
    label "&#347;liwka"
  ]
  node [
    id 25
    label "&#347;liwa_domowa"
  ]
  node [
    id 26
    label "wykop"
  ]
  node [
    id 27
    label "efekt"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 26
    target 27
  ]
]
