graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.0150375939849625
  density 0.015265436318067897
  graphCliqueNumber 2
  node [
    id 0
    label "te&#380;"
    origin "text"
  ]
  node [
    id 1
    label "nasa"
    origin "text"
  ]
  node [
    id 2
    label "nie"
    origin "text"
  ]
  node [
    id 3
    label "trza"
    origin "text"
  ]
  node [
    id 4
    label "spr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 6
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 7
    label "para"
    origin "text"
  ]
  node [
    id 8
    label "&#380;ywy"
    origin "text"
  ]
  node [
    id 9
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 10
    label "bych"
    origin "text"
  ]
  node [
    id 11
    label "ochfiarowa&#322;"
    origin "text"
  ]
  node [
    id 12
    label "dzienia"
    origin "text"
  ]
  node [
    id 13
    label "po&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 14
    label "ten"
    origin "text"
  ]
  node [
    id 15
    label "intencyj&#281;"
    origin "text"
  ]
  node [
    id 16
    label "kiedy"
    origin "text"
  ]
  node [
    id 17
    label "strasznie"
    origin "text"
  ]
  node [
    id 18
    label "md&#322;y"
    origin "text"
  ]
  node [
    id 19
    label "zaraz"
    origin "text"
  ]
  node [
    id 20
    label "&#322;apy"
    origin "text"
  ]
  node [
    id 21
    label "zdycha&#263;"
    origin "text"
  ]
  node [
    id 22
    label "cho&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 23
    label "uj&#261;&#263;"
    origin "text"
  ]
  node [
    id 24
    label "pazdur"
    origin "text"
  ]
  node [
    id 25
    label "sprzeciw"
  ]
  node [
    id 26
    label "trzeba"
  ]
  node [
    id 27
    label "sprawdzi&#263;"
  ]
  node [
    id 28
    label "spo&#380;y&#263;"
  ]
  node [
    id 29
    label "pos&#322;u&#380;y&#263;_si&#281;"
  ]
  node [
    id 30
    label "try"
  ]
  node [
    id 31
    label "zakosztowa&#263;"
  ]
  node [
    id 32
    label "podj&#261;&#263;"
  ]
  node [
    id 33
    label "zrobi&#263;"
  ]
  node [
    id 34
    label "get"
  ]
  node [
    id 35
    label "doczeka&#263;"
  ]
  node [
    id 36
    label "zwiastun"
  ]
  node [
    id 37
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 38
    label "develop"
  ]
  node [
    id 39
    label "catch"
  ]
  node [
    id 40
    label "uzyska&#263;"
  ]
  node [
    id 41
    label "kupi&#263;"
  ]
  node [
    id 42
    label "wzi&#261;&#263;"
  ]
  node [
    id 43
    label "naby&#263;"
  ]
  node [
    id 44
    label "nabawienie_si&#281;"
  ]
  node [
    id 45
    label "obskoczy&#263;"
  ]
  node [
    id 46
    label "zapanowa&#263;"
  ]
  node [
    id 47
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 48
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 49
    label "nabawianie_si&#281;"
  ]
  node [
    id 50
    label "range"
  ]
  node [
    id 51
    label "schorzenie"
  ]
  node [
    id 52
    label "wystarczy&#263;"
  ]
  node [
    id 53
    label "wysta&#263;"
  ]
  node [
    id 54
    label "gaz_cieplarniany"
  ]
  node [
    id 55
    label "grupa"
  ]
  node [
    id 56
    label "smoke"
  ]
  node [
    id 57
    label "pair"
  ]
  node [
    id 58
    label "sztuka"
  ]
  node [
    id 59
    label "Albania"
  ]
  node [
    id 60
    label "dodatek"
  ]
  node [
    id 61
    label "odparowanie"
  ]
  node [
    id 62
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 63
    label "odparowywa&#263;"
  ]
  node [
    id 64
    label "nale&#380;e&#263;"
  ]
  node [
    id 65
    label "wyparowanie"
  ]
  node [
    id 66
    label "zesp&#243;&#322;"
  ]
  node [
    id 67
    label "parowanie"
  ]
  node [
    id 68
    label "damp"
  ]
  node [
    id 69
    label "odparowywanie"
  ]
  node [
    id 70
    label "poker"
  ]
  node [
    id 71
    label "moneta"
  ]
  node [
    id 72
    label "odparowa&#263;"
  ]
  node [
    id 73
    label "jednostka_monetarna"
  ]
  node [
    id 74
    label "uk&#322;ad"
  ]
  node [
    id 75
    label "gaz"
  ]
  node [
    id 76
    label "chodzi&#263;"
  ]
  node [
    id 77
    label "zbi&#243;r"
  ]
  node [
    id 78
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 79
    label "czynny"
  ]
  node [
    id 80
    label "cz&#322;owiek"
  ]
  node [
    id 81
    label "aktualny"
  ]
  node [
    id 82
    label "realistyczny"
  ]
  node [
    id 83
    label "silny"
  ]
  node [
    id 84
    label "&#380;ywotny"
  ]
  node [
    id 85
    label "g&#322;&#281;boki"
  ]
  node [
    id 86
    label "naturalny"
  ]
  node [
    id 87
    label "&#380;ycie"
  ]
  node [
    id 88
    label "ciekawy"
  ]
  node [
    id 89
    label "&#380;ywo"
  ]
  node [
    id 90
    label "prawdziwy"
  ]
  node [
    id 91
    label "zgrabny"
  ]
  node [
    id 92
    label "o&#380;ywianie"
  ]
  node [
    id 93
    label "szybki"
  ]
  node [
    id 94
    label "wyra&#378;ny"
  ]
  node [
    id 95
    label "energiczny"
  ]
  node [
    id 96
    label "zachowywa&#263;"
  ]
  node [
    id 97
    label "robi&#263;"
  ]
  node [
    id 98
    label "fast"
  ]
  node [
    id 99
    label "okre&#347;lony"
  ]
  node [
    id 100
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 101
    label "jak_cholera"
  ]
  node [
    id 102
    label "kurewsko"
  ]
  node [
    id 103
    label "ogromnie"
  ]
  node [
    id 104
    label "straszny"
  ]
  node [
    id 105
    label "okropno"
  ]
  node [
    id 106
    label "md&#322;o"
  ]
  node [
    id 107
    label "nijaki"
  ]
  node [
    id 108
    label "s&#322;aby"
  ]
  node [
    id 109
    label "ckliwy"
  ]
  node [
    id 110
    label "przykry"
  ]
  node [
    id 111
    label "nieprzyjemny"
  ]
  node [
    id 112
    label "nik&#322;y"
  ]
  node [
    id 113
    label "blisko"
  ]
  node [
    id 114
    label "zara"
  ]
  node [
    id 115
    label "nied&#322;ugo"
  ]
  node [
    id 116
    label "ko&#324;czy&#263;"
  ]
  node [
    id 117
    label "pada&#263;"
  ]
  node [
    id 118
    label "umiera&#263;"
  ]
  node [
    id 119
    label "podupada&#263;"
  ]
  node [
    id 120
    label "die"
  ]
  node [
    id 121
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 122
    label "zabra&#263;"
  ]
  node [
    id 123
    label "zakomunikowa&#263;"
  ]
  node [
    id 124
    label "zaaresztowa&#263;"
  ]
  node [
    id 125
    label "fascinate"
  ]
  node [
    id 126
    label "reduce"
  ]
  node [
    id 127
    label "suspend"
  ]
  node [
    id 128
    label "testify"
  ]
  node [
    id 129
    label "wzbudzi&#263;"
  ]
  node [
    id 130
    label "zamkn&#261;&#263;"
  ]
  node [
    id 131
    label "dach"
  ]
  node [
    id 132
    label "ozdoba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 18
    target 107
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 114
  ]
  edge [
    source 19
    target 115
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 116
  ]
  edge [
    source 21
    target 117
  ]
  edge [
    source 21
    target 118
  ]
  edge [
    source 21
    target 119
  ]
  edge [
    source 21
    target 120
  ]
  edge [
    source 21
    target 121
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 122
  ]
  edge [
    source 23
    target 123
  ]
  edge [
    source 23
    target 124
  ]
  edge [
    source 23
    target 42
  ]
  edge [
    source 23
    target 125
  ]
  edge [
    source 23
    target 126
  ]
  edge [
    source 23
    target 127
  ]
  edge [
    source 23
    target 128
  ]
  edge [
    source 23
    target 129
  ]
  edge [
    source 23
    target 130
  ]
  edge [
    source 24
    target 131
  ]
  edge [
    source 24
    target 132
  ]
]
