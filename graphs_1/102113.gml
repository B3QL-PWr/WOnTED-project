graph [
  maxDegree 17
  minDegree 1
  meanDegree 2.16793893129771
  density 0.016676453317674693
  graphCliqueNumber 3
  node [
    id 0
    label "wyr&#243;wnywarka"
    origin "text"
  ]
  node [
    id 1
    label "zwa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "popularnie"
    origin "text"
  ]
  node [
    id 3
    label "paj&#261;czek"
    origin "text"
  ]
  node [
    id 4
    label "s&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wyr&#243;wnywa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "napi&#281;cie"
    origin "text"
  ]
  node [
    id 7
    label "poszczeg&#243;lny"
    origin "text"
  ]
  node [
    id 8
    label "ogniwo"
    origin "text"
  ]
  node [
    id 9
    label "pakiet"
    origin "text"
  ]
  node [
    id 10
    label "zabieg"
    origin "text"
  ]
  node [
    id 11
    label "taki"
    origin "text"
  ]
  node [
    id 12
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 13
    label "korzystny"
    origin "text"
  ]
  node [
    id 14
    label "wp&#322;yw"
    origin "text"
  ]
  node [
    id 15
    label "&#380;ywotno&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "wydajno&#347;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "polega&#263;"
    origin "text"
  ]
  node [
    id 18
    label "kontrolowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "roz&#322;adowywa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 21
    label "indywidualnie"
    origin "text"
  ]
  node [
    id 22
    label "ustali&#263;"
    origin "text"
  ]
  node [
    id 23
    label "nazywa&#263;"
  ]
  node [
    id 24
    label "obiegowy"
  ]
  node [
    id 25
    label "cz&#281;sto"
  ]
  node [
    id 26
    label "nisko"
  ]
  node [
    id 27
    label "popularny"
  ]
  node [
    id 28
    label "przyst&#281;pnie"
  ]
  node [
    id 29
    label "&#322;atwo"
  ]
  node [
    id 30
    label "normally"
  ]
  node [
    id 31
    label "a&#380;ur"
  ]
  node [
    id 32
    label "naczynie"
  ]
  node [
    id 33
    label "uszkodzenie"
  ]
  node [
    id 34
    label "use"
  ]
  node [
    id 35
    label "trwa&#263;"
  ]
  node [
    id 36
    label "by&#263;"
  ]
  node [
    id 37
    label "&#380;o&#322;nierz"
  ]
  node [
    id 38
    label "pies"
  ]
  node [
    id 39
    label "robi&#263;"
  ]
  node [
    id 40
    label "wait"
  ]
  node [
    id 41
    label "pomaga&#263;"
  ]
  node [
    id 42
    label "cel"
  ]
  node [
    id 43
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 44
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 45
    label "pracowa&#263;"
  ]
  node [
    id 46
    label "suffice"
  ]
  node [
    id 47
    label "match"
  ]
  node [
    id 48
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 49
    label "zdobywa&#263;"
  ]
  node [
    id 50
    label "check"
  ]
  node [
    id 51
    label "level"
  ]
  node [
    id 52
    label "ujednolica&#263;"
  ]
  node [
    id 53
    label "trim"
  ]
  node [
    id 54
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 55
    label "settle"
  ]
  node [
    id 56
    label "rusza&#263;"
  ]
  node [
    id 57
    label "usztywnienie"
  ]
  node [
    id 58
    label "napr&#281;&#380;enie"
  ]
  node [
    id 59
    label "striving"
  ]
  node [
    id 60
    label "nastr&#243;j"
  ]
  node [
    id 61
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 62
    label "tension"
  ]
  node [
    id 63
    label "poszczeg&#243;lnie"
  ]
  node [
    id 64
    label "pojedynczy"
  ]
  node [
    id 65
    label "k&#243;&#322;ko"
  ]
  node [
    id 66
    label "&#322;a&#324;cuch"
  ]
  node [
    id 67
    label "czynnik"
  ]
  node [
    id 68
    label "cell"
  ]
  node [
    id 69
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 70
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 71
    label "kontakt"
  ]
  node [
    id 72
    label "filia"
  ]
  node [
    id 73
    label "kompozycja"
  ]
  node [
    id 74
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 75
    label "akcja"
  ]
  node [
    id 76
    label "ilo&#347;&#263;"
  ]
  node [
    id 77
    label "porcja"
  ]
  node [
    id 78
    label "package"
  ]
  node [
    id 79
    label "bundle"
  ]
  node [
    id 80
    label "jednostka_informacji"
  ]
  node [
    id 81
    label "czyn"
  ]
  node [
    id 82
    label "leczenie"
  ]
  node [
    id 83
    label "operation"
  ]
  node [
    id 84
    label "okre&#347;lony"
  ]
  node [
    id 85
    label "jaki&#347;"
  ]
  node [
    id 86
    label "czyj&#347;"
  ]
  node [
    id 87
    label "m&#261;&#380;"
  ]
  node [
    id 88
    label "dobry"
  ]
  node [
    id 89
    label "korzystnie"
  ]
  node [
    id 90
    label "&#347;lad"
  ]
  node [
    id 91
    label "doch&#243;d_narodowy"
  ]
  node [
    id 92
    label "zjawisko"
  ]
  node [
    id 93
    label "rezultat"
  ]
  node [
    id 94
    label "kwota"
  ]
  node [
    id 95
    label "lobbysta"
  ]
  node [
    id 96
    label "wa&#380;ko&#347;&#263;"
  ]
  node [
    id 97
    label "kategoria_gramatyczna"
  ]
  node [
    id 98
    label "czas"
  ]
  node [
    id 99
    label "cecha"
  ]
  node [
    id 100
    label "aktualno&#347;&#263;"
  ]
  node [
    id 101
    label "charakter"
  ]
  node [
    id 102
    label "skuteczno&#347;&#263;"
  ]
  node [
    id 103
    label "op&#322;acalno&#347;&#263;"
  ]
  node [
    id 104
    label "ufa&#263;"
  ]
  node [
    id 105
    label "consist"
  ]
  node [
    id 106
    label "trust"
  ]
  node [
    id 107
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 108
    label "examine"
  ]
  node [
    id 109
    label "manipulate"
  ]
  node [
    id 110
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 111
    label "throng"
  ]
  node [
    id 112
    label "odreagowywa&#263;"
  ]
  node [
    id 113
    label "opr&#243;&#380;nia&#263;"
  ]
  node [
    id 114
    label "&#322;agodzi&#263;"
  ]
  node [
    id 115
    label "discharge"
  ]
  node [
    id 116
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 117
    label "rozbraja&#263;"
  ]
  node [
    id 118
    label "powodowa&#263;"
  ]
  node [
    id 119
    label "osobno"
  ]
  node [
    id 120
    label "indywidualny"
  ]
  node [
    id 121
    label "singly"
  ]
  node [
    id 122
    label "swoi&#347;cie"
  ]
  node [
    id 123
    label "individually"
  ]
  node [
    id 124
    label "umocni&#263;"
  ]
  node [
    id 125
    label "bind"
  ]
  node [
    id 126
    label "zdecydowa&#263;"
  ]
  node [
    id 127
    label "spowodowa&#263;"
  ]
  node [
    id 128
    label "unwrap"
  ]
  node [
    id 129
    label "zrobi&#263;"
  ]
  node [
    id 130
    label "put"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 17
    target 106
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 39
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 45
  ]
  edge [
    source 18
    target 47
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 111
  ]
  edge [
    source 19
    target 112
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 114
  ]
  edge [
    source 19
    target 115
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 20
    target 85
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 119
  ]
  edge [
    source 21
    target 120
  ]
  edge [
    source 21
    target 121
  ]
  edge [
    source 21
    target 122
  ]
  edge [
    source 21
    target 123
  ]
  edge [
    source 22
    target 124
  ]
  edge [
    source 22
    target 125
  ]
  edge [
    source 22
    target 126
  ]
  edge [
    source 22
    target 127
  ]
  edge [
    source 22
    target 128
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 130
  ]
]
