graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 2
  node [
    id 0
    label "dokument"
    origin "text"
  ]
  node [
    id 1
    label "sk&#322;adany"
    origin "text"
  ]
  node [
    id 2
    label "przez"
    origin "text"
  ]
  node [
    id 3
    label "kandydat"
    origin "text"
  ]
  node [
    id 4
    label "musza"
    origin "text"
  ]
  node [
    id 5
    label "zawiera&#263;"
    origin "text"
  ]
  node [
    id 6
    label "record"
  ]
  node [
    id 7
    label "wytw&#243;r"
  ]
  node [
    id 8
    label "&#347;wiadectwo"
  ]
  node [
    id 9
    label "zapis"
  ]
  node [
    id 10
    label "raport&#243;wka"
  ]
  node [
    id 11
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 12
    label "artyku&#322;"
  ]
  node [
    id 13
    label "plik"
  ]
  node [
    id 14
    label "writing"
  ]
  node [
    id 15
    label "utw&#243;r"
  ]
  node [
    id 16
    label "dokumentacja"
  ]
  node [
    id 17
    label "registratura"
  ]
  node [
    id 18
    label "parafa"
  ]
  node [
    id 19
    label "sygnatariusz"
  ]
  node [
    id 20
    label "fascyku&#322;"
  ]
  node [
    id 21
    label "cz&#322;owiek"
  ]
  node [
    id 22
    label "materia&#322;"
  ]
  node [
    id 23
    label "lista_wyborcza"
  ]
  node [
    id 24
    label "bierne_prawo_wyborcze"
  ]
  node [
    id 25
    label "aspirowanie"
  ]
  node [
    id 26
    label "obejmowa&#263;"
  ]
  node [
    id 27
    label "mie&#263;"
  ]
  node [
    id 28
    label "zamyka&#263;"
  ]
  node [
    id 29
    label "lock"
  ]
  node [
    id 30
    label "poznawa&#263;"
  ]
  node [
    id 31
    label "fold"
  ]
  node [
    id 32
    label "make"
  ]
  node [
    id 33
    label "ustala&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
]
