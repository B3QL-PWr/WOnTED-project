graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.272727272727273
  density 0.03496503496503497
  graphCliqueNumber 6
  node [
    id 0
    label "wchodzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "autobus"
    origin "text"
  ]
  node [
    id 2
    label "pks"
    origin "text"
  ]
  node [
    id 3
    label "bilet"
    origin "text"
  ]
  node [
    id 4
    label "miejscowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 6
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 7
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 8
    label "przenika&#263;"
  ]
  node [
    id 9
    label "przekracza&#263;"
  ]
  node [
    id 10
    label "nast&#281;powa&#263;"
  ]
  node [
    id 11
    label "dochodzi&#263;"
  ]
  node [
    id 12
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 13
    label "intervene"
  ]
  node [
    id 14
    label "scale"
  ]
  node [
    id 15
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 16
    label "&#322;oi&#263;"
  ]
  node [
    id 17
    label "osi&#261;ga&#263;"
  ]
  node [
    id 18
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 19
    label "poznawa&#263;"
  ]
  node [
    id 20
    label "go"
  ]
  node [
    id 21
    label "atakowa&#263;"
  ]
  node [
    id 22
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 23
    label "mount"
  ]
  node [
    id 24
    label "invade"
  ]
  node [
    id 25
    label "bra&#263;"
  ]
  node [
    id 26
    label "wnika&#263;"
  ]
  node [
    id 27
    label "move"
  ]
  node [
    id 28
    label "zaziera&#263;"
  ]
  node [
    id 29
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 30
    label "spotyka&#263;"
  ]
  node [
    id 31
    label "zaczyna&#263;"
  ]
  node [
    id 32
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 33
    label "samoch&#243;d"
  ]
  node [
    id 34
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 35
    label "karta_wst&#281;pu"
  ]
  node [
    id 36
    label "cedu&#322;a"
  ]
  node [
    id 37
    label "passe-partout"
  ]
  node [
    id 38
    label "konik"
  ]
  node [
    id 39
    label "Aurignac"
  ]
  node [
    id 40
    label "osiedle"
  ]
  node [
    id 41
    label "Levallois-Perret"
  ]
  node [
    id 42
    label "Sabaudia"
  ]
  node [
    id 43
    label "Saint-Acheul"
  ]
  node [
    id 44
    label "Opat&#243;wek"
  ]
  node [
    id 45
    label "Boulogne"
  ]
  node [
    id 46
    label "Cecora"
  ]
  node [
    id 47
    label "szlachetny"
  ]
  node [
    id 48
    label "metaliczny"
  ]
  node [
    id 49
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 50
    label "z&#322;ocenie"
  ]
  node [
    id 51
    label "grosz"
  ]
  node [
    id 52
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 53
    label "utytu&#322;owany"
  ]
  node [
    id 54
    label "poz&#322;ocenie"
  ]
  node [
    id 55
    label "Polska"
  ]
  node [
    id 56
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 57
    label "wspania&#322;y"
  ]
  node [
    id 58
    label "doskona&#322;y"
  ]
  node [
    id 59
    label "kochany"
  ]
  node [
    id 60
    label "jednostka_monetarna"
  ]
  node [
    id 61
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 62
    label "PKS"
  ]
  node [
    id 63
    label "do"
  ]
  node [
    id 64
    label "X"
  ]
  node [
    id 65
    label "9"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 64
    target 65
  ]
]
