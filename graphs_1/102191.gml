graph [
  maxDegree 31
  minDegree 1
  meanDegree 1.9523809523809523
  density 0.047619047619047616
  graphCliqueNumber 2
  node [
    id 0
    label "czeladnik"
    origin "text"
  ]
  node [
    id 1
    label "kamionek"
    origin "text"
  ]
  node [
    id 2
    label "b&#322;ysn&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "bia&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "z&#261;b"
    origin "text"
  ]
  node [
    id 5
    label "rzemie&#347;lniczek"
  ]
  node [
    id 6
    label "rzemie&#347;lnik"
  ]
  node [
    id 7
    label "ucze&#324;"
  ]
  node [
    id 8
    label "za&#347;wieci&#263;_si&#281;"
  ]
  node [
    id 9
    label "zademonstrowa&#263;"
  ]
  node [
    id 10
    label "fall"
  ]
  node [
    id 11
    label "&#322;ysn&#261;&#263;"
  ]
  node [
    id 12
    label "uz&#281;bienie"
  ]
  node [
    id 13
    label "plombowa&#263;"
  ]
  node [
    id 14
    label "polakowa&#263;"
  ]
  node [
    id 15
    label "leczenie_kana&#322;owe"
  ]
  node [
    id 16
    label "korona"
  ]
  node [
    id 17
    label "zaplombowa&#263;"
  ]
  node [
    id 18
    label "ubytek"
  ]
  node [
    id 19
    label "emaliowa&#263;"
  ]
  node [
    id 20
    label "zaplombowanie"
  ]
  node [
    id 21
    label "kamfenol"
  ]
  node [
    id 22
    label "obr&#281;cz"
  ]
  node [
    id 23
    label "artykulator"
  ]
  node [
    id 24
    label "plombowanie"
  ]
  node [
    id 25
    label "z&#281;batka"
  ]
  node [
    id 26
    label "szczoteczka"
  ]
  node [
    id 27
    label "mostek"
  ]
  node [
    id 28
    label "&#322;uk_z&#281;bowy"
  ]
  node [
    id 29
    label "&#380;uchwa"
  ]
  node [
    id 30
    label "emaliowanie"
  ]
  node [
    id 31
    label "z&#281;bina"
  ]
  node [
    id 32
    label "ostrze"
  ]
  node [
    id 33
    label "polakowanie"
  ]
  node [
    id 34
    label "tooth"
  ]
  node [
    id 35
    label "borowa&#263;"
  ]
  node [
    id 36
    label "borowanie"
  ]
  node [
    id 37
    label "cement"
  ]
  node [
    id 38
    label "szkliwo"
  ]
  node [
    id 39
    label "miazga_z&#281;ba"
  ]
  node [
    id 40
    label "wierzcho&#322;ek_korzenia"
  ]
  node [
    id 41
    label "element_anatomiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
]
