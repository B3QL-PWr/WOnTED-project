graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 1
    label "bekazprawakow"
    origin "text"
  ]
  node [
    id 2
    label "bekazpisu"
    origin "text"
  ]
  node [
    id 3
    label "neuropa"
    origin "text"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
