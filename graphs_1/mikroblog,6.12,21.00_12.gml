graph [
  maxDegree 36
  minDegree 1
  meanDegree 2
  density 0.0125
  graphCliqueNumber 3
  node [
    id 0
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "temu"
    origin "text"
  ]
  node [
    id 2
    label "zawodnik"
    origin "text"
  ]
  node [
    id 3
    label "seria"
    origin "text"
  ]
  node [
    id 4
    label "gra&#263;"
    origin "text"
  ]
  node [
    id 5
    label "twarz"
    origin "text"
  ]
  node [
    id 6
    label "pomalowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "czerwono"
    origin "text"
  ]
  node [
    id 8
    label "zaprotestowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "przeciwko"
    origin "text"
  ]
  node [
    id 10
    label "przemoc"
    origin "text"
  ]
  node [
    id 11
    label "wobec"
    origin "text"
  ]
  node [
    id 12
    label "kobieta"
    origin "text"
  ]
  node [
    id 13
    label "s&#322;o&#324;ce"
  ]
  node [
    id 14
    label "czynienie_si&#281;"
  ]
  node [
    id 15
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 16
    label "czas"
  ]
  node [
    id 17
    label "long_time"
  ]
  node [
    id 18
    label "przedpo&#322;udnie"
  ]
  node [
    id 19
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 20
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 21
    label "tydzie&#324;"
  ]
  node [
    id 22
    label "godzina"
  ]
  node [
    id 23
    label "t&#322;usty_czwartek"
  ]
  node [
    id 24
    label "wsta&#263;"
  ]
  node [
    id 25
    label "day"
  ]
  node [
    id 26
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 27
    label "przedwiecz&#243;r"
  ]
  node [
    id 28
    label "Sylwester"
  ]
  node [
    id 29
    label "po&#322;udnie"
  ]
  node [
    id 30
    label "wzej&#347;cie"
  ]
  node [
    id 31
    label "podwiecz&#243;r"
  ]
  node [
    id 32
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 33
    label "rano"
  ]
  node [
    id 34
    label "termin"
  ]
  node [
    id 35
    label "ranek"
  ]
  node [
    id 36
    label "doba"
  ]
  node [
    id 37
    label "wiecz&#243;r"
  ]
  node [
    id 38
    label "walentynki"
  ]
  node [
    id 39
    label "popo&#322;udnie"
  ]
  node [
    id 40
    label "noc"
  ]
  node [
    id 41
    label "wstanie"
  ]
  node [
    id 42
    label "facet"
  ]
  node [
    id 43
    label "czo&#322;&#243;wka"
  ]
  node [
    id 44
    label "lista_startowa"
  ]
  node [
    id 45
    label "uczestnik"
  ]
  node [
    id 46
    label "orygina&#322;"
  ]
  node [
    id 47
    label "sportowiec"
  ]
  node [
    id 48
    label "zi&#243;&#322;ko"
  ]
  node [
    id 49
    label "stage_set"
  ]
  node [
    id 50
    label "partia"
  ]
  node [
    id 51
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 52
    label "sekwencja"
  ]
  node [
    id 53
    label "komplet"
  ]
  node [
    id 54
    label "przebieg"
  ]
  node [
    id 55
    label "zestawienie"
  ]
  node [
    id 56
    label "jednostka_systematyczna"
  ]
  node [
    id 57
    label "d&#378;wi&#281;k"
  ]
  node [
    id 58
    label "line"
  ]
  node [
    id 59
    label "zbi&#243;r"
  ]
  node [
    id 60
    label "produkcja"
  ]
  node [
    id 61
    label "set"
  ]
  node [
    id 62
    label "jednostka"
  ]
  node [
    id 63
    label "&#347;wieci&#263;"
  ]
  node [
    id 64
    label "typify"
  ]
  node [
    id 65
    label "majaczy&#263;"
  ]
  node [
    id 66
    label "dzia&#322;a&#263;"
  ]
  node [
    id 67
    label "rola"
  ]
  node [
    id 68
    label "wykonywa&#263;"
  ]
  node [
    id 69
    label "tokowa&#263;"
  ]
  node [
    id 70
    label "prezentowa&#263;"
  ]
  node [
    id 71
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 72
    label "rozgrywa&#263;"
  ]
  node [
    id 73
    label "przedstawia&#263;"
  ]
  node [
    id 74
    label "wykorzystywa&#263;"
  ]
  node [
    id 75
    label "wida&#263;"
  ]
  node [
    id 76
    label "brzmie&#263;"
  ]
  node [
    id 77
    label "dally"
  ]
  node [
    id 78
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 79
    label "robi&#263;"
  ]
  node [
    id 80
    label "do"
  ]
  node [
    id 81
    label "instrument_muzyczny"
  ]
  node [
    id 82
    label "play"
  ]
  node [
    id 83
    label "otwarcie"
  ]
  node [
    id 84
    label "szczeka&#263;"
  ]
  node [
    id 85
    label "cope"
  ]
  node [
    id 86
    label "pasowa&#263;"
  ]
  node [
    id 87
    label "napierdziela&#263;"
  ]
  node [
    id 88
    label "sound"
  ]
  node [
    id 89
    label "muzykowa&#263;"
  ]
  node [
    id 90
    label "stara&#263;_si&#281;"
  ]
  node [
    id 91
    label "i&#347;&#263;"
  ]
  node [
    id 92
    label "cz&#322;owiek"
  ]
  node [
    id 93
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 94
    label "profil"
  ]
  node [
    id 95
    label "ucho"
  ]
  node [
    id 96
    label "policzek"
  ]
  node [
    id 97
    label "czo&#322;o"
  ]
  node [
    id 98
    label "usta"
  ]
  node [
    id 99
    label "micha"
  ]
  node [
    id 100
    label "powieka"
  ]
  node [
    id 101
    label "podbr&#243;dek"
  ]
  node [
    id 102
    label "p&#243;&#322;profil"
  ]
  node [
    id 103
    label "liczko"
  ]
  node [
    id 104
    label "wyraz_twarzy"
  ]
  node [
    id 105
    label "dzi&#243;b"
  ]
  node [
    id 106
    label "rys"
  ]
  node [
    id 107
    label "zas&#322;ona"
  ]
  node [
    id 108
    label "twarzyczka"
  ]
  node [
    id 109
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 110
    label "nos"
  ]
  node [
    id 111
    label "reputacja"
  ]
  node [
    id 112
    label "pysk"
  ]
  node [
    id 113
    label "cera"
  ]
  node [
    id 114
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 115
    label "p&#322;e&#263;"
  ]
  node [
    id 116
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 117
    label "maskowato&#347;&#263;"
  ]
  node [
    id 118
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 119
    label "przedstawiciel"
  ]
  node [
    id 120
    label "brew"
  ]
  node [
    id 121
    label "uj&#281;cie"
  ]
  node [
    id 122
    label "prz&#243;d"
  ]
  node [
    id 123
    label "posta&#263;"
  ]
  node [
    id 124
    label "wielko&#347;&#263;"
  ]
  node [
    id 125
    label "oko"
  ]
  node [
    id 126
    label "pokry&#263;"
  ]
  node [
    id 127
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 128
    label "upi&#281;kszy&#263;"
  ]
  node [
    id 129
    label "paint"
  ]
  node [
    id 130
    label "key"
  ]
  node [
    id 131
    label "na&#322;o&#380;y&#263;"
  ]
  node [
    id 132
    label "lewicowo"
  ]
  node [
    id 133
    label "czerwony"
  ]
  node [
    id 134
    label "gor&#261;co"
  ]
  node [
    id 135
    label "komunistyczny"
  ]
  node [
    id 136
    label "ciep&#322;o"
  ]
  node [
    id 137
    label "post&#261;pi&#263;"
  ]
  node [
    id 138
    label "object"
  ]
  node [
    id 139
    label "przewaga"
  ]
  node [
    id 140
    label "drastyczny"
  ]
  node [
    id 141
    label "agresja"
  ]
  node [
    id 142
    label "patologia"
  ]
  node [
    id 143
    label "przekwitanie"
  ]
  node [
    id 144
    label "m&#281;&#380;yna"
  ]
  node [
    id 145
    label "babka"
  ]
  node [
    id 146
    label "samica"
  ]
  node [
    id 147
    label "doros&#322;y"
  ]
  node [
    id 148
    label "ulec"
  ]
  node [
    id 149
    label "uleganie"
  ]
  node [
    id 150
    label "partnerka"
  ]
  node [
    id 151
    label "&#380;ona"
  ]
  node [
    id 152
    label "ulega&#263;"
  ]
  node [
    id 153
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 154
    label "pa&#324;stwo"
  ]
  node [
    id 155
    label "ulegni&#281;cie"
  ]
  node [
    id 156
    label "menopauza"
  ]
  node [
    id 157
    label "&#322;ono"
  ]
  node [
    id 158
    label "a"
  ]
  node [
    id 159
    label "Arabia"
  ]
  node [
    id 160
    label "saudyjski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 159
    target 160
  ]
]
