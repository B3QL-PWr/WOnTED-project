graph [
  maxDegree 27
  minDegree 1
  meanDegree 3.878787878787879
  density 0.023651145602365115
  graphCliqueNumber 18
  node [
    id 0
    label "instytucja"
    origin "text"
  ]
  node [
    id 1
    label "zarz&#261;dzaj&#261;ca"
    origin "text"
  ]
  node [
    id 2
    label "podstawa"
    origin "text"
  ]
  node [
    id 3
    label "wsparcie"
    origin "text"
  ]
  node [
    id 4
    label "wsp&#243;lnota"
    origin "text"
  ]
  node [
    id 5
    label "wydzia&#322;"
    origin "text"
  ]
  node [
    id 6
    label "szkolenie"
    origin "text"
  ]
  node [
    id 7
    label "informacja"
    origin "text"
  ]
  node [
    id 8
    label "promocja"
    origin "text"
  ]
  node [
    id 9
    label "afiliowa&#263;"
  ]
  node [
    id 10
    label "osoba_prawna"
  ]
  node [
    id 11
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 12
    label "urz&#261;d"
  ]
  node [
    id 13
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 14
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 15
    label "establishment"
  ]
  node [
    id 16
    label "standard"
  ]
  node [
    id 17
    label "organizacja"
  ]
  node [
    id 18
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 19
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 20
    label "zamykanie"
  ]
  node [
    id 21
    label "zamyka&#263;"
  ]
  node [
    id 22
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 23
    label "poj&#281;cie"
  ]
  node [
    id 24
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 25
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 26
    label "Fundusze_Unijne"
  ]
  node [
    id 27
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 28
    label "biuro"
  ]
  node [
    id 29
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 30
    label "podstawowy"
  ]
  node [
    id 31
    label "strategia"
  ]
  node [
    id 32
    label "pot&#281;ga"
  ]
  node [
    id 33
    label "zasadzenie"
  ]
  node [
    id 34
    label "przedmiot"
  ]
  node [
    id 35
    label "za&#322;o&#380;enie"
  ]
  node [
    id 36
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 37
    label "&#347;ciana"
  ]
  node [
    id 38
    label "documentation"
  ]
  node [
    id 39
    label "dzieci&#281;ctwo"
  ]
  node [
    id 40
    label "pomys&#322;"
  ]
  node [
    id 41
    label "bok"
  ]
  node [
    id 42
    label "d&#243;&#322;"
  ]
  node [
    id 43
    label "punkt_odniesienia"
  ]
  node [
    id 44
    label "column"
  ]
  node [
    id 45
    label "zasadzi&#263;"
  ]
  node [
    id 46
    label "background"
  ]
  node [
    id 47
    label "comfort"
  ]
  node [
    id 48
    label "u&#322;atwienie"
  ]
  node [
    id 49
    label "doch&#243;d"
  ]
  node [
    id 50
    label "oparcie"
  ]
  node [
    id 51
    label "telefon_zaufania"
  ]
  node [
    id 52
    label "dar"
  ]
  node [
    id 53
    label "zapomoga"
  ]
  node [
    id 54
    label "pocieszenie"
  ]
  node [
    id 55
    label "darowizna"
  ]
  node [
    id 56
    label "pomoc"
  ]
  node [
    id 57
    label "&#347;rodek"
  ]
  node [
    id 58
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 59
    label "support"
  ]
  node [
    id 60
    label "Skandynawia"
  ]
  node [
    id 61
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 62
    label "partnership"
  ]
  node [
    id 63
    label "zwi&#261;zek"
  ]
  node [
    id 64
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 65
    label "Walencja"
  ]
  node [
    id 66
    label "society"
  ]
  node [
    id 67
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 68
    label "zwi&#261;za&#263;"
  ]
  node [
    id 69
    label "bratnia_dusza"
  ]
  node [
    id 70
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 71
    label "marriage"
  ]
  node [
    id 72
    label "zwi&#261;zanie"
  ]
  node [
    id 73
    label "Ba&#322;kany"
  ]
  node [
    id 74
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 75
    label "wi&#261;zanie"
  ]
  node [
    id 76
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 77
    label "podobie&#324;stwo"
  ]
  node [
    id 78
    label "podsekcja"
  ]
  node [
    id 79
    label "whole"
  ]
  node [
    id 80
    label "relation"
  ]
  node [
    id 81
    label "politechnika"
  ]
  node [
    id 82
    label "katedra"
  ]
  node [
    id 83
    label "uniwersytet"
  ]
  node [
    id 84
    label "jednostka_organizacyjna"
  ]
  node [
    id 85
    label "insourcing"
  ]
  node [
    id 86
    label "ministerstwo"
  ]
  node [
    id 87
    label "miejsce_pracy"
  ]
  node [
    id 88
    label "dzia&#322;"
  ]
  node [
    id 89
    label "zapoznawanie"
  ]
  node [
    id 90
    label "seria"
  ]
  node [
    id 91
    label "course"
  ]
  node [
    id 92
    label "training"
  ]
  node [
    id 93
    label "zaj&#281;cia"
  ]
  node [
    id 94
    label "Lira"
  ]
  node [
    id 95
    label "pomaganie"
  ]
  node [
    id 96
    label "o&#347;wiecanie"
  ]
  node [
    id 97
    label "kliker"
  ]
  node [
    id 98
    label "pouczenie"
  ]
  node [
    id 99
    label "nauka"
  ]
  node [
    id 100
    label "doj&#347;cie"
  ]
  node [
    id 101
    label "doj&#347;&#263;"
  ]
  node [
    id 102
    label "powzi&#261;&#263;"
  ]
  node [
    id 103
    label "wiedza"
  ]
  node [
    id 104
    label "sygna&#322;"
  ]
  node [
    id 105
    label "obiegni&#281;cie"
  ]
  node [
    id 106
    label "obieganie"
  ]
  node [
    id 107
    label "obiec"
  ]
  node [
    id 108
    label "dane"
  ]
  node [
    id 109
    label "obiega&#263;"
  ]
  node [
    id 110
    label "punkt"
  ]
  node [
    id 111
    label "publikacja"
  ]
  node [
    id 112
    label "powzi&#281;cie"
  ]
  node [
    id 113
    label "nominacja"
  ]
  node [
    id 114
    label "sprzeda&#380;"
  ]
  node [
    id 115
    label "zamiana"
  ]
  node [
    id 116
    label "graduacja"
  ]
  node [
    id 117
    label "&#347;wiadectwo"
  ]
  node [
    id 118
    label "gradation"
  ]
  node [
    id 119
    label "brief"
  ]
  node [
    id 120
    label "uzyska&#263;"
  ]
  node [
    id 121
    label "promotion"
  ]
  node [
    id 122
    label "promowa&#263;"
  ]
  node [
    id 123
    label "klasa"
  ]
  node [
    id 124
    label "akcja"
  ]
  node [
    id 125
    label "wypromowa&#263;"
  ]
  node [
    id 126
    label "warcaby"
  ]
  node [
    id 127
    label "popularyzacja"
  ]
  node [
    id 128
    label "bran&#380;a"
  ]
  node [
    id 129
    label "impreza"
  ]
  node [
    id 130
    label "decyzja"
  ]
  node [
    id 131
    label "okazja"
  ]
  node [
    id 132
    label "commencement"
  ]
  node [
    id 133
    label "udzieli&#263;"
  ]
  node [
    id 134
    label "szachy"
  ]
  node [
    id 135
    label "damka"
  ]
  node [
    id 136
    label "i"
  ]
  node [
    id 137
    label "program"
  ]
  node [
    id 138
    label "operacyjny"
  ]
  node [
    id 139
    label "techniczny"
  ]
  node [
    id 140
    label "2004"
  ]
  node [
    id 141
    label "2006"
  ]
  node [
    id 142
    label "mi&#281;dzynarodowy"
  ]
  node [
    id 143
    label "relacja"
  ]
  node [
    id 144
    label "skarbowy"
  ]
  node [
    id 145
    label "minister"
  ]
  node [
    id 146
    label "gospodarka"
  ]
  node [
    id 147
    label "praca"
  ]
  node [
    id 148
    label "prezes"
  ]
  node [
    id 149
    label "rada"
  ]
  node [
    id 150
    label "J"
  ]
  node [
    id 151
    label "piechota"
  ]
  node [
    id 152
    label "rozporz&#261;dzi&#263;"
  ]
  node [
    id 153
    label "zeszyt"
  ]
  node [
    id 154
    label "dzie&#324;"
  ]
  node [
    id 155
    label "11"
  ]
  node [
    id 156
    label "czerwiec"
  ]
  node [
    id 157
    label "rok"
  ]
  node [
    id 158
    label "wyspa"
  ]
  node [
    id 159
    label "sprawa"
  ]
  node [
    id 160
    label "szczeg&#243;&#322;owy"
  ]
  node [
    id 161
    label "zakres"
  ]
  node [
    id 162
    label "dzia&#322;a&#263;"
  ]
  node [
    id 163
    label "dziennik"
  ]
  node [
    id 164
    label "u"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 28
    target 142
  ]
  edge [
    source 28
    target 143
  ]
  edge [
    source 28
    target 144
  ]
  edge [
    source 56
    target 137
  ]
  edge [
    source 56
    target 138
  ]
  edge [
    source 56
    target 139
  ]
  edge [
    source 56
    target 140
  ]
  edge [
    source 56
    target 141
  ]
  edge [
    source 136
    target 145
  ]
  edge [
    source 136
    target 146
  ]
  edge [
    source 136
    target 147
  ]
  edge [
    source 136
    target 152
  ]
  edge [
    source 136
    target 148
  ]
  edge [
    source 136
    target 149
  ]
  edge [
    source 136
    target 153
  ]
  edge [
    source 136
    target 154
  ]
  edge [
    source 136
    target 155
  ]
  edge [
    source 136
    target 156
  ]
  edge [
    source 136
    target 140
  ]
  edge [
    source 136
    target 157
  ]
  edge [
    source 136
    target 158
  ]
  edge [
    source 136
    target 159
  ]
  edge [
    source 136
    target 160
  ]
  edge [
    source 136
    target 161
  ]
  edge [
    source 136
    target 162
  ]
  edge [
    source 137
    target 138
  ]
  edge [
    source 137
    target 139
  ]
  edge [
    source 137
    target 140
  ]
  edge [
    source 137
    target 141
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 138
    target 140
  ]
  edge [
    source 138
    target 141
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 139
    target 141
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 140
    target 152
  ]
  edge [
    source 140
    target 148
  ]
  edge [
    source 140
    target 149
  ]
  edge [
    source 140
    target 145
  ]
  edge [
    source 140
    target 153
  ]
  edge [
    source 140
    target 154
  ]
  edge [
    source 140
    target 155
  ]
  edge [
    source 140
    target 156
  ]
  edge [
    source 140
    target 157
  ]
  edge [
    source 140
    target 158
  ]
  edge [
    source 140
    target 159
  ]
  edge [
    source 140
    target 160
  ]
  edge [
    source 140
    target 161
  ]
  edge [
    source 140
    target 162
  ]
  edge [
    source 140
    target 146
  ]
  edge [
    source 140
    target 147
  ]
  edge [
    source 142
    target 143
  ]
  edge [
    source 142
    target 144
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 145
    target 146
  ]
  edge [
    source 145
    target 147
  ]
  edge [
    source 145
    target 148
  ]
  edge [
    source 145
    target 149
  ]
  edge [
    source 145
    target 152
  ]
  edge [
    source 145
    target 153
  ]
  edge [
    source 145
    target 154
  ]
  edge [
    source 145
    target 155
  ]
  edge [
    source 145
    target 156
  ]
  edge [
    source 145
    target 157
  ]
  edge [
    source 145
    target 158
  ]
  edge [
    source 145
    target 159
  ]
  edge [
    source 145
    target 160
  ]
  edge [
    source 145
    target 161
  ]
  edge [
    source 145
    target 162
  ]
  edge [
    source 145
    target 145
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 146
    target 152
  ]
  edge [
    source 146
    target 148
  ]
  edge [
    source 146
    target 149
  ]
  edge [
    source 146
    target 153
  ]
  edge [
    source 146
    target 154
  ]
  edge [
    source 146
    target 155
  ]
  edge [
    source 146
    target 156
  ]
  edge [
    source 146
    target 157
  ]
  edge [
    source 146
    target 158
  ]
  edge [
    source 146
    target 159
  ]
  edge [
    source 146
    target 160
  ]
  edge [
    source 146
    target 161
  ]
  edge [
    source 146
    target 162
  ]
  edge [
    source 147
    target 152
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 147
    target 149
  ]
  edge [
    source 147
    target 153
  ]
  edge [
    source 147
    target 154
  ]
  edge [
    source 147
    target 155
  ]
  edge [
    source 147
    target 156
  ]
  edge [
    source 147
    target 157
  ]
  edge [
    source 147
    target 158
  ]
  edge [
    source 147
    target 159
  ]
  edge [
    source 147
    target 160
  ]
  edge [
    source 147
    target 161
  ]
  edge [
    source 147
    target 162
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 148
    target 152
  ]
  edge [
    source 148
    target 153
  ]
  edge [
    source 148
    target 154
  ]
  edge [
    source 148
    target 155
  ]
  edge [
    source 148
    target 156
  ]
  edge [
    source 148
    target 157
  ]
  edge [
    source 148
    target 158
  ]
  edge [
    source 148
    target 159
  ]
  edge [
    source 148
    target 160
  ]
  edge [
    source 148
    target 161
  ]
  edge [
    source 148
    target 162
  ]
  edge [
    source 149
    target 152
  ]
  edge [
    source 149
    target 153
  ]
  edge [
    source 149
    target 154
  ]
  edge [
    source 149
    target 155
  ]
  edge [
    source 149
    target 156
  ]
  edge [
    source 149
    target 157
  ]
  edge [
    source 149
    target 158
  ]
  edge [
    source 149
    target 159
  ]
  edge [
    source 149
    target 160
  ]
  edge [
    source 149
    target 161
  ]
  edge [
    source 149
    target 162
  ]
  edge [
    source 150
    target 151
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 152
    target 154
  ]
  edge [
    source 152
    target 155
  ]
  edge [
    source 152
    target 156
  ]
  edge [
    source 152
    target 157
  ]
  edge [
    source 152
    target 158
  ]
  edge [
    source 152
    target 159
  ]
  edge [
    source 152
    target 160
  ]
  edge [
    source 152
    target 161
  ]
  edge [
    source 152
    target 162
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 153
    target 155
  ]
  edge [
    source 153
    target 156
  ]
  edge [
    source 153
    target 157
  ]
  edge [
    source 153
    target 158
  ]
  edge [
    source 153
    target 159
  ]
  edge [
    source 153
    target 160
  ]
  edge [
    source 153
    target 161
  ]
  edge [
    source 153
    target 162
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 154
    target 156
  ]
  edge [
    source 154
    target 157
  ]
  edge [
    source 154
    target 158
  ]
  edge [
    source 154
    target 159
  ]
  edge [
    source 154
    target 160
  ]
  edge [
    source 154
    target 161
  ]
  edge [
    source 154
    target 162
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 155
    target 157
  ]
  edge [
    source 155
    target 158
  ]
  edge [
    source 155
    target 159
  ]
  edge [
    source 155
    target 160
  ]
  edge [
    source 155
    target 161
  ]
  edge [
    source 155
    target 162
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 156
    target 158
  ]
  edge [
    source 156
    target 159
  ]
  edge [
    source 156
    target 160
  ]
  edge [
    source 156
    target 161
  ]
  edge [
    source 156
    target 162
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 157
    target 159
  ]
  edge [
    source 157
    target 160
  ]
  edge [
    source 157
    target 161
  ]
  edge [
    source 157
    target 162
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 158
    target 160
  ]
  edge [
    source 158
    target 161
  ]
  edge [
    source 158
    target 162
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 159
    target 161
  ]
  edge [
    source 159
    target 162
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 160
    target 162
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 163
    target 164
  ]
]
