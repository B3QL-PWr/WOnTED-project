graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.97979797979798
  density 0.020202020202020204
  graphCliqueNumber 3
  node [
    id 0
    label "narazi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wykop"
    origin "text"
  ]
  node [
    id 2
    label "dobre"
    origin "text"
  ]
  node [
    id 3
    label "imi&#281;"
    origin "text"
  ]
  node [
    id 4
    label "sklep"
    origin "text"
  ]
  node [
    id 5
    label "sprzeda&#263;"
    origin "text"
  ]
  node [
    id 6
    label "samsunga"
    origin "text"
  ]
  node [
    id 7
    label "galaxy"
    origin "text"
  ]
  node [
    id 8
    label "krzywo"
    origin "text"
  ]
  node [
    id 9
    label "wklei&#263;"
    origin "text"
  ]
  node [
    id 10
    label "szk&#322;o"
    origin "text"
  ]
  node [
    id 11
    label "aparat"
    origin "text"
  ]
  node [
    id 12
    label "odwa&#322;"
  ]
  node [
    id 13
    label "chody"
  ]
  node [
    id 14
    label "grodzisko"
  ]
  node [
    id 15
    label "budowa"
  ]
  node [
    id 16
    label "kopniak"
  ]
  node [
    id 17
    label "wyrobisko"
  ]
  node [
    id 18
    label "zrzutowy"
  ]
  node [
    id 19
    label "szaniec"
  ]
  node [
    id 20
    label "odk&#322;ad"
  ]
  node [
    id 21
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 22
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 23
    label "reputacja"
  ]
  node [
    id 24
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 25
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 26
    label "patron"
  ]
  node [
    id 27
    label "nazwa_w&#322;asna"
  ]
  node [
    id 28
    label "deklinacja"
  ]
  node [
    id 29
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 30
    label "imiennictwo"
  ]
  node [
    id 31
    label "wezwanie"
  ]
  node [
    id 32
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 33
    label "personalia"
  ]
  node [
    id 34
    label "term"
  ]
  node [
    id 35
    label "leksem"
  ]
  node [
    id 36
    label "wielko&#347;&#263;"
  ]
  node [
    id 37
    label "stoisko"
  ]
  node [
    id 38
    label "sk&#322;ad"
  ]
  node [
    id 39
    label "firma"
  ]
  node [
    id 40
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 41
    label "witryna"
  ]
  node [
    id 42
    label "obiekt_handlowy"
  ]
  node [
    id 43
    label "zaplecze"
  ]
  node [
    id 44
    label "p&#243;&#322;ka"
  ]
  node [
    id 45
    label "zhandlowa&#263;"
  ]
  node [
    id 46
    label "odda&#263;"
  ]
  node [
    id 47
    label "zach&#281;ci&#263;"
  ]
  node [
    id 48
    label "give_birth"
  ]
  node [
    id 49
    label "zdradzi&#263;"
  ]
  node [
    id 50
    label "op&#281;dzi&#263;"
  ]
  node [
    id 51
    label "sell"
  ]
  node [
    id 52
    label "niemile"
  ]
  node [
    id 53
    label "znacz&#261;co"
  ]
  node [
    id 54
    label "nie&#380;yczliwie"
  ]
  node [
    id 55
    label "&#378;le"
  ]
  node [
    id 56
    label "krzywy"
  ]
  node [
    id 57
    label "paste"
  ]
  node [
    id 58
    label "wgra&#263;"
  ]
  node [
    id 59
    label "przyklei&#263;"
  ]
  node [
    id 60
    label "krajalno&#347;&#263;"
  ]
  node [
    id 61
    label "szklarstwo"
  ]
  node [
    id 62
    label "alkohol"
  ]
  node [
    id 63
    label "zeszklenie_si&#281;"
  ]
  node [
    id 64
    label "naczynie"
  ]
  node [
    id 65
    label "kawa&#322;ek"
  ]
  node [
    id 66
    label "zastawa"
  ]
  node [
    id 67
    label "zeszklenie"
  ]
  node [
    id 68
    label "lufka"
  ]
  node [
    id 69
    label "substancja"
  ]
  node [
    id 70
    label "ciemnia_optyczna"
  ]
  node [
    id 71
    label "przyrz&#261;d"
  ]
  node [
    id 72
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 73
    label "aparatownia"
  ]
  node [
    id 74
    label "orygina&#322;"
  ]
  node [
    id 75
    label "zi&#243;&#322;ko"
  ]
  node [
    id 76
    label "miech"
  ]
  node [
    id 77
    label "device"
  ]
  node [
    id 78
    label "w&#322;adza"
  ]
  node [
    id 79
    label "wy&#347;wietlacz"
  ]
  node [
    id 80
    label "dzwonienie"
  ]
  node [
    id 81
    label "zadzwoni&#263;"
  ]
  node [
    id 82
    label "dzwoni&#263;"
  ]
  node [
    id 83
    label "mikrotelefon"
  ]
  node [
    id 84
    label "wyzwalacz"
  ]
  node [
    id 85
    label "zesp&#243;&#322;"
  ]
  node [
    id 86
    label "dekielek"
  ]
  node [
    id 87
    label "celownik"
  ]
  node [
    id 88
    label "spust"
  ]
  node [
    id 89
    label "odleg&#322;o&#347;&#263;_hiperfokalna"
  ]
  node [
    id 90
    label "facet"
  ]
  node [
    id 91
    label "lampa_b&#322;yskowa"
  ]
  node [
    id 92
    label "mat&#243;wka"
  ]
  node [
    id 93
    label "urz&#261;dzenie"
  ]
  node [
    id 94
    label "migawka"
  ]
  node [
    id 95
    label "obiektyw"
  ]
  node [
    id 96
    label "Samsunga"
  ]
  node [
    id 97
    label "Galaxy"
  ]
  node [
    id 98
    label "S8"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 98
  ]
  edge [
    source 97
    target 98
  ]
]
