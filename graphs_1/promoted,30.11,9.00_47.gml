graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.1839080459770117
  density 0.025394279604383854
  graphCliqueNumber 5
  node [
    id 0
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 1
    label "holenderski"
    origin "text"
  ]
  node [
    id 2
    label "poinformowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rosja"
    origin "text"
  ]
  node [
    id 4
    label "opracowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zakazany"
    origin "text"
  ]
  node [
    id 6
    label "pocisk"
    origin "text"
  ]
  node [
    id 7
    label "samosteruj&#261;cy"
    origin "text"
  ]
  node [
    id 8
    label "zgodnie"
    origin "text"
  ]
  node [
    id 9
    label "list"
    origin "text"
  ]
  node [
    id 10
    label "sporz&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "przez"
    origin "text"
  ]
  node [
    id 12
    label "minister"
    origin "text"
  ]
  node [
    id 13
    label "stefek"
    origin "text"
  ]
  node [
    id 14
    label "blocka"
    origin "text"
  ]
  node [
    id 15
    label "ank"
    origin "text"
  ]
  node [
    id 16
    label "bijleveld"
    origin "text"
  ]
  node [
    id 17
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 18
    label "kategoria"
  ]
  node [
    id 19
    label "egzekutywa"
  ]
  node [
    id 20
    label "gabinet_cieni"
  ]
  node [
    id 21
    label "gromada"
  ]
  node [
    id 22
    label "premier"
  ]
  node [
    id 23
    label "Londyn"
  ]
  node [
    id 24
    label "Konsulat"
  ]
  node [
    id 25
    label "uporz&#261;dkowanie"
  ]
  node [
    id 26
    label "jednostka_systematyczna"
  ]
  node [
    id 27
    label "szpaler"
  ]
  node [
    id 28
    label "przybli&#380;enie"
  ]
  node [
    id 29
    label "tract"
  ]
  node [
    id 30
    label "number"
  ]
  node [
    id 31
    label "lon&#380;a"
  ]
  node [
    id 32
    label "w&#322;adza"
  ]
  node [
    id 33
    label "instytucja"
  ]
  node [
    id 34
    label "klasa"
  ]
  node [
    id 35
    label "niderlandzki"
  ]
  node [
    id 36
    label "j&#281;zyk_niderlandzki"
  ]
  node [
    id 37
    label "europejski"
  ]
  node [
    id 38
    label "holendersko"
  ]
  node [
    id 39
    label "po_holendersku"
  ]
  node [
    id 40
    label "zakomunikowa&#263;"
  ]
  node [
    id 41
    label "inform"
  ]
  node [
    id 42
    label "invent"
  ]
  node [
    id 43
    label "przygotowa&#263;"
  ]
  node [
    id 44
    label "niedopuszczalnie"
  ]
  node [
    id 45
    label "niezgodny"
  ]
  node [
    id 46
    label "g&#322;owica"
  ]
  node [
    id 47
    label "kulka"
  ]
  node [
    id 48
    label "amunicja"
  ]
  node [
    id 49
    label "przenie&#347;&#263;"
  ]
  node [
    id 50
    label "przenoszenie"
  ]
  node [
    id 51
    label "trafianie"
  ]
  node [
    id 52
    label "trafia&#263;"
  ]
  node [
    id 53
    label "przenosi&#263;"
  ]
  node [
    id 54
    label "trafienie"
  ]
  node [
    id 55
    label "&#322;adunek_bojowy"
  ]
  node [
    id 56
    label "trafi&#263;"
  ]
  node [
    id 57
    label "bro&#324;"
  ]
  node [
    id 58
    label "prochownia"
  ]
  node [
    id 59
    label "rdze&#324;"
  ]
  node [
    id 60
    label "przeniesienie"
  ]
  node [
    id 61
    label "jednakowo"
  ]
  node [
    id 62
    label "spokojnie"
  ]
  node [
    id 63
    label "zgodny"
  ]
  node [
    id 64
    label "dobrze"
  ]
  node [
    id 65
    label "zbie&#380;nie"
  ]
  node [
    id 66
    label "li&#347;&#263;"
  ]
  node [
    id 67
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 68
    label "poczta"
  ]
  node [
    id 69
    label "epistolografia"
  ]
  node [
    id 70
    label "przesy&#322;ka"
  ]
  node [
    id 71
    label "poczta_elektroniczna"
  ]
  node [
    id 72
    label "znaczek_pocztowy"
  ]
  node [
    id 73
    label "stworzy&#263;"
  ]
  node [
    id 74
    label "wytworzy&#263;"
  ]
  node [
    id 75
    label "draw"
  ]
  node [
    id 76
    label "oprawi&#263;"
  ]
  node [
    id 77
    label "zrobi&#263;"
  ]
  node [
    id 78
    label "podzieli&#263;"
  ]
  node [
    id 79
    label "Goebbels"
  ]
  node [
    id 80
    label "Sto&#322;ypin"
  ]
  node [
    id 81
    label "dostojnik"
  ]
  node [
    id 82
    label "Stefek"
  ]
  node [
    id 83
    label "Blocka"
  ]
  node [
    id 84
    label "i"
  ]
  node [
    id 85
    label "Ank"
  ]
  node [
    id 86
    label "Bijleveld"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 84
  ]
  edge [
    source 82
    target 85
  ]
  edge [
    source 82
    target 86
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 85
  ]
  edge [
    source 83
    target 86
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 86
  ]
  edge [
    source 85
    target 86
  ]
]
