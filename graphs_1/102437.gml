graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 2
  node [
    id 0
    label "m&#322;ody"
    origin "text"
  ]
  node [
    id 1
    label "media"
    origin "text"
  ]
  node [
    id 2
    label "premier"
    origin "text"
  ]
  node [
    id 3
    label "raport"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "nie&#380;onaty"
  ]
  node [
    id 6
    label "wczesny"
  ]
  node [
    id 7
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 8
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 9
    label "charakterystyczny"
  ]
  node [
    id 10
    label "nowo&#380;eniec"
  ]
  node [
    id 11
    label "m&#261;&#380;"
  ]
  node [
    id 12
    label "m&#322;odo"
  ]
  node [
    id 13
    label "nowy"
  ]
  node [
    id 14
    label "przekazior"
  ]
  node [
    id 15
    label "mass-media"
  ]
  node [
    id 16
    label "uzbrajanie"
  ]
  node [
    id 17
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 18
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 19
    label "medium"
  ]
  node [
    id 20
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 21
    label "Jelcyn"
  ]
  node [
    id 22
    label "Sto&#322;ypin"
  ]
  node [
    id 23
    label "dostojnik"
  ]
  node [
    id 24
    label "Chruszczow"
  ]
  node [
    id 25
    label "Miko&#322;ajczyk"
  ]
  node [
    id 26
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 27
    label "Bismarck"
  ]
  node [
    id 28
    label "rz&#261;d"
  ]
  node [
    id 29
    label "zwierzchnik"
  ]
  node [
    id 30
    label "raport_Beveridge'a"
  ]
  node [
    id 31
    label "relacja"
  ]
  node [
    id 32
    label "raport_Fischlera"
  ]
  node [
    id 33
    label "statement"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
]
