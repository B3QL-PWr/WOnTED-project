graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.76
  density 0.07333333333333333
  graphCliqueNumber 3
  node [
    id 0
    label "ludwik"
    origin "text"
  ]
  node [
    id 1
    label "gmina"
    origin "text"
  ]
  node [
    id 2
    label "teresin"
    origin "text"
  ]
  node [
    id 3
    label "moneta"
  ]
  node [
    id 4
    label "antyk"
  ]
  node [
    id 5
    label "rada_gminy"
  ]
  node [
    id 6
    label "Wielka_Wie&#347;"
  ]
  node [
    id 7
    label "jednostka_administracyjna"
  ]
  node [
    id 8
    label "powiat"
  ]
  node [
    id 9
    label "Dobro&#324;"
  ]
  node [
    id 10
    label "Karlsbad"
  ]
  node [
    id 11
    label "urz&#261;d"
  ]
  node [
    id 12
    label "Biskupice"
  ]
  node [
    id 13
    label "radny"
  ]
  node [
    id 14
    label "organizacja_religijna"
  ]
  node [
    id 15
    label "pierwszy"
  ]
  node [
    id 16
    label "wojna"
  ]
  node [
    id 17
    label "&#347;wiatowy"
  ]
  node [
    id 18
    label "Tadeusz"
  ]
  node [
    id 19
    label "Sroczy&#324;ski"
  ]
  node [
    id 20
    label "Seroki"
  ]
  node [
    id 21
    label "parcela"
  ]
  node [
    id 22
    label "kolonia"
  ]
  node [
    id 23
    label "podlesie"
  ]
  node [
    id 24
    label "drugi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
]
