graph [
  maxDegree 28
  minDegree 1
  meanDegree 2
  density 0.020833333333333332
  graphCliqueNumber 2
  node [
    id 0
    label "firma"
    origin "text"
  ]
  node [
    id 1
    label "maja"
    origin "text"
  ]
  node [
    id 2
    label "pocz&#261;tkowo"
    origin "text"
  ]
  node [
    id 3
    label "p&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pe&#322;ny"
    origin "text"
  ]
  node [
    id 5
    label "rachunek"
    origin "text"
  ]
  node [
    id 6
    label "pr&#261;d"
    origin "text"
  ]
  node [
    id 7
    label "dopiero"
    origin "text"
  ]
  node [
    id 8
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 9
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 10
    label "samodzielnie"
    origin "text"
  ]
  node [
    id 11
    label "ubiega&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "rekompensata"
    origin "text"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "MAC"
  ]
  node [
    id 16
    label "Hortex"
  ]
  node [
    id 17
    label "reengineering"
  ]
  node [
    id 18
    label "nazwa_w&#322;asna"
  ]
  node [
    id 19
    label "podmiot_gospodarczy"
  ]
  node [
    id 20
    label "Google"
  ]
  node [
    id 21
    label "zaufanie"
  ]
  node [
    id 22
    label "biurowiec"
  ]
  node [
    id 23
    label "interes"
  ]
  node [
    id 24
    label "zasoby_ludzkie"
  ]
  node [
    id 25
    label "networking"
  ]
  node [
    id 26
    label "paczkarnia"
  ]
  node [
    id 27
    label "Canon"
  ]
  node [
    id 28
    label "HP"
  ]
  node [
    id 29
    label "Baltona"
  ]
  node [
    id 30
    label "Pewex"
  ]
  node [
    id 31
    label "MAN_SE"
  ]
  node [
    id 32
    label "Apeks"
  ]
  node [
    id 33
    label "zasoby"
  ]
  node [
    id 34
    label "Orbis"
  ]
  node [
    id 35
    label "miejsce_pracy"
  ]
  node [
    id 36
    label "siedziba"
  ]
  node [
    id 37
    label "Spo&#322;em"
  ]
  node [
    id 38
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 39
    label "Orlen"
  ]
  node [
    id 40
    label "klasa"
  ]
  node [
    id 41
    label "wedyzm"
  ]
  node [
    id 42
    label "energia"
  ]
  node [
    id 43
    label "buddyzm"
  ]
  node [
    id 44
    label "pocz&#261;tkowy"
  ]
  node [
    id 45
    label "dzieci&#281;co"
  ]
  node [
    id 46
    label "zrazu"
  ]
  node [
    id 47
    label "buli&#263;"
  ]
  node [
    id 48
    label "osi&#261;ga&#263;"
  ]
  node [
    id 49
    label "give"
  ]
  node [
    id 50
    label "wydawa&#263;"
  ]
  node [
    id 51
    label "pay"
  ]
  node [
    id 52
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 53
    label "nieograniczony"
  ]
  node [
    id 54
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 55
    label "kompletny"
  ]
  node [
    id 56
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 57
    label "r&#243;wny"
  ]
  node [
    id 58
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 59
    label "bezwzgl&#281;dny"
  ]
  node [
    id 60
    label "zupe&#322;ny"
  ]
  node [
    id 61
    label "ca&#322;y"
  ]
  node [
    id 62
    label "satysfakcja"
  ]
  node [
    id 63
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 64
    label "pe&#322;no"
  ]
  node [
    id 65
    label "wype&#322;nienie"
  ]
  node [
    id 66
    label "otwarty"
  ]
  node [
    id 67
    label "spis"
  ]
  node [
    id 68
    label "wytw&#243;r"
  ]
  node [
    id 69
    label "check"
  ]
  node [
    id 70
    label "count"
  ]
  node [
    id 71
    label "mienie"
  ]
  node [
    id 72
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 73
    label "system"
  ]
  node [
    id 74
    label "przep&#322;yw"
  ]
  node [
    id 75
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 76
    label "ideologia"
  ]
  node [
    id 77
    label "ruch"
  ]
  node [
    id 78
    label "dreszcz"
  ]
  node [
    id 79
    label "praktyka"
  ]
  node [
    id 80
    label "metoda"
  ]
  node [
    id 81
    label "apparent_motion"
  ]
  node [
    id 82
    label "electricity"
  ]
  node [
    id 83
    label "zjawisko"
  ]
  node [
    id 84
    label "przyp&#322;yw"
  ]
  node [
    id 85
    label "p&#243;&#378;ny"
  ]
  node [
    id 86
    label "samodzielny"
  ]
  node [
    id 87
    label "autonomiczny"
  ]
  node [
    id 88
    label "osobno"
  ]
  node [
    id 89
    label "niepodlegle"
  ]
  node [
    id 90
    label "robi&#263;"
  ]
  node [
    id 91
    label "anticipate"
  ]
  node [
    id 92
    label "return"
  ]
  node [
    id 93
    label "zap&#322;ata"
  ]
  node [
    id 94
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 95
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 96
    label "refund"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
]
