graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9838709677419355
  density 0.016129032258064516
  graphCliqueNumber 2
  node [
    id 0
    label "zako&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "pi&#261;ta"
    origin "text"
  ]
  node [
    id 3
    label "edycja"
    origin "text"
  ]
  node [
    id 4
    label "konferencja"
    origin "text"
  ]
  node [
    id 5
    label "media"
    origin "text"
  ]
  node [
    id 6
    label "transition"
    origin "text"
  ]
  node [
    id 7
    label "jeden"
    origin "text"
  ]
  node [
    id 8
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 9
    label "impreza"
    origin "text"
  ]
  node [
    id 10
    label "po&#347;wi&#281;cony"
    origin "text"
  ]
  node [
    id 11
    label "przemian"
    origin "text"
  ]
  node [
    id 12
    label "pejza&#380;"
    origin "text"
  ]
  node [
    id 13
    label "medialny"
    origin "text"
  ]
  node [
    id 14
    label "has&#322;o"
    origin "text"
  ]
  node [
    id 15
    label "tegoroczny"
    origin "text"
  ]
  node [
    id 16
    label "ods&#322;ona"
    origin "text"
  ]
  node [
    id 17
    label "era"
    origin "text"
  ]
  node [
    id 18
    label "cyfrowy"
    origin "text"
  ]
  node [
    id 19
    label "communicate"
  ]
  node [
    id 20
    label "cause"
  ]
  node [
    id 21
    label "zrezygnowa&#263;"
  ]
  node [
    id 22
    label "wytworzy&#263;"
  ]
  node [
    id 23
    label "przesta&#263;"
  ]
  node [
    id 24
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 25
    label "dispose"
  ]
  node [
    id 26
    label "zrobi&#263;"
  ]
  node [
    id 27
    label "godzina"
  ]
  node [
    id 28
    label "impression"
  ]
  node [
    id 29
    label "odmiana"
  ]
  node [
    id 30
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 31
    label "notification"
  ]
  node [
    id 32
    label "cykl"
  ]
  node [
    id 33
    label "zmiana"
  ]
  node [
    id 34
    label "produkcja"
  ]
  node [
    id 35
    label "egzemplarz"
  ]
  node [
    id 36
    label "konferencyjka"
  ]
  node [
    id 37
    label "Poczdam"
  ]
  node [
    id 38
    label "conference"
  ]
  node [
    id 39
    label "spotkanie"
  ]
  node [
    id 40
    label "grusza_pospolita"
  ]
  node [
    id 41
    label "Ja&#322;ta"
  ]
  node [
    id 42
    label "przekazior"
  ]
  node [
    id 43
    label "mass-media"
  ]
  node [
    id 44
    label "uzbrajanie"
  ]
  node [
    id 45
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 46
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 47
    label "medium"
  ]
  node [
    id 48
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 49
    label "kieliszek"
  ]
  node [
    id 50
    label "shot"
  ]
  node [
    id 51
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 52
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 53
    label "jaki&#347;"
  ]
  node [
    id 54
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 55
    label "jednolicie"
  ]
  node [
    id 56
    label "w&#243;dka"
  ]
  node [
    id 57
    label "ten"
  ]
  node [
    id 58
    label "ujednolicenie"
  ]
  node [
    id 59
    label "jednakowy"
  ]
  node [
    id 60
    label "silny"
  ]
  node [
    id 61
    label "wa&#380;nie"
  ]
  node [
    id 62
    label "eksponowany"
  ]
  node [
    id 63
    label "istotnie"
  ]
  node [
    id 64
    label "znaczny"
  ]
  node [
    id 65
    label "dobry"
  ]
  node [
    id 66
    label "wynios&#322;y"
  ]
  node [
    id 67
    label "dono&#347;ny"
  ]
  node [
    id 68
    label "party"
  ]
  node [
    id 69
    label "rozrywka"
  ]
  node [
    id 70
    label "przyj&#281;cie"
  ]
  node [
    id 71
    label "okazja"
  ]
  node [
    id 72
    label "impra"
  ]
  node [
    id 73
    label "oddany"
  ]
  node [
    id 74
    label "zaj&#347;cie"
  ]
  node [
    id 75
    label "malarstwo"
  ]
  node [
    id 76
    label "widok"
  ]
  node [
    id 77
    label "obraz"
  ]
  node [
    id 78
    label "przestrze&#324;"
  ]
  node [
    id 79
    label "dzie&#322;o"
  ]
  node [
    id 80
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 81
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 82
    label "human_body"
  ]
  node [
    id 83
    label "teren"
  ]
  node [
    id 84
    label "zjawisko"
  ]
  node [
    id 85
    label "landscape"
  ]
  node [
    id 86
    label "nieprawdziwy"
  ]
  node [
    id 87
    label "medialnie"
  ]
  node [
    id 88
    label "popularny"
  ]
  node [
    id 89
    label "&#347;rodkowy"
  ]
  node [
    id 90
    label "ochrona"
  ]
  node [
    id 91
    label "sztuka_dla_sztuki"
  ]
  node [
    id 92
    label "dost&#281;p"
  ]
  node [
    id 93
    label "przes&#322;anie"
  ]
  node [
    id 94
    label "definicja"
  ]
  node [
    id 95
    label "idea"
  ]
  node [
    id 96
    label "sygna&#322;"
  ]
  node [
    id 97
    label "kwalifikator"
  ]
  node [
    id 98
    label "wyra&#380;enie"
  ]
  node [
    id 99
    label "artyku&#322;"
  ]
  node [
    id 100
    label "powiedzenie"
  ]
  node [
    id 101
    label "artyku&#322;_has&#322;owy"
  ]
  node [
    id 102
    label "guide_word"
  ]
  node [
    id 103
    label "rozwi&#261;zanie"
  ]
  node [
    id 104
    label "solicitation"
  ]
  node [
    id 105
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 106
    label "kod"
  ]
  node [
    id 107
    label "pozycja"
  ]
  node [
    id 108
    label "leksem"
  ]
  node [
    id 109
    label "tegorocznie"
  ]
  node [
    id 110
    label "bie&#380;&#261;cy"
  ]
  node [
    id 111
    label "przedstawienie"
  ]
  node [
    id 112
    label "czas"
  ]
  node [
    id 113
    label "eon"
  ]
  node [
    id 114
    label "schy&#322;ek"
  ]
  node [
    id 115
    label "okres"
  ]
  node [
    id 116
    label "jednostka_geologiczna"
  ]
  node [
    id 117
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 118
    label "Zeitgeist"
  ]
  node [
    id 119
    label "dzieje"
  ]
  node [
    id 120
    label "cyfrowo"
  ]
  node [
    id 121
    label "sygna&#322;_cyfrowy"
  ]
  node [
    id 122
    label "elektroniczny"
  ]
  node [
    id 123
    label "cyfryzacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 123
  ]
]
