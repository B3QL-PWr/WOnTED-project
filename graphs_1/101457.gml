graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.6470588235294117
  density 0.10294117647058823
  graphCliqueNumber 3
  node [
    id 0
    label "gordon"
    origin "text"
  ]
  node [
    id 1
    label "willis"
    origin "text"
  ]
  node [
    id 2
    label "seter"
  ]
  node [
    id 3
    label "Willis"
  ]
  node [
    id 4
    label "nowy"
  ]
  node [
    id 5
    label "Jork"
  ]
  node [
    id 6
    label "ojciec"
  ]
  node [
    id 7
    label "chrzestny"
  ]
  node [
    id 8
    label "iii"
  ]
  node [
    id 9
    label "Woody"
  ]
  node [
    id 10
    label "allen"
  ]
  node [
    id 11
    label "Francis"
  ]
  node [
    id 12
    label "ford"
  ]
  node [
    id 13
    label "Coppola"
  ]
  node [
    id 14
    label "Alan"
  ]
  node [
    id 15
    label "J"
  ]
  node [
    id 16
    label "Pakula"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
]
