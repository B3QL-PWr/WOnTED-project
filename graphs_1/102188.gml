graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.9666666666666666
  density 0.03333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "nazajutrz"
    origin "text"
  ]
  node [
    id 1
    label "kola"
    origin "text"
  ]
  node [
    id 2
    label "po&#322;udnie"
    origin "text"
  ]
  node [
    id 3
    label "wej&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "izba"
    origin "text"
  ]
  node [
    id 5
    label "jambro&#380;y"
    origin "text"
  ]
  node [
    id 6
    label "blisko"
  ]
  node [
    id 7
    label "jutrzejszy"
  ]
  node [
    id 8
    label "nap&#243;j_gazowany"
  ]
  node [
    id 9
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 10
    label "kola_zaostrzona"
  ]
  node [
    id 11
    label "ro&#347;lina"
  ]
  node [
    id 12
    label "egzotyk"
  ]
  node [
    id 13
    label "krzew_kokainowy"
  ]
  node [
    id 14
    label "&#347;lazowate"
  ]
  node [
    id 15
    label "zatwarowate"
  ]
  node [
    id 16
    label "drzewo"
  ]
  node [
    id 17
    label "dwunasta"
  ]
  node [
    id 18
    label "obszar"
  ]
  node [
    id 19
    label "Ziemia"
  ]
  node [
    id 20
    label "godzina"
  ]
  node [
    id 21
    label "strona_&#347;wiata"
  ]
  node [
    id 22
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 23
    label "&#347;rodek"
  ]
  node [
    id 24
    label "pora"
  ]
  node [
    id 25
    label "dzie&#324;"
  ]
  node [
    id 26
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 27
    label "get"
  ]
  node [
    id 28
    label "pozna&#263;"
  ]
  node [
    id 29
    label "spotka&#263;"
  ]
  node [
    id 30
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 31
    label "przenikn&#261;&#263;"
  ]
  node [
    id 32
    label "submit"
  ]
  node [
    id 33
    label "nast&#261;pi&#263;"
  ]
  node [
    id 34
    label "zag&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 35
    label "ascend"
  ]
  node [
    id 36
    label "intervene"
  ]
  node [
    id 37
    label "zacz&#261;&#263;"
  ]
  node [
    id 38
    label "catch"
  ]
  node [
    id 39
    label "doj&#347;&#263;"
  ]
  node [
    id 40
    label "wnikn&#261;&#263;"
  ]
  node [
    id 41
    label "przekroczy&#263;"
  ]
  node [
    id 42
    label "zaistnie&#263;"
  ]
  node [
    id 43
    label "sta&#263;_si&#281;"
  ]
  node [
    id 44
    label "wzi&#261;&#263;"
  ]
  node [
    id 45
    label "drapn&#261;&#263;_si&#281;"
  ]
  node [
    id 46
    label "z&#322;oi&#263;"
  ]
  node [
    id 47
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 48
    label "w&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 49
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 50
    label "move"
  ]
  node [
    id 51
    label "become"
  ]
  node [
    id 52
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 53
    label "pok&#243;j"
  ]
  node [
    id 54
    label "parlament"
  ]
  node [
    id 55
    label "zwi&#261;zek"
  ]
  node [
    id 56
    label "NIK"
  ]
  node [
    id 57
    label "urz&#261;d"
  ]
  node [
    id 58
    label "organ"
  ]
  node [
    id 59
    label "pomieszczenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
]
