graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.2969187675070026
  density 0.0021466530537448624
  graphCliqueNumber 6
  node [
    id 0
    label "panel"
    origin "text"
  ]
  node [
    id 1
    label "po&#347;wi&#281;cony"
    origin "text"
  ]
  node [
    id 2
    label "ocena"
    origin "text"
  ]
  node [
    id 3
    label "relacja"
    origin "text"
  ]
  node [
    id 4
    label "reporterski"
    origin "text"
  ]
  node [
    id 5
    label "gor&#261;cy"
    origin "text"
  ]
  node [
    id 6
    label "okres"
    origin "text"
  ]
  node [
    id 7
    label "konflikt"
    origin "text"
  ]
  node [
    id 8
    label "krzy&#380;"
    origin "text"
  ]
  node [
    id 9
    label "sierpie&#324;"
    origin "text"
  ]
  node [
    id 10
    label "janina"
    origin "text"
  ]
  node [
    id 11
    label "jankowska"
    origin "text"
  ]
  node [
    id 12
    label "wzi&#261;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 14
    label "&#347;wietlik"
    origin "text"
  ]
  node [
    id 15
    label "cmwp"
    origin "text"
  ]
  node [
    id 16
    label "rzeczpospolita"
    origin "text"
  ]
  node [
    id 17
    label "piotr"
    origin "text"
  ]
  node [
    id 18
    label "zaremba"
    origin "text"
  ]
  node [
    id 19
    label "jachowicz"
    origin "text"
  ]
  node [
    id 20
    label "radio"
    origin "text"
  ]
  node [
    id 21
    label "wnet"
    origin "text"
  ]
  node [
    id 22
    label "oraz"
    origin "text"
  ]
  node [
    id 23
    label "zaj&#261;c"
    origin "text"
  ]
  node [
    id 24
    label "tygodnik"
    origin "text"
  ]
  node [
    id 25
    label "przekr&#243;j"
    origin "text"
  ]
  node [
    id 26
    label "zdanie"
    origin "text"
  ]
  node [
    id 27
    label "ostatni"
    origin "text"
  ]
  node [
    id 28
    label "raz"
    origin "text"
  ]
  node [
    id 29
    label "kolejny"
    origin "text"
  ]
  node [
    id 30
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 31
    label "rytualny"
    origin "text"
  ]
  node [
    id 32
    label "wojna"
    origin "text"
  ]
  node [
    id 33
    label "&#347;wiatopogl&#261;dowy"
    origin "text"
  ]
  node [
    id 34
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 35
    label "pos&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 36
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 37
    label "by&#263;"
    origin "text"
  ]
  node [
    id 38
    label "strona"
    origin "text"
  ]
  node [
    id 39
    label "krakowskie"
    origin "text"
  ]
  node [
    id 40
    label "przedmie&#347;cie"
    origin "text"
  ]
  node [
    id 41
    label "wiceszef"
    origin "text"
  ]
  node [
    id 42
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 43
    label "media"
    origin "text"
  ]
  node [
    id 44
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 45
    label "b&#243;l"
    origin "text"
  ]
  node [
    id 46
    label "przewidywalny"
    origin "text"
  ]
  node [
    id 47
    label "wystarczy&#263;"
    origin "text"
  ]
  node [
    id 48
    label "winieta"
    origin "text"
  ]
  node [
    id 49
    label "przeczyta&#263;"
    origin "text"
  ]
  node [
    id 50
    label "ten"
    origin "text"
  ]
  node [
    id 51
    label "opinia"
    origin "text"
  ]
  node [
    id 52
    label "zgodzi&#263;"
    origin "text"
  ]
  node [
    id 53
    label "si&#281;"
    origin "text"
  ]
  node [
    id 54
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 55
    label "tym"
    origin "text"
  ]
  node [
    id 56
    label "problem"
    origin "text"
  ]
  node [
    id 57
    label "wskazywa&#263;"
    origin "text"
  ]
  node [
    id 58
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 59
    label "wr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 60
    label "bardzo"
    origin "text"
  ]
  node [
    id 61
    label "elementarny"
    origin "text"
  ]
  node [
    id 62
    label "zasada"
    origin "text"
  ]
  node [
    id 63
    label "dziennikarstwo"
    origin "text"
  ]
  node [
    id 64
    label "informacja"
    origin "text"
  ]
  node [
    id 65
    label "komentarz"
    origin "text"
  ]
  node [
    id 66
    label "emocjonuj&#261;cy"
    origin "text"
  ]
  node [
    id 67
    label "okaza&#322;y"
    origin "text"
  ]
  node [
    id 68
    label "druga"
    origin "text"
  ]
  node [
    id 69
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 70
    label "sesja"
    origin "text"
  ]
  node [
    id 71
    label "dyskusyjny"
    origin "text"
  ]
  node [
    id 72
    label "polityka"
    origin "text"
  ]
  node [
    id 73
    label "redakcyjny"
    origin "text"
  ]
  node [
    id 74
    label "redaktor"
    origin "text"
  ]
  node [
    id 75
    label "naczelny"
    origin "text"
  ]
  node [
    id 76
    label "newsweek"
    origin "text"
  ]
  node [
    id 77
    label "wojciech"
    origin "text"
  ]
  node [
    id 78
    label "maziarski"
    origin "text"
  ]
  node [
    id 79
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 80
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 81
    label "taki"
    origin "text"
  ]
  node [
    id 82
    label "rola"
    origin "text"
  ]
  node [
    id 83
    label "udost&#281;pnienie"
    origin "text"
  ]
  node [
    id 84
    label "&#322;am"
    origin "text"
  ]
  node [
    id 85
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 86
    label "dyskusja"
    origin "text"
  ]
  node [
    id 87
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 88
    label "sprawa"
    origin "text"
  ]
  node [
    id 89
    label "tak"
    origin "text"
  ]
  node [
    id 90
    label "przypadek"
    origin "text"
  ]
  node [
    id 91
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 92
    label "inny"
    origin "text"
  ]
  node [
    id 93
    label "wesprze&#263;"
    origin "text"
  ]
  node [
    id 94
    label "szel&#261;g"
    origin "text"
  ]
  node [
    id 95
    label "polsat"
    origin "text"
  ]
  node [
    id 96
    label "news"
    origin "text"
  ]
  node [
    id 97
    label "telewizja"
    origin "text"
  ]
  node [
    id 98
    label "informacyjny"
    origin "text"
  ]
  node [
    id 99
    label "zadanie"
    origin "text"
  ]
  node [
    id 100
    label "prosta"
    origin "text"
  ]
  node [
    id 101
    label "robi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 102
    label "oponowa&#263;"
    origin "text"
  ]
  node [
    id 103
    label "tomasz"
    origin "text"
  ]
  node [
    id 104
    label "sakiewicz"
    origin "text"
  ]
  node [
    id 105
    label "gazeta"
    origin "text"
  ]
  node [
    id 106
    label "polski"
    origin "text"
  ]
  node [
    id 107
    label "r&#243;&#380;ni&#263;"
    origin "text"
  ]
  node [
    id 108
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 109
    label "zobaczy&#263;"
    origin "text"
  ]
  node [
    id 110
    label "maina"
    origin "text"
  ]
  node [
    id 111
    label "dziennikarz"
    origin "text"
  ]
  node [
    id 112
    label "powinny"
    origin "text"
  ]
  node [
    id 113
    label "wyt&#322;umaczy&#263;"
    origin "text"
  ]
  node [
    id 114
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 115
    label "jak"
    origin "text"
  ]
  node [
    id 116
    label "pokazywa&#263;"
    origin "text"
  ]
  node [
    id 117
    label "aspekt"
    origin "text"
  ]
  node [
    id 118
    label "wskaza&#263;"
    origin "text"
  ]
  node [
    id 119
    label "bogumi&#322;"
    origin "text"
  ]
  node [
    id 120
    label "&#322;ozi&#324;ski"
    origin "text"
  ]
  node [
    id 121
    label "go&#347;cia"
    origin "text"
  ]
  node [
    id 122
    label "niedzielny"
    origin "text"
  ]
  node [
    id 123
    label "powinienby&#263;"
    origin "text"
  ]
  node [
    id 124
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 125
    label "dowiedzie&#263;"
    origin "text"
  ]
  node [
    id 126
    label "co&#347;"
    origin "text"
  ]
  node [
    id 127
    label "wra&#380;liwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 128
    label "uczestnik"
    origin "text"
  ]
  node [
    id 129
    label "sp&#243;r"
    origin "text"
  ]
  node [
    id 130
    label "tymczasem"
    origin "text"
  ]
  node [
    id 131
    label "materia&#322;"
    origin "text"
  ]
  node [
    id 132
    label "dziennikarski"
    origin "text"
  ]
  node [
    id 133
    label "zbyt"
    origin "text"
  ]
  node [
    id 134
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 135
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 136
    label "inwektywa"
    origin "text"
  ]
  node [
    id 137
    label "sonda&#380;"
  ]
  node [
    id 138
    label "coffer"
  ]
  node [
    id 139
    label "p&#322;yta"
  ]
  node [
    id 140
    label "konsola"
  ]
  node [
    id 141
    label "ok&#322;adzina"
  ]
  node [
    id 142
    label "opakowanie"
  ]
  node [
    id 143
    label "oddany"
  ]
  node [
    id 144
    label "sofcik"
  ]
  node [
    id 145
    label "appraisal"
  ]
  node [
    id 146
    label "decyzja"
  ]
  node [
    id 147
    label "pogl&#261;d"
  ]
  node [
    id 148
    label "kryterium"
  ]
  node [
    id 149
    label "wypowied&#378;"
  ]
  node [
    id 150
    label "message"
  ]
  node [
    id 151
    label "podzbi&#243;r"
  ]
  node [
    id 152
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 153
    label "ustosunkowywa&#263;"
  ]
  node [
    id 154
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 155
    label "bratnia_dusza"
  ]
  node [
    id 156
    label "zwi&#261;zanie"
  ]
  node [
    id 157
    label "ustosunkowanie"
  ]
  node [
    id 158
    label "ustosunkowywanie"
  ]
  node [
    id 159
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 160
    label "zwi&#261;za&#263;"
  ]
  node [
    id 161
    label "ustosunkowa&#263;"
  ]
  node [
    id 162
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 163
    label "korespondent"
  ]
  node [
    id 164
    label "marriage"
  ]
  node [
    id 165
    label "wi&#261;zanie"
  ]
  node [
    id 166
    label "trasa"
  ]
  node [
    id 167
    label "zwi&#261;zek"
  ]
  node [
    id 168
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 169
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 170
    label "sprawko"
  ]
  node [
    id 171
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 172
    label "charakterystyczny"
  ]
  node [
    id 173
    label "na_gor&#261;co"
  ]
  node [
    id 174
    label "szczery"
  ]
  node [
    id 175
    label "&#380;arki"
  ]
  node [
    id 176
    label "zdecydowany"
  ]
  node [
    id 177
    label "gor&#261;co"
  ]
  node [
    id 178
    label "rozpalenie_si&#281;"
  ]
  node [
    id 179
    label "g&#322;&#281;boki"
  ]
  node [
    id 180
    label "sensacyjny"
  ]
  node [
    id 181
    label "seksowny"
  ]
  node [
    id 182
    label "rozpalanie_si&#281;"
  ]
  node [
    id 183
    label "&#347;wie&#380;y"
  ]
  node [
    id 184
    label "ciep&#322;y"
  ]
  node [
    id 185
    label "stresogenny"
  ]
  node [
    id 186
    label "serdeczny"
  ]
  node [
    id 187
    label "paleogen"
  ]
  node [
    id 188
    label "spell"
  ]
  node [
    id 189
    label "czas"
  ]
  node [
    id 190
    label "period"
  ]
  node [
    id 191
    label "prekambr"
  ]
  node [
    id 192
    label "jura"
  ]
  node [
    id 193
    label "interstadia&#322;"
  ]
  node [
    id 194
    label "jednostka_geologiczna"
  ]
  node [
    id 195
    label "izochronizm"
  ]
  node [
    id 196
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 197
    label "okres_noachijski"
  ]
  node [
    id 198
    label "orosir"
  ]
  node [
    id 199
    label "kreda"
  ]
  node [
    id 200
    label "sten"
  ]
  node [
    id 201
    label "drugorz&#281;d"
  ]
  node [
    id 202
    label "semester"
  ]
  node [
    id 203
    label "trzeciorz&#281;d"
  ]
  node [
    id 204
    label "ton"
  ]
  node [
    id 205
    label "dzieje"
  ]
  node [
    id 206
    label "poprzednik"
  ]
  node [
    id 207
    label "ordowik"
  ]
  node [
    id 208
    label "karbon"
  ]
  node [
    id 209
    label "trias"
  ]
  node [
    id 210
    label "kalim"
  ]
  node [
    id 211
    label "stater"
  ]
  node [
    id 212
    label "era"
  ]
  node [
    id 213
    label "cykl"
  ]
  node [
    id 214
    label "p&#243;&#322;okres"
  ]
  node [
    id 215
    label "czwartorz&#281;d"
  ]
  node [
    id 216
    label "pulsacja"
  ]
  node [
    id 217
    label "okres_amazo&#324;ski"
  ]
  node [
    id 218
    label "kambr"
  ]
  node [
    id 219
    label "Zeitgeist"
  ]
  node [
    id 220
    label "nast&#281;pnik"
  ]
  node [
    id 221
    label "kriogen"
  ]
  node [
    id 222
    label "glacja&#322;"
  ]
  node [
    id 223
    label "fala"
  ]
  node [
    id 224
    label "okres_czasu"
  ]
  node [
    id 225
    label "riak"
  ]
  node [
    id 226
    label "schy&#322;ek"
  ]
  node [
    id 227
    label "okres_hesperyjski"
  ]
  node [
    id 228
    label "sylur"
  ]
  node [
    id 229
    label "dewon"
  ]
  node [
    id 230
    label "ciota"
  ]
  node [
    id 231
    label "epoka"
  ]
  node [
    id 232
    label "pierwszorz&#281;d"
  ]
  node [
    id 233
    label "okres_halsztacki"
  ]
  node [
    id 234
    label "ektas"
  ]
  node [
    id 235
    label "condition"
  ]
  node [
    id 236
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 237
    label "rok_akademicki"
  ]
  node [
    id 238
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 239
    label "postglacja&#322;"
  ]
  node [
    id 240
    label "faza"
  ]
  node [
    id 241
    label "proces_fizjologiczny"
  ]
  node [
    id 242
    label "ediakar"
  ]
  node [
    id 243
    label "time_period"
  ]
  node [
    id 244
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 245
    label "perm"
  ]
  node [
    id 246
    label "rok_szkolny"
  ]
  node [
    id 247
    label "neogen"
  ]
  node [
    id 248
    label "sider"
  ]
  node [
    id 249
    label "flow"
  ]
  node [
    id 250
    label "podokres"
  ]
  node [
    id 251
    label "preglacja&#322;"
  ]
  node [
    id 252
    label "retoryka"
  ]
  node [
    id 253
    label "choroba_przyrodzona"
  ]
  node [
    id 254
    label "clash"
  ]
  node [
    id 255
    label "wydarzenie"
  ]
  node [
    id 256
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 257
    label "traverse"
  ]
  node [
    id 258
    label "gest"
  ]
  node [
    id 259
    label "cierpienie"
  ]
  node [
    id 260
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 261
    label "symbol"
  ]
  node [
    id 262
    label "order"
  ]
  node [
    id 263
    label "przedmiot"
  ]
  node [
    id 264
    label "d&#322;o&#324;"
  ]
  node [
    id 265
    label "biblizm"
  ]
  node [
    id 266
    label "kszta&#322;t"
  ]
  node [
    id 267
    label "kara_&#347;mierci"
  ]
  node [
    id 268
    label "Wniebowzi&#281;cie_Naj&#347;wi&#281;tszej_Maryi_Panny"
  ]
  node [
    id 269
    label "miesi&#261;c"
  ]
  node [
    id 270
    label "Sierpie&#324;"
  ]
  node [
    id 271
    label "wej&#347;&#263;"
  ]
  node [
    id 272
    label "get"
  ]
  node [
    id 273
    label "wzi&#281;cie"
  ]
  node [
    id 274
    label "wyrucha&#263;"
  ]
  node [
    id 275
    label "uciec"
  ]
  node [
    id 276
    label "ruszy&#263;"
  ]
  node [
    id 277
    label "wygra&#263;"
  ]
  node [
    id 278
    label "obj&#261;&#263;"
  ]
  node [
    id 279
    label "zacz&#261;&#263;"
  ]
  node [
    id 280
    label "wyciupcia&#263;"
  ]
  node [
    id 281
    label "World_Health_Organization"
  ]
  node [
    id 282
    label "skorzysta&#263;"
  ]
  node [
    id 283
    label "pokona&#263;"
  ]
  node [
    id 284
    label "poczyta&#263;"
  ]
  node [
    id 285
    label "poruszy&#263;"
  ]
  node [
    id 286
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 287
    label "take"
  ]
  node [
    id 288
    label "aim"
  ]
  node [
    id 289
    label "arise"
  ]
  node [
    id 290
    label "u&#380;y&#263;"
  ]
  node [
    id 291
    label "zaatakowa&#263;"
  ]
  node [
    id 292
    label "receive"
  ]
  node [
    id 293
    label "uda&#263;_si&#281;"
  ]
  node [
    id 294
    label "dosta&#263;"
  ]
  node [
    id 295
    label "otrzyma&#263;"
  ]
  node [
    id 296
    label "obskoczy&#263;"
  ]
  node [
    id 297
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 298
    label "zrobi&#263;"
  ]
  node [
    id 299
    label "bra&#263;"
  ]
  node [
    id 300
    label "nakaza&#263;"
  ]
  node [
    id 301
    label "chwyci&#263;"
  ]
  node [
    id 302
    label "przyj&#261;&#263;"
  ]
  node [
    id 303
    label "seize"
  ]
  node [
    id 304
    label "odziedziczy&#263;"
  ]
  node [
    id 305
    label "withdraw"
  ]
  node [
    id 306
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 307
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 308
    label "obecno&#347;&#263;"
  ]
  node [
    id 309
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 310
    label "kwota"
  ]
  node [
    id 311
    label "ilo&#347;&#263;"
  ]
  node [
    id 312
    label "sp&#322;awik"
  ]
  node [
    id 313
    label "ro&#347;lina_jednoroczna"
  ]
  node [
    id 314
    label "przyrz&#261;d"
  ]
  node [
    id 315
    label "chrz&#261;szcz"
  ]
  node [
    id 316
    label "&#347;wietlikowate"
  ]
  node [
    id 317
    label "sztuczne_&#378;r&#243;d&#322;o_&#347;wiat&#322;a"
  ]
  node [
    id 318
    label "zarazowate"
  ]
  node [
    id 319
    label "okno"
  ]
  node [
    id 320
    label "ro&#347;lina_paso&#380;ytnicza"
  ]
  node [
    id 321
    label "Buriacja"
  ]
  node [
    id 322
    label "Abchazja"
  ]
  node [
    id 323
    label "Inguszetia"
  ]
  node [
    id 324
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 325
    label "Nachiczewan"
  ]
  node [
    id 326
    label "Karaka&#322;pacja"
  ]
  node [
    id 327
    label "Jakucja"
  ]
  node [
    id 328
    label "Singapur"
  ]
  node [
    id 329
    label "Komi"
  ]
  node [
    id 330
    label "Karelia"
  ]
  node [
    id 331
    label "Tatarstan"
  ]
  node [
    id 332
    label "Chakasja"
  ]
  node [
    id 333
    label "Dagestan"
  ]
  node [
    id 334
    label "Mordowia"
  ]
  node [
    id 335
    label "Ka&#322;mucja"
  ]
  node [
    id 336
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 337
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 338
    label "Baszkiria"
  ]
  node [
    id 339
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 340
    label "Mari_El"
  ]
  node [
    id 341
    label "Ad&#380;aria"
  ]
  node [
    id 342
    label "Czuwaszja"
  ]
  node [
    id 343
    label "Tuwa"
  ]
  node [
    id 344
    label "Czeczenia"
  ]
  node [
    id 345
    label "Udmurcja"
  ]
  node [
    id 346
    label "pa&#324;stwo"
  ]
  node [
    id 347
    label "uk&#322;ad"
  ]
  node [
    id 348
    label "paj&#281;czarz"
  ]
  node [
    id 349
    label "fala_radiowa"
  ]
  node [
    id 350
    label "spot"
  ]
  node [
    id 351
    label "programowiec"
  ]
  node [
    id 352
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 353
    label "eliminator"
  ]
  node [
    id 354
    label "studio"
  ]
  node [
    id 355
    label "radiola"
  ]
  node [
    id 356
    label "redakcja"
  ]
  node [
    id 357
    label "odbieranie"
  ]
  node [
    id 358
    label "dyskryminator"
  ]
  node [
    id 359
    label "odbiera&#263;"
  ]
  node [
    id 360
    label "odbiornik"
  ]
  node [
    id 361
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 362
    label "stacja"
  ]
  node [
    id 363
    label "radiolinia"
  ]
  node [
    id 364
    label "radiofonia"
  ]
  node [
    id 365
    label "wpr&#281;dce"
  ]
  node [
    id 366
    label "blisko"
  ]
  node [
    id 367
    label "nied&#322;ugi"
  ]
  node [
    id 368
    label "skrom"
  ]
  node [
    id 369
    label "parkot"
  ]
  node [
    id 370
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 371
    label "omyk"
  ]
  node [
    id 372
    label "trzeszcze"
  ]
  node [
    id 373
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 374
    label "kicaj"
  ]
  node [
    id 375
    label "kopyra"
  ]
  node [
    id 376
    label "dziczyzna"
  ]
  node [
    id 377
    label "turzyca"
  ]
  node [
    id 378
    label "zaj&#261;cowate"
  ]
  node [
    id 379
    label "skok"
  ]
  node [
    id 380
    label "czasopismo"
  ]
  node [
    id 381
    label "krajobraz"
  ]
  node [
    id 382
    label "part"
  ]
  node [
    id 383
    label "scene"
  ]
  node [
    id 384
    label "zbi&#243;r"
  ]
  node [
    id 385
    label "mie&#263;_cz&#281;&#347;&#263;_wsp&#243;ln&#261;"
  ]
  node [
    id 386
    label "rysunek"
  ]
  node [
    id 387
    label "p&#322;aszczyzna"
  ]
  node [
    id 388
    label "attitude"
  ]
  node [
    id 389
    label "system"
  ]
  node [
    id 390
    label "przedstawienie"
  ]
  node [
    id 391
    label "fraza"
  ]
  node [
    id 392
    label "prison_term"
  ]
  node [
    id 393
    label "adjudication"
  ]
  node [
    id 394
    label "przekazanie"
  ]
  node [
    id 395
    label "pass"
  ]
  node [
    id 396
    label "wyra&#380;enie"
  ]
  node [
    id 397
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 398
    label "wypowiedzenie"
  ]
  node [
    id 399
    label "konektyw"
  ]
  node [
    id 400
    label "zaliczenie"
  ]
  node [
    id 401
    label "stanowisko"
  ]
  node [
    id 402
    label "powierzenie"
  ]
  node [
    id 403
    label "antylogizm"
  ]
  node [
    id 404
    label "zmuszenie"
  ]
  node [
    id 405
    label "szko&#322;a"
  ]
  node [
    id 406
    label "istota_&#380;ywa"
  ]
  node [
    id 407
    label "najgorszy"
  ]
  node [
    id 408
    label "aktualny"
  ]
  node [
    id 409
    label "ostatnio"
  ]
  node [
    id 410
    label "niedawno"
  ]
  node [
    id 411
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 412
    label "sko&#324;czony"
  ]
  node [
    id 413
    label "poprzedni"
  ]
  node [
    id 414
    label "pozosta&#322;y"
  ]
  node [
    id 415
    label "w&#261;tpliwy"
  ]
  node [
    id 416
    label "chwila"
  ]
  node [
    id 417
    label "uderzenie"
  ]
  node [
    id 418
    label "cios"
  ]
  node [
    id 419
    label "time"
  ]
  node [
    id 420
    label "nast&#281;pnie"
  ]
  node [
    id 421
    label "kt&#243;ry&#347;"
  ]
  node [
    id 422
    label "kolejno"
  ]
  node [
    id 423
    label "nastopny"
  ]
  node [
    id 424
    label "reserve"
  ]
  node [
    id 425
    label "przej&#347;&#263;"
  ]
  node [
    id 426
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 427
    label "rytualnie"
  ]
  node [
    id 428
    label "obrz&#281;dowy"
  ]
  node [
    id 429
    label "tradycyjny"
  ]
  node [
    id 430
    label "ceremonialny"
  ]
  node [
    id 431
    label "zimna_wojna"
  ]
  node [
    id 432
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 433
    label "angaria"
  ]
  node [
    id 434
    label "wr&#243;g"
  ]
  node [
    id 435
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 436
    label "walka"
  ]
  node [
    id 437
    label "war"
  ]
  node [
    id 438
    label "wojna_stuletnia"
  ]
  node [
    id 439
    label "burza"
  ]
  node [
    id 440
    label "zbrodnia_wojenna"
  ]
  node [
    id 441
    label "gra_w_karty"
  ]
  node [
    id 442
    label "&#347;wiatopogl&#261;dowo"
  ]
  node [
    id 443
    label "dok&#322;adnie"
  ]
  node [
    id 444
    label "przyda&#263;_si&#281;"
  ]
  node [
    id 445
    label "asymilowa&#263;"
  ]
  node [
    id 446
    label "wapniak"
  ]
  node [
    id 447
    label "dwun&#243;g"
  ]
  node [
    id 448
    label "polifag"
  ]
  node [
    id 449
    label "wz&#243;r"
  ]
  node [
    id 450
    label "profanum"
  ]
  node [
    id 451
    label "hominid"
  ]
  node [
    id 452
    label "homo_sapiens"
  ]
  node [
    id 453
    label "nasada"
  ]
  node [
    id 454
    label "podw&#322;adny"
  ]
  node [
    id 455
    label "ludzko&#347;&#263;"
  ]
  node [
    id 456
    label "os&#322;abianie"
  ]
  node [
    id 457
    label "mikrokosmos"
  ]
  node [
    id 458
    label "portrecista"
  ]
  node [
    id 459
    label "duch"
  ]
  node [
    id 460
    label "oddzia&#322;ywanie"
  ]
  node [
    id 461
    label "g&#322;owa"
  ]
  node [
    id 462
    label "asymilowanie"
  ]
  node [
    id 463
    label "osoba"
  ]
  node [
    id 464
    label "os&#322;abia&#263;"
  ]
  node [
    id 465
    label "figura"
  ]
  node [
    id 466
    label "Adam"
  ]
  node [
    id 467
    label "senior"
  ]
  node [
    id 468
    label "antropochoria"
  ]
  node [
    id 469
    label "posta&#263;"
  ]
  node [
    id 470
    label "si&#281;ga&#263;"
  ]
  node [
    id 471
    label "trwa&#263;"
  ]
  node [
    id 472
    label "stan"
  ]
  node [
    id 473
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 474
    label "stand"
  ]
  node [
    id 475
    label "mie&#263;_miejsce"
  ]
  node [
    id 476
    label "uczestniczy&#263;"
  ]
  node [
    id 477
    label "chodzi&#263;"
  ]
  node [
    id 478
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 479
    label "equal"
  ]
  node [
    id 480
    label "skr&#281;canie"
  ]
  node [
    id 481
    label "voice"
  ]
  node [
    id 482
    label "forma"
  ]
  node [
    id 483
    label "internet"
  ]
  node [
    id 484
    label "skr&#281;ci&#263;"
  ]
  node [
    id 485
    label "kartka"
  ]
  node [
    id 486
    label "orientowa&#263;"
  ]
  node [
    id 487
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 488
    label "powierzchnia"
  ]
  node [
    id 489
    label "plik"
  ]
  node [
    id 490
    label "bok"
  ]
  node [
    id 491
    label "pagina"
  ]
  node [
    id 492
    label "orientowanie"
  ]
  node [
    id 493
    label "fragment"
  ]
  node [
    id 494
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 495
    label "s&#261;d"
  ]
  node [
    id 496
    label "skr&#281;ca&#263;"
  ]
  node [
    id 497
    label "g&#243;ra"
  ]
  node [
    id 498
    label "serwis_internetowy"
  ]
  node [
    id 499
    label "orientacja"
  ]
  node [
    id 500
    label "linia"
  ]
  node [
    id 501
    label "skr&#281;cenie"
  ]
  node [
    id 502
    label "layout"
  ]
  node [
    id 503
    label "zorientowa&#263;"
  ]
  node [
    id 504
    label "zorientowanie"
  ]
  node [
    id 505
    label "obiekt"
  ]
  node [
    id 506
    label "podmiot"
  ]
  node [
    id 507
    label "ty&#322;"
  ]
  node [
    id 508
    label "logowanie"
  ]
  node [
    id 509
    label "adres_internetowy"
  ]
  node [
    id 510
    label "uj&#281;cie"
  ]
  node [
    id 511
    label "prz&#243;d"
  ]
  node [
    id 512
    label "obrze&#380;e"
  ]
  node [
    id 513
    label "zast&#281;pca"
  ]
  node [
    id 514
    label "majority"
  ]
  node [
    id 515
    label "przekazior"
  ]
  node [
    id 516
    label "mass-media"
  ]
  node [
    id 517
    label "uzbrajanie"
  ]
  node [
    id 518
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 519
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 520
    label "medium"
  ]
  node [
    id 521
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 522
    label "partnerka"
  ]
  node [
    id 523
    label "m&#281;ka_Pa&#324;ska"
  ]
  node [
    id 524
    label "doznanie"
  ]
  node [
    id 525
    label "tkliwo&#347;&#263;"
  ]
  node [
    id 526
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 527
    label "toleration"
  ]
  node [
    id 528
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 529
    label "irradiacja"
  ]
  node [
    id 530
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 531
    label "prze&#380;ycie"
  ]
  node [
    id 532
    label "drzazga"
  ]
  node [
    id 533
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 534
    label "cier&#324;"
  ]
  node [
    id 535
    label "mo&#380;liwy"
  ]
  node [
    id 536
    label "stabilny"
  ]
  node [
    id 537
    label "przewidywalnie"
  ]
  node [
    id 538
    label "szacunkowy"
  ]
  node [
    id 539
    label "spowodowa&#263;"
  ]
  node [
    id 540
    label "stan&#261;&#263;"
  ]
  node [
    id 541
    label "suffice"
  ]
  node [
    id 542
    label "zaspokoi&#263;"
  ]
  node [
    id 543
    label "fotografia"
  ]
  node [
    id 544
    label "pasek"
  ]
  node [
    id 545
    label "op&#322;ata_winietowa"
  ]
  node [
    id 546
    label "ilustracja"
  ]
  node [
    id 547
    label "naklejka"
  ]
  node [
    id 548
    label "efekt"
  ]
  node [
    id 549
    label "nadruk"
  ]
  node [
    id 550
    label "pozna&#263;"
  ]
  node [
    id 551
    label "wywr&#243;&#380;y&#263;"
  ]
  node [
    id 552
    label "przetworzy&#263;"
  ]
  node [
    id 553
    label "read"
  ]
  node [
    id 554
    label "zaobserwowa&#263;"
  ]
  node [
    id 555
    label "odczyta&#263;"
  ]
  node [
    id 556
    label "okre&#347;lony"
  ]
  node [
    id 557
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 558
    label "dokument"
  ]
  node [
    id 559
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 560
    label "reputacja"
  ]
  node [
    id 561
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 562
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 563
    label "cecha"
  ]
  node [
    id 564
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 565
    label "ekspertyza"
  ]
  node [
    id 566
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 567
    label "wielko&#347;&#263;"
  ]
  node [
    id 568
    label "zatrudni&#263;"
  ]
  node [
    id 569
    label "zgodzenie"
  ]
  node [
    id 570
    label "zgadza&#263;"
  ]
  node [
    id 571
    label "pomoc"
  ]
  node [
    id 572
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 573
    label "perceive"
  ]
  node [
    id 574
    label "reagowa&#263;"
  ]
  node [
    id 575
    label "male&#263;"
  ]
  node [
    id 576
    label "zmale&#263;"
  ]
  node [
    id 577
    label "spotka&#263;"
  ]
  node [
    id 578
    label "go_steady"
  ]
  node [
    id 579
    label "dostrzega&#263;"
  ]
  node [
    id 580
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 581
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 582
    label "ogl&#261;da&#263;"
  ]
  node [
    id 583
    label "os&#261;dza&#263;"
  ]
  node [
    id 584
    label "aprobowa&#263;"
  ]
  node [
    id 585
    label "punkt_widzenia"
  ]
  node [
    id 586
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 587
    label "wzrok"
  ]
  node [
    id 588
    label "postrzega&#263;"
  ]
  node [
    id 589
    label "notice"
  ]
  node [
    id 590
    label "trudno&#347;&#263;"
  ]
  node [
    id 591
    label "ambaras"
  ]
  node [
    id 592
    label "problemat"
  ]
  node [
    id 593
    label "pierepa&#322;ka"
  ]
  node [
    id 594
    label "obstruction"
  ]
  node [
    id 595
    label "problematyka"
  ]
  node [
    id 596
    label "jajko_Kolumba"
  ]
  node [
    id 597
    label "subiekcja"
  ]
  node [
    id 598
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 599
    label "warto&#347;&#263;"
  ]
  node [
    id 600
    label "set"
  ]
  node [
    id 601
    label "represent"
  ]
  node [
    id 602
    label "indicate"
  ]
  node [
    id 603
    label "wyraz"
  ]
  node [
    id 604
    label "wybiera&#263;"
  ]
  node [
    id 605
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 606
    label "podawa&#263;"
  ]
  node [
    id 607
    label "signify"
  ]
  node [
    id 608
    label "podkre&#347;la&#263;"
  ]
  node [
    id 609
    label "wynik"
  ]
  node [
    id 610
    label "wyj&#347;cie"
  ]
  node [
    id 611
    label "spe&#322;nienie"
  ]
  node [
    id 612
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 613
    label "po&#322;o&#380;na"
  ]
  node [
    id 614
    label "przestanie"
  ]
  node [
    id 615
    label "marc&#243;wka"
  ]
  node [
    id 616
    label "usuni&#281;cie"
  ]
  node [
    id 617
    label "uniewa&#380;nienie"
  ]
  node [
    id 618
    label "pomys&#322;"
  ]
  node [
    id 619
    label "birth"
  ]
  node [
    id 620
    label "wymy&#347;lenie"
  ]
  node [
    id 621
    label "po&#322;&#243;g"
  ]
  node [
    id 622
    label "szok_poporodowy"
  ]
  node [
    id 623
    label "event"
  ]
  node [
    id 624
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 625
    label "spos&#243;b"
  ]
  node [
    id 626
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 627
    label "dula"
  ]
  node [
    id 628
    label "render"
  ]
  node [
    id 629
    label "return"
  ]
  node [
    id 630
    label "zosta&#263;"
  ]
  node [
    id 631
    label "przyj&#347;&#263;"
  ]
  node [
    id 632
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 633
    label "revive"
  ]
  node [
    id 634
    label "pojedna&#263;_si&#281;"
  ]
  node [
    id 635
    label "podj&#261;&#263;"
  ]
  node [
    id 636
    label "nawi&#261;za&#263;"
  ]
  node [
    id 637
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 638
    label "przyby&#263;"
  ]
  node [
    id 639
    label "recur"
  ]
  node [
    id 640
    label "w_chuj"
  ]
  node [
    id 641
    label "pocz&#261;tkowy"
  ]
  node [
    id 642
    label "pierwszy"
  ]
  node [
    id 643
    label "podstawowy"
  ]
  node [
    id 644
    label "elementarnie"
  ]
  node [
    id 645
    label "obserwacja"
  ]
  node [
    id 646
    label "moralno&#347;&#263;"
  ]
  node [
    id 647
    label "podstawa"
  ]
  node [
    id 648
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 649
    label "umowa"
  ]
  node [
    id 650
    label "dominion"
  ]
  node [
    id 651
    label "qualification"
  ]
  node [
    id 652
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 653
    label "opis"
  ]
  node [
    id 654
    label "regu&#322;a_Allena"
  ]
  node [
    id 655
    label "normalizacja"
  ]
  node [
    id 656
    label "regu&#322;a_Glogera"
  ]
  node [
    id 657
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 658
    label "standard"
  ]
  node [
    id 659
    label "base"
  ]
  node [
    id 660
    label "substancja"
  ]
  node [
    id 661
    label "prawid&#322;o"
  ]
  node [
    id 662
    label "prawo_Mendla"
  ]
  node [
    id 663
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 664
    label "criterion"
  ]
  node [
    id 665
    label "twierdzenie"
  ]
  node [
    id 666
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 667
    label "prawo"
  ]
  node [
    id 668
    label "occupation"
  ]
  node [
    id 669
    label "zasada_d'Alemberta"
  ]
  node [
    id 670
    label "nauka_humanistyczna"
  ]
  node [
    id 671
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 672
    label "medioznawca"
  ]
  node [
    id 673
    label "doj&#347;cie"
  ]
  node [
    id 674
    label "doj&#347;&#263;"
  ]
  node [
    id 675
    label "powzi&#261;&#263;"
  ]
  node [
    id 676
    label "wiedza"
  ]
  node [
    id 677
    label "sygna&#322;"
  ]
  node [
    id 678
    label "obiegni&#281;cie"
  ]
  node [
    id 679
    label "obieganie"
  ]
  node [
    id 680
    label "obiec"
  ]
  node [
    id 681
    label "dane"
  ]
  node [
    id 682
    label "obiega&#263;"
  ]
  node [
    id 683
    label "punkt"
  ]
  node [
    id 684
    label "publikacja"
  ]
  node [
    id 685
    label "powzi&#281;cie"
  ]
  node [
    id 686
    label "comment"
  ]
  node [
    id 687
    label "artyku&#322;"
  ]
  node [
    id 688
    label "gossip"
  ]
  node [
    id 689
    label "interpretacja"
  ]
  node [
    id 690
    label "audycja"
  ]
  node [
    id 691
    label "tekst"
  ]
  node [
    id 692
    label "emocjonuj&#261;co"
  ]
  node [
    id 693
    label "pe&#322;ny"
  ]
  node [
    id 694
    label "ciekawy"
  ]
  node [
    id 695
    label "bogaty"
  ]
  node [
    id 696
    label "okazale"
  ]
  node [
    id 697
    label "imponuj&#261;cy"
  ]
  node [
    id 698
    label "poka&#378;ny"
  ]
  node [
    id 699
    label "godzina"
  ]
  node [
    id 700
    label "whole"
  ]
  node [
    id 701
    label "Rzym_Zachodni"
  ]
  node [
    id 702
    label "element"
  ]
  node [
    id 703
    label "urz&#261;dzenie"
  ]
  node [
    id 704
    label "Rzym_Wschodni"
  ]
  node [
    id 705
    label "seria"
  ]
  node [
    id 706
    label "conference"
  ]
  node [
    id 707
    label "sesyjka"
  ]
  node [
    id 708
    label "spotkanie"
  ]
  node [
    id 709
    label "gie&#322;da_papier&#243;w_warto&#347;ciowych"
  ]
  node [
    id 710
    label "dzie&#324;_pracy"
  ]
  node [
    id 711
    label "konsylium"
  ]
  node [
    id 712
    label "egzamin"
  ]
  node [
    id 713
    label "dogrywka"
  ]
  node [
    id 714
    label "psychoterapia"
  ]
  node [
    id 715
    label "kontrowersyjnie"
  ]
  node [
    id 716
    label "dyskusyjnie"
  ]
  node [
    id 717
    label "policy"
  ]
  node [
    id 718
    label "dyplomacja"
  ]
  node [
    id 719
    label "Europejska_Polityka_S&#261;siedztwa"
  ]
  node [
    id 720
    label "metoda"
  ]
  node [
    id 721
    label "bran&#380;owiec"
  ]
  node [
    id 722
    label "wydawnictwo"
  ]
  node [
    id 723
    label "edytor"
  ]
  node [
    id 724
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 725
    label "nadrz&#281;dny"
  ]
  node [
    id 726
    label "jeneralny"
  ]
  node [
    id 727
    label "zawo&#322;any"
  ]
  node [
    id 728
    label "naczelnie"
  ]
  node [
    id 729
    label "g&#322;&#243;wny"
  ]
  node [
    id 730
    label "Michnik"
  ]
  node [
    id 731
    label "znany"
  ]
  node [
    id 732
    label "zwierzchnik"
  ]
  node [
    id 733
    label "powiedzie&#263;"
  ]
  node [
    id 734
    label "testify"
  ]
  node [
    id 735
    label "uzna&#263;"
  ]
  node [
    id 736
    label "oznajmi&#263;"
  ]
  node [
    id 737
    label "declare"
  ]
  node [
    id 738
    label "nijaki"
  ]
  node [
    id 739
    label "jaki&#347;"
  ]
  node [
    id 740
    label "pole"
  ]
  node [
    id 741
    label "znaczenie"
  ]
  node [
    id 742
    label "ziemia"
  ]
  node [
    id 743
    label "sk&#322;ad"
  ]
  node [
    id 744
    label "zastosowanie"
  ]
  node [
    id 745
    label "zreinterpretowa&#263;"
  ]
  node [
    id 746
    label "zreinterpretowanie"
  ]
  node [
    id 747
    label "function"
  ]
  node [
    id 748
    label "zagranie"
  ]
  node [
    id 749
    label "p&#322;osa"
  ]
  node [
    id 750
    label "cel"
  ]
  node [
    id 751
    label "reinterpretowanie"
  ]
  node [
    id 752
    label "wykonywa&#263;"
  ]
  node [
    id 753
    label "uprawi&#263;"
  ]
  node [
    id 754
    label "uprawienie"
  ]
  node [
    id 755
    label "gra&#263;"
  ]
  node [
    id 756
    label "radlina"
  ]
  node [
    id 757
    label "ustawi&#263;"
  ]
  node [
    id 758
    label "irygowa&#263;"
  ]
  node [
    id 759
    label "wrench"
  ]
  node [
    id 760
    label "irygowanie"
  ]
  node [
    id 761
    label "dialog"
  ]
  node [
    id 762
    label "zagon"
  ]
  node [
    id 763
    label "scenariusz"
  ]
  node [
    id 764
    label "zagra&#263;"
  ]
  node [
    id 765
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 766
    label "ustawienie"
  ]
  node [
    id 767
    label "czyn"
  ]
  node [
    id 768
    label "gospodarstwo"
  ]
  node [
    id 769
    label "reinterpretowa&#263;"
  ]
  node [
    id 770
    label "granie"
  ]
  node [
    id 771
    label "wykonywanie"
  ]
  node [
    id 772
    label "aktorstwo"
  ]
  node [
    id 773
    label "kostium"
  ]
  node [
    id 774
    label "umo&#380;liwienie"
  ]
  node [
    id 775
    label "kolumna"
  ]
  node [
    id 776
    label "r&#243;&#380;nie"
  ]
  node [
    id 777
    label "rozmowa"
  ]
  node [
    id 778
    label "sympozjon"
  ]
  node [
    id 779
    label "silny"
  ]
  node [
    id 780
    label "wa&#380;nie"
  ]
  node [
    id 781
    label "eksponowany"
  ]
  node [
    id 782
    label "istotnie"
  ]
  node [
    id 783
    label "znaczny"
  ]
  node [
    id 784
    label "dobry"
  ]
  node [
    id 785
    label "wynios&#322;y"
  ]
  node [
    id 786
    label "dono&#347;ny"
  ]
  node [
    id 787
    label "temat"
  ]
  node [
    id 788
    label "kognicja"
  ]
  node [
    id 789
    label "idea"
  ]
  node [
    id 790
    label "szczeg&#243;&#322;"
  ]
  node [
    id 791
    label "rzecz"
  ]
  node [
    id 792
    label "przes&#322;anka"
  ]
  node [
    id 793
    label "rozprawa"
  ]
  node [
    id 794
    label "object"
  ]
  node [
    id 795
    label "proposition"
  ]
  node [
    id 796
    label "pacjent"
  ]
  node [
    id 797
    label "kategoria_gramatyczna"
  ]
  node [
    id 798
    label "schorzenie"
  ]
  node [
    id 799
    label "przeznaczenie"
  ]
  node [
    id 800
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 801
    label "happening"
  ]
  node [
    id 802
    label "przyk&#322;ad"
  ]
  node [
    id 803
    label "inaczej"
  ]
  node [
    id 804
    label "inszy"
  ]
  node [
    id 805
    label "osobno"
  ]
  node [
    id 806
    label "oprze&#263;"
  ]
  node [
    id 807
    label "help"
  ]
  node [
    id 808
    label "support"
  ]
  node [
    id 809
    label "lean"
  ]
  node [
    id 810
    label "u&#322;atwi&#263;"
  ]
  node [
    id 811
    label "pocieszy&#263;"
  ]
  node [
    id 812
    label "farthing"
  ]
  node [
    id 813
    label "moneta"
  ]
  node [
    id 814
    label "nowostka"
  ]
  node [
    id 815
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 816
    label "doniesienie"
  ]
  node [
    id 817
    label "nius"
  ]
  node [
    id 818
    label "Polsat"
  ]
  node [
    id 819
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 820
    label "technologia"
  ]
  node [
    id 821
    label "Interwizja"
  ]
  node [
    id 822
    label "BBC"
  ]
  node [
    id 823
    label "ekran"
  ]
  node [
    id 824
    label "instytucja"
  ]
  node [
    id 825
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 826
    label "telekomunikacja"
  ]
  node [
    id 827
    label "muza"
  ]
  node [
    id 828
    label "informacyjnie"
  ]
  node [
    id 829
    label "yield"
  ]
  node [
    id 830
    label "czynno&#347;&#263;"
  ]
  node [
    id 831
    label "przepisanie"
  ]
  node [
    id 832
    label "przepisa&#263;"
  ]
  node [
    id 833
    label "za&#322;o&#380;enie"
  ]
  node [
    id 834
    label "work"
  ]
  node [
    id 835
    label "nakarmienie"
  ]
  node [
    id 836
    label "duty"
  ]
  node [
    id 837
    label "powierzanie"
  ]
  node [
    id 838
    label "zaszkodzenie"
  ]
  node [
    id 839
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 840
    label "zaj&#281;cie"
  ]
  node [
    id 841
    label "zobowi&#261;zanie"
  ]
  node [
    id 842
    label "odcinek"
  ]
  node [
    id 843
    label "proste_sko&#347;ne"
  ]
  node [
    id 844
    label "straight_line"
  ]
  node [
    id 845
    label "krzywa"
  ]
  node [
    id 846
    label "napastowa&#263;"
  ]
  node [
    id 847
    label "post&#281;powa&#263;"
  ]
  node [
    id 848
    label "anticipate"
  ]
  node [
    id 849
    label "prasa"
  ]
  node [
    id 850
    label "tytu&#322;"
  ]
  node [
    id 851
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 852
    label "lacki"
  ]
  node [
    id 853
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 854
    label "sztajer"
  ]
  node [
    id 855
    label "drabant"
  ]
  node [
    id 856
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 857
    label "polak"
  ]
  node [
    id 858
    label "pierogi_ruskie"
  ]
  node [
    id 859
    label "krakowiak"
  ]
  node [
    id 860
    label "Polish"
  ]
  node [
    id 861
    label "j&#281;zyk"
  ]
  node [
    id 862
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 863
    label "oberek"
  ]
  node [
    id 864
    label "po_polsku"
  ]
  node [
    id 865
    label "mazur"
  ]
  node [
    id 866
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 867
    label "chodzony"
  ]
  node [
    id 868
    label "skoczny"
  ]
  node [
    id 869
    label "ryba_po_grecku"
  ]
  node [
    id 870
    label "goniony"
  ]
  node [
    id 871
    label "polsko"
  ]
  node [
    id 872
    label "sprawowa&#263;"
  ]
  node [
    id 873
    label "free"
  ]
  node [
    id 874
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 875
    label "peek"
  ]
  node [
    id 876
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 877
    label "spoziera&#263;"
  ]
  node [
    id 878
    label "obejrze&#263;"
  ]
  node [
    id 879
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 880
    label "postrzec"
  ]
  node [
    id 881
    label "pojrze&#263;"
  ]
  node [
    id 882
    label "popatrze&#263;"
  ]
  node [
    id 883
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 884
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 885
    label "see"
  ]
  node [
    id 886
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 887
    label "dostrzec"
  ]
  node [
    id 888
    label "zinterpretowa&#263;"
  ]
  node [
    id 889
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 890
    label "znale&#378;&#263;"
  ]
  node [
    id 891
    label "cognizance"
  ]
  node [
    id 892
    label "nowiniarz"
  ]
  node [
    id 893
    label "akredytowa&#263;"
  ]
  node [
    id 894
    label "akredytowanie"
  ]
  node [
    id 895
    label "publicysta"
  ]
  node [
    id 896
    label "nale&#380;ny"
  ]
  node [
    id 897
    label "frame"
  ]
  node [
    id 898
    label "przekona&#263;"
  ]
  node [
    id 899
    label "uzasadni&#263;"
  ]
  node [
    id 900
    label "przedstawi&#263;"
  ]
  node [
    id 901
    label "poja&#347;ni&#263;"
  ]
  node [
    id 902
    label "obroni&#263;"
  ]
  node [
    id 903
    label "clear"
  ]
  node [
    id 904
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 905
    label "obszar"
  ]
  node [
    id 906
    label "obiekt_naturalny"
  ]
  node [
    id 907
    label "Stary_&#346;wiat"
  ]
  node [
    id 908
    label "grupa"
  ]
  node [
    id 909
    label "stw&#243;r"
  ]
  node [
    id 910
    label "biosfera"
  ]
  node [
    id 911
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 912
    label "magnetosfera"
  ]
  node [
    id 913
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 914
    label "environment"
  ]
  node [
    id 915
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 916
    label "geosfera"
  ]
  node [
    id 917
    label "Nowy_&#346;wiat"
  ]
  node [
    id 918
    label "planeta"
  ]
  node [
    id 919
    label "przejmowa&#263;"
  ]
  node [
    id 920
    label "litosfera"
  ]
  node [
    id 921
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 922
    label "makrokosmos"
  ]
  node [
    id 923
    label "barysfera"
  ]
  node [
    id 924
    label "biota"
  ]
  node [
    id 925
    label "p&#243;&#322;noc"
  ]
  node [
    id 926
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 927
    label "fauna"
  ]
  node [
    id 928
    label "wszechstworzenie"
  ]
  node [
    id 929
    label "geotermia"
  ]
  node [
    id 930
    label "biegun"
  ]
  node [
    id 931
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 932
    label "ekosystem"
  ]
  node [
    id 933
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 934
    label "teren"
  ]
  node [
    id 935
    label "zjawisko"
  ]
  node [
    id 936
    label "p&#243;&#322;kula"
  ]
  node [
    id 937
    label "atmosfera"
  ]
  node [
    id 938
    label "class"
  ]
  node [
    id 939
    label "po&#322;udnie"
  ]
  node [
    id 940
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 941
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 942
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 943
    label "przejmowanie"
  ]
  node [
    id 944
    label "przestrze&#324;"
  ]
  node [
    id 945
    label "asymilowanie_si&#281;"
  ]
  node [
    id 946
    label "przej&#261;&#263;"
  ]
  node [
    id 947
    label "ekosfera"
  ]
  node [
    id 948
    label "przyroda"
  ]
  node [
    id 949
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 950
    label "ciemna_materia"
  ]
  node [
    id 951
    label "geoida"
  ]
  node [
    id 952
    label "Wsch&#243;d"
  ]
  node [
    id 953
    label "populace"
  ]
  node [
    id 954
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 955
    label "huczek"
  ]
  node [
    id 956
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 957
    label "Ziemia"
  ]
  node [
    id 958
    label "universe"
  ]
  node [
    id 959
    label "ozonosfera"
  ]
  node [
    id 960
    label "rze&#378;ba"
  ]
  node [
    id 961
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 962
    label "zagranica"
  ]
  node [
    id 963
    label "hydrosfera"
  ]
  node [
    id 964
    label "woda"
  ]
  node [
    id 965
    label "kuchnia"
  ]
  node [
    id 966
    label "przej&#281;cie"
  ]
  node [
    id 967
    label "czarna_dziura"
  ]
  node [
    id 968
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 969
    label "morze"
  ]
  node [
    id 970
    label "byd&#322;o"
  ]
  node [
    id 971
    label "zobo"
  ]
  node [
    id 972
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 973
    label "yakalo"
  ]
  node [
    id 974
    label "dzo"
  ]
  node [
    id 975
    label "przeszkala&#263;"
  ]
  node [
    id 976
    label "informowa&#263;"
  ]
  node [
    id 977
    label "introduce"
  ]
  node [
    id 978
    label "bespeak"
  ]
  node [
    id 979
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 980
    label "wyra&#380;a&#263;"
  ]
  node [
    id 981
    label "exhibit"
  ]
  node [
    id 982
    label "powodowa&#263;"
  ]
  node [
    id 983
    label "exsert"
  ]
  node [
    id 984
    label "przedstawia&#263;"
  ]
  node [
    id 985
    label "poziom"
  ]
  node [
    id 986
    label "iteratywno&#347;&#263;"
  ]
  node [
    id 987
    label "S&#322;o&#324;ce"
  ]
  node [
    id 988
    label "po&#322;o&#380;enie"
  ]
  node [
    id 989
    label "znak_zodiaku"
  ]
  node [
    id 990
    label "pokaza&#263;"
  ]
  node [
    id 991
    label "podkre&#347;li&#263;"
  ]
  node [
    id 992
    label "wybra&#263;"
  ]
  node [
    id 993
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 994
    label "poda&#263;"
  ]
  node [
    id 995
    label "picture"
  ]
  node [
    id 996
    label "point"
  ]
  node [
    id 997
    label "&#347;wi&#261;teczny"
  ]
  node [
    id 998
    label "gotowy"
  ]
  node [
    id 999
    label "might"
  ]
  node [
    id 1000
    label "thing"
  ]
  node [
    id 1001
    label "cosik"
  ]
  node [
    id 1002
    label "podatno&#347;&#263;"
  ]
  node [
    id 1003
    label "responsiveness"
  ]
  node [
    id 1004
    label "delikatno&#347;&#263;"
  ]
  node [
    id 1005
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1006
    label "wsp&#243;r"
  ]
  node [
    id 1007
    label "obrona"
  ]
  node [
    id 1008
    label "czasowo"
  ]
  node [
    id 1009
    label "wtedy"
  ]
  node [
    id 1010
    label "krajka"
  ]
  node [
    id 1011
    label "tworzywo"
  ]
  node [
    id 1012
    label "krajalno&#347;&#263;"
  ]
  node [
    id 1013
    label "archiwum"
  ]
  node [
    id 1014
    label "kandydat"
  ]
  node [
    id 1015
    label "bielarnia"
  ]
  node [
    id 1016
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 1017
    label "materia"
  ]
  node [
    id 1018
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 1019
    label "nawil&#380;arka"
  ]
  node [
    id 1020
    label "dyspozycja"
  ]
  node [
    id 1021
    label "zawodowy"
  ]
  node [
    id 1022
    label "tre&#347;ciwy"
  ]
  node [
    id 1023
    label "rzetelny"
  ]
  node [
    id 1024
    label "po_dziennikarsku"
  ]
  node [
    id 1025
    label "dziennikarsko"
  ]
  node [
    id 1026
    label "obiektywny"
  ]
  node [
    id 1027
    label "wzorowy"
  ]
  node [
    id 1028
    label "typowy"
  ]
  node [
    id 1029
    label "nadmiernie"
  ]
  node [
    id 1030
    label "sprzedawanie"
  ]
  node [
    id 1031
    label "sprzeda&#380;"
  ]
  node [
    id 1032
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 1033
    label "cz&#281;sty"
  ]
  node [
    id 1034
    label "doznawa&#263;"
  ]
  node [
    id 1035
    label "znachodzi&#263;"
  ]
  node [
    id 1036
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 1037
    label "pozyskiwa&#263;"
  ]
  node [
    id 1038
    label "odzyskiwa&#263;"
  ]
  node [
    id 1039
    label "wykrywa&#263;"
  ]
  node [
    id 1040
    label "unwrap"
  ]
  node [
    id 1041
    label "detect"
  ]
  node [
    id 1042
    label "wymy&#347;la&#263;"
  ]
  node [
    id 1043
    label "cholera"
  ]
  node [
    id 1044
    label "pies"
  ]
  node [
    id 1045
    label "szmata"
  ]
  node [
    id 1046
    label "bluzg"
  ]
  node [
    id 1047
    label "obelga"
  ]
  node [
    id 1048
    label "chujowy"
  ]
  node [
    id 1049
    label "chuj"
  ]
  node [
    id 1050
    label "wiktor"
  ]
  node [
    id 1051
    label "marka"
  ]
  node [
    id 1052
    label "Janina"
  ]
  node [
    id 1053
    label "Jankowska"
  ]
  node [
    id 1054
    label "Jerzy"
  ]
  node [
    id 1055
    label "Jachowicz"
  ]
  node [
    id 1056
    label "Piotr"
  ]
  node [
    id 1057
    label "Zaremba"
  ]
  node [
    id 1058
    label "krakowski"
  ]
  node [
    id 1059
    label "Wojciecha"
  ]
  node [
    id 1060
    label "Maziarski"
  ]
  node [
    id 1061
    label "Tomasz"
  ]
  node [
    id 1062
    label "Sakiewicz"
  ]
  node [
    id 1063
    label "Bogumi&#322;a"
  ]
  node [
    id 1064
    label "&#321;ozi&#324;ski"
  ]
  node [
    id 1065
    label "go&#347;&#263;"
  ]
  node [
    id 1066
    label "Rafa&#322;"
  ]
  node [
    id 1067
    label "zimny"
  ]
  node [
    id 1068
    label "uniwersytet"
  ]
  node [
    id 1069
    label "Kazimierz"
  ]
  node [
    id 1070
    label "wielki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 276
  ]
  edge [
    source 12
    target 277
  ]
  edge [
    source 12
    target 278
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 280
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 12
    target 282
  ]
  edge [
    source 12
    target 283
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 285
  ]
  edge [
    source 12
    target 286
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 312
  ]
  edge [
    source 14
    target 313
  ]
  edge [
    source 14
    target 314
  ]
  edge [
    source 14
    target 315
  ]
  edge [
    source 14
    target 316
  ]
  edge [
    source 14
    target 317
  ]
  edge [
    source 14
    target 318
  ]
  edge [
    source 14
    target 319
  ]
  edge [
    source 14
    target 320
  ]
  edge [
    source 14
    target 1050
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 321
  ]
  edge [
    source 16
    target 322
  ]
  edge [
    source 16
    target 323
  ]
  edge [
    source 16
    target 324
  ]
  edge [
    source 16
    target 325
  ]
  edge [
    source 16
    target 326
  ]
  edge [
    source 16
    target 327
  ]
  edge [
    source 16
    target 328
  ]
  edge [
    source 16
    target 329
  ]
  edge [
    source 16
    target 330
  ]
  edge [
    source 16
    target 331
  ]
  edge [
    source 16
    target 332
  ]
  edge [
    source 16
    target 333
  ]
  edge [
    source 16
    target 334
  ]
  edge [
    source 16
    target 335
  ]
  edge [
    source 16
    target 336
  ]
  edge [
    source 16
    target 337
  ]
  edge [
    source 16
    target 338
  ]
  edge [
    source 16
    target 339
  ]
  edge [
    source 16
    target 340
  ]
  edge [
    source 16
    target 341
  ]
  edge [
    source 16
    target 342
  ]
  edge [
    source 16
    target 343
  ]
  edge [
    source 16
    target 344
  ]
  edge [
    source 16
    target 345
  ]
  edge [
    source 16
    target 346
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 90
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 347
  ]
  edge [
    source 20
    target 348
  ]
  edge [
    source 20
    target 349
  ]
  edge [
    source 20
    target 350
  ]
  edge [
    source 20
    target 351
  ]
  edge [
    source 20
    target 352
  ]
  edge [
    source 20
    target 353
  ]
  edge [
    source 20
    target 354
  ]
  edge [
    source 20
    target 355
  ]
  edge [
    source 20
    target 356
  ]
  edge [
    source 20
    target 357
  ]
  edge [
    source 20
    target 358
  ]
  edge [
    source 20
    target 359
  ]
  edge [
    source 20
    target 360
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 20
    target 361
  ]
  edge [
    source 20
    target 362
  ]
  edge [
    source 20
    target 363
  ]
  edge [
    source 20
    target 364
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 365
  ]
  edge [
    source 21
    target 366
  ]
  edge [
    source 21
    target 367
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 368
  ]
  edge [
    source 23
    target 369
  ]
  edge [
    source 23
    target 370
  ]
  edge [
    source 23
    target 371
  ]
  edge [
    source 23
    target 372
  ]
  edge [
    source 23
    target 373
  ]
  edge [
    source 23
    target 374
  ]
  edge [
    source 23
    target 375
  ]
  edge [
    source 23
    target 376
  ]
  edge [
    source 23
    target 377
  ]
  edge [
    source 23
    target 378
  ]
  edge [
    source 23
    target 379
  ]
  edge [
    source 23
    target 1051
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 75
  ]
  edge [
    source 24
    target 76
  ]
  edge [
    source 24
    target 82
  ]
  edge [
    source 24
    target 37
  ]
  edge [
    source 24
    target 380
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 41
  ]
  edge [
    source 25
    target 42
  ]
  edge [
    source 25
    target 381
  ]
  edge [
    source 25
    target 382
  ]
  edge [
    source 25
    target 383
  ]
  edge [
    source 25
    target 384
  ]
  edge [
    source 25
    target 385
  ]
  edge [
    source 25
    target 386
  ]
  edge [
    source 25
    target 387
  ]
  edge [
    source 25
    target 102
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 41
  ]
  edge [
    source 26
    target 388
  ]
  edge [
    source 26
    target 389
  ]
  edge [
    source 26
    target 390
  ]
  edge [
    source 26
    target 391
  ]
  edge [
    source 26
    target 392
  ]
  edge [
    source 26
    target 393
  ]
  edge [
    source 26
    target 394
  ]
  edge [
    source 26
    target 395
  ]
  edge [
    source 26
    target 396
  ]
  edge [
    source 26
    target 397
  ]
  edge [
    source 26
    target 398
  ]
  edge [
    source 26
    target 399
  ]
  edge [
    source 26
    target 400
  ]
  edge [
    source 26
    target 401
  ]
  edge [
    source 26
    target 402
  ]
  edge [
    source 26
    target 403
  ]
  edge [
    source 26
    target 404
  ]
  edge [
    source 26
    target 405
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 406
  ]
  edge [
    source 27
    target 407
  ]
  edge [
    source 27
    target 408
  ]
  edge [
    source 27
    target 409
  ]
  edge [
    source 27
    target 410
  ]
  edge [
    source 27
    target 411
  ]
  edge [
    source 27
    target 412
  ]
  edge [
    source 27
    target 413
  ]
  edge [
    source 27
    target 414
  ]
  edge [
    source 27
    target 415
  ]
  edge [
    source 27
    target 87
  ]
  edge [
    source 27
    target 97
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 79
  ]
  edge [
    source 28
    target 80
  ]
  edge [
    source 28
    target 416
  ]
  edge [
    source 28
    target 417
  ]
  edge [
    source 28
    target 418
  ]
  edge [
    source 28
    target 419
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 92
  ]
  edge [
    source 29
    target 420
  ]
  edge [
    source 29
    target 421
  ]
  edge [
    source 29
    target 422
  ]
  edge [
    source 29
    target 423
  ]
  edge [
    source 29
    target 48
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 424
  ]
  edge [
    source 30
    target 425
  ]
  edge [
    source 30
    target 426
  ]
  edge [
    source 30
    target 46
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 427
  ]
  edge [
    source 31
    target 428
  ]
  edge [
    source 31
    target 429
  ]
  edge [
    source 31
    target 430
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 431
  ]
  edge [
    source 32
    target 432
  ]
  edge [
    source 32
    target 433
  ]
  edge [
    source 32
    target 434
  ]
  edge [
    source 32
    target 435
  ]
  edge [
    source 32
    target 436
  ]
  edge [
    source 32
    target 437
  ]
  edge [
    source 32
    target 438
  ]
  edge [
    source 32
    target 439
  ]
  edge [
    source 32
    target 440
  ]
  edge [
    source 32
    target 441
  ]
  edge [
    source 32
    target 129
  ]
  edge [
    source 32
    target 93
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 442
  ]
  edge [
    source 33
    target 93
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 443
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 444
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 445
  ]
  edge [
    source 36
    target 446
  ]
  edge [
    source 36
    target 447
  ]
  edge [
    source 36
    target 448
  ]
  edge [
    source 36
    target 449
  ]
  edge [
    source 36
    target 450
  ]
  edge [
    source 36
    target 451
  ]
  edge [
    source 36
    target 452
  ]
  edge [
    source 36
    target 453
  ]
  edge [
    source 36
    target 454
  ]
  edge [
    source 36
    target 455
  ]
  edge [
    source 36
    target 456
  ]
  edge [
    source 36
    target 457
  ]
  edge [
    source 36
    target 458
  ]
  edge [
    source 36
    target 459
  ]
  edge [
    source 36
    target 460
  ]
  edge [
    source 36
    target 461
  ]
  edge [
    source 36
    target 462
  ]
  edge [
    source 36
    target 463
  ]
  edge [
    source 36
    target 464
  ]
  edge [
    source 36
    target 465
  ]
  edge [
    source 36
    target 466
  ]
  edge [
    source 36
    target 467
  ]
  edge [
    source 36
    target 468
  ]
  edge [
    source 36
    target 469
  ]
  edge [
    source 36
    target 74
  ]
  edge [
    source 36
    target 128
  ]
  edge [
    source 36
    target 131
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 83
  ]
  edge [
    source 37
    target 89
  ]
  edge [
    source 37
    target 90
  ]
  edge [
    source 37
    target 91
  ]
  edge [
    source 37
    target 99
  ]
  edge [
    source 37
    target 100
  ]
  edge [
    source 37
    target 108
  ]
  edge [
    source 37
    target 109
  ]
  edge [
    source 37
    target 123
  ]
  edge [
    source 37
    target 124
  ]
  edge [
    source 37
    target 470
  ]
  edge [
    source 37
    target 471
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 472
  ]
  edge [
    source 37
    target 473
  ]
  edge [
    source 37
    target 474
  ]
  edge [
    source 37
    target 475
  ]
  edge [
    source 37
    target 476
  ]
  edge [
    source 37
    target 477
  ]
  edge [
    source 37
    target 478
  ]
  edge [
    source 37
    target 479
  ]
  edge [
    source 37
    target 57
  ]
  edge [
    source 37
    target 116
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 85
  ]
  edge [
    source 38
    target 86
  ]
  edge [
    source 38
    target 480
  ]
  edge [
    source 38
    target 481
  ]
  edge [
    source 38
    target 482
  ]
  edge [
    source 38
    target 483
  ]
  edge [
    source 38
    target 484
  ]
  edge [
    source 38
    target 485
  ]
  edge [
    source 38
    target 486
  ]
  edge [
    source 38
    target 487
  ]
  edge [
    source 38
    target 488
  ]
  edge [
    source 38
    target 489
  ]
  edge [
    source 38
    target 490
  ]
  edge [
    source 38
    target 491
  ]
  edge [
    source 38
    target 492
  ]
  edge [
    source 38
    target 493
  ]
  edge [
    source 38
    target 494
  ]
  edge [
    source 38
    target 495
  ]
  edge [
    source 38
    target 496
  ]
  edge [
    source 38
    target 497
  ]
  edge [
    source 38
    target 498
  ]
  edge [
    source 38
    target 499
  ]
  edge [
    source 38
    target 500
  ]
  edge [
    source 38
    target 501
  ]
  edge [
    source 38
    target 502
  ]
  edge [
    source 38
    target 503
  ]
  edge [
    source 38
    target 504
  ]
  edge [
    source 38
    target 505
  ]
  edge [
    source 38
    target 506
  ]
  edge [
    source 38
    target 507
  ]
  edge [
    source 38
    target 69
  ]
  edge [
    source 38
    target 508
  ]
  edge [
    source 38
    target 509
  ]
  edge [
    source 38
    target 510
  ]
  edge [
    source 38
    target 511
  ]
  edge [
    source 38
    target 469
  ]
  edge [
    source 38
    target 117
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 512
  ]
  edge [
    source 40
    target 69
  ]
  edge [
    source 40
    target 1058
  ]
  edge [
    source 41
    target 513
  ]
  edge [
    source 41
    target 98
  ]
  edge [
    source 41
    target 109
  ]
  edge [
    source 41
    target 119
  ]
  edge [
    source 41
    target 135
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 514
  ]
  edge [
    source 42
    target 69
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 109
  ]
  edge [
    source 43
    target 110
  ]
  edge [
    source 43
    target 515
  ]
  edge [
    source 43
    target 516
  ]
  edge [
    source 43
    target 517
  ]
  edge [
    source 43
    target 518
  ]
  edge [
    source 43
    target 519
  ]
  edge [
    source 43
    target 520
  ]
  edge [
    source 43
    target 521
  ]
  edge [
    source 43
    target 97
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 522
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 523
  ]
  edge [
    source 45
    target 524
  ]
  edge [
    source 45
    target 525
  ]
  edge [
    source 45
    target 526
  ]
  edge [
    source 45
    target 527
  ]
  edge [
    source 45
    target 528
  ]
  edge [
    source 45
    target 529
  ]
  edge [
    source 45
    target 530
  ]
  edge [
    source 45
    target 531
  ]
  edge [
    source 45
    target 532
  ]
  edge [
    source 45
    target 533
  ]
  edge [
    source 45
    target 534
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 535
  ]
  edge [
    source 46
    target 536
  ]
  edge [
    source 46
    target 537
  ]
  edge [
    source 46
    target 538
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 539
  ]
  edge [
    source 47
    target 540
  ]
  edge [
    source 47
    target 294
  ]
  edge [
    source 47
    target 541
  ]
  edge [
    source 47
    target 542
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 543
  ]
  edge [
    source 48
    target 544
  ]
  edge [
    source 48
    target 545
  ]
  edge [
    source 48
    target 546
  ]
  edge [
    source 48
    target 547
  ]
  edge [
    source 48
    target 548
  ]
  edge [
    source 48
    target 549
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 550
  ]
  edge [
    source 49
    target 551
  ]
  edge [
    source 49
    target 552
  ]
  edge [
    source 49
    target 553
  ]
  edge [
    source 49
    target 554
  ]
  edge [
    source 49
    target 555
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 556
  ]
  edge [
    source 50
    target 557
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 558
  ]
  edge [
    source 51
    target 559
  ]
  edge [
    source 51
    target 560
  ]
  edge [
    source 51
    target 561
  ]
  edge [
    source 51
    target 562
  ]
  edge [
    source 51
    target 563
  ]
  edge [
    source 51
    target 564
  ]
  edge [
    source 51
    target 64
  ]
  edge [
    source 51
    target 144
  ]
  edge [
    source 51
    target 145
  ]
  edge [
    source 51
    target 565
  ]
  edge [
    source 51
    target 566
  ]
  edge [
    source 51
    target 147
  ]
  edge [
    source 51
    target 148
  ]
  edge [
    source 51
    target 567
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 568
  ]
  edge [
    source 52
    target 569
  ]
  edge [
    source 52
    target 570
  ]
  edge [
    source 52
    target 571
  ]
  edge [
    source 53
    target 67
  ]
  edge [
    source 53
    target 68
  ]
  edge [
    source 53
    target 107
  ]
  edge [
    source 53
    target 108
  ]
  edge [
    source 53
    target 113
  ]
  edge [
    source 53
    target 114
  ]
  edge [
    source 53
    target 125
  ]
  edge [
    source 53
    target 126
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 572
  ]
  edge [
    source 54
    target 573
  ]
  edge [
    source 54
    target 574
  ]
  edge [
    source 54
    target 539
  ]
  edge [
    source 54
    target 575
  ]
  edge [
    source 54
    target 576
  ]
  edge [
    source 54
    target 577
  ]
  edge [
    source 54
    target 578
  ]
  edge [
    source 54
    target 579
  ]
  edge [
    source 54
    target 580
  ]
  edge [
    source 54
    target 581
  ]
  edge [
    source 54
    target 582
  ]
  edge [
    source 54
    target 583
  ]
  edge [
    source 54
    target 584
  ]
  edge [
    source 54
    target 585
  ]
  edge [
    source 54
    target 586
  ]
  edge [
    source 54
    target 587
  ]
  edge [
    source 54
    target 588
  ]
  edge [
    source 54
    target 589
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 117
  ]
  edge [
    source 56
    target 118
  ]
  edge [
    source 56
    target 590
  ]
  edge [
    source 56
    target 88
  ]
  edge [
    source 56
    target 591
  ]
  edge [
    source 56
    target 592
  ]
  edge [
    source 56
    target 593
  ]
  edge [
    source 56
    target 594
  ]
  edge [
    source 56
    target 595
  ]
  edge [
    source 56
    target 596
  ]
  edge [
    source 56
    target 597
  ]
  edge [
    source 56
    target 598
  ]
  edge [
    source 56
    target 99
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 116
  ]
  edge [
    source 57
    target 599
  ]
  edge [
    source 57
    target 600
  ]
  edge [
    source 57
    target 601
  ]
  edge [
    source 57
    target 602
  ]
  edge [
    source 57
    target 603
  ]
  edge [
    source 57
    target 604
  ]
  edge [
    source 57
    target 605
  ]
  edge [
    source 57
    target 606
  ]
  edge [
    source 57
    target 607
  ]
  edge [
    source 57
    target 608
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 609
  ]
  edge [
    source 58
    target 610
  ]
  edge [
    source 58
    target 611
  ]
  edge [
    source 58
    target 612
  ]
  edge [
    source 58
    target 613
  ]
  edge [
    source 58
    target 241
  ]
  edge [
    source 58
    target 614
  ]
  edge [
    source 58
    target 615
  ]
  edge [
    source 58
    target 616
  ]
  edge [
    source 58
    target 617
  ]
  edge [
    source 58
    target 618
  ]
  edge [
    source 58
    target 619
  ]
  edge [
    source 58
    target 620
  ]
  edge [
    source 58
    target 621
  ]
  edge [
    source 58
    target 622
  ]
  edge [
    source 58
    target 623
  ]
  edge [
    source 58
    target 624
  ]
  edge [
    source 58
    target 625
  ]
  edge [
    source 58
    target 626
  ]
  edge [
    source 58
    target 627
  ]
  edge [
    source 58
    target 94
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 628
  ]
  edge [
    source 59
    target 629
  ]
  edge [
    source 59
    target 630
  ]
  edge [
    source 59
    target 539
  ]
  edge [
    source 59
    target 631
  ]
  edge [
    source 59
    target 632
  ]
  edge [
    source 59
    target 633
  ]
  edge [
    source 59
    target 634
  ]
  edge [
    source 59
    target 635
  ]
  edge [
    source 59
    target 636
  ]
  edge [
    source 59
    target 637
  ]
  edge [
    source 59
    target 638
  ]
  edge [
    source 59
    target 639
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 65
  ]
  edge [
    source 60
    target 66
  ]
  edge [
    source 60
    target 640
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 641
  ]
  edge [
    source 61
    target 642
  ]
  edge [
    source 61
    target 643
  ]
  edge [
    source 61
    target 644
  ]
  edge [
    source 61
    target 76
  ]
  edge [
    source 61
    target 132
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 645
  ]
  edge [
    source 62
    target 646
  ]
  edge [
    source 62
    target 647
  ]
  edge [
    source 62
    target 648
  ]
  edge [
    source 62
    target 649
  ]
  edge [
    source 62
    target 650
  ]
  edge [
    source 62
    target 651
  ]
  edge [
    source 62
    target 652
  ]
  edge [
    source 62
    target 653
  ]
  edge [
    source 62
    target 654
  ]
  edge [
    source 62
    target 655
  ]
  edge [
    source 62
    target 656
  ]
  edge [
    source 62
    target 657
  ]
  edge [
    source 62
    target 658
  ]
  edge [
    source 62
    target 659
  ]
  edge [
    source 62
    target 660
  ]
  edge [
    source 62
    target 625
  ]
  edge [
    source 62
    target 661
  ]
  edge [
    source 62
    target 662
  ]
  edge [
    source 62
    target 663
  ]
  edge [
    source 62
    target 664
  ]
  edge [
    source 62
    target 665
  ]
  edge [
    source 62
    target 666
  ]
  edge [
    source 62
    target 667
  ]
  edge [
    source 62
    target 668
  ]
  edge [
    source 62
    target 669
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 670
  ]
  edge [
    source 63
    target 671
  ]
  edge [
    source 63
    target 672
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 673
  ]
  edge [
    source 64
    target 674
  ]
  edge [
    source 64
    target 675
  ]
  edge [
    source 64
    target 676
  ]
  edge [
    source 64
    target 677
  ]
  edge [
    source 64
    target 678
  ]
  edge [
    source 64
    target 679
  ]
  edge [
    source 64
    target 680
  ]
  edge [
    source 64
    target 681
  ]
  edge [
    source 64
    target 682
  ]
  edge [
    source 64
    target 683
  ]
  edge [
    source 64
    target 684
  ]
  edge [
    source 64
    target 685
  ]
  edge [
    source 64
    target 96
  ]
  edge [
    source 65
    target 686
  ]
  edge [
    source 65
    target 687
  ]
  edge [
    source 65
    target 688
  ]
  edge [
    source 65
    target 689
  ]
  edge [
    source 65
    target 690
  ]
  edge [
    source 65
    target 691
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 692
  ]
  edge [
    source 66
    target 693
  ]
  edge [
    source 66
    target 694
  ]
  edge [
    source 67
    target 695
  ]
  edge [
    source 67
    target 696
  ]
  edge [
    source 67
    target 697
  ]
  edge [
    source 67
    target 698
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 699
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 700
  ]
  edge [
    source 69
    target 701
  ]
  edge [
    source 69
    target 702
  ]
  edge [
    source 69
    target 311
  ]
  edge [
    source 69
    target 703
  ]
  edge [
    source 69
    target 704
  ]
  edge [
    source 69
    target 74
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 705
  ]
  edge [
    source 70
    target 505
  ]
  edge [
    source 70
    target 86
  ]
  edge [
    source 70
    target 706
  ]
  edge [
    source 70
    target 707
  ]
  edge [
    source 70
    target 708
  ]
  edge [
    source 70
    target 709
  ]
  edge [
    source 70
    target 710
  ]
  edge [
    source 70
    target 711
  ]
  edge [
    source 70
    target 712
  ]
  edge [
    source 70
    target 713
  ]
  edge [
    source 70
    target 237
  ]
  edge [
    source 70
    target 714
  ]
  edge [
    source 71
    target 715
  ]
  edge [
    source 71
    target 415
  ]
  edge [
    source 71
    target 716
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 671
  ]
  edge [
    source 72
    target 717
  ]
  edge [
    source 72
    target 718
  ]
  edge [
    source 72
    target 719
  ]
  edge [
    source 72
    target 720
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 721
  ]
  edge [
    source 74
    target 722
  ]
  edge [
    source 74
    target 723
  ]
  edge [
    source 74
    target 356
  ]
  edge [
    source 74
    target 724
  ]
  edge [
    source 75
    target 725
  ]
  edge [
    source 75
    target 726
  ]
  edge [
    source 75
    target 727
  ]
  edge [
    source 75
    target 728
  ]
  edge [
    source 75
    target 729
  ]
  edge [
    source 75
    target 730
  ]
  edge [
    source 75
    target 731
  ]
  edge [
    source 75
    target 732
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 132
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 93
  ]
  edge [
    source 77
    target 94
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 79
    target 733
  ]
  edge [
    source 79
    target 734
  ]
  edge [
    source 79
    target 735
  ]
  edge [
    source 79
    target 736
  ]
  edge [
    source 79
    target 737
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 738
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 556
  ]
  edge [
    source 81
    target 739
  ]
  edge [
    source 82
    target 740
  ]
  edge [
    source 82
    target 741
  ]
  edge [
    source 82
    target 742
  ]
  edge [
    source 82
    target 743
  ]
  edge [
    source 82
    target 744
  ]
  edge [
    source 82
    target 745
  ]
  edge [
    source 82
    target 746
  ]
  edge [
    source 82
    target 747
  ]
  edge [
    source 82
    target 748
  ]
  edge [
    source 82
    target 749
  ]
  edge [
    source 82
    target 489
  ]
  edge [
    source 82
    target 750
  ]
  edge [
    source 82
    target 751
  ]
  edge [
    source 82
    target 691
  ]
  edge [
    source 82
    target 752
  ]
  edge [
    source 82
    target 753
  ]
  edge [
    source 82
    target 754
  ]
  edge [
    source 82
    target 755
  ]
  edge [
    source 82
    target 756
  ]
  edge [
    source 82
    target 757
  ]
  edge [
    source 82
    target 758
  ]
  edge [
    source 82
    target 759
  ]
  edge [
    source 82
    target 760
  ]
  edge [
    source 82
    target 761
  ]
  edge [
    source 82
    target 762
  ]
  edge [
    source 82
    target 763
  ]
  edge [
    source 82
    target 764
  ]
  edge [
    source 82
    target 266
  ]
  edge [
    source 82
    target 765
  ]
  edge [
    source 82
    target 766
  ]
  edge [
    source 82
    target 767
  ]
  edge [
    source 82
    target 768
  ]
  edge [
    source 82
    target 769
  ]
  edge [
    source 82
    target 770
  ]
  edge [
    source 82
    target 771
  ]
  edge [
    source 82
    target 772
  ]
  edge [
    source 82
    target 773
  ]
  edge [
    source 82
    target 469
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 774
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 775
  ]
  edge [
    source 85
    target 776
  ]
  edge [
    source 85
    target 92
  ]
  edge [
    source 85
    target 739
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 777
  ]
  edge [
    source 86
    target 778
  ]
  edge [
    source 86
    target 706
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 779
  ]
  edge [
    source 87
    target 780
  ]
  edge [
    source 87
    target 781
  ]
  edge [
    source 87
    target 782
  ]
  edge [
    source 87
    target 783
  ]
  edge [
    source 87
    target 784
  ]
  edge [
    source 87
    target 785
  ]
  edge [
    source 87
    target 786
  ]
  edge [
    source 87
    target 97
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 787
  ]
  edge [
    source 88
    target 788
  ]
  edge [
    source 88
    target 789
  ]
  edge [
    source 88
    target 790
  ]
  edge [
    source 88
    target 791
  ]
  edge [
    source 88
    target 255
  ]
  edge [
    source 88
    target 792
  ]
  edge [
    source 88
    target 793
  ]
  edge [
    source 88
    target 794
  ]
  edge [
    source 88
    target 795
  ]
  edge [
    source 90
    target 796
  ]
  edge [
    source 90
    target 797
  ]
  edge [
    source 90
    target 798
  ]
  edge [
    source 90
    target 799
  ]
  edge [
    source 90
    target 800
  ]
  edge [
    source 90
    target 255
  ]
  edge [
    source 90
    target 801
  ]
  edge [
    source 90
    target 802
  ]
  edge [
    source 90
    target 112
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 739
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 116
  ]
  edge [
    source 92
    target 117
  ]
  edge [
    source 92
    target 803
  ]
  edge [
    source 92
    target 804
  ]
  edge [
    source 92
    target 805
  ]
  edge [
    source 93
    target 806
  ]
  edge [
    source 93
    target 807
  ]
  edge [
    source 93
    target 808
  ]
  edge [
    source 93
    target 809
  ]
  edge [
    source 93
    target 810
  ]
  edge [
    source 93
    target 811
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 812
  ]
  edge [
    source 94
    target 813
  ]
  edge [
    source 94
    target 110
  ]
  edge [
    source 94
    target 1059
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 389
  ]
  edge [
    source 96
    target 814
  ]
  edge [
    source 96
    target 815
  ]
  edge [
    source 96
    target 816
  ]
  edge [
    source 96
    target 150
  ]
  edge [
    source 96
    target 817
  ]
  edge [
    source 96
    target 818
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 818
  ]
  edge [
    source 97
    target 348
  ]
  edge [
    source 97
    target 819
  ]
  edge [
    source 97
    target 351
  ]
  edge [
    source 97
    target 820
  ]
  edge [
    source 97
    target 352
  ]
  edge [
    source 97
    target 821
  ]
  edge [
    source 97
    target 822
  ]
  edge [
    source 97
    target 823
  ]
  edge [
    source 97
    target 356
  ]
  edge [
    source 97
    target 357
  ]
  edge [
    source 97
    target 359
  ]
  edge [
    source 97
    target 360
  ]
  edge [
    source 97
    target 824
  ]
  edge [
    source 97
    target 825
  ]
  edge [
    source 97
    target 354
  ]
  edge [
    source 97
    target 826
  ]
  edge [
    source 97
    target 827
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 828
  ]
  edge [
    source 98
    target 109
  ]
  edge [
    source 98
    target 119
  ]
  edge [
    source 98
    target 135
  ]
  edge [
    source 99
    target 829
  ]
  edge [
    source 99
    target 830
  ]
  edge [
    source 99
    target 831
  ]
  edge [
    source 99
    target 832
  ]
  edge [
    source 99
    target 833
  ]
  edge [
    source 99
    target 834
  ]
  edge [
    source 99
    target 835
  ]
  edge [
    source 99
    target 836
  ]
  edge [
    source 99
    target 384
  ]
  edge [
    source 99
    target 837
  ]
  edge [
    source 99
    target 838
  ]
  edge [
    source 99
    target 839
  ]
  edge [
    source 99
    target 840
  ]
  edge [
    source 99
    target 841
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 189
  ]
  edge [
    source 100
    target 842
  ]
  edge [
    source 100
    target 843
  ]
  edge [
    source 100
    target 844
  ]
  edge [
    source 100
    target 683
  ]
  edge [
    source 100
    target 166
  ]
  edge [
    source 100
    target 845
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 846
  ]
  edge [
    source 102
    target 847
  ]
  edge [
    source 102
    target 848
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 849
  ]
  edge [
    source 105
    target 356
  ]
  edge [
    source 105
    target 850
  ]
  edge [
    source 105
    target 851
  ]
  edge [
    source 105
    target 380
  ]
  edge [
    source 106
    target 852
  ]
  edge [
    source 106
    target 853
  ]
  edge [
    source 106
    target 263
  ]
  edge [
    source 106
    target 854
  ]
  edge [
    source 106
    target 855
  ]
  edge [
    source 106
    target 856
  ]
  edge [
    source 106
    target 857
  ]
  edge [
    source 106
    target 858
  ]
  edge [
    source 106
    target 859
  ]
  edge [
    source 106
    target 860
  ]
  edge [
    source 106
    target 861
  ]
  edge [
    source 106
    target 862
  ]
  edge [
    source 106
    target 863
  ]
  edge [
    source 106
    target 864
  ]
  edge [
    source 106
    target 865
  ]
  edge [
    source 106
    target 866
  ]
  edge [
    source 106
    target 867
  ]
  edge [
    source 106
    target 868
  ]
  edge [
    source 106
    target 869
  ]
  edge [
    source 106
    target 870
  ]
  edge [
    source 106
    target 871
  ]
  edge [
    source 107
    target 872
  ]
  edge [
    source 108
    target 873
  ]
  edge [
    source 109
    target 577
  ]
  edge [
    source 109
    target 874
  ]
  edge [
    source 109
    target 875
  ]
  edge [
    source 109
    target 876
  ]
  edge [
    source 109
    target 877
  ]
  edge [
    source 109
    target 878
  ]
  edge [
    source 109
    target 879
  ]
  edge [
    source 109
    target 880
  ]
  edge [
    source 109
    target 350
  ]
  edge [
    source 109
    target 578
  ]
  edge [
    source 109
    target 881
  ]
  edge [
    source 109
    target 882
  ]
  edge [
    source 109
    target 883
  ]
  edge [
    source 109
    target 884
  ]
  edge [
    source 109
    target 885
  ]
  edge [
    source 109
    target 886
  ]
  edge [
    source 109
    target 887
  ]
  edge [
    source 109
    target 888
  ]
  edge [
    source 109
    target 889
  ]
  edge [
    source 109
    target 890
  ]
  edge [
    source 109
    target 891
  ]
  edge [
    source 109
    target 119
  ]
  edge [
    source 109
    target 135
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 892
  ]
  edge [
    source 111
    target 893
  ]
  edge [
    source 111
    target 894
  ]
  edge [
    source 111
    target 721
  ]
  edge [
    source 111
    target 895
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 896
  ]
  edge [
    source 113
    target 897
  ]
  edge [
    source 113
    target 888
  ]
  edge [
    source 113
    target 898
  ]
  edge [
    source 113
    target 899
  ]
  edge [
    source 113
    target 900
  ]
  edge [
    source 113
    target 901
  ]
  edge [
    source 113
    target 902
  ]
  edge [
    source 113
    target 810
  ]
  edge [
    source 113
    target 903
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 114
    target 904
  ]
  edge [
    source 114
    target 905
  ]
  edge [
    source 114
    target 906
  ]
  edge [
    source 114
    target 263
  ]
  edge [
    source 114
    target 907
  ]
  edge [
    source 114
    target 908
  ]
  edge [
    source 114
    target 909
  ]
  edge [
    source 114
    target 910
  ]
  edge [
    source 114
    target 911
  ]
  edge [
    source 114
    target 791
  ]
  edge [
    source 114
    target 912
  ]
  edge [
    source 114
    target 913
  ]
  edge [
    source 114
    target 914
  ]
  edge [
    source 114
    target 915
  ]
  edge [
    source 114
    target 916
  ]
  edge [
    source 114
    target 917
  ]
  edge [
    source 114
    target 918
  ]
  edge [
    source 114
    target 919
  ]
  edge [
    source 114
    target 920
  ]
  edge [
    source 114
    target 921
  ]
  edge [
    source 114
    target 922
  ]
  edge [
    source 114
    target 923
  ]
  edge [
    source 114
    target 924
  ]
  edge [
    source 114
    target 925
  ]
  edge [
    source 114
    target 926
  ]
  edge [
    source 114
    target 927
  ]
  edge [
    source 114
    target 928
  ]
  edge [
    source 114
    target 929
  ]
  edge [
    source 114
    target 930
  ]
  edge [
    source 114
    target 931
  ]
  edge [
    source 114
    target 932
  ]
  edge [
    source 114
    target 933
  ]
  edge [
    source 114
    target 934
  ]
  edge [
    source 114
    target 935
  ]
  edge [
    source 114
    target 936
  ]
  edge [
    source 114
    target 937
  ]
  edge [
    source 114
    target 457
  ]
  edge [
    source 114
    target 938
  ]
  edge [
    source 114
    target 939
  ]
  edge [
    source 114
    target 940
  ]
  edge [
    source 114
    target 941
  ]
  edge [
    source 114
    target 942
  ]
  edge [
    source 114
    target 943
  ]
  edge [
    source 114
    target 944
  ]
  edge [
    source 114
    target 945
  ]
  edge [
    source 114
    target 946
  ]
  edge [
    source 114
    target 947
  ]
  edge [
    source 114
    target 948
  ]
  edge [
    source 114
    target 949
  ]
  edge [
    source 114
    target 950
  ]
  edge [
    source 114
    target 951
  ]
  edge [
    source 114
    target 952
  ]
  edge [
    source 114
    target 953
  ]
  edge [
    source 114
    target 954
  ]
  edge [
    source 114
    target 955
  ]
  edge [
    source 114
    target 956
  ]
  edge [
    source 114
    target 957
  ]
  edge [
    source 114
    target 958
  ]
  edge [
    source 114
    target 959
  ]
  edge [
    source 114
    target 960
  ]
  edge [
    source 114
    target 961
  ]
  edge [
    source 114
    target 962
  ]
  edge [
    source 114
    target 963
  ]
  edge [
    source 114
    target 964
  ]
  edge [
    source 114
    target 965
  ]
  edge [
    source 114
    target 966
  ]
  edge [
    source 114
    target 967
  ]
  edge [
    source 114
    target 968
  ]
  edge [
    source 114
    target 969
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 115
    target 970
  ]
  edge [
    source 115
    target 971
  ]
  edge [
    source 115
    target 972
  ]
  edge [
    source 115
    target 973
  ]
  edge [
    source 115
    target 974
  ]
  edge [
    source 116
    target 975
  ]
  edge [
    source 116
    target 599
  ]
  edge [
    source 116
    target 976
  ]
  edge [
    source 116
    target 977
  ]
  edge [
    source 116
    target 978
  ]
  edge [
    source 116
    target 979
  ]
  edge [
    source 116
    target 601
  ]
  edge [
    source 116
    target 602
  ]
  edge [
    source 116
    target 603
  ]
  edge [
    source 116
    target 980
  ]
  edge [
    source 116
    target 981
  ]
  edge [
    source 116
    target 982
  ]
  edge [
    source 116
    target 606
  ]
  edge [
    source 116
    target 983
  ]
  edge [
    source 116
    target 984
  ]
  edge [
    source 117
    target 985
  ]
  edge [
    source 117
    target 741
  ]
  edge [
    source 117
    target 986
  ]
  edge [
    source 117
    target 797
  ]
  edge [
    source 117
    target 954
  ]
  edge [
    source 117
    target 987
  ]
  edge [
    source 117
    target 988
  ]
  edge [
    source 117
    target 918
  ]
  edge [
    source 117
    target 989
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 288
  ]
  edge [
    source 118
    target 990
  ]
  edge [
    source 118
    target 991
  ]
  edge [
    source 118
    target 992
  ]
  edge [
    source 118
    target 602
  ]
  edge [
    source 118
    target 993
  ]
  edge [
    source 118
    target 994
  ]
  edge [
    source 118
    target 995
  ]
  edge [
    source 118
    target 996
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 135
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 997
  ]
  edge [
    source 122
    target 1065
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 753
  ]
  edge [
    source 124
    target 998
  ]
  edge [
    source 124
    target 999
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 1000
  ]
  edge [
    source 126
    target 1001
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 1002
  ]
  edge [
    source 127
    target 1003
  ]
  edge [
    source 127
    target 563
  ]
  edge [
    source 127
    target 1004
  ]
  edge [
    source 127
    target 1005
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 1006
  ]
  edge [
    source 129
    target 254
  ]
  edge [
    source 129
    target 1007
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 130
    target 1008
  ]
  edge [
    source 130
    target 1009
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 1010
  ]
  edge [
    source 131
    target 1011
  ]
  edge [
    source 131
    target 1012
  ]
  edge [
    source 131
    target 1013
  ]
  edge [
    source 131
    target 1014
  ]
  edge [
    source 131
    target 1015
  ]
  edge [
    source 131
    target 1016
  ]
  edge [
    source 131
    target 681
  ]
  edge [
    source 131
    target 1017
  ]
  edge [
    source 131
    target 1018
  ]
  edge [
    source 131
    target 660
  ]
  edge [
    source 131
    target 1019
  ]
  edge [
    source 131
    target 1020
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 132
    target 1021
  ]
  edge [
    source 132
    target 1022
  ]
  edge [
    source 132
    target 1023
  ]
  edge [
    source 132
    target 1024
  ]
  edge [
    source 132
    target 1025
  ]
  edge [
    source 132
    target 1026
  ]
  edge [
    source 132
    target 171
  ]
  edge [
    source 132
    target 1027
  ]
  edge [
    source 132
    target 1028
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 133
    target 1029
  ]
  edge [
    source 133
    target 1030
  ]
  edge [
    source 133
    target 1031
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 134
    target 1032
  ]
  edge [
    source 134
    target 1033
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 135
    target 1034
  ]
  edge [
    source 135
    target 1035
  ]
  edge [
    source 135
    target 1036
  ]
  edge [
    source 135
    target 1037
  ]
  edge [
    source 135
    target 1038
  ]
  edge [
    source 135
    target 583
  ]
  edge [
    source 135
    target 1039
  ]
  edge [
    source 135
    target 1040
  ]
  edge [
    source 135
    target 1041
  ]
  edge [
    source 135
    target 1042
  ]
  edge [
    source 135
    target 982
  ]
  edge [
    source 136
    target 1043
  ]
  edge [
    source 136
    target 149
  ]
  edge [
    source 136
    target 1044
  ]
  edge [
    source 136
    target 1045
  ]
  edge [
    source 136
    target 1046
  ]
  edge [
    source 136
    target 1047
  ]
  edge [
    source 136
    target 1048
  ]
  edge [
    source 136
    target 1049
  ]
  edge [
    source 1052
    target 1053
  ]
  edge [
    source 1054
    target 1055
  ]
  edge [
    source 1056
    target 1057
  ]
  edge [
    source 1059
    target 1060
  ]
  edge [
    source 1061
    target 1062
  ]
  edge [
    source 1063
    target 1064
  ]
  edge [
    source 1066
    target 1067
  ]
  edge [
    source 1068
    target 1069
  ]
  edge [
    source 1068
    target 1070
  ]
  edge [
    source 1069
    target 1070
  ]
]
