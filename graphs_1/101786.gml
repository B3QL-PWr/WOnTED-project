graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.0625
  density 0.007186411149825784
  graphCliqueNumber 3
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "tylko"
    origin "text"
  ]
  node [
    id 2
    label "nauczy&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "obs&#322;ugiwa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "moje"
    origin "text"
  ]
  node [
    id 6
    label "nowy"
    origin "text"
  ]
  node [
    id 7
    label "laptop"
    origin "text"
  ]
  node [
    id 8
    label "pewno"
    origin "text"
  ]
  node [
    id 9
    label "co&#347;"
    origin "text"
  ]
  node [
    id 10
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 12
    label "z&#322;o&#347;liwy"
    origin "text"
  ]
  node [
    id 13
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 14
    label "nieczysty"
    origin "text"
  ]
  node [
    id 15
    label "by&#263;"
    origin "text"
  ]
  node [
    id 16
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 17
    label "kosmiczny"
    origin "text"
  ]
  node [
    id 18
    label "popsu&#263;"
    origin "text"
  ]
  node [
    id 19
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 20
    label "star"
    origin "text"
  ]
  node [
    id 21
    label "komp"
    origin "text"
  ]
  node [
    id 22
    label "teraz"
    origin "text"
  ]
  node [
    id 23
    label "&#263;wiczy&#263;"
    origin "text"
  ]
  node [
    id 24
    label "sztuka"
    origin "text"
  ]
  node [
    id 25
    label "trafia&#263;"
    origin "text"
  ]
  node [
    id 26
    label "klawisz"
    origin "text"
  ]
  node [
    id 27
    label "klawiatura"
    origin "text"
  ]
  node [
    id 28
    label "inny"
    origin "text"
  ]
  node [
    id 29
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 30
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 31
    label "lata"
    origin "text"
  ]
  node [
    id 32
    label "przywykn&#261;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 33
    label "mysz"
    origin "text"
  ]
  node [
    id 34
    label "sprawa"
    origin "text"
  ]
  node [
    id 35
    label "byd&#322;o"
  ]
  node [
    id 36
    label "zobo"
  ]
  node [
    id 37
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 38
    label "yakalo"
  ]
  node [
    id 39
    label "dzo"
  ]
  node [
    id 40
    label "instruct"
  ]
  node [
    id 41
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 42
    label "wyszkoli&#263;"
  ]
  node [
    id 43
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 44
    label "treat"
  ]
  node [
    id 45
    label "uprawia&#263;_seks"
  ]
  node [
    id 46
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 47
    label "serve"
  ]
  node [
    id 48
    label "zaspakaja&#263;"
  ]
  node [
    id 49
    label "suffice"
  ]
  node [
    id 50
    label "zaspokaja&#263;"
  ]
  node [
    id 51
    label "cz&#322;owiek"
  ]
  node [
    id 52
    label "nowotny"
  ]
  node [
    id 53
    label "drugi"
  ]
  node [
    id 54
    label "kolejny"
  ]
  node [
    id 55
    label "bie&#380;&#261;cy"
  ]
  node [
    id 56
    label "nowo"
  ]
  node [
    id 57
    label "narybek"
  ]
  node [
    id 58
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 59
    label "obcy"
  ]
  node [
    id 60
    label "touchpad"
  ]
  node [
    id 61
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 62
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 63
    label "Ultrabook"
  ]
  node [
    id 64
    label "thing"
  ]
  node [
    id 65
    label "cosik"
  ]
  node [
    id 66
    label "postawi&#263;"
  ]
  node [
    id 67
    label "prasa"
  ]
  node [
    id 68
    label "stworzy&#263;"
  ]
  node [
    id 69
    label "donie&#347;&#263;"
  ]
  node [
    id 70
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 71
    label "write"
  ]
  node [
    id 72
    label "styl"
  ]
  node [
    id 73
    label "read"
  ]
  node [
    id 74
    label "jako&#347;"
  ]
  node [
    id 75
    label "charakterystyczny"
  ]
  node [
    id 76
    label "ciekawy"
  ]
  node [
    id 77
    label "jako_tako"
  ]
  node [
    id 78
    label "dziwny"
  ]
  node [
    id 79
    label "niez&#322;y"
  ]
  node [
    id 80
    label "przyzwoity"
  ]
  node [
    id 81
    label "krytyczny"
  ]
  node [
    id 82
    label "prze&#347;miewczy"
  ]
  node [
    id 83
    label "z&#322;o&#347;liwie"
  ]
  node [
    id 84
    label "niemi&#322;y"
  ]
  node [
    id 85
    label "przekl&#281;ty"
  ]
  node [
    id 86
    label "celowy"
  ]
  node [
    id 87
    label "ci&#281;&#380;ki"
  ]
  node [
    id 88
    label "wojsko"
  ]
  node [
    id 89
    label "magnitude"
  ]
  node [
    id 90
    label "energia"
  ]
  node [
    id 91
    label "capacity"
  ]
  node [
    id 92
    label "wuchta"
  ]
  node [
    id 93
    label "cecha"
  ]
  node [
    id 94
    label "parametr"
  ]
  node [
    id 95
    label "moment_si&#322;y"
  ]
  node [
    id 96
    label "przemoc"
  ]
  node [
    id 97
    label "zdolno&#347;&#263;"
  ]
  node [
    id 98
    label "mn&#243;stwo"
  ]
  node [
    id 99
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 100
    label "rozwi&#261;zanie"
  ]
  node [
    id 101
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 102
    label "potencja"
  ]
  node [
    id 103
    label "zjawisko"
  ]
  node [
    id 104
    label "zaleta"
  ]
  node [
    id 105
    label "nieczysto"
  ]
  node [
    id 106
    label "nieca&#322;y"
  ]
  node [
    id 107
    label "przest&#281;pstwo"
  ]
  node [
    id 108
    label "brudno"
  ]
  node [
    id 109
    label "zabrudzenie_si&#281;"
  ]
  node [
    id 110
    label "brudzenie"
  ]
  node [
    id 111
    label "nieharmonijny"
  ]
  node [
    id 112
    label "chory"
  ]
  node [
    id 113
    label "ciecz"
  ]
  node [
    id 114
    label "nieostry"
  ]
  node [
    id 115
    label "zanieczyszczanie"
  ]
  node [
    id 116
    label "wspinaczka"
  ]
  node [
    id 117
    label "naganny"
  ]
  node [
    id 118
    label "niedoskona&#322;y"
  ]
  node [
    id 119
    label "m&#261;cenie"
  ]
  node [
    id 120
    label "fa&#322;szywie"
  ]
  node [
    id 121
    label "brudzenie_si&#281;"
  ]
  node [
    id 122
    label "rozbudowany"
  ]
  node [
    id 123
    label "nieprzejrzy&#347;cie"
  ]
  node [
    id 124
    label "z&#322;y"
  ]
  node [
    id 125
    label "ciemny"
  ]
  node [
    id 126
    label "zanieczyszczenie"
  ]
  node [
    id 127
    label "nielegalny"
  ]
  node [
    id 128
    label "nieudany"
  ]
  node [
    id 129
    label "niejednolity"
  ]
  node [
    id 130
    label "si&#281;ga&#263;"
  ]
  node [
    id 131
    label "trwa&#263;"
  ]
  node [
    id 132
    label "obecno&#347;&#263;"
  ]
  node [
    id 133
    label "stan"
  ]
  node [
    id 134
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 135
    label "stand"
  ]
  node [
    id 136
    label "mie&#263;_miejsce"
  ]
  node [
    id 137
    label "uczestniczy&#263;"
  ]
  node [
    id 138
    label "chodzi&#263;"
  ]
  node [
    id 139
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 140
    label "equal"
  ]
  node [
    id 141
    label "specjalny"
  ]
  node [
    id 142
    label "kosmicznie"
  ]
  node [
    id 143
    label "nieziemski"
  ]
  node [
    id 144
    label "futurystyczny"
  ]
  node [
    id 145
    label "pot&#281;&#380;ny"
  ]
  node [
    id 146
    label "astralny"
  ]
  node [
    id 147
    label "pozaziemski"
  ]
  node [
    id 148
    label "tajemniczy"
  ]
  node [
    id 149
    label "niestworzony"
  ]
  node [
    id 150
    label "olbrzymi"
  ]
  node [
    id 151
    label "zaszkodzi&#263;"
  ]
  node [
    id 152
    label "damage"
  ]
  node [
    id 153
    label "zdemoralizowa&#263;"
  ]
  node [
    id 154
    label "uszkodzi&#263;"
  ]
  node [
    id 155
    label "skopa&#263;"
  ]
  node [
    id 156
    label "wypierdoli&#263;_si&#281;"
  ]
  node [
    id 157
    label "spapra&#263;"
  ]
  node [
    id 158
    label "zjeba&#263;"
  ]
  node [
    id 159
    label "drop_the_ball"
  ]
  node [
    id 160
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 161
    label "zrobi&#263;"
  ]
  node [
    id 162
    label "pogorszy&#263;"
  ]
  node [
    id 163
    label "czyj&#347;"
  ]
  node [
    id 164
    label "m&#261;&#380;"
  ]
  node [
    id 165
    label "pami&#281;&#263;"
  ]
  node [
    id 166
    label "urz&#261;dzenie"
  ]
  node [
    id 167
    label "maszyna_Turinga"
  ]
  node [
    id 168
    label "emulacja"
  ]
  node [
    id 169
    label "botnet"
  ]
  node [
    id 170
    label "moc_obliczeniowa"
  ]
  node [
    id 171
    label "stacja_dysk&#243;w"
  ]
  node [
    id 172
    label "monitor"
  ]
  node [
    id 173
    label "instalowanie"
  ]
  node [
    id 174
    label "karta"
  ]
  node [
    id 175
    label "instalowa&#263;"
  ]
  node [
    id 176
    label "elektroniczna_maszyna_licz&#261;ca"
  ]
  node [
    id 177
    label "komputer"
  ]
  node [
    id 178
    label "pad"
  ]
  node [
    id 179
    label "zainstalowanie"
  ]
  node [
    id 180
    label "twardy_dysk"
  ]
  node [
    id 181
    label "radiator"
  ]
  node [
    id 182
    label "modem"
  ]
  node [
    id 183
    label "zainstalowa&#263;"
  ]
  node [
    id 184
    label "p&#322;yta_g&#322;&#243;wna"
  ]
  node [
    id 185
    label "procesor"
  ]
  node [
    id 186
    label "chwila"
  ]
  node [
    id 187
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 188
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 189
    label "ulepsza&#263;"
  ]
  node [
    id 190
    label "train"
  ]
  node [
    id 191
    label "ch&#322;osta&#263;"
  ]
  node [
    id 192
    label "hone"
  ]
  node [
    id 193
    label "doskonali&#263;"
  ]
  node [
    id 194
    label "discipline"
  ]
  node [
    id 195
    label "theatrical_performance"
  ]
  node [
    id 196
    label "sprawno&#347;&#263;"
  ]
  node [
    id 197
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 198
    label "ods&#322;ona"
  ]
  node [
    id 199
    label "przedmiot"
  ]
  node [
    id 200
    label "Faust"
  ]
  node [
    id 201
    label "jednostka"
  ]
  node [
    id 202
    label "fortel"
  ]
  node [
    id 203
    label "environment"
  ]
  node [
    id 204
    label "rola"
  ]
  node [
    id 205
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 206
    label "utw&#243;r"
  ]
  node [
    id 207
    label "egzemplarz"
  ]
  node [
    id 208
    label "realizacja"
  ]
  node [
    id 209
    label "turn"
  ]
  node [
    id 210
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 211
    label "scenografia"
  ]
  node [
    id 212
    label "kultura_duchowa"
  ]
  node [
    id 213
    label "ilo&#347;&#263;"
  ]
  node [
    id 214
    label "pokaz"
  ]
  node [
    id 215
    label "przedstawia&#263;"
  ]
  node [
    id 216
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 217
    label "pr&#243;bowanie"
  ]
  node [
    id 218
    label "kobieta"
  ]
  node [
    id 219
    label "scena"
  ]
  node [
    id 220
    label "przedstawianie"
  ]
  node [
    id 221
    label "scenariusz"
  ]
  node [
    id 222
    label "didaskalia"
  ]
  node [
    id 223
    label "Apollo"
  ]
  node [
    id 224
    label "czyn"
  ]
  node [
    id 225
    label "przedstawienie"
  ]
  node [
    id 226
    label "head"
  ]
  node [
    id 227
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 228
    label "przedstawi&#263;"
  ]
  node [
    id 229
    label "ambala&#380;"
  ]
  node [
    id 230
    label "kultura"
  ]
  node [
    id 231
    label "towar"
  ]
  node [
    id 232
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 233
    label "wpada&#263;"
  ]
  node [
    id 234
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 235
    label "dolatywa&#263;"
  ]
  node [
    id 236
    label "happen"
  ]
  node [
    id 237
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 238
    label "hit"
  ]
  node [
    id 239
    label "spotyka&#263;"
  ]
  node [
    id 240
    label "indicate"
  ]
  node [
    id 241
    label "znajdowa&#263;"
  ]
  node [
    id 242
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 243
    label "pocisk"
  ]
  node [
    id 244
    label "dociera&#263;"
  ]
  node [
    id 245
    label "dopasowywa&#263;_si&#281;"
  ]
  node [
    id 246
    label "przycisk"
  ]
  node [
    id 247
    label "kalkulator"
  ]
  node [
    id 248
    label "stra&#380;nik_wi&#281;zienny"
  ]
  node [
    id 249
    label "uk&#322;ad"
  ]
  node [
    id 250
    label "wystukiwanie"
  ]
  node [
    id 251
    label "uz&#281;bienie"
  ]
  node [
    id 252
    label "wysuwka"
  ]
  node [
    id 253
    label "wystuka&#263;"
  ]
  node [
    id 254
    label "wystukiwa&#263;"
  ]
  node [
    id 255
    label "Bajca"
  ]
  node [
    id 256
    label "wystukanie"
  ]
  node [
    id 257
    label "narz&#281;dzie"
  ]
  node [
    id 258
    label "palc&#243;wka"
  ]
  node [
    id 259
    label "inaczej"
  ]
  node [
    id 260
    label "r&#243;&#380;ny"
  ]
  node [
    id 261
    label "inszy"
  ]
  node [
    id 262
    label "osobno"
  ]
  node [
    id 263
    label "poziom"
  ]
  node [
    id 264
    label "faza"
  ]
  node [
    id 265
    label "depression"
  ]
  node [
    id 266
    label "nizina"
  ]
  node [
    id 267
    label "summer"
  ]
  node [
    id 268
    label "czas"
  ]
  node [
    id 269
    label "urz&#261;dzenie_wskazuj&#261;ce"
  ]
  node [
    id 270
    label "gryzo&#324;"
  ]
  node [
    id 271
    label "piszcze&#263;"
  ]
  node [
    id 272
    label "bary&#322;ka"
  ]
  node [
    id 273
    label "klawisz_myszki"
  ]
  node [
    id 274
    label "zapiszcze&#263;"
  ]
  node [
    id 275
    label "myszy_w&#322;a&#347;ciwe"
  ]
  node [
    id 276
    label "temat"
  ]
  node [
    id 277
    label "kognicja"
  ]
  node [
    id 278
    label "idea"
  ]
  node [
    id 279
    label "szczeg&#243;&#322;"
  ]
  node [
    id 280
    label "rzecz"
  ]
  node [
    id 281
    label "wydarzenie"
  ]
  node [
    id 282
    label "przes&#322;anka"
  ]
  node [
    id 283
    label "rozprawa"
  ]
  node [
    id 284
    label "object"
  ]
  node [
    id 285
    label "proposition"
  ]
  node [
    id 286
    label "Alice"
  ]
  node [
    id 287
    label "Miller"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 76
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 174
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 188
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 51
  ]
  edge [
    source 24
    target 196
  ]
  edge [
    source 24
    target 197
  ]
  edge [
    source 24
    target 198
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 24
    target 201
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 24
    target 225
  ]
  edge [
    source 24
    target 226
  ]
  edge [
    source 24
    target 227
  ]
  edge [
    source 24
    target 228
  ]
  edge [
    source 24
    target 229
  ]
  edge [
    source 24
    target 230
  ]
  edge [
    source 24
    target 231
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 232
  ]
  edge [
    source 25
    target 233
  ]
  edge [
    source 25
    target 234
  ]
  edge [
    source 25
    target 235
  ]
  edge [
    source 25
    target 236
  ]
  edge [
    source 25
    target 237
  ]
  edge [
    source 25
    target 238
  ]
  edge [
    source 25
    target 239
  ]
  edge [
    source 25
    target 240
  ]
  edge [
    source 25
    target 241
  ]
  edge [
    source 25
    target 242
  ]
  edge [
    source 25
    target 243
  ]
  edge [
    source 25
    target 244
  ]
  edge [
    source 25
    target 245
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 26
    target 248
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 249
  ]
  edge [
    source 27
    target 250
  ]
  edge [
    source 27
    target 251
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 253
  ]
  edge [
    source 27
    target 254
  ]
  edge [
    source 27
    target 255
  ]
  edge [
    source 27
    target 256
  ]
  edge [
    source 27
    target 257
  ]
  edge [
    source 27
    target 258
  ]
  edge [
    source 27
    target 177
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 54
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 262
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 263
  ]
  edge [
    source 29
    target 264
  ]
  edge [
    source 29
    target 265
  ]
  edge [
    source 29
    target 103
  ]
  edge [
    source 29
    target 266
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 267
  ]
  edge [
    source 31
    target 268
  ]
  edge [
    source 33
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 269
  ]
  edge [
    source 33
    target 270
  ]
  edge [
    source 33
    target 271
  ]
  edge [
    source 33
    target 177
  ]
  edge [
    source 33
    target 272
  ]
  edge [
    source 33
    target 273
  ]
  edge [
    source 33
    target 274
  ]
  edge [
    source 33
    target 275
  ]
  edge [
    source 34
    target 276
  ]
  edge [
    source 34
    target 277
  ]
  edge [
    source 34
    target 278
  ]
  edge [
    source 34
    target 279
  ]
  edge [
    source 34
    target 280
  ]
  edge [
    source 34
    target 281
  ]
  edge [
    source 34
    target 282
  ]
  edge [
    source 34
    target 283
  ]
  edge [
    source 34
    target 284
  ]
  edge [
    source 34
    target 285
  ]
  edge [
    source 286
    target 287
  ]
]
