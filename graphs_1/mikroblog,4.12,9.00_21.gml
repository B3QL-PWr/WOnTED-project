graph [
  maxDegree 23
  minDegree 1
  meanDegree 2.02
  density 0.020404040404040404
  graphCliqueNumber 2
  node [
    id 0
    label "dobry"
    origin "text"
  ]
  node [
    id 1
    label "taktyka"
    origin "text"
  ]
  node [
    id 2
    label "mecz"
    origin "text"
  ]
  node [
    id 3
    label "polska"
    origin "text"
  ]
  node [
    id 4
    label "izrael"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "&#322;apa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "&#380;yd"
    origin "text"
  ]
  node [
    id 8
    label "spalony"
    origin "text"
  ]
  node [
    id 9
    label "xdddd"
    origin "text"
  ]
  node [
    id 10
    label "polski"
    origin "text"
  ]
  node [
    id 11
    label "pilkanozna"
    origin "text"
  ]
  node [
    id 12
    label "heheszki"
    origin "text"
  ]
  node [
    id 13
    label "smieszne"
    origin "text"
  ]
  node [
    id 14
    label "pomy&#347;lny"
  ]
  node [
    id 15
    label "skuteczny"
  ]
  node [
    id 16
    label "moralny"
  ]
  node [
    id 17
    label "korzystny"
  ]
  node [
    id 18
    label "odpowiedni"
  ]
  node [
    id 19
    label "zwrot"
  ]
  node [
    id 20
    label "dobrze"
  ]
  node [
    id 21
    label "pozytywny"
  ]
  node [
    id 22
    label "grzeczny"
  ]
  node [
    id 23
    label "powitanie"
  ]
  node [
    id 24
    label "mi&#322;y"
  ]
  node [
    id 25
    label "dobroczynny"
  ]
  node [
    id 26
    label "pos&#322;uszny"
  ]
  node [
    id 27
    label "ca&#322;y"
  ]
  node [
    id 28
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 29
    label "czw&#243;rka"
  ]
  node [
    id 30
    label "spokojny"
  ]
  node [
    id 31
    label "&#347;mieszny"
  ]
  node [
    id 32
    label "drogi"
  ]
  node [
    id 33
    label "manewr"
  ]
  node [
    id 34
    label "pocz&#261;tki"
  ]
  node [
    id 35
    label "metoda"
  ]
  node [
    id 36
    label "zwiad"
  ]
  node [
    id 37
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 38
    label "wrinkle"
  ]
  node [
    id 39
    label "obrona"
  ]
  node [
    id 40
    label "gra"
  ]
  node [
    id 41
    label "dwumecz"
  ]
  node [
    id 42
    label "game"
  ]
  node [
    id 43
    label "serw"
  ]
  node [
    id 44
    label "si&#281;ga&#263;"
  ]
  node [
    id 45
    label "trwa&#263;"
  ]
  node [
    id 46
    label "obecno&#347;&#263;"
  ]
  node [
    id 47
    label "stan"
  ]
  node [
    id 48
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 49
    label "stand"
  ]
  node [
    id 50
    label "mie&#263;_miejsce"
  ]
  node [
    id 51
    label "uczestniczy&#263;"
  ]
  node [
    id 52
    label "chodzi&#263;"
  ]
  node [
    id 53
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 54
    label "equal"
  ]
  node [
    id 55
    label "get"
  ]
  node [
    id 56
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 57
    label "ogarnia&#263;"
  ]
  node [
    id 58
    label "cope"
  ]
  node [
    id 59
    label "zabiera&#263;"
  ]
  node [
    id 60
    label "d&#322;o&#324;"
  ]
  node [
    id 61
    label "rozumie&#263;"
  ]
  node [
    id 62
    label "bra&#263;"
  ]
  node [
    id 63
    label "ujmowa&#263;"
  ]
  node [
    id 64
    label "zara&#380;a&#263;_si&#281;"
  ]
  node [
    id 65
    label "chciwiec"
  ]
  node [
    id 66
    label "judenrat"
  ]
  node [
    id 67
    label "materialista"
  ]
  node [
    id 68
    label "sk&#261;piarz"
  ]
  node [
    id 69
    label "wyznawca"
  ]
  node [
    id 70
    label "&#379;ydziak"
  ]
  node [
    id 71
    label "sk&#261;py"
  ]
  node [
    id 72
    label "szmonces"
  ]
  node [
    id 73
    label "monoteista"
  ]
  node [
    id 74
    label "mosiek"
  ]
  node [
    id 75
    label "zagrywka"
  ]
  node [
    id 76
    label "zgorza&#322;y"
  ]
  node [
    id 77
    label "zniszczony"
  ]
  node [
    id 78
    label "wykroczenie"
  ]
  node [
    id 79
    label "lacki"
  ]
  node [
    id 80
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 81
    label "przedmiot"
  ]
  node [
    id 82
    label "sztajer"
  ]
  node [
    id 83
    label "drabant"
  ]
  node [
    id 84
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 85
    label "polak"
  ]
  node [
    id 86
    label "pierogi_ruskie"
  ]
  node [
    id 87
    label "krakowiak"
  ]
  node [
    id 88
    label "Polish"
  ]
  node [
    id 89
    label "j&#281;zyk"
  ]
  node [
    id 90
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 91
    label "oberek"
  ]
  node [
    id 92
    label "po_polsku"
  ]
  node [
    id 93
    label "mazur"
  ]
  node [
    id 94
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 95
    label "chodzony"
  ]
  node [
    id 96
    label "skoczny"
  ]
  node [
    id 97
    label "ryba_po_grecku"
  ]
  node [
    id 98
    label "goniony"
  ]
  node [
    id 99
    label "polsko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
]
