graph [
  maxDegree 17
  minDegree 1
  meanDegree 2
  density 0.02531645569620253
  graphCliqueNumber 2
  node [
    id 0
    label "si&#281;"
    origin "text"
  ]
  node [
    id 1
    label "tutaj"
    origin "text"
  ]
  node [
    id 2
    label "zdarzy&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 4
    label "zabawny"
    origin "text"
  ]
  node [
    id 5
    label "tym"
    origin "text"
  ]
  node [
    id 6
    label "polega&#263;"
    origin "text"
  ]
  node [
    id 7
    label "problem"
    origin "text"
  ]
  node [
    id 8
    label "internet"
    origin "text"
  ]
  node [
    id 9
    label "rzecz"
    origin "text"
  ]
  node [
    id 10
    label "przemieszcza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "zbyt"
    origin "text"
  ]
  node [
    id 12
    label "pr&#281;dko"
    origin "text"
  ]
  node [
    id 13
    label "tam"
  ]
  node [
    id 14
    label "mo&#380;liwie"
  ]
  node [
    id 15
    label "nieznaczny"
  ]
  node [
    id 16
    label "kr&#243;tko"
  ]
  node [
    id 17
    label "nieistotnie"
  ]
  node [
    id 18
    label "nieliczny"
  ]
  node [
    id 19
    label "mikroskopijnie"
  ]
  node [
    id 20
    label "pomiernie"
  ]
  node [
    id 21
    label "ma&#322;y"
  ]
  node [
    id 22
    label "bawny"
  ]
  node [
    id 23
    label "&#347;miesznie"
  ]
  node [
    id 24
    label "ufa&#263;"
  ]
  node [
    id 25
    label "consist"
  ]
  node [
    id 26
    label "trust"
  ]
  node [
    id 27
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 28
    label "trudno&#347;&#263;"
  ]
  node [
    id 29
    label "sprawa"
  ]
  node [
    id 30
    label "ambaras"
  ]
  node [
    id 31
    label "problemat"
  ]
  node [
    id 32
    label "pierepa&#322;ka"
  ]
  node [
    id 33
    label "obstruction"
  ]
  node [
    id 34
    label "problematyka"
  ]
  node [
    id 35
    label "jajko_Kolumba"
  ]
  node [
    id 36
    label "subiekcja"
  ]
  node [
    id 37
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 38
    label "us&#322;uga_internetowa"
  ]
  node [
    id 39
    label "biznes_elektroniczny"
  ]
  node [
    id 40
    label "punkt_dost&#281;pu"
  ]
  node [
    id 41
    label "hipertekst"
  ]
  node [
    id 42
    label "gra_sieciowa"
  ]
  node [
    id 43
    label "mem"
  ]
  node [
    id 44
    label "e-hazard"
  ]
  node [
    id 45
    label "sie&#263;_komputerowa"
  ]
  node [
    id 46
    label "media"
  ]
  node [
    id 47
    label "podcast"
  ]
  node [
    id 48
    label "netbook"
  ]
  node [
    id 49
    label "provider"
  ]
  node [
    id 50
    label "cyberprzestrze&#324;"
  ]
  node [
    id 51
    label "grooming"
  ]
  node [
    id 52
    label "strona"
  ]
  node [
    id 53
    label "obiekt"
  ]
  node [
    id 54
    label "temat"
  ]
  node [
    id 55
    label "istota"
  ]
  node [
    id 56
    label "wpa&#347;&#263;"
  ]
  node [
    id 57
    label "wpadanie"
  ]
  node [
    id 58
    label "przedmiot"
  ]
  node [
    id 59
    label "wpada&#263;"
  ]
  node [
    id 60
    label "kultura"
  ]
  node [
    id 61
    label "przyroda"
  ]
  node [
    id 62
    label "mienie"
  ]
  node [
    id 63
    label "object"
  ]
  node [
    id 64
    label "wpadni&#281;cie"
  ]
  node [
    id 65
    label "translokowa&#263;"
  ]
  node [
    id 66
    label "robi&#263;"
  ]
  node [
    id 67
    label "powodowa&#263;"
  ]
  node [
    id 68
    label "go"
  ]
  node [
    id 69
    label "nadmiernie"
  ]
  node [
    id 70
    label "sprzedawanie"
  ]
  node [
    id 71
    label "sprzeda&#380;"
  ]
  node [
    id 72
    label "quicker"
  ]
  node [
    id 73
    label "promptly"
  ]
  node [
    id 74
    label "quickest"
  ]
  node [
    id 75
    label "sprawnie"
  ]
  node [
    id 76
    label "dynamicznie"
  ]
  node [
    id 77
    label "szybciej"
  ]
  node [
    id 78
    label "szybciochem"
  ]
  node [
    id 79
    label "szybki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
]
