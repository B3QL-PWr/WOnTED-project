graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.0833333333333335
  density 0.01750700280112045
  graphCliqueNumber 2
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kolejny"
    origin "text"
  ]
  node [
    id 2
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "zabawa"
    origin "text"
  ]
  node [
    id 4
    label "mirkoangielski"
    origin "text"
  ]
  node [
    id 5
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 6
    label "kto"
    origin "text"
  ]
  node [
    id 7
    label "zaplusuje"
    origin "text"
  ]
  node [
    id 8
    label "ten"
    origin "text"
  ]
  node [
    id 9
    label "wpis"
    origin "text"
  ]
  node [
    id 10
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "nast&#281;pny"
    origin "text"
  ]
  node [
    id 12
    label "nowa"
    origin "text"
  ]
  node [
    id 13
    label "s&#322;&#243;wko"
    origin "text"
  ]
  node [
    id 14
    label "nauka"
    origin "text"
  ]
  node [
    id 15
    label "invite"
  ]
  node [
    id 16
    label "ask"
  ]
  node [
    id 17
    label "oferowa&#263;"
  ]
  node [
    id 18
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 19
    label "inny"
  ]
  node [
    id 20
    label "nast&#281;pnie"
  ]
  node [
    id 21
    label "kt&#243;ry&#347;"
  ]
  node [
    id 22
    label "kolejno"
  ]
  node [
    id 23
    label "nastopny"
  ]
  node [
    id 24
    label "s&#322;o&#324;ce"
  ]
  node [
    id 25
    label "czynienie_si&#281;"
  ]
  node [
    id 26
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 27
    label "czas"
  ]
  node [
    id 28
    label "long_time"
  ]
  node [
    id 29
    label "przedpo&#322;udnie"
  ]
  node [
    id 30
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 31
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 32
    label "tydzie&#324;"
  ]
  node [
    id 33
    label "godzina"
  ]
  node [
    id 34
    label "t&#322;usty_czwartek"
  ]
  node [
    id 35
    label "wsta&#263;"
  ]
  node [
    id 36
    label "day"
  ]
  node [
    id 37
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 38
    label "przedwiecz&#243;r"
  ]
  node [
    id 39
    label "Sylwester"
  ]
  node [
    id 40
    label "po&#322;udnie"
  ]
  node [
    id 41
    label "wzej&#347;cie"
  ]
  node [
    id 42
    label "podwiecz&#243;r"
  ]
  node [
    id 43
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 44
    label "rano"
  ]
  node [
    id 45
    label "termin"
  ]
  node [
    id 46
    label "ranek"
  ]
  node [
    id 47
    label "doba"
  ]
  node [
    id 48
    label "wiecz&#243;r"
  ]
  node [
    id 49
    label "walentynki"
  ]
  node [
    id 50
    label "popo&#322;udnie"
  ]
  node [
    id 51
    label "noc"
  ]
  node [
    id 52
    label "wstanie"
  ]
  node [
    id 53
    label "wodzirej"
  ]
  node [
    id 54
    label "rozrywka"
  ]
  node [
    id 55
    label "nabawienie_si&#281;"
  ]
  node [
    id 56
    label "cecha"
  ]
  node [
    id 57
    label "gambling"
  ]
  node [
    id 58
    label "taniec"
  ]
  node [
    id 59
    label "impreza"
  ]
  node [
    id 60
    label "igraszka"
  ]
  node [
    id 61
    label "igra"
  ]
  node [
    id 62
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 63
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 64
    label "game"
  ]
  node [
    id 65
    label "ubaw"
  ]
  node [
    id 66
    label "chwyt"
  ]
  node [
    id 67
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 68
    label "jaki&#347;"
  ]
  node [
    id 69
    label "okre&#347;lony"
  ]
  node [
    id 70
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 71
    label "czynno&#347;&#263;"
  ]
  node [
    id 72
    label "entrance"
  ]
  node [
    id 73
    label "inscription"
  ]
  node [
    id 74
    label "akt"
  ]
  node [
    id 75
    label "op&#322;ata"
  ]
  node [
    id 76
    label "tekst"
  ]
  node [
    id 77
    label "get"
  ]
  node [
    id 78
    label "doczeka&#263;"
  ]
  node [
    id 79
    label "zwiastun"
  ]
  node [
    id 80
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 81
    label "develop"
  ]
  node [
    id 82
    label "catch"
  ]
  node [
    id 83
    label "uzyska&#263;"
  ]
  node [
    id 84
    label "kupi&#263;"
  ]
  node [
    id 85
    label "wzi&#261;&#263;"
  ]
  node [
    id 86
    label "naby&#263;"
  ]
  node [
    id 87
    label "obskoczy&#263;"
  ]
  node [
    id 88
    label "zapanowa&#263;"
  ]
  node [
    id 89
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 90
    label "zrobi&#263;"
  ]
  node [
    id 91
    label "nabawianie_si&#281;"
  ]
  node [
    id 92
    label "range"
  ]
  node [
    id 93
    label "schorzenie"
  ]
  node [
    id 94
    label "wystarczy&#263;"
  ]
  node [
    id 95
    label "wysta&#263;"
  ]
  node [
    id 96
    label "gwiazda"
  ]
  node [
    id 97
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 98
    label "nauki_o_Ziemi"
  ]
  node [
    id 99
    label "teoria_naukowa"
  ]
  node [
    id 100
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 101
    label "nauki_o_poznaniu"
  ]
  node [
    id 102
    label "nomotetyczny"
  ]
  node [
    id 103
    label "metodologia"
  ]
  node [
    id 104
    label "przem&#243;wienie"
  ]
  node [
    id 105
    label "wiedza"
  ]
  node [
    id 106
    label "kultura_duchowa"
  ]
  node [
    id 107
    label "nauki_penalne"
  ]
  node [
    id 108
    label "systematyka"
  ]
  node [
    id 109
    label "inwentyka"
  ]
  node [
    id 110
    label "dziedzina"
  ]
  node [
    id 111
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 112
    label "miasteczko_rowerowe"
  ]
  node [
    id 113
    label "fotowoltaika"
  ]
  node [
    id 114
    label "porada"
  ]
  node [
    id 115
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 116
    label "proces"
  ]
  node [
    id 117
    label "imagineskopia"
  ]
  node [
    id 118
    label "typologia"
  ]
  node [
    id 119
    label "&#322;awa_szkolna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
]
