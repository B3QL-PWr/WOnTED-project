graph [
  maxDegree 29
  minDegree 1
  meanDegree 2
  density 0.016
  graphCliqueNumber 2
  node [
    id 0
    label "cytat"
    origin "text"
  ]
  node [
    id 1
    label "moi"
    origin "text"
  ]
  node [
    id 2
    label "zdanie"
    origin "text"
  ]
  node [
    id 3
    label "tak"
    origin "text"
  ]
  node [
    id 4
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 5
    label "wolumen"
    origin "text"
  ]
  node [
    id 6
    label "pokazowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "aktywno&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "firma"
    origin "text"
  ]
  node [
    id 9
    label "inwestycyjny"
    origin "text"
  ]
  node [
    id 10
    label "uczestnik"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "posiada&#263;"
    origin "text"
  ]
  node [
    id 13
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 14
    label "emisja"
    origin "text"
  ]
  node [
    id 15
    label "portfel"
    origin "text"
  ]
  node [
    id 16
    label "finansowy"
    origin "text"
  ]
  node [
    id 17
    label "wyimek"
  ]
  node [
    id 18
    label "ekscerptor"
  ]
  node [
    id 19
    label "alegacja"
  ]
  node [
    id 20
    label "konkordancja"
  ]
  node [
    id 21
    label "fragment"
  ]
  node [
    id 22
    label "attitude"
  ]
  node [
    id 23
    label "system"
  ]
  node [
    id 24
    label "przedstawienie"
  ]
  node [
    id 25
    label "fraza"
  ]
  node [
    id 26
    label "prison_term"
  ]
  node [
    id 27
    label "adjudication"
  ]
  node [
    id 28
    label "przekazanie"
  ]
  node [
    id 29
    label "pass"
  ]
  node [
    id 30
    label "wyra&#380;enie"
  ]
  node [
    id 31
    label "okres"
  ]
  node [
    id 32
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 33
    label "wypowiedzenie"
  ]
  node [
    id 34
    label "konektyw"
  ]
  node [
    id 35
    label "zaliczenie"
  ]
  node [
    id 36
    label "stanowisko"
  ]
  node [
    id 37
    label "powierzenie"
  ]
  node [
    id 38
    label "antylogizm"
  ]
  node [
    id 39
    label "zmuszenie"
  ]
  node [
    id 40
    label "szko&#322;a"
  ]
  node [
    id 41
    label "doros&#322;y"
  ]
  node [
    id 42
    label "wiele"
  ]
  node [
    id 43
    label "dorodny"
  ]
  node [
    id 44
    label "znaczny"
  ]
  node [
    id 45
    label "du&#380;o"
  ]
  node [
    id 46
    label "prawdziwy"
  ]
  node [
    id 47
    label "niema&#322;o"
  ]
  node [
    id 48
    label "wa&#380;ny"
  ]
  node [
    id 49
    label "rozwini&#281;ty"
  ]
  node [
    id 50
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 51
    label "brzmienie"
  ]
  node [
    id 52
    label "g&#322;o&#347;no&#347;&#263;"
  ]
  node [
    id 53
    label "sztuka"
  ]
  node [
    id 54
    label "pi&#281;cioksi&#261;g"
  ]
  node [
    id 55
    label "stan"
  ]
  node [
    id 56
    label "kanciasty"
  ]
  node [
    id 57
    label "commercial_enterprise"
  ]
  node [
    id 58
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 59
    label "action"
  ]
  node [
    id 60
    label "proces"
  ]
  node [
    id 61
    label "postawa"
  ]
  node [
    id 62
    label "cz&#322;owiek"
  ]
  node [
    id 63
    label "Hortex"
  ]
  node [
    id 64
    label "MAC"
  ]
  node [
    id 65
    label "reengineering"
  ]
  node [
    id 66
    label "nazwa_w&#322;asna"
  ]
  node [
    id 67
    label "podmiot_gospodarczy"
  ]
  node [
    id 68
    label "Google"
  ]
  node [
    id 69
    label "zaufanie"
  ]
  node [
    id 70
    label "biurowiec"
  ]
  node [
    id 71
    label "networking"
  ]
  node [
    id 72
    label "zasoby_ludzkie"
  ]
  node [
    id 73
    label "interes"
  ]
  node [
    id 74
    label "paczkarnia"
  ]
  node [
    id 75
    label "Canon"
  ]
  node [
    id 76
    label "HP"
  ]
  node [
    id 77
    label "Baltona"
  ]
  node [
    id 78
    label "Pewex"
  ]
  node [
    id 79
    label "MAN_SE"
  ]
  node [
    id 80
    label "Apeks"
  ]
  node [
    id 81
    label "zasoby"
  ]
  node [
    id 82
    label "Orbis"
  ]
  node [
    id 83
    label "miejsce_pracy"
  ]
  node [
    id 84
    label "siedziba"
  ]
  node [
    id 85
    label "Spo&#322;em"
  ]
  node [
    id 86
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 87
    label "Orlen"
  ]
  node [
    id 88
    label "klasa"
  ]
  node [
    id 89
    label "inwestycyjnie"
  ]
  node [
    id 90
    label "wiedzie&#263;"
  ]
  node [
    id 91
    label "mie&#263;"
  ]
  node [
    id 92
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 93
    label "keep_open"
  ]
  node [
    id 94
    label "zawiera&#263;"
  ]
  node [
    id 95
    label "support"
  ]
  node [
    id 96
    label "zdolno&#347;&#263;"
  ]
  node [
    id 97
    label "dokument"
  ]
  node [
    id 98
    label "czynno&#347;&#263;"
  ]
  node [
    id 99
    label "spowodowanie"
  ]
  node [
    id 100
    label "title"
  ]
  node [
    id 101
    label "law"
  ]
  node [
    id 102
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 103
    label "authorization"
  ]
  node [
    id 104
    label "expense"
  ]
  node [
    id 105
    label "consequence"
  ]
  node [
    id 106
    label "wydobywanie"
  ]
  node [
    id 107
    label "g&#322;os"
  ]
  node [
    id 108
    label "introdukcja"
  ]
  node [
    id 109
    label "przesy&#322;"
  ]
  node [
    id 110
    label "wydzielanie"
  ]
  node [
    id 111
    label "zjawisko"
  ]
  node [
    id 112
    label "publikacja"
  ]
  node [
    id 113
    label "pojemnik"
  ]
  node [
    id 114
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 115
    label "bag"
  ]
  node [
    id 116
    label "bud&#380;et"
  ]
  node [
    id 117
    label "galanteria"
  ]
  node [
    id 118
    label "pugilares"
  ]
  node [
    id 119
    label "pieni&#261;dze"
  ]
  node [
    id 120
    label "zas&#243;b"
  ]
  node [
    id 121
    label "mi&#281;dzybankowy"
  ]
  node [
    id 122
    label "finansowo"
  ]
  node [
    id 123
    label "fizyczny"
  ]
  node [
    id 124
    label "pozamaterialny"
  ]
  node [
    id 125
    label "materjalny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
]
