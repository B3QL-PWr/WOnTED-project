graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.813953488372093
  density 0.04318936877076412
  graphCliqueNumber 3
  node [
    id 0
    label "ulica"
    origin "text"
  ]
  node [
    id 1
    label "gminny"
    origin "text"
  ]
  node [
    id 2
    label "zamo&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "&#347;rodowisko"
  ]
  node [
    id 4
    label "miasteczko"
  ]
  node [
    id 5
    label "streetball"
  ]
  node [
    id 6
    label "pierzeja"
  ]
  node [
    id 7
    label "grupa"
  ]
  node [
    id 8
    label "pas_ruchu"
  ]
  node [
    id 9
    label "jezdnia"
  ]
  node [
    id 10
    label "pas_rozdzielczy"
  ]
  node [
    id 11
    label "droga"
  ]
  node [
    id 12
    label "korona_drogi"
  ]
  node [
    id 13
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 14
    label "chodnik"
  ]
  node [
    id 15
    label "arteria"
  ]
  node [
    id 16
    label "Broadway"
  ]
  node [
    id 17
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 18
    label "wysepka"
  ]
  node [
    id 19
    label "autostrada"
  ]
  node [
    id 20
    label "po_prostacku"
  ]
  node [
    id 21
    label "przedpokojowy"
  ]
  node [
    id 22
    label "pospolity"
  ]
  node [
    id 23
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 24
    label "ludowy"
  ]
  node [
    id 25
    label "prymas"
  ]
  node [
    id 26
    label "syn"
  ]
  node [
    id 27
    label "Wyszy&#324;ski"
  ]
  node [
    id 28
    label "nowy"
  ]
  node [
    id 29
    label "rynek"
  ]
  node [
    id 30
    label "DH"
  ]
  node [
    id 31
    label "agora"
  ]
  node [
    id 32
    label "Mieszko"
  ]
  node [
    id 33
    label "Resta"
  ]
  node [
    id 34
    label "dworzec"
  ]
  node [
    id 35
    label "PKS"
  ]
  node [
    id 36
    label "Tomasz"
  ]
  node [
    id 37
    label "ii"
  ]
  node [
    id 38
    label "wojna"
  ]
  node [
    id 39
    label "&#347;wiatowy"
  ]
  node [
    id 40
    label "zb&#243;r"
  ]
  node [
    id 41
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 42
    label "zielono&#347;wi&#261;tkowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 41
    target 42
  ]
]
