graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9583333333333333
  density 0.041666666666666664
  graphCliqueNumber 2
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wszyscy"
    origin "text"
  ]
  node [
    id 2
    label "lektura"
    origin "text"
  ]
  node [
    id 3
    label "nowy"
    origin "text"
  ]
  node [
    id 4
    label "numer"
    origin "text"
  ]
  node [
    id 5
    label "outro"
    origin "text"
  ]
  node [
    id 6
    label "invite"
  ]
  node [
    id 7
    label "ask"
  ]
  node [
    id 8
    label "oferowa&#263;"
  ]
  node [
    id 9
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 10
    label "wyczytywanie"
  ]
  node [
    id 11
    label "doczytywanie"
  ]
  node [
    id 12
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 13
    label "oczytywanie_si&#281;"
  ]
  node [
    id 14
    label "zaczytanie_si&#281;"
  ]
  node [
    id 15
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 16
    label "czytywanie"
  ]
  node [
    id 17
    label "recitation"
  ]
  node [
    id 18
    label "poznawanie"
  ]
  node [
    id 19
    label "poczytanie"
  ]
  node [
    id 20
    label "wczytywanie_si&#281;"
  ]
  node [
    id 21
    label "tekst"
  ]
  node [
    id 22
    label "cz&#322;owiek"
  ]
  node [
    id 23
    label "nowotny"
  ]
  node [
    id 24
    label "drugi"
  ]
  node [
    id 25
    label "kolejny"
  ]
  node [
    id 26
    label "bie&#380;&#261;cy"
  ]
  node [
    id 27
    label "nowo"
  ]
  node [
    id 28
    label "narybek"
  ]
  node [
    id 29
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 30
    label "obcy"
  ]
  node [
    id 31
    label "manewr"
  ]
  node [
    id 32
    label "sztos"
  ]
  node [
    id 33
    label "pok&#243;j"
  ]
  node [
    id 34
    label "facet"
  ]
  node [
    id 35
    label "wyst&#281;p"
  ]
  node [
    id 36
    label "turn"
  ]
  node [
    id 37
    label "impression"
  ]
  node [
    id 38
    label "hotel"
  ]
  node [
    id 39
    label "liczba"
  ]
  node [
    id 40
    label "punkt"
  ]
  node [
    id 41
    label "czasopismo"
  ]
  node [
    id 42
    label "&#380;art"
  ]
  node [
    id 43
    label "orygina&#322;"
  ]
  node [
    id 44
    label "oznaczenie"
  ]
  node [
    id 45
    label "zi&#243;&#322;ko"
  ]
  node [
    id 46
    label "akt_p&#322;ciowy"
  ]
  node [
    id 47
    label "publikacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
]
