graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 2
    label "smieszne"
    origin "text"
  ]
  node [
    id 3
    label "pomidor"
    origin "text"
  ]
  node [
    id 4
    label "memy"
    origin "text"
  ]
  node [
    id 5
    label "jagoda"
  ]
  node [
    id 6
    label "psiankowate"
  ]
  node [
    id 7
    label "warzywo"
  ]
  node [
    id 8
    label "ro&#347;lina"
  ]
  node [
    id 9
    label "tomato"
  ]
  node [
    id 10
    label "zabawa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
]
