graph [
  maxDegree 70
  minDegree 1
  meanDegree 2.522671063478978
  density 0.002081411768547011
  graphCliqueNumber 8
  node [
    id 0
    label "liczny"
    origin "text"
  ]
  node [
    id 1
    label "blog"
    origin "text"
  ]
  node [
    id 2
    label "akurat"
    origin "text"
  ]
  node [
    id 3
    label "zacz&#261;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wycieczka"
    origin "text"
  ]
  node [
    id 5
    label "vagli"
    origin "text"
  ]
  node [
    id 6
    label "kampania"
    origin "text"
  ]
  node [
    id 7
    label "noadblock"
    origin "text"
  ]
  node [
    id 8
    label "zal&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 9
    label "nie"
    origin "text"
  ]
  node [
    id 10
    label "kto"
    origin "text"
  ]
  node [
    id 11
    label "paso&#380;yt"
    origin "text"
  ]
  node [
    id 12
    label "by&#263;"
    origin "text"
  ]
  node [
    id 13
    label "bardzo"
    origin "text"
  ]
  node [
    id 14
    label "jednak"
    origin "text"
  ]
  node [
    id 15
    label "si&#281;"
    origin "text"
  ]
  node [
    id 16
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 17
    label "odwaga"
    origin "text"
  ]
  node [
    id 18
    label "pod"
    origin "text"
  ]
  node [
    id 19
    label "tym"
    origin "text"
  ]
  node [
    id 20
    label "podpisa&#263;"
    origin "text"
  ]
  node [
    id 21
    label "tch&#243;rzliwy"
    origin "text"
  ]
  node [
    id 22
    label "autor"
    origin "text"
  ]
  node [
    id 23
    label "strona"
    origin "text"
  ]
  node [
    id 24
    label "twierdzi&#263;"
    origin "text"
  ]
  node [
    id 25
    label "bez"
    origin "text"
  ]
  node [
    id 26
    label "reklama"
    origin "text"
  ]
  node [
    id 27
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 28
    label "internet"
    origin "text"
  ]
  node [
    id 29
    label "ile"
    origin "text"
  ]
  node [
    id 30
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 31
    label "bomba"
    origin "text"
  ]
  node [
    id 32
    label "atomowy"
    origin "text"
  ]
  node [
    id 33
    label "znaczy&#263;"
    origin "text"
  ]
  node [
    id 34
    label "siebie"
    origin "text"
  ]
  node [
    id 35
    label "kupi&#263;"
    origin "text"
  ]
  node [
    id 36
    label "po&#322;a"
    origin "text"
  ]
  node [
    id 37
    label "kilo"
    origin "text"
  ]
  node [
    id 38
    label "pluton"
    origin "text"
  ]
  node [
    id 39
    label "sam"
    origin "text"
  ]
  node [
    id 40
    label "adblocka"
    origin "text"
  ]
  node [
    id 41
    label "u&#380;ywa&#263;"
    origin "text"
  ]
  node [
    id 42
    label "t&#322;umaczy&#263;"
    origin "text"
  ]
  node [
    id 43
    label "flash"
    origin "text"
  ]
  node [
    id 44
    label "wprawia&#263;"
    origin "text"
  ]
  node [
    id 45
    label "moje"
    origin "text"
  ]
  node [
    id 46
    label "zdycha&#263;"
    origin "text"
  ]
  node [
    id 47
    label "laptop"
    origin "text"
  ]
  node [
    id 48
    label "&#347;miertelny"
    origin "text"
  ]
  node [
    id 49
    label "drgawka"
    origin "text"
  ]
  node [
    id 50
    label "blokowa&#263;"
    origin "text"
  ]
  node [
    id 51
    label "premedytacja"
    origin "text"
  ]
  node [
    id 52
    label "jako"
    origin "text"
  ]
  node [
    id 53
    label "&#347;wiadomy"
    origin "text"
  ]
  node [
    id 54
    label "ekonomista"
    origin "text"
  ]
  node [
    id 55
    label "konsument"
    origin "text"
  ]
  node [
    id 56
    label "zbieracz"
    origin "text"
  ]
  node [
    id 57
    label "makulatura"
    origin "text"
  ]
  node [
    id 58
    label "mama"
    origin "text"
  ]
  node [
    id 59
    label "zupe&#322;nie"
    origin "text"
  ]
  node [
    id 60
    label "inny"
    origin "text"
  ]
  node [
    id 61
    label "zdanie"
    origin "text"
  ]
  node [
    id 62
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 63
    label "pawe&#322;"
    origin "text"
  ]
  node [
    id 64
    label "wimmer"
    origin "text"
  ]
  node [
    id 65
    label "wcale"
    origin "text"
  ]
  node [
    id 66
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 67
    label "patrze&#263;"
    origin "text"
  ]
  node [
    id 68
    label "gwarantowa&#263;"
    origin "text"
  ]
  node [
    id 69
    label "u&#380;ywanie"
    origin "text"
  ]
  node [
    id 70
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 71
    label "pozosta&#263;"
    origin "text"
  ]
  node [
    id 72
    label "bezp&#322;atny"
    origin "text"
  ]
  node [
    id 73
    label "bowiem"
    origin "text"
  ]
  node [
    id 74
    label "przekonany"
    origin "text"
  ]
  node [
    id 75
    label "przegl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 76
    label "komercyjny"
    origin "text"
  ]
  node [
    id 77
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 78
    label "cieszy&#263;"
    origin "text"
  ]
  node [
    id 79
    label "pionierski"
    origin "text"
  ]
  node [
    id 80
    label "okres"
    origin "text"
  ]
  node [
    id 81
    label "gdy"
    origin "text"
  ]
  node [
    id 82
    label "jeszcze"
    origin "text"
  ]
  node [
    id 83
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 84
    label "naklika&#263;"
    origin "text"
  ]
  node [
    id 85
    label "szkoda"
    origin "text"
  ]
  node [
    id 86
    label "dla"
    origin "text"
  ]
  node [
    id 87
    label "portfel"
    origin "text"
  ]
  node [
    id 88
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 89
    label "te&#380;"
    origin "text"
  ]
  node [
    id 90
    label "dziwna"
    origin "text"
  ]
  node [
    id 91
    label "alek"
    origin "text"
  ]
  node [
    id 92
    label "tarkowski"
    origin "text"
  ]
  node [
    id 93
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 94
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 95
    label "ten"
    origin "text"
  ]
  node [
    id 96
    label "chwila"
    origin "text"
  ]
  node [
    id 97
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 98
    label "zainspirowa&#263;"
    origin "text"
  ]
  node [
    id 99
    label "cho&#263;by"
    origin "text"
  ]
  node [
    id 100
    label "przenikliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 101
    label "georga"
    origin "text"
  ]
  node [
    id 102
    label "bush"
    origin "text"
  ]
  node [
    id 103
    label "potwierdzi&#263;"
    origin "text"
  ]
  node [
    id 104
    label "por&#243;wnywa&#263;"
    origin "text"
  ]
  node [
    id 105
    label "internetowy"
    origin "text"
  ]
  node [
    id 106
    label "telewizja"
    origin "text"
  ]
  node [
    id 107
    label "chyba"
    origin "text"
  ]
  node [
    id 108
    label "pvr"
    origin "text"
  ]
  node [
    id 109
    label "przesuni&#281;cie"
    origin "text"
  ]
  node [
    id 110
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 111
    label "godzina"
    origin "text"
  ]
  node [
    id 112
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 113
    label "przewin&#261;&#263;"
    origin "text"
  ]
  node [
    id 114
    label "gazeta"
    origin "text"
  ]
  node [
    id 115
    label "sens"
    origin "text"
  ]
  node [
    id 116
    label "ale"
    origin "text"
  ]
  node [
    id 117
    label "mimo"
    origin "text"
  ]
  node [
    id 118
    label "wszystko"
    origin "text"
  ]
  node [
    id 119
    label "skojarzy&#263;"
    origin "text"
  ]
  node [
    id 120
    label "wpis"
    origin "text"
  ]
  node [
    id 121
    label "tima"
    origin "text"
  ]
  node [
    id 122
    label "kisnera"
    origin "text"
  ]
  node [
    id 123
    label "umie&#263;"
    origin "text"
  ]
  node [
    id 124
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 125
    label "rzut"
    origin "text"
  ]
  node [
    id 126
    label "osobisty"
    origin "text"
  ]
  node [
    id 127
    label "rozgrywa&#263;"
    origin "text"
  ]
  node [
    id 128
    label "z&#322;o&#347;liwy"
    origin "text"
  ]
  node [
    id 129
    label "adblock"
    origin "text"
  ]
  node [
    id 130
    label "wycina&#263;"
    origin "text"
  ]
  node [
    id 131
    label "nazwa"
    origin "text"
  ]
  node [
    id 132
    label "sponsor"
    origin "text"
  ]
  node [
    id 133
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 134
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 135
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 136
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 137
    label "zmieni&#263;"
    origin "text"
  ]
  node [
    id 138
    label "&#347;l&#261;sk"
    origin "text"
  ]
  node [
    id 139
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 140
    label "pisz&#261;cy"
    origin "text"
  ]
  node [
    id 141
    label "nieciekawie"
    origin "text"
  ]
  node [
    id 142
    label "prosta"
    origin "text"
  ]
  node [
    id 143
    label "bia&#322;a"
    origin "text"
  ]
  node [
    id 144
    label "amerykanin"
    origin "text"
  ]
  node [
    id 145
    label "polska"
    origin "text"
  ]
  node [
    id 146
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 147
    label "znacznie"
    origin "text"
  ]
  node [
    id 148
    label "ciekawy"
    origin "text"
  ]
  node [
    id 149
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 150
    label "superbowl"
    origin "text"
  ]
  node [
    id 151
    label "cierpie&#263;"
    origin "text"
  ]
  node [
    id 152
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 153
    label "brak"
    origin "text"
  ]
  node [
    id 154
    label "ot&#243;&#380;"
    origin "text"
  ]
  node [
    id 155
    label "zdarza&#263;"
    origin "text"
  ]
  node [
    id 156
    label "czas"
    origin "text"
  ]
  node [
    id 157
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 158
    label "przez"
    origin "text"
  ]
  node [
    id 159
    label "przecie&#380;"
    origin "text"
  ]
  node [
    id 160
    label "wy&#322;&#261;cza&#263;"
    origin "text"
  ]
  node [
    id 161
    label "przeciwie&#324;stwo"
    origin "text"
  ]
  node [
    id 162
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 163
    label "ochota"
    origin "text"
  ]
  node [
    id 164
    label "powt&#243;rka"
    origin "text"
  ]
  node [
    id 165
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 166
    label "dorosn&#261;&#263;"
    origin "text"
  ]
  node [
    id 167
    label "swoje"
    origin "text"
  ]
  node [
    id 168
    label "model"
    origin "text"
  ]
  node [
    id 169
    label "dobry"
    origin "text"
  ]
  node [
    id 170
    label "wiele"
    origin "text"
  ]
  node [
    id 171
    label "serwis"
    origin "text"
  ]
  node [
    id 172
    label "zbiera&#263;"
    origin "text"
  ]
  node [
    id 173
    label "przepustowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 174
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 175
    label "zaoszcz&#281;dzi&#263;"
    origin "text"
  ]
  node [
    id 176
    label "optymalizowa&#263;"
    origin "text"
  ]
  node [
    id 177
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 178
    label "zamiast"
    origin "text"
  ]
  node [
    id 179
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 180
    label "pokrywa&#263;"
    origin "text"
  ]
  node [
    id 181
    label "koszt"
    origin "text"
  ]
  node [
    id 182
    label "dodawanie"
    origin "text"
  ]
  node [
    id 183
    label "wodotrysk"
    origin "text"
  ]
  node [
    id 184
    label "portal"
    origin "text"
  ]
  node [
    id 185
    label "eksploatowa&#263;"
    origin "text"
  ]
  node [
    id 186
    label "pierwsza"
    origin "text"
  ]
  node [
    id 187
    label "wyklikuj&#281;"
    origin "text"
  ]
  node [
    id 188
    label "odpowiednio"
    origin "text"
  ]
  node [
    id 189
    label "wysokie"
    origin "text"
  ]
  node [
    id 190
    label "miejsce"
    origin "text"
  ]
  node [
    id 191
    label "klasyfikacja"
    origin "text"
  ]
  node [
    id 192
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 193
    label "liczba"
    origin "text"
  ]
  node [
    id 194
    label "ods&#322;ona"
    origin "text"
  ]
  node [
    id 195
    label "konkretny"
    origin "text"
  ]
  node [
    id 196
    label "warto&#347;&#263;"
    origin "text"
  ]
  node [
    id 197
    label "rynkowy"
    origin "text"
  ]
  node [
    id 198
    label "regularnie"
    origin "text"
  ]
  node [
    id 199
    label "kupowa&#263;"
    origin "text"
  ]
  node [
    id 200
    label "produkt"
    origin "text"
  ]
  node [
    id 201
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 202
    label "poogl&#261;da&#263;by&#263;"
    origin "text"
  ]
  node [
    id 203
    label "reklamowa&#263;"
    origin "text"
  ]
  node [
    id 204
    label "g&#322;&#243;wnie"
    origin "text"
  ]
  node [
    id 205
    label "rojenie_si&#281;"
  ]
  node [
    id 206
    label "cz&#281;sty"
  ]
  node [
    id 207
    label "licznie"
  ]
  node [
    id 208
    label "komcio"
  ]
  node [
    id 209
    label "blogosfera"
  ]
  node [
    id 210
    label "pami&#281;tnik"
  ]
  node [
    id 211
    label "odpowiedni"
  ]
  node [
    id 212
    label "dok&#322;adnie"
  ]
  node [
    id 213
    label "akuratny"
  ]
  node [
    id 214
    label "wyjazd"
  ]
  node [
    id 215
    label "grupa"
  ]
  node [
    id 216
    label "odpoczynek"
  ]
  node [
    id 217
    label "chadzka"
  ]
  node [
    id 218
    label "dzia&#322;anie"
  ]
  node [
    id 219
    label "campaign"
  ]
  node [
    id 220
    label "wydarzenie"
  ]
  node [
    id 221
    label "akcja"
  ]
  node [
    id 222
    label "Kampania_Wrze&#347;niowa"
  ]
  node [
    id 223
    label "integument"
  ]
  node [
    id 224
    label "zar&#243;d&#378;"
  ]
  node [
    id 225
    label "organ"
  ]
  node [
    id 226
    label "pocz&#261;tek"
  ]
  node [
    id 227
    label "o&#347;rodek"
  ]
  node [
    id 228
    label "sprzeciw"
  ]
  node [
    id 229
    label "cz&#322;owiek"
  ]
  node [
    id 230
    label "istota_&#380;ywa"
  ]
  node [
    id 231
    label "odrobacza&#263;"
  ]
  node [
    id 232
    label "odwszawianie"
  ]
  node [
    id 233
    label "odrobaczanie"
  ]
  node [
    id 234
    label "si&#281;ga&#263;"
  ]
  node [
    id 235
    label "trwa&#263;"
  ]
  node [
    id 236
    label "obecno&#347;&#263;"
  ]
  node [
    id 237
    label "stan"
  ]
  node [
    id 238
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 239
    label "stand"
  ]
  node [
    id 240
    label "mie&#263;_miejsce"
  ]
  node [
    id 241
    label "uczestniczy&#263;"
  ]
  node [
    id 242
    label "chodzi&#263;"
  ]
  node [
    id 243
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 244
    label "equal"
  ]
  node [
    id 245
    label "w_chuj"
  ]
  node [
    id 246
    label "czyj&#347;"
  ]
  node [
    id 247
    label "m&#261;&#380;"
  ]
  node [
    id 248
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 249
    label "cecha"
  ]
  node [
    id 250
    label "courage"
  ]
  node [
    id 251
    label "dusza"
  ]
  node [
    id 252
    label "za&#347;wiadczy&#263;"
  ]
  node [
    id 253
    label "postawi&#263;"
  ]
  node [
    id 254
    label "sign"
  ]
  node [
    id 255
    label "opatrzy&#263;"
  ]
  node [
    id 256
    label "tch&#243;rzowsko"
  ]
  node [
    id 257
    label "strachliwy"
  ]
  node [
    id 258
    label "pomys&#322;odawca"
  ]
  node [
    id 259
    label "kszta&#322;ciciel"
  ]
  node [
    id 260
    label "tworzyciel"
  ]
  node [
    id 261
    label "&#347;w"
  ]
  node [
    id 262
    label "wykonawca"
  ]
  node [
    id 263
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 264
    label "skr&#281;canie"
  ]
  node [
    id 265
    label "voice"
  ]
  node [
    id 266
    label "forma"
  ]
  node [
    id 267
    label "skr&#281;ci&#263;"
  ]
  node [
    id 268
    label "kartka"
  ]
  node [
    id 269
    label "orientowa&#263;"
  ]
  node [
    id 270
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 271
    label "powierzchnia"
  ]
  node [
    id 272
    label "plik"
  ]
  node [
    id 273
    label "bok"
  ]
  node [
    id 274
    label "pagina"
  ]
  node [
    id 275
    label "orientowanie"
  ]
  node [
    id 276
    label "fragment"
  ]
  node [
    id 277
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 278
    label "s&#261;d"
  ]
  node [
    id 279
    label "skr&#281;ca&#263;"
  ]
  node [
    id 280
    label "g&#243;ra"
  ]
  node [
    id 281
    label "serwis_internetowy"
  ]
  node [
    id 282
    label "orientacja"
  ]
  node [
    id 283
    label "linia"
  ]
  node [
    id 284
    label "skr&#281;cenie"
  ]
  node [
    id 285
    label "layout"
  ]
  node [
    id 286
    label "zorientowa&#263;"
  ]
  node [
    id 287
    label "zorientowanie"
  ]
  node [
    id 288
    label "obiekt"
  ]
  node [
    id 289
    label "podmiot"
  ]
  node [
    id 290
    label "ty&#322;"
  ]
  node [
    id 291
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 292
    label "logowanie"
  ]
  node [
    id 293
    label "adres_internetowy"
  ]
  node [
    id 294
    label "uj&#281;cie"
  ]
  node [
    id 295
    label "prz&#243;d"
  ]
  node [
    id 296
    label "posta&#263;"
  ]
  node [
    id 297
    label "zapewnia&#263;"
  ]
  node [
    id 298
    label "oznajmia&#263;"
  ]
  node [
    id 299
    label "komunikowa&#263;"
  ]
  node [
    id 300
    label "attest"
  ]
  node [
    id 301
    label "argue"
  ]
  node [
    id 302
    label "ki&#347;&#263;"
  ]
  node [
    id 303
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 304
    label "krzew"
  ]
  node [
    id 305
    label "pi&#380;maczkowate"
  ]
  node [
    id 306
    label "pestkowiec"
  ]
  node [
    id 307
    label "kwiat"
  ]
  node [
    id 308
    label "owoc"
  ]
  node [
    id 309
    label "oliwkowate"
  ]
  node [
    id 310
    label "ro&#347;lina"
  ]
  node [
    id 311
    label "hy&#263;ka"
  ]
  node [
    id 312
    label "lilac"
  ]
  node [
    id 313
    label "delfinidyna"
  ]
  node [
    id 314
    label "copywriting"
  ]
  node [
    id 315
    label "brief"
  ]
  node [
    id 316
    label "bran&#380;a"
  ]
  node [
    id 317
    label "informacja"
  ]
  node [
    id 318
    label "promowa&#263;"
  ]
  node [
    id 319
    label "wypromowa&#263;"
  ]
  node [
    id 320
    label "samplowanie"
  ]
  node [
    id 321
    label "tekst"
  ]
  node [
    id 322
    label "us&#322;uga_internetowa"
  ]
  node [
    id 323
    label "biznes_elektroniczny"
  ]
  node [
    id 324
    label "punkt_dost&#281;pu"
  ]
  node [
    id 325
    label "hipertekst"
  ]
  node [
    id 326
    label "gra_sieciowa"
  ]
  node [
    id 327
    label "mem"
  ]
  node [
    id 328
    label "e-hazard"
  ]
  node [
    id 329
    label "sie&#263;_komputerowa"
  ]
  node [
    id 330
    label "media"
  ]
  node [
    id 331
    label "podcast"
  ]
  node [
    id 332
    label "netbook"
  ]
  node [
    id 333
    label "provider"
  ]
  node [
    id 334
    label "cyberprzestrze&#324;"
  ]
  node [
    id 335
    label "grooming"
  ]
  node [
    id 336
    label "cognizance"
  ]
  node [
    id 337
    label "niedostateczny"
  ]
  node [
    id 338
    label "zapalnik"
  ]
  node [
    id 339
    label "novum"
  ]
  node [
    id 340
    label "czerep"
  ]
  node [
    id 341
    label "strza&#322;"
  ]
  node [
    id 342
    label "pocisk"
  ]
  node [
    id 343
    label "materia&#322;_piroklastyczny"
  ]
  node [
    id 344
    label "bombowiec"
  ]
  node [
    id 345
    label "nab&#243;j"
  ]
  node [
    id 346
    label "spell"
  ]
  node [
    id 347
    label "odgrywa&#263;_rol&#281;"
  ]
  node [
    id 348
    label "zostawia&#263;"
  ]
  node [
    id 349
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 350
    label "represent"
  ]
  node [
    id 351
    label "count"
  ]
  node [
    id 352
    label "wyraz"
  ]
  node [
    id 353
    label "zdobi&#263;"
  ]
  node [
    id 354
    label "get"
  ]
  node [
    id 355
    label "wzi&#261;&#263;"
  ]
  node [
    id 356
    label "catch"
  ]
  node [
    id 357
    label "przyj&#261;&#263;"
  ]
  node [
    id 358
    label "beget"
  ]
  node [
    id 359
    label "pozyska&#263;"
  ]
  node [
    id 360
    label "ustawi&#263;"
  ]
  node [
    id 361
    label "uzna&#263;"
  ]
  node [
    id 362
    label "zagra&#263;"
  ]
  node [
    id 363
    label "uwierzy&#263;"
  ]
  node [
    id 364
    label "str&#243;j"
  ]
  node [
    id 365
    label "dekagram"
  ]
  node [
    id 366
    label "uk&#322;ad_SI"
  ]
  node [
    id 367
    label "metryczna_jednostka_masy"
  ]
  node [
    id 368
    label "hektogram"
  ]
  node [
    id 369
    label "tona"
  ]
  node [
    id 370
    label "bateria"
  ]
  node [
    id 371
    label "dzia&#322;on"
  ]
  node [
    id 372
    label "kompania"
  ]
  node [
    id 373
    label "transuranowiec"
  ]
  node [
    id 374
    label "jednostka_organizacyjna"
  ]
  node [
    id 375
    label "dru&#380;yna"
  ]
  node [
    id 376
    label "aktynowiec"
  ]
  node [
    id 377
    label "pododdzia&#322;"
  ]
  node [
    id 378
    label "sklep"
  ]
  node [
    id 379
    label "bash"
  ]
  node [
    id 380
    label "distribute"
  ]
  node [
    id 381
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 382
    label "give"
  ]
  node [
    id 383
    label "korzysta&#263;"
  ]
  node [
    id 384
    label "doznawa&#263;"
  ]
  node [
    id 385
    label "przek&#322;ada&#263;"
  ]
  node [
    id 386
    label "uzasadnia&#263;"
  ]
  node [
    id 387
    label "poja&#347;nia&#263;"
  ]
  node [
    id 388
    label "elaborate"
  ]
  node [
    id 389
    label "explain"
  ]
  node [
    id 390
    label "suplikowa&#263;"
  ]
  node [
    id 391
    label "j&#281;zyk"
  ]
  node [
    id 392
    label "sprawowa&#263;"
  ]
  node [
    id 393
    label "robi&#263;"
  ]
  node [
    id 394
    label "przekonywa&#263;"
  ]
  node [
    id 395
    label "u&#322;atwia&#263;"
  ]
  node [
    id 396
    label "broni&#263;"
  ]
  node [
    id 397
    label "interpretowa&#263;"
  ]
  node [
    id 398
    label "przedstawia&#263;"
  ]
  node [
    id 399
    label "program"
  ]
  node [
    id 400
    label "pami&#281;&#263;"
  ]
  node [
    id 401
    label "Adobe_Flash"
  ]
  node [
    id 402
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 403
    label "bind"
  ]
  node [
    id 404
    label "obraz"
  ]
  node [
    id 405
    label "train"
  ]
  node [
    id 406
    label "umieszcza&#263;"
  ]
  node [
    id 407
    label "doskonali&#263;"
  ]
  node [
    id 408
    label "discipline"
  ]
  node [
    id 409
    label "ko&#324;czy&#263;"
  ]
  node [
    id 410
    label "pada&#263;"
  ]
  node [
    id 411
    label "umiera&#263;"
  ]
  node [
    id 412
    label "podupada&#263;"
  ]
  node [
    id 413
    label "die"
  ]
  node [
    id 414
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 415
    label "touchpad"
  ]
  node [
    id 416
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 417
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 418
    label "Ultrabook"
  ]
  node [
    id 419
    label "przed&#347;miertelny"
  ]
  node [
    id 420
    label "&#347;miertelnie"
  ]
  node [
    id 421
    label "zaciek&#322;y"
  ]
  node [
    id 422
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 423
    label "wielki"
  ]
  node [
    id 424
    label "niebezpieczny"
  ]
  node [
    id 425
    label "tragiczny"
  ]
  node [
    id 426
    label "za&#380;arty"
  ]
  node [
    id 427
    label "ko&#324;cowy"
  ]
  node [
    id 428
    label "kurcz"
  ]
  node [
    id 429
    label "ostatnie_podrygi"
  ]
  node [
    id 430
    label "spasm"
  ]
  node [
    id 431
    label "rzuci&#263;"
  ]
  node [
    id 432
    label "rzucenie"
  ]
  node [
    id 433
    label "throng"
  ]
  node [
    id 434
    label "zajmowa&#263;"
  ]
  node [
    id 435
    label "przeszkadza&#263;"
  ]
  node [
    id 436
    label "zablokowywa&#263;"
  ]
  node [
    id 437
    label "sk&#322;ada&#263;"
  ]
  node [
    id 438
    label "walczy&#263;"
  ]
  node [
    id 439
    label "interlock"
  ]
  node [
    id 440
    label "wstrzymywa&#263;"
  ]
  node [
    id 441
    label "unieruchamia&#263;"
  ]
  node [
    id 442
    label "kiblowa&#263;"
  ]
  node [
    id 443
    label "zatrzymywa&#263;"
  ]
  node [
    id 444
    label "parry"
  ]
  node [
    id 445
    label "przyczyna"
  ]
  node [
    id 446
    label "wyrachowanie"
  ]
  node [
    id 447
    label "przytomnie"
  ]
  node [
    id 448
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 449
    label "przemy&#347;lany"
  ]
  node [
    id 450
    label "rozs&#261;dny"
  ]
  node [
    id 451
    label "&#347;wiadomie"
  ]
  node [
    id 452
    label "dojrza&#322;y"
  ]
  node [
    id 453
    label "Malthus"
  ]
  node [
    id 454
    label "specjalista"
  ]
  node [
    id 455
    label "Marks"
  ]
  node [
    id 456
    label "Keynes"
  ]
  node [
    id 457
    label "Henri_de_Saint-Simon"
  ]
  node [
    id 458
    label "ekonomik"
  ]
  node [
    id 459
    label "rynek"
  ]
  node [
    id 460
    label "odbiorca"
  ]
  node [
    id 461
    label "go&#347;&#263;"
  ]
  node [
    id 462
    label "zjadacz"
  ]
  node [
    id 463
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 464
    label "restauracja"
  ]
  node [
    id 465
    label "heterotrof"
  ]
  node [
    id 466
    label "klient"
  ]
  node [
    id 467
    label "u&#380;ytkownik"
  ]
  node [
    id 468
    label "zapiski"
  ]
  node [
    id 469
    label "grafoman"
  ]
  node [
    id 470
    label "surowiec_wt&#243;rny"
  ]
  node [
    id 471
    label "matczysko"
  ]
  node [
    id 472
    label "macierz"
  ]
  node [
    id 473
    label "przodkini"
  ]
  node [
    id 474
    label "Matka_Boska"
  ]
  node [
    id 475
    label "macocha"
  ]
  node [
    id 476
    label "matka_zast&#281;pcza"
  ]
  node [
    id 477
    label "stara"
  ]
  node [
    id 478
    label "rodzice"
  ]
  node [
    id 479
    label "rodzic"
  ]
  node [
    id 480
    label "kompletny"
  ]
  node [
    id 481
    label "wniwecz"
  ]
  node [
    id 482
    label "zupe&#322;ny"
  ]
  node [
    id 483
    label "kolejny"
  ]
  node [
    id 484
    label "inaczej"
  ]
  node [
    id 485
    label "inszy"
  ]
  node [
    id 486
    label "osobno"
  ]
  node [
    id 487
    label "attitude"
  ]
  node [
    id 488
    label "system"
  ]
  node [
    id 489
    label "przedstawienie"
  ]
  node [
    id 490
    label "fraza"
  ]
  node [
    id 491
    label "prison_term"
  ]
  node [
    id 492
    label "adjudication"
  ]
  node [
    id 493
    label "przekazanie"
  ]
  node [
    id 494
    label "pass"
  ]
  node [
    id 495
    label "wyra&#380;enie"
  ]
  node [
    id 496
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 497
    label "wypowiedzenie"
  ]
  node [
    id 498
    label "konektyw"
  ]
  node [
    id 499
    label "zaliczenie"
  ]
  node [
    id 500
    label "stanowisko"
  ]
  node [
    id 501
    label "powierzenie"
  ]
  node [
    id 502
    label "antylogizm"
  ]
  node [
    id 503
    label "zmuszenie"
  ]
  node [
    id 504
    label "szko&#322;a"
  ]
  node [
    id 505
    label "poziom"
  ]
  node [
    id 506
    label "faza"
  ]
  node [
    id 507
    label "depression"
  ]
  node [
    id 508
    label "zjawisko"
  ]
  node [
    id 509
    label "nizina"
  ]
  node [
    id 510
    label "ni_chuja"
  ]
  node [
    id 511
    label "ca&#322;kiem"
  ]
  node [
    id 512
    label "continue"
  ]
  node [
    id 513
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 514
    label "consider"
  ]
  node [
    id 515
    label "pilnowa&#263;"
  ]
  node [
    id 516
    label "uznawa&#263;"
  ]
  node [
    id 517
    label "obserwowa&#263;"
  ]
  node [
    id 518
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 519
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 520
    label "deliver"
  ]
  node [
    id 521
    label "koso"
  ]
  node [
    id 522
    label "szuka&#263;"
  ]
  node [
    id 523
    label "go_steady"
  ]
  node [
    id 524
    label "dba&#263;"
  ]
  node [
    id 525
    label "traktowa&#263;"
  ]
  node [
    id 526
    label "os&#261;dza&#263;"
  ]
  node [
    id 527
    label "punkt_widzenia"
  ]
  node [
    id 528
    label "look"
  ]
  node [
    id 529
    label "pogl&#261;da&#263;"
  ]
  node [
    id 530
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 531
    label "guarantee"
  ]
  node [
    id 532
    label "prosecute"
  ]
  node [
    id 533
    label "use"
  ]
  node [
    id 534
    label "robienie"
  ]
  node [
    id 535
    label "czynno&#347;&#263;"
  ]
  node [
    id 536
    label "exercise"
  ]
  node [
    id 537
    label "zniszczenie"
  ]
  node [
    id 538
    label "stosowanie"
  ]
  node [
    id 539
    label "przejaskrawianie"
  ]
  node [
    id 540
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 541
    label "zu&#380;ywanie"
  ]
  node [
    id 542
    label "u&#380;yteczny"
  ]
  node [
    id 543
    label "relish"
  ]
  node [
    id 544
    label "zaznawanie"
  ]
  node [
    id 545
    label "gauze"
  ]
  node [
    id 546
    label "nitka"
  ]
  node [
    id 547
    label "mesh"
  ]
  node [
    id 548
    label "snu&#263;"
  ]
  node [
    id 549
    label "organization"
  ]
  node [
    id 550
    label "zasadzka"
  ]
  node [
    id 551
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 552
    label "web"
  ]
  node [
    id 553
    label "struktura"
  ]
  node [
    id 554
    label "organizacja"
  ]
  node [
    id 555
    label "vane"
  ]
  node [
    id 556
    label "kszta&#322;t"
  ]
  node [
    id 557
    label "wysnu&#263;"
  ]
  node [
    id 558
    label "instalacja"
  ]
  node [
    id 559
    label "net"
  ]
  node [
    id 560
    label "plecionka"
  ]
  node [
    id 561
    label "rozmieszczenie"
  ]
  node [
    id 562
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 563
    label "proceed"
  ]
  node [
    id 564
    label "osta&#263;_si&#281;"
  ]
  node [
    id 565
    label "support"
  ]
  node [
    id 566
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 567
    label "prze&#380;y&#263;"
  ]
  node [
    id 568
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 569
    label "bezp&#322;atnie"
  ]
  node [
    id 570
    label "darmowo"
  ]
  node [
    id 571
    label "upewnienie_si&#281;"
  ]
  node [
    id 572
    label "ufanie"
  ]
  node [
    id 573
    label "wierzenie"
  ]
  node [
    id 574
    label "upewnianie_si&#281;"
  ]
  node [
    id 575
    label "examine"
  ]
  node [
    id 576
    label "scan"
  ]
  node [
    id 577
    label "prze&#347;witywa&#263;"
  ]
  node [
    id 578
    label "wygl&#261;da&#263;"
  ]
  node [
    id 579
    label "sprawdza&#263;"
  ]
  node [
    id 580
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 581
    label "survey"
  ]
  node [
    id 582
    label "przeszukiwa&#263;"
  ]
  node [
    id 583
    label "skomercjalizowanie"
  ]
  node [
    id 584
    label "komercjalizowanie"
  ]
  node [
    id 585
    label "masowy"
  ]
  node [
    id 586
    label "komercyjnie"
  ]
  node [
    id 587
    label "trza"
  ]
  node [
    id 588
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 589
    label "para"
  ]
  node [
    id 590
    label "necessity"
  ]
  node [
    id 591
    label "amuse"
  ]
  node [
    id 592
    label "pie&#347;ci&#263;"
  ]
  node [
    id 593
    label "nape&#322;nia&#263;"
  ]
  node [
    id 594
    label "wzbudza&#263;"
  ]
  node [
    id 595
    label "raczy&#263;"
  ]
  node [
    id 596
    label "porusza&#263;"
  ]
  node [
    id 597
    label "szczerzy&#263;"
  ]
  node [
    id 598
    label "zaspokaja&#263;"
  ]
  node [
    id 599
    label "prze&#322;omowy"
  ]
  node [
    id 600
    label "pioniersko"
  ]
  node [
    id 601
    label "prekursorski"
  ]
  node [
    id 602
    label "niezwyk&#322;y"
  ]
  node [
    id 603
    label "paleogen"
  ]
  node [
    id 604
    label "period"
  ]
  node [
    id 605
    label "prekambr"
  ]
  node [
    id 606
    label "jura"
  ]
  node [
    id 607
    label "interstadia&#322;"
  ]
  node [
    id 608
    label "jednostka_geologiczna"
  ]
  node [
    id 609
    label "izochronizm"
  ]
  node [
    id 610
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 611
    label "okres_noachijski"
  ]
  node [
    id 612
    label "orosir"
  ]
  node [
    id 613
    label "sten"
  ]
  node [
    id 614
    label "kreda"
  ]
  node [
    id 615
    label "drugorz&#281;d"
  ]
  node [
    id 616
    label "semester"
  ]
  node [
    id 617
    label "trzeciorz&#281;d"
  ]
  node [
    id 618
    label "ton"
  ]
  node [
    id 619
    label "dzieje"
  ]
  node [
    id 620
    label "poprzednik"
  ]
  node [
    id 621
    label "kalim"
  ]
  node [
    id 622
    label "ordowik"
  ]
  node [
    id 623
    label "karbon"
  ]
  node [
    id 624
    label "trias"
  ]
  node [
    id 625
    label "stater"
  ]
  node [
    id 626
    label "era"
  ]
  node [
    id 627
    label "cykl"
  ]
  node [
    id 628
    label "p&#243;&#322;okres"
  ]
  node [
    id 629
    label "czwartorz&#281;d"
  ]
  node [
    id 630
    label "pulsacja"
  ]
  node [
    id 631
    label "okres_amazo&#324;ski"
  ]
  node [
    id 632
    label "kambr"
  ]
  node [
    id 633
    label "Zeitgeist"
  ]
  node [
    id 634
    label "nast&#281;pnik"
  ]
  node [
    id 635
    label "kriogen"
  ]
  node [
    id 636
    label "glacja&#322;"
  ]
  node [
    id 637
    label "fala"
  ]
  node [
    id 638
    label "okres_czasu"
  ]
  node [
    id 639
    label "riak"
  ]
  node [
    id 640
    label "schy&#322;ek"
  ]
  node [
    id 641
    label "okres_hesperyjski"
  ]
  node [
    id 642
    label "sylur"
  ]
  node [
    id 643
    label "dewon"
  ]
  node [
    id 644
    label "ciota"
  ]
  node [
    id 645
    label "epoka"
  ]
  node [
    id 646
    label "pierwszorz&#281;d"
  ]
  node [
    id 647
    label "okres_halsztacki"
  ]
  node [
    id 648
    label "ektas"
  ]
  node [
    id 649
    label "condition"
  ]
  node [
    id 650
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 651
    label "rok_akademicki"
  ]
  node [
    id 652
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 653
    label "postglacja&#322;"
  ]
  node [
    id 654
    label "proces_fizjologiczny"
  ]
  node [
    id 655
    label "ediakar"
  ]
  node [
    id 656
    label "time_period"
  ]
  node [
    id 657
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 658
    label "perm"
  ]
  node [
    id 659
    label "rok_szkolny"
  ]
  node [
    id 660
    label "neogen"
  ]
  node [
    id 661
    label "sider"
  ]
  node [
    id 662
    label "flow"
  ]
  node [
    id 663
    label "podokres"
  ]
  node [
    id 664
    label "preglacja&#322;"
  ]
  node [
    id 665
    label "retoryka"
  ]
  node [
    id 666
    label "choroba_przyrodzona"
  ]
  node [
    id 667
    label "ci&#261;gle"
  ]
  node [
    id 668
    label "free"
  ]
  node [
    id 669
    label "pole"
  ]
  node [
    id 670
    label "commiseration"
  ]
  node [
    id 671
    label "czu&#263;"
  ]
  node [
    id 672
    label "niepowodzenie"
  ]
  node [
    id 673
    label "ubytek"
  ]
  node [
    id 674
    label "&#380;erowisko"
  ]
  node [
    id 675
    label "szwank"
  ]
  node [
    id 676
    label "pojemnik"
  ]
  node [
    id 677
    label "bag"
  ]
  node [
    id 678
    label "bud&#380;et"
  ]
  node [
    id 679
    label "galanteria"
  ]
  node [
    id 680
    label "pugilares"
  ]
  node [
    id 681
    label "pieni&#261;dze"
  ]
  node [
    id 682
    label "zas&#243;b"
  ]
  node [
    id 683
    label "r&#243;&#380;nie"
  ]
  node [
    id 684
    label "jaki&#347;"
  ]
  node [
    id 685
    label "okre&#347;lony"
  ]
  node [
    id 686
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 687
    label "time"
  ]
  node [
    id 688
    label "report"
  ]
  node [
    id 689
    label "dodawa&#263;"
  ]
  node [
    id 690
    label "wymienia&#263;"
  ]
  node [
    id 691
    label "okre&#347;la&#263;"
  ]
  node [
    id 692
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 693
    label "dyskalkulia"
  ]
  node [
    id 694
    label "wynagrodzenie"
  ]
  node [
    id 695
    label "admit"
  ]
  node [
    id 696
    label "osi&#261;ga&#263;"
  ]
  node [
    id 697
    label "wyznacza&#263;"
  ]
  node [
    id 698
    label "posiada&#263;"
  ]
  node [
    id 699
    label "mierzy&#263;"
  ]
  node [
    id 700
    label "odlicza&#263;"
  ]
  node [
    id 701
    label "bra&#263;"
  ]
  node [
    id 702
    label "wycenia&#263;"
  ]
  node [
    id 703
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 704
    label "rachowa&#263;"
  ]
  node [
    id 705
    label "tell"
  ]
  node [
    id 706
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 707
    label "policza&#263;"
  ]
  node [
    id 708
    label "tug"
  ]
  node [
    id 709
    label "pobudzi&#263;"
  ]
  node [
    id 710
    label "wla&#263;"
  ]
  node [
    id 711
    label "skopiowa&#263;"
  ]
  node [
    id 712
    label "zakres"
  ]
  node [
    id 713
    label "uwa&#380;no&#347;&#263;"
  ]
  node [
    id 714
    label "g&#322;&#281;bia"
  ]
  node [
    id 715
    label "g&#322;o&#347;no&#347;&#263;"
  ]
  node [
    id 716
    label "intensywno&#347;&#263;"
  ]
  node [
    id 717
    label "intuicja"
  ]
  node [
    id 718
    label "inteligencja"
  ]
  node [
    id 719
    label "bystro&#347;&#263;"
  ]
  node [
    id 720
    label "stwierdzi&#263;"
  ]
  node [
    id 721
    label "acknowledge"
  ]
  node [
    id 722
    label "przy&#347;wiadczy&#263;"
  ]
  node [
    id 723
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 724
    label "analizowa&#263;"
  ]
  node [
    id 725
    label "szacowa&#263;"
  ]
  node [
    id 726
    label "nowoczesny"
  ]
  node [
    id 727
    label "elektroniczny"
  ]
  node [
    id 728
    label "sieciowo"
  ]
  node [
    id 729
    label "netowy"
  ]
  node [
    id 730
    label "internetowo"
  ]
  node [
    id 731
    label "Polsat"
  ]
  node [
    id 732
    label "paj&#281;czarz"
  ]
  node [
    id 733
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 734
    label "programowiec"
  ]
  node [
    id 735
    label "technologia"
  ]
  node [
    id 736
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 737
    label "Interwizja"
  ]
  node [
    id 738
    label "BBC"
  ]
  node [
    id 739
    label "ekran"
  ]
  node [
    id 740
    label "redakcja"
  ]
  node [
    id 741
    label "odbieranie"
  ]
  node [
    id 742
    label "odbiera&#263;"
  ]
  node [
    id 743
    label "odbiornik"
  ]
  node [
    id 744
    label "instytucja"
  ]
  node [
    id 745
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 746
    label "studio"
  ]
  node [
    id 747
    label "telekomunikacja"
  ]
  node [
    id 748
    label "muza"
  ]
  node [
    id 749
    label "shunt"
  ]
  node [
    id 750
    label "zasuni&#281;cie"
  ]
  node [
    id 751
    label "przemieszczenie"
  ]
  node [
    id 752
    label "poprzesuwanie"
  ]
  node [
    id 753
    label "zmienienie"
  ]
  node [
    id 754
    label "move"
  ]
  node [
    id 755
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 756
    label "przeniesienie"
  ]
  node [
    id 757
    label "dostosowanie"
  ]
  node [
    id 758
    label "ruszenie"
  ]
  node [
    id 759
    label "notice"
  ]
  node [
    id 760
    label "styka&#263;_si&#281;"
  ]
  node [
    id 761
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 762
    label "minuta"
  ]
  node [
    id 763
    label "doba"
  ]
  node [
    id 764
    label "p&#243;&#322;godzina"
  ]
  node [
    id 765
    label "kwadrans"
  ]
  node [
    id 766
    label "jednostka_czasu"
  ]
  node [
    id 767
    label "owin&#261;&#263;"
  ]
  node [
    id 768
    label "wind"
  ]
  node [
    id 769
    label "przebra&#263;"
  ]
  node [
    id 770
    label "prasa"
  ]
  node [
    id 771
    label "tytu&#322;"
  ]
  node [
    id 772
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 773
    label "czasopismo"
  ]
  node [
    id 774
    label "istota"
  ]
  node [
    id 775
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 776
    label "piwo"
  ]
  node [
    id 777
    label "lock"
  ]
  node [
    id 778
    label "absolut"
  ]
  node [
    id 779
    label "swat"
  ]
  node [
    id 780
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 781
    label "consort"
  ]
  node [
    id 782
    label "powi&#261;za&#263;"
  ]
  node [
    id 783
    label "entrance"
  ]
  node [
    id 784
    label "inscription"
  ]
  node [
    id 785
    label "akt"
  ]
  node [
    id 786
    label "op&#322;ata"
  ]
  node [
    id 787
    label "can"
  ]
  node [
    id 788
    label "m&#243;c"
  ]
  node [
    id 789
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 790
    label "work"
  ]
  node [
    id 791
    label "muzyka"
  ]
  node [
    id 792
    label "rola"
  ]
  node [
    id 793
    label "create"
  ]
  node [
    id 794
    label "wytwarza&#263;"
  ]
  node [
    id 795
    label "praca"
  ]
  node [
    id 796
    label "infimum"
  ]
  node [
    id 797
    label "float"
  ]
  node [
    id 798
    label "scene"
  ]
  node [
    id 799
    label "punkt"
  ]
  node [
    id 800
    label "nawr&#243;t_choroby"
  ]
  node [
    id 801
    label "odwzorowanie"
  ]
  node [
    id 802
    label "supremum"
  ]
  node [
    id 803
    label "mold"
  ]
  node [
    id 804
    label "funkcja"
  ]
  node [
    id 805
    label "rysunek"
  ]
  node [
    id 806
    label "blow"
  ]
  node [
    id 807
    label "potomstwo"
  ]
  node [
    id 808
    label "ruch"
  ]
  node [
    id 809
    label "pomys&#322;"
  ]
  node [
    id 810
    label "k&#322;ad"
  ]
  node [
    id 811
    label "injection"
  ]
  node [
    id 812
    label "armia"
  ]
  node [
    id 813
    label "matematyka"
  ]
  node [
    id 814
    label "throw"
  ]
  node [
    id 815
    label "projection"
  ]
  node [
    id 816
    label "jednostka"
  ]
  node [
    id 817
    label "osobi&#347;cie"
  ]
  node [
    id 818
    label "bezpo&#347;redni"
  ]
  node [
    id 819
    label "szczery"
  ]
  node [
    id 820
    label "intymny"
  ]
  node [
    id 821
    label "prywatny"
  ]
  node [
    id 822
    label "personalny"
  ]
  node [
    id 823
    label "w&#322;asny"
  ]
  node [
    id 824
    label "prywatnie"
  ]
  node [
    id 825
    label "emocjonalny"
  ]
  node [
    id 826
    label "play"
  ]
  node [
    id 827
    label "przeprowadza&#263;"
  ]
  node [
    id 828
    label "gra&#263;"
  ]
  node [
    id 829
    label "krytyczny"
  ]
  node [
    id 830
    label "prze&#347;miewczy"
  ]
  node [
    id 831
    label "z&#322;o&#347;liwie"
  ]
  node [
    id 832
    label "niemi&#322;y"
  ]
  node [
    id 833
    label "przekl&#281;ty"
  ]
  node [
    id 834
    label "celowy"
  ]
  node [
    id 835
    label "ci&#281;&#380;ki"
  ]
  node [
    id 836
    label "fall"
  ]
  node [
    id 837
    label "m&#243;wi&#263;"
  ]
  node [
    id 838
    label "pocina&#263;"
  ]
  node [
    id 839
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 840
    label "write_out"
  ]
  node [
    id 841
    label "usuwa&#263;"
  ]
  node [
    id 842
    label "hack"
  ]
  node [
    id 843
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 844
    label "kopiowa&#263;"
  ]
  node [
    id 845
    label "wy&#380;&#322;abia&#263;"
  ]
  node [
    id 846
    label "mordowa&#263;"
  ]
  node [
    id 847
    label "unwrap"
  ]
  node [
    id 848
    label "oddziela&#263;"
  ]
  node [
    id 849
    label "wytraca&#263;"
  ]
  node [
    id 850
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 851
    label "uderza&#263;"
  ]
  node [
    id 852
    label "term"
  ]
  node [
    id 853
    label "wezwanie"
  ]
  node [
    id 854
    label "leksem"
  ]
  node [
    id 855
    label "patron"
  ]
  node [
    id 856
    label "kochanek"
  ]
  node [
    id 857
    label "darczy&#324;ca"
  ]
  node [
    id 858
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 859
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 860
    label "weekend"
  ]
  node [
    id 861
    label "miesi&#261;c"
  ]
  node [
    id 862
    label "come_up"
  ]
  node [
    id 863
    label "straci&#263;"
  ]
  node [
    id 864
    label "przej&#347;&#263;"
  ]
  node [
    id 865
    label "zast&#261;pi&#263;"
  ]
  node [
    id 866
    label "sprawi&#263;"
  ]
  node [
    id 867
    label "zyska&#263;"
  ]
  node [
    id 868
    label "change"
  ]
  node [
    id 869
    label "zwyczajnie"
  ]
  node [
    id 870
    label "nijaki"
  ]
  node [
    id 871
    label "blandly"
  ]
  node [
    id 872
    label "niepokoj&#261;co"
  ]
  node [
    id 873
    label "&#378;le"
  ]
  node [
    id 874
    label "nieciekawy"
  ]
  node [
    id 875
    label "nieefektownie"
  ]
  node [
    id 876
    label "odcinek"
  ]
  node [
    id 877
    label "proste_sko&#347;ne"
  ]
  node [
    id 878
    label "straight_line"
  ]
  node [
    id 879
    label "trasa"
  ]
  node [
    id 880
    label "krzywa"
  ]
  node [
    id 881
    label "znaczny"
  ]
  node [
    id 882
    label "zauwa&#380;alnie"
  ]
  node [
    id 883
    label "swoisty"
  ]
  node [
    id 884
    label "interesowanie"
  ]
  node [
    id 885
    label "nietuzinkowy"
  ]
  node [
    id 886
    label "ciekawie"
  ]
  node [
    id 887
    label "indagator"
  ]
  node [
    id 888
    label "interesuj&#261;cy"
  ]
  node [
    id 889
    label "dziwny"
  ]
  node [
    id 890
    label "intryguj&#261;cy"
  ]
  node [
    id 891
    label "ch&#281;tny"
  ]
  node [
    id 892
    label "koniec"
  ]
  node [
    id 893
    label "conclusion"
  ]
  node [
    id 894
    label "coating"
  ]
  node [
    id 895
    label "runda"
  ]
  node [
    id 896
    label "j&#281;cze&#263;"
  ]
  node [
    id 897
    label "martwi&#263;_si&#281;"
  ]
  node [
    id 898
    label "wytrzymywa&#263;"
  ]
  node [
    id 899
    label "pochyla&#263;_si&#281;"
  ]
  node [
    id 900
    label "traci&#263;"
  ]
  node [
    id 901
    label "narzeka&#263;"
  ]
  node [
    id 902
    label "sting"
  ]
  node [
    id 903
    label "hurt"
  ]
  node [
    id 904
    label "boryka&#263;_si&#281;"
  ]
  node [
    id 905
    label "matuszka"
  ]
  node [
    id 906
    label "geneza"
  ]
  node [
    id 907
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 908
    label "czynnik"
  ]
  node [
    id 909
    label "poci&#261;ganie"
  ]
  node [
    id 910
    label "rezultat"
  ]
  node [
    id 911
    label "uprz&#261;&#380;"
  ]
  node [
    id 912
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 913
    label "subject"
  ]
  node [
    id 914
    label "prywatywny"
  ]
  node [
    id 915
    label "defect"
  ]
  node [
    id 916
    label "odej&#347;cie"
  ]
  node [
    id 917
    label "gap"
  ]
  node [
    id 918
    label "kr&#243;tki"
  ]
  node [
    id 919
    label "wyr&#243;b"
  ]
  node [
    id 920
    label "nieistnienie"
  ]
  node [
    id 921
    label "wada"
  ]
  node [
    id 922
    label "odej&#347;&#263;"
  ]
  node [
    id 923
    label "odchodzenie"
  ]
  node [
    id 924
    label "odchodzi&#263;"
  ]
  node [
    id 925
    label "czasokres"
  ]
  node [
    id 926
    label "trawienie"
  ]
  node [
    id 927
    label "kategoria_gramatyczna"
  ]
  node [
    id 928
    label "odczyt"
  ]
  node [
    id 929
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 930
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 931
    label "poprzedzenie"
  ]
  node [
    id 932
    label "koniugacja"
  ]
  node [
    id 933
    label "poprzedzi&#263;"
  ]
  node [
    id 934
    label "przep&#322;ywanie"
  ]
  node [
    id 935
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 936
    label "odwlekanie_si&#281;"
  ]
  node [
    id 937
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 938
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 939
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 940
    label "pochodzi&#263;"
  ]
  node [
    id 941
    label "czwarty_wymiar"
  ]
  node [
    id 942
    label "chronometria"
  ]
  node [
    id 943
    label "poprzedzanie"
  ]
  node [
    id 944
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 945
    label "pogoda"
  ]
  node [
    id 946
    label "zegar"
  ]
  node [
    id 947
    label "trawi&#263;"
  ]
  node [
    id 948
    label "pochodzenie"
  ]
  node [
    id 949
    label "poprzedza&#263;"
  ]
  node [
    id 950
    label "rachuba_czasu"
  ]
  node [
    id 951
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 952
    label "czasoprzestrze&#324;"
  ]
  node [
    id 953
    label "laba"
  ]
  node [
    id 954
    label "zorganizowa&#263;"
  ]
  node [
    id 955
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 956
    label "przerobi&#263;"
  ]
  node [
    id 957
    label "wystylizowa&#263;"
  ]
  node [
    id 958
    label "cause"
  ]
  node [
    id 959
    label "wydali&#263;"
  ]
  node [
    id 960
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 961
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 962
    label "post&#261;pi&#263;"
  ]
  node [
    id 963
    label "appoint"
  ]
  node [
    id 964
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 965
    label "nabra&#263;"
  ]
  node [
    id 966
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 967
    label "make"
  ]
  node [
    id 968
    label "challenge"
  ]
  node [
    id 969
    label "odcina&#263;"
  ]
  node [
    id 970
    label "odrzuca&#263;"
  ]
  node [
    id 971
    label "przestawa&#263;"
  ]
  node [
    id 972
    label "nieprawdopodobie&#324;stwo"
  ]
  node [
    id 973
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 974
    label "przerywa&#263;"
  ]
  node [
    id 975
    label "reject"
  ]
  node [
    id 976
    label "exsert"
  ]
  node [
    id 977
    label "trudno&#347;&#263;"
  ]
  node [
    id 978
    label "odmienno&#347;&#263;"
  ]
  node [
    id 979
    label "odwrotny"
  ]
  node [
    id 980
    label "obstruction"
  ]
  node [
    id 981
    label "reverse"
  ]
  node [
    id 982
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 983
    label "oskoma"
  ]
  node [
    id 984
    label "uczta"
  ]
  node [
    id 985
    label "emocja"
  ]
  node [
    id 986
    label "inclination"
  ]
  node [
    id 987
    label "zajawka"
  ]
  node [
    id 988
    label "nauka"
  ]
  node [
    id 989
    label "take_care"
  ]
  node [
    id 990
    label "troska&#263;_si&#281;"
  ]
  node [
    id 991
    label "zamierza&#263;"
  ]
  node [
    id 992
    label "rozpatrywa&#263;"
  ]
  node [
    id 993
    label "turn"
  ]
  node [
    id 994
    label "zr&#243;wna&#263;_si&#281;"
  ]
  node [
    id 995
    label "urosn&#261;&#263;"
  ]
  node [
    id 996
    label "wyrosn&#261;&#263;"
  ]
  node [
    id 997
    label "podrosn&#261;&#263;"
  ]
  node [
    id 998
    label "heed"
  ]
  node [
    id 999
    label "typ"
  ]
  node [
    id 1000
    label "pozowa&#263;"
  ]
  node [
    id 1001
    label "ideal"
  ]
  node [
    id 1002
    label "matryca"
  ]
  node [
    id 1003
    label "imitacja"
  ]
  node [
    id 1004
    label "motif"
  ]
  node [
    id 1005
    label "pozowanie"
  ]
  node [
    id 1006
    label "wz&#243;r"
  ]
  node [
    id 1007
    label "miniatura"
  ]
  node [
    id 1008
    label "prezenter"
  ]
  node [
    id 1009
    label "facet"
  ]
  node [
    id 1010
    label "orygina&#322;"
  ]
  node [
    id 1011
    label "mildew"
  ]
  node [
    id 1012
    label "spos&#243;b"
  ]
  node [
    id 1013
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1014
    label "adaptation"
  ]
  node [
    id 1015
    label "pomy&#347;lny"
  ]
  node [
    id 1016
    label "skuteczny"
  ]
  node [
    id 1017
    label "moralny"
  ]
  node [
    id 1018
    label "korzystny"
  ]
  node [
    id 1019
    label "zwrot"
  ]
  node [
    id 1020
    label "dobrze"
  ]
  node [
    id 1021
    label "pozytywny"
  ]
  node [
    id 1022
    label "grzeczny"
  ]
  node [
    id 1023
    label "powitanie"
  ]
  node [
    id 1024
    label "mi&#322;y"
  ]
  node [
    id 1025
    label "dobroczynny"
  ]
  node [
    id 1026
    label "pos&#322;uszny"
  ]
  node [
    id 1027
    label "ca&#322;y"
  ]
  node [
    id 1028
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1029
    label "czw&#243;rka"
  ]
  node [
    id 1030
    label "spokojny"
  ]
  node [
    id 1031
    label "&#347;mieszny"
  ]
  node [
    id 1032
    label "drogi"
  ]
  node [
    id 1033
    label "wiela"
  ]
  node [
    id 1034
    label "du&#380;y"
  ]
  node [
    id 1035
    label "mecz"
  ]
  node [
    id 1036
    label "service"
  ]
  node [
    id 1037
    label "wytw&#243;r"
  ]
  node [
    id 1038
    label "zak&#322;ad"
  ]
  node [
    id 1039
    label "us&#322;uga"
  ]
  node [
    id 1040
    label "uderzenie"
  ]
  node [
    id 1041
    label "doniesienie"
  ]
  node [
    id 1042
    label "zastawa"
  ]
  node [
    id 1043
    label "YouTube"
  ]
  node [
    id 1044
    label "porcja"
  ]
  node [
    id 1045
    label "consolidate"
  ]
  node [
    id 1046
    label "przejmowa&#263;"
  ]
  node [
    id 1047
    label "dostawa&#263;"
  ]
  node [
    id 1048
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 1049
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 1050
    label "pozyskiwa&#263;"
  ]
  node [
    id 1051
    label "meet"
  ]
  node [
    id 1052
    label "congregate"
  ]
  node [
    id 1053
    label "poci&#261;ga&#263;"
  ]
  node [
    id 1054
    label "uk&#322;ada&#263;"
  ]
  node [
    id 1055
    label "wzbiera&#263;"
  ]
  node [
    id 1056
    label "gromadzi&#263;"
  ]
  node [
    id 1057
    label "powodowa&#263;"
  ]
  node [
    id 1058
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 1059
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1060
    label "capacity"
  ]
  node [
    id 1061
    label "preserve"
  ]
  node [
    id 1062
    label "uchroni&#263;"
  ]
  node [
    id 1063
    label "postpone"
  ]
  node [
    id 1064
    label "znie&#347;&#263;"
  ]
  node [
    id 1065
    label "ulepsza&#263;"
  ]
  node [
    id 1066
    label "optimize"
  ]
  node [
    id 1067
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1068
    label "temat"
  ]
  node [
    id 1069
    label "wojsko"
  ]
  node [
    id 1070
    label "magnitude"
  ]
  node [
    id 1071
    label "energia"
  ]
  node [
    id 1072
    label "wuchta"
  ]
  node [
    id 1073
    label "parametr"
  ]
  node [
    id 1074
    label "moment_si&#322;y"
  ]
  node [
    id 1075
    label "przemoc"
  ]
  node [
    id 1076
    label "mn&#243;stwo"
  ]
  node [
    id 1077
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1078
    label "rozwi&#261;zanie"
  ]
  node [
    id 1079
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1080
    label "potencja"
  ]
  node [
    id 1081
    label "zaleta"
  ]
  node [
    id 1082
    label "cover"
  ]
  node [
    id 1083
    label "p&#322;aci&#263;"
  ]
  node [
    id 1084
    label "zap&#322;adnia&#263;"
  ]
  node [
    id 1085
    label "defray"
  ]
  node [
    id 1086
    label "smother"
  ]
  node [
    id 1087
    label "przykrywa&#263;"
  ]
  node [
    id 1088
    label "supernatural"
  ]
  node [
    id 1089
    label "rozwija&#263;"
  ]
  node [
    id 1090
    label "r&#243;wna&#263;"
  ]
  node [
    id 1091
    label "maskowa&#263;"
  ]
  node [
    id 1092
    label "nak&#322;ad"
  ]
  node [
    id 1093
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 1094
    label "sumpt"
  ]
  node [
    id 1095
    label "wydatek"
  ]
  node [
    id 1096
    label "uzupe&#322;nianie"
  ]
  node [
    id 1097
    label "liczenie"
  ]
  node [
    id 1098
    label "dop&#322;acanie"
  ]
  node [
    id 1099
    label "do&#347;wietlanie"
  ]
  node [
    id 1100
    label "do&#322;&#261;czanie"
  ]
  node [
    id 1101
    label "suma"
  ]
  node [
    id 1102
    label "wspominanie"
  ]
  node [
    id 1103
    label "rozdzielno&#347;&#263;_mno&#380;enia_wzgl&#281;dem_dodawania"
  ]
  node [
    id 1104
    label "summation"
  ]
  node [
    id 1105
    label "dokupowanie"
  ]
  node [
    id 1106
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 1107
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 1108
    label "addition"
  ]
  node [
    id 1109
    label "znak_matematyczny"
  ]
  node [
    id 1110
    label "plus"
  ]
  node [
    id 1111
    label "urz&#261;dzenie_wodne"
  ]
  node [
    id 1112
    label "rzecz"
  ]
  node [
    id 1113
    label "dodatek"
  ]
  node [
    id 1114
    label "obramienie"
  ]
  node [
    id 1115
    label "forum"
  ]
  node [
    id 1116
    label "wej&#347;cie"
  ]
  node [
    id 1117
    label "Onet"
  ]
  node [
    id 1118
    label "archiwolta"
  ]
  node [
    id 1119
    label "wyzyskiwa&#263;"
  ]
  node [
    id 1120
    label "wydobywa&#263;"
  ]
  node [
    id 1121
    label "stosowny"
  ]
  node [
    id 1122
    label "nale&#380;ycie"
  ]
  node [
    id 1123
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1124
    label "nale&#380;nie"
  ]
  node [
    id 1125
    label "cia&#322;o"
  ]
  node [
    id 1126
    label "plac"
  ]
  node [
    id 1127
    label "uwaga"
  ]
  node [
    id 1128
    label "przestrze&#324;"
  ]
  node [
    id 1129
    label "status"
  ]
  node [
    id 1130
    label "rz&#261;d"
  ]
  node [
    id 1131
    label "location"
  ]
  node [
    id 1132
    label "warunek_lokalowy"
  ]
  node [
    id 1133
    label "uplasowa&#263;_si&#281;"
  ]
  node [
    id 1134
    label "podzia&#322;"
  ]
  node [
    id 1135
    label "competence"
  ]
  node [
    id 1136
    label "stopie&#324;"
  ]
  node [
    id 1137
    label "ocena"
  ]
  node [
    id 1138
    label "kolejno&#347;&#263;"
  ]
  node [
    id 1139
    label "plasowanie_si&#281;"
  ]
  node [
    id 1140
    label "poj&#281;cie"
  ]
  node [
    id 1141
    label "uplasowanie_si&#281;"
  ]
  node [
    id 1142
    label "plasowa&#263;_si&#281;"
  ]
  node [
    id 1143
    label "distribution"
  ]
  node [
    id 1144
    label "division"
  ]
  node [
    id 1145
    label "kategoria"
  ]
  node [
    id 1146
    label "kwadrat_magiczny"
  ]
  node [
    id 1147
    label "pierwiastek"
  ]
  node [
    id 1148
    label "rozmiar"
  ]
  node [
    id 1149
    label "number"
  ]
  node [
    id 1150
    label "tre&#347;ciwy"
  ]
  node [
    id 1151
    label "&#322;adny"
  ]
  node [
    id 1152
    label "jasny"
  ]
  node [
    id 1153
    label "skupiony"
  ]
  node [
    id 1154
    label "po&#380;ywny"
  ]
  node [
    id 1155
    label "ogarni&#281;ty"
  ]
  node [
    id 1156
    label "konkretnie"
  ]
  node [
    id 1157
    label "posilny"
  ]
  node [
    id 1158
    label "abstrakcyjny"
  ]
  node [
    id 1159
    label "solidnie"
  ]
  node [
    id 1160
    label "niez&#322;y"
  ]
  node [
    id 1161
    label "rewaluowa&#263;"
  ]
  node [
    id 1162
    label "wabik"
  ]
  node [
    id 1163
    label "wskazywanie"
  ]
  node [
    id 1164
    label "korzy&#347;&#263;"
  ]
  node [
    id 1165
    label "worth"
  ]
  node [
    id 1166
    label "rewaluowanie"
  ]
  node [
    id 1167
    label "zmienna"
  ]
  node [
    id 1168
    label "zrewaluowa&#263;"
  ]
  node [
    id 1169
    label "cel"
  ]
  node [
    id 1170
    label "wskazywa&#263;"
  ]
  node [
    id 1171
    label "zrewaluowanie"
  ]
  node [
    id 1172
    label "urynkowienie"
  ]
  node [
    id 1173
    label "rynkowo"
  ]
  node [
    id 1174
    label "urynkawianie"
  ]
  node [
    id 1175
    label "harmonijnie"
  ]
  node [
    id 1176
    label "poprostu"
  ]
  node [
    id 1177
    label "zwyczajny"
  ]
  node [
    id 1178
    label "stale"
  ]
  node [
    id 1179
    label "regularny"
  ]
  node [
    id 1180
    label "ustawia&#263;"
  ]
  node [
    id 1181
    label "wierzy&#263;"
  ]
  node [
    id 1182
    label "przyjmowa&#263;"
  ]
  node [
    id 1183
    label "kupywa&#263;"
  ]
  node [
    id 1184
    label "production"
  ]
  node [
    id 1185
    label "substancja"
  ]
  node [
    id 1186
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 1187
    label "tobo&#322;ek"
  ]
  node [
    id 1188
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 1189
    label "scali&#263;"
  ]
  node [
    id 1190
    label "zawi&#261;za&#263;"
  ]
  node [
    id 1191
    label "zatrzyma&#263;"
  ]
  node [
    id 1192
    label "form"
  ]
  node [
    id 1193
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 1194
    label "unify"
  ]
  node [
    id 1195
    label "incorporate"
  ]
  node [
    id 1196
    label "wi&#281;&#378;"
  ]
  node [
    id 1197
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 1198
    label "w&#281;ze&#322;"
  ]
  node [
    id 1199
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 1200
    label "opakowa&#263;"
  ]
  node [
    id 1201
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 1202
    label "cement"
  ]
  node [
    id 1203
    label "zaprawa"
  ]
  node [
    id 1204
    label "relate"
  ]
  node [
    id 1205
    label "ask"
  ]
  node [
    id 1206
    label "wywy&#380;sza&#263;"
  ]
  node [
    id 1207
    label "domaga&#263;_si&#281;"
  ]
  node [
    id 1208
    label "zachwala&#263;"
  ]
  node [
    id 1209
    label "nagradza&#263;"
  ]
  node [
    id 1210
    label "publicize"
  ]
  node [
    id 1211
    label "glorify"
  ]
  node [
    id 1212
    label "g&#322;&#243;wny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 41
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 12
    target 236
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 238
  ]
  edge [
    source 12
    target 239
  ]
  edge [
    source 12
    target 240
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 242
  ]
  edge [
    source 12
    target 243
  ]
  edge [
    source 12
    target 244
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 245
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 38
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 42
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 15
    target 84
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 26
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 246
  ]
  edge [
    source 16
    target 247
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 249
  ]
  edge [
    source 17
    target 250
  ]
  edge [
    source 17
    target 251
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 252
  ]
  edge [
    source 20
    target 253
  ]
  edge [
    source 20
    target 254
  ]
  edge [
    source 20
    target 255
  ]
  edge [
    source 20
    target 61
  ]
  edge [
    source 20
    target 99
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 256
  ]
  edge [
    source 21
    target 257
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 258
  ]
  edge [
    source 22
    target 259
  ]
  edge [
    source 22
    target 260
  ]
  edge [
    source 22
    target 261
  ]
  edge [
    source 22
    target 262
  ]
  edge [
    source 22
    target 263
  ]
  edge [
    source 22
    target 100
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 23
    target 281
  ]
  edge [
    source 23
    target 282
  ]
  edge [
    source 23
    target 283
  ]
  edge [
    source 23
    target 284
  ]
  edge [
    source 23
    target 285
  ]
  edge [
    source 23
    target 286
  ]
  edge [
    source 23
    target 287
  ]
  edge [
    source 23
    target 288
  ]
  edge [
    source 23
    target 289
  ]
  edge [
    source 23
    target 290
  ]
  edge [
    source 23
    target 291
  ]
  edge [
    source 23
    target 292
  ]
  edge [
    source 23
    target 293
  ]
  edge [
    source 23
    target 294
  ]
  edge [
    source 23
    target 295
  ]
  edge [
    source 23
    target 296
  ]
  edge [
    source 23
    target 70
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 63
  ]
  edge [
    source 23
    target 110
  ]
  edge [
    source 23
    target 119
  ]
  edge [
    source 23
    target 82
  ]
  edge [
    source 23
    target 183
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 297
  ]
  edge [
    source 24
    target 298
  ]
  edge [
    source 24
    target 299
  ]
  edge [
    source 24
    target 300
  ]
  edge [
    source 24
    target 301
  ]
  edge [
    source 24
    target 161
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 25
    target 84
  ]
  edge [
    source 25
    target 85
  ]
  edge [
    source 25
    target 302
  ]
  edge [
    source 25
    target 303
  ]
  edge [
    source 25
    target 304
  ]
  edge [
    source 25
    target 305
  ]
  edge [
    source 25
    target 306
  ]
  edge [
    source 25
    target 307
  ]
  edge [
    source 25
    target 308
  ]
  edge [
    source 25
    target 309
  ]
  edge [
    source 25
    target 310
  ]
  edge [
    source 25
    target 311
  ]
  edge [
    source 25
    target 312
  ]
  edge [
    source 25
    target 313
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 50
  ]
  edge [
    source 26
    target 67
  ]
  edge [
    source 26
    target 68
  ]
  edge [
    source 26
    target 104
  ]
  edge [
    source 26
    target 105
  ]
  edge [
    source 26
    target 112
  ]
  edge [
    source 26
    target 113
  ]
  edge [
    source 26
    target 153
  ]
  edge [
    source 26
    target 154
  ]
  edge [
    source 26
    target 158
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 169
  ]
  edge [
    source 26
    target 170
  ]
  edge [
    source 26
    target 184
  ]
  edge [
    source 26
    target 314
  ]
  edge [
    source 26
    target 315
  ]
  edge [
    source 26
    target 316
  ]
  edge [
    source 26
    target 317
  ]
  edge [
    source 26
    target 318
  ]
  edge [
    source 26
    target 221
  ]
  edge [
    source 26
    target 319
  ]
  edge [
    source 26
    target 320
  ]
  edge [
    source 26
    target 321
  ]
  edge [
    source 26
    target 88
  ]
  edge [
    source 26
    target 97
  ]
  edge [
    source 26
    target 130
  ]
  edge [
    source 26
    target 132
  ]
  edge [
    source 26
    target 163
  ]
  edge [
    source 26
    target 188
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 97
  ]
  edge [
    source 28
    target 98
  ]
  edge [
    source 28
    target 165
  ]
  edge [
    source 28
    target 82
  ]
  edge [
    source 28
    target 322
  ]
  edge [
    source 28
    target 323
  ]
  edge [
    source 28
    target 324
  ]
  edge [
    source 28
    target 325
  ]
  edge [
    source 28
    target 326
  ]
  edge [
    source 28
    target 327
  ]
  edge [
    source 28
    target 328
  ]
  edge [
    source 28
    target 329
  ]
  edge [
    source 28
    target 330
  ]
  edge [
    source 28
    target 331
  ]
  edge [
    source 28
    target 332
  ]
  edge [
    source 28
    target 333
  ]
  edge [
    source 28
    target 334
  ]
  edge [
    source 28
    target 335
  ]
  edge [
    source 28
    target 106
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 336
  ]
  edge [
    source 30
    target 123
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 337
  ]
  edge [
    source 31
    target 338
  ]
  edge [
    source 31
    target 339
  ]
  edge [
    source 31
    target 340
  ]
  edge [
    source 31
    target 341
  ]
  edge [
    source 31
    target 342
  ]
  edge [
    source 31
    target 343
  ]
  edge [
    source 31
    target 344
  ]
  edge [
    source 31
    target 345
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 346
  ]
  edge [
    source 33
    target 347
  ]
  edge [
    source 33
    target 348
  ]
  edge [
    source 33
    target 349
  ]
  edge [
    source 33
    target 350
  ]
  edge [
    source 33
    target 351
  ]
  edge [
    source 33
    target 352
  ]
  edge [
    source 33
    target 353
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 354
  ]
  edge [
    source 35
    target 355
  ]
  edge [
    source 35
    target 356
  ]
  edge [
    source 35
    target 357
  ]
  edge [
    source 35
    target 358
  ]
  edge [
    source 35
    target 359
  ]
  edge [
    source 35
    target 360
  ]
  edge [
    source 35
    target 361
  ]
  edge [
    source 35
    target 362
  ]
  edge [
    source 35
    target 363
  ]
  edge [
    source 35
    target 46
  ]
  edge [
    source 35
    target 123
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 364
  ]
  edge [
    source 36
    target 276
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 365
  ]
  edge [
    source 37
    target 366
  ]
  edge [
    source 37
    target 367
  ]
  edge [
    source 37
    target 368
  ]
  edge [
    source 37
    target 369
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 370
  ]
  edge [
    source 38
    target 371
  ]
  edge [
    source 38
    target 372
  ]
  edge [
    source 38
    target 373
  ]
  edge [
    source 38
    target 374
  ]
  edge [
    source 38
    target 375
  ]
  edge [
    source 38
    target 376
  ]
  edge [
    source 38
    target 377
  ]
  edge [
    source 38
    target 72
  ]
  edge [
    source 38
    target 92
  ]
  edge [
    source 38
    target 83
  ]
  edge [
    source 38
    target 202
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 204
  ]
  edge [
    source 39
    target 378
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 83
  ]
  edge [
    source 41
    target 379
  ]
  edge [
    source 41
    target 380
  ]
  edge [
    source 41
    target 381
  ]
  edge [
    source 41
    target 382
  ]
  edge [
    source 41
    target 383
  ]
  edge [
    source 41
    target 384
  ]
  edge [
    source 41
    target 185
  ]
  edge [
    source 41
    target 144
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 385
  ]
  edge [
    source 42
    target 386
  ]
  edge [
    source 42
    target 387
  ]
  edge [
    source 42
    target 388
  ]
  edge [
    source 42
    target 389
  ]
  edge [
    source 42
    target 390
  ]
  edge [
    source 42
    target 391
  ]
  edge [
    source 42
    target 392
  ]
  edge [
    source 42
    target 393
  ]
  edge [
    source 42
    target 382
  ]
  edge [
    source 42
    target 394
  ]
  edge [
    source 42
    target 395
  ]
  edge [
    source 42
    target 396
  ]
  edge [
    source 42
    target 397
  ]
  edge [
    source 42
    target 398
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 399
  ]
  edge [
    source 43
    target 400
  ]
  edge [
    source 43
    target 401
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 402
  ]
  edge [
    source 44
    target 403
  ]
  edge [
    source 44
    target 404
  ]
  edge [
    source 44
    target 405
  ]
  edge [
    source 44
    target 406
  ]
  edge [
    source 44
    target 407
  ]
  edge [
    source 44
    target 408
  ]
  edge [
    source 44
    target 49
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 409
  ]
  edge [
    source 46
    target 410
  ]
  edge [
    source 46
    target 411
  ]
  edge [
    source 46
    target 412
  ]
  edge [
    source 46
    target 413
  ]
  edge [
    source 46
    target 414
  ]
  edge [
    source 46
    target 123
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 415
  ]
  edge [
    source 47
    target 416
  ]
  edge [
    source 47
    target 417
  ]
  edge [
    source 47
    target 418
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 419
  ]
  edge [
    source 48
    target 420
  ]
  edge [
    source 48
    target 421
  ]
  edge [
    source 48
    target 422
  ]
  edge [
    source 48
    target 423
  ]
  edge [
    source 48
    target 424
  ]
  edge [
    source 48
    target 425
  ]
  edge [
    source 48
    target 426
  ]
  edge [
    source 48
    target 427
  ]
  edge [
    source 49
    target 428
  ]
  edge [
    source 49
    target 429
  ]
  edge [
    source 49
    target 430
  ]
  edge [
    source 49
    target 431
  ]
  edge [
    source 49
    target 432
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 433
  ]
  edge [
    source 50
    target 434
  ]
  edge [
    source 50
    target 435
  ]
  edge [
    source 50
    target 436
  ]
  edge [
    source 50
    target 437
  ]
  edge [
    source 50
    target 438
  ]
  edge [
    source 50
    target 439
  ]
  edge [
    source 50
    target 440
  ]
  edge [
    source 50
    target 441
  ]
  edge [
    source 50
    target 442
  ]
  edge [
    source 50
    target 443
  ]
  edge [
    source 50
    target 444
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 445
  ]
  edge [
    source 51
    target 446
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 447
  ]
  edge [
    source 53
    target 448
  ]
  edge [
    source 53
    target 449
  ]
  edge [
    source 53
    target 450
  ]
  edge [
    source 53
    target 451
  ]
  edge [
    source 53
    target 452
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 453
  ]
  edge [
    source 54
    target 454
  ]
  edge [
    source 54
    target 455
  ]
  edge [
    source 54
    target 456
  ]
  edge [
    source 54
    target 457
  ]
  edge [
    source 54
    target 458
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 459
  ]
  edge [
    source 55
    target 460
  ]
  edge [
    source 55
    target 461
  ]
  edge [
    source 55
    target 462
  ]
  edge [
    source 55
    target 463
  ]
  edge [
    source 55
    target 464
  ]
  edge [
    source 55
    target 465
  ]
  edge [
    source 55
    target 466
  ]
  edge [
    source 55
    target 467
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 229
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 468
  ]
  edge [
    source 57
    target 469
  ]
  edge [
    source 57
    target 321
  ]
  edge [
    source 57
    target 470
  ]
  edge [
    source 57
    target 174
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 162
  ]
  edge [
    source 58
    target 163
  ]
  edge [
    source 58
    target 471
  ]
  edge [
    source 58
    target 472
  ]
  edge [
    source 58
    target 473
  ]
  edge [
    source 58
    target 474
  ]
  edge [
    source 58
    target 475
  ]
  edge [
    source 58
    target 476
  ]
  edge [
    source 58
    target 477
  ]
  edge [
    source 58
    target 478
  ]
  edge [
    source 58
    target 479
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 480
  ]
  edge [
    source 59
    target 481
  ]
  edge [
    source 59
    target 482
  ]
  edge [
    source 59
    target 65
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 110
  ]
  edge [
    source 60
    target 111
  ]
  edge [
    source 60
    target 483
  ]
  edge [
    source 60
    target 484
  ]
  edge [
    source 60
    target 88
  ]
  edge [
    source 60
    target 485
  ]
  edge [
    source 60
    target 486
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 487
  ]
  edge [
    source 61
    target 488
  ]
  edge [
    source 61
    target 489
  ]
  edge [
    source 61
    target 490
  ]
  edge [
    source 61
    target 491
  ]
  edge [
    source 61
    target 492
  ]
  edge [
    source 61
    target 493
  ]
  edge [
    source 61
    target 494
  ]
  edge [
    source 61
    target 495
  ]
  edge [
    source 61
    target 80
  ]
  edge [
    source 61
    target 496
  ]
  edge [
    source 61
    target 497
  ]
  edge [
    source 61
    target 498
  ]
  edge [
    source 61
    target 499
  ]
  edge [
    source 61
    target 500
  ]
  edge [
    source 61
    target 501
  ]
  edge [
    source 61
    target 502
  ]
  edge [
    source 61
    target 503
  ]
  edge [
    source 61
    target 504
  ]
  edge [
    source 61
    target 99
  ]
  edge [
    source 61
    target 128
  ]
  edge [
    source 61
    target 133
  ]
  edge [
    source 61
    target 138
  ]
  edge [
    source 61
    target 189
  ]
  edge [
    source 61
    target 194
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 505
  ]
  edge [
    source 62
    target 506
  ]
  edge [
    source 62
    target 507
  ]
  edge [
    source 62
    target 508
  ]
  edge [
    source 62
    target 509
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 110
  ]
  edge [
    source 63
    target 119
  ]
  edge [
    source 63
    target 82
  ]
  edge [
    source 63
    target 183
  ]
  edge [
    source 63
    target 203
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 89
  ]
  edge [
    source 65
    target 510
  ]
  edge [
    source 65
    target 511
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 512
  ]
  edge [
    source 66
    target 513
  ]
  edge [
    source 66
    target 514
  ]
  edge [
    source 66
    target 165
  ]
  edge [
    source 66
    target 515
  ]
  edge [
    source 66
    target 393
  ]
  edge [
    source 66
    target 516
  ]
  edge [
    source 66
    target 517
  ]
  edge [
    source 66
    target 518
  ]
  edge [
    source 66
    target 519
  ]
  edge [
    source 66
    target 520
  ]
  edge [
    source 67
    target 521
  ]
  edge [
    source 67
    target 522
  ]
  edge [
    source 67
    target 523
  ]
  edge [
    source 67
    target 524
  ]
  edge [
    source 67
    target 525
  ]
  edge [
    source 67
    target 526
  ]
  edge [
    source 67
    target 527
  ]
  edge [
    source 67
    target 393
  ]
  edge [
    source 67
    target 528
  ]
  edge [
    source 67
    target 529
  ]
  edge [
    source 67
    target 530
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 297
  ]
  edge [
    source 68
    target 531
  ]
  edge [
    source 68
    target 300
  ]
  edge [
    source 68
    target 532
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 533
  ]
  edge [
    source 69
    target 534
  ]
  edge [
    source 69
    target 535
  ]
  edge [
    source 69
    target 536
  ]
  edge [
    source 69
    target 537
  ]
  edge [
    source 69
    target 538
  ]
  edge [
    source 69
    target 381
  ]
  edge [
    source 69
    target 539
  ]
  edge [
    source 69
    target 540
  ]
  edge [
    source 69
    target 541
  ]
  edge [
    source 69
    target 542
  ]
  edge [
    source 69
    target 543
  ]
  edge [
    source 69
    target 544
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 76
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 70
    target 88
  ]
  edge [
    source 70
    target 89
  ]
  edge [
    source 70
    target 325
  ]
  edge [
    source 70
    target 545
  ]
  edge [
    source 70
    target 546
  ]
  edge [
    source 70
    target 547
  ]
  edge [
    source 70
    target 328
  ]
  edge [
    source 70
    target 332
  ]
  edge [
    source 70
    target 334
  ]
  edge [
    source 70
    target 323
  ]
  edge [
    source 70
    target 548
  ]
  edge [
    source 70
    target 549
  ]
  edge [
    source 70
    target 550
  ]
  edge [
    source 70
    target 551
  ]
  edge [
    source 70
    target 552
  ]
  edge [
    source 70
    target 333
  ]
  edge [
    source 70
    target 553
  ]
  edge [
    source 70
    target 322
  ]
  edge [
    source 70
    target 324
  ]
  edge [
    source 70
    target 554
  ]
  edge [
    source 70
    target 327
  ]
  edge [
    source 70
    target 555
  ]
  edge [
    source 70
    target 331
  ]
  edge [
    source 70
    target 335
  ]
  edge [
    source 70
    target 556
  ]
  edge [
    source 70
    target 288
  ]
  edge [
    source 70
    target 557
  ]
  edge [
    source 70
    target 326
  ]
  edge [
    source 70
    target 558
  ]
  edge [
    source 70
    target 329
  ]
  edge [
    source 70
    target 559
  ]
  edge [
    source 70
    target 560
  ]
  edge [
    source 70
    target 330
  ]
  edge [
    source 70
    target 561
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 77
  ]
  edge [
    source 71
    target 562
  ]
  edge [
    source 71
    target 563
  ]
  edge [
    source 71
    target 356
  ]
  edge [
    source 71
    target 564
  ]
  edge [
    source 71
    target 565
  ]
  edge [
    source 71
    target 566
  ]
  edge [
    source 71
    target 567
  ]
  edge [
    source 71
    target 568
  ]
  edge [
    source 72
    target 569
  ]
  edge [
    source 72
    target 570
  ]
  edge [
    source 72
    target 92
  ]
  edge [
    source 72
    target 83
  ]
  edge [
    source 72
    target 202
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 571
  ]
  edge [
    source 74
    target 572
  ]
  edge [
    source 74
    target 573
  ]
  edge [
    source 74
    target 574
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 575
  ]
  edge [
    source 75
    target 576
  ]
  edge [
    source 75
    target 577
  ]
  edge [
    source 75
    target 578
  ]
  edge [
    source 75
    target 579
  ]
  edge [
    source 75
    target 580
  ]
  edge [
    source 75
    target 581
  ]
  edge [
    source 75
    target 582
  ]
  edge [
    source 76
    target 583
  ]
  edge [
    source 76
    target 197
  ]
  edge [
    source 76
    target 584
  ]
  edge [
    source 76
    target 585
  ]
  edge [
    source 76
    target 586
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 587
  ]
  edge [
    source 77
    target 241
  ]
  edge [
    source 77
    target 588
  ]
  edge [
    source 77
    target 589
  ]
  edge [
    source 77
    target 590
  ]
  edge [
    source 78
    target 591
  ]
  edge [
    source 78
    target 592
  ]
  edge [
    source 78
    target 593
  ]
  edge [
    source 78
    target 594
  ]
  edge [
    source 78
    target 595
  ]
  edge [
    source 78
    target 596
  ]
  edge [
    source 78
    target 597
  ]
  edge [
    source 78
    target 598
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 599
  ]
  edge [
    source 79
    target 600
  ]
  edge [
    source 79
    target 601
  ]
  edge [
    source 79
    target 602
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 603
  ]
  edge [
    source 80
    target 346
  ]
  edge [
    source 80
    target 156
  ]
  edge [
    source 80
    target 604
  ]
  edge [
    source 80
    target 605
  ]
  edge [
    source 80
    target 606
  ]
  edge [
    source 80
    target 607
  ]
  edge [
    source 80
    target 608
  ]
  edge [
    source 80
    target 609
  ]
  edge [
    source 80
    target 610
  ]
  edge [
    source 80
    target 611
  ]
  edge [
    source 80
    target 612
  ]
  edge [
    source 80
    target 613
  ]
  edge [
    source 80
    target 614
  ]
  edge [
    source 80
    target 615
  ]
  edge [
    source 80
    target 616
  ]
  edge [
    source 80
    target 617
  ]
  edge [
    source 80
    target 618
  ]
  edge [
    source 80
    target 619
  ]
  edge [
    source 80
    target 620
  ]
  edge [
    source 80
    target 621
  ]
  edge [
    source 80
    target 622
  ]
  edge [
    source 80
    target 623
  ]
  edge [
    source 80
    target 624
  ]
  edge [
    source 80
    target 625
  ]
  edge [
    source 80
    target 626
  ]
  edge [
    source 80
    target 627
  ]
  edge [
    source 80
    target 628
  ]
  edge [
    source 80
    target 629
  ]
  edge [
    source 80
    target 630
  ]
  edge [
    source 80
    target 631
  ]
  edge [
    source 80
    target 632
  ]
  edge [
    source 80
    target 633
  ]
  edge [
    source 80
    target 634
  ]
  edge [
    source 80
    target 635
  ]
  edge [
    source 80
    target 636
  ]
  edge [
    source 80
    target 637
  ]
  edge [
    source 80
    target 638
  ]
  edge [
    source 80
    target 639
  ]
  edge [
    source 80
    target 640
  ]
  edge [
    source 80
    target 641
  ]
  edge [
    source 80
    target 642
  ]
  edge [
    source 80
    target 643
  ]
  edge [
    source 80
    target 644
  ]
  edge [
    source 80
    target 645
  ]
  edge [
    source 80
    target 646
  ]
  edge [
    source 80
    target 647
  ]
  edge [
    source 80
    target 648
  ]
  edge [
    source 80
    target 649
  ]
  edge [
    source 80
    target 650
  ]
  edge [
    source 80
    target 651
  ]
  edge [
    source 80
    target 652
  ]
  edge [
    source 80
    target 653
  ]
  edge [
    source 80
    target 506
  ]
  edge [
    source 80
    target 654
  ]
  edge [
    source 80
    target 655
  ]
  edge [
    source 80
    target 656
  ]
  edge [
    source 80
    target 657
  ]
  edge [
    source 80
    target 658
  ]
  edge [
    source 80
    target 659
  ]
  edge [
    source 80
    target 660
  ]
  edge [
    source 80
    target 661
  ]
  edge [
    source 80
    target 662
  ]
  edge [
    source 80
    target 663
  ]
  edge [
    source 80
    target 664
  ]
  edge [
    source 80
    target 665
  ]
  edge [
    source 80
    target 666
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 166
  ]
  edge [
    source 82
    target 667
  ]
  edge [
    source 82
    target 158
  ]
  edge [
    source 82
    target 110
  ]
  edge [
    source 82
    target 119
  ]
  edge [
    source 82
    target 183
  ]
  edge [
    source 82
    target 203
  ]
  edge [
    source 83
    target 159
  ]
  edge [
    source 83
    target 668
  ]
  edge [
    source 83
    target 92
  ]
  edge [
    source 83
    target 202
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 669
  ]
  edge [
    source 85
    target 670
  ]
  edge [
    source 85
    target 671
  ]
  edge [
    source 85
    target 537
  ]
  edge [
    source 85
    target 672
  ]
  edge [
    source 85
    target 673
  ]
  edge [
    source 85
    target 674
  ]
  edge [
    source 85
    target 675
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 90
  ]
  edge [
    source 87
    target 676
  ]
  edge [
    source 87
    target 551
  ]
  edge [
    source 87
    target 677
  ]
  edge [
    source 87
    target 678
  ]
  edge [
    source 87
    target 679
  ]
  edge [
    source 87
    target 680
  ]
  edge [
    source 87
    target 681
  ]
  edge [
    source 87
    target 682
  ]
  edge [
    source 88
    target 683
  ]
  edge [
    source 88
    target 684
  ]
  edge [
    source 88
    target 132
  ]
  edge [
    source 89
    target 156
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 92
    target 202
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 102
  ]
  edge [
    source 93
    target 103
  ]
  edge [
    source 93
    target 135
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 212
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 133
  ]
  edge [
    source 95
    target 184
  ]
  edge [
    source 95
    target 685
  ]
  edge [
    source 95
    target 686
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 158
  ]
  edge [
    source 96
    target 202
  ]
  edge [
    source 96
    target 156
  ]
  edge [
    source 96
    target 687
  ]
  edge [
    source 96
    target 190
  ]
  edge [
    source 97
    target 688
  ]
  edge [
    source 97
    target 689
  ]
  edge [
    source 97
    target 690
  ]
  edge [
    source 97
    target 691
  ]
  edge [
    source 97
    target 692
  ]
  edge [
    source 97
    target 693
  ]
  edge [
    source 97
    target 694
  ]
  edge [
    source 97
    target 695
  ]
  edge [
    source 97
    target 696
  ]
  edge [
    source 97
    target 697
  ]
  edge [
    source 97
    target 698
  ]
  edge [
    source 97
    target 699
  ]
  edge [
    source 97
    target 700
  ]
  edge [
    source 97
    target 701
  ]
  edge [
    source 97
    target 702
  ]
  edge [
    source 97
    target 703
  ]
  edge [
    source 97
    target 704
  ]
  edge [
    source 97
    target 705
  ]
  edge [
    source 97
    target 706
  ]
  edge [
    source 97
    target 707
  ]
  edge [
    source 97
    target 351
  ]
  edge [
    source 97
    target 130
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 708
  ]
  edge [
    source 98
    target 709
  ]
  edge [
    source 98
    target 710
  ]
  edge [
    source 98
    target 711
  ]
  edge [
    source 98
    target 105
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 128
  ]
  edge [
    source 99
    target 133
  ]
  edge [
    source 99
    target 138
  ]
  edge [
    source 99
    target 189
  ]
  edge [
    source 99
    target 194
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 712
  ]
  edge [
    source 100
    target 713
  ]
  edge [
    source 100
    target 714
  ]
  edge [
    source 100
    target 715
  ]
  edge [
    source 100
    target 716
  ]
  edge [
    source 100
    target 717
  ]
  edge [
    source 100
    target 718
  ]
  edge [
    source 100
    target 719
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 720
  ]
  edge [
    source 103
    target 721
  ]
  edge [
    source 103
    target 722
  ]
  edge [
    source 103
    target 723
  ]
  edge [
    source 103
    target 300
  ]
  edge [
    source 104
    target 724
  ]
  edge [
    source 104
    target 725
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 726
  ]
  edge [
    source 105
    target 727
  ]
  edge [
    source 105
    target 728
  ]
  edge [
    source 105
    target 729
  ]
  edge [
    source 105
    target 730
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 731
  ]
  edge [
    source 106
    target 732
  ]
  edge [
    source 106
    target 733
  ]
  edge [
    source 106
    target 734
  ]
  edge [
    source 106
    target 735
  ]
  edge [
    source 106
    target 736
  ]
  edge [
    source 106
    target 737
  ]
  edge [
    source 106
    target 738
  ]
  edge [
    source 106
    target 739
  ]
  edge [
    source 106
    target 740
  ]
  edge [
    source 106
    target 330
  ]
  edge [
    source 106
    target 741
  ]
  edge [
    source 106
    target 742
  ]
  edge [
    source 106
    target 743
  ]
  edge [
    source 106
    target 744
  ]
  edge [
    source 106
    target 745
  ]
  edge [
    source 106
    target 746
  ]
  edge [
    source 106
    target 747
  ]
  edge [
    source 106
    target 748
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 749
  ]
  edge [
    source 109
    target 750
  ]
  edge [
    source 109
    target 751
  ]
  edge [
    source 109
    target 752
  ]
  edge [
    source 109
    target 753
  ]
  edge [
    source 109
    target 754
  ]
  edge [
    source 109
    target 755
  ]
  edge [
    source 109
    target 756
  ]
  edge [
    source 109
    target 757
  ]
  edge [
    source 109
    target 758
  ]
  edge [
    source 110
    target 133
  ]
  edge [
    source 110
    target 145
  ]
  edge [
    source 110
    target 759
  ]
  edge [
    source 110
    target 580
  ]
  edge [
    source 110
    target 760
  ]
  edge [
    source 110
    target 761
  ]
  edge [
    source 110
    target 119
  ]
  edge [
    source 110
    target 183
  ]
  edge [
    source 110
    target 203
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 762
  ]
  edge [
    source 111
    target 763
  ]
  edge [
    source 111
    target 156
  ]
  edge [
    source 111
    target 764
  ]
  edge [
    source 111
    target 765
  ]
  edge [
    source 111
    target 687
  ]
  edge [
    source 111
    target 766
  ]
  edge [
    source 111
    target 186
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 767
  ]
  edge [
    source 113
    target 157
  ]
  edge [
    source 113
    target 768
  ]
  edge [
    source 113
    target 769
  ]
  edge [
    source 114
    target 770
  ]
  edge [
    source 114
    target 740
  ]
  edge [
    source 114
    target 771
  ]
  edge [
    source 114
    target 772
  ]
  edge [
    source 114
    target 773
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 115
    target 774
  ]
  edge [
    source 115
    target 775
  ]
  edge [
    source 115
    target 317
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 141
  ]
  edge [
    source 116
    target 776
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 777
  ]
  edge [
    source 118
    target 778
  ]
  edge [
    source 118
    target 551
  ]
  edge [
    source 119
    target 779
  ]
  edge [
    source 119
    target 780
  ]
  edge [
    source 119
    target 781
  ]
  edge [
    source 119
    target 782
  ]
  edge [
    source 119
    target 183
  ]
  edge [
    source 119
    target 203
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 535
  ]
  edge [
    source 120
    target 783
  ]
  edge [
    source 120
    target 784
  ]
  edge [
    source 120
    target 785
  ]
  edge [
    source 120
    target 786
  ]
  edge [
    source 120
    target 321
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 161
  ]
  edge [
    source 121
    target 162
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 787
  ]
  edge [
    source 123
    target 788
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 789
  ]
  edge [
    source 124
    target 790
  ]
  edge [
    source 124
    target 393
  ]
  edge [
    source 124
    target 791
  ]
  edge [
    source 124
    target 792
  ]
  edge [
    source 124
    target 793
  ]
  edge [
    source 124
    target 794
  ]
  edge [
    source 124
    target 795
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 796
  ]
  edge [
    source 125
    target 797
  ]
  edge [
    source 125
    target 798
  ]
  edge [
    source 125
    target 799
  ]
  edge [
    source 125
    target 800
  ]
  edge [
    source 125
    target 801
  ]
  edge [
    source 125
    target 802
  ]
  edge [
    source 125
    target 803
  ]
  edge [
    source 125
    target 804
  ]
  edge [
    source 125
    target 805
  ]
  edge [
    source 125
    target 806
  ]
  edge [
    source 125
    target 807
  ]
  edge [
    source 125
    target 808
  ]
  edge [
    source 125
    target 809
  ]
  edge [
    source 125
    target 810
  ]
  edge [
    source 125
    target 811
  ]
  edge [
    source 125
    target 703
  ]
  edge [
    source 125
    target 812
  ]
  edge [
    source 125
    target 813
  ]
  edge [
    source 125
    target 814
  ]
  edge [
    source 125
    target 291
  ]
  edge [
    source 125
    target 815
  ]
  edge [
    source 125
    target 816
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 817
  ]
  edge [
    source 126
    target 818
  ]
  edge [
    source 126
    target 819
  ]
  edge [
    source 126
    target 246
  ]
  edge [
    source 126
    target 820
  ]
  edge [
    source 126
    target 821
  ]
  edge [
    source 126
    target 822
  ]
  edge [
    source 126
    target 823
  ]
  edge [
    source 126
    target 824
  ]
  edge [
    source 126
    target 825
  ]
  edge [
    source 126
    target 134
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 393
  ]
  edge [
    source 127
    target 826
  ]
  edge [
    source 127
    target 827
  ]
  edge [
    source 127
    target 828
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 829
  ]
  edge [
    source 128
    target 830
  ]
  edge [
    source 128
    target 831
  ]
  edge [
    source 128
    target 832
  ]
  edge [
    source 128
    target 833
  ]
  edge [
    source 128
    target 834
  ]
  edge [
    source 128
    target 835
  ]
  edge [
    source 128
    target 133
  ]
  edge [
    source 128
    target 138
  ]
  edge [
    source 128
    target 189
  ]
  edge [
    source 128
    target 194
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 399
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 130
    target 836
  ]
  edge [
    source 130
    target 837
  ]
  edge [
    source 130
    target 838
  ]
  edge [
    source 130
    target 839
  ]
  edge [
    source 130
    target 840
  ]
  edge [
    source 130
    target 841
  ]
  edge [
    source 130
    target 842
  ]
  edge [
    source 130
    target 843
  ]
  edge [
    source 130
    target 844
  ]
  edge [
    source 130
    target 393
  ]
  edge [
    source 130
    target 845
  ]
  edge [
    source 130
    target 846
  ]
  edge [
    source 130
    target 847
  ]
  edge [
    source 130
    target 848
  ]
  edge [
    source 130
    target 849
  ]
  edge [
    source 130
    target 850
  ]
  edge [
    source 130
    target 851
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 852
  ]
  edge [
    source 131
    target 853
  ]
  edge [
    source 131
    target 854
  ]
  edge [
    source 131
    target 855
  ]
  edge [
    source 131
    target 140
  ]
  edge [
    source 131
    target 184
  ]
  edge [
    source 131
    target 204
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 132
    target 856
  ]
  edge [
    source 132
    target 857
  ]
  edge [
    source 132
    target 855
  ]
  edge [
    source 132
    target 466
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 133
    target 148
  ]
  edge [
    source 133
    target 184
  ]
  edge [
    source 133
    target 185
  ]
  edge [
    source 133
    target 200
  ]
  edge [
    source 133
    target 138
  ]
  edge [
    source 133
    target 189
  ]
  edge [
    source 133
    target 194
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 134
    target 483
  ]
  edge [
    source 135
    target 858
  ]
  edge [
    source 135
    target 763
  ]
  edge [
    source 135
    target 156
  ]
  edge [
    source 135
    target 859
  ]
  edge [
    source 135
    target 860
  ]
  edge [
    source 135
    target 861
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 137
    target 138
  ]
  edge [
    source 137
    target 862
  ]
  edge [
    source 137
    target 863
  ]
  edge [
    source 137
    target 864
  ]
  edge [
    source 137
    target 865
  ]
  edge [
    source 137
    target 866
  ]
  edge [
    source 137
    target 867
  ]
  edge [
    source 137
    target 157
  ]
  edge [
    source 137
    target 868
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 138
    target 189
  ]
  edge [
    source 138
    target 194
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 139
    target 145
  ]
  edge [
    source 139
    target 182
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 140
    target 204
  ]
  edge [
    source 141
    target 869
  ]
  edge [
    source 141
    target 870
  ]
  edge [
    source 141
    target 871
  ]
  edge [
    source 141
    target 872
  ]
  edge [
    source 141
    target 873
  ]
  edge [
    source 141
    target 874
  ]
  edge [
    source 141
    target 875
  ]
  edge [
    source 142
    target 143
  ]
  edge [
    source 142
    target 156
  ]
  edge [
    source 142
    target 876
  ]
  edge [
    source 142
    target 877
  ]
  edge [
    source 142
    target 878
  ]
  edge [
    source 142
    target 799
  ]
  edge [
    source 142
    target 879
  ]
  edge [
    source 142
    target 880
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 144
    target 145
  ]
  edge [
    source 145
    target 146
  ]
  edge [
    source 145
    target 149
  ]
  edge [
    source 145
    target 182
  ]
  edge [
    source 146
    target 194
  ]
  edge [
    source 146
    target 195
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 147
    target 881
  ]
  edge [
    source 147
    target 882
  ]
  edge [
    source 148
    target 883
  ]
  edge [
    source 148
    target 229
  ]
  edge [
    source 148
    target 884
  ]
  edge [
    source 148
    target 885
  ]
  edge [
    source 148
    target 886
  ]
  edge [
    source 148
    target 887
  ]
  edge [
    source 148
    target 888
  ]
  edge [
    source 148
    target 889
  ]
  edge [
    source 148
    target 890
  ]
  edge [
    source 148
    target 891
  ]
  edge [
    source 149
    target 150
  ]
  edge [
    source 149
    target 892
  ]
  edge [
    source 149
    target 893
  ]
  edge [
    source 149
    target 894
  ]
  edge [
    source 149
    target 895
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 151
    target 896
  ]
  edge [
    source 151
    target 897
  ]
  edge [
    source 151
    target 898
  ]
  edge [
    source 151
    target 671
  ]
  edge [
    source 151
    target 350
  ]
  edge [
    source 151
    target 899
  ]
  edge [
    source 151
    target 900
  ]
  edge [
    source 151
    target 901
  ]
  edge [
    source 151
    target 902
  ]
  edge [
    source 151
    target 384
  ]
  edge [
    source 151
    target 903
  ]
  edge [
    source 151
    target 904
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 152
    target 445
  ]
  edge [
    source 152
    target 905
  ]
  edge [
    source 152
    target 906
  ]
  edge [
    source 152
    target 907
  ]
  edge [
    source 152
    target 908
  ]
  edge [
    source 152
    target 909
  ]
  edge [
    source 152
    target 910
  ]
  edge [
    source 152
    target 911
  ]
  edge [
    source 152
    target 912
  ]
  edge [
    source 152
    target 913
  ]
  edge [
    source 153
    target 914
  ]
  edge [
    source 153
    target 915
  ]
  edge [
    source 153
    target 916
  ]
  edge [
    source 153
    target 917
  ]
  edge [
    source 153
    target 918
  ]
  edge [
    source 153
    target 919
  ]
  edge [
    source 153
    target 920
  ]
  edge [
    source 153
    target 921
  ]
  edge [
    source 153
    target 922
  ]
  edge [
    source 153
    target 923
  ]
  edge [
    source 153
    target 924
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 156
    target 925
  ]
  edge [
    source 156
    target 926
  ]
  edge [
    source 156
    target 927
  ]
  edge [
    source 156
    target 604
  ]
  edge [
    source 156
    target 928
  ]
  edge [
    source 156
    target 929
  ]
  edge [
    source 156
    target 930
  ]
  edge [
    source 156
    target 610
  ]
  edge [
    source 156
    target 931
  ]
  edge [
    source 156
    target 932
  ]
  edge [
    source 156
    target 619
  ]
  edge [
    source 156
    target 933
  ]
  edge [
    source 156
    target 934
  ]
  edge [
    source 156
    target 935
  ]
  edge [
    source 156
    target 936
  ]
  edge [
    source 156
    target 937
  ]
  edge [
    source 156
    target 633
  ]
  edge [
    source 156
    target 938
  ]
  edge [
    source 156
    target 638
  ]
  edge [
    source 156
    target 939
  ]
  edge [
    source 156
    target 940
  ]
  edge [
    source 156
    target 640
  ]
  edge [
    source 156
    target 941
  ]
  edge [
    source 156
    target 942
  ]
  edge [
    source 156
    target 943
  ]
  edge [
    source 156
    target 944
  ]
  edge [
    source 156
    target 945
  ]
  edge [
    source 156
    target 946
  ]
  edge [
    source 156
    target 947
  ]
  edge [
    source 156
    target 948
  ]
  edge [
    source 156
    target 949
  ]
  edge [
    source 156
    target 656
  ]
  edge [
    source 156
    target 950
  ]
  edge [
    source 156
    target 951
  ]
  edge [
    source 156
    target 952
  ]
  edge [
    source 156
    target 953
  ]
  edge [
    source 157
    target 954
  ]
  edge [
    source 157
    target 955
  ]
  edge [
    source 157
    target 956
  ]
  edge [
    source 157
    target 957
  ]
  edge [
    source 157
    target 958
  ]
  edge [
    source 157
    target 959
  ]
  edge [
    source 157
    target 960
  ]
  edge [
    source 157
    target 961
  ]
  edge [
    source 157
    target 962
  ]
  edge [
    source 157
    target 963
  ]
  edge [
    source 157
    target 964
  ]
  edge [
    source 157
    target 965
  ]
  edge [
    source 157
    target 966
  ]
  edge [
    source 157
    target 967
  ]
  edge [
    source 158
    target 201
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 160
    target 968
  ]
  edge [
    source 160
    target 969
  ]
  edge [
    source 160
    target 970
  ]
  edge [
    source 160
    target 839
  ]
  edge [
    source 160
    target 971
  ]
  edge [
    source 160
    target 972
  ]
  edge [
    source 160
    target 441
  ]
  edge [
    source 160
    target 973
  ]
  edge [
    source 160
    target 974
  ]
  edge [
    source 160
    target 975
  ]
  edge [
    source 160
    target 976
  ]
  edge [
    source 161
    target 977
  ]
  edge [
    source 161
    target 978
  ]
  edge [
    source 161
    target 979
  ]
  edge [
    source 161
    target 980
  ]
  edge [
    source 161
    target 981
  ]
  edge [
    source 162
    target 982
  ]
  edge [
    source 162
    target 206
  ]
  edge [
    source 162
    target 198
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 163
    target 983
  ]
  edge [
    source 163
    target 984
  ]
  edge [
    source 163
    target 985
  ]
  edge [
    source 163
    target 986
  ]
  edge [
    source 163
    target 987
  ]
  edge [
    source 163
    target 188
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 164
    target 399
  ]
  edge [
    source 164
    target 535
  ]
  edge [
    source 164
    target 988
  ]
  edge [
    source 165
    target 989
  ]
  edge [
    source 165
    target 990
  ]
  edge [
    source 165
    target 991
  ]
  edge [
    source 165
    target 526
  ]
  edge [
    source 165
    target 393
  ]
  edge [
    source 165
    target 301
  ]
  edge [
    source 165
    target 992
  ]
  edge [
    source 165
    target 520
  ]
  edge [
    source 166
    target 167
  ]
  edge [
    source 166
    target 993
  ]
  edge [
    source 166
    target 994
  ]
  edge [
    source 166
    target 995
  ]
  edge [
    source 166
    target 996
  ]
  edge [
    source 166
    target 997
  ]
  edge [
    source 166
    target 998
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 168
    target 999
  ]
  edge [
    source 168
    target 229
  ]
  edge [
    source 168
    target 1000
  ]
  edge [
    source 168
    target 1001
  ]
  edge [
    source 168
    target 1002
  ]
  edge [
    source 168
    target 1003
  ]
  edge [
    source 168
    target 808
  ]
  edge [
    source 168
    target 1004
  ]
  edge [
    source 168
    target 1005
  ]
  edge [
    source 168
    target 1006
  ]
  edge [
    source 168
    target 1007
  ]
  edge [
    source 168
    target 1008
  ]
  edge [
    source 168
    target 1009
  ]
  edge [
    source 168
    target 1010
  ]
  edge [
    source 168
    target 1011
  ]
  edge [
    source 168
    target 1012
  ]
  edge [
    source 168
    target 1013
  ]
  edge [
    source 168
    target 1014
  ]
  edge [
    source 169
    target 1015
  ]
  edge [
    source 169
    target 1016
  ]
  edge [
    source 169
    target 1017
  ]
  edge [
    source 169
    target 1018
  ]
  edge [
    source 169
    target 211
  ]
  edge [
    source 169
    target 1019
  ]
  edge [
    source 169
    target 1020
  ]
  edge [
    source 169
    target 1021
  ]
  edge [
    source 169
    target 1022
  ]
  edge [
    source 169
    target 1023
  ]
  edge [
    source 169
    target 1024
  ]
  edge [
    source 169
    target 1025
  ]
  edge [
    source 169
    target 1026
  ]
  edge [
    source 169
    target 1027
  ]
  edge [
    source 169
    target 1028
  ]
  edge [
    source 169
    target 1029
  ]
  edge [
    source 169
    target 1030
  ]
  edge [
    source 169
    target 1031
  ]
  edge [
    source 169
    target 1032
  ]
  edge [
    source 170
    target 171
  ]
  edge [
    source 170
    target 1033
  ]
  edge [
    source 170
    target 1034
  ]
  edge [
    source 171
    target 172
  ]
  edge [
    source 171
    target 1035
  ]
  edge [
    source 171
    target 1036
  ]
  edge [
    source 171
    target 1037
  ]
  edge [
    source 171
    target 1038
  ]
  edge [
    source 171
    target 1039
  ]
  edge [
    source 171
    target 1040
  ]
  edge [
    source 171
    target 1041
  ]
  edge [
    source 171
    target 1042
  ]
  edge [
    source 171
    target 1043
  ]
  edge [
    source 171
    target 799
  ]
  edge [
    source 171
    target 1044
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 172
    target 1045
  ]
  edge [
    source 172
    target 1046
  ]
  edge [
    source 172
    target 1047
  ]
  edge [
    source 172
    target 240
  ]
  edge [
    source 172
    target 1048
  ]
  edge [
    source 172
    target 1049
  ]
  edge [
    source 172
    target 1050
  ]
  edge [
    source 172
    target 1051
  ]
  edge [
    source 172
    target 1052
  ]
  edge [
    source 172
    target 1053
  ]
  edge [
    source 172
    target 393
  ]
  edge [
    source 172
    target 1054
  ]
  edge [
    source 172
    target 406
  ]
  edge [
    source 172
    target 1055
  ]
  edge [
    source 172
    target 1056
  ]
  edge [
    source 172
    target 1057
  ]
  edge [
    source 172
    target 701
  ]
  edge [
    source 172
    target 1058
  ]
  edge [
    source 173
    target 174
  ]
  edge [
    source 173
    target 1059
  ]
  edge [
    source 173
    target 1060
  ]
  edge [
    source 174
    target 175
  ]
  edge [
    source 175
    target 176
  ]
  edge [
    source 175
    target 1061
  ]
  edge [
    source 175
    target 1062
  ]
  edge [
    source 175
    target 1063
  ]
  edge [
    source 175
    target 1064
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 176
    target 1065
  ]
  edge [
    source 176
    target 1066
  ]
  edge [
    source 177
    target 178
  ]
  edge [
    source 177
    target 1067
  ]
  edge [
    source 177
    target 1068
  ]
  edge [
    source 177
    target 774
  ]
  edge [
    source 177
    target 775
  ]
  edge [
    source 177
    target 317
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 178
    target 197
  ]
  edge [
    source 179
    target 180
  ]
  edge [
    source 179
    target 1069
  ]
  edge [
    source 179
    target 1070
  ]
  edge [
    source 179
    target 1071
  ]
  edge [
    source 179
    target 1060
  ]
  edge [
    source 179
    target 1072
  ]
  edge [
    source 179
    target 249
  ]
  edge [
    source 179
    target 1073
  ]
  edge [
    source 179
    target 1074
  ]
  edge [
    source 179
    target 1075
  ]
  edge [
    source 179
    target 1059
  ]
  edge [
    source 179
    target 1076
  ]
  edge [
    source 179
    target 1077
  ]
  edge [
    source 179
    target 1078
  ]
  edge [
    source 179
    target 1079
  ]
  edge [
    source 179
    target 1080
  ]
  edge [
    source 179
    target 508
  ]
  edge [
    source 179
    target 1081
  ]
  edge [
    source 180
    target 181
  ]
  edge [
    source 180
    target 1082
  ]
  edge [
    source 180
    target 1083
  ]
  edge [
    source 180
    target 1084
  ]
  edge [
    source 180
    target 688
  ]
  edge [
    source 180
    target 1085
  ]
  edge [
    source 180
    target 1086
  ]
  edge [
    source 180
    target 1087
  ]
  edge [
    source 180
    target 1088
  ]
  edge [
    source 180
    target 406
  ]
  edge [
    source 180
    target 598
  ]
  edge [
    source 180
    target 1089
  ]
  edge [
    source 180
    target 1090
  ]
  edge [
    source 180
    target 1091
  ]
  edge [
    source 181
    target 182
  ]
  edge [
    source 181
    target 1092
  ]
  edge [
    source 181
    target 1093
  ]
  edge [
    source 181
    target 1094
  ]
  edge [
    source 181
    target 1095
  ]
  edge [
    source 182
    target 183
  ]
  edge [
    source 182
    target 1096
  ]
  edge [
    source 182
    target 1097
  ]
  edge [
    source 182
    target 1098
  ]
  edge [
    source 182
    target 1099
  ]
  edge [
    source 182
    target 1100
  ]
  edge [
    source 182
    target 1101
  ]
  edge [
    source 182
    target 1102
  ]
  edge [
    source 182
    target 1103
  ]
  edge [
    source 182
    target 1104
  ]
  edge [
    source 182
    target 1105
  ]
  edge [
    source 182
    target 1106
  ]
  edge [
    source 182
    target 1107
  ]
  edge [
    source 182
    target 1108
  ]
  edge [
    source 182
    target 1109
  ]
  edge [
    source 182
    target 1110
  ]
  edge [
    source 183
    target 184
  ]
  edge [
    source 183
    target 1111
  ]
  edge [
    source 183
    target 1112
  ]
  edge [
    source 183
    target 1113
  ]
  edge [
    source 183
    target 203
  ]
  edge [
    source 184
    target 202
  ]
  edge [
    source 184
    target 1114
  ]
  edge [
    source 184
    target 1115
  ]
  edge [
    source 184
    target 281
  ]
  edge [
    source 184
    target 1116
  ]
  edge [
    source 184
    target 1117
  ]
  edge [
    source 184
    target 1118
  ]
  edge [
    source 184
    target 204
  ]
  edge [
    source 185
    target 533
  ]
  edge [
    source 185
    target 1119
  ]
  edge [
    source 185
    target 1120
  ]
  edge [
    source 186
    target 187
  ]
  edge [
    source 187
    target 188
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 188
    target 1121
  ]
  edge [
    source 188
    target 1028
  ]
  edge [
    source 188
    target 1122
  ]
  edge [
    source 188
    target 211
  ]
  edge [
    source 188
    target 1123
  ]
  edge [
    source 188
    target 1124
  ]
  edge [
    source 189
    target 190
  ]
  edge [
    source 189
    target 194
  ]
  edge [
    source 190
    target 191
  ]
  edge [
    source 190
    target 1125
  ]
  edge [
    source 190
    target 1126
  ]
  edge [
    source 190
    target 249
  ]
  edge [
    source 190
    target 1127
  ]
  edge [
    source 190
    target 1128
  ]
  edge [
    source 190
    target 1129
  ]
  edge [
    source 190
    target 270
  ]
  edge [
    source 190
    target 291
  ]
  edge [
    source 190
    target 1130
  ]
  edge [
    source 190
    target 795
  ]
  edge [
    source 190
    target 1131
  ]
  edge [
    source 190
    target 1132
  ]
  edge [
    source 191
    target 1037
  ]
  edge [
    source 191
    target 1133
  ]
  edge [
    source 191
    target 1134
  ]
  edge [
    source 191
    target 1135
  ]
  edge [
    source 191
    target 1136
  ]
  edge [
    source 191
    target 1137
  ]
  edge [
    source 191
    target 1138
  ]
  edge [
    source 191
    target 1139
  ]
  edge [
    source 191
    target 1140
  ]
  edge [
    source 191
    target 1141
  ]
  edge [
    source 191
    target 1142
  ]
  edge [
    source 191
    target 1143
  ]
  edge [
    source 191
    target 1144
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 192
    target 199
  ]
  edge [
    source 193
    target 194
  ]
  edge [
    source 193
    target 1145
  ]
  edge [
    source 193
    target 927
  ]
  edge [
    source 193
    target 1146
  ]
  edge [
    source 193
    target 215
  ]
  edge [
    source 193
    target 249
  ]
  edge [
    source 193
    target 495
  ]
  edge [
    source 193
    target 1147
  ]
  edge [
    source 193
    target 1148
  ]
  edge [
    source 193
    target 1149
  ]
  edge [
    source 193
    target 1140
  ]
  edge [
    source 193
    target 932
  ]
  edge [
    source 194
    target 489
  ]
  edge [
    source 195
    target 196
  ]
  edge [
    source 195
    target 1150
  ]
  edge [
    source 195
    target 1151
  ]
  edge [
    source 195
    target 1152
  ]
  edge [
    source 195
    target 685
  ]
  edge [
    source 195
    target 1153
  ]
  edge [
    source 195
    target 684
  ]
  edge [
    source 195
    target 1154
  ]
  edge [
    source 195
    target 1155
  ]
  edge [
    source 195
    target 1156
  ]
  edge [
    source 195
    target 1157
  ]
  edge [
    source 195
    target 1158
  ]
  edge [
    source 195
    target 1159
  ]
  edge [
    source 195
    target 1160
  ]
  edge [
    source 196
    target 197
  ]
  edge [
    source 196
    target 1161
  ]
  edge [
    source 196
    target 1162
  ]
  edge [
    source 196
    target 1163
  ]
  edge [
    source 196
    target 1164
  ]
  edge [
    source 196
    target 1165
  ]
  edge [
    source 196
    target 1166
  ]
  edge [
    source 196
    target 249
  ]
  edge [
    source 196
    target 1167
  ]
  edge [
    source 196
    target 1168
  ]
  edge [
    source 196
    target 1148
  ]
  edge [
    source 196
    target 1140
  ]
  edge [
    source 196
    target 1169
  ]
  edge [
    source 196
    target 1170
  ]
  edge [
    source 196
    target 1171
  ]
  edge [
    source 197
    target 198
  ]
  edge [
    source 197
    target 1172
  ]
  edge [
    source 197
    target 1173
  ]
  edge [
    source 197
    target 1174
  ]
  edge [
    source 198
    target 199
  ]
  edge [
    source 198
    target 1175
  ]
  edge [
    source 198
    target 1176
  ]
  edge [
    source 198
    target 1177
  ]
  edge [
    source 198
    target 1178
  ]
  edge [
    source 198
    target 1179
  ]
  edge [
    source 199
    target 200
  ]
  edge [
    source 199
    target 354
  ]
  edge [
    source 199
    target 1180
  ]
  edge [
    source 199
    target 1181
  ]
  edge [
    source 199
    target 1182
  ]
  edge [
    source 199
    target 1050
  ]
  edge [
    source 199
    target 828
  ]
  edge [
    source 199
    target 1183
  ]
  edge [
    source 199
    target 516
  ]
  edge [
    source 199
    target 701
  ]
  edge [
    source 200
    target 1184
  ]
  edge [
    source 200
    target 1185
  ]
  edge [
    source 200
    target 910
  ]
  edge [
    source 200
    target 1037
  ]
  edge [
    source 201
    target 1186
  ]
  edge [
    source 201
    target 1187
  ]
  edge [
    source 201
    target 1188
  ]
  edge [
    source 201
    target 1189
  ]
  edge [
    source 201
    target 1190
  ]
  edge [
    source 201
    target 1191
  ]
  edge [
    source 201
    target 1192
  ]
  edge [
    source 201
    target 403
  ]
  edge [
    source 201
    target 1193
  ]
  edge [
    source 201
    target 1194
  ]
  edge [
    source 201
    target 781
  ]
  edge [
    source 201
    target 1195
  ]
  edge [
    source 201
    target 1196
  ]
  edge [
    source 201
    target 1197
  ]
  edge [
    source 201
    target 780
  ]
  edge [
    source 201
    target 1198
  ]
  edge [
    source 201
    target 1199
  ]
  edge [
    source 201
    target 782
  ]
  edge [
    source 201
    target 1200
  ]
  edge [
    source 201
    target 1201
  ]
  edge [
    source 201
    target 1202
  ]
  edge [
    source 201
    target 1203
  ]
  edge [
    source 201
    target 1204
  ]
  edge [
    source 203
    target 204
  ]
  edge [
    source 203
    target 1205
  ]
  edge [
    source 203
    target 1206
  ]
  edge [
    source 203
    target 437
  ]
  edge [
    source 203
    target 1207
  ]
  edge [
    source 203
    target 1208
  ]
  edge [
    source 203
    target 1209
  ]
  edge [
    source 203
    target 1210
  ]
  edge [
    source 203
    target 1211
  ]
  edge [
    source 204
    target 1212
  ]
]
