graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "twierdza"
    origin "text"
  ]
  node [
    id 1
    label "heheszki"
    origin "text"
  ]
  node [
    id 2
    label "Szlisselburg"
  ]
  node [
    id 3
    label "Dyjament"
  ]
  node [
    id 4
    label "flanka"
  ]
  node [
    id 5
    label "schronienie"
  ]
  node [
    id 6
    label "budowla"
  ]
  node [
    id 7
    label "Brenna"
  ]
  node [
    id 8
    label "bastion"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
]
