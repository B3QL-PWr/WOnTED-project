graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8571428571428572
  density 0.14285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 1
    label "lodz"
    origin "text"
  ]
  node [
    id 2
    label "czujedobrzeczlowiek"
    origin "text"
  ]
  node [
    id 3
    label "stanie"
  ]
  node [
    id 4
    label "przebywanie"
  ]
  node [
    id 5
    label "panowanie"
  ]
  node [
    id 6
    label "zajmowanie"
  ]
  node [
    id 7
    label "pomieszkanie"
  ]
  node [
    id 8
    label "adjustment"
  ]
  node [
    id 9
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 10
    label "lokal"
  ]
  node [
    id 11
    label "kwadrat"
  ]
  node [
    id 12
    label "animation"
  ]
  node [
    id 13
    label "dom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
]
