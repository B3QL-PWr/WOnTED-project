graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.95
  density 0.024683544303797468
  graphCliqueNumber 3
  node [
    id 0
    label "pedofil"
    origin "text"
  ]
  node [
    id 1
    label "polski"
    origin "text"
  ]
  node [
    id 2
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 3
    label "katolicki"
    origin "text"
  ]
  node [
    id 4
    label "wydarzenie"
    origin "text"
  ]
  node [
    id 5
    label "kilka"
    origin "text"
  ]
  node [
    id 6
    label "ostatni"
    origin "text"
  ]
  node [
    id 7
    label "lato"
    origin "text"
  ]
  node [
    id 8
    label "organizm"
  ]
  node [
    id 9
    label "dewiant"
  ]
  node [
    id 10
    label "dziecko"
  ]
  node [
    id 11
    label "lacki"
  ]
  node [
    id 12
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 13
    label "przedmiot"
  ]
  node [
    id 14
    label "sztajer"
  ]
  node [
    id 15
    label "drabant"
  ]
  node [
    id 16
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 17
    label "polak"
  ]
  node [
    id 18
    label "pierogi_ruskie"
  ]
  node [
    id 19
    label "krakowiak"
  ]
  node [
    id 20
    label "Polish"
  ]
  node [
    id 21
    label "j&#281;zyk"
  ]
  node [
    id 22
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 23
    label "oberek"
  ]
  node [
    id 24
    label "po_polsku"
  ]
  node [
    id 25
    label "mazur"
  ]
  node [
    id 26
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 27
    label "chodzony"
  ]
  node [
    id 28
    label "skoczny"
  ]
  node [
    id 29
    label "ryba_po_grecku"
  ]
  node [
    id 30
    label "goniony"
  ]
  node [
    id 31
    label "polsko"
  ]
  node [
    id 32
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 33
    label "zakrystia"
  ]
  node [
    id 34
    label "organizacja_religijna"
  ]
  node [
    id 35
    label "nawa"
  ]
  node [
    id 36
    label "nerwica_eklezjogenna"
  ]
  node [
    id 37
    label "prezbiterium"
  ]
  node [
    id 38
    label "kropielnica"
  ]
  node [
    id 39
    label "wsp&#243;lnota"
  ]
  node [
    id 40
    label "church"
  ]
  node [
    id 41
    label "kruchta"
  ]
  node [
    id 42
    label "Ska&#322;ka"
  ]
  node [
    id 43
    label "kult"
  ]
  node [
    id 44
    label "ub&#322;agalnia"
  ]
  node [
    id 45
    label "dom"
  ]
  node [
    id 46
    label "po_katolicku"
  ]
  node [
    id 47
    label "zgodny"
  ]
  node [
    id 48
    label "chrze&#347;cija&#324;ski"
  ]
  node [
    id 49
    label "wyznaniowy"
  ]
  node [
    id 50
    label "powszechny"
  ]
  node [
    id 51
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 52
    label "typowy"
  ]
  node [
    id 53
    label "czynno&#347;&#263;"
  ]
  node [
    id 54
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 55
    label "motyw"
  ]
  node [
    id 56
    label "fabu&#322;a"
  ]
  node [
    id 57
    label "przebiec"
  ]
  node [
    id 58
    label "przebiegni&#281;cie"
  ]
  node [
    id 59
    label "charakter"
  ]
  node [
    id 60
    label "&#347;ledziowate"
  ]
  node [
    id 61
    label "ryba"
  ]
  node [
    id 62
    label "cz&#322;owiek"
  ]
  node [
    id 63
    label "kolejny"
  ]
  node [
    id 64
    label "istota_&#380;ywa"
  ]
  node [
    id 65
    label "najgorszy"
  ]
  node [
    id 66
    label "aktualny"
  ]
  node [
    id 67
    label "ostatnio"
  ]
  node [
    id 68
    label "niedawno"
  ]
  node [
    id 69
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 70
    label "sko&#324;czony"
  ]
  node [
    id 71
    label "poprzedni"
  ]
  node [
    id 72
    label "pozosta&#322;y"
  ]
  node [
    id 73
    label "w&#261;tpliwy"
  ]
  node [
    id 74
    label "pora_roku"
  ]
  node [
    id 75
    label "Henryk"
  ]
  node [
    id 76
    label "Jankowski"
  ]
  node [
    id 77
    label "&#347;wi&#281;ty"
  ]
  node [
    id 78
    label "Brygida"
  ]
  node [
    id 79
    label "parafia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 79
  ]
  edge [
    source 78
    target 79
  ]
]
