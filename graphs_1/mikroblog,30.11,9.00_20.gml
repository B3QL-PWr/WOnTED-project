graph [
  maxDegree 41
  minDegree 1
  meanDegree 2
  density 0.010638297872340425
  graphCliqueNumber 2
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "boldupy"
    origin "text"
  ]
  node [
    id 2
    label "rozumie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "gdy"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 5
    label "potrzebowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 7
    label "leczenie"
    origin "text"
  ]
  node [
    id 8
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 11
    label "rodzaj"
    origin "text"
  ]
  node [
    id 12
    label "zbi&#243;rka"
    origin "text"
  ]
  node [
    id 13
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 14
    label "kurwa"
    origin "text"
  ]
  node [
    id 15
    label "umiar"
    origin "text"
  ]
  node [
    id 16
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 17
    label "matczysko"
  ]
  node [
    id 18
    label "macierz"
  ]
  node [
    id 19
    label "przodkini"
  ]
  node [
    id 20
    label "Matka_Boska"
  ]
  node [
    id 21
    label "macocha"
  ]
  node [
    id 22
    label "matka_zast&#281;pcza"
  ]
  node [
    id 23
    label "stara"
  ]
  node [
    id 24
    label "rodzice"
  ]
  node [
    id 25
    label "rodzic"
  ]
  node [
    id 26
    label "wiedzie&#263;"
  ]
  node [
    id 27
    label "zna&#263;"
  ]
  node [
    id 28
    label "czu&#263;"
  ]
  node [
    id 29
    label "j&#281;zyk"
  ]
  node [
    id 30
    label "kuma&#263;"
  ]
  node [
    id 31
    label "give"
  ]
  node [
    id 32
    label "odbiera&#263;"
  ]
  node [
    id 33
    label "empatia"
  ]
  node [
    id 34
    label "see"
  ]
  node [
    id 35
    label "match"
  ]
  node [
    id 36
    label "dziama&#263;"
  ]
  node [
    id 37
    label "asymilowa&#263;"
  ]
  node [
    id 38
    label "wapniak"
  ]
  node [
    id 39
    label "dwun&#243;g"
  ]
  node [
    id 40
    label "polifag"
  ]
  node [
    id 41
    label "wz&#243;r"
  ]
  node [
    id 42
    label "profanum"
  ]
  node [
    id 43
    label "hominid"
  ]
  node [
    id 44
    label "homo_sapiens"
  ]
  node [
    id 45
    label "nasada"
  ]
  node [
    id 46
    label "podw&#322;adny"
  ]
  node [
    id 47
    label "ludzko&#347;&#263;"
  ]
  node [
    id 48
    label "os&#322;abianie"
  ]
  node [
    id 49
    label "mikrokosmos"
  ]
  node [
    id 50
    label "portrecista"
  ]
  node [
    id 51
    label "duch"
  ]
  node [
    id 52
    label "g&#322;owa"
  ]
  node [
    id 53
    label "oddzia&#322;ywanie"
  ]
  node [
    id 54
    label "asymilowanie"
  ]
  node [
    id 55
    label "osoba"
  ]
  node [
    id 56
    label "os&#322;abia&#263;"
  ]
  node [
    id 57
    label "figura"
  ]
  node [
    id 58
    label "Adam"
  ]
  node [
    id 59
    label "senior"
  ]
  node [
    id 60
    label "antropochoria"
  ]
  node [
    id 61
    label "posta&#263;"
  ]
  node [
    id 62
    label "zapotrzebowa&#263;"
  ]
  node [
    id 63
    label "by&#263;"
  ]
  node [
    id 64
    label "chcie&#263;"
  ]
  node [
    id 65
    label "need"
  ]
  node [
    id 66
    label "claim"
  ]
  node [
    id 67
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 68
    label "zdewaluowa&#263;"
  ]
  node [
    id 69
    label "moniak"
  ]
  node [
    id 70
    label "wytw&#243;r"
  ]
  node [
    id 71
    label "zdewaluowanie"
  ]
  node [
    id 72
    label "jednostka_monetarna"
  ]
  node [
    id 73
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 74
    label "numizmat"
  ]
  node [
    id 75
    label "rozmieni&#263;"
  ]
  node [
    id 76
    label "rozmienienie"
  ]
  node [
    id 77
    label "rozmienianie"
  ]
  node [
    id 78
    label "dewaluowanie"
  ]
  node [
    id 79
    label "nomina&#322;"
  ]
  node [
    id 80
    label "coin"
  ]
  node [
    id 81
    label "dewaluowa&#263;"
  ]
  node [
    id 82
    label "pieni&#261;dze"
  ]
  node [
    id 83
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 84
    label "rozmienia&#263;"
  ]
  node [
    id 85
    label "opatrywanie"
  ]
  node [
    id 86
    label "homeopata"
  ]
  node [
    id 87
    label "zabieg"
  ]
  node [
    id 88
    label "sanatoryjny"
  ]
  node [
    id 89
    label "&#322;agodzenie"
  ]
  node [
    id 90
    label "odwykowy"
  ]
  node [
    id 91
    label "alopata"
  ]
  node [
    id 92
    label "nauka_medyczna"
  ]
  node [
    id 93
    label "pomaganie"
  ]
  node [
    id 94
    label "plombowanie"
  ]
  node [
    id 95
    label "uzdrawianie"
  ]
  node [
    id 96
    label "wizyta"
  ]
  node [
    id 97
    label "zdiagnozowanie"
  ]
  node [
    id 98
    label "wizytowanie"
  ]
  node [
    id 99
    label "opatrzenie"
  ]
  node [
    id 100
    label "medication"
  ]
  node [
    id 101
    label "opieka_medyczna"
  ]
  node [
    id 102
    label "planowa&#263;"
  ]
  node [
    id 103
    label "dostosowywa&#263;"
  ]
  node [
    id 104
    label "pozyskiwa&#263;"
  ]
  node [
    id 105
    label "wprowadza&#263;"
  ]
  node [
    id 106
    label "treat"
  ]
  node [
    id 107
    label "przygotowywa&#263;"
  ]
  node [
    id 108
    label "create"
  ]
  node [
    id 109
    label "ensnare"
  ]
  node [
    id 110
    label "tworzy&#263;"
  ]
  node [
    id 111
    label "standard"
  ]
  node [
    id 112
    label "skupia&#263;"
  ]
  node [
    id 113
    label "r&#243;&#380;nie"
  ]
  node [
    id 114
    label "inny"
  ]
  node [
    id 115
    label "jaki&#347;"
  ]
  node [
    id 116
    label "kategoria_gramatyczna"
  ]
  node [
    id 117
    label "autorament"
  ]
  node [
    id 118
    label "jednostka_systematyczna"
  ]
  node [
    id 119
    label "fashion"
  ]
  node [
    id 120
    label "rodzina"
  ]
  node [
    id 121
    label "variety"
  ]
  node [
    id 122
    label "kwestowanie"
  ]
  node [
    id 123
    label "apel"
  ]
  node [
    id 124
    label "czynno&#347;&#263;"
  ]
  node [
    id 125
    label "koszyk&#243;wka"
  ]
  node [
    id 126
    label "recoil"
  ]
  node [
    id 127
    label "spotkanie"
  ]
  node [
    id 128
    label "kwestarz"
  ]
  node [
    id 129
    label "collection"
  ]
  node [
    id 130
    label "chwyt"
  ]
  node [
    id 131
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 132
    label "hide"
  ]
  node [
    id 133
    label "support"
  ]
  node [
    id 134
    label "szmaciarz"
  ]
  node [
    id 135
    label "zo&#322;za"
  ]
  node [
    id 136
    label "skurwienie_si&#281;"
  ]
  node [
    id 137
    label "kurwienie_si&#281;"
  ]
  node [
    id 138
    label "karze&#322;"
  ]
  node [
    id 139
    label "kurcz&#281;"
  ]
  node [
    id 140
    label "wyzwisko"
  ]
  node [
    id 141
    label "przekle&#324;stwo"
  ]
  node [
    id 142
    label "prostytutka"
  ]
  node [
    id 143
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 144
    label "continence"
  ]
  node [
    id 145
    label "oszcz&#281;dno&#347;&#263;"
  ]
  node [
    id 146
    label "rozs&#261;dek"
  ]
  node [
    id 147
    label "energy"
  ]
  node [
    id 148
    label "czas"
  ]
  node [
    id 149
    label "bycie"
  ]
  node [
    id 150
    label "zegar_biologiczny"
  ]
  node [
    id 151
    label "okres_noworodkowy"
  ]
  node [
    id 152
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 153
    label "entity"
  ]
  node [
    id 154
    label "prze&#380;ywanie"
  ]
  node [
    id 155
    label "prze&#380;ycie"
  ]
  node [
    id 156
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 157
    label "wiek_matuzalemowy"
  ]
  node [
    id 158
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 159
    label "dzieci&#324;stwo"
  ]
  node [
    id 160
    label "power"
  ]
  node [
    id 161
    label "szwung"
  ]
  node [
    id 162
    label "menopauza"
  ]
  node [
    id 163
    label "umarcie"
  ]
  node [
    id 164
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 165
    label "life"
  ]
  node [
    id 166
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 167
    label "&#380;ywy"
  ]
  node [
    id 168
    label "rozw&#243;j"
  ]
  node [
    id 169
    label "po&#322;&#243;g"
  ]
  node [
    id 170
    label "byt"
  ]
  node [
    id 171
    label "przebywanie"
  ]
  node [
    id 172
    label "subsistence"
  ]
  node [
    id 173
    label "koleje_losu"
  ]
  node [
    id 174
    label "raj_utracony"
  ]
  node [
    id 175
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 176
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 177
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 178
    label "andropauza"
  ]
  node [
    id 179
    label "warunki"
  ]
  node [
    id 180
    label "do&#380;ywanie"
  ]
  node [
    id 181
    label "niemowl&#281;ctwo"
  ]
  node [
    id 182
    label "umieranie"
  ]
  node [
    id 183
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 184
    label "staro&#347;&#263;"
  ]
  node [
    id 185
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 186
    label "&#347;mier&#263;"
  ]
  node [
    id 187
    label "Anna"
  ]
  node [
    id 188
    label "Dymna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 28
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 187
    target 188
  ]
]
