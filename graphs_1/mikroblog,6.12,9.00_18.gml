graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 1
    label "nied&#322;ugo"
    origin "text"
  ]
  node [
    id 2
    label "wpr&#281;dce"
  ]
  node [
    id 3
    label "blisko"
  ]
  node [
    id 4
    label "nied&#322;ugi"
  ]
  node [
    id 5
    label "kr&#243;tki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
]
