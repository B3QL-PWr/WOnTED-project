graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9047619047619047
  density 0.04645760743321719
  graphCliqueNumber 3
  node [
    id 0
    label "budowa"
    origin "text"
  ]
  node [
    id 1
    label "model"
    origin "text"
  ]
  node [
    id 2
    label "axial"
    origin "text"
  ]
  node [
    id 3
    label "scorpion"
    origin "text"
  ]
  node [
    id 4
    label "figura"
  ]
  node [
    id 5
    label "wjazd"
  ]
  node [
    id 6
    label "struktura"
  ]
  node [
    id 7
    label "konstrukcja"
  ]
  node [
    id 8
    label "r&#243;w"
  ]
  node [
    id 9
    label "kreacja"
  ]
  node [
    id 10
    label "posesja"
  ]
  node [
    id 11
    label "cecha"
  ]
  node [
    id 12
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 13
    label "organ"
  ]
  node [
    id 14
    label "mechanika"
  ]
  node [
    id 15
    label "zwierz&#281;"
  ]
  node [
    id 16
    label "miejsce_pracy"
  ]
  node [
    id 17
    label "praca"
  ]
  node [
    id 18
    label "constitution"
  ]
  node [
    id 19
    label "typ"
  ]
  node [
    id 20
    label "cz&#322;owiek"
  ]
  node [
    id 21
    label "pozowa&#263;"
  ]
  node [
    id 22
    label "ideal"
  ]
  node [
    id 23
    label "matryca"
  ]
  node [
    id 24
    label "imitacja"
  ]
  node [
    id 25
    label "ruch"
  ]
  node [
    id 26
    label "motif"
  ]
  node [
    id 27
    label "pozowanie"
  ]
  node [
    id 28
    label "wz&#243;r"
  ]
  node [
    id 29
    label "miniatura"
  ]
  node [
    id 30
    label "prezenter"
  ]
  node [
    id 31
    label "facet"
  ]
  node [
    id 32
    label "orygina&#322;"
  ]
  node [
    id 33
    label "mildew"
  ]
  node [
    id 34
    label "spos&#243;b"
  ]
  node [
    id 35
    label "zi&#243;&#322;ko"
  ]
  node [
    id 36
    label "adaptation"
  ]
  node [
    id 37
    label "Axial"
  ]
  node [
    id 38
    label "AX10"
  ]
  node [
    id 39
    label "Scorpion"
  ]
  node [
    id 40
    label "AX80004"
  ]
  node [
    id 41
    label "1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
]
