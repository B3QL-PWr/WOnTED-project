graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.079646017699115
  density 0.009242871189773844
  graphCliqueNumber 3
  node [
    id 0
    label "oznacza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "liczba"
    origin "text"
  ]
  node [
    id 2
    label "student"
    origin "text"
  ]
  node [
    id 3
    label "studia"
    origin "text"
  ]
  node [
    id 4
    label "stacjonarny"
    origin "text"
  ]
  node [
    id 5
    label "wy&#322;&#261;czenie"
    origin "text"
  ]
  node [
    id 6
    label "osoba"
    origin "text"
  ]
  node [
    id 7
    label "ostatni"
    origin "text"
  ]
  node [
    id 8
    label "rok"
    origin "text"
  ]
  node [
    id 9
    label "bez"
    origin "text"
  ]
  node [
    id 10
    label "egzamin"
    origin "text"
  ]
  node [
    id 11
    label "dyplomowy"
    origin "text"
  ]
  node [
    id 12
    label "ten"
    origin "text"
  ]
  node [
    id 13
    label "grupa"
    origin "text"
  ]
  node [
    id 14
    label "kierunek"
    origin "text"
  ]
  node [
    id 15
    label "uczelnia"
    origin "text"
  ]
  node [
    id 16
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 17
    label "dana"
    origin "text"
  ]
  node [
    id 18
    label "nades&#322;a&#263;"
    origin "text"
  ]
  node [
    id 19
    label "przez"
    origin "text"
  ]
  node [
    id 20
    label "by&#263;"
  ]
  node [
    id 21
    label "okre&#347;la&#263;"
  ]
  node [
    id 22
    label "represent"
  ]
  node [
    id 23
    label "wyraz"
  ]
  node [
    id 24
    label "wskazywa&#263;"
  ]
  node [
    id 25
    label "stanowi&#263;"
  ]
  node [
    id 26
    label "signify"
  ]
  node [
    id 27
    label "set"
  ]
  node [
    id 28
    label "ustala&#263;"
  ]
  node [
    id 29
    label "kategoria"
  ]
  node [
    id 30
    label "kategoria_gramatyczna"
  ]
  node [
    id 31
    label "kwadrat_magiczny"
  ]
  node [
    id 32
    label "cecha"
  ]
  node [
    id 33
    label "wyra&#380;enie"
  ]
  node [
    id 34
    label "pierwiastek"
  ]
  node [
    id 35
    label "rozmiar"
  ]
  node [
    id 36
    label "number"
  ]
  node [
    id 37
    label "poj&#281;cie"
  ]
  node [
    id 38
    label "koniugacja"
  ]
  node [
    id 39
    label "tutor"
  ]
  node [
    id 40
    label "akademik"
  ]
  node [
    id 41
    label "immatrykulowanie"
  ]
  node [
    id 42
    label "s&#322;uchacz"
  ]
  node [
    id 43
    label "immatrykulowa&#263;"
  ]
  node [
    id 44
    label "absolwent"
  ]
  node [
    id 45
    label "indeks"
  ]
  node [
    id 46
    label "badanie"
  ]
  node [
    id 47
    label "nauka"
  ]
  node [
    id 48
    label "stacjonarnie"
  ]
  node [
    id 49
    label "od&#322;&#261;czenie"
  ]
  node [
    id 50
    label "cutoff"
  ]
  node [
    id 51
    label "przestanie"
  ]
  node [
    id 52
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 53
    label "w&#322;&#261;czenie"
  ]
  node [
    id 54
    label "nieprawdopodobie&#324;stwo"
  ]
  node [
    id 55
    label "wydzielenie"
  ]
  node [
    id 56
    label "przygaszenie"
  ]
  node [
    id 57
    label "wykluczenie"
  ]
  node [
    id 58
    label "debarment"
  ]
  node [
    id 59
    label "gotowanie"
  ]
  node [
    id 60
    label "przerwanie"
  ]
  node [
    id 61
    label "zatrzymanie"
  ]
  node [
    id 62
    label "release"
  ]
  node [
    id 63
    label "closure"
  ]
  node [
    id 64
    label "odrzucenie"
  ]
  node [
    id 65
    label "odci&#281;cie"
  ]
  node [
    id 66
    label "Zgredek"
  ]
  node [
    id 67
    label "Casanova"
  ]
  node [
    id 68
    label "Don_Juan"
  ]
  node [
    id 69
    label "Gargantua"
  ]
  node [
    id 70
    label "Faust"
  ]
  node [
    id 71
    label "profanum"
  ]
  node [
    id 72
    label "Chocho&#322;"
  ]
  node [
    id 73
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 74
    label "Winnetou"
  ]
  node [
    id 75
    label "Dwukwiat"
  ]
  node [
    id 76
    label "homo_sapiens"
  ]
  node [
    id 77
    label "Edyp"
  ]
  node [
    id 78
    label "Herkules_Poirot"
  ]
  node [
    id 79
    label "ludzko&#347;&#263;"
  ]
  node [
    id 80
    label "mikrokosmos"
  ]
  node [
    id 81
    label "person"
  ]
  node [
    id 82
    label "Szwejk"
  ]
  node [
    id 83
    label "portrecista"
  ]
  node [
    id 84
    label "Sherlock_Holmes"
  ]
  node [
    id 85
    label "Hamlet"
  ]
  node [
    id 86
    label "duch"
  ]
  node [
    id 87
    label "oddzia&#322;ywanie"
  ]
  node [
    id 88
    label "g&#322;owa"
  ]
  node [
    id 89
    label "Quasimodo"
  ]
  node [
    id 90
    label "Dulcynea"
  ]
  node [
    id 91
    label "Wallenrod"
  ]
  node [
    id 92
    label "Don_Kiszot"
  ]
  node [
    id 93
    label "Plastu&#347;"
  ]
  node [
    id 94
    label "Harry_Potter"
  ]
  node [
    id 95
    label "figura"
  ]
  node [
    id 96
    label "parali&#380;owa&#263;"
  ]
  node [
    id 97
    label "istota"
  ]
  node [
    id 98
    label "Werter"
  ]
  node [
    id 99
    label "antropochoria"
  ]
  node [
    id 100
    label "posta&#263;"
  ]
  node [
    id 101
    label "cz&#322;owiek"
  ]
  node [
    id 102
    label "kolejny"
  ]
  node [
    id 103
    label "istota_&#380;ywa"
  ]
  node [
    id 104
    label "najgorszy"
  ]
  node [
    id 105
    label "aktualny"
  ]
  node [
    id 106
    label "ostatnio"
  ]
  node [
    id 107
    label "niedawno"
  ]
  node [
    id 108
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 109
    label "sko&#324;czony"
  ]
  node [
    id 110
    label "poprzedni"
  ]
  node [
    id 111
    label "pozosta&#322;y"
  ]
  node [
    id 112
    label "w&#261;tpliwy"
  ]
  node [
    id 113
    label "stulecie"
  ]
  node [
    id 114
    label "kalendarz"
  ]
  node [
    id 115
    label "czas"
  ]
  node [
    id 116
    label "pora_roku"
  ]
  node [
    id 117
    label "cykl_astronomiczny"
  ]
  node [
    id 118
    label "p&#243;&#322;rocze"
  ]
  node [
    id 119
    label "kwarta&#322;"
  ]
  node [
    id 120
    label "kurs"
  ]
  node [
    id 121
    label "jubileusz"
  ]
  node [
    id 122
    label "miesi&#261;c"
  ]
  node [
    id 123
    label "lata"
  ]
  node [
    id 124
    label "martwy_sezon"
  ]
  node [
    id 125
    label "ki&#347;&#263;"
  ]
  node [
    id 126
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 127
    label "krzew"
  ]
  node [
    id 128
    label "pi&#380;maczkowate"
  ]
  node [
    id 129
    label "pestkowiec"
  ]
  node [
    id 130
    label "kwiat"
  ]
  node [
    id 131
    label "owoc"
  ]
  node [
    id 132
    label "oliwkowate"
  ]
  node [
    id 133
    label "ro&#347;lina"
  ]
  node [
    id 134
    label "hy&#263;ka"
  ]
  node [
    id 135
    label "lilac"
  ]
  node [
    id 136
    label "delfinidyna"
  ]
  node [
    id 137
    label "magiel"
  ]
  node [
    id 138
    label "faza"
  ]
  node [
    id 139
    label "arkusz"
  ]
  node [
    id 140
    label "sprawdzian"
  ]
  node [
    id 141
    label "praca_pisemna"
  ]
  node [
    id 142
    label "oblewanie"
  ]
  node [
    id 143
    label "examination"
  ]
  node [
    id 144
    label "pr&#243;ba"
  ]
  node [
    id 145
    label "oblewa&#263;"
  ]
  node [
    id 146
    label "sesja_egzaminacyjna"
  ]
  node [
    id 147
    label "okre&#347;lony"
  ]
  node [
    id 148
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 149
    label "odm&#322;adza&#263;"
  ]
  node [
    id 150
    label "asymilowa&#263;"
  ]
  node [
    id 151
    label "cz&#261;steczka"
  ]
  node [
    id 152
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 153
    label "egzemplarz"
  ]
  node [
    id 154
    label "formacja_geologiczna"
  ]
  node [
    id 155
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 156
    label "harcerze_starsi"
  ]
  node [
    id 157
    label "liga"
  ]
  node [
    id 158
    label "Terranie"
  ]
  node [
    id 159
    label "&#346;wietliki"
  ]
  node [
    id 160
    label "pakiet_klimatyczny"
  ]
  node [
    id 161
    label "oddzia&#322;"
  ]
  node [
    id 162
    label "stage_set"
  ]
  node [
    id 163
    label "Entuzjastki"
  ]
  node [
    id 164
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 165
    label "odm&#322;odzenie"
  ]
  node [
    id 166
    label "type"
  ]
  node [
    id 167
    label "category"
  ]
  node [
    id 168
    label "asymilowanie"
  ]
  node [
    id 169
    label "specgrupa"
  ]
  node [
    id 170
    label "odm&#322;adzanie"
  ]
  node [
    id 171
    label "gromada"
  ]
  node [
    id 172
    label "Eurogrupa"
  ]
  node [
    id 173
    label "jednostka_systematyczna"
  ]
  node [
    id 174
    label "kompozycja"
  ]
  node [
    id 175
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 176
    label "zbi&#243;r"
  ]
  node [
    id 177
    label "skr&#281;canie"
  ]
  node [
    id 178
    label "skr&#281;ci&#263;"
  ]
  node [
    id 179
    label "orientowa&#263;"
  ]
  node [
    id 180
    label "bok"
  ]
  node [
    id 181
    label "praktyka"
  ]
  node [
    id 182
    label "metoda"
  ]
  node [
    id 183
    label "orientowanie"
  ]
  node [
    id 184
    label "system"
  ]
  node [
    id 185
    label "skr&#281;ca&#263;"
  ]
  node [
    id 186
    label "g&#243;ra"
  ]
  node [
    id 187
    label "przebieg"
  ]
  node [
    id 188
    label "orientacja"
  ]
  node [
    id 189
    label "spos&#243;b"
  ]
  node [
    id 190
    label "linia"
  ]
  node [
    id 191
    label "ideologia"
  ]
  node [
    id 192
    label "skr&#281;cenie"
  ]
  node [
    id 193
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 194
    label "przeorientowywa&#263;"
  ]
  node [
    id 195
    label "bearing"
  ]
  node [
    id 196
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 197
    label "zorientowa&#263;"
  ]
  node [
    id 198
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 199
    label "zorientowanie"
  ]
  node [
    id 200
    label "przeorientowa&#263;"
  ]
  node [
    id 201
    label "przeorientowanie"
  ]
  node [
    id 202
    label "ty&#322;"
  ]
  node [
    id 203
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 204
    label "przeorientowywanie"
  ]
  node [
    id 205
    label "prz&#243;d"
  ]
  node [
    id 206
    label "rektorat"
  ]
  node [
    id 207
    label "podkanclerz"
  ]
  node [
    id 208
    label "kanclerz"
  ]
  node [
    id 209
    label "kwestura"
  ]
  node [
    id 210
    label "miasteczko_studenckie"
  ]
  node [
    id 211
    label "school"
  ]
  node [
    id 212
    label "senat"
  ]
  node [
    id 213
    label "wyk&#322;adanie"
  ]
  node [
    id 214
    label "promotorstwo"
  ]
  node [
    id 215
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 216
    label "szko&#322;a"
  ]
  node [
    id 217
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 218
    label "rise"
  ]
  node [
    id 219
    label "appear"
  ]
  node [
    id 220
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 221
    label "dar"
  ]
  node [
    id 222
    label "cnota"
  ]
  node [
    id 223
    label "buddyzm"
  ]
  node [
    id 224
    label "dostarczy&#263;"
  ]
  node [
    id 225
    label "air"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 217
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 224
  ]
  edge [
    source 18
    target 225
  ]
]
