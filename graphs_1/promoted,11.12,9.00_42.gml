graph [
  maxDegree 26
  minDegree 1
  meanDegree 1.980952380952381
  density 0.01904761904761905
  graphCliqueNumber 2
  node [
    id 0
    label "nadawca"
    origin "text"
  ]
  node [
    id 1
    label "telewizyjny"
    origin "text"
  ]
  node [
    id 2
    label "hbo"
    origin "text"
  ]
  node [
    id 3
    label "europe"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 6
    label "polski"
    origin "text"
  ]
  node [
    id 7
    label "oddzia&#322;"
    origin "text"
  ]
  node [
    id 8
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "kontynuacja"
    origin "text"
  ]
  node [
    id 10
    label "serialowy"
    origin "text"
  ]
  node [
    id 11
    label "opowie&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "stra&#380;"
    origin "text"
  ]
  node [
    id 13
    label "graniczny"
    origin "text"
  ]
  node [
    id 14
    label "bieszczady"
    origin "text"
  ]
  node [
    id 15
    label "organizacja"
  ]
  node [
    id 16
    label "podmiot"
  ]
  node [
    id 17
    label "klient"
  ]
  node [
    id 18
    label "przesy&#322;ka"
  ]
  node [
    id 19
    label "autor"
  ]
  node [
    id 20
    label "specjalny"
  ]
  node [
    id 21
    label "medialny"
  ]
  node [
    id 22
    label "telewizyjnie"
  ]
  node [
    id 23
    label "trza"
  ]
  node [
    id 24
    label "uczestniczy&#263;"
  ]
  node [
    id 25
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 26
    label "para"
  ]
  node [
    id 27
    label "necessity"
  ]
  node [
    id 28
    label "lacki"
  ]
  node [
    id 29
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 30
    label "przedmiot"
  ]
  node [
    id 31
    label "sztajer"
  ]
  node [
    id 32
    label "drabant"
  ]
  node [
    id 33
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 34
    label "polak"
  ]
  node [
    id 35
    label "pierogi_ruskie"
  ]
  node [
    id 36
    label "krakowiak"
  ]
  node [
    id 37
    label "Polish"
  ]
  node [
    id 38
    label "j&#281;zyk"
  ]
  node [
    id 39
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 40
    label "oberek"
  ]
  node [
    id 41
    label "po_polsku"
  ]
  node [
    id 42
    label "mazur"
  ]
  node [
    id 43
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 44
    label "chodzony"
  ]
  node [
    id 45
    label "skoczny"
  ]
  node [
    id 46
    label "ryba_po_grecku"
  ]
  node [
    id 47
    label "goniony"
  ]
  node [
    id 48
    label "polsko"
  ]
  node [
    id 49
    label "whole"
  ]
  node [
    id 50
    label "jednostka"
  ]
  node [
    id 51
    label "jednostka_geologiczna"
  ]
  node [
    id 52
    label "system"
  ]
  node [
    id 53
    label "poziom"
  ]
  node [
    id 54
    label "agencja"
  ]
  node [
    id 55
    label "dogger"
  ]
  node [
    id 56
    label "formacja"
  ]
  node [
    id 57
    label "pi&#281;tro"
  ]
  node [
    id 58
    label "filia"
  ]
  node [
    id 59
    label "dzia&#322;"
  ]
  node [
    id 60
    label "promocja"
  ]
  node [
    id 61
    label "zesp&#243;&#322;"
  ]
  node [
    id 62
    label "kurs"
  ]
  node [
    id 63
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 64
    label "wojsko"
  ]
  node [
    id 65
    label "siedziba"
  ]
  node [
    id 66
    label "bank"
  ]
  node [
    id 67
    label "lias"
  ]
  node [
    id 68
    label "szpital"
  ]
  node [
    id 69
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 70
    label "malm"
  ]
  node [
    id 71
    label "ajencja"
  ]
  node [
    id 72
    label "klasa"
  ]
  node [
    id 73
    label "sta&#263;_si&#281;"
  ]
  node [
    id 74
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 75
    label "podj&#261;&#263;"
  ]
  node [
    id 76
    label "determine"
  ]
  node [
    id 77
    label "decide"
  ]
  node [
    id 78
    label "zrobi&#263;"
  ]
  node [
    id 79
    label "follow-up"
  ]
  node [
    id 80
    label "proces"
  ]
  node [
    id 81
    label "utw&#243;r"
  ]
  node [
    id 82
    label "typowy"
  ]
  node [
    id 83
    label "report"
  ]
  node [
    id 84
    label "fabu&#322;a"
  ]
  node [
    id 85
    label "opowiadanie"
  ]
  node [
    id 86
    label "wypowied&#378;"
  ]
  node [
    id 87
    label "ochrona"
  ]
  node [
    id 88
    label "s&#322;u&#380;ba_publiczna"
  ]
  node [
    id 89
    label "rota"
  ]
  node [
    id 90
    label "wedeta"
  ]
  node [
    id 91
    label "posterunek"
  ]
  node [
    id 92
    label "s&#322;u&#380;ba"
  ]
  node [
    id 93
    label "stra&#380;_ogniowa"
  ]
  node [
    id 94
    label "przyleg&#322;y"
  ]
  node [
    id 95
    label "skrajny"
  ]
  node [
    id 96
    label "wa&#380;ny"
  ]
  node [
    id 97
    label "granicznie"
  ]
  node [
    id 98
    label "ostateczny"
  ]
  node [
    id 99
    label "HBO"
  ]
  node [
    id 100
    label "Europe"
  ]
  node [
    id 101
    label "stra&#380;a"
  ]
  node [
    id 102
    label "wataha"
  ]
  node [
    id 103
    label "3"
  ]
  node [
    id 104
    label "Polska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 104
  ]
  edge [
    source 102
    target 103
  ]
]
