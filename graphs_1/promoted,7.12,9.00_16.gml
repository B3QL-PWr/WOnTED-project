graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.0120481927710845
  density 0.0040483867057768295
  graphCliqueNumber 4
  node [
    id 0
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 1
    label "raport"
    origin "text"
  ]
  node [
    id 2
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "przez"
    origin "text"
  ]
  node [
    id 4
    label "niemiecki"
    origin "text"
  ]
  node [
    id 5
    label "instytut"
    origin "text"
  ]
  node [
    id 6
    label "prawy"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 8
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "imigrant"
    origin "text"
  ]
  node [
    id 10
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "przyby&#322;a"
    origin "text"
  ]
  node [
    id 12
    label "kraj"
    origin "text"
  ]
  node [
    id 13
    label "cel"
    origin "text"
  ]
  node [
    id 14
    label "podj&#281;cie"
    origin "text"
  ]
  node [
    id 15
    label "praca"
    origin "text"
  ]
  node [
    id 16
    label "spotka&#263;"
    origin "text"
  ]
  node [
    id 17
    label "ci&#281;&#380;ki"
    origin "text"
  ]
  node [
    id 18
    label "wyzysk"
    origin "text"
  ]
  node [
    id 19
    label "raport_Beveridge'a"
  ]
  node [
    id 20
    label "relacja"
  ]
  node [
    id 21
    label "raport_Fischlera"
  ]
  node [
    id 22
    label "statement"
  ]
  node [
    id 23
    label "upubliczni&#263;"
  ]
  node [
    id 24
    label "wydawnictwo"
  ]
  node [
    id 25
    label "wprowadzi&#263;"
  ]
  node [
    id 26
    label "picture"
  ]
  node [
    id 27
    label "szwabski"
  ]
  node [
    id 28
    label "po_niemiecku"
  ]
  node [
    id 29
    label "niemiec"
  ]
  node [
    id 30
    label "cenar"
  ]
  node [
    id 31
    label "j&#281;zyk"
  ]
  node [
    id 32
    label "europejski"
  ]
  node [
    id 33
    label "German"
  ]
  node [
    id 34
    label "pionier"
  ]
  node [
    id 35
    label "niemiecko"
  ]
  node [
    id 36
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 37
    label "zachodnioeuropejski"
  ]
  node [
    id 38
    label "strudel"
  ]
  node [
    id 39
    label "junkers"
  ]
  node [
    id 40
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 41
    label "plac&#243;wka"
  ]
  node [
    id 42
    label "institute"
  ]
  node [
    id 43
    label "&#379;ydowski_Instytut_Historyczny"
  ]
  node [
    id 44
    label "Ossolineum"
  ]
  node [
    id 45
    label "instytucja"
  ]
  node [
    id 46
    label "plac&#243;wka_naukowa"
  ]
  node [
    id 47
    label "z_prawa"
  ]
  node [
    id 48
    label "cnotliwy"
  ]
  node [
    id 49
    label "moralny"
  ]
  node [
    id 50
    label "naturalny"
  ]
  node [
    id 51
    label "zgodnie_z_prawem"
  ]
  node [
    id 52
    label "legalny"
  ]
  node [
    id 53
    label "na_prawo"
  ]
  node [
    id 54
    label "s&#322;uszny"
  ]
  node [
    id 55
    label "w_prawo"
  ]
  node [
    id 56
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 57
    label "prawicowy"
  ]
  node [
    id 58
    label "chwalebny"
  ]
  node [
    id 59
    label "zacny"
  ]
  node [
    id 60
    label "asymilowa&#263;"
  ]
  node [
    id 61
    label "wapniak"
  ]
  node [
    id 62
    label "dwun&#243;g"
  ]
  node [
    id 63
    label "polifag"
  ]
  node [
    id 64
    label "wz&#243;r"
  ]
  node [
    id 65
    label "profanum"
  ]
  node [
    id 66
    label "hominid"
  ]
  node [
    id 67
    label "homo_sapiens"
  ]
  node [
    id 68
    label "nasada"
  ]
  node [
    id 69
    label "podw&#322;adny"
  ]
  node [
    id 70
    label "ludzko&#347;&#263;"
  ]
  node [
    id 71
    label "os&#322;abianie"
  ]
  node [
    id 72
    label "mikrokosmos"
  ]
  node [
    id 73
    label "portrecista"
  ]
  node [
    id 74
    label "duch"
  ]
  node [
    id 75
    label "oddzia&#322;ywanie"
  ]
  node [
    id 76
    label "g&#322;owa"
  ]
  node [
    id 77
    label "asymilowanie"
  ]
  node [
    id 78
    label "osoba"
  ]
  node [
    id 79
    label "os&#322;abia&#263;"
  ]
  node [
    id 80
    label "figura"
  ]
  node [
    id 81
    label "Adam"
  ]
  node [
    id 82
    label "senior"
  ]
  node [
    id 83
    label "antropochoria"
  ]
  node [
    id 84
    label "posta&#263;"
  ]
  node [
    id 85
    label "whole"
  ]
  node [
    id 86
    label "Rzym_Zachodni"
  ]
  node [
    id 87
    label "element"
  ]
  node [
    id 88
    label "ilo&#347;&#263;"
  ]
  node [
    id 89
    label "urz&#261;dzenie"
  ]
  node [
    id 90
    label "Rzym_Wschodni"
  ]
  node [
    id 91
    label "cudzoziemiec"
  ]
  node [
    id 92
    label "przybysz"
  ]
  node [
    id 93
    label "migrant"
  ]
  node [
    id 94
    label "imigracja"
  ]
  node [
    id 95
    label "ochrona_uzupe&#322;niaj&#261;ca"
  ]
  node [
    id 96
    label "Skandynawia"
  ]
  node [
    id 97
    label "Filipiny"
  ]
  node [
    id 98
    label "Rwanda"
  ]
  node [
    id 99
    label "Kaukaz"
  ]
  node [
    id 100
    label "Kaszmir"
  ]
  node [
    id 101
    label "Toskania"
  ]
  node [
    id 102
    label "Yorkshire"
  ]
  node [
    id 103
    label "&#321;emkowszczyzna"
  ]
  node [
    id 104
    label "obszar"
  ]
  node [
    id 105
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 106
    label "Monako"
  ]
  node [
    id 107
    label "Amhara"
  ]
  node [
    id 108
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 109
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 110
    label "Lombardia"
  ]
  node [
    id 111
    label "Korea"
  ]
  node [
    id 112
    label "Kalabria"
  ]
  node [
    id 113
    label "Ghana"
  ]
  node [
    id 114
    label "Czarnog&#243;ra"
  ]
  node [
    id 115
    label "Tyrol"
  ]
  node [
    id 116
    label "Malawi"
  ]
  node [
    id 117
    label "Indonezja"
  ]
  node [
    id 118
    label "Bu&#322;garia"
  ]
  node [
    id 119
    label "Nauru"
  ]
  node [
    id 120
    label "Kenia"
  ]
  node [
    id 121
    label "Pamir"
  ]
  node [
    id 122
    label "Kambod&#380;a"
  ]
  node [
    id 123
    label "Lubelszczyzna"
  ]
  node [
    id 124
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 125
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 126
    label "Mali"
  ]
  node [
    id 127
    label "&#379;ywiecczyzna"
  ]
  node [
    id 128
    label "Austria"
  ]
  node [
    id 129
    label "interior"
  ]
  node [
    id 130
    label "Europa_Wschodnia"
  ]
  node [
    id 131
    label "Armenia"
  ]
  node [
    id 132
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 133
    label "Fid&#380;i"
  ]
  node [
    id 134
    label "Tuwalu"
  ]
  node [
    id 135
    label "Zabajkale"
  ]
  node [
    id 136
    label "Etiopia"
  ]
  node [
    id 137
    label "Malta"
  ]
  node [
    id 138
    label "Malezja"
  ]
  node [
    id 139
    label "Kaszuby"
  ]
  node [
    id 140
    label "Bo&#347;nia"
  ]
  node [
    id 141
    label "Noworosja"
  ]
  node [
    id 142
    label "Grenada"
  ]
  node [
    id 143
    label "Tad&#380;ykistan"
  ]
  node [
    id 144
    label "Ba&#322;kany"
  ]
  node [
    id 145
    label "Wehrlen"
  ]
  node [
    id 146
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 147
    label "Anglia"
  ]
  node [
    id 148
    label "Kielecczyzna"
  ]
  node [
    id 149
    label "Rumunia"
  ]
  node [
    id 150
    label "Pomorze_Zachodnie"
  ]
  node [
    id 151
    label "Maroko"
  ]
  node [
    id 152
    label "Bhutan"
  ]
  node [
    id 153
    label "Opolskie"
  ]
  node [
    id 154
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 155
    label "Ko&#322;yma"
  ]
  node [
    id 156
    label "Oksytania"
  ]
  node [
    id 157
    label "S&#322;owacja"
  ]
  node [
    id 158
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 159
    label "Seszele"
  ]
  node [
    id 160
    label "Syjon"
  ]
  node [
    id 161
    label "Kuwejt"
  ]
  node [
    id 162
    label "Arabia_Saudyjska"
  ]
  node [
    id 163
    label "Kociewie"
  ]
  node [
    id 164
    label "Ekwador"
  ]
  node [
    id 165
    label "Kanada"
  ]
  node [
    id 166
    label "ziemia"
  ]
  node [
    id 167
    label "Japonia"
  ]
  node [
    id 168
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 169
    label "Hiszpania"
  ]
  node [
    id 170
    label "Wyspy_Marshalla"
  ]
  node [
    id 171
    label "Botswana"
  ]
  node [
    id 172
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 173
    label "D&#380;ibuti"
  ]
  node [
    id 174
    label "Huculszczyzna"
  ]
  node [
    id 175
    label "Wietnam"
  ]
  node [
    id 176
    label "Egipt"
  ]
  node [
    id 177
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 178
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 179
    label "Burkina_Faso"
  ]
  node [
    id 180
    label "Bawaria"
  ]
  node [
    id 181
    label "Niemcy"
  ]
  node [
    id 182
    label "Khitai"
  ]
  node [
    id 183
    label "Macedonia"
  ]
  node [
    id 184
    label "Albania"
  ]
  node [
    id 185
    label "Madagaskar"
  ]
  node [
    id 186
    label "Bahrajn"
  ]
  node [
    id 187
    label "Jemen"
  ]
  node [
    id 188
    label "Lesoto"
  ]
  node [
    id 189
    label "Maghreb"
  ]
  node [
    id 190
    label "Samoa"
  ]
  node [
    id 191
    label "Andora"
  ]
  node [
    id 192
    label "Bory_Tucholskie"
  ]
  node [
    id 193
    label "Chiny"
  ]
  node [
    id 194
    label "Europa_Zachodnia"
  ]
  node [
    id 195
    label "Cypr"
  ]
  node [
    id 196
    label "Wielka_Brytania"
  ]
  node [
    id 197
    label "Kerala"
  ]
  node [
    id 198
    label "Podhale"
  ]
  node [
    id 199
    label "Kabylia"
  ]
  node [
    id 200
    label "Ukraina"
  ]
  node [
    id 201
    label "Paragwaj"
  ]
  node [
    id 202
    label "Trynidad_i_Tobago"
  ]
  node [
    id 203
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 204
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 205
    label "Ma&#322;opolska"
  ]
  node [
    id 206
    label "Polesie"
  ]
  node [
    id 207
    label "Liguria"
  ]
  node [
    id 208
    label "Libia"
  ]
  node [
    id 209
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 210
    label "&#321;&#243;dzkie"
  ]
  node [
    id 211
    label "Surinam"
  ]
  node [
    id 212
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 213
    label "Palestyna"
  ]
  node [
    id 214
    label "Australia"
  ]
  node [
    id 215
    label "Nigeria"
  ]
  node [
    id 216
    label "Honduras"
  ]
  node [
    id 217
    label "Bojkowszczyzna"
  ]
  node [
    id 218
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 219
    label "Karaiby"
  ]
  node [
    id 220
    label "Bangladesz"
  ]
  node [
    id 221
    label "Peru"
  ]
  node [
    id 222
    label "Kazachstan"
  ]
  node [
    id 223
    label "USA"
  ]
  node [
    id 224
    label "Irak"
  ]
  node [
    id 225
    label "Nepal"
  ]
  node [
    id 226
    label "S&#261;decczyzna"
  ]
  node [
    id 227
    label "Sudan"
  ]
  node [
    id 228
    label "Sand&#380;ak"
  ]
  node [
    id 229
    label "Nadrenia"
  ]
  node [
    id 230
    label "San_Marino"
  ]
  node [
    id 231
    label "Burundi"
  ]
  node [
    id 232
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 233
    label "Dominikana"
  ]
  node [
    id 234
    label "Komory"
  ]
  node [
    id 235
    label "Zakarpacie"
  ]
  node [
    id 236
    label "Gwatemala"
  ]
  node [
    id 237
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 238
    label "Zag&#243;rze"
  ]
  node [
    id 239
    label "Andaluzja"
  ]
  node [
    id 240
    label "granica_pa&#324;stwa"
  ]
  node [
    id 241
    label "Turkiestan"
  ]
  node [
    id 242
    label "Naddniestrze"
  ]
  node [
    id 243
    label "Hercegowina"
  ]
  node [
    id 244
    label "Brunei"
  ]
  node [
    id 245
    label "Iran"
  ]
  node [
    id 246
    label "jednostka_administracyjna"
  ]
  node [
    id 247
    label "Zimbabwe"
  ]
  node [
    id 248
    label "Namibia"
  ]
  node [
    id 249
    label "Meksyk"
  ]
  node [
    id 250
    label "Lotaryngia"
  ]
  node [
    id 251
    label "Kamerun"
  ]
  node [
    id 252
    label "Opolszczyzna"
  ]
  node [
    id 253
    label "Afryka_Wschodnia"
  ]
  node [
    id 254
    label "Szlezwik"
  ]
  node [
    id 255
    label "Somalia"
  ]
  node [
    id 256
    label "Angola"
  ]
  node [
    id 257
    label "Gabon"
  ]
  node [
    id 258
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 259
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 260
    label "Mozambik"
  ]
  node [
    id 261
    label "Tajwan"
  ]
  node [
    id 262
    label "Tunezja"
  ]
  node [
    id 263
    label "Nowa_Zelandia"
  ]
  node [
    id 264
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 265
    label "Podbeskidzie"
  ]
  node [
    id 266
    label "Liban"
  ]
  node [
    id 267
    label "Jordania"
  ]
  node [
    id 268
    label "Tonga"
  ]
  node [
    id 269
    label "Czad"
  ]
  node [
    id 270
    label "Liberia"
  ]
  node [
    id 271
    label "Gwinea"
  ]
  node [
    id 272
    label "Belize"
  ]
  node [
    id 273
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 274
    label "Mazowsze"
  ]
  node [
    id 275
    label "&#321;otwa"
  ]
  node [
    id 276
    label "Syria"
  ]
  node [
    id 277
    label "Benin"
  ]
  node [
    id 278
    label "Afryka_Zachodnia"
  ]
  node [
    id 279
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 280
    label "Dominika"
  ]
  node [
    id 281
    label "Antigua_i_Barbuda"
  ]
  node [
    id 282
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 283
    label "Hanower"
  ]
  node [
    id 284
    label "Galicja"
  ]
  node [
    id 285
    label "Szkocja"
  ]
  node [
    id 286
    label "Walia"
  ]
  node [
    id 287
    label "Afganistan"
  ]
  node [
    id 288
    label "Kiribati"
  ]
  node [
    id 289
    label "W&#322;ochy"
  ]
  node [
    id 290
    label "Szwajcaria"
  ]
  node [
    id 291
    label "Powi&#347;le"
  ]
  node [
    id 292
    label "Sahara_Zachodnia"
  ]
  node [
    id 293
    label "Chorwacja"
  ]
  node [
    id 294
    label "Tajlandia"
  ]
  node [
    id 295
    label "Salwador"
  ]
  node [
    id 296
    label "Bahamy"
  ]
  node [
    id 297
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 298
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 299
    label "Zamojszczyzna"
  ]
  node [
    id 300
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 301
    label "S&#322;owenia"
  ]
  node [
    id 302
    label "Gambia"
  ]
  node [
    id 303
    label "Kujawy"
  ]
  node [
    id 304
    label "Urugwaj"
  ]
  node [
    id 305
    label "Podlasie"
  ]
  node [
    id 306
    label "Zair"
  ]
  node [
    id 307
    label "Erytrea"
  ]
  node [
    id 308
    label "Laponia"
  ]
  node [
    id 309
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 310
    label "Umbria"
  ]
  node [
    id 311
    label "Rosja"
  ]
  node [
    id 312
    label "Uganda"
  ]
  node [
    id 313
    label "Niger"
  ]
  node [
    id 314
    label "Mauritius"
  ]
  node [
    id 315
    label "Turkmenistan"
  ]
  node [
    id 316
    label "Turcja"
  ]
  node [
    id 317
    label "Mezoameryka"
  ]
  node [
    id 318
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 319
    label "Irlandia"
  ]
  node [
    id 320
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 321
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 322
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 323
    label "Gwinea_Bissau"
  ]
  node [
    id 324
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 325
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 326
    label "Kurdystan"
  ]
  node [
    id 327
    label "Belgia"
  ]
  node [
    id 328
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 329
    label "Palau"
  ]
  node [
    id 330
    label "Barbados"
  ]
  node [
    id 331
    label "Chile"
  ]
  node [
    id 332
    label "Wenezuela"
  ]
  node [
    id 333
    label "W&#281;gry"
  ]
  node [
    id 334
    label "Argentyna"
  ]
  node [
    id 335
    label "Kolumbia"
  ]
  node [
    id 336
    label "Kampania"
  ]
  node [
    id 337
    label "Armagnac"
  ]
  node [
    id 338
    label "Sierra_Leone"
  ]
  node [
    id 339
    label "Azerbejd&#380;an"
  ]
  node [
    id 340
    label "Kongo"
  ]
  node [
    id 341
    label "Polinezja"
  ]
  node [
    id 342
    label "Warmia"
  ]
  node [
    id 343
    label "Pakistan"
  ]
  node [
    id 344
    label "Liechtenstein"
  ]
  node [
    id 345
    label "Wielkopolska"
  ]
  node [
    id 346
    label "Nikaragua"
  ]
  node [
    id 347
    label "Senegal"
  ]
  node [
    id 348
    label "brzeg"
  ]
  node [
    id 349
    label "Bordeaux"
  ]
  node [
    id 350
    label "Lauda"
  ]
  node [
    id 351
    label "Indie"
  ]
  node [
    id 352
    label "Mazury"
  ]
  node [
    id 353
    label "Suazi"
  ]
  node [
    id 354
    label "Polska"
  ]
  node [
    id 355
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 356
    label "Algieria"
  ]
  node [
    id 357
    label "Jamajka"
  ]
  node [
    id 358
    label "Timor_Wschodni"
  ]
  node [
    id 359
    label "Oceania"
  ]
  node [
    id 360
    label "Kostaryka"
  ]
  node [
    id 361
    label "Podkarpacie"
  ]
  node [
    id 362
    label "Lasko"
  ]
  node [
    id 363
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 364
    label "Kuba"
  ]
  node [
    id 365
    label "Mauretania"
  ]
  node [
    id 366
    label "Amazonia"
  ]
  node [
    id 367
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 368
    label "Portoryko"
  ]
  node [
    id 369
    label "Brazylia"
  ]
  node [
    id 370
    label "Mo&#322;dawia"
  ]
  node [
    id 371
    label "organizacja"
  ]
  node [
    id 372
    label "Litwa"
  ]
  node [
    id 373
    label "Kirgistan"
  ]
  node [
    id 374
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 375
    label "Izrael"
  ]
  node [
    id 376
    label "Grecja"
  ]
  node [
    id 377
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 378
    label "Kurpie"
  ]
  node [
    id 379
    label "Holandia"
  ]
  node [
    id 380
    label "Sri_Lanka"
  ]
  node [
    id 381
    label "Tonkin"
  ]
  node [
    id 382
    label "Katar"
  ]
  node [
    id 383
    label "Azja_Wschodnia"
  ]
  node [
    id 384
    label "Mikronezja"
  ]
  node [
    id 385
    label "Ukraina_Zachodnia"
  ]
  node [
    id 386
    label "Laos"
  ]
  node [
    id 387
    label "Mongolia"
  ]
  node [
    id 388
    label "Turyngia"
  ]
  node [
    id 389
    label "Malediwy"
  ]
  node [
    id 390
    label "Zambia"
  ]
  node [
    id 391
    label "Baszkiria"
  ]
  node [
    id 392
    label "Tanzania"
  ]
  node [
    id 393
    label "Gujana"
  ]
  node [
    id 394
    label "Apulia"
  ]
  node [
    id 395
    label "Czechy"
  ]
  node [
    id 396
    label "Panama"
  ]
  node [
    id 397
    label "Uzbekistan"
  ]
  node [
    id 398
    label "Gruzja"
  ]
  node [
    id 399
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 400
    label "Serbia"
  ]
  node [
    id 401
    label "Francja"
  ]
  node [
    id 402
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 403
    label "Togo"
  ]
  node [
    id 404
    label "Estonia"
  ]
  node [
    id 405
    label "Indochiny"
  ]
  node [
    id 406
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 407
    label "Oman"
  ]
  node [
    id 408
    label "Boliwia"
  ]
  node [
    id 409
    label "Portugalia"
  ]
  node [
    id 410
    label "Wyspy_Salomona"
  ]
  node [
    id 411
    label "Luksemburg"
  ]
  node [
    id 412
    label "Haiti"
  ]
  node [
    id 413
    label "Biskupizna"
  ]
  node [
    id 414
    label "Lubuskie"
  ]
  node [
    id 415
    label "Birma"
  ]
  node [
    id 416
    label "Rodezja"
  ]
  node [
    id 417
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 418
    label "miejsce"
  ]
  node [
    id 419
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 420
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 421
    label "rzecz"
  ]
  node [
    id 422
    label "punkt"
  ]
  node [
    id 423
    label "thing"
  ]
  node [
    id 424
    label "rezultat"
  ]
  node [
    id 425
    label "zrobienie"
  ]
  node [
    id 426
    label "consumption"
  ]
  node [
    id 427
    label "czynno&#347;&#263;"
  ]
  node [
    id 428
    label "spowodowanie"
  ]
  node [
    id 429
    label "zareagowanie"
  ]
  node [
    id 430
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 431
    label "erecting"
  ]
  node [
    id 432
    label "movement"
  ]
  node [
    id 433
    label "entertainment"
  ]
  node [
    id 434
    label "zacz&#281;cie"
  ]
  node [
    id 435
    label "stosunek_pracy"
  ]
  node [
    id 436
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 437
    label "benedykty&#324;ski"
  ]
  node [
    id 438
    label "pracowanie"
  ]
  node [
    id 439
    label "zaw&#243;d"
  ]
  node [
    id 440
    label "kierownictwo"
  ]
  node [
    id 441
    label "zmiana"
  ]
  node [
    id 442
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 443
    label "wytw&#243;r"
  ]
  node [
    id 444
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 445
    label "tynkarski"
  ]
  node [
    id 446
    label "czynnik_produkcji"
  ]
  node [
    id 447
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 448
    label "zobowi&#261;zanie"
  ]
  node [
    id 449
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 450
    label "tyrka"
  ]
  node [
    id 451
    label "pracowa&#263;"
  ]
  node [
    id 452
    label "siedziba"
  ]
  node [
    id 453
    label "poda&#380;_pracy"
  ]
  node [
    id 454
    label "zak&#322;ad"
  ]
  node [
    id 455
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 456
    label "najem"
  ]
  node [
    id 457
    label "befall"
  ]
  node [
    id 458
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 459
    label "pozna&#263;"
  ]
  node [
    id 460
    label "spowodowa&#263;"
  ]
  node [
    id 461
    label "go_steady"
  ]
  node [
    id 462
    label "insert"
  ]
  node [
    id 463
    label "znale&#378;&#263;"
  ]
  node [
    id 464
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 465
    label "visualize"
  ]
  node [
    id 466
    label "nieprzejrzysty"
  ]
  node [
    id 467
    label "wolny"
  ]
  node [
    id 468
    label "grubo"
  ]
  node [
    id 469
    label "przyswajalny"
  ]
  node [
    id 470
    label "masywny"
  ]
  node [
    id 471
    label "zbrojny"
  ]
  node [
    id 472
    label "gro&#378;ny"
  ]
  node [
    id 473
    label "trudny"
  ]
  node [
    id 474
    label "wymagaj&#261;cy"
  ]
  node [
    id 475
    label "ambitny"
  ]
  node [
    id 476
    label "monumentalny"
  ]
  node [
    id 477
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 478
    label "niezgrabny"
  ]
  node [
    id 479
    label "charakterystyczny"
  ]
  node [
    id 480
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 481
    label "k&#322;opotliwy"
  ]
  node [
    id 482
    label "dotkliwy"
  ]
  node [
    id 483
    label "nieudany"
  ]
  node [
    id 484
    label "mocny"
  ]
  node [
    id 485
    label "bojowy"
  ]
  node [
    id 486
    label "ci&#281;&#380;ko"
  ]
  node [
    id 487
    label "kompletny"
  ]
  node [
    id 488
    label "intensywny"
  ]
  node [
    id 489
    label "wielki"
  ]
  node [
    id 490
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 491
    label "liczny"
  ]
  node [
    id 492
    label "niedelikatny"
  ]
  node [
    id 493
    label "exploitation"
  ]
  node [
    id 494
    label "rozb&#243;j"
  ]
  node [
    id 495
    label "krzywda"
  ]
  node [
    id 496
    label "Europa"
  ]
  node [
    id 497
    label "wschodni"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 200
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 203
  ]
  edge [
    source 12
    target 204
  ]
  edge [
    source 12
    target 205
  ]
  edge [
    source 12
    target 206
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 208
  ]
  edge [
    source 12
    target 209
  ]
  edge [
    source 12
    target 210
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 212
  ]
  edge [
    source 12
    target 213
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 215
  ]
  edge [
    source 12
    target 216
  ]
  edge [
    source 12
    target 217
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 221
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 224
  ]
  edge [
    source 12
    target 225
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 229
  ]
  edge [
    source 12
    target 230
  ]
  edge [
    source 12
    target 231
  ]
  edge [
    source 12
    target 232
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 12
    target 236
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 238
  ]
  edge [
    source 12
    target 239
  ]
  edge [
    source 12
    target 240
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 242
  ]
  edge [
    source 12
    target 243
  ]
  edge [
    source 12
    target 244
  ]
  edge [
    source 12
    target 245
  ]
  edge [
    source 12
    target 246
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 248
  ]
  edge [
    source 12
    target 249
  ]
  edge [
    source 12
    target 250
  ]
  edge [
    source 12
    target 251
  ]
  edge [
    source 12
    target 252
  ]
  edge [
    source 12
    target 253
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 255
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 257
  ]
  edge [
    source 12
    target 258
  ]
  edge [
    source 12
    target 259
  ]
  edge [
    source 12
    target 260
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 262
  ]
  edge [
    source 12
    target 263
  ]
  edge [
    source 12
    target 264
  ]
  edge [
    source 12
    target 265
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 276
  ]
  edge [
    source 12
    target 277
  ]
  edge [
    source 12
    target 278
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 280
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 12
    target 282
  ]
  edge [
    source 12
    target 283
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 285
  ]
  edge [
    source 12
    target 286
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 312
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 314
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 326
  ]
  edge [
    source 12
    target 327
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 366
  ]
  edge [
    source 12
    target 367
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 370
  ]
  edge [
    source 12
    target 371
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 425
  ]
  edge [
    source 14
    target 426
  ]
  edge [
    source 14
    target 427
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 429
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 431
  ]
  edge [
    source 14
    target 432
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 435
  ]
  edge [
    source 15
    target 436
  ]
  edge [
    source 15
    target 437
  ]
  edge [
    source 15
    target 438
  ]
  edge [
    source 15
    target 439
  ]
  edge [
    source 15
    target 440
  ]
  edge [
    source 15
    target 441
  ]
  edge [
    source 15
    target 442
  ]
  edge [
    source 15
    target 443
  ]
  edge [
    source 15
    target 444
  ]
  edge [
    source 15
    target 445
  ]
  edge [
    source 15
    target 446
  ]
  edge [
    source 15
    target 447
  ]
  edge [
    source 15
    target 448
  ]
  edge [
    source 15
    target 449
  ]
  edge [
    source 15
    target 427
  ]
  edge [
    source 15
    target 450
  ]
  edge [
    source 15
    target 451
  ]
  edge [
    source 15
    target 452
  ]
  edge [
    source 15
    target 453
  ]
  edge [
    source 15
    target 418
  ]
  edge [
    source 15
    target 454
  ]
  edge [
    source 15
    target 455
  ]
  edge [
    source 15
    target 456
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 457
  ]
  edge [
    source 16
    target 458
  ]
  edge [
    source 16
    target 459
  ]
  edge [
    source 16
    target 460
  ]
  edge [
    source 16
    target 461
  ]
  edge [
    source 16
    target 462
  ]
  edge [
    source 16
    target 463
  ]
  edge [
    source 16
    target 464
  ]
  edge [
    source 16
    target 465
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 466
  ]
  edge [
    source 17
    target 467
  ]
  edge [
    source 17
    target 468
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 470
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 472
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 17
    target 474
  ]
  edge [
    source 17
    target 475
  ]
  edge [
    source 17
    target 476
  ]
  edge [
    source 17
    target 477
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 479
  ]
  edge [
    source 17
    target 480
  ]
  edge [
    source 17
    target 481
  ]
  edge [
    source 17
    target 482
  ]
  edge [
    source 17
    target 483
  ]
  edge [
    source 17
    target 484
  ]
  edge [
    source 17
    target 485
  ]
  edge [
    source 17
    target 486
  ]
  edge [
    source 17
    target 487
  ]
  edge [
    source 17
    target 488
  ]
  edge [
    source 17
    target 489
  ]
  edge [
    source 17
    target 490
  ]
  edge [
    source 17
    target 491
  ]
  edge [
    source 17
    target 492
  ]
  edge [
    source 18
    target 493
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 495
  ]
  edge [
    source 496
    target 497
  ]
]
