graph [
  maxDegree 17
  minDegree 1
  meanDegree 2.0504201680672267
  density 0.017376442102264633
  graphCliqueNumber 6
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "czo&#322;&#243;wka"
    origin "text"
  ]
  node [
    id 2
    label "znana"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zaawansowana"
    origin "text"
  ]
  node [
    id 5
    label "animacja"
    origin "text"
  ]
  node [
    id 6
    label "przystawa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "lata"
    origin "text"
  ]
  node [
    id 8
    label "kogut"
    origin "text"
  ]
  node [
    id 9
    label "milicja"
    origin "text"
  ]
  node [
    id 10
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 11
    label "symbol"
    origin "text"
  ]
  node [
    id 12
    label "audycja"
    origin "text"
  ]
  node [
    id 13
    label "godzina"
  ]
  node [
    id 14
    label "alpinizm"
  ]
  node [
    id 15
    label "materia&#322;"
  ]
  node [
    id 16
    label "&#347;ciana"
  ]
  node [
    id 17
    label "grupa"
  ]
  node [
    id 18
    label "film"
  ]
  node [
    id 19
    label "rajd"
  ]
  node [
    id 20
    label "bieg"
  ]
  node [
    id 21
    label "poligrafia"
  ]
  node [
    id 22
    label "front"
  ]
  node [
    id 23
    label "rz&#261;d"
  ]
  node [
    id 24
    label "pododdzia&#322;"
  ]
  node [
    id 25
    label "wst&#281;p"
  ]
  node [
    id 26
    label "zderzenie"
  ]
  node [
    id 27
    label "elita"
  ]
  node [
    id 28
    label "latarka_czo&#322;owa"
  ]
  node [
    id 29
    label "si&#281;ga&#263;"
  ]
  node [
    id 30
    label "trwa&#263;"
  ]
  node [
    id 31
    label "obecno&#347;&#263;"
  ]
  node [
    id 32
    label "stan"
  ]
  node [
    id 33
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 34
    label "stand"
  ]
  node [
    id 35
    label "mie&#263;_miejsce"
  ]
  node [
    id 36
    label "uczestniczy&#263;"
  ]
  node [
    id 37
    label "chodzi&#263;"
  ]
  node [
    id 38
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 39
    label "equal"
  ]
  node [
    id 40
    label "morfing"
  ]
  node [
    id 41
    label "technika"
  ]
  node [
    id 42
    label "boost"
  ]
  node [
    id 43
    label "pomoc"
  ]
  node [
    id 44
    label "Pok&#233;mon"
  ]
  node [
    id 45
    label "wchodzi&#263;"
  ]
  node [
    id 46
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 47
    label "authorize"
  ]
  node [
    id 48
    label "submit"
  ]
  node [
    id 49
    label "tone"
  ]
  node [
    id 50
    label "przylega&#263;"
  ]
  node [
    id 51
    label "uznawa&#263;"
  ]
  node [
    id 52
    label "summer"
  ]
  node [
    id 53
    label "czas"
  ]
  node [
    id 54
    label "zapia&#263;"
  ]
  node [
    id 55
    label "zapianie"
  ]
  node [
    id 56
    label "zadziora"
  ]
  node [
    id 57
    label "sygna&#322;"
  ]
  node [
    id 58
    label "ko&#324;skie_zaloty"
  ]
  node [
    id 59
    label "samiec"
  ]
  node [
    id 60
    label "kurczak"
  ]
  node [
    id 61
    label "&#347;wintuch"
  ]
  node [
    id 62
    label "ryzykant"
  ]
  node [
    id 63
    label "lampa"
  ]
  node [
    id 64
    label "uwodziciel"
  ]
  node [
    id 65
    label "w&#322;osy"
  ]
  node [
    id 66
    label "kura"
  ]
  node [
    id 67
    label "pia&#263;"
  ]
  node [
    id 68
    label "pianie"
  ]
  node [
    id 69
    label "armia"
  ]
  node [
    id 70
    label "formacja_paramilitarna"
  ]
  node [
    id 71
    label "policja"
  ]
  node [
    id 72
    label "doba"
  ]
  node [
    id 73
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 74
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 75
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 76
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 77
    label "symbolizowanie"
  ]
  node [
    id 78
    label "wcielenie"
  ]
  node [
    id 79
    label "notacja"
  ]
  node [
    id 80
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 81
    label "znak_pisarski"
  ]
  node [
    id 82
    label "znak"
  ]
  node [
    id 83
    label "character"
  ]
  node [
    id 84
    label "program"
  ]
  node [
    id 85
    label "magazyn"
  ]
  node [
    id 86
    label "kryminalny"
  ]
  node [
    id 87
    label "997"
  ]
  node [
    id 88
    label "Jan"
  ]
  node [
    id 89
    label "P&#322;&#243;cienniczak"
  ]
  node [
    id 90
    label "bernard"
  ]
  node [
    id 91
    label "Margueritte"
  ]
  node [
    id 92
    label "zalesie"
  ]
  node [
    id 93
    label "g&#243;rny"
  ]
  node [
    id 94
    label "da&#263;"
  ]
  node [
    id 95
    label "&#380;y&#263;"
  ]
  node [
    id 96
    label "ojciec"
  ]
  node [
    id 97
    label "ludzie"
  ]
  node [
    id 98
    label "zaginiony"
  ]
  node [
    id 99
    label "komendant"
  ]
  node [
    id 100
    label "g&#322;&#243;wny"
  ]
  node [
    id 101
    label "Zenon"
  ]
  node [
    id 102
    label "Smolarek"
  ]
  node [
    id 103
    label "Micha&#322;"
  ]
  node [
    id 104
    label "Fajbusiewicz"
  ]
  node [
    id 105
    label "TVP"
  ]
  node [
    id 106
    label "info"
  ]
  node [
    id 107
    label "2"
  ]
  node [
    id 108
    label "mi"
  ]
  node [
    id 109
    label "8T"
  ]
  node [
    id 110
    label "103"
  ]
  node [
    id 111
    label "pu&#322;k"
  ]
  node [
    id 112
    label "lotniczy"
  ]
  node [
    id 113
    label "ministerstwo"
  ]
  node [
    id 114
    label "sprawi&#263;"
  ]
  node [
    id 115
    label "wewn&#281;trzny"
  ]
  node [
    id 116
    label "nadwi&#347;la&#324;ski"
  ]
  node [
    id 117
    label "jednostka"
  ]
  node [
    id 118
    label "wojskowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 82
    target 94
  ]
  edge [
    source 82
    target 95
  ]
  edge [
    source 82
    target 96
  ]
  edge [
    source 82
    target 97
  ]
  edge [
    source 82
    target 98
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 87
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 96
  ]
  edge [
    source 94
    target 97
  ]
  edge [
    source 94
    target 98
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 97
  ]
  edge [
    source 95
    target 98
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 98
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 107
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 112
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 115
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 118
  ]
  edge [
    source 117
    target 118
  ]
]
