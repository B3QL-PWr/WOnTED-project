graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.07384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "dzienia"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "wykopki"
    origin "text"
  ]
  node [
    id 3
    label "pomy&#347;lny"
  ]
  node [
    id 4
    label "skuteczny"
  ]
  node [
    id 5
    label "moralny"
  ]
  node [
    id 6
    label "korzystny"
  ]
  node [
    id 7
    label "odpowiedni"
  ]
  node [
    id 8
    label "zwrot"
  ]
  node [
    id 9
    label "dobrze"
  ]
  node [
    id 10
    label "pozytywny"
  ]
  node [
    id 11
    label "grzeczny"
  ]
  node [
    id 12
    label "powitanie"
  ]
  node [
    id 13
    label "mi&#322;y"
  ]
  node [
    id 14
    label "dobroczynny"
  ]
  node [
    id 15
    label "pos&#322;uszny"
  ]
  node [
    id 16
    label "ca&#322;y"
  ]
  node [
    id 17
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 18
    label "czw&#243;rka"
  ]
  node [
    id 19
    label "spokojny"
  ]
  node [
    id 20
    label "&#347;mieszny"
  ]
  node [
    id 21
    label "drogi"
  ]
  node [
    id 22
    label "zbi&#243;r"
  ]
  node [
    id 23
    label "sadzeniak"
  ]
  node [
    id 24
    label "monstera"
  ]
  node [
    id 25
    label "hunter"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 24
    target 25
  ]
]
