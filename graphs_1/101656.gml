graph [
  maxDegree 152
  minDegree 1
  meanDegree 2
  density 0.010582010582010581
  graphCliqueNumber 3
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "jerzy"
    origin "text"
  ]
  node [
    id 2
    label "kozdro&#324;"
    origin "text"
  ]
  node [
    id 3
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 4
    label "wszyscy"
    origin "text"
  ]
  node [
    id 5
    label "znak"
    origin "text"
  ]
  node [
    id 6
    label "niebo"
    origin "text"
  ]
  node [
    id 7
    label "ziemia"
    origin "text"
  ]
  node [
    id 8
    label "dyplomata"
  ]
  node [
    id 9
    label "wys&#322;annik"
  ]
  node [
    id 10
    label "przedstawiciel"
  ]
  node [
    id 11
    label "kurier_dyplomatyczny"
  ]
  node [
    id 12
    label "ablegat"
  ]
  node [
    id 13
    label "klubista"
  ]
  node [
    id 14
    label "Miko&#322;ajczyk"
  ]
  node [
    id 15
    label "Korwin"
  ]
  node [
    id 16
    label "parlamentarzysta"
  ]
  node [
    id 17
    label "dyscyplina_partyjna"
  ]
  node [
    id 18
    label "izba_ni&#380;sza"
  ]
  node [
    id 19
    label "poselstwo"
  ]
  node [
    id 20
    label "postawi&#263;"
  ]
  node [
    id 21
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 22
    label "wytw&#243;r"
  ]
  node [
    id 23
    label "implikowa&#263;"
  ]
  node [
    id 24
    label "stawia&#263;"
  ]
  node [
    id 25
    label "mark"
  ]
  node [
    id 26
    label "kodzik"
  ]
  node [
    id 27
    label "attribute"
  ]
  node [
    id 28
    label "dow&#243;d"
  ]
  node [
    id 29
    label "herb"
  ]
  node [
    id 30
    label "fakt"
  ]
  node [
    id 31
    label "oznakowanie"
  ]
  node [
    id 32
    label "point"
  ]
  node [
    id 33
    label "Waruna"
  ]
  node [
    id 34
    label "przestrze&#324;"
  ]
  node [
    id 35
    label "za&#347;wiaty"
  ]
  node [
    id 36
    label "zodiak"
  ]
  node [
    id 37
    label "znak_zodiaku"
  ]
  node [
    id 38
    label "bezchmurno&#347;&#263;"
  ]
  node [
    id 39
    label "Kr&#243;lestwo_Niebieskie"
  ]
  node [
    id 40
    label "Skandynawia"
  ]
  node [
    id 41
    label "Yorkshire"
  ]
  node [
    id 42
    label "Kaukaz"
  ]
  node [
    id 43
    label "Kaszmir"
  ]
  node [
    id 44
    label "Toskania"
  ]
  node [
    id 45
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 46
    label "&#321;emkowszczyzna"
  ]
  node [
    id 47
    label "obszar"
  ]
  node [
    id 48
    label "Amhara"
  ]
  node [
    id 49
    label "Lombardia"
  ]
  node [
    id 50
    label "Podbeskidzie"
  ]
  node [
    id 51
    label "Kalabria"
  ]
  node [
    id 52
    label "kort"
  ]
  node [
    id 53
    label "Tyrol"
  ]
  node [
    id 54
    label "Pamir"
  ]
  node [
    id 55
    label "Lubelszczyzna"
  ]
  node [
    id 56
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 57
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 58
    label "&#379;ywiecczyzna"
  ]
  node [
    id 59
    label "ryzosfera"
  ]
  node [
    id 60
    label "Europa_Wschodnia"
  ]
  node [
    id 61
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 62
    label "Zabajkale"
  ]
  node [
    id 63
    label "Kaszuby"
  ]
  node [
    id 64
    label "Bo&#347;nia"
  ]
  node [
    id 65
    label "Noworosja"
  ]
  node [
    id 66
    label "Ba&#322;kany"
  ]
  node [
    id 67
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 68
    label "Anglia"
  ]
  node [
    id 69
    label "Kielecczyzna"
  ]
  node [
    id 70
    label "Pomorze_Zachodnie"
  ]
  node [
    id 71
    label "Opolskie"
  ]
  node [
    id 72
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 73
    label "skorupa_ziemska"
  ]
  node [
    id 74
    label "Ko&#322;yma"
  ]
  node [
    id 75
    label "Oksytania"
  ]
  node [
    id 76
    label "Syjon"
  ]
  node [
    id 77
    label "posadzka"
  ]
  node [
    id 78
    label "pa&#324;stwo"
  ]
  node [
    id 79
    label "Kociewie"
  ]
  node [
    id 80
    label "Huculszczyzna"
  ]
  node [
    id 81
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 82
    label "budynek"
  ]
  node [
    id 83
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 84
    label "Bawaria"
  ]
  node [
    id 85
    label "pomieszczenie"
  ]
  node [
    id 86
    label "pr&#243;chnica"
  ]
  node [
    id 87
    label "glinowanie"
  ]
  node [
    id 88
    label "Maghreb"
  ]
  node [
    id 89
    label "Bory_Tucholskie"
  ]
  node [
    id 90
    label "Europa_Zachodnia"
  ]
  node [
    id 91
    label "Kerala"
  ]
  node [
    id 92
    label "Podhale"
  ]
  node [
    id 93
    label "Kabylia"
  ]
  node [
    id 94
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 95
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 96
    label "Ma&#322;opolska"
  ]
  node [
    id 97
    label "Polesie"
  ]
  node [
    id 98
    label "Liguria"
  ]
  node [
    id 99
    label "&#321;&#243;dzkie"
  ]
  node [
    id 100
    label "geosystem"
  ]
  node [
    id 101
    label "Palestyna"
  ]
  node [
    id 102
    label "Bojkowszczyzna"
  ]
  node [
    id 103
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 104
    label "Karaiby"
  ]
  node [
    id 105
    label "S&#261;decczyzna"
  ]
  node [
    id 106
    label "Sand&#380;ak"
  ]
  node [
    id 107
    label "Nadrenia"
  ]
  node [
    id 108
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 109
    label "Zakarpacie"
  ]
  node [
    id 110
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 111
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 112
    label "Zag&#243;rze"
  ]
  node [
    id 113
    label "Andaluzja"
  ]
  node [
    id 114
    label "Turkiestan"
  ]
  node [
    id 115
    label "Naddniestrze"
  ]
  node [
    id 116
    label "Hercegowina"
  ]
  node [
    id 117
    label "p&#322;aszczyzna"
  ]
  node [
    id 118
    label "Opolszczyzna"
  ]
  node [
    id 119
    label "jednostka_administracyjna"
  ]
  node [
    id 120
    label "Lotaryngia"
  ]
  node [
    id 121
    label "Afryka_Wschodnia"
  ]
  node [
    id 122
    label "Szlezwik"
  ]
  node [
    id 123
    label "powierzchnia"
  ]
  node [
    id 124
    label "glinowa&#263;"
  ]
  node [
    id 125
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 126
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 127
    label "podglebie"
  ]
  node [
    id 128
    label "Mazowsze"
  ]
  node [
    id 129
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 130
    label "teren"
  ]
  node [
    id 131
    label "Afryka_Zachodnia"
  ]
  node [
    id 132
    label "czynnik_produkcji"
  ]
  node [
    id 133
    label "Galicja"
  ]
  node [
    id 134
    label "Szkocja"
  ]
  node [
    id 135
    label "Walia"
  ]
  node [
    id 136
    label "Powi&#347;le"
  ]
  node [
    id 137
    label "penetrator"
  ]
  node [
    id 138
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 139
    label "kompleks_sorpcyjny"
  ]
  node [
    id 140
    label "Zamojszczyzna"
  ]
  node [
    id 141
    label "Kujawy"
  ]
  node [
    id 142
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 143
    label "Podlasie"
  ]
  node [
    id 144
    label "Laponia"
  ]
  node [
    id 145
    label "Umbria"
  ]
  node [
    id 146
    label "plantowa&#263;"
  ]
  node [
    id 147
    label "Mezoameryka"
  ]
  node [
    id 148
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 149
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 150
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 151
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 152
    label "Kurdystan"
  ]
  node [
    id 153
    label "Kampania"
  ]
  node [
    id 154
    label "Armagnac"
  ]
  node [
    id 155
    label "Polinezja"
  ]
  node [
    id 156
    label "Warmia"
  ]
  node [
    id 157
    label "Wielkopolska"
  ]
  node [
    id 158
    label "litosfera"
  ]
  node [
    id 159
    label "Bordeaux"
  ]
  node [
    id 160
    label "Lauda"
  ]
  node [
    id 161
    label "Mazury"
  ]
  node [
    id 162
    label "Podkarpacie"
  ]
  node [
    id 163
    label "Oceania"
  ]
  node [
    id 164
    label "Lasko"
  ]
  node [
    id 165
    label "Amazonia"
  ]
  node [
    id 166
    label "pojazd"
  ]
  node [
    id 167
    label "glej"
  ]
  node [
    id 168
    label "martwica"
  ]
  node [
    id 169
    label "zapadnia"
  ]
  node [
    id 170
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 171
    label "dotleni&#263;"
  ]
  node [
    id 172
    label "Kurpie"
  ]
  node [
    id 173
    label "Tonkin"
  ]
  node [
    id 174
    label "Azja_Wschodnia"
  ]
  node [
    id 175
    label "Mikronezja"
  ]
  node [
    id 176
    label "Ukraina_Zachodnia"
  ]
  node [
    id 177
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 178
    label "Turyngia"
  ]
  node [
    id 179
    label "Baszkiria"
  ]
  node [
    id 180
    label "Apulia"
  ]
  node [
    id 181
    label "miejsce"
  ]
  node [
    id 182
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 183
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 184
    label "Indochiny"
  ]
  node [
    id 185
    label "Biskupizna"
  ]
  node [
    id 186
    label "Lubuskie"
  ]
  node [
    id 187
    label "domain"
  ]
  node [
    id 188
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 189
    label "Ameryka_&#321;aci&#324;ska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
]
