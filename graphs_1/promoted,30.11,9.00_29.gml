graph [
  maxDegree 51
  minDegree 1
  meanDegree 1.9813084112149533
  density 0.018691588785046728
  graphCliqueNumber 3
  node [
    id 0
    label "zabytkowy"
    origin "text"
  ]
  node [
    id 1
    label "nagranie"
    origin "text"
  ]
  node [
    id 2
    label "ukazowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "alpinista"
    origin "text"
  ]
  node [
    id 4
    label "zdobywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "szczyt"
    origin "text"
  ]
  node [
    id 6
    label "dent"
    origin "text"
  ]
  node [
    id 7
    label "geant"
    origin "text"
  ]
  node [
    id 8
    label "uznawa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "jeden"
    origin "text"
  ]
  node [
    id 10
    label "trudny"
    origin "text"
  ]
  node [
    id 11
    label "technicznie"
    origin "text"
  ]
  node [
    id 12
    label "czterotysi&#281;cznik&#243;w"
    origin "text"
  ]
  node [
    id 13
    label "alpy"
    origin "text"
  ]
  node [
    id 14
    label "stary"
  ]
  node [
    id 15
    label "zabytkowo"
  ]
  node [
    id 16
    label "cenny"
  ]
  node [
    id 17
    label "staromodny"
  ]
  node [
    id 18
    label "wys&#322;uchanie"
  ]
  node [
    id 19
    label "wytw&#243;r"
  ]
  node [
    id 20
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 21
    label "recording"
  ]
  node [
    id 22
    label "utrwalenie"
  ]
  node [
    id 23
    label "wspinacz"
  ]
  node [
    id 24
    label "uzyskiwa&#263;"
  ]
  node [
    id 25
    label "dostawa&#263;"
  ]
  node [
    id 26
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 27
    label "tease"
  ]
  node [
    id 28
    label "have"
  ]
  node [
    id 29
    label "niewoli&#263;"
  ]
  node [
    id 30
    label "robi&#263;"
  ]
  node [
    id 31
    label "raise"
  ]
  node [
    id 32
    label "Lubogoszcz"
  ]
  node [
    id 33
    label "koniec"
  ]
  node [
    id 34
    label "wierch"
  ]
  node [
    id 35
    label "czas"
  ]
  node [
    id 36
    label "&#321;omnica"
  ]
  node [
    id 37
    label "Magura"
  ]
  node [
    id 38
    label "Wo&#322;ek"
  ]
  node [
    id 39
    label "Wielki_Chocz"
  ]
  node [
    id 40
    label "Turbacz"
  ]
  node [
    id 41
    label "Walig&#243;ra"
  ]
  node [
    id 42
    label "Orlica"
  ]
  node [
    id 43
    label "korona"
  ]
  node [
    id 44
    label "bok"
  ]
  node [
    id 45
    label "Jaworzyna"
  ]
  node [
    id 46
    label "Groniczki"
  ]
  node [
    id 47
    label "Radunia"
  ]
  node [
    id 48
    label "&#346;winica"
  ]
  node [
    id 49
    label "Okr&#261;glica"
  ]
  node [
    id 50
    label "Beskid"
  ]
  node [
    id 51
    label "poziom"
  ]
  node [
    id 52
    label "wzmo&#380;enie"
  ]
  node [
    id 53
    label "Czupel"
  ]
  node [
    id 54
    label "fasada"
  ]
  node [
    id 55
    label "Rysianka"
  ]
  node [
    id 56
    label "g&#243;ra"
  ]
  node [
    id 57
    label "Jaworz"
  ]
  node [
    id 58
    label "Rudawiec"
  ]
  node [
    id 59
    label "Che&#322;miec"
  ]
  node [
    id 60
    label "zwie&#324;czenie"
  ]
  node [
    id 61
    label "Wielki_Bukowiec"
  ]
  node [
    id 62
    label "wzniesienie"
  ]
  node [
    id 63
    label "godzina_szczytu"
  ]
  node [
    id 64
    label "summit"
  ]
  node [
    id 65
    label "Wielka_Racza"
  ]
  node [
    id 66
    label "wierzcho&#322;"
  ]
  node [
    id 67
    label "&#346;nie&#380;nik"
  ]
  node [
    id 68
    label "&#347;ciana"
  ]
  node [
    id 69
    label "Barania_G&#243;ra"
  ]
  node [
    id 70
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 71
    label "Ja&#322;owiec"
  ]
  node [
    id 72
    label "Wielka_Sowa"
  ]
  node [
    id 73
    label "wierzcho&#322;ek"
  ]
  node [
    id 74
    label "Obidowa"
  ]
  node [
    id 75
    label "konferencja"
  ]
  node [
    id 76
    label "Cubryna"
  ]
  node [
    id 77
    label "Szrenica"
  ]
  node [
    id 78
    label "Czarna_G&#243;ra"
  ]
  node [
    id 79
    label "Mody&#324;"
  ]
  node [
    id 80
    label "&#346;l&#281;&#380;a"
  ]
  node [
    id 81
    label "consider"
  ]
  node [
    id 82
    label "os&#261;dza&#263;"
  ]
  node [
    id 83
    label "stwierdza&#263;"
  ]
  node [
    id 84
    label "notice"
  ]
  node [
    id 85
    label "przyznawa&#263;"
  ]
  node [
    id 86
    label "kieliszek"
  ]
  node [
    id 87
    label "shot"
  ]
  node [
    id 88
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 89
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 90
    label "jaki&#347;"
  ]
  node [
    id 91
    label "jednolicie"
  ]
  node [
    id 92
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 93
    label "w&#243;dka"
  ]
  node [
    id 94
    label "ten"
  ]
  node [
    id 95
    label "ujednolicenie"
  ]
  node [
    id 96
    label "jednakowy"
  ]
  node [
    id 97
    label "wymagaj&#261;cy"
  ]
  node [
    id 98
    label "skomplikowany"
  ]
  node [
    id 99
    label "k&#322;opotliwy"
  ]
  node [
    id 100
    label "ci&#281;&#380;ko"
  ]
  node [
    id 101
    label "technically"
  ]
  node [
    id 102
    label "techniczny"
  ]
  node [
    id 103
    label "sucho"
  ]
  node [
    id 104
    label "Dent"
  ]
  node [
    id 105
    label "du"
  ]
  node [
    id 106
    label "Geant"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 106
  ]
  edge [
    source 105
    target 106
  ]
]
