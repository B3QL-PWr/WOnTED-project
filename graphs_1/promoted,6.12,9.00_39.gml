graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8095238095238095
  density 0.09047619047619047
  graphCliqueNumber 2
  node [
    id 0
    label "dominik"
    origin "text"
  ]
  node [
    id 1
    label "olszewski"
    origin "text"
  ]
  node [
    id 2
    label "zalega&#263;"
    origin "text"
  ]
  node [
    id 3
    label "abonament"
    origin "text"
  ]
  node [
    id 4
    label "rtv"
    origin "text"
  ]
  node [
    id 5
    label "cover"
  ]
  node [
    id 6
    label "pozostawa&#263;"
  ]
  node [
    id 7
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 8
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 9
    label "screen"
  ]
  node [
    id 10
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 11
    label "wype&#322;nia&#263;"
  ]
  node [
    id 12
    label "front"
  ]
  node [
    id 13
    label "z&#322;o&#380;e"
  ]
  node [
    id 14
    label "season"
  ]
  node [
    id 15
    label "przedp&#322;ata"
  ]
  node [
    id 16
    label "bloczek"
  ]
  node [
    id 17
    label "elektronika"
  ]
  node [
    id 18
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 19
    label "Dominik"
  ]
  node [
    id 20
    label "Olszewski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 19
    target 20
  ]
]
