graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.971830985915493
  density 0.028169014084507043
  graphCliqueNumber 2
  node [
    id 0
    label "policjant"
    origin "text"
  ]
  node [
    id 1
    label "odnale&#378;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "srebrny"
    origin "text"
  ]
  node [
    id 3
    label "lexus"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "kierowca"
    origin "text"
  ]
  node [
    id 6
    label "miniony"
    origin "text"
  ]
  node [
    id 7
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 8
    label "krajowy"
    origin "text"
  ]
  node [
    id 9
    label "szesnastka"
    origin "text"
  ]
  node [
    id 10
    label "warmi&#324;sko"
    origin "text"
  ]
  node [
    id 11
    label "mazurskiem"
    origin "text"
  ]
  node [
    id 12
    label "&#347;miertelnie"
    origin "text"
  ]
  node [
    id 13
    label "potr&#261;ci&#263;"
    origin "text"
  ]
  node [
    id 14
    label "latko"
    origin "text"
  ]
  node [
    id 15
    label "policja"
  ]
  node [
    id 16
    label "blacharz"
  ]
  node [
    id 17
    label "pa&#322;a"
  ]
  node [
    id 18
    label "mundurowy"
  ]
  node [
    id 19
    label "str&#243;&#380;"
  ]
  node [
    id 20
    label "glina"
  ]
  node [
    id 21
    label "odzyska&#263;"
  ]
  node [
    id 22
    label "zauwa&#380;y&#263;"
  ]
  node [
    id 23
    label "wybra&#263;"
  ]
  node [
    id 24
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 25
    label "znale&#378;&#263;"
  ]
  node [
    id 26
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 27
    label "discover"
  ]
  node [
    id 28
    label "znaj&#347;&#263;"
  ]
  node [
    id 29
    label "devise"
  ]
  node [
    id 30
    label "metaliczny"
  ]
  node [
    id 31
    label "szary"
  ]
  node [
    id 32
    label "jasny"
  ]
  node [
    id 33
    label "utytu&#322;owany"
  ]
  node [
    id 34
    label "srebrzenie_si&#281;"
  ]
  node [
    id 35
    label "srebrno"
  ]
  node [
    id 36
    label "posrebrzenie"
  ]
  node [
    id 37
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 38
    label "srebrzenie"
  ]
  node [
    id 39
    label "srebrzy&#347;cie"
  ]
  node [
    id 40
    label "Lexus"
  ]
  node [
    id 41
    label "japo&#324;czyk"
  ]
  node [
    id 42
    label "cz&#322;owiek"
  ]
  node [
    id 43
    label "transportowiec"
  ]
  node [
    id 44
    label "ostatni"
  ]
  node [
    id 45
    label "dawny"
  ]
  node [
    id 46
    label "przesz&#322;y"
  ]
  node [
    id 47
    label "dzie&#324;_powszedni"
  ]
  node [
    id 48
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 49
    label "tydzie&#324;"
  ]
  node [
    id 50
    label "rodzimy"
  ]
  node [
    id 51
    label "podlotek"
  ]
  node [
    id 52
    label "nuta"
  ]
  node [
    id 53
    label "panna"
  ]
  node [
    id 54
    label "obiekt"
  ]
  node [
    id 55
    label "zbi&#243;r"
  ]
  node [
    id 56
    label "liczba"
  ]
  node [
    id 57
    label "format_arkusza"
  ]
  node [
    id 58
    label "urodziny"
  ]
  node [
    id 59
    label "tragicznie"
  ]
  node [
    id 60
    label "niebezpiecznie"
  ]
  node [
    id 61
    label "&#347;miertelny"
  ]
  node [
    id 62
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 63
    label "strasznie"
  ]
  node [
    id 64
    label "zaciekle"
  ]
  node [
    id 65
    label "precipitate"
  ]
  node [
    id 66
    label "allude"
  ]
  node [
    id 67
    label "odliczy&#263;"
  ]
  node [
    id 68
    label "uderzy&#263;"
  ]
  node [
    id 69
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 70
    label "smell"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
]
