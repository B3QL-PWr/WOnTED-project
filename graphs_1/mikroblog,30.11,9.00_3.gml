graph [
  maxDegree 43
  minDegree 1
  meanDegree 2.0631578947368423
  density 0.0219484882418813
  graphCliqueNumber 3
  node [
    id 0
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "pierwszy"
    origin "text"
  ]
  node [
    id 2
    label "raz"
    origin "text"
  ]
  node [
    id 3
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 4
    label "pokazmorde"
    origin "text"
  ]
  node [
    id 5
    label "chwila"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "historyczny"
    origin "text"
  ]
  node [
    id 8
    label "tentegowa&#263;"
  ]
  node [
    id 9
    label "urz&#261;dza&#263;"
  ]
  node [
    id 10
    label "give"
  ]
  node [
    id 11
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 12
    label "czyni&#263;"
  ]
  node [
    id 13
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 14
    label "post&#281;powa&#263;"
  ]
  node [
    id 15
    label "wydala&#263;"
  ]
  node [
    id 16
    label "oszukiwa&#263;"
  ]
  node [
    id 17
    label "organizowa&#263;"
  ]
  node [
    id 18
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 19
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 20
    label "work"
  ]
  node [
    id 21
    label "przerabia&#263;"
  ]
  node [
    id 22
    label "stylizowa&#263;"
  ]
  node [
    id 23
    label "falowa&#263;"
  ]
  node [
    id 24
    label "act"
  ]
  node [
    id 25
    label "peddle"
  ]
  node [
    id 26
    label "ukazywa&#263;"
  ]
  node [
    id 27
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 28
    label "praca"
  ]
  node [
    id 29
    label "najwa&#380;niejszy"
  ]
  node [
    id 30
    label "pocz&#261;tkowy"
  ]
  node [
    id 31
    label "dobry"
  ]
  node [
    id 32
    label "ch&#281;tny"
  ]
  node [
    id 33
    label "dzie&#324;"
  ]
  node [
    id 34
    label "pr&#281;dki"
  ]
  node [
    id 35
    label "uderzenie"
  ]
  node [
    id 36
    label "cios"
  ]
  node [
    id 37
    label "time"
  ]
  node [
    id 38
    label "energy"
  ]
  node [
    id 39
    label "czas"
  ]
  node [
    id 40
    label "bycie"
  ]
  node [
    id 41
    label "zegar_biologiczny"
  ]
  node [
    id 42
    label "okres_noworodkowy"
  ]
  node [
    id 43
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 44
    label "entity"
  ]
  node [
    id 45
    label "prze&#380;ywanie"
  ]
  node [
    id 46
    label "prze&#380;ycie"
  ]
  node [
    id 47
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 48
    label "wiek_matuzalemowy"
  ]
  node [
    id 49
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 50
    label "dzieci&#324;stwo"
  ]
  node [
    id 51
    label "power"
  ]
  node [
    id 52
    label "szwung"
  ]
  node [
    id 53
    label "menopauza"
  ]
  node [
    id 54
    label "umarcie"
  ]
  node [
    id 55
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 56
    label "life"
  ]
  node [
    id 57
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 58
    label "&#380;ywy"
  ]
  node [
    id 59
    label "rozw&#243;j"
  ]
  node [
    id 60
    label "po&#322;&#243;g"
  ]
  node [
    id 61
    label "byt"
  ]
  node [
    id 62
    label "przebywanie"
  ]
  node [
    id 63
    label "subsistence"
  ]
  node [
    id 64
    label "koleje_losu"
  ]
  node [
    id 65
    label "raj_utracony"
  ]
  node [
    id 66
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 67
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 68
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 69
    label "andropauza"
  ]
  node [
    id 70
    label "warunki"
  ]
  node [
    id 71
    label "do&#380;ywanie"
  ]
  node [
    id 72
    label "niemowl&#281;ctwo"
  ]
  node [
    id 73
    label "umieranie"
  ]
  node [
    id 74
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 75
    label "staro&#347;&#263;"
  ]
  node [
    id 76
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 77
    label "&#347;mier&#263;"
  ]
  node [
    id 78
    label "si&#281;ga&#263;"
  ]
  node [
    id 79
    label "trwa&#263;"
  ]
  node [
    id 80
    label "obecno&#347;&#263;"
  ]
  node [
    id 81
    label "stan"
  ]
  node [
    id 82
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 83
    label "stand"
  ]
  node [
    id 84
    label "mie&#263;_miejsce"
  ]
  node [
    id 85
    label "uczestniczy&#263;"
  ]
  node [
    id 86
    label "chodzi&#263;"
  ]
  node [
    id 87
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 88
    label "equal"
  ]
  node [
    id 89
    label "dawny"
  ]
  node [
    id 90
    label "zgodny"
  ]
  node [
    id 91
    label "historycznie"
  ]
  node [
    id 92
    label "wiekopomny"
  ]
  node [
    id 93
    label "prawdziwy"
  ]
  node [
    id 94
    label "dziejowo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
]
