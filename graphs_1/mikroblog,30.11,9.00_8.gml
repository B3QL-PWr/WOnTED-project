graph [
  maxDegree 31
  minDegree 1
  meanDegree 1.9473684210526316
  density 0.05263157894736842
  graphCliqueNumber 2
  node [
    id 0
    label "dzien"
    origin "text"
  ]
  node [
    id 1
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "wcionz"
    origin "text"
  ]
  node [
    id 3
    label "dzieje"
    origin "text"
  ]
  node [
    id 4
    label "wyspa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "cuuud"
    origin "text"
  ]
  node [
    id 6
    label "s&#322;o&#324;ce"
  ]
  node [
    id 7
    label "czynienie_si&#281;"
  ]
  node [
    id 8
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 9
    label "czas"
  ]
  node [
    id 10
    label "long_time"
  ]
  node [
    id 11
    label "przedpo&#322;udnie"
  ]
  node [
    id 12
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 13
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 14
    label "tydzie&#324;"
  ]
  node [
    id 15
    label "godzina"
  ]
  node [
    id 16
    label "t&#322;usty_czwartek"
  ]
  node [
    id 17
    label "wsta&#263;"
  ]
  node [
    id 18
    label "day"
  ]
  node [
    id 19
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 20
    label "przedwiecz&#243;r"
  ]
  node [
    id 21
    label "Sylwester"
  ]
  node [
    id 22
    label "po&#322;udnie"
  ]
  node [
    id 23
    label "wzej&#347;cie"
  ]
  node [
    id 24
    label "podwiecz&#243;r"
  ]
  node [
    id 25
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 26
    label "rano"
  ]
  node [
    id 27
    label "termin"
  ]
  node [
    id 28
    label "ranek"
  ]
  node [
    id 29
    label "doba"
  ]
  node [
    id 30
    label "wiecz&#243;r"
  ]
  node [
    id 31
    label "walentynki"
  ]
  node [
    id 32
    label "popo&#322;udnie"
  ]
  node [
    id 33
    label "noc"
  ]
  node [
    id 34
    label "wstanie"
  ]
  node [
    id 35
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 36
    label "epoka"
  ]
  node [
    id 37
    label "przespa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
]
