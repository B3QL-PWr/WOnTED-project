graph [
  maxDegree 40
  minDegree 1
  meanDegree 2
  density 0.019230769230769232
  graphCliqueNumber 2
  node [
    id 0
    label "letni"
    origin "text"
  ]
  node [
    id 1
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 2
    label "powiesi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "gdy"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "gn&#281;bi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przez"
    origin "text"
  ]
  node [
    id 8
    label "azjatycki"
    origin "text"
  ]
  node [
    id 9
    label "gang"
    origin "text"
  ]
  node [
    id 10
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 11
    label "nijaki"
  ]
  node [
    id 12
    label "sezonowy"
  ]
  node [
    id 13
    label "letnio"
  ]
  node [
    id 14
    label "s&#322;oneczny"
  ]
  node [
    id 15
    label "weso&#322;y"
  ]
  node [
    id 16
    label "oboj&#281;tny"
  ]
  node [
    id 17
    label "latowy"
  ]
  node [
    id 18
    label "ciep&#322;y"
  ]
  node [
    id 19
    label "typowy"
  ]
  node [
    id 20
    label "cz&#322;owiek"
  ]
  node [
    id 21
    label "pomocnik"
  ]
  node [
    id 22
    label "g&#243;wniarz"
  ]
  node [
    id 23
    label "&#347;l&#261;ski"
  ]
  node [
    id 24
    label "m&#322;odzieniec"
  ]
  node [
    id 25
    label "kajtek"
  ]
  node [
    id 26
    label "kawaler"
  ]
  node [
    id 27
    label "usynawianie"
  ]
  node [
    id 28
    label "dziecko"
  ]
  node [
    id 29
    label "okrzos"
  ]
  node [
    id 30
    label "usynowienie"
  ]
  node [
    id 31
    label "sympatia"
  ]
  node [
    id 32
    label "pederasta"
  ]
  node [
    id 33
    label "synek"
  ]
  node [
    id 34
    label "boyfriend"
  ]
  node [
    id 35
    label "stryczek"
  ]
  node [
    id 36
    label "szubienica"
  ]
  node [
    id 37
    label "bent"
  ]
  node [
    id 38
    label "suspend"
  ]
  node [
    id 39
    label "umie&#347;ci&#263;"
  ]
  node [
    id 40
    label "zabi&#263;"
  ]
  node [
    id 41
    label "si&#281;ga&#263;"
  ]
  node [
    id 42
    label "trwa&#263;"
  ]
  node [
    id 43
    label "obecno&#347;&#263;"
  ]
  node [
    id 44
    label "stan"
  ]
  node [
    id 45
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 46
    label "stand"
  ]
  node [
    id 47
    label "mie&#263;_miejsce"
  ]
  node [
    id 48
    label "uczestniczy&#263;"
  ]
  node [
    id 49
    label "chodzi&#263;"
  ]
  node [
    id 50
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 51
    label "equal"
  ]
  node [
    id 52
    label "niepokoi&#263;"
  ]
  node [
    id 53
    label "lower"
  ]
  node [
    id 54
    label "dr&#281;czy&#263;"
  ]
  node [
    id 55
    label "ka&#322;mucki"
  ]
  node [
    id 56
    label "charakterystyczny"
  ]
  node [
    id 57
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 58
    label "balut"
  ]
  node [
    id 59
    label "kampong"
  ]
  node [
    id 60
    label "azjatycko"
  ]
  node [
    id 61
    label "ghaty"
  ]
  node [
    id 62
    label "banda"
  ]
  node [
    id 63
    label "organizacja"
  ]
  node [
    id 64
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 65
    label "gangster"
  ]
  node [
    id 66
    label "Mickiewicz"
  ]
  node [
    id 67
    label "czas"
  ]
  node [
    id 68
    label "szkolenie"
  ]
  node [
    id 69
    label "przepisa&#263;"
  ]
  node [
    id 70
    label "lesson"
  ]
  node [
    id 71
    label "grupa"
  ]
  node [
    id 72
    label "praktyka"
  ]
  node [
    id 73
    label "metoda"
  ]
  node [
    id 74
    label "niepokalanki"
  ]
  node [
    id 75
    label "kara"
  ]
  node [
    id 76
    label "zda&#263;"
  ]
  node [
    id 77
    label "form"
  ]
  node [
    id 78
    label "kwalifikacje"
  ]
  node [
    id 79
    label "system"
  ]
  node [
    id 80
    label "przepisanie"
  ]
  node [
    id 81
    label "sztuba"
  ]
  node [
    id 82
    label "wiedza"
  ]
  node [
    id 83
    label "stopek"
  ]
  node [
    id 84
    label "school"
  ]
  node [
    id 85
    label "absolwent"
  ]
  node [
    id 86
    label "urszulanki"
  ]
  node [
    id 87
    label "gabinet"
  ]
  node [
    id 88
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 89
    label "ideologia"
  ]
  node [
    id 90
    label "lekcja"
  ]
  node [
    id 91
    label "muzyka"
  ]
  node [
    id 92
    label "podr&#281;cznik"
  ]
  node [
    id 93
    label "zdanie"
  ]
  node [
    id 94
    label "siedziba"
  ]
  node [
    id 95
    label "sekretariat"
  ]
  node [
    id 96
    label "nauka"
  ]
  node [
    id 97
    label "do&#347;wiadczenie"
  ]
  node [
    id 98
    label "tablica"
  ]
  node [
    id 99
    label "teren_szko&#322;y"
  ]
  node [
    id 100
    label "instytucja"
  ]
  node [
    id 101
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 102
    label "skolaryzacja"
  ]
  node [
    id 103
    label "&#322;awa_szkolna"
  ]
  node [
    id 104
    label "klasa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
]
