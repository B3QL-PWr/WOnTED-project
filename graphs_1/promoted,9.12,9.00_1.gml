graph [
  maxDegree 31
  minDegree 1
  meanDegree 1.975
  density 0.025
  graphCliqueNumber 2
  node [
    id 0
    label "dobromir"
    origin "text"
  ]
  node [
    id 1
    label "so&#347;nierz"
    origin "text"
  ]
  node [
    id 2
    label "parlament"
    origin "text"
  ]
  node [
    id 3
    label "europejski"
    origin "text"
  ]
  node [
    id 4
    label "da&#263;"
    origin "text"
  ]
  node [
    id 5
    label "lekcja"
    origin "text"
  ]
  node [
    id 6
    label "brukselski"
    origin "text"
  ]
  node [
    id 7
    label "mi&#322;o&#347;nik"
    origin "text"
  ]
  node [
    id 8
    label "podatek"
    origin "text"
  ]
  node [
    id 9
    label "gn&#281;bienie"
    origin "text"
  ]
  node [
    id 10
    label "obywatel"
    origin "text"
  ]
  node [
    id 11
    label "grupa"
  ]
  node [
    id 12
    label "plankton_polityczny"
  ]
  node [
    id 13
    label "ustawodawca"
  ]
  node [
    id 14
    label "urz&#261;d"
  ]
  node [
    id 15
    label "europarlament"
  ]
  node [
    id 16
    label "grupa_bilateralna"
  ]
  node [
    id 17
    label "European"
  ]
  node [
    id 18
    label "po_europejsku"
  ]
  node [
    id 19
    label "charakterystyczny"
  ]
  node [
    id 20
    label "europejsko"
  ]
  node [
    id 21
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 22
    label "typowy"
  ]
  node [
    id 23
    label "dostarczy&#263;"
  ]
  node [
    id 24
    label "obieca&#263;"
  ]
  node [
    id 25
    label "pozwoli&#263;"
  ]
  node [
    id 26
    label "przeznaczy&#263;"
  ]
  node [
    id 27
    label "doda&#263;"
  ]
  node [
    id 28
    label "give"
  ]
  node [
    id 29
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 30
    label "wyrzec_si&#281;"
  ]
  node [
    id 31
    label "supply"
  ]
  node [
    id 32
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 33
    label "zada&#263;"
  ]
  node [
    id 34
    label "odst&#261;pi&#263;"
  ]
  node [
    id 35
    label "feed"
  ]
  node [
    id 36
    label "testify"
  ]
  node [
    id 37
    label "powierzy&#263;"
  ]
  node [
    id 38
    label "convey"
  ]
  node [
    id 39
    label "przekaza&#263;"
  ]
  node [
    id 40
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 41
    label "zap&#322;aci&#263;"
  ]
  node [
    id 42
    label "dress"
  ]
  node [
    id 43
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 44
    label "udost&#281;pni&#263;"
  ]
  node [
    id 45
    label "sztachn&#261;&#263;"
  ]
  node [
    id 46
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 47
    label "zrobi&#263;"
  ]
  node [
    id 48
    label "przywali&#263;"
  ]
  node [
    id 49
    label "rap"
  ]
  node [
    id 50
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 51
    label "picture"
  ]
  node [
    id 52
    label "do&#347;wiadczenie"
  ]
  node [
    id 53
    label "lektor"
  ]
  node [
    id 54
    label "materia&#322;"
  ]
  node [
    id 55
    label "zaj&#281;cia"
  ]
  node [
    id 56
    label "obrz&#261;dek"
  ]
  node [
    id 57
    label "nauka"
  ]
  node [
    id 58
    label "Biblia"
  ]
  node [
    id 59
    label "spos&#243;b"
  ]
  node [
    id 60
    label "tekst"
  ]
  node [
    id 61
    label "belgijski"
  ]
  node [
    id 62
    label "po_brukselsku"
  ]
  node [
    id 63
    label "entuzjasta"
  ]
  node [
    id 64
    label "sympatyk"
  ]
  node [
    id 65
    label "bilans_handlowy"
  ]
  node [
    id 66
    label "op&#322;ata"
  ]
  node [
    id 67
    label "danina"
  ]
  node [
    id 68
    label "trybut"
  ]
  node [
    id 69
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 70
    label "gn&#281;bienie_si&#281;"
  ]
  node [
    id 71
    label "niepokojenie"
  ]
  node [
    id 72
    label "dr&#281;czenie"
  ]
  node [
    id 73
    label "cz&#322;owiek"
  ]
  node [
    id 74
    label "przedstawiciel"
  ]
  node [
    id 75
    label "miastowy"
  ]
  node [
    id 76
    label "mieszkaniec"
  ]
  node [
    id 77
    label "pa&#324;stwo"
  ]
  node [
    id 78
    label "Dobromir"
  ]
  node [
    id 79
    label "So&#347;nierz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 78
    target 79
  ]
]
