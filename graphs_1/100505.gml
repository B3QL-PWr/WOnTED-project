graph [
  maxDegree 4
  minDegree 0
  meanDegree 2
  density 0.25
  graphCliqueNumber 3
  node [
    id 0
    label "wnukowo"
    origin "text"
  ]
  node [
    id 1
    label "zachodni"
  ]
  node [
    id 2
    label "okr&#281;g"
  ]
  node [
    id 3
    label "administracyjny"
  ]
  node [
    id 4
    label "&#1052;&#1091;&#1085;&#1080;&#1094;&#1080;&#1087;&#1072;&#1083;&#1100;&#1085;&#1099;&#1081;"
  ]
  node [
    id 5
    label "&#1086;&#1082;&#1088;&#1091;&#1075;"
  ]
  node [
    id 6
    label "&#1042;&#1085;&#1091;&#1082;&#1086;&#1074;&#1086;"
  ]
  node [
    id 7
    label "miejski"
  ]
  node [
    id 8
    label "Wnukowo"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
]
