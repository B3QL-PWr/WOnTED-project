graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.1839080459770117
  density 0.012623745930502956
  graphCliqueNumber 4
  node [
    id 0
    label "bydgoszczanin"
    origin "text"
  ]
  node [
    id 1
    label "kolejny"
    origin "text"
  ]
  node [
    id 2
    label "ligowy"
    origin "text"
  ]
  node [
    id 3
    label "spotkanie"
    origin "text"
  ]
  node [
    id 4
    label "zmierzy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "wyjazd"
    origin "text"
  ]
  node [
    id 7
    label "bzura"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "dobre"
    origin "text"
  ]
  node [
    id 10
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 11
    label "rozgrywka"
    origin "text"
  ]
  node [
    id 12
    label "mecz"
    origin "text"
  ]
  node [
    id 13
    label "gra"
    origin "text"
  ]
  node [
    id 14
    label "coraz"
    origin "text"
  ]
  node [
    id 15
    label "s&#322;abo"
    origin "text"
  ]
  node [
    id 16
    label "jak"
    origin "text"
  ]
  node [
    id 17
    label "zwykle"
    origin "text"
  ]
  node [
    id 18
    label "rozpocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "godz"
    origin "text"
  ]
  node [
    id 20
    label "serwis"
    origin "text"
  ]
  node [
    id 21
    label "ozorek"
    origin "text"
  ]
  node [
    id 22
    label "net"
    origin "text"
  ]
  node [
    id 23
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "transmisja"
    origin "text"
  ]
  node [
    id 25
    label "&#380;ywo"
    origin "text"
  ]
  node [
    id 26
    label "mieszkaniec"
  ]
  node [
    id 27
    label "Kujawiak"
  ]
  node [
    id 28
    label "inny"
  ]
  node [
    id 29
    label "nast&#281;pnie"
  ]
  node [
    id 30
    label "kt&#243;ry&#347;"
  ]
  node [
    id 31
    label "kolejno"
  ]
  node [
    id 32
    label "nastopny"
  ]
  node [
    id 33
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 34
    label "po&#380;egnanie"
  ]
  node [
    id 35
    label "spowodowanie"
  ]
  node [
    id 36
    label "znalezienie"
  ]
  node [
    id 37
    label "znajomy"
  ]
  node [
    id 38
    label "doznanie"
  ]
  node [
    id 39
    label "employment"
  ]
  node [
    id 40
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 41
    label "gather"
  ]
  node [
    id 42
    label "powitanie"
  ]
  node [
    id 43
    label "spotykanie"
  ]
  node [
    id 44
    label "wydarzenie"
  ]
  node [
    id 45
    label "gathering"
  ]
  node [
    id 46
    label "spotkanie_si&#281;"
  ]
  node [
    id 47
    label "zdarzenie_si&#281;"
  ]
  node [
    id 48
    label "match"
  ]
  node [
    id 49
    label "zawarcie"
  ]
  node [
    id 50
    label "boundary_line"
  ]
  node [
    id 51
    label "okre&#347;li&#263;"
  ]
  node [
    id 52
    label "podr&#243;&#380;"
  ]
  node [
    id 53
    label "digression"
  ]
  node [
    id 54
    label "miejsce"
  ]
  node [
    id 55
    label "faza"
  ]
  node [
    id 56
    label "upgrade"
  ]
  node [
    id 57
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 58
    label "pierworodztwo"
  ]
  node [
    id 59
    label "nast&#281;pstwo"
  ]
  node [
    id 60
    label "euroliga"
  ]
  node [
    id 61
    label "zagrywka"
  ]
  node [
    id 62
    label "rewan&#380;owy"
  ]
  node [
    id 63
    label "interliga"
  ]
  node [
    id 64
    label "trafienie"
  ]
  node [
    id 65
    label "runda"
  ]
  node [
    id 66
    label "contest"
  ]
  node [
    id 67
    label "obrona"
  ]
  node [
    id 68
    label "dwumecz"
  ]
  node [
    id 69
    label "game"
  ]
  node [
    id 70
    label "serw"
  ]
  node [
    id 71
    label "zabawa"
  ]
  node [
    id 72
    label "rywalizacja"
  ]
  node [
    id 73
    label "czynno&#347;&#263;"
  ]
  node [
    id 74
    label "Pok&#233;mon"
  ]
  node [
    id 75
    label "synteza"
  ]
  node [
    id 76
    label "odtworzenie"
  ]
  node [
    id 77
    label "komplet"
  ]
  node [
    id 78
    label "rekwizyt_do_gry"
  ]
  node [
    id 79
    label "odg&#322;os"
  ]
  node [
    id 80
    label "post&#281;powanie"
  ]
  node [
    id 81
    label "apparent_motion"
  ]
  node [
    id 82
    label "zmienno&#347;&#263;"
  ]
  node [
    id 83
    label "zasada"
  ]
  node [
    id 84
    label "akcja"
  ]
  node [
    id 85
    label "play"
  ]
  node [
    id 86
    label "zbijany"
  ]
  node [
    id 87
    label "marny"
  ]
  node [
    id 88
    label "po&#347;lednio"
  ]
  node [
    id 89
    label "s&#322;aby"
  ]
  node [
    id 90
    label "chorowicie"
  ]
  node [
    id 91
    label "nieznacznie"
  ]
  node [
    id 92
    label "si&#322;a"
  ]
  node [
    id 93
    label "feebly"
  ]
  node [
    id 94
    label "&#378;le"
  ]
  node [
    id 95
    label "marnie"
  ]
  node [
    id 96
    label "kiepski"
  ]
  node [
    id 97
    label "nieswojo"
  ]
  node [
    id 98
    label "nietrwale"
  ]
  node [
    id 99
    label "niefajnie"
  ]
  node [
    id 100
    label "w&#261;t&#322;y"
  ]
  node [
    id 101
    label "zawodnie"
  ]
  node [
    id 102
    label "byd&#322;o"
  ]
  node [
    id 103
    label "zobo"
  ]
  node [
    id 104
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 105
    label "yakalo"
  ]
  node [
    id 106
    label "dzo"
  ]
  node [
    id 107
    label "zwyk&#322;y"
  ]
  node [
    id 108
    label "cz&#281;sto"
  ]
  node [
    id 109
    label "cause"
  ]
  node [
    id 110
    label "zacz&#261;&#263;"
  ]
  node [
    id 111
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 112
    label "do"
  ]
  node [
    id 113
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 114
    label "zrobi&#263;"
  ]
  node [
    id 115
    label "service"
  ]
  node [
    id 116
    label "wytw&#243;r"
  ]
  node [
    id 117
    label "zak&#322;ad"
  ]
  node [
    id 118
    label "us&#322;uga"
  ]
  node [
    id 119
    label "uderzenie"
  ]
  node [
    id 120
    label "doniesienie"
  ]
  node [
    id 121
    label "zastawa"
  ]
  node [
    id 122
    label "YouTube"
  ]
  node [
    id 123
    label "punkt"
  ]
  node [
    id 124
    label "porcja"
  ]
  node [
    id 125
    label "strona"
  ]
  node [
    id 126
    label "pieczarkowiec"
  ]
  node [
    id 127
    label "ozorkowate"
  ]
  node [
    id 128
    label "saprotrof"
  ]
  node [
    id 129
    label "grzyb"
  ]
  node [
    id 130
    label "paso&#380;yt"
  ]
  node [
    id 131
    label "podroby"
  ]
  node [
    id 132
    label "us&#322;uga_internetowa"
  ]
  node [
    id 133
    label "biznes_elektroniczny"
  ]
  node [
    id 134
    label "punkt_dost&#281;pu"
  ]
  node [
    id 135
    label "hipertekst"
  ]
  node [
    id 136
    label "gra_sieciowa"
  ]
  node [
    id 137
    label "mem"
  ]
  node [
    id 138
    label "e-hazard"
  ]
  node [
    id 139
    label "sie&#263;_komputerowa"
  ]
  node [
    id 140
    label "media"
  ]
  node [
    id 141
    label "podcast"
  ]
  node [
    id 142
    label "b&#322;&#261;d"
  ]
  node [
    id 143
    label "netbook"
  ]
  node [
    id 144
    label "provider"
  ]
  node [
    id 145
    label "cyberprzestrze&#324;"
  ]
  node [
    id 146
    label "grooming"
  ]
  node [
    id 147
    label "pom&#243;c"
  ]
  node [
    id 148
    label "zbudowa&#263;"
  ]
  node [
    id 149
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 150
    label "leave"
  ]
  node [
    id 151
    label "przewie&#347;&#263;"
  ]
  node [
    id 152
    label "wykona&#263;"
  ]
  node [
    id 153
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 154
    label "draw"
  ]
  node [
    id 155
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 156
    label "carry"
  ]
  node [
    id 157
    label "program"
  ]
  node [
    id 158
    label "przekaz"
  ]
  node [
    id 159
    label "zjawisko"
  ]
  node [
    id 160
    label "proces"
  ]
  node [
    id 161
    label "silnie"
  ]
  node [
    id 162
    label "wyra&#378;nie"
  ]
  node [
    id 163
    label "naturalnie"
  ]
  node [
    id 164
    label "ciekawie"
  ]
  node [
    id 165
    label "&#380;ywy"
  ]
  node [
    id 166
    label "zgrabnie"
  ]
  node [
    id 167
    label "szybko"
  ]
  node [
    id 168
    label "prawdziwie"
  ]
  node [
    id 169
    label "realistycznie"
  ]
  node [
    id 170
    label "nasycony"
  ]
  node [
    id 171
    label "energicznie"
  ]
  node [
    id 172
    label "g&#322;&#281;boko"
  ]
  node [
    id 173
    label "neta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 17
    target 108
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 126
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 21
    target 129
  ]
  edge [
    source 21
    target 130
  ]
  edge [
    source 21
    target 131
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 22
    target 133
  ]
  edge [
    source 22
    target 134
  ]
  edge [
    source 22
    target 135
  ]
  edge [
    source 22
    target 136
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 138
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 22
    target 140
  ]
  edge [
    source 22
    target 141
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 22
    target 143
  ]
  edge [
    source 22
    target 144
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 125
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 23
    target 151
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 23
    target 153
  ]
  edge [
    source 23
    target 154
  ]
  edge [
    source 23
    target 155
  ]
  edge [
    source 23
    target 156
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 158
  ]
  edge [
    source 24
    target 159
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 162
  ]
  edge [
    source 25
    target 163
  ]
  edge [
    source 25
    target 164
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 167
  ]
  edge [
    source 25
    target 168
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 25
    target 172
  ]
]
