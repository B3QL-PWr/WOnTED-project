graph [
  maxDegree 70
  minDegree 1
  meanDegree 2
  density 0.019801980198019802
  graphCliqueNumber 2
  node [
    id 0
    label "jarmark"
    origin "text"
  ]
  node [
    id 1
    label "bo&#380;onarodzeniowy"
    origin "text"
  ]
  node [
    id 2
    label "dortmund"
    origin "text"
  ]
  node [
    id 3
    label "jeden"
    origin "text"
  ]
  node [
    id 4
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 5
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 6
    label "stoisko"
  ]
  node [
    id 7
    label "plac"
  ]
  node [
    id 8
    label "kram"
  ]
  node [
    id 9
    label "market"
  ]
  node [
    id 10
    label "obiekt_handlowy"
  ]
  node [
    id 11
    label "targowica"
  ]
  node [
    id 12
    label "targ"
  ]
  node [
    id 13
    label "kieliszek"
  ]
  node [
    id 14
    label "shot"
  ]
  node [
    id 15
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 16
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 17
    label "jaki&#347;"
  ]
  node [
    id 18
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 19
    label "jednolicie"
  ]
  node [
    id 20
    label "w&#243;dka"
  ]
  node [
    id 21
    label "ten"
  ]
  node [
    id 22
    label "ujednolicenie"
  ]
  node [
    id 23
    label "jednakowy"
  ]
  node [
    id 24
    label "doros&#322;y"
  ]
  node [
    id 25
    label "wiele"
  ]
  node [
    id 26
    label "dorodny"
  ]
  node [
    id 27
    label "znaczny"
  ]
  node [
    id 28
    label "du&#380;o"
  ]
  node [
    id 29
    label "prawdziwy"
  ]
  node [
    id 30
    label "niema&#322;o"
  ]
  node [
    id 31
    label "wa&#380;ny"
  ]
  node [
    id 32
    label "rozwini&#281;ty"
  ]
  node [
    id 33
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 34
    label "obszar"
  ]
  node [
    id 35
    label "obiekt_naturalny"
  ]
  node [
    id 36
    label "przedmiot"
  ]
  node [
    id 37
    label "Stary_&#346;wiat"
  ]
  node [
    id 38
    label "grupa"
  ]
  node [
    id 39
    label "stw&#243;r"
  ]
  node [
    id 40
    label "biosfera"
  ]
  node [
    id 41
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 42
    label "rzecz"
  ]
  node [
    id 43
    label "magnetosfera"
  ]
  node [
    id 44
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 45
    label "environment"
  ]
  node [
    id 46
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 47
    label "geosfera"
  ]
  node [
    id 48
    label "Nowy_&#346;wiat"
  ]
  node [
    id 49
    label "planeta"
  ]
  node [
    id 50
    label "przejmowa&#263;"
  ]
  node [
    id 51
    label "litosfera"
  ]
  node [
    id 52
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 53
    label "makrokosmos"
  ]
  node [
    id 54
    label "barysfera"
  ]
  node [
    id 55
    label "biota"
  ]
  node [
    id 56
    label "p&#243;&#322;noc"
  ]
  node [
    id 57
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 58
    label "fauna"
  ]
  node [
    id 59
    label "wszechstworzenie"
  ]
  node [
    id 60
    label "geotermia"
  ]
  node [
    id 61
    label "biegun"
  ]
  node [
    id 62
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 63
    label "ekosystem"
  ]
  node [
    id 64
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 65
    label "teren"
  ]
  node [
    id 66
    label "zjawisko"
  ]
  node [
    id 67
    label "p&#243;&#322;kula"
  ]
  node [
    id 68
    label "atmosfera"
  ]
  node [
    id 69
    label "mikrokosmos"
  ]
  node [
    id 70
    label "class"
  ]
  node [
    id 71
    label "po&#322;udnie"
  ]
  node [
    id 72
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 73
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 74
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 75
    label "przejmowanie"
  ]
  node [
    id 76
    label "przestrze&#324;"
  ]
  node [
    id 77
    label "asymilowanie_si&#281;"
  ]
  node [
    id 78
    label "przej&#261;&#263;"
  ]
  node [
    id 79
    label "ekosfera"
  ]
  node [
    id 80
    label "przyroda"
  ]
  node [
    id 81
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 82
    label "ciemna_materia"
  ]
  node [
    id 83
    label "geoida"
  ]
  node [
    id 84
    label "Wsch&#243;d"
  ]
  node [
    id 85
    label "populace"
  ]
  node [
    id 86
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 87
    label "huczek"
  ]
  node [
    id 88
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 89
    label "Ziemia"
  ]
  node [
    id 90
    label "universe"
  ]
  node [
    id 91
    label "ozonosfera"
  ]
  node [
    id 92
    label "rze&#378;ba"
  ]
  node [
    id 93
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 94
    label "zagranica"
  ]
  node [
    id 95
    label "hydrosfera"
  ]
  node [
    id 96
    label "woda"
  ]
  node [
    id 97
    label "kuchnia"
  ]
  node [
    id 98
    label "przej&#281;cie"
  ]
  node [
    id 99
    label "czarna_dziura"
  ]
  node [
    id 100
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 101
    label "morze"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
]
