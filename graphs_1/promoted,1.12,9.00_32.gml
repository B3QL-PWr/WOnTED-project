graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9565217391304348
  density 0.043478260869565216
  graphCliqueNumber 2
  node [
    id 0
    label "misia"
    origin "text"
  ]
  node [
    id 1
    label "polarny"
    origin "text"
  ]
  node [
    id 2
    label "opracowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "w&#322;asny"
    origin "text"
  ]
  node [
    id 4
    label "niezawodny"
    origin "text"
  ]
  node [
    id 5
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 6
    label "suszy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wcale"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "r&#281;cznik"
    origin "text"
  ]
  node [
    id 10
    label "invent"
  ]
  node [
    id 11
    label "przygotowa&#263;"
  ]
  node [
    id 12
    label "swoisty"
  ]
  node [
    id 13
    label "czyj&#347;"
  ]
  node [
    id 14
    label "osobny"
  ]
  node [
    id 15
    label "zwi&#261;zany"
  ]
  node [
    id 16
    label "samodzielny"
  ]
  node [
    id 17
    label "niezawodnie"
  ]
  node [
    id 18
    label "rzetelny"
  ]
  node [
    id 19
    label "pewny"
  ]
  node [
    id 20
    label "model"
  ]
  node [
    id 21
    label "zbi&#243;r"
  ]
  node [
    id 22
    label "tryb"
  ]
  node [
    id 23
    label "narz&#281;dzie"
  ]
  node [
    id 24
    label "nature"
  ]
  node [
    id 25
    label "zmienia&#263;"
  ]
  node [
    id 26
    label "pemikan"
  ]
  node [
    id 27
    label "&#347;cina&#263;"
  ]
  node [
    id 28
    label "robi&#263;"
  ]
  node [
    id 29
    label "drain"
  ]
  node [
    id 30
    label "powodowa&#263;"
  ]
  node [
    id 31
    label "ni_chuja"
  ]
  node [
    id 32
    label "ca&#322;kiem"
  ]
  node [
    id 33
    label "zupe&#322;nie"
  ]
  node [
    id 34
    label "si&#281;ga&#263;"
  ]
  node [
    id 35
    label "trwa&#263;"
  ]
  node [
    id 36
    label "obecno&#347;&#263;"
  ]
  node [
    id 37
    label "stan"
  ]
  node [
    id 38
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 39
    label "stand"
  ]
  node [
    id 40
    label "mie&#263;_miejsce"
  ]
  node [
    id 41
    label "uczestniczy&#263;"
  ]
  node [
    id 42
    label "chodzi&#263;"
  ]
  node [
    id 43
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 44
    label "equal"
  ]
  node [
    id 45
    label "przedmiot"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 9
    target 45
  ]
]
