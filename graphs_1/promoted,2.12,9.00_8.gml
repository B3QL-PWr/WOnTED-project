graph [
  maxDegree 22
  minDegree 1
  meanDegree 2
  density 0.030303030303030304
  graphCliqueNumber 3
  node [
    id 0
    label "swoje"
    origin "text"
  ]
  node [
    id 1
    label "wart"
    origin "text"
  ]
  node [
    id 2
    label "obuwie"
    origin "text"
  ]
  node [
    id 3
    label "wystawi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zaprosi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "kilka"
    origin "text"
  ]
  node [
    id 6
    label "influencer&#243;w"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "zachwyca&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "niespotykany"
    origin "text"
  ]
  node [
    id 11
    label "jako&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "towar"
    origin "text"
  ]
  node [
    id 13
    label "godnie"
  ]
  node [
    id 14
    label "przedmiot"
  ]
  node [
    id 15
    label "but"
  ]
  node [
    id 16
    label "footwear"
  ]
  node [
    id 17
    label "element"
  ]
  node [
    id 18
    label "wysun&#261;&#263;"
  ]
  node [
    id 19
    label "zbudowa&#263;"
  ]
  node [
    id 20
    label "wyrazi&#263;"
  ]
  node [
    id 21
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 22
    label "przedstawi&#263;"
  ]
  node [
    id 23
    label "wypisa&#263;"
  ]
  node [
    id 24
    label "wskaza&#263;"
  ]
  node [
    id 25
    label "indicate"
  ]
  node [
    id 26
    label "wynie&#347;&#263;"
  ]
  node [
    id 27
    label "pies_my&#347;liwski"
  ]
  node [
    id 28
    label "zaproponowa&#263;"
  ]
  node [
    id 29
    label "wyeksponowa&#263;"
  ]
  node [
    id 30
    label "wychyli&#263;"
  ]
  node [
    id 31
    label "set"
  ]
  node [
    id 32
    label "wyj&#261;&#263;"
  ]
  node [
    id 33
    label "invite"
  ]
  node [
    id 34
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 35
    label "ask"
  ]
  node [
    id 36
    label "&#347;ledziowate"
  ]
  node [
    id 37
    label "ryba"
  ]
  node [
    id 38
    label "wzbudza&#263;"
  ]
  node [
    id 39
    label "enthuse"
  ]
  node [
    id 40
    label "niezwyk&#322;y"
  ]
  node [
    id 41
    label "warto&#347;&#263;"
  ]
  node [
    id 42
    label "co&#347;"
  ]
  node [
    id 43
    label "syf"
  ]
  node [
    id 44
    label "state"
  ]
  node [
    id 45
    label "quality"
  ]
  node [
    id 46
    label "cz&#322;owiek"
  ]
  node [
    id 47
    label "obr&#243;t_handlowy"
  ]
  node [
    id 48
    label "rzuca&#263;"
  ]
  node [
    id 49
    label "tkanina"
  ]
  node [
    id 50
    label "rzucenie"
  ]
  node [
    id 51
    label "tandeta"
  ]
  node [
    id 52
    label "naszprycowa&#263;"
  ]
  node [
    id 53
    label "&#322;&#243;dzki"
  ]
  node [
    id 54
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 55
    label "naszprycowanie"
  ]
  node [
    id 56
    label "za&#322;adownia"
  ]
  node [
    id 57
    label "szprycowa&#263;"
  ]
  node [
    id 58
    label "rzuci&#263;"
  ]
  node [
    id 59
    label "wyr&#243;b"
  ]
  node [
    id 60
    label "szprycowanie"
  ]
  node [
    id 61
    label "asortyment"
  ]
  node [
    id 62
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 63
    label "rzucanie"
  ]
  node [
    id 64
    label "narkobiznes"
  ]
  node [
    id 65
    label "metka"
  ]
  node [
    id 66
    label "zbi&#243;r"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 41
  ]
  edge [
    source 11
    target 42
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 11
    target 44
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 12
    target 46
  ]
  edge [
    source 12
    target 47
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 49
  ]
  edge [
    source 12
    target 50
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 54
  ]
  edge [
    source 12
    target 55
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 12
    target 57
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
]
