graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.0355555555555553
  density 0.009087301587301587
  graphCliqueNumber 2
  node [
    id 0
    label "te&#380;"
    origin "text"
  ]
  node [
    id 1
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zakolarski"
    origin "text"
  ]
  node [
    id 4
    label "przegrywem"
    origin "text"
  ]
  node [
    id 5
    label "przez"
    origin "text"
  ]
  node [
    id 6
    label "lato"
    origin "text"
  ]
  node [
    id 7
    label "swoje"
    origin "text"
  ]
  node [
    id 8
    label "incelowego"
    origin "text"
  ]
  node [
    id 9
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 10
    label "pod"
    origin "text"
  ]
  node [
    id 11
    label "czapka"
    origin "text"
  ]
  node [
    id 12
    label "wyda&#263;"
    origin "text"
  ]
  node [
    id 13
    label "pi&#281;kne"
    origin "text"
  ]
  node [
    id 14
    label "sierpniowy"
    origin "text"
  ]
  node [
    id 15
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 16
    label "tyby&#263;"
    origin "text"
  ]
  node [
    id 17
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 18
    label "aby"
    origin "text"
  ]
  node [
    id 19
    label "zamieni&#263;"
    origin "text"
  ]
  node [
    id 20
    label "si&#281;"
    origin "text"
  ]
  node [
    id 21
    label "alfa"
    origin "text"
  ]
  node [
    id 22
    label "by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "warto"
    origin "text"
  ]
  node [
    id 24
    label "nowa"
    origin "text"
  ]
  node [
    id 25
    label "nawet"
    origin "text"
  ]
  node [
    id 26
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 27
    label "zarucha&#263;"
    origin "text"
  ]
  node [
    id 28
    label "wysoce"
    origin "text"
  ]
  node [
    id 29
    label "musie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 30
    label "&#347;ci&#261;ga&#263;"
    origin "text"
  ]
  node [
    id 31
    label "siebie"
    origin "text"
  ]
  node [
    id 32
    label "sk&#243;rka"
    origin "text"
  ]
  node [
    id 33
    label "sam"
    origin "text"
  ]
  node [
    id 34
    label "bez"
    origin "text"
  ]
  node [
    id 35
    label "pozdrowienie"
    origin "text"
  ]
  node [
    id 36
    label "pora_roku"
  ]
  node [
    id 37
    label "energy"
  ]
  node [
    id 38
    label "czas"
  ]
  node [
    id 39
    label "bycie"
  ]
  node [
    id 40
    label "zegar_biologiczny"
  ]
  node [
    id 41
    label "okres_noworodkowy"
  ]
  node [
    id 42
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 43
    label "entity"
  ]
  node [
    id 44
    label "prze&#380;ywanie"
  ]
  node [
    id 45
    label "prze&#380;ycie"
  ]
  node [
    id 46
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 47
    label "wiek_matuzalemowy"
  ]
  node [
    id 48
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 49
    label "dzieci&#324;stwo"
  ]
  node [
    id 50
    label "power"
  ]
  node [
    id 51
    label "szwung"
  ]
  node [
    id 52
    label "menopauza"
  ]
  node [
    id 53
    label "umarcie"
  ]
  node [
    id 54
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 55
    label "life"
  ]
  node [
    id 56
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 57
    label "&#380;ywy"
  ]
  node [
    id 58
    label "rozw&#243;j"
  ]
  node [
    id 59
    label "po&#322;&#243;g"
  ]
  node [
    id 60
    label "byt"
  ]
  node [
    id 61
    label "przebywanie"
  ]
  node [
    id 62
    label "subsistence"
  ]
  node [
    id 63
    label "koleje_losu"
  ]
  node [
    id 64
    label "raj_utracony"
  ]
  node [
    id 65
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 66
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 67
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 68
    label "andropauza"
  ]
  node [
    id 69
    label "warunki"
  ]
  node [
    id 70
    label "do&#380;ywanie"
  ]
  node [
    id 71
    label "niemowl&#281;ctwo"
  ]
  node [
    id 72
    label "umieranie"
  ]
  node [
    id 73
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 74
    label "staro&#347;&#263;"
  ]
  node [
    id 75
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 76
    label "&#347;mier&#263;"
  ]
  node [
    id 77
    label "czapczysko"
  ]
  node [
    id 78
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 79
    label "impart"
  ]
  node [
    id 80
    label "panna_na_wydaniu"
  ]
  node [
    id 81
    label "translate"
  ]
  node [
    id 82
    label "give"
  ]
  node [
    id 83
    label "pieni&#261;dze"
  ]
  node [
    id 84
    label "supply"
  ]
  node [
    id 85
    label "wprowadzi&#263;"
  ]
  node [
    id 86
    label "da&#263;"
  ]
  node [
    id 87
    label "zapach"
  ]
  node [
    id 88
    label "wydawnictwo"
  ]
  node [
    id 89
    label "powierzy&#263;"
  ]
  node [
    id 90
    label "produkcja"
  ]
  node [
    id 91
    label "poda&#263;"
  ]
  node [
    id 92
    label "skojarzy&#263;"
  ]
  node [
    id 93
    label "dress"
  ]
  node [
    id 94
    label "plon"
  ]
  node [
    id 95
    label "ujawni&#263;"
  ]
  node [
    id 96
    label "reszta"
  ]
  node [
    id 97
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 98
    label "zadenuncjowa&#263;"
  ]
  node [
    id 99
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 100
    label "zrobi&#263;"
  ]
  node [
    id 101
    label "tajemnica"
  ]
  node [
    id 102
    label "wiano"
  ]
  node [
    id 103
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 104
    label "wytworzy&#263;"
  ]
  node [
    id 105
    label "d&#378;wi&#281;k"
  ]
  node [
    id 106
    label "picture"
  ]
  node [
    id 107
    label "letni"
  ]
  node [
    id 108
    label "s&#322;o&#324;ce"
  ]
  node [
    id 109
    label "czynienie_si&#281;"
  ]
  node [
    id 110
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 111
    label "long_time"
  ]
  node [
    id 112
    label "przedpo&#322;udnie"
  ]
  node [
    id 113
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 114
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 115
    label "tydzie&#324;"
  ]
  node [
    id 116
    label "godzina"
  ]
  node [
    id 117
    label "t&#322;usty_czwartek"
  ]
  node [
    id 118
    label "wsta&#263;"
  ]
  node [
    id 119
    label "day"
  ]
  node [
    id 120
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 121
    label "przedwiecz&#243;r"
  ]
  node [
    id 122
    label "Sylwester"
  ]
  node [
    id 123
    label "po&#322;udnie"
  ]
  node [
    id 124
    label "wzej&#347;cie"
  ]
  node [
    id 125
    label "podwiecz&#243;r"
  ]
  node [
    id 126
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 127
    label "rano"
  ]
  node [
    id 128
    label "termin"
  ]
  node [
    id 129
    label "ranek"
  ]
  node [
    id 130
    label "doba"
  ]
  node [
    id 131
    label "wiecz&#243;r"
  ]
  node [
    id 132
    label "walentynki"
  ]
  node [
    id 133
    label "popo&#322;udnie"
  ]
  node [
    id 134
    label "noc"
  ]
  node [
    id 135
    label "wstanie"
  ]
  node [
    id 136
    label "szlachetny"
  ]
  node [
    id 137
    label "metaliczny"
  ]
  node [
    id 138
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 139
    label "z&#322;ocenie"
  ]
  node [
    id 140
    label "grosz"
  ]
  node [
    id 141
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 142
    label "utytu&#322;owany"
  ]
  node [
    id 143
    label "poz&#322;ocenie"
  ]
  node [
    id 144
    label "Polska"
  ]
  node [
    id 145
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 146
    label "wspania&#322;y"
  ]
  node [
    id 147
    label "doskona&#322;y"
  ]
  node [
    id 148
    label "kochany"
  ]
  node [
    id 149
    label "jednostka_monetarna"
  ]
  node [
    id 150
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 151
    label "troch&#281;"
  ]
  node [
    id 152
    label "komunikowa&#263;"
  ]
  node [
    id 153
    label "zmieni&#263;"
  ]
  node [
    id 154
    label "zast&#261;pi&#263;"
  ]
  node [
    id 155
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 156
    label "Alfa_Romeo"
  ]
  node [
    id 157
    label "alfabet_grecki"
  ]
  node [
    id 158
    label "grusza_pospolita"
  ]
  node [
    id 159
    label "samoch&#243;d"
  ]
  node [
    id 160
    label "litera"
  ]
  node [
    id 161
    label "si&#281;ga&#263;"
  ]
  node [
    id 162
    label "trwa&#263;"
  ]
  node [
    id 163
    label "obecno&#347;&#263;"
  ]
  node [
    id 164
    label "stan"
  ]
  node [
    id 165
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 166
    label "stand"
  ]
  node [
    id 167
    label "mie&#263;_miejsce"
  ]
  node [
    id 168
    label "uczestniczy&#263;"
  ]
  node [
    id 169
    label "chodzi&#263;"
  ]
  node [
    id 170
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 171
    label "equal"
  ]
  node [
    id 172
    label "przysparza&#263;"
  ]
  node [
    id 173
    label "kali&#263;_si&#281;"
  ]
  node [
    id 174
    label "bonanza"
  ]
  node [
    id 175
    label "gwiazda"
  ]
  node [
    id 176
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 177
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 178
    label "represent"
  ]
  node [
    id 179
    label "wysoki"
  ]
  node [
    id 180
    label "intensywnie"
  ]
  node [
    id 181
    label "wielki"
  ]
  node [
    id 182
    label "zmusza&#263;"
  ]
  node [
    id 183
    label "stiffen"
  ]
  node [
    id 184
    label "bind"
  ]
  node [
    id 185
    label "przepisywa&#263;"
  ]
  node [
    id 186
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 187
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 188
    label "sprawdzian"
  ]
  node [
    id 189
    label "pozyskiwa&#263;"
  ]
  node [
    id 190
    label "znosi&#263;"
  ]
  node [
    id 191
    label "kurczy&#263;"
  ]
  node [
    id 192
    label "odprowadza&#263;"
  ]
  node [
    id 193
    label "kopiowa&#263;"
  ]
  node [
    id 194
    label "robi&#263;"
  ]
  node [
    id 195
    label "przewi&#261;zywa&#263;"
  ]
  node [
    id 196
    label "kra&#347;&#263;"
  ]
  node [
    id 197
    label "clamp"
  ]
  node [
    id 198
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 199
    label "powodowa&#263;"
  ]
  node [
    id 200
    label "zdejmowa&#263;"
  ]
  node [
    id 201
    label "ciecz"
  ]
  node [
    id 202
    label "tkanka_okrywaj&#261;ca"
  ]
  node [
    id 203
    label "okrywa"
  ]
  node [
    id 204
    label "shell"
  ]
  node [
    id 205
    label "naklejka"
  ]
  node [
    id 206
    label "pow&#322;oka"
  ]
  node [
    id 207
    label "ro&#347;lina"
  ]
  node [
    id 208
    label "&#322;upa"
  ]
  node [
    id 209
    label "obudowa"
  ]
  node [
    id 210
    label "sklep"
  ]
  node [
    id 211
    label "ki&#347;&#263;"
  ]
  node [
    id 212
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 213
    label "krzew"
  ]
  node [
    id 214
    label "pi&#380;maczkowate"
  ]
  node [
    id 215
    label "pestkowiec"
  ]
  node [
    id 216
    label "kwiat"
  ]
  node [
    id 217
    label "owoc"
  ]
  node [
    id 218
    label "oliwkowate"
  ]
  node [
    id 219
    label "hy&#263;ka"
  ]
  node [
    id 220
    label "lilac"
  ]
  node [
    id 221
    label "delfinidyna"
  ]
  node [
    id 222
    label "greeting"
  ]
  node [
    id 223
    label "zrobienie"
  ]
  node [
    id 224
    label "wypowied&#378;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 38
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 23
    target 82
  ]
  edge [
    source 23
    target 173
  ]
  edge [
    source 23
    target 174
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 177
  ]
  edge [
    source 26
    target 178
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 179
  ]
  edge [
    source 28
    target 180
  ]
  edge [
    source 28
    target 181
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 182
  ]
  edge [
    source 30
    target 183
  ]
  edge [
    source 30
    target 184
  ]
  edge [
    source 30
    target 185
  ]
  edge [
    source 30
    target 186
  ]
  edge [
    source 30
    target 187
  ]
  edge [
    source 30
    target 188
  ]
  edge [
    source 30
    target 189
  ]
  edge [
    source 30
    target 190
  ]
  edge [
    source 30
    target 191
  ]
  edge [
    source 30
    target 192
  ]
  edge [
    source 30
    target 193
  ]
  edge [
    source 30
    target 194
  ]
  edge [
    source 30
    target 195
  ]
  edge [
    source 30
    target 196
  ]
  edge [
    source 30
    target 197
  ]
  edge [
    source 30
    target 198
  ]
  edge [
    source 30
    target 199
  ]
  edge [
    source 30
    target 200
  ]
  edge [
    source 30
    target 201
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 202
  ]
  edge [
    source 32
    target 203
  ]
  edge [
    source 32
    target 204
  ]
  edge [
    source 32
    target 205
  ]
  edge [
    source 32
    target 206
  ]
  edge [
    source 32
    target 207
  ]
  edge [
    source 32
    target 208
  ]
  edge [
    source 32
    target 209
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 210
  ]
  edge [
    source 34
    target 211
  ]
  edge [
    source 34
    target 212
  ]
  edge [
    source 34
    target 213
  ]
  edge [
    source 34
    target 214
  ]
  edge [
    source 34
    target 215
  ]
  edge [
    source 34
    target 216
  ]
  edge [
    source 34
    target 217
  ]
  edge [
    source 34
    target 218
  ]
  edge [
    source 34
    target 207
  ]
  edge [
    source 34
    target 219
  ]
  edge [
    source 34
    target 220
  ]
  edge [
    source 34
    target 221
  ]
  edge [
    source 35
    target 222
  ]
  edge [
    source 35
    target 223
  ]
  edge [
    source 35
    target 224
  ]
]
