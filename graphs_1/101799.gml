graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.092485549132948
  density 0.012165613657749697
  graphCliqueNumber 6
  node [
    id 0
    label "fundacja"
    origin "text"
  ]
  node [
    id 1
    label "nowa"
    origin "text"
  ]
  node [
    id 2
    label "media"
    origin "text"
  ]
  node [
    id 3
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sesja"
    origin "text"
  ]
  node [
    id 5
    label "temat"
    origin "text"
  ]
  node [
    id 6
    label "rola"
    origin "text"
  ]
  node [
    id 7
    label "sprawa"
    origin "text"
  ]
  node [
    id 8
    label "krzy&#380;"
    origin "text"
  ]
  node [
    id 9
    label "krakowskie"
    origin "text"
  ]
  node [
    id 10
    label "przedmie&#347;cie"
    origin "text"
  ]
  node [
    id 11
    label "foundation"
  ]
  node [
    id 12
    label "instytucja"
  ]
  node [
    id 13
    label "dar"
  ]
  node [
    id 14
    label "darowizna"
  ]
  node [
    id 15
    label "pocz&#261;tek"
  ]
  node [
    id 16
    label "gwiazda"
  ]
  node [
    id 17
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 18
    label "przekazior"
  ]
  node [
    id 19
    label "mass-media"
  ]
  node [
    id 20
    label "uzbrajanie"
  ]
  node [
    id 21
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 22
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 23
    label "medium"
  ]
  node [
    id 24
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 25
    label "invite"
  ]
  node [
    id 26
    label "ask"
  ]
  node [
    id 27
    label "oferowa&#263;"
  ]
  node [
    id 28
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 29
    label "seria"
  ]
  node [
    id 30
    label "obiekt"
  ]
  node [
    id 31
    label "dyskusja"
  ]
  node [
    id 32
    label "conference"
  ]
  node [
    id 33
    label "sesyjka"
  ]
  node [
    id 34
    label "spotkanie"
  ]
  node [
    id 35
    label "gie&#322;da_papier&#243;w_warto&#347;ciowych"
  ]
  node [
    id 36
    label "dzie&#324;_pracy"
  ]
  node [
    id 37
    label "konsylium"
  ]
  node [
    id 38
    label "egzamin"
  ]
  node [
    id 39
    label "dogrywka"
  ]
  node [
    id 40
    label "rok_akademicki"
  ]
  node [
    id 41
    label "psychoterapia"
  ]
  node [
    id 42
    label "fraza"
  ]
  node [
    id 43
    label "forma"
  ]
  node [
    id 44
    label "melodia"
  ]
  node [
    id 45
    label "rzecz"
  ]
  node [
    id 46
    label "zbacza&#263;"
  ]
  node [
    id 47
    label "entity"
  ]
  node [
    id 48
    label "omawia&#263;"
  ]
  node [
    id 49
    label "topik"
  ]
  node [
    id 50
    label "wyraz_pochodny"
  ]
  node [
    id 51
    label "om&#243;wi&#263;"
  ]
  node [
    id 52
    label "omawianie"
  ]
  node [
    id 53
    label "w&#261;tek"
  ]
  node [
    id 54
    label "forum"
  ]
  node [
    id 55
    label "cecha"
  ]
  node [
    id 56
    label "zboczenie"
  ]
  node [
    id 57
    label "zbaczanie"
  ]
  node [
    id 58
    label "tre&#347;&#263;"
  ]
  node [
    id 59
    label "tematyka"
  ]
  node [
    id 60
    label "istota"
  ]
  node [
    id 61
    label "otoczka"
  ]
  node [
    id 62
    label "zboczy&#263;"
  ]
  node [
    id 63
    label "om&#243;wienie"
  ]
  node [
    id 64
    label "pole"
  ]
  node [
    id 65
    label "znaczenie"
  ]
  node [
    id 66
    label "ziemia"
  ]
  node [
    id 67
    label "sk&#322;ad"
  ]
  node [
    id 68
    label "zastosowanie"
  ]
  node [
    id 69
    label "zreinterpretowa&#263;"
  ]
  node [
    id 70
    label "zreinterpretowanie"
  ]
  node [
    id 71
    label "function"
  ]
  node [
    id 72
    label "zagranie"
  ]
  node [
    id 73
    label "p&#322;osa"
  ]
  node [
    id 74
    label "plik"
  ]
  node [
    id 75
    label "cel"
  ]
  node [
    id 76
    label "reinterpretowanie"
  ]
  node [
    id 77
    label "tekst"
  ]
  node [
    id 78
    label "wykonywa&#263;"
  ]
  node [
    id 79
    label "uprawi&#263;"
  ]
  node [
    id 80
    label "uprawienie"
  ]
  node [
    id 81
    label "gra&#263;"
  ]
  node [
    id 82
    label "radlina"
  ]
  node [
    id 83
    label "ustawi&#263;"
  ]
  node [
    id 84
    label "irygowa&#263;"
  ]
  node [
    id 85
    label "wrench"
  ]
  node [
    id 86
    label "irygowanie"
  ]
  node [
    id 87
    label "dialog"
  ]
  node [
    id 88
    label "zagon"
  ]
  node [
    id 89
    label "scenariusz"
  ]
  node [
    id 90
    label "zagra&#263;"
  ]
  node [
    id 91
    label "kszta&#322;t"
  ]
  node [
    id 92
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 93
    label "ustawienie"
  ]
  node [
    id 94
    label "czyn"
  ]
  node [
    id 95
    label "gospodarstwo"
  ]
  node [
    id 96
    label "reinterpretowa&#263;"
  ]
  node [
    id 97
    label "granie"
  ]
  node [
    id 98
    label "wykonywanie"
  ]
  node [
    id 99
    label "aktorstwo"
  ]
  node [
    id 100
    label "kostium"
  ]
  node [
    id 101
    label "posta&#263;"
  ]
  node [
    id 102
    label "kognicja"
  ]
  node [
    id 103
    label "idea"
  ]
  node [
    id 104
    label "szczeg&#243;&#322;"
  ]
  node [
    id 105
    label "wydarzenie"
  ]
  node [
    id 106
    label "przes&#322;anka"
  ]
  node [
    id 107
    label "rozprawa"
  ]
  node [
    id 108
    label "object"
  ]
  node [
    id 109
    label "proposition"
  ]
  node [
    id 110
    label "traverse"
  ]
  node [
    id 111
    label "gest"
  ]
  node [
    id 112
    label "cierpienie"
  ]
  node [
    id 113
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 114
    label "symbol"
  ]
  node [
    id 115
    label "order"
  ]
  node [
    id 116
    label "przedmiot"
  ]
  node [
    id 117
    label "d&#322;o&#324;"
  ]
  node [
    id 118
    label "biblizm"
  ]
  node [
    id 119
    label "kara_&#347;mierci"
  ]
  node [
    id 120
    label "obrze&#380;e"
  ]
  node [
    id 121
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 122
    label "nowy"
  ]
  node [
    id 123
    label "krakowski"
  ]
  node [
    id 124
    label "stowarzyszy&#263;"
  ]
  node [
    id 125
    label "dziennikarz"
  ]
  node [
    id 126
    label "polskie"
  ]
  node [
    id 127
    label "centrum"
  ]
  node [
    id 128
    label "monitoring"
  ]
  node [
    id 129
    label "wolno&#347;&#263;"
  ]
  node [
    id 130
    label "prasa"
  ]
  node [
    id 131
    label "polski"
  ]
  node [
    id 132
    label "agencja"
  ]
  node [
    id 133
    label "prasowy"
  ]
  node [
    id 134
    label "Janina"
  ]
  node [
    id 135
    label "Jankowska"
  ]
  node [
    id 136
    label "Piotr"
  ]
  node [
    id 137
    label "Zaremba"
  ]
  node [
    id 138
    label "Roberta"
  ]
  node [
    id 139
    label "Bogda&#324;ski"
  ]
  node [
    id 140
    label "marka"
  ]
  node [
    id 141
    label "zaj&#261;c"
  ]
  node [
    id 142
    label "Krzysztofa"
  ]
  node [
    id 143
    label "Skowro&#324;ski"
  ]
  node [
    id 144
    label "radio"
  ]
  node [
    id 145
    label "wnet"
  ]
  node [
    id 146
    label "wiktor"
  ]
  node [
    id 147
    label "&#347;wietlik"
  ]
  node [
    id 148
    label "Bogus&#322;awa"
  ]
  node [
    id 149
    label "Chraboty"
  ]
  node [
    id 150
    label "Tomasz"
  ]
  node [
    id 151
    label "Sakiewicza"
  ]
  node [
    id 152
    label "Bogumi&#322;a"
  ]
  node [
    id 153
    label "&#321;ozi&#324;ski"
  ]
  node [
    id 154
    label "Wojciecha"
  ]
  node [
    id 155
    label "Maziarski"
  ]
  node [
    id 156
    label "go&#347;&#263;"
  ]
  node [
    id 157
    label "niedzielny"
  ]
  node [
    id 158
    label "gazeta"
  ]
  node [
    id 159
    label "wyborczy"
  ]
  node [
    id 160
    label "Rafa&#322;"
  ]
  node [
    id 161
    label "zimny"
  ]
  node [
    id 162
    label "uniwersytet"
  ]
  node [
    id 163
    label "Kazimierz"
  ]
  node [
    id 164
    label "wielki"
  ]
  node [
    id 165
    label "s&#322;ownik"
  ]
  node [
    id 166
    label "polszczyzna"
  ]
  node [
    id 167
    label "polityczny"
  ]
  node [
    id 168
    label "po"
  ]
  node [
    id 169
    label "rok"
  ]
  node [
    id 170
    label "1989"
  ]
  node [
    id 171
    label "albo"
  ]
  node [
    id 172
    label "Nomejki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 23
    target 122
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 126
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 129
  ]
  edge [
    source 127
    target 130
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 130
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 133
  ]
  edge [
    source 131
    target 158
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 142
    target 143
  ]
  edge [
    source 144
    target 145
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 150
    target 151
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 154
    target 172
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 162
    target 164
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 165
    target 167
  ]
  edge [
    source 165
    target 168
  ]
  edge [
    source 165
    target 169
  ]
  edge [
    source 165
    target 170
  ]
  edge [
    source 166
    target 167
  ]
  edge [
    source 166
    target 168
  ]
  edge [
    source 166
    target 169
  ]
  edge [
    source 166
    target 170
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 167
    target 169
  ]
  edge [
    source 167
    target 170
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 168
    target 170
  ]
  edge [
    source 169
    target 170
  ]
]
