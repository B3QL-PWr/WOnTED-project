graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.2083333333333335
  density 0.04698581560283688
  graphCliqueNumber 5
  node [
    id 0
    label "komunistyczny"
    origin "text"
  ]
  node [
    id 1
    label "partia"
    origin "text"
  ]
  node [
    id 2
    label "austria"
    origin "text"
  ]
  node [
    id 3
    label "lewicowy"
  ]
  node [
    id 4
    label "skomunizowanie"
  ]
  node [
    id 5
    label "czerwono"
  ]
  node [
    id 6
    label "komunizowanie"
  ]
  node [
    id 7
    label "SLD"
  ]
  node [
    id 8
    label "niedoczas"
  ]
  node [
    id 9
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 10
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 11
    label "grupa"
  ]
  node [
    id 12
    label "game"
  ]
  node [
    id 13
    label "ZChN"
  ]
  node [
    id 14
    label "wybranka"
  ]
  node [
    id 15
    label "Wigowie"
  ]
  node [
    id 16
    label "egzekutywa"
  ]
  node [
    id 17
    label "unit"
  ]
  node [
    id 18
    label "blok"
  ]
  node [
    id 19
    label "Razem"
  ]
  node [
    id 20
    label "si&#322;a"
  ]
  node [
    id 21
    label "organizacja"
  ]
  node [
    id 22
    label "wybranek"
  ]
  node [
    id 23
    label "materia&#322;"
  ]
  node [
    id 24
    label "PiS"
  ]
  node [
    id 25
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 26
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 27
    label "AWS"
  ]
  node [
    id 28
    label "package"
  ]
  node [
    id 29
    label "Bund"
  ]
  node [
    id 30
    label "Kuomintang"
  ]
  node [
    id 31
    label "aktyw"
  ]
  node [
    id 32
    label "Jakobici"
  ]
  node [
    id 33
    label "PSL"
  ]
  node [
    id 34
    label "Federali&#347;ci"
  ]
  node [
    id 35
    label "gra"
  ]
  node [
    id 36
    label "ZSL"
  ]
  node [
    id 37
    label "PPR"
  ]
  node [
    id 38
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 39
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 40
    label "PO"
  ]
  node [
    id 41
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 42
    label "Austria"
  ]
  node [
    id 43
    label "Kommunistische"
  ]
  node [
    id 44
    label "Jugend"
  ]
  node [
    id 45
    label "&#214;sterreichs"
  ]
  node [
    id 46
    label "Junge"
  ]
  node [
    id 47
    label "Linke"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 43
    target 47
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 46
    target 47
  ]
]
