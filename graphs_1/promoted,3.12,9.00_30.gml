graph [
  maxDegree 30
  minDegree 1
  meanDegree 1.9710144927536233
  density 0.028985507246376812
  graphCliqueNumber 2
  node [
    id 0
    label "reprezentacja"
    origin "text"
  ]
  node [
    id 1
    label "polska"
    origin "text"
  ]
  node [
    id 2
    label "awans"
    origin "text"
  ]
  node [
    id 3
    label "turniej"
    origin "text"
  ]
  node [
    id 4
    label "powalczy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "grupa"
    origin "text"
  ]
  node [
    id 6
    label "gram"
    origin "text"
  ]
  node [
    id 7
    label "austria"
    origin "text"
  ]
  node [
    id 8
    label "izrael"
    origin "text"
  ]
  node [
    id 9
    label "s&#322;owenia"
    origin "text"
  ]
  node [
    id 10
    label "macedonia"
    origin "text"
  ]
  node [
    id 11
    label "&#322;otwa"
    origin "text"
  ]
  node [
    id 12
    label "dru&#380;yna"
  ]
  node [
    id 13
    label "zesp&#243;&#322;"
  ]
  node [
    id 14
    label "emblemat"
  ]
  node [
    id 15
    label "deputation"
  ]
  node [
    id 16
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 17
    label "korzy&#347;&#263;"
  ]
  node [
    id 18
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 19
    label "kariera"
  ]
  node [
    id 20
    label "zaliczka"
  ]
  node [
    id 21
    label "status"
  ]
  node [
    id 22
    label "wzrost"
  ]
  node [
    id 23
    label "nagroda"
  ]
  node [
    id 24
    label "preferment"
  ]
  node [
    id 25
    label "position"
  ]
  node [
    id 26
    label "eliminacje"
  ]
  node [
    id 27
    label "zawody"
  ]
  node [
    id 28
    label "Wielki_Szlem"
  ]
  node [
    id 29
    label "drive"
  ]
  node [
    id 30
    label "impreza"
  ]
  node [
    id 31
    label "pojedynek"
  ]
  node [
    id 32
    label "runda"
  ]
  node [
    id 33
    label "tournament"
  ]
  node [
    id 34
    label "rywalizacja"
  ]
  node [
    id 35
    label "porobi&#263;"
  ]
  node [
    id 36
    label "odm&#322;adza&#263;"
  ]
  node [
    id 37
    label "asymilowa&#263;"
  ]
  node [
    id 38
    label "cz&#261;steczka"
  ]
  node [
    id 39
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 40
    label "egzemplarz"
  ]
  node [
    id 41
    label "formacja_geologiczna"
  ]
  node [
    id 42
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 43
    label "harcerze_starsi"
  ]
  node [
    id 44
    label "liga"
  ]
  node [
    id 45
    label "Terranie"
  ]
  node [
    id 46
    label "&#346;wietliki"
  ]
  node [
    id 47
    label "pakiet_klimatyczny"
  ]
  node [
    id 48
    label "oddzia&#322;"
  ]
  node [
    id 49
    label "stage_set"
  ]
  node [
    id 50
    label "Entuzjastki"
  ]
  node [
    id 51
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 52
    label "odm&#322;odzenie"
  ]
  node [
    id 53
    label "type"
  ]
  node [
    id 54
    label "category"
  ]
  node [
    id 55
    label "asymilowanie"
  ]
  node [
    id 56
    label "specgrupa"
  ]
  node [
    id 57
    label "odm&#322;adzanie"
  ]
  node [
    id 58
    label "gromada"
  ]
  node [
    id 59
    label "Eurogrupa"
  ]
  node [
    id 60
    label "jednostka_systematyczna"
  ]
  node [
    id 61
    label "kompozycja"
  ]
  node [
    id 62
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 63
    label "zbi&#243;r"
  ]
  node [
    id 64
    label "dekagram"
  ]
  node [
    id 65
    label "megagram"
  ]
  node [
    id 66
    label "centygram"
  ]
  node [
    id 67
    label "miligram"
  ]
  node [
    id 68
    label "metryczna_jednostka_masy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
]
