graph [
  maxDegree 8
  minDegree 1
  meanDegree 3.3684210526315788
  density 0.1871345029239766
  graphCliqueNumber 7
  node [
    id 0
    label "amanda"
    origin "text"
  ]
  node [
    id 1
    label "gill"
    origin "text"
  ]
  node [
    id 2
    label "zmar&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "hospital"
    origin "text"
  ]
  node [
    id 4
    label "cos"
    origin "text"
  ]
  node [
    id 5
    label "meksyk"
    origin "text"
  ]
  node [
    id 6
    label "cosine"
  ]
  node [
    id 7
    label "funkcja_trygonometryczna"
  ]
  node [
    id 8
    label "arcus_cosinus"
  ]
  node [
    id 9
    label "dostawa"
  ]
  node [
    id 10
    label "ba&#322;agan"
  ]
  node [
    id 11
    label "Amanda"
  ]
  node [
    id 12
    label "Gill"
  ]
  node [
    id 13
    label "w"
  ]
  node [
    id 14
    label "Hospital"
  ]
  node [
    id 15
    label "de"
  ]
  node [
    id 16
    label "Cos"
  ]
  node [
    id 17
    label "wielki"
  ]
  node [
    id 18
    label "brytania"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
]
