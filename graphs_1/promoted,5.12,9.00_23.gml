graph [
  maxDegree 18
  minDegree 1
  meanDegree 2.020408163265306
  density 0.020828950136755734
  graphCliqueNumber 2
  node [
    id 0
    label "manager"
    origin "text"
  ]
  node [
    id 1
    label "wysoki"
    origin "text"
  ]
  node [
    id 2
    label "szczebel"
    origin "text"
  ]
  node [
    id 3
    label "tak"
    origin "text"
  ]
  node [
    id 4
    label "bardzo"
    origin "text"
  ]
  node [
    id 5
    label "boja"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "oskar&#380;enie"
    origin "text"
  ]
  node [
    id 8
    label "kobieta"
    origin "text"
  ]
  node [
    id 9
    label "napa&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "seksualny"
    origin "text"
  ]
  node [
    id 11
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 12
    label "coraz"
    origin "text"
  ]
  node [
    id 13
    label "bardziej"
    origin "text"
  ]
  node [
    id 14
    label "izolowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "kierownictwo"
  ]
  node [
    id 16
    label "zwierzchnik"
  ]
  node [
    id 17
    label "warto&#347;ciowy"
  ]
  node [
    id 18
    label "du&#380;y"
  ]
  node [
    id 19
    label "wysoce"
  ]
  node [
    id 20
    label "daleki"
  ]
  node [
    id 21
    label "znaczny"
  ]
  node [
    id 22
    label "wysoko"
  ]
  node [
    id 23
    label "szczytnie"
  ]
  node [
    id 24
    label "wznios&#322;y"
  ]
  node [
    id 25
    label "wyrafinowany"
  ]
  node [
    id 26
    label "z_wysoka"
  ]
  node [
    id 27
    label "chwalebny"
  ]
  node [
    id 28
    label "uprzywilejowany"
  ]
  node [
    id 29
    label "niepo&#347;ledni"
  ]
  node [
    id 30
    label "stopie&#324;"
  ]
  node [
    id 31
    label "drabina"
  ]
  node [
    id 32
    label "faza"
  ]
  node [
    id 33
    label "gradation"
  ]
  node [
    id 34
    label "w_chuj"
  ]
  node [
    id 35
    label "float"
  ]
  node [
    id 36
    label "znak_nawigacyjny"
  ]
  node [
    id 37
    label "p&#322;ywak"
  ]
  node [
    id 38
    label "s&#261;d"
  ]
  node [
    id 39
    label "wypowied&#378;"
  ]
  node [
    id 40
    label "ocena"
  ]
  node [
    id 41
    label "ocenienie"
  ]
  node [
    id 42
    label "post&#281;powanie"
  ]
  node [
    id 43
    label "skar&#380;yciel"
  ]
  node [
    id 44
    label "poj&#281;cie"
  ]
  node [
    id 45
    label "suspicja"
  ]
  node [
    id 46
    label "strona"
  ]
  node [
    id 47
    label "cz&#322;owiek"
  ]
  node [
    id 48
    label "przekwitanie"
  ]
  node [
    id 49
    label "m&#281;&#380;yna"
  ]
  node [
    id 50
    label "babka"
  ]
  node [
    id 51
    label "samica"
  ]
  node [
    id 52
    label "doros&#322;y"
  ]
  node [
    id 53
    label "ulec"
  ]
  node [
    id 54
    label "uleganie"
  ]
  node [
    id 55
    label "partnerka"
  ]
  node [
    id 56
    label "&#380;ona"
  ]
  node [
    id 57
    label "ulega&#263;"
  ]
  node [
    id 58
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 59
    label "pa&#324;stwo"
  ]
  node [
    id 60
    label "ulegni&#281;cie"
  ]
  node [
    id 61
    label "menopauza"
  ]
  node [
    id 62
    label "&#322;ono"
  ]
  node [
    id 63
    label "ofensywa"
  ]
  node [
    id 64
    label "atak"
  ]
  node [
    id 65
    label "skrytykowa&#263;"
  ]
  node [
    id 66
    label "dopa&#347;&#263;"
  ]
  node [
    id 67
    label "spell"
  ]
  node [
    id 68
    label "powiedzie&#263;"
  ]
  node [
    id 69
    label "knock"
  ]
  node [
    id 70
    label "przest&#281;pstwo"
  ]
  node [
    id 71
    label "krytyka"
  ]
  node [
    id 72
    label "zaatakowa&#263;"
  ]
  node [
    id 73
    label "irruption"
  ]
  node [
    id 74
    label "p&#322;ciowo"
  ]
  node [
    id 75
    label "seksualnie"
  ]
  node [
    id 76
    label "seksowny"
  ]
  node [
    id 77
    label "erotyczny"
  ]
  node [
    id 78
    label "open"
  ]
  node [
    id 79
    label "odejmowa&#263;"
  ]
  node [
    id 80
    label "mie&#263;_miejsce"
  ]
  node [
    id 81
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 82
    label "set_about"
  ]
  node [
    id 83
    label "begin"
  ]
  node [
    id 84
    label "post&#281;powa&#263;"
  ]
  node [
    id 85
    label "bankrupt"
  ]
  node [
    id 86
    label "odosobnia&#263;"
  ]
  node [
    id 87
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 88
    label "wydzieli&#263;"
  ]
  node [
    id 89
    label "wyizolowa&#263;"
  ]
  node [
    id 90
    label "zabezpiecza&#263;"
  ]
  node [
    id 91
    label "abstract"
  ]
  node [
    id 92
    label "wyizolowywa&#263;"
  ]
  node [
    id 93
    label "oddziela&#263;"
  ]
  node [
    id 94
    label "zabezpieczy&#263;"
  ]
  node [
    id 95
    label "odizolowa&#263;"
  ]
  node [
    id 96
    label "abstraction"
  ]
  node [
    id 97
    label "oddzieli&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
]
