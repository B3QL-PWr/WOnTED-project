graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9047619047619047
  density 0.09523809523809523
  graphCliqueNumber 2
  node [
    id 0
    label "sz&#243;stka"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "lubelskie"
    origin "text"
  ]
  node [
    id 3
    label "blotka"
  ]
  node [
    id 4
    label "obiekt"
  ]
  node [
    id 5
    label "trafienie"
  ]
  node [
    id 6
    label "przedtrzonowiec"
  ]
  node [
    id 7
    label "stopie&#324;"
  ]
  node [
    id 8
    label "six"
  ]
  node [
    id 9
    label "zbi&#243;r"
  ]
  node [
    id 10
    label "cyfra"
  ]
  node [
    id 11
    label "toto-lotek"
  ]
  node [
    id 12
    label "zaprz&#281;g"
  ]
  node [
    id 13
    label "bilard"
  ]
  node [
    id 14
    label "jednostka_administracyjna"
  ]
  node [
    id 15
    label "makroregion"
  ]
  node [
    id 16
    label "powiat"
  ]
  node [
    id 17
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 18
    label "mikroregion"
  ]
  node [
    id 19
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 20
    label "pa&#324;stwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
]
