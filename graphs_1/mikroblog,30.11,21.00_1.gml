graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.13333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "wykop"
    origin "text"
  ]
  node [
    id 1
    label "bojowkaas"
    origin "text"
  ]
  node [
    id 2
    label "aswykopu"
    origin "text"
  ]
  node [
    id 3
    label "itaksiezyjenatymwykopie"
    origin "text"
  ]
  node [
    id 4
    label "heheszki"
    origin "text"
  ]
  node [
    id 5
    label "odwa&#322;"
  ]
  node [
    id 6
    label "chody"
  ]
  node [
    id 7
    label "grodzisko"
  ]
  node [
    id 8
    label "budowa"
  ]
  node [
    id 9
    label "kopniak"
  ]
  node [
    id 10
    label "wyrobisko"
  ]
  node [
    id 11
    label "zrzutowy"
  ]
  node [
    id 12
    label "szaniec"
  ]
  node [
    id 13
    label "odk&#322;ad"
  ]
  node [
    id 14
    label "wg&#322;&#281;bienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
]
