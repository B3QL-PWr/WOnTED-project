graph [
  maxDegree 7
  minDegree 1
  meanDegree 2.125
  density 0.14166666666666666
  graphCliqueNumber 4
  node [
    id 0
    label "alc&#225;cer"
    origin "text"
  ]
  node [
    id 1
    label "sala"
    origin "text"
  ]
  node [
    id 2
    label "zgromadzenie"
  ]
  node [
    id 3
    label "publiczno&#347;&#263;"
  ]
  node [
    id 4
    label "audience"
  ]
  node [
    id 5
    label "pomieszczenie"
  ]
  node [
    id 6
    label "Alc&#225;cer"
  ]
  node [
    id 7
    label "do"
  ]
  node [
    id 8
    label "Alentejo"
  ]
  node [
    id 9
    label "litoral"
  ]
  node [
    id 10
    label "Santa"
  ]
  node [
    id 11
    label "Maria"
  ]
  node [
    id 12
    label "Castelo"
  ]
  node [
    id 13
    label "Susana"
  ]
  node [
    id 14
    label "S&#227;o"
  ]
  node [
    id 15
    label "Martinho"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 14
    target 15
  ]
]
