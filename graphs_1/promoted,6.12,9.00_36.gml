graph [
  maxDegree 66
  minDegree 1
  meanDegree 2
  density 0.007407407407407408
  graphCliqueNumber 2
  node [
    id 0
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "godzina"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "polski"
    origin "text"
  ]
  node [
    id 4
    label "utc"
    origin "text"
  ]
  node [
    id 5
    label "zaplanowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "start"
    origin "text"
  ]
  node [
    id 7
    label "rakieta"
    origin "text"
  ]
  node [
    id 8
    label "falcon"
    origin "text"
  ]
  node [
    id 9
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 10
    label "zadanie"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "wynie&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "orbita"
    origin "text"
  ]
  node [
    id 14
    label "statek"
    origin "text"
  ]
  node [
    id 15
    label "dragon"
    origin "text"
  ]
  node [
    id 16
    label "zapas"
    origin "text"
  ]
  node [
    id 17
    label "sprz&#281;t"
    origin "text"
  ]
  node [
    id 18
    label "eksperyment"
    origin "text"
  ]
  node [
    id 19
    label "naukowy"
    origin "text"
  ]
  node [
    id 20
    label "issa"
    origin "text"
  ]
  node [
    id 21
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 22
    label "miesi&#261;c"
  ]
  node [
    id 23
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 24
    label "Barb&#243;rka"
  ]
  node [
    id 25
    label "Sylwester"
  ]
  node [
    id 26
    label "minuta"
  ]
  node [
    id 27
    label "doba"
  ]
  node [
    id 28
    label "p&#243;&#322;godzina"
  ]
  node [
    id 29
    label "kwadrans"
  ]
  node [
    id 30
    label "time"
  ]
  node [
    id 31
    label "jednostka_czasu"
  ]
  node [
    id 32
    label "czasokres"
  ]
  node [
    id 33
    label "trawienie"
  ]
  node [
    id 34
    label "kategoria_gramatyczna"
  ]
  node [
    id 35
    label "period"
  ]
  node [
    id 36
    label "odczyt"
  ]
  node [
    id 37
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 38
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 39
    label "chwila"
  ]
  node [
    id 40
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 41
    label "poprzedzenie"
  ]
  node [
    id 42
    label "koniugacja"
  ]
  node [
    id 43
    label "dzieje"
  ]
  node [
    id 44
    label "poprzedzi&#263;"
  ]
  node [
    id 45
    label "przep&#322;ywanie"
  ]
  node [
    id 46
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 47
    label "odwlekanie_si&#281;"
  ]
  node [
    id 48
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 49
    label "Zeitgeist"
  ]
  node [
    id 50
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 51
    label "okres_czasu"
  ]
  node [
    id 52
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 53
    label "pochodzi&#263;"
  ]
  node [
    id 54
    label "schy&#322;ek"
  ]
  node [
    id 55
    label "czwarty_wymiar"
  ]
  node [
    id 56
    label "chronometria"
  ]
  node [
    id 57
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 58
    label "poprzedzanie"
  ]
  node [
    id 59
    label "pogoda"
  ]
  node [
    id 60
    label "zegar"
  ]
  node [
    id 61
    label "pochodzenie"
  ]
  node [
    id 62
    label "poprzedza&#263;"
  ]
  node [
    id 63
    label "trawi&#263;"
  ]
  node [
    id 64
    label "time_period"
  ]
  node [
    id 65
    label "rachuba_czasu"
  ]
  node [
    id 66
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 67
    label "czasoprzestrze&#324;"
  ]
  node [
    id 68
    label "laba"
  ]
  node [
    id 69
    label "lacki"
  ]
  node [
    id 70
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 71
    label "przedmiot"
  ]
  node [
    id 72
    label "sztajer"
  ]
  node [
    id 73
    label "drabant"
  ]
  node [
    id 74
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 75
    label "polak"
  ]
  node [
    id 76
    label "pierogi_ruskie"
  ]
  node [
    id 77
    label "krakowiak"
  ]
  node [
    id 78
    label "Polish"
  ]
  node [
    id 79
    label "j&#281;zyk"
  ]
  node [
    id 80
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 81
    label "oberek"
  ]
  node [
    id 82
    label "po_polsku"
  ]
  node [
    id 83
    label "mazur"
  ]
  node [
    id 84
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 85
    label "chodzony"
  ]
  node [
    id 86
    label "skoczny"
  ]
  node [
    id 87
    label "ryba_po_grecku"
  ]
  node [
    id 88
    label "goniony"
  ]
  node [
    id 89
    label "polsko"
  ]
  node [
    id 90
    label "pomy&#347;le&#263;"
  ]
  node [
    id 91
    label "opracowa&#263;"
  ]
  node [
    id 92
    label "line_up"
  ]
  node [
    id 93
    label "zrobi&#263;"
  ]
  node [
    id 94
    label "przemy&#347;le&#263;"
  ]
  node [
    id 95
    label "map"
  ]
  node [
    id 96
    label "okno_startowe"
  ]
  node [
    id 97
    label "wy&#347;cig"
  ]
  node [
    id 98
    label "rozpocz&#281;cie"
  ]
  node [
    id 99
    label "uczestnictwo"
  ]
  node [
    id 100
    label "blok_startowy"
  ]
  node [
    id 101
    label "pocz&#261;tek"
  ]
  node [
    id 102
    label "lot"
  ]
  node [
    id 103
    label "silnik_rakietowy"
  ]
  node [
    id 104
    label "szybki"
  ]
  node [
    id 105
    label "pocisk_odrzutowy"
  ]
  node [
    id 106
    label "przyrz&#261;d"
  ]
  node [
    id 107
    label "but"
  ]
  node [
    id 108
    label "naci&#261;g"
  ]
  node [
    id 109
    label "tenisista"
  ]
  node [
    id 110
    label "pojazd_lataj&#261;cy"
  ]
  node [
    id 111
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 112
    label "&#347;nieg"
  ]
  node [
    id 113
    label "pojazd"
  ]
  node [
    id 114
    label "&#322;&#261;cznik"
  ]
  node [
    id 115
    label "g&#322;&#243;wka"
  ]
  node [
    id 116
    label "statek_kosmiczny"
  ]
  node [
    id 117
    label "yield"
  ]
  node [
    id 118
    label "czynno&#347;&#263;"
  ]
  node [
    id 119
    label "problem"
  ]
  node [
    id 120
    label "przepisanie"
  ]
  node [
    id 121
    label "przepisa&#263;"
  ]
  node [
    id 122
    label "za&#322;o&#380;enie"
  ]
  node [
    id 123
    label "work"
  ]
  node [
    id 124
    label "nakarmienie"
  ]
  node [
    id 125
    label "duty"
  ]
  node [
    id 126
    label "zbi&#243;r"
  ]
  node [
    id 127
    label "powierzanie"
  ]
  node [
    id 128
    label "zaszkodzenie"
  ]
  node [
    id 129
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 130
    label "zaj&#281;cie"
  ]
  node [
    id 131
    label "zobowi&#261;zanie"
  ]
  node [
    id 132
    label "si&#281;ga&#263;"
  ]
  node [
    id 133
    label "trwa&#263;"
  ]
  node [
    id 134
    label "obecno&#347;&#263;"
  ]
  node [
    id 135
    label "stan"
  ]
  node [
    id 136
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 137
    label "stand"
  ]
  node [
    id 138
    label "mie&#263;_miejsce"
  ]
  node [
    id 139
    label "uczestniczy&#263;"
  ]
  node [
    id 140
    label "chodzi&#263;"
  ]
  node [
    id 141
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 142
    label "equal"
  ]
  node [
    id 143
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 144
    label "nasi&#261;kn&#261;&#263;"
  ]
  node [
    id 145
    label "ukra&#347;&#263;"
  ]
  node [
    id 146
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 147
    label "ujawni&#263;"
  ]
  node [
    id 148
    label "podnie&#347;&#263;"
  ]
  node [
    id 149
    label "otrzyma&#263;"
  ]
  node [
    id 150
    label "rozpowszechni&#263;"
  ]
  node [
    id 151
    label "odsun&#261;&#263;"
  ]
  node [
    id 152
    label "kwota"
  ]
  node [
    id 153
    label "zanie&#347;&#263;"
  ]
  node [
    id 154
    label "raise"
  ]
  node [
    id 155
    label "progress"
  ]
  node [
    id 156
    label "apogeum"
  ]
  node [
    id 157
    label "aphelium"
  ]
  node [
    id 158
    label "ruchy_planet"
  ]
  node [
    id 159
    label "w&#281;ze&#322;"
  ]
  node [
    id 160
    label "oczod&#243;&#322;"
  ]
  node [
    id 161
    label "tor"
  ]
  node [
    id 162
    label "punkt_przyziemny"
  ]
  node [
    id 163
    label "punkt_przys&#322;oneczny"
  ]
  node [
    id 164
    label "korab"
  ]
  node [
    id 165
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 166
    label "zr&#281;bnica"
  ]
  node [
    id 167
    label "odkotwiczenie"
  ]
  node [
    id 168
    label "cumowanie"
  ]
  node [
    id 169
    label "zadokowanie"
  ]
  node [
    id 170
    label "bumsztak"
  ]
  node [
    id 171
    label "dobi&#263;"
  ]
  node [
    id 172
    label "odkotwiczanie"
  ]
  node [
    id 173
    label "zacumowanie"
  ]
  node [
    id 174
    label "kotwica"
  ]
  node [
    id 175
    label "zwodowa&#263;"
  ]
  node [
    id 176
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 177
    label "zakotwiczenie"
  ]
  node [
    id 178
    label "dzi&#243;b"
  ]
  node [
    id 179
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 180
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 181
    label "armada"
  ]
  node [
    id 182
    label "grobla"
  ]
  node [
    id 183
    label "kad&#322;ub"
  ]
  node [
    id 184
    label "odkotwicza&#263;"
  ]
  node [
    id 185
    label "dobijanie"
  ]
  node [
    id 186
    label "luk"
  ]
  node [
    id 187
    label "proporczyk"
  ]
  node [
    id 188
    label "odcumowanie"
  ]
  node [
    id 189
    label "kabina"
  ]
  node [
    id 190
    label "skrajnik"
  ]
  node [
    id 191
    label "szkutnictwo"
  ]
  node [
    id 192
    label "kotwiczenie"
  ]
  node [
    id 193
    label "p&#322;ywa&#263;"
  ]
  node [
    id 194
    label "zwodowanie"
  ]
  node [
    id 195
    label "zacumowa&#263;"
  ]
  node [
    id 196
    label "sterownik_automatyczny"
  ]
  node [
    id 197
    label "zadokowa&#263;"
  ]
  node [
    id 198
    label "wodowanie"
  ]
  node [
    id 199
    label "zakotwiczy&#263;"
  ]
  node [
    id 200
    label "pok&#322;ad"
  ]
  node [
    id 201
    label "sztormtrap"
  ]
  node [
    id 202
    label "kotwiczy&#263;"
  ]
  node [
    id 203
    label "&#380;yroskop"
  ]
  node [
    id 204
    label "odcumowa&#263;"
  ]
  node [
    id 205
    label "armator"
  ]
  node [
    id 206
    label "dobicie"
  ]
  node [
    id 207
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 208
    label "odbijacz"
  ]
  node [
    id 209
    label "reling"
  ]
  node [
    id 210
    label "flota"
  ]
  node [
    id 211
    label "kabestan"
  ]
  node [
    id 212
    label "nadbud&#243;wka"
  ]
  node [
    id 213
    label "dokowa&#263;"
  ]
  node [
    id 214
    label "cumowa&#263;"
  ]
  node [
    id 215
    label "dobija&#263;"
  ]
  node [
    id 216
    label "odkotwiczy&#263;"
  ]
  node [
    id 217
    label "odcumowywanie"
  ]
  node [
    id 218
    label "ster"
  ]
  node [
    id 219
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 220
    label "odcumowywa&#263;"
  ]
  node [
    id 221
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 222
    label "futr&#243;wka"
  ]
  node [
    id 223
    label "dokowanie"
  ]
  node [
    id 224
    label "trap"
  ]
  node [
    id 225
    label "zaw&#243;r_denny"
  ]
  node [
    id 226
    label "rostra"
  ]
  node [
    id 227
    label "bersalier"
  ]
  node [
    id 228
    label "cz&#322;owiek"
  ]
  node [
    id 229
    label "patka"
  ]
  node [
    id 230
    label "piechur"
  ]
  node [
    id 231
    label "istota_&#380;ywa"
  ]
  node [
    id 232
    label "kobieta"
  ]
  node [
    id 233
    label "hajduk"
  ]
  node [
    id 234
    label "kosynier"
  ]
  node [
    id 235
    label "jegier"
  ]
  node [
    id 236
    label "dragonia"
  ]
  node [
    id 237
    label "kawalerzysta"
  ]
  node [
    id 238
    label "drab"
  ]
  node [
    id 239
    label "pikinier"
  ]
  node [
    id 240
    label "tarczownik"
  ]
  node [
    id 241
    label "&#380;uaw"
  ]
  node [
    id 242
    label "stock"
  ]
  node [
    id 243
    label "substytut"
  ]
  node [
    id 244
    label "nadwy&#380;ka"
  ]
  node [
    id 245
    label "resource"
  ]
  node [
    id 246
    label "zapasy"
  ]
  node [
    id 247
    label "zas&#243;b"
  ]
  node [
    id 248
    label "kolekcja"
  ]
  node [
    id 249
    label "sprz&#281;cior"
  ]
  node [
    id 250
    label "furniture"
  ]
  node [
    id 251
    label "equipment"
  ]
  node [
    id 252
    label "penis"
  ]
  node [
    id 253
    label "sprz&#281;cik"
  ]
  node [
    id 254
    label "innowacja"
  ]
  node [
    id 255
    label "badanie"
  ]
  node [
    id 256
    label "assay"
  ]
  node [
    id 257
    label "obserwowanie"
  ]
  node [
    id 258
    label "specjalny"
  ]
  node [
    id 259
    label "edukacyjnie"
  ]
  node [
    id 260
    label "intelektualny"
  ]
  node [
    id 261
    label "skomplikowany"
  ]
  node [
    id 262
    label "zgodny"
  ]
  node [
    id 263
    label "naukowo"
  ]
  node [
    id 264
    label "scjentyficzny"
  ]
  node [
    id 265
    label "teoretyczny"
  ]
  node [
    id 266
    label "specjalistyczny"
  ]
  node [
    id 267
    label "Falcon"
  ]
  node [
    id 268
    label "9"
  ]
  node [
    id 269
    label "LZ"
  ]
  node [
    id 270
    label "1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 14
    target 213
  ]
  edge [
    source 14
    target 214
  ]
  edge [
    source 14
    target 215
  ]
  edge [
    source 14
    target 216
  ]
  edge [
    source 14
    target 217
  ]
  edge [
    source 14
    target 218
  ]
  edge [
    source 14
    target 219
  ]
  edge [
    source 14
    target 220
  ]
  edge [
    source 14
    target 221
  ]
  edge [
    source 14
    target 222
  ]
  edge [
    source 14
    target 223
  ]
  edge [
    source 14
    target 224
  ]
  edge [
    source 14
    target 225
  ]
  edge [
    source 14
    target 226
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 227
  ]
  edge [
    source 15
    target 228
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 15
    target 230
  ]
  edge [
    source 15
    target 231
  ]
  edge [
    source 15
    target 232
  ]
  edge [
    source 15
    target 233
  ]
  edge [
    source 15
    target 234
  ]
  edge [
    source 15
    target 235
  ]
  edge [
    source 15
    target 236
  ]
  edge [
    source 15
    target 237
  ]
  edge [
    source 15
    target 238
  ]
  edge [
    source 15
    target 239
  ]
  edge [
    source 15
    target 240
  ]
  edge [
    source 15
    target 241
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 242
  ]
  edge [
    source 16
    target 243
  ]
  edge [
    source 16
    target 244
  ]
  edge [
    source 16
    target 245
  ]
  edge [
    source 16
    target 246
  ]
  edge [
    source 16
    target 247
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 71
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 249
  ]
  edge [
    source 17
    target 250
  ]
  edge [
    source 17
    target 251
  ]
  edge [
    source 17
    target 252
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 254
  ]
  edge [
    source 18
    target 255
  ]
  edge [
    source 18
    target 256
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 258
  ]
  edge [
    source 19
    target 259
  ]
  edge [
    source 19
    target 260
  ]
  edge [
    source 19
    target 261
  ]
  edge [
    source 19
    target 262
  ]
  edge [
    source 19
    target 263
  ]
  edge [
    source 19
    target 264
  ]
  edge [
    source 19
    target 265
  ]
  edge [
    source 19
    target 266
  ]
  edge [
    source 267
    target 268
  ]
  edge [
    source 269
    target 270
  ]
]
