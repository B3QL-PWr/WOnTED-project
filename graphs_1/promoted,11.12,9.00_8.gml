graph [
  maxDegree 24
  minDegree 1
  meanDegree 2
  density 0.04878048780487805
  graphCliqueNumber 2
  node [
    id 0
    label "uber"
    origin "text"
  ]
  node [
    id 1
    label "twierdzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "moja"
    origin "text"
  ]
  node [
    id 3
    label "partnerka"
    origin "text"
  ]
  node [
    id 4
    label "zarzyga&#263;"
    origin "text"
  ]
  node [
    id 5
    label "auto"
    origin "text"
  ]
  node [
    id 6
    label "kierowca"
    origin "text"
  ]
  node [
    id 7
    label "zapewnia&#263;"
  ]
  node [
    id 8
    label "oznajmia&#263;"
  ]
  node [
    id 9
    label "komunikowa&#263;"
  ]
  node [
    id 10
    label "attest"
  ]
  node [
    id 11
    label "argue"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "kobieta"
  ]
  node [
    id 14
    label "aktorka"
  ]
  node [
    id 15
    label "partner"
  ]
  node [
    id 16
    label "kobita"
  ]
  node [
    id 17
    label "wydali&#263;"
  ]
  node [
    id 18
    label "zabrudzi&#263;"
  ]
  node [
    id 19
    label "baga&#380;nik"
  ]
  node [
    id 20
    label "immobilizer"
  ]
  node [
    id 21
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 22
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 23
    label "poduszka_powietrzna"
  ]
  node [
    id 24
    label "dachowanie"
  ]
  node [
    id 25
    label "dwu&#347;lad"
  ]
  node [
    id 26
    label "deska_rozdzielcza"
  ]
  node [
    id 27
    label "poci&#261;g_drogowy"
  ]
  node [
    id 28
    label "kierownica"
  ]
  node [
    id 29
    label "pojazd_drogowy"
  ]
  node [
    id 30
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 31
    label "pompa_wodna"
  ]
  node [
    id 32
    label "silnik"
  ]
  node [
    id 33
    label "wycieraczka"
  ]
  node [
    id 34
    label "bak"
  ]
  node [
    id 35
    label "ABS"
  ]
  node [
    id 36
    label "most"
  ]
  node [
    id 37
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 38
    label "spryskiwacz"
  ]
  node [
    id 39
    label "t&#322;umik"
  ]
  node [
    id 40
    label "tempomat"
  ]
  node [
    id 41
    label "transportowiec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 41
  ]
]
