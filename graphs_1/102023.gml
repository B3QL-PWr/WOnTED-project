graph [
  maxDegree 23
  minDegree 1
  meanDegree 2.138888888888889
  density 0.009948320413436693
  graphCliqueNumber 4
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "serwis"
    origin "text"
  ]
  node [
    id 3
    label "wirtualnemedia"
    origin "text"
  ]
  node [
    id 4
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 7
    label "superstacja"
    origin "text"
  ]
  node [
    id 8
    label "rozwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 9
    label "umowa"
    origin "text"
  ]
  node [
    id 10
    label "inwestycyjny"
    origin "text"
  ]
  node [
    id 11
    label "partners"
    origin "text"
  ]
  node [
    id 12
    label "zawrze&#263;"
    origin "text"
  ]
  node [
    id 13
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 14
    label "moc"
    origin "text"
  ]
  node [
    id 15
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 16
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 17
    label "naby&#263;"
    origin "text"
  ]
  node [
    id 18
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 19
    label "superstacji"
    origin "text"
  ]
  node [
    id 20
    label "ryszard"
    origin "text"
  ]
  node [
    id 21
    label "krajewski"
    origin "text"
  ]
  node [
    id 22
    label "prezes"
    origin "text"
  ]
  node [
    id 23
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 24
    label "portal"
    origin "text"
  ]
  node [
    id 25
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 26
    label "nast&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 27
    label "skutek"
    origin "text"
  ]
  node [
    id 28
    label "niemo&#380;no&#347;&#263;"
    origin "text"
  ]
  node [
    id 29
    label "daleki"
    origin "text"
  ]
  node [
    id 30
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 31
    label "byd&#322;o"
  ]
  node [
    id 32
    label "zobo"
  ]
  node [
    id 33
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 34
    label "yakalo"
  ]
  node [
    id 35
    label "dzo"
  ]
  node [
    id 36
    label "tenis"
  ]
  node [
    id 37
    label "cover"
  ]
  node [
    id 38
    label "siatk&#243;wka"
  ]
  node [
    id 39
    label "dawa&#263;"
  ]
  node [
    id 40
    label "faszerowa&#263;"
  ]
  node [
    id 41
    label "informowa&#263;"
  ]
  node [
    id 42
    label "introduce"
  ]
  node [
    id 43
    label "jedzenie"
  ]
  node [
    id 44
    label "tender"
  ]
  node [
    id 45
    label "deal"
  ]
  node [
    id 46
    label "kelner"
  ]
  node [
    id 47
    label "serwowa&#263;"
  ]
  node [
    id 48
    label "rozgrywa&#263;"
  ]
  node [
    id 49
    label "stawia&#263;"
  ]
  node [
    id 50
    label "mecz"
  ]
  node [
    id 51
    label "service"
  ]
  node [
    id 52
    label "wytw&#243;r"
  ]
  node [
    id 53
    label "zak&#322;ad"
  ]
  node [
    id 54
    label "us&#322;uga"
  ]
  node [
    id 55
    label "uderzenie"
  ]
  node [
    id 56
    label "doniesienie"
  ]
  node [
    id 57
    label "zastawa"
  ]
  node [
    id 58
    label "YouTube"
  ]
  node [
    id 59
    label "punkt"
  ]
  node [
    id 60
    label "porcja"
  ]
  node [
    id 61
    label "strona"
  ]
  node [
    id 62
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 63
    label "miesi&#261;c"
  ]
  node [
    id 64
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 65
    label "Barb&#243;rka"
  ]
  node [
    id 66
    label "Sylwester"
  ]
  node [
    id 67
    label "stulecie"
  ]
  node [
    id 68
    label "kalendarz"
  ]
  node [
    id 69
    label "czas"
  ]
  node [
    id 70
    label "pora_roku"
  ]
  node [
    id 71
    label "cykl_astronomiczny"
  ]
  node [
    id 72
    label "p&#243;&#322;rocze"
  ]
  node [
    id 73
    label "grupa"
  ]
  node [
    id 74
    label "kwarta&#322;"
  ]
  node [
    id 75
    label "kurs"
  ]
  node [
    id 76
    label "jubileusz"
  ]
  node [
    id 77
    label "lata"
  ]
  node [
    id 78
    label "martwy_sezon"
  ]
  node [
    id 79
    label "wykupienie"
  ]
  node [
    id 80
    label "bycie_w_posiadaniu"
  ]
  node [
    id 81
    label "wykupywanie"
  ]
  node [
    id 82
    label "podmiot"
  ]
  node [
    id 83
    label "unloose"
  ]
  node [
    id 84
    label "urzeczywistni&#263;"
  ]
  node [
    id 85
    label "usun&#261;&#263;"
  ]
  node [
    id 86
    label "wymy&#347;li&#263;"
  ]
  node [
    id 87
    label "bring"
  ]
  node [
    id 88
    label "przesta&#263;"
  ]
  node [
    id 89
    label "undo"
  ]
  node [
    id 90
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 91
    label "czyn"
  ]
  node [
    id 92
    label "contract"
  ]
  node [
    id 93
    label "gestia_transportowa"
  ]
  node [
    id 94
    label "klauzula"
  ]
  node [
    id 95
    label "porozumienie"
  ]
  node [
    id 96
    label "warunek"
  ]
  node [
    id 97
    label "zawarcie"
  ]
  node [
    id 98
    label "inwestycyjnie"
  ]
  node [
    id 99
    label "uk&#322;ad"
  ]
  node [
    id 100
    label "sta&#263;_si&#281;"
  ]
  node [
    id 101
    label "raptowny"
  ]
  node [
    id 102
    label "embrace"
  ]
  node [
    id 103
    label "pozna&#263;"
  ]
  node [
    id 104
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 105
    label "insert"
  ]
  node [
    id 106
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 107
    label "admit"
  ]
  node [
    id 108
    label "boil"
  ]
  node [
    id 109
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 110
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 111
    label "incorporate"
  ]
  node [
    id 112
    label "wezbra&#263;"
  ]
  node [
    id 113
    label "ustali&#263;"
  ]
  node [
    id 114
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 115
    label "zamkn&#261;&#263;"
  ]
  node [
    id 116
    label "wa&#380;no&#347;&#263;"
  ]
  node [
    id 117
    label "nasycenie"
  ]
  node [
    id 118
    label "wuchta"
  ]
  node [
    id 119
    label "parametr"
  ]
  node [
    id 120
    label "immunity"
  ]
  node [
    id 121
    label "zdolno&#347;&#263;"
  ]
  node [
    id 122
    label "wytrzyma&#322;o&#347;&#263;"
  ]
  node [
    id 123
    label "mn&#243;stwo"
  ]
  node [
    id 124
    label "poj&#281;cie"
  ]
  node [
    id 125
    label "potencja"
  ]
  node [
    id 126
    label "izotonia"
  ]
  node [
    id 127
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 128
    label "nefelometria"
  ]
  node [
    id 129
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 130
    label "czu&#263;"
  ]
  node [
    id 131
    label "need"
  ]
  node [
    id 132
    label "hide"
  ]
  node [
    id 133
    label "support"
  ]
  node [
    id 134
    label "wzi&#261;&#263;"
  ]
  node [
    id 135
    label "catch"
  ]
  node [
    id 136
    label "obecno&#347;&#263;"
  ]
  node [
    id 137
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 138
    label "kwota"
  ]
  node [
    id 139
    label "ilo&#347;&#263;"
  ]
  node [
    id 140
    label "gruba_ryba"
  ]
  node [
    id 141
    label "zwierzchnik"
  ]
  node [
    id 142
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 143
    label "express"
  ]
  node [
    id 144
    label "rzekn&#261;&#263;"
  ]
  node [
    id 145
    label "okre&#347;li&#263;"
  ]
  node [
    id 146
    label "wyrazi&#263;"
  ]
  node [
    id 147
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 148
    label "unwrap"
  ]
  node [
    id 149
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 150
    label "convey"
  ]
  node [
    id 151
    label "discover"
  ]
  node [
    id 152
    label "wydoby&#263;"
  ]
  node [
    id 153
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 154
    label "poda&#263;"
  ]
  node [
    id 155
    label "obramienie"
  ]
  node [
    id 156
    label "forum"
  ]
  node [
    id 157
    label "serwis_internetowy"
  ]
  node [
    id 158
    label "wej&#347;cie"
  ]
  node [
    id 159
    label "Onet"
  ]
  node [
    id 160
    label "archiwolta"
  ]
  node [
    id 161
    label "wynik"
  ]
  node [
    id 162
    label "wyj&#347;cie"
  ]
  node [
    id 163
    label "spe&#322;nienie"
  ]
  node [
    id 164
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 165
    label "po&#322;o&#380;na"
  ]
  node [
    id 166
    label "proces_fizjologiczny"
  ]
  node [
    id 167
    label "przestanie"
  ]
  node [
    id 168
    label "marc&#243;wka"
  ]
  node [
    id 169
    label "usuni&#281;cie"
  ]
  node [
    id 170
    label "uniewa&#380;nienie"
  ]
  node [
    id 171
    label "pomys&#322;"
  ]
  node [
    id 172
    label "birth"
  ]
  node [
    id 173
    label "wymy&#347;lenie"
  ]
  node [
    id 174
    label "po&#322;&#243;g"
  ]
  node [
    id 175
    label "szok_poporodowy"
  ]
  node [
    id 176
    label "event"
  ]
  node [
    id 177
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 178
    label "spos&#243;b"
  ]
  node [
    id 179
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 180
    label "dula"
  ]
  node [
    id 181
    label "nacisn&#261;&#263;"
  ]
  node [
    id 182
    label "zaatakowa&#263;"
  ]
  node [
    id 183
    label "gamble"
  ]
  node [
    id 184
    label "supervene"
  ]
  node [
    id 185
    label "rezultat"
  ]
  node [
    id 186
    label "inability"
  ]
  node [
    id 187
    label "stan"
  ]
  node [
    id 188
    label "brak"
  ]
  node [
    id 189
    label "dawny"
  ]
  node [
    id 190
    label "du&#380;y"
  ]
  node [
    id 191
    label "s&#322;aby"
  ]
  node [
    id 192
    label "oddalony"
  ]
  node [
    id 193
    label "daleko"
  ]
  node [
    id 194
    label "przysz&#322;y"
  ]
  node [
    id 195
    label "ogl&#281;dny"
  ]
  node [
    id 196
    label "r&#243;&#380;ny"
  ]
  node [
    id 197
    label "g&#322;&#281;boki"
  ]
  node [
    id 198
    label "odlegle"
  ]
  node [
    id 199
    label "nieobecny"
  ]
  node [
    id 200
    label "odleg&#322;y"
  ]
  node [
    id 201
    label "d&#322;ugi"
  ]
  node [
    id 202
    label "zwi&#261;zany"
  ]
  node [
    id 203
    label "obcy"
  ]
  node [
    id 204
    label "wsp&#243;&#322;pracownictwo"
  ]
  node [
    id 205
    label "Capital"
  ]
  node [
    id 206
    label "Partners"
  ]
  node [
    id 207
    label "Superstacja"
  ]
  node [
    id 208
    label "sp&#243;&#322;ka"
  ]
  node [
    id 209
    label "zeszyt"
  ]
  node [
    id 210
    label "ojciec"
  ]
  node [
    id 211
    label "Ryszarda"
  ]
  node [
    id 212
    label "Krajewski"
  ]
  node [
    id 213
    label "nasz"
  ]
  node [
    id 214
    label "ukocha&#263;"
  ]
  node [
    id 215
    label "koalicja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 140
  ]
  edge [
    source 22
    target 141
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 23
    target 151
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 23
    target 153
  ]
  edge [
    source 23
    target 154
  ]
  edge [
    source 24
    target 155
  ]
  edge [
    source 24
    target 156
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 158
  ]
  edge [
    source 24
    target 159
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 162
  ]
  edge [
    source 25
    target 163
  ]
  edge [
    source 25
    target 164
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 167
  ]
  edge [
    source 25
    target 168
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 25
    target 172
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 26
    target 182
  ]
  edge [
    source 26
    target 183
  ]
  edge [
    source 26
    target 184
  ]
  edge [
    source 26
    target 109
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 185
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 186
  ]
  edge [
    source 28
    target 187
  ]
  edge [
    source 28
    target 188
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 189
  ]
  edge [
    source 29
    target 190
  ]
  edge [
    source 29
    target 191
  ]
  edge [
    source 29
    target 192
  ]
  edge [
    source 29
    target 193
  ]
  edge [
    source 29
    target 194
  ]
  edge [
    source 29
    target 195
  ]
  edge [
    source 29
    target 196
  ]
  edge [
    source 29
    target 197
  ]
  edge [
    source 29
    target 198
  ]
  edge [
    source 29
    target 199
  ]
  edge [
    source 29
    target 200
  ]
  edge [
    source 29
    target 201
  ]
  edge [
    source 29
    target 202
  ]
  edge [
    source 29
    target 203
  ]
  edge [
    source 30
    target 204
  ]
  edge [
    source 30
    target 137
  ]
  edge [
    source 205
    target 206
  ]
  edge [
    source 207
    target 208
  ]
  edge [
    source 207
    target 209
  ]
  edge [
    source 207
    target 210
  ]
  edge [
    source 208
    target 209
  ]
  edge [
    source 208
    target 210
  ]
  edge [
    source 209
    target 210
  ]
  edge [
    source 210
    target 210
  ]
  edge [
    source 211
    target 212
  ]
  edge [
    source 213
    target 214
  ]
  edge [
    source 213
    target 215
  ]
  edge [
    source 214
    target 215
  ]
]
