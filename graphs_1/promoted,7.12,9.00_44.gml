graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.0545454545454547
  density 0.01884904086738949
  graphCliqueNumber 3
  node [
    id 0
    label "justyn"
    origin "text"
  ]
  node [
    id 1
    label "&#380;y&#322;a"
    origin "text"
  ]
  node [
    id 2
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "&#380;ona"
    origin "text"
  ]
  node [
    id 4
    label "skoczek"
    origin "text"
  ]
  node [
    id 5
    label "narciarski"
    origin "text"
  ]
  node [
    id 6
    label "piotr"
    origin "text"
  ]
  node [
    id 7
    label "swoje"
    origin "text"
  ]
  node [
    id 8
    label "program"
    origin "text"
  ]
  node [
    id 9
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 10
    label "rozwi&#261;zywa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "konflikt"
    origin "text"
  ]
  node [
    id 12
    label "uczestnik"
    origin "text"
  ]
  node [
    id 13
    label "projekt"
    origin "text"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "formacja_geologiczna"
  ]
  node [
    id 16
    label "vein"
  ]
  node [
    id 17
    label "chciwiec"
  ]
  node [
    id 18
    label "dost&#281;p_do&#380;ylny"
  ]
  node [
    id 19
    label "wymagaj&#261;cy"
  ]
  node [
    id 20
    label "lina"
  ]
  node [
    id 21
    label "materialista"
  ]
  node [
    id 22
    label "naczynie"
  ]
  node [
    id 23
    label "okrutnik"
  ]
  node [
    id 24
    label "sk&#261;piarz"
  ]
  node [
    id 25
    label "przew&#243;d"
  ]
  node [
    id 26
    label "sk&#261;py"
  ]
  node [
    id 27
    label "atleta"
  ]
  node [
    id 28
    label "partnerka"
  ]
  node [
    id 29
    label "ma&#322;&#380;onek"
  ]
  node [
    id 30
    label "panna_m&#322;oda"
  ]
  node [
    id 31
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 32
    label "&#347;lubna"
  ]
  node [
    id 33
    label "kobita"
  ]
  node [
    id 34
    label "figura"
  ]
  node [
    id 35
    label "szara&#324;czowate"
  ]
  node [
    id 36
    label "szara&#324;czak"
  ]
  node [
    id 37
    label "sportowiec"
  ]
  node [
    id 38
    label "specjalny"
  ]
  node [
    id 39
    label "zimowy"
  ]
  node [
    id 40
    label "spis"
  ]
  node [
    id 41
    label "odinstalowanie"
  ]
  node [
    id 42
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 43
    label "za&#322;o&#380;enie"
  ]
  node [
    id 44
    label "podstawa"
  ]
  node [
    id 45
    label "emitowanie"
  ]
  node [
    id 46
    label "odinstalowywanie"
  ]
  node [
    id 47
    label "instrukcja"
  ]
  node [
    id 48
    label "punkt"
  ]
  node [
    id 49
    label "teleferie"
  ]
  node [
    id 50
    label "emitowa&#263;"
  ]
  node [
    id 51
    label "wytw&#243;r"
  ]
  node [
    id 52
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 53
    label "sekcja_krytyczna"
  ]
  node [
    id 54
    label "prezentowa&#263;"
  ]
  node [
    id 55
    label "blok"
  ]
  node [
    id 56
    label "podprogram"
  ]
  node [
    id 57
    label "tryb"
  ]
  node [
    id 58
    label "dzia&#322;"
  ]
  node [
    id 59
    label "broszura"
  ]
  node [
    id 60
    label "deklaracja"
  ]
  node [
    id 61
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 62
    label "struktura_organizacyjna"
  ]
  node [
    id 63
    label "zaprezentowanie"
  ]
  node [
    id 64
    label "informatyka"
  ]
  node [
    id 65
    label "booklet"
  ]
  node [
    id 66
    label "menu"
  ]
  node [
    id 67
    label "oprogramowanie"
  ]
  node [
    id 68
    label "instalowanie"
  ]
  node [
    id 69
    label "furkacja"
  ]
  node [
    id 70
    label "odinstalowa&#263;"
  ]
  node [
    id 71
    label "instalowa&#263;"
  ]
  node [
    id 72
    label "okno"
  ]
  node [
    id 73
    label "pirat"
  ]
  node [
    id 74
    label "zainstalowanie"
  ]
  node [
    id 75
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 76
    label "ogranicznik_referencyjny"
  ]
  node [
    id 77
    label "zainstalowa&#263;"
  ]
  node [
    id 78
    label "kana&#322;"
  ]
  node [
    id 79
    label "zaprezentowa&#263;"
  ]
  node [
    id 80
    label "interfejs"
  ]
  node [
    id 81
    label "odinstalowywa&#263;"
  ]
  node [
    id 82
    label "folder"
  ]
  node [
    id 83
    label "course_of_study"
  ]
  node [
    id 84
    label "ram&#243;wka"
  ]
  node [
    id 85
    label "prezentowanie"
  ]
  node [
    id 86
    label "oferta"
  ]
  node [
    id 87
    label "czyj&#347;"
  ]
  node [
    id 88
    label "m&#261;&#380;"
  ]
  node [
    id 89
    label "cope"
  ]
  node [
    id 90
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 91
    label "odkrywa&#263;"
  ]
  node [
    id 92
    label "przestawa&#263;"
  ]
  node [
    id 93
    label "urzeczywistnia&#263;"
  ]
  node [
    id 94
    label "usuwa&#263;"
  ]
  node [
    id 95
    label "undo"
  ]
  node [
    id 96
    label "clash"
  ]
  node [
    id 97
    label "wydarzenie"
  ]
  node [
    id 98
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 99
    label "dokument"
  ]
  node [
    id 100
    label "device"
  ]
  node [
    id 101
    label "program_u&#380;ytkowy"
  ]
  node [
    id 102
    label "intencja"
  ]
  node [
    id 103
    label "agreement"
  ]
  node [
    id 104
    label "pomys&#322;"
  ]
  node [
    id 105
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 106
    label "plan"
  ]
  node [
    id 107
    label "dokumentacja"
  ]
  node [
    id 108
    label "Justyn"
  ]
  node [
    id 109
    label "Piotr"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
]
