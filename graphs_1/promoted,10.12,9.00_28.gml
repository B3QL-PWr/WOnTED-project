graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9565217391304348
  density 0.043478260869565216
  graphCliqueNumber 2
  node [
    id 0
    label "ukra&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "szt"
    origin "text"
  ]
  node [
    id 2
    label "opakowanie"
    origin "text"
  ]
  node [
    id 3
    label "puste"
    origin "text"
  ]
  node [
    id 4
    label "gra"
    origin "text"
  ]
  node [
    id 5
    label "konsola"
    origin "text"
  ]
  node [
    id 6
    label "zabra&#263;"
  ]
  node [
    id 7
    label "zw&#281;dzi&#263;"
  ]
  node [
    id 8
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 9
    label "podpierdoli&#263;"
  ]
  node [
    id 10
    label "pomys&#322;"
  ]
  node [
    id 11
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 12
    label "overcharge"
  ]
  node [
    id 13
    label "dash_off"
  ]
  node [
    id 14
    label "owini&#281;cie"
  ]
  node [
    id 15
    label "zawarto&#347;&#263;"
  ]
  node [
    id 16
    label "popakowanie"
  ]
  node [
    id 17
    label "przedmiot"
  ]
  node [
    id 18
    label "promotion"
  ]
  node [
    id 19
    label "zabawa"
  ]
  node [
    id 20
    label "rywalizacja"
  ]
  node [
    id 21
    label "czynno&#347;&#263;"
  ]
  node [
    id 22
    label "Pok&#233;mon"
  ]
  node [
    id 23
    label "synteza"
  ]
  node [
    id 24
    label "odtworzenie"
  ]
  node [
    id 25
    label "komplet"
  ]
  node [
    id 26
    label "rekwizyt_do_gry"
  ]
  node [
    id 27
    label "odg&#322;os"
  ]
  node [
    id 28
    label "rozgrywka"
  ]
  node [
    id 29
    label "post&#281;powanie"
  ]
  node [
    id 30
    label "wydarzenie"
  ]
  node [
    id 31
    label "apparent_motion"
  ]
  node [
    id 32
    label "game"
  ]
  node [
    id 33
    label "zmienno&#347;&#263;"
  ]
  node [
    id 34
    label "zasada"
  ]
  node [
    id 35
    label "akcja"
  ]
  node [
    id 36
    label "play"
  ]
  node [
    id 37
    label "contest"
  ]
  node [
    id 38
    label "zbijany"
  ]
  node [
    id 39
    label "ozdobny"
  ]
  node [
    id 40
    label "stolik"
  ]
  node [
    id 41
    label "urz&#261;dzenie"
  ]
  node [
    id 42
    label "pulpit"
  ]
  node [
    id 43
    label "wspornik"
  ]
  node [
    id 44
    label "tremo"
  ]
  node [
    id 45
    label "pad"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
]
