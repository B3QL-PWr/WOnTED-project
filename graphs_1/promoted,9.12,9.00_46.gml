graph [
  maxDegree 69
  minDegree 1
  meanDegree 1.9444444444444444
  density 0.027386541471048513
  graphCliqueNumber 2
  node [
    id 0
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 1
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 2
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 3
    label "obszar"
  ]
  node [
    id 4
    label "obiekt_naturalny"
  ]
  node [
    id 5
    label "przedmiot"
  ]
  node [
    id 6
    label "biosfera"
  ]
  node [
    id 7
    label "grupa"
  ]
  node [
    id 8
    label "stw&#243;r"
  ]
  node [
    id 9
    label "Stary_&#346;wiat"
  ]
  node [
    id 10
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 11
    label "rzecz"
  ]
  node [
    id 12
    label "magnetosfera"
  ]
  node [
    id 13
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 14
    label "environment"
  ]
  node [
    id 15
    label "Nowy_&#346;wiat"
  ]
  node [
    id 16
    label "geosfera"
  ]
  node [
    id 17
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 18
    label "planeta"
  ]
  node [
    id 19
    label "przejmowa&#263;"
  ]
  node [
    id 20
    label "litosfera"
  ]
  node [
    id 21
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 22
    label "makrokosmos"
  ]
  node [
    id 23
    label "barysfera"
  ]
  node [
    id 24
    label "biota"
  ]
  node [
    id 25
    label "p&#243;&#322;noc"
  ]
  node [
    id 26
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 27
    label "fauna"
  ]
  node [
    id 28
    label "wszechstworzenie"
  ]
  node [
    id 29
    label "geotermia"
  ]
  node [
    id 30
    label "biegun"
  ]
  node [
    id 31
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 32
    label "ekosystem"
  ]
  node [
    id 33
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 34
    label "teren"
  ]
  node [
    id 35
    label "zjawisko"
  ]
  node [
    id 36
    label "p&#243;&#322;kula"
  ]
  node [
    id 37
    label "atmosfera"
  ]
  node [
    id 38
    label "mikrokosmos"
  ]
  node [
    id 39
    label "class"
  ]
  node [
    id 40
    label "po&#322;udnie"
  ]
  node [
    id 41
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 42
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 43
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 44
    label "przejmowanie"
  ]
  node [
    id 45
    label "przestrze&#324;"
  ]
  node [
    id 46
    label "asymilowanie_si&#281;"
  ]
  node [
    id 47
    label "przej&#261;&#263;"
  ]
  node [
    id 48
    label "ekosfera"
  ]
  node [
    id 49
    label "przyroda"
  ]
  node [
    id 50
    label "ciemna_materia"
  ]
  node [
    id 51
    label "geoida"
  ]
  node [
    id 52
    label "Wsch&#243;d"
  ]
  node [
    id 53
    label "populace"
  ]
  node [
    id 54
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 55
    label "huczek"
  ]
  node [
    id 56
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 57
    label "Ziemia"
  ]
  node [
    id 58
    label "universe"
  ]
  node [
    id 59
    label "ozonosfera"
  ]
  node [
    id 60
    label "rze&#378;ba"
  ]
  node [
    id 61
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 62
    label "zagranica"
  ]
  node [
    id 63
    label "hydrosfera"
  ]
  node [
    id 64
    label "woda"
  ]
  node [
    id 65
    label "kuchnia"
  ]
  node [
    id 66
    label "przej&#281;cie"
  ]
  node [
    id 67
    label "czarna_dziura"
  ]
  node [
    id 68
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 69
    label "morze"
  ]
  node [
    id 70
    label "Arabia"
  ]
  node [
    id 71
    label "saudyjski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 70
    target 71
  ]
]
