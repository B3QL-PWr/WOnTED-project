graph [
  maxDegree 13
  minDegree 1
  meanDegree 2.4827586206896552
  density 0.043557168784029036
  graphCliqueNumber 7
  node [
    id 0
    label "elo"
    origin "text"
  ]
  node [
    id 1
    label "mordeczka"
    origin "text"
  ]
  node [
    id 2
    label "kto"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "niegrzeczny"
    origin "text"
  ]
  node [
    id 5
    label "darmowy"
    origin "text"
  ]
  node [
    id 6
    label "wysy&#322;ka"
    origin "text"
  ]
  node [
    id 7
    label "&#380;elek"
    origin "text"
  ]
  node [
    id 8
    label "gusto"
    origin "text"
  ]
  node [
    id 9
    label "rozdajo"
    origin "text"
  ]
  node [
    id 10
    label "tutaj"
    origin "text"
  ]
  node [
    id 11
    label "miko&#322;aj"
    origin "text"
  ]
  node [
    id 12
    label "ecco"
    origin "text"
  ]
  node [
    id 13
    label "gruba"
    origin "text"
  ]
  node [
    id 14
    label "broda"
    origin "text"
  ]
  node [
    id 15
    label "wszystko"
    origin "text"
  ]
  node [
    id 16
    label "si&#281;"
    origin "text"
  ]
  node [
    id 17
    label "zgadza&#263;"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;ga&#263;"
  ]
  node [
    id 19
    label "trwa&#263;"
  ]
  node [
    id 20
    label "obecno&#347;&#263;"
  ]
  node [
    id 21
    label "stan"
  ]
  node [
    id 22
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 23
    label "stand"
  ]
  node [
    id 24
    label "mie&#263;_miejsce"
  ]
  node [
    id 25
    label "uczestniczy&#263;"
  ]
  node [
    id 26
    label "chodzi&#263;"
  ]
  node [
    id 27
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 28
    label "equal"
  ]
  node [
    id 29
    label "trudny"
  ]
  node [
    id 30
    label "niezno&#347;ny"
  ]
  node [
    id 31
    label "niestosowny"
  ]
  node [
    id 32
    label "niegrzecznie"
  ]
  node [
    id 33
    label "niepos&#322;uszny"
  ]
  node [
    id 34
    label "brzydal"
  ]
  node [
    id 35
    label "darmowo"
  ]
  node [
    id 36
    label "package"
  ]
  node [
    id 37
    label "transport"
  ]
  node [
    id 38
    label "cukierek"
  ]
  node [
    id 39
    label "tam"
  ]
  node [
    id 40
    label "zarost"
  ]
  node [
    id 41
    label "sier&#347;&#263;"
  ]
  node [
    id 42
    label "twarz"
  ]
  node [
    id 43
    label "lock"
  ]
  node [
    id 44
    label "absolut"
  ]
  node [
    id 45
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 46
    label "zgodzi&#263;"
  ]
  node [
    id 47
    label "assent"
  ]
  node [
    id 48
    label "zgadzanie"
  ]
  node [
    id 49
    label "zatrudnia&#263;"
  ]
  node [
    id 50
    label "XD"
  ]
  node [
    id 51
    label "D"
  ]
  node [
    id 52
    label "z"
  ]
  node [
    id 53
    label "i"
  ]
  node [
    id 54
    label "e"
  ]
  node [
    id 55
    label "sekunda"
  ]
  node [
    id 56
    label "&#280;"
  ]
  node [
    id 57
    label "&#262;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 40
  ]
  edge [
    source 14
    target 41
  ]
  edge [
    source 14
    target 42
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 43
  ]
  edge [
    source 15
    target 44
  ]
  edge [
    source 15
    target 45
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 46
  ]
  edge [
    source 17
    target 47
  ]
  edge [
    source 17
    target 48
  ]
  edge [
    source 17
    target 49
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 51
    target 56
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 52
    target 57
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 53
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 56
    target 57
  ]
]
