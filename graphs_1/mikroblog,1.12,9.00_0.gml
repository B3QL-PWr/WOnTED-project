graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "uwaga"
    origin "text"
  ]
  node [
    id 1
    label "rebus"
    origin "text"
  ]
  node [
    id 2
    label "nagana"
  ]
  node [
    id 3
    label "wypowied&#378;"
  ]
  node [
    id 4
    label "stan"
  ]
  node [
    id 5
    label "dzienniczek"
  ]
  node [
    id 6
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 7
    label "wzgl&#261;d"
  ]
  node [
    id 8
    label "gossip"
  ]
  node [
    id 9
    label "upomnienie"
  ]
  node [
    id 10
    label "tekst"
  ]
  node [
    id 11
    label "&#322;amig&#322;&#243;wka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 11
  ]
]
