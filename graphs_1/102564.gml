graph [
  maxDegree 70
  minDegree 1
  meanDegree 2.198529411764706
  density 0.008112654655958325
  graphCliqueNumber 4
  node [
    id 0
    label "wymaga&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dokument"
    origin "text"
  ]
  node [
    id 2
    label "ustalenie"
    origin "text"
  ]
  node [
    id 3
    label "prawa"
    origin "text"
  ]
  node [
    id 4
    label "zasi&#322;ek"
    origin "text"
  ]
  node [
    id 5
    label "rodzinny"
    origin "text"
  ]
  node [
    id 6
    label "oraz"
    origin "text"
  ]
  node [
    id 7
    label "wniosek"
    origin "text"
  ]
  node [
    id 8
    label "dodatek"
    origin "text"
  ]
  node [
    id 9
    label "o&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 10
    label "cz&#322;onek"
    origin "text"
  ]
  node [
    id 11
    label "rodzina"
    origin "text"
  ]
  node [
    id 12
    label "wysoko&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "doch&#243;d"
    origin "text"
  ]
  node [
    id 14
    label "podlega&#263;"
    origin "text"
  ]
  node [
    id 15
    label "opodatkowanie"
    origin "text"
  ]
  node [
    id 16
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 17
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 18
    label "uzyska&#263;"
    origin "text"
  ]
  node [
    id 19
    label "rok"
    origin "text"
  ]
  node [
    id 20
    label "kalendarzowy"
    origin "text"
  ]
  node [
    id 21
    label "poprzedza&#263;"
    origin "text"
  ]
  node [
    id 22
    label "okres"
    origin "text"
  ]
  node [
    id 23
    label "zasi&#322;kowy"
    origin "text"
  ]
  node [
    id 24
    label "niezb&#281;dny"
    origin "text"
  ]
  node [
    id 25
    label "dla"
    origin "text"
  ]
  node [
    id 26
    label "prawid&#322;owy"
    origin "text"
  ]
  node [
    id 27
    label "nadanie"
    origin "text"
  ]
  node [
    id 28
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 29
    label "zmusza&#263;"
  ]
  node [
    id 30
    label "by&#263;"
  ]
  node [
    id 31
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 32
    label "claim"
  ]
  node [
    id 33
    label "force"
  ]
  node [
    id 34
    label "take"
  ]
  node [
    id 35
    label "record"
  ]
  node [
    id 36
    label "wytw&#243;r"
  ]
  node [
    id 37
    label "&#347;wiadectwo"
  ]
  node [
    id 38
    label "zapis"
  ]
  node [
    id 39
    label "fascyku&#322;"
  ]
  node [
    id 40
    label "raport&#243;wka"
  ]
  node [
    id 41
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 42
    label "artyku&#322;"
  ]
  node [
    id 43
    label "plik"
  ]
  node [
    id 44
    label "writing"
  ]
  node [
    id 45
    label "utw&#243;r"
  ]
  node [
    id 46
    label "dokumentacja"
  ]
  node [
    id 47
    label "parafa"
  ]
  node [
    id 48
    label "sygnatariusz"
  ]
  node [
    id 49
    label "registratura"
  ]
  node [
    id 50
    label "zrobienie"
  ]
  node [
    id 51
    label "zdecydowanie"
  ]
  node [
    id 52
    label "czynno&#347;&#263;"
  ]
  node [
    id 53
    label "spowodowanie"
  ]
  node [
    id 54
    label "appointment"
  ]
  node [
    id 55
    label "localization"
  ]
  node [
    id 56
    label "informacja"
  ]
  node [
    id 57
    label "decyzja"
  ]
  node [
    id 58
    label "umocnienie"
  ]
  node [
    id 59
    label "zapomoga"
  ]
  node [
    id 60
    label "familijnie"
  ]
  node [
    id 61
    label "rodzinnie"
  ]
  node [
    id 62
    label "przyjemny"
  ]
  node [
    id 63
    label "wsp&#243;lny"
  ]
  node [
    id 64
    label "charakterystyczny"
  ]
  node [
    id 65
    label "swobodny"
  ]
  node [
    id 66
    label "zwi&#261;zany"
  ]
  node [
    id 67
    label "towarzyski"
  ]
  node [
    id 68
    label "ciep&#322;y"
  ]
  node [
    id 69
    label "twierdzenie"
  ]
  node [
    id 70
    label "my&#347;l"
  ]
  node [
    id 71
    label "wnioskowanie"
  ]
  node [
    id 72
    label "propozycja"
  ]
  node [
    id 73
    label "motion"
  ]
  node [
    id 74
    label "pismo"
  ]
  node [
    id 75
    label "prayer"
  ]
  node [
    id 76
    label "doj&#347;cie"
  ]
  node [
    id 77
    label "doj&#347;&#263;"
  ]
  node [
    id 78
    label "przedmiot"
  ]
  node [
    id 79
    label "dochodzenie"
  ]
  node [
    id 80
    label "dziennik"
  ]
  node [
    id 81
    label "aneks"
  ]
  node [
    id 82
    label "rzecz"
  ]
  node [
    id 83
    label "element"
  ]
  node [
    id 84
    label "galanteria"
  ]
  node [
    id 85
    label "resolution"
  ]
  node [
    id 86
    label "wypowied&#378;"
  ]
  node [
    id 87
    label "poinformowanie"
  ]
  node [
    id 88
    label "announcement"
  ]
  node [
    id 89
    label "zwiastowanie"
  ]
  node [
    id 90
    label "komunikat"
  ]
  node [
    id 91
    label "statement"
  ]
  node [
    id 92
    label "cz&#322;owiek"
  ]
  node [
    id 93
    label "cia&#322;o"
  ]
  node [
    id 94
    label "organizacja"
  ]
  node [
    id 95
    label "przedstawiciel"
  ]
  node [
    id 96
    label "shaft"
  ]
  node [
    id 97
    label "podmiot"
  ]
  node [
    id 98
    label "fiut"
  ]
  node [
    id 99
    label "przyrodzenie"
  ]
  node [
    id 100
    label "wchodzenie"
  ]
  node [
    id 101
    label "grupa"
  ]
  node [
    id 102
    label "ptaszek"
  ]
  node [
    id 103
    label "organ"
  ]
  node [
    id 104
    label "wej&#347;cie"
  ]
  node [
    id 105
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 106
    label "element_anatomiczny"
  ]
  node [
    id 107
    label "krewni"
  ]
  node [
    id 108
    label "Firlejowie"
  ]
  node [
    id 109
    label "Ossoli&#324;scy"
  ]
  node [
    id 110
    label "rodze&#324;stwo"
  ]
  node [
    id 111
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 112
    label "rz&#261;d"
  ]
  node [
    id 113
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 114
    label "przyjaciel_domu"
  ]
  node [
    id 115
    label "Ostrogscy"
  ]
  node [
    id 116
    label "theater"
  ]
  node [
    id 117
    label "dom_rodzinny"
  ]
  node [
    id 118
    label "potomstwo"
  ]
  node [
    id 119
    label "Soplicowie"
  ]
  node [
    id 120
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 121
    label "Czartoryscy"
  ]
  node [
    id 122
    label "family"
  ]
  node [
    id 123
    label "kin"
  ]
  node [
    id 124
    label "bliscy"
  ]
  node [
    id 125
    label "powinowaci"
  ]
  node [
    id 126
    label "Sapiehowie"
  ]
  node [
    id 127
    label "ordynacja"
  ]
  node [
    id 128
    label "jednostka_systematyczna"
  ]
  node [
    id 129
    label "zbi&#243;r"
  ]
  node [
    id 130
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 131
    label "Kossakowie"
  ]
  node [
    id 132
    label "rodzice"
  ]
  node [
    id 133
    label "dom"
  ]
  node [
    id 134
    label "tallness"
  ]
  node [
    id 135
    label "sum"
  ]
  node [
    id 136
    label "degree"
  ]
  node [
    id 137
    label "brzmienie"
  ]
  node [
    id 138
    label "altitude"
  ]
  node [
    id 139
    label "odcinek"
  ]
  node [
    id 140
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 141
    label "rozmiar"
  ]
  node [
    id 142
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 143
    label "k&#261;t"
  ]
  node [
    id 144
    label "wielko&#347;&#263;"
  ]
  node [
    id 145
    label "korzy&#347;&#263;"
  ]
  node [
    id 146
    label "krzywa_Engla"
  ]
  node [
    id 147
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 148
    label "wp&#322;yw"
  ]
  node [
    id 149
    label "income"
  ]
  node [
    id 150
    label "stopa_procentowa"
  ]
  node [
    id 151
    label "czu&#263;"
  ]
  node [
    id 152
    label "zale&#380;e&#263;"
  ]
  node [
    id 153
    label "doznawa&#263;"
  ]
  node [
    id 154
    label "op&#322;ata"
  ]
  node [
    id 155
    label "danina"
  ]
  node [
    id 156
    label "tax"
  ]
  node [
    id 157
    label "trybut"
  ]
  node [
    id 158
    label "obci&#261;&#380;enie"
  ]
  node [
    id 159
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 160
    label "proceed"
  ]
  node [
    id 161
    label "catch"
  ]
  node [
    id 162
    label "pozosta&#263;"
  ]
  node [
    id 163
    label "osta&#263;_si&#281;"
  ]
  node [
    id 164
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 165
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 166
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 167
    label "change"
  ]
  node [
    id 168
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 169
    label "promocja"
  ]
  node [
    id 170
    label "give_birth"
  ]
  node [
    id 171
    label "wytworzy&#263;"
  ]
  node [
    id 172
    label "realize"
  ]
  node [
    id 173
    label "zrobi&#263;"
  ]
  node [
    id 174
    label "make"
  ]
  node [
    id 175
    label "stulecie"
  ]
  node [
    id 176
    label "kalendarz"
  ]
  node [
    id 177
    label "czas"
  ]
  node [
    id 178
    label "pora_roku"
  ]
  node [
    id 179
    label "cykl_astronomiczny"
  ]
  node [
    id 180
    label "p&#243;&#322;rocze"
  ]
  node [
    id 181
    label "kwarta&#322;"
  ]
  node [
    id 182
    label "kurs"
  ]
  node [
    id 183
    label "jubileusz"
  ]
  node [
    id 184
    label "miesi&#261;c"
  ]
  node [
    id 185
    label "lata"
  ]
  node [
    id 186
    label "martwy_sezon"
  ]
  node [
    id 187
    label "opatrywa&#263;"
  ]
  node [
    id 188
    label "paleogen"
  ]
  node [
    id 189
    label "spell"
  ]
  node [
    id 190
    label "period"
  ]
  node [
    id 191
    label "prekambr"
  ]
  node [
    id 192
    label "jura"
  ]
  node [
    id 193
    label "interstadia&#322;"
  ]
  node [
    id 194
    label "jednostka_geologiczna"
  ]
  node [
    id 195
    label "izochronizm"
  ]
  node [
    id 196
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 197
    label "okres_noachijski"
  ]
  node [
    id 198
    label "orosir"
  ]
  node [
    id 199
    label "kreda"
  ]
  node [
    id 200
    label "sten"
  ]
  node [
    id 201
    label "drugorz&#281;d"
  ]
  node [
    id 202
    label "semester"
  ]
  node [
    id 203
    label "trzeciorz&#281;d"
  ]
  node [
    id 204
    label "ton"
  ]
  node [
    id 205
    label "dzieje"
  ]
  node [
    id 206
    label "poprzednik"
  ]
  node [
    id 207
    label "ordowik"
  ]
  node [
    id 208
    label "karbon"
  ]
  node [
    id 209
    label "trias"
  ]
  node [
    id 210
    label "kalim"
  ]
  node [
    id 211
    label "stater"
  ]
  node [
    id 212
    label "era"
  ]
  node [
    id 213
    label "cykl"
  ]
  node [
    id 214
    label "p&#243;&#322;okres"
  ]
  node [
    id 215
    label "czwartorz&#281;d"
  ]
  node [
    id 216
    label "pulsacja"
  ]
  node [
    id 217
    label "okres_amazo&#324;ski"
  ]
  node [
    id 218
    label "kambr"
  ]
  node [
    id 219
    label "Zeitgeist"
  ]
  node [
    id 220
    label "nast&#281;pnik"
  ]
  node [
    id 221
    label "kriogen"
  ]
  node [
    id 222
    label "glacja&#322;"
  ]
  node [
    id 223
    label "fala"
  ]
  node [
    id 224
    label "okres_czasu"
  ]
  node [
    id 225
    label "riak"
  ]
  node [
    id 226
    label "schy&#322;ek"
  ]
  node [
    id 227
    label "okres_hesperyjski"
  ]
  node [
    id 228
    label "sylur"
  ]
  node [
    id 229
    label "dewon"
  ]
  node [
    id 230
    label "ciota"
  ]
  node [
    id 231
    label "epoka"
  ]
  node [
    id 232
    label "pierwszorz&#281;d"
  ]
  node [
    id 233
    label "okres_halsztacki"
  ]
  node [
    id 234
    label "ektas"
  ]
  node [
    id 235
    label "zdanie"
  ]
  node [
    id 236
    label "condition"
  ]
  node [
    id 237
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 238
    label "rok_akademicki"
  ]
  node [
    id 239
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 240
    label "postglacja&#322;"
  ]
  node [
    id 241
    label "faza"
  ]
  node [
    id 242
    label "proces_fizjologiczny"
  ]
  node [
    id 243
    label "ediakar"
  ]
  node [
    id 244
    label "time_period"
  ]
  node [
    id 245
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 246
    label "perm"
  ]
  node [
    id 247
    label "rok_szkolny"
  ]
  node [
    id 248
    label "neogen"
  ]
  node [
    id 249
    label "sider"
  ]
  node [
    id 250
    label "flow"
  ]
  node [
    id 251
    label "podokres"
  ]
  node [
    id 252
    label "preglacja&#322;"
  ]
  node [
    id 253
    label "retoryka"
  ]
  node [
    id 254
    label "choroba_przyrodzona"
  ]
  node [
    id 255
    label "niezb&#281;dnie"
  ]
  node [
    id 256
    label "symetryczny"
  ]
  node [
    id 257
    label "po&#380;&#261;dany"
  ]
  node [
    id 258
    label "dobry"
  ]
  node [
    id 259
    label "s&#322;uszny"
  ]
  node [
    id 260
    label "prawid&#322;owo"
  ]
  node [
    id 261
    label "nazwanie"
  ]
  node [
    id 262
    label "akt"
  ]
  node [
    id 263
    label "broadcast"
  ]
  node [
    id 264
    label "denomination"
  ]
  node [
    id 265
    label "przyznanie"
  ]
  node [
    id 266
    label "zdarzenie_si&#281;"
  ]
  node [
    id 267
    label "przes&#322;anie"
  ]
  node [
    id 268
    label "title"
  ]
  node [
    id 269
    label "law"
  ]
  node [
    id 270
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 271
    label "authorization"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 5
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 27
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 101
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 205
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 22
    target 208
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 217
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 219
  ]
  edge [
    source 22
    target 220
  ]
  edge [
    source 22
    target 221
  ]
  edge [
    source 22
    target 222
  ]
  edge [
    source 22
    target 223
  ]
  edge [
    source 22
    target 224
  ]
  edge [
    source 22
    target 225
  ]
  edge [
    source 22
    target 226
  ]
  edge [
    source 22
    target 227
  ]
  edge [
    source 22
    target 228
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 22
    target 231
  ]
  edge [
    source 22
    target 232
  ]
  edge [
    source 22
    target 233
  ]
  edge [
    source 22
    target 234
  ]
  edge [
    source 22
    target 235
  ]
  edge [
    source 22
    target 236
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 22
    target 238
  ]
  edge [
    source 22
    target 239
  ]
  edge [
    source 22
    target 240
  ]
  edge [
    source 22
    target 241
  ]
  edge [
    source 22
    target 242
  ]
  edge [
    source 22
    target 243
  ]
  edge [
    source 22
    target 244
  ]
  edge [
    source 22
    target 245
  ]
  edge [
    source 22
    target 246
  ]
  edge [
    source 22
    target 247
  ]
  edge [
    source 22
    target 248
  ]
  edge [
    source 22
    target 249
  ]
  edge [
    source 22
    target 250
  ]
  edge [
    source 22
    target 251
  ]
  edge [
    source 22
    target 252
  ]
  edge [
    source 22
    target 253
  ]
  edge [
    source 22
    target 254
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 255
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 256
  ]
  edge [
    source 26
    target 257
  ]
  edge [
    source 26
    target 258
  ]
  edge [
    source 26
    target 259
  ]
  edge [
    source 26
    target 260
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 52
  ]
  edge [
    source 27
    target 53
  ]
  edge [
    source 27
    target 261
  ]
  edge [
    source 27
    target 262
  ]
  edge [
    source 27
    target 263
  ]
  edge [
    source 27
    target 264
  ]
  edge [
    source 27
    target 265
  ]
  edge [
    source 27
    target 266
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 28
    target 52
  ]
  edge [
    source 28
    target 53
  ]
  edge [
    source 28
    target 268
  ]
  edge [
    source 28
    target 269
  ]
  edge [
    source 28
    target 270
  ]
  edge [
    source 28
    target 271
  ]
]
