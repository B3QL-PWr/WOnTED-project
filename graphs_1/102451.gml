graph [
  maxDegree 868
  minDegree 1
  meanDegree 2.0304054054054053
  density 0.0017163190240113318
  graphCliqueNumber 3
  node [
    id 0
    label "listening"
    origin "text"
  ]
  node [
    id 1
    label "post"
    origin "text"
  ]
  node [
    id 2
    label "donosi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 4
    label "kraftwerk"
    origin "text"
  ]
  node [
    id 5
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 6
    label "udost&#281;pni&#263;"
    origin "text"
  ]
  node [
    id 7
    label "strona"
    origin "text"
  ]
  node [
    id 8
    label "projekt"
    origin "text"
  ]
  node [
    id 9
    label "stop"
    origin "text"
  ]
  node [
    id 10
    label "rokkasho"
    origin "text"
  ]
  node [
    id 11
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 12
    label "legendarny"
    origin "text"
  ]
  node [
    id 13
    label "utw&#243;r"
    origin "text"
  ]
  node [
    id 14
    label "activity"
    origin "text"
  ]
  node [
    id 15
    label "creative"
    origin "text"
  ]
  node [
    id 16
    label "commons"
    origin "text"
  ]
  node [
    id 17
    label "sampling"
    origin "text"
  ]
  node [
    id 18
    label "za&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 19
    label "japo&#324;ski"
    origin "text"
  ]
  node [
    id 20
    label "muzyk"
    origin "text"
  ]
  node [
    id 21
    label "ryuichi"
    origin "text"
  ]
  node [
    id 22
    label "sakamoto"
    origin "text"
  ]
  node [
    id 23
    label "jako"
    origin "text"
  ]
  node [
    id 24
    label "forma"
    origin "text"
  ]
  node [
    id 25
    label "protest"
    origin "text"
  ]
  node [
    id 26
    label "przeciw"
    origin "text"
  ]
  node [
    id 27
    label "budowa"
    origin "text"
  ]
  node [
    id 28
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 29
    label "przetwarzanie"
    origin "text"
  ]
  node [
    id 30
    label "materia&#322;"
    origin "text"
  ]
  node [
    id 31
    label "radioaktywny"
    origin "text"
  ]
  node [
    id 32
    label "wioska"
    origin "text"
  ]
  node [
    id 33
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 34
    label "ten"
    origin "text"
  ]
  node [
    id 35
    label "temat"
    origin "text"
  ]
  node [
    id 36
    label "przeczyta&#263;"
    origin "text"
  ]
  node [
    id 37
    label "machine"
    origin "text"
  ]
  node [
    id 38
    label "zachowa&#263;"
  ]
  node [
    id 39
    label "zachowanie"
  ]
  node [
    id 40
    label "czas"
  ]
  node [
    id 41
    label "zachowywa&#263;"
  ]
  node [
    id 42
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 43
    label "rok_ko&#347;cielny"
  ]
  node [
    id 44
    label "praktyka"
  ]
  node [
    id 45
    label "zachowywanie"
  ]
  node [
    id 46
    label "tekst"
  ]
  node [
    id 47
    label "zu&#380;y&#263;"
  ]
  node [
    id 48
    label "get"
  ]
  node [
    id 49
    label "render"
  ]
  node [
    id 50
    label "ci&#261;&#380;a"
  ]
  node [
    id 51
    label "informowa&#263;"
  ]
  node [
    id 52
    label "zanosi&#263;"
  ]
  node [
    id 53
    label "introduce"
  ]
  node [
    id 54
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 55
    label "spill_the_beans"
  ]
  node [
    id 56
    label "give"
  ]
  node [
    id 57
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 58
    label "inform"
  ]
  node [
    id 59
    label "przeby&#263;"
  ]
  node [
    id 60
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 61
    label "whole"
  ]
  node [
    id 62
    label "odm&#322;adza&#263;"
  ]
  node [
    id 63
    label "zabudowania"
  ]
  node [
    id 64
    label "odm&#322;odzenie"
  ]
  node [
    id 65
    label "zespolik"
  ]
  node [
    id 66
    label "skupienie"
  ]
  node [
    id 67
    label "schorzenie"
  ]
  node [
    id 68
    label "grupa"
  ]
  node [
    id 69
    label "Depeche_Mode"
  ]
  node [
    id 70
    label "Mazowsze"
  ]
  node [
    id 71
    label "ro&#347;lina"
  ]
  node [
    id 72
    label "zbi&#243;r"
  ]
  node [
    id 73
    label "The_Beatles"
  ]
  node [
    id 74
    label "group"
  ]
  node [
    id 75
    label "&#346;wietliki"
  ]
  node [
    id 76
    label "odm&#322;adzanie"
  ]
  node [
    id 77
    label "batch"
  ]
  node [
    id 78
    label "dok&#322;adnie"
  ]
  node [
    id 79
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 80
    label "open"
  ]
  node [
    id 81
    label "skr&#281;canie"
  ]
  node [
    id 82
    label "voice"
  ]
  node [
    id 83
    label "internet"
  ]
  node [
    id 84
    label "skr&#281;ci&#263;"
  ]
  node [
    id 85
    label "kartka"
  ]
  node [
    id 86
    label "orientowa&#263;"
  ]
  node [
    id 87
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 88
    label "powierzchnia"
  ]
  node [
    id 89
    label "plik"
  ]
  node [
    id 90
    label "bok"
  ]
  node [
    id 91
    label "pagina"
  ]
  node [
    id 92
    label "orientowanie"
  ]
  node [
    id 93
    label "fragment"
  ]
  node [
    id 94
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 95
    label "s&#261;d"
  ]
  node [
    id 96
    label "skr&#281;ca&#263;"
  ]
  node [
    id 97
    label "g&#243;ra"
  ]
  node [
    id 98
    label "serwis_internetowy"
  ]
  node [
    id 99
    label "orientacja"
  ]
  node [
    id 100
    label "linia"
  ]
  node [
    id 101
    label "skr&#281;cenie"
  ]
  node [
    id 102
    label "layout"
  ]
  node [
    id 103
    label "zorientowa&#263;"
  ]
  node [
    id 104
    label "zorientowanie"
  ]
  node [
    id 105
    label "obiekt"
  ]
  node [
    id 106
    label "podmiot"
  ]
  node [
    id 107
    label "ty&#322;"
  ]
  node [
    id 108
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 109
    label "logowanie"
  ]
  node [
    id 110
    label "adres_internetowy"
  ]
  node [
    id 111
    label "uj&#281;cie"
  ]
  node [
    id 112
    label "prz&#243;d"
  ]
  node [
    id 113
    label "posta&#263;"
  ]
  node [
    id 114
    label "dokument"
  ]
  node [
    id 115
    label "device"
  ]
  node [
    id 116
    label "program_u&#380;ytkowy"
  ]
  node [
    id 117
    label "intencja"
  ]
  node [
    id 118
    label "agreement"
  ]
  node [
    id 119
    label "pomys&#322;"
  ]
  node [
    id 120
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 121
    label "plan"
  ]
  node [
    id 122
    label "dokumentacja"
  ]
  node [
    id 123
    label "mieszanina"
  ]
  node [
    id 124
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 125
    label "przesycenie"
  ]
  node [
    id 126
    label "przesyci&#263;"
  ]
  node [
    id 127
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 128
    label "alia&#380;"
  ]
  node [
    id 129
    label "struktura_metalu"
  ]
  node [
    id 130
    label "znak_nakazu"
  ]
  node [
    id 131
    label "przesyca&#263;"
  ]
  node [
    id 132
    label "reflektor"
  ]
  node [
    id 133
    label "przesycanie"
  ]
  node [
    id 134
    label "cz&#322;owiek"
  ]
  node [
    id 135
    label "bli&#378;ni"
  ]
  node [
    id 136
    label "odpowiedni"
  ]
  node [
    id 137
    label "swojak"
  ]
  node [
    id 138
    label "samodzielny"
  ]
  node [
    id 139
    label "mitycznie"
  ]
  node [
    id 140
    label "s&#322;ynny"
  ]
  node [
    id 141
    label "nieprawdziwy"
  ]
  node [
    id 142
    label "tre&#347;&#263;"
  ]
  node [
    id 143
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 144
    label "obrazowanie"
  ]
  node [
    id 145
    label "part"
  ]
  node [
    id 146
    label "organ"
  ]
  node [
    id 147
    label "komunikat"
  ]
  node [
    id 148
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 149
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 150
    label "element_anatomiczny"
  ]
  node [
    id 151
    label "miks"
  ]
  node [
    id 152
    label "miksowa&#263;"
  ]
  node [
    id 153
    label "emisja"
  ]
  node [
    id 154
    label "technika"
  ]
  node [
    id 155
    label "przer&#243;bka"
  ]
  node [
    id 156
    label "archeologia"
  ]
  node [
    id 157
    label "insert"
  ]
  node [
    id 158
    label "utworzy&#263;"
  ]
  node [
    id 159
    label "ubra&#263;"
  ]
  node [
    id 160
    label "set"
  ]
  node [
    id 161
    label "invest"
  ]
  node [
    id 162
    label "pokry&#263;"
  ]
  node [
    id 163
    label "przewidzie&#263;"
  ]
  node [
    id 164
    label "umie&#347;ci&#263;"
  ]
  node [
    id 165
    label "map"
  ]
  node [
    id 166
    label "load"
  ]
  node [
    id 167
    label "zap&#322;aci&#263;"
  ]
  node [
    id 168
    label "oblec_si&#281;"
  ]
  node [
    id 169
    label "podwin&#261;&#263;"
  ]
  node [
    id 170
    label "plant"
  ]
  node [
    id 171
    label "create"
  ]
  node [
    id 172
    label "zrobi&#263;"
  ]
  node [
    id 173
    label "str&#243;j"
  ]
  node [
    id 174
    label "jell"
  ]
  node [
    id 175
    label "spowodowa&#263;"
  ]
  node [
    id 176
    label "oblec"
  ]
  node [
    id 177
    label "przyodzia&#263;"
  ]
  node [
    id 178
    label "install"
  ]
  node [
    id 179
    label "Japanese"
  ]
  node [
    id 180
    label "po_japo&#324;sku"
  ]
  node [
    id 181
    label "katsudon"
  ]
  node [
    id 182
    label "j&#281;zyk"
  ]
  node [
    id 183
    label "futon"
  ]
  node [
    id 184
    label "japo&#324;sko"
  ]
  node [
    id 185
    label "hanafuda"
  ]
  node [
    id 186
    label "ky&#363;d&#333;"
  ]
  node [
    id 187
    label "j&#281;zyk_izolowany"
  ]
  node [
    id 188
    label "ju-jitsu"
  ]
  node [
    id 189
    label "karate"
  ]
  node [
    id 190
    label "sh&#333;gi"
  ]
  node [
    id 191
    label "azjatycki"
  ]
  node [
    id 192
    label "ikebana"
  ]
  node [
    id 193
    label "dalekowschodni"
  ]
  node [
    id 194
    label "artysta"
  ]
  node [
    id 195
    label "wykonawca"
  ]
  node [
    id 196
    label "nauczyciel"
  ]
  node [
    id 197
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 198
    label "punkt_widzenia"
  ]
  node [
    id 199
    label "do&#322;ek"
  ]
  node [
    id 200
    label "formality"
  ]
  node [
    id 201
    label "wz&#243;r"
  ]
  node [
    id 202
    label "kantyzm"
  ]
  node [
    id 203
    label "ornamentyka"
  ]
  node [
    id 204
    label "odmiana"
  ]
  node [
    id 205
    label "mode"
  ]
  node [
    id 206
    label "style"
  ]
  node [
    id 207
    label "formacja"
  ]
  node [
    id 208
    label "maszyna_drukarska"
  ]
  node [
    id 209
    label "poznanie"
  ]
  node [
    id 210
    label "szablon"
  ]
  node [
    id 211
    label "struktura"
  ]
  node [
    id 212
    label "spirala"
  ]
  node [
    id 213
    label "blaszka"
  ]
  node [
    id 214
    label "linearno&#347;&#263;"
  ]
  node [
    id 215
    label "stan"
  ]
  node [
    id 216
    label "cecha"
  ]
  node [
    id 217
    label "g&#322;owa"
  ]
  node [
    id 218
    label "kielich"
  ]
  node [
    id 219
    label "zdolno&#347;&#263;"
  ]
  node [
    id 220
    label "poj&#281;cie"
  ]
  node [
    id 221
    label "kszta&#322;t"
  ]
  node [
    id 222
    label "pasmo"
  ]
  node [
    id 223
    label "rdze&#324;"
  ]
  node [
    id 224
    label "leksem"
  ]
  node [
    id 225
    label "dyspozycja"
  ]
  node [
    id 226
    label "wygl&#261;d"
  ]
  node [
    id 227
    label "October"
  ]
  node [
    id 228
    label "zawarto&#347;&#263;"
  ]
  node [
    id 229
    label "creation"
  ]
  node [
    id 230
    label "p&#281;tla"
  ]
  node [
    id 231
    label "p&#322;at"
  ]
  node [
    id 232
    label "gwiazda"
  ]
  node [
    id 233
    label "arystotelizm"
  ]
  node [
    id 234
    label "dzie&#322;o"
  ]
  node [
    id 235
    label "naczynie"
  ]
  node [
    id 236
    label "wyra&#380;enie"
  ]
  node [
    id 237
    label "jednostka_systematyczna"
  ]
  node [
    id 238
    label "miniatura"
  ]
  node [
    id 239
    label "zwyczaj"
  ]
  node [
    id 240
    label "morfem"
  ]
  node [
    id 241
    label "reakcja"
  ]
  node [
    id 242
    label "protestacja"
  ]
  node [
    id 243
    label "czerwona_kartka"
  ]
  node [
    id 244
    label "figura"
  ]
  node [
    id 245
    label "wjazd"
  ]
  node [
    id 246
    label "konstrukcja"
  ]
  node [
    id 247
    label "r&#243;w"
  ]
  node [
    id 248
    label "kreacja"
  ]
  node [
    id 249
    label "posesja"
  ]
  node [
    id 250
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 251
    label "mechanika"
  ]
  node [
    id 252
    label "zwierz&#281;"
  ]
  node [
    id 253
    label "miejsce_pracy"
  ]
  node [
    id 254
    label "praca"
  ]
  node [
    id 255
    label "constitution"
  ]
  node [
    id 256
    label "miejsce"
  ]
  node [
    id 257
    label "Hollywood"
  ]
  node [
    id 258
    label "zal&#261;&#380;ek"
  ]
  node [
    id 259
    label "otoczenie"
  ]
  node [
    id 260
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 261
    label "&#347;rodek"
  ]
  node [
    id 262
    label "center"
  ]
  node [
    id 263
    label "instytucja"
  ]
  node [
    id 264
    label "skupisko"
  ]
  node [
    id 265
    label "warunki"
  ]
  node [
    id 266
    label "przerabianie"
  ]
  node [
    id 267
    label "conversion"
  ]
  node [
    id 268
    label "transduction"
  ]
  node [
    id 269
    label "tworzenie"
  ]
  node [
    id 270
    label "zmiana"
  ]
  node [
    id 271
    label "krajka"
  ]
  node [
    id 272
    label "tworzywo"
  ]
  node [
    id 273
    label "krajalno&#347;&#263;"
  ]
  node [
    id 274
    label "archiwum"
  ]
  node [
    id 275
    label "kandydat"
  ]
  node [
    id 276
    label "bielarnia"
  ]
  node [
    id 277
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 278
    label "dane"
  ]
  node [
    id 279
    label "materia"
  ]
  node [
    id 280
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 281
    label "substancja"
  ]
  node [
    id 282
    label "nawil&#380;arka"
  ]
  node [
    id 283
    label "metamiktyczny"
  ]
  node [
    id 284
    label "radiofarmecutyk"
  ]
  node [
    id 285
    label "Warta_Boles&#322;awiecka"
  ]
  node [
    id 286
    label "Waliszewo"
  ]
  node [
    id 287
    label "Miejsce_Piastowe"
  ]
  node [
    id 288
    label "Przechlewo"
  ]
  node [
    id 289
    label "G&#322;uch&#243;w"
  ]
  node [
    id 290
    label "Trzci&#324;sko"
  ]
  node [
    id 291
    label "Wda"
  ]
  node [
    id 292
    label "&#321;apsze_Ni&#380;ne"
  ]
  node [
    id 293
    label "Horodyszcze"
  ]
  node [
    id 294
    label "Kro&#347;cienko_Wy&#380;ne"
  ]
  node [
    id 295
    label "Karsin"
  ]
  node [
    id 296
    label "Kampinos"
  ]
  node [
    id 297
    label "Kro&#347;nice"
  ]
  node [
    id 298
    label "M&#281;cina"
  ]
  node [
    id 299
    label "Wo&#322;owo"
  ]
  node [
    id 300
    label "Pietrzyk&#243;w"
  ]
  node [
    id 301
    label "Je&#380;ewo_Stare"
  ]
  node [
    id 302
    label "L&#261;dek"
  ]
  node [
    id 303
    label "Chrzanowo"
  ]
  node [
    id 304
    label "Przem&#281;t"
  ]
  node [
    id 305
    label "Wdzydze"
  ]
  node [
    id 306
    label "Sobolewo"
  ]
  node [
    id 307
    label "Nieborowo"
  ]
  node [
    id 308
    label "Micha&#322;&#243;w"
  ]
  node [
    id 309
    label "Kaw&#281;czyn"
  ]
  node [
    id 310
    label "Czerwi&#324;sk_nad_Wis&#322;&#261;"
  ]
  node [
    id 311
    label "Lgota_Wielka"
  ]
  node [
    id 312
    label "Bobrek"
  ]
  node [
    id 313
    label "Zieleniewo"
  ]
  node [
    id 314
    label "Lipce"
  ]
  node [
    id 315
    label "Walew"
  ]
  node [
    id 316
    label "P&#281;c&#322;aw"
  ]
  node [
    id 317
    label "Giebu&#322;t&#243;w"
  ]
  node [
    id 318
    label "Niek&#322;a&#324;_Wielki"
  ]
  node [
    id 319
    label "Gocza&#322;kowice-Zdr&#243;j"
  ]
  node [
    id 320
    label "Wambierzyce"
  ]
  node [
    id 321
    label "Lutomiersk"
  ]
  node [
    id 322
    label "W&#243;jcin"
  ]
  node [
    id 323
    label "Bielice"
  ]
  node [
    id 324
    label "Korycin"
  ]
  node [
    id 325
    label "Mieszk&#243;w"
  ]
  node [
    id 326
    label "&#379;ernica"
  ]
  node [
    id 327
    label "Wa&#347;ni&#243;w"
  ]
  node [
    id 328
    label "Wi&#281;ck&#243;w"
  ]
  node [
    id 329
    label "Kal"
  ]
  node [
    id 330
    label "Ko&#347;cielisko"
  ]
  node [
    id 331
    label "Kulczyce"
  ]
  node [
    id 332
    label "Herby"
  ]
  node [
    id 333
    label "Rembert&#243;w"
  ]
  node [
    id 334
    label "&#321;opuszno"
  ]
  node [
    id 335
    label "Wereszczyn"
  ]
  node [
    id 336
    label "Piecki"
  ]
  node [
    id 337
    label "Go&#347;cierad&#243;w_Ukazowy"
  ]
  node [
    id 338
    label "Zakrz&#243;w"
  ]
  node [
    id 339
    label "Jawiszowice"
  ]
  node [
    id 340
    label "Wartkowice"
  ]
  node [
    id 341
    label "S&#322;o&#324;sk"
  ]
  node [
    id 342
    label "Tar&#322;&#243;w"
  ]
  node [
    id 343
    label "Pogorzela"
  ]
  node [
    id 344
    label "Skalmierzyce"
  ]
  node [
    id 345
    label "Podhorce"
  ]
  node [
    id 346
    label "Barcice_Dolne"
  ]
  node [
    id 347
    label "Sul&#281;czyno"
  ]
  node [
    id 348
    label "W&#261;wolnica"
  ]
  node [
    id 349
    label "Kro&#347;cienko_nad_Dunajcem"
  ]
  node [
    id 350
    label "Maciejowice"
  ]
  node [
    id 351
    label "Kruszyn"
  ]
  node [
    id 352
    label "Wojakowa"
  ]
  node [
    id 353
    label "Dzier&#380;an&#243;w"
  ]
  node [
    id 354
    label "Janis&#322;awice"
  ]
  node [
    id 355
    label "Boles&#322;aw"
  ]
  node [
    id 356
    label "&#321;&#281;ki_Ko&#347;cielne"
  ]
  node [
    id 357
    label "Jan&#243;w_Podlaski"
  ]
  node [
    id 358
    label "Mycielin"
  ]
  node [
    id 359
    label "Sadowa"
  ]
  node [
    id 360
    label "Czarny_Dunajec"
  ]
  node [
    id 361
    label "Pietrowice_Wielkie"
  ]
  node [
    id 362
    label "Ka&#378;mierz"
  ]
  node [
    id 363
    label "Ku&#378;nica_Grabowska"
  ]
  node [
    id 364
    label "Schengen"
  ]
  node [
    id 365
    label "Ro&#380;n&#243;w"
  ]
  node [
    id 366
    label "Mniszk&#243;w"
  ]
  node [
    id 367
    label "Brodnica"
  ]
  node [
    id 368
    label "Mir&#243;w"
  ]
  node [
    id 369
    label "Biedrusko"
  ]
  node [
    id 370
    label "Biskupice_Podg&#243;rne"
  ]
  node [
    id 371
    label "Jasienica_Zamkowa"
  ]
  node [
    id 372
    label "Siedliska"
  ]
  node [
    id 373
    label "Skrzeszew"
  ]
  node [
    id 374
    label "Malec"
  ]
  node [
    id 375
    label "&#379;mijewo_Ko&#347;cielne"
  ]
  node [
    id 376
    label "D&#281;bowiec"
  ]
  node [
    id 377
    label "&#321;&#281;g"
  ]
  node [
    id 378
    label "Radomy&#347;l_nad_Sanem"
  ]
  node [
    id 379
    label "Pa&#322;ecznica"
  ]
  node [
    id 380
    label "Zabierz&#243;w"
  ]
  node [
    id 381
    label "Lis&#243;w"
  ]
  node [
    id 382
    label "Hermanov"
  ]
  node [
    id 383
    label "Santok"
  ]
  node [
    id 384
    label "Lip&#243;wka"
  ]
  node [
    id 385
    label "Koma&#324;cza"
  ]
  node [
    id 386
    label "Orzechowo"
  ]
  node [
    id 387
    label "Upita"
  ]
  node [
    id 388
    label "Bukowiec"
  ]
  node [
    id 389
    label "Gr&#243;dek"
  ]
  node [
    id 390
    label "Lusina"
  ]
  node [
    id 391
    label "Janowice_Wielkie"
  ]
  node [
    id 392
    label "Turawa"
  ]
  node [
    id 393
    label "Budz&#243;w"
  ]
  node [
    id 394
    label "Zag&#243;rze"
  ]
  node [
    id 395
    label "Dziekan&#243;w"
  ]
  node [
    id 396
    label "Subkowy"
  ]
  node [
    id 397
    label "Zi&#243;&#322;kowo"
  ]
  node [
    id 398
    label "Drzewica"
  ]
  node [
    id 399
    label "Pysznica"
  ]
  node [
    id 400
    label "&#379;waniec"
  ]
  node [
    id 401
    label "Rani&#380;&#243;w"
  ]
  node [
    id 402
    label "Rokiciny"
  ]
  node [
    id 403
    label "Kazan&#243;w"
  ]
  node [
    id 404
    label "&#379;yrak&#243;w"
  ]
  node [
    id 405
    label "Dyb&#243;w"
  ]
  node [
    id 406
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 407
    label "&#321;&#281;gowo"
  ]
  node [
    id 408
    label "Pruszcz"
  ]
  node [
    id 409
    label "Wodzis&#322;aw"
  ]
  node [
    id 410
    label "Motycz"
  ]
  node [
    id 411
    label "Baligr&#243;d"
  ]
  node [
    id 412
    label "D&#281;bnica_Kaszubska"
  ]
  node [
    id 413
    label "Wkra"
  ]
  node [
    id 414
    label "Wielbark"
  ]
  node [
    id 415
    label "Sobota"
  ]
  node [
    id 416
    label "Zbyszewo"
  ]
  node [
    id 417
    label "Mi&#281;dzybrodzie_Bialskie"
  ]
  node [
    id 418
    label "Prandocin"
  ]
  node [
    id 419
    label "Grz&#281;da"
  ]
  node [
    id 420
    label "Samson&#243;w"
  ]
  node [
    id 421
    label "Brzeziny"
  ]
  node [
    id 422
    label "U&#347;cie_Solne"
  ]
  node [
    id 423
    label "&#346;wiecko"
  ]
  node [
    id 424
    label "Poronin"
  ]
  node [
    id 425
    label "Bukowina_Tatrza&#324;ska"
  ]
  node [
    id 426
    label "Ropa"
  ]
  node [
    id 427
    label "Sadkowice"
  ]
  node [
    id 428
    label "Miko&#322;ajki_Pomorskie"
  ]
  node [
    id 429
    label "Je&#380;&#243;w_Sudecki"
  ]
  node [
    id 430
    label "Podegrodzie"
  ]
  node [
    id 431
    label "Krewo"
  ]
  node [
    id 432
    label "Gorze&#324;"
  ]
  node [
    id 433
    label "Kur&#243;w"
  ]
  node [
    id 434
    label "S&#322;awno"
  ]
  node [
    id 435
    label "P&#281;chery"
  ]
  node [
    id 436
    label "Otorowo"
  ]
  node [
    id 437
    label "Mokrsko"
  ]
  node [
    id 438
    label "Karwica"
  ]
  node [
    id 439
    label "Lubi&#261;&#380;"
  ]
  node [
    id 440
    label "D&#322;ugo&#322;&#281;ka"
  ]
  node [
    id 441
    label "Skok&#243;w"
  ]
  node [
    id 442
    label "Kro&#347;nica"
  ]
  node [
    id 443
    label "Skrwilno"
  ]
  node [
    id 444
    label "Ma&#322;a_Wie&#347;"
  ]
  node [
    id 445
    label "Wysowa-Zdr&#243;j"
  ]
  node [
    id 446
    label "Rac&#322;awice"
  ]
  node [
    id 447
    label "&#379;arnowiec"
  ]
  node [
    id 448
    label "Zembrzyce"
  ]
  node [
    id 449
    label "Krzesz&#243;w"
  ]
  node [
    id 450
    label "Ka&#322;uszyn"
  ]
  node [
    id 451
    label "Ibramowice"
  ]
  node [
    id 452
    label "Iwanowice_W&#322;o&#347;cia&#324;skie"
  ]
  node [
    id 453
    label "Widawa"
  ]
  node [
    id 454
    label "&#321;agiewniki"
  ]
  node [
    id 455
    label "Domaniew"
  ]
  node [
    id 456
    label "Giecz"
  ]
  node [
    id 457
    label "Czosn&#243;w"
  ]
  node [
    id 458
    label "Okocim"
  ]
  node [
    id 459
    label "Jurg&#243;w"
  ]
  node [
    id 460
    label "Janisz&#243;w"
  ]
  node [
    id 461
    label "Mniszek"
  ]
  node [
    id 462
    label "Bogdaniec"
  ]
  node [
    id 463
    label "Polanka_Wielka"
  ]
  node [
    id 464
    label "Wit&#243;w"
  ]
  node [
    id 465
    label "Szyd&#322;&#243;w"
  ]
  node [
    id 466
    label "Ligota"
  ]
  node [
    id 467
    label "&#379;upawa"
  ]
  node [
    id 468
    label "Szczerc&#243;w"
  ]
  node [
    id 469
    label "Brzezinka"
  ]
  node [
    id 470
    label "Linia"
  ]
  node [
    id 471
    label "Weso&#322;owo"
  ]
  node [
    id 472
    label "Kunice"
  ]
  node [
    id 473
    label "Bodzech&#243;w"
  ]
  node [
    id 474
    label "Szumowo"
  ]
  node [
    id 475
    label "Rogalin"
  ]
  node [
    id 476
    label "Popiel&#243;w"
  ]
  node [
    id 477
    label "Konopnica"
  ]
  node [
    id 478
    label "Micha&#322;owice"
  ]
  node [
    id 479
    label "Komorniki"
  ]
  node [
    id 480
    label "Ceg&#322;&#243;w"
  ]
  node [
    id 481
    label "Bia&#322;ka_Tatrza&#324;ska"
  ]
  node [
    id 482
    label "Naliboki"
  ]
  node [
    id 483
    label "Wi&#347;niew"
  ]
  node [
    id 484
    label "Tu&#322;owice"
  ]
  node [
    id 485
    label "Gutkowo"
  ]
  node [
    id 486
    label "Hermanowa"
  ]
  node [
    id 487
    label "Cewice"
  ]
  node [
    id 488
    label "Olszanica"
  ]
  node [
    id 489
    label "B&#261;k&#243;w"
  ]
  node [
    id 490
    label "R&#243;wne"
  ]
  node [
    id 491
    label "Przewa&#322;ka"
  ]
  node [
    id 492
    label "Radziechowy"
  ]
  node [
    id 493
    label "Krokowa"
  ]
  node [
    id 494
    label "Kijewo_Kr&#243;lewskie"
  ]
  node [
    id 495
    label "Obory"
  ]
  node [
    id 496
    label "Radgoszcz"
  ]
  node [
    id 497
    label "Szyman&#243;w"
  ]
  node [
    id 498
    label "Sztabin"
  ]
  node [
    id 499
    label "Krzy&#380;anowice"
  ]
  node [
    id 500
    label "Rz&#281;dziny"
  ]
  node [
    id 501
    label "Kozin"
  ]
  node [
    id 502
    label "Dobroszyce"
  ]
  node [
    id 503
    label "Radzan&#243;w"
  ]
  node [
    id 504
    label "Strykowo"
  ]
  node [
    id 505
    label "Zbiersk"
  ]
  node [
    id 506
    label "Gorzk&#243;w"
  ]
  node [
    id 507
    label "Wielkie_Soroczy&#324;ce"
  ]
  node [
    id 508
    label "Rytwiany"
  ]
  node [
    id 509
    label "Kaw&#281;czyn_S&#281;dziszowski"
  ]
  node [
    id 510
    label "Mi&#322;ob&#261;dz"
  ]
  node [
    id 511
    label "Piechoty"
  ]
  node [
    id 512
    label "Grzegorzew"
  ]
  node [
    id 513
    label "Okuniew"
  ]
  node [
    id 514
    label "My&#347;liwiec"
  ]
  node [
    id 515
    label "Wojs&#322;awice"
  ]
  node [
    id 516
    label "Jele&#347;nia"
  ]
  node [
    id 517
    label "Jakub&#243;w"
  ]
  node [
    id 518
    label "Zwierzy&#324;"
  ]
  node [
    id 519
    label "Horyniec-Zdr&#243;j"
  ]
  node [
    id 520
    label "Weso&#322;&#243;w"
  ]
  node [
    id 521
    label "Kruszyn_Kraje&#324;ski"
  ]
  node [
    id 522
    label "Bor&#243;wiec"
  ]
  node [
    id 523
    label "Potulice"
  ]
  node [
    id 524
    label "Ple&#347;na"
  ]
  node [
    id 525
    label "Szczaniec"
  ]
  node [
    id 526
    label "Zabor&#243;w"
  ]
  node [
    id 527
    label "Koszuty"
  ]
  node [
    id 528
    label "Bobrowniki"
  ]
  node [
    id 529
    label "Iwiny"
  ]
  node [
    id 530
    label "Siennica"
  ]
  node [
    id 531
    label "Lisowo"
  ]
  node [
    id 532
    label "Krzywcza"
  ]
  node [
    id 533
    label "&#321;&#281;ki_Ma&#322;e"
  ]
  node [
    id 534
    label "Kie&#322;pino"
  ]
  node [
    id 535
    label "K&#261;kolewo"
  ]
  node [
    id 536
    label "Wielka_Wie&#347;"
  ]
  node [
    id 537
    label "Trzebiel"
  ]
  node [
    id 538
    label "Potok_Wielki"
  ]
  node [
    id 539
    label "Stojan&#243;w"
  ]
  node [
    id 540
    label "Czaniec"
  ]
  node [
    id 541
    label "Ci&#281;&#380;kowice"
  ]
  node [
    id 542
    label "Rogo&#378;nica"
  ]
  node [
    id 543
    label "Szaflary"
  ]
  node [
    id 544
    label "Krasnowola"
  ]
  node [
    id 545
    label "Jab&#322;onka"
  ]
  node [
    id 546
    label "Zebrzydowice"
  ]
  node [
    id 547
    label "Lubieszewo"
  ]
  node [
    id 548
    label "Marcisz&#243;w"
  ]
  node [
    id 549
    label "Bor&#243;w"
  ]
  node [
    id 550
    label "Kode&#324;"
  ]
  node [
    id 551
    label "Biesiekierz"
  ]
  node [
    id 552
    label "Szlachtowa"
  ]
  node [
    id 553
    label "Klonowa"
  ]
  node [
    id 554
    label "Szczur&#243;w"
  ]
  node [
    id 555
    label "Klonowo"
  ]
  node [
    id 556
    label "Moszczenica"
  ]
  node [
    id 557
    label "Kleszcz&#243;w"
  ]
  node [
    id 558
    label "R&#281;bk&#243;w"
  ]
  node [
    id 559
    label "Czudec"
  ]
  node [
    id 560
    label "Rogo&#378;nik"
  ]
  node [
    id 561
    label "Staro&#378;reby"
  ]
  node [
    id 562
    label "K&#281;sowo"
  ]
  node [
    id 563
    label "Mirk&#243;w"
  ]
  node [
    id 564
    label "Medyka"
  ]
  node [
    id 565
    label "Rychliki"
  ]
  node [
    id 566
    label "Niebor&#243;w"
  ]
  node [
    id 567
    label "Kosakowo"
  ]
  node [
    id 568
    label "Straszyn"
  ]
  node [
    id 569
    label "sio&#322;o"
  ]
  node [
    id 570
    label "Spytkowice"
  ]
  node [
    id 571
    label "G&#322;&#281;bock"
  ]
  node [
    id 572
    label "Paprotnia"
  ]
  node [
    id 573
    label "Sieciech&#243;w"
  ]
  node [
    id 574
    label "Spa&#322;a"
  ]
  node [
    id 575
    label "Dankowice"
  ]
  node [
    id 576
    label "Ligota_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 577
    label "Krasiczyn"
  ]
  node [
    id 578
    label "Ziembin"
  ]
  node [
    id 579
    label "Orchowo"
  ]
  node [
    id 580
    label "Gorzyce"
  ]
  node [
    id 581
    label "Mszana"
  ]
  node [
    id 582
    label "Dru&#380;bice"
  ]
  node [
    id 583
    label "Karnice"
  ]
  node [
    id 584
    label "Horod&#322;o"
  ]
  node [
    id 585
    label "Bielin"
  ]
  node [
    id 586
    label "Barcice"
  ]
  node [
    id 587
    label "Piaski"
  ]
  node [
    id 588
    label "Sobienie-Jeziory"
  ]
  node [
    id 589
    label "Bledzew"
  ]
  node [
    id 590
    label "Mys&#322;akowice"
  ]
  node [
    id 591
    label "W&#243;jcice"
  ]
  node [
    id 592
    label "Lipka"
  ]
  node [
    id 593
    label "Turczyn"
  ]
  node [
    id 594
    label "&#346;wierklaniec"
  ]
  node [
    id 595
    label "Wetlina"
  ]
  node [
    id 596
    label "Ziemin"
  ]
  node [
    id 597
    label "Skokowa"
  ]
  node [
    id 598
    label "Kobierzyce"
  ]
  node [
    id 599
    label "Stopnica"
  ]
  node [
    id 600
    label "Kocmyrz&#243;w"
  ]
  node [
    id 601
    label "Gidle"
  ]
  node [
    id 602
    label "Bulkowo"
  ]
  node [
    id 603
    label "Z&#322;otowo"
  ]
  node [
    id 604
    label "W&#281;glewo"
  ]
  node [
    id 605
    label "Koniak&#243;w"
  ]
  node [
    id 606
    label "Nowa_Ruda"
  ]
  node [
    id 607
    label "D&#322;ugopole_Zdr&#243;j"
  ]
  node [
    id 608
    label "Jad&#243;w"
  ]
  node [
    id 609
    label "Siciny"
  ]
  node [
    id 610
    label "Komar&#243;w"
  ]
  node [
    id 611
    label "Waksmund"
  ]
  node [
    id 612
    label "Czarn&#243;w"
  ]
  node [
    id 613
    label "Wr&#281;czyca_Wielka"
  ]
  node [
    id 614
    label "Kruszyna"
  ]
  node [
    id 615
    label "Branice"
  ]
  node [
    id 616
    label "Nowiny_Brdowskie"
  ]
  node [
    id 617
    label "Secemin"
  ]
  node [
    id 618
    label "Ko&#322;aczkowo"
  ]
  node [
    id 619
    label "Odrzyko&#324;"
  ]
  node [
    id 620
    label "Radziemice"
  ]
  node [
    id 621
    label "Konstantyn&#243;w"
  ]
  node [
    id 622
    label "&#379;arn&#243;w"
  ]
  node [
    id 623
    label "Zg&#322;obice"
  ]
  node [
    id 624
    label "P&#322;aszewo"
  ]
  node [
    id 625
    label "Kiszkowo"
  ]
  node [
    id 626
    label "Zielonki"
  ]
  node [
    id 627
    label "Kiwity"
  ]
  node [
    id 628
    label "Siemi&#261;tkowo"
  ]
  node [
    id 629
    label "Mochowo"
  ]
  node [
    id 630
    label "Szpetal_G&#243;rny"
  ]
  node [
    id 631
    label "Brenna"
  ]
  node [
    id 632
    label "G&#322;uchowo"
  ]
  node [
    id 633
    label "Wola_Komborska"
  ]
  node [
    id 634
    label "Kamieniec_Z&#261;bkowicki"
  ]
  node [
    id 635
    label "Grudusk"
  ]
  node [
    id 636
    label "Katy&#324;"
  ]
  node [
    id 637
    label "Gron&#243;w"
  ]
  node [
    id 638
    label "Palmiry"
  ]
  node [
    id 639
    label "&#321;abowa"
  ]
  node [
    id 640
    label "Bobrowice"
  ]
  node [
    id 641
    label "Komorowo"
  ]
  node [
    id 642
    label "Nawojowa"
  ]
  node [
    id 643
    label "Unis&#322;aw"
  ]
  node [
    id 644
    label "Kosz&#281;cin"
  ]
  node [
    id 645
    label "G&#322;uch&#243;w_G&#243;rny"
  ]
  node [
    id 646
    label "S&#281;kowa"
  ]
  node [
    id 647
    label "Gr&#281;bosz&#243;w"
  ]
  node [
    id 648
    label "&#321;&#281;ki_Szlacheckie"
  ]
  node [
    id 649
    label "G&#322;usk"
  ]
  node [
    id 650
    label "Ku&#378;nica"
  ]
  node [
    id 651
    label "Kie&#322;pin"
  ]
  node [
    id 652
    label "Iwanowice_Du&#380;e"
  ]
  node [
    id 653
    label "Jeleniewo"
  ]
  node [
    id 654
    label "Wr&#281;czyca"
  ]
  node [
    id 655
    label "&#321;ambinowice"
  ]
  node [
    id 656
    label "Miastk&#243;w_Ko&#347;cielny"
  ]
  node [
    id 657
    label "Grochowalsk"
  ]
  node [
    id 658
    label "&#379;&#243;rawina"
  ]
  node [
    id 659
    label "Wilkowo_Polskie"
  ]
  node [
    id 660
    label "Bys&#322;aw"
  ]
  node [
    id 661
    label "Banie"
  ]
  node [
    id 662
    label "Henryk&#243;w"
  ]
  node [
    id 663
    label "Klukowo"
  ]
  node [
    id 664
    label "Poraj"
  ]
  node [
    id 665
    label "Soko&#322;y"
  ]
  node [
    id 666
    label "Nowa_Wie&#347;"
  ]
  node [
    id 667
    label "Sobolew"
  ]
  node [
    id 668
    label "Sadki"
  ]
  node [
    id 669
    label "Kobylanka"
  ]
  node [
    id 670
    label "Stryszawa"
  ]
  node [
    id 671
    label "&#321;ukowica"
  ]
  node [
    id 672
    label "Ostaszewo"
  ]
  node [
    id 673
    label "&#321;any"
  ]
  node [
    id 674
    label "Cierlicko"
  ]
  node [
    id 675
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 676
    label "Papowo_Biskupie"
  ]
  node [
    id 677
    label "Somonino"
  ]
  node [
    id 678
    label "&#321;&#281;ki"
  ]
  node [
    id 679
    label "Zuberzec"
  ]
  node [
    id 680
    label "Piast&#243;w"
  ]
  node [
    id 681
    label "Korzecko"
  ]
  node [
    id 682
    label "Umiast&#243;w"
  ]
  node [
    id 683
    label "Grunwald"
  ]
  node [
    id 684
    label "Rytel"
  ]
  node [
    id 685
    label "W&#322;oszakowice"
  ]
  node [
    id 686
    label "Chocho&#322;&#243;w"
  ]
  node [
    id 687
    label "Gronowo"
  ]
  node [
    id 688
    label "Mucharz"
  ]
  node [
    id 689
    label "Szymbark"
  ]
  node [
    id 690
    label "Przodkowo"
  ]
  node [
    id 691
    label "Frysztak"
  ]
  node [
    id 692
    label "Izbicko"
  ]
  node [
    id 693
    label "Manowo"
  ]
  node [
    id 694
    label "Borz&#281;cin"
  ]
  node [
    id 695
    label "Ko&#324;skowola"
  ]
  node [
    id 696
    label "Zaborze"
  ]
  node [
    id 697
    label "Krzecz&#243;w"
  ]
  node [
    id 698
    label "Zalesie"
  ]
  node [
    id 699
    label "Solec-Zdr&#243;j"
  ]
  node [
    id 700
    label "Pacyna"
  ]
  node [
    id 701
    label "Jerzmanowice"
  ]
  node [
    id 702
    label "Oss&#243;w"
  ]
  node [
    id 703
    label "Lubichowo"
  ]
  node [
    id 704
    label "Wiechowo"
  ]
  node [
    id 705
    label "Srebrna_G&#243;ra"
  ]
  node [
    id 706
    label "Wierzbica"
  ]
  node [
    id 707
    label "Hacz&#243;w"
  ]
  node [
    id 708
    label "Mi&#281;dzybrodzie_&#379;ywieckie"
  ]
  node [
    id 709
    label "Sterdy&#324;"
  ]
  node [
    id 710
    label "Gromnik"
  ]
  node [
    id 711
    label "Jakubowo"
  ]
  node [
    id 712
    label "Filip&#243;w"
  ]
  node [
    id 713
    label "Stromiec"
  ]
  node [
    id 714
    label "Ochotnica_Dolna"
  ]
  node [
    id 715
    label "Obl&#281;gorek"
  ]
  node [
    id 716
    label "Luzino"
  ]
  node [
    id 717
    label "Babin"
  ]
  node [
    id 718
    label "Biskupin"
  ]
  node [
    id 719
    label "Iwkowa"
  ]
  node [
    id 720
    label "Kobyla_G&#243;ra"
  ]
  node [
    id 721
    label "Wicko"
  ]
  node [
    id 722
    label "Targowica"
  ]
  node [
    id 723
    label "Rejowiec"
  ]
  node [
    id 724
    label "Tymbark"
  ]
  node [
    id 725
    label "Go&#322;uchowo"
  ]
  node [
    id 726
    label "Brze&#378;nica"
  ]
  node [
    id 727
    label "Dalk&#243;w"
  ]
  node [
    id 728
    label "Dzier&#380;anowo"
  ]
  node [
    id 729
    label "Ligotka_Kameralna"
  ]
  node [
    id 730
    label "Osina"
  ]
  node [
    id 731
    label "Dzikowo"
  ]
  node [
    id 732
    label "Zap&#281;dowo"
  ]
  node [
    id 733
    label "&#321;&#281;g_Tarnowski"
  ]
  node [
    id 734
    label "Celestyn&#243;w"
  ]
  node [
    id 735
    label "Mas&#322;&#243;w"
  ]
  node [
    id 736
    label "Kostkowo"
  ]
  node [
    id 737
    label "Bia&#322;obrzegi"
  ]
  node [
    id 738
    label "Ba&#322;t&#243;w"
  ]
  node [
    id 739
    label "Wi&#324;sko"
  ]
  node [
    id 740
    label "Ptaszkowa"
  ]
  node [
    id 741
    label "Chmiele&#324;"
  ]
  node [
    id 742
    label "Niedzica"
  ]
  node [
    id 743
    label "Sokolniki"
  ]
  node [
    id 744
    label "Topor&#243;w"
  ]
  node [
    id 745
    label "Wigry"
  ]
  node [
    id 746
    label "&#321;a&#324;sk"
  ]
  node [
    id 747
    label "Nadole"
  ]
  node [
    id 748
    label "Karg&#243;w"
  ]
  node [
    id 749
    label "St&#281;&#380;yca"
  ]
  node [
    id 750
    label "Mark&#243;w"
  ]
  node [
    id 751
    label "Kwilcz"
  ]
  node [
    id 752
    label "Rudna"
  ]
  node [
    id 753
    label "Kamienica_Polska"
  ]
  node [
    id 754
    label "Czorsztyn"
  ]
  node [
    id 755
    label "Jaworze"
  ]
  node [
    id 756
    label "Paw&#322;owice"
  ]
  node [
    id 757
    label "&#321;&#281;ki_G&#243;rne"
  ]
  node [
    id 758
    label "Zapa&#322;&#243;w"
  ]
  node [
    id 759
    label "Olszyna"
  ]
  node [
    id 760
    label "Nadarzyn"
  ]
  node [
    id 761
    label "Sz&#243;wsko"
  ]
  node [
    id 762
    label "Nowa_G&#243;ra"
  ]
  node [
    id 763
    label "kompromitacja"
  ]
  node [
    id 764
    label "Wielebn&#243;w"
  ]
  node [
    id 765
    label "Jedlina"
  ]
  node [
    id 766
    label "Wilkowice"
  ]
  node [
    id 767
    label "Burzenin"
  ]
  node [
    id 768
    label "Rewal"
  ]
  node [
    id 769
    label "Spychowo"
  ]
  node [
    id 770
    label "Budzy&#324;"
  ]
  node [
    id 771
    label "Lasek"
  ]
  node [
    id 772
    label "J&#243;zef&#243;w"
  ]
  node [
    id 773
    label "Klimont&#243;w"
  ]
  node [
    id 774
    label "Byszewo"
  ]
  node [
    id 775
    label "Igo&#322;omia"
  ]
  node [
    id 776
    label "Kra&#347;niczyn"
  ]
  node [
    id 777
    label "Teresin"
  ]
  node [
    id 778
    label "Lubrza"
  ]
  node [
    id 779
    label "&#321;&#281;ka_Opatowska"
  ]
  node [
    id 780
    label "Wi&#347;ni&#243;w"
  ]
  node [
    id 781
    label "Fa&#322;k&#243;w"
  ]
  node [
    id 782
    label "&#321;ubno"
  ]
  node [
    id 783
    label "Tenczynek"
  ]
  node [
    id 784
    label "Izbice"
  ]
  node [
    id 785
    label "Tylicz"
  ]
  node [
    id 786
    label "&#379;yga&#324;sk"
  ]
  node [
    id 787
    label "Czersk"
  ]
  node [
    id 788
    label "Borzech&#243;w"
  ]
  node [
    id 789
    label "Prosz&#243;w"
  ]
  node [
    id 790
    label "Nowiny"
  ]
  node [
    id 791
    label "&#379;elich&#243;w"
  ]
  node [
    id 792
    label "B&#261;kowo"
  ]
  node [
    id 793
    label "Azowo"
  ]
  node [
    id 794
    label "Gniewkowo"
  ]
  node [
    id 795
    label "Konotopa"
  ]
  node [
    id 796
    label "Dopiewo"
  ]
  node [
    id 797
    label "Zebrzyd&#243;w"
  ]
  node [
    id 798
    label "Lanckorona"
  ]
  node [
    id 799
    label "Wolan&#243;w"
  ]
  node [
    id 800
    label "Skierbiesz&#243;w"
  ]
  node [
    id 801
    label "B&#281;dk&#243;w"
  ]
  node [
    id 802
    label "Lipowa"
  ]
  node [
    id 803
    label "Izbica"
  ]
  node [
    id 804
    label "Byty&#324;"
  ]
  node [
    id 805
    label "Bobolice"
  ]
  node [
    id 806
    label "Mielno"
  ]
  node [
    id 807
    label "B&#322;&#281;d&#243;w"
  ]
  node [
    id 808
    label "Mi&#322;ki"
  ]
  node [
    id 809
    label "Turk&#243;w"
  ]
  node [
    id 810
    label "Bolim&#243;w"
  ]
  node [
    id 811
    label "Doruch&#243;w"
  ]
  node [
    id 812
    label "Chotom&#243;w"
  ]
  node [
    id 813
    label "Zawistowszczyzna"
  ]
  node [
    id 814
    label "Solnica"
  ]
  node [
    id 815
    label "Tarn&#243;w_Opolski"
  ]
  node [
    id 816
    label "Lichnowy"
  ]
  node [
    id 817
    label "Nowa_Wie&#347;_L&#281;borska"
  ]
  node [
    id 818
    label "Domani&#243;w"
  ]
  node [
    id 819
    label "Charzykowy"
  ]
  node [
    id 820
    label "Janowiec"
  ]
  node [
    id 821
    label "Gietrzwa&#322;d"
  ]
  node [
    id 822
    label "Drel&#243;w"
  ]
  node [
    id 823
    label "Mieszkowice"
  ]
  node [
    id 824
    label "Kicin"
  ]
  node [
    id 825
    label "&#321;ososina_Dolna"
  ]
  node [
    id 826
    label "Kami&#324;sko"
  ]
  node [
    id 827
    label "Czernich&#243;w"
  ]
  node [
    id 828
    label "Gilowice"
  ]
  node [
    id 829
    label "Pratulin"
  ]
  node [
    id 830
    label "&#379;egiest&#243;w-Zdr&#243;j"
  ]
  node [
    id 831
    label "L&#261;d"
  ]
  node [
    id 832
    label "Zieli&#324;sk"
  ]
  node [
    id 833
    label "Zdan&#243;w"
  ]
  node [
    id 834
    label "Krzy&#380;an&#243;w"
  ]
  node [
    id 835
    label "Ma&#322;kinia_G&#243;rna"
  ]
  node [
    id 836
    label "Komor&#243;w"
  ]
  node [
    id 837
    label "D&#322;ugopole-Zdr&#243;j"
  ]
  node [
    id 838
    label "Wieck"
  ]
  node [
    id 839
    label "Sieroszewice"
  ]
  node [
    id 840
    label "Marysin"
  ]
  node [
    id 841
    label "Wdzydze_Tucholskie"
  ]
  node [
    id 842
    label "Gorajec"
  ]
  node [
    id 843
    label "Urz&#281;d&#243;w"
  ]
  node [
    id 844
    label "Przywidz"
  ]
  node [
    id 845
    label "Nur"
  ]
  node [
    id 846
    label "Szczurowa"
  ]
  node [
    id 847
    label "Dobrzyca"
  ]
  node [
    id 848
    label "Niemir&#243;w"
  ]
  node [
    id 849
    label "D&#322;ugopole"
  ]
  node [
    id 850
    label "Oro&#324;sko"
  ]
  node [
    id 851
    label "Kulig&#243;w"
  ]
  node [
    id 852
    label "Drohoj&#243;w"
  ]
  node [
    id 853
    label "Zaleszany"
  ]
  node [
    id 854
    label "Giera&#322;towice"
  ]
  node [
    id 855
    label "Bo&#263;ki"
  ]
  node [
    id 856
    label "Dziekan&#243;w_Polski"
  ]
  node [
    id 857
    label "Moch&#243;w"
  ]
  node [
    id 858
    label "Sulik&#243;w"
  ]
  node [
    id 859
    label "Laski"
  ]
  node [
    id 860
    label "My&#347;lina"
  ]
  node [
    id 861
    label "&#321;&#261;g"
  ]
  node [
    id 862
    label "Jaraczewo"
  ]
  node [
    id 863
    label "God&#243;w"
  ]
  node [
    id 864
    label "Mas&#322;&#243;w_Pierwszy"
  ]
  node [
    id 865
    label "Jedlnia-Letnisko"
  ]
  node [
    id 866
    label "Lud&#378;mierz"
  ]
  node [
    id 867
    label "Firlej"
  ]
  node [
    id 868
    label "Magdalenka"
  ]
  node [
    id 869
    label "Dybowo"
  ]
  node [
    id 870
    label "Wr&#243;blew"
  ]
  node [
    id 871
    label "Lack"
  ]
  node [
    id 872
    label "Kotlin"
  ]
  node [
    id 873
    label "S&#281;kocin_Stary"
  ]
  node [
    id 874
    label "Rojewo"
  ]
  node [
    id 875
    label "Baranowo"
  ]
  node [
    id 876
    label "Chynowa"
  ]
  node [
    id 877
    label "Mierzyn"
  ]
  node [
    id 878
    label "Biecz"
  ]
  node [
    id 879
    label "Wolice"
  ]
  node [
    id 880
    label "Pola&#324;czyk"
  ]
  node [
    id 881
    label "Jod&#322;owa"
  ]
  node [
    id 882
    label "&#321;apsze_Wy&#380;ne"
  ]
  node [
    id 883
    label "Choczewo"
  ]
  node [
    id 884
    label "Leszczyna"
  ]
  node [
    id 885
    label "B&#281;domin"
  ]
  node [
    id 886
    label "Guz&#243;w"
  ]
  node [
    id 887
    label "Wojnowo"
  ]
  node [
    id 888
    label "W&#281;grzyn&#243;w"
  ]
  node [
    id 889
    label "D&#261;bki"
  ]
  node [
    id 890
    label "U&#347;cie_Gorlickie"
  ]
  node [
    id 891
    label "Gwiazdowo"
  ]
  node [
    id 892
    label "Siepraw"
  ]
  node [
    id 893
    label "Bychowo"
  ]
  node [
    id 894
    label "Tyrawa_Wo&#322;oska"
  ]
  node [
    id 895
    label "Malan&#243;w"
  ]
  node [
    id 896
    label "Przygodzice"
  ]
  node [
    id 897
    label "Jaminy"
  ]
  node [
    id 898
    label "&#321;&#281;ka_Wielka"
  ]
  node [
    id 899
    label "&#321;&#261;cko"
  ]
  node [
    id 900
    label "Tusz&#243;w_Narodowy"
  ]
  node [
    id 901
    label "Str&#243;&#380;e"
  ]
  node [
    id 902
    label "W&#243;lka_Konopna"
  ]
  node [
    id 903
    label "Czarnocin"
  ]
  node [
    id 904
    label "Zwierzyn"
  ]
  node [
    id 905
    label "Parady&#380;"
  ]
  node [
    id 906
    label "Wilczyn"
  ]
  node [
    id 907
    label "Czarnolas"
  ]
  node [
    id 908
    label "St&#281;bark"
  ]
  node [
    id 909
    label "Korczew"
  ]
  node [
    id 910
    label "Przecisz&#243;w"
  ]
  node [
    id 911
    label "Barczew"
  ]
  node [
    id 912
    label "Herman&#243;w"
  ]
  node [
    id 913
    label "&#321;apan&#243;w"
  ]
  node [
    id 914
    label "Osiny"
  ]
  node [
    id 915
    label "Adam&#243;w"
  ]
  node [
    id 916
    label "Leszczyny"
  ]
  node [
    id 917
    label "Warszyce"
  ]
  node [
    id 918
    label "Gd&#243;w"
  ]
  node [
    id 919
    label "Wierzchos&#322;awice"
  ]
  node [
    id 920
    label "Mich&#243;w"
  ]
  node [
    id 921
    label "Dolsk"
  ]
  node [
    id 922
    label "Opinog&#243;ra_G&#243;rna"
  ]
  node [
    id 923
    label "S&#322;abosz&#243;w"
  ]
  node [
    id 924
    label "Magnuszew"
  ]
  node [
    id 925
    label "Jan&#243;w"
  ]
  node [
    id 926
    label "Serniki"
  ]
  node [
    id 927
    label "Sku&#322;y"
  ]
  node [
    id 928
    label "Dawid&#243;w"
  ]
  node [
    id 929
    label "Kodr&#261;b"
  ]
  node [
    id 930
    label "Skotniki"
  ]
  node [
    id 931
    label "Je&#380;ewo"
  ]
  node [
    id 932
    label "Sk&#281;psk"
  ]
  node [
    id 933
    label "&#346;niadowo"
  ]
  node [
    id 934
    label "Dubiecko"
  ]
  node [
    id 935
    label "&#379;arowo"
  ]
  node [
    id 936
    label "Pomianowo"
  ]
  node [
    id 937
    label "Rytro"
  ]
  node [
    id 938
    label "&#321;&#281;ki_Dukielskie"
  ]
  node [
    id 939
    label "Mi&#281;kinia"
  ]
  node [
    id 940
    label "Bielany"
  ]
  node [
    id 941
    label "Ostrzyce"
  ]
  node [
    id 942
    label "&#321;odygowice"
  ]
  node [
    id 943
    label "Dru&#380;bice-Kolonia"
  ]
  node [
    id 944
    label "Zawad&#243;w"
  ]
  node [
    id 945
    label "Wi&#347;lica"
  ]
  node [
    id 946
    label "G&#243;ra"
  ]
  node [
    id 947
    label "Koz&#322;owo"
  ]
  node [
    id 948
    label "Paszk&#243;w"
  ]
  node [
    id 949
    label "Pia&#347;nica"
  ]
  node [
    id 950
    label "Sulmierzyce"
  ]
  node [
    id 951
    label "Gumniska"
  ]
  node [
    id 952
    label "Goworowo"
  ]
  node [
    id 953
    label "Przerzeczyn-Zdr&#243;j"
  ]
  node [
    id 954
    label "Bia&#322;acz&#243;w"
  ]
  node [
    id 955
    label "Cieszk&#243;w"
  ]
  node [
    id 956
    label "Stara_Wie&#347;"
  ]
  node [
    id 957
    label "Nowe_Sio&#322;o"
  ]
  node [
    id 958
    label "Swija&#380;sk"
  ]
  node [
    id 959
    label "Iwno"
  ]
  node [
    id 960
    label "Strza&#322;kowo"
  ]
  node [
    id 961
    label "O&#322;tarzew"
  ]
  node [
    id 962
    label "Szreniawa"
  ]
  node [
    id 963
    label "Parz&#281;czew"
  ]
  node [
    id 964
    label "&#379;abin"
  ]
  node [
    id 965
    label "Zawoja"
  ]
  node [
    id 966
    label "Tuczapy"
  ]
  node [
    id 967
    label "Bobrowo"
  ]
  node [
    id 968
    label "Parze&#324;"
  ]
  node [
    id 969
    label "Borzykowa"
  ]
  node [
    id 970
    label "Bircza"
  ]
  node [
    id 971
    label "Gosprzydowa"
  ]
  node [
    id 972
    label "Rusiec"
  ]
  node [
    id 973
    label "Daniszew"
  ]
  node [
    id 974
    label "&#346;wi&#261;tki"
  ]
  node [
    id 975
    label "Z&#322;otopolice"
  ]
  node [
    id 976
    label "Ligota_Turawska"
  ]
  node [
    id 977
    label "Istebna"
  ]
  node [
    id 978
    label "&#321;azy"
  ]
  node [
    id 979
    label "Korbiel&#243;w"
  ]
  node [
    id 980
    label "Rychtal"
  ]
  node [
    id 981
    label "Zblewo"
  ]
  node [
    id 982
    label "Zakrzew"
  ]
  node [
    id 983
    label "Grodziec"
  ]
  node [
    id 984
    label "Raszyn"
  ]
  node [
    id 985
    label "Ochmat&#243;w"
  ]
  node [
    id 986
    label "&#379;egocina"
  ]
  node [
    id 987
    label "Swarzewo"
  ]
  node [
    id 988
    label "Wojciech&#243;w"
  ]
  node [
    id 989
    label "Leszczyn"
  ]
  node [
    id 990
    label "Zarszyn"
  ]
  node [
    id 991
    label "Andrusz&#243;w"
  ]
  node [
    id 992
    label "Zarzecze"
  ]
  node [
    id 993
    label "Gnojnik"
  ]
  node [
    id 994
    label "Por&#281;ba"
  ]
  node [
    id 995
    label "Rak&#243;w"
  ]
  node [
    id 996
    label "Radziwi&#322;&#322;&#243;w"
  ]
  node [
    id 997
    label "Gurowo"
  ]
  node [
    id 998
    label "Twary"
  ]
  node [
    id 999
    label "Ja&#347;liska"
  ]
  node [
    id 1000
    label "Baran&#243;w"
  ]
  node [
    id 1001
    label "&#379;uraw"
  ]
  node [
    id 1002
    label "Przybroda"
  ]
  node [
    id 1003
    label "Pilchowice"
  ]
  node [
    id 1004
    label "Tursk"
  ]
  node [
    id 1005
    label "Wilamowice"
  ]
  node [
    id 1006
    label "Rokitnica"
  ]
  node [
    id 1007
    label "Bogucice"
  ]
  node [
    id 1008
    label "Zegrze"
  ]
  node [
    id 1009
    label "Szre&#324;sk"
  ]
  node [
    id 1010
    label "Mi&#281;dzybrodzie"
  ]
  node [
    id 1011
    label "Kie&#322;czewo"
  ]
  node [
    id 1012
    label "Sobib&#243;r"
  ]
  node [
    id 1013
    label "B&#322;onie"
  ]
  node [
    id 1014
    label "Korczyna"
  ]
  node [
    id 1015
    label "Su&#322;owo"
  ]
  node [
    id 1016
    label "Granowo"
  ]
  node [
    id 1017
    label "Pacan&#243;w"
  ]
  node [
    id 1018
    label "&#346;wierklany"
  ]
  node [
    id 1019
    label "Klucze"
  ]
  node [
    id 1020
    label "Mogilany"
  ]
  node [
    id 1021
    label "Byczyna"
  ]
  node [
    id 1022
    label "Kobierzycko"
  ]
  node [
    id 1023
    label "Czernica"
  ]
  node [
    id 1024
    label "Jasienica"
  ]
  node [
    id 1025
    label "&#379;o&#322;ynia"
  ]
  node [
    id 1026
    label "Skrzy&#324;sko"
  ]
  node [
    id 1027
    label "&#321;ubowo"
  ]
  node [
    id 1028
    label "Jasienica_Rosielna"
  ]
  node [
    id 1029
    label "Sm&#281;towo_Graniczne"
  ]
  node [
    id 1030
    label "Twork&#243;w"
  ]
  node [
    id 1031
    label "P&#322;owce"
  ]
  node [
    id 1032
    label "Bia&#322;owie&#380;a"
  ]
  node [
    id 1033
    label "Zab&#322;ocie"
  ]
  node [
    id 1034
    label "Turobin"
  ]
  node [
    id 1035
    label "Przytyk"
  ]
  node [
    id 1036
    label "Bobrza"
  ]
  node [
    id 1037
    label "Sztutowo"
  ]
  node [
    id 1038
    label "Bia&#322;o&#347;liwie"
  ]
  node [
    id 1039
    label "Sapie&#380;yn"
  ]
  node [
    id 1040
    label "Grzybowo"
  ]
  node [
    id 1041
    label "Lubenia"
  ]
  node [
    id 1042
    label "Bobr&#243;w"
  ]
  node [
    id 1043
    label "Lipnica"
  ]
  node [
    id 1044
    label "Walim"
  ]
  node [
    id 1045
    label "Sawin"
  ]
  node [
    id 1046
    label "Mnich&#243;w"
  ]
  node [
    id 1047
    label "Karniewo"
  ]
  node [
    id 1048
    label "Wr&#281;czyca_Ma&#322;a"
  ]
  node [
    id 1049
    label "Gozdy"
  ]
  node [
    id 1050
    label "Izdebnik"
  ]
  node [
    id 1051
    label "Z&#322;otniki_Kujawskie"
  ]
  node [
    id 1052
    label "Babice"
  ]
  node [
    id 1053
    label "Sabaudia"
  ]
  node [
    id 1054
    label "Suchod&#243;&#322;_Szlachecki"
  ]
  node [
    id 1055
    label "Uszew"
  ]
  node [
    id 1056
    label "Bia&#322;a"
  ]
  node [
    id 1057
    label "Przyr&#243;w"
  ]
  node [
    id 1058
    label "Ko&#347;cielec"
  ]
  node [
    id 1059
    label "Skibice"
  ]
  node [
    id 1060
    label "Charsznica"
  ]
  node [
    id 1061
    label "&#321;ososina_G&#243;rna"
  ]
  node [
    id 1062
    label "Go&#322;uch&#243;w"
  ]
  node [
    id 1063
    label "Popowo"
  ]
  node [
    id 1064
    label "Annopol"
  ]
  node [
    id 1065
    label "G&#322;osk&#243;w"
  ]
  node [
    id 1066
    label "Sierakowice"
  ]
  node [
    id 1067
    label "Daszyna"
  ]
  node [
    id 1068
    label "Malechowo"
  ]
  node [
    id 1069
    label "Krzczon&#243;w"
  ]
  node [
    id 1070
    label "Wieniawa"
  ]
  node [
    id 1071
    label "&#346;liwice"
  ]
  node [
    id 1072
    label "Je&#380;&#243;w"
  ]
  node [
    id 1073
    label "We&#322;nica"
  ]
  node [
    id 1074
    label "Lutom"
  ]
  node [
    id 1075
    label "Tomice"
  ]
  node [
    id 1076
    label "Bia&#322;y_Dunajec"
  ]
  node [
    id 1077
    label "Daszewo"
  ]
  node [
    id 1078
    label "Garb&#243;w"
  ]
  node [
    id 1079
    label "Wielowie&#347;"
  ]
  node [
    id 1080
    label "Turza"
  ]
  node [
    id 1081
    label "Stadniki"
  ]
  node [
    id 1082
    label "Jasienica_Dolna"
  ]
  node [
    id 1083
    label "G&#261;sawa"
  ]
  node [
    id 1084
    label "Osielec"
  ]
  node [
    id 1085
    label "Kocza&#322;a"
  ]
  node [
    id 1086
    label "Koz&#322;&#243;w"
  ]
  node [
    id 1087
    label "Niedrzwica_Du&#380;a"
  ]
  node [
    id 1088
    label "Pcim"
  ]
  node [
    id 1089
    label "Wilk&#243;w"
  ]
  node [
    id 1090
    label "Pra&#380;m&#243;w"
  ]
  node [
    id 1091
    label "Gardzienice"
  ]
  node [
    id 1092
    label "Guty"
  ]
  node [
    id 1093
    label "Ptaszkowo"
  ]
  node [
    id 1094
    label "Kluczewsko"
  ]
  node [
    id 1095
    label "Tuchlino"
  ]
  node [
    id 1096
    label "Ujso&#322;y"
  ]
  node [
    id 1097
    label "Zakrz&#243;wek"
  ]
  node [
    id 1098
    label "Milan&#243;w"
  ]
  node [
    id 1099
    label "Nurzec"
  ]
  node [
    id 1100
    label "Powidz"
  ]
  node [
    id 1101
    label "Lipk&#243;w"
  ]
  node [
    id 1102
    label "Mielnik"
  ]
  node [
    id 1103
    label "Chrz&#261;stowice"
  ]
  node [
    id 1104
    label "Walk&#243;w"
  ]
  node [
    id 1105
    label "K&#322;aj"
  ]
  node [
    id 1106
    label "Peczera"
  ]
  node [
    id 1107
    label "Orla"
  ]
  node [
    id 1108
    label "Pastwa"
  ]
  node [
    id 1109
    label "R&#261;czki"
  ]
  node [
    id 1110
    label "&#379;urawica"
  ]
  node [
    id 1111
    label "Mak&#243;w"
  ]
  node [
    id 1112
    label "Wereszyca"
  ]
  node [
    id 1113
    label "Kopanica"
  ]
  node [
    id 1114
    label "wygon"
  ]
  node [
    id 1115
    label "&#321;yszkowice"
  ]
  node [
    id 1116
    label "Dobro&#324;"
  ]
  node [
    id 1117
    label "Jab&#322;onna"
  ]
  node [
    id 1118
    label "Dziekan&#243;w_Le&#347;ny"
  ]
  node [
    id 1119
    label "Myczkowce"
  ]
  node [
    id 1120
    label "Bron&#243;w"
  ]
  node [
    id 1121
    label "Jerzmanowa"
  ]
  node [
    id 1122
    label "Strysz&#243;w"
  ]
  node [
    id 1123
    label "Solina"
  ]
  node [
    id 1124
    label "&#321;&#281;ki_Strzy&#380;owskie"
  ]
  node [
    id 1125
    label "Zar&#281;ba"
  ]
  node [
    id 1126
    label "&#346;wi&#281;ta_Lipka"
  ]
  node [
    id 1127
    label "Iwanowice_Ma&#322;e"
  ]
  node [
    id 1128
    label "Chmielno"
  ]
  node [
    id 1129
    label "Wilkowo"
  ]
  node [
    id 1130
    label "Rabsztyn"
  ]
  node [
    id 1131
    label "wie&#347;niak"
  ]
  node [
    id 1132
    label "Melsztyn"
  ]
  node [
    id 1133
    label "Raciechowice"
  ]
  node [
    id 1134
    label "Zagna&#324;sk"
  ]
  node [
    id 1135
    label "Dzia&#322;oszyn"
  ]
  node [
    id 1136
    label "Wach&#243;w"
  ]
  node [
    id 1137
    label "Orsk"
  ]
  node [
    id 1138
    label "Raba_Wy&#380;na"
  ]
  node [
    id 1139
    label "&#379;abnica"
  ]
  node [
    id 1140
    label "Cary&#324;skie"
  ]
  node [
    id 1141
    label "W&#281;g&#322;&#243;w"
  ]
  node [
    id 1142
    label "Olszewo"
  ]
  node [
    id 1143
    label "B&#261;dkowo"
  ]
  node [
    id 1144
    label "&#321;&#261;ck"
  ]
  node [
    id 1145
    label "&#321;ag&#243;w"
  ]
  node [
    id 1146
    label "D&#281;be"
  ]
  node [
    id 1147
    label "Psary"
  ]
  node [
    id 1148
    label "Rzezawa"
  ]
  node [
    id 1149
    label "Krechowce"
  ]
  node [
    id 1150
    label "Skulsk"
  ]
  node [
    id 1151
    label "du&#380;y"
  ]
  node [
    id 1152
    label "cz&#281;sto"
  ]
  node [
    id 1153
    label "bardzo"
  ]
  node [
    id 1154
    label "mocno"
  ]
  node [
    id 1155
    label "wiela"
  ]
  node [
    id 1156
    label "okre&#347;lony"
  ]
  node [
    id 1157
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1158
    label "fraza"
  ]
  node [
    id 1159
    label "melodia"
  ]
  node [
    id 1160
    label "rzecz"
  ]
  node [
    id 1161
    label "zbacza&#263;"
  ]
  node [
    id 1162
    label "entity"
  ]
  node [
    id 1163
    label "omawia&#263;"
  ]
  node [
    id 1164
    label "topik"
  ]
  node [
    id 1165
    label "wyraz_pochodny"
  ]
  node [
    id 1166
    label "om&#243;wi&#263;"
  ]
  node [
    id 1167
    label "omawianie"
  ]
  node [
    id 1168
    label "w&#261;tek"
  ]
  node [
    id 1169
    label "forum"
  ]
  node [
    id 1170
    label "zboczenie"
  ]
  node [
    id 1171
    label "zbaczanie"
  ]
  node [
    id 1172
    label "tematyka"
  ]
  node [
    id 1173
    label "sprawa"
  ]
  node [
    id 1174
    label "istota"
  ]
  node [
    id 1175
    label "otoczka"
  ]
  node [
    id 1176
    label "zboczy&#263;"
  ]
  node [
    id 1177
    label "om&#243;wienie"
  ]
  node [
    id 1178
    label "pozna&#263;"
  ]
  node [
    id 1179
    label "wywr&#243;&#380;y&#263;"
  ]
  node [
    id 1180
    label "przetworzy&#263;"
  ]
  node [
    id 1181
    label "read"
  ]
  node [
    id 1182
    label "zaobserwowa&#263;"
  ]
  node [
    id 1183
    label "odczyta&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 46
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 197
  ]
  edge [
    source 24
    target 198
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 24
    target 201
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 94
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 24
    target 225
  ]
  edge [
    source 24
    target 226
  ]
  edge [
    source 24
    target 227
  ]
  edge [
    source 24
    target 228
  ]
  edge [
    source 24
    target 229
  ]
  edge [
    source 24
    target 230
  ]
  edge [
    source 24
    target 231
  ]
  edge [
    source 24
    target 232
  ]
  edge [
    source 24
    target 233
  ]
  edge [
    source 24
    target 105
  ]
  edge [
    source 24
    target 234
  ]
  edge [
    source 24
    target 235
  ]
  edge [
    source 24
    target 236
  ]
  edge [
    source 24
    target 237
  ]
  edge [
    source 24
    target 238
  ]
  edge [
    source 24
    target 239
  ]
  edge [
    source 24
    target 240
  ]
  edge [
    source 24
    target 113
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 241
  ]
  edge [
    source 25
    target 242
  ]
  edge [
    source 25
    target 243
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 244
  ]
  edge [
    source 27
    target 245
  ]
  edge [
    source 27
    target 211
  ]
  edge [
    source 27
    target 246
  ]
  edge [
    source 27
    target 247
  ]
  edge [
    source 27
    target 248
  ]
  edge [
    source 27
    target 249
  ]
  edge [
    source 27
    target 216
  ]
  edge [
    source 27
    target 250
  ]
  edge [
    source 27
    target 146
  ]
  edge [
    source 27
    target 251
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 253
  ]
  edge [
    source 27
    target 254
  ]
  edge [
    source 27
    target 255
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 262
  ]
  edge [
    source 28
    target 263
  ]
  edge [
    source 28
    target 264
  ]
  edge [
    source 28
    target 265
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 266
  ]
  edge [
    source 29
    target 267
  ]
  edge [
    source 29
    target 268
  ]
  edge [
    source 29
    target 269
  ]
  edge [
    source 29
    target 270
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 271
  ]
  edge [
    source 30
    target 134
  ]
  edge [
    source 30
    target 272
  ]
  edge [
    source 30
    target 273
  ]
  edge [
    source 30
    target 274
  ]
  edge [
    source 30
    target 275
  ]
  edge [
    source 30
    target 276
  ]
  edge [
    source 30
    target 277
  ]
  edge [
    source 30
    target 278
  ]
  edge [
    source 30
    target 279
  ]
  edge [
    source 30
    target 280
  ]
  edge [
    source 30
    target 281
  ]
  edge [
    source 30
    target 282
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 283
  ]
  edge [
    source 31
    target 284
  ]
  edge [
    source 32
    target 285
  ]
  edge [
    source 32
    target 286
  ]
  edge [
    source 32
    target 287
  ]
  edge [
    source 32
    target 288
  ]
  edge [
    source 32
    target 289
  ]
  edge [
    source 32
    target 290
  ]
  edge [
    source 32
    target 291
  ]
  edge [
    source 32
    target 292
  ]
  edge [
    source 32
    target 293
  ]
  edge [
    source 32
    target 294
  ]
  edge [
    source 32
    target 295
  ]
  edge [
    source 32
    target 296
  ]
  edge [
    source 32
    target 297
  ]
  edge [
    source 32
    target 298
  ]
  edge [
    source 32
    target 299
  ]
  edge [
    source 32
    target 300
  ]
  edge [
    source 32
    target 301
  ]
  edge [
    source 32
    target 302
  ]
  edge [
    source 32
    target 303
  ]
  edge [
    source 32
    target 304
  ]
  edge [
    source 32
    target 305
  ]
  edge [
    source 32
    target 306
  ]
  edge [
    source 32
    target 307
  ]
  edge [
    source 32
    target 308
  ]
  edge [
    source 32
    target 309
  ]
  edge [
    source 32
    target 310
  ]
  edge [
    source 32
    target 311
  ]
  edge [
    source 32
    target 312
  ]
  edge [
    source 32
    target 313
  ]
  edge [
    source 32
    target 314
  ]
  edge [
    source 32
    target 315
  ]
  edge [
    source 32
    target 316
  ]
  edge [
    source 32
    target 317
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 32
    target 323
  ]
  edge [
    source 32
    target 324
  ]
  edge [
    source 32
    target 325
  ]
  edge [
    source 32
    target 326
  ]
  edge [
    source 32
    target 327
  ]
  edge [
    source 32
    target 328
  ]
  edge [
    source 32
    target 329
  ]
  edge [
    source 32
    target 330
  ]
  edge [
    source 32
    target 331
  ]
  edge [
    source 32
    target 332
  ]
  edge [
    source 32
    target 333
  ]
  edge [
    source 32
    target 334
  ]
  edge [
    source 32
    target 335
  ]
  edge [
    source 32
    target 336
  ]
  edge [
    source 32
    target 337
  ]
  edge [
    source 32
    target 338
  ]
  edge [
    source 32
    target 339
  ]
  edge [
    source 32
    target 340
  ]
  edge [
    source 32
    target 341
  ]
  edge [
    source 32
    target 342
  ]
  edge [
    source 32
    target 343
  ]
  edge [
    source 32
    target 344
  ]
  edge [
    source 32
    target 345
  ]
  edge [
    source 32
    target 346
  ]
  edge [
    source 32
    target 347
  ]
  edge [
    source 32
    target 348
  ]
  edge [
    source 32
    target 349
  ]
  edge [
    source 32
    target 350
  ]
  edge [
    source 32
    target 351
  ]
  edge [
    source 32
    target 352
  ]
  edge [
    source 32
    target 353
  ]
  edge [
    source 32
    target 354
  ]
  edge [
    source 32
    target 355
  ]
  edge [
    source 32
    target 356
  ]
  edge [
    source 32
    target 357
  ]
  edge [
    source 32
    target 358
  ]
  edge [
    source 32
    target 359
  ]
  edge [
    source 32
    target 360
  ]
  edge [
    source 32
    target 361
  ]
  edge [
    source 32
    target 362
  ]
  edge [
    source 32
    target 363
  ]
  edge [
    source 32
    target 364
  ]
  edge [
    source 32
    target 365
  ]
  edge [
    source 32
    target 366
  ]
  edge [
    source 32
    target 367
  ]
  edge [
    source 32
    target 368
  ]
  edge [
    source 32
    target 369
  ]
  edge [
    source 32
    target 370
  ]
  edge [
    source 32
    target 371
  ]
  edge [
    source 32
    target 372
  ]
  edge [
    source 32
    target 373
  ]
  edge [
    source 32
    target 374
  ]
  edge [
    source 32
    target 375
  ]
  edge [
    source 32
    target 376
  ]
  edge [
    source 32
    target 377
  ]
  edge [
    source 32
    target 378
  ]
  edge [
    source 32
    target 379
  ]
  edge [
    source 32
    target 380
  ]
  edge [
    source 32
    target 381
  ]
  edge [
    source 32
    target 382
  ]
  edge [
    source 32
    target 383
  ]
  edge [
    source 32
    target 384
  ]
  edge [
    source 32
    target 385
  ]
  edge [
    source 32
    target 386
  ]
  edge [
    source 32
    target 387
  ]
  edge [
    source 32
    target 388
  ]
  edge [
    source 32
    target 389
  ]
  edge [
    source 32
    target 390
  ]
  edge [
    source 32
    target 391
  ]
  edge [
    source 32
    target 392
  ]
  edge [
    source 32
    target 393
  ]
  edge [
    source 32
    target 394
  ]
  edge [
    source 32
    target 395
  ]
  edge [
    source 32
    target 396
  ]
  edge [
    source 32
    target 397
  ]
  edge [
    source 32
    target 398
  ]
  edge [
    source 32
    target 399
  ]
  edge [
    source 32
    target 400
  ]
  edge [
    source 32
    target 401
  ]
  edge [
    source 32
    target 402
  ]
  edge [
    source 32
    target 403
  ]
  edge [
    source 32
    target 404
  ]
  edge [
    source 32
    target 405
  ]
  edge [
    source 32
    target 406
  ]
  edge [
    source 32
    target 407
  ]
  edge [
    source 32
    target 408
  ]
  edge [
    source 32
    target 409
  ]
  edge [
    source 32
    target 410
  ]
  edge [
    source 32
    target 411
  ]
  edge [
    source 32
    target 412
  ]
  edge [
    source 32
    target 413
  ]
  edge [
    source 32
    target 414
  ]
  edge [
    source 32
    target 415
  ]
  edge [
    source 32
    target 416
  ]
  edge [
    source 32
    target 417
  ]
  edge [
    source 32
    target 418
  ]
  edge [
    source 32
    target 419
  ]
  edge [
    source 32
    target 420
  ]
  edge [
    source 32
    target 421
  ]
  edge [
    source 32
    target 422
  ]
  edge [
    source 32
    target 423
  ]
  edge [
    source 32
    target 424
  ]
  edge [
    source 32
    target 425
  ]
  edge [
    source 32
    target 426
  ]
  edge [
    source 32
    target 427
  ]
  edge [
    source 32
    target 428
  ]
  edge [
    source 32
    target 429
  ]
  edge [
    source 32
    target 430
  ]
  edge [
    source 32
    target 431
  ]
  edge [
    source 32
    target 432
  ]
  edge [
    source 32
    target 433
  ]
  edge [
    source 32
    target 434
  ]
  edge [
    source 32
    target 435
  ]
  edge [
    source 32
    target 436
  ]
  edge [
    source 32
    target 437
  ]
  edge [
    source 32
    target 438
  ]
  edge [
    source 32
    target 439
  ]
  edge [
    source 32
    target 440
  ]
  edge [
    source 32
    target 441
  ]
  edge [
    source 32
    target 442
  ]
  edge [
    source 32
    target 443
  ]
  edge [
    source 32
    target 444
  ]
  edge [
    source 32
    target 445
  ]
  edge [
    source 32
    target 446
  ]
  edge [
    source 32
    target 447
  ]
  edge [
    source 32
    target 448
  ]
  edge [
    source 32
    target 449
  ]
  edge [
    source 32
    target 450
  ]
  edge [
    source 32
    target 451
  ]
  edge [
    source 32
    target 452
  ]
  edge [
    source 32
    target 453
  ]
  edge [
    source 32
    target 454
  ]
  edge [
    source 32
    target 455
  ]
  edge [
    source 32
    target 456
  ]
  edge [
    source 32
    target 457
  ]
  edge [
    source 32
    target 458
  ]
  edge [
    source 32
    target 459
  ]
  edge [
    source 32
    target 460
  ]
  edge [
    source 32
    target 461
  ]
  edge [
    source 32
    target 462
  ]
  edge [
    source 32
    target 463
  ]
  edge [
    source 32
    target 464
  ]
  edge [
    source 32
    target 465
  ]
  edge [
    source 32
    target 466
  ]
  edge [
    source 32
    target 467
  ]
  edge [
    source 32
    target 468
  ]
  edge [
    source 32
    target 469
  ]
  edge [
    source 32
    target 470
  ]
  edge [
    source 32
    target 471
  ]
  edge [
    source 32
    target 472
  ]
  edge [
    source 32
    target 473
  ]
  edge [
    source 32
    target 474
  ]
  edge [
    source 32
    target 475
  ]
  edge [
    source 32
    target 476
  ]
  edge [
    source 32
    target 477
  ]
  edge [
    source 32
    target 478
  ]
  edge [
    source 32
    target 479
  ]
  edge [
    source 32
    target 480
  ]
  edge [
    source 32
    target 481
  ]
  edge [
    source 32
    target 482
  ]
  edge [
    source 32
    target 483
  ]
  edge [
    source 32
    target 484
  ]
  edge [
    source 32
    target 485
  ]
  edge [
    source 32
    target 486
  ]
  edge [
    source 32
    target 487
  ]
  edge [
    source 32
    target 488
  ]
  edge [
    source 32
    target 489
  ]
  edge [
    source 32
    target 490
  ]
  edge [
    source 32
    target 491
  ]
  edge [
    source 32
    target 492
  ]
  edge [
    source 32
    target 493
  ]
  edge [
    source 32
    target 494
  ]
  edge [
    source 32
    target 495
  ]
  edge [
    source 32
    target 496
  ]
  edge [
    source 32
    target 497
  ]
  edge [
    source 32
    target 498
  ]
  edge [
    source 32
    target 499
  ]
  edge [
    source 32
    target 500
  ]
  edge [
    source 32
    target 501
  ]
  edge [
    source 32
    target 502
  ]
  edge [
    source 32
    target 503
  ]
  edge [
    source 32
    target 504
  ]
  edge [
    source 32
    target 505
  ]
  edge [
    source 32
    target 506
  ]
  edge [
    source 32
    target 507
  ]
  edge [
    source 32
    target 508
  ]
  edge [
    source 32
    target 509
  ]
  edge [
    source 32
    target 510
  ]
  edge [
    source 32
    target 511
  ]
  edge [
    source 32
    target 512
  ]
  edge [
    source 32
    target 513
  ]
  edge [
    source 32
    target 514
  ]
  edge [
    source 32
    target 515
  ]
  edge [
    source 32
    target 516
  ]
  edge [
    source 32
    target 517
  ]
  edge [
    source 32
    target 518
  ]
  edge [
    source 32
    target 519
  ]
  edge [
    source 32
    target 520
  ]
  edge [
    source 32
    target 521
  ]
  edge [
    source 32
    target 522
  ]
  edge [
    source 32
    target 523
  ]
  edge [
    source 32
    target 524
  ]
  edge [
    source 32
    target 525
  ]
  edge [
    source 32
    target 526
  ]
  edge [
    source 32
    target 527
  ]
  edge [
    source 32
    target 528
  ]
  edge [
    source 32
    target 529
  ]
  edge [
    source 32
    target 530
  ]
  edge [
    source 32
    target 531
  ]
  edge [
    source 32
    target 532
  ]
  edge [
    source 32
    target 533
  ]
  edge [
    source 32
    target 534
  ]
  edge [
    source 32
    target 535
  ]
  edge [
    source 32
    target 536
  ]
  edge [
    source 32
    target 537
  ]
  edge [
    source 32
    target 538
  ]
  edge [
    source 32
    target 539
  ]
  edge [
    source 32
    target 540
  ]
  edge [
    source 32
    target 541
  ]
  edge [
    source 32
    target 542
  ]
  edge [
    source 32
    target 543
  ]
  edge [
    source 32
    target 544
  ]
  edge [
    source 32
    target 545
  ]
  edge [
    source 32
    target 546
  ]
  edge [
    source 32
    target 547
  ]
  edge [
    source 32
    target 548
  ]
  edge [
    source 32
    target 549
  ]
  edge [
    source 32
    target 550
  ]
  edge [
    source 32
    target 551
  ]
  edge [
    source 32
    target 552
  ]
  edge [
    source 32
    target 553
  ]
  edge [
    source 32
    target 554
  ]
  edge [
    source 32
    target 555
  ]
  edge [
    source 32
    target 556
  ]
  edge [
    source 32
    target 557
  ]
  edge [
    source 32
    target 558
  ]
  edge [
    source 32
    target 559
  ]
  edge [
    source 32
    target 560
  ]
  edge [
    source 32
    target 561
  ]
  edge [
    source 32
    target 562
  ]
  edge [
    source 32
    target 563
  ]
  edge [
    source 32
    target 564
  ]
  edge [
    source 32
    target 565
  ]
  edge [
    source 32
    target 566
  ]
  edge [
    source 32
    target 567
  ]
  edge [
    source 32
    target 568
  ]
  edge [
    source 32
    target 569
  ]
  edge [
    source 32
    target 570
  ]
  edge [
    source 32
    target 571
  ]
  edge [
    source 32
    target 572
  ]
  edge [
    source 32
    target 573
  ]
  edge [
    source 32
    target 574
  ]
  edge [
    source 32
    target 575
  ]
  edge [
    source 32
    target 576
  ]
  edge [
    source 32
    target 577
  ]
  edge [
    source 32
    target 578
  ]
  edge [
    source 32
    target 579
  ]
  edge [
    source 32
    target 580
  ]
  edge [
    source 32
    target 581
  ]
  edge [
    source 32
    target 582
  ]
  edge [
    source 32
    target 583
  ]
  edge [
    source 32
    target 584
  ]
  edge [
    source 32
    target 585
  ]
  edge [
    source 32
    target 586
  ]
  edge [
    source 32
    target 587
  ]
  edge [
    source 32
    target 588
  ]
  edge [
    source 32
    target 589
  ]
  edge [
    source 32
    target 590
  ]
  edge [
    source 32
    target 591
  ]
  edge [
    source 32
    target 592
  ]
  edge [
    source 32
    target 593
  ]
  edge [
    source 32
    target 594
  ]
  edge [
    source 32
    target 595
  ]
  edge [
    source 32
    target 596
  ]
  edge [
    source 32
    target 597
  ]
  edge [
    source 32
    target 598
  ]
  edge [
    source 32
    target 599
  ]
  edge [
    source 32
    target 600
  ]
  edge [
    source 32
    target 601
  ]
  edge [
    source 32
    target 602
  ]
  edge [
    source 32
    target 603
  ]
  edge [
    source 32
    target 604
  ]
  edge [
    source 32
    target 605
  ]
  edge [
    source 32
    target 606
  ]
  edge [
    source 32
    target 607
  ]
  edge [
    source 32
    target 608
  ]
  edge [
    source 32
    target 609
  ]
  edge [
    source 32
    target 610
  ]
  edge [
    source 32
    target 611
  ]
  edge [
    source 32
    target 612
  ]
  edge [
    source 32
    target 613
  ]
  edge [
    source 32
    target 614
  ]
  edge [
    source 32
    target 615
  ]
  edge [
    source 32
    target 616
  ]
  edge [
    source 32
    target 617
  ]
  edge [
    source 32
    target 618
  ]
  edge [
    source 32
    target 619
  ]
  edge [
    source 32
    target 620
  ]
  edge [
    source 32
    target 621
  ]
  edge [
    source 32
    target 622
  ]
  edge [
    source 32
    target 623
  ]
  edge [
    source 32
    target 624
  ]
  edge [
    source 32
    target 625
  ]
  edge [
    source 32
    target 626
  ]
  edge [
    source 32
    target 627
  ]
  edge [
    source 32
    target 628
  ]
  edge [
    source 32
    target 629
  ]
  edge [
    source 32
    target 630
  ]
  edge [
    source 32
    target 631
  ]
  edge [
    source 32
    target 632
  ]
  edge [
    source 32
    target 633
  ]
  edge [
    source 32
    target 634
  ]
  edge [
    source 32
    target 635
  ]
  edge [
    source 32
    target 636
  ]
  edge [
    source 32
    target 637
  ]
  edge [
    source 32
    target 638
  ]
  edge [
    source 32
    target 639
  ]
  edge [
    source 32
    target 640
  ]
  edge [
    source 32
    target 641
  ]
  edge [
    source 32
    target 642
  ]
  edge [
    source 32
    target 643
  ]
  edge [
    source 32
    target 644
  ]
  edge [
    source 32
    target 645
  ]
  edge [
    source 32
    target 646
  ]
  edge [
    source 32
    target 647
  ]
  edge [
    source 32
    target 648
  ]
  edge [
    source 32
    target 649
  ]
  edge [
    source 32
    target 650
  ]
  edge [
    source 32
    target 651
  ]
  edge [
    source 32
    target 652
  ]
  edge [
    source 32
    target 653
  ]
  edge [
    source 32
    target 654
  ]
  edge [
    source 32
    target 655
  ]
  edge [
    source 32
    target 656
  ]
  edge [
    source 32
    target 657
  ]
  edge [
    source 32
    target 658
  ]
  edge [
    source 32
    target 659
  ]
  edge [
    source 32
    target 660
  ]
  edge [
    source 32
    target 661
  ]
  edge [
    source 32
    target 662
  ]
  edge [
    source 32
    target 663
  ]
  edge [
    source 32
    target 664
  ]
  edge [
    source 32
    target 665
  ]
  edge [
    source 32
    target 666
  ]
  edge [
    source 32
    target 667
  ]
  edge [
    source 32
    target 668
  ]
  edge [
    source 32
    target 669
  ]
  edge [
    source 32
    target 670
  ]
  edge [
    source 32
    target 671
  ]
  edge [
    source 32
    target 672
  ]
  edge [
    source 32
    target 673
  ]
  edge [
    source 32
    target 674
  ]
  edge [
    source 32
    target 675
  ]
  edge [
    source 32
    target 676
  ]
  edge [
    source 32
    target 677
  ]
  edge [
    source 32
    target 678
  ]
  edge [
    source 32
    target 679
  ]
  edge [
    source 32
    target 680
  ]
  edge [
    source 32
    target 681
  ]
  edge [
    source 32
    target 682
  ]
  edge [
    source 32
    target 683
  ]
  edge [
    source 32
    target 684
  ]
  edge [
    source 32
    target 685
  ]
  edge [
    source 32
    target 686
  ]
  edge [
    source 32
    target 687
  ]
  edge [
    source 32
    target 688
  ]
  edge [
    source 32
    target 689
  ]
  edge [
    source 32
    target 690
  ]
  edge [
    source 32
    target 691
  ]
  edge [
    source 32
    target 692
  ]
  edge [
    source 32
    target 693
  ]
  edge [
    source 32
    target 694
  ]
  edge [
    source 32
    target 695
  ]
  edge [
    source 32
    target 696
  ]
  edge [
    source 32
    target 697
  ]
  edge [
    source 32
    target 698
  ]
  edge [
    source 32
    target 699
  ]
  edge [
    source 32
    target 700
  ]
  edge [
    source 32
    target 701
  ]
  edge [
    source 32
    target 702
  ]
  edge [
    source 32
    target 703
  ]
  edge [
    source 32
    target 704
  ]
  edge [
    source 32
    target 705
  ]
  edge [
    source 32
    target 706
  ]
  edge [
    source 32
    target 707
  ]
  edge [
    source 32
    target 708
  ]
  edge [
    source 32
    target 709
  ]
  edge [
    source 32
    target 710
  ]
  edge [
    source 32
    target 711
  ]
  edge [
    source 32
    target 712
  ]
  edge [
    source 32
    target 713
  ]
  edge [
    source 32
    target 714
  ]
  edge [
    source 32
    target 715
  ]
  edge [
    source 32
    target 716
  ]
  edge [
    source 32
    target 717
  ]
  edge [
    source 32
    target 718
  ]
  edge [
    source 32
    target 719
  ]
  edge [
    source 32
    target 720
  ]
  edge [
    source 32
    target 721
  ]
  edge [
    source 32
    target 722
  ]
  edge [
    source 32
    target 723
  ]
  edge [
    source 32
    target 724
  ]
  edge [
    source 32
    target 725
  ]
  edge [
    source 32
    target 726
  ]
  edge [
    source 32
    target 727
  ]
  edge [
    source 32
    target 728
  ]
  edge [
    source 32
    target 729
  ]
  edge [
    source 32
    target 730
  ]
  edge [
    source 32
    target 731
  ]
  edge [
    source 32
    target 732
  ]
  edge [
    source 32
    target 733
  ]
  edge [
    source 32
    target 734
  ]
  edge [
    source 32
    target 735
  ]
  edge [
    source 32
    target 736
  ]
  edge [
    source 32
    target 737
  ]
  edge [
    source 32
    target 738
  ]
  edge [
    source 32
    target 739
  ]
  edge [
    source 32
    target 740
  ]
  edge [
    source 32
    target 741
  ]
  edge [
    source 32
    target 742
  ]
  edge [
    source 32
    target 743
  ]
  edge [
    source 32
    target 744
  ]
  edge [
    source 32
    target 745
  ]
  edge [
    source 32
    target 746
  ]
  edge [
    source 32
    target 747
  ]
  edge [
    source 32
    target 748
  ]
  edge [
    source 32
    target 749
  ]
  edge [
    source 32
    target 750
  ]
  edge [
    source 32
    target 751
  ]
  edge [
    source 32
    target 752
  ]
  edge [
    source 32
    target 753
  ]
  edge [
    source 32
    target 754
  ]
  edge [
    source 32
    target 755
  ]
  edge [
    source 32
    target 756
  ]
  edge [
    source 32
    target 757
  ]
  edge [
    source 32
    target 758
  ]
  edge [
    source 32
    target 759
  ]
  edge [
    source 32
    target 760
  ]
  edge [
    source 32
    target 761
  ]
  edge [
    source 32
    target 762
  ]
  edge [
    source 32
    target 763
  ]
  edge [
    source 32
    target 764
  ]
  edge [
    source 32
    target 765
  ]
  edge [
    source 32
    target 766
  ]
  edge [
    source 32
    target 767
  ]
  edge [
    source 32
    target 768
  ]
  edge [
    source 32
    target 769
  ]
  edge [
    source 32
    target 770
  ]
  edge [
    source 32
    target 771
  ]
  edge [
    source 32
    target 772
  ]
  edge [
    source 32
    target 773
  ]
  edge [
    source 32
    target 774
  ]
  edge [
    source 32
    target 775
  ]
  edge [
    source 32
    target 776
  ]
  edge [
    source 32
    target 777
  ]
  edge [
    source 32
    target 778
  ]
  edge [
    source 32
    target 779
  ]
  edge [
    source 32
    target 780
  ]
  edge [
    source 32
    target 781
  ]
  edge [
    source 32
    target 782
  ]
  edge [
    source 32
    target 783
  ]
  edge [
    source 32
    target 784
  ]
  edge [
    source 32
    target 785
  ]
  edge [
    source 32
    target 786
  ]
  edge [
    source 32
    target 787
  ]
  edge [
    source 32
    target 788
  ]
  edge [
    source 32
    target 789
  ]
  edge [
    source 32
    target 790
  ]
  edge [
    source 32
    target 791
  ]
  edge [
    source 32
    target 792
  ]
  edge [
    source 32
    target 793
  ]
  edge [
    source 32
    target 794
  ]
  edge [
    source 32
    target 795
  ]
  edge [
    source 32
    target 796
  ]
  edge [
    source 32
    target 797
  ]
  edge [
    source 32
    target 798
  ]
  edge [
    source 32
    target 799
  ]
  edge [
    source 32
    target 800
  ]
  edge [
    source 32
    target 801
  ]
  edge [
    source 32
    target 802
  ]
  edge [
    source 32
    target 803
  ]
  edge [
    source 32
    target 804
  ]
  edge [
    source 32
    target 805
  ]
  edge [
    source 32
    target 806
  ]
  edge [
    source 32
    target 807
  ]
  edge [
    source 32
    target 808
  ]
  edge [
    source 32
    target 809
  ]
  edge [
    source 32
    target 810
  ]
  edge [
    source 32
    target 811
  ]
  edge [
    source 32
    target 812
  ]
  edge [
    source 32
    target 813
  ]
  edge [
    source 32
    target 814
  ]
  edge [
    source 32
    target 815
  ]
  edge [
    source 32
    target 816
  ]
  edge [
    source 32
    target 817
  ]
  edge [
    source 32
    target 818
  ]
  edge [
    source 32
    target 819
  ]
  edge [
    source 32
    target 820
  ]
  edge [
    source 32
    target 821
  ]
  edge [
    source 32
    target 822
  ]
  edge [
    source 32
    target 823
  ]
  edge [
    source 32
    target 824
  ]
  edge [
    source 32
    target 825
  ]
  edge [
    source 32
    target 826
  ]
  edge [
    source 32
    target 827
  ]
  edge [
    source 32
    target 828
  ]
  edge [
    source 32
    target 829
  ]
  edge [
    source 32
    target 830
  ]
  edge [
    source 32
    target 831
  ]
  edge [
    source 32
    target 832
  ]
  edge [
    source 32
    target 833
  ]
  edge [
    source 32
    target 834
  ]
  edge [
    source 32
    target 835
  ]
  edge [
    source 32
    target 836
  ]
  edge [
    source 32
    target 837
  ]
  edge [
    source 32
    target 838
  ]
  edge [
    source 32
    target 839
  ]
  edge [
    source 32
    target 840
  ]
  edge [
    source 32
    target 841
  ]
  edge [
    source 32
    target 842
  ]
  edge [
    source 32
    target 843
  ]
  edge [
    source 32
    target 844
  ]
  edge [
    source 32
    target 845
  ]
  edge [
    source 32
    target 846
  ]
  edge [
    source 32
    target 847
  ]
  edge [
    source 32
    target 848
  ]
  edge [
    source 32
    target 849
  ]
  edge [
    source 32
    target 850
  ]
  edge [
    source 32
    target 851
  ]
  edge [
    source 32
    target 852
  ]
  edge [
    source 32
    target 853
  ]
  edge [
    source 32
    target 854
  ]
  edge [
    source 32
    target 855
  ]
  edge [
    source 32
    target 856
  ]
  edge [
    source 32
    target 857
  ]
  edge [
    source 32
    target 858
  ]
  edge [
    source 32
    target 859
  ]
  edge [
    source 32
    target 860
  ]
  edge [
    source 32
    target 861
  ]
  edge [
    source 32
    target 862
  ]
  edge [
    source 32
    target 863
  ]
  edge [
    source 32
    target 864
  ]
  edge [
    source 32
    target 865
  ]
  edge [
    source 32
    target 866
  ]
  edge [
    source 32
    target 867
  ]
  edge [
    source 32
    target 868
  ]
  edge [
    source 32
    target 869
  ]
  edge [
    source 32
    target 870
  ]
  edge [
    source 32
    target 871
  ]
  edge [
    source 32
    target 872
  ]
  edge [
    source 32
    target 873
  ]
  edge [
    source 32
    target 874
  ]
  edge [
    source 32
    target 875
  ]
  edge [
    source 32
    target 876
  ]
  edge [
    source 32
    target 877
  ]
  edge [
    source 32
    target 878
  ]
  edge [
    source 32
    target 879
  ]
  edge [
    source 32
    target 880
  ]
  edge [
    source 32
    target 881
  ]
  edge [
    source 32
    target 882
  ]
  edge [
    source 32
    target 883
  ]
  edge [
    source 32
    target 884
  ]
  edge [
    source 32
    target 885
  ]
  edge [
    source 32
    target 886
  ]
  edge [
    source 32
    target 887
  ]
  edge [
    source 32
    target 888
  ]
  edge [
    source 32
    target 889
  ]
  edge [
    source 32
    target 890
  ]
  edge [
    source 32
    target 891
  ]
  edge [
    source 32
    target 892
  ]
  edge [
    source 32
    target 893
  ]
  edge [
    source 32
    target 894
  ]
  edge [
    source 32
    target 895
  ]
  edge [
    source 32
    target 896
  ]
  edge [
    source 32
    target 897
  ]
  edge [
    source 32
    target 898
  ]
  edge [
    source 32
    target 899
  ]
  edge [
    source 32
    target 900
  ]
  edge [
    source 32
    target 901
  ]
  edge [
    source 32
    target 902
  ]
  edge [
    source 32
    target 903
  ]
  edge [
    source 32
    target 904
  ]
  edge [
    source 32
    target 905
  ]
  edge [
    source 32
    target 906
  ]
  edge [
    source 32
    target 907
  ]
  edge [
    source 32
    target 908
  ]
  edge [
    source 32
    target 909
  ]
  edge [
    source 32
    target 910
  ]
  edge [
    source 32
    target 911
  ]
  edge [
    source 32
    target 912
  ]
  edge [
    source 32
    target 913
  ]
  edge [
    source 32
    target 914
  ]
  edge [
    source 32
    target 915
  ]
  edge [
    source 32
    target 916
  ]
  edge [
    source 32
    target 917
  ]
  edge [
    source 32
    target 918
  ]
  edge [
    source 32
    target 919
  ]
  edge [
    source 32
    target 920
  ]
  edge [
    source 32
    target 921
  ]
  edge [
    source 32
    target 922
  ]
  edge [
    source 32
    target 923
  ]
  edge [
    source 32
    target 924
  ]
  edge [
    source 32
    target 925
  ]
  edge [
    source 32
    target 926
  ]
  edge [
    source 32
    target 927
  ]
  edge [
    source 32
    target 928
  ]
  edge [
    source 32
    target 929
  ]
  edge [
    source 32
    target 930
  ]
  edge [
    source 32
    target 931
  ]
  edge [
    source 32
    target 932
  ]
  edge [
    source 32
    target 933
  ]
  edge [
    source 32
    target 934
  ]
  edge [
    source 32
    target 935
  ]
  edge [
    source 32
    target 936
  ]
  edge [
    source 32
    target 937
  ]
  edge [
    source 32
    target 938
  ]
  edge [
    source 32
    target 939
  ]
  edge [
    source 32
    target 940
  ]
  edge [
    source 32
    target 941
  ]
  edge [
    source 32
    target 942
  ]
  edge [
    source 32
    target 943
  ]
  edge [
    source 32
    target 944
  ]
  edge [
    source 32
    target 945
  ]
  edge [
    source 32
    target 946
  ]
  edge [
    source 32
    target 947
  ]
  edge [
    source 32
    target 948
  ]
  edge [
    source 32
    target 949
  ]
  edge [
    source 32
    target 950
  ]
  edge [
    source 32
    target 951
  ]
  edge [
    source 32
    target 952
  ]
  edge [
    source 32
    target 953
  ]
  edge [
    source 32
    target 954
  ]
  edge [
    source 32
    target 955
  ]
  edge [
    source 32
    target 956
  ]
  edge [
    source 32
    target 957
  ]
  edge [
    source 32
    target 958
  ]
  edge [
    source 32
    target 959
  ]
  edge [
    source 32
    target 960
  ]
  edge [
    source 32
    target 961
  ]
  edge [
    source 32
    target 962
  ]
  edge [
    source 32
    target 963
  ]
  edge [
    source 32
    target 964
  ]
  edge [
    source 32
    target 965
  ]
  edge [
    source 32
    target 966
  ]
  edge [
    source 32
    target 967
  ]
  edge [
    source 32
    target 968
  ]
  edge [
    source 32
    target 969
  ]
  edge [
    source 32
    target 970
  ]
  edge [
    source 32
    target 971
  ]
  edge [
    source 32
    target 972
  ]
  edge [
    source 32
    target 973
  ]
  edge [
    source 32
    target 974
  ]
  edge [
    source 32
    target 975
  ]
  edge [
    source 32
    target 976
  ]
  edge [
    source 32
    target 977
  ]
  edge [
    source 32
    target 978
  ]
  edge [
    source 32
    target 979
  ]
  edge [
    source 32
    target 980
  ]
  edge [
    source 32
    target 981
  ]
  edge [
    source 32
    target 982
  ]
  edge [
    source 32
    target 983
  ]
  edge [
    source 32
    target 984
  ]
  edge [
    source 32
    target 985
  ]
  edge [
    source 32
    target 986
  ]
  edge [
    source 32
    target 987
  ]
  edge [
    source 32
    target 988
  ]
  edge [
    source 32
    target 989
  ]
  edge [
    source 32
    target 990
  ]
  edge [
    source 32
    target 991
  ]
  edge [
    source 32
    target 992
  ]
  edge [
    source 32
    target 993
  ]
  edge [
    source 32
    target 994
  ]
  edge [
    source 32
    target 995
  ]
  edge [
    source 32
    target 996
  ]
  edge [
    source 32
    target 997
  ]
  edge [
    source 32
    target 998
  ]
  edge [
    source 32
    target 999
  ]
  edge [
    source 32
    target 1000
  ]
  edge [
    source 32
    target 1001
  ]
  edge [
    source 32
    target 1002
  ]
  edge [
    source 32
    target 1003
  ]
  edge [
    source 32
    target 1004
  ]
  edge [
    source 32
    target 1005
  ]
  edge [
    source 32
    target 1006
  ]
  edge [
    source 32
    target 1007
  ]
  edge [
    source 32
    target 1008
  ]
  edge [
    source 32
    target 1009
  ]
  edge [
    source 32
    target 1010
  ]
  edge [
    source 32
    target 1011
  ]
  edge [
    source 32
    target 1012
  ]
  edge [
    source 32
    target 1013
  ]
  edge [
    source 32
    target 1014
  ]
  edge [
    source 32
    target 1015
  ]
  edge [
    source 32
    target 1016
  ]
  edge [
    source 32
    target 1017
  ]
  edge [
    source 32
    target 1018
  ]
  edge [
    source 32
    target 1019
  ]
  edge [
    source 32
    target 1020
  ]
  edge [
    source 32
    target 1021
  ]
  edge [
    source 32
    target 1022
  ]
  edge [
    source 32
    target 1023
  ]
  edge [
    source 32
    target 1024
  ]
  edge [
    source 32
    target 1025
  ]
  edge [
    source 32
    target 1026
  ]
  edge [
    source 32
    target 1027
  ]
  edge [
    source 32
    target 1028
  ]
  edge [
    source 32
    target 1029
  ]
  edge [
    source 32
    target 1030
  ]
  edge [
    source 32
    target 1031
  ]
  edge [
    source 32
    target 1032
  ]
  edge [
    source 32
    target 1033
  ]
  edge [
    source 32
    target 1034
  ]
  edge [
    source 32
    target 1035
  ]
  edge [
    source 32
    target 1036
  ]
  edge [
    source 32
    target 1037
  ]
  edge [
    source 32
    target 1038
  ]
  edge [
    source 32
    target 1039
  ]
  edge [
    source 32
    target 1040
  ]
  edge [
    source 32
    target 1041
  ]
  edge [
    source 32
    target 1042
  ]
  edge [
    source 32
    target 1043
  ]
  edge [
    source 32
    target 1044
  ]
  edge [
    source 32
    target 1045
  ]
  edge [
    source 32
    target 1046
  ]
  edge [
    source 32
    target 1047
  ]
  edge [
    source 32
    target 1048
  ]
  edge [
    source 32
    target 1049
  ]
  edge [
    source 32
    target 1050
  ]
  edge [
    source 32
    target 1051
  ]
  edge [
    source 32
    target 1052
  ]
  edge [
    source 32
    target 1053
  ]
  edge [
    source 32
    target 1054
  ]
  edge [
    source 32
    target 1055
  ]
  edge [
    source 32
    target 1056
  ]
  edge [
    source 32
    target 1057
  ]
  edge [
    source 32
    target 1058
  ]
  edge [
    source 32
    target 1059
  ]
  edge [
    source 32
    target 1060
  ]
  edge [
    source 32
    target 1061
  ]
  edge [
    source 32
    target 1062
  ]
  edge [
    source 32
    target 1063
  ]
  edge [
    source 32
    target 1064
  ]
  edge [
    source 32
    target 1065
  ]
  edge [
    source 32
    target 1066
  ]
  edge [
    source 32
    target 1067
  ]
  edge [
    source 32
    target 1068
  ]
  edge [
    source 32
    target 1069
  ]
  edge [
    source 32
    target 1070
  ]
  edge [
    source 32
    target 1071
  ]
  edge [
    source 32
    target 1072
  ]
  edge [
    source 32
    target 1073
  ]
  edge [
    source 32
    target 1074
  ]
  edge [
    source 32
    target 1075
  ]
  edge [
    source 32
    target 1076
  ]
  edge [
    source 32
    target 1077
  ]
  edge [
    source 32
    target 1078
  ]
  edge [
    source 32
    target 1079
  ]
  edge [
    source 32
    target 1080
  ]
  edge [
    source 32
    target 1081
  ]
  edge [
    source 32
    target 1082
  ]
  edge [
    source 32
    target 1083
  ]
  edge [
    source 32
    target 1084
  ]
  edge [
    source 32
    target 1085
  ]
  edge [
    source 32
    target 1086
  ]
  edge [
    source 32
    target 1087
  ]
  edge [
    source 32
    target 1088
  ]
  edge [
    source 32
    target 1089
  ]
  edge [
    source 32
    target 1090
  ]
  edge [
    source 32
    target 1091
  ]
  edge [
    source 32
    target 1092
  ]
  edge [
    source 32
    target 1093
  ]
  edge [
    source 32
    target 1094
  ]
  edge [
    source 32
    target 1095
  ]
  edge [
    source 32
    target 1096
  ]
  edge [
    source 32
    target 1097
  ]
  edge [
    source 32
    target 1098
  ]
  edge [
    source 32
    target 1099
  ]
  edge [
    source 32
    target 1100
  ]
  edge [
    source 32
    target 1101
  ]
  edge [
    source 32
    target 1102
  ]
  edge [
    source 32
    target 1103
  ]
  edge [
    source 32
    target 1104
  ]
  edge [
    source 32
    target 1105
  ]
  edge [
    source 32
    target 1106
  ]
  edge [
    source 32
    target 1107
  ]
  edge [
    source 32
    target 1108
  ]
  edge [
    source 32
    target 1109
  ]
  edge [
    source 32
    target 1110
  ]
  edge [
    source 32
    target 1111
  ]
  edge [
    source 32
    target 1112
  ]
  edge [
    source 32
    target 1113
  ]
  edge [
    source 32
    target 1114
  ]
  edge [
    source 32
    target 1115
  ]
  edge [
    source 32
    target 1116
  ]
  edge [
    source 32
    target 1117
  ]
  edge [
    source 32
    target 1118
  ]
  edge [
    source 32
    target 1119
  ]
  edge [
    source 32
    target 1120
  ]
  edge [
    source 32
    target 1121
  ]
  edge [
    source 32
    target 1122
  ]
  edge [
    source 32
    target 1123
  ]
  edge [
    source 32
    target 1124
  ]
  edge [
    source 32
    target 1125
  ]
  edge [
    source 32
    target 1126
  ]
  edge [
    source 32
    target 1127
  ]
  edge [
    source 32
    target 1128
  ]
  edge [
    source 32
    target 1129
  ]
  edge [
    source 32
    target 1130
  ]
  edge [
    source 32
    target 1131
  ]
  edge [
    source 32
    target 1132
  ]
  edge [
    source 32
    target 1133
  ]
  edge [
    source 32
    target 1134
  ]
  edge [
    source 32
    target 1135
  ]
  edge [
    source 32
    target 1136
  ]
  edge [
    source 32
    target 1137
  ]
  edge [
    source 32
    target 1138
  ]
  edge [
    source 32
    target 1139
  ]
  edge [
    source 32
    target 1140
  ]
  edge [
    source 32
    target 1141
  ]
  edge [
    source 32
    target 1142
  ]
  edge [
    source 32
    target 1143
  ]
  edge [
    source 32
    target 1144
  ]
  edge [
    source 32
    target 1145
  ]
  edge [
    source 32
    target 1146
  ]
  edge [
    source 32
    target 1147
  ]
  edge [
    source 32
    target 1148
  ]
  edge [
    source 32
    target 1149
  ]
  edge [
    source 32
    target 1150
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1151
  ]
  edge [
    source 33
    target 1152
  ]
  edge [
    source 33
    target 1153
  ]
  edge [
    source 33
    target 1154
  ]
  edge [
    source 33
    target 1155
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1156
  ]
  edge [
    source 34
    target 1157
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1158
  ]
  edge [
    source 35
    target 1159
  ]
  edge [
    source 35
    target 1160
  ]
  edge [
    source 35
    target 1161
  ]
  edge [
    source 35
    target 1162
  ]
  edge [
    source 35
    target 1163
  ]
  edge [
    source 35
    target 1164
  ]
  edge [
    source 35
    target 1165
  ]
  edge [
    source 35
    target 1166
  ]
  edge [
    source 35
    target 1167
  ]
  edge [
    source 35
    target 1168
  ]
  edge [
    source 35
    target 1169
  ]
  edge [
    source 35
    target 216
  ]
  edge [
    source 35
    target 1170
  ]
  edge [
    source 35
    target 1171
  ]
  edge [
    source 35
    target 142
  ]
  edge [
    source 35
    target 1172
  ]
  edge [
    source 35
    target 1173
  ]
  edge [
    source 35
    target 1174
  ]
  edge [
    source 35
    target 1175
  ]
  edge [
    source 35
    target 1176
  ]
  edge [
    source 35
    target 1177
  ]
  edge [
    source 36
    target 1178
  ]
  edge [
    source 36
    target 1179
  ]
  edge [
    source 36
    target 1180
  ]
  edge [
    source 36
    target 1181
  ]
  edge [
    source 36
    target 1182
  ]
  edge [
    source 36
    target 1183
  ]
]
