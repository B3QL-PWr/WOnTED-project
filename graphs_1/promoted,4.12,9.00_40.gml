graph [
  maxDegree 26
  minDegree 1
  meanDegree 2
  density 0.015267175572519083
  graphCliqueNumber 3
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "znalezisko"
    origin "text"
  ]
  node [
    id 2
    label "chajzer"
    origin "text"
  ]
  node [
    id 3
    label "u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 4
    label "bez"
    origin "text"
  ]
  node [
    id 5
    label "pytanie"
    origin "text"
  ]
  node [
    id 6
    label "swoje"
    origin "text"
  ]
  node [
    id 7
    label "reporta&#380;"
    origin "text"
  ]
  node [
    id 8
    label "praca"
    origin "text"
  ]
  node [
    id 9
    label "rysowniczka"
    origin "text"
  ]
  node [
    id 10
    label "rynn"
    origin "text"
  ]
  node [
    id 11
    label "rysowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "zablokowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "facebookowy"
    origin "text"
  ]
  node [
    id 14
    label "konto"
    origin "text"
  ]
  node [
    id 15
    label "udost&#281;pni&#263;"
    origin "text"
  ]
  node [
    id 16
    label "fragment"
    origin "text"
  ]
  node [
    id 17
    label "si&#281;ga&#263;"
  ]
  node [
    id 18
    label "zna&#263;"
  ]
  node [
    id 19
    label "troska&#263;_si&#281;"
  ]
  node [
    id 20
    label "zachowywa&#263;"
  ]
  node [
    id 21
    label "chowa&#263;"
  ]
  node [
    id 22
    label "think"
  ]
  node [
    id 23
    label "pilnowa&#263;"
  ]
  node [
    id 24
    label "robi&#263;"
  ]
  node [
    id 25
    label "recall"
  ]
  node [
    id 26
    label "echo"
  ]
  node [
    id 27
    label "take_care"
  ]
  node [
    id 28
    label "przedmiot"
  ]
  node [
    id 29
    label "zrobi&#263;"
  ]
  node [
    id 30
    label "employment"
  ]
  node [
    id 31
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 32
    label "skorzysta&#263;"
  ]
  node [
    id 33
    label "seize"
  ]
  node [
    id 34
    label "dozna&#263;"
  ]
  node [
    id 35
    label "utilize"
  ]
  node [
    id 36
    label "wykorzysta&#263;"
  ]
  node [
    id 37
    label "ki&#347;&#263;"
  ]
  node [
    id 38
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 39
    label "krzew"
  ]
  node [
    id 40
    label "pi&#380;maczkowate"
  ]
  node [
    id 41
    label "pestkowiec"
  ]
  node [
    id 42
    label "kwiat"
  ]
  node [
    id 43
    label "owoc"
  ]
  node [
    id 44
    label "oliwkowate"
  ]
  node [
    id 45
    label "ro&#347;lina"
  ]
  node [
    id 46
    label "hy&#263;ka"
  ]
  node [
    id 47
    label "lilac"
  ]
  node [
    id 48
    label "delfinidyna"
  ]
  node [
    id 49
    label "sprawa"
  ]
  node [
    id 50
    label "zadanie"
  ]
  node [
    id 51
    label "wypowied&#378;"
  ]
  node [
    id 52
    label "problemat"
  ]
  node [
    id 53
    label "rozpytywanie"
  ]
  node [
    id 54
    label "sprawdzian"
  ]
  node [
    id 55
    label "przes&#322;uchiwanie"
  ]
  node [
    id 56
    label "wypytanie"
  ]
  node [
    id 57
    label "zwracanie_si&#281;"
  ]
  node [
    id 58
    label "wypowiedzenie"
  ]
  node [
    id 59
    label "wywo&#322;ywanie"
  ]
  node [
    id 60
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 61
    label "problematyka"
  ]
  node [
    id 62
    label "question"
  ]
  node [
    id 63
    label "sprawdzanie"
  ]
  node [
    id 64
    label "odpowiadanie"
  ]
  node [
    id 65
    label "survey"
  ]
  node [
    id 66
    label "odpowiada&#263;"
  ]
  node [
    id 67
    label "egzaminowanie"
  ]
  node [
    id 68
    label "publicystyka"
  ]
  node [
    id 69
    label "audycja"
  ]
  node [
    id 70
    label "utw&#243;r"
  ]
  node [
    id 71
    label "stosunek_pracy"
  ]
  node [
    id 72
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 73
    label "benedykty&#324;ski"
  ]
  node [
    id 74
    label "pracowanie"
  ]
  node [
    id 75
    label "zaw&#243;d"
  ]
  node [
    id 76
    label "kierownictwo"
  ]
  node [
    id 77
    label "zmiana"
  ]
  node [
    id 78
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 79
    label "wytw&#243;r"
  ]
  node [
    id 80
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 81
    label "tynkarski"
  ]
  node [
    id 82
    label "czynnik_produkcji"
  ]
  node [
    id 83
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 84
    label "zobowi&#261;zanie"
  ]
  node [
    id 85
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 86
    label "czynno&#347;&#263;"
  ]
  node [
    id 87
    label "tyrka"
  ]
  node [
    id 88
    label "pracowa&#263;"
  ]
  node [
    id 89
    label "siedziba"
  ]
  node [
    id 90
    label "poda&#380;_pracy"
  ]
  node [
    id 91
    label "miejsce"
  ]
  node [
    id 92
    label "zak&#322;ad"
  ]
  node [
    id 93
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 94
    label "najem"
  ]
  node [
    id 95
    label "opowiada&#263;"
  ]
  node [
    id 96
    label "kancerowa&#263;"
  ]
  node [
    id 97
    label "kre&#347;li&#263;"
  ]
  node [
    id 98
    label "draw"
  ]
  node [
    id 99
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 100
    label "describe"
  ]
  node [
    id 101
    label "throng"
  ]
  node [
    id 102
    label "zaj&#261;&#263;"
  ]
  node [
    id 103
    label "przerwa&#263;"
  ]
  node [
    id 104
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 105
    label "zatrzyma&#263;"
  ]
  node [
    id 106
    label "wstrzyma&#263;"
  ]
  node [
    id 107
    label "interlock"
  ]
  node [
    id 108
    label "unieruchomi&#263;"
  ]
  node [
    id 109
    label "suspend"
  ]
  node [
    id 110
    label "przeszkodzi&#263;"
  ]
  node [
    id 111
    label "lock"
  ]
  node [
    id 112
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 113
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 114
    label "dost&#281;p"
  ]
  node [
    id 115
    label "bank"
  ]
  node [
    id 116
    label "kariera"
  ]
  node [
    id 117
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 118
    label "dorobek"
  ]
  node [
    id 119
    label "debet"
  ]
  node [
    id 120
    label "rachunek"
  ]
  node [
    id 121
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 122
    label "mienie"
  ]
  node [
    id 123
    label "subkonto"
  ]
  node [
    id 124
    label "kredyt"
  ]
  node [
    id 125
    label "reprezentacja"
  ]
  node [
    id 126
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 127
    label "open"
  ]
  node [
    id 128
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 129
    label "dzienia"
  ]
  node [
    id 130
    label "dobry"
  ]
  node [
    id 131
    label "TVN"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 70
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 131
  ]
  edge [
    source 130
    target 131
  ]
]
