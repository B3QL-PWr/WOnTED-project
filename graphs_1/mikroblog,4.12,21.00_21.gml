graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9705882352941178
  density 0.029411764705882353
  graphCliqueNumber 2
  node [
    id 0
    label "pe&#322;nia"
    origin "text"
  ]
  node [
    id 1
    label "sprawny"
    origin "text"
  ]
  node [
    id 2
    label "telefon"
    origin "text"
  ]
  node [
    id 3
    label "wystawi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "nieco"
    origin "text"
  ]
  node [
    id 5
    label "ponad"
    origin "text"
  ]
  node [
    id 6
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 7
    label "zakres"
  ]
  node [
    id 8
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 9
    label "szczyt"
  ]
  node [
    id 10
    label "letki"
  ]
  node [
    id 11
    label "zdrowy"
  ]
  node [
    id 12
    label "dzia&#322;alny"
  ]
  node [
    id 13
    label "sprawnie"
  ]
  node [
    id 14
    label "dobry"
  ]
  node [
    id 15
    label "umiej&#281;tny"
  ]
  node [
    id 16
    label "szybki"
  ]
  node [
    id 17
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 18
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 19
    label "coalescence"
  ]
  node [
    id 20
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 21
    label "phreaker"
  ]
  node [
    id 22
    label "infrastruktura"
  ]
  node [
    id 23
    label "wy&#347;wietlacz"
  ]
  node [
    id 24
    label "provider"
  ]
  node [
    id 25
    label "dzwonienie"
  ]
  node [
    id 26
    label "zadzwoni&#263;"
  ]
  node [
    id 27
    label "dzwoni&#263;"
  ]
  node [
    id 28
    label "kontakt"
  ]
  node [
    id 29
    label "mikrotelefon"
  ]
  node [
    id 30
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 31
    label "po&#322;&#261;czenie"
  ]
  node [
    id 32
    label "numer"
  ]
  node [
    id 33
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 34
    label "instalacja"
  ]
  node [
    id 35
    label "billing"
  ]
  node [
    id 36
    label "urz&#261;dzenie"
  ]
  node [
    id 37
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 38
    label "wysun&#261;&#263;"
  ]
  node [
    id 39
    label "zbudowa&#263;"
  ]
  node [
    id 40
    label "wyrazi&#263;"
  ]
  node [
    id 41
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 42
    label "przedstawi&#263;"
  ]
  node [
    id 43
    label "wypisa&#263;"
  ]
  node [
    id 44
    label "wskaza&#263;"
  ]
  node [
    id 45
    label "indicate"
  ]
  node [
    id 46
    label "wynie&#347;&#263;"
  ]
  node [
    id 47
    label "pies_my&#347;liwski"
  ]
  node [
    id 48
    label "zaproponowa&#263;"
  ]
  node [
    id 49
    label "wyeksponowa&#263;"
  ]
  node [
    id 50
    label "wychyli&#263;"
  ]
  node [
    id 51
    label "set"
  ]
  node [
    id 52
    label "wyj&#261;&#263;"
  ]
  node [
    id 53
    label "szlachetny"
  ]
  node [
    id 54
    label "metaliczny"
  ]
  node [
    id 55
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 56
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 57
    label "grosz"
  ]
  node [
    id 58
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 59
    label "utytu&#322;owany"
  ]
  node [
    id 60
    label "poz&#322;ocenie"
  ]
  node [
    id 61
    label "Polska"
  ]
  node [
    id 62
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 63
    label "wspania&#322;y"
  ]
  node [
    id 64
    label "doskona&#322;y"
  ]
  node [
    id 65
    label "kochany"
  ]
  node [
    id 66
    label "jednostka_monetarna"
  ]
  node [
    id 67
    label "z&#322;ocenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
]
