graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9714285714285715
  density 0.02857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "w&#322;a&#347;ciwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "grzyb"
    origin "text"
  ]
  node [
    id 2
    label "odnale&#378;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pakistan"
    origin "text"
  ]
  node [
    id 4
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "zbawienny"
    origin "text"
  ]
  node [
    id 7
    label "dla"
    origin "text"
  ]
  node [
    id 8
    label "nasz"
    origin "text"
  ]
  node [
    id 9
    label "planet"
    origin "text"
  ]
  node [
    id 10
    label "wyregulowanie"
  ]
  node [
    id 11
    label "warto&#347;&#263;"
  ]
  node [
    id 12
    label "charakterystyka"
  ]
  node [
    id 13
    label "kompetencja"
  ]
  node [
    id 14
    label "cecha"
  ]
  node [
    id 15
    label "regulowanie"
  ]
  node [
    id 16
    label "feature"
  ]
  node [
    id 17
    label "wyregulowa&#263;"
  ]
  node [
    id 18
    label "regulowa&#263;"
  ]
  node [
    id 19
    label "standard"
  ]
  node [
    id 20
    label "attribute"
  ]
  node [
    id 21
    label "zaleta"
  ]
  node [
    id 22
    label "pierdo&#322;a"
  ]
  node [
    id 23
    label "borowiec"
  ]
  node [
    id 24
    label "zarodnia"
  ]
  node [
    id 25
    label "fungus"
  ]
  node [
    id 26
    label "ko&#378;larz"
  ]
  node [
    id 27
    label "tetryk"
  ]
  node [
    id 28
    label "gametangium"
  ]
  node [
    id 29
    label "fungal_infection"
  ]
  node [
    id 30
    label "grzyby"
  ]
  node [
    id 31
    label "pieczarniak"
  ]
  node [
    id 32
    label "blanszownik"
  ]
  node [
    id 33
    label "plechowiec"
  ]
  node [
    id 34
    label "plemnia"
  ]
  node [
    id 35
    label "zramolenie"
  ]
  node [
    id 36
    label "starzec"
  ]
  node [
    id 37
    label "zrz&#281;da"
  ]
  node [
    id 38
    label "borowikowate"
  ]
  node [
    id 39
    label "ramolenie"
  ]
  node [
    id 40
    label "kszta&#322;t"
  ]
  node [
    id 41
    label "choroba_somatyczna"
  ]
  node [
    id 42
    label "papierzak"
  ]
  node [
    id 43
    label "odzyska&#263;"
  ]
  node [
    id 44
    label "zauwa&#380;y&#263;"
  ]
  node [
    id 45
    label "wybra&#263;"
  ]
  node [
    id 46
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 47
    label "znale&#378;&#263;"
  ]
  node [
    id 48
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 49
    label "discover"
  ]
  node [
    id 50
    label "znaj&#347;&#263;"
  ]
  node [
    id 51
    label "devise"
  ]
  node [
    id 52
    label "uprawi&#263;"
  ]
  node [
    id 53
    label "gotowy"
  ]
  node [
    id 54
    label "might"
  ]
  node [
    id 55
    label "si&#281;ga&#263;"
  ]
  node [
    id 56
    label "trwa&#263;"
  ]
  node [
    id 57
    label "obecno&#347;&#263;"
  ]
  node [
    id 58
    label "stan"
  ]
  node [
    id 59
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 60
    label "stand"
  ]
  node [
    id 61
    label "mie&#263;_miejsce"
  ]
  node [
    id 62
    label "uczestniczy&#263;"
  ]
  node [
    id 63
    label "chodzi&#263;"
  ]
  node [
    id 64
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 65
    label "equal"
  ]
  node [
    id 66
    label "dobroczynny"
  ]
  node [
    id 67
    label "zbawiennie"
  ]
  node [
    id 68
    label "czyj&#347;"
  ]
  node [
    id 69
    label "narz&#281;dzie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 9
    target 69
  ]
]
