graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.1052631578947367
  density 0.018630647414997672
  graphCliqueNumber 4
  node [
    id 0
    label "stwierdza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "sejm"
    origin "text"
  ]
  node [
    id 2
    label "wobec"
    origin "text"
  ]
  node [
    id 3
    label "uzyska&#263;"
    origin "text"
  ]
  node [
    id 4
    label "bezwzgl&#281;dny"
    origin "text"
  ]
  node [
    id 5
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 7
    label "poprawka"
    origin "text"
  ]
  node [
    id 8
    label "senat"
    origin "text"
  ]
  node [
    id 9
    label "przyj&#261;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "uznawa&#263;"
  ]
  node [
    id 11
    label "oznajmia&#263;"
  ]
  node [
    id 12
    label "attest"
  ]
  node [
    id 13
    label "parlament"
  ]
  node [
    id 14
    label "obrady"
  ]
  node [
    id 15
    label "grupa"
  ]
  node [
    id 16
    label "lewica"
  ]
  node [
    id 17
    label "zgromadzenie"
  ]
  node [
    id 18
    label "prawica"
  ]
  node [
    id 19
    label "centrum"
  ]
  node [
    id 20
    label "izba_ni&#380;sza"
  ]
  node [
    id 21
    label "siedziba"
  ]
  node [
    id 22
    label "parliament"
  ]
  node [
    id 23
    label "promocja"
  ]
  node [
    id 24
    label "give_birth"
  ]
  node [
    id 25
    label "wytworzy&#263;"
  ]
  node [
    id 26
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 27
    label "realize"
  ]
  node [
    id 28
    label "zrobi&#263;"
  ]
  node [
    id 29
    label "make"
  ]
  node [
    id 30
    label "generalizowa&#263;"
  ]
  node [
    id 31
    label "bezwzgl&#281;dnie"
  ]
  node [
    id 32
    label "zdecydowany"
  ]
  node [
    id 33
    label "bezsporny"
  ]
  node [
    id 34
    label "okrutny"
  ]
  node [
    id 35
    label "jednoznaczny"
  ]
  node [
    id 36
    label "zupe&#322;ny"
  ]
  node [
    id 37
    label "obiektywny"
  ]
  node [
    id 38
    label "pe&#322;ny"
  ]
  node [
    id 39
    label "surowy"
  ]
  node [
    id 40
    label "majority"
  ]
  node [
    id 41
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 42
    label "opinion"
  ]
  node [
    id 43
    label "wypowied&#378;"
  ]
  node [
    id 44
    label "zmatowienie"
  ]
  node [
    id 45
    label "wpa&#347;&#263;"
  ]
  node [
    id 46
    label "wokal"
  ]
  node [
    id 47
    label "note"
  ]
  node [
    id 48
    label "wydawa&#263;"
  ]
  node [
    id 49
    label "nakaz"
  ]
  node [
    id 50
    label "regestr"
  ]
  node [
    id 51
    label "&#347;piewak_operowy"
  ]
  node [
    id 52
    label "matowie&#263;"
  ]
  node [
    id 53
    label "wpada&#263;"
  ]
  node [
    id 54
    label "stanowisko"
  ]
  node [
    id 55
    label "zjawisko"
  ]
  node [
    id 56
    label "mutacja"
  ]
  node [
    id 57
    label "partia"
  ]
  node [
    id 58
    label "&#347;piewak"
  ]
  node [
    id 59
    label "emisja"
  ]
  node [
    id 60
    label "brzmienie"
  ]
  node [
    id 61
    label "zmatowie&#263;"
  ]
  node [
    id 62
    label "wydanie"
  ]
  node [
    id 63
    label "zesp&#243;&#322;"
  ]
  node [
    id 64
    label "wyda&#263;"
  ]
  node [
    id 65
    label "zdolno&#347;&#263;"
  ]
  node [
    id 66
    label "decyzja"
  ]
  node [
    id 67
    label "wpadni&#281;cie"
  ]
  node [
    id 68
    label "linia_melodyczna"
  ]
  node [
    id 69
    label "wpadanie"
  ]
  node [
    id 70
    label "onomatopeja"
  ]
  node [
    id 71
    label "sound"
  ]
  node [
    id 72
    label "matowienie"
  ]
  node [
    id 73
    label "ch&#243;rzysta"
  ]
  node [
    id 74
    label "d&#378;wi&#281;k"
  ]
  node [
    id 75
    label "foniatra"
  ]
  node [
    id 76
    label "&#347;piewaczka"
  ]
  node [
    id 77
    label "poprawa"
  ]
  node [
    id 78
    label "warto&#347;&#263;"
  ]
  node [
    id 79
    label "alternation"
  ]
  node [
    id 80
    label "akt"
  ]
  node [
    id 81
    label "modyfikacja"
  ]
  node [
    id 82
    label "egzamin"
  ]
  node [
    id 83
    label "deputation"
  ]
  node [
    id 84
    label "izba_wy&#380;sza"
  ]
  node [
    id 85
    label "organ"
  ]
  node [
    id 86
    label "kolegium"
  ]
  node [
    id 87
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 88
    label "magistrat"
  ]
  node [
    id 89
    label "dostarczy&#263;"
  ]
  node [
    id 90
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 91
    label "strike"
  ]
  node [
    id 92
    label "przybra&#263;"
  ]
  node [
    id 93
    label "swallow"
  ]
  node [
    id 94
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 95
    label "odebra&#263;"
  ]
  node [
    id 96
    label "umie&#347;ci&#263;"
  ]
  node [
    id 97
    label "obra&#263;"
  ]
  node [
    id 98
    label "fall"
  ]
  node [
    id 99
    label "wzi&#261;&#263;"
  ]
  node [
    id 100
    label "undertake"
  ]
  node [
    id 101
    label "absorb"
  ]
  node [
    id 102
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 103
    label "receive"
  ]
  node [
    id 104
    label "draw"
  ]
  node [
    id 105
    label "przyj&#281;cie"
  ]
  node [
    id 106
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 107
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 108
    label "uzna&#263;"
  ]
  node [
    id 109
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 110
    label "krajowy"
  ]
  node [
    id 111
    label "zarz&#261;d"
  ]
  node [
    id 112
    label "gospodarka"
  ]
  node [
    id 113
    label "wodny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 112
  ]
  edge [
    source 110
    target 113
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 113
  ]
  edge [
    source 112
    target 113
  ]
]
