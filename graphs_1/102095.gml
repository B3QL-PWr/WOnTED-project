graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.11764705882352941
  graphCliqueNumber 2
  node [
    id 0
    label "pronar"
    origin "text"
  ]
  node [
    id 1
    label "osir"
    origin "text"
  ]
  node [
    id 2
    label "hajn&#243;wka"
    origin "text"
  ]
  node [
    id 3
    label "bzura"
    origin "text"
  ]
  node [
    id 4
    label "ozorek"
    origin "text"
  ]
  node [
    id 5
    label "set"
    origin "text"
  ]
  node [
    id 6
    label "pieczarkowiec"
  ]
  node [
    id 7
    label "ozorkowate"
  ]
  node [
    id 8
    label "saprotrof"
  ]
  node [
    id 9
    label "grzyb"
  ]
  node [
    id 10
    label "paso&#380;yt"
  ]
  node [
    id 11
    label "podroby"
  ]
  node [
    id 12
    label "zestaw"
  ]
  node [
    id 13
    label "kompozycja"
  ]
  node [
    id 14
    label "gem"
  ]
  node [
    id 15
    label "muzyka"
  ]
  node [
    id 16
    label "runda"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
]
