graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.951219512195122
  density 0.04878048780487805
  graphCliqueNumber 2
  node [
    id 0
    label "podrywajzwykopem"
    origin "text"
  ]
  node [
    id 1
    label "logikaniebieskichpaskow"
    origin "text"
  ]
  node [
    id 2
    label "zwiazki"
    origin "text"
  ]
  node [
    id 3
    label "dopiero"
    origin "text"
  ]
  node [
    id 4
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 5
    label "dotrze&#263;"
    origin "text"
  ]
  node [
    id 6
    label "odjeba&#263;by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "chyba"
    origin "text"
  ]
  node [
    id 8
    label "nie"
    origin "text"
  ]
  node [
    id 9
    label "dobrze"
    origin "text"
  ]
  node [
    id 10
    label "doba"
  ]
  node [
    id 11
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 12
    label "dzi&#347;"
  ]
  node [
    id 13
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 14
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 15
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 16
    label "get"
  ]
  node [
    id 17
    label "utrze&#263;"
  ]
  node [
    id 18
    label "spowodowa&#263;"
  ]
  node [
    id 19
    label "catch"
  ]
  node [
    id 20
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 21
    label "become"
  ]
  node [
    id 22
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 23
    label "znale&#378;&#263;"
  ]
  node [
    id 24
    label "dorobi&#263;"
  ]
  node [
    id 25
    label "advance"
  ]
  node [
    id 26
    label "dopasowa&#263;"
  ]
  node [
    id 27
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 28
    label "silnik"
  ]
  node [
    id 29
    label "sprzeciw"
  ]
  node [
    id 30
    label "moralnie"
  ]
  node [
    id 31
    label "wiele"
  ]
  node [
    id 32
    label "lepiej"
  ]
  node [
    id 33
    label "korzystnie"
  ]
  node [
    id 34
    label "pomy&#347;lnie"
  ]
  node [
    id 35
    label "pozytywnie"
  ]
  node [
    id 36
    label "dobry"
  ]
  node [
    id 37
    label "dobroczynnie"
  ]
  node [
    id 38
    label "odpowiednio"
  ]
  node [
    id 39
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 40
    label "skutecznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
]
