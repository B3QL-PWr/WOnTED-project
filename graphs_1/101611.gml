graph [
  maxDegree 36
  minDegree 1
  meanDegree 2.0930232558139537
  density 0.009780482503803522
  graphCliqueNumber 3
  node [
    id 0
    label "znowu"
    origin "text"
  ]
  node [
    id 1
    label "dwa"
    origin "text"
  ]
  node [
    id 2
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 3
    label "ruda"
    origin "text"
  ]
  node [
    id 4
    label "walczy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "siebie"
    origin "text"
  ]
  node [
    id 6
    label "niegdy&#347;"
    origin "text"
  ]
  node [
    id 7
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 8
    label "bardzo"
    origin "text"
  ]
  node [
    id 9
    label "rozleg&#322;y"
    origin "text"
  ]
  node [
    id 10
    label "powoli"
    origin "text"
  ]
  node [
    id 11
    label "s&#261;siadek"
    origin "text"
  ]
  node [
    id 12
    label "wyprze&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zaj&#261;&#263;"
    origin "text"
  ]
  node [
    id 14
    label "miejsce"
    origin "text"
  ]
  node [
    id 15
    label "spojrze&#263;"
    origin "text"
  ]
  node [
    id 16
    label "zielony"
    origin "text"
  ]
  node [
    id 17
    label "siwy"
    origin "text"
  ]
  node [
    id 18
    label "zapuszcza&#263;"
    origin "text"
  ]
  node [
    id 19
    label "zagon"
    origin "text"
  ]
  node [
    id 20
    label "zielonem"
    origin "text"
  ]
  node [
    id 21
    label "t&#322;o"
    origin "text"
  ]
  node [
    id 22
    label "wida&#263;"
    origin "text"
  ]
  node [
    id 23
    label "szary"
    origin "text"
  ]
  node [
    id 24
    label "pasek"
    origin "text"
  ]
  node [
    id 25
    label "jasny"
  ]
  node [
    id 26
    label "typ_mongoloidalny"
  ]
  node [
    id 27
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 28
    label "kolorowy"
  ]
  node [
    id 29
    label "ciep&#322;y"
  ]
  node [
    id 30
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 31
    label "aglomerownia"
  ]
  node [
    id 32
    label "kopalina_podstawowa"
  ]
  node [
    id 33
    label "wypa&#322;ek"
  ]
  node [
    id 34
    label "pra&#380;alnia"
  ]
  node [
    id 35
    label "ore"
  ]
  node [
    id 36
    label "atakamit"
  ]
  node [
    id 37
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 38
    label "cope"
  ]
  node [
    id 39
    label "contend"
  ]
  node [
    id 40
    label "zawody"
  ]
  node [
    id 41
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 42
    label "dzia&#322;a&#263;"
  ]
  node [
    id 43
    label "wrestle"
  ]
  node [
    id 44
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 45
    label "robi&#263;"
  ]
  node [
    id 46
    label "my&#347;lenie"
  ]
  node [
    id 47
    label "argue"
  ]
  node [
    id 48
    label "stara&#263;_si&#281;"
  ]
  node [
    id 49
    label "fight"
  ]
  node [
    id 50
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 51
    label "kiedy&#347;"
  ]
  node [
    id 52
    label "drzewiej"
  ]
  node [
    id 53
    label "niegdysiejszy"
  ]
  node [
    id 54
    label "partnerka"
  ]
  node [
    id 55
    label "w_chuj"
  ]
  node [
    id 56
    label "rozlegle"
  ]
  node [
    id 57
    label "szeroki"
  ]
  node [
    id 58
    label "du&#380;y"
  ]
  node [
    id 59
    label "wolny"
  ]
  node [
    id 60
    label "stopniowo"
  ]
  node [
    id 61
    label "wolniej"
  ]
  node [
    id 62
    label "niespiesznie"
  ]
  node [
    id 63
    label "bezproblemowo"
  ]
  node [
    id 64
    label "spokojny"
  ]
  node [
    id 65
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 66
    label "wygin&#261;&#263;"
  ]
  node [
    id 67
    label "usun&#261;&#263;"
  ]
  node [
    id 68
    label "ro&#347;liny"
  ]
  node [
    id 69
    label "zast&#261;pi&#263;"
  ]
  node [
    id 70
    label "rozogni&#263;_si&#281;"
  ]
  node [
    id 71
    label "sk&#243;ra"
  ]
  node [
    id 72
    label "dostarczy&#263;"
  ]
  node [
    id 73
    label "wype&#322;ni&#263;"
  ]
  node [
    id 74
    label "anektowa&#263;"
  ]
  node [
    id 75
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 76
    label "obj&#261;&#263;"
  ]
  node [
    id 77
    label "zada&#263;"
  ]
  node [
    id 78
    label "sorb"
  ]
  node [
    id 79
    label "interest"
  ]
  node [
    id 80
    label "skorzysta&#263;"
  ]
  node [
    id 81
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 82
    label "wzi&#261;&#263;"
  ]
  node [
    id 83
    label "employment"
  ]
  node [
    id 84
    label "zapanowa&#263;"
  ]
  node [
    id 85
    label "do"
  ]
  node [
    id 86
    label "klasyfikacja"
  ]
  node [
    id 87
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 88
    label "bankrupt"
  ]
  node [
    id 89
    label "zabra&#263;"
  ]
  node [
    id 90
    label "spowodowa&#263;"
  ]
  node [
    id 91
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 92
    label "komornik"
  ]
  node [
    id 93
    label "prosecute"
  ]
  node [
    id 94
    label "seize"
  ]
  node [
    id 95
    label "topographic_point"
  ]
  node [
    id 96
    label "wzbudzi&#263;"
  ]
  node [
    id 97
    label "rozciekawi&#263;"
  ]
  node [
    id 98
    label "cia&#322;o"
  ]
  node [
    id 99
    label "plac"
  ]
  node [
    id 100
    label "cecha"
  ]
  node [
    id 101
    label "uwaga"
  ]
  node [
    id 102
    label "przestrze&#324;"
  ]
  node [
    id 103
    label "status"
  ]
  node [
    id 104
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 105
    label "chwila"
  ]
  node [
    id 106
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 107
    label "rz&#261;d"
  ]
  node [
    id 108
    label "praca"
  ]
  node [
    id 109
    label "location"
  ]
  node [
    id 110
    label "warunek_lokalowy"
  ]
  node [
    id 111
    label "pojrze&#263;"
  ]
  node [
    id 112
    label "popatrze&#263;"
  ]
  node [
    id 113
    label "zinterpretowa&#263;"
  ]
  node [
    id 114
    label "spoziera&#263;"
  ]
  node [
    id 115
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 116
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 117
    label "see"
  ]
  node [
    id 118
    label "peek"
  ]
  node [
    id 119
    label "Palau"
  ]
  node [
    id 120
    label "zwolennik"
  ]
  node [
    id 121
    label "zieloni"
  ]
  node [
    id 122
    label "zielenienie"
  ]
  node [
    id 123
    label "Zimbabwe"
  ]
  node [
    id 124
    label "zazielenianie"
  ]
  node [
    id 125
    label "Wyspy_Marshalla"
  ]
  node [
    id 126
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 127
    label "niedojrza&#322;y"
  ]
  node [
    id 128
    label "zazielenienie"
  ]
  node [
    id 129
    label "ch&#322;odny"
  ]
  node [
    id 130
    label "pokryty"
  ]
  node [
    id 131
    label "blady"
  ]
  node [
    id 132
    label "Mariany_P&#243;&#322;nocne"
  ]
  node [
    id 133
    label "naturalny"
  ]
  node [
    id 134
    label "&#380;ywy"
  ]
  node [
    id 135
    label "Timor_Wschodni"
  ]
  node [
    id 136
    label "Portoryko"
  ]
  node [
    id 137
    label "zielono"
  ]
  node [
    id 138
    label "Sint_Eustatius"
  ]
  node [
    id 139
    label "Salwador"
  ]
  node [
    id 140
    label "Saba"
  ]
  node [
    id 141
    label "Bonaire"
  ]
  node [
    id 142
    label "dzia&#322;acz"
  ]
  node [
    id 143
    label "&#347;wie&#380;y"
  ]
  node [
    id 144
    label "Mikronezja"
  ]
  node [
    id 145
    label "USA"
  ]
  node [
    id 146
    label "Panama"
  ]
  node [
    id 147
    label "zzielenienie"
  ]
  node [
    id 148
    label "dolar"
  ]
  node [
    id 149
    label "socjalista"
  ]
  node [
    id 150
    label "polityk"
  ]
  node [
    id 151
    label "majny"
  ]
  node [
    id 152
    label "Ekwador"
  ]
  node [
    id 153
    label "pobielenie"
  ]
  node [
    id 154
    label "nieprzejrzysty"
  ]
  node [
    id 155
    label "siwienie"
  ]
  node [
    id 156
    label "jasnoszary"
  ]
  node [
    id 157
    label "go&#322;&#261;b"
  ]
  node [
    id 158
    label "ko&#324;"
  ]
  node [
    id 159
    label "szymel"
  ]
  node [
    id 160
    label "siwo"
  ]
  node [
    id 161
    label "posiwienie"
  ]
  node [
    id 162
    label "opuszcza&#263;"
  ]
  node [
    id 163
    label "wyd&#322;u&#380;a&#263;"
  ]
  node [
    id 164
    label "za&#347;wieca&#263;"
  ]
  node [
    id 165
    label "zaniedbywa&#263;"
  ]
  node [
    id 166
    label "umieszcza&#263;"
  ]
  node [
    id 167
    label "hodowa&#263;"
  ]
  node [
    id 168
    label "reject"
  ]
  node [
    id 169
    label "go"
  ]
  node [
    id 170
    label "pole"
  ]
  node [
    id 171
    label "melodia"
  ]
  node [
    id 172
    label "partia"
  ]
  node [
    id 173
    label "&#347;rodowisko"
  ]
  node [
    id 174
    label "obiekt"
  ]
  node [
    id 175
    label "ubarwienie"
  ]
  node [
    id 176
    label "gestaltyzm"
  ]
  node [
    id 177
    label "lingwistyka_kognitywna"
  ]
  node [
    id 178
    label "obraz"
  ]
  node [
    id 179
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 180
    label "pod&#322;o&#380;e"
  ]
  node [
    id 181
    label "informacja"
  ]
  node [
    id 182
    label "causal_agent"
  ]
  node [
    id 183
    label "dalszoplanowy"
  ]
  node [
    id 184
    label "layer"
  ]
  node [
    id 185
    label "plan"
  ]
  node [
    id 186
    label "warunki"
  ]
  node [
    id 187
    label "background"
  ]
  node [
    id 188
    label "p&#322;aszczyzna"
  ]
  node [
    id 189
    label "p&#322;owy"
  ]
  node [
    id 190
    label "pochmurno"
  ]
  node [
    id 191
    label "szaro"
  ]
  node [
    id 192
    label "chmurnienie"
  ]
  node [
    id 193
    label "zwyczajny"
  ]
  node [
    id 194
    label "zwyk&#322;y"
  ]
  node [
    id 195
    label "szarzenie"
  ]
  node [
    id 196
    label "niezabawny"
  ]
  node [
    id 197
    label "brzydki"
  ]
  node [
    id 198
    label "nieciekawy"
  ]
  node [
    id 199
    label "oboj&#281;tny"
  ]
  node [
    id 200
    label "poszarzenie"
  ]
  node [
    id 201
    label "bezbarwnie"
  ]
  node [
    id 202
    label "srebrny"
  ]
  node [
    id 203
    label "spochmurnienie"
  ]
  node [
    id 204
    label "zwi&#261;zek"
  ]
  node [
    id 205
    label "dyktando"
  ]
  node [
    id 206
    label "dodatek"
  ]
  node [
    id 207
    label "oznaka"
  ]
  node [
    id 208
    label "prevention"
  ]
  node [
    id 209
    label "spekulacja"
  ]
  node [
    id 210
    label "handel"
  ]
  node [
    id 211
    label "zone"
  ]
  node [
    id 212
    label "naszywka"
  ]
  node [
    id 213
    label "us&#322;uga"
  ]
  node [
    id 214
    label "przewi&#261;zka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 174
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 137
  ]
  edge [
    source 23
    target 131
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 129
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
]
