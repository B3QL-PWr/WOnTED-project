graph [
  maxDegree 66
  minDegree 1
  meanDegree 2.0343347639484977
  density 0.008768684327364214
  graphCliqueNumber 3
  node [
    id 0
    label "kilka"
    origin "text"
  ]
  node [
    id 1
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "chiny"
    origin "text"
  ]
  node [
    id 4
    label "wystrzeli&#263;"
    origin "text"
  ]
  node [
    id 5
    label "misja"
    origin "text"
  ]
  node [
    id 6
    label "chang'e"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "wyl&#261;dowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "tam"
    origin "text"
  ]
  node [
    id 10
    label "gdzie"
    origin "text"
  ]
  node [
    id 11
    label "l&#261;dowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "jeszcze"
    origin "text"
  ]
  node [
    id 13
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 14
    label "statek"
    origin "text"
  ]
  node [
    id 15
    label "wys&#322;a&#263;"
    origin "text"
  ]
  node [
    id 16
    label "przez"
    origin "text"
  ]
  node [
    id 17
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 18
    label "niewidoczny"
    origin "text"
  ]
  node [
    id 19
    label "strona"
    origin "text"
  ]
  node [
    id 20
    label "ksi&#281;&#380;yc"
    origin "text"
  ]
  node [
    id 21
    label "&#347;ledziowate"
  ]
  node [
    id 22
    label "ryba"
  ]
  node [
    id 23
    label "s&#322;o&#324;ce"
  ]
  node [
    id 24
    label "czynienie_si&#281;"
  ]
  node [
    id 25
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 26
    label "czas"
  ]
  node [
    id 27
    label "long_time"
  ]
  node [
    id 28
    label "przedpo&#322;udnie"
  ]
  node [
    id 29
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 30
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 31
    label "tydzie&#324;"
  ]
  node [
    id 32
    label "godzina"
  ]
  node [
    id 33
    label "t&#322;usty_czwartek"
  ]
  node [
    id 34
    label "wsta&#263;"
  ]
  node [
    id 35
    label "day"
  ]
  node [
    id 36
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 37
    label "przedwiecz&#243;r"
  ]
  node [
    id 38
    label "Sylwester"
  ]
  node [
    id 39
    label "po&#322;udnie"
  ]
  node [
    id 40
    label "wzej&#347;cie"
  ]
  node [
    id 41
    label "podwiecz&#243;r"
  ]
  node [
    id 42
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 43
    label "rano"
  ]
  node [
    id 44
    label "termin"
  ]
  node [
    id 45
    label "ranek"
  ]
  node [
    id 46
    label "doba"
  ]
  node [
    id 47
    label "wiecz&#243;r"
  ]
  node [
    id 48
    label "walentynki"
  ]
  node [
    id 49
    label "popo&#322;udnie"
  ]
  node [
    id 50
    label "noc"
  ]
  node [
    id 51
    label "wstanie"
  ]
  node [
    id 52
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 53
    label "miesi&#261;c"
  ]
  node [
    id 54
    label "Barb&#243;rka"
  ]
  node [
    id 55
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 56
    label "fly"
  ]
  node [
    id 57
    label "wypu&#347;ci&#263;"
  ]
  node [
    id 58
    label "blast"
  ]
  node [
    id 59
    label "explode"
  ]
  node [
    id 60
    label "plun&#261;&#263;"
  ]
  node [
    id 61
    label "odpali&#263;"
  ]
  node [
    id 62
    label "zrobi&#263;"
  ]
  node [
    id 63
    label "plac&#243;wka"
  ]
  node [
    id 64
    label "misje"
  ]
  node [
    id 65
    label "zadanie"
  ]
  node [
    id 66
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 67
    label "obowi&#261;zek"
  ]
  node [
    id 68
    label "reprezentacja"
  ]
  node [
    id 69
    label "dotrze&#263;"
  ]
  node [
    id 70
    label "land"
  ]
  node [
    id 71
    label "stan&#261;&#263;"
  ]
  node [
    id 72
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 73
    label "tu"
  ]
  node [
    id 74
    label "przybywa&#263;"
  ]
  node [
    id 75
    label "trafia&#263;"
  ]
  node [
    id 76
    label "finish_up"
  ]
  node [
    id 77
    label "radzi&#263;_sobie"
  ]
  node [
    id 78
    label "ci&#261;gle"
  ]
  node [
    id 79
    label "nijaki"
  ]
  node [
    id 80
    label "korab"
  ]
  node [
    id 81
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 82
    label "zr&#281;bnica"
  ]
  node [
    id 83
    label "odkotwiczenie"
  ]
  node [
    id 84
    label "cumowanie"
  ]
  node [
    id 85
    label "zadokowanie"
  ]
  node [
    id 86
    label "bumsztak"
  ]
  node [
    id 87
    label "zacumowanie"
  ]
  node [
    id 88
    label "dobi&#263;"
  ]
  node [
    id 89
    label "odkotwiczanie"
  ]
  node [
    id 90
    label "kotwica"
  ]
  node [
    id 91
    label "zwodowa&#263;"
  ]
  node [
    id 92
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 93
    label "zakotwiczenie"
  ]
  node [
    id 94
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 95
    label "dzi&#243;b"
  ]
  node [
    id 96
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 97
    label "armada"
  ]
  node [
    id 98
    label "grobla"
  ]
  node [
    id 99
    label "kad&#322;ub"
  ]
  node [
    id 100
    label "dobijanie"
  ]
  node [
    id 101
    label "odkotwicza&#263;"
  ]
  node [
    id 102
    label "proporczyk"
  ]
  node [
    id 103
    label "luk"
  ]
  node [
    id 104
    label "odcumowanie"
  ]
  node [
    id 105
    label "kabina"
  ]
  node [
    id 106
    label "skrajnik"
  ]
  node [
    id 107
    label "kotwiczenie"
  ]
  node [
    id 108
    label "zwodowanie"
  ]
  node [
    id 109
    label "szkutnictwo"
  ]
  node [
    id 110
    label "pojazd"
  ]
  node [
    id 111
    label "wodowanie"
  ]
  node [
    id 112
    label "zacumowa&#263;"
  ]
  node [
    id 113
    label "sterownik_automatyczny"
  ]
  node [
    id 114
    label "p&#322;ywa&#263;"
  ]
  node [
    id 115
    label "zadokowa&#263;"
  ]
  node [
    id 116
    label "zakotwiczy&#263;"
  ]
  node [
    id 117
    label "sztormtrap"
  ]
  node [
    id 118
    label "pok&#322;ad"
  ]
  node [
    id 119
    label "kotwiczy&#263;"
  ]
  node [
    id 120
    label "&#380;yroskop"
  ]
  node [
    id 121
    label "odcumowa&#263;"
  ]
  node [
    id 122
    label "dobicie"
  ]
  node [
    id 123
    label "armator"
  ]
  node [
    id 124
    label "odbijacz"
  ]
  node [
    id 125
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 126
    label "reling"
  ]
  node [
    id 127
    label "flota"
  ]
  node [
    id 128
    label "kabestan"
  ]
  node [
    id 129
    label "nadbud&#243;wka"
  ]
  node [
    id 130
    label "dokowa&#263;"
  ]
  node [
    id 131
    label "cumowa&#263;"
  ]
  node [
    id 132
    label "odkotwiczy&#263;"
  ]
  node [
    id 133
    label "dobija&#263;"
  ]
  node [
    id 134
    label "odcumowywanie"
  ]
  node [
    id 135
    label "ster"
  ]
  node [
    id 136
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 137
    label "odcumowywa&#263;"
  ]
  node [
    id 138
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 139
    label "futr&#243;wka"
  ]
  node [
    id 140
    label "dokowanie"
  ]
  node [
    id 141
    label "trap"
  ]
  node [
    id 142
    label "zaw&#243;r_denny"
  ]
  node [
    id 143
    label "rostra"
  ]
  node [
    id 144
    label "wytworzy&#263;"
  ]
  node [
    id 145
    label "line"
  ]
  node [
    id 146
    label "ship"
  ]
  node [
    id 147
    label "convey"
  ]
  node [
    id 148
    label "przekaza&#263;"
  ]
  node [
    id 149
    label "post"
  ]
  node [
    id 150
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 151
    label "nakaza&#263;"
  ]
  node [
    id 152
    label "asymilowa&#263;"
  ]
  node [
    id 153
    label "wapniak"
  ]
  node [
    id 154
    label "dwun&#243;g"
  ]
  node [
    id 155
    label "polifag"
  ]
  node [
    id 156
    label "wz&#243;r"
  ]
  node [
    id 157
    label "profanum"
  ]
  node [
    id 158
    label "hominid"
  ]
  node [
    id 159
    label "homo_sapiens"
  ]
  node [
    id 160
    label "nasada"
  ]
  node [
    id 161
    label "podw&#322;adny"
  ]
  node [
    id 162
    label "ludzko&#347;&#263;"
  ]
  node [
    id 163
    label "os&#322;abianie"
  ]
  node [
    id 164
    label "mikrokosmos"
  ]
  node [
    id 165
    label "portrecista"
  ]
  node [
    id 166
    label "duch"
  ]
  node [
    id 167
    label "g&#322;owa"
  ]
  node [
    id 168
    label "oddzia&#322;ywanie"
  ]
  node [
    id 169
    label "asymilowanie"
  ]
  node [
    id 170
    label "osoba"
  ]
  node [
    id 171
    label "os&#322;abia&#263;"
  ]
  node [
    id 172
    label "figura"
  ]
  node [
    id 173
    label "Adam"
  ]
  node [
    id 174
    label "senior"
  ]
  node [
    id 175
    label "antropochoria"
  ]
  node [
    id 176
    label "posta&#263;"
  ]
  node [
    id 177
    label "ukrycie"
  ]
  node [
    id 178
    label "niewidzialnie"
  ]
  node [
    id 179
    label "zamazywanie"
  ]
  node [
    id 180
    label "zas&#322;oni&#281;cie"
  ]
  node [
    id 181
    label "znikni&#281;cie"
  ]
  node [
    id 182
    label "zamazanie"
  ]
  node [
    id 183
    label "niewidny"
  ]
  node [
    id 184
    label "niewidomy"
  ]
  node [
    id 185
    label "znikanie"
  ]
  node [
    id 186
    label "ukrywanie"
  ]
  node [
    id 187
    label "niezauwa&#380;alny"
  ]
  node [
    id 188
    label "niedostrzegalny"
  ]
  node [
    id 189
    label "skr&#281;canie"
  ]
  node [
    id 190
    label "voice"
  ]
  node [
    id 191
    label "forma"
  ]
  node [
    id 192
    label "internet"
  ]
  node [
    id 193
    label "skr&#281;ci&#263;"
  ]
  node [
    id 194
    label "kartka"
  ]
  node [
    id 195
    label "orientowa&#263;"
  ]
  node [
    id 196
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 197
    label "powierzchnia"
  ]
  node [
    id 198
    label "plik"
  ]
  node [
    id 199
    label "bok"
  ]
  node [
    id 200
    label "pagina"
  ]
  node [
    id 201
    label "orientowanie"
  ]
  node [
    id 202
    label "fragment"
  ]
  node [
    id 203
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 204
    label "s&#261;d"
  ]
  node [
    id 205
    label "skr&#281;ca&#263;"
  ]
  node [
    id 206
    label "g&#243;ra"
  ]
  node [
    id 207
    label "serwis_internetowy"
  ]
  node [
    id 208
    label "orientacja"
  ]
  node [
    id 209
    label "linia"
  ]
  node [
    id 210
    label "skr&#281;cenie"
  ]
  node [
    id 211
    label "layout"
  ]
  node [
    id 212
    label "zorientowa&#263;"
  ]
  node [
    id 213
    label "zorientowanie"
  ]
  node [
    id 214
    label "obiekt"
  ]
  node [
    id 215
    label "podmiot"
  ]
  node [
    id 216
    label "ty&#322;"
  ]
  node [
    id 217
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 218
    label "logowanie"
  ]
  node [
    id 219
    label "adres_internetowy"
  ]
  node [
    id 220
    label "uj&#281;cie"
  ]
  node [
    id 221
    label "prz&#243;d"
  ]
  node [
    id 222
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 223
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 224
    label "moon"
  ]
  node [
    id 225
    label "&#347;wiat&#322;o"
  ]
  node [
    id 226
    label "aposelenium"
  ]
  node [
    id 227
    label "peryselenium"
  ]
  node [
    id 228
    label "Tytan"
  ]
  node [
    id 229
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 230
    label "satelita"
  ]
  node [
    id 231
    label "Change"
  ]
  node [
    id 232
    label "4"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 228
  ]
  edge [
    source 20
    target 229
  ]
  edge [
    source 20
    target 53
  ]
  edge [
    source 20
    target 230
  ]
  edge [
    source 231
    target 232
  ]
]
