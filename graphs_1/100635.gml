graph [
  maxDegree 30
  minDegree 1
  meanDegree 2.036363636363636
  density 0.012416851441241685
  graphCliqueNumber 3
  node [
    id 0
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "swi&#281;to"
    origin "text"
  ]
  node [
    id 2
    label "plon"
    origin "text"
  ]
  node [
    id 3
    label "dziesiejsze"
    origin "text"
  ]
  node [
    id 4
    label "do&#380;ynek"
    origin "text"
  ]
  node [
    id 5
    label "dzienia"
    origin "text"
  ]
  node [
    id 6
    label "ten"
    origin "text"
  ]
  node [
    id 7
    label "radownao"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "otrzyma&#263;"
    origin "text"
  ]
  node [
    id 10
    label "ale"
    origin "text"
  ]
  node [
    id 11
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 12
    label "zanosi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "b&#322;aganie"
    origin "text"
  ]
  node [
    id 14
    label "b&#243;stwo"
    origin "text"
  ]
  node [
    id 15
    label "r&#243;wnie"
    origin "text"
  ]
  node [
    id 16
    label "obfity"
    origin "text"
  ]
  node [
    id 17
    label "zbi&#243;r"
    origin "text"
  ]
  node [
    id 18
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 19
    label "rok"
    origin "text"
  ]
  node [
    id 20
    label "zako&#324;czenie"
    origin "text"
  ]
  node [
    id 21
    label "&#380;niwa"
    origin "text"
  ]
  node [
    id 22
    label "ostatek"
    origin "text"
  ]
  node [
    id 23
    label "skosi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "zbo&#380;e"
    origin "text"
  ]
  node [
    id 25
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 26
    label "kult"
    origin "text"
  ]
  node [
    id 27
    label "miesi&#261;c"
  ]
  node [
    id 28
    label "return"
  ]
  node [
    id 29
    label "naturalia"
  ]
  node [
    id 30
    label "wyda&#263;"
  ]
  node [
    id 31
    label "wydawa&#263;"
  ]
  node [
    id 32
    label "metr"
  ]
  node [
    id 33
    label "produkcja"
  ]
  node [
    id 34
    label "rezultat"
  ]
  node [
    id 35
    label "okre&#347;lony"
  ]
  node [
    id 36
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 37
    label "wytworzy&#263;"
  ]
  node [
    id 38
    label "give_birth"
  ]
  node [
    id 39
    label "dosta&#263;"
  ]
  node [
    id 40
    label "piwo"
  ]
  node [
    id 41
    label "get"
  ]
  node [
    id 42
    label "przenosi&#263;"
  ]
  node [
    id 43
    label "dostarcza&#263;"
  ]
  node [
    id 44
    label "usi&#322;owa&#263;"
  ]
  node [
    id 45
    label "kry&#263;"
  ]
  node [
    id 46
    label "proszenie"
  ]
  node [
    id 47
    label "naleganie"
  ]
  node [
    id 48
    label "figura_my&#347;li"
  ]
  node [
    id 49
    label "&#380;ebranie"
  ]
  node [
    id 50
    label "solicitation"
  ]
  node [
    id 51
    label "Dionizos"
  ]
  node [
    id 52
    label "Neptun"
  ]
  node [
    id 53
    label "Hesperos"
  ]
  node [
    id 54
    label "apolinaryzm"
  ]
  node [
    id 55
    label "ba&#322;wan"
  ]
  node [
    id 56
    label "niebiosa"
  ]
  node [
    id 57
    label "Ereb"
  ]
  node [
    id 58
    label "Sylen"
  ]
  node [
    id 59
    label "uwielbienie"
  ]
  node [
    id 60
    label "s&#261;d_ostateczny"
  ]
  node [
    id 61
    label "idol"
  ]
  node [
    id 62
    label "Bachus"
  ]
  node [
    id 63
    label "ofiarowa&#263;"
  ]
  node [
    id 64
    label "tr&#243;jca"
  ]
  node [
    id 65
    label "Posejdon"
  ]
  node [
    id 66
    label "Waruna"
  ]
  node [
    id 67
    label "ofiarowanie"
  ]
  node [
    id 68
    label "igrzyska_greckie"
  ]
  node [
    id 69
    label "Janus"
  ]
  node [
    id 70
    label "Kupidyn"
  ]
  node [
    id 71
    label "ofiarowywanie"
  ]
  node [
    id 72
    label "osoba"
  ]
  node [
    id 73
    label "Boreasz"
  ]
  node [
    id 74
    label "politeizm"
  ]
  node [
    id 75
    label "istota_nadprzyrodzona"
  ]
  node [
    id 76
    label "ofiarowywa&#263;"
  ]
  node [
    id 77
    label "&#347;wi&#281;to&#347;&#263;"
  ]
  node [
    id 78
    label "r&#243;wny"
  ]
  node [
    id 79
    label "dok&#322;adnie"
  ]
  node [
    id 80
    label "obficie"
  ]
  node [
    id 81
    label "spory"
  ]
  node [
    id 82
    label "intensywny"
  ]
  node [
    id 83
    label "praca_rolnicza"
  ]
  node [
    id 84
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 85
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 86
    label "sum"
  ]
  node [
    id 87
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 88
    label "album"
  ]
  node [
    id 89
    label "uprawianie"
  ]
  node [
    id 90
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 91
    label "dane"
  ]
  node [
    id 92
    label "collection"
  ]
  node [
    id 93
    label "gathering"
  ]
  node [
    id 94
    label "series"
  ]
  node [
    id 95
    label "poj&#281;cie"
  ]
  node [
    id 96
    label "egzemplarz"
  ]
  node [
    id 97
    label "pakiet_klimatyczny"
  ]
  node [
    id 98
    label "kolejny"
  ]
  node [
    id 99
    label "stulecie"
  ]
  node [
    id 100
    label "kalendarz"
  ]
  node [
    id 101
    label "czas"
  ]
  node [
    id 102
    label "pora_roku"
  ]
  node [
    id 103
    label "cykl_astronomiczny"
  ]
  node [
    id 104
    label "p&#243;&#322;rocze"
  ]
  node [
    id 105
    label "grupa"
  ]
  node [
    id 106
    label "kwarta&#322;"
  ]
  node [
    id 107
    label "kurs"
  ]
  node [
    id 108
    label "jubileusz"
  ]
  node [
    id 109
    label "lata"
  ]
  node [
    id 110
    label "martwy_sezon"
  ]
  node [
    id 111
    label "dzia&#322;anie"
  ]
  node [
    id 112
    label "zrobienie"
  ]
  node [
    id 113
    label "koniec"
  ]
  node [
    id 114
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 115
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 116
    label "zrezygnowanie"
  ]
  node [
    id 117
    label "closing"
  ]
  node [
    id 118
    label "adjustment"
  ]
  node [
    id 119
    label "ukszta&#322;towanie"
  ]
  node [
    id 120
    label "conclusion"
  ]
  node [
    id 121
    label "termination"
  ]
  node [
    id 122
    label "closure"
  ]
  node [
    id 123
    label "sezon"
  ]
  node [
    id 124
    label "resztka"
  ]
  node [
    id 125
    label "scythe"
  ]
  node [
    id 126
    label "obla&#263;"
  ]
  node [
    id 127
    label "pozyska&#263;"
  ]
  node [
    id 128
    label "za&#380;&#261;&#263;"
  ]
  node [
    id 129
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 130
    label "pout"
  ]
  node [
    id 131
    label "zabi&#263;"
  ]
  node [
    id 132
    label "gluten"
  ]
  node [
    id 133
    label "produkt"
  ]
  node [
    id 134
    label "ziarno"
  ]
  node [
    id 135
    label "k&#322;os"
  ]
  node [
    id 136
    label "ro&#347;lina"
  ]
  node [
    id 137
    label "spichlerz"
  ]
  node [
    id 138
    label "mlewnik"
  ]
  node [
    id 139
    label "s&#261;siek"
  ]
  node [
    id 140
    label "miejsce"
  ]
  node [
    id 141
    label "Hollywood"
  ]
  node [
    id 142
    label "zal&#261;&#380;ek"
  ]
  node [
    id 143
    label "otoczenie"
  ]
  node [
    id 144
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 145
    label "&#347;rodek"
  ]
  node [
    id 146
    label "center"
  ]
  node [
    id 147
    label "instytucja"
  ]
  node [
    id 148
    label "skupisko"
  ]
  node [
    id 149
    label "warunki"
  ]
  node [
    id 150
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 151
    label "religia"
  ]
  node [
    id 152
    label "egzegeta"
  ]
  node [
    id 153
    label "translacja"
  ]
  node [
    id 154
    label "worship"
  ]
  node [
    id 155
    label "obrz&#281;d"
  ]
  node [
    id 156
    label "postawa"
  ]
  node [
    id 157
    label "Swi&#281;to"
  ]
  node [
    id 158
    label "wielki"
  ]
  node [
    id 159
    label "czwartka"
  ]
  node [
    id 160
    label "S&#322;owianin"
  ]
  node [
    id 161
    label "zachodni"
  ]
  node [
    id 162
    label "&#347;wi&#281;to"
  ]
  node [
    id 163
    label "zmar&#322;y"
  ]
  node [
    id 164
    label "katolicki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 12
    target 25
  ]
  edge [
    source 12
    target 41
  ]
  edge [
    source 12
    target 42
  ]
  edge [
    source 12
    target 43
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 45
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 46
  ]
  edge [
    source 13
    target 47
  ]
  edge [
    source 13
    target 48
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 51
  ]
  edge [
    source 14
    target 52
  ]
  edge [
    source 14
    target 53
  ]
  edge [
    source 14
    target 54
  ]
  edge [
    source 14
    target 55
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 57
  ]
  edge [
    source 14
    target 58
  ]
  edge [
    source 14
    target 59
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 61
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 65
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 14
    target 67
  ]
  edge [
    source 14
    target 68
  ]
  edge [
    source 14
    target 69
  ]
  edge [
    source 14
    target 70
  ]
  edge [
    source 14
    target 71
  ]
  edge [
    source 14
    target 72
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 75
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 17
    target 85
  ]
  edge [
    source 17
    target 86
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 17
    target 91
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 17
    target 93
  ]
  edge [
    source 17
    target 94
  ]
  edge [
    source 17
    target 95
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 97
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 98
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 99
  ]
  edge [
    source 19
    target 100
  ]
  edge [
    source 19
    target 101
  ]
  edge [
    source 19
    target 102
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 106
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 108
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 111
  ]
  edge [
    source 20
    target 112
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 123
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 124
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 125
  ]
  edge [
    source 23
    target 126
  ]
  edge [
    source 23
    target 127
  ]
  edge [
    source 23
    target 128
  ]
  edge [
    source 23
    target 129
  ]
  edge [
    source 23
    target 130
  ]
  edge [
    source 23
    target 131
  ]
  edge [
    source 24
    target 132
  ]
  edge [
    source 24
    target 133
  ]
  edge [
    source 24
    target 134
  ]
  edge [
    source 24
    target 135
  ]
  edge [
    source 24
    target 136
  ]
  edge [
    source 24
    target 137
  ]
  edge [
    source 24
    target 138
  ]
  edge [
    source 24
    target 139
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 140
  ]
  edge [
    source 25
    target 141
  ]
  edge [
    source 25
    target 142
  ]
  edge [
    source 25
    target 143
  ]
  edge [
    source 25
    target 144
  ]
  edge [
    source 25
    target 145
  ]
  edge [
    source 25
    target 146
  ]
  edge [
    source 25
    target 147
  ]
  edge [
    source 25
    target 148
  ]
  edge [
    source 25
    target 149
  ]
  edge [
    source 26
    target 150
  ]
  edge [
    source 26
    target 151
  ]
  edge [
    source 26
    target 152
  ]
  edge [
    source 26
    target 59
  ]
  edge [
    source 26
    target 153
  ]
  edge [
    source 26
    target 154
  ]
  edge [
    source 26
    target 155
  ]
  edge [
    source 26
    target 156
  ]
  edge [
    source 150
    target 164
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 162
    target 163
  ]
]
