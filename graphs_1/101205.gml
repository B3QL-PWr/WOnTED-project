graph [
  maxDegree 30
  minDegree 1
  meanDegree 2.6666666666666665
  density 0.06060606060606061
  graphCliqueNumber 5
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "fundacja"
    origin "text"
  ]
  node [
    id 2
    label "upowszechnia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nauka"
    origin "text"
  ]
  node [
    id 4
    label "foundation"
  ]
  node [
    id 5
    label "instytucja"
  ]
  node [
    id 6
    label "dar"
  ]
  node [
    id 7
    label "darowizna"
  ]
  node [
    id 8
    label "pocz&#261;tek"
  ]
  node [
    id 9
    label "generalize"
  ]
  node [
    id 10
    label "sprawia&#263;"
  ]
  node [
    id 11
    label "nauki_o_Ziemi"
  ]
  node [
    id 12
    label "teoria_naukowa"
  ]
  node [
    id 13
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 14
    label "nauki_o_poznaniu"
  ]
  node [
    id 15
    label "nomotetyczny"
  ]
  node [
    id 16
    label "metodologia"
  ]
  node [
    id 17
    label "przem&#243;wienie"
  ]
  node [
    id 18
    label "wiedza"
  ]
  node [
    id 19
    label "kultura_duchowa"
  ]
  node [
    id 20
    label "nauki_penalne"
  ]
  node [
    id 21
    label "systematyka"
  ]
  node [
    id 22
    label "inwentyka"
  ]
  node [
    id 23
    label "dziedzina"
  ]
  node [
    id 24
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 25
    label "miasteczko_rowerowe"
  ]
  node [
    id 26
    label "fotowoltaika"
  ]
  node [
    id 27
    label "porada"
  ]
  node [
    id 28
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 29
    label "proces"
  ]
  node [
    id 30
    label "imagineskopia"
  ]
  node [
    id 31
    label "typologia"
  ]
  node [
    id 32
    label "&#322;awa_szkolna"
  ]
  node [
    id 33
    label "polski"
  ]
  node [
    id 34
    label "akademia"
  ]
  node [
    id 35
    label "towarzystwo"
  ]
  node [
    id 36
    label "popiera&#263;"
  ]
  node [
    id 37
    label "i"
  ]
  node [
    id 38
    label "krzewi&#263;"
  ]
  node [
    id 39
    label "wolny"
  ]
  node [
    id 40
    label "wszechnica"
  ]
  node [
    id 41
    label "polskie"
  ]
  node [
    id 42
    label "stowarzyszy&#263;"
  ]
  node [
    id 43
    label "film"
  ]
  node [
    id 44
    label "naukowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 39
  ]
  edge [
    source 35
    target 40
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
]
