graph [
  maxDegree 23
  minDegree 1
  meanDegree 2.0294117647058822
  density 0.00999710228919154
  graphCliqueNumber 2
  node [
    id 0
    label "lekarz"
    origin "text"
  ]
  node [
    id 1
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 2
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 3
    label "przekonany"
    origin "text"
  ]
  node [
    id 4
    label "brak"
    origin "text"
  ]
  node [
    id 5
    label "migda&#322;ek"
    origin "text"
  ]
  node [
    id 6
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "szczeg&#243;lny"
    origin "text"
  ]
  node [
    id 8
    label "r&#243;&#380;nica"
    origin "text"
  ]
  node [
    id 9
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 10
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 11
    label "decydowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "uwolni&#263;"
    origin "text"
  ]
  node [
    id 14
    label "dziecko"
    origin "text"
  ]
  node [
    id 15
    label "utrzymywa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "b&#243;l"
    origin "text"
  ]
  node [
    id 17
    label "gard&#322;o"
    origin "text"
  ]
  node [
    id 18
    label "infekcja"
    origin "text"
  ]
  node [
    id 19
    label "przez"
    origin "text"
  ]
  node [
    id 20
    label "usuni&#281;cie"
    origin "text"
  ]
  node [
    id 21
    label "&#378;r&#243;d&#322;o"
    origin "text"
  ]
  node [
    id 22
    label "zbada&#263;"
  ]
  node [
    id 23
    label "medyk"
  ]
  node [
    id 24
    label "lekarze"
  ]
  node [
    id 25
    label "pracownik"
  ]
  node [
    id 26
    label "eskulap"
  ]
  node [
    id 27
    label "Hipokrates"
  ]
  node [
    id 28
    label "Mesmer"
  ]
  node [
    id 29
    label "Galen"
  ]
  node [
    id 30
    label "dokt&#243;r"
  ]
  node [
    id 31
    label "dawny"
  ]
  node [
    id 32
    label "rozw&#243;d"
  ]
  node [
    id 33
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 34
    label "eksprezydent"
  ]
  node [
    id 35
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 36
    label "partner"
  ]
  node [
    id 37
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 38
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 39
    label "wcze&#347;niejszy"
  ]
  node [
    id 40
    label "upewnienie_si&#281;"
  ]
  node [
    id 41
    label "ufanie"
  ]
  node [
    id 42
    label "wierzenie"
  ]
  node [
    id 43
    label "upewnianie_si&#281;"
  ]
  node [
    id 44
    label "prywatywny"
  ]
  node [
    id 45
    label "defect"
  ]
  node [
    id 46
    label "odej&#347;cie"
  ]
  node [
    id 47
    label "gap"
  ]
  node [
    id 48
    label "kr&#243;tki"
  ]
  node [
    id 49
    label "wyr&#243;b"
  ]
  node [
    id 50
    label "nieistnienie"
  ]
  node [
    id 51
    label "wada"
  ]
  node [
    id 52
    label "odej&#347;&#263;"
  ]
  node [
    id 53
    label "odchodzenie"
  ]
  node [
    id 54
    label "odchodzi&#263;"
  ]
  node [
    id 55
    label "pier&#347;cie&#324;_gard&#322;owy_Waldeyera"
  ]
  node [
    id 56
    label "tentegowa&#263;"
  ]
  node [
    id 57
    label "urz&#261;dza&#263;"
  ]
  node [
    id 58
    label "give"
  ]
  node [
    id 59
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 60
    label "czyni&#263;"
  ]
  node [
    id 61
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 62
    label "post&#281;powa&#263;"
  ]
  node [
    id 63
    label "wydala&#263;"
  ]
  node [
    id 64
    label "oszukiwa&#263;"
  ]
  node [
    id 65
    label "organizowa&#263;"
  ]
  node [
    id 66
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 67
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 68
    label "work"
  ]
  node [
    id 69
    label "przerabia&#263;"
  ]
  node [
    id 70
    label "stylizowa&#263;"
  ]
  node [
    id 71
    label "falowa&#263;"
  ]
  node [
    id 72
    label "act"
  ]
  node [
    id 73
    label "peddle"
  ]
  node [
    id 74
    label "ukazywa&#263;"
  ]
  node [
    id 75
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 76
    label "praca"
  ]
  node [
    id 77
    label "szczeg&#243;lnie"
  ]
  node [
    id 78
    label "wyj&#261;tkowy"
  ]
  node [
    id 79
    label "wynik"
  ]
  node [
    id 80
    label "r&#243;&#380;nienie"
  ]
  node [
    id 81
    label "cecha"
  ]
  node [
    id 82
    label "kontrastowy"
  ]
  node [
    id 83
    label "discord"
  ]
  node [
    id 84
    label "odwodnienie"
  ]
  node [
    id 85
    label "konstytucja"
  ]
  node [
    id 86
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 87
    label "substancja_chemiczna"
  ]
  node [
    id 88
    label "bratnia_dusza"
  ]
  node [
    id 89
    label "zwi&#261;zanie"
  ]
  node [
    id 90
    label "lokant"
  ]
  node [
    id 91
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 92
    label "zwi&#261;za&#263;"
  ]
  node [
    id 93
    label "organizacja"
  ]
  node [
    id 94
    label "odwadnia&#263;"
  ]
  node [
    id 95
    label "marriage"
  ]
  node [
    id 96
    label "marketing_afiliacyjny"
  ]
  node [
    id 97
    label "bearing"
  ]
  node [
    id 98
    label "wi&#261;zanie"
  ]
  node [
    id 99
    label "odwadnianie"
  ]
  node [
    id 100
    label "koligacja"
  ]
  node [
    id 101
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 102
    label "odwodni&#263;"
  ]
  node [
    id 103
    label "azeotrop"
  ]
  node [
    id 104
    label "powi&#261;zanie"
  ]
  node [
    id 105
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 106
    label "cz&#281;sty"
  ]
  node [
    id 107
    label "klasyfikator"
  ]
  node [
    id 108
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 109
    label "decide"
  ]
  node [
    id 110
    label "mean"
  ]
  node [
    id 111
    label "pom&#243;c"
  ]
  node [
    id 112
    label "spowodowa&#263;"
  ]
  node [
    id 113
    label "release"
  ]
  node [
    id 114
    label "wytworzy&#263;"
  ]
  node [
    id 115
    label "wzbudzi&#263;"
  ]
  node [
    id 116
    label "deliver"
  ]
  node [
    id 117
    label "cz&#322;owiek"
  ]
  node [
    id 118
    label "potomstwo"
  ]
  node [
    id 119
    label "organizm"
  ]
  node [
    id 120
    label "sraluch"
  ]
  node [
    id 121
    label "utulanie"
  ]
  node [
    id 122
    label "pediatra"
  ]
  node [
    id 123
    label "dzieciarnia"
  ]
  node [
    id 124
    label "m&#322;odziak"
  ]
  node [
    id 125
    label "dzieciak"
  ]
  node [
    id 126
    label "utula&#263;"
  ]
  node [
    id 127
    label "potomek"
  ]
  node [
    id 128
    label "pedofil"
  ]
  node [
    id 129
    label "entliczek-pentliczek"
  ]
  node [
    id 130
    label "m&#322;odzik"
  ]
  node [
    id 131
    label "cz&#322;owieczek"
  ]
  node [
    id 132
    label "zwierz&#281;"
  ]
  node [
    id 133
    label "niepe&#322;noletni"
  ]
  node [
    id 134
    label "fledgling"
  ]
  node [
    id 135
    label "utuli&#263;"
  ]
  node [
    id 136
    label "utulenie"
  ]
  node [
    id 137
    label "zapewnia&#263;"
  ]
  node [
    id 138
    label "manewr"
  ]
  node [
    id 139
    label "byt"
  ]
  node [
    id 140
    label "cope"
  ]
  node [
    id 141
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 142
    label "zachowywa&#263;"
  ]
  node [
    id 143
    label "twierdzi&#263;"
  ]
  node [
    id 144
    label "trzyma&#263;"
  ]
  node [
    id 145
    label "corroborate"
  ]
  node [
    id 146
    label "sprawowa&#263;"
  ]
  node [
    id 147
    label "s&#261;dzi&#263;"
  ]
  node [
    id 148
    label "podtrzymywa&#263;"
  ]
  node [
    id 149
    label "defy"
  ]
  node [
    id 150
    label "panowa&#263;"
  ]
  node [
    id 151
    label "argue"
  ]
  node [
    id 152
    label "broni&#263;"
  ]
  node [
    id 153
    label "m&#281;ka_Pa&#324;ska"
  ]
  node [
    id 154
    label "doznanie"
  ]
  node [
    id 155
    label "tkliwo&#347;&#263;"
  ]
  node [
    id 156
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 157
    label "toleration"
  ]
  node [
    id 158
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 159
    label "irradiacja"
  ]
  node [
    id 160
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 161
    label "prze&#380;ycie"
  ]
  node [
    id 162
    label "drzazga"
  ]
  node [
    id 163
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 164
    label "cier&#324;"
  ]
  node [
    id 165
    label "miejsce"
  ]
  node [
    id 166
    label "zdarcie"
  ]
  node [
    id 167
    label "jama_gard&#322;owa"
  ]
  node [
    id 168
    label "zachy&#322;ek_gruszkowaty"
  ]
  node [
    id 169
    label "throat"
  ]
  node [
    id 170
    label "zedrze&#263;"
  ]
  node [
    id 171
    label "przew&#243;d_pokarmowy"
  ]
  node [
    id 172
    label "element_anatomiczny"
  ]
  node [
    id 173
    label "choroba_somatyczna"
  ]
  node [
    id 174
    label "pami&#281;&#263;_immunologiczna"
  ]
  node [
    id 175
    label "wyrugowanie"
  ]
  node [
    id 176
    label "spowodowanie"
  ]
  node [
    id 177
    label "czynno&#347;&#263;"
  ]
  node [
    id 178
    label "przesuni&#281;cie"
  ]
  node [
    id 179
    label "pousuwanie"
  ]
  node [
    id 180
    label "znikni&#281;cie"
  ]
  node [
    id 181
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 182
    label "pozbycie_si&#281;"
  ]
  node [
    id 183
    label "coitus_interruptus"
  ]
  node [
    id 184
    label "przeniesienie"
  ]
  node [
    id 185
    label "wyniesienie"
  ]
  node [
    id 186
    label "abstraction"
  ]
  node [
    id 187
    label "pozabieranie"
  ]
  node [
    id 188
    label "removal"
  ]
  node [
    id 189
    label "bra&#263;_si&#281;"
  ]
  node [
    id 190
    label "&#347;wiadectwo"
  ]
  node [
    id 191
    label "przyczyna"
  ]
  node [
    id 192
    label "matuszka"
  ]
  node [
    id 193
    label "geneza"
  ]
  node [
    id 194
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 195
    label "kamena"
  ]
  node [
    id 196
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 197
    label "czynnik"
  ]
  node [
    id 198
    label "pocz&#261;tek"
  ]
  node [
    id 199
    label "poci&#261;ganie"
  ]
  node [
    id 200
    label "rezultat"
  ]
  node [
    id 201
    label "ciek_wodny"
  ]
  node [
    id 202
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 203
    label "subject"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 55
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 46
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 201
  ]
  edge [
    source 21
    target 202
  ]
  edge [
    source 21
    target 203
  ]
]
