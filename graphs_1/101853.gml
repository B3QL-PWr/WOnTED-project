graph [
  maxDegree 35
  minDegree 1
  meanDegree 2.0683760683760686
  density 0.008877150508051795
  graphCliqueNumber 3
  node [
    id 0
    label "lawrence"
    origin "text"
  ]
  node [
    id 1
    label "lessig"
    origin "text"
  ]
  node [
    id 2
    label "swoje"
    origin "text"
  ]
  node [
    id 3
    label "blog"
    origin "text"
  ]
  node [
    id 4
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 6
    label "krytyka"
    origin "text"
  ]
  node [
    id 7
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 8
    label "keena"
    origin "text"
  ]
  node [
    id 9
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 10
    label "niedziela"
    origin "text"
  ]
  node [
    id 11
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "mirek"
    origin "text"
  ]
  node [
    id 13
    label "poprzedza&#263;"
    origin "text"
  ]
  node [
    id 14
    label "wst&#281;p"
    origin "text"
  ]
  node [
    id 15
    label "zauwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 16
    label "kwestia"
    origin "text"
  ]
  node [
    id 17
    label "paradoksalny"
    origin "text"
  ]
  node [
    id 18
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 19
    label "krytykowa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "niechlujno&#347;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "ignorancja"
    origin "text"
  ]
  node [
    id 22
    label "internetowy"
    origin "text"
  ]
  node [
    id 23
    label "kultura"
    origin "text"
  ]
  node [
    id 24
    label "partycypacyjnej"
    origin "text"
  ]
  node [
    id 25
    label "sam"
    origin "text"
  ]
  node [
    id 26
    label "pope&#322;nia&#263;"
    origin "text"
  ]
  node [
    id 27
    label "liczny"
    origin "text"
  ]
  node [
    id 28
    label "b&#322;&#261;d"
    origin "text"
  ]
  node [
    id 29
    label "mimo"
    origin "text"
  ]
  node [
    id 30
    label "weryfikacja"
    origin "text"
  ]
  node [
    id 31
    label "przefiltrowa&#263;"
    origin "text"
  ]
  node [
    id 32
    label "tekst"
    origin "text"
  ]
  node [
    id 33
    label "przez"
    origin "text"
  ]
  node [
    id 34
    label "szereg"
    origin "text"
  ]
  node [
    id 35
    label "instytucja"
    origin "text"
  ]
  node [
    id 36
    label "komcio"
  ]
  node [
    id 37
    label "strona"
  ]
  node [
    id 38
    label "blogosfera"
  ]
  node [
    id 39
    label "pami&#281;tnik"
  ]
  node [
    id 40
    label "upubliczni&#263;"
  ]
  node [
    id 41
    label "wydawnictwo"
  ]
  node [
    id 42
    label "wprowadzi&#263;"
  ]
  node [
    id 43
    label "picture"
  ]
  node [
    id 44
    label "daleki"
  ]
  node [
    id 45
    label "d&#322;ugo"
  ]
  node [
    id 46
    label "ruch"
  ]
  node [
    id 47
    label "krytyka_literacka"
  ]
  node [
    id 48
    label "cenzura"
  ]
  node [
    id 49
    label "review"
  ]
  node [
    id 50
    label "ocena"
  ]
  node [
    id 51
    label "publiczno&#347;&#263;"
  ]
  node [
    id 52
    label "streszczenie"
  ]
  node [
    id 53
    label "diatryba"
  ]
  node [
    id 54
    label "criticism"
  ]
  node [
    id 55
    label "publicystyka"
  ]
  node [
    id 56
    label "ok&#322;adka"
  ]
  node [
    id 57
    label "zak&#322;adka"
  ]
  node [
    id 58
    label "ekslibris"
  ]
  node [
    id 59
    label "wk&#322;ad"
  ]
  node [
    id 60
    label "przek&#322;adacz"
  ]
  node [
    id 61
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 62
    label "tytu&#322;"
  ]
  node [
    id 63
    label "bibliofilstwo"
  ]
  node [
    id 64
    label "falc"
  ]
  node [
    id 65
    label "nomina&#322;"
  ]
  node [
    id 66
    label "pagina"
  ]
  node [
    id 67
    label "rozdzia&#322;"
  ]
  node [
    id 68
    label "egzemplarz"
  ]
  node [
    id 69
    label "zw&#243;j"
  ]
  node [
    id 70
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 71
    label "Wielkanoc"
  ]
  node [
    id 72
    label "dzie&#324;_&#347;wi&#281;ty"
  ]
  node [
    id 73
    label "weekend"
  ]
  node [
    id 74
    label "Niedziela_Palmowa"
  ]
  node [
    id 75
    label "Zes&#322;anie_Ducha_&#346;wi&#281;tego"
  ]
  node [
    id 76
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 77
    label "bia&#322;a_niedziela"
  ]
  node [
    id 78
    label "niedziela_przewodnia"
  ]
  node [
    id 79
    label "tydzie&#324;"
  ]
  node [
    id 80
    label "ozdabia&#263;"
  ]
  node [
    id 81
    label "dysgrafia"
  ]
  node [
    id 82
    label "prasa"
  ]
  node [
    id 83
    label "spell"
  ]
  node [
    id 84
    label "skryba"
  ]
  node [
    id 85
    label "donosi&#263;"
  ]
  node [
    id 86
    label "code"
  ]
  node [
    id 87
    label "dysortografia"
  ]
  node [
    id 88
    label "read"
  ]
  node [
    id 89
    label "tworzy&#263;"
  ]
  node [
    id 90
    label "formu&#322;owa&#263;"
  ]
  node [
    id 91
    label "styl"
  ]
  node [
    id 92
    label "stawia&#263;"
  ]
  node [
    id 93
    label "opatrywa&#263;"
  ]
  node [
    id 94
    label "czas"
  ]
  node [
    id 95
    label "doj&#347;cie"
  ]
  node [
    id 96
    label "zapowied&#378;"
  ]
  node [
    id 97
    label "evocation"
  ]
  node [
    id 98
    label "g&#322;oska"
  ]
  node [
    id 99
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 100
    label "wymowa"
  ]
  node [
    id 101
    label "pocz&#261;tek"
  ]
  node [
    id 102
    label "utw&#243;r"
  ]
  node [
    id 103
    label "podstawy"
  ]
  node [
    id 104
    label "widzie&#263;"
  ]
  node [
    id 105
    label "perceive"
  ]
  node [
    id 106
    label "obacza&#263;"
  ]
  node [
    id 107
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 108
    label "notice"
  ]
  node [
    id 109
    label "sprawa"
  ]
  node [
    id 110
    label "problemat"
  ]
  node [
    id 111
    label "wypowied&#378;"
  ]
  node [
    id 112
    label "dialog"
  ]
  node [
    id 113
    label "problematyka"
  ]
  node [
    id 114
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 115
    label "subject"
  ]
  node [
    id 116
    label "sprzeczny"
  ]
  node [
    id 117
    label "paradoksalnie"
  ]
  node [
    id 118
    label "strike"
  ]
  node [
    id 119
    label "rap"
  ]
  node [
    id 120
    label "opiniowa&#263;"
  ]
  node [
    id 121
    label "os&#261;dza&#263;"
  ]
  node [
    id 122
    label "wada"
  ]
  node [
    id 123
    label "niedba&#322;o&#347;&#263;"
  ]
  node [
    id 124
    label "niewiadomo&#347;&#263;"
  ]
  node [
    id 125
    label "stan"
  ]
  node [
    id 126
    label "brak"
  ]
  node [
    id 127
    label "bia&#322;e_plamy"
  ]
  node [
    id 128
    label "nowoczesny"
  ]
  node [
    id 129
    label "elektroniczny"
  ]
  node [
    id 130
    label "sieciowo"
  ]
  node [
    id 131
    label "netowy"
  ]
  node [
    id 132
    label "internetowo"
  ]
  node [
    id 133
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 134
    label "przedmiot"
  ]
  node [
    id 135
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 136
    label "Wsch&#243;d"
  ]
  node [
    id 137
    label "rzecz"
  ]
  node [
    id 138
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 139
    label "sztuka"
  ]
  node [
    id 140
    label "religia"
  ]
  node [
    id 141
    label "przejmowa&#263;"
  ]
  node [
    id 142
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 143
    label "makrokosmos"
  ]
  node [
    id 144
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 145
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 146
    label "zjawisko"
  ]
  node [
    id 147
    label "praca_rolnicza"
  ]
  node [
    id 148
    label "tradycja"
  ]
  node [
    id 149
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 150
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 151
    label "przejmowanie"
  ]
  node [
    id 152
    label "cecha"
  ]
  node [
    id 153
    label "asymilowanie_si&#281;"
  ]
  node [
    id 154
    label "przej&#261;&#263;"
  ]
  node [
    id 155
    label "hodowla"
  ]
  node [
    id 156
    label "brzoskwiniarnia"
  ]
  node [
    id 157
    label "populace"
  ]
  node [
    id 158
    label "konwencja"
  ]
  node [
    id 159
    label "propriety"
  ]
  node [
    id 160
    label "jako&#347;&#263;"
  ]
  node [
    id 161
    label "kuchnia"
  ]
  node [
    id 162
    label "zwyczaj"
  ]
  node [
    id 163
    label "przej&#281;cie"
  ]
  node [
    id 164
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 165
    label "sklep"
  ]
  node [
    id 166
    label "robi&#263;"
  ]
  node [
    id 167
    label "rojenie_si&#281;"
  ]
  node [
    id 168
    label "cz&#281;sty"
  ]
  node [
    id 169
    label "licznie"
  ]
  node [
    id 170
    label "baseball"
  ]
  node [
    id 171
    label "czyn"
  ]
  node [
    id 172
    label "error"
  ]
  node [
    id 173
    label "pomyli&#263;_si&#281;"
  ]
  node [
    id 174
    label "pomylenie_si&#281;"
  ]
  node [
    id 175
    label "mniemanie"
  ]
  node [
    id 176
    label "byk"
  ]
  node [
    id 177
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 178
    label "rezultat"
  ]
  node [
    id 179
    label "k&#322;amca_lustracyjny"
  ]
  node [
    id 180
    label "post&#281;powanie"
  ]
  node [
    id 181
    label "misja_weryfikacyjna"
  ]
  node [
    id 182
    label "postkomunizm"
  ]
  node [
    id 183
    label "kontrola"
  ]
  node [
    id 184
    label "analiza"
  ]
  node [
    id 185
    label "wyczy&#347;ci&#263;"
  ]
  node [
    id 186
    label "przepu&#347;ci&#263;"
  ]
  node [
    id 187
    label "usun&#261;&#263;"
  ]
  node [
    id 188
    label "nerka"
  ]
  node [
    id 189
    label "filter"
  ]
  node [
    id 190
    label "odmianka"
  ]
  node [
    id 191
    label "opu&#347;ci&#263;"
  ]
  node [
    id 192
    label "wytw&#243;r"
  ]
  node [
    id 193
    label "koniektura"
  ]
  node [
    id 194
    label "preparacja"
  ]
  node [
    id 195
    label "ekscerpcja"
  ]
  node [
    id 196
    label "redakcja"
  ]
  node [
    id 197
    label "obelga"
  ]
  node [
    id 198
    label "dzie&#322;o"
  ]
  node [
    id 199
    label "j&#281;zykowo"
  ]
  node [
    id 200
    label "pomini&#281;cie"
  ]
  node [
    id 201
    label "koniec"
  ]
  node [
    id 202
    label "unit"
  ]
  node [
    id 203
    label "uporz&#261;dkowanie"
  ]
  node [
    id 204
    label "szpaler"
  ]
  node [
    id 205
    label "tract"
  ]
  node [
    id 206
    label "wyra&#380;enie"
  ]
  node [
    id 207
    label "zbi&#243;r"
  ]
  node [
    id 208
    label "mn&#243;stwo"
  ]
  node [
    id 209
    label "rozmieszczenie"
  ]
  node [
    id 210
    label "column"
  ]
  node [
    id 211
    label "afiliowa&#263;"
  ]
  node [
    id 212
    label "osoba_prawna"
  ]
  node [
    id 213
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 214
    label "urz&#261;d"
  ]
  node [
    id 215
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 216
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 217
    label "establishment"
  ]
  node [
    id 218
    label "standard"
  ]
  node [
    id 219
    label "organizacja"
  ]
  node [
    id 220
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 221
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 222
    label "zamykanie"
  ]
  node [
    id 223
    label "zamyka&#263;"
  ]
  node [
    id 224
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 225
    label "poj&#281;cie"
  ]
  node [
    id 226
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 227
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 228
    label "Fundusze_Unijne"
  ]
  node [
    id 229
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 230
    label "biuro"
  ]
  node [
    id 231
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 232
    label "Lawrence"
  ]
  node [
    id 233
    label "Lessig"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 124
  ]
  edge [
    source 21
    target 125
  ]
  edge [
    source 21
    target 126
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 128
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 130
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 23
    target 133
  ]
  edge [
    source 23
    target 134
  ]
  edge [
    source 23
    target 135
  ]
  edge [
    source 23
    target 136
  ]
  edge [
    source 23
    target 137
  ]
  edge [
    source 23
    target 138
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 23
    target 140
  ]
  edge [
    source 23
    target 141
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 23
    target 151
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 23
    target 153
  ]
  edge [
    source 23
    target 154
  ]
  edge [
    source 23
    target 155
  ]
  edge [
    source 23
    target 156
  ]
  edge [
    source 23
    target 157
  ]
  edge [
    source 23
    target 158
  ]
  edge [
    source 23
    target 159
  ]
  edge [
    source 23
    target 160
  ]
  edge [
    source 23
    target 161
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 23
    target 164
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 166
  ]
  edge [
    source 26
    target 89
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 167
  ]
  edge [
    source 27
    target 168
  ]
  edge [
    source 27
    target 169
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 170
  ]
  edge [
    source 28
    target 171
  ]
  edge [
    source 28
    target 172
  ]
  edge [
    source 28
    target 173
  ]
  edge [
    source 28
    target 174
  ]
  edge [
    source 28
    target 175
  ]
  edge [
    source 28
    target 176
  ]
  edge [
    source 28
    target 177
  ]
  edge [
    source 28
    target 178
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 30
    target 181
  ]
  edge [
    source 30
    target 182
  ]
  edge [
    source 30
    target 183
  ]
  edge [
    source 30
    target 184
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 185
  ]
  edge [
    source 31
    target 186
  ]
  edge [
    source 31
    target 187
  ]
  edge [
    source 31
    target 188
  ]
  edge [
    source 31
    target 189
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 190
  ]
  edge [
    source 32
    target 191
  ]
  edge [
    source 32
    target 111
  ]
  edge [
    source 32
    target 192
  ]
  edge [
    source 32
    target 193
  ]
  edge [
    source 32
    target 194
  ]
  edge [
    source 32
    target 195
  ]
  edge [
    source 32
    target 196
  ]
  edge [
    source 32
    target 197
  ]
  edge [
    source 32
    target 198
  ]
  edge [
    source 32
    target 199
  ]
  edge [
    source 32
    target 200
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 201
  ]
  edge [
    source 34
    target 202
  ]
  edge [
    source 34
    target 203
  ]
  edge [
    source 34
    target 204
  ]
  edge [
    source 34
    target 205
  ]
  edge [
    source 34
    target 206
  ]
  edge [
    source 34
    target 207
  ]
  edge [
    source 34
    target 208
  ]
  edge [
    source 34
    target 209
  ]
  edge [
    source 34
    target 210
  ]
  edge [
    source 35
    target 211
  ]
  edge [
    source 35
    target 212
  ]
  edge [
    source 35
    target 213
  ]
  edge [
    source 35
    target 214
  ]
  edge [
    source 35
    target 215
  ]
  edge [
    source 35
    target 216
  ]
  edge [
    source 35
    target 217
  ]
  edge [
    source 35
    target 218
  ]
  edge [
    source 35
    target 219
  ]
  edge [
    source 35
    target 220
  ]
  edge [
    source 35
    target 221
  ]
  edge [
    source 35
    target 222
  ]
  edge [
    source 35
    target 223
  ]
  edge [
    source 35
    target 224
  ]
  edge [
    source 35
    target 225
  ]
  edge [
    source 35
    target 226
  ]
  edge [
    source 35
    target 227
  ]
  edge [
    source 35
    target 228
  ]
  edge [
    source 35
    target 229
  ]
  edge [
    source 35
    target 230
  ]
  edge [
    source 35
    target 231
  ]
  edge [
    source 232
    target 233
  ]
]
