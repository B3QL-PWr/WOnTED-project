graph [
  maxDegree 70
  minDegree 1
  meanDegree 2.3975659229208923
  density 0.002434077079107505
  graphCliqueNumber 4
  node [
    id 0
    label "lot"
    origin "text"
  ]
  node [
    id 1
    label "bardzo"
    origin "text"
  ]
  node [
    id 2
    label "&#378;le"
    origin "text"
  ]
  node [
    id 3
    label "spa&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "ten"
    origin "text"
  ]
  node [
    id 5
    label "noc"
    origin "text"
  ]
  node [
    id 6
    label "rozstrzygn&#261;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "czego"
    origin "text"
  ]
  node [
    id 9
    label "obawia&#263;"
    origin "text"
  ]
  node [
    id 10
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 11
    label "tak"
    origin "text"
  ]
  node [
    id 12
    label "zgo&#322;a"
    origin "text"
  ]
  node [
    id 13
    label "niespodziewany"
    origin "text"
  ]
  node [
    id 14
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 15
    label "przewidzie&#263;"
    origin "text"
  ]
  node [
    id 16
    label "nape&#322;ni&#263;"
    origin "text"
  ]
  node [
    id 17
    label "przera&#380;enie"
    origin "text"
  ]
  node [
    id 18
    label "zazwyczaj"
    origin "text"
  ]
  node [
    id 19
    label "spokojny"
    origin "text"
  ]
  node [
    id 20
    label "poczucie"
    origin "text"
  ]
  node [
    id 21
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 22
    label "niewinno&#347;&#263;"
    origin "text"
  ]
  node [
    id 23
    label "umie&#263;"
    origin "text"
  ]
  node [
    id 24
    label "siebie"
    origin "text"
  ]
  node [
    id 25
    label "radzi&#263;"
    origin "text"
  ]
  node [
    id 26
    label "popad&#322;y"
    origin "text"
  ]
  node [
    id 27
    label "gor&#261;czkowy"
    origin "text"
  ]
  node [
    id 28
    label "wzburzenie"
    origin "text"
  ]
  node [
    id 29
    label "gwa&#322;towny"
    origin "text"
  ]
  node [
    id 30
    label "uczucie"
    origin "text"
  ]
  node [
    id 31
    label "wstrz&#261;sa&#263;"
    origin "text"
  ]
  node [
    id 32
    label "serce"
    origin "text"
  ]
  node [
    id 33
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 34
    label "&#380;ar"
    origin "text"
  ]
  node [
    id 35
    label "obj&#281;cia"
    origin "text"
  ]
  node [
    id 36
    label "werter"
    origin "text"
  ]
  node [
    id 37
    label "jeszcze"
    origin "text"
  ]
  node [
    id 38
    label "odczuwa&#263;"
    origin "text"
  ]
  node [
    id 39
    label "oburza&#263;"
    origin "text"
  ]
  node [
    id 40
    label "zuchwalstwo"
    origin "text"
  ]
  node [
    id 41
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 42
    label "cierpie&#263;"
    origin "text"
  ]
  node [
    id 43
    label "por&#243;wnywa&#263;"
    origin "text"
  ]
  node [
    id 44
    label "obecny"
    origin "text"
  ]
  node [
    id 45
    label "stan"
    origin "text"
  ]
  node [
    id 46
    label "duchowy"
    origin "text"
  ]
  node [
    id 47
    label "poprzedni"
    origin "text"
  ]
  node [
    id 48
    label "swoboda"
    origin "text"
  ]
  node [
    id 49
    label "pewien"
    origin "text"
  ]
  node [
    id 50
    label "beztroski"
    origin "text"
  ]
  node [
    id 51
    label "zaufanie"
    origin "text"
  ]
  node [
    id 52
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 53
    label "jak"
    origin "text"
  ]
  node [
    id 54
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 55
    label "zachowa&#263;"
    origin "text"
  ]
  node [
    id 56
    label "wobec"
    origin "text"
  ]
  node [
    id 57
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 58
    label "rozmy&#347;la&#263;"
    origin "text"
  ]
  node [
    id 59
    label "opowiedzie&#263;"
    origin "text"
  ]
  node [
    id 60
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 61
    label "zaj&#347;cie"
    origin "text"
  ]
  node [
    id 62
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 63
    label "opisa&#263;"
    origin "text"
  ]
  node [
    id 64
    label "&#347;mia&#322;y"
    origin "text"
  ]
  node [
    id 65
    label "d&#322;ugo"
    origin "text"
  ]
  node [
    id 66
    label "porusza&#263;"
    origin "text"
  ]
  node [
    id 67
    label "temat"
    origin "text"
  ]
  node [
    id 68
    label "czy&#380;"
    origin "text"
  ]
  node [
    id 69
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 70
    label "pierwsza"
    origin "text"
  ]
  node [
    id 71
    label "przerwa&#263;"
    origin "text"
  ]
  node [
    id 72
    label "milczenie"
    origin "text"
  ]
  node [
    id 73
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 74
    label "niesposobny"
    origin "text"
  ]
  node [
    id 75
    label "chwila"
    origin "text"
  ]
  node [
    id 76
    label "uczyni&#263;"
    origin "text"
  ]
  node [
    id 77
    label "niespodziany"
    origin "text"
  ]
  node [
    id 78
    label "odkrycie"
    origin "text"
  ]
  node [
    id 79
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 80
    label "sam"
    origin "text"
  ]
  node [
    id 81
    label "wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 82
    label "bytno&#347;&#263;"
    origin "text"
  ]
  node [
    id 83
    label "by&#263;"
    origin "text"
  ]
  node [
    id 84
    label "dlaon"
    origin "text"
  ]
  node [
    id 85
    label "nader"
    origin "text"
  ]
  node [
    id 86
    label "niemi&#322;y"
    origin "text"
  ]
  node [
    id 87
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 88
    label "katastrofa"
    origin "text"
  ]
  node [
    id 89
    label "nadzieja"
    origin "text"
  ]
  node [
    id 90
    label "patrzy&#263;"
    origin "text"
  ]
  node [
    id 91
    label "rzecz"
    origin "text"
  ]
  node [
    id 92
    label "w&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 93
    label "punkt"
    origin "text"
  ]
  node [
    id 94
    label "os&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 95
    label "bez"
    origin "text"
  ]
  node [
    id 96
    label "uprzedzenie"
    origin "text"
  ]
  node [
    id 97
    label "druga"
    origin "text"
  ]
  node [
    id 98
    label "pragn&#261;&#263;"
    origin "text"
  ]
  node [
    id 99
    label "naprawd&#281;"
    origin "text"
  ]
  node [
    id 100
    label "wszystko"
    origin "text"
  ]
  node [
    id 101
    label "wyczyta&#263;"
    origin "text"
  ]
  node [
    id 102
    label "dusza"
    origin "text"
  ]
  node [
    id 103
    label "zdoby&#263;"
    origin "text"
  ]
  node [
    id 104
    label "przy"
    origin "text"
  ]
  node [
    id 105
    label "tym"
    origin "text"
  ]
  node [
    id 106
    label "zatai&#263;"
    origin "text"
  ]
  node [
    id 107
    label "przed"
    origin "text"
  ]
  node [
    id 108
    label "pewne"
    origin "text"
  ]
  node [
    id 109
    label "uczu&#263;"
    origin "text"
  ]
  node [
    id 110
    label "gdy&#380;"
    origin "text"
  ]
  node [
    id 111
    label "dot&#261;d"
    origin "text"
  ]
  node [
    id 112
    label "nigdy"
    origin "text"
  ]
  node [
    id 113
    label "zawsze"
    origin "text"
  ]
  node [
    id 114
    label "krystalicznie"
    origin "text"
  ]
  node [
    id 115
    label "czysta"
    origin "text"
  ]
  node [
    id 116
    label "otwarty"
    origin "text"
  ]
  node [
    id 117
    label "pe&#322;ny"
    origin "text"
  ]
  node [
    id 118
    label "trawi&#263;"
    origin "text"
  ]
  node [
    id 119
    label "niezmiernie"
    origin "text"
  ]
  node [
    id 120
    label "wprowadza&#263;"
    origin "text"
  ]
  node [
    id 121
    label "zak&#322;opotanie"
    origin "text"
  ]
  node [
    id 122
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 123
    label "wraca&#263;"
    origin "text"
  ]
  node [
    id 124
    label "ci&#261;gle"
    origin "text"
  ]
  node [
    id 125
    label "dla"
    origin "text"
  ]
  node [
    id 126
    label "stracony"
    origin "text"
  ]
  node [
    id 127
    label "strata"
    origin "text"
  ]
  node [
    id 128
    label "pogodzi&#263;"
    origin "text"
  ]
  node [
    id 129
    label "niestety"
    origin "text"
  ]
  node [
    id 130
    label "pozostawi&#263;"
    origin "text"
  ]
  node [
    id 131
    label "musia&#322;"
    origin "text"
  ]
  node [
    id 132
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 133
    label "los"
    origin "text"
  ]
  node [
    id 134
    label "czu&#322;y"
    origin "text"
  ]
  node [
    id 135
    label "gdy"
    origin "text"
  ]
  node [
    id 136
    label "utraci&#263;"
    origin "text"
  ]
  node [
    id 137
    label "pozosta&#263;"
    origin "text"
  ]
  node [
    id 138
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 139
    label "nic"
    origin "text"
  ]
  node [
    id 140
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 141
    label "chronometra&#380;ysta"
  ]
  node [
    id 142
    label "start"
  ]
  node [
    id 143
    label "ruch"
  ]
  node [
    id 144
    label "ci&#261;g"
  ]
  node [
    id 145
    label "l&#261;dowanie"
  ]
  node [
    id 146
    label "odlot"
  ]
  node [
    id 147
    label "podr&#243;&#380;"
  ]
  node [
    id 148
    label "flight"
  ]
  node [
    id 149
    label "w_chuj"
  ]
  node [
    id 150
    label "niezgodnie"
  ]
  node [
    id 151
    label "niepomy&#347;lnie"
  ]
  node [
    id 152
    label "negatywnie"
  ]
  node [
    id 153
    label "piesko"
  ]
  node [
    id 154
    label "z&#322;y"
  ]
  node [
    id 155
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 156
    label "gorzej"
  ]
  node [
    id 157
    label "niekorzystnie"
  ]
  node [
    id 158
    label "pie&#324;"
  ]
  node [
    id 159
    label "okre&#347;lony"
  ]
  node [
    id 160
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 161
    label "doba"
  ]
  node [
    id 162
    label "czas"
  ]
  node [
    id 163
    label "p&#243;&#322;noc"
  ]
  node [
    id 164
    label "nokturn"
  ]
  node [
    id 165
    label "zjawisko"
  ]
  node [
    id 166
    label "night"
  ]
  node [
    id 167
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 168
    label "decide"
  ]
  node [
    id 169
    label "determine"
  ]
  node [
    id 170
    label "zdecydowa&#263;"
  ]
  node [
    id 171
    label "model"
  ]
  node [
    id 172
    label "zbi&#243;r"
  ]
  node [
    id 173
    label "tryb"
  ]
  node [
    id 174
    label "narz&#281;dzie"
  ]
  node [
    id 175
    label "nature"
  ]
  node [
    id 176
    label "niespodziewanie"
  ]
  node [
    id 177
    label "zaskakuj&#261;cy"
  ]
  node [
    id 178
    label "nieprzewidzianie"
  ]
  node [
    id 179
    label "uprawi&#263;"
  ]
  node [
    id 180
    label "gotowy"
  ]
  node [
    id 181
    label "might"
  ]
  node [
    id 182
    label "envision"
  ]
  node [
    id 183
    label "zaplanowa&#263;"
  ]
  node [
    id 184
    label "spodzia&#263;_si&#281;"
  ]
  node [
    id 185
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 186
    label "wywo&#322;a&#263;"
  ]
  node [
    id 187
    label "do"
  ]
  node [
    id 188
    label "umie&#347;ci&#263;"
  ]
  node [
    id 189
    label "perform"
  ]
  node [
    id 190
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 191
    label "sprawi&#263;"
  ]
  node [
    id 192
    label "wzbudzi&#263;"
  ]
  node [
    id 193
    label "perturbation"
  ]
  node [
    id 194
    label "l&#281;k"
  ]
  node [
    id 195
    label "przera&#380;enie_si&#281;"
  ]
  node [
    id 196
    label "przestraszenie"
  ]
  node [
    id 197
    label "zwykle"
  ]
  node [
    id 198
    label "uspokojenie"
  ]
  node [
    id 199
    label "uspokojenie_si&#281;"
  ]
  node [
    id 200
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 201
    label "przyjemny"
  ]
  node [
    id 202
    label "wolny"
  ]
  node [
    id 203
    label "spokojnie"
  ]
  node [
    id 204
    label "cicho"
  ]
  node [
    id 205
    label "nietrudny"
  ]
  node [
    id 206
    label "bezproblemowy"
  ]
  node [
    id 207
    label "uspokajanie_si&#281;"
  ]
  node [
    id 208
    label "uspokajanie"
  ]
  node [
    id 209
    label "zareagowanie"
  ]
  node [
    id 210
    label "opanowanie"
  ]
  node [
    id 211
    label "doznanie"
  ]
  node [
    id 212
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 213
    label "intuition"
  ]
  node [
    id 214
    label "wiedza"
  ]
  node [
    id 215
    label "ekstraspekcja"
  ]
  node [
    id 216
    label "os&#322;upienie"
  ]
  node [
    id 217
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 218
    label "smell"
  ]
  node [
    id 219
    label "zdarzenie_si&#281;"
  ]
  node [
    id 220
    label "feeling"
  ]
  node [
    id 221
    label "cz&#322;owiek"
  ]
  node [
    id 222
    label "bli&#378;ni"
  ]
  node [
    id 223
    label "odpowiedni"
  ]
  node [
    id 224
    label "swojak"
  ]
  node [
    id 225
    label "samodzielny"
  ]
  node [
    id 226
    label "fineness"
  ]
  node [
    id 227
    label "skromno&#347;&#263;"
  ]
  node [
    id 228
    label "dobro&#263;"
  ]
  node [
    id 229
    label "cecha"
  ]
  node [
    id 230
    label "can"
  ]
  node [
    id 231
    label "rede"
  ]
  node [
    id 232
    label "udziela&#263;"
  ]
  node [
    id 233
    label "rozmawia&#263;"
  ]
  node [
    id 234
    label "argue"
  ]
  node [
    id 235
    label "nieprzytomny"
  ]
  node [
    id 236
    label "nerwowy"
  ]
  node [
    id 237
    label "gor&#261;czkowo"
  ]
  node [
    id 238
    label "pop&#281;dliwy"
  ]
  node [
    id 239
    label "gorliwy"
  ]
  node [
    id 240
    label "niezdrowy"
  ]
  node [
    id 241
    label "zm&#261;cenie"
  ]
  node [
    id 242
    label "wzburzenie_si&#281;"
  ]
  node [
    id 243
    label "oburzenie_si&#281;"
  ]
  node [
    id 244
    label "zdenerwowanie"
  ]
  node [
    id 245
    label "emocja"
  ]
  node [
    id 246
    label "bustle"
  ]
  node [
    id 247
    label "move"
  ]
  node [
    id 248
    label "poruszenie"
  ]
  node [
    id 249
    label "afekcja"
  ]
  node [
    id 250
    label "wrath"
  ]
  node [
    id 251
    label "wzburzony"
  ]
  node [
    id 252
    label "nieopanowany"
  ]
  node [
    id 253
    label "nieoczekiwany"
  ]
  node [
    id 254
    label "porywczy"
  ]
  node [
    id 255
    label "raptownie"
  ]
  node [
    id 256
    label "silny"
  ]
  node [
    id 257
    label "radykalny"
  ]
  node [
    id 258
    label "zawrze&#263;"
  ]
  node [
    id 259
    label "gwa&#322;townie"
  ]
  node [
    id 260
    label "zawrzenie"
  ]
  node [
    id 261
    label "dziko"
  ]
  node [
    id 262
    label "szybki"
  ]
  node [
    id 263
    label "wpa&#347;&#263;"
  ]
  node [
    id 264
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 265
    label "d&#322;awi&#263;"
  ]
  node [
    id 266
    label "wpada&#263;"
  ]
  node [
    id 267
    label "zmys&#322;"
  ]
  node [
    id 268
    label "zaanga&#380;owanie"
  ]
  node [
    id 269
    label "ostygn&#261;&#263;"
  ]
  node [
    id 270
    label "afekt"
  ]
  node [
    id 271
    label "iskrzy&#263;"
  ]
  node [
    id 272
    label "przeczulica"
  ]
  node [
    id 273
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 274
    label "czucie"
  ]
  node [
    id 275
    label "ogrom"
  ]
  node [
    id 276
    label "stygn&#261;&#263;"
  ]
  node [
    id 277
    label "temperatura"
  ]
  node [
    id 278
    label "rusza&#263;"
  ]
  node [
    id 279
    label "wzbudza&#263;"
  ]
  node [
    id 280
    label "narusza&#263;"
  ]
  node [
    id 281
    label "go"
  ]
  node [
    id 282
    label "shake"
  ]
  node [
    id 283
    label "courage"
  ]
  node [
    id 284
    label "mi&#281;sie&#324;"
  ]
  node [
    id 285
    label "kompleks"
  ]
  node [
    id 286
    label "sfera_afektywna"
  ]
  node [
    id 287
    label "heart"
  ]
  node [
    id 288
    label "sumienie"
  ]
  node [
    id 289
    label "przedsionek"
  ]
  node [
    id 290
    label "entity"
  ]
  node [
    id 291
    label "nastawienie"
  ]
  node [
    id 292
    label "kompleksja"
  ]
  node [
    id 293
    label "kier"
  ]
  node [
    id 294
    label "systol"
  ]
  node [
    id 295
    label "pikawa"
  ]
  node [
    id 296
    label "power"
  ]
  node [
    id 297
    label "zastawka"
  ]
  node [
    id 298
    label "psychika"
  ]
  node [
    id 299
    label "wola"
  ]
  node [
    id 300
    label "komora"
  ]
  node [
    id 301
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 302
    label "zapalno&#347;&#263;"
  ]
  node [
    id 303
    label "wsierdzie"
  ]
  node [
    id 304
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 305
    label "podekscytowanie"
  ]
  node [
    id 306
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 307
    label "strunowiec"
  ]
  node [
    id 308
    label "favor"
  ]
  node [
    id 309
    label "fizjonomia"
  ]
  node [
    id 310
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 311
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 312
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 313
    label "charakter"
  ]
  node [
    id 314
    label "mikrokosmos"
  ]
  node [
    id 315
    label "dzwon"
  ]
  node [
    id 316
    label "koniuszek_serca"
  ]
  node [
    id 317
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 318
    label "passion"
  ]
  node [
    id 319
    label "pulsowa&#263;"
  ]
  node [
    id 320
    label "ego"
  ]
  node [
    id 321
    label "organ"
  ]
  node [
    id 322
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 323
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 324
    label "kardiografia"
  ]
  node [
    id 325
    label "osobowo&#347;&#263;"
  ]
  node [
    id 326
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 327
    label "kszta&#322;t"
  ]
  node [
    id 328
    label "karta"
  ]
  node [
    id 329
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 330
    label "podroby"
  ]
  node [
    id 331
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 332
    label "pulsowanie"
  ]
  node [
    id 333
    label "deformowa&#263;"
  ]
  node [
    id 334
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 335
    label "seksualno&#347;&#263;"
  ]
  node [
    id 336
    label "deformowanie"
  ]
  node [
    id 337
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 338
    label "elektrokardiografia"
  ]
  node [
    id 339
    label "cognizance"
  ]
  node [
    id 340
    label "gor&#261;co"
  ]
  node [
    id 341
    label "co&#347;"
  ]
  node [
    id 342
    label "p&#322;omie&#324;"
  ]
  node [
    id 343
    label "fire"
  ]
  node [
    id 344
    label "pogoda"
  ]
  node [
    id 345
    label "przyp&#322;yw"
  ]
  node [
    id 346
    label "feel"
  ]
  node [
    id 347
    label "widzie&#263;"
  ]
  node [
    id 348
    label "uczuwa&#263;"
  ]
  node [
    id 349
    label "notice"
  ]
  node [
    id 350
    label "konsekwencja"
  ]
  node [
    id 351
    label "postrzega&#263;"
  ]
  node [
    id 352
    label "doznawa&#263;"
  ]
  node [
    id 353
    label "disgust"
  ]
  node [
    id 354
    label "dzia&#322;a&#263;_na_nerwy"
  ]
  node [
    id 355
    label "charakterek"
  ]
  node [
    id 356
    label "chojractwo"
  ]
  node [
    id 357
    label "chutzpa"
  ]
  node [
    id 358
    label "pewno&#347;&#263;_siebie"
  ]
  node [
    id 359
    label "niegrzeczno&#347;&#263;"
  ]
  node [
    id 360
    label "przewa&#322;"
  ]
  node [
    id 361
    label "post&#281;pek"
  ]
  node [
    id 362
    label "j&#281;cze&#263;"
  ]
  node [
    id 363
    label "martwi&#263;_si&#281;"
  ]
  node [
    id 364
    label "wytrzymywa&#263;"
  ]
  node [
    id 365
    label "czu&#263;"
  ]
  node [
    id 366
    label "represent"
  ]
  node [
    id 367
    label "pochyla&#263;_si&#281;"
  ]
  node [
    id 368
    label "traci&#263;"
  ]
  node [
    id 369
    label "narzeka&#263;"
  ]
  node [
    id 370
    label "sting"
  ]
  node [
    id 371
    label "hurt"
  ]
  node [
    id 372
    label "boryka&#263;_si&#281;"
  ]
  node [
    id 373
    label "analizowa&#263;"
  ]
  node [
    id 374
    label "szacowa&#263;"
  ]
  node [
    id 375
    label "przytomny"
  ]
  node [
    id 376
    label "obliczny"
  ]
  node [
    id 377
    label "lista_obecno&#347;ci"
  ]
  node [
    id 378
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 379
    label "aktualnie"
  ]
  node [
    id 380
    label "&#347;wiadomy"
  ]
  node [
    id 381
    label "Arizona"
  ]
  node [
    id 382
    label "Georgia"
  ]
  node [
    id 383
    label "warstwa"
  ]
  node [
    id 384
    label "jednostka_administracyjna"
  ]
  node [
    id 385
    label "Hawaje"
  ]
  node [
    id 386
    label "Goa"
  ]
  node [
    id 387
    label "Floryda"
  ]
  node [
    id 388
    label "Oklahoma"
  ]
  node [
    id 389
    label "Alaska"
  ]
  node [
    id 390
    label "wci&#281;cie"
  ]
  node [
    id 391
    label "Alabama"
  ]
  node [
    id 392
    label "Oregon"
  ]
  node [
    id 393
    label "poziom"
  ]
  node [
    id 394
    label "Teksas"
  ]
  node [
    id 395
    label "Illinois"
  ]
  node [
    id 396
    label "Waszyngton"
  ]
  node [
    id 397
    label "Jukatan"
  ]
  node [
    id 398
    label "shape"
  ]
  node [
    id 399
    label "Nowy_Meksyk"
  ]
  node [
    id 400
    label "ilo&#347;&#263;"
  ]
  node [
    id 401
    label "state"
  ]
  node [
    id 402
    label "Nowy_York"
  ]
  node [
    id 403
    label "Arakan"
  ]
  node [
    id 404
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 405
    label "Kalifornia"
  ]
  node [
    id 406
    label "wektor"
  ]
  node [
    id 407
    label "Massachusetts"
  ]
  node [
    id 408
    label "miejsce"
  ]
  node [
    id 409
    label "Pensylwania"
  ]
  node [
    id 410
    label "Michigan"
  ]
  node [
    id 411
    label "Maryland"
  ]
  node [
    id 412
    label "Ohio"
  ]
  node [
    id 413
    label "Kansas"
  ]
  node [
    id 414
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 415
    label "Luizjana"
  ]
  node [
    id 416
    label "samopoczucie"
  ]
  node [
    id 417
    label "Wirginia"
  ]
  node [
    id 418
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 419
    label "psychiczny"
  ]
  node [
    id 420
    label "duchowo"
  ]
  node [
    id 421
    label "poprzednio"
  ]
  node [
    id 422
    label "przesz&#322;y"
  ]
  node [
    id 423
    label "wcze&#347;niejszy"
  ]
  node [
    id 424
    label "naturalno&#347;&#263;"
  ]
  node [
    id 425
    label "zdolno&#347;&#263;"
  ]
  node [
    id 426
    label "freedom"
  ]
  node [
    id 427
    label "upewnienie_si&#281;"
  ]
  node [
    id 428
    label "wierzenie"
  ]
  node [
    id 429
    label "mo&#380;liwy"
  ]
  node [
    id 430
    label "ufanie"
  ]
  node [
    id 431
    label "jaki&#347;"
  ]
  node [
    id 432
    label "upewnianie_si&#281;"
  ]
  node [
    id 433
    label "letki"
  ]
  node [
    id 434
    label "nierozwa&#380;ny"
  ]
  node [
    id 435
    label "beztrosko"
  ]
  node [
    id 436
    label "pogodny"
  ]
  node [
    id 437
    label "beztroskliwy"
  ]
  node [
    id 438
    label "lekko"
  ]
  node [
    id 439
    label "zrobienie"
  ]
  node [
    id 440
    label "credit"
  ]
  node [
    id 441
    label "firma"
  ]
  node [
    id 442
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 443
    label "zacz&#281;cie"
  ]
  node [
    id 444
    label "opoka"
  ]
  node [
    id 445
    label "faith"
  ]
  node [
    id 446
    label "postawa"
  ]
  node [
    id 447
    label "wojsko"
  ]
  node [
    id 448
    label "magnitude"
  ]
  node [
    id 449
    label "energia"
  ]
  node [
    id 450
    label "capacity"
  ]
  node [
    id 451
    label "wuchta"
  ]
  node [
    id 452
    label "parametr"
  ]
  node [
    id 453
    label "moment_si&#322;y"
  ]
  node [
    id 454
    label "przemoc"
  ]
  node [
    id 455
    label "mn&#243;stwo"
  ]
  node [
    id 456
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 457
    label "rozwi&#261;zanie"
  ]
  node [
    id 458
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 459
    label "potencja"
  ]
  node [
    id 460
    label "zaleta"
  ]
  node [
    id 461
    label "byd&#322;o"
  ]
  node [
    id 462
    label "zobo"
  ]
  node [
    id 463
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 464
    label "yakalo"
  ]
  node [
    id 465
    label "dzo"
  ]
  node [
    id 466
    label "czyj&#347;"
  ]
  node [
    id 467
    label "tajemnica"
  ]
  node [
    id 468
    label "pami&#281;&#263;"
  ]
  node [
    id 469
    label "bury"
  ]
  node [
    id 470
    label "zdyscyplinowanie"
  ]
  node [
    id 471
    label "podtrzyma&#263;"
  ]
  node [
    id 472
    label "preserve"
  ]
  node [
    id 473
    label "post&#261;pi&#263;"
  ]
  node [
    id 474
    label "post"
  ]
  node [
    id 475
    label "zrobi&#263;"
  ]
  node [
    id 476
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 477
    label "przechowa&#263;"
  ]
  node [
    id 478
    label "dieta"
  ]
  node [
    id 479
    label "pan_domu"
  ]
  node [
    id 480
    label "ch&#322;op"
  ]
  node [
    id 481
    label "ma&#322;&#380;onek"
  ]
  node [
    id 482
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 483
    label "stary"
  ]
  node [
    id 484
    label "&#347;lubny"
  ]
  node [
    id 485
    label "pan_i_w&#322;adca"
  ]
  node [
    id 486
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 487
    label "pan_m&#322;ody"
  ]
  node [
    id 488
    label "describe"
  ]
  node [
    id 489
    label "przedstawi&#263;"
  ]
  node [
    id 490
    label "du&#380;y"
  ]
  node [
    id 491
    label "jedyny"
  ]
  node [
    id 492
    label "kompletny"
  ]
  node [
    id 493
    label "zdr&#243;w"
  ]
  node [
    id 494
    label "&#380;ywy"
  ]
  node [
    id 495
    label "ca&#322;o"
  ]
  node [
    id 496
    label "calu&#347;ko"
  ]
  node [
    id 497
    label "podobny"
  ]
  node [
    id 498
    label "porobienie_si&#281;"
  ]
  node [
    id 499
    label "doj&#347;cie"
  ]
  node [
    id 500
    label "przestanie"
  ]
  node [
    id 501
    label "entrance"
  ]
  node [
    id 502
    label "skrycie_si&#281;"
  ]
  node [
    id 503
    label "krajobraz"
  ]
  node [
    id 504
    label "podej&#347;cie"
  ]
  node [
    id 505
    label "zakrycie"
  ]
  node [
    id 506
    label "ploy"
  ]
  node [
    id 507
    label "przyobleczenie_si&#281;"
  ]
  node [
    id 508
    label "zaniesienie"
  ]
  node [
    id 509
    label "wydarzenie"
  ]
  node [
    id 510
    label "happening"
  ]
  node [
    id 511
    label "event"
  ]
  node [
    id 512
    label "stanie_si&#281;"
  ]
  node [
    id 513
    label "odwiedzenie"
  ]
  node [
    id 514
    label "set"
  ]
  node [
    id 515
    label "zinterpretowa&#263;"
  ]
  node [
    id 516
    label "relate"
  ]
  node [
    id 517
    label "zapozna&#263;"
  ]
  node [
    id 518
    label "delineate"
  ]
  node [
    id 519
    label "&#347;mia&#322;o"
  ]
  node [
    id 520
    label "dziarski"
  ]
  node [
    id 521
    label "odwa&#380;ny"
  ]
  node [
    id 522
    label "d&#322;ugi"
  ]
  node [
    id 523
    label "podnosi&#263;"
  ]
  node [
    id 524
    label "meet"
  ]
  node [
    id 525
    label "act"
  ]
  node [
    id 526
    label "porobi&#263;"
  ]
  node [
    id 527
    label "drive"
  ]
  node [
    id 528
    label "robi&#263;"
  ]
  node [
    id 529
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 530
    label "powodowa&#263;"
  ]
  node [
    id 531
    label "fraza"
  ]
  node [
    id 532
    label "forma"
  ]
  node [
    id 533
    label "melodia"
  ]
  node [
    id 534
    label "zbacza&#263;"
  ]
  node [
    id 535
    label "omawia&#263;"
  ]
  node [
    id 536
    label "topik"
  ]
  node [
    id 537
    label "wyraz_pochodny"
  ]
  node [
    id 538
    label "om&#243;wi&#263;"
  ]
  node [
    id 539
    label "omawianie"
  ]
  node [
    id 540
    label "w&#261;tek"
  ]
  node [
    id 541
    label "forum"
  ]
  node [
    id 542
    label "zboczenie"
  ]
  node [
    id 543
    label "zbaczanie"
  ]
  node [
    id 544
    label "tre&#347;&#263;"
  ]
  node [
    id 545
    label "tematyka"
  ]
  node [
    id 546
    label "sprawa"
  ]
  node [
    id 547
    label "istota"
  ]
  node [
    id 548
    label "otoczka"
  ]
  node [
    id 549
    label "zboczy&#263;"
  ]
  node [
    id 550
    label "om&#243;wienie"
  ]
  node [
    id 551
    label "ptak_&#347;piewaj&#261;cy"
  ]
  node [
    id 552
    label "&#322;uskacze"
  ]
  node [
    id 553
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 554
    label "need"
  ]
  node [
    id 555
    label "hide"
  ]
  node [
    id 556
    label "support"
  ]
  node [
    id 557
    label "godzina"
  ]
  node [
    id 558
    label "calve"
  ]
  node [
    id 559
    label "rozerwa&#263;"
  ]
  node [
    id 560
    label "forbid"
  ]
  node [
    id 561
    label "przerywanie"
  ]
  node [
    id 562
    label "przerzedzi&#263;"
  ]
  node [
    id 563
    label "break"
  ]
  node [
    id 564
    label "wstrzyma&#263;"
  ]
  node [
    id 565
    label "urwa&#263;"
  ]
  node [
    id 566
    label "przesta&#263;"
  ]
  node [
    id 567
    label "przedziurawi&#263;"
  ]
  node [
    id 568
    label "przeszkodzi&#263;"
  ]
  node [
    id 569
    label "suspend"
  ]
  node [
    id 570
    label "przerwanie"
  ]
  node [
    id 571
    label "kultywar"
  ]
  node [
    id 572
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 573
    label "przerywa&#263;"
  ]
  node [
    id 574
    label "przemilczanie"
  ]
  node [
    id 575
    label "przemilczenie"
  ]
  node [
    id 576
    label "bycie"
  ]
  node [
    id 577
    label "hush"
  ]
  node [
    id 578
    label "omerta"
  ]
  node [
    id 579
    label "pomilczenie"
  ]
  node [
    id 580
    label "cisza"
  ]
  node [
    id 581
    label "motionlessness"
  ]
  node [
    id 582
    label "dok&#322;adnie"
  ]
  node [
    id 583
    label "time"
  ]
  node [
    id 584
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 585
    label "objawienie"
  ]
  node [
    id 586
    label "jawny"
  ]
  node [
    id 587
    label "zsuni&#281;cie"
  ]
  node [
    id 588
    label "znalezienie"
  ]
  node [
    id 589
    label "poinformowanie"
  ]
  node [
    id 590
    label "novum"
  ]
  node [
    id 591
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 592
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 593
    label "ukazanie"
  ]
  node [
    id 594
    label "disclosure"
  ]
  node [
    id 595
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 596
    label "poznanie"
  ]
  node [
    id 597
    label "podniesienie"
  ]
  node [
    id 598
    label "detection"
  ]
  node [
    id 599
    label "discovery"
  ]
  node [
    id 600
    label "niespodzianka"
  ]
  node [
    id 601
    label "partnerka"
  ]
  node [
    id 602
    label "sklep"
  ]
  node [
    id 603
    label "zarys"
  ]
  node [
    id 604
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 605
    label "zniesienie"
  ]
  node [
    id 606
    label "nap&#322;ywanie"
  ]
  node [
    id 607
    label "znosi&#263;"
  ]
  node [
    id 608
    label "informacja"
  ]
  node [
    id 609
    label "znie&#347;&#263;"
  ]
  node [
    id 610
    label "komunikat"
  ]
  node [
    id 611
    label "depesza_emska"
  ]
  node [
    id 612
    label "communication"
  ]
  node [
    id 613
    label "znoszenie"
  ]
  node [
    id 614
    label "signal"
  ]
  node [
    id 615
    label "obecno&#347;&#263;"
  ]
  node [
    id 616
    label "si&#281;ga&#263;"
  ]
  node [
    id 617
    label "trwa&#263;"
  ]
  node [
    id 618
    label "stand"
  ]
  node [
    id 619
    label "mie&#263;_miejsce"
  ]
  node [
    id 620
    label "uczestniczy&#263;"
  ]
  node [
    id 621
    label "chodzi&#263;"
  ]
  node [
    id 622
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 623
    label "equal"
  ]
  node [
    id 624
    label "niemile"
  ]
  node [
    id 625
    label "rozd&#281;cie_si&#281;"
  ]
  node [
    id 626
    label "niezgodny"
  ]
  node [
    id 627
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 628
    label "nieprzyjemnie"
  ]
  node [
    id 629
    label "remark"
  ]
  node [
    id 630
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 631
    label "u&#380;ywa&#263;"
  ]
  node [
    id 632
    label "okre&#347;la&#263;"
  ]
  node [
    id 633
    label "j&#281;zyk"
  ]
  node [
    id 634
    label "say"
  ]
  node [
    id 635
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 636
    label "formu&#322;owa&#263;"
  ]
  node [
    id 637
    label "talk"
  ]
  node [
    id 638
    label "powiada&#263;"
  ]
  node [
    id 639
    label "informowa&#263;"
  ]
  node [
    id 640
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 641
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 642
    label "wydobywa&#263;"
  ]
  node [
    id 643
    label "express"
  ]
  node [
    id 644
    label "chew_the_fat"
  ]
  node [
    id 645
    label "dysfonia"
  ]
  node [
    id 646
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 647
    label "tell"
  ]
  node [
    id 648
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 649
    label "wyra&#380;a&#263;"
  ]
  node [
    id 650
    label "gaworzy&#263;"
  ]
  node [
    id 651
    label "dziama&#263;"
  ]
  node [
    id 652
    label "prawi&#263;"
  ]
  node [
    id 653
    label "Smole&#324;sk"
  ]
  node [
    id 654
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 655
    label "visitation"
  ]
  node [
    id 656
    label "&#347;wiate&#322;ko_w_tunelu"
  ]
  node [
    id 657
    label "wierzy&#263;"
  ]
  node [
    id 658
    label "szansa"
  ]
  node [
    id 659
    label "oczekiwanie"
  ]
  node [
    id 660
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 661
    label "spoczywa&#263;"
  ]
  node [
    id 662
    label "koso"
  ]
  node [
    id 663
    label "szuka&#263;"
  ]
  node [
    id 664
    label "go_steady"
  ]
  node [
    id 665
    label "dba&#263;"
  ]
  node [
    id 666
    label "traktowa&#263;"
  ]
  node [
    id 667
    label "os&#261;dza&#263;"
  ]
  node [
    id 668
    label "punkt_widzenia"
  ]
  node [
    id 669
    label "uwa&#380;a&#263;"
  ]
  node [
    id 670
    label "look"
  ]
  node [
    id 671
    label "pogl&#261;da&#263;"
  ]
  node [
    id 672
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 673
    label "obiekt"
  ]
  node [
    id 674
    label "wpadanie"
  ]
  node [
    id 675
    label "przedmiot"
  ]
  node [
    id 676
    label "kultura"
  ]
  node [
    id 677
    label "przyroda"
  ]
  node [
    id 678
    label "mienie"
  ]
  node [
    id 679
    label "object"
  ]
  node [
    id 680
    label "wpadni&#281;cie"
  ]
  node [
    id 681
    label "taki"
  ]
  node [
    id 682
    label "nale&#380;yty"
  ]
  node [
    id 683
    label "charakterystyczny"
  ]
  node [
    id 684
    label "stosownie"
  ]
  node [
    id 685
    label "dobry"
  ]
  node [
    id 686
    label "prawdziwy"
  ]
  node [
    id 687
    label "uprawniony"
  ]
  node [
    id 688
    label "zasadniczy"
  ]
  node [
    id 689
    label "typowy"
  ]
  node [
    id 690
    label "nale&#380;ny"
  ]
  node [
    id 691
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 692
    label "prosta"
  ]
  node [
    id 693
    label "po&#322;o&#380;enie"
  ]
  node [
    id 694
    label "ust&#281;p"
  ]
  node [
    id 695
    label "problemat"
  ]
  node [
    id 696
    label "kres"
  ]
  node [
    id 697
    label "mark"
  ]
  node [
    id 698
    label "pozycja"
  ]
  node [
    id 699
    label "point"
  ]
  node [
    id 700
    label "stopie&#324;_pisma"
  ]
  node [
    id 701
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 702
    label "przestrze&#324;"
  ]
  node [
    id 703
    label "problematyka"
  ]
  node [
    id 704
    label "zapunktowa&#263;"
  ]
  node [
    id 705
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 706
    label "obiekt_matematyczny"
  ]
  node [
    id 707
    label "plamka"
  ]
  node [
    id 708
    label "plan"
  ]
  node [
    id 709
    label "podpunkt"
  ]
  node [
    id 710
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 711
    label "jednostka"
  ]
  node [
    id 712
    label "znale&#378;&#263;"
  ]
  node [
    id 713
    label "pomy&#347;le&#263;"
  ]
  node [
    id 714
    label "evaluate"
  ]
  node [
    id 715
    label "ki&#347;&#263;"
  ]
  node [
    id 716
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 717
    label "krzew"
  ]
  node [
    id 718
    label "pi&#380;maczkowate"
  ]
  node [
    id 719
    label "pestkowiec"
  ]
  node [
    id 720
    label "kwiat"
  ]
  node [
    id 721
    label "owoc"
  ]
  node [
    id 722
    label "oliwkowate"
  ]
  node [
    id 723
    label "ro&#347;lina"
  ]
  node [
    id 724
    label "hy&#263;ka"
  ]
  node [
    id 725
    label "lilac"
  ]
  node [
    id 726
    label "delfinidyna"
  ]
  node [
    id 727
    label "anticipation"
  ]
  node [
    id 728
    label "niech&#281;&#263;"
  ]
  node [
    id 729
    label "og&#322;oszenie"
  ]
  node [
    id 730
    label "bias"
  ]
  node [
    id 731
    label "przygotowanie"
  ]
  node [
    id 732
    label "progress"
  ]
  node [
    id 733
    label "desire"
  ]
  node [
    id 734
    label "&#322;akn&#261;&#263;"
  ]
  node [
    id 735
    label "chcie&#263;"
  ]
  node [
    id 736
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 737
    label "t&#281;skni&#263;"
  ]
  node [
    id 738
    label "lock"
  ]
  node [
    id 739
    label "absolut"
  ]
  node [
    id 740
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 741
    label "odczyta&#263;"
  ]
  node [
    id 742
    label "wywr&#243;&#380;y&#263;"
  ]
  node [
    id 743
    label "przeczyta&#263;"
  ]
  node [
    id 744
    label "odwaga"
  ]
  node [
    id 745
    label "pupa"
  ]
  node [
    id 746
    label "sztuka"
  ]
  node [
    id 747
    label "core"
  ]
  node [
    id 748
    label "piek&#322;o"
  ]
  node [
    id 749
    label "pi&#243;ro"
  ]
  node [
    id 750
    label "mind"
  ]
  node [
    id 751
    label "marrow"
  ]
  node [
    id 752
    label "byt"
  ]
  node [
    id 753
    label "mi&#281;kisz"
  ]
  node [
    id 754
    label "motor"
  ]
  node [
    id 755
    label "rdze&#324;"
  ]
  node [
    id 756
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 757
    label "sztabka"
  ]
  node [
    id 758
    label "lina"
  ]
  node [
    id 759
    label "schody"
  ]
  node [
    id 760
    label "&#380;elazko"
  ]
  node [
    id 761
    label "instrument_smyczkowy"
  ]
  node [
    id 762
    label "klocek"
  ]
  node [
    id 763
    label "uzyska&#263;"
  ]
  node [
    id 764
    label "stage"
  ]
  node [
    id 765
    label "dosta&#263;"
  ]
  node [
    id 766
    label "manipulate"
  ]
  node [
    id 767
    label "realize"
  ]
  node [
    id 768
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 769
    label "przytai&#263;"
  ]
  node [
    id 770
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 771
    label "zagorze&#263;"
  ]
  node [
    id 772
    label "dotychczasowy"
  ]
  node [
    id 773
    label "kompletnie"
  ]
  node [
    id 774
    label "zaw&#380;dy"
  ]
  node [
    id 775
    label "na_zawsze"
  ]
  node [
    id 776
    label "cz&#281;sto"
  ]
  node [
    id 777
    label "kryszta&#322;owy"
  ]
  node [
    id 778
    label "czysto"
  ]
  node [
    id 779
    label "nieskazitelnie"
  ]
  node [
    id 780
    label "pi&#281;knie"
  ]
  node [
    id 781
    label "doskonale"
  ]
  node [
    id 782
    label "w&#243;dka"
  ]
  node [
    id 783
    label "czy&#347;ciocha"
  ]
  node [
    id 784
    label "ewidentny"
  ]
  node [
    id 785
    label "bezpo&#347;redni"
  ]
  node [
    id 786
    label "otwarcie"
  ]
  node [
    id 787
    label "nieograniczony"
  ]
  node [
    id 788
    label "zdecydowany"
  ]
  node [
    id 789
    label "aktualny"
  ]
  node [
    id 790
    label "prostoduszny"
  ]
  node [
    id 791
    label "jawnie"
  ]
  node [
    id 792
    label "otworzysty"
  ]
  node [
    id 793
    label "dost&#281;pny"
  ]
  node [
    id 794
    label "publiczny"
  ]
  node [
    id 795
    label "aktywny"
  ]
  node [
    id 796
    label "kontaktowy"
  ]
  node [
    id 797
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 798
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 799
    label "r&#243;wny"
  ]
  node [
    id 800
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 801
    label "bezwzgl&#281;dny"
  ]
  node [
    id 802
    label "zupe&#322;ny"
  ]
  node [
    id 803
    label "satysfakcja"
  ]
  node [
    id 804
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 805
    label "pe&#322;no"
  ]
  node [
    id 806
    label "wype&#322;nienie"
  ]
  node [
    id 807
    label "sp&#281;dza&#263;"
  ]
  node [
    id 808
    label "poch&#322;ania&#263;"
  ]
  node [
    id 809
    label "metal"
  ]
  node [
    id 810
    label "usuwa&#263;"
  ]
  node [
    id 811
    label "lutowa&#263;"
  ]
  node [
    id 812
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 813
    label "przetrawia&#263;"
  ]
  node [
    id 814
    label "digest"
  ]
  node [
    id 815
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 816
    label "marnowa&#263;"
  ]
  node [
    id 817
    label "szczytnie"
  ]
  node [
    id 818
    label "niezmierny"
  ]
  node [
    id 819
    label "ogromnie"
  ]
  node [
    id 820
    label "rynek"
  ]
  node [
    id 821
    label "zapoznawa&#263;"
  ]
  node [
    id 822
    label "take"
  ]
  node [
    id 823
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 824
    label "wchodzi&#263;"
  ]
  node [
    id 825
    label "begin"
  ]
  node [
    id 826
    label "wprawia&#263;"
  ]
  node [
    id 827
    label "schodzi&#263;"
  ]
  node [
    id 828
    label "wpisywa&#263;"
  ]
  node [
    id 829
    label "umieszcza&#263;"
  ]
  node [
    id 830
    label "induct"
  ]
  node [
    id 831
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 832
    label "zaczyna&#263;"
  ]
  node [
    id 833
    label "doprowadza&#263;"
  ]
  node [
    id 834
    label "inflict"
  ]
  node [
    id 835
    label "konfuzja"
  ]
  node [
    id 836
    label "skonfundowanie"
  ]
  node [
    id 837
    label "confusion"
  ]
  node [
    id 838
    label "zmieszanie_si&#281;"
  ]
  node [
    id 839
    label "shame"
  ]
  node [
    id 840
    label "wzbudzenie"
  ]
  node [
    id 841
    label "system"
  ]
  node [
    id 842
    label "s&#261;d"
  ]
  node [
    id 843
    label "wytw&#243;r"
  ]
  node [
    id 844
    label "thinking"
  ]
  node [
    id 845
    label "idea"
  ]
  node [
    id 846
    label "political_orientation"
  ]
  node [
    id 847
    label "pomys&#322;"
  ]
  node [
    id 848
    label "szko&#322;a"
  ]
  node [
    id 849
    label "umys&#322;"
  ]
  node [
    id 850
    label "fantomatyka"
  ]
  node [
    id 851
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 852
    label "p&#322;&#243;d"
  ]
  node [
    id 853
    label "zostawa&#263;"
  ]
  node [
    id 854
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 855
    label "return"
  ]
  node [
    id 856
    label "godzi&#263;_si&#281;"
  ]
  node [
    id 857
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 858
    label "przybywa&#263;"
  ]
  node [
    id 859
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 860
    label "przychodzi&#263;"
  ]
  node [
    id 861
    label "tax_return"
  ]
  node [
    id 862
    label "nawi&#261;zywa&#263;"
  ]
  node [
    id 863
    label "recur"
  ]
  node [
    id 864
    label "nieprzerwanie"
  ]
  node [
    id 865
    label "ci&#261;g&#322;y"
  ]
  node [
    id 866
    label "stale"
  ]
  node [
    id 867
    label "beznadziejny"
  ]
  node [
    id 868
    label "zniszczenie"
  ]
  node [
    id 869
    label "niepowodzenie"
  ]
  node [
    id 870
    label "ubytek"
  ]
  node [
    id 871
    label "bilans"
  ]
  node [
    id 872
    label "szwank"
  ]
  node [
    id 873
    label "accommodate"
  ]
  node [
    id 874
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 875
    label "skrzywdzi&#263;"
  ]
  node [
    id 876
    label "impart"
  ]
  node [
    id 877
    label "liszy&#263;"
  ]
  node [
    id 878
    label "doprowadzi&#263;"
  ]
  node [
    id 879
    label "da&#263;"
  ]
  node [
    id 880
    label "stworzy&#263;"
  ]
  node [
    id 881
    label "permit"
  ]
  node [
    id 882
    label "przekaza&#263;"
  ]
  node [
    id 883
    label "wyda&#263;"
  ]
  node [
    id 884
    label "zerwa&#263;"
  ]
  node [
    id 885
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 886
    label "zabra&#263;"
  ]
  node [
    id 887
    label "shove"
  ]
  node [
    id 888
    label "spowodowa&#263;"
  ]
  node [
    id 889
    label "zrezygnowa&#263;"
  ]
  node [
    id 890
    label "wyznaczy&#263;"
  ]
  node [
    id 891
    label "drop"
  ]
  node [
    id 892
    label "release"
  ]
  node [
    id 893
    label "shelve"
  ]
  node [
    id 894
    label "przymus"
  ]
  node [
    id 895
    label "rzuci&#263;"
  ]
  node [
    id 896
    label "hazard"
  ]
  node [
    id 897
    label "destiny"
  ]
  node [
    id 898
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 899
    label "bilet"
  ]
  node [
    id 900
    label "przebieg_&#380;ycia"
  ]
  node [
    id 901
    label "&#380;ycie"
  ]
  node [
    id 902
    label "rzucenie"
  ]
  node [
    id 903
    label "zmi&#281;kczenie"
  ]
  node [
    id 904
    label "zmi&#281;kczanie"
  ]
  node [
    id 905
    label "uczulenie"
  ]
  node [
    id 906
    label "wra&#380;liwy"
  ]
  node [
    id 907
    label "precyzyjny"
  ]
  node [
    id 908
    label "mi&#322;y"
  ]
  node [
    id 909
    label "uczulanie"
  ]
  node [
    id 910
    label "czule"
  ]
  node [
    id 911
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 912
    label "forfeit"
  ]
  node [
    id 913
    label "wytraci&#263;"
  ]
  node [
    id 914
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 915
    label "proceed"
  ]
  node [
    id 916
    label "catch"
  ]
  node [
    id 917
    label "osta&#263;_si&#281;"
  ]
  node [
    id 918
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 919
    label "prze&#380;y&#263;"
  ]
  node [
    id 920
    label "miernota"
  ]
  node [
    id 921
    label "g&#243;wno"
  ]
  node [
    id 922
    label "love"
  ]
  node [
    id 923
    label "brak"
  ]
  node [
    id 924
    label "ciura"
  ]
  node [
    id 925
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 926
    label "obszar"
  ]
  node [
    id 927
    label "obiekt_naturalny"
  ]
  node [
    id 928
    label "Stary_&#346;wiat"
  ]
  node [
    id 929
    label "grupa"
  ]
  node [
    id 930
    label "stw&#243;r"
  ]
  node [
    id 931
    label "biosfera"
  ]
  node [
    id 932
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 933
    label "magnetosfera"
  ]
  node [
    id 934
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 935
    label "environment"
  ]
  node [
    id 936
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 937
    label "geosfera"
  ]
  node [
    id 938
    label "Nowy_&#346;wiat"
  ]
  node [
    id 939
    label "planeta"
  ]
  node [
    id 940
    label "przejmowa&#263;"
  ]
  node [
    id 941
    label "litosfera"
  ]
  node [
    id 942
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 943
    label "makrokosmos"
  ]
  node [
    id 944
    label "barysfera"
  ]
  node [
    id 945
    label "biota"
  ]
  node [
    id 946
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 947
    label "fauna"
  ]
  node [
    id 948
    label "wszechstworzenie"
  ]
  node [
    id 949
    label "geotermia"
  ]
  node [
    id 950
    label "biegun"
  ]
  node [
    id 951
    label "ekosystem"
  ]
  node [
    id 952
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 953
    label "teren"
  ]
  node [
    id 954
    label "p&#243;&#322;kula"
  ]
  node [
    id 955
    label "atmosfera"
  ]
  node [
    id 956
    label "class"
  ]
  node [
    id 957
    label "po&#322;udnie"
  ]
  node [
    id 958
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 959
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 960
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 961
    label "przejmowanie"
  ]
  node [
    id 962
    label "asymilowanie_si&#281;"
  ]
  node [
    id 963
    label "przej&#261;&#263;"
  ]
  node [
    id 964
    label "ekosfera"
  ]
  node [
    id 965
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 966
    label "ciemna_materia"
  ]
  node [
    id 967
    label "geoida"
  ]
  node [
    id 968
    label "Wsch&#243;d"
  ]
  node [
    id 969
    label "populace"
  ]
  node [
    id 970
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 971
    label "huczek"
  ]
  node [
    id 972
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 973
    label "Ziemia"
  ]
  node [
    id 974
    label "universe"
  ]
  node [
    id 975
    label "ozonosfera"
  ]
  node [
    id 976
    label "rze&#378;ba"
  ]
  node [
    id 977
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 978
    label "zagranica"
  ]
  node [
    id 979
    label "hydrosfera"
  ]
  node [
    id 980
    label "woda"
  ]
  node [
    id 981
    label "kuchnia"
  ]
  node [
    id 982
    label "przej&#281;cie"
  ]
  node [
    id 983
    label "czarna_dziura"
  ]
  node [
    id 984
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 985
    label "morze"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 13
    target 177
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 50
  ]
  edge [
    source 16
    target 76
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 49
  ]
  edge [
    source 19
    target 74
  ]
  edge [
    source 19
    target 89
  ]
  edge [
    source 19
    target 108
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 20
    target 101
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 43
  ]
  edge [
    source 21
    target 44
  ]
  edge [
    source 21
    target 51
  ]
  edge [
    source 21
    target 52
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 223
  ]
  edge [
    source 21
    target 224
  ]
  edge [
    source 21
    target 225
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 50
  ]
  edge [
    source 22
    target 226
  ]
  edge [
    source 22
    target 227
  ]
  edge [
    source 22
    target 228
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 109
  ]
  edge [
    source 22
    target 119
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 33
  ]
  edge [
    source 23
    target 230
  ]
  edge [
    source 23
    target 87
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 49
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 231
  ]
  edge [
    source 25
    target 87
  ]
  edge [
    source 25
    target 232
  ]
  edge [
    source 25
    target 233
  ]
  edge [
    source 25
    target 234
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 38
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 235
  ]
  edge [
    source 27
    target 236
  ]
  edge [
    source 27
    target 237
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 239
  ]
  edge [
    source 27
    target 240
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 241
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 252
  ]
  edge [
    source 29
    target 253
  ]
  edge [
    source 29
    target 254
  ]
  edge [
    source 29
    target 255
  ]
  edge [
    source 29
    target 256
  ]
  edge [
    source 29
    target 257
  ]
  edge [
    source 29
    target 258
  ]
  edge [
    source 29
    target 259
  ]
  edge [
    source 29
    target 260
  ]
  edge [
    source 29
    target 261
  ]
  edge [
    source 29
    target 262
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 209
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 264
  ]
  edge [
    source 30
    target 210
  ]
  edge [
    source 30
    target 265
  ]
  edge [
    source 30
    target 266
  ]
  edge [
    source 30
    target 216
  ]
  edge [
    source 30
    target 267
  ]
  edge [
    source 30
    target 268
  ]
  edge [
    source 30
    target 218
  ]
  edge [
    source 30
    target 219
  ]
  edge [
    source 30
    target 269
  ]
  edge [
    source 30
    target 270
  ]
  edge [
    source 30
    target 45
  ]
  edge [
    source 30
    target 271
  ]
  edge [
    source 30
    target 249
  ]
  edge [
    source 30
    target 272
  ]
  edge [
    source 30
    target 273
  ]
  edge [
    source 30
    target 274
  ]
  edge [
    source 30
    target 211
  ]
  edge [
    source 30
    target 245
  ]
  edge [
    source 30
    target 275
  ]
  edge [
    source 30
    target 276
  ]
  edge [
    source 30
    target 277
  ]
  edge [
    source 30
    target 58
  ]
  edge [
    source 30
    target 72
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 278
  ]
  edge [
    source 31
    target 279
  ]
  edge [
    source 31
    target 280
  ]
  edge [
    source 31
    target 281
  ]
  edge [
    source 31
    target 282
  ]
  edge [
    source 31
    target 67
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 221
  ]
  edge [
    source 32
    target 283
  ]
  edge [
    source 32
    target 284
  ]
  edge [
    source 32
    target 285
  ]
  edge [
    source 32
    target 286
  ]
  edge [
    source 32
    target 287
  ]
  edge [
    source 32
    target 288
  ]
  edge [
    source 32
    target 289
  ]
  edge [
    source 32
    target 290
  ]
  edge [
    source 32
    target 291
  ]
  edge [
    source 32
    target 93
  ]
  edge [
    source 32
    target 292
  ]
  edge [
    source 32
    target 264
  ]
  edge [
    source 32
    target 293
  ]
  edge [
    source 32
    target 294
  ]
  edge [
    source 32
    target 295
  ]
  edge [
    source 32
    target 296
  ]
  edge [
    source 32
    target 297
  ]
  edge [
    source 32
    target 298
  ]
  edge [
    source 32
    target 299
  ]
  edge [
    source 32
    target 300
  ]
  edge [
    source 32
    target 301
  ]
  edge [
    source 32
    target 302
  ]
  edge [
    source 32
    target 303
  ]
  edge [
    source 32
    target 304
  ]
  edge [
    source 32
    target 305
  ]
  edge [
    source 32
    target 306
  ]
  edge [
    source 32
    target 307
  ]
  edge [
    source 32
    target 308
  ]
  edge [
    source 32
    target 102
  ]
  edge [
    source 32
    target 309
  ]
  edge [
    source 32
    target 310
  ]
  edge [
    source 32
    target 311
  ]
  edge [
    source 32
    target 312
  ]
  edge [
    source 32
    target 313
  ]
  edge [
    source 32
    target 314
  ]
  edge [
    source 32
    target 315
  ]
  edge [
    source 32
    target 316
  ]
  edge [
    source 32
    target 317
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 32
    target 229
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 32
    target 323
  ]
  edge [
    source 32
    target 324
  ]
  edge [
    source 32
    target 325
  ]
  edge [
    source 32
    target 326
  ]
  edge [
    source 32
    target 327
  ]
  edge [
    source 32
    target 328
  ]
  edge [
    source 32
    target 329
  ]
  edge [
    source 32
    target 228
  ]
  edge [
    source 32
    target 330
  ]
  edge [
    source 32
    target 331
  ]
  edge [
    source 32
    target 332
  ]
  edge [
    source 32
    target 333
  ]
  edge [
    source 32
    target 334
  ]
  edge [
    source 32
    target 335
  ]
  edge [
    source 32
    target 336
  ]
  edge [
    source 32
    target 337
  ]
  edge [
    source 32
    target 338
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 52
  ]
  edge [
    source 33
    target 53
  ]
  edge [
    source 33
    target 339
  ]
  edge [
    source 33
    target 66
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 340
  ]
  edge [
    source 34
    target 341
  ]
  edge [
    source 34
    target 342
  ]
  edge [
    source 34
    target 343
  ]
  edge [
    source 34
    target 165
  ]
  edge [
    source 34
    target 344
  ]
  edge [
    source 34
    target 345
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 82
  ]
  edge [
    source 36
    target 83
  ]
  edge [
    source 36
    target 124
  ]
  edge [
    source 36
    target 62
  ]
  edge [
    source 36
    target 134
  ]
  edge [
    source 36
    target 135
  ]
  edge [
    source 36
    target 49
  ]
  edge [
    source 36
    target 107
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 124
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 346
  ]
  edge [
    source 38
    target 347
  ]
  edge [
    source 38
    target 348
  ]
  edge [
    source 38
    target 349
  ]
  edge [
    source 38
    target 350
  ]
  edge [
    source 38
    target 218
  ]
  edge [
    source 38
    target 351
  ]
  edge [
    source 38
    target 352
  ]
  edge [
    source 39
    target 353
  ]
  edge [
    source 39
    target 354
  ]
  edge [
    source 39
    target 66
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 355
  ]
  edge [
    source 40
    target 287
  ]
  edge [
    source 40
    target 356
  ]
  edge [
    source 40
    target 357
  ]
  edge [
    source 40
    target 358
  ]
  edge [
    source 40
    target 359
  ]
  edge [
    source 40
    target 360
  ]
  edge [
    source 40
    target 361
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 362
  ]
  edge [
    source 42
    target 363
  ]
  edge [
    source 42
    target 364
  ]
  edge [
    source 42
    target 365
  ]
  edge [
    source 42
    target 366
  ]
  edge [
    source 42
    target 367
  ]
  edge [
    source 42
    target 368
  ]
  edge [
    source 42
    target 369
  ]
  edge [
    source 42
    target 370
  ]
  edge [
    source 42
    target 352
  ]
  edge [
    source 42
    target 371
  ]
  edge [
    source 42
    target 372
  ]
  edge [
    source 43
    target 373
  ]
  edge [
    source 43
    target 374
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 375
  ]
  edge [
    source 44
    target 376
  ]
  edge [
    source 44
    target 377
  ]
  edge [
    source 44
    target 378
  ]
  edge [
    source 44
    target 379
  ]
  edge [
    source 44
    target 380
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 381
  ]
  edge [
    source 45
    target 382
  ]
  edge [
    source 45
    target 383
  ]
  edge [
    source 45
    target 384
  ]
  edge [
    source 45
    target 385
  ]
  edge [
    source 45
    target 386
  ]
  edge [
    source 45
    target 387
  ]
  edge [
    source 45
    target 388
  ]
  edge [
    source 45
    target 93
  ]
  edge [
    source 45
    target 389
  ]
  edge [
    source 45
    target 390
  ]
  edge [
    source 45
    target 391
  ]
  edge [
    source 45
    target 392
  ]
  edge [
    source 45
    target 393
  ]
  edge [
    source 45
    target 83
  ]
  edge [
    source 45
    target 394
  ]
  edge [
    source 45
    target 395
  ]
  edge [
    source 45
    target 396
  ]
  edge [
    source 45
    target 397
  ]
  edge [
    source 45
    target 398
  ]
  edge [
    source 45
    target 399
  ]
  edge [
    source 45
    target 400
  ]
  edge [
    source 45
    target 401
  ]
  edge [
    source 45
    target 402
  ]
  edge [
    source 45
    target 403
  ]
  edge [
    source 45
    target 404
  ]
  edge [
    source 45
    target 405
  ]
  edge [
    source 45
    target 406
  ]
  edge [
    source 45
    target 407
  ]
  edge [
    source 45
    target 408
  ]
  edge [
    source 45
    target 409
  ]
  edge [
    source 45
    target 410
  ]
  edge [
    source 45
    target 411
  ]
  edge [
    source 45
    target 412
  ]
  edge [
    source 45
    target 413
  ]
  edge [
    source 45
    target 414
  ]
  edge [
    source 45
    target 415
  ]
  edge [
    source 45
    target 416
  ]
  edge [
    source 45
    target 417
  ]
  edge [
    source 45
    target 418
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 419
  ]
  edge [
    source 46
    target 420
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 421
  ]
  edge [
    source 47
    target 422
  ]
  edge [
    source 47
    target 423
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 424
  ]
  edge [
    source 48
    target 229
  ]
  edge [
    source 48
    target 425
  ]
  edge [
    source 48
    target 426
  ]
  edge [
    source 48
    target 115
  ]
  edge [
    source 49
    target 79
  ]
  edge [
    source 49
    target 80
  ]
  edge [
    source 49
    target 427
  ]
  edge [
    source 49
    target 428
  ]
  edge [
    source 49
    target 429
  ]
  edge [
    source 49
    target 430
  ]
  edge [
    source 49
    target 431
  ]
  edge [
    source 49
    target 432
  ]
  edge [
    source 49
    target 107
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 433
  ]
  edge [
    source 50
    target 202
  ]
  edge [
    source 50
    target 434
  ]
  edge [
    source 50
    target 435
  ]
  edge [
    source 50
    target 436
  ]
  edge [
    source 50
    target 437
  ]
  edge [
    source 50
    target 438
  ]
  edge [
    source 50
    target 76
  ]
  edge [
    source 51
    target 117
  ]
  edge [
    source 51
    target 100
  ]
  edge [
    source 51
    target 439
  ]
  edge [
    source 51
    target 440
  ]
  edge [
    source 51
    target 441
  ]
  edge [
    source 51
    target 442
  ]
  edge [
    source 51
    target 443
  ]
  edge [
    source 51
    target 444
  ]
  edge [
    source 51
    target 445
  ]
  edge [
    source 51
    target 446
  ]
  edge [
    source 52
    target 447
  ]
  edge [
    source 52
    target 448
  ]
  edge [
    source 52
    target 449
  ]
  edge [
    source 52
    target 450
  ]
  edge [
    source 52
    target 451
  ]
  edge [
    source 52
    target 229
  ]
  edge [
    source 52
    target 452
  ]
  edge [
    source 52
    target 453
  ]
  edge [
    source 52
    target 454
  ]
  edge [
    source 52
    target 425
  ]
  edge [
    source 52
    target 455
  ]
  edge [
    source 52
    target 456
  ]
  edge [
    source 52
    target 457
  ]
  edge [
    source 52
    target 458
  ]
  edge [
    source 52
    target 459
  ]
  edge [
    source 52
    target 165
  ]
  edge [
    source 52
    target 460
  ]
  edge [
    source 52
    target 133
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 461
  ]
  edge [
    source 53
    target 462
  ]
  edge [
    source 53
    target 463
  ]
  edge [
    source 53
    target 464
  ]
  edge [
    source 53
    target 465
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 54
    target 59
  ]
  edge [
    source 54
    target 466
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 467
  ]
  edge [
    source 55
    target 468
  ]
  edge [
    source 55
    target 469
  ]
  edge [
    source 55
    target 470
  ]
  edge [
    source 55
    target 471
  ]
  edge [
    source 55
    target 472
  ]
  edge [
    source 55
    target 473
  ]
  edge [
    source 55
    target 474
  ]
  edge [
    source 55
    target 475
  ]
  edge [
    source 55
    target 476
  ]
  edge [
    source 55
    target 477
  ]
  edge [
    source 55
    target 478
  ]
  edge [
    source 55
    target 106
  ]
  edge [
    source 55
    target 130
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 79
  ]
  edge [
    source 56
    target 113
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 76
  ]
  edge [
    source 57
    target 89
  ]
  edge [
    source 57
    target 90
  ]
  edge [
    source 57
    target 107
  ]
  edge [
    source 57
    target 108
  ]
  edge [
    source 57
    target 479
  ]
  edge [
    source 57
    target 221
  ]
  edge [
    source 57
    target 480
  ]
  edge [
    source 57
    target 481
  ]
  edge [
    source 57
    target 482
  ]
  edge [
    source 57
    target 483
  ]
  edge [
    source 57
    target 484
  ]
  edge [
    source 57
    target 485
  ]
  edge [
    source 57
    target 486
  ]
  edge [
    source 57
    target 487
  ]
  edge [
    source 58
    target 234
  ]
  edge [
    source 58
    target 72
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 488
  ]
  edge [
    source 59
    target 489
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 90
  ]
  edge [
    source 60
    target 91
  ]
  edge [
    source 60
    target 490
  ]
  edge [
    source 60
    target 491
  ]
  edge [
    source 60
    target 492
  ]
  edge [
    source 60
    target 493
  ]
  edge [
    source 60
    target 494
  ]
  edge [
    source 60
    target 495
  ]
  edge [
    source 60
    target 117
  ]
  edge [
    source 60
    target 496
  ]
  edge [
    source 60
    target 497
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 498
  ]
  edge [
    source 61
    target 499
  ]
  edge [
    source 61
    target 500
  ]
  edge [
    source 61
    target 501
  ]
  edge [
    source 61
    target 502
  ]
  edge [
    source 61
    target 503
  ]
  edge [
    source 61
    target 504
  ]
  edge [
    source 61
    target 505
  ]
  edge [
    source 61
    target 506
  ]
  edge [
    source 61
    target 507
  ]
  edge [
    source 61
    target 508
  ]
  edge [
    source 61
    target 509
  ]
  edge [
    source 61
    target 510
  ]
  edge [
    source 61
    target 511
  ]
  edge [
    source 61
    target 512
  ]
  edge [
    source 61
    target 513
  ]
  edge [
    source 61
    target 514
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 83
  ]
  edge [
    source 62
    target 126
  ]
  edge [
    source 62
    target 127
  ]
  edge [
    source 62
    target 129
  ]
  edge [
    source 63
    target 515
  ]
  edge [
    source 63
    target 516
  ]
  edge [
    source 63
    target 517
  ]
  edge [
    source 63
    target 518
  ]
  edge [
    source 64
    target 519
  ]
  edge [
    source 64
    target 520
  ]
  edge [
    source 64
    target 521
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 522
  ]
  edge [
    source 65
    target 112
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 523
  ]
  edge [
    source 66
    target 524
  ]
  edge [
    source 66
    target 247
  ]
  edge [
    source 66
    target 525
  ]
  edge [
    source 66
    target 279
  ]
  edge [
    source 66
    target 526
  ]
  edge [
    source 66
    target 527
  ]
  edge [
    source 66
    target 528
  ]
  edge [
    source 66
    target 529
  ]
  edge [
    source 66
    target 281
  ]
  edge [
    source 66
    target 530
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 531
  ]
  edge [
    source 67
    target 532
  ]
  edge [
    source 67
    target 533
  ]
  edge [
    source 67
    target 91
  ]
  edge [
    source 67
    target 534
  ]
  edge [
    source 67
    target 290
  ]
  edge [
    source 67
    target 535
  ]
  edge [
    source 67
    target 536
  ]
  edge [
    source 67
    target 537
  ]
  edge [
    source 67
    target 538
  ]
  edge [
    source 67
    target 539
  ]
  edge [
    source 67
    target 540
  ]
  edge [
    source 67
    target 541
  ]
  edge [
    source 67
    target 229
  ]
  edge [
    source 67
    target 542
  ]
  edge [
    source 67
    target 543
  ]
  edge [
    source 67
    target 544
  ]
  edge [
    source 67
    target 545
  ]
  edge [
    source 67
    target 546
  ]
  edge [
    source 67
    target 547
  ]
  edge [
    source 67
    target 548
  ]
  edge [
    source 67
    target 549
  ]
  edge [
    source 67
    target 550
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 88
  ]
  edge [
    source 68
    target 79
  ]
  edge [
    source 68
    target 97
  ]
  edge [
    source 68
    target 98
  ]
  edge [
    source 68
    target 551
  ]
  edge [
    source 68
    target 552
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 553
  ]
  edge [
    source 69
    target 365
  ]
  edge [
    source 69
    target 554
  ]
  edge [
    source 69
    target 555
  ]
  edge [
    source 69
    target 556
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 557
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 558
  ]
  edge [
    source 71
    target 559
  ]
  edge [
    source 71
    target 560
  ]
  edge [
    source 71
    target 561
  ]
  edge [
    source 71
    target 562
  ]
  edge [
    source 71
    target 563
  ]
  edge [
    source 71
    target 564
  ]
  edge [
    source 71
    target 565
  ]
  edge [
    source 71
    target 566
  ]
  edge [
    source 71
    target 567
  ]
  edge [
    source 71
    target 568
  ]
  edge [
    source 71
    target 569
  ]
  edge [
    source 71
    target 570
  ]
  edge [
    source 71
    target 571
  ]
  edge [
    source 71
    target 572
  ]
  edge [
    source 71
    target 573
  ]
  edge [
    source 72
    target 574
  ]
  edge [
    source 72
    target 575
  ]
  edge [
    source 72
    target 576
  ]
  edge [
    source 72
    target 577
  ]
  edge [
    source 72
    target 578
  ]
  edge [
    source 72
    target 579
  ]
  edge [
    source 72
    target 580
  ]
  edge [
    source 72
    target 581
  ]
  edge [
    source 73
    target 582
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 89
  ]
  edge [
    source 74
    target 108
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 162
  ]
  edge [
    source 75
    target 583
  ]
  edge [
    source 75
    target 93
  ]
  edge [
    source 75
    target 82
  ]
  edge [
    source 76
    target 110
  ]
  edge [
    source 76
    target 111
  ]
  edge [
    source 76
    target 191
  ]
  edge [
    source 76
    target 473
  ]
  edge [
    source 76
    target 584
  ]
  edge [
    source 76
    target 475
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 176
  ]
  edge [
    source 77
    target 177
  ]
  edge [
    source 77
    target 178
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 585
  ]
  edge [
    source 78
    target 586
  ]
  edge [
    source 78
    target 587
  ]
  edge [
    source 78
    target 588
  ]
  edge [
    source 78
    target 589
  ]
  edge [
    source 78
    target 590
  ]
  edge [
    source 78
    target 591
  ]
  edge [
    source 78
    target 592
  ]
  edge [
    source 78
    target 593
  ]
  edge [
    source 78
    target 594
  ]
  edge [
    source 78
    target 595
  ]
  edge [
    source 78
    target 596
  ]
  edge [
    source 78
    target 597
  ]
  edge [
    source 78
    target 598
  ]
  edge [
    source 78
    target 599
  ]
  edge [
    source 78
    target 600
  ]
  edge [
    source 79
    target 89
  ]
  edge [
    source 79
    target 112
  ]
  edge [
    source 79
    target 601
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 602
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 603
  ]
  edge [
    source 81
    target 604
  ]
  edge [
    source 81
    target 605
  ]
  edge [
    source 81
    target 606
  ]
  edge [
    source 81
    target 607
  ]
  edge [
    source 81
    target 608
  ]
  edge [
    source 81
    target 609
  ]
  edge [
    source 81
    target 610
  ]
  edge [
    source 81
    target 611
  ]
  edge [
    source 81
    target 612
  ]
  edge [
    source 81
    target 613
  ]
  edge [
    source 81
    target 614
  ]
  edge [
    source 82
    target 615
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 125
  ]
  edge [
    source 83
    target 616
  ]
  edge [
    source 83
    target 617
  ]
  edge [
    source 83
    target 615
  ]
  edge [
    source 83
    target 553
  ]
  edge [
    source 83
    target 618
  ]
  edge [
    source 83
    target 619
  ]
  edge [
    source 83
    target 620
  ]
  edge [
    source 83
    target 621
  ]
  edge [
    source 83
    target 622
  ]
  edge [
    source 83
    target 623
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 126
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 624
  ]
  edge [
    source 86
    target 625
  ]
  edge [
    source 86
    target 626
  ]
  edge [
    source 86
    target 154
  ]
  edge [
    source 86
    target 627
  ]
  edge [
    source 86
    target 628
  ]
  edge [
    source 87
    target 629
  ]
  edge [
    source 87
    target 630
  ]
  edge [
    source 87
    target 631
  ]
  edge [
    source 87
    target 632
  ]
  edge [
    source 87
    target 633
  ]
  edge [
    source 87
    target 634
  ]
  edge [
    source 87
    target 635
  ]
  edge [
    source 87
    target 636
  ]
  edge [
    source 87
    target 637
  ]
  edge [
    source 87
    target 638
  ]
  edge [
    source 87
    target 639
  ]
  edge [
    source 87
    target 640
  ]
  edge [
    source 87
    target 641
  ]
  edge [
    source 87
    target 642
  ]
  edge [
    source 87
    target 643
  ]
  edge [
    source 87
    target 644
  ]
  edge [
    source 87
    target 645
  ]
  edge [
    source 87
    target 646
  ]
  edge [
    source 87
    target 647
  ]
  edge [
    source 87
    target 648
  ]
  edge [
    source 87
    target 649
  ]
  edge [
    source 87
    target 650
  ]
  edge [
    source 87
    target 233
  ]
  edge [
    source 87
    target 651
  ]
  edge [
    source 87
    target 652
  ]
  edge [
    source 88
    target 653
  ]
  edge [
    source 88
    target 654
  ]
  edge [
    source 88
    target 655
  ]
  edge [
    source 88
    target 509
  ]
  edge [
    source 89
    target 656
  ]
  edge [
    source 89
    target 657
  ]
  edge [
    source 89
    target 658
  ]
  edge [
    source 89
    target 659
  ]
  edge [
    source 89
    target 660
  ]
  edge [
    source 89
    target 661
  ]
  edge [
    source 89
    target 108
  ]
  edge [
    source 90
    target 662
  ]
  edge [
    source 90
    target 663
  ]
  edge [
    source 90
    target 664
  ]
  edge [
    source 90
    target 665
  ]
  edge [
    source 90
    target 666
  ]
  edge [
    source 90
    target 667
  ]
  edge [
    source 90
    target 668
  ]
  edge [
    source 90
    target 528
  ]
  edge [
    source 90
    target 669
  ]
  edge [
    source 90
    target 670
  ]
  edge [
    source 90
    target 671
  ]
  edge [
    source 90
    target 672
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 673
  ]
  edge [
    source 91
    target 547
  ]
  edge [
    source 91
    target 263
  ]
  edge [
    source 91
    target 674
  ]
  edge [
    source 91
    target 675
  ]
  edge [
    source 91
    target 266
  ]
  edge [
    source 91
    target 676
  ]
  edge [
    source 91
    target 677
  ]
  edge [
    source 91
    target 678
  ]
  edge [
    source 91
    target 679
  ]
  edge [
    source 91
    target 680
  ]
  edge [
    source 91
    target 140
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 681
  ]
  edge [
    source 92
    target 682
  ]
  edge [
    source 92
    target 683
  ]
  edge [
    source 92
    target 684
  ]
  edge [
    source 92
    target 685
  ]
  edge [
    source 92
    target 686
  ]
  edge [
    source 92
    target 687
  ]
  edge [
    source 92
    target 688
  ]
  edge [
    source 92
    target 689
  ]
  edge [
    source 92
    target 690
  ]
  edge [
    source 92
    target 691
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 692
  ]
  edge [
    source 93
    target 693
  ]
  edge [
    source 93
    target 694
  ]
  edge [
    source 93
    target 695
  ]
  edge [
    source 93
    target 696
  ]
  edge [
    source 93
    target 697
  ]
  edge [
    source 93
    target 698
  ]
  edge [
    source 93
    target 699
  ]
  edge [
    source 93
    target 700
  ]
  edge [
    source 93
    target 701
  ]
  edge [
    source 93
    target 702
  ]
  edge [
    source 93
    target 447
  ]
  edge [
    source 93
    target 703
  ]
  edge [
    source 93
    target 704
  ]
  edge [
    source 93
    target 705
  ]
  edge [
    source 93
    target 706
  ]
  edge [
    source 93
    target 546
  ]
  edge [
    source 93
    target 707
  ]
  edge [
    source 93
    target 408
  ]
  edge [
    source 93
    target 673
  ]
  edge [
    source 93
    target 708
  ]
  edge [
    source 93
    target 709
  ]
  edge [
    source 93
    target 710
  ]
  edge [
    source 93
    target 711
  ]
  edge [
    source 93
    target 121
  ]
  edge [
    source 93
    target 132
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 712
  ]
  edge [
    source 94
    target 713
  ]
  edge [
    source 94
    target 475
  ]
  edge [
    source 94
    target 714
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 715
  ]
  edge [
    source 95
    target 716
  ]
  edge [
    source 95
    target 717
  ]
  edge [
    source 95
    target 718
  ]
  edge [
    source 95
    target 719
  ]
  edge [
    source 95
    target 720
  ]
  edge [
    source 95
    target 721
  ]
  edge [
    source 95
    target 722
  ]
  edge [
    source 95
    target 723
  ]
  edge [
    source 95
    target 724
  ]
  edge [
    source 95
    target 725
  ]
  edge [
    source 95
    target 726
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 439
  ]
  edge [
    source 96
    target 589
  ]
  edge [
    source 96
    target 727
  ]
  edge [
    source 96
    target 728
  ]
  edge [
    source 96
    target 729
  ]
  edge [
    source 96
    target 730
  ]
  edge [
    source 96
    target 731
  ]
  edge [
    source 96
    target 732
  ]
  edge [
    source 96
    target 131
  ]
  edge [
    source 97
    target 557
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 733
  ]
  edge [
    source 98
    target 734
  ]
  edge [
    source 98
    target 735
  ]
  edge [
    source 98
    target 365
  ]
  edge [
    source 98
    target 736
  ]
  edge [
    source 98
    target 737
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 686
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 118
  ]
  edge [
    source 100
    target 738
  ]
  edge [
    source 100
    target 739
  ]
  edge [
    source 100
    target 740
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 741
  ]
  edge [
    source 101
    target 742
  ]
  edge [
    source 101
    target 743
  ]
  edge [
    source 102
    target 221
  ]
  edge [
    source 102
    target 285
  ]
  edge [
    source 102
    target 286
  ]
  edge [
    source 102
    target 288
  ]
  edge [
    source 102
    target 744
  ]
  edge [
    source 102
    target 745
  ]
  edge [
    source 102
    target 746
  ]
  edge [
    source 102
    target 747
  ]
  edge [
    source 102
    target 748
  ]
  edge [
    source 102
    target 749
  ]
  edge [
    source 102
    target 398
  ]
  edge [
    source 102
    target 306
  ]
  edge [
    source 102
    target 312
  ]
  edge [
    source 102
    target 313
  ]
  edge [
    source 102
    target 750
  ]
  edge [
    source 102
    target 751
  ]
  edge [
    source 102
    target 314
  ]
  edge [
    source 102
    target 752
  ]
  edge [
    source 102
    target 702
  ]
  edge [
    source 102
    target 322
  ]
  edge [
    source 102
    target 753
  ]
  edge [
    source 102
    target 320
  ]
  edge [
    source 102
    target 754
  ]
  edge [
    source 102
    target 325
  ]
  edge [
    source 102
    target 755
  ]
  edge [
    source 102
    target 756
  ]
  edge [
    source 102
    target 757
  ]
  edge [
    source 102
    target 758
  ]
  edge [
    source 102
    target 333
  ]
  edge [
    source 102
    target 759
  ]
  edge [
    source 102
    target 335
  ]
  edge [
    source 102
    target 336
  ]
  edge [
    source 102
    target 760
  ]
  edge [
    source 102
    target 761
  ]
  edge [
    source 102
    target 762
  ]
  edge [
    source 103
    target 763
  ]
  edge [
    source 103
    target 764
  ]
  edge [
    source 103
    target 765
  ]
  edge [
    source 103
    target 766
  ]
  edge [
    source 103
    target 767
  ]
  edge [
    source 103
    target 768
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 769
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 770
  ]
  edge [
    source 109
    target 346
  ]
  edge [
    source 109
    target 771
  ]
  edge [
    source 109
    target 119
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 772
  ]
  edge [
    source 111
    target 123
  ]
  edge [
    source 112
    target 773
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 774
  ]
  edge [
    source 113
    target 124
  ]
  edge [
    source 113
    target 775
  ]
  edge [
    source 113
    target 776
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 114
    target 777
  ]
  edge [
    source 114
    target 778
  ]
  edge [
    source 114
    target 779
  ]
  edge [
    source 114
    target 780
  ]
  edge [
    source 114
    target 781
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 115
    target 782
  ]
  edge [
    source 115
    target 783
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 784
  ]
  edge [
    source 116
    target 785
  ]
  edge [
    source 116
    target 786
  ]
  edge [
    source 116
    target 787
  ]
  edge [
    source 116
    target 788
  ]
  edge [
    source 116
    target 180
  ]
  edge [
    source 116
    target 789
  ]
  edge [
    source 116
    target 790
  ]
  edge [
    source 116
    target 791
  ]
  edge [
    source 116
    target 792
  ]
  edge [
    source 116
    target 793
  ]
  edge [
    source 116
    target 794
  ]
  edge [
    source 116
    target 795
  ]
  edge [
    source 116
    target 796
  ]
  edge [
    source 117
    target 797
  ]
  edge [
    source 117
    target 787
  ]
  edge [
    source 117
    target 798
  ]
  edge [
    source 117
    target 492
  ]
  edge [
    source 117
    target 740
  ]
  edge [
    source 117
    target 799
  ]
  edge [
    source 117
    target 800
  ]
  edge [
    source 117
    target 801
  ]
  edge [
    source 117
    target 802
  ]
  edge [
    source 117
    target 803
  ]
  edge [
    source 117
    target 804
  ]
  edge [
    source 117
    target 805
  ]
  edge [
    source 117
    target 806
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 807
  ]
  edge [
    source 118
    target 162
  ]
  edge [
    source 118
    target 808
  ]
  edge [
    source 118
    target 809
  ]
  edge [
    source 118
    target 810
  ]
  edge [
    source 118
    target 811
  ]
  edge [
    source 118
    target 812
  ]
  edge [
    source 118
    target 813
  ]
  edge [
    source 118
    target 814
  ]
  edge [
    source 118
    target 815
  ]
  edge [
    source 118
    target 816
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 817
  ]
  edge [
    source 119
    target 818
  ]
  edge [
    source 119
    target 819
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 820
  ]
  edge [
    source 120
    target 821
  ]
  edge [
    source 120
    target 822
  ]
  edge [
    source 120
    target 823
  ]
  edge [
    source 120
    target 824
  ]
  edge [
    source 120
    target 825
  ]
  edge [
    source 120
    target 826
  ]
  edge [
    source 120
    target 827
  ]
  edge [
    source 120
    target 828
  ]
  edge [
    source 120
    target 528
  ]
  edge [
    source 120
    target 829
  ]
  edge [
    source 120
    target 830
  ]
  edge [
    source 120
    target 831
  ]
  edge [
    source 120
    target 832
  ]
  edge [
    source 120
    target 833
  ]
  edge [
    source 120
    target 834
  ]
  edge [
    source 120
    target 530
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 835
  ]
  edge [
    source 121
    target 836
  ]
  edge [
    source 121
    target 245
  ]
  edge [
    source 121
    target 837
  ]
  edge [
    source 121
    target 838
  ]
  edge [
    source 121
    target 839
  ]
  edge [
    source 121
    target 840
  ]
  edge [
    source 121
    target 132
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 841
  ]
  edge [
    source 122
    target 842
  ]
  edge [
    source 122
    target 843
  ]
  edge [
    source 122
    target 547
  ]
  edge [
    source 122
    target 844
  ]
  edge [
    source 122
    target 845
  ]
  edge [
    source 122
    target 846
  ]
  edge [
    source 122
    target 847
  ]
  edge [
    source 122
    target 848
  ]
  edge [
    source 122
    target 849
  ]
  edge [
    source 122
    target 850
  ]
  edge [
    source 122
    target 851
  ]
  edge [
    source 122
    target 852
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 853
  ]
  edge [
    source 123
    target 854
  ]
  edge [
    source 123
    target 855
  ]
  edge [
    source 123
    target 856
  ]
  edge [
    source 123
    target 857
  ]
  edge [
    source 123
    target 858
  ]
  edge [
    source 123
    target 859
  ]
  edge [
    source 123
    target 860
  ]
  edge [
    source 123
    target 832
  ]
  edge [
    source 123
    target 861
  ]
  edge [
    source 123
    target 862
  ]
  edge [
    source 123
    target 863
  ]
  edge [
    source 123
    target 530
  ]
  edge [
    source 124
    target 864
  ]
  edge [
    source 124
    target 865
  ]
  edge [
    source 124
    target 866
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 126
    target 867
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 868
  ]
  edge [
    source 127
    target 869
  ]
  edge [
    source 127
    target 870
  ]
  edge [
    source 127
    target 871
  ]
  edge [
    source 127
    target 872
  ]
  edge [
    source 128
    target 873
  ]
  edge [
    source 128
    target 874
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 130
    target 875
  ]
  edge [
    source 130
    target 876
  ]
  edge [
    source 130
    target 877
  ]
  edge [
    source 130
    target 874
  ]
  edge [
    source 130
    target 878
  ]
  edge [
    source 130
    target 879
  ]
  edge [
    source 130
    target 880
  ]
  edge [
    source 130
    target 881
  ]
  edge [
    source 130
    target 882
  ]
  edge [
    source 130
    target 883
  ]
  edge [
    source 130
    target 770
  ]
  edge [
    source 130
    target 475
  ]
  edge [
    source 130
    target 884
  ]
  edge [
    source 130
    target 885
  ]
  edge [
    source 130
    target 183
  ]
  edge [
    source 130
    target 886
  ]
  edge [
    source 130
    target 887
  ]
  edge [
    source 130
    target 888
  ]
  edge [
    source 130
    target 889
  ]
  edge [
    source 130
    target 890
  ]
  edge [
    source 130
    target 891
  ]
  edge [
    source 130
    target 892
  ]
  edge [
    source 130
    target 893
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 133
    target 894
  ]
  edge [
    source 133
    target 895
  ]
  edge [
    source 133
    target 896
  ]
  edge [
    source 133
    target 897
  ]
  edge [
    source 133
    target 898
  ]
  edge [
    source 133
    target 899
  ]
  edge [
    source 133
    target 900
  ]
  edge [
    source 133
    target 901
  ]
  edge [
    source 133
    target 902
  ]
  edge [
    source 134
    target 903
  ]
  edge [
    source 134
    target 904
  ]
  edge [
    source 134
    target 905
  ]
  edge [
    source 134
    target 906
  ]
  edge [
    source 134
    target 907
  ]
  edge [
    source 134
    target 908
  ]
  edge [
    source 134
    target 909
  ]
  edge [
    source 134
    target 910
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 136
    target 770
  ]
  edge [
    source 136
    target 911
  ]
  edge [
    source 136
    target 912
  ]
  edge [
    source 136
    target 913
  ]
  edge [
    source 137
    target 138
  ]
  edge [
    source 137
    target 914
  ]
  edge [
    source 137
    target 915
  ]
  edge [
    source 137
    target 916
  ]
  edge [
    source 137
    target 917
  ]
  edge [
    source 137
    target 556
  ]
  edge [
    source 137
    target 918
  ]
  edge [
    source 137
    target 919
  ]
  edge [
    source 137
    target 885
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 139
    target 920
  ]
  edge [
    source 139
    target 921
  ]
  edge [
    source 139
    target 922
  ]
  edge [
    source 139
    target 400
  ]
  edge [
    source 139
    target 923
  ]
  edge [
    source 139
    target 924
  ]
  edge [
    source 140
    target 925
  ]
  edge [
    source 140
    target 926
  ]
  edge [
    source 140
    target 927
  ]
  edge [
    source 140
    target 675
  ]
  edge [
    source 140
    target 928
  ]
  edge [
    source 140
    target 929
  ]
  edge [
    source 140
    target 930
  ]
  edge [
    source 140
    target 931
  ]
  edge [
    source 140
    target 932
  ]
  edge [
    source 140
    target 933
  ]
  edge [
    source 140
    target 934
  ]
  edge [
    source 140
    target 935
  ]
  edge [
    source 140
    target 936
  ]
  edge [
    source 140
    target 937
  ]
  edge [
    source 140
    target 938
  ]
  edge [
    source 140
    target 939
  ]
  edge [
    source 140
    target 940
  ]
  edge [
    source 140
    target 941
  ]
  edge [
    source 140
    target 942
  ]
  edge [
    source 140
    target 943
  ]
  edge [
    source 140
    target 944
  ]
  edge [
    source 140
    target 945
  ]
  edge [
    source 140
    target 163
  ]
  edge [
    source 140
    target 946
  ]
  edge [
    source 140
    target 947
  ]
  edge [
    source 140
    target 948
  ]
  edge [
    source 140
    target 949
  ]
  edge [
    source 140
    target 950
  ]
  edge [
    source 140
    target 740
  ]
  edge [
    source 140
    target 951
  ]
  edge [
    source 140
    target 952
  ]
  edge [
    source 140
    target 953
  ]
  edge [
    source 140
    target 165
  ]
  edge [
    source 140
    target 954
  ]
  edge [
    source 140
    target 955
  ]
  edge [
    source 140
    target 314
  ]
  edge [
    source 140
    target 956
  ]
  edge [
    source 140
    target 957
  ]
  edge [
    source 140
    target 958
  ]
  edge [
    source 140
    target 959
  ]
  edge [
    source 140
    target 960
  ]
  edge [
    source 140
    target 961
  ]
  edge [
    source 140
    target 702
  ]
  edge [
    source 140
    target 962
  ]
  edge [
    source 140
    target 963
  ]
  edge [
    source 140
    target 964
  ]
  edge [
    source 140
    target 677
  ]
  edge [
    source 140
    target 965
  ]
  edge [
    source 140
    target 966
  ]
  edge [
    source 140
    target 967
  ]
  edge [
    source 140
    target 968
  ]
  edge [
    source 140
    target 969
  ]
  edge [
    source 140
    target 970
  ]
  edge [
    source 140
    target 971
  ]
  edge [
    source 140
    target 972
  ]
  edge [
    source 140
    target 973
  ]
  edge [
    source 140
    target 974
  ]
  edge [
    source 140
    target 975
  ]
  edge [
    source 140
    target 976
  ]
  edge [
    source 140
    target 977
  ]
  edge [
    source 140
    target 978
  ]
  edge [
    source 140
    target 979
  ]
  edge [
    source 140
    target 980
  ]
  edge [
    source 140
    target 981
  ]
  edge [
    source 140
    target 982
  ]
  edge [
    source 140
    target 983
  ]
  edge [
    source 140
    target 984
  ]
  edge [
    source 140
    target 985
  ]
]
