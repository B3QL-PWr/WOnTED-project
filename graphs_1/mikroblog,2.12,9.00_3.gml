graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.2839506172839505
  density 0.014186028678782303
  graphCliqueNumber 7
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ten"
    origin "text"
  ]
  node [
    id 3
    label "historia"
    origin "text"
  ]
  node [
    id 4
    label "przeczyta&#263;"
    origin "text"
  ]
  node [
    id 5
    label "tutaj"
    origin "text"
  ]
  node [
    id 6
    label "godzina"
  ]
  node [
    id 7
    label "whole"
  ]
  node [
    id 8
    label "Rzym_Zachodni"
  ]
  node [
    id 9
    label "element"
  ]
  node [
    id 10
    label "ilo&#347;&#263;"
  ]
  node [
    id 11
    label "urz&#261;dzenie"
  ]
  node [
    id 12
    label "Rzym_Wschodni"
  ]
  node [
    id 13
    label "okre&#347;lony"
  ]
  node [
    id 14
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 15
    label "report"
  ]
  node [
    id 16
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 17
    label "wypowied&#378;"
  ]
  node [
    id 18
    label "neografia"
  ]
  node [
    id 19
    label "przedmiot"
  ]
  node [
    id 20
    label "papirologia"
  ]
  node [
    id 21
    label "historia_gospodarcza"
  ]
  node [
    id 22
    label "przebiec"
  ]
  node [
    id 23
    label "hista"
  ]
  node [
    id 24
    label "nauka_humanistyczna"
  ]
  node [
    id 25
    label "filigranistyka"
  ]
  node [
    id 26
    label "dyplomatyka"
  ]
  node [
    id 27
    label "annalistyka"
  ]
  node [
    id 28
    label "historyka"
  ]
  node [
    id 29
    label "heraldyka"
  ]
  node [
    id 30
    label "fabu&#322;a"
  ]
  node [
    id 31
    label "muzealnictwo"
  ]
  node [
    id 32
    label "varsavianistyka"
  ]
  node [
    id 33
    label "prezentyzm"
  ]
  node [
    id 34
    label "mediewistyka"
  ]
  node [
    id 35
    label "przebiegni&#281;cie"
  ]
  node [
    id 36
    label "charakter"
  ]
  node [
    id 37
    label "paleografia"
  ]
  node [
    id 38
    label "genealogia"
  ]
  node [
    id 39
    label "czynno&#347;&#263;"
  ]
  node [
    id 40
    label "prozopografia"
  ]
  node [
    id 41
    label "motyw"
  ]
  node [
    id 42
    label "nautologia"
  ]
  node [
    id 43
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 44
    label "epoka"
  ]
  node [
    id 45
    label "numizmatyka"
  ]
  node [
    id 46
    label "ruralistyka"
  ]
  node [
    id 47
    label "epigrafika"
  ]
  node [
    id 48
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 49
    label "historiografia"
  ]
  node [
    id 50
    label "bizantynistyka"
  ]
  node [
    id 51
    label "weksylologia"
  ]
  node [
    id 52
    label "kierunek"
  ]
  node [
    id 53
    label "ikonografia"
  ]
  node [
    id 54
    label "chronologia"
  ]
  node [
    id 55
    label "archiwistyka"
  ]
  node [
    id 56
    label "sfragistyka"
  ]
  node [
    id 57
    label "zabytkoznawstwo"
  ]
  node [
    id 58
    label "historia_sztuki"
  ]
  node [
    id 59
    label "pozna&#263;"
  ]
  node [
    id 60
    label "wywr&#243;&#380;y&#263;"
  ]
  node [
    id 61
    label "przetworzy&#263;"
  ]
  node [
    id 62
    label "read"
  ]
  node [
    id 63
    label "zaobserwowa&#263;"
  ]
  node [
    id 64
    label "odczyta&#263;"
  ]
  node [
    id 65
    label "tam"
  ]
  node [
    id 66
    label "2"
  ]
  node [
    id 67
    label "J&#243;zef"
  ]
  node [
    id 68
    label "k"
  ]
  node [
    id 69
    label "sp&#243;&#322;dzielnia"
  ]
  node [
    id 70
    label "k&#243;&#322;ko"
  ]
  node [
    id 71
    label "rolniczy"
  ]
  node [
    id 72
    label "Star"
  ]
  node [
    id 73
    label "klapa"
  ]
  node [
    id 74
    label "d&#261;browa"
  ]
  node [
    id 75
    label "tarnowski"
  ]
  node [
    id 76
    label "Leszek"
  ]
  node [
    id 77
    label "Miller"
  ]
  node [
    id 78
    label "Tankpol"
  ]
  node [
    id 79
    label "Roman"
  ]
  node [
    id 80
    label "M"
  ]
  node [
    id 81
    label "Andrzej"
  ]
  node [
    id 82
    label "&#321;"
  ]
  node [
    id 83
    label "komenda"
  ]
  node [
    id 84
    label "powiatowy"
  ]
  node [
    id 85
    label "policja"
  ]
  node [
    id 86
    label "Bogus&#322;aw"
  ]
  node [
    id 87
    label "P"
  ]
  node [
    id 88
    label "Renata"
  ]
  node [
    id 89
    label "gram"
  ]
  node [
    id 90
    label "D"
  ]
  node [
    id 91
    label "stra&#380;"
  ]
  node [
    id 92
    label "graniczny"
  ]
  node [
    id 93
    label "Szczucina"
  ]
  node [
    id 94
    label "Witaszek"
  ]
  node [
    id 95
    label "Grzegorz"
  ]
  node [
    id 96
    label "J"
  ]
  node [
    id 97
    label "Jerzy"
  ]
  node [
    id 98
    label "sekunda"
  ]
  node [
    id 99
    label "Jacek"
  ]
  node [
    id 100
    label "Waldemar"
  ]
  node [
    id 101
    label "marek"
  ]
  node [
    id 102
    label "Pawe&#322;"
  ]
  node [
    id 103
    label "w"
  ]
  node [
    id 104
    label "longin"
  ]
  node [
    id 105
    label "F"
  ]
  node [
    id 106
    label "Maciej"
  ]
  node [
    id 107
    label "C"
  ]
  node [
    id 108
    label "Krzysztof"
  ]
  node [
    id 109
    label "bit"
  ]
  node [
    id 110
    label "Robert"
  ]
  node [
    id 111
    label "N"
  ]
  node [
    id 112
    label "Boles&#322;aw"
  ]
  node [
    id 113
    label "postscriptum"
  ]
  node [
    id 114
    label "Papu&#347;ny"
  ]
  node [
    id 115
    label "zarzuci&#263;"
  ]
  node [
    id 116
    label "Ryszard"
  ]
  node [
    id 117
    label "Witaszka"
  ]
  node [
    id 118
    label "Tadeusz"
  ]
  node [
    id 119
    label "drab"
  ]
  node [
    id 120
    label "archiwum"
  ]
  node [
    id 121
    label "X"
  ]
  node [
    id 122
    label "kapela"
  ]
  node [
    id 123
    label "Iwona"
  ]
  node [
    id 124
    label "cygan"
  ]
  node [
    id 125
    label "s&#261;d"
  ]
  node [
    id 126
    label "okr&#281;gowy"
  ]
  node [
    id 127
    label "prokuratura"
  ]
  node [
    id 128
    label "krajowy"
  ]
  node [
    id 129
    label "Rzesz&#243;w"
  ]
  node [
    id 130
    label "Roberto"
  ]
  node [
    id 131
    label "m&#322;ody"
  ]
  node [
    id 132
    label "oko&#322;o"
  ]
  node [
    id 133
    label "obok"
  ]
  node [
    id 134
    label "u"
  ]
  node [
    id 135
    label "trabant"
  ]
  node [
    id 136
    label "wywie&#378;&#263;"
  ]
  node [
    id 137
    label "Witaszkiem"
  ]
  node [
    id 138
    label "Stany"
  ]
  node [
    id 139
    label "zjednoczy&#263;"
  ]
  node [
    id 140
    label "do"
  ]
  node [
    id 141
    label "klasa"
  ]
  node [
    id 142
    label "Renat"
  ]
  node [
    id 143
    label "Barbar"
  ]
  node [
    id 144
    label "Piwnik"
  ]
  node [
    id 145
    label "Piotr"
  ]
  node [
    id 146
    label "Krupi&#324;ski"
  ]
  node [
    id 147
    label "ma&#322;opolski"
  ]
  node [
    id 148
    label "wydzia&#322;"
  ]
  node [
    id 149
    label "sprawa"
  ]
  node [
    id 150
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 151
    label "zorganizowa&#263;"
  ]
  node [
    id 152
    label "i"
  ]
  node [
    id 153
    label "korupcja"
  ]
  node [
    id 154
    label "apelacyjny"
  ]
  node [
    id 155
    label "TVN"
  ]
  node [
    id 156
    label "info"
  ]
  node [
    id 157
    label "rejestr"
  ]
  node [
    id 158
    label "sprawca"
  ]
  node [
    id 159
    label "przest&#281;pstwo"
  ]
  node [
    id 160
    label "t&#322;o"
  ]
  node [
    id 161
    label "seksualny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 65
    target 131
  ]
  edge [
    source 65
    target 73
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 68
    target 81
  ]
  edge [
    source 68
    target 101
  ]
  edge [
    source 68
    target 102
  ]
  edge [
    source 68
    target 130
  ]
  edge [
    source 68
    target 110
  ]
  edge [
    source 68
    target 133
  ]
  edge [
    source 68
    target 136
  ]
  edge [
    source 68
    target 123
  ]
  edge [
    source 68
    target 113
  ]
  edge [
    source 68
    target 135
  ]
  edge [
    source 68
    target 140
  ]
  edge [
    source 68
    target 141
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 73
    target 131
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 93
  ]
  edge [
    source 76
    target 94
  ]
  edge [
    source 76
    target 117
  ]
  edge [
    source 76
    target 137
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 80
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 80
    target 99
  ]
  edge [
    source 80
    target 115
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 96
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 85
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 87
    target 112
  ]
  edge [
    source 87
    target 113
  ]
  edge [
    source 87
    target 114
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 90
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 100
  ]
  edge [
    source 89
    target 142
  ]
  edge [
    source 90
    target 142
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 132
  ]
  edge [
    source 96
    target 132
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 98
    target 116
  ]
  edge [
    source 99
    target 115
  ]
  edge [
    source 101
    target 122
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 133
  ]
  edge [
    source 103
    target 125
  ]
  edge [
    source 103
    target 126
  ]
  edge [
    source 103
    target 129
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 136
  ]
  edge [
    source 110
    target 123
  ]
  edge [
    source 110
    target 113
  ]
  edge [
    source 110
    target 135
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 114
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 135
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 136
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 129
  ]
  edge [
    source 125
    target 154
  ]
  edge [
    source 126
    target 129
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 130
    target 140
  ]
  edge [
    source 130
    target 141
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 140
    target 147
  ]
  edge [
    source 140
    target 148
  ]
  edge [
    source 140
    target 149
  ]
  edge [
    source 140
    target 150
  ]
  edge [
    source 140
    target 151
  ]
  edge [
    source 140
    target 152
  ]
  edge [
    source 140
    target 153
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 145
    target 146
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 147
    target 149
  ]
  edge [
    source 147
    target 150
  ]
  edge [
    source 147
    target 151
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 148
    target 150
  ]
  edge [
    source 148
    target 151
  ]
  edge [
    source 148
    target 152
  ]
  edge [
    source 148
    target 153
  ]
  edge [
    source 149
    target 150
  ]
  edge [
    source 149
    target 151
  ]
  edge [
    source 149
    target 152
  ]
  edge [
    source 149
    target 153
  ]
  edge [
    source 150
    target 151
  ]
  edge [
    source 150
    target 152
  ]
  edge [
    source 150
    target 153
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 151
    target 153
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 157
    target 159
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 160
    target 161
  ]
]
