graph [
  maxDegree 34
  minDegree 1
  meanDegree 2.8035714285714284
  density 0.025257400257400257
  graphCliqueNumber 8
  node [
    id 0
    label "stowarzyszenie"
    origin "text"
  ]
  node [
    id 1
    label "badacz"
    origin "text"
  ]
  node [
    id 2
    label "pismo"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wi&#281;ty"
    origin "text"
  ]
  node [
    id 4
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 5
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 6
    label "organizacja"
  ]
  node [
    id 7
    label "Eleusis"
  ]
  node [
    id 8
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 9
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 10
    label "grupa"
  ]
  node [
    id 11
    label "fabianie"
  ]
  node [
    id 12
    label "Chewra_Kadisza"
  ]
  node [
    id 13
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 14
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 15
    label "Rotary_International"
  ]
  node [
    id 16
    label "Monar"
  ]
  node [
    id 17
    label "Miczurin"
  ]
  node [
    id 18
    label "&#347;ledziciel"
  ]
  node [
    id 19
    label "uczony"
  ]
  node [
    id 20
    label "paleograf"
  ]
  node [
    id 21
    label "list"
  ]
  node [
    id 22
    label "egzemplarz"
  ]
  node [
    id 23
    label "komunikacja"
  ]
  node [
    id 24
    label "psychotest"
  ]
  node [
    id 25
    label "ortografia"
  ]
  node [
    id 26
    label "handwriting"
  ]
  node [
    id 27
    label "grafia"
  ]
  node [
    id 28
    label "prasa"
  ]
  node [
    id 29
    label "j&#281;zyk"
  ]
  node [
    id 30
    label "adres"
  ]
  node [
    id 31
    label "script"
  ]
  node [
    id 32
    label "dzia&#322;"
  ]
  node [
    id 33
    label "paleografia"
  ]
  node [
    id 34
    label "Zwrotnica"
  ]
  node [
    id 35
    label "wk&#322;ad"
  ]
  node [
    id 36
    label "cecha"
  ]
  node [
    id 37
    label "przekaz"
  ]
  node [
    id 38
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 39
    label "interpunkcja"
  ]
  node [
    id 40
    label "communication"
  ]
  node [
    id 41
    label "dokument"
  ]
  node [
    id 42
    label "ok&#322;adka"
  ]
  node [
    id 43
    label "dzie&#322;o"
  ]
  node [
    id 44
    label "czasopismo"
  ]
  node [
    id 45
    label "letter"
  ]
  node [
    id 46
    label "zajawka"
  ]
  node [
    id 47
    label "aureola"
  ]
  node [
    id 48
    label "&#347;wi&#281;ty_Micha&#322;_Archanio&#322;"
  ]
  node [
    id 49
    label "&#347;wi&#281;tny"
  ]
  node [
    id 50
    label "&#347;wi&#281;ty_Ambro&#380;y"
  ]
  node [
    id 51
    label "niebianin"
  ]
  node [
    id 52
    label "wz&#243;r"
  ]
  node [
    id 53
    label "Klaret"
  ]
  node [
    id 54
    label "&#347;wi&#281;ty_Jerzy"
  ]
  node [
    id 55
    label "raj"
  ]
  node [
    id 56
    label "hagiografia"
  ]
  node [
    id 57
    label "&#347;wi&#281;cie"
  ]
  node [
    id 58
    label "Metody"
  ]
  node [
    id 59
    label "kanonizowanie"
  ]
  node [
    id 60
    label "niepodwa&#380;alny"
  ]
  node [
    id 61
    label "ikonograf"
  ]
  node [
    id 62
    label "Jan_Pawe&#322;_II"
  ]
  node [
    id 63
    label "&#347;w"
  ]
  node [
    id 64
    label "zmar&#322;y"
  ]
  node [
    id 65
    label "br"
  ]
  node [
    id 66
    label "kanonizowany"
  ]
  node [
    id 67
    label "Wojciech"
  ]
  node [
    id 68
    label "przedmiot_kultu"
  ]
  node [
    id 69
    label "Grzegorz_I"
  ]
  node [
    id 70
    label "relikwia"
  ]
  node [
    id 71
    label "stowarzyszy&#263;"
  ]
  node [
    id 72
    label "Charles"
  ]
  node [
    id 73
    label "Taze"
  ]
  node [
    id 74
    label "Russell"
  ]
  node [
    id 75
    label "Wincenty"
  ]
  node [
    id 76
    label "kino"
  ]
  node [
    id 77
    label "mi&#281;dzynarodowy"
  ]
  node [
    id 78
    label "wyspa"
  ]
  node [
    id 79
    label "ko&#322;omyjski"
  ]
  node [
    id 80
    label "Czes&#322;awa"
  ]
  node [
    id 81
    label "Kasprzykowski"
  ]
  node [
    id 82
    label "ii"
  ]
  node [
    id 83
    label "&#347;wiecki"
  ]
  node [
    id 84
    label "rucho"
  ]
  node [
    id 85
    label "misyjny"
  ]
  node [
    id 86
    label "epifania"
  ]
  node [
    id 87
    label "wojna"
  ]
  node [
    id 88
    label "&#347;wiatowy"
  ]
  node [
    id 89
    label "urz&#261;d"
  ]
  node [
    id 90
    label "do"
  ]
  node [
    id 91
    label "sprawi&#263;"
  ]
  node [
    id 92
    label "wyznanie"
  ]
  node [
    id 93
    label "wyzna&#263;"
  ]
  node [
    id 94
    label "religijny"
  ]
  node [
    id 95
    label "ministerstwo"
  ]
  node [
    id 96
    label "wewn&#281;trzny"
  ]
  node [
    id 97
    label "i"
  ]
  node [
    id 98
    label "administracja"
  ]
  node [
    id 99
    label "&#347;wita"
  ]
  node [
    id 100
    label "kr&#243;lestwo"
  ]
  node [
    id 101
    label "bo&#380;y"
  ]
  node [
    id 102
    label "wt&#243;ry"
  ]
  node [
    id 103
    label "obecno&#347;&#263;"
  ]
  node [
    id 104
    label "Jezus"
  ]
  node [
    id 105
    label "Chrystus"
  ]
  node [
    id 106
    label "biuletyn"
  ]
  node [
    id 107
    label "Polska"
  ]
  node [
    id 108
    label "nadzieja"
  ]
  node [
    id 109
    label "b&#243;g"
  ]
  node [
    id 110
    label "wiekuisty"
  ]
  node [
    id 111
    label "duch"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 71
    target 77
  ]
  edge [
    source 71
    target 106
  ]
  edge [
    source 71
    target 78
  ]
  edge [
    source 71
    target 107
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 74
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 106
  ]
  edge [
    source 78
    target 107
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 82
    target 87
  ]
  edge [
    source 82
    target 88
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 85
  ]
  edge [
    source 83
    target 86
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 86
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 89
    target 92
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 92
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 95
  ]
  edge [
    source 91
    target 96
  ]
  edge [
    source 91
    target 97
  ]
  edge [
    source 91
    target 98
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 97
  ]
  edge [
    source 95
    target 98
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 98
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 99
  ]
  edge [
    source 97
    target 100
  ]
  edge [
    source 97
    target 101
  ]
  edge [
    source 97
    target 102
  ]
  edge [
    source 97
    target 103
  ]
  edge [
    source 97
    target 104
  ]
  edge [
    source 97
    target 105
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 101
  ]
  edge [
    source 99
    target 102
  ]
  edge [
    source 99
    target 103
  ]
  edge [
    source 99
    target 104
  ]
  edge [
    source 99
    target 105
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 102
  ]
  edge [
    source 100
    target 103
  ]
  edge [
    source 100
    target 104
  ]
  edge [
    source 100
    target 105
  ]
  edge [
    source 100
    target 108
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 103
  ]
  edge [
    source 101
    target 104
  ]
  edge [
    source 101
    target 105
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 104
  ]
  edge [
    source 102
    target 105
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 105
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 109
    target 110
  ]
]
