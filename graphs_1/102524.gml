graph [
  maxDegree 36
  minDegree 1
  meanDegree 2.087719298245614
  density 0.00919700131385733
  graphCliqueNumber 2
  node [
    id 0
    label "okaza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "badany"
    origin "text"
  ]
  node [
    id 3
    label "grupa"
    origin "text"
  ]
  node [
    id 4
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "technika"
    origin "text"
  ]
  node [
    id 6
    label "m&#243;wienie"
    origin "text"
  ]
  node [
    id 7
    label "siebie"
    origin "text"
  ]
  node [
    id 8
    label "osi&#261;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "lepsze"
    origin "text"
  ]
  node [
    id 10
    label "wynik"
    origin "text"
  ]
  node [
    id 11
    label "turniej"
    origin "text"
  ]
  node [
    id 12
    label "samoinstruowanie"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "skuteczny"
    origin "text"
  ]
  node [
    id 15
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 16
    label "polepszenie"
    origin "text"
  ]
  node [
    id 17
    label "efektywno&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "gra"
    origin "text"
  ]
  node [
    id 19
    label "szczeg&#243;lnie"
    origin "text"
  ]
  node [
    id 20
    label "dyscyplina"
    origin "text"
  ]
  node [
    id 21
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 22
    label "zale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 23
    label "precyzja"
    origin "text"
  ]
  node [
    id 24
    label "ruch"
    origin "text"
  ]
  node [
    id 25
    label "pokaza&#263;"
  ]
  node [
    id 26
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 27
    label "testify"
  ]
  node [
    id 28
    label "give"
  ]
  node [
    id 29
    label "uczestnik"
  ]
  node [
    id 30
    label "pacjent"
  ]
  node [
    id 31
    label "analizowa&#263;"
  ]
  node [
    id 32
    label "odm&#322;adza&#263;"
  ]
  node [
    id 33
    label "asymilowa&#263;"
  ]
  node [
    id 34
    label "cz&#261;steczka"
  ]
  node [
    id 35
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 36
    label "egzemplarz"
  ]
  node [
    id 37
    label "formacja_geologiczna"
  ]
  node [
    id 38
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 39
    label "harcerze_starsi"
  ]
  node [
    id 40
    label "liga"
  ]
  node [
    id 41
    label "Terranie"
  ]
  node [
    id 42
    label "&#346;wietliki"
  ]
  node [
    id 43
    label "pakiet_klimatyczny"
  ]
  node [
    id 44
    label "oddzia&#322;"
  ]
  node [
    id 45
    label "stage_set"
  ]
  node [
    id 46
    label "Entuzjastki"
  ]
  node [
    id 47
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 48
    label "odm&#322;odzenie"
  ]
  node [
    id 49
    label "type"
  ]
  node [
    id 50
    label "category"
  ]
  node [
    id 51
    label "asymilowanie"
  ]
  node [
    id 52
    label "specgrupa"
  ]
  node [
    id 53
    label "odm&#322;adzanie"
  ]
  node [
    id 54
    label "gromada"
  ]
  node [
    id 55
    label "Eurogrupa"
  ]
  node [
    id 56
    label "jednostka_systematyczna"
  ]
  node [
    id 57
    label "kompozycja"
  ]
  node [
    id 58
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 59
    label "zbi&#243;r"
  ]
  node [
    id 60
    label "u&#380;ywa&#263;"
  ]
  node [
    id 61
    label "engineering"
  ]
  node [
    id 62
    label "sprawno&#347;&#263;"
  ]
  node [
    id 63
    label "technologia"
  ]
  node [
    id 64
    label "przedmiot"
  ]
  node [
    id 65
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 66
    label "wiedza"
  ]
  node [
    id 67
    label "teletechnika"
  ]
  node [
    id 68
    label "fotowoltaika"
  ]
  node [
    id 69
    label "mechanika_precyzyjna"
  ]
  node [
    id 70
    label "telekomunikacja"
  ]
  node [
    id 71
    label "cywilizacja"
  ]
  node [
    id 72
    label "wygadanie"
  ]
  node [
    id 73
    label "przepowiadanie"
  ]
  node [
    id 74
    label "przerywanie"
  ]
  node [
    id 75
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 76
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 77
    label "dodawanie"
  ]
  node [
    id 78
    label "speaking"
  ]
  node [
    id 79
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 80
    label "public_speaking"
  ]
  node [
    id 81
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 82
    label "stosowanie"
  ]
  node [
    id 83
    label "j&#281;zyk"
  ]
  node [
    id 84
    label "zwracanie_si&#281;"
  ]
  node [
    id 85
    label "wypowiadanie"
  ]
  node [
    id 86
    label "gaworzenie"
  ]
  node [
    id 87
    label "mawianie"
  ]
  node [
    id 88
    label "wydobywanie"
  ]
  node [
    id 89
    label "opowiadanie"
  ]
  node [
    id 90
    label "wyra&#380;anie"
  ]
  node [
    id 91
    label "zapeszanie"
  ]
  node [
    id 92
    label "prawienie"
  ]
  node [
    id 93
    label "formu&#322;owanie"
  ]
  node [
    id 94
    label "ozywanie_si&#281;"
  ]
  node [
    id 95
    label "dysfonia"
  ]
  node [
    id 96
    label "opowiedzenie"
  ]
  node [
    id 97
    label "informowanie"
  ]
  node [
    id 98
    label "dowalenie"
  ]
  node [
    id 99
    label "zauwa&#380;enie"
  ]
  node [
    id 100
    label "powiadanie"
  ]
  node [
    id 101
    label "dogadywanie_si&#281;"
  ]
  node [
    id 102
    label "dogadanie_si&#281;"
  ]
  node [
    id 103
    label "wyznawanie"
  ]
  node [
    id 104
    label "wydawanie"
  ]
  node [
    id 105
    label "profit"
  ]
  node [
    id 106
    label "score"
  ]
  node [
    id 107
    label "dotrze&#263;"
  ]
  node [
    id 108
    label "uzyska&#263;"
  ]
  node [
    id 109
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 110
    label "make"
  ]
  node [
    id 111
    label "typ"
  ]
  node [
    id 112
    label "dzia&#322;anie"
  ]
  node [
    id 113
    label "przyczyna"
  ]
  node [
    id 114
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 115
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 116
    label "zaokr&#261;glenie"
  ]
  node [
    id 117
    label "event"
  ]
  node [
    id 118
    label "rezultat"
  ]
  node [
    id 119
    label "eliminacje"
  ]
  node [
    id 120
    label "zawody"
  ]
  node [
    id 121
    label "Wielki_Szlem"
  ]
  node [
    id 122
    label "drive"
  ]
  node [
    id 123
    label "impreza"
  ]
  node [
    id 124
    label "pojedynek"
  ]
  node [
    id 125
    label "runda"
  ]
  node [
    id 126
    label "tournament"
  ]
  node [
    id 127
    label "rywalizacja"
  ]
  node [
    id 128
    label "si&#281;ga&#263;"
  ]
  node [
    id 129
    label "trwa&#263;"
  ]
  node [
    id 130
    label "obecno&#347;&#263;"
  ]
  node [
    id 131
    label "stan"
  ]
  node [
    id 132
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 133
    label "stand"
  ]
  node [
    id 134
    label "mie&#263;_miejsce"
  ]
  node [
    id 135
    label "uczestniczy&#263;"
  ]
  node [
    id 136
    label "chodzi&#263;"
  ]
  node [
    id 137
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 138
    label "equal"
  ]
  node [
    id 139
    label "skutkowanie"
  ]
  node [
    id 140
    label "poskutkowanie"
  ]
  node [
    id 141
    label "dobry"
  ]
  node [
    id 142
    label "sprawny"
  ]
  node [
    id 143
    label "skutecznie"
  ]
  node [
    id 144
    label "model"
  ]
  node [
    id 145
    label "tryb"
  ]
  node [
    id 146
    label "narz&#281;dzie"
  ]
  node [
    id 147
    label "nature"
  ]
  node [
    id 148
    label "poprawa"
  ]
  node [
    id 149
    label "ulepszenie"
  ]
  node [
    id 150
    label "zmienienie"
  ]
  node [
    id 151
    label "lepszy"
  ]
  node [
    id 152
    label "zmiana"
  ]
  node [
    id 153
    label "skuteczno&#347;&#263;"
  ]
  node [
    id 154
    label "zabawa"
  ]
  node [
    id 155
    label "czynno&#347;&#263;"
  ]
  node [
    id 156
    label "Pok&#233;mon"
  ]
  node [
    id 157
    label "synteza"
  ]
  node [
    id 158
    label "odtworzenie"
  ]
  node [
    id 159
    label "komplet"
  ]
  node [
    id 160
    label "rekwizyt_do_gry"
  ]
  node [
    id 161
    label "odg&#322;os"
  ]
  node [
    id 162
    label "rozgrywka"
  ]
  node [
    id 163
    label "post&#281;powanie"
  ]
  node [
    id 164
    label "wydarzenie"
  ]
  node [
    id 165
    label "apparent_motion"
  ]
  node [
    id 166
    label "game"
  ]
  node [
    id 167
    label "zmienno&#347;&#263;"
  ]
  node [
    id 168
    label "zasada"
  ]
  node [
    id 169
    label "akcja"
  ]
  node [
    id 170
    label "play"
  ]
  node [
    id 171
    label "contest"
  ]
  node [
    id 172
    label "zbijany"
  ]
  node [
    id 173
    label "szczeg&#243;lny"
  ]
  node [
    id 174
    label "osobnie"
  ]
  node [
    id 175
    label "wyj&#261;tkowo"
  ]
  node [
    id 176
    label "specially"
  ]
  node [
    id 177
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 178
    label "sport"
  ]
  node [
    id 179
    label "zakres"
  ]
  node [
    id 180
    label "zachowa&#263;"
  ]
  node [
    id 181
    label "zachowanie"
  ]
  node [
    id 182
    label "konkurencja"
  ]
  node [
    id 183
    label "bezdro&#380;e"
  ]
  node [
    id 184
    label "zachowywa&#263;"
  ]
  node [
    id 185
    label "bat"
  ]
  node [
    id 186
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 187
    label "porz&#261;dek"
  ]
  node [
    id 188
    label "zachowywanie"
  ]
  node [
    id 189
    label "wielob&#243;j"
  ]
  node [
    id 190
    label "sfera"
  ]
  node [
    id 191
    label "mores"
  ]
  node [
    id 192
    label "styl"
  ]
  node [
    id 193
    label "poddzia&#322;"
  ]
  node [
    id 194
    label "potrzebowa&#263;"
  ]
  node [
    id 195
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 196
    label "lie"
  ]
  node [
    id 197
    label "cecha"
  ]
  node [
    id 198
    label "manewr"
  ]
  node [
    id 199
    label "movement"
  ]
  node [
    id 200
    label "apraksja"
  ]
  node [
    id 201
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 202
    label "poruszenie"
  ]
  node [
    id 203
    label "commercial_enterprise"
  ]
  node [
    id 204
    label "dyssypacja_energii"
  ]
  node [
    id 205
    label "utrzymanie"
  ]
  node [
    id 206
    label "utrzyma&#263;"
  ]
  node [
    id 207
    label "komunikacja"
  ]
  node [
    id 208
    label "tumult"
  ]
  node [
    id 209
    label "kr&#243;tki"
  ]
  node [
    id 210
    label "drift"
  ]
  node [
    id 211
    label "utrzymywa&#263;"
  ]
  node [
    id 212
    label "stopek"
  ]
  node [
    id 213
    label "kanciasty"
  ]
  node [
    id 214
    label "d&#322;ugi"
  ]
  node [
    id 215
    label "zjawisko"
  ]
  node [
    id 216
    label "utrzymywanie"
  ]
  node [
    id 217
    label "myk"
  ]
  node [
    id 218
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 219
    label "taktyka"
  ]
  node [
    id 220
    label "move"
  ]
  node [
    id 221
    label "natural_process"
  ]
  node [
    id 222
    label "lokomocja"
  ]
  node [
    id 223
    label "mechanika"
  ]
  node [
    id 224
    label "proces"
  ]
  node [
    id 225
    label "strumie&#324;"
  ]
  node [
    id 226
    label "aktywno&#347;&#263;"
  ]
  node [
    id 227
    label "travel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 59
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 24
    target 198
  ]
  edge [
    source 24
    target 144
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 24
    target 65
  ]
  edge [
    source 24
    target 201
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 152
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 155
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 24
    target 225
  ]
  edge [
    source 24
    target 226
  ]
  edge [
    source 24
    target 227
  ]
]
