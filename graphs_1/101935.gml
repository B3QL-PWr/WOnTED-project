graph [
  maxDegree 44
  minDegree 1
  meanDegree 2.1176470588235294
  density 0.009625668449197862
  graphCliqueNumber 3
  node [
    id 0
    label "producent"
    origin "text"
  ]
  node [
    id 1
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wszelki"
    origin "text"
  ]
  node [
    id 3
    label "konieczna"
    origin "text"
  ]
  node [
    id 4
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 5
    label "aby"
    origin "text"
  ]
  node [
    id 6
    label "proces"
    origin "text"
  ]
  node [
    id 7
    label "produkcja"
    origin "text"
  ]
  node [
    id 8
    label "zapewni&#263;"
    origin "text"
  ]
  node [
    id 9
    label "zgodno&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "jednostka"
    origin "text"
  ]
  node [
    id 11
    label "p&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "lub"
    origin "text"
  ]
  node [
    id 13
    label "element"
    origin "text"
  ]
  node [
    id 14
    label "wzorzec"
    origin "text"
  ]
  node [
    id 15
    label "wyr&#243;b"
    origin "text"
  ]
  node [
    id 16
    label "okre&#347;lony"
    origin "text"
  ]
  node [
    id 17
    label "&#347;wiadectwo"
    origin "text"
  ]
  node [
    id 18
    label "badanie"
    origin "text"
  ]
  node [
    id 19
    label "reprezentatywny"
    origin "text"
  ]
  node [
    id 20
    label "spe&#322;nia&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wymagania"
    origin "text"
  ]
  node [
    id 22
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 23
    label "rynek"
  ]
  node [
    id 24
    label "podmiot"
  ]
  node [
    id 25
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 26
    label "manufacturer"
  ]
  node [
    id 27
    label "bran&#380;owiec"
  ]
  node [
    id 28
    label "artel"
  ]
  node [
    id 29
    label "filmowiec"
  ]
  node [
    id 30
    label "muzyk"
  ]
  node [
    id 31
    label "Canon"
  ]
  node [
    id 32
    label "wykonawca"
  ]
  node [
    id 33
    label "autotrof"
  ]
  node [
    id 34
    label "Wedel"
  ]
  node [
    id 35
    label "zmienia&#263;"
  ]
  node [
    id 36
    label "reagowa&#263;"
  ]
  node [
    id 37
    label "rise"
  ]
  node [
    id 38
    label "admit"
  ]
  node [
    id 39
    label "drive"
  ]
  node [
    id 40
    label "robi&#263;"
  ]
  node [
    id 41
    label "draw"
  ]
  node [
    id 42
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 43
    label "podnosi&#263;"
  ]
  node [
    id 44
    label "ka&#380;dy"
  ]
  node [
    id 45
    label "miejsce"
  ]
  node [
    id 46
    label "czas"
  ]
  node [
    id 47
    label "abstrakcja"
  ]
  node [
    id 48
    label "punkt"
  ]
  node [
    id 49
    label "substancja"
  ]
  node [
    id 50
    label "spos&#243;b"
  ]
  node [
    id 51
    label "chemikalia"
  ]
  node [
    id 52
    label "troch&#281;"
  ]
  node [
    id 53
    label "legislacyjnie"
  ]
  node [
    id 54
    label "kognicja"
  ]
  node [
    id 55
    label "przebieg"
  ]
  node [
    id 56
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 57
    label "wydarzenie"
  ]
  node [
    id 58
    label "przes&#322;anka"
  ]
  node [
    id 59
    label "rozprawa"
  ]
  node [
    id 60
    label "zjawisko"
  ]
  node [
    id 61
    label "nast&#281;pstwo"
  ]
  node [
    id 62
    label "tingel-tangel"
  ]
  node [
    id 63
    label "odtworzenie"
  ]
  node [
    id 64
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 65
    label "wydawa&#263;"
  ]
  node [
    id 66
    label "realizacja"
  ]
  node [
    id 67
    label "monta&#380;"
  ]
  node [
    id 68
    label "rozw&#243;j"
  ]
  node [
    id 69
    label "fabrication"
  ]
  node [
    id 70
    label "kreacja"
  ]
  node [
    id 71
    label "uzysk"
  ]
  node [
    id 72
    label "dorobek"
  ]
  node [
    id 73
    label "wyda&#263;"
  ]
  node [
    id 74
    label "impreza"
  ]
  node [
    id 75
    label "postprodukcja"
  ]
  node [
    id 76
    label "numer"
  ]
  node [
    id 77
    label "kooperowa&#263;"
  ]
  node [
    id 78
    label "creation"
  ]
  node [
    id 79
    label "trema"
  ]
  node [
    id 80
    label "zbi&#243;r"
  ]
  node [
    id 81
    label "product"
  ]
  node [
    id 82
    label "performance"
  ]
  node [
    id 83
    label "translate"
  ]
  node [
    id 84
    label "give"
  ]
  node [
    id 85
    label "poinformowa&#263;"
  ]
  node [
    id 86
    label "spowodowa&#263;"
  ]
  node [
    id 87
    label "spok&#243;j"
  ]
  node [
    id 88
    label "cecha"
  ]
  node [
    id 89
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 90
    label "entity"
  ]
  node [
    id 91
    label "zbie&#380;no&#347;&#263;"
  ]
  node [
    id 92
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 93
    label "infimum"
  ]
  node [
    id 94
    label "przyswoi&#263;"
  ]
  node [
    id 95
    label "ewoluowanie"
  ]
  node [
    id 96
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 97
    label "reakcja"
  ]
  node [
    id 98
    label "wyewoluowanie"
  ]
  node [
    id 99
    label "individual"
  ]
  node [
    id 100
    label "profanum"
  ]
  node [
    id 101
    label "starzenie_si&#281;"
  ]
  node [
    id 102
    label "homo_sapiens"
  ]
  node [
    id 103
    label "skala"
  ]
  node [
    id 104
    label "supremum"
  ]
  node [
    id 105
    label "przyswaja&#263;"
  ]
  node [
    id 106
    label "ludzko&#347;&#263;"
  ]
  node [
    id 107
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 108
    label "one"
  ]
  node [
    id 109
    label "funkcja"
  ]
  node [
    id 110
    label "przeliczenie"
  ]
  node [
    id 111
    label "przeliczanie"
  ]
  node [
    id 112
    label "mikrokosmos"
  ]
  node [
    id 113
    label "rzut"
  ]
  node [
    id 114
    label "portrecista"
  ]
  node [
    id 115
    label "przelicza&#263;"
  ]
  node [
    id 116
    label "przyswajanie"
  ]
  node [
    id 117
    label "duch"
  ]
  node [
    id 118
    label "wyewoluowa&#263;"
  ]
  node [
    id 119
    label "ewoluowa&#263;"
  ]
  node [
    id 120
    label "g&#322;owa"
  ]
  node [
    id 121
    label "oddzia&#322;ywanie"
  ]
  node [
    id 122
    label "liczba_naturalna"
  ]
  node [
    id 123
    label "poj&#281;cie"
  ]
  node [
    id 124
    label "osoba"
  ]
  node [
    id 125
    label "figura"
  ]
  node [
    id 126
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 127
    label "obiekt"
  ]
  node [
    id 128
    label "matematyka"
  ]
  node [
    id 129
    label "przyswojenie"
  ]
  node [
    id 130
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 131
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 132
    label "czynnik_biotyczny"
  ]
  node [
    id 133
    label "przeliczy&#263;"
  ]
  node [
    id 134
    label "antropochoria"
  ]
  node [
    id 135
    label "lata&#263;"
  ]
  node [
    id 136
    label "by&#263;"
  ]
  node [
    id 137
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 138
    label "mie&#263;"
  ]
  node [
    id 139
    label "m&#243;wi&#263;"
  ]
  node [
    id 140
    label "sterowa&#263;"
  ]
  node [
    id 141
    label "statek"
  ]
  node [
    id 142
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 143
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 144
    label "zanika&#263;"
  ]
  node [
    id 145
    label "falowa&#263;"
  ]
  node [
    id 146
    label "sink"
  ]
  node [
    id 147
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 148
    label "pracowa&#263;"
  ]
  node [
    id 149
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 150
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 151
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 152
    label "swimming"
  ]
  node [
    id 153
    label "ciecz"
  ]
  node [
    id 154
    label "szkodnik"
  ]
  node [
    id 155
    label "&#347;rodowisko"
  ]
  node [
    id 156
    label "component"
  ]
  node [
    id 157
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 158
    label "r&#243;&#380;niczka"
  ]
  node [
    id 159
    label "przedmiot"
  ]
  node [
    id 160
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 161
    label "gangsterski"
  ]
  node [
    id 162
    label "szambo"
  ]
  node [
    id 163
    label "materia"
  ]
  node [
    id 164
    label "aspo&#322;eczny"
  ]
  node [
    id 165
    label "underworld"
  ]
  node [
    id 166
    label "cz&#322;owiek"
  ]
  node [
    id 167
    label "ruch"
  ]
  node [
    id 168
    label "punkt_odniesienia"
  ]
  node [
    id 169
    label "ideal"
  ]
  node [
    id 170
    label "mildew"
  ]
  node [
    id 171
    label "wytw&#243;r"
  ]
  node [
    id 172
    label "produkt"
  ]
  node [
    id 173
    label "p&#322;uczkarnia"
  ]
  node [
    id 174
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 175
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 176
    label "znakowarka"
  ]
  node [
    id 177
    label "wiadomy"
  ]
  node [
    id 178
    label "dokument"
  ]
  node [
    id 179
    label "certificate"
  ]
  node [
    id 180
    label "promocja"
  ]
  node [
    id 181
    label "o&#347;wiadczenie"
  ]
  node [
    id 182
    label "dow&#243;d"
  ]
  node [
    id 183
    label "za&#347;wiadczenie"
  ]
  node [
    id 184
    label "usi&#322;owanie"
  ]
  node [
    id 185
    label "examination"
  ]
  node [
    id 186
    label "investigation"
  ]
  node [
    id 187
    label "ustalenie"
  ]
  node [
    id 188
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 189
    label "ustalanie"
  ]
  node [
    id 190
    label "bia&#322;a_niedziela"
  ]
  node [
    id 191
    label "analysis"
  ]
  node [
    id 192
    label "rozpatrywanie"
  ]
  node [
    id 193
    label "wziernikowanie"
  ]
  node [
    id 194
    label "obserwowanie"
  ]
  node [
    id 195
    label "omawianie"
  ]
  node [
    id 196
    label "sprawdzanie"
  ]
  node [
    id 197
    label "udowadnianie"
  ]
  node [
    id 198
    label "diagnostyka"
  ]
  node [
    id 199
    label "czynno&#347;&#263;"
  ]
  node [
    id 200
    label "macanie"
  ]
  node [
    id 201
    label "rektalny"
  ]
  node [
    id 202
    label "penetrowanie"
  ]
  node [
    id 203
    label "krytykowanie"
  ]
  node [
    id 204
    label "kontrola"
  ]
  node [
    id 205
    label "dociekanie"
  ]
  node [
    id 206
    label "zrecenzowanie"
  ]
  node [
    id 207
    label "praca"
  ]
  node [
    id 208
    label "rezultat"
  ]
  node [
    id 209
    label "dobry"
  ]
  node [
    id 210
    label "typowy"
  ]
  node [
    id 211
    label "close"
  ]
  node [
    id 212
    label "perform"
  ]
  node [
    id 213
    label "urzeczywistnia&#263;"
  ]
  node [
    id 214
    label "rule"
  ]
  node [
    id 215
    label "polecenie"
  ]
  node [
    id 216
    label "akt"
  ]
  node [
    id 217
    label "arrangement"
  ]
  node [
    id 218
    label "zarz&#261;dzenie"
  ]
  node [
    id 219
    label "commission"
  ]
  node [
    id 220
    label "ordonans"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 50
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 64
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 18
    target 195
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 207
  ]
  edge [
    source 18
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 217
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 219
  ]
  edge [
    source 22
    target 220
  ]
]
