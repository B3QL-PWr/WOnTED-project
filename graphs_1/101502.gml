graph [
  maxDegree 24
  minDegree 1
  meanDegree 3.8536585365853657
  density 0.04757603131586871
  graphCliqueNumber 10
  node [
    id 0
    label "&#347;wiatowy"
    origin "text"
  ]
  node [
    id 1
    label "organizacja"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;asno&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "intelektualny"
    origin "text"
  ]
  node [
    id 4
    label "kulturalny"
  ]
  node [
    id 5
    label "&#347;wiatowo"
  ]
  node [
    id 6
    label "generalny"
  ]
  node [
    id 7
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 8
    label "endecki"
  ]
  node [
    id 9
    label "komitet_koordynacyjny"
  ]
  node [
    id 10
    label "przybud&#243;wka"
  ]
  node [
    id 11
    label "ZOMO"
  ]
  node [
    id 12
    label "podmiot"
  ]
  node [
    id 13
    label "boj&#243;wka"
  ]
  node [
    id 14
    label "zesp&#243;&#322;"
  ]
  node [
    id 15
    label "organization"
  ]
  node [
    id 16
    label "TOPR"
  ]
  node [
    id 17
    label "jednostka_organizacyjna"
  ]
  node [
    id 18
    label "przedstawicielstwo"
  ]
  node [
    id 19
    label "Cepelia"
  ]
  node [
    id 20
    label "GOPR"
  ]
  node [
    id 21
    label "ZMP"
  ]
  node [
    id 22
    label "ZBoWiD"
  ]
  node [
    id 23
    label "struktura"
  ]
  node [
    id 24
    label "od&#322;am"
  ]
  node [
    id 25
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 26
    label "centrala"
  ]
  node [
    id 27
    label "rodowo&#347;&#263;"
  ]
  node [
    id 28
    label "prawo_rzeczowe"
  ]
  node [
    id 29
    label "possession"
  ]
  node [
    id 30
    label "stan"
  ]
  node [
    id 31
    label "dobra"
  ]
  node [
    id 32
    label "charakterystyka"
  ]
  node [
    id 33
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 34
    label "patent"
  ]
  node [
    id 35
    label "mienie"
  ]
  node [
    id 36
    label "przej&#347;&#263;"
  ]
  node [
    id 37
    label "attribute"
  ]
  node [
    id 38
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 39
    label "przej&#347;cie"
  ]
  node [
    id 40
    label "g&#322;&#281;boki"
  ]
  node [
    id 41
    label "naukowy"
  ]
  node [
    id 42
    label "inteligentny"
  ]
  node [
    id 43
    label "wznios&#322;y"
  ]
  node [
    id 44
    label "umys&#322;owy"
  ]
  node [
    id 45
    label "intelektualnie"
  ]
  node [
    id 46
    label "my&#347;l&#261;cy"
  ]
  node [
    id 47
    label "World"
  ]
  node [
    id 48
    label "Intellectual"
  ]
  node [
    id 49
    label "Property"
  ]
  node [
    id 50
    label "Organization"
  ]
  node [
    id 51
    label "frank"
  ]
  node [
    id 52
    label "szwajcarski"
  ]
  node [
    id 53
    label "zgromadzi&#263;"
  ]
  node [
    id 54
    label "og&#243;lny"
  ]
  node [
    id 55
    label "Francis"
  ]
  node [
    id 56
    label "Gurry"
  ]
  node [
    id 57
    label "konferencja"
  ]
  node [
    id 58
    label "wszystek"
  ]
  node [
    id 59
    label "cz&#322;onki"
  ]
  node [
    id 60
    label "komitet"
  ]
  node [
    id 61
    label "koordynacyjny"
  ]
  node [
    id 62
    label "harmonizacja"
  ]
  node [
    id 63
    label "prawy"
  ]
  node [
    id 64
    label "ochrona"
  ]
  node [
    id 65
    label "znak"
  ]
  node [
    id 66
    label "towarowy"
  ]
  node [
    id 67
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 68
    label "sp&#243;r"
  ]
  node [
    id 69
    label "co"
  ]
  node [
    id 70
    label "do"
  ]
  node [
    id 71
    label "mi&#281;dzy"
  ]
  node [
    id 72
    label "pa&#324;stwo"
  ]
  node [
    id 73
    label "konwencja"
  ]
  node [
    id 74
    label "zeszyt"
  ]
  node [
    id 75
    label "Berno"
  ]
  node [
    id 76
    label "ojciec"
  ]
  node [
    id 77
    label "dzie&#322;o"
  ]
  node [
    id 78
    label "literacki"
  ]
  node [
    id 79
    label "i"
  ]
  node [
    id 80
    label "artystyczny"
  ]
  node [
    id 81
    label "handel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 62
  ]
  edge [
    source 60
    target 63
  ]
  edge [
    source 60
    target 64
  ]
  edge [
    source 60
    target 65
  ]
  edge [
    source 60
    target 66
  ]
  edge [
    source 60
    target 67
  ]
  edge [
    source 60
    target 68
  ]
  edge [
    source 60
    target 69
  ]
  edge [
    source 60
    target 70
  ]
  edge [
    source 60
    target 71
  ]
  edge [
    source 60
    target 72
  ]
  edge [
    source 60
    target 73
  ]
  edge [
    source 60
    target 74
  ]
  edge [
    source 60
    target 75
  ]
  edge [
    source 60
    target 76
  ]
  edge [
    source 60
    target 77
  ]
  edge [
    source 60
    target 78
  ]
  edge [
    source 60
    target 79
  ]
  edge [
    source 60
    target 80
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 62
    target 66
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 63
    target 66
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 64
    target 73
  ]
  edge [
    source 64
    target 74
  ]
  edge [
    source 64
    target 75
  ]
  edge [
    source 64
    target 76
  ]
  edge [
    source 64
    target 77
  ]
  edge [
    source 64
    target 78
  ]
  edge [
    source 64
    target 79
  ]
  edge [
    source 64
    target 80
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 69
  ]
  edge [
    source 67
    target 70
  ]
  edge [
    source 67
    target 71
  ]
  edge [
    source 67
    target 72
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 68
    target 71
  ]
  edge [
    source 68
    target 72
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 69
    target 72
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 75
  ]
  edge [
    source 73
    target 76
  ]
  edge [
    source 73
    target 77
  ]
  edge [
    source 73
    target 78
  ]
  edge [
    source 73
    target 79
  ]
  edge [
    source 73
    target 80
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 76
  ]
  edge [
    source 74
    target 77
  ]
  edge [
    source 74
    target 78
  ]
  edge [
    source 74
    target 79
  ]
  edge [
    source 74
    target 80
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 77
  ]
  edge [
    source 75
    target 78
  ]
  edge [
    source 75
    target 79
  ]
  edge [
    source 75
    target 80
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 78
  ]
  edge [
    source 76
    target 79
  ]
  edge [
    source 76
    target 80
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 79
  ]
  edge [
    source 77
    target 80
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 80
  ]
  edge [
    source 79
    target 80
  ]
]
