graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "jeszcze"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "sylwester"
    origin "text"
  ]
  node [
    id 3
    label "ci&#261;gle"
  ]
  node [
    id 4
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 5
    label "doba"
  ]
  node [
    id 6
    label "czas"
  ]
  node [
    id 7
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 8
    label "weekend"
  ]
  node [
    id 9
    label "miesi&#261;c"
  ]
  node [
    id 10
    label "impreza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 10
  ]
]
