graph [
  maxDegree 37
  minDegree 1
  meanDegree 1.9789473684210526
  density 0.021052631578947368
  graphCliqueNumber 2
  node [
    id 0
    label "chyba"
    origin "text"
  ]
  node [
    id 1
    label "mama"
    origin "text"
  ]
  node [
    id 2
    label "s&#322;aby"
    origin "text"
  ]
  node [
    id 3
    label "poczucie"
    origin "text"
  ]
  node [
    id 4
    label "humor"
    origin "text"
  ]
  node [
    id 5
    label "za&#347;miane"
    origin "text"
  ]
  node [
    id 6
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 7
    label "matczysko"
  ]
  node [
    id 8
    label "macierz"
  ]
  node [
    id 9
    label "przodkini"
  ]
  node [
    id 10
    label "Matka_Boska"
  ]
  node [
    id 11
    label "macocha"
  ]
  node [
    id 12
    label "matka_zast&#281;pcza"
  ]
  node [
    id 13
    label "stara"
  ]
  node [
    id 14
    label "rodzice"
  ]
  node [
    id 15
    label "rodzic"
  ]
  node [
    id 16
    label "nieznaczny"
  ]
  node [
    id 17
    label "nieumiej&#281;tny"
  ]
  node [
    id 18
    label "marnie"
  ]
  node [
    id 19
    label "md&#322;y"
  ]
  node [
    id 20
    label "przemijaj&#261;cy"
  ]
  node [
    id 21
    label "zawodny"
  ]
  node [
    id 22
    label "delikatny"
  ]
  node [
    id 23
    label "&#322;agodny"
  ]
  node [
    id 24
    label "niedoskona&#322;y"
  ]
  node [
    id 25
    label "nietrwa&#322;y"
  ]
  node [
    id 26
    label "po&#347;ledni"
  ]
  node [
    id 27
    label "s&#322;abowity"
  ]
  node [
    id 28
    label "niefajny"
  ]
  node [
    id 29
    label "z&#322;y"
  ]
  node [
    id 30
    label "niemocny"
  ]
  node [
    id 31
    label "kiepsko"
  ]
  node [
    id 32
    label "niezdrowy"
  ]
  node [
    id 33
    label "lura"
  ]
  node [
    id 34
    label "s&#322;abo"
  ]
  node [
    id 35
    label "nieudany"
  ]
  node [
    id 36
    label "mizerny"
  ]
  node [
    id 37
    label "zareagowanie"
  ]
  node [
    id 38
    label "opanowanie"
  ]
  node [
    id 39
    label "doznanie"
  ]
  node [
    id 40
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 41
    label "intuition"
  ]
  node [
    id 42
    label "wiedza"
  ]
  node [
    id 43
    label "ekstraspekcja"
  ]
  node [
    id 44
    label "os&#322;upienie"
  ]
  node [
    id 45
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 46
    label "smell"
  ]
  node [
    id 47
    label "zdarzenie_si&#281;"
  ]
  node [
    id 48
    label "feeling"
  ]
  node [
    id 49
    label "stan"
  ]
  node [
    id 50
    label "temper"
  ]
  node [
    id 51
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 52
    label "mechanizm_obronny"
  ]
  node [
    id 53
    label "nastr&#243;j"
  ]
  node [
    id 54
    label "state"
  ]
  node [
    id 55
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 56
    label "samopoczucie"
  ]
  node [
    id 57
    label "fondness"
  ]
  node [
    id 58
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 59
    label "opinion"
  ]
  node [
    id 60
    label "wypowied&#378;"
  ]
  node [
    id 61
    label "zmatowienie"
  ]
  node [
    id 62
    label "wpa&#347;&#263;"
  ]
  node [
    id 63
    label "grupa"
  ]
  node [
    id 64
    label "wokal"
  ]
  node [
    id 65
    label "note"
  ]
  node [
    id 66
    label "wydawa&#263;"
  ]
  node [
    id 67
    label "nakaz"
  ]
  node [
    id 68
    label "regestr"
  ]
  node [
    id 69
    label "&#347;piewak_operowy"
  ]
  node [
    id 70
    label "matowie&#263;"
  ]
  node [
    id 71
    label "wpada&#263;"
  ]
  node [
    id 72
    label "stanowisko"
  ]
  node [
    id 73
    label "zjawisko"
  ]
  node [
    id 74
    label "mutacja"
  ]
  node [
    id 75
    label "partia"
  ]
  node [
    id 76
    label "&#347;piewak"
  ]
  node [
    id 77
    label "emisja"
  ]
  node [
    id 78
    label "brzmienie"
  ]
  node [
    id 79
    label "zmatowie&#263;"
  ]
  node [
    id 80
    label "wydanie"
  ]
  node [
    id 81
    label "zesp&#243;&#322;"
  ]
  node [
    id 82
    label "wyda&#263;"
  ]
  node [
    id 83
    label "zdolno&#347;&#263;"
  ]
  node [
    id 84
    label "decyzja"
  ]
  node [
    id 85
    label "wpadni&#281;cie"
  ]
  node [
    id 86
    label "linia_melodyczna"
  ]
  node [
    id 87
    label "wpadanie"
  ]
  node [
    id 88
    label "onomatopeja"
  ]
  node [
    id 89
    label "sound"
  ]
  node [
    id 90
    label "matowienie"
  ]
  node [
    id 91
    label "ch&#243;rzysta"
  ]
  node [
    id 92
    label "d&#378;wi&#281;k"
  ]
  node [
    id 93
    label "foniatra"
  ]
  node [
    id 94
    label "&#347;piewaczka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
]
