graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9622641509433962
  density 0.03773584905660377
  graphCliqueNumber 2
  node [
    id 0
    label "tomasz"
    origin "text"
  ]
  node [
    id 1
    label "musia&#322;"
    origin "text"
  ]
  node [
    id 2
    label "wspania&#322;y"
    origin "text"
  ]
  node [
    id 3
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 4
    label "sport"
    origin "text"
  ]
  node [
    id 5
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 6
    label "bariera"
    origin "text"
  ]
  node [
    id 7
    label "pokonanie"
    origin "text"
  ]
  node [
    id 8
    label "pomy&#347;lny"
  ]
  node [
    id 9
    label "warto&#347;ciowy"
  ]
  node [
    id 10
    label "pozytywny"
  ]
  node [
    id 11
    label "wspaniale"
  ]
  node [
    id 12
    label "zajebisty"
  ]
  node [
    id 13
    label "dobry"
  ]
  node [
    id 14
    label "&#347;wietnie"
  ]
  node [
    id 15
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 16
    label "spania&#322;y"
  ]
  node [
    id 17
    label "och&#281;do&#380;ny"
  ]
  node [
    id 18
    label "bogato"
  ]
  node [
    id 19
    label "cz&#322;owiek"
  ]
  node [
    id 20
    label "czyn"
  ]
  node [
    id 21
    label "przedstawiciel"
  ]
  node [
    id 22
    label "ilustracja"
  ]
  node [
    id 23
    label "fakt"
  ]
  node [
    id 24
    label "zaatakowanie"
  ]
  node [
    id 25
    label "usportowi&#263;"
  ]
  node [
    id 26
    label "atakowanie"
  ]
  node [
    id 27
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 28
    label "zgrupowanie"
  ]
  node [
    id 29
    label "zaatakowa&#263;"
  ]
  node [
    id 30
    label "usportowienie"
  ]
  node [
    id 31
    label "sokolstwo"
  ]
  node [
    id 32
    label "kultura_fizyczna"
  ]
  node [
    id 33
    label "atakowa&#263;"
  ]
  node [
    id 34
    label "czyj&#347;"
  ]
  node [
    id 35
    label "m&#261;&#380;"
  ]
  node [
    id 36
    label "ochrona"
  ]
  node [
    id 37
    label "trudno&#347;&#263;"
  ]
  node [
    id 38
    label "parapet"
  ]
  node [
    id 39
    label "obstruction"
  ]
  node [
    id 40
    label "pasmo"
  ]
  node [
    id 41
    label "przeszkoda"
  ]
  node [
    id 42
    label "zrobienie"
  ]
  node [
    id 43
    label "zwojowanie"
  ]
  node [
    id 44
    label "beat"
  ]
  node [
    id 45
    label "zapanowanie"
  ]
  node [
    id 46
    label "zniesienie"
  ]
  node [
    id 47
    label "poradzenie_sobie"
  ]
  node [
    id 48
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 49
    label "wygrywanie"
  ]
  node [
    id 50
    label "theodolite"
  ]
  node [
    id 51
    label "Tomasz"
  ]
  node [
    id 52
    label "Musia&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 51
    target 52
  ]
]
