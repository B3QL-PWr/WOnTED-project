graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.0199004975124377
  density 0.010099502487562188
  graphCliqueNumber 2
  node [
    id 0
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 1
    label "czas"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "blog"
    origin "text"
  ]
  node [
    id 4
    label "learning"
    origin "text"
  ]
  node [
    id 5
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "zestawienie"
    origin "text"
  ]
  node [
    id 8
    label "dobry"
    origin "text"
  ]
  node [
    id 9
    label "zdanie"
    origin "text"
  ]
  node [
    id 10
    label "autor"
    origin "text"
  ]
  node [
    id 11
    label "strona"
    origin "text"
  ]
  node [
    id 12
    label "darmowy"
    origin "text"
  ]
  node [
    id 13
    label "kurs"
    origin "text"
  ]
  node [
    id 14
    label "jako&#347;"
  ]
  node [
    id 15
    label "charakterystyczny"
  ]
  node [
    id 16
    label "ciekawy"
  ]
  node [
    id 17
    label "jako_tako"
  ]
  node [
    id 18
    label "dziwny"
  ]
  node [
    id 19
    label "niez&#322;y"
  ]
  node [
    id 20
    label "przyzwoity"
  ]
  node [
    id 21
    label "czasokres"
  ]
  node [
    id 22
    label "trawienie"
  ]
  node [
    id 23
    label "kategoria_gramatyczna"
  ]
  node [
    id 24
    label "period"
  ]
  node [
    id 25
    label "odczyt"
  ]
  node [
    id 26
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 27
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 28
    label "chwila"
  ]
  node [
    id 29
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 30
    label "poprzedzenie"
  ]
  node [
    id 31
    label "koniugacja"
  ]
  node [
    id 32
    label "dzieje"
  ]
  node [
    id 33
    label "poprzedzi&#263;"
  ]
  node [
    id 34
    label "przep&#322;ywanie"
  ]
  node [
    id 35
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 36
    label "odwlekanie_si&#281;"
  ]
  node [
    id 37
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 38
    label "Zeitgeist"
  ]
  node [
    id 39
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 40
    label "okres_czasu"
  ]
  node [
    id 41
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 42
    label "pochodzi&#263;"
  ]
  node [
    id 43
    label "schy&#322;ek"
  ]
  node [
    id 44
    label "czwarty_wymiar"
  ]
  node [
    id 45
    label "chronometria"
  ]
  node [
    id 46
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 47
    label "poprzedzanie"
  ]
  node [
    id 48
    label "pogoda"
  ]
  node [
    id 49
    label "zegar"
  ]
  node [
    id 50
    label "pochodzenie"
  ]
  node [
    id 51
    label "poprzedza&#263;"
  ]
  node [
    id 52
    label "trawi&#263;"
  ]
  node [
    id 53
    label "time_period"
  ]
  node [
    id 54
    label "rachuba_czasu"
  ]
  node [
    id 55
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 56
    label "czasoprzestrze&#324;"
  ]
  node [
    id 57
    label "laba"
  ]
  node [
    id 58
    label "komcio"
  ]
  node [
    id 59
    label "blogosfera"
  ]
  node [
    id 60
    label "pami&#281;tnik"
  ]
  node [
    id 61
    label "figurowa&#263;"
  ]
  node [
    id 62
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 63
    label "set"
  ]
  node [
    id 64
    label "tekst"
  ]
  node [
    id 65
    label "sumariusz"
  ]
  node [
    id 66
    label "obrot&#243;wka"
  ]
  node [
    id 67
    label "z&#322;o&#380;enie"
  ]
  node [
    id 68
    label "strata"
  ]
  node [
    id 69
    label "pozycja"
  ]
  node [
    id 70
    label "wyliczanka"
  ]
  node [
    id 71
    label "stock"
  ]
  node [
    id 72
    label "deficyt"
  ]
  node [
    id 73
    label "informacja"
  ]
  node [
    id 74
    label "zanalizowanie"
  ]
  node [
    id 75
    label "ustawienie"
  ]
  node [
    id 76
    label "przedstawienie"
  ]
  node [
    id 77
    label "sprawozdanie_finansowe"
  ]
  node [
    id 78
    label "catalog"
  ]
  node [
    id 79
    label "z&#322;&#261;czenie"
  ]
  node [
    id 80
    label "z&#322;amanie"
  ]
  node [
    id 81
    label "kompozycja"
  ]
  node [
    id 82
    label "wyra&#380;enie"
  ]
  node [
    id 83
    label "count"
  ]
  node [
    id 84
    label "zbi&#243;r"
  ]
  node [
    id 85
    label "comparison"
  ]
  node [
    id 86
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 87
    label "analiza"
  ]
  node [
    id 88
    label "composition"
  ]
  node [
    id 89
    label "book"
  ]
  node [
    id 90
    label "pomy&#347;lny"
  ]
  node [
    id 91
    label "skuteczny"
  ]
  node [
    id 92
    label "moralny"
  ]
  node [
    id 93
    label "korzystny"
  ]
  node [
    id 94
    label "odpowiedni"
  ]
  node [
    id 95
    label "zwrot"
  ]
  node [
    id 96
    label "dobrze"
  ]
  node [
    id 97
    label "pozytywny"
  ]
  node [
    id 98
    label "grzeczny"
  ]
  node [
    id 99
    label "powitanie"
  ]
  node [
    id 100
    label "mi&#322;y"
  ]
  node [
    id 101
    label "dobroczynny"
  ]
  node [
    id 102
    label "pos&#322;uszny"
  ]
  node [
    id 103
    label "ca&#322;y"
  ]
  node [
    id 104
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 105
    label "czw&#243;rka"
  ]
  node [
    id 106
    label "spokojny"
  ]
  node [
    id 107
    label "&#347;mieszny"
  ]
  node [
    id 108
    label "drogi"
  ]
  node [
    id 109
    label "attitude"
  ]
  node [
    id 110
    label "system"
  ]
  node [
    id 111
    label "fraza"
  ]
  node [
    id 112
    label "prison_term"
  ]
  node [
    id 113
    label "adjudication"
  ]
  node [
    id 114
    label "przekazanie"
  ]
  node [
    id 115
    label "pass"
  ]
  node [
    id 116
    label "okres"
  ]
  node [
    id 117
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 118
    label "wypowiedzenie"
  ]
  node [
    id 119
    label "konektyw"
  ]
  node [
    id 120
    label "zaliczenie"
  ]
  node [
    id 121
    label "stanowisko"
  ]
  node [
    id 122
    label "powierzenie"
  ]
  node [
    id 123
    label "antylogizm"
  ]
  node [
    id 124
    label "zmuszenie"
  ]
  node [
    id 125
    label "szko&#322;a"
  ]
  node [
    id 126
    label "pomys&#322;odawca"
  ]
  node [
    id 127
    label "kszta&#322;ciciel"
  ]
  node [
    id 128
    label "tworzyciel"
  ]
  node [
    id 129
    label "&#347;w"
  ]
  node [
    id 130
    label "wykonawca"
  ]
  node [
    id 131
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 132
    label "skr&#281;canie"
  ]
  node [
    id 133
    label "voice"
  ]
  node [
    id 134
    label "forma"
  ]
  node [
    id 135
    label "internet"
  ]
  node [
    id 136
    label "skr&#281;ci&#263;"
  ]
  node [
    id 137
    label "kartka"
  ]
  node [
    id 138
    label "orientowa&#263;"
  ]
  node [
    id 139
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 140
    label "powierzchnia"
  ]
  node [
    id 141
    label "plik"
  ]
  node [
    id 142
    label "bok"
  ]
  node [
    id 143
    label "pagina"
  ]
  node [
    id 144
    label "orientowanie"
  ]
  node [
    id 145
    label "fragment"
  ]
  node [
    id 146
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 147
    label "s&#261;d"
  ]
  node [
    id 148
    label "skr&#281;ca&#263;"
  ]
  node [
    id 149
    label "g&#243;ra"
  ]
  node [
    id 150
    label "serwis_internetowy"
  ]
  node [
    id 151
    label "orientacja"
  ]
  node [
    id 152
    label "linia"
  ]
  node [
    id 153
    label "skr&#281;cenie"
  ]
  node [
    id 154
    label "layout"
  ]
  node [
    id 155
    label "zorientowa&#263;"
  ]
  node [
    id 156
    label "zorientowanie"
  ]
  node [
    id 157
    label "obiekt"
  ]
  node [
    id 158
    label "podmiot"
  ]
  node [
    id 159
    label "ty&#322;"
  ]
  node [
    id 160
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 161
    label "logowanie"
  ]
  node [
    id 162
    label "adres_internetowy"
  ]
  node [
    id 163
    label "uj&#281;cie"
  ]
  node [
    id 164
    label "prz&#243;d"
  ]
  node [
    id 165
    label "posta&#263;"
  ]
  node [
    id 166
    label "darmowo"
  ]
  node [
    id 167
    label "seria"
  ]
  node [
    id 168
    label "stawka"
  ]
  node [
    id 169
    label "course"
  ]
  node [
    id 170
    label "way"
  ]
  node [
    id 171
    label "zaj&#281;cia"
  ]
  node [
    id 172
    label "grupa"
  ]
  node [
    id 173
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 174
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 175
    label "rok"
  ]
  node [
    id 176
    label "Lira"
  ]
  node [
    id 177
    label "cedu&#322;a"
  ]
  node [
    id 178
    label "zwy&#380;kowanie"
  ]
  node [
    id 179
    label "spos&#243;b"
  ]
  node [
    id 180
    label "passage"
  ]
  node [
    id 181
    label "deprecjacja"
  ]
  node [
    id 182
    label "przejazd"
  ]
  node [
    id 183
    label "drive"
  ]
  node [
    id 184
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 185
    label "bearing"
  ]
  node [
    id 186
    label "przeorientowywa&#263;"
  ]
  node [
    id 187
    label "trasa"
  ]
  node [
    id 188
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 189
    label "manner"
  ]
  node [
    id 190
    label "nauka"
  ]
  node [
    id 191
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 192
    label "przeorientowa&#263;"
  ]
  node [
    id 193
    label "kierunek"
  ]
  node [
    id 194
    label "przeorientowanie"
  ]
  node [
    id 195
    label "zni&#380;kowanie"
  ]
  node [
    id 196
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 197
    label "przeorientowywanie"
  ]
  node [
    id 198
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 199
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 200
    label "klasa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 13
    target 177
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 180
  ]
  edge [
    source 13
    target 181
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
]
