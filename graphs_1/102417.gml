graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.038095238095238
  density 0.019597069597069597
  graphCliqueNumber 3
  node [
    id 0
    label "okazja"
    origin "text"
  ]
  node [
    id 1
    label "urodziny"
    origin "text"
  ]
  node [
    id 2
    label "dwutygodnik"
    origin "text"
  ]
  node [
    id 3
    label "czwartek"
    origin "text"
  ]
  node [
    id 4
    label "sobota"
    origin "text"
  ]
  node [
    id 5
    label "przy"
    origin "text"
  ]
  node [
    id 6
    label "ch&#322;odny"
    origin "text"
  ]
  node [
    id 7
    label "warszawa"
    origin "text"
  ]
  node [
    id 8
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "seria"
    origin "text"
  ]
  node [
    id 11
    label "impreza"
    origin "text"
  ]
  node [
    id 12
    label "tym"
    origin "text"
  ]
  node [
    id 13
    label "prowadzony"
    origin "text"
  ]
  node [
    id 14
    label "przez"
    origin "text"
  ]
  node [
    id 15
    label "nasa"
    origin "text"
  ]
  node [
    id 16
    label "dyskusja"
    origin "text"
  ]
  node [
    id 17
    label "kultura"
    origin "text"
  ]
  node [
    id 18
    label "atrakcyjny"
  ]
  node [
    id 19
    label "oferta"
  ]
  node [
    id 20
    label "adeptness"
  ]
  node [
    id 21
    label "okazka"
  ]
  node [
    id 22
    label "wydarzenie"
  ]
  node [
    id 23
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 24
    label "podw&#243;zka"
  ]
  node [
    id 25
    label "autostop"
  ]
  node [
    id 26
    label "sytuacja"
  ]
  node [
    id 27
    label "jubileusz"
  ]
  node [
    id 28
    label "&#347;wi&#281;to"
  ]
  node [
    id 29
    label "pocz&#261;tek"
  ]
  node [
    id 30
    label "czasopismo"
  ]
  node [
    id 31
    label "dzie&#324;_powszedni"
  ]
  node [
    id 32
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 33
    label "Wielki_Czwartek"
  ]
  node [
    id 34
    label "tydzie&#324;"
  ]
  node [
    id 35
    label "weekend"
  ]
  node [
    id 36
    label "Wielka_Sobota"
  ]
  node [
    id 37
    label "och&#322;odzenie"
  ]
  node [
    id 38
    label "sch&#322;adzanie"
  ]
  node [
    id 39
    label "rozs&#261;dny"
  ]
  node [
    id 40
    label "opanowany"
  ]
  node [
    id 41
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 42
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 43
    label "ch&#322;odno"
  ]
  node [
    id 44
    label "zi&#281;bienie"
  ]
  node [
    id 45
    label "niesympatyczny"
  ]
  node [
    id 46
    label "Warszawa"
  ]
  node [
    id 47
    label "samoch&#243;d"
  ]
  node [
    id 48
    label "fastback"
  ]
  node [
    id 49
    label "reserve"
  ]
  node [
    id 50
    label "przej&#347;&#263;"
  ]
  node [
    id 51
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 52
    label "stage_set"
  ]
  node [
    id 53
    label "partia"
  ]
  node [
    id 54
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 55
    label "sekwencja"
  ]
  node [
    id 56
    label "komplet"
  ]
  node [
    id 57
    label "przebieg"
  ]
  node [
    id 58
    label "zestawienie"
  ]
  node [
    id 59
    label "jednostka_systematyczna"
  ]
  node [
    id 60
    label "d&#378;wi&#281;k"
  ]
  node [
    id 61
    label "line"
  ]
  node [
    id 62
    label "zbi&#243;r"
  ]
  node [
    id 63
    label "produkcja"
  ]
  node [
    id 64
    label "set"
  ]
  node [
    id 65
    label "jednostka"
  ]
  node [
    id 66
    label "party"
  ]
  node [
    id 67
    label "rozrywka"
  ]
  node [
    id 68
    label "przyj&#281;cie"
  ]
  node [
    id 69
    label "impra"
  ]
  node [
    id 70
    label "rozmowa"
  ]
  node [
    id 71
    label "sympozjon"
  ]
  node [
    id 72
    label "conference"
  ]
  node [
    id 73
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 74
    label "przedmiot"
  ]
  node [
    id 75
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 76
    label "Wsch&#243;d"
  ]
  node [
    id 77
    label "rzecz"
  ]
  node [
    id 78
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 79
    label "sztuka"
  ]
  node [
    id 80
    label "religia"
  ]
  node [
    id 81
    label "przejmowa&#263;"
  ]
  node [
    id 82
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 83
    label "makrokosmos"
  ]
  node [
    id 84
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 85
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 86
    label "zjawisko"
  ]
  node [
    id 87
    label "praca_rolnicza"
  ]
  node [
    id 88
    label "tradycja"
  ]
  node [
    id 89
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 90
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 91
    label "przejmowanie"
  ]
  node [
    id 92
    label "cecha"
  ]
  node [
    id 93
    label "asymilowanie_si&#281;"
  ]
  node [
    id 94
    label "przej&#261;&#263;"
  ]
  node [
    id 95
    label "hodowla"
  ]
  node [
    id 96
    label "brzoskwiniarnia"
  ]
  node [
    id 97
    label "populace"
  ]
  node [
    id 98
    label "konwencja"
  ]
  node [
    id 99
    label "propriety"
  ]
  node [
    id 100
    label "jako&#347;&#263;"
  ]
  node [
    id 101
    label "kuchnia"
  ]
  node [
    id 102
    label "zwyczaj"
  ]
  node [
    id 103
    label "przej&#281;cie"
  ]
  node [
    id 104
    label "ro&#347;linno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 70
  ]
  edge [
    source 16
    target 71
  ]
  edge [
    source 16
    target 72
  ]
  edge [
    source 17
    target 73
  ]
  edge [
    source 17
    target 74
  ]
  edge [
    source 17
    target 75
  ]
  edge [
    source 17
    target 76
  ]
  edge [
    source 17
    target 77
  ]
  edge [
    source 17
    target 78
  ]
  edge [
    source 17
    target 79
  ]
  edge [
    source 17
    target 80
  ]
  edge [
    source 17
    target 81
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 17
    target 85
  ]
  edge [
    source 17
    target 86
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 17
    target 91
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 17
    target 93
  ]
  edge [
    source 17
    target 94
  ]
  edge [
    source 17
    target 95
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 97
  ]
  edge [
    source 17
    target 98
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
]
