graph [
  maxDegree 15
  minDegree 1
  meanDegree 2
  density 0.041666666666666664
  graphCliqueNumber 3
  node [
    id 0
    label "krytyk"
    origin "text"
  ]
  node [
    id 1
    label "migracja"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "podci&#261;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pod"
    origin "text"
  ]
  node [
    id 5
    label "hate"
    origin "text"
  ]
  node [
    id 6
    label "speech"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 8
    label "&#347;ciga&#263;"
    origin "text"
  ]
  node [
    id 9
    label "przest&#281;pstwo"
    origin "text"
  ]
  node [
    id 10
    label "przeciwnik"
  ]
  node [
    id 11
    label "publicysta"
  ]
  node [
    id 12
    label "krytyka"
  ]
  node [
    id 13
    label "exodus"
  ]
  node [
    id 14
    label "ruch"
  ]
  node [
    id 15
    label "si&#281;ga&#263;"
  ]
  node [
    id 16
    label "trwa&#263;"
  ]
  node [
    id 17
    label "obecno&#347;&#263;"
  ]
  node [
    id 18
    label "stan"
  ]
  node [
    id 19
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 20
    label "stand"
  ]
  node [
    id 21
    label "mie&#263;_miejsce"
  ]
  node [
    id 22
    label "uczestniczy&#263;"
  ]
  node [
    id 23
    label "chodzi&#263;"
  ]
  node [
    id 24
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 25
    label "equal"
  ]
  node [
    id 26
    label "poprawi&#263;"
  ]
  node [
    id 27
    label "przybli&#380;y&#263;"
  ]
  node [
    id 28
    label "podnie&#347;&#263;"
  ]
  node [
    id 29
    label "enhance"
  ]
  node [
    id 30
    label "zakwalifikowa&#263;"
  ]
  node [
    id 31
    label "raise"
  ]
  node [
    id 32
    label "przem&#243;wienie"
  ]
  node [
    id 33
    label "czyj&#347;"
  ]
  node [
    id 34
    label "m&#261;&#380;"
  ]
  node [
    id 35
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 36
    label "straszy&#263;"
  ]
  node [
    id 37
    label "prosecute"
  ]
  node [
    id 38
    label "kara&#263;"
  ]
  node [
    id 39
    label "usi&#322;owa&#263;"
  ]
  node [
    id 40
    label "poszukiwa&#263;"
  ]
  node [
    id 41
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 42
    label "brudny"
  ]
  node [
    id 43
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 44
    label "sprawstwo"
  ]
  node [
    id 45
    label "crime"
  ]
  node [
    id 46
    label "nowa"
  ]
  node [
    id 47
    label "wspania&#322;y"
  ]
  node [
    id 48
    label "&#347;wiat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
]
