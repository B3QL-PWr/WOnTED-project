graph [
  maxDegree 14
  minDegree 1
  meanDegree 2.056338028169014
  density 0.02937625754527163
  graphCliqueNumber 4
  node [
    id 0
    label "lotnisko"
    origin "text"
  ]
  node [
    id 1
    label "warszawski"
    origin "text"
  ]
  node [
    id 2
    label "bem"
    origin "text"
  ]
  node [
    id 3
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 6
    label "uroczysto&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przekaza&#263;"
    origin "text"
  ]
  node [
    id 8
    label "policja"
    origin "text"
  ]
  node [
    id 9
    label "dwa"
    origin "text"
  ]
  node [
    id 10
    label "&#347;mig&#322;owiec"
    origin "text"
  ]
  node [
    id 11
    label "sekunda"
    origin "text"
  ]
  node [
    id 12
    label "black"
    origin "text"
  ]
  node [
    id 13
    label "hawk"
    origin "text"
  ]
  node [
    id 14
    label "hala"
  ]
  node [
    id 15
    label "droga_ko&#322;owania"
  ]
  node [
    id 16
    label "baza"
  ]
  node [
    id 17
    label "p&#322;yta_postojowa"
  ]
  node [
    id 18
    label "wie&#380;a_kontroli_lot&#243;w"
  ]
  node [
    id 19
    label "betonka"
  ]
  node [
    id 20
    label "budowla"
  ]
  node [
    id 21
    label "aerodrom"
  ]
  node [
    id 22
    label "pas_startowy"
  ]
  node [
    id 23
    label "terminal"
  ]
  node [
    id 24
    label "po_warszawsku"
  ]
  node [
    id 25
    label "marmuzela"
  ]
  node [
    id 26
    label "mazowiecki"
  ]
  node [
    id 27
    label "reserve"
  ]
  node [
    id 28
    label "przej&#347;&#263;"
  ]
  node [
    id 29
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 30
    label "doba"
  ]
  node [
    id 31
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 32
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 33
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 34
    label "cecha"
  ]
  node [
    id 35
    label "przysi&#281;ga_wojskowa"
  ]
  node [
    id 36
    label "egzaltacja"
  ]
  node [
    id 37
    label "wydarzenie"
  ]
  node [
    id 38
    label "przyrzeczenie_harcerskie"
  ]
  node [
    id 39
    label "atmosfera"
  ]
  node [
    id 40
    label "patos"
  ]
  node [
    id 41
    label "impart"
  ]
  node [
    id 42
    label "sygna&#322;"
  ]
  node [
    id 43
    label "propagate"
  ]
  node [
    id 44
    label "transfer"
  ]
  node [
    id 45
    label "give"
  ]
  node [
    id 46
    label "wys&#322;a&#263;"
  ]
  node [
    id 47
    label "zrobi&#263;"
  ]
  node [
    id 48
    label "poda&#263;"
  ]
  node [
    id 49
    label "wp&#322;aci&#263;"
  ]
  node [
    id 50
    label "komisariat"
  ]
  node [
    id 51
    label "psiarnia"
  ]
  node [
    id 52
    label "posterunek"
  ]
  node [
    id 53
    label "grupa"
  ]
  node [
    id 54
    label "organ"
  ]
  node [
    id 55
    label "s&#322;u&#380;ba"
  ]
  node [
    id 56
    label "wirop&#322;at"
  ]
  node [
    id 57
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 58
    label "&#347;mig&#322;o"
  ]
  node [
    id 59
    label "minuta"
  ]
  node [
    id 60
    label "tercja"
  ]
  node [
    id 61
    label "milisekunda"
  ]
  node [
    id 62
    label "nanosekunda"
  ]
  node [
    id 63
    label "uk&#322;ad_SI"
  ]
  node [
    id 64
    label "mikrosekunda"
  ]
  node [
    id 65
    label "time"
  ]
  node [
    id 66
    label "jednostka_czasu"
  ]
  node [
    id 67
    label "jednostka"
  ]
  node [
    id 68
    label "70i"
  ]
  node [
    id 69
    label "Black"
  ]
  node [
    id 70
    label "Hawk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 69
    target 70
  ]
]
