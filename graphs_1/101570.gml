graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "rzym"
    origin "text"
  ]
  node [
    id 1
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 2
    label "Nowy_Rok"
  ]
  node [
    id 3
    label "miesi&#261;c"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
]
