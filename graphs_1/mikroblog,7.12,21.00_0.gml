graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9130434782608696
  density 0.08695652173913043
  graphCliqueNumber 2
  node [
    id 0
    label "&#3232;_&#3232;"
    origin "text"
  ]
  node [
    id 1
    label "dziecko"
    origin "text"
  ]
  node [
    id 2
    label "szkola"
    origin "text"
  ]
  node [
    id 3
    label "cz&#322;owiek"
  ]
  node [
    id 4
    label "potomstwo"
  ]
  node [
    id 5
    label "organizm"
  ]
  node [
    id 6
    label "sraluch"
  ]
  node [
    id 7
    label "utulanie"
  ]
  node [
    id 8
    label "pediatra"
  ]
  node [
    id 9
    label "dzieciarnia"
  ]
  node [
    id 10
    label "m&#322;odziak"
  ]
  node [
    id 11
    label "dzieciak"
  ]
  node [
    id 12
    label "utula&#263;"
  ]
  node [
    id 13
    label "potomek"
  ]
  node [
    id 14
    label "pedofil"
  ]
  node [
    id 15
    label "entliczek-pentliczek"
  ]
  node [
    id 16
    label "m&#322;odzik"
  ]
  node [
    id 17
    label "cz&#322;owieczek"
  ]
  node [
    id 18
    label "zwierz&#281;"
  ]
  node [
    id 19
    label "niepe&#322;noletni"
  ]
  node [
    id 20
    label "fledgling"
  ]
  node [
    id 21
    label "utuli&#263;"
  ]
  node [
    id 22
    label "utulenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
]
