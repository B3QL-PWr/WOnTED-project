graph [
  maxDegree 16
  minDegree 1
  meanDegree 2.0294117647058822
  density 0.03028972783143108
  graphCliqueNumber 2
  node [
    id 0
    label "niesamowity"
    origin "text"
  ]
  node [
    id 1
    label "rozlewisko"
    origin "text"
  ]
  node [
    id 2
    label "rzeczny"
    origin "text"
  ]
  node [
    id 3
    label "tajemniczy"
    origin "text"
  ]
  node [
    id 4
    label "ruina"
    origin "text"
  ]
  node [
    id 5
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 6
    label "niespotykany"
    origin "text"
  ]
  node [
    id 7
    label "nigdzie"
    origin "text"
  ]
  node [
    id 8
    label "indziej"
    origin "text"
  ]
  node [
    id 9
    label "zabytek"
    origin "text"
  ]
  node [
    id 10
    label "wiele"
    origin "text"
  ]
  node [
    id 11
    label "naturalny"
    origin "text"
  ]
  node [
    id 12
    label "zjawiskowy"
    origin "text"
  ]
  node [
    id 13
    label "atrakcja"
    origin "text"
  ]
  node [
    id 14
    label "niesamowicie"
  ]
  node [
    id 15
    label "niezwyk&#322;y"
  ]
  node [
    id 16
    label "zbiornik_wodny"
  ]
  node [
    id 17
    label "wodny"
  ]
  node [
    id 18
    label "ciekawy"
  ]
  node [
    id 19
    label "dziwny"
  ]
  node [
    id 20
    label "nastrojowy"
  ]
  node [
    id 21
    label "nieznany"
  ]
  node [
    id 22
    label "intryguj&#261;cy"
  ]
  node [
    id 23
    label "tajemniczo"
  ]
  node [
    id 24
    label "skryty"
  ]
  node [
    id 25
    label "niejednoznaczny"
  ]
  node [
    id 26
    label "descent"
  ]
  node [
    id 27
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 28
    label "stan"
  ]
  node [
    id 29
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 30
    label "zakrystia"
  ]
  node [
    id 31
    label "organizacja_religijna"
  ]
  node [
    id 32
    label "nawa"
  ]
  node [
    id 33
    label "nerwica_eklezjogenna"
  ]
  node [
    id 34
    label "kropielnica"
  ]
  node [
    id 35
    label "prezbiterium"
  ]
  node [
    id 36
    label "wsp&#243;lnota"
  ]
  node [
    id 37
    label "church"
  ]
  node [
    id 38
    label "kruchta"
  ]
  node [
    id 39
    label "Ska&#322;ka"
  ]
  node [
    id 40
    label "kult"
  ]
  node [
    id 41
    label "ub&#322;agalnia"
  ]
  node [
    id 42
    label "dom"
  ]
  node [
    id 43
    label "&#347;wiadectwo"
  ]
  node [
    id 44
    label "przedmiot"
  ]
  node [
    id 45
    label "starzyzna"
  ]
  node [
    id 46
    label "keepsake"
  ]
  node [
    id 47
    label "wiela"
  ]
  node [
    id 48
    label "du&#380;y"
  ]
  node [
    id 49
    label "szczery"
  ]
  node [
    id 50
    label "prawy"
  ]
  node [
    id 51
    label "bezsporny"
  ]
  node [
    id 52
    label "organicznie"
  ]
  node [
    id 53
    label "immanentny"
  ]
  node [
    id 54
    label "naturalnie"
  ]
  node [
    id 55
    label "zwyczajny"
  ]
  node [
    id 56
    label "neutralny"
  ]
  node [
    id 57
    label "zrozumia&#322;y"
  ]
  node [
    id 58
    label "rzeczywisty"
  ]
  node [
    id 59
    label "normalny"
  ]
  node [
    id 60
    label "pierwotny"
  ]
  node [
    id 61
    label "zjawiskowo"
  ]
  node [
    id 62
    label "pi&#281;kny"
  ]
  node [
    id 63
    label "urozmaicenie"
  ]
  node [
    id 64
    label "sensacja"
  ]
  node [
    id 65
    label "ciekawostka"
  ]
  node [
    id 66
    label "punkt"
  ]
  node [
    id 67
    label "urok"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 64
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 67
  ]
]
