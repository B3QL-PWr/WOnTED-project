graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.046153846153846
  density 0.01586165772212284
  graphCliqueNumber 2
  node [
    id 0
    label "jeden"
    origin "text"
  ]
  node [
    id 1
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 2
    label "klasyfikowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zawarto&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "serwis"
    origin "text"
  ]
  node [
    id 5
    label "typ"
    origin "text"
  ]
  node [
    id 6
    label "wika"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "tworzenie"
    origin "text"
  ]
  node [
    id 9
    label "list"
    origin "text"
  ]
  node [
    id 10
    label "zestawienie"
    origin "text"
  ]
  node [
    id 11
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 12
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 14
    label "kieliszek"
  ]
  node [
    id 15
    label "shot"
  ]
  node [
    id 16
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 17
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 18
    label "jaki&#347;"
  ]
  node [
    id 19
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 20
    label "jednolicie"
  ]
  node [
    id 21
    label "w&#243;dka"
  ]
  node [
    id 22
    label "ten"
  ]
  node [
    id 23
    label "ujednolicenie"
  ]
  node [
    id 24
    label "jednakowy"
  ]
  node [
    id 25
    label "model"
  ]
  node [
    id 26
    label "zbi&#243;r"
  ]
  node [
    id 27
    label "tryb"
  ]
  node [
    id 28
    label "narz&#281;dzie"
  ]
  node [
    id 29
    label "nature"
  ]
  node [
    id 30
    label "zalicza&#263;"
  ]
  node [
    id 31
    label "ocenia&#263;"
  ]
  node [
    id 32
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 33
    label "digest"
  ]
  node [
    id 34
    label "wn&#281;trze"
  ]
  node [
    id 35
    label "temat"
  ]
  node [
    id 36
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 37
    label "informacja"
  ]
  node [
    id 38
    label "ilo&#347;&#263;"
  ]
  node [
    id 39
    label "mecz"
  ]
  node [
    id 40
    label "service"
  ]
  node [
    id 41
    label "wytw&#243;r"
  ]
  node [
    id 42
    label "zak&#322;ad"
  ]
  node [
    id 43
    label "us&#322;uga"
  ]
  node [
    id 44
    label "uderzenie"
  ]
  node [
    id 45
    label "doniesienie"
  ]
  node [
    id 46
    label "zastawa"
  ]
  node [
    id 47
    label "YouTube"
  ]
  node [
    id 48
    label "punkt"
  ]
  node [
    id 49
    label "porcja"
  ]
  node [
    id 50
    label "strona"
  ]
  node [
    id 51
    label "cz&#322;owiek"
  ]
  node [
    id 52
    label "gromada"
  ]
  node [
    id 53
    label "autorament"
  ]
  node [
    id 54
    label "przypuszczenie"
  ]
  node [
    id 55
    label "cynk"
  ]
  node [
    id 56
    label "rezultat"
  ]
  node [
    id 57
    label "jednostka_systematyczna"
  ]
  node [
    id 58
    label "kr&#243;lestwo"
  ]
  node [
    id 59
    label "obstawia&#263;"
  ]
  node [
    id 60
    label "design"
  ]
  node [
    id 61
    label "facet"
  ]
  node [
    id 62
    label "variety"
  ]
  node [
    id 63
    label "sztuka"
  ]
  node [
    id 64
    label "antycypacja"
  ]
  node [
    id 65
    label "si&#281;ga&#263;"
  ]
  node [
    id 66
    label "trwa&#263;"
  ]
  node [
    id 67
    label "obecno&#347;&#263;"
  ]
  node [
    id 68
    label "stan"
  ]
  node [
    id 69
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 70
    label "stand"
  ]
  node [
    id 71
    label "mie&#263;_miejsce"
  ]
  node [
    id 72
    label "uczestniczy&#263;"
  ]
  node [
    id 73
    label "chodzi&#263;"
  ]
  node [
    id 74
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 75
    label "equal"
  ]
  node [
    id 76
    label "robienie"
  ]
  node [
    id 77
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 78
    label "pope&#322;nianie"
  ]
  node [
    id 79
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 80
    label "development"
  ]
  node [
    id 81
    label "stanowienie"
  ]
  node [
    id 82
    label "exploitation"
  ]
  node [
    id 83
    label "structure"
  ]
  node [
    id 84
    label "li&#347;&#263;"
  ]
  node [
    id 85
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 86
    label "poczta"
  ]
  node [
    id 87
    label "epistolografia"
  ]
  node [
    id 88
    label "przesy&#322;ka"
  ]
  node [
    id 89
    label "poczta_elektroniczna"
  ]
  node [
    id 90
    label "znaczek_pocztowy"
  ]
  node [
    id 91
    label "figurowa&#263;"
  ]
  node [
    id 92
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 93
    label "set"
  ]
  node [
    id 94
    label "tekst"
  ]
  node [
    id 95
    label "sumariusz"
  ]
  node [
    id 96
    label "obrot&#243;wka"
  ]
  node [
    id 97
    label "z&#322;o&#380;enie"
  ]
  node [
    id 98
    label "strata"
  ]
  node [
    id 99
    label "pozycja"
  ]
  node [
    id 100
    label "wyliczanka"
  ]
  node [
    id 101
    label "stock"
  ]
  node [
    id 102
    label "deficyt"
  ]
  node [
    id 103
    label "zanalizowanie"
  ]
  node [
    id 104
    label "ustawienie"
  ]
  node [
    id 105
    label "przedstawienie"
  ]
  node [
    id 106
    label "sprawozdanie_finansowe"
  ]
  node [
    id 107
    label "catalog"
  ]
  node [
    id 108
    label "z&#322;&#261;czenie"
  ]
  node [
    id 109
    label "z&#322;amanie"
  ]
  node [
    id 110
    label "kompozycja"
  ]
  node [
    id 111
    label "wyra&#380;enie"
  ]
  node [
    id 112
    label "count"
  ]
  node [
    id 113
    label "comparison"
  ]
  node [
    id 114
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 115
    label "analiza"
  ]
  node [
    id 116
    label "composition"
  ]
  node [
    id 117
    label "book"
  ]
  node [
    id 118
    label "dokument"
  ]
  node [
    id 119
    label "towar"
  ]
  node [
    id 120
    label "nag&#322;&#243;wek"
  ]
  node [
    id 121
    label "znak_j&#281;zykowy"
  ]
  node [
    id 122
    label "wyr&#243;b"
  ]
  node [
    id 123
    label "blok"
  ]
  node [
    id 124
    label "line"
  ]
  node [
    id 125
    label "paragraf"
  ]
  node [
    id 126
    label "rodzajnik"
  ]
  node [
    id 127
    label "prawda"
  ]
  node [
    id 128
    label "szkic"
  ]
  node [
    id 129
    label "fragment"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 129
  ]
]
