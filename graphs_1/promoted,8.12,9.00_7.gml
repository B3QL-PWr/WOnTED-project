graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.0994475138121547
  density 0.011663597298956415
  graphCliqueNumber 5
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "dziecko"
    origin "text"
  ]
  node [
    id 2
    label "granica"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "na_przyk&#322;ad"
    origin "text"
  ]
  node [
    id 5
    label "wynik"
    origin "text"
  ]
  node [
    id 6
    label "rozw&#243;d"
    origin "text"
  ]
  node [
    id 7
    label "rodzic"
    origin "text"
  ]
  node [
    id 8
    label "umieszcza&#263;"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "rodzina"
    origin "text"
  ]
  node [
    id 11
    label "zast&#281;pczy"
    origin "text"
  ]
  node [
    id 12
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 13
    label "trafi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 15
    label "ten"
    origin "text"
  ]
  node [
    id 16
    label "sam"
    origin "text"
  ]
  node [
    id 17
    label "to&#380;samo&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "kulturowy"
    origin "text"
  ]
  node [
    id 19
    label "lacki"
  ]
  node [
    id 20
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 21
    label "przedmiot"
  ]
  node [
    id 22
    label "sztajer"
  ]
  node [
    id 23
    label "drabant"
  ]
  node [
    id 24
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 25
    label "polak"
  ]
  node [
    id 26
    label "pierogi_ruskie"
  ]
  node [
    id 27
    label "krakowiak"
  ]
  node [
    id 28
    label "Polish"
  ]
  node [
    id 29
    label "j&#281;zyk"
  ]
  node [
    id 30
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 31
    label "oberek"
  ]
  node [
    id 32
    label "po_polsku"
  ]
  node [
    id 33
    label "mazur"
  ]
  node [
    id 34
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 35
    label "chodzony"
  ]
  node [
    id 36
    label "skoczny"
  ]
  node [
    id 37
    label "ryba_po_grecku"
  ]
  node [
    id 38
    label "goniony"
  ]
  node [
    id 39
    label "polsko"
  ]
  node [
    id 40
    label "cz&#322;owiek"
  ]
  node [
    id 41
    label "potomstwo"
  ]
  node [
    id 42
    label "organizm"
  ]
  node [
    id 43
    label "sraluch"
  ]
  node [
    id 44
    label "utulanie"
  ]
  node [
    id 45
    label "pediatra"
  ]
  node [
    id 46
    label "dzieciarnia"
  ]
  node [
    id 47
    label "m&#322;odziak"
  ]
  node [
    id 48
    label "dzieciak"
  ]
  node [
    id 49
    label "utula&#263;"
  ]
  node [
    id 50
    label "potomek"
  ]
  node [
    id 51
    label "entliczek-pentliczek"
  ]
  node [
    id 52
    label "pedofil"
  ]
  node [
    id 53
    label "m&#322;odzik"
  ]
  node [
    id 54
    label "cz&#322;owieczek"
  ]
  node [
    id 55
    label "zwierz&#281;"
  ]
  node [
    id 56
    label "niepe&#322;noletni"
  ]
  node [
    id 57
    label "fledgling"
  ]
  node [
    id 58
    label "utuli&#263;"
  ]
  node [
    id 59
    label "utulenie"
  ]
  node [
    id 60
    label "zakres"
  ]
  node [
    id 61
    label "Ural"
  ]
  node [
    id 62
    label "koniec"
  ]
  node [
    id 63
    label "kres"
  ]
  node [
    id 64
    label "granice"
  ]
  node [
    id 65
    label "granica_pa&#324;stwa"
  ]
  node [
    id 66
    label "pu&#322;ap"
  ]
  node [
    id 67
    label "frontier"
  ]
  node [
    id 68
    label "end"
  ]
  node [
    id 69
    label "miara"
  ]
  node [
    id 70
    label "poj&#281;cie"
  ]
  node [
    id 71
    label "przej&#347;cie"
  ]
  node [
    id 72
    label "typ"
  ]
  node [
    id 73
    label "dzia&#322;anie"
  ]
  node [
    id 74
    label "przyczyna"
  ]
  node [
    id 75
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 76
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 77
    label "zaokr&#261;glenie"
  ]
  node [
    id 78
    label "event"
  ]
  node [
    id 79
    label "rezultat"
  ]
  node [
    id 80
    label "ekspartner"
  ]
  node [
    id 81
    label "rozbita_rodzina"
  ]
  node [
    id 82
    label "uniewa&#380;nienie"
  ]
  node [
    id 83
    label "separation"
  ]
  node [
    id 84
    label "rozstanie"
  ]
  node [
    id 85
    label "opiekun"
  ]
  node [
    id 86
    label "wapniak"
  ]
  node [
    id 87
    label "rodzic_chrzestny"
  ]
  node [
    id 88
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 89
    label "rodzice"
  ]
  node [
    id 90
    label "zmienia&#263;"
  ]
  node [
    id 91
    label "plasowa&#263;"
  ]
  node [
    id 92
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 93
    label "pomieszcza&#263;"
  ]
  node [
    id 94
    label "wpiernicza&#263;"
  ]
  node [
    id 95
    label "robi&#263;"
  ]
  node [
    id 96
    label "accommodate"
  ]
  node [
    id 97
    label "umie&#347;ci&#263;"
  ]
  node [
    id 98
    label "venture"
  ]
  node [
    id 99
    label "powodowa&#263;"
  ]
  node [
    id 100
    label "okre&#347;la&#263;"
  ]
  node [
    id 101
    label "si&#281;ga&#263;"
  ]
  node [
    id 102
    label "trwa&#263;"
  ]
  node [
    id 103
    label "obecno&#347;&#263;"
  ]
  node [
    id 104
    label "stan"
  ]
  node [
    id 105
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 106
    label "stand"
  ]
  node [
    id 107
    label "mie&#263;_miejsce"
  ]
  node [
    id 108
    label "uczestniczy&#263;"
  ]
  node [
    id 109
    label "chodzi&#263;"
  ]
  node [
    id 110
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 111
    label "equal"
  ]
  node [
    id 112
    label "krewni"
  ]
  node [
    id 113
    label "Firlejowie"
  ]
  node [
    id 114
    label "Ossoli&#324;scy"
  ]
  node [
    id 115
    label "grupa"
  ]
  node [
    id 116
    label "rodze&#324;stwo"
  ]
  node [
    id 117
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 118
    label "rz&#261;d"
  ]
  node [
    id 119
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 120
    label "przyjaciel_domu"
  ]
  node [
    id 121
    label "Ostrogscy"
  ]
  node [
    id 122
    label "theater"
  ]
  node [
    id 123
    label "dom_rodzinny"
  ]
  node [
    id 124
    label "Soplicowie"
  ]
  node [
    id 125
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 126
    label "Czartoryscy"
  ]
  node [
    id 127
    label "family"
  ]
  node [
    id 128
    label "kin"
  ]
  node [
    id 129
    label "bliscy"
  ]
  node [
    id 130
    label "powinowaci"
  ]
  node [
    id 131
    label "Sapiehowie"
  ]
  node [
    id 132
    label "ordynacja"
  ]
  node [
    id 133
    label "jednostka_systematyczna"
  ]
  node [
    id 134
    label "zbi&#243;r"
  ]
  node [
    id 135
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 136
    label "Kossakowie"
  ]
  node [
    id 137
    label "dom"
  ]
  node [
    id 138
    label "zast&#281;pczo"
  ]
  node [
    id 139
    label "drugi"
  ]
  node [
    id 140
    label "przydatny"
  ]
  node [
    id 141
    label "uprawi&#263;"
  ]
  node [
    id 142
    label "gotowy"
  ]
  node [
    id 143
    label "might"
  ]
  node [
    id 144
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 145
    label "przypasowa&#263;"
  ]
  node [
    id 146
    label "wpa&#347;&#263;"
  ]
  node [
    id 147
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 148
    label "spotka&#263;"
  ]
  node [
    id 149
    label "dotrze&#263;"
  ]
  node [
    id 150
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 151
    label "happen"
  ]
  node [
    id 152
    label "znale&#378;&#263;"
  ]
  node [
    id 153
    label "hit"
  ]
  node [
    id 154
    label "pocisk"
  ]
  node [
    id 155
    label "stumble"
  ]
  node [
    id 156
    label "dolecie&#263;"
  ]
  node [
    id 157
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 158
    label "wy&#322;&#261;czny"
  ]
  node [
    id 159
    label "okre&#347;lony"
  ]
  node [
    id 160
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 161
    label "sklep"
  ]
  node [
    id 162
    label "identity"
  ]
  node [
    id 163
    label "pesel"
  ]
  node [
    id 164
    label "uniformizm"
  ]
  node [
    id 165
    label "imi&#281;"
  ]
  node [
    id 166
    label "depersonalizacja"
  ]
  node [
    id 167
    label "to&#380;samo&#347;&#263;_osobista"
  ]
  node [
    id 168
    label "dane"
  ]
  node [
    id 169
    label "adres"
  ]
  node [
    id 170
    label "nazwisko"
  ]
  node [
    id 171
    label "self-consciousness"
  ]
  node [
    id 172
    label "r&#243;wno&#347;&#263;"
  ]
  node [
    id 173
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 174
    label "NN"
  ]
  node [
    id 175
    label "kulturowo"
  ]
  node [
    id 176
    label "RMF"
  ]
  node [
    id 177
    label "FM"
  ]
  node [
    id 178
    label "Katarzyna"
  ]
  node [
    id 179
    label "Szyma&#324;ska"
  ]
  node [
    id 180
    label "Borginon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 70
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 176
    target 178
  ]
  edge [
    source 176
    target 179
  ]
  edge [
    source 176
    target 180
  ]
  edge [
    source 177
    target 178
  ]
  edge [
    source 177
    target 179
  ]
  edge [
    source 177
    target 180
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 178
    target 180
  ]
  edge [
    source 179
    target 180
  ]
]
