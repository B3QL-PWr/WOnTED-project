graph [
  maxDegree 4
  minDegree 1
  meanDegree 2.235294117647059
  density 0.13970588235294118
  graphCliqueNumber 5
  node [
    id 0
    label "hortensja"
    origin "text"
  ]
  node [
    id 1
    label "pi&#261;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "krzew_ozdobny"
  ]
  node [
    id 3
    label "hortensjowate"
  ]
  node [
    id 4
    label "ro&#347;lina_kwasolubna"
  ]
  node [
    id 5
    label "wyspa"
  ]
  node [
    id 6
    label "kurylski"
  ]
  node [
    id 7
    label "Korea"
  ]
  node [
    id 8
    label "po&#322;udniowy"
  ]
  node [
    id 9
    label "krytyczny"
  ]
  node [
    id 10
    label "lista"
  ]
  node [
    id 11
    label "ro&#347;lina"
  ]
  node [
    id 12
    label "naczyniowy"
  ]
  node [
    id 13
    label "polski"
  ]
  node [
    id 14
    label "e"
  ]
  node [
    id 15
    label "m&#281;ski"
  ]
  node [
    id 16
    label "McClint"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
]
