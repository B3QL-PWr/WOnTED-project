graph [
  maxDegree 18
  minDegree 1
  meanDegree 2
  density 0.02531645569620253
  graphCliqueNumber 2
  node [
    id 0
    label "kompletny"
    origin "text"
  ]
  node [
    id 1
    label "brak"
    origin "text"
  ]
  node [
    id 2
    label "wyobra&#378;nia"
    origin "text"
  ]
  node [
    id 3
    label "szacunek"
    origin "text"
  ]
  node [
    id 4
    label "dla"
    origin "text"
  ]
  node [
    id 5
    label "dziki"
    origin "text"
  ]
  node [
    id 6
    label "przyroda"
    origin "text"
  ]
  node [
    id 7
    label "popisa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "turysta"
    origin "text"
  ]
  node [
    id 10
    label "bieszczady"
    origin "text"
  ]
  node [
    id 11
    label "kompletnie"
  ]
  node [
    id 12
    label "w_pizdu"
  ]
  node [
    id 13
    label "zupe&#322;ny"
  ]
  node [
    id 14
    label "pe&#322;ny"
  ]
  node [
    id 15
    label "prywatywny"
  ]
  node [
    id 16
    label "defect"
  ]
  node [
    id 17
    label "odej&#347;cie"
  ]
  node [
    id 18
    label "gap"
  ]
  node [
    id 19
    label "kr&#243;tki"
  ]
  node [
    id 20
    label "wyr&#243;b"
  ]
  node [
    id 21
    label "nieistnienie"
  ]
  node [
    id 22
    label "wada"
  ]
  node [
    id 23
    label "odej&#347;&#263;"
  ]
  node [
    id 24
    label "odchodzenie"
  ]
  node [
    id 25
    label "odchodzi&#263;"
  ]
  node [
    id 26
    label "kraina"
  ]
  node [
    id 27
    label "zdolno&#347;&#263;"
  ]
  node [
    id 28
    label "imagineskopia"
  ]
  node [
    id 29
    label "umys&#322;"
  ]
  node [
    id 30
    label "fondness"
  ]
  node [
    id 31
    label "zaimponowanie"
  ]
  node [
    id 32
    label "follow-up"
  ]
  node [
    id 33
    label "szanowa&#263;"
  ]
  node [
    id 34
    label "uhonorowa&#263;"
  ]
  node [
    id 35
    label "honorowanie"
  ]
  node [
    id 36
    label "uszanowa&#263;"
  ]
  node [
    id 37
    label "rewerencja"
  ]
  node [
    id 38
    label "uszanowanie"
  ]
  node [
    id 39
    label "imponowanie"
  ]
  node [
    id 40
    label "uhonorowanie"
  ]
  node [
    id 41
    label "respect"
  ]
  node [
    id 42
    label "przewidzenie"
  ]
  node [
    id 43
    label "honorowa&#263;"
  ]
  node [
    id 44
    label "postawa"
  ]
  node [
    id 45
    label "szacuneczek"
  ]
  node [
    id 46
    label "cz&#322;owiek"
  ]
  node [
    id 47
    label "nieopanowany"
  ]
  node [
    id 48
    label "dziczenie"
  ]
  node [
    id 49
    label "nielegalny"
  ]
  node [
    id 50
    label "szalony"
  ]
  node [
    id 51
    label "nieucywilizowany"
  ]
  node [
    id 52
    label "naturalny"
  ]
  node [
    id 53
    label "podejrzliwy"
  ]
  node [
    id 54
    label "straszny"
  ]
  node [
    id 55
    label "wrogi"
  ]
  node [
    id 56
    label "dziko"
  ]
  node [
    id 57
    label "nieobliczalny"
  ]
  node [
    id 58
    label "zdziczenie"
  ]
  node [
    id 59
    label "ostry"
  ]
  node [
    id 60
    label "nieobyty"
  ]
  node [
    id 61
    label "nietowarzyski"
  ]
  node [
    id 62
    label "biota"
  ]
  node [
    id 63
    label "wszechstworzenie"
  ]
  node [
    id 64
    label "obiekt_naturalny"
  ]
  node [
    id 65
    label "Ziemia"
  ]
  node [
    id 66
    label "fauna"
  ]
  node [
    id 67
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 68
    label "przedmiot"
  ]
  node [
    id 69
    label "stw&#243;r"
  ]
  node [
    id 70
    label "ekosystem"
  ]
  node [
    id 71
    label "rzecz"
  ]
  node [
    id 72
    label "teren"
  ]
  node [
    id 73
    label "environment"
  ]
  node [
    id 74
    label "woda"
  ]
  node [
    id 75
    label "przyra"
  ]
  node [
    id 76
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 77
    label "mikrokosmos"
  ]
  node [
    id 78
    label "pokry&#263;"
  ]
  node [
    id 79
    label "podr&#243;&#380;nik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 79
  ]
]
