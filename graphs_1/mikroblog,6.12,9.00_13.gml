graph [
  maxDegree 11
  minDegree 1
  meanDegree 2.08955223880597
  density 0.031659882406151064
  graphCliqueNumber 2
  node [
    id 0
    label "chyba"
    origin "text"
  ]
  node [
    id 1
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 2
    label "trasa"
    origin "text"
  ]
  node [
    id 3
    label "piechota"
    origin "text"
  ]
  node [
    id 4
    label "jedyna"
    origin "text"
  ]
  node [
    id 5
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 6
    label "podr&#243;&#380;"
    origin "text"
  ]
  node [
    id 7
    label "maszerowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "godzina"
    origin "text"
  ]
  node [
    id 9
    label "dziennie"
    origin "text"
  ]
  node [
    id 10
    label "ciekawostka"
    origin "text"
  ]
  node [
    id 11
    label "mapa"
    origin "text"
  ]
  node [
    id 12
    label "daleki"
  ]
  node [
    id 13
    label "d&#322;ugo"
  ]
  node [
    id 14
    label "ruch"
  ]
  node [
    id 15
    label "marszrutyzacja"
  ]
  node [
    id 16
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 17
    label "w&#281;ze&#322;"
  ]
  node [
    id 18
    label "przebieg"
  ]
  node [
    id 19
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 20
    label "droga"
  ]
  node [
    id 21
    label "infrastruktura"
  ]
  node [
    id 22
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 23
    label "podbieg"
  ]
  node [
    id 24
    label "armia"
  ]
  node [
    id 25
    label "kompania_honorowa"
  ]
  node [
    id 26
    label "formacja"
  ]
  node [
    id 27
    label "falanga"
  ]
  node [
    id 28
    label "infantry"
  ]
  node [
    id 29
    label "kobieta"
  ]
  node [
    id 30
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 31
    label "czas"
  ]
  node [
    id 32
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 33
    label "rok"
  ]
  node [
    id 34
    label "miech"
  ]
  node [
    id 35
    label "kalendy"
  ]
  node [
    id 36
    label "tydzie&#324;"
  ]
  node [
    id 37
    label "zbior&#243;wka"
  ]
  node [
    id 38
    label "ekskursja"
  ]
  node [
    id 39
    label "rajza"
  ]
  node [
    id 40
    label "ekwipunek"
  ]
  node [
    id 41
    label "journey"
  ]
  node [
    id 42
    label "zmiana"
  ]
  node [
    id 43
    label "bezsilnikowy"
  ]
  node [
    id 44
    label "turystyka"
  ]
  node [
    id 45
    label "chodzi&#263;"
  ]
  node [
    id 46
    label "i&#347;&#263;"
  ]
  node [
    id 47
    label "March"
  ]
  node [
    id 48
    label "minuta"
  ]
  node [
    id 49
    label "doba"
  ]
  node [
    id 50
    label "p&#243;&#322;godzina"
  ]
  node [
    id 51
    label "kwadrans"
  ]
  node [
    id 52
    label "time"
  ]
  node [
    id 53
    label "jednostka_czasu"
  ]
  node [
    id 54
    label "stacjonarny"
  ]
  node [
    id 55
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 56
    label "informacja"
  ]
  node [
    id 57
    label "uk&#322;ad"
  ]
  node [
    id 58
    label "masztab"
  ]
  node [
    id 59
    label "izarytma"
  ]
  node [
    id 60
    label "plot"
  ]
  node [
    id 61
    label "legenda"
  ]
  node [
    id 62
    label "fotoszkic"
  ]
  node [
    id 63
    label "atlas"
  ]
  node [
    id 64
    label "wododzia&#322;"
  ]
  node [
    id 65
    label "rysunek"
  ]
  node [
    id 66
    label "god&#322;o_mapy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
]
