graph [
  maxDegree 70
  minDegree 1
  meanDegree 2.0557491289198606
  density 0.00718793401720231
  graphCliqueNumber 2
  node [
    id 0
    label "rusza"
    origin "text"
  ]
  node [
    id 1
    label "wypowied&#378;"
    origin "text"
  ]
  node [
    id 2
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 3
    label "organizacja"
    origin "text"
  ]
  node [
    id 4
    label "obro&#324;ca"
    origin "text"
  ]
  node [
    id 5
    label "prawy"
    origin "text"
  ]
  node [
    id 6
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 7
    label "pojawia&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "stwierdzenie"
    origin "text"
  ]
  node [
    id 10
    label "jakoby"
    origin "text"
  ]
  node [
    id 11
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 12
    label "rozpoczyna&#263;"
    origin "text"
  ]
  node [
    id 13
    label "igrzyska"
    origin "text"
  ]
  node [
    id 14
    label "pekin"
    origin "text"
  ]
  node [
    id 15
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 16
    label "jaskrawy"
    origin "text"
  ]
  node [
    id 17
    label "zaprzeczenie"
    origin "text"
  ]
  node [
    id 18
    label "idea"
    origin "text"
  ]
  node [
    id 19
    label "olimpijski"
    origin "text"
  ]
  node [
    id 20
    label "mie&#263;by"
    origin "text"
  ]
  node [
    id 21
    label "promowa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "uczciwy"
    origin "text"
  ]
  node [
    id 23
    label "rywalizacja"
    origin "text"
  ]
  node [
    id 24
    label "oraz"
    origin "text"
  ]
  node [
    id 25
    label "wsp&#243;&#322;istnienie"
    origin "text"
  ]
  node [
    id 26
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 27
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 28
    label "parafrazowa&#263;"
  ]
  node [
    id 29
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 30
    label "sparafrazowa&#263;"
  ]
  node [
    id 31
    label "s&#261;d"
  ]
  node [
    id 32
    label "trawestowanie"
  ]
  node [
    id 33
    label "sparafrazowanie"
  ]
  node [
    id 34
    label "strawestowa&#263;"
  ]
  node [
    id 35
    label "sformu&#322;owanie"
  ]
  node [
    id 36
    label "strawestowanie"
  ]
  node [
    id 37
    label "komunikat"
  ]
  node [
    id 38
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 39
    label "delimitacja"
  ]
  node [
    id 40
    label "trawestowa&#263;"
  ]
  node [
    id 41
    label "parafrazowanie"
  ]
  node [
    id 42
    label "stylizacja"
  ]
  node [
    id 43
    label "ozdobnik"
  ]
  node [
    id 44
    label "pos&#322;uchanie"
  ]
  node [
    id 45
    label "rezultat"
  ]
  node [
    id 46
    label "cz&#322;onek"
  ]
  node [
    id 47
    label "substytuowanie"
  ]
  node [
    id 48
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 49
    label "przyk&#322;ad"
  ]
  node [
    id 50
    label "zast&#281;pca"
  ]
  node [
    id 51
    label "substytuowa&#263;"
  ]
  node [
    id 52
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 53
    label "endecki"
  ]
  node [
    id 54
    label "komitet_koordynacyjny"
  ]
  node [
    id 55
    label "przybud&#243;wka"
  ]
  node [
    id 56
    label "ZOMO"
  ]
  node [
    id 57
    label "podmiot"
  ]
  node [
    id 58
    label "boj&#243;wka"
  ]
  node [
    id 59
    label "zesp&#243;&#322;"
  ]
  node [
    id 60
    label "organization"
  ]
  node [
    id 61
    label "TOPR"
  ]
  node [
    id 62
    label "jednostka_organizacyjna"
  ]
  node [
    id 63
    label "przedstawicielstwo"
  ]
  node [
    id 64
    label "Cepelia"
  ]
  node [
    id 65
    label "GOPR"
  ]
  node [
    id 66
    label "ZMP"
  ]
  node [
    id 67
    label "ZBoWiD"
  ]
  node [
    id 68
    label "struktura"
  ]
  node [
    id 69
    label "od&#322;am"
  ]
  node [
    id 70
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 71
    label "centrala"
  ]
  node [
    id 72
    label "obroniciel"
  ]
  node [
    id 73
    label "tarcza"
  ]
  node [
    id 74
    label "prawnik"
  ]
  node [
    id 75
    label "obrona"
  ]
  node [
    id 76
    label "palestra"
  ]
  node [
    id 77
    label "rzecznik"
  ]
  node [
    id 78
    label "palestrant"
  ]
  node [
    id 79
    label "gracz"
  ]
  node [
    id 80
    label "strona"
  ]
  node [
    id 81
    label "zawodnik"
  ]
  node [
    id 82
    label "papuga"
  ]
  node [
    id 83
    label "z_prawa"
  ]
  node [
    id 84
    label "cnotliwy"
  ]
  node [
    id 85
    label "moralny"
  ]
  node [
    id 86
    label "zgodnie_z_prawem"
  ]
  node [
    id 87
    label "naturalny"
  ]
  node [
    id 88
    label "legalny"
  ]
  node [
    id 89
    label "na_prawo"
  ]
  node [
    id 90
    label "s&#322;uszny"
  ]
  node [
    id 91
    label "w_prawo"
  ]
  node [
    id 92
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 93
    label "prawicowy"
  ]
  node [
    id 94
    label "chwalebny"
  ]
  node [
    id 95
    label "zacny"
  ]
  node [
    id 96
    label "asymilowa&#263;"
  ]
  node [
    id 97
    label "wapniak"
  ]
  node [
    id 98
    label "dwun&#243;g"
  ]
  node [
    id 99
    label "polifag"
  ]
  node [
    id 100
    label "wz&#243;r"
  ]
  node [
    id 101
    label "profanum"
  ]
  node [
    id 102
    label "hominid"
  ]
  node [
    id 103
    label "homo_sapiens"
  ]
  node [
    id 104
    label "nasada"
  ]
  node [
    id 105
    label "podw&#322;adny"
  ]
  node [
    id 106
    label "ludzko&#347;&#263;"
  ]
  node [
    id 107
    label "os&#322;abianie"
  ]
  node [
    id 108
    label "mikrokosmos"
  ]
  node [
    id 109
    label "portrecista"
  ]
  node [
    id 110
    label "duch"
  ]
  node [
    id 111
    label "g&#322;owa"
  ]
  node [
    id 112
    label "oddzia&#322;ywanie"
  ]
  node [
    id 113
    label "asymilowanie"
  ]
  node [
    id 114
    label "osoba"
  ]
  node [
    id 115
    label "os&#322;abia&#263;"
  ]
  node [
    id 116
    label "figura"
  ]
  node [
    id 117
    label "Adam"
  ]
  node [
    id 118
    label "senior"
  ]
  node [
    id 119
    label "antropochoria"
  ]
  node [
    id 120
    label "posta&#263;"
  ]
  node [
    id 121
    label "ustalenie"
  ]
  node [
    id 122
    label "claim"
  ]
  node [
    id 123
    label "statement"
  ]
  node [
    id 124
    label "oznajmienie"
  ]
  node [
    id 125
    label "doba"
  ]
  node [
    id 126
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 127
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 128
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 129
    label "start"
  ]
  node [
    id 130
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 131
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 132
    label "begin"
  ]
  node [
    id 133
    label "sprawowa&#263;"
  ]
  node [
    id 134
    label "festiwal"
  ]
  node [
    id 135
    label "game"
  ]
  node [
    id 136
    label "arena"
  ]
  node [
    id 137
    label "zawody"
  ]
  node [
    id 138
    label "dawny"
  ]
  node [
    id 139
    label "rozw&#243;d"
  ]
  node [
    id 140
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 141
    label "eksprezydent"
  ]
  node [
    id 142
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 143
    label "partner"
  ]
  node [
    id 144
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 145
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 146
    label "wcze&#347;niejszy"
  ]
  node [
    id 147
    label "ewidentny"
  ]
  node [
    id 148
    label "oczojebkowy"
  ]
  node [
    id 149
    label "jasny"
  ]
  node [
    id 150
    label "krzykliwy"
  ]
  node [
    id 151
    label "ra&#380;&#261;cy"
  ]
  node [
    id 152
    label "jaskrawo"
  ]
  node [
    id 153
    label "oczojebny"
  ]
  node [
    id 154
    label "dosadny"
  ]
  node [
    id 155
    label "nasycony"
  ]
  node [
    id 156
    label "ostry"
  ]
  node [
    id 157
    label "zakwestionowanie"
  ]
  node [
    id 158
    label "rejection"
  ]
  node [
    id 159
    label "negation"
  ]
  node [
    id 160
    label "denial"
  ]
  node [
    id 161
    label "zdementowanie"
  ]
  node [
    id 162
    label "ocenienie"
  ]
  node [
    id 163
    label "zdarzenie_si&#281;"
  ]
  node [
    id 164
    label "negacja"
  ]
  node [
    id 165
    label "byt"
  ]
  node [
    id 166
    label "istota"
  ]
  node [
    id 167
    label "ideologia"
  ]
  node [
    id 168
    label "intelekt"
  ]
  node [
    id 169
    label "Kant"
  ]
  node [
    id 170
    label "pomys&#322;"
  ]
  node [
    id 171
    label "poj&#281;cie"
  ]
  node [
    id 172
    label "cel"
  ]
  node [
    id 173
    label "p&#322;&#243;d"
  ]
  node [
    id 174
    label "ideacja"
  ]
  node [
    id 175
    label "wypromowywa&#263;"
  ]
  node [
    id 176
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 177
    label "rozpowszechnia&#263;"
  ]
  node [
    id 178
    label "nada&#263;"
  ]
  node [
    id 179
    label "zach&#281;ca&#263;"
  ]
  node [
    id 180
    label "promocja"
  ]
  node [
    id 181
    label "udzieli&#263;"
  ]
  node [
    id 182
    label "udziela&#263;"
  ]
  node [
    id 183
    label "pomaga&#263;"
  ]
  node [
    id 184
    label "advance"
  ]
  node [
    id 185
    label "doprowadza&#263;"
  ]
  node [
    id 186
    label "reklama"
  ]
  node [
    id 187
    label "nadawa&#263;"
  ]
  node [
    id 188
    label "szczery"
  ]
  node [
    id 189
    label "solidny"
  ]
  node [
    id 190
    label "rzetelny"
  ]
  node [
    id 191
    label "nale&#380;yty"
  ]
  node [
    id 192
    label "zgodny"
  ]
  node [
    id 193
    label "s&#322;usznie"
  ]
  node [
    id 194
    label "porz&#261;dnie"
  ]
  node [
    id 195
    label "intensywny"
  ]
  node [
    id 196
    label "uczciwie"
  ]
  node [
    id 197
    label "prawdziwy"
  ]
  node [
    id 198
    label "contest"
  ]
  node [
    id 199
    label "wydarzenie"
  ]
  node [
    id 200
    label "wsp&#243;&#322;wyst&#281;powanie"
  ]
  node [
    id 201
    label "coexistence"
  ]
  node [
    id 202
    label "Samojedzi"
  ]
  node [
    id 203
    label "nacja"
  ]
  node [
    id 204
    label "Aztekowie"
  ]
  node [
    id 205
    label "Irokezi"
  ]
  node [
    id 206
    label "Buriaci"
  ]
  node [
    id 207
    label "Komancze"
  ]
  node [
    id 208
    label "t&#322;um"
  ]
  node [
    id 209
    label "ludno&#347;&#263;"
  ]
  node [
    id 210
    label "lud"
  ]
  node [
    id 211
    label "Siuksowie"
  ]
  node [
    id 212
    label "Czejenowie"
  ]
  node [
    id 213
    label "Wotiacy"
  ]
  node [
    id 214
    label "Baszkirzy"
  ]
  node [
    id 215
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 216
    label "Mohikanie"
  ]
  node [
    id 217
    label "Apacze"
  ]
  node [
    id 218
    label "Syngalezi"
  ]
  node [
    id 219
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 220
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 221
    label "obszar"
  ]
  node [
    id 222
    label "obiekt_naturalny"
  ]
  node [
    id 223
    label "przedmiot"
  ]
  node [
    id 224
    label "biosfera"
  ]
  node [
    id 225
    label "grupa"
  ]
  node [
    id 226
    label "stw&#243;r"
  ]
  node [
    id 227
    label "Stary_&#346;wiat"
  ]
  node [
    id 228
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 229
    label "rzecz"
  ]
  node [
    id 230
    label "magnetosfera"
  ]
  node [
    id 231
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 232
    label "environment"
  ]
  node [
    id 233
    label "Nowy_&#346;wiat"
  ]
  node [
    id 234
    label "geosfera"
  ]
  node [
    id 235
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 236
    label "planeta"
  ]
  node [
    id 237
    label "przejmowa&#263;"
  ]
  node [
    id 238
    label "litosfera"
  ]
  node [
    id 239
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 240
    label "makrokosmos"
  ]
  node [
    id 241
    label "barysfera"
  ]
  node [
    id 242
    label "biota"
  ]
  node [
    id 243
    label "p&#243;&#322;noc"
  ]
  node [
    id 244
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 245
    label "fauna"
  ]
  node [
    id 246
    label "wszechstworzenie"
  ]
  node [
    id 247
    label "geotermia"
  ]
  node [
    id 248
    label "biegun"
  ]
  node [
    id 249
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 250
    label "ekosystem"
  ]
  node [
    id 251
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 252
    label "teren"
  ]
  node [
    id 253
    label "zjawisko"
  ]
  node [
    id 254
    label "p&#243;&#322;kula"
  ]
  node [
    id 255
    label "atmosfera"
  ]
  node [
    id 256
    label "class"
  ]
  node [
    id 257
    label "po&#322;udnie"
  ]
  node [
    id 258
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 259
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 260
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 261
    label "przejmowanie"
  ]
  node [
    id 262
    label "przestrze&#324;"
  ]
  node [
    id 263
    label "asymilowanie_si&#281;"
  ]
  node [
    id 264
    label "przej&#261;&#263;"
  ]
  node [
    id 265
    label "ekosfera"
  ]
  node [
    id 266
    label "przyroda"
  ]
  node [
    id 267
    label "ciemna_materia"
  ]
  node [
    id 268
    label "geoida"
  ]
  node [
    id 269
    label "Wsch&#243;d"
  ]
  node [
    id 270
    label "populace"
  ]
  node [
    id 271
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 272
    label "huczek"
  ]
  node [
    id 273
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 274
    label "Ziemia"
  ]
  node [
    id 275
    label "universe"
  ]
  node [
    id 276
    label "ozonosfera"
  ]
  node [
    id 277
    label "rze&#378;ba"
  ]
  node [
    id 278
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 279
    label "zagranica"
  ]
  node [
    id 280
    label "hydrosfera"
  ]
  node [
    id 281
    label "woda"
  ]
  node [
    id 282
    label "kuchnia"
  ]
  node [
    id 283
    label "przej&#281;cie"
  ]
  node [
    id 284
    label "czarna_dziura"
  ]
  node [
    id 285
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 286
    label "morze"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 85
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 90
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 200
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 202
  ]
  edge [
    source 26
    target 203
  ]
  edge [
    source 26
    target 204
  ]
  edge [
    source 26
    target 205
  ]
  edge [
    source 26
    target 206
  ]
  edge [
    source 26
    target 207
  ]
  edge [
    source 26
    target 208
  ]
  edge [
    source 26
    target 209
  ]
  edge [
    source 26
    target 210
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 213
  ]
  edge [
    source 26
    target 214
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 26
    target 217
  ]
  edge [
    source 26
    target 218
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 27
    target 228
  ]
  edge [
    source 27
    target 229
  ]
  edge [
    source 27
    target 230
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 232
  ]
  edge [
    source 27
    target 233
  ]
  edge [
    source 27
    target 234
  ]
  edge [
    source 27
    target 235
  ]
  edge [
    source 27
    target 236
  ]
  edge [
    source 27
    target 237
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 239
  ]
  edge [
    source 27
    target 240
  ]
  edge [
    source 27
    target 241
  ]
  edge [
    source 27
    target 242
  ]
  edge [
    source 27
    target 243
  ]
  edge [
    source 27
    target 244
  ]
  edge [
    source 27
    target 245
  ]
  edge [
    source 27
    target 246
  ]
  edge [
    source 27
    target 247
  ]
  edge [
    source 27
    target 248
  ]
  edge [
    source 27
    target 249
  ]
  edge [
    source 27
    target 250
  ]
  edge [
    source 27
    target 251
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 253
  ]
  edge [
    source 27
    target 254
  ]
  edge [
    source 27
    target 255
  ]
  edge [
    source 27
    target 108
  ]
  edge [
    source 27
    target 256
  ]
  edge [
    source 27
    target 257
  ]
  edge [
    source 27
    target 258
  ]
  edge [
    source 27
    target 259
  ]
  edge [
    source 27
    target 260
  ]
  edge [
    source 27
    target 261
  ]
  edge [
    source 27
    target 262
  ]
  edge [
    source 27
    target 263
  ]
  edge [
    source 27
    target 264
  ]
  edge [
    source 27
    target 265
  ]
  edge [
    source 27
    target 266
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 27
    target 268
  ]
  edge [
    source 27
    target 269
  ]
  edge [
    source 27
    target 270
  ]
  edge [
    source 27
    target 271
  ]
  edge [
    source 27
    target 272
  ]
  edge [
    source 27
    target 273
  ]
  edge [
    source 27
    target 274
  ]
  edge [
    source 27
    target 275
  ]
  edge [
    source 27
    target 276
  ]
  edge [
    source 27
    target 277
  ]
  edge [
    source 27
    target 278
  ]
  edge [
    source 27
    target 279
  ]
  edge [
    source 27
    target 280
  ]
  edge [
    source 27
    target 281
  ]
  edge [
    source 27
    target 282
  ]
  edge [
    source 27
    target 283
  ]
  edge [
    source 27
    target 284
  ]
  edge [
    source 27
    target 285
  ]
  edge [
    source 27
    target 286
  ]
]
