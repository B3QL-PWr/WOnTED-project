graph [
  maxDegree 218
  minDegree 1
  meanDegree 2.0224719101123596
  density 0.004555116914667477
  graphCliqueNumber 2
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "taka"
    origin "text"
  ]
  node [
    id 2
    label "lista"
    origin "text"
  ]
  node [
    id 3
    label "wa&#380;ki"
    origin "text"
  ]
  node [
    id 4
    label "temat"
    origin "text"
  ]
  node [
    id 5
    label "poruszenie"
    origin "text"
  ]
  node [
    id 6
    label "ale"
    origin "text"
  ]
  node [
    id 7
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 8
    label "przeczyta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "co&#347;"
    origin "text"
  ]
  node [
    id 11
    label "tak"
    origin "text"
  ]
  node [
    id 12
    label "nieistotny"
    origin "text"
  ]
  node [
    id 13
    label "por&#243;wnanie"
    origin "text"
  ]
  node [
    id 14
    label "chrystus"
    origin "text"
  ]
  node [
    id 15
    label "kr&#243;l"
    origin "text"
  ]
  node [
    id 16
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 17
    label "interesuj&#261;cy"
    origin "text"
  ]
  node [
    id 18
    label "przeciwie&#324;stwo"
    origin "text"
  ]
  node [
    id 19
    label "daleki"
    origin "text"
  ]
  node [
    id 20
    label "los"
    origin "text"
  ]
  node [
    id 21
    label "zawodowy"
    origin "text"
  ]
  node [
    id 22
    label "karier"
    origin "text"
  ]
  node [
    id 23
    label "atrakcyjny"
    origin "text"
  ]
  node [
    id 24
    label "kazimierz"
    origin "text"
  ]
  node [
    id 25
    label "musza"
    origin "text"
  ]
  node [
    id 26
    label "si&#281;"
    origin "text"
  ]
  node [
    id 27
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 28
    label "tym"
    origin "text"
  ]
  node [
    id 29
    label "podzieli&#263;"
    origin "text"
  ]
  node [
    id 30
    label "matczysko"
  ]
  node [
    id 31
    label "macierz"
  ]
  node [
    id 32
    label "przodkini"
  ]
  node [
    id 33
    label "Matka_Boska"
  ]
  node [
    id 34
    label "macocha"
  ]
  node [
    id 35
    label "matka_zast&#281;pcza"
  ]
  node [
    id 36
    label "stara"
  ]
  node [
    id 37
    label "rodzice"
  ]
  node [
    id 38
    label "rodzic"
  ]
  node [
    id 39
    label "Bangladesz"
  ]
  node [
    id 40
    label "jednostka_monetarna"
  ]
  node [
    id 41
    label "wyliczanka"
  ]
  node [
    id 42
    label "catalog"
  ]
  node [
    id 43
    label "stock"
  ]
  node [
    id 44
    label "figurowa&#263;"
  ]
  node [
    id 45
    label "zbi&#243;r"
  ]
  node [
    id 46
    label "book"
  ]
  node [
    id 47
    label "pozycja"
  ]
  node [
    id 48
    label "tekst"
  ]
  node [
    id 49
    label "sumariusz"
  ]
  node [
    id 50
    label "owady_uskrzydlone"
  ]
  node [
    id 51
    label "wa&#380;ny"
  ]
  node [
    id 52
    label "fraza"
  ]
  node [
    id 53
    label "forma"
  ]
  node [
    id 54
    label "melodia"
  ]
  node [
    id 55
    label "rzecz"
  ]
  node [
    id 56
    label "zbacza&#263;"
  ]
  node [
    id 57
    label "entity"
  ]
  node [
    id 58
    label "omawia&#263;"
  ]
  node [
    id 59
    label "topik"
  ]
  node [
    id 60
    label "wyraz_pochodny"
  ]
  node [
    id 61
    label "om&#243;wi&#263;"
  ]
  node [
    id 62
    label "omawianie"
  ]
  node [
    id 63
    label "w&#261;tek"
  ]
  node [
    id 64
    label "forum"
  ]
  node [
    id 65
    label "cecha"
  ]
  node [
    id 66
    label "zboczenie"
  ]
  node [
    id 67
    label "zbaczanie"
  ]
  node [
    id 68
    label "tre&#347;&#263;"
  ]
  node [
    id 69
    label "tematyka"
  ]
  node [
    id 70
    label "sprawa"
  ]
  node [
    id 71
    label "istota"
  ]
  node [
    id 72
    label "otoczka"
  ]
  node [
    id 73
    label "zboczy&#263;"
  ]
  node [
    id 74
    label "om&#243;wienie"
  ]
  node [
    id 75
    label "zrobienie"
  ]
  node [
    id 76
    label "spowodowanie"
  ]
  node [
    id 77
    label "gesture"
  ]
  node [
    id 78
    label "movement"
  ]
  node [
    id 79
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 80
    label "ruch"
  ]
  node [
    id 81
    label "wzbudzenie"
  ]
  node [
    id 82
    label "zdarzenie_si&#281;"
  ]
  node [
    id 83
    label "poruszanie_si&#281;"
  ]
  node [
    id 84
    label "piwo"
  ]
  node [
    id 85
    label "dok&#322;adnie"
  ]
  node [
    id 86
    label "zarys"
  ]
  node [
    id 87
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 88
    label "zniesienie"
  ]
  node [
    id 89
    label "nap&#322;ywanie"
  ]
  node [
    id 90
    label "znosi&#263;"
  ]
  node [
    id 91
    label "informacja"
  ]
  node [
    id 92
    label "znie&#347;&#263;"
  ]
  node [
    id 93
    label "komunikat"
  ]
  node [
    id 94
    label "depesza_emska"
  ]
  node [
    id 95
    label "communication"
  ]
  node [
    id 96
    label "znoszenie"
  ]
  node [
    id 97
    label "signal"
  ]
  node [
    id 98
    label "thing"
  ]
  node [
    id 99
    label "cosik"
  ]
  node [
    id 100
    label "nieistotnie"
  ]
  node [
    id 101
    label "nieznaczny"
  ]
  node [
    id 102
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 103
    label "s&#322;aby"
  ]
  node [
    id 104
    label "simile"
  ]
  node [
    id 105
    label "figura_stylistyczna"
  ]
  node [
    id 106
    label "zestawienie"
  ]
  node [
    id 107
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 108
    label "comparison"
  ]
  node [
    id 109
    label "zanalizowanie"
  ]
  node [
    id 110
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 111
    label "Otton_III"
  ]
  node [
    id 112
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 113
    label "monarchista"
  ]
  node [
    id 114
    label "Syzyf"
  ]
  node [
    id 115
    label "turzyca"
  ]
  node [
    id 116
    label "Fryderyk_II_Wielki"
  ]
  node [
    id 117
    label "Zygmunt_III_Waza"
  ]
  node [
    id 118
    label "Aleksander_Wielki"
  ]
  node [
    id 119
    label "monarcha"
  ]
  node [
    id 120
    label "gruba_ryba"
  ]
  node [
    id 121
    label "August_III_Sas"
  ]
  node [
    id 122
    label "koronowa&#263;"
  ]
  node [
    id 123
    label "tytu&#322;"
  ]
  node [
    id 124
    label "Boles&#322;aw_Chrobry"
  ]
  node [
    id 125
    label "zwierz&#281;_futerkowe"
  ]
  node [
    id 126
    label "basileus"
  ]
  node [
    id 127
    label "Zygmunt_II_August"
  ]
  node [
    id 128
    label "Jan_Kazimierz"
  ]
  node [
    id 129
    label "Manuel_I_Szcz&#281;&#347;liwy"
  ]
  node [
    id 130
    label "Ludwik_XVI"
  ]
  node [
    id 131
    label "trusia"
  ]
  node [
    id 132
    label "Kazimierz_Wielki"
  ]
  node [
    id 133
    label "Tantal"
  ]
  node [
    id 134
    label "HP"
  ]
  node [
    id 135
    label "Edward_VII"
  ]
  node [
    id 136
    label "Jugurta"
  ]
  node [
    id 137
    label "Herod"
  ]
  node [
    id 138
    label "omyk"
  ]
  node [
    id 139
    label "Augiasz"
  ]
  node [
    id 140
    label "Stanis&#322;aw_August_Poniatowski"
  ]
  node [
    id 141
    label "zaj&#261;cowate"
  ]
  node [
    id 142
    label "Salomon"
  ]
  node [
    id 143
    label "figura_karciana"
  ]
  node [
    id 144
    label "baron"
  ]
  node [
    id 145
    label "Karol_Albert"
  ]
  node [
    id 146
    label "figura"
  ]
  node [
    id 147
    label "Artur"
  ]
  node [
    id 148
    label "kicaj"
  ]
  node [
    id 149
    label "Dawid"
  ]
  node [
    id 150
    label "kr&#243;lowa_matka"
  ]
  node [
    id 151
    label "koronowanie"
  ]
  node [
    id 152
    label "Henryk_IV"
  ]
  node [
    id 153
    label "Zygmunt_I_Stary"
  ]
  node [
    id 154
    label "mo&#380;liwie"
  ]
  node [
    id 155
    label "kr&#243;tko"
  ]
  node [
    id 156
    label "nieliczny"
  ]
  node [
    id 157
    label "mikroskopijnie"
  ]
  node [
    id 158
    label "pomiernie"
  ]
  node [
    id 159
    label "ma&#322;y"
  ]
  node [
    id 160
    label "swoisty"
  ]
  node [
    id 161
    label "ciekawie"
  ]
  node [
    id 162
    label "interesuj&#261;co"
  ]
  node [
    id 163
    label "dziwny"
  ]
  node [
    id 164
    label "trudno&#347;&#263;"
  ]
  node [
    id 165
    label "odmienno&#347;&#263;"
  ]
  node [
    id 166
    label "odwrotny"
  ]
  node [
    id 167
    label "obstruction"
  ]
  node [
    id 168
    label "reverse"
  ]
  node [
    id 169
    label "dawny"
  ]
  node [
    id 170
    label "du&#380;y"
  ]
  node [
    id 171
    label "oddalony"
  ]
  node [
    id 172
    label "daleko"
  ]
  node [
    id 173
    label "przysz&#322;y"
  ]
  node [
    id 174
    label "ogl&#281;dny"
  ]
  node [
    id 175
    label "r&#243;&#380;ny"
  ]
  node [
    id 176
    label "g&#322;&#281;boki"
  ]
  node [
    id 177
    label "odlegle"
  ]
  node [
    id 178
    label "nieobecny"
  ]
  node [
    id 179
    label "odleg&#322;y"
  ]
  node [
    id 180
    label "d&#322;ugi"
  ]
  node [
    id 181
    label "zwi&#261;zany"
  ]
  node [
    id 182
    label "obcy"
  ]
  node [
    id 183
    label "si&#322;a"
  ]
  node [
    id 184
    label "przymus"
  ]
  node [
    id 185
    label "rzuci&#263;"
  ]
  node [
    id 186
    label "hazard"
  ]
  node [
    id 187
    label "destiny"
  ]
  node [
    id 188
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 189
    label "bilet"
  ]
  node [
    id 190
    label "przebieg_&#380;ycia"
  ]
  node [
    id 191
    label "&#380;ycie"
  ]
  node [
    id 192
    label "rzucenie"
  ]
  node [
    id 193
    label "formalny"
  ]
  node [
    id 194
    label "zawodowo"
  ]
  node [
    id 195
    label "zawo&#322;any"
  ]
  node [
    id 196
    label "profesjonalny"
  ]
  node [
    id 197
    label "czadowy"
  ]
  node [
    id 198
    label "fajny"
  ]
  node [
    id 199
    label "fachowy"
  ]
  node [
    id 200
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 201
    label "klawy"
  ]
  node [
    id 202
    label "go&#322;&#261;b"
  ]
  node [
    id 203
    label "g&#322;adki"
  ]
  node [
    id 204
    label "po&#380;&#261;dany"
  ]
  node [
    id 205
    label "uatrakcyjnienie"
  ]
  node [
    id 206
    label "atrakcyjnie"
  ]
  node [
    id 207
    label "dobry"
  ]
  node [
    id 208
    label "poci&#261;gaj&#261;co"
  ]
  node [
    id 209
    label "uatrakcyjnianie"
  ]
  node [
    id 210
    label "Filipiny"
  ]
  node [
    id 211
    label "Rwanda"
  ]
  node [
    id 212
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 213
    label "Monako"
  ]
  node [
    id 214
    label "Korea"
  ]
  node [
    id 215
    label "Ghana"
  ]
  node [
    id 216
    label "Czarnog&#243;ra"
  ]
  node [
    id 217
    label "Malawi"
  ]
  node [
    id 218
    label "Indonezja"
  ]
  node [
    id 219
    label "Bu&#322;garia"
  ]
  node [
    id 220
    label "Nauru"
  ]
  node [
    id 221
    label "Kenia"
  ]
  node [
    id 222
    label "Kambod&#380;a"
  ]
  node [
    id 223
    label "Mali"
  ]
  node [
    id 224
    label "Austria"
  ]
  node [
    id 225
    label "interior"
  ]
  node [
    id 226
    label "Armenia"
  ]
  node [
    id 227
    label "Fid&#380;i"
  ]
  node [
    id 228
    label "Tuwalu"
  ]
  node [
    id 229
    label "Etiopia"
  ]
  node [
    id 230
    label "Malta"
  ]
  node [
    id 231
    label "Malezja"
  ]
  node [
    id 232
    label "Grenada"
  ]
  node [
    id 233
    label "Tad&#380;ykistan"
  ]
  node [
    id 234
    label "Wehrlen"
  ]
  node [
    id 235
    label "para"
  ]
  node [
    id 236
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 237
    label "Rumunia"
  ]
  node [
    id 238
    label "Maroko"
  ]
  node [
    id 239
    label "Bhutan"
  ]
  node [
    id 240
    label "S&#322;owacja"
  ]
  node [
    id 241
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 242
    label "Seszele"
  ]
  node [
    id 243
    label "Kuwejt"
  ]
  node [
    id 244
    label "Arabia_Saudyjska"
  ]
  node [
    id 245
    label "Ekwador"
  ]
  node [
    id 246
    label "Kanada"
  ]
  node [
    id 247
    label "Japonia"
  ]
  node [
    id 248
    label "ziemia"
  ]
  node [
    id 249
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 250
    label "Hiszpania"
  ]
  node [
    id 251
    label "Wyspy_Marshalla"
  ]
  node [
    id 252
    label "Botswana"
  ]
  node [
    id 253
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 254
    label "D&#380;ibuti"
  ]
  node [
    id 255
    label "grupa"
  ]
  node [
    id 256
    label "Wietnam"
  ]
  node [
    id 257
    label "Egipt"
  ]
  node [
    id 258
    label "Burkina_Faso"
  ]
  node [
    id 259
    label "Niemcy"
  ]
  node [
    id 260
    label "Khitai"
  ]
  node [
    id 261
    label "Macedonia"
  ]
  node [
    id 262
    label "Albania"
  ]
  node [
    id 263
    label "Madagaskar"
  ]
  node [
    id 264
    label "Bahrajn"
  ]
  node [
    id 265
    label "Jemen"
  ]
  node [
    id 266
    label "Lesoto"
  ]
  node [
    id 267
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 268
    label "Samoa"
  ]
  node [
    id 269
    label "Andora"
  ]
  node [
    id 270
    label "Chiny"
  ]
  node [
    id 271
    label "Cypr"
  ]
  node [
    id 272
    label "Wielka_Brytania"
  ]
  node [
    id 273
    label "Ukraina"
  ]
  node [
    id 274
    label "Paragwaj"
  ]
  node [
    id 275
    label "Trynidad_i_Tobago"
  ]
  node [
    id 276
    label "Libia"
  ]
  node [
    id 277
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 278
    label "Surinam"
  ]
  node [
    id 279
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 280
    label "Australia"
  ]
  node [
    id 281
    label "Nigeria"
  ]
  node [
    id 282
    label "Honduras"
  ]
  node [
    id 283
    label "Peru"
  ]
  node [
    id 284
    label "Kazachstan"
  ]
  node [
    id 285
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 286
    label "Irak"
  ]
  node [
    id 287
    label "holoarktyka"
  ]
  node [
    id 288
    label "USA"
  ]
  node [
    id 289
    label "Sudan"
  ]
  node [
    id 290
    label "Nepal"
  ]
  node [
    id 291
    label "San_Marino"
  ]
  node [
    id 292
    label "Burundi"
  ]
  node [
    id 293
    label "Dominikana"
  ]
  node [
    id 294
    label "Komory"
  ]
  node [
    id 295
    label "granica_pa&#324;stwa"
  ]
  node [
    id 296
    label "Gwatemala"
  ]
  node [
    id 297
    label "Antarktis"
  ]
  node [
    id 298
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 299
    label "Brunei"
  ]
  node [
    id 300
    label "Iran"
  ]
  node [
    id 301
    label "Zimbabwe"
  ]
  node [
    id 302
    label "Namibia"
  ]
  node [
    id 303
    label "Meksyk"
  ]
  node [
    id 304
    label "Kamerun"
  ]
  node [
    id 305
    label "zwrot"
  ]
  node [
    id 306
    label "Somalia"
  ]
  node [
    id 307
    label "Angola"
  ]
  node [
    id 308
    label "Gabon"
  ]
  node [
    id 309
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 310
    label "Mozambik"
  ]
  node [
    id 311
    label "Tajwan"
  ]
  node [
    id 312
    label "Tunezja"
  ]
  node [
    id 313
    label "Nowa_Zelandia"
  ]
  node [
    id 314
    label "Liban"
  ]
  node [
    id 315
    label "Jordania"
  ]
  node [
    id 316
    label "Tonga"
  ]
  node [
    id 317
    label "Czad"
  ]
  node [
    id 318
    label "Liberia"
  ]
  node [
    id 319
    label "Gwinea"
  ]
  node [
    id 320
    label "Belize"
  ]
  node [
    id 321
    label "&#321;otwa"
  ]
  node [
    id 322
    label "Syria"
  ]
  node [
    id 323
    label "Benin"
  ]
  node [
    id 324
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 325
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 326
    label "Dominika"
  ]
  node [
    id 327
    label "Antigua_i_Barbuda"
  ]
  node [
    id 328
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 329
    label "Hanower"
  ]
  node [
    id 330
    label "partia"
  ]
  node [
    id 331
    label "Afganistan"
  ]
  node [
    id 332
    label "Kiribati"
  ]
  node [
    id 333
    label "W&#322;ochy"
  ]
  node [
    id 334
    label "Szwajcaria"
  ]
  node [
    id 335
    label "Sahara_Zachodnia"
  ]
  node [
    id 336
    label "Chorwacja"
  ]
  node [
    id 337
    label "Tajlandia"
  ]
  node [
    id 338
    label "Salwador"
  ]
  node [
    id 339
    label "Bahamy"
  ]
  node [
    id 340
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 341
    label "S&#322;owenia"
  ]
  node [
    id 342
    label "Gambia"
  ]
  node [
    id 343
    label "Urugwaj"
  ]
  node [
    id 344
    label "Zair"
  ]
  node [
    id 345
    label "Erytrea"
  ]
  node [
    id 346
    label "Rosja"
  ]
  node [
    id 347
    label "Uganda"
  ]
  node [
    id 348
    label "Niger"
  ]
  node [
    id 349
    label "Mauritius"
  ]
  node [
    id 350
    label "Turkmenistan"
  ]
  node [
    id 351
    label "Turcja"
  ]
  node [
    id 352
    label "Irlandia"
  ]
  node [
    id 353
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 354
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 355
    label "Gwinea_Bissau"
  ]
  node [
    id 356
    label "Belgia"
  ]
  node [
    id 357
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 358
    label "Palau"
  ]
  node [
    id 359
    label "Barbados"
  ]
  node [
    id 360
    label "Chile"
  ]
  node [
    id 361
    label "Wenezuela"
  ]
  node [
    id 362
    label "W&#281;gry"
  ]
  node [
    id 363
    label "Argentyna"
  ]
  node [
    id 364
    label "Kolumbia"
  ]
  node [
    id 365
    label "Sierra_Leone"
  ]
  node [
    id 366
    label "Azerbejd&#380;an"
  ]
  node [
    id 367
    label "Kongo"
  ]
  node [
    id 368
    label "Pakistan"
  ]
  node [
    id 369
    label "Liechtenstein"
  ]
  node [
    id 370
    label "Nikaragua"
  ]
  node [
    id 371
    label "Senegal"
  ]
  node [
    id 372
    label "Indie"
  ]
  node [
    id 373
    label "Suazi"
  ]
  node [
    id 374
    label "Polska"
  ]
  node [
    id 375
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 376
    label "Algieria"
  ]
  node [
    id 377
    label "terytorium"
  ]
  node [
    id 378
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 379
    label "Jamajka"
  ]
  node [
    id 380
    label "Kostaryka"
  ]
  node [
    id 381
    label "Timor_Wschodni"
  ]
  node [
    id 382
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 383
    label "Kuba"
  ]
  node [
    id 384
    label "Mauretania"
  ]
  node [
    id 385
    label "Portoryko"
  ]
  node [
    id 386
    label "Brazylia"
  ]
  node [
    id 387
    label "Mo&#322;dawia"
  ]
  node [
    id 388
    label "organizacja"
  ]
  node [
    id 389
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 390
    label "Litwa"
  ]
  node [
    id 391
    label "Kirgistan"
  ]
  node [
    id 392
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 393
    label "Izrael"
  ]
  node [
    id 394
    label "Grecja"
  ]
  node [
    id 395
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 396
    label "Holandia"
  ]
  node [
    id 397
    label "Sri_Lanka"
  ]
  node [
    id 398
    label "Katar"
  ]
  node [
    id 399
    label "Mikronezja"
  ]
  node [
    id 400
    label "Mongolia"
  ]
  node [
    id 401
    label "Laos"
  ]
  node [
    id 402
    label "Malediwy"
  ]
  node [
    id 403
    label "Zambia"
  ]
  node [
    id 404
    label "Tanzania"
  ]
  node [
    id 405
    label "Gujana"
  ]
  node [
    id 406
    label "Czechy"
  ]
  node [
    id 407
    label "Panama"
  ]
  node [
    id 408
    label "Uzbekistan"
  ]
  node [
    id 409
    label "Gruzja"
  ]
  node [
    id 410
    label "Serbia"
  ]
  node [
    id 411
    label "Francja"
  ]
  node [
    id 412
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 413
    label "Togo"
  ]
  node [
    id 414
    label "Estonia"
  ]
  node [
    id 415
    label "Oman"
  ]
  node [
    id 416
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 417
    label "Portugalia"
  ]
  node [
    id 418
    label "Boliwia"
  ]
  node [
    id 419
    label "Luksemburg"
  ]
  node [
    id 420
    label "Haiti"
  ]
  node [
    id 421
    label "Wyspy_Salomona"
  ]
  node [
    id 422
    label "Birma"
  ]
  node [
    id 423
    label "Rodezja"
  ]
  node [
    id 424
    label "transgress"
  ]
  node [
    id 425
    label "impart"
  ]
  node [
    id 426
    label "spowodowa&#263;"
  ]
  node [
    id 427
    label "distribute"
  ]
  node [
    id 428
    label "wydzieli&#263;"
  ]
  node [
    id 429
    label "divide"
  ]
  node [
    id 430
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 431
    label "przyzna&#263;"
  ]
  node [
    id 432
    label "policzy&#263;"
  ]
  node [
    id 433
    label "pigeonhole"
  ]
  node [
    id 434
    label "exchange"
  ]
  node [
    id 435
    label "rozda&#263;"
  ]
  node [
    id 436
    label "sk&#322;&#243;ci&#263;"
  ]
  node [
    id 437
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 438
    label "zrobi&#263;"
  ]
  node [
    id 439
    label "change"
  ]
  node [
    id 440
    label "Chrystus"
  ]
  node [
    id 441
    label "Kazimierz"
  ]
  node [
    id 442
    label "po&#322;udniowy"
  ]
  node [
    id 443
    label "sony"
  ]
  node [
    id 444
    label "ericsson"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 440
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 201
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 441
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 210
  ]
  edge [
    source 27
    target 211
  ]
  edge [
    source 27
    target 212
  ]
  edge [
    source 27
    target 213
  ]
  edge [
    source 27
    target 214
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 216
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 27
    target 228
  ]
  edge [
    source 27
    target 229
  ]
  edge [
    source 27
    target 230
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 232
  ]
  edge [
    source 27
    target 233
  ]
  edge [
    source 27
    target 234
  ]
  edge [
    source 27
    target 235
  ]
  edge [
    source 27
    target 236
  ]
  edge [
    source 27
    target 237
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 239
  ]
  edge [
    source 27
    target 240
  ]
  edge [
    source 27
    target 241
  ]
  edge [
    source 27
    target 242
  ]
  edge [
    source 27
    target 243
  ]
  edge [
    source 27
    target 244
  ]
  edge [
    source 27
    target 245
  ]
  edge [
    source 27
    target 246
  ]
  edge [
    source 27
    target 247
  ]
  edge [
    source 27
    target 248
  ]
  edge [
    source 27
    target 249
  ]
  edge [
    source 27
    target 250
  ]
  edge [
    source 27
    target 251
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 253
  ]
  edge [
    source 27
    target 254
  ]
  edge [
    source 27
    target 255
  ]
  edge [
    source 27
    target 256
  ]
  edge [
    source 27
    target 257
  ]
  edge [
    source 27
    target 258
  ]
  edge [
    source 27
    target 259
  ]
  edge [
    source 27
    target 260
  ]
  edge [
    source 27
    target 261
  ]
  edge [
    source 27
    target 262
  ]
  edge [
    source 27
    target 263
  ]
  edge [
    source 27
    target 264
  ]
  edge [
    source 27
    target 265
  ]
  edge [
    source 27
    target 266
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 27
    target 268
  ]
  edge [
    source 27
    target 269
  ]
  edge [
    source 27
    target 270
  ]
  edge [
    source 27
    target 271
  ]
  edge [
    source 27
    target 272
  ]
  edge [
    source 27
    target 273
  ]
  edge [
    source 27
    target 274
  ]
  edge [
    source 27
    target 275
  ]
  edge [
    source 27
    target 276
  ]
  edge [
    source 27
    target 277
  ]
  edge [
    source 27
    target 278
  ]
  edge [
    source 27
    target 279
  ]
  edge [
    source 27
    target 280
  ]
  edge [
    source 27
    target 281
  ]
  edge [
    source 27
    target 282
  ]
  edge [
    source 27
    target 39
  ]
  edge [
    source 27
    target 283
  ]
  edge [
    source 27
    target 284
  ]
  edge [
    source 27
    target 285
  ]
  edge [
    source 27
    target 286
  ]
  edge [
    source 27
    target 287
  ]
  edge [
    source 27
    target 288
  ]
  edge [
    source 27
    target 289
  ]
  edge [
    source 27
    target 290
  ]
  edge [
    source 27
    target 291
  ]
  edge [
    source 27
    target 292
  ]
  edge [
    source 27
    target 293
  ]
  edge [
    source 27
    target 294
  ]
  edge [
    source 27
    target 295
  ]
  edge [
    source 27
    target 296
  ]
  edge [
    source 27
    target 297
  ]
  edge [
    source 27
    target 298
  ]
  edge [
    source 27
    target 299
  ]
  edge [
    source 27
    target 300
  ]
  edge [
    source 27
    target 301
  ]
  edge [
    source 27
    target 302
  ]
  edge [
    source 27
    target 303
  ]
  edge [
    source 27
    target 304
  ]
  edge [
    source 27
    target 305
  ]
  edge [
    source 27
    target 306
  ]
  edge [
    source 27
    target 307
  ]
  edge [
    source 27
    target 308
  ]
  edge [
    source 27
    target 309
  ]
  edge [
    source 27
    target 310
  ]
  edge [
    source 27
    target 311
  ]
  edge [
    source 27
    target 312
  ]
  edge [
    source 27
    target 313
  ]
  edge [
    source 27
    target 314
  ]
  edge [
    source 27
    target 315
  ]
  edge [
    source 27
    target 316
  ]
  edge [
    source 27
    target 317
  ]
  edge [
    source 27
    target 318
  ]
  edge [
    source 27
    target 319
  ]
  edge [
    source 27
    target 320
  ]
  edge [
    source 27
    target 321
  ]
  edge [
    source 27
    target 322
  ]
  edge [
    source 27
    target 323
  ]
  edge [
    source 27
    target 324
  ]
  edge [
    source 27
    target 325
  ]
  edge [
    source 27
    target 326
  ]
  edge [
    source 27
    target 327
  ]
  edge [
    source 27
    target 328
  ]
  edge [
    source 27
    target 329
  ]
  edge [
    source 27
    target 330
  ]
  edge [
    source 27
    target 331
  ]
  edge [
    source 27
    target 332
  ]
  edge [
    source 27
    target 333
  ]
  edge [
    source 27
    target 334
  ]
  edge [
    source 27
    target 335
  ]
  edge [
    source 27
    target 336
  ]
  edge [
    source 27
    target 337
  ]
  edge [
    source 27
    target 338
  ]
  edge [
    source 27
    target 339
  ]
  edge [
    source 27
    target 340
  ]
  edge [
    source 27
    target 341
  ]
  edge [
    source 27
    target 342
  ]
  edge [
    source 27
    target 343
  ]
  edge [
    source 27
    target 344
  ]
  edge [
    source 27
    target 345
  ]
  edge [
    source 27
    target 346
  ]
  edge [
    source 27
    target 347
  ]
  edge [
    source 27
    target 348
  ]
  edge [
    source 27
    target 349
  ]
  edge [
    source 27
    target 350
  ]
  edge [
    source 27
    target 351
  ]
  edge [
    source 27
    target 352
  ]
  edge [
    source 27
    target 353
  ]
  edge [
    source 27
    target 354
  ]
  edge [
    source 27
    target 355
  ]
  edge [
    source 27
    target 356
  ]
  edge [
    source 27
    target 357
  ]
  edge [
    source 27
    target 358
  ]
  edge [
    source 27
    target 359
  ]
  edge [
    source 27
    target 360
  ]
  edge [
    source 27
    target 361
  ]
  edge [
    source 27
    target 362
  ]
  edge [
    source 27
    target 363
  ]
  edge [
    source 27
    target 364
  ]
  edge [
    source 27
    target 365
  ]
  edge [
    source 27
    target 366
  ]
  edge [
    source 27
    target 367
  ]
  edge [
    source 27
    target 368
  ]
  edge [
    source 27
    target 369
  ]
  edge [
    source 27
    target 370
  ]
  edge [
    source 27
    target 371
  ]
  edge [
    source 27
    target 372
  ]
  edge [
    source 27
    target 373
  ]
  edge [
    source 27
    target 374
  ]
  edge [
    source 27
    target 375
  ]
  edge [
    source 27
    target 376
  ]
  edge [
    source 27
    target 377
  ]
  edge [
    source 27
    target 378
  ]
  edge [
    source 27
    target 379
  ]
  edge [
    source 27
    target 380
  ]
  edge [
    source 27
    target 381
  ]
  edge [
    source 27
    target 382
  ]
  edge [
    source 27
    target 383
  ]
  edge [
    source 27
    target 384
  ]
  edge [
    source 27
    target 385
  ]
  edge [
    source 27
    target 386
  ]
  edge [
    source 27
    target 387
  ]
  edge [
    source 27
    target 388
  ]
  edge [
    source 27
    target 389
  ]
  edge [
    source 27
    target 390
  ]
  edge [
    source 27
    target 391
  ]
  edge [
    source 27
    target 392
  ]
  edge [
    source 27
    target 393
  ]
  edge [
    source 27
    target 394
  ]
  edge [
    source 27
    target 395
  ]
  edge [
    source 27
    target 396
  ]
  edge [
    source 27
    target 397
  ]
  edge [
    source 27
    target 398
  ]
  edge [
    source 27
    target 399
  ]
  edge [
    source 27
    target 400
  ]
  edge [
    source 27
    target 401
  ]
  edge [
    source 27
    target 402
  ]
  edge [
    source 27
    target 403
  ]
  edge [
    source 27
    target 404
  ]
  edge [
    source 27
    target 405
  ]
  edge [
    source 27
    target 406
  ]
  edge [
    source 27
    target 407
  ]
  edge [
    source 27
    target 408
  ]
  edge [
    source 27
    target 409
  ]
  edge [
    source 27
    target 410
  ]
  edge [
    source 27
    target 411
  ]
  edge [
    source 27
    target 412
  ]
  edge [
    source 27
    target 413
  ]
  edge [
    source 27
    target 414
  ]
  edge [
    source 27
    target 415
  ]
  edge [
    source 27
    target 416
  ]
  edge [
    source 27
    target 417
  ]
  edge [
    source 27
    target 418
  ]
  edge [
    source 27
    target 419
  ]
  edge [
    source 27
    target 420
  ]
  edge [
    source 27
    target 421
  ]
  edge [
    source 27
    target 422
  ]
  edge [
    source 27
    target 423
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 424
  ]
  edge [
    source 29
    target 425
  ]
  edge [
    source 29
    target 426
  ]
  edge [
    source 29
    target 427
  ]
  edge [
    source 29
    target 428
  ]
  edge [
    source 29
    target 429
  ]
  edge [
    source 29
    target 430
  ]
  edge [
    source 29
    target 431
  ]
  edge [
    source 29
    target 432
  ]
  edge [
    source 29
    target 433
  ]
  edge [
    source 29
    target 434
  ]
  edge [
    source 29
    target 435
  ]
  edge [
    source 29
    target 436
  ]
  edge [
    source 29
    target 437
  ]
  edge [
    source 29
    target 438
  ]
  edge [
    source 29
    target 439
  ]
  edge [
    source 214
    target 442
  ]
  edge [
    source 443
    target 444
  ]
]
