graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "uwe"
    origin "text"
  ]
  node [
    id 1
    label "beyer"
    origin "text"
  ]
  node [
    id 2
    label "Uwe"
  ]
  node [
    id 3
    label "Beyer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
]
