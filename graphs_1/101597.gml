graph [
  maxDegree 11
  minDegree 1
  meanDegree 2
  density 0.09523809523809523
  graphCliqueNumber 2
  node [
    id 0
    label "istotnie"
    origin "text"
  ]
  node [
    id 1
    label "winny"
    origin "text"
  ]
  node [
    id 2
    label "kulka"
    origin "text"
  ]
  node [
    id 3
    label "&#322;eba"
    origin "text"
  ]
  node [
    id 4
    label "realnie"
  ]
  node [
    id 5
    label "importantly"
  ]
  node [
    id 6
    label "istotny"
  ]
  node [
    id 7
    label "wa&#380;ny"
  ]
  node [
    id 8
    label "cierpki"
  ]
  node [
    id 9
    label "alkoholowy"
  ]
  node [
    id 10
    label "przewinienie"
  ]
  node [
    id 11
    label "sprawca"
  ]
  node [
    id 12
    label "odpowiadanie"
  ]
  node [
    id 13
    label "sphere"
  ]
  node [
    id 14
    label "czasza"
  ]
  node [
    id 15
    label "ma&#322;y_przedmiot"
  ]
  node [
    id 16
    label "pocisk"
  ]
  node [
    id 17
    label "kula"
  ]
  node [
    id 18
    label "ball"
  ]
  node [
    id 19
    label "porcja"
  ]
  node [
    id 20
    label "bry&#322;a"
  ]
  node [
    id 21
    label "marmurki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
]
