graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.0106382978723403
  density 0.010752076459210377
  graphCliqueNumber 2
  node [
    id 0
    label "zimowy"
    origin "text"
  ]
  node [
    id 1
    label "zwyczaj"
    origin "text"
  ]
  node [
    id 2
    label "publikowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kolejny"
    origin "text"
  ]
  node [
    id 4
    label "gor&#261;c"
    origin "text"
  ]
  node [
    id 5
    label "wie&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "prosto"
    origin "text"
  ]
  node [
    id 7
    label "jura"
    origin "text"
  ]
  node [
    id 8
    label "konkurs"
    origin "text"
  ]
  node [
    id 9
    label "qmam"
    origin "text"
  ]
  node [
    id 10
    label "media"
    origin "text"
  ]
  node [
    id 11
    label "oto"
    origin "text"
  ]
  node [
    id 12
    label "zwyci&#281;zca"
    origin "text"
  ]
  node [
    id 13
    label "miesi&#281;czny"
    origin "text"
  ]
  node [
    id 14
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 15
    label "dwa"
    origin "text"
  ]
  node [
    id 16
    label "kategoria"
    origin "text"
  ]
  node [
    id 17
    label "qmama"
    origin "text"
  ]
  node [
    id 18
    label "warsztatowy"
    origin "text"
  ]
  node [
    id 19
    label "szkolny"
    origin "text"
  ]
  node [
    id 20
    label "magiel"
    origin "text"
  ]
  node [
    id 21
    label "anta"
    origin "text"
  ]
  node [
    id 22
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 23
    label "chaos"
    origin "text"
  ]
  node [
    id 24
    label "cykliczny"
    origin "text"
  ]
  node [
    id 25
    label "platern&#243;wka"
    origin "text"
  ]
  node [
    id 26
    label "ladies"
    origin "text"
  ]
  node [
    id 27
    label "sezonowy"
  ]
  node [
    id 28
    label "zimowo"
  ]
  node [
    id 29
    label "ch&#322;odny"
  ]
  node [
    id 30
    label "typowy"
  ]
  node [
    id 31
    label "hibernowy"
  ]
  node [
    id 32
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 33
    label "zachowanie"
  ]
  node [
    id 34
    label "kultura"
  ]
  node [
    id 35
    label "kultura_duchowa"
  ]
  node [
    id 36
    label "ceremony"
  ]
  node [
    id 37
    label "wydawnictwo"
  ]
  node [
    id 38
    label "wprowadza&#263;"
  ]
  node [
    id 39
    label "upublicznia&#263;"
  ]
  node [
    id 40
    label "give"
  ]
  node [
    id 41
    label "inny"
  ]
  node [
    id 42
    label "nast&#281;pnie"
  ]
  node [
    id 43
    label "kt&#243;ry&#347;"
  ]
  node [
    id 44
    label "kolejno"
  ]
  node [
    id 45
    label "nastopny"
  ]
  node [
    id 46
    label "ardor"
  ]
  node [
    id 47
    label "ciep&#322;o"
  ]
  node [
    id 48
    label "war"
  ]
  node [
    id 49
    label "control"
  ]
  node [
    id 50
    label "zniesienie"
  ]
  node [
    id 51
    label "manage"
  ]
  node [
    id 52
    label "depesza_emska"
  ]
  node [
    id 53
    label "message"
  ]
  node [
    id 54
    label "signal"
  ]
  node [
    id 55
    label "zarys"
  ]
  node [
    id 56
    label "komunikat"
  ]
  node [
    id 57
    label "prowadzi&#263;"
  ]
  node [
    id 58
    label "kierowa&#263;"
  ]
  node [
    id 59
    label "znosi&#263;"
  ]
  node [
    id 60
    label "informacja"
  ]
  node [
    id 61
    label "znie&#347;&#263;"
  ]
  node [
    id 62
    label "manipulate"
  ]
  node [
    id 63
    label "communication"
  ]
  node [
    id 64
    label "znoszenie"
  ]
  node [
    id 65
    label "navigate"
  ]
  node [
    id 66
    label "przewodniczy&#263;"
  ]
  node [
    id 67
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 68
    label "cope"
  ]
  node [
    id 69
    label "nap&#322;ywanie"
  ]
  node [
    id 70
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 71
    label "powodowa&#263;"
  ]
  node [
    id 72
    label "popycha&#263;"
  ]
  node [
    id 73
    label "p&#281;dzi&#263;"
  ]
  node [
    id 74
    label "elementarily"
  ]
  node [
    id 75
    label "bezpo&#347;rednio"
  ]
  node [
    id 76
    label "naturalnie"
  ]
  node [
    id 77
    label "skromnie"
  ]
  node [
    id 78
    label "prosty"
  ]
  node [
    id 79
    label "&#322;atwo"
  ]
  node [
    id 80
    label "niepozornie"
  ]
  node [
    id 81
    label "formacja_geologiczna"
  ]
  node [
    id 82
    label "jura_&#347;rodkowa"
  ]
  node [
    id 83
    label "dogger"
  ]
  node [
    id 84
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 85
    label "era_mezozoiczna"
  ]
  node [
    id 86
    label "plezjozaur"
  ]
  node [
    id 87
    label "euoplocefal"
  ]
  node [
    id 88
    label "jura_wczesna"
  ]
  node [
    id 89
    label "eliminacje"
  ]
  node [
    id 90
    label "Interwizja"
  ]
  node [
    id 91
    label "emulation"
  ]
  node [
    id 92
    label "impreza"
  ]
  node [
    id 93
    label "casting"
  ]
  node [
    id 94
    label "Eurowizja"
  ]
  node [
    id 95
    label "nab&#243;r"
  ]
  node [
    id 96
    label "przekazior"
  ]
  node [
    id 97
    label "mass-media"
  ]
  node [
    id 98
    label "uzbrajanie"
  ]
  node [
    id 99
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 100
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 101
    label "medium"
  ]
  node [
    id 102
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 103
    label "zwyci&#281;&#380;yciel"
  ]
  node [
    id 104
    label "uczestnik"
  ]
  node [
    id 105
    label "jasny"
  ]
  node [
    id 106
    label "miesi&#281;cznie"
  ]
  node [
    id 107
    label "trzydziestodniowy"
  ]
  node [
    id 108
    label "kilkudziesi&#281;ciodniowy"
  ]
  node [
    id 109
    label "czynno&#347;&#263;"
  ]
  node [
    id 110
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 111
    label "decyzja"
  ]
  node [
    id 112
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 113
    label "pick"
  ]
  node [
    id 114
    label "forma"
  ]
  node [
    id 115
    label "wytw&#243;r"
  ]
  node [
    id 116
    label "type"
  ]
  node [
    id 117
    label "teoria"
  ]
  node [
    id 118
    label "zbi&#243;r"
  ]
  node [
    id 119
    label "poj&#281;cie"
  ]
  node [
    id 120
    label "klasa"
  ]
  node [
    id 121
    label "technicznie"
  ]
  node [
    id 122
    label "szkolnie"
  ]
  node [
    id 123
    label "szkoleniowy"
  ]
  node [
    id 124
    label "podstawowy"
  ]
  node [
    id 125
    label "rozmowa"
  ]
  node [
    id 126
    label "maglownik"
  ]
  node [
    id 127
    label "zak&#322;ad"
  ]
  node [
    id 128
    label "egzamin"
  ]
  node [
    id 129
    label "t&#322;ok"
  ]
  node [
    id 130
    label "wydarzenie"
  ]
  node [
    id 131
    label "punkt"
  ]
  node [
    id 132
    label "urz&#261;dzenie"
  ]
  node [
    id 133
    label "plotka"
  ]
  node [
    id 134
    label "przes&#322;uchanie"
  ]
  node [
    id 135
    label "pilaster"
  ]
  node [
    id 136
    label "Mickiewicz"
  ]
  node [
    id 137
    label "czas"
  ]
  node [
    id 138
    label "szkolenie"
  ]
  node [
    id 139
    label "przepisa&#263;"
  ]
  node [
    id 140
    label "lesson"
  ]
  node [
    id 141
    label "grupa"
  ]
  node [
    id 142
    label "praktyka"
  ]
  node [
    id 143
    label "metoda"
  ]
  node [
    id 144
    label "niepokalanki"
  ]
  node [
    id 145
    label "kara"
  ]
  node [
    id 146
    label "zda&#263;"
  ]
  node [
    id 147
    label "form"
  ]
  node [
    id 148
    label "kwalifikacje"
  ]
  node [
    id 149
    label "system"
  ]
  node [
    id 150
    label "przepisanie"
  ]
  node [
    id 151
    label "sztuba"
  ]
  node [
    id 152
    label "wiedza"
  ]
  node [
    id 153
    label "stopek"
  ]
  node [
    id 154
    label "school"
  ]
  node [
    id 155
    label "absolwent"
  ]
  node [
    id 156
    label "urszulanki"
  ]
  node [
    id 157
    label "gabinet"
  ]
  node [
    id 158
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 159
    label "ideologia"
  ]
  node [
    id 160
    label "lekcja"
  ]
  node [
    id 161
    label "muzyka"
  ]
  node [
    id 162
    label "podr&#281;cznik"
  ]
  node [
    id 163
    label "zdanie"
  ]
  node [
    id 164
    label "siedziba"
  ]
  node [
    id 165
    label "sekretariat"
  ]
  node [
    id 166
    label "nauka"
  ]
  node [
    id 167
    label "do&#347;wiadczenie"
  ]
  node [
    id 168
    label "tablica"
  ]
  node [
    id 169
    label "teren_szko&#322;y"
  ]
  node [
    id 170
    label "instytucja"
  ]
  node [
    id 171
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 172
    label "skolaryzacja"
  ]
  node [
    id 173
    label "&#322;awa_szkolna"
  ]
  node [
    id 174
    label "&#380;mij"
  ]
  node [
    id 175
    label "zdezorganizowanie"
  ]
  node [
    id 176
    label "cecha"
  ]
  node [
    id 177
    label "disorder"
  ]
  node [
    id 178
    label "materia"
  ]
  node [
    id 179
    label "rozpierducha"
  ]
  node [
    id 180
    label "ba&#322;agan"
  ]
  node [
    id 181
    label "ko&#322;omyja"
  ]
  node [
    id 182
    label "sytuacja"
  ]
  node [
    id 183
    label "tyrocydyna"
  ]
  node [
    id 184
    label "zwi&#261;zek_cykliczny"
  ]
  node [
    id 185
    label "zwi&#261;zek_heterocykliczny"
  ]
  node [
    id 186
    label "cyklicznie"
  ]
  node [
    id 187
    label "regularny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 78
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 135
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 136
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 138
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 22
    target 140
  ]
  edge [
    source 22
    target 141
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 22
    target 143
  ]
  edge [
    source 22
    target 144
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 147
  ]
  edge [
    source 22
    target 148
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 22
    target 150
  ]
  edge [
    source 22
    target 151
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 22
    target 155
  ]
  edge [
    source 22
    target 156
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 22
    target 120
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 174
  ]
  edge [
    source 23
    target 175
  ]
  edge [
    source 23
    target 176
  ]
  edge [
    source 23
    target 177
  ]
  edge [
    source 23
    target 178
  ]
  edge [
    source 23
    target 179
  ]
  edge [
    source 23
    target 180
  ]
  edge [
    source 23
    target 181
  ]
  edge [
    source 23
    target 182
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 25
    target 26
  ]
]
