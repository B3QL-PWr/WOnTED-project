graph [
  maxDegree 13
  minDegree 1
  meanDegree 2
  density 0.07407407407407407
  graphCliqueNumber 2
  node [
    id 0
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "naprawd&#281;"
    origin "text"
  ]
  node [
    id 3
    label "dobrze"
    origin "text"
  ]
  node [
    id 4
    label "czu&#263;"
    origin "text"
  ]
  node [
    id 5
    label "desire"
  ]
  node [
    id 6
    label "kcie&#263;"
  ]
  node [
    id 7
    label "prawdziwy"
  ]
  node [
    id 8
    label "moralnie"
  ]
  node [
    id 9
    label "wiele"
  ]
  node [
    id 10
    label "lepiej"
  ]
  node [
    id 11
    label "korzystnie"
  ]
  node [
    id 12
    label "pomy&#347;lnie"
  ]
  node [
    id 13
    label "pozytywnie"
  ]
  node [
    id 14
    label "dobry"
  ]
  node [
    id 15
    label "dobroczynnie"
  ]
  node [
    id 16
    label "odpowiednio"
  ]
  node [
    id 17
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 18
    label "skutecznie"
  ]
  node [
    id 19
    label "by&#263;"
  ]
  node [
    id 20
    label "uczuwa&#263;"
  ]
  node [
    id 21
    label "przewidywa&#263;"
  ]
  node [
    id 22
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 23
    label "smell"
  ]
  node [
    id 24
    label "postrzega&#263;"
  ]
  node [
    id 25
    label "doznawa&#263;"
  ]
  node [
    id 26
    label "spirit"
  ]
  node [
    id 27
    label "anticipate"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
]
