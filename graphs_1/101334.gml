graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 1
    label "bojowy"
    origin "text"
  ]
  node [
    id 2
    label "strategia"
  ]
  node [
    id 3
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 4
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 5
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 6
    label "waleczny"
  ]
  node [
    id 7
    label "pewny"
  ]
  node [
    id 8
    label "bojowo"
  ]
  node [
    id 9
    label "bojowniczy"
  ]
  node [
    id 10
    label "&#347;mia&#322;y"
  ]
  node [
    id 11
    label "zadziorny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
]
