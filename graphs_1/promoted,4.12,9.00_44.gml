graph [
  maxDegree 24
  minDegree 1
  meanDegree 2
  density 0.018518518518518517
  graphCliqueNumber 2
  node [
    id 0
    label "bethesda"
    origin "text"
  ]
  node [
    id 1
    label "t&#322;umaczy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "materia&#322;owy"
    origin "text"
  ]
  node [
    id 4
    label "torba"
    origin "text"
  ]
  node [
    id 5
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 6
    label "zbyt"
    origin "text"
  ]
  node [
    id 7
    label "drogi"
    origin "text"
  ]
  node [
    id 8
    label "produkcja"
    origin "text"
  ]
  node [
    id 9
    label "aby"
    origin "text"
  ]
  node [
    id 10
    label "dorzuci&#263;"
    origin "text"
  ]
  node [
    id 11
    label "kosztowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 13
    label "edycja"
    origin "text"
  ]
  node [
    id 14
    label "kolekcjonerski"
    origin "text"
  ]
  node [
    id 15
    label "fallouta"
    origin "text"
  ]
  node [
    id 16
    label "przek&#322;ada&#263;"
  ]
  node [
    id 17
    label "uzasadnia&#263;"
  ]
  node [
    id 18
    label "poja&#347;nia&#263;"
  ]
  node [
    id 19
    label "elaborate"
  ]
  node [
    id 20
    label "explain"
  ]
  node [
    id 21
    label "suplikowa&#263;"
  ]
  node [
    id 22
    label "j&#281;zyk"
  ]
  node [
    id 23
    label "sprawowa&#263;"
  ]
  node [
    id 24
    label "robi&#263;"
  ]
  node [
    id 25
    label "give"
  ]
  node [
    id 26
    label "przekonywa&#263;"
  ]
  node [
    id 27
    label "u&#322;atwia&#263;"
  ]
  node [
    id 28
    label "broni&#263;"
  ]
  node [
    id 29
    label "interpretowa&#263;"
  ]
  node [
    id 30
    label "przedstawia&#263;"
  ]
  node [
    id 31
    label "tekstylny"
  ]
  node [
    id 32
    label "baba"
  ]
  node [
    id 33
    label "baga&#380;"
  ]
  node [
    id 34
    label "opakowanie"
  ]
  node [
    id 35
    label "fa&#322;da"
  ]
  node [
    id 36
    label "dawny"
  ]
  node [
    id 37
    label "rozw&#243;d"
  ]
  node [
    id 38
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 39
    label "eksprezydent"
  ]
  node [
    id 40
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 41
    label "partner"
  ]
  node [
    id 42
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 43
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 44
    label "wcze&#347;niejszy"
  ]
  node [
    id 45
    label "nadmiernie"
  ]
  node [
    id 46
    label "sprzedawanie"
  ]
  node [
    id 47
    label "sprzeda&#380;"
  ]
  node [
    id 48
    label "cz&#322;owiek"
  ]
  node [
    id 49
    label "warto&#347;ciowy"
  ]
  node [
    id 50
    label "bliski"
  ]
  node [
    id 51
    label "drogo"
  ]
  node [
    id 52
    label "przyjaciel"
  ]
  node [
    id 53
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 54
    label "mi&#322;y"
  ]
  node [
    id 55
    label "tingel-tangel"
  ]
  node [
    id 56
    label "odtworzenie"
  ]
  node [
    id 57
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 58
    label "wydawa&#263;"
  ]
  node [
    id 59
    label "realizacja"
  ]
  node [
    id 60
    label "monta&#380;"
  ]
  node [
    id 61
    label "rozw&#243;j"
  ]
  node [
    id 62
    label "fabrication"
  ]
  node [
    id 63
    label "kreacja"
  ]
  node [
    id 64
    label "uzysk"
  ]
  node [
    id 65
    label "dorobek"
  ]
  node [
    id 66
    label "wyda&#263;"
  ]
  node [
    id 67
    label "impreza"
  ]
  node [
    id 68
    label "postprodukcja"
  ]
  node [
    id 69
    label "numer"
  ]
  node [
    id 70
    label "kooperowa&#263;"
  ]
  node [
    id 71
    label "creation"
  ]
  node [
    id 72
    label "trema"
  ]
  node [
    id 73
    label "zbi&#243;r"
  ]
  node [
    id 74
    label "product"
  ]
  node [
    id 75
    label "performance"
  ]
  node [
    id 76
    label "troch&#281;"
  ]
  node [
    id 77
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 78
    label "bewilder"
  ]
  node [
    id 79
    label "do&#322;o&#380;y&#263;"
  ]
  node [
    id 80
    label "by&#263;"
  ]
  node [
    id 81
    label "cena"
  ]
  node [
    id 82
    label "try"
  ]
  node [
    id 83
    label "essay"
  ]
  node [
    id 84
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 85
    label "doznawa&#263;"
  ]
  node [
    id 86
    label "savor"
  ]
  node [
    id 87
    label "szlachetny"
  ]
  node [
    id 88
    label "metaliczny"
  ]
  node [
    id 89
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 90
    label "z&#322;ocenie"
  ]
  node [
    id 91
    label "grosz"
  ]
  node [
    id 92
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 93
    label "utytu&#322;owany"
  ]
  node [
    id 94
    label "poz&#322;ocenie"
  ]
  node [
    id 95
    label "Polska"
  ]
  node [
    id 96
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 97
    label "wspania&#322;y"
  ]
  node [
    id 98
    label "doskona&#322;y"
  ]
  node [
    id 99
    label "kochany"
  ]
  node [
    id 100
    label "jednostka_monetarna"
  ]
  node [
    id 101
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 102
    label "impression"
  ]
  node [
    id 103
    label "odmiana"
  ]
  node [
    id 104
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 105
    label "notification"
  ]
  node [
    id 106
    label "cykl"
  ]
  node [
    id 107
    label "zmiana"
  ]
  node [
    id 108
    label "egzemplarz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 14
    target 15
  ]
]
