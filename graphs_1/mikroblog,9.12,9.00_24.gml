graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9555555555555555
  density 0.044444444444444446
  graphCliqueNumber 2
  node [
    id 0
    label "jejku"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "ciesa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mama"
    origin "text"
  ]
  node [
    id 4
    label "wolny"
    origin "text"
  ]
  node [
    id 5
    label "dom"
    origin "text"
  ]
  node [
    id 6
    label "matczysko"
  ]
  node [
    id 7
    label "macierz"
  ]
  node [
    id 8
    label "przodkini"
  ]
  node [
    id 9
    label "Matka_Boska"
  ]
  node [
    id 10
    label "macocha"
  ]
  node [
    id 11
    label "matka_zast&#281;pcza"
  ]
  node [
    id 12
    label "stara"
  ]
  node [
    id 13
    label "rodzice"
  ]
  node [
    id 14
    label "rodzic"
  ]
  node [
    id 15
    label "niezale&#380;ny"
  ]
  node [
    id 16
    label "swobodnie"
  ]
  node [
    id 17
    label "niespieszny"
  ]
  node [
    id 18
    label "rozrzedzanie"
  ]
  node [
    id 19
    label "zwolnienie_si&#281;"
  ]
  node [
    id 20
    label "wolno"
  ]
  node [
    id 21
    label "rozrzedzenie"
  ]
  node [
    id 22
    label "lu&#378;no"
  ]
  node [
    id 23
    label "zwalnianie_si&#281;"
  ]
  node [
    id 24
    label "wolnie"
  ]
  node [
    id 25
    label "strza&#322;"
  ]
  node [
    id 26
    label "rozwodnienie"
  ]
  node [
    id 27
    label "wakowa&#263;"
  ]
  node [
    id 28
    label "rozwadnianie"
  ]
  node [
    id 29
    label "rzedni&#281;cie"
  ]
  node [
    id 30
    label "zrzedni&#281;cie"
  ]
  node [
    id 31
    label "garderoba"
  ]
  node [
    id 32
    label "wiecha"
  ]
  node [
    id 33
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 34
    label "grupa"
  ]
  node [
    id 35
    label "budynek"
  ]
  node [
    id 36
    label "fratria"
  ]
  node [
    id 37
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 38
    label "poj&#281;cie"
  ]
  node [
    id 39
    label "rodzina"
  ]
  node [
    id 40
    label "substancja_mieszkaniowa"
  ]
  node [
    id 41
    label "instytucja"
  ]
  node [
    id 42
    label "dom_rodzinny"
  ]
  node [
    id 43
    label "stead"
  ]
  node [
    id 44
    label "siedziba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
]
