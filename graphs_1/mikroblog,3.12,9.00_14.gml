graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "jezus"
    origin "text"
  ]
  node [
    id 1
    label "ca&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "przez"
    origin "text"
  ]
  node [
    id 3
    label "judasz"
    origin "text"
  ]
  node [
    id 4
    label "koloryzowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "rolnikszukazony"
    origin "text"
  ]
  node [
    id 6
    label "dotyka&#263;"
  ]
  node [
    id 7
    label "smack"
  ]
  node [
    id 8
    label "smokta&#263;"
  ]
  node [
    id 9
    label "cz&#322;owiek"
  ]
  node [
    id 10
    label "drzwi"
  ]
  node [
    id 11
    label "istota_&#380;ywa"
  ]
  node [
    id 12
    label "wziernik"
  ]
  node [
    id 13
    label "wizjerka"
  ]
  node [
    id 14
    label "barwi&#263;"
  ]
  node [
    id 15
    label "ubarwia&#263;"
  ]
  node [
    id 16
    label "przesadza&#263;"
  ]
  node [
    id 17
    label "color"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
]
