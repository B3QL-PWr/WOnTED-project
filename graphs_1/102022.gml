graph [
  maxDegree 49
  minDegree 1
  meanDegree 2
  density 0.007246376811594203
  graphCliqueNumber 3
  node [
    id 0
    label "bogumi&#322;"
    origin "text"
  ]
  node [
    id 1
    label "luft"
    origin "text"
  ]
  node [
    id 2
    label "jeden"
    origin "text"
  ]
  node [
    id 3
    label "filar"
    origin "text"
  ]
  node [
    id 4
    label "publicystyczny"
    origin "text"
  ]
  node [
    id 5
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 6
    label "nie"
    origin "text"
  ]
  node [
    id 7
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "jak"
    origin "text"
  ]
  node [
    id 9
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 10
    label "lo&#380;a"
    origin "text"
  ]
  node [
    id 11
    label "prasowy"
    origin "text"
  ]
  node [
    id 12
    label "popularny"
    origin "text"
  ]
  node [
    id 13
    label "program"
    origin "text"
  ]
  node [
    id 14
    label "prowadzony"
    origin "text"
  ]
  node [
    id 15
    label "przez"
    origin "text"
  ]
  node [
    id 16
    label "sympatyczny"
    origin "text"
  ]
  node [
    id 17
    label "spokojny"
    origin "text"
  ]
  node [
    id 18
    label "kompetentny"
    origin "text"
  ]
  node [
    id 19
    label "&#322;aszczy&#263;"
    origin "text"
  ]
  node [
    id 20
    label "odej&#347;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 22
    label "jednocze&#347;nie"
    origin "text"
  ]
  node [
    id 23
    label "nadej&#347;&#263;"
    origin "text"
  ]
  node [
    id 24
    label "informacja"
    origin "text"
  ]
  node [
    id 25
    label "masowy"
    origin "text"
  ]
  node [
    id 26
    label "zwolnienie"
    origin "text"
  ]
  node [
    id 27
    label "nowy"
    origin "text"
  ]
  node [
    id 28
    label "ultraprawicowego"
    origin "text"
  ]
  node [
    id 29
    label "ultrakatolicki"
    origin "text"
  ]
  node [
    id 30
    label "pawe&#322;"
    origin "text"
  ]
  node [
    id 31
    label "poni&#380;ej"
    origin "text"
  ]
  node [
    id 32
    label "piec"
  ]
  node [
    id 33
    label "powietrze"
  ]
  node [
    id 34
    label "przew&#243;d"
  ]
  node [
    id 35
    label "komin"
  ]
  node [
    id 36
    label "kieliszek"
  ]
  node [
    id 37
    label "shot"
  ]
  node [
    id 38
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 39
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 40
    label "jaki&#347;"
  ]
  node [
    id 41
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 42
    label "jednolicie"
  ]
  node [
    id 43
    label "w&#243;dka"
  ]
  node [
    id 44
    label "ten"
  ]
  node [
    id 45
    label "ujednolicenie"
  ]
  node [
    id 46
    label "jednakowy"
  ]
  node [
    id 47
    label "cz&#322;owiek"
  ]
  node [
    id 48
    label "miejsce"
  ]
  node [
    id 49
    label "podstawowy"
  ]
  node [
    id 50
    label "zasadzenie"
  ]
  node [
    id 51
    label "za&#322;o&#380;enie"
  ]
  node [
    id 52
    label "documentation"
  ]
  node [
    id 53
    label "kszta&#322;t"
  ]
  node [
    id 54
    label "punkt_odniesienia"
  ]
  node [
    id 55
    label "system_emerytalny"
  ]
  node [
    id 56
    label "podpora"
  ]
  node [
    id 57
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 58
    label "poj&#281;cie"
  ]
  node [
    id 59
    label "s&#322;up"
  ]
  node [
    id 60
    label "zasadzi&#263;"
  ]
  node [
    id 61
    label "calizna"
  ]
  node [
    id 62
    label "sprzeciw"
  ]
  node [
    id 63
    label "endeavor"
  ]
  node [
    id 64
    label "funkcjonowa&#263;"
  ]
  node [
    id 65
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 66
    label "mie&#263;_miejsce"
  ]
  node [
    id 67
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 68
    label "dzia&#322;a&#263;"
  ]
  node [
    id 69
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 70
    label "work"
  ]
  node [
    id 71
    label "bangla&#263;"
  ]
  node [
    id 72
    label "do"
  ]
  node [
    id 73
    label "maszyna"
  ]
  node [
    id 74
    label "tryb"
  ]
  node [
    id 75
    label "dziama&#263;"
  ]
  node [
    id 76
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 77
    label "praca"
  ]
  node [
    id 78
    label "podejmowa&#263;"
  ]
  node [
    id 79
    label "byd&#322;o"
  ]
  node [
    id 80
    label "zobo"
  ]
  node [
    id 81
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 82
    label "yakalo"
  ]
  node [
    id 83
    label "dzo"
  ]
  node [
    id 84
    label "doba"
  ]
  node [
    id 85
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 86
    label "dzi&#347;"
  ]
  node [
    id 87
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 88
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 89
    label "obediencja"
  ]
  node [
    id 90
    label "miejsce_zebra&#324;"
  ]
  node [
    id 91
    label "masoneria"
  ]
  node [
    id 92
    label "karbonaryzm"
  ]
  node [
    id 93
    label "jednostka_organizacyjna"
  ]
  node [
    id 94
    label "widownia"
  ]
  node [
    id 95
    label "pomieszczenie"
  ]
  node [
    id 96
    label "club"
  ]
  node [
    id 97
    label "bractwo"
  ]
  node [
    id 98
    label "medialny"
  ]
  node [
    id 99
    label "medialnie"
  ]
  node [
    id 100
    label "przyst&#281;pny"
  ]
  node [
    id 101
    label "&#322;atwy"
  ]
  node [
    id 102
    label "popularnie"
  ]
  node [
    id 103
    label "znany"
  ]
  node [
    id 104
    label "spis"
  ]
  node [
    id 105
    label "odinstalowanie"
  ]
  node [
    id 106
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 107
    label "podstawa"
  ]
  node [
    id 108
    label "emitowanie"
  ]
  node [
    id 109
    label "odinstalowywanie"
  ]
  node [
    id 110
    label "instrukcja"
  ]
  node [
    id 111
    label "punkt"
  ]
  node [
    id 112
    label "teleferie"
  ]
  node [
    id 113
    label "emitowa&#263;"
  ]
  node [
    id 114
    label "wytw&#243;r"
  ]
  node [
    id 115
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 116
    label "sekcja_krytyczna"
  ]
  node [
    id 117
    label "oferta"
  ]
  node [
    id 118
    label "prezentowa&#263;"
  ]
  node [
    id 119
    label "blok"
  ]
  node [
    id 120
    label "podprogram"
  ]
  node [
    id 121
    label "dzia&#322;"
  ]
  node [
    id 122
    label "broszura"
  ]
  node [
    id 123
    label "deklaracja"
  ]
  node [
    id 124
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 125
    label "struktura_organizacyjna"
  ]
  node [
    id 126
    label "zaprezentowanie"
  ]
  node [
    id 127
    label "informatyka"
  ]
  node [
    id 128
    label "booklet"
  ]
  node [
    id 129
    label "menu"
  ]
  node [
    id 130
    label "oprogramowanie"
  ]
  node [
    id 131
    label "instalowanie"
  ]
  node [
    id 132
    label "furkacja"
  ]
  node [
    id 133
    label "odinstalowa&#263;"
  ]
  node [
    id 134
    label "instalowa&#263;"
  ]
  node [
    id 135
    label "pirat"
  ]
  node [
    id 136
    label "zainstalowanie"
  ]
  node [
    id 137
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 138
    label "ogranicznik_referencyjny"
  ]
  node [
    id 139
    label "zainstalowa&#263;"
  ]
  node [
    id 140
    label "kana&#322;"
  ]
  node [
    id 141
    label "zaprezentowa&#263;"
  ]
  node [
    id 142
    label "interfejs"
  ]
  node [
    id 143
    label "odinstalowywa&#263;"
  ]
  node [
    id 144
    label "folder"
  ]
  node [
    id 145
    label "course_of_study"
  ]
  node [
    id 146
    label "ram&#243;wka"
  ]
  node [
    id 147
    label "prezentowanie"
  ]
  node [
    id 148
    label "okno"
  ]
  node [
    id 149
    label "sympatycznie"
  ]
  node [
    id 150
    label "weso&#322;y"
  ]
  node [
    id 151
    label "mi&#322;y"
  ]
  node [
    id 152
    label "przyjemny"
  ]
  node [
    id 153
    label "uspokojenie"
  ]
  node [
    id 154
    label "uspokojenie_si&#281;"
  ]
  node [
    id 155
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 156
    label "wolny"
  ]
  node [
    id 157
    label "spokojnie"
  ]
  node [
    id 158
    label "cicho"
  ]
  node [
    id 159
    label "nietrudny"
  ]
  node [
    id 160
    label "bezproblemowy"
  ]
  node [
    id 161
    label "uspokajanie_si&#281;"
  ]
  node [
    id 162
    label "uspokajanie"
  ]
  node [
    id 163
    label "kompetentnie"
  ]
  node [
    id 164
    label "kompetencyjny"
  ]
  node [
    id 165
    label "w&#322;ady"
  ]
  node [
    id 166
    label "fachowy"
  ]
  node [
    id 167
    label "odpowiedni"
  ]
  node [
    id 168
    label "opu&#347;ci&#263;"
  ]
  node [
    id 169
    label "zrezygnowa&#263;"
  ]
  node [
    id 170
    label "proceed"
  ]
  node [
    id 171
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 172
    label "leave_office"
  ]
  node [
    id 173
    label "retract"
  ]
  node [
    id 174
    label "min&#261;&#263;"
  ]
  node [
    id 175
    label "przesta&#263;"
  ]
  node [
    id 176
    label "odrzut"
  ]
  node [
    id 177
    label "ruszy&#263;"
  ]
  node [
    id 178
    label "drop"
  ]
  node [
    id 179
    label "zrobi&#263;"
  ]
  node [
    id 180
    label "die"
  ]
  node [
    id 181
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 182
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 183
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 184
    label "whole"
  ]
  node [
    id 185
    label "odm&#322;adza&#263;"
  ]
  node [
    id 186
    label "zabudowania"
  ]
  node [
    id 187
    label "odm&#322;odzenie"
  ]
  node [
    id 188
    label "zespolik"
  ]
  node [
    id 189
    label "skupienie"
  ]
  node [
    id 190
    label "schorzenie"
  ]
  node [
    id 191
    label "grupa"
  ]
  node [
    id 192
    label "Depeche_Mode"
  ]
  node [
    id 193
    label "Mazowsze"
  ]
  node [
    id 194
    label "ro&#347;lina"
  ]
  node [
    id 195
    label "zbi&#243;r"
  ]
  node [
    id 196
    label "The_Beatles"
  ]
  node [
    id 197
    label "group"
  ]
  node [
    id 198
    label "&#346;wietliki"
  ]
  node [
    id 199
    label "odm&#322;adzanie"
  ]
  node [
    id 200
    label "batch"
  ]
  node [
    id 201
    label "simultaneously"
  ]
  node [
    id 202
    label "coincidentally"
  ]
  node [
    id 203
    label "synchronously"
  ]
  node [
    id 204
    label "concurrently"
  ]
  node [
    id 205
    label "jednoczesny"
  ]
  node [
    id 206
    label "sta&#263;_si&#281;"
  ]
  node [
    id 207
    label "czas"
  ]
  node [
    id 208
    label "catch"
  ]
  node [
    id 209
    label "become"
  ]
  node [
    id 210
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 211
    label "line_up"
  ]
  node [
    id 212
    label "przyby&#263;"
  ]
  node [
    id 213
    label "doj&#347;cie"
  ]
  node [
    id 214
    label "doj&#347;&#263;"
  ]
  node [
    id 215
    label "powzi&#261;&#263;"
  ]
  node [
    id 216
    label "wiedza"
  ]
  node [
    id 217
    label "sygna&#322;"
  ]
  node [
    id 218
    label "obiegni&#281;cie"
  ]
  node [
    id 219
    label "obieganie"
  ]
  node [
    id 220
    label "obiec"
  ]
  node [
    id 221
    label "dane"
  ]
  node [
    id 222
    label "obiega&#263;"
  ]
  node [
    id 223
    label "publikacja"
  ]
  node [
    id 224
    label "powzi&#281;cie"
  ]
  node [
    id 225
    label "niski"
  ]
  node [
    id 226
    label "masowo"
  ]
  node [
    id 227
    label "seryjny"
  ]
  node [
    id 228
    label "orzeczenie"
  ]
  node [
    id 229
    label "spowodowanie"
  ]
  node [
    id 230
    label "czynno&#347;&#263;"
  ]
  node [
    id 231
    label "oddalenie"
  ]
  node [
    id 232
    label "zmniejszenie"
  ]
  node [
    id 233
    label "performance"
  ]
  node [
    id 234
    label "oddzia&#322;anie"
  ]
  node [
    id 235
    label "wolniejszy"
  ]
  node [
    id 236
    label "nieobecno&#347;&#263;"
  ]
  node [
    id 237
    label "uwolnienie"
  ]
  node [
    id 238
    label "wypowiedzenie"
  ]
  node [
    id 239
    label "liberation"
  ]
  node [
    id 240
    label "spowolnienie"
  ]
  node [
    id 241
    label "relief"
  ]
  node [
    id 242
    label "dowolny"
  ]
  node [
    id 243
    label "ulga"
  ]
  node [
    id 244
    label "release"
  ]
  node [
    id 245
    label "za&#347;wiadczenie"
  ]
  node [
    id 246
    label "wylanie"
  ]
  node [
    id 247
    label "nowotny"
  ]
  node [
    id 248
    label "drugi"
  ]
  node [
    id 249
    label "kolejny"
  ]
  node [
    id 250
    label "bie&#380;&#261;cy"
  ]
  node [
    id 251
    label "nowo"
  ]
  node [
    id 252
    label "narybek"
  ]
  node [
    id 253
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 254
    label "obcy"
  ]
  node [
    id 255
    label "skrajny"
  ]
  node [
    id 256
    label "arcykatolicki"
  ]
  node [
    id 257
    label "ultrakatolicko"
  ]
  node [
    id 258
    label "katolicki"
  ]
  node [
    id 259
    label "nast&#281;pnie"
  ]
  node [
    id 260
    label "poni&#380;szy"
  ]
  node [
    id 261
    label "Bogumi&#322;a"
  ]
  node [
    id 262
    label "Ma&#322;gorzata"
  ]
  node [
    id 263
    label "Lisickiego"
  ]
  node [
    id 264
    label "&#8222;"
  ]
  node [
    id 265
    label "solidarno&#347;&#263;"
  ]
  node [
    id 266
    label "Piotr"
  ]
  node [
    id 267
    label "Ko&#347;ci&#324;ski"
  ]
  node [
    id 268
    label "S&#322;awomira"
  ]
  node [
    id 269
    label "popowski"
  ]
  node [
    id 270
    label "Jan"
  ]
  node [
    id 271
    label "ordy&#324;ski"
  ]
  node [
    id 272
    label "Szymona"
  ]
  node [
    id 273
    label "Ho&#322;ownia"
  ]
  node [
    id 274
    label "Andrzej"
  ]
  node [
    id 275
    label "Filipowicz"
  ]
  node [
    id 276
    label "Lisicki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 25
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 51
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 262
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 205
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 23
    target 211
  ]
  edge [
    source 23
    target 212
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 111
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 228
  ]
  edge [
    source 26
    target 229
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 239
  ]
  edge [
    source 26
    target 240
  ]
  edge [
    source 26
    target 241
  ]
  edge [
    source 26
    target 242
  ]
  edge [
    source 26
    target 243
  ]
  edge [
    source 26
    target 244
  ]
  edge [
    source 26
    target 245
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 47
  ]
  edge [
    source 27
    target 247
  ]
  edge [
    source 27
    target 248
  ]
  edge [
    source 27
    target 249
  ]
  edge [
    source 27
    target 250
  ]
  edge [
    source 27
    target 251
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 253
  ]
  edge [
    source 27
    target 254
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 255
  ]
  edge [
    source 29
    target 256
  ]
  edge [
    source 29
    target 257
  ]
  edge [
    source 29
    target 258
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 276
  ]
  edge [
    source 31
    target 259
  ]
  edge [
    source 31
    target 260
  ]
  edge [
    source 264
    target 265
  ]
  edge [
    source 266
    target 267
  ]
  edge [
    source 268
    target 269
  ]
  edge [
    source 270
    target 271
  ]
  edge [
    source 272
    target 273
  ]
  edge [
    source 274
    target 275
  ]
]
