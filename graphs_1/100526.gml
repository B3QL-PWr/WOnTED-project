graph [
  maxDegree 35
  minDegree 1
  meanDegree 2.15625
  density 0.03422619047619048
  graphCliqueNumber 6
  node [
    id 0
    label "morski"
    origin "text"
  ]
  node [
    id 1
    label "korpus"
    origin "text"
  ]
  node [
    id 2
    label "kadecki"
    origin "text"
  ]
  node [
    id 3
    label "specjalny"
  ]
  node [
    id 4
    label "niebieski"
  ]
  node [
    id 5
    label "nadmorski"
  ]
  node [
    id 6
    label "morsko"
  ]
  node [
    id 7
    label "wodny"
  ]
  node [
    id 8
    label "s&#322;ony"
  ]
  node [
    id 9
    label "zielony"
  ]
  node [
    id 10
    label "przypominaj&#261;cy"
  ]
  node [
    id 11
    label "typowy"
  ]
  node [
    id 12
    label "brzuch"
  ]
  node [
    id 13
    label "za&#322;o&#380;enie"
  ]
  node [
    id 14
    label "zwi&#261;zek_taktyczny"
  ]
  node [
    id 15
    label "grupa"
  ]
  node [
    id 16
    label "bok"
  ]
  node [
    id 17
    label "pier&#347;"
  ]
  node [
    id 18
    label "tuszka"
  ]
  node [
    id 19
    label "pupa"
  ]
  node [
    id 20
    label "obudowa"
  ]
  node [
    id 21
    label "pacha"
  ]
  node [
    id 22
    label "krocze"
  ]
  node [
    id 23
    label "biodro"
  ]
  node [
    id 24
    label "corpus"
  ]
  node [
    id 25
    label "zasadzenie"
  ]
  node [
    id 26
    label "documentation"
  ]
  node [
    id 27
    label "budowla"
  ]
  node [
    id 28
    label "zasadzi&#263;"
  ]
  node [
    id 29
    label "dr&#243;b"
  ]
  node [
    id 30
    label "dekolt"
  ]
  node [
    id 31
    label "punkt_odniesienia"
  ]
  node [
    id 32
    label "plecy"
  ]
  node [
    id 33
    label "wojsko"
  ]
  node [
    id 34
    label "pachwina"
  ]
  node [
    id 35
    label "dywizja"
  ]
  node [
    id 36
    label "podstawowy"
  ]
  node [
    id 37
    label "struktura_anatomiczna"
  ]
  node [
    id 38
    label "zbi&#243;r"
  ]
  node [
    id 39
    label "zad"
  ]
  node [
    id 40
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 41
    label "sto&#380;ek_wzrostu"
  ]
  node [
    id 42
    label "konkordancja"
  ]
  node [
    id 43
    label "mi&#281;so"
  ]
  node [
    id 44
    label "klatka_piersiowa"
  ]
  node [
    id 45
    label "Sankt"
  ]
  node [
    id 46
    label "Petersburg"
  ]
  node [
    id 47
    label "&#1052;&#1086;&#1088;&#1089;&#1082;&#1086;&#1081;"
  ]
  node [
    id 48
    label "&#1082;&#1072;&#1076;&#1077;&#1090;&#1089;&#1082;&#1080;&#1081;"
  ]
  node [
    id 49
    label "&#1082;&#1086;&#1088;&#1087;&#1091;&#1089;"
  ]
  node [
    id 50
    label "&#1080;&#1084;"
  ]
  node [
    id 51
    label "&#1055;&#1077;&#1090;&#1088;&#1072;"
  ]
  node [
    id 52
    label "&#1042;&#1077;&#1083;&#1080;&#1082;&#1086;&#1075;&#1086;"
  ]
  node [
    id 53
    label "akademia"
  ]
  node [
    id 54
    label "gwardia"
  ]
  node [
    id 55
    label "Piotr"
  ]
  node [
    id 56
    label "i"
  ]
  node [
    id 57
    label "Mamert"
  ]
  node [
    id 58
    label "Stankiewicz"
  ]
  node [
    id 59
    label "Niko&#322;aj"
  ]
  node [
    id 60
    label "Essen"
  ]
  node [
    id 61
    label "Puti&#322;ow"
  ]
  node [
    id 62
    label "Roberta"
  ]
  node [
    id 63
    label "Wiren"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 47
    target 52
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 49
    target 52
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 62
    target 63
  ]
]
