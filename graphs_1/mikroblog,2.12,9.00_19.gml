graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "legia"
    origin "text"
  ]
  node [
    id 1
    label "&#380;al"
    origin "text"
  ]
  node [
    id 2
    label "kupowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "roleta"
    origin "text"
  ]
  node [
    id 4
    label "formacja"
  ]
  node [
    id 5
    label "armia"
  ]
  node [
    id 6
    label "host"
  ]
  node [
    id 7
    label "oddzia&#322;"
  ]
  node [
    id 8
    label "niezadowolenie"
  ]
  node [
    id 9
    label "wstyd"
  ]
  node [
    id 10
    label "gniewa&#263;_si&#281;"
  ]
  node [
    id 11
    label "gniewanie_si&#281;"
  ]
  node [
    id 12
    label "smutek"
  ]
  node [
    id 13
    label "czu&#263;"
  ]
  node [
    id 14
    label "emocja"
  ]
  node [
    id 15
    label "commiseration"
  ]
  node [
    id 16
    label "pang"
  ]
  node [
    id 17
    label "krytyka"
  ]
  node [
    id 18
    label "pogniewa&#263;_si&#281;"
  ]
  node [
    id 19
    label "uraza"
  ]
  node [
    id 20
    label "criticism"
  ]
  node [
    id 21
    label "sorrow"
  ]
  node [
    id 22
    label "pogniewanie_si&#281;"
  ]
  node [
    id 23
    label "sytuacja"
  ]
  node [
    id 24
    label "get"
  ]
  node [
    id 25
    label "ustawia&#263;"
  ]
  node [
    id 26
    label "wierzy&#263;"
  ]
  node [
    id 27
    label "przyjmowa&#263;"
  ]
  node [
    id 28
    label "pozyskiwa&#263;"
  ]
  node [
    id 29
    label "gra&#263;"
  ]
  node [
    id 30
    label "kupywa&#263;"
  ]
  node [
    id 31
    label "uznawa&#263;"
  ]
  node [
    id 32
    label "bra&#263;"
  ]
  node [
    id 33
    label "zas&#322;ona"
  ]
  node [
    id 34
    label "dekoracja_okna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
]
