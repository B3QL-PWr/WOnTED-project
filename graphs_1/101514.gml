graph [
  maxDegree 153
  minDegree 1
  meanDegree 2.1872749099639854
  density 0.0026289361898605597
  graphCliqueNumber 4
  node [
    id 0
    label "teraz"
    origin "text"
  ]
  node [
    id 1
    label "codziennie"
    origin "text"
  ]
  node [
    id 2
    label "&#347;ciska&#263;"
    origin "text"
  ]
  node [
    id 3
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 4
    label "srebrny"
    origin "text"
  ]
  node [
    id 5
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 6
    label "trzyma&#263;"
    origin "text"
  ]
  node [
    id 7
    label "d&#322;o&#324;"
    origin "text"
  ]
  node [
    id 8
    label "przez"
    origin "text"
  ]
  node [
    id 9
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 10
    label "noc"
    origin "text"
  ]
  node [
    id 11
    label "&#347;ni&#263;"
    origin "text"
  ]
  node [
    id 12
    label "jawa"
    origin "text"
  ]
  node [
    id 13
    label "sen"
    origin "text"
  ]
  node [
    id 14
    label "rano"
    origin "text"
  ]
  node [
    id 15
    label "list"
    origin "text"
  ]
  node [
    id 16
    label "nadej&#347;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "ocknieniu"
    origin "text"
  ]
  node [
    id 18
    label "wszystek"
    origin "text"
  ]
  node [
    id 19
    label "zmienia&#263;"
    origin "text"
  ]
  node [
    id 20
    label "si&#281;"
    origin "text"
  ]
  node [
    id 21
    label "s&#322;uch"
    origin "text"
  ]
  node [
    id 22
    label "czekan"
    origin "text"
  ]
  node [
    id 23
    label "znany"
    origin "text"
  ]
  node [
    id 24
    label "odg&#322;os"
    origin "text"
  ]
  node [
    id 25
    label "krok"
    origin "text"
  ]
  node [
    id 26
    label "nieraz"
    origin "text"
  ]
  node [
    id 27
    label "zawodny"
    origin "text"
  ]
  node [
    id 28
    label "m&#347;ciwy"
    origin "text"
  ]
  node [
    id 29
    label "zmami&#263;"
    origin "text"
  ]
  node [
    id 30
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 31
    label "s&#322;ysze&#263;"
    origin "text"
  ]
  node [
    id 32
    label "daleki"
    origin "text"
  ]
  node [
    id 33
    label "&#322;oskot"
    origin "text"
  ]
  node [
    id 34
    label "bi&#263;"
    origin "text"
  ]
  node [
    id 35
    label "serce"
    origin "text"
  ]
  node [
    id 36
    label "podwaja&#263;"
    origin "text"
  ]
  node [
    id 37
    label "szybko&#347;&#263;"
    origin "text"
  ]
  node [
    id 38
    label "uderzenie"
    origin "text"
  ]
  node [
    id 39
    label "zamkni&#281;ty"
    origin "text"
  ]
  node [
    id 40
    label "oko"
    origin "text"
  ]
  node [
    id 41
    label "nieruchomy"
    origin "text"
  ]
  node [
    id 42
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 43
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 44
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 45
    label "zlitowa&#263;"
    origin "text"
  ]
  node [
    id 46
    label "jak"
    origin "text"
  ]
  node [
    id 47
    label "kamie&#324;"
    origin "text"
  ]
  node [
    id 48
    label "ton&#261;&#263;"
    origin "text"
  ]
  node [
    id 49
    label "niesko&#324;czony"
    origin "text"
  ]
  node [
    id 50
    label "cisz"
    origin "text"
  ]
  node [
    id 51
    label "straszliwy"
    origin "text"
  ]
  node [
    id 52
    label "cisza"
    origin "text"
  ]
  node [
    id 53
    label "otacza&#263;"
    origin "text"
  ]
  node [
    id 54
    label "znowu"
    origin "text"
  ]
  node [
    id 55
    label "przywala&#263;"
    origin "text"
  ]
  node [
    id 56
    label "pola"
    origin "text"
  ]
  node [
    id 57
    label "wielki"
    origin "text"
  ]
  node [
    id 58
    label "gliniasty"
    origin "text"
  ]
  node [
    id 59
    label "ziemia"
    origin "text"
  ]
  node [
    id 60
    label "schodzi&#263;"
    origin "text"
  ]
  node [
    id 61
    label "fizyczny"
    origin "text"
  ]
  node [
    id 62
    label "bezczucie"
    origin "text"
  ]
  node [
    id 63
    label "albo"
    origin "text"
  ]
  node [
    id 64
    label "cudaczny"
    origin "text"
  ]
  node [
    id 65
    label "zachcie&#263;"
    origin "text"
  ]
  node [
    id 66
    label "chwila"
  ]
  node [
    id 67
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 68
    label "daily"
  ]
  node [
    id 69
    label "cz&#281;sto"
  ]
  node [
    id 70
    label "codzienny"
  ]
  node [
    id 71
    label "prozaicznie"
  ]
  node [
    id 72
    label "stale"
  ]
  node [
    id 73
    label "regularnie"
  ]
  node [
    id 74
    label "pospolicie"
  ]
  node [
    id 75
    label "dotyka&#263;"
  ]
  node [
    id 76
    label "obejmowa&#263;"
  ]
  node [
    id 77
    label "get"
  ]
  node [
    id 78
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 79
    label "clamp"
  ]
  node [
    id 80
    label "constipate"
  ]
  node [
    id 81
    label "krzy&#380;"
  ]
  node [
    id 82
    label "paw"
  ]
  node [
    id 83
    label "rami&#281;"
  ]
  node [
    id 84
    label "gestykulowanie"
  ]
  node [
    id 85
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 86
    label "pracownik"
  ]
  node [
    id 87
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 88
    label "bramkarz"
  ]
  node [
    id 89
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 90
    label "handwriting"
  ]
  node [
    id 91
    label "hasta"
  ]
  node [
    id 92
    label "pi&#322;ka"
  ]
  node [
    id 93
    label "&#322;okie&#263;"
  ]
  node [
    id 94
    label "spos&#243;b"
  ]
  node [
    id 95
    label "zagrywka"
  ]
  node [
    id 96
    label "obietnica"
  ]
  node [
    id 97
    label "przedrami&#281;"
  ]
  node [
    id 98
    label "chwyta&#263;"
  ]
  node [
    id 99
    label "r&#261;czyna"
  ]
  node [
    id 100
    label "cecha"
  ]
  node [
    id 101
    label "wykroczenie"
  ]
  node [
    id 102
    label "kroki"
  ]
  node [
    id 103
    label "palec"
  ]
  node [
    id 104
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 105
    label "graba"
  ]
  node [
    id 106
    label "hand"
  ]
  node [
    id 107
    label "nadgarstek"
  ]
  node [
    id 108
    label "pomocnik"
  ]
  node [
    id 109
    label "k&#322;&#261;b"
  ]
  node [
    id 110
    label "hazena"
  ]
  node [
    id 111
    label "gestykulowa&#263;"
  ]
  node [
    id 112
    label "cmoknonsens"
  ]
  node [
    id 113
    label "chwytanie"
  ]
  node [
    id 114
    label "czerwona_kartka"
  ]
  node [
    id 115
    label "metaliczny"
  ]
  node [
    id 116
    label "szary"
  ]
  node [
    id 117
    label "jasny"
  ]
  node [
    id 118
    label "utytu&#322;owany"
  ]
  node [
    id 119
    label "srebrzenie_si&#281;"
  ]
  node [
    id 120
    label "srebrno"
  ]
  node [
    id 121
    label "posrebrzenie"
  ]
  node [
    id 122
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 123
    label "srebrzenie"
  ]
  node [
    id 124
    label "srebrzy&#347;cie"
  ]
  node [
    id 125
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 126
    label "zdewaluowa&#263;"
  ]
  node [
    id 127
    label "moniak"
  ]
  node [
    id 128
    label "wytw&#243;r"
  ]
  node [
    id 129
    label "zdewaluowanie"
  ]
  node [
    id 130
    label "jednostka_monetarna"
  ]
  node [
    id 131
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 132
    label "numizmat"
  ]
  node [
    id 133
    label "rozmieni&#263;"
  ]
  node [
    id 134
    label "rozmienienie"
  ]
  node [
    id 135
    label "rozmienianie"
  ]
  node [
    id 136
    label "dewaluowanie"
  ]
  node [
    id 137
    label "nomina&#322;"
  ]
  node [
    id 138
    label "coin"
  ]
  node [
    id 139
    label "dewaluowa&#263;"
  ]
  node [
    id 140
    label "pieni&#261;dze"
  ]
  node [
    id 141
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 142
    label "rozmienia&#263;"
  ]
  node [
    id 143
    label "continue"
  ]
  node [
    id 144
    label "zmusza&#263;"
  ]
  node [
    id 145
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 146
    label "sympatyzowa&#263;"
  ]
  node [
    id 147
    label "pozostawa&#263;"
  ]
  node [
    id 148
    label "zachowywa&#263;"
  ]
  node [
    id 149
    label "utrzymywa&#263;"
  ]
  node [
    id 150
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 151
    label "treat"
  ]
  node [
    id 152
    label "przetrzymywa&#263;"
  ]
  node [
    id 153
    label "sprawowa&#263;"
  ]
  node [
    id 154
    label "administrowa&#263;"
  ]
  node [
    id 155
    label "robi&#263;"
  ]
  node [
    id 156
    label "adhere"
  ]
  node [
    id 157
    label "dzier&#380;y&#263;"
  ]
  node [
    id 158
    label "podtrzymywa&#263;"
  ]
  node [
    id 159
    label "argue"
  ]
  node [
    id 160
    label "hodowa&#263;"
  ]
  node [
    id 161
    label "wychowywa&#263;"
  ]
  node [
    id 162
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 163
    label "szeroko&#347;&#263;_d&#322;oni"
  ]
  node [
    id 164
    label "linia_rozumu"
  ]
  node [
    id 165
    label "wyklepanie"
  ]
  node [
    id 166
    label "dotykanie"
  ]
  node [
    id 167
    label "linia_mi&#322;o&#347;ci"
  ]
  node [
    id 168
    label "linia_&#380;ycia"
  ]
  node [
    id 169
    label "klepanie"
  ]
  node [
    id 170
    label "chiromancja"
  ]
  node [
    id 171
    label "wyklepa&#263;"
  ]
  node [
    id 172
    label "klepa&#263;"
  ]
  node [
    id 173
    label "poduszka"
  ]
  node [
    id 174
    label "du&#380;y"
  ]
  node [
    id 175
    label "jedyny"
  ]
  node [
    id 176
    label "kompletny"
  ]
  node [
    id 177
    label "zdr&#243;w"
  ]
  node [
    id 178
    label "&#380;ywy"
  ]
  node [
    id 179
    label "ca&#322;o"
  ]
  node [
    id 180
    label "pe&#322;ny"
  ]
  node [
    id 181
    label "calu&#347;ko"
  ]
  node [
    id 182
    label "podobny"
  ]
  node [
    id 183
    label "doba"
  ]
  node [
    id 184
    label "czas"
  ]
  node [
    id 185
    label "p&#243;&#322;noc"
  ]
  node [
    id 186
    label "nokturn"
  ]
  node [
    id 187
    label "zjawisko"
  ]
  node [
    id 188
    label "night"
  ]
  node [
    id 189
    label "doznawa&#263;"
  ]
  node [
    id 190
    label "my&#347;le&#263;"
  ]
  node [
    id 191
    label "pour"
  ]
  node [
    id 192
    label "dream"
  ]
  node [
    id 193
    label "reality"
  ]
  node [
    id 194
    label "realno&#347;&#263;"
  ]
  node [
    id 195
    label "kima"
  ]
  node [
    id 196
    label "proces_fizjologiczny"
  ]
  node [
    id 197
    label "relaxation"
  ]
  node [
    id 198
    label "marzenie_senne"
  ]
  node [
    id 199
    label "sen_wolnofalowy"
  ]
  node [
    id 200
    label "odpoczynek"
  ]
  node [
    id 201
    label "hipersomnia"
  ]
  node [
    id 202
    label "jen"
  ]
  node [
    id 203
    label "fun"
  ]
  node [
    id 204
    label "wymys&#322;"
  ]
  node [
    id 205
    label "sen_paradoksalny"
  ]
  node [
    id 206
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 207
    label "wsch&#243;d"
  ]
  node [
    id 208
    label "aurora"
  ]
  node [
    id 209
    label "pora"
  ]
  node [
    id 210
    label "dzie&#324;"
  ]
  node [
    id 211
    label "li&#347;&#263;"
  ]
  node [
    id 212
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 213
    label "poczta"
  ]
  node [
    id 214
    label "epistolografia"
  ]
  node [
    id 215
    label "przesy&#322;ka"
  ]
  node [
    id 216
    label "poczta_elektroniczna"
  ]
  node [
    id 217
    label "znaczek_pocztowy"
  ]
  node [
    id 218
    label "sta&#263;_si&#281;"
  ]
  node [
    id 219
    label "catch"
  ]
  node [
    id 220
    label "become"
  ]
  node [
    id 221
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 222
    label "line_up"
  ]
  node [
    id 223
    label "przyby&#263;"
  ]
  node [
    id 224
    label "reengineering"
  ]
  node [
    id 225
    label "alternate"
  ]
  node [
    id 226
    label "przechodzi&#263;"
  ]
  node [
    id 227
    label "zast&#281;powa&#263;"
  ]
  node [
    id 228
    label "sprawia&#263;"
  ]
  node [
    id 229
    label "traci&#263;"
  ]
  node [
    id 230
    label "zyskiwa&#263;"
  ]
  node [
    id 231
    label "change"
  ]
  node [
    id 232
    label "hearing"
  ]
  node [
    id 233
    label "zmys&#322;"
  ]
  node [
    id 234
    label "narz&#261;d_otolitowy"
  ]
  node [
    id 235
    label "talent"
  ]
  node [
    id 236
    label "solfe&#380;"
  ]
  node [
    id 237
    label "sprz&#281;t_wspinaczkowy"
  ]
  node [
    id 238
    label "bro&#324;"
  ]
  node [
    id 239
    label "narz&#281;dzie"
  ]
  node [
    id 240
    label "laska"
  ]
  node [
    id 241
    label "bro&#324;_obuchowa"
  ]
  node [
    id 242
    label "rozpowszechnianie"
  ]
  node [
    id 243
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 244
    label "resonance"
  ]
  node [
    id 245
    label "brzmienie"
  ]
  node [
    id 246
    label "wpada&#263;"
  ]
  node [
    id 247
    label "wpadanie"
  ]
  node [
    id 248
    label "onomatopeja"
  ]
  node [
    id 249
    label "wydanie"
  ]
  node [
    id 250
    label "wyda&#263;"
  ]
  node [
    id 251
    label "wpa&#347;&#263;"
  ]
  node [
    id 252
    label "sound"
  ]
  node [
    id 253
    label "note"
  ]
  node [
    id 254
    label "wydawa&#263;"
  ]
  node [
    id 255
    label "d&#378;wi&#281;k"
  ]
  node [
    id 256
    label "wpadni&#281;cie"
  ]
  node [
    id 257
    label "pace"
  ]
  node [
    id 258
    label "czyn"
  ]
  node [
    id 259
    label "passus"
  ]
  node [
    id 260
    label "measurement"
  ]
  node [
    id 261
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 262
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 263
    label "ruch"
  ]
  node [
    id 264
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 265
    label "action"
  ]
  node [
    id 266
    label "chodzi&#263;"
  ]
  node [
    id 267
    label "tu&#322;&#243;w"
  ]
  node [
    id 268
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 269
    label "skejt"
  ]
  node [
    id 270
    label "step"
  ]
  node [
    id 271
    label "czasami"
  ]
  node [
    id 272
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 273
    label "tylekro&#263;"
  ]
  node [
    id 274
    label "cz&#281;sty"
  ]
  node [
    id 275
    label "wielekro&#263;"
  ]
  node [
    id 276
    label "wielokrotny"
  ]
  node [
    id 277
    label "niepewny"
  ]
  node [
    id 278
    label "zawodnie"
  ]
  node [
    id 279
    label "z&#322;y"
  ]
  node [
    id 280
    label "m&#347;ciwie"
  ]
  node [
    id 281
    label "okrutny"
  ]
  node [
    id 282
    label "pami&#281;tliwy"
  ]
  node [
    id 283
    label "render"
  ]
  node [
    id 284
    label "hold"
  ]
  node [
    id 285
    label "surrender"
  ]
  node [
    id 286
    label "traktowa&#263;"
  ]
  node [
    id 287
    label "dostarcza&#263;"
  ]
  node [
    id 288
    label "tender"
  ]
  node [
    id 289
    label "train"
  ]
  node [
    id 290
    label "give"
  ]
  node [
    id 291
    label "umieszcza&#263;"
  ]
  node [
    id 292
    label "nalewa&#263;"
  ]
  node [
    id 293
    label "przeznacza&#263;"
  ]
  node [
    id 294
    label "p&#322;aci&#263;"
  ]
  node [
    id 295
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 296
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 297
    label "powierza&#263;"
  ]
  node [
    id 298
    label "hold_out"
  ]
  node [
    id 299
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 300
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 301
    label "mie&#263;_miejsce"
  ]
  node [
    id 302
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 303
    label "t&#322;uc"
  ]
  node [
    id 304
    label "wpiernicza&#263;"
  ]
  node [
    id 305
    label "przekazywa&#263;"
  ]
  node [
    id 306
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 307
    label "zezwala&#263;"
  ]
  node [
    id 308
    label "rap"
  ]
  node [
    id 309
    label "obiecywa&#263;"
  ]
  node [
    id 310
    label "&#322;adowa&#263;"
  ]
  node [
    id 311
    label "odst&#281;powa&#263;"
  ]
  node [
    id 312
    label "exsert"
  ]
  node [
    id 313
    label "listen"
  ]
  node [
    id 314
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 315
    label "postrzega&#263;"
  ]
  node [
    id 316
    label "s&#322;ycha&#263;"
  ]
  node [
    id 317
    label "read"
  ]
  node [
    id 318
    label "dawny"
  ]
  node [
    id 319
    label "s&#322;aby"
  ]
  node [
    id 320
    label "oddalony"
  ]
  node [
    id 321
    label "daleko"
  ]
  node [
    id 322
    label "przysz&#322;y"
  ]
  node [
    id 323
    label "ogl&#281;dny"
  ]
  node [
    id 324
    label "r&#243;&#380;ny"
  ]
  node [
    id 325
    label "g&#322;&#281;boki"
  ]
  node [
    id 326
    label "odlegle"
  ]
  node [
    id 327
    label "nieobecny"
  ]
  node [
    id 328
    label "odleg&#322;y"
  ]
  node [
    id 329
    label "d&#322;ugi"
  ]
  node [
    id 330
    label "zwi&#261;zany"
  ]
  node [
    id 331
    label "obcy"
  ]
  node [
    id 332
    label "ha&#322;as"
  ]
  node [
    id 333
    label "stan"
  ]
  node [
    id 334
    label "funkcjonowa&#263;"
  ]
  node [
    id 335
    label "rejestrowa&#263;"
  ]
  node [
    id 336
    label "proceed"
  ]
  node [
    id 337
    label "strike"
  ]
  node [
    id 338
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 339
    label "wygrywa&#263;"
  ]
  node [
    id 340
    label "zag&#322;&#281;bia&#263;"
  ]
  node [
    id 341
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 342
    label "przygotowywa&#263;"
  ]
  node [
    id 343
    label "zabija&#263;"
  ]
  node [
    id 344
    label "dzwoni&#263;"
  ]
  node [
    id 345
    label "take"
  ]
  node [
    id 346
    label "emanowa&#263;"
  ]
  node [
    id 347
    label "niszczy&#263;"
  ]
  node [
    id 348
    label "str&#261;ca&#263;"
  ]
  node [
    id 349
    label "beat"
  ]
  node [
    id 350
    label "butcher"
  ]
  node [
    id 351
    label "przerabia&#263;"
  ]
  node [
    id 352
    label "balansjerka"
  ]
  node [
    id 353
    label "murder"
  ]
  node [
    id 354
    label "krzywdzi&#263;"
  ]
  node [
    id 355
    label "napierdziela&#263;"
  ]
  node [
    id 356
    label "pra&#263;"
  ]
  node [
    id 357
    label "usuwa&#263;"
  ]
  node [
    id 358
    label "chop"
  ]
  node [
    id 359
    label "skuwa&#263;"
  ]
  node [
    id 360
    label "macha&#263;"
  ]
  node [
    id 361
    label "t&#322;oczy&#263;"
  ]
  node [
    id 362
    label "peddle"
  ]
  node [
    id 363
    label "tug"
  ]
  node [
    id 364
    label "powodowa&#263;"
  ]
  node [
    id 365
    label "uderza&#263;"
  ]
  node [
    id 366
    label "&#380;&#322;obi&#263;"
  ]
  node [
    id 367
    label "zwalcza&#263;"
  ]
  node [
    id 368
    label "cz&#322;owiek"
  ]
  node [
    id 369
    label "courage"
  ]
  node [
    id 370
    label "mi&#281;sie&#324;"
  ]
  node [
    id 371
    label "kompleks"
  ]
  node [
    id 372
    label "sfera_afektywna"
  ]
  node [
    id 373
    label "heart"
  ]
  node [
    id 374
    label "sumienie"
  ]
  node [
    id 375
    label "przedsionek"
  ]
  node [
    id 376
    label "entity"
  ]
  node [
    id 377
    label "nastawienie"
  ]
  node [
    id 378
    label "punkt"
  ]
  node [
    id 379
    label "kompleksja"
  ]
  node [
    id 380
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 381
    label "kier"
  ]
  node [
    id 382
    label "systol"
  ]
  node [
    id 383
    label "pikawa"
  ]
  node [
    id 384
    label "power"
  ]
  node [
    id 385
    label "zastawka"
  ]
  node [
    id 386
    label "psychika"
  ]
  node [
    id 387
    label "wola"
  ]
  node [
    id 388
    label "komora"
  ]
  node [
    id 389
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 390
    label "zapalno&#347;&#263;"
  ]
  node [
    id 391
    label "wsierdzie"
  ]
  node [
    id 392
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 393
    label "podekscytowanie"
  ]
  node [
    id 394
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 395
    label "strunowiec"
  ]
  node [
    id 396
    label "favor"
  ]
  node [
    id 397
    label "dusza"
  ]
  node [
    id 398
    label "fizjonomia"
  ]
  node [
    id 399
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 400
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 401
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 402
    label "charakter"
  ]
  node [
    id 403
    label "mikrokosmos"
  ]
  node [
    id 404
    label "dzwon"
  ]
  node [
    id 405
    label "koniuszek_serca"
  ]
  node [
    id 406
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 407
    label "passion"
  ]
  node [
    id 408
    label "pulsowa&#263;"
  ]
  node [
    id 409
    label "ego"
  ]
  node [
    id 410
    label "organ"
  ]
  node [
    id 411
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 412
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 413
    label "kardiografia"
  ]
  node [
    id 414
    label "osobowo&#347;&#263;"
  ]
  node [
    id 415
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 416
    label "kszta&#322;t"
  ]
  node [
    id 417
    label "karta"
  ]
  node [
    id 418
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 419
    label "dobro&#263;"
  ]
  node [
    id 420
    label "podroby"
  ]
  node [
    id 421
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 422
    label "pulsowanie"
  ]
  node [
    id 423
    label "deformowa&#263;"
  ]
  node [
    id 424
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 425
    label "seksualno&#347;&#263;"
  ]
  node [
    id 426
    label "deformowanie"
  ]
  node [
    id 427
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 428
    label "elektrokardiografia"
  ]
  node [
    id 429
    label "double"
  ]
  node [
    id 430
    label "zwielokrotnia&#263;"
  ]
  node [
    id 431
    label "wzmaga&#263;"
  ]
  node [
    id 432
    label "sprawno&#347;&#263;"
  ]
  node [
    id 433
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 434
    label "celerity"
  ]
  node [
    id 435
    label "kr&#243;tko&#347;&#263;"
  ]
  node [
    id 436
    label "tempo"
  ]
  node [
    id 437
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 438
    label "pr&#281;dko&#347;&#263;"
  ]
  node [
    id 439
    label "dawka"
  ]
  node [
    id 440
    label "manewr"
  ]
  node [
    id 441
    label "reakcja"
  ]
  node [
    id 442
    label "odbicie"
  ]
  node [
    id 443
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 444
    label "dotyk"
  ]
  node [
    id 445
    label "contact"
  ]
  node [
    id 446
    label "st&#322;uczenie"
  ]
  node [
    id 447
    label "trafienie"
  ]
  node [
    id 448
    label "pogorszenie"
  ]
  node [
    id 449
    label "skrytykowanie"
  ]
  node [
    id 450
    label "&#347;ci&#281;cie"
  ]
  node [
    id 451
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 452
    label "zadanie"
  ]
  node [
    id 453
    label "rush"
  ]
  node [
    id 454
    label "odbicie_si&#281;"
  ]
  node [
    id 455
    label "cios"
  ]
  node [
    id 456
    label "wdarcie_si&#281;"
  ]
  node [
    id 457
    label "stroke"
  ]
  node [
    id 458
    label "uderzanie"
  ]
  node [
    id 459
    label "time"
  ]
  node [
    id 460
    label "zdarzenie_si&#281;"
  ]
  node [
    id 461
    label "stukni&#281;cie"
  ]
  node [
    id 462
    label "zrobienie"
  ]
  node [
    id 463
    label "nast&#261;pienie"
  ]
  node [
    id 464
    label "charge"
  ]
  node [
    id 465
    label "walka"
  ]
  node [
    id 466
    label "dostanie"
  ]
  node [
    id 467
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 468
    label "pogoda"
  ]
  node [
    id 469
    label "flap"
  ]
  node [
    id 470
    label "pobicie"
  ]
  node [
    id 471
    label "spowodowanie"
  ]
  node [
    id 472
    label "bat"
  ]
  node [
    id 473
    label "poczucie"
  ]
  node [
    id 474
    label "coup"
  ]
  node [
    id 475
    label "dotkni&#281;cie"
  ]
  node [
    id 476
    label "instrumentalizacja"
  ]
  node [
    id 477
    label "introwertyczny"
  ]
  node [
    id 478
    label "hermetycznie"
  ]
  node [
    id 479
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 480
    label "kryjomy"
  ]
  node [
    id 481
    label "ograniczony"
  ]
  node [
    id 482
    label "wypowied&#378;"
  ]
  node [
    id 483
    label "siniec"
  ]
  node [
    id 484
    label "uwaga"
  ]
  node [
    id 485
    label "rzecz"
  ]
  node [
    id 486
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 487
    label "powieka"
  ]
  node [
    id 488
    label "oczy"
  ]
  node [
    id 489
    label "&#347;lepko"
  ]
  node [
    id 490
    label "ga&#322;ka_oczna"
  ]
  node [
    id 491
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 492
    label "&#347;lepie"
  ]
  node [
    id 493
    label "twarz"
  ]
  node [
    id 494
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 495
    label "nerw_wzrokowy"
  ]
  node [
    id 496
    label "wzrok"
  ]
  node [
    id 497
    label "&#378;renica"
  ]
  node [
    id 498
    label "spoj&#243;wka"
  ]
  node [
    id 499
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 500
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 501
    label "kaprawie&#263;"
  ]
  node [
    id 502
    label "kaprawienie"
  ]
  node [
    id 503
    label "spojrzenie"
  ]
  node [
    id 504
    label "net"
  ]
  node [
    id 505
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 506
    label "coloboma"
  ]
  node [
    id 507
    label "ros&#243;&#322;"
  ]
  node [
    id 508
    label "znieruchomienie"
  ]
  node [
    id 509
    label "stacjonarnie"
  ]
  node [
    id 510
    label "unieruchamianie"
  ]
  node [
    id 511
    label "nieruchomo"
  ]
  node [
    id 512
    label "nieruchomienie"
  ]
  node [
    id 513
    label "unieruchomienie"
  ]
  node [
    id 514
    label "trwa&#263;"
  ]
  node [
    id 515
    label "ask"
  ]
  node [
    id 516
    label "invite"
  ]
  node [
    id 517
    label "zach&#281;ca&#263;"
  ]
  node [
    id 518
    label "preach"
  ]
  node [
    id 519
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 520
    label "pies"
  ]
  node [
    id 521
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 522
    label "poleca&#263;"
  ]
  node [
    id 523
    label "zaprasza&#263;"
  ]
  node [
    id 524
    label "suffice"
  ]
  node [
    id 525
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 526
    label "opinion"
  ]
  node [
    id 527
    label "zmatowienie"
  ]
  node [
    id 528
    label "grupa"
  ]
  node [
    id 529
    label "wokal"
  ]
  node [
    id 530
    label "nakaz"
  ]
  node [
    id 531
    label "regestr"
  ]
  node [
    id 532
    label "&#347;piewak_operowy"
  ]
  node [
    id 533
    label "matowie&#263;"
  ]
  node [
    id 534
    label "stanowisko"
  ]
  node [
    id 535
    label "mutacja"
  ]
  node [
    id 536
    label "partia"
  ]
  node [
    id 537
    label "&#347;piewak"
  ]
  node [
    id 538
    label "emisja"
  ]
  node [
    id 539
    label "zmatowie&#263;"
  ]
  node [
    id 540
    label "zesp&#243;&#322;"
  ]
  node [
    id 541
    label "zdolno&#347;&#263;"
  ]
  node [
    id 542
    label "decyzja"
  ]
  node [
    id 543
    label "linia_melodyczna"
  ]
  node [
    id 544
    label "matowienie"
  ]
  node [
    id 545
    label "ch&#243;rzysta"
  ]
  node [
    id 546
    label "foniatra"
  ]
  node [
    id 547
    label "&#347;piewaczka"
  ]
  node [
    id 548
    label "byd&#322;o"
  ]
  node [
    id 549
    label "zobo"
  ]
  node [
    id 550
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 551
    label "yakalo"
  ]
  node [
    id 552
    label "dzo"
  ]
  node [
    id 553
    label "skamienienie"
  ]
  node [
    id 554
    label "z&#322;&#243;g"
  ]
  node [
    id 555
    label "jednostka_avoirdupois"
  ]
  node [
    id 556
    label "tworzywo"
  ]
  node [
    id 557
    label "rock"
  ]
  node [
    id 558
    label "lapidarium"
  ]
  node [
    id 559
    label "minera&#322;_barwny"
  ]
  node [
    id 560
    label "Had&#380;ar"
  ]
  node [
    id 561
    label "autografia"
  ]
  node [
    id 562
    label "rekwizyt_do_gry"
  ]
  node [
    id 563
    label "osad"
  ]
  node [
    id 564
    label "funt"
  ]
  node [
    id 565
    label "mad&#380;ong"
  ]
  node [
    id 566
    label "oczko"
  ]
  node [
    id 567
    label "cube"
  ]
  node [
    id 568
    label "p&#322;ytka"
  ]
  node [
    id 569
    label "domino"
  ]
  node [
    id 570
    label "ci&#281;&#380;ar"
  ]
  node [
    id 571
    label "ska&#322;a"
  ]
  node [
    id 572
    label "kamienienie"
  ]
  node [
    id 573
    label "by&#263;"
  ]
  node [
    id 574
    label "flood"
  ]
  node [
    id 575
    label "opada&#263;"
  ]
  node [
    id 576
    label "pogr&#261;&#380;a&#263;_si&#281;"
  ]
  node [
    id 577
    label "sink"
  ]
  node [
    id 578
    label "zanika&#263;"
  ]
  node [
    id 579
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 580
    label "przepada&#263;"
  ]
  node [
    id 581
    label "zalewa&#263;"
  ]
  node [
    id 582
    label "pogarsza&#263;_si&#281;"
  ]
  node [
    id 583
    label "p&#322;ywa&#263;"
  ]
  node [
    id 584
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 585
    label "swimming"
  ]
  node [
    id 586
    label "zanurza&#263;_si&#281;"
  ]
  node [
    id 587
    label "nadzwyczajny"
  ]
  node [
    id 588
    label "nieograniczenie"
  ]
  node [
    id 589
    label "przestrze&#324;"
  ]
  node [
    id 590
    label "niesko&#324;czenie"
  ]
  node [
    id 591
    label "bezczasowy"
  ]
  node [
    id 592
    label "rozleg&#322;y"
  ]
  node [
    id 593
    label "ogromny"
  ]
  node [
    id 594
    label "otwarty"
  ]
  node [
    id 595
    label "olbrzymi"
  ]
  node [
    id 596
    label "strasznie"
  ]
  node [
    id 597
    label "straszny"
  ]
  node [
    id 598
    label "straszliwie"
  ]
  node [
    id 599
    label "kurewski"
  ]
  node [
    id 600
    label "przerwa"
  ]
  node [
    id 601
    label "pok&#243;j"
  ]
  node [
    id 602
    label "rozmowa"
  ]
  node [
    id 603
    label "cicha_modlitwa"
  ]
  node [
    id 604
    label "g&#322;adki"
  ]
  node [
    id 605
    label "spok&#243;j"
  ]
  node [
    id 606
    label "ci&#261;g"
  ]
  node [
    id 607
    label "peace"
  ]
  node [
    id 608
    label "tajemno&#347;&#263;"
  ]
  node [
    id 609
    label "cicha_praca"
  ]
  node [
    id 610
    label "cicha_msza"
  ]
  node [
    id 611
    label "motionlessness"
  ]
  node [
    id 612
    label "span"
  ]
  node [
    id 613
    label "obdarowywa&#263;"
  ]
  node [
    id 614
    label "towarzyszy&#263;"
  ]
  node [
    id 615
    label "admit"
  ]
  node [
    id 616
    label "tacza&#263;"
  ]
  node [
    id 617
    label "enclose"
  ]
  node [
    id 618
    label "roztacza&#263;"
  ]
  node [
    id 619
    label "stuka&#263;"
  ]
  node [
    id 620
    label "dorzuca&#263;"
  ]
  node [
    id 621
    label "obarcza&#263;"
  ]
  node [
    id 622
    label "rzn&#261;&#263;"
  ]
  node [
    id 623
    label "r&#380;n&#261;&#263;"
  ]
  node [
    id 624
    label "przygniata&#263;"
  ]
  node [
    id 625
    label "doskwiera&#263;"
  ]
  node [
    id 626
    label "urge"
  ]
  node [
    id 627
    label "dupny"
  ]
  node [
    id 628
    label "wysoce"
  ]
  node [
    id 629
    label "wyj&#261;tkowy"
  ]
  node [
    id 630
    label "wybitny"
  ]
  node [
    id 631
    label "znaczny"
  ]
  node [
    id 632
    label "prawdziwy"
  ]
  node [
    id 633
    label "wa&#380;ny"
  ]
  node [
    id 634
    label "nieprzeci&#281;tny"
  ]
  node [
    id 635
    label "jasnobr&#261;zowy"
  ]
  node [
    id 636
    label "&#380;&#243;&#322;toszary"
  ]
  node [
    id 637
    label "Skandynawia"
  ]
  node [
    id 638
    label "Yorkshire"
  ]
  node [
    id 639
    label "Kaukaz"
  ]
  node [
    id 640
    label "Kaszmir"
  ]
  node [
    id 641
    label "Podbeskidzie"
  ]
  node [
    id 642
    label "Toskania"
  ]
  node [
    id 643
    label "&#321;emkowszczyzna"
  ]
  node [
    id 644
    label "obszar"
  ]
  node [
    id 645
    label "Amhara"
  ]
  node [
    id 646
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 647
    label "Lombardia"
  ]
  node [
    id 648
    label "Kalabria"
  ]
  node [
    id 649
    label "kort"
  ]
  node [
    id 650
    label "Tyrol"
  ]
  node [
    id 651
    label "Pamir"
  ]
  node [
    id 652
    label "Lubelszczyzna"
  ]
  node [
    id 653
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 654
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 655
    label "&#379;ywiecczyzna"
  ]
  node [
    id 656
    label "ryzosfera"
  ]
  node [
    id 657
    label "Europa_Wschodnia"
  ]
  node [
    id 658
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 659
    label "Zabajkale"
  ]
  node [
    id 660
    label "Kaszuby"
  ]
  node [
    id 661
    label "Noworosja"
  ]
  node [
    id 662
    label "Bo&#347;nia"
  ]
  node [
    id 663
    label "Ba&#322;kany"
  ]
  node [
    id 664
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 665
    label "Anglia"
  ]
  node [
    id 666
    label "Kielecczyzna"
  ]
  node [
    id 667
    label "Pomorze_Zachodnie"
  ]
  node [
    id 668
    label "Opolskie"
  ]
  node [
    id 669
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 670
    label "skorupa_ziemska"
  ]
  node [
    id 671
    label "Ko&#322;yma"
  ]
  node [
    id 672
    label "Oksytania"
  ]
  node [
    id 673
    label "Syjon"
  ]
  node [
    id 674
    label "posadzka"
  ]
  node [
    id 675
    label "pa&#324;stwo"
  ]
  node [
    id 676
    label "Kociewie"
  ]
  node [
    id 677
    label "Huculszczyzna"
  ]
  node [
    id 678
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 679
    label "budynek"
  ]
  node [
    id 680
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 681
    label "Bawaria"
  ]
  node [
    id 682
    label "pomieszczenie"
  ]
  node [
    id 683
    label "pr&#243;chnica"
  ]
  node [
    id 684
    label "glinowanie"
  ]
  node [
    id 685
    label "Maghreb"
  ]
  node [
    id 686
    label "Bory_Tucholskie"
  ]
  node [
    id 687
    label "Europa_Zachodnia"
  ]
  node [
    id 688
    label "Kerala"
  ]
  node [
    id 689
    label "Podhale"
  ]
  node [
    id 690
    label "Kabylia"
  ]
  node [
    id 691
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 692
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 693
    label "Ma&#322;opolska"
  ]
  node [
    id 694
    label "Polesie"
  ]
  node [
    id 695
    label "Liguria"
  ]
  node [
    id 696
    label "&#321;&#243;dzkie"
  ]
  node [
    id 697
    label "geosystem"
  ]
  node [
    id 698
    label "Palestyna"
  ]
  node [
    id 699
    label "Bojkowszczyzna"
  ]
  node [
    id 700
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 701
    label "Karaiby"
  ]
  node [
    id 702
    label "S&#261;decczyzna"
  ]
  node [
    id 703
    label "Sand&#380;ak"
  ]
  node [
    id 704
    label "Nadrenia"
  ]
  node [
    id 705
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 706
    label "Zakarpacie"
  ]
  node [
    id 707
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 708
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 709
    label "Zag&#243;rze"
  ]
  node [
    id 710
    label "Andaluzja"
  ]
  node [
    id 711
    label "Turkiestan"
  ]
  node [
    id 712
    label "Naddniestrze"
  ]
  node [
    id 713
    label "Hercegowina"
  ]
  node [
    id 714
    label "p&#322;aszczyzna"
  ]
  node [
    id 715
    label "Opolszczyzna"
  ]
  node [
    id 716
    label "jednostka_administracyjna"
  ]
  node [
    id 717
    label "Lotaryngia"
  ]
  node [
    id 718
    label "Afryka_Wschodnia"
  ]
  node [
    id 719
    label "Szlezwik"
  ]
  node [
    id 720
    label "powierzchnia"
  ]
  node [
    id 721
    label "glinowa&#263;"
  ]
  node [
    id 722
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 723
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 724
    label "podglebie"
  ]
  node [
    id 725
    label "Mazowsze"
  ]
  node [
    id 726
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 727
    label "teren"
  ]
  node [
    id 728
    label "Afryka_Zachodnia"
  ]
  node [
    id 729
    label "czynnik_produkcji"
  ]
  node [
    id 730
    label "Galicja"
  ]
  node [
    id 731
    label "Szkocja"
  ]
  node [
    id 732
    label "Walia"
  ]
  node [
    id 733
    label "Powi&#347;le"
  ]
  node [
    id 734
    label "penetrator"
  ]
  node [
    id 735
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 736
    label "kompleks_sorpcyjny"
  ]
  node [
    id 737
    label "Zamojszczyzna"
  ]
  node [
    id 738
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 739
    label "Kujawy"
  ]
  node [
    id 740
    label "Podlasie"
  ]
  node [
    id 741
    label "Laponia"
  ]
  node [
    id 742
    label "Umbria"
  ]
  node [
    id 743
    label "plantowa&#263;"
  ]
  node [
    id 744
    label "Mezoameryka"
  ]
  node [
    id 745
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 746
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 747
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 748
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 749
    label "Kurdystan"
  ]
  node [
    id 750
    label "Kampania"
  ]
  node [
    id 751
    label "Armagnac"
  ]
  node [
    id 752
    label "Polinezja"
  ]
  node [
    id 753
    label "Warmia"
  ]
  node [
    id 754
    label "Wielkopolska"
  ]
  node [
    id 755
    label "litosfera"
  ]
  node [
    id 756
    label "Bordeaux"
  ]
  node [
    id 757
    label "Lauda"
  ]
  node [
    id 758
    label "Mazury"
  ]
  node [
    id 759
    label "Podkarpacie"
  ]
  node [
    id 760
    label "Oceania"
  ]
  node [
    id 761
    label "Lasko"
  ]
  node [
    id 762
    label "Amazonia"
  ]
  node [
    id 763
    label "pojazd"
  ]
  node [
    id 764
    label "glej"
  ]
  node [
    id 765
    label "martwica"
  ]
  node [
    id 766
    label "zapadnia"
  ]
  node [
    id 767
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 768
    label "dotleni&#263;"
  ]
  node [
    id 769
    label "Tonkin"
  ]
  node [
    id 770
    label "Kurpie"
  ]
  node [
    id 771
    label "Azja_Wschodnia"
  ]
  node [
    id 772
    label "Mikronezja"
  ]
  node [
    id 773
    label "Ukraina_Zachodnia"
  ]
  node [
    id 774
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 775
    label "Turyngia"
  ]
  node [
    id 776
    label "Baszkiria"
  ]
  node [
    id 777
    label "Apulia"
  ]
  node [
    id 778
    label "miejsce"
  ]
  node [
    id 779
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 780
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 781
    label "Indochiny"
  ]
  node [
    id 782
    label "Lubuskie"
  ]
  node [
    id 783
    label "Biskupizna"
  ]
  node [
    id 784
    label "domain"
  ]
  node [
    id 785
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 786
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 787
    label "zu&#380;y&#263;"
  ]
  node [
    id 788
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 789
    label "zbacza&#263;"
  ]
  node [
    id 790
    label "ubywa&#263;"
  ]
  node [
    id 791
    label "podrze&#263;"
  ]
  node [
    id 792
    label "obni&#380;a&#263;_si&#281;"
  ]
  node [
    id 793
    label "set"
  ]
  node [
    id 794
    label "wschodzi&#263;"
  ]
  node [
    id 795
    label "refuse"
  ]
  node [
    id 796
    label "wprowadza&#263;"
  ]
  node [
    id 797
    label "osi&#261;ga&#263;"
  ]
  node [
    id 798
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 799
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 800
    label "temat"
  ]
  node [
    id 801
    label "umiera&#263;"
  ]
  node [
    id 802
    label "authorize"
  ]
  node [
    id 803
    label "przestawa&#263;"
  ]
  node [
    id 804
    label "odpuszcza&#263;"
  ]
  node [
    id 805
    label "digress"
  ]
  node [
    id 806
    label "str&#243;j"
  ]
  node [
    id 807
    label "opuszcza&#263;"
  ]
  node [
    id 808
    label "obni&#380;a&#263;"
  ]
  node [
    id 809
    label "&#347;piewa&#263;"
  ]
  node [
    id 810
    label "odpada&#263;"
  ]
  node [
    id 811
    label "gin&#261;&#263;"
  ]
  node [
    id 812
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 813
    label "przenosi&#263;_si&#281;"
  ]
  node [
    id 814
    label "przej&#347;&#263;"
  ]
  node [
    id 815
    label "mija&#263;"
  ]
  node [
    id 816
    label "i&#347;&#263;"
  ]
  node [
    id 817
    label "fizykalnie"
  ]
  node [
    id 818
    label "fizycznie"
  ]
  node [
    id 819
    label "materializowanie"
  ]
  node [
    id 820
    label "gimnastyczny"
  ]
  node [
    id 821
    label "widoczny"
  ]
  node [
    id 822
    label "namacalny"
  ]
  node [
    id 823
    label "zmaterializowanie"
  ]
  node [
    id 824
    label "organiczny"
  ]
  node [
    id 825
    label "materjalny"
  ]
  node [
    id 826
    label "apatyczno&#347;&#263;"
  ]
  node [
    id 827
    label "bierno&#347;&#263;"
  ]
  node [
    id 828
    label "nastr&#243;j"
  ]
  node [
    id 829
    label "cudacznie"
  ]
  node [
    id 830
    label "&#347;mieszny"
  ]
  node [
    id 831
    label "dziwaczny"
  ]
  node [
    id 832
    label "ekscentryczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 206
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 38
  ]
  edge [
    source 20
    target 20
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 20
    target 42
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 20
    target 44
  ]
  edge [
    source 20
    target 45
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 232
  ]
  edge [
    source 21
    target 233
  ]
  edge [
    source 21
    target 234
  ]
  edge [
    source 21
    target 235
  ]
  edge [
    source 21
    target 236
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 22
    target 238
  ]
  edge [
    source 22
    target 239
  ]
  edge [
    source 22
    target 240
  ]
  edge [
    source 22
    target 241
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 32
  ]
  edge [
    source 23
    target 33
  ]
  edge [
    source 23
    target 57
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 243
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 244
  ]
  edge [
    source 24
    target 245
  ]
  edge [
    source 24
    target 246
  ]
  edge [
    source 24
    target 247
  ]
  edge [
    source 24
    target 248
  ]
  edge [
    source 24
    target 249
  ]
  edge [
    source 24
    target 250
  ]
  edge [
    source 24
    target 251
  ]
  edge [
    source 24
    target 252
  ]
  edge [
    source 24
    target 253
  ]
  edge [
    source 24
    target 254
  ]
  edge [
    source 24
    target 255
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 24
    target 48
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 257
  ]
  edge [
    source 25
    target 258
  ]
  edge [
    source 25
    target 259
  ]
  edge [
    source 25
    target 260
  ]
  edge [
    source 25
    target 261
  ]
  edge [
    source 25
    target 262
  ]
  edge [
    source 25
    target 263
  ]
  edge [
    source 25
    target 264
  ]
  edge [
    source 25
    target 265
  ]
  edge [
    source 25
    target 266
  ]
  edge [
    source 25
    target 267
  ]
  edge [
    source 25
    target 268
  ]
  edge [
    source 25
    target 269
  ]
  edge [
    source 25
    target 270
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 271
  ]
  edge [
    source 26
    target 272
  ]
  edge [
    source 26
    target 273
  ]
  edge [
    source 26
    target 274
  ]
  edge [
    source 26
    target 275
  ]
  edge [
    source 26
    target 276
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 277
  ]
  edge [
    source 27
    target 278
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 281
  ]
  edge [
    source 28
    target 282
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 283
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 30
    target 288
  ]
  edge [
    source 30
    target 289
  ]
  edge [
    source 30
    target 290
  ]
  edge [
    source 30
    target 291
  ]
  edge [
    source 30
    target 292
  ]
  edge [
    source 30
    target 293
  ]
  edge [
    source 30
    target 294
  ]
  edge [
    source 30
    target 295
  ]
  edge [
    source 30
    target 296
  ]
  edge [
    source 30
    target 297
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 155
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 308
  ]
  edge [
    source 30
    target 309
  ]
  edge [
    source 30
    target 310
  ]
  edge [
    source 30
    target 311
  ]
  edge [
    source 30
    target 312
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 313
  ]
  edge [
    source 31
    target 314
  ]
  edge [
    source 31
    target 315
  ]
  edge [
    source 31
    target 316
  ]
  edge [
    source 31
    target 317
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 32
    target 174
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 32
    target 323
  ]
  edge [
    source 32
    target 324
  ]
  edge [
    source 32
    target 325
  ]
  edge [
    source 32
    target 326
  ]
  edge [
    source 32
    target 327
  ]
  edge [
    source 32
    target 328
  ]
  edge [
    source 32
    target 329
  ]
  edge [
    source 32
    target 330
  ]
  edge [
    source 32
    target 331
  ]
  edge [
    source 32
    target 57
  ]
  edge [
    source 32
    target 58
  ]
  edge [
    source 32
    target 64
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 332
  ]
  edge [
    source 33
    target 333
  ]
  edge [
    source 34
    target 334
  ]
  edge [
    source 34
    target 335
  ]
  edge [
    source 34
    target 336
  ]
  edge [
    source 34
    target 286
  ]
  edge [
    source 34
    target 337
  ]
  edge [
    source 34
    target 292
  ]
  edge [
    source 34
    target 338
  ]
  edge [
    source 34
    target 339
  ]
  edge [
    source 34
    target 340
  ]
  edge [
    source 34
    target 341
  ]
  edge [
    source 34
    target 342
  ]
  edge [
    source 34
    target 343
  ]
  edge [
    source 34
    target 344
  ]
  edge [
    source 34
    target 345
  ]
  edge [
    source 34
    target 346
  ]
  edge [
    source 34
    target 347
  ]
  edge [
    source 34
    target 348
  ]
  edge [
    source 34
    target 349
  ]
  edge [
    source 34
    target 350
  ]
  edge [
    source 34
    target 351
  ]
  edge [
    source 34
    target 303
  ]
  edge [
    source 34
    target 304
  ]
  edge [
    source 34
    target 352
  ]
  edge [
    source 34
    target 353
  ]
  edge [
    source 34
    target 354
  ]
  edge [
    source 34
    target 308
  ]
  edge [
    source 34
    target 355
  ]
  edge [
    source 34
    target 356
  ]
  edge [
    source 34
    target 357
  ]
  edge [
    source 34
    target 358
  ]
  edge [
    source 34
    target 359
  ]
  edge [
    source 34
    target 360
  ]
  edge [
    source 34
    target 361
  ]
  edge [
    source 34
    target 362
  ]
  edge [
    source 34
    target 363
  ]
  edge [
    source 34
    target 310
  ]
  edge [
    source 34
    target 364
  ]
  edge [
    source 34
    target 365
  ]
  edge [
    source 34
    target 366
  ]
  edge [
    source 34
    target 367
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 368
  ]
  edge [
    source 35
    target 369
  ]
  edge [
    source 35
    target 370
  ]
  edge [
    source 35
    target 371
  ]
  edge [
    source 35
    target 372
  ]
  edge [
    source 35
    target 373
  ]
  edge [
    source 35
    target 374
  ]
  edge [
    source 35
    target 375
  ]
  edge [
    source 35
    target 376
  ]
  edge [
    source 35
    target 377
  ]
  edge [
    source 35
    target 378
  ]
  edge [
    source 35
    target 379
  ]
  edge [
    source 35
    target 380
  ]
  edge [
    source 35
    target 381
  ]
  edge [
    source 35
    target 382
  ]
  edge [
    source 35
    target 383
  ]
  edge [
    source 35
    target 384
  ]
  edge [
    source 35
    target 385
  ]
  edge [
    source 35
    target 386
  ]
  edge [
    source 35
    target 387
  ]
  edge [
    source 35
    target 388
  ]
  edge [
    source 35
    target 389
  ]
  edge [
    source 35
    target 390
  ]
  edge [
    source 35
    target 391
  ]
  edge [
    source 35
    target 392
  ]
  edge [
    source 35
    target 393
  ]
  edge [
    source 35
    target 394
  ]
  edge [
    source 35
    target 395
  ]
  edge [
    source 35
    target 396
  ]
  edge [
    source 35
    target 397
  ]
  edge [
    source 35
    target 398
  ]
  edge [
    source 35
    target 399
  ]
  edge [
    source 35
    target 400
  ]
  edge [
    source 35
    target 401
  ]
  edge [
    source 35
    target 402
  ]
  edge [
    source 35
    target 403
  ]
  edge [
    source 35
    target 404
  ]
  edge [
    source 35
    target 405
  ]
  edge [
    source 35
    target 406
  ]
  edge [
    source 35
    target 407
  ]
  edge [
    source 35
    target 100
  ]
  edge [
    source 35
    target 408
  ]
  edge [
    source 35
    target 409
  ]
  edge [
    source 35
    target 410
  ]
  edge [
    source 35
    target 411
  ]
  edge [
    source 35
    target 412
  ]
  edge [
    source 35
    target 413
  ]
  edge [
    source 35
    target 414
  ]
  edge [
    source 35
    target 415
  ]
  edge [
    source 35
    target 416
  ]
  edge [
    source 35
    target 417
  ]
  edge [
    source 35
    target 418
  ]
  edge [
    source 35
    target 419
  ]
  edge [
    source 35
    target 420
  ]
  edge [
    source 35
    target 421
  ]
  edge [
    source 35
    target 422
  ]
  edge [
    source 35
    target 423
  ]
  edge [
    source 35
    target 424
  ]
  edge [
    source 35
    target 425
  ]
  edge [
    source 35
    target 426
  ]
  edge [
    source 35
    target 427
  ]
  edge [
    source 35
    target 428
  ]
  edge [
    source 35
    target 49
  ]
  edge [
    source 35
    target 52
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 429
  ]
  edge [
    source 36
    target 430
  ]
  edge [
    source 36
    target 431
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 432
  ]
  edge [
    source 37
    target 433
  ]
  edge [
    source 37
    target 434
  ]
  edge [
    source 37
    target 435
  ]
  edge [
    source 37
    target 436
  ]
  edge [
    source 37
    target 437
  ]
  edge [
    source 37
    target 438
  ]
  edge [
    source 38
    target 439
  ]
  edge [
    source 38
    target 440
  ]
  edge [
    source 38
    target 441
  ]
  edge [
    source 38
    target 442
  ]
  edge [
    source 38
    target 443
  ]
  edge [
    source 38
    target 444
  ]
  edge [
    source 38
    target 445
  ]
  edge [
    source 38
    target 446
  ]
  edge [
    source 38
    target 447
  ]
  edge [
    source 38
    target 448
  ]
  edge [
    source 38
    target 449
  ]
  edge [
    source 38
    target 450
  ]
  edge [
    source 38
    target 451
  ]
  edge [
    source 38
    target 452
  ]
  edge [
    source 38
    target 453
  ]
  edge [
    source 38
    target 454
  ]
  edge [
    source 38
    target 455
  ]
  edge [
    source 38
    target 456
  ]
  edge [
    source 38
    target 457
  ]
  edge [
    source 38
    target 458
  ]
  edge [
    source 38
    target 459
  ]
  edge [
    source 38
    target 460
  ]
  edge [
    source 38
    target 461
  ]
  edge [
    source 38
    target 95
  ]
  edge [
    source 38
    target 462
  ]
  edge [
    source 38
    target 463
  ]
  edge [
    source 38
    target 464
  ]
  edge [
    source 38
    target 263
  ]
  edge [
    source 38
    target 465
  ]
  edge [
    source 38
    target 466
  ]
  edge [
    source 38
    target 467
  ]
  edge [
    source 38
    target 468
  ]
  edge [
    source 38
    target 469
  ]
  edge [
    source 38
    target 470
  ]
  edge [
    source 38
    target 471
  ]
  edge [
    source 38
    target 472
  ]
  edge [
    source 38
    target 473
  ]
  edge [
    source 38
    target 474
  ]
  edge [
    source 38
    target 475
  ]
  edge [
    source 38
    target 255
  ]
  edge [
    source 38
    target 476
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 477
  ]
  edge [
    source 39
    target 478
  ]
  edge [
    source 39
    target 479
  ]
  edge [
    source 39
    target 480
  ]
  edge [
    source 39
    target 481
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 482
  ]
  edge [
    source 40
    target 483
  ]
  edge [
    source 40
    target 484
  ]
  edge [
    source 40
    target 485
  ]
  edge [
    source 40
    target 486
  ]
  edge [
    source 40
    target 487
  ]
  edge [
    source 40
    target 488
  ]
  edge [
    source 40
    target 489
  ]
  edge [
    source 40
    target 490
  ]
  edge [
    source 40
    target 491
  ]
  edge [
    source 40
    target 492
  ]
  edge [
    source 40
    target 493
  ]
  edge [
    source 40
    target 410
  ]
  edge [
    source 40
    target 494
  ]
  edge [
    source 40
    target 495
  ]
  edge [
    source 40
    target 496
  ]
  edge [
    source 40
    target 497
  ]
  edge [
    source 40
    target 498
  ]
  edge [
    source 40
    target 499
  ]
  edge [
    source 40
    target 500
  ]
  edge [
    source 40
    target 501
  ]
  edge [
    source 40
    target 502
  ]
  edge [
    source 40
    target 503
  ]
  edge [
    source 40
    target 504
  ]
  edge [
    source 40
    target 505
  ]
  edge [
    source 40
    target 506
  ]
  edge [
    source 40
    target 507
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 508
  ]
  edge [
    source 41
    target 509
  ]
  edge [
    source 41
    target 510
  ]
  edge [
    source 41
    target 511
  ]
  edge [
    source 41
    target 512
  ]
  edge [
    source 41
    target 513
  ]
  edge [
    source 42
    target 514
  ]
  edge [
    source 42
    target 307
  ]
  edge [
    source 42
    target 515
  ]
  edge [
    source 42
    target 516
  ]
  edge [
    source 42
    target 517
  ]
  edge [
    source 42
    target 518
  ]
  edge [
    source 42
    target 519
  ]
  edge [
    source 42
    target 520
  ]
  edge [
    source 42
    target 521
  ]
  edge [
    source 42
    target 522
  ]
  edge [
    source 42
    target 523
  ]
  edge [
    source 42
    target 524
  ]
  edge [
    source 42
    target 525
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 526
  ]
  edge [
    source 43
    target 482
  ]
  edge [
    source 43
    target 527
  ]
  edge [
    source 43
    target 251
  ]
  edge [
    source 43
    target 528
  ]
  edge [
    source 43
    target 529
  ]
  edge [
    source 43
    target 253
  ]
  edge [
    source 43
    target 254
  ]
  edge [
    source 43
    target 530
  ]
  edge [
    source 43
    target 531
  ]
  edge [
    source 43
    target 532
  ]
  edge [
    source 43
    target 533
  ]
  edge [
    source 43
    target 246
  ]
  edge [
    source 43
    target 534
  ]
  edge [
    source 43
    target 187
  ]
  edge [
    source 43
    target 535
  ]
  edge [
    source 43
    target 536
  ]
  edge [
    source 43
    target 537
  ]
  edge [
    source 43
    target 538
  ]
  edge [
    source 43
    target 245
  ]
  edge [
    source 43
    target 539
  ]
  edge [
    source 43
    target 249
  ]
  edge [
    source 43
    target 540
  ]
  edge [
    source 43
    target 250
  ]
  edge [
    source 43
    target 541
  ]
  edge [
    source 43
    target 542
  ]
  edge [
    source 43
    target 256
  ]
  edge [
    source 43
    target 543
  ]
  edge [
    source 43
    target 247
  ]
  edge [
    source 43
    target 248
  ]
  edge [
    source 43
    target 252
  ]
  edge [
    source 43
    target 544
  ]
  edge [
    source 43
    target 545
  ]
  edge [
    source 43
    target 255
  ]
  edge [
    source 43
    target 546
  ]
  edge [
    source 43
    target 547
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 54
  ]
  edge [
    source 46
    target 56
  ]
  edge [
    source 46
    target 548
  ]
  edge [
    source 46
    target 549
  ]
  edge [
    source 46
    target 550
  ]
  edge [
    source 46
    target 551
  ]
  edge [
    source 46
    target 552
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 553
  ]
  edge [
    source 47
    target 554
  ]
  edge [
    source 47
    target 555
  ]
  edge [
    source 47
    target 556
  ]
  edge [
    source 47
    target 557
  ]
  edge [
    source 47
    target 558
  ]
  edge [
    source 47
    target 559
  ]
  edge [
    source 47
    target 560
  ]
  edge [
    source 47
    target 561
  ]
  edge [
    source 47
    target 562
  ]
  edge [
    source 47
    target 563
  ]
  edge [
    source 47
    target 564
  ]
  edge [
    source 47
    target 565
  ]
  edge [
    source 47
    target 566
  ]
  edge [
    source 47
    target 567
  ]
  edge [
    source 47
    target 568
  ]
  edge [
    source 47
    target 569
  ]
  edge [
    source 47
    target 570
  ]
  edge [
    source 47
    target 571
  ]
  edge [
    source 47
    target 572
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 573
  ]
  edge [
    source 48
    target 574
  ]
  edge [
    source 48
    target 575
  ]
  edge [
    source 48
    target 576
  ]
  edge [
    source 48
    target 577
  ]
  edge [
    source 48
    target 578
  ]
  edge [
    source 48
    target 579
  ]
  edge [
    source 48
    target 580
  ]
  edge [
    source 48
    target 581
  ]
  edge [
    source 48
    target 582
  ]
  edge [
    source 48
    target 583
  ]
  edge [
    source 48
    target 584
  ]
  edge [
    source 48
    target 585
  ]
  edge [
    source 48
    target 586
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 587
  ]
  edge [
    source 49
    target 588
  ]
  edge [
    source 49
    target 589
  ]
  edge [
    source 49
    target 590
  ]
  edge [
    source 49
    target 591
  ]
  edge [
    source 49
    target 592
  ]
  edge [
    source 49
    target 593
  ]
  edge [
    source 49
    target 594
  ]
  edge [
    source 49
    target 595
  ]
  edge [
    source 49
    target 52
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 596
  ]
  edge [
    source 51
    target 597
  ]
  edge [
    source 51
    target 598
  ]
  edge [
    source 51
    target 599
  ]
  edge [
    source 51
    target 595
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 600
  ]
  edge [
    source 52
    target 601
  ]
  edge [
    source 52
    target 602
  ]
  edge [
    source 52
    target 603
  ]
  edge [
    source 52
    target 604
  ]
  edge [
    source 52
    target 184
  ]
  edge [
    source 52
    target 605
  ]
  edge [
    source 52
    target 100
  ]
  edge [
    source 52
    target 606
  ]
  edge [
    source 52
    target 607
  ]
  edge [
    source 52
    target 608
  ]
  edge [
    source 52
    target 609
  ]
  edge [
    source 52
    target 610
  ]
  edge [
    source 52
    target 187
  ]
  edge [
    source 52
    target 611
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 612
  ]
  edge [
    source 53
    target 613
  ]
  edge [
    source 53
    target 614
  ]
  edge [
    source 53
    target 615
  ]
  edge [
    source 53
    target 155
  ]
  edge [
    source 53
    target 616
  ]
  edge [
    source 53
    target 617
  ]
  edge [
    source 53
    target 364
  ]
  edge [
    source 53
    target 618
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 619
  ]
  edge [
    source 55
    target 620
  ]
  edge [
    source 55
    target 621
  ]
  edge [
    source 55
    target 622
  ]
  edge [
    source 55
    target 623
  ]
  edge [
    source 55
    target 624
  ]
  edge [
    source 55
    target 362
  ]
  edge [
    source 55
    target 625
  ]
  edge [
    source 55
    target 626
  ]
  edge [
    source 55
    target 365
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 627
  ]
  edge [
    source 57
    target 628
  ]
  edge [
    source 57
    target 629
  ]
  edge [
    source 57
    target 630
  ]
  edge [
    source 57
    target 631
  ]
  edge [
    source 57
    target 632
  ]
  edge [
    source 57
    target 633
  ]
  edge [
    source 57
    target 634
  ]
  edge [
    source 57
    target 64
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 635
  ]
  edge [
    source 58
    target 636
  ]
  edge [
    source 58
    target 64
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 637
  ]
  edge [
    source 59
    target 638
  ]
  edge [
    source 59
    target 639
  ]
  edge [
    source 59
    target 640
  ]
  edge [
    source 59
    target 641
  ]
  edge [
    source 59
    target 642
  ]
  edge [
    source 59
    target 643
  ]
  edge [
    source 59
    target 644
  ]
  edge [
    source 59
    target 645
  ]
  edge [
    source 59
    target 646
  ]
  edge [
    source 59
    target 647
  ]
  edge [
    source 59
    target 648
  ]
  edge [
    source 59
    target 649
  ]
  edge [
    source 59
    target 650
  ]
  edge [
    source 59
    target 651
  ]
  edge [
    source 59
    target 652
  ]
  edge [
    source 59
    target 653
  ]
  edge [
    source 59
    target 654
  ]
  edge [
    source 59
    target 655
  ]
  edge [
    source 59
    target 656
  ]
  edge [
    source 59
    target 657
  ]
  edge [
    source 59
    target 658
  ]
  edge [
    source 59
    target 659
  ]
  edge [
    source 59
    target 660
  ]
  edge [
    source 59
    target 661
  ]
  edge [
    source 59
    target 662
  ]
  edge [
    source 59
    target 663
  ]
  edge [
    source 59
    target 664
  ]
  edge [
    source 59
    target 665
  ]
  edge [
    source 59
    target 666
  ]
  edge [
    source 59
    target 667
  ]
  edge [
    source 59
    target 668
  ]
  edge [
    source 59
    target 669
  ]
  edge [
    source 59
    target 670
  ]
  edge [
    source 59
    target 671
  ]
  edge [
    source 59
    target 672
  ]
  edge [
    source 59
    target 673
  ]
  edge [
    source 59
    target 674
  ]
  edge [
    source 59
    target 675
  ]
  edge [
    source 59
    target 676
  ]
  edge [
    source 59
    target 677
  ]
  edge [
    source 59
    target 678
  ]
  edge [
    source 59
    target 679
  ]
  edge [
    source 59
    target 680
  ]
  edge [
    source 59
    target 681
  ]
  edge [
    source 59
    target 682
  ]
  edge [
    source 59
    target 683
  ]
  edge [
    source 59
    target 684
  ]
  edge [
    source 59
    target 685
  ]
  edge [
    source 59
    target 686
  ]
  edge [
    source 59
    target 687
  ]
  edge [
    source 59
    target 688
  ]
  edge [
    source 59
    target 689
  ]
  edge [
    source 59
    target 690
  ]
  edge [
    source 59
    target 691
  ]
  edge [
    source 59
    target 692
  ]
  edge [
    source 59
    target 693
  ]
  edge [
    source 59
    target 694
  ]
  edge [
    source 59
    target 695
  ]
  edge [
    source 59
    target 696
  ]
  edge [
    source 59
    target 697
  ]
  edge [
    source 59
    target 698
  ]
  edge [
    source 59
    target 699
  ]
  edge [
    source 59
    target 700
  ]
  edge [
    source 59
    target 701
  ]
  edge [
    source 59
    target 702
  ]
  edge [
    source 59
    target 703
  ]
  edge [
    source 59
    target 704
  ]
  edge [
    source 59
    target 705
  ]
  edge [
    source 59
    target 706
  ]
  edge [
    source 59
    target 707
  ]
  edge [
    source 59
    target 708
  ]
  edge [
    source 59
    target 709
  ]
  edge [
    source 59
    target 710
  ]
  edge [
    source 59
    target 711
  ]
  edge [
    source 59
    target 712
  ]
  edge [
    source 59
    target 713
  ]
  edge [
    source 59
    target 714
  ]
  edge [
    source 59
    target 715
  ]
  edge [
    source 59
    target 716
  ]
  edge [
    source 59
    target 717
  ]
  edge [
    source 59
    target 718
  ]
  edge [
    source 59
    target 719
  ]
  edge [
    source 59
    target 720
  ]
  edge [
    source 59
    target 721
  ]
  edge [
    source 59
    target 722
  ]
  edge [
    source 59
    target 723
  ]
  edge [
    source 59
    target 724
  ]
  edge [
    source 59
    target 725
  ]
  edge [
    source 59
    target 726
  ]
  edge [
    source 59
    target 727
  ]
  edge [
    source 59
    target 728
  ]
  edge [
    source 59
    target 729
  ]
  edge [
    source 59
    target 730
  ]
  edge [
    source 59
    target 731
  ]
  edge [
    source 59
    target 732
  ]
  edge [
    source 59
    target 733
  ]
  edge [
    source 59
    target 734
  ]
  edge [
    source 59
    target 735
  ]
  edge [
    source 59
    target 736
  ]
  edge [
    source 59
    target 737
  ]
  edge [
    source 59
    target 738
  ]
  edge [
    source 59
    target 739
  ]
  edge [
    source 59
    target 740
  ]
  edge [
    source 59
    target 741
  ]
  edge [
    source 59
    target 742
  ]
  edge [
    source 59
    target 743
  ]
  edge [
    source 59
    target 744
  ]
  edge [
    source 59
    target 745
  ]
  edge [
    source 59
    target 746
  ]
  edge [
    source 59
    target 747
  ]
  edge [
    source 59
    target 748
  ]
  edge [
    source 59
    target 749
  ]
  edge [
    source 59
    target 750
  ]
  edge [
    source 59
    target 751
  ]
  edge [
    source 59
    target 752
  ]
  edge [
    source 59
    target 753
  ]
  edge [
    source 59
    target 754
  ]
  edge [
    source 59
    target 755
  ]
  edge [
    source 59
    target 756
  ]
  edge [
    source 59
    target 757
  ]
  edge [
    source 59
    target 758
  ]
  edge [
    source 59
    target 759
  ]
  edge [
    source 59
    target 760
  ]
  edge [
    source 59
    target 761
  ]
  edge [
    source 59
    target 762
  ]
  edge [
    source 59
    target 763
  ]
  edge [
    source 59
    target 764
  ]
  edge [
    source 59
    target 765
  ]
  edge [
    source 59
    target 766
  ]
  edge [
    source 59
    target 589
  ]
  edge [
    source 59
    target 767
  ]
  edge [
    source 59
    target 768
  ]
  edge [
    source 59
    target 769
  ]
  edge [
    source 59
    target 770
  ]
  edge [
    source 59
    target 771
  ]
  edge [
    source 59
    target 772
  ]
  edge [
    source 59
    target 773
  ]
  edge [
    source 59
    target 774
  ]
  edge [
    source 59
    target 775
  ]
  edge [
    source 59
    target 776
  ]
  edge [
    source 59
    target 777
  ]
  edge [
    source 59
    target 778
  ]
  edge [
    source 59
    target 779
  ]
  edge [
    source 59
    target 780
  ]
  edge [
    source 59
    target 781
  ]
  edge [
    source 59
    target 782
  ]
  edge [
    source 59
    target 783
  ]
  edge [
    source 59
    target 784
  ]
  edge [
    source 59
    target 785
  ]
  edge [
    source 59
    target 786
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 787
  ]
  edge [
    source 60
    target 788
  ]
  edge [
    source 60
    target 789
  ]
  edge [
    source 60
    target 790
  ]
  edge [
    source 60
    target 791
  ]
  edge [
    source 60
    target 792
  ]
  edge [
    source 60
    target 338
  ]
  edge [
    source 60
    target 793
  ]
  edge [
    source 60
    target 794
  ]
  edge [
    source 60
    target 795
  ]
  edge [
    source 60
    target 796
  ]
  edge [
    source 60
    target 797
  ]
  edge [
    source 60
    target 798
  ]
  edge [
    source 60
    target 799
  ]
  edge [
    source 60
    target 800
  ]
  edge [
    source 60
    target 801
  ]
  edge [
    source 60
    target 802
  ]
  edge [
    source 60
    target 302
  ]
  edge [
    source 60
    target 803
  ]
  edge [
    source 60
    target 155
  ]
  edge [
    source 60
    target 804
  ]
  edge [
    source 60
    target 805
  ]
  edge [
    source 60
    target 806
  ]
  edge [
    source 60
    target 807
  ]
  edge [
    source 60
    target 808
  ]
  edge [
    source 60
    target 809
  ]
  edge [
    source 60
    target 810
  ]
  edge [
    source 60
    target 811
  ]
  edge [
    source 60
    target 812
  ]
  edge [
    source 60
    target 813
  ]
  edge [
    source 60
    target 814
  ]
  edge [
    source 60
    target 815
  ]
  edge [
    source 60
    target 364
  ]
  edge [
    source 60
    target 816
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 817
  ]
  edge [
    source 61
    target 818
  ]
  edge [
    source 61
    target 819
  ]
  edge [
    source 61
    target 86
  ]
  edge [
    source 61
    target 820
  ]
  edge [
    source 61
    target 821
  ]
  edge [
    source 61
    target 822
  ]
  edge [
    source 61
    target 823
  ]
  edge [
    source 61
    target 824
  ]
  edge [
    source 61
    target 825
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 826
  ]
  edge [
    source 62
    target 827
  ]
  edge [
    source 62
    target 611
  ]
  edge [
    source 62
    target 828
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 829
  ]
  edge [
    source 64
    target 830
  ]
  edge [
    source 64
    target 831
  ]
  edge [
    source 64
    target 832
  ]
]
