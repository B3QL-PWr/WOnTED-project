graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 1
    label "probacyjny"
    origin "text"
  ]
  node [
    id 2
    label "miejsce"
  ]
  node [
    id 3
    label "czas"
  ]
  node [
    id 4
    label "abstrakcja"
  ]
  node [
    id 5
    label "punkt"
  ]
  node [
    id 6
    label "substancja"
  ]
  node [
    id 7
    label "spos&#243;b"
  ]
  node [
    id 8
    label "chemikalia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
]
