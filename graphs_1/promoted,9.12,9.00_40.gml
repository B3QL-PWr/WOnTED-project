graph [
  maxDegree 19
  minDegree 1
  meanDegree 2
  density 0.03333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "policjantka"
    origin "text"
  ]
  node [
    id 1
    label "choszczna"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 4
    label "zatrzyma&#263;"
    origin "text"
  ]
  node [
    id 5
    label "nietrze&#378;wy"
    origin "text"
  ]
  node [
    id 6
    label "kierowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;ga&#263;"
  ]
  node [
    id 8
    label "trwa&#263;"
  ]
  node [
    id 9
    label "obecno&#347;&#263;"
  ]
  node [
    id 10
    label "stan"
  ]
  node [
    id 11
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 12
    label "stand"
  ]
  node [
    id 13
    label "mie&#263;_miejsce"
  ]
  node [
    id 14
    label "uczestniczy&#263;"
  ]
  node [
    id 15
    label "chodzi&#263;"
  ]
  node [
    id 16
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 17
    label "equal"
  ]
  node [
    id 18
    label "service"
  ]
  node [
    id 19
    label "ZOMO"
  ]
  node [
    id 20
    label "czworak"
  ]
  node [
    id 21
    label "zesp&#243;&#322;"
  ]
  node [
    id 22
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 23
    label "instytucja"
  ]
  node [
    id 24
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 25
    label "praca"
  ]
  node [
    id 26
    label "wys&#322;uga"
  ]
  node [
    id 27
    label "continue"
  ]
  node [
    id 28
    label "zabra&#263;"
  ]
  node [
    id 29
    label "zaaresztowa&#263;"
  ]
  node [
    id 30
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 31
    label "spowodowa&#263;"
  ]
  node [
    id 32
    label "przerwa&#263;"
  ]
  node [
    id 33
    label "zamkn&#261;&#263;"
  ]
  node [
    id 34
    label "bury"
  ]
  node [
    id 35
    label "komornik"
  ]
  node [
    id 36
    label "unieruchomi&#263;"
  ]
  node [
    id 37
    label "suspend"
  ]
  node [
    id 38
    label "give"
  ]
  node [
    id 39
    label "bankrupt"
  ]
  node [
    id 40
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 41
    label "zaczepi&#263;"
  ]
  node [
    id 42
    label "przechowa&#263;"
  ]
  node [
    id 43
    label "anticipate"
  ]
  node [
    id 44
    label "pijany"
  ]
  node [
    id 45
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 46
    label "control"
  ]
  node [
    id 47
    label "ustawia&#263;"
  ]
  node [
    id 48
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 49
    label "motywowa&#263;"
  ]
  node [
    id 50
    label "order"
  ]
  node [
    id 51
    label "administrowa&#263;"
  ]
  node [
    id 52
    label "manipulate"
  ]
  node [
    id 53
    label "indicate"
  ]
  node [
    id 54
    label "przeznacza&#263;"
  ]
  node [
    id 55
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 56
    label "match"
  ]
  node [
    id 57
    label "sterowa&#263;"
  ]
  node [
    id 58
    label "wysy&#322;a&#263;"
  ]
  node [
    id 59
    label "zwierzchnik"
  ]
  node [
    id 60
    label "zarz&#261;dza&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
]
