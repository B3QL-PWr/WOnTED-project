graph [
  maxDegree 37
  minDegree 1
  meanDegree 2.863849765258216
  density 0.013508725307821774
  graphCliqueNumber 9
  node [
    id 0
    label "pomimo"
    origin "text"
  ]
  node [
    id 1
    label "grozi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "niebezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 3
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "inicjator"
    origin "text"
  ]
  node [
    id 6
    label "obozowy"
    origin "text"
  ]
  node [
    id 7
    label "ruch"
    origin "text"
  ]
  node [
    id 8
    label "op&#243;r"
    origin "text"
  ]
  node [
    id 9
    label "potajemnie"
    origin "text"
  ]
  node [
    id 10
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "wieczor"
    origin "text"
  ]
  node [
    id 12
    label "patriotyczny"
    origin "text"
  ]
  node [
    id 13
    label "pomaga&#263;"
    origin "text"
  ]
  node [
    id 14
    label "prze&#380;y&#263;"
    origin "text"
  ]
  node [
    id 15
    label "s&#322;aby"
    origin "text"
  ]
  node [
    id 16
    label "wi&#281;zie&#324;"
    origin "text"
  ]
  node [
    id 17
    label "zapowiada&#263;"
  ]
  node [
    id 18
    label "boast"
  ]
  node [
    id 19
    label "hazard"
  ]
  node [
    id 20
    label "zawisa&#263;"
  ]
  node [
    id 21
    label "zawisanie"
  ]
  node [
    id 22
    label "cecha"
  ]
  node [
    id 23
    label "czarny_punkt"
  ]
  node [
    id 24
    label "zagrozi&#263;"
  ]
  node [
    id 25
    label "pozostawa&#263;"
  ]
  node [
    id 26
    label "trwa&#263;"
  ]
  node [
    id 27
    label "by&#263;"
  ]
  node [
    id 28
    label "wystarcza&#263;"
  ]
  node [
    id 29
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 30
    label "czeka&#263;"
  ]
  node [
    id 31
    label "stand"
  ]
  node [
    id 32
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 33
    label "mieszka&#263;"
  ]
  node [
    id 34
    label "wystarczy&#263;"
  ]
  node [
    id 35
    label "sprawowa&#263;"
  ]
  node [
    id 36
    label "przebywa&#263;"
  ]
  node [
    id 37
    label "kosztowa&#263;"
  ]
  node [
    id 38
    label "undertaking"
  ]
  node [
    id 39
    label "wystawa&#263;"
  ]
  node [
    id 40
    label "base"
  ]
  node [
    id 41
    label "digest"
  ]
  node [
    id 42
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 43
    label "czynnik"
  ]
  node [
    id 44
    label "motor"
  ]
  node [
    id 45
    label "manewr"
  ]
  node [
    id 46
    label "model"
  ]
  node [
    id 47
    label "movement"
  ]
  node [
    id 48
    label "apraksja"
  ]
  node [
    id 49
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 50
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 51
    label "poruszenie"
  ]
  node [
    id 52
    label "commercial_enterprise"
  ]
  node [
    id 53
    label "dyssypacja_energii"
  ]
  node [
    id 54
    label "zmiana"
  ]
  node [
    id 55
    label "utrzymanie"
  ]
  node [
    id 56
    label "utrzyma&#263;"
  ]
  node [
    id 57
    label "komunikacja"
  ]
  node [
    id 58
    label "tumult"
  ]
  node [
    id 59
    label "kr&#243;tki"
  ]
  node [
    id 60
    label "drift"
  ]
  node [
    id 61
    label "utrzymywa&#263;"
  ]
  node [
    id 62
    label "stopek"
  ]
  node [
    id 63
    label "kanciasty"
  ]
  node [
    id 64
    label "d&#322;ugi"
  ]
  node [
    id 65
    label "zjawisko"
  ]
  node [
    id 66
    label "utrzymywanie"
  ]
  node [
    id 67
    label "czynno&#347;&#263;"
  ]
  node [
    id 68
    label "myk"
  ]
  node [
    id 69
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 70
    label "wydarzenie"
  ]
  node [
    id 71
    label "taktyka"
  ]
  node [
    id 72
    label "move"
  ]
  node [
    id 73
    label "natural_process"
  ]
  node [
    id 74
    label "lokomocja"
  ]
  node [
    id 75
    label "mechanika"
  ]
  node [
    id 76
    label "proces"
  ]
  node [
    id 77
    label "strumie&#324;"
  ]
  node [
    id 78
    label "aktywno&#347;&#263;"
  ]
  node [
    id 79
    label "travel"
  ]
  node [
    id 80
    label "reakcja"
  ]
  node [
    id 81
    label "si&#322;a"
  ]
  node [
    id 82
    label "resistance"
  ]
  node [
    id 83
    label "impedancja"
  ]
  node [
    id 84
    label "reluctance"
  ]
  node [
    id 85
    label "immunity"
  ]
  node [
    id 86
    label "opory_ruchu"
  ]
  node [
    id 87
    label "niech&#281;&#263;"
  ]
  node [
    id 88
    label "w&#322;a&#347;ciwo&#347;&#263;_fizyczna"
  ]
  node [
    id 89
    label "protestacja"
  ]
  node [
    id 90
    label "czerwona_kartka"
  ]
  node [
    id 91
    label "kryjomy"
  ]
  node [
    id 92
    label "sekretnie"
  ]
  node [
    id 93
    label "potajemny"
  ]
  node [
    id 94
    label "kryjomo"
  ]
  node [
    id 95
    label "planowa&#263;"
  ]
  node [
    id 96
    label "dostosowywa&#263;"
  ]
  node [
    id 97
    label "pozyskiwa&#263;"
  ]
  node [
    id 98
    label "wprowadza&#263;"
  ]
  node [
    id 99
    label "treat"
  ]
  node [
    id 100
    label "przygotowywa&#263;"
  ]
  node [
    id 101
    label "create"
  ]
  node [
    id 102
    label "ensnare"
  ]
  node [
    id 103
    label "tworzy&#263;"
  ]
  node [
    id 104
    label "standard"
  ]
  node [
    id 105
    label "skupia&#263;"
  ]
  node [
    id 106
    label "patriotycznie"
  ]
  node [
    id 107
    label "bogoojczy&#378;niany"
  ]
  node [
    id 108
    label "wierny"
  ]
  node [
    id 109
    label "patriotic"
  ]
  node [
    id 110
    label "sprzyja&#263;"
  ]
  node [
    id 111
    label "back"
  ]
  node [
    id 112
    label "skutkowa&#263;"
  ]
  node [
    id 113
    label "mie&#263;_miejsce"
  ]
  node [
    id 114
    label "concur"
  ]
  node [
    id 115
    label "Warszawa"
  ]
  node [
    id 116
    label "robi&#263;"
  ]
  node [
    id 117
    label "aid"
  ]
  node [
    id 118
    label "u&#322;atwia&#263;"
  ]
  node [
    id 119
    label "powodowa&#263;"
  ]
  node [
    id 120
    label "wytrzyma&#263;"
  ]
  node [
    id 121
    label "przej&#347;&#263;"
  ]
  node [
    id 122
    label "poradzi&#263;_sobie"
  ]
  node [
    id 123
    label "see"
  ]
  node [
    id 124
    label "visualize"
  ]
  node [
    id 125
    label "dozna&#263;"
  ]
  node [
    id 126
    label "nieznaczny"
  ]
  node [
    id 127
    label "nieumiej&#281;tny"
  ]
  node [
    id 128
    label "marnie"
  ]
  node [
    id 129
    label "md&#322;y"
  ]
  node [
    id 130
    label "przemijaj&#261;cy"
  ]
  node [
    id 131
    label "zawodny"
  ]
  node [
    id 132
    label "delikatny"
  ]
  node [
    id 133
    label "&#322;agodny"
  ]
  node [
    id 134
    label "niedoskona&#322;y"
  ]
  node [
    id 135
    label "nietrwa&#322;y"
  ]
  node [
    id 136
    label "po&#347;ledni"
  ]
  node [
    id 137
    label "s&#322;abowity"
  ]
  node [
    id 138
    label "niefajny"
  ]
  node [
    id 139
    label "z&#322;y"
  ]
  node [
    id 140
    label "niemocny"
  ]
  node [
    id 141
    label "kiepsko"
  ]
  node [
    id 142
    label "niezdrowy"
  ]
  node [
    id 143
    label "lura"
  ]
  node [
    id 144
    label "s&#322;abo"
  ]
  node [
    id 145
    label "nieudany"
  ]
  node [
    id 146
    label "mizerny"
  ]
  node [
    id 147
    label "cz&#322;owiek"
  ]
  node [
    id 148
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 149
    label "kiciarz"
  ]
  node [
    id 150
    label "Butyrki"
  ]
  node [
    id 151
    label "ciupa"
  ]
  node [
    id 152
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 153
    label "pasiak"
  ]
  node [
    id 154
    label "reedukator"
  ]
  node [
    id 155
    label "miejsce_odosobnienia"
  ]
  node [
    id 156
    label "&#321;ubianka"
  ]
  node [
    id 157
    label "pierdel"
  ]
  node [
    id 158
    label "francuski"
  ]
  node [
    id 159
    label "czerwony"
  ]
  node [
    id 160
    label "krzy&#380;"
  ]
  node [
    id 161
    label "Wac&#322;awa"
  ]
  node [
    id 162
    label "Milkego"
  ]
  node [
    id 163
    label "zwi&#261;zek"
  ]
  node [
    id 164
    label "m&#322;odzie&#380;"
  ]
  node [
    id 165
    label "&#8222;"
  ]
  node [
    id 166
    label "Grunwald"
  ]
  node [
    id 167
    label "&#8221;"
  ]
  node [
    id 168
    label "liceum"
  ]
  node [
    id 169
    label "og&#243;lnokszta&#322;c&#261;cy"
  ]
  node [
    id 170
    label "on"
  ]
  node [
    id 171
    label "marsza&#322;ek"
  ]
  node [
    id 172
    label "Stanis&#322;awa"
  ]
  node [
    id 173
    label "Ma&#322;achowski"
  ]
  node [
    id 174
    label "harcerski"
  ]
  node [
    id 175
    label "dru&#380;yna"
  ]
  node [
    id 176
    label "artystyczny"
  ]
  node [
    id 177
    label "zesp&#243;&#322;"
  ]
  node [
    id 178
    label "pie&#347;&#324;"
  ]
  node [
    id 179
    label "i"
  ]
  node [
    id 180
    label "taniec"
  ]
  node [
    id 181
    label "dziecko"
  ]
  node [
    id 182
    label "p&#322;ocki"
  ]
  node [
    id 183
    label "&#347;wiatowy"
  ]
  node [
    id 184
    label "festiwal"
  ]
  node [
    id 185
    label "student"
  ]
  node [
    id 186
    label "Milke"
  ]
  node [
    id 187
    label "wielki"
  ]
  node [
    id 188
    label "order"
  ]
  node [
    id 189
    label "odrodzi&#263;"
  ]
  node [
    id 190
    label "polski"
  ]
  node [
    id 191
    label "zas&#322;u&#380;y&#263;"
  ]
  node [
    id 192
    label "dla"
  ]
  node [
    id 193
    label "kultura"
  ]
  node [
    id 194
    label "za"
  ]
  node [
    id 195
    label "zas&#322;uga"
  ]
  node [
    id 196
    label "o&#347;wiata"
  ]
  node [
    id 197
    label "wychowa&#263;"
  ]
  node [
    id 198
    label "komandorski"
  ]
  node [
    id 199
    label "zeszyt"
  ]
  node [
    id 200
    label "gwiazda"
  ]
  node [
    id 201
    label "obronno&#347;&#263;"
  ]
  node [
    id 202
    label "kraj"
  ]
  node [
    id 203
    label "gloria"
  ]
  node [
    id 204
    label "Artis"
  ]
  node [
    id 205
    label "medal"
  ]
  node [
    id 206
    label "komisja"
  ]
  node [
    id 207
    label "edukacja"
  ]
  node [
    id 208
    label "narodowy"
  ]
  node [
    id 209
    label "z&#322;oty"
  ]
  node [
    id 210
    label "towarzystwo"
  ]
  node [
    id 211
    label "przyjaciel"
  ]
  node [
    id 212
    label "u&#347;miech"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 41
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 158
    target 160
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 160
    target 187
  ]
  edge [
    source 160
    target 188
  ]
  edge [
    source 160
    target 189
  ]
  edge [
    source 160
    target 190
  ]
  edge [
    source 160
    target 198
  ]
  edge [
    source 160
    target 199
  ]
  edge [
    source 160
    target 200
  ]
  edge [
    source 160
    target 209
  ]
  edge [
    source 160
    target 195
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 161
    target 186
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 163
    target 165
  ]
  edge [
    source 163
    target 166
  ]
  edge [
    source 163
    target 167
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 164
    target 166
  ]
  edge [
    source 164
    target 167
  ]
  edge [
    source 164
    target 183
  ]
  edge [
    source 164
    target 184
  ]
  edge [
    source 164
    target 179
  ]
  edge [
    source 164
    target 185
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 165
    target 167
  ]
  edge [
    source 165
    target 174
  ]
  edge [
    source 165
    target 177
  ]
  edge [
    source 165
    target 178
  ]
  edge [
    source 165
    target 179
  ]
  edge [
    source 165
    target 180
  ]
  edge [
    source 165
    target 181
  ]
  edge [
    source 165
    target 182
  ]
  edge [
    source 166
    target 167
  ]
  edge [
    source 167
    target 174
  ]
  edge [
    source 167
    target 177
  ]
  edge [
    source 167
    target 178
  ]
  edge [
    source 167
    target 179
  ]
  edge [
    source 167
    target 180
  ]
  edge [
    source 167
    target 181
  ]
  edge [
    source 167
    target 182
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 168
    target 170
  ]
  edge [
    source 168
    target 171
  ]
  edge [
    source 168
    target 172
  ]
  edge [
    source 168
    target 173
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 169
    target 171
  ]
  edge [
    source 169
    target 172
  ]
  edge [
    source 169
    target 173
  ]
  edge [
    source 170
    target 171
  ]
  edge [
    source 170
    target 172
  ]
  edge [
    source 170
    target 173
  ]
  edge [
    source 171
    target 172
  ]
  edge [
    source 171
    target 173
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 174
    target 175
  ]
  edge [
    source 174
    target 176
  ]
  edge [
    source 174
    target 177
  ]
  edge [
    source 174
    target 178
  ]
  edge [
    source 174
    target 179
  ]
  edge [
    source 174
    target 180
  ]
  edge [
    source 174
    target 181
  ]
  edge [
    source 174
    target 182
  ]
  edge [
    source 175
    target 176
  ]
  edge [
    source 177
    target 178
  ]
  edge [
    source 177
    target 179
  ]
  edge [
    source 177
    target 180
  ]
  edge [
    source 177
    target 181
  ]
  edge [
    source 177
    target 182
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 178
    target 180
  ]
  edge [
    source 178
    target 181
  ]
  edge [
    source 178
    target 182
  ]
  edge [
    source 179
    target 180
  ]
  edge [
    source 179
    target 181
  ]
  edge [
    source 179
    target 182
  ]
  edge [
    source 179
    target 183
  ]
  edge [
    source 179
    target 184
  ]
  edge [
    source 179
    target 185
  ]
  edge [
    source 179
    target 194
  ]
  edge [
    source 179
    target 195
  ]
  edge [
    source 179
    target 192
  ]
  edge [
    source 179
    target 196
  ]
  edge [
    source 179
    target 197
  ]
  edge [
    source 180
    target 181
  ]
  edge [
    source 180
    target 182
  ]
  edge [
    source 181
    target 182
  ]
  edge [
    source 181
    target 210
  ]
  edge [
    source 181
    target 211
  ]
  edge [
    source 183
    target 184
  ]
  edge [
    source 183
    target 185
  ]
  edge [
    source 184
    target 185
  ]
  edge [
    source 187
    target 188
  ]
  edge [
    source 187
    target 189
  ]
  edge [
    source 187
    target 190
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 188
    target 190
  ]
  edge [
    source 188
    target 198
  ]
  edge [
    source 188
    target 199
  ]
  edge [
    source 188
    target 200
  ]
  edge [
    source 188
    target 212
  ]
  edge [
    source 189
    target 190
  ]
  edge [
    source 189
    target 198
  ]
  edge [
    source 189
    target 199
  ]
  edge [
    source 189
    target 200
  ]
  edge [
    source 190
    target 191
  ]
  edge [
    source 190
    target 192
  ]
  edge [
    source 190
    target 193
  ]
  edge [
    source 190
    target 198
  ]
  edge [
    source 190
    target 199
  ]
  edge [
    source 190
    target 200
  ]
  edge [
    source 191
    target 192
  ]
  edge [
    source 191
    target 193
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 192
    target 194
  ]
  edge [
    source 192
    target 195
  ]
  edge [
    source 192
    target 196
  ]
  edge [
    source 192
    target 197
  ]
  edge [
    source 192
    target 201
  ]
  edge [
    source 192
    target 202
  ]
  edge [
    source 194
    target 195
  ]
  edge [
    source 194
    target 196
  ]
  edge [
    source 194
    target 197
  ]
  edge [
    source 194
    target 201
  ]
  edge [
    source 194
    target 202
  ]
  edge [
    source 195
    target 196
  ]
  edge [
    source 195
    target 197
  ]
  edge [
    source 195
    target 201
  ]
  edge [
    source 195
    target 202
  ]
  edge [
    source 195
    target 209
  ]
  edge [
    source 196
    target 197
  ]
  edge [
    source 198
    target 199
  ]
  edge [
    source 198
    target 200
  ]
  edge [
    source 199
    target 200
  ]
  edge [
    source 201
    target 202
  ]
  edge [
    source 203
    target 204
  ]
  edge [
    source 205
    target 206
  ]
  edge [
    source 205
    target 207
  ]
  edge [
    source 205
    target 208
  ]
  edge [
    source 206
    target 207
  ]
  edge [
    source 206
    target 208
  ]
  edge [
    source 207
    target 208
  ]
  edge [
    source 210
    target 211
  ]
]
