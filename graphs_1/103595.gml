graph [
  maxDegree 28
  minDegree 1
  meanDegree 2
  density 0.028169014084507043
  graphCliqueNumber 2
  node [
    id 0
    label "gdyby"
    origin "text"
  ]
  node [
    id 1
    label "droga"
    origin "text"
  ]
  node [
    id 2
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "prosta"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "nudno"
    origin "text"
  ]
  node [
    id 6
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "tomasz"
    origin "text"
  ]
  node [
    id 8
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 9
    label "journey"
  ]
  node [
    id 10
    label "podbieg"
  ]
  node [
    id 11
    label "bezsilnikowy"
  ]
  node [
    id 12
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 13
    label "wylot"
  ]
  node [
    id 14
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 15
    label "drogowskaz"
  ]
  node [
    id 16
    label "nawierzchnia"
  ]
  node [
    id 17
    label "turystyka"
  ]
  node [
    id 18
    label "budowla"
  ]
  node [
    id 19
    label "spos&#243;b"
  ]
  node [
    id 20
    label "passage"
  ]
  node [
    id 21
    label "marszrutyzacja"
  ]
  node [
    id 22
    label "zbior&#243;wka"
  ]
  node [
    id 23
    label "rajza"
  ]
  node [
    id 24
    label "ekskursja"
  ]
  node [
    id 25
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 26
    label "ruch"
  ]
  node [
    id 27
    label "trasa"
  ]
  node [
    id 28
    label "wyb&#243;j"
  ]
  node [
    id 29
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 30
    label "ekwipunek"
  ]
  node [
    id 31
    label "korona_drogi"
  ]
  node [
    id 32
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 33
    label "pobocze"
  ]
  node [
    id 34
    label "partnerka"
  ]
  node [
    id 35
    label "czas"
  ]
  node [
    id 36
    label "odcinek"
  ]
  node [
    id 37
    label "proste_sko&#347;ne"
  ]
  node [
    id 38
    label "straight_line"
  ]
  node [
    id 39
    label "punkt"
  ]
  node [
    id 40
    label "krzywa"
  ]
  node [
    id 41
    label "si&#281;ga&#263;"
  ]
  node [
    id 42
    label "trwa&#263;"
  ]
  node [
    id 43
    label "obecno&#347;&#263;"
  ]
  node [
    id 44
    label "stan"
  ]
  node [
    id 45
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 46
    label "stand"
  ]
  node [
    id 47
    label "mie&#263;_miejsce"
  ]
  node [
    id 48
    label "uczestniczy&#263;"
  ]
  node [
    id 49
    label "chodzi&#263;"
  ]
  node [
    id 50
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 51
    label "equal"
  ]
  node [
    id 52
    label "zwyczajnie"
  ]
  node [
    id 53
    label "nijaki"
  ]
  node [
    id 54
    label "nudny"
  ]
  node [
    id 55
    label "blandly"
  ]
  node [
    id 56
    label "boringly"
  ]
  node [
    id 57
    label "nieefektownie"
  ]
  node [
    id 58
    label "nieciekawie"
  ]
  node [
    id 59
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 60
    label "express"
  ]
  node [
    id 61
    label "rzekn&#261;&#263;"
  ]
  node [
    id 62
    label "okre&#347;li&#263;"
  ]
  node [
    id 63
    label "wyrazi&#263;"
  ]
  node [
    id 64
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 65
    label "unwrap"
  ]
  node [
    id 66
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 67
    label "convey"
  ]
  node [
    id 68
    label "discover"
  ]
  node [
    id 69
    label "wydoby&#263;"
  ]
  node [
    id 70
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 71
    label "poda&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
]
