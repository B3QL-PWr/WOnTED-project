graph [
  maxDegree 3
  minDegree 1
  meanDegree 2.25
  density 0.32142857142857145
  graphCliqueNumber 4
  node [
    id 0
    label "mirka"
    origin "text"
  ]
  node [
    id 1
    label "mirabelka"
    origin "text"
  ]
  node [
    id 2
    label "&#347;liwka"
  ]
  node [
    id 3
    label "&#347;liwa_domowa"
  ]
  node [
    id 4
    label "zielonka"
  ]
  node [
    id 5
    label "oczywi&#347;cie"
  ]
  node [
    id 6
    label "bra&#263;"
  ]
  node [
    id 7
    label "udzia&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
]
