graph [
  maxDegree 23
  minDegree 1
  meanDegree 2
  density 0.02666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "codziennie"
    origin "text"
  ]
  node [
    id 1
    label "tr&#261;ba"
    origin "text"
  ]
  node [
    id 2
    label "rowerzysta"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "jecha&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ulica"
    origin "text"
  ]
  node [
    id 6
    label "mimo"
    origin "text"
  ]
  node [
    id 7
    label "obok"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 9
    label "&#347;cie&#380;ka"
    origin "text"
  ]
  node [
    id 10
    label "rowerowy"
    origin "text"
  ]
  node [
    id 11
    label "daily"
  ]
  node [
    id 12
    label "cz&#281;sto"
  ]
  node [
    id 13
    label "codzienny"
  ]
  node [
    id 14
    label "prozaicznie"
  ]
  node [
    id 15
    label "stale"
  ]
  node [
    id 16
    label "regularnie"
  ]
  node [
    id 17
    label "pospolicie"
  ]
  node [
    id 18
    label "proboscis"
  ]
  node [
    id 19
    label "grubas"
  ]
  node [
    id 20
    label "wa&#322;"
  ]
  node [
    id 21
    label "niezgrabny"
  ]
  node [
    id 22
    label "organ"
  ]
  node [
    id 23
    label "oferma"
  ]
  node [
    id 24
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 25
    label "g&#322;upiec"
  ]
  node [
    id 26
    label "pokraka"
  ]
  node [
    id 27
    label "kierowca"
  ]
  node [
    id 28
    label "rider"
  ]
  node [
    id 29
    label "peda&#322;owicz"
  ]
  node [
    id 30
    label "proceed"
  ]
  node [
    id 31
    label "napada&#263;"
  ]
  node [
    id 32
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 33
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 34
    label "wykonywa&#263;"
  ]
  node [
    id 35
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 36
    label "czu&#263;"
  ]
  node [
    id 37
    label "overdrive"
  ]
  node [
    id 38
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 39
    label "ride"
  ]
  node [
    id 40
    label "korzysta&#263;"
  ]
  node [
    id 41
    label "go"
  ]
  node [
    id 42
    label "prowadzi&#263;"
  ]
  node [
    id 43
    label "continue"
  ]
  node [
    id 44
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 45
    label "drive"
  ]
  node [
    id 46
    label "kontynuowa&#263;"
  ]
  node [
    id 47
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 48
    label "odbywa&#263;"
  ]
  node [
    id 49
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 50
    label "carry"
  ]
  node [
    id 51
    label "&#347;rodowisko"
  ]
  node [
    id 52
    label "miasteczko"
  ]
  node [
    id 53
    label "streetball"
  ]
  node [
    id 54
    label "pierzeja"
  ]
  node [
    id 55
    label "grupa"
  ]
  node [
    id 56
    label "pas_ruchu"
  ]
  node [
    id 57
    label "pas_rozdzielczy"
  ]
  node [
    id 58
    label "jezdnia"
  ]
  node [
    id 59
    label "droga"
  ]
  node [
    id 60
    label "korona_drogi"
  ]
  node [
    id 61
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 62
    label "chodnik"
  ]
  node [
    id 63
    label "arteria"
  ]
  node [
    id 64
    label "Broadway"
  ]
  node [
    id 65
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 66
    label "wysepka"
  ]
  node [
    id 67
    label "autostrada"
  ]
  node [
    id 68
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 69
    label "bliski"
  ]
  node [
    id 70
    label "czyj&#347;"
  ]
  node [
    id 71
    label "m&#261;&#380;"
  ]
  node [
    id 72
    label "chody"
  ]
  node [
    id 73
    label "&#347;cie&#380;a"
  ]
  node [
    id 74
    label "teoria_graf&#243;w"
  ]
  node [
    id 75
    label "ta&#347;ma"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 75
  ]
]
