graph [
  maxDegree 10
  minDegree 1
  meanDegree 2
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "link"
    origin "text"
  ]
  node [
    id 2
    label "ksw"
    origin "text"
  ]
  node [
    id 3
    label "wysy&#322;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "priv"
    origin "text"
  ]
  node [
    id 5
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 6
    label "kto"
    origin "text"
  ]
  node [
    id 7
    label "plusuje"
    origin "text"
  ]
  node [
    id 8
    label "matczysko"
  ]
  node [
    id 9
    label "macierz"
  ]
  node [
    id 10
    label "przodkini"
  ]
  node [
    id 11
    label "Matka_Boska"
  ]
  node [
    id 12
    label "macocha"
  ]
  node [
    id 13
    label "matka_zast&#281;pcza"
  ]
  node [
    id 14
    label "stara"
  ]
  node [
    id 15
    label "rodzice"
  ]
  node [
    id 16
    label "rodzic"
  ]
  node [
    id 17
    label "buton"
  ]
  node [
    id 18
    label "odsy&#322;acz"
  ]
  node [
    id 19
    label "nakazywa&#263;"
  ]
  node [
    id 20
    label "order"
  ]
  node [
    id 21
    label "dispatch"
  ]
  node [
    id 22
    label "grant"
  ]
  node [
    id 23
    label "wytwarza&#263;"
  ]
  node [
    id 24
    label "przekazywa&#263;"
  ]
  node [
    id 25
    label "jaki&#347;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 7
  ]
]
