graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "bekazlewactwa"
    origin "text"
  ]
  node [
    id 1
    label "xddd"
    origin "text"
  ]
  node [
    id 2
    label "podprogowy"
    origin "text"
  ]
  node [
    id 3
    label "przekaz"
    origin "text"
  ]
  node [
    id 4
    label "podprogowo"
  ]
  node [
    id 5
    label "pod&#347;wiadomy"
  ]
  node [
    id 6
    label "dokument"
  ]
  node [
    id 7
    label "znaczenie"
  ]
  node [
    id 8
    label "implicite"
  ]
  node [
    id 9
    label "transakcja"
  ]
  node [
    id 10
    label "order"
  ]
  node [
    id 11
    label "explicite"
  ]
  node [
    id 12
    label "draft"
  ]
  node [
    id 13
    label "proces"
  ]
  node [
    id 14
    label "kwota"
  ]
  node [
    id 15
    label "blankiet"
  ]
  node [
    id 16
    label "tekst"
  ]
  node [
    id 17
    label "XDDD"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
]
