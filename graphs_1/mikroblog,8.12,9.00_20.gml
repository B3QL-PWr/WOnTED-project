graph [
  maxDegree 30
  minDegree 1
  meanDegree 1.9583333333333333
  density 0.041666666666666664
  graphCliqueNumber 2
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rodzina"
    origin "text"
  ]
  node [
    id 4
    label "przy"
    origin "text"
  ]
  node [
    id 5
    label "stola"
    origin "text"
  ]
  node [
    id 6
    label "wigilijny"
    origin "text"
  ]
  node [
    id 7
    label "dok&#322;adnie"
  ]
  node [
    id 8
    label "si&#281;ga&#263;"
  ]
  node [
    id 9
    label "trwa&#263;"
  ]
  node [
    id 10
    label "obecno&#347;&#263;"
  ]
  node [
    id 11
    label "stan"
  ]
  node [
    id 12
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "stand"
  ]
  node [
    id 14
    label "mie&#263;_miejsce"
  ]
  node [
    id 15
    label "uczestniczy&#263;"
  ]
  node [
    id 16
    label "chodzi&#263;"
  ]
  node [
    id 17
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 18
    label "equal"
  ]
  node [
    id 19
    label "krewni"
  ]
  node [
    id 20
    label "Firlejowie"
  ]
  node [
    id 21
    label "Ossoli&#324;scy"
  ]
  node [
    id 22
    label "grupa"
  ]
  node [
    id 23
    label "rodze&#324;stwo"
  ]
  node [
    id 24
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 25
    label "rz&#261;d"
  ]
  node [
    id 26
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 27
    label "przyjaciel_domu"
  ]
  node [
    id 28
    label "Ostrogscy"
  ]
  node [
    id 29
    label "theater"
  ]
  node [
    id 30
    label "dom_rodzinny"
  ]
  node [
    id 31
    label "potomstwo"
  ]
  node [
    id 32
    label "Soplicowie"
  ]
  node [
    id 33
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 34
    label "Czartoryscy"
  ]
  node [
    id 35
    label "family"
  ]
  node [
    id 36
    label "kin"
  ]
  node [
    id 37
    label "bliscy"
  ]
  node [
    id 38
    label "powinowaci"
  ]
  node [
    id 39
    label "Sapiehowie"
  ]
  node [
    id 40
    label "ordynacja"
  ]
  node [
    id 41
    label "jednostka_systematyczna"
  ]
  node [
    id 42
    label "zbi&#243;r"
  ]
  node [
    id 43
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 44
    label "Kossakowie"
  ]
  node [
    id 45
    label "rodzice"
  ]
  node [
    id 46
    label "dom"
  ]
  node [
    id 47
    label "sztolnia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
]
