graph [
  maxDegree 13
  minDegree 1
  meanDegree 2
  density 0.05405405405405406
  graphCliqueNumber 3
  node [
    id 0
    label "wczoraj"
    origin "text"
  ]
  node [
    id 1
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 2
    label "impreza"
    origin "text"
  ]
  node [
    id 3
    label "firmowy"
    origin "text"
  ]
  node [
    id 4
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 5
    label "trzeba"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "doba"
  ]
  node [
    id 8
    label "dawno"
  ]
  node [
    id 9
    label "niedawno"
  ]
  node [
    id 10
    label "proszek"
  ]
  node [
    id 11
    label "party"
  ]
  node [
    id 12
    label "rozrywka"
  ]
  node [
    id 13
    label "przyj&#281;cie"
  ]
  node [
    id 14
    label "okazja"
  ]
  node [
    id 15
    label "impra"
  ]
  node [
    id 16
    label "markowy"
  ]
  node [
    id 17
    label "oryginalny"
  ]
  node [
    id 18
    label "firmowo"
  ]
  node [
    id 19
    label "czeka&#263;"
  ]
  node [
    id 20
    label "lookout"
  ]
  node [
    id 21
    label "wyziera&#263;"
  ]
  node [
    id 22
    label "peep"
  ]
  node [
    id 23
    label "look"
  ]
  node [
    id 24
    label "patrze&#263;"
  ]
  node [
    id 25
    label "trza"
  ]
  node [
    id 26
    label "necessity"
  ]
  node [
    id 27
    label "si&#281;ga&#263;"
  ]
  node [
    id 28
    label "trwa&#263;"
  ]
  node [
    id 29
    label "obecno&#347;&#263;"
  ]
  node [
    id 30
    label "stan"
  ]
  node [
    id 31
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "stand"
  ]
  node [
    id 33
    label "mie&#263;_miejsce"
  ]
  node [
    id 34
    label "uczestniczy&#263;"
  ]
  node [
    id 35
    label "chodzi&#263;"
  ]
  node [
    id 36
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 37
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
]
