graph [
  maxDegree 33
  minDegree 1
  meanDegree 2
  density 0.018691588785046728
  graphCliqueNumber 2
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wysy&#322;a&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nosacz"
    origin "text"
  ]
  node [
    id 3
    label "zanim"
    origin "text"
  ]
  node [
    id 4
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 5
    label "modny"
    origin "text"
  ]
  node [
    id 6
    label "fejsbuk"
    origin "text"
  ]
  node [
    id 7
    label "poza"
    origin "text"
  ]
  node [
    id 8
    label "wykop"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "pyta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "chuj"
    origin "text"
  ]
  node [
    id 12
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "ma&#322;pa"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;ga&#263;"
  ]
  node [
    id 15
    label "zna&#263;"
  ]
  node [
    id 16
    label "troska&#263;_si&#281;"
  ]
  node [
    id 17
    label "zachowywa&#263;"
  ]
  node [
    id 18
    label "chowa&#263;"
  ]
  node [
    id 19
    label "think"
  ]
  node [
    id 20
    label "pilnowa&#263;"
  ]
  node [
    id 21
    label "robi&#263;"
  ]
  node [
    id 22
    label "recall"
  ]
  node [
    id 23
    label "echo"
  ]
  node [
    id 24
    label "take_care"
  ]
  node [
    id 25
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 26
    label "gerezy"
  ]
  node [
    id 27
    label "ma&#322;pa_ogoniasta"
  ]
  node [
    id 28
    label "punkt"
  ]
  node [
    id 29
    label "nadnosie"
  ]
  node [
    id 30
    label "dawny"
  ]
  node [
    id 31
    label "rozw&#243;d"
  ]
  node [
    id 32
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 33
    label "eksprezydent"
  ]
  node [
    id 34
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 35
    label "partner"
  ]
  node [
    id 36
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 37
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 38
    label "wcze&#347;niejszy"
  ]
  node [
    id 39
    label "dobry"
  ]
  node [
    id 40
    label "popularny"
  ]
  node [
    id 41
    label "modnie"
  ]
  node [
    id 42
    label "mode"
  ]
  node [
    id 43
    label "gra"
  ]
  node [
    id 44
    label "przesada"
  ]
  node [
    id 45
    label "ustawienie"
  ]
  node [
    id 46
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 47
    label "odwa&#322;"
  ]
  node [
    id 48
    label "chody"
  ]
  node [
    id 49
    label "grodzisko"
  ]
  node [
    id 50
    label "budowa"
  ]
  node [
    id 51
    label "kopniak"
  ]
  node [
    id 52
    label "wyrobisko"
  ]
  node [
    id 53
    label "zrzutowy"
  ]
  node [
    id 54
    label "szaniec"
  ]
  node [
    id 55
    label "odk&#322;ad"
  ]
  node [
    id 56
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 57
    label "inspect"
  ]
  node [
    id 58
    label "przes&#322;uchiwa&#263;"
  ]
  node [
    id 59
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 60
    label "ankieter"
  ]
  node [
    id 61
    label "sprawdza&#263;"
  ]
  node [
    id 62
    label "question"
  ]
  node [
    id 63
    label "dupek"
  ]
  node [
    id 64
    label "skurwysyn"
  ]
  node [
    id 65
    label "wyzwisko"
  ]
  node [
    id 66
    label "penis"
  ]
  node [
    id 67
    label "ciul"
  ]
  node [
    id 68
    label "przekle&#324;stwo"
  ]
  node [
    id 69
    label "proceed"
  ]
  node [
    id 70
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 71
    label "bangla&#263;"
  ]
  node [
    id 72
    label "by&#263;"
  ]
  node [
    id 73
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 74
    label "run"
  ]
  node [
    id 75
    label "tryb"
  ]
  node [
    id 76
    label "p&#322;ywa&#263;"
  ]
  node [
    id 77
    label "continue"
  ]
  node [
    id 78
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 79
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 80
    label "przebiega&#263;"
  ]
  node [
    id 81
    label "mie&#263;_miejsce"
  ]
  node [
    id 82
    label "wk&#322;ada&#263;"
  ]
  node [
    id 83
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 84
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 85
    label "para"
  ]
  node [
    id 86
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 87
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 88
    label "krok"
  ]
  node [
    id 89
    label "str&#243;j"
  ]
  node [
    id 90
    label "bywa&#263;"
  ]
  node [
    id 91
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 92
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 93
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 94
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 95
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 96
    label "dziama&#263;"
  ]
  node [
    id 97
    label "stara&#263;_si&#281;"
  ]
  node [
    id 98
    label "carry"
  ]
  node [
    id 99
    label "cz&#322;owiek"
  ]
  node [
    id 100
    label "monkey"
  ]
  node [
    id 101
    label "istota_&#380;ywa"
  ]
  node [
    id 102
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 103
    label "dokucza&#263;"
  ]
  node [
    id 104
    label "humanoid"
  ]
  node [
    id 105
    label "z&#322;o&#347;liwy"
  ]
  node [
    id 106
    label "prostytutka"
  ]
  node [
    id 107
    label "naczelne"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 46
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
]
