graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9692307692307693
  density 0.03076923076923077
  graphCliqueNumber 2
  node [
    id 0
    label "dawny"
    origin "text"
  ]
  node [
    id 1
    label "wymagaj&#261;ca"
    origin "text"
  ]
  node [
    id 2
    label "ustalenie"
    origin "text"
  ]
  node [
    id 3
    label "strefa"
    origin "text"
  ]
  node [
    id 4
    label "ochrona"
    origin "text"
  ]
  node [
    id 5
    label "ostoja"
    origin "text"
  ]
  node [
    id 6
    label "lub"
    origin "text"
  ]
  node [
    id 7
    label "stanowisko"
    origin "text"
  ]
  node [
    id 8
    label "przesz&#322;y"
  ]
  node [
    id 9
    label "dawno"
  ]
  node [
    id 10
    label "dawniej"
  ]
  node [
    id 11
    label "kombatant"
  ]
  node [
    id 12
    label "stary"
  ]
  node [
    id 13
    label "odleg&#322;y"
  ]
  node [
    id 14
    label "anachroniczny"
  ]
  node [
    id 15
    label "przestarza&#322;y"
  ]
  node [
    id 16
    label "od_dawna"
  ]
  node [
    id 17
    label "poprzedni"
  ]
  node [
    id 18
    label "d&#322;ugoletni"
  ]
  node [
    id 19
    label "wcze&#347;niejszy"
  ]
  node [
    id 20
    label "niegdysiejszy"
  ]
  node [
    id 21
    label "zrobienie"
  ]
  node [
    id 22
    label "zdecydowanie"
  ]
  node [
    id 23
    label "czynno&#347;&#263;"
  ]
  node [
    id 24
    label "spowodowanie"
  ]
  node [
    id 25
    label "appointment"
  ]
  node [
    id 26
    label "localization"
  ]
  node [
    id 27
    label "informacja"
  ]
  node [
    id 28
    label "decyzja"
  ]
  node [
    id 29
    label "umocnienie"
  ]
  node [
    id 30
    label "obszar"
  ]
  node [
    id 31
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 32
    label "obrona_strefowa"
  ]
  node [
    id 33
    label "chemical_bond"
  ]
  node [
    id 34
    label "tarcza"
  ]
  node [
    id 35
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 36
    label "obiekt"
  ]
  node [
    id 37
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 38
    label "borowiec"
  ]
  node [
    id 39
    label "obstawienie"
  ]
  node [
    id 40
    label "formacja"
  ]
  node [
    id 41
    label "ubezpieczenie"
  ]
  node [
    id 42
    label "obstawia&#263;"
  ]
  node [
    id 43
    label "obstawianie"
  ]
  node [
    id 44
    label "transportacja"
  ]
  node [
    id 45
    label "rama"
  ]
  node [
    id 46
    label "miejsce"
  ]
  node [
    id 47
    label "pojazd_szynowy"
  ]
  node [
    id 48
    label "podstawa"
  ]
  node [
    id 49
    label "siedlisko"
  ]
  node [
    id 50
    label "podpora"
  ]
  node [
    id 51
    label "postawi&#263;"
  ]
  node [
    id 52
    label "awansowanie"
  ]
  node [
    id 53
    label "po&#322;o&#380;enie"
  ]
  node [
    id 54
    label "awansowa&#263;"
  ]
  node [
    id 55
    label "uprawianie"
  ]
  node [
    id 56
    label "powierzanie"
  ]
  node [
    id 57
    label "punkt"
  ]
  node [
    id 58
    label "pogl&#261;d"
  ]
  node [
    id 59
    label "wojsko"
  ]
  node [
    id 60
    label "praca"
  ]
  node [
    id 61
    label "wakowa&#263;"
  ]
  node [
    id 62
    label "stawia&#263;"
  ]
  node [
    id 63
    label "unia"
  ]
  node [
    id 64
    label "europejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 63
    target 64
  ]
]
