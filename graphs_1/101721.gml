graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9375
  density 0.0625
  graphCliqueNumber 2
  node [
    id 0
    label "swobodny"
    origin "text"
  ]
  node [
    id 1
    label "przep&#322;yw"
    origin "text"
  ]
  node [
    id 2
    label "kapita&#322;"
    origin "text"
  ]
  node [
    id 3
    label "niezale&#380;ny"
  ]
  node [
    id 4
    label "swobodnie"
  ]
  node [
    id 5
    label "naturalny"
  ]
  node [
    id 6
    label "bezpruderyjny"
  ]
  node [
    id 7
    label "wolnie"
  ]
  node [
    id 8
    label "wygodny"
  ]
  node [
    id 9
    label "dowolny"
  ]
  node [
    id 10
    label "obieg"
  ]
  node [
    id 11
    label "ruch"
  ]
  node [
    id 12
    label "flux"
  ]
  node [
    id 13
    label "uruchomienie"
  ]
  node [
    id 14
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 15
    label "&#347;rodowisko"
  ]
  node [
    id 16
    label "absolutorium"
  ]
  node [
    id 17
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 18
    label "podupada&#263;"
  ]
  node [
    id 19
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 20
    label "supernadz&#243;r"
  ]
  node [
    id 21
    label "nap&#322;ywanie"
  ]
  node [
    id 22
    label "kapitalista"
  ]
  node [
    id 23
    label "podupadanie"
  ]
  node [
    id 24
    label "uruchamia&#263;"
  ]
  node [
    id 25
    label "kwestor"
  ]
  node [
    id 26
    label "mienie"
  ]
  node [
    id 27
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 28
    label "uruchamianie"
  ]
  node [
    id 29
    label "czynnik_produkcji"
  ]
  node [
    id 30
    label "zaleta"
  ]
  node [
    id 31
    label "zas&#243;b"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
]
