graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.010152284263959
  density 0.01025587900134673
  graphCliqueNumber 3
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "serdecznie"
    origin "text"
  ]
  node [
    id 2
    label "gala"
    origin "text"
  ]
  node [
    id 3
    label "pi&#281;tnasta"
    origin "text"
  ]
  node [
    id 4
    label "edycja"
    origin "text"
  ]
  node [
    id 5
    label "mama"
    origin "text"
  ]
  node [
    id 6
    label "forum"
    origin "text"
  ]
  node [
    id 7
    label "pismak"
    origin "text"
  ]
  node [
    id 8
    label "og&#243;lnopolski"
    origin "text"
  ]
  node [
    id 9
    label "konkurs"
    origin "text"
  ]
  node [
    id 10
    label "dla"
    origin "text"
  ]
  node [
    id 11
    label "redakcja"
    origin "text"
  ]
  node [
    id 12
    label "gazetka"
    origin "text"
  ]
  node [
    id 13
    label "szkolny"
    origin "text"
  ]
  node [
    id 14
    label "pod"
    origin "text"
  ]
  node [
    id 15
    label "patronat"
    origin "text"
  ]
  node [
    id 16
    label "pan"
    origin "text"
  ]
  node [
    id 17
    label "katarzyna"
    origin "text"
  ]
  node [
    id 18
    label "hall"
    origin "text"
  ]
  node [
    id 19
    label "minister"
    origin "text"
  ]
  node [
    id 20
    label "edukacja"
    origin "text"
  ]
  node [
    id 21
    label "narodowy"
    origin "text"
  ]
  node [
    id 22
    label "nowa"
    origin "text"
  ]
  node [
    id 23
    label "strona"
    origin "text"
  ]
  node [
    id 24
    label "fundacja"
    origin "text"
  ]
  node [
    id 25
    label "invite"
  ]
  node [
    id 26
    label "ask"
  ]
  node [
    id 27
    label "oferowa&#263;"
  ]
  node [
    id 28
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 29
    label "siarczy&#347;cie"
  ]
  node [
    id 30
    label "serdeczny"
  ]
  node [
    id 31
    label "szczerze"
  ]
  node [
    id 32
    label "mi&#322;o"
  ]
  node [
    id 33
    label "jab&#322;ko"
  ]
  node [
    id 34
    label "ceremonia"
  ]
  node [
    id 35
    label "jab&#322;o&#324;_domowa"
  ]
  node [
    id 36
    label "godzina"
  ]
  node [
    id 37
    label "impression"
  ]
  node [
    id 38
    label "odmiana"
  ]
  node [
    id 39
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 40
    label "notification"
  ]
  node [
    id 41
    label "cykl"
  ]
  node [
    id 42
    label "zmiana"
  ]
  node [
    id 43
    label "produkcja"
  ]
  node [
    id 44
    label "egzemplarz"
  ]
  node [
    id 45
    label "matczysko"
  ]
  node [
    id 46
    label "macierz"
  ]
  node [
    id 47
    label "przodkini"
  ]
  node [
    id 48
    label "Matka_Boska"
  ]
  node [
    id 49
    label "macocha"
  ]
  node [
    id 50
    label "matka_zast&#281;pcza"
  ]
  node [
    id 51
    label "stara"
  ]
  node [
    id 52
    label "rodzice"
  ]
  node [
    id 53
    label "rodzic"
  ]
  node [
    id 54
    label "s&#261;d"
  ]
  node [
    id 55
    label "plac"
  ]
  node [
    id 56
    label "miejsce"
  ]
  node [
    id 57
    label "grupa_dyskusyjna"
  ]
  node [
    id 58
    label "grupa"
  ]
  node [
    id 59
    label "przestrze&#324;"
  ]
  node [
    id 60
    label "agora"
  ]
  node [
    id 61
    label "bazylika"
  ]
  node [
    id 62
    label "konferencja"
  ]
  node [
    id 63
    label "portal"
  ]
  node [
    id 64
    label "gryzipi&#243;rek"
  ]
  node [
    id 65
    label "og&#243;lnopolsko"
  ]
  node [
    id 66
    label "og&#243;lnokrajowy"
  ]
  node [
    id 67
    label "eliminacje"
  ]
  node [
    id 68
    label "Interwizja"
  ]
  node [
    id 69
    label "emulation"
  ]
  node [
    id 70
    label "impreza"
  ]
  node [
    id 71
    label "casting"
  ]
  node [
    id 72
    label "Eurowizja"
  ]
  node [
    id 73
    label "nab&#243;r"
  ]
  node [
    id 74
    label "telewizja"
  ]
  node [
    id 75
    label "redaction"
  ]
  node [
    id 76
    label "obr&#243;bka"
  ]
  node [
    id 77
    label "radio"
  ]
  node [
    id 78
    label "wydawnictwo"
  ]
  node [
    id 79
    label "zesp&#243;&#322;"
  ]
  node [
    id 80
    label "redaktor"
  ]
  node [
    id 81
    label "siedziba"
  ]
  node [
    id 82
    label "composition"
  ]
  node [
    id 83
    label "tekst"
  ]
  node [
    id 84
    label "czasopismo"
  ]
  node [
    id 85
    label "szkolnie"
  ]
  node [
    id 86
    label "szkoleniowy"
  ]
  node [
    id 87
    label "podstawowy"
  ]
  node [
    id 88
    label "prosty"
  ]
  node [
    id 89
    label "licencja"
  ]
  node [
    id 90
    label "opieka"
  ]
  node [
    id 91
    label "nadz&#243;r"
  ]
  node [
    id 92
    label "sponsorship"
  ]
  node [
    id 93
    label "cz&#322;owiek"
  ]
  node [
    id 94
    label "profesor"
  ]
  node [
    id 95
    label "kszta&#322;ciciel"
  ]
  node [
    id 96
    label "jegomo&#347;&#263;"
  ]
  node [
    id 97
    label "zwrot"
  ]
  node [
    id 98
    label "pracodawca"
  ]
  node [
    id 99
    label "rz&#261;dzenie"
  ]
  node [
    id 100
    label "m&#261;&#380;"
  ]
  node [
    id 101
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 102
    label "ch&#322;opina"
  ]
  node [
    id 103
    label "bratek"
  ]
  node [
    id 104
    label "opiekun"
  ]
  node [
    id 105
    label "doros&#322;y"
  ]
  node [
    id 106
    label "preceptor"
  ]
  node [
    id 107
    label "Midas"
  ]
  node [
    id 108
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 109
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 110
    label "murza"
  ]
  node [
    id 111
    label "ojciec"
  ]
  node [
    id 112
    label "androlog"
  ]
  node [
    id 113
    label "pupil"
  ]
  node [
    id 114
    label "efendi"
  ]
  node [
    id 115
    label "nabab"
  ]
  node [
    id 116
    label "w&#322;odarz"
  ]
  node [
    id 117
    label "szkolnik"
  ]
  node [
    id 118
    label "pedagog"
  ]
  node [
    id 119
    label "popularyzator"
  ]
  node [
    id 120
    label "andropauza"
  ]
  node [
    id 121
    label "gra_w_karty"
  ]
  node [
    id 122
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 123
    label "Mieszko_I"
  ]
  node [
    id 124
    label "bogaty"
  ]
  node [
    id 125
    label "samiec"
  ]
  node [
    id 126
    label "przyw&#243;dca"
  ]
  node [
    id 127
    label "pa&#324;stwo"
  ]
  node [
    id 128
    label "belfer"
  ]
  node [
    id 129
    label "korytarz"
  ]
  node [
    id 130
    label "przedpok&#243;j"
  ]
  node [
    id 131
    label "Goebbels"
  ]
  node [
    id 132
    label "Sto&#322;ypin"
  ]
  node [
    id 133
    label "rz&#261;d"
  ]
  node [
    id 134
    label "dostojnik"
  ]
  node [
    id 135
    label "kwalifikacje"
  ]
  node [
    id 136
    label "Karta_Nauczyciela"
  ]
  node [
    id 137
    label "szkolnictwo"
  ]
  node [
    id 138
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 139
    label "program_nauczania"
  ]
  node [
    id 140
    label "formation"
  ]
  node [
    id 141
    label "miasteczko_rowerowe"
  ]
  node [
    id 142
    label "gospodarka"
  ]
  node [
    id 143
    label "urszulanki"
  ]
  node [
    id 144
    label "wiedza"
  ]
  node [
    id 145
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 146
    label "skolaryzacja"
  ]
  node [
    id 147
    label "proces"
  ]
  node [
    id 148
    label "niepokalanki"
  ]
  node [
    id 149
    label "heureza"
  ]
  node [
    id 150
    label "form"
  ]
  node [
    id 151
    label "nauka"
  ]
  node [
    id 152
    label "&#322;awa_szkolna"
  ]
  node [
    id 153
    label "nacjonalistyczny"
  ]
  node [
    id 154
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 155
    label "narodowo"
  ]
  node [
    id 156
    label "wa&#380;ny"
  ]
  node [
    id 157
    label "gwiazda"
  ]
  node [
    id 158
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 159
    label "skr&#281;canie"
  ]
  node [
    id 160
    label "voice"
  ]
  node [
    id 161
    label "forma"
  ]
  node [
    id 162
    label "internet"
  ]
  node [
    id 163
    label "skr&#281;ci&#263;"
  ]
  node [
    id 164
    label "kartka"
  ]
  node [
    id 165
    label "orientowa&#263;"
  ]
  node [
    id 166
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 167
    label "powierzchnia"
  ]
  node [
    id 168
    label "plik"
  ]
  node [
    id 169
    label "bok"
  ]
  node [
    id 170
    label "pagina"
  ]
  node [
    id 171
    label "orientowanie"
  ]
  node [
    id 172
    label "fragment"
  ]
  node [
    id 173
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 174
    label "skr&#281;ca&#263;"
  ]
  node [
    id 175
    label "g&#243;ra"
  ]
  node [
    id 176
    label "serwis_internetowy"
  ]
  node [
    id 177
    label "orientacja"
  ]
  node [
    id 178
    label "linia"
  ]
  node [
    id 179
    label "skr&#281;cenie"
  ]
  node [
    id 180
    label "layout"
  ]
  node [
    id 181
    label "zorientowa&#263;"
  ]
  node [
    id 182
    label "zorientowanie"
  ]
  node [
    id 183
    label "obiekt"
  ]
  node [
    id 184
    label "podmiot"
  ]
  node [
    id 185
    label "ty&#322;"
  ]
  node [
    id 186
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 187
    label "logowanie"
  ]
  node [
    id 188
    label "adres_internetowy"
  ]
  node [
    id 189
    label "uj&#281;cie"
  ]
  node [
    id 190
    label "prz&#243;d"
  ]
  node [
    id 191
    label "posta&#263;"
  ]
  node [
    id 192
    label "foundation"
  ]
  node [
    id 193
    label "instytucja"
  ]
  node [
    id 194
    label "dar"
  ]
  node [
    id 195
    label "darowizna"
  ]
  node [
    id 196
    label "pocz&#261;tek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 159
  ]
  edge [
    source 23
    target 160
  ]
  edge [
    source 23
    target 161
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 23
    target 164
  ]
  edge [
    source 23
    target 165
  ]
  edge [
    source 23
    target 166
  ]
  edge [
    source 23
    target 167
  ]
  edge [
    source 23
    target 168
  ]
  edge [
    source 23
    target 169
  ]
  edge [
    source 23
    target 170
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 23
    target 173
  ]
  edge [
    source 23
    target 54
  ]
  edge [
    source 23
    target 174
  ]
  edge [
    source 23
    target 175
  ]
  edge [
    source 23
    target 176
  ]
  edge [
    source 23
    target 177
  ]
  edge [
    source 23
    target 178
  ]
  edge [
    source 23
    target 179
  ]
  edge [
    source 23
    target 180
  ]
  edge [
    source 23
    target 181
  ]
  edge [
    source 23
    target 182
  ]
  edge [
    source 23
    target 183
  ]
  edge [
    source 23
    target 184
  ]
  edge [
    source 23
    target 185
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 23
    target 187
  ]
  edge [
    source 23
    target 188
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 24
    target 192
  ]
  edge [
    source 24
    target 193
  ]
  edge [
    source 24
    target 194
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 196
  ]
]
