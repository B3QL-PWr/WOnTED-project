graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.1147540983606556
  density 0.01747730659802195
  graphCliqueNumber 3
  node [
    id 0
    label "drzewo"
    origin "text"
  ]
  node [
    id 1
    label "spina&#263;"
    origin "text"
  ]
  node [
    id 2
    label "graf"
    origin "text"
  ]
  node [
    id 3
    label "sp&#243;jny"
    origin "text"
  ]
  node [
    id 4
    label "godzina"
    origin "text"
  ]
  node [
    id 5
    label "nazywa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "podgraf"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "zawiera&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wszystek"
    origin "text"
  ]
  node [
    id 10
    label "wierzcho&#322;ek"
    origin "text"
  ]
  node [
    id 11
    label "korona"
  ]
  node [
    id 12
    label "kora"
  ]
  node [
    id 13
    label "&#322;yko"
  ]
  node [
    id 14
    label "szpaler"
  ]
  node [
    id 15
    label "fanerofit"
  ]
  node [
    id 16
    label "drzewostan"
  ]
  node [
    id 17
    label "chodnik"
  ]
  node [
    id 18
    label "wykarczowanie"
  ]
  node [
    id 19
    label "surowiec"
  ]
  node [
    id 20
    label "cecha"
  ]
  node [
    id 21
    label "las"
  ]
  node [
    id 22
    label "wykarczowa&#263;"
  ]
  node [
    id 23
    label "zacios"
  ]
  node [
    id 24
    label "brodaczka"
  ]
  node [
    id 25
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 26
    label "karczowa&#263;"
  ]
  node [
    id 27
    label "pie&#324;"
  ]
  node [
    id 28
    label "pier&#347;nica"
  ]
  node [
    id 29
    label "zadrzewienie"
  ]
  node [
    id 30
    label "karczowanie"
  ]
  node [
    id 31
    label "parzelnia"
  ]
  node [
    id 32
    label "&#380;ywica"
  ]
  node [
    id 33
    label "skupina"
  ]
  node [
    id 34
    label "zaciska&#263;"
  ]
  node [
    id 35
    label "pin"
  ]
  node [
    id 36
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 37
    label "tighten"
  ]
  node [
    id 38
    label "scala&#263;"
  ]
  node [
    id 39
    label "wykres"
  ]
  node [
    id 40
    label "hrabia"
  ]
  node [
    id 41
    label "graph"
  ]
  node [
    id 42
    label "arystokrata"
  ]
  node [
    id 43
    label "kraw&#281;d&#378;"
  ]
  node [
    id 44
    label "tytu&#322;"
  ]
  node [
    id 45
    label "Fredro"
  ]
  node [
    id 46
    label "poj&#281;cie"
  ]
  node [
    id 47
    label "jednolity"
  ]
  node [
    id 48
    label "zwarty"
  ]
  node [
    id 49
    label "spojny"
  ]
  node [
    id 50
    label "spoi&#347;cie"
  ]
  node [
    id 51
    label "harmonijny"
  ]
  node [
    id 52
    label "sp&#243;jnie"
  ]
  node [
    id 53
    label "minuta"
  ]
  node [
    id 54
    label "doba"
  ]
  node [
    id 55
    label "czas"
  ]
  node [
    id 56
    label "p&#243;&#322;godzina"
  ]
  node [
    id 57
    label "kwadrans"
  ]
  node [
    id 58
    label "time"
  ]
  node [
    id 59
    label "jednostka_czasu"
  ]
  node [
    id 60
    label "give"
  ]
  node [
    id 61
    label "mieni&#263;"
  ]
  node [
    id 62
    label "okre&#347;la&#263;"
  ]
  node [
    id 63
    label "nadawa&#263;"
  ]
  node [
    id 64
    label "si&#281;ga&#263;"
  ]
  node [
    id 65
    label "trwa&#263;"
  ]
  node [
    id 66
    label "obecno&#347;&#263;"
  ]
  node [
    id 67
    label "stan"
  ]
  node [
    id 68
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 69
    label "stand"
  ]
  node [
    id 70
    label "mie&#263;_miejsce"
  ]
  node [
    id 71
    label "uczestniczy&#263;"
  ]
  node [
    id 72
    label "chodzi&#263;"
  ]
  node [
    id 73
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 74
    label "equal"
  ]
  node [
    id 75
    label "obejmowa&#263;"
  ]
  node [
    id 76
    label "mie&#263;"
  ]
  node [
    id 77
    label "zamyka&#263;"
  ]
  node [
    id 78
    label "lock"
  ]
  node [
    id 79
    label "poznawa&#263;"
  ]
  node [
    id 80
    label "fold"
  ]
  node [
    id 81
    label "make"
  ]
  node [
    id 82
    label "ustala&#263;"
  ]
  node [
    id 83
    label "ca&#322;y"
  ]
  node [
    id 84
    label "Lubogoszcz"
  ]
  node [
    id 85
    label "koniec"
  ]
  node [
    id 86
    label "wierch"
  ]
  node [
    id 87
    label "&#321;omnica"
  ]
  node [
    id 88
    label "Wielki_Chocz"
  ]
  node [
    id 89
    label "Magura"
  ]
  node [
    id 90
    label "Turbacz"
  ]
  node [
    id 91
    label "Wo&#322;ek"
  ]
  node [
    id 92
    label "Walig&#243;ra"
  ]
  node [
    id 93
    label "Orlica"
  ]
  node [
    id 94
    label "Jaworzyna"
  ]
  node [
    id 95
    label "punkt"
  ]
  node [
    id 96
    label "Groniczki"
  ]
  node [
    id 97
    label "Radunia"
  ]
  node [
    id 98
    label "Okr&#261;glica"
  ]
  node [
    id 99
    label "&#346;winica"
  ]
  node [
    id 100
    label "Beskid"
  ]
  node [
    id 101
    label "Czupel"
  ]
  node [
    id 102
    label "Rysianka"
  ]
  node [
    id 103
    label "g&#243;ra"
  ]
  node [
    id 104
    label "Jaworz"
  ]
  node [
    id 105
    label "Che&#322;miec"
  ]
  node [
    id 106
    label "Rudawiec"
  ]
  node [
    id 107
    label "Wielki_Bukowiec"
  ]
  node [
    id 108
    label "wzniesienie"
  ]
  node [
    id 109
    label "Wielka_Racza"
  ]
  node [
    id 110
    label "wierzcho&#322;"
  ]
  node [
    id 111
    label "&#346;nie&#380;nik"
  ]
  node [
    id 112
    label "Barania_G&#243;ra"
  ]
  node [
    id 113
    label "Ja&#322;owiec"
  ]
  node [
    id 114
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 115
    label "Wielka_Sowa"
  ]
  node [
    id 116
    label "Obidowa"
  ]
  node [
    id 117
    label "Cubryna"
  ]
  node [
    id 118
    label "Szrenica"
  ]
  node [
    id 119
    label "Czarna_G&#243;ra"
  ]
  node [
    id 120
    label "Mody&#324;"
  ]
  node [
    id 121
    label "&#346;l&#281;&#380;a"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
]
