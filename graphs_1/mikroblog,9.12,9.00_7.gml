graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.3333333333333333
  density 0.26666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "lea"
    origin "text"
  ]
  node [
    id 1
    label "messi"
    origin "text"
  ]
  node [
    id 2
    label "espanyol"
    origin "text"
  ]
  node [
    id 3
    label "barcelona"
    origin "text"
  ]
  node [
    id 4
    label "Lea"
  ]
  node [
    id 5
    label "Messi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
]
