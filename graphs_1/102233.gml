graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.8333333333333335
  density 0.0264797507788162
  graphCliqueNumber 10
  node [
    id 0
    label "art"
    origin "text"
  ]
  node [
    id 1
    label "pkt"
    origin "text"
  ]
  node [
    id 2
    label "wyraz"
    origin "text"
  ]
  node [
    id 3
    label "numer"
    origin "text"
  ]
  node [
    id 4
    label "nip"
    origin "text"
  ]
  node [
    id 5
    label "regon"
    origin "text"
  ]
  node [
    id 6
    label "ewentualnie"
    origin "text"
  ]
  node [
    id 7
    label "pesel"
    origin "text"
  ]
  node [
    id 8
    label "zast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "dana"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "mowa"
    origin "text"
  ]
  node [
    id 13
    label "usta"
    origin "text"
  ]
  node [
    id 14
    label "term"
  ]
  node [
    id 15
    label "oznaka"
  ]
  node [
    id 16
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 17
    label "cecha"
  ]
  node [
    id 18
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 19
    label "&#347;wiadczenie"
  ]
  node [
    id 20
    label "element"
  ]
  node [
    id 21
    label "posta&#263;"
  ]
  node [
    id 22
    label "leksem"
  ]
  node [
    id 23
    label "manewr"
  ]
  node [
    id 24
    label "sztos"
  ]
  node [
    id 25
    label "pok&#243;j"
  ]
  node [
    id 26
    label "facet"
  ]
  node [
    id 27
    label "wyst&#281;p"
  ]
  node [
    id 28
    label "turn"
  ]
  node [
    id 29
    label "impression"
  ]
  node [
    id 30
    label "hotel"
  ]
  node [
    id 31
    label "liczba"
  ]
  node [
    id 32
    label "punkt"
  ]
  node [
    id 33
    label "czasopismo"
  ]
  node [
    id 34
    label "&#380;art"
  ]
  node [
    id 35
    label "orygina&#322;"
  ]
  node [
    id 36
    label "oznaczenie"
  ]
  node [
    id 37
    label "zi&#243;&#322;ko"
  ]
  node [
    id 38
    label "akt_p&#322;ciowy"
  ]
  node [
    id 39
    label "publikacja"
  ]
  node [
    id 40
    label "mo&#380;liwie"
  ]
  node [
    id 41
    label "ewentualny"
  ]
  node [
    id 42
    label "personalia"
  ]
  node [
    id 43
    label "zmienia&#263;"
  ]
  node [
    id 44
    label "decydowa&#263;"
  ]
  node [
    id 45
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 46
    label "dar"
  ]
  node [
    id 47
    label "cnota"
  ]
  node [
    id 48
    label "buddyzm"
  ]
  node [
    id 49
    label "wypowied&#378;"
  ]
  node [
    id 50
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 51
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 52
    label "po_koroniarsku"
  ]
  node [
    id 53
    label "m&#243;wienie"
  ]
  node [
    id 54
    label "rozumie&#263;"
  ]
  node [
    id 55
    label "komunikacja"
  ]
  node [
    id 56
    label "rozumienie"
  ]
  node [
    id 57
    label "m&#243;wi&#263;"
  ]
  node [
    id 58
    label "gramatyka"
  ]
  node [
    id 59
    label "address"
  ]
  node [
    id 60
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 61
    label "przet&#322;umaczenie"
  ]
  node [
    id 62
    label "czynno&#347;&#263;"
  ]
  node [
    id 63
    label "tongue"
  ]
  node [
    id 64
    label "t&#322;umaczenie"
  ]
  node [
    id 65
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 66
    label "pismo"
  ]
  node [
    id 67
    label "zdolno&#347;&#263;"
  ]
  node [
    id 68
    label "fonetyka"
  ]
  node [
    id 69
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 70
    label "wokalizm"
  ]
  node [
    id 71
    label "s&#322;ownictwo"
  ]
  node [
    id 72
    label "konsonantyzm"
  ]
  node [
    id 73
    label "kod"
  ]
  node [
    id 74
    label "warga_dolna"
  ]
  node [
    id 75
    label "ssa&#263;"
  ]
  node [
    id 76
    label "zaci&#261;&#263;"
  ]
  node [
    id 77
    label "ryjek"
  ]
  node [
    id 78
    label "twarz"
  ]
  node [
    id 79
    label "dzi&#243;b"
  ]
  node [
    id 80
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 81
    label "ssanie"
  ]
  node [
    id 82
    label "zaci&#281;cie"
  ]
  node [
    id 83
    label "jadaczka"
  ]
  node [
    id 84
    label "zacinanie"
  ]
  node [
    id 85
    label "organ"
  ]
  node [
    id 86
    label "jama_ustna"
  ]
  node [
    id 87
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 88
    label "warga_g&#243;rna"
  ]
  node [
    id 89
    label "zacina&#263;"
  ]
  node [
    id 90
    label "ustawa"
  ]
  node [
    id 91
    label "zeszyt"
  ]
  node [
    id 92
    label "dzie&#324;"
  ]
  node [
    id 93
    label "26"
  ]
  node [
    id 94
    label "listopad"
  ]
  node [
    id 95
    label "1998"
  ]
  node [
    id 96
    label "rok"
  ]
  node [
    id 97
    label "ojciec"
  ]
  node [
    id 98
    label "finanse"
  ]
  node [
    id 99
    label "publiczny"
  ]
  node [
    id 100
    label "dziennik"
  ]
  node [
    id 101
    label "u"
  ]
  node [
    id 102
    label "komisja"
  ]
  node [
    id 103
    label "do"
  ]
  node [
    id 104
    label "sprawi&#263;"
  ]
  node [
    id 105
    label "zaopatrzy&#263;"
  ]
  node [
    id 106
    label "emerytalny"
  ]
  node [
    id 107
    label "tw&#243;rca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 49
  ]
  edge [
    source 12
    target 50
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 54
  ]
  edge [
    source 12
    target 55
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 12
    target 57
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 92
  ]
  edge [
    source 90
    target 93
  ]
  edge [
    source 90
    target 94
  ]
  edge [
    source 90
    target 95
  ]
  edge [
    source 90
    target 96
  ]
  edge [
    source 90
    target 97
  ]
  edge [
    source 90
    target 98
  ]
  edge [
    source 90
    target 99
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 93
  ]
  edge [
    source 91
    target 94
  ]
  edge [
    source 91
    target 95
  ]
  edge [
    source 91
    target 96
  ]
  edge [
    source 91
    target 97
  ]
  edge [
    source 91
    target 98
  ]
  edge [
    source 91
    target 99
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 94
  ]
  edge [
    source 92
    target 95
  ]
  edge [
    source 92
    target 96
  ]
  edge [
    source 92
    target 97
  ]
  edge [
    source 92
    target 98
  ]
  edge [
    source 92
    target 99
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 95
  ]
  edge [
    source 93
    target 96
  ]
  edge [
    source 93
    target 97
  ]
  edge [
    source 93
    target 98
  ]
  edge [
    source 93
    target 99
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 96
  ]
  edge [
    source 94
    target 97
  ]
  edge [
    source 94
    target 98
  ]
  edge [
    source 94
    target 99
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 97
  ]
  edge [
    source 95
    target 98
  ]
  edge [
    source 95
    target 99
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 98
  ]
  edge [
    source 96
    target 99
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 99
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 104
  ]
  edge [
    source 102
    target 105
  ]
  edge [
    source 102
    target 106
  ]
  edge [
    source 102
    target 107
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 105
  ]
  edge [
    source 103
    target 106
  ]
  edge [
    source 103
    target 107
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 106
  ]
  edge [
    source 104
    target 107
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 107
  ]
  edge [
    source 106
    target 107
  ]
]
