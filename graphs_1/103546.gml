graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9863945578231292
  density 0.013605442176870748
  graphCliqueNumber 2
  node [
    id 0
    label "lekarka"
    origin "text"
  ]
  node [
    id 1
    label "pi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "&#322;yk"
    origin "text"
  ]
  node [
    id 3
    label "kawa"
    origin "text"
  ]
  node [
    id 4
    label "pukanie"
    origin "text"
  ]
  node [
    id 5
    label "drzwi"
    origin "text"
  ]
  node [
    id 6
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zaproszenie"
    origin "text"
  ]
  node [
    id 8
    label "gabinet"
    origin "text"
  ]
  node [
    id 9
    label "wchodzi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "j&#281;drek"
    origin "text"
  ]
  node [
    id 11
    label "goli&#263;"
  ]
  node [
    id 12
    label "ch&#322;on&#261;&#263;"
  ]
  node [
    id 13
    label "gulp"
  ]
  node [
    id 14
    label "nabiera&#263;"
  ]
  node [
    id 15
    label "naoliwia&#263;_si&#281;"
  ]
  node [
    id 16
    label "dostarcza&#263;"
  ]
  node [
    id 17
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 18
    label "obci&#261;ga&#263;"
  ]
  node [
    id 19
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 20
    label "nawi&#261;zywa&#263;"
  ]
  node [
    id 21
    label "wch&#322;ania&#263;"
  ]
  node [
    id 22
    label "uwiera&#263;"
  ]
  node [
    id 23
    label "pull"
  ]
  node [
    id 24
    label "ilo&#347;&#263;"
  ]
  node [
    id 25
    label "u&#380;ywka"
  ]
  node [
    id 26
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 27
    label "dripper"
  ]
  node [
    id 28
    label "marzanowate"
  ]
  node [
    id 29
    label "produkt"
  ]
  node [
    id 30
    label "ziarno"
  ]
  node [
    id 31
    label "pestkowiec"
  ]
  node [
    id 32
    label "nap&#243;j"
  ]
  node [
    id 33
    label "jedzenie"
  ]
  node [
    id 34
    label "ro&#347;lina"
  ]
  node [
    id 35
    label "egzotyk"
  ]
  node [
    id 36
    label "porcja"
  ]
  node [
    id 37
    label "kofeina"
  ]
  node [
    id 38
    label "chemex"
  ]
  node [
    id 39
    label "odpalanie"
  ]
  node [
    id 40
    label "wystrzelanie"
  ]
  node [
    id 41
    label "robienie"
  ]
  node [
    id 42
    label "trafianie"
  ]
  node [
    id 43
    label "uleganie"
  ]
  node [
    id 44
    label "ostrzeliwanie"
  ]
  node [
    id 45
    label "wiretap"
  ]
  node [
    id 46
    label "odstrzelenie"
  ]
  node [
    id 47
    label "plucie"
  ]
  node [
    id 48
    label "palenie"
  ]
  node [
    id 49
    label "stuk"
  ]
  node [
    id 50
    label "kropni&#281;cie"
  ]
  node [
    id 51
    label "wstrzeliwanie_si&#281;"
  ]
  node [
    id 52
    label "uderzanie"
  ]
  node [
    id 53
    label "branie"
  ]
  node [
    id 54
    label "wylatywanie"
  ]
  node [
    id 55
    label "prze&#322;adowywanie"
  ]
  node [
    id 56
    label "czynno&#347;&#263;"
  ]
  node [
    id 57
    label "zestrzelenie"
  ]
  node [
    id 58
    label "grzanie"
  ]
  node [
    id 59
    label "fire"
  ]
  node [
    id 60
    label "chybienie"
  ]
  node [
    id 61
    label "wystukanie"
  ]
  node [
    id 62
    label "postukanie"
  ]
  node [
    id 63
    label "postukiwanie"
  ]
  node [
    id 64
    label "odstrzeliwanie"
  ]
  node [
    id 65
    label "wystukiwanie"
  ]
  node [
    id 66
    label "przestrzeliwanie"
  ]
  node [
    id 67
    label "kucie"
  ]
  node [
    id 68
    label "zastukanie"
  ]
  node [
    id 69
    label "postrzelanie"
  ]
  node [
    id 70
    label "zestrzeliwanie"
  ]
  node [
    id 71
    label "walczenie"
  ]
  node [
    id 72
    label "ostrzelanie"
  ]
  node [
    id 73
    label "pat"
  ]
  node [
    id 74
    label "chybianie"
  ]
  node [
    id 75
    label "powodowanie"
  ]
  node [
    id 76
    label "wyj&#347;cie"
  ]
  node [
    id 77
    label "wytw&#243;r"
  ]
  node [
    id 78
    label "doj&#347;cie"
  ]
  node [
    id 79
    label "skrzyd&#322;o"
  ]
  node [
    id 80
    label "zamek"
  ]
  node [
    id 81
    label "futryna"
  ]
  node [
    id 82
    label "antaba"
  ]
  node [
    id 83
    label "szafa"
  ]
  node [
    id 84
    label "szafka"
  ]
  node [
    id 85
    label "wej&#347;cie"
  ]
  node [
    id 86
    label "zawiasy"
  ]
  node [
    id 87
    label "samozamykacz"
  ]
  node [
    id 88
    label "ko&#322;atka"
  ]
  node [
    id 89
    label "klamka"
  ]
  node [
    id 90
    label "wrzeci&#261;dz"
  ]
  node [
    id 91
    label "by&#263;"
  ]
  node [
    id 92
    label "hold"
  ]
  node [
    id 93
    label "sp&#281;dza&#263;"
  ]
  node [
    id 94
    label "look"
  ]
  node [
    id 95
    label "decydowa&#263;"
  ]
  node [
    id 96
    label "oczekiwa&#263;"
  ]
  node [
    id 97
    label "pauzowa&#263;"
  ]
  node [
    id 98
    label "anticipate"
  ]
  node [
    id 99
    label "zaproponowanie"
  ]
  node [
    id 100
    label "propozycja"
  ]
  node [
    id 101
    label "pro&#347;ba"
  ]
  node [
    id 102
    label "invitation"
  ]
  node [
    id 103
    label "druk"
  ]
  node [
    id 104
    label "karteczka"
  ]
  node [
    id 105
    label "pok&#243;j"
  ]
  node [
    id 106
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 107
    label "s&#261;d"
  ]
  node [
    id 108
    label "egzekutywa"
  ]
  node [
    id 109
    label "biurko"
  ]
  node [
    id 110
    label "gabinet_cieni"
  ]
  node [
    id 111
    label "palestra"
  ]
  node [
    id 112
    label "premier"
  ]
  node [
    id 113
    label "Londyn"
  ]
  node [
    id 114
    label "Konsulat"
  ]
  node [
    id 115
    label "pracownia"
  ]
  node [
    id 116
    label "instytucja"
  ]
  node [
    id 117
    label "boks"
  ]
  node [
    id 118
    label "pomieszczenie"
  ]
  node [
    id 119
    label "szko&#322;a"
  ]
  node [
    id 120
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 121
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 122
    label "przenika&#263;"
  ]
  node [
    id 123
    label "przekracza&#263;"
  ]
  node [
    id 124
    label "nast&#281;powa&#263;"
  ]
  node [
    id 125
    label "dochodzi&#263;"
  ]
  node [
    id 126
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 127
    label "intervene"
  ]
  node [
    id 128
    label "scale"
  ]
  node [
    id 129
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 130
    label "&#322;oi&#263;"
  ]
  node [
    id 131
    label "osi&#261;ga&#263;"
  ]
  node [
    id 132
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 133
    label "poznawa&#263;"
  ]
  node [
    id 134
    label "go"
  ]
  node [
    id 135
    label "atakowa&#263;"
  ]
  node [
    id 136
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 137
    label "mount"
  ]
  node [
    id 138
    label "invade"
  ]
  node [
    id 139
    label "bra&#263;"
  ]
  node [
    id 140
    label "wnika&#263;"
  ]
  node [
    id 141
    label "move"
  ]
  node [
    id 142
    label "zaziera&#263;"
  ]
  node [
    id 143
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 144
    label "spotyka&#263;"
  ]
  node [
    id 145
    label "zaczyna&#263;"
  ]
  node [
    id 146
    label "stawa&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
]
