graph [
  maxDegree 218
  minDegree 1
  meanDegree 2.396747967479675
  density 0.003903498318370806
  graphCliqueNumber 12
  node [
    id 0
    label "zastrze&#380;enie"
    origin "text"
  ]
  node [
    id 1
    label "poszanowanie"
    origin "text"
  ]
  node [
    id 2
    label "obowi&#261;zuj&#261;cy"
    origin "text"
  ]
  node [
    id 3
    label "norma"
    origin "text"
  ]
  node [
    id 4
    label "prawa"
    origin "text"
  ]
  node [
    id 5
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 6
    label "szczeg&#243;lno&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "konwencja"
    origin "text"
  ]
  node [
    id 8
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 9
    label "zjednoczy&#263;"
    origin "text"
  ]
  node [
    id 10
    label "przeciwko"
    origin "text"
  ]
  node [
    id 11
    label "przest&#281;pczo&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "zorganizowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "sporz&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "palermo"
    origin "text"
  ]
  node [
    id 15
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 16
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 17
    label "rok"
    origin "text"
  ]
  node [
    id 18
    label "zwalczanie"
    origin "text"
  ]
  node [
    id 19
    label "nielegalny"
    origin "text"
  ]
  node [
    id 20
    label "obr&#243;t"
    origin "text"
  ]
  node [
    id 21
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 22
    label "odurzaj&#261;cy"
    origin "text"
  ]
  node [
    id 23
    label "substancja"
    origin "text"
  ]
  node [
    id 24
    label "psychotropowy"
    origin "text"
  ]
  node [
    id 25
    label "wiede&#324;"
    origin "text"
  ]
  node [
    id 26
    label "wewn&#281;trzny"
    origin "text"
  ]
  node [
    id 27
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 28
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 29
    label "kierowa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "si&#281;"
    origin "text"
  ]
  node [
    id 31
    label "zasada"
    origin "text"
  ]
  node [
    id 32
    label "r&#243;wno&#347;&#263;"
    origin "text"
  ]
  node [
    id 33
    label "wzajemno&#347;&#263;"
    origin "text"
  ]
  node [
    id 34
    label "dwustronny"
    origin "text"
  ]
  node [
    id 35
    label "korzy&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "umawia&#263;"
    origin "text"
  ]
  node [
    id 37
    label "strona"
    origin "text"
  ]
  node [
    id 38
    label "zobowi&#261;zywa&#263;"
    origin "text"
  ]
  node [
    id 39
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 40
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 41
    label "organ"
    origin "text"
  ]
  node [
    id 42
    label "ochrona"
    origin "text"
  ]
  node [
    id 43
    label "bezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 44
    label "porz&#261;dek"
    origin "text"
  ]
  node [
    id 45
    label "publiczny"
    origin "text"
  ]
  node [
    id 46
    label "zakres"
    origin "text"
  ]
  node [
    id 47
    label "zapobieganie"
    origin "text"
  ]
  node [
    id 48
    label "inny"
    origin "text"
  ]
  node [
    id 49
    label "rodzaj"
    origin "text"
  ]
  node [
    id 50
    label "powa&#380;ny"
    origin "text"
  ]
  node [
    id 51
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 52
    label "przest&#281;pstwo"
    origin "text"
  ]
  node [
    id 53
    label "uprzedzenie"
  ]
  node [
    id 54
    label "wypowied&#378;"
  ]
  node [
    id 55
    label "wym&#243;wienie"
  ]
  node [
    id 56
    label "restriction"
  ]
  node [
    id 57
    label "umowa"
  ]
  node [
    id 58
    label "zapewnienie"
  ]
  node [
    id 59
    label "question"
  ]
  node [
    id 60
    label "condition"
  ]
  node [
    id 61
    label "zaimponowanie"
  ]
  node [
    id 62
    label "szanowa&#263;"
  ]
  node [
    id 63
    label "honorowanie"
  ]
  node [
    id 64
    label "uszanowa&#263;"
  ]
  node [
    id 65
    label "rewerencja"
  ]
  node [
    id 66
    label "uszanowanie"
  ]
  node [
    id 67
    label "imponowanie"
  ]
  node [
    id 68
    label "uhonorowanie"
  ]
  node [
    id 69
    label "respect"
  ]
  node [
    id 70
    label "honorowa&#263;"
  ]
  node [
    id 71
    label "uhonorowa&#263;"
  ]
  node [
    id 72
    label "szacuneczek"
  ]
  node [
    id 73
    label "postawa"
  ]
  node [
    id 74
    label "obowi&#261;zuj&#261;co"
  ]
  node [
    id 75
    label "uprawomocnianie_si&#281;"
  ]
  node [
    id 76
    label "uprawomocnienie_si&#281;"
  ]
  node [
    id 77
    label "aktualny"
  ]
  node [
    id 78
    label "dokument"
  ]
  node [
    id 79
    label "pace"
  ]
  node [
    id 80
    label "moralno&#347;&#263;"
  ]
  node [
    id 81
    label "powszednio&#347;&#263;"
  ]
  node [
    id 82
    label "konstrukt"
  ]
  node [
    id 83
    label "prawid&#322;o"
  ]
  node [
    id 84
    label "criterion"
  ]
  node [
    id 85
    label "rozmiar"
  ]
  node [
    id 86
    label "standard"
  ]
  node [
    id 87
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 88
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 89
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 90
    label "wyj&#261;tkowo&#347;&#263;"
  ]
  node [
    id 91
    label "uk&#322;ad"
  ]
  node [
    id 92
    label "zjazd"
  ]
  node [
    id 93
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 94
    label "line"
  ]
  node [
    id 95
    label "zbi&#243;r"
  ]
  node [
    id 96
    label "kanon"
  ]
  node [
    id 97
    label "zwyczaj"
  ]
  node [
    id 98
    label "styl"
  ]
  node [
    id 99
    label "Samojedzi"
  ]
  node [
    id 100
    label "nacja"
  ]
  node [
    id 101
    label "Aztekowie"
  ]
  node [
    id 102
    label "Buriaci"
  ]
  node [
    id 103
    label "Irokezi"
  ]
  node [
    id 104
    label "Komancze"
  ]
  node [
    id 105
    label "t&#322;um"
  ]
  node [
    id 106
    label "ludno&#347;&#263;"
  ]
  node [
    id 107
    label "lud"
  ]
  node [
    id 108
    label "Czejenowie"
  ]
  node [
    id 109
    label "Siuksowie"
  ]
  node [
    id 110
    label "Wotiacy"
  ]
  node [
    id 111
    label "Baszkirzy"
  ]
  node [
    id 112
    label "Apacze"
  ]
  node [
    id 113
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 114
    label "Mohikanie"
  ]
  node [
    id 115
    label "Syngalezi"
  ]
  node [
    id 116
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 117
    label "consort"
  ]
  node [
    id 118
    label "gang"
  ]
  node [
    id 119
    label "bezprawie"
  ]
  node [
    id 120
    label "patologia"
  ]
  node [
    id 121
    label "criminalism"
  ]
  node [
    id 122
    label "banda"
  ]
  node [
    id 123
    label "problem_spo&#322;eczny"
  ]
  node [
    id 124
    label "zaplanowa&#263;"
  ]
  node [
    id 125
    label "dostosowa&#263;"
  ]
  node [
    id 126
    label "przygotowa&#263;"
  ]
  node [
    id 127
    label "stworzy&#263;"
  ]
  node [
    id 128
    label "stage"
  ]
  node [
    id 129
    label "pozyska&#263;"
  ]
  node [
    id 130
    label "urobi&#263;"
  ]
  node [
    id 131
    label "plan"
  ]
  node [
    id 132
    label "ensnare"
  ]
  node [
    id 133
    label "skupi&#263;"
  ]
  node [
    id 134
    label "wprowadzi&#263;"
  ]
  node [
    id 135
    label "wytworzy&#263;"
  ]
  node [
    id 136
    label "opracowa&#263;"
  ]
  node [
    id 137
    label "draw"
  ]
  node [
    id 138
    label "oprawi&#263;"
  ]
  node [
    id 139
    label "zrobi&#263;"
  ]
  node [
    id 140
    label "podzieli&#263;"
  ]
  node [
    id 141
    label "s&#322;o&#324;ce"
  ]
  node [
    id 142
    label "czynienie_si&#281;"
  ]
  node [
    id 143
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 144
    label "czas"
  ]
  node [
    id 145
    label "long_time"
  ]
  node [
    id 146
    label "przedpo&#322;udnie"
  ]
  node [
    id 147
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 148
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 149
    label "tydzie&#324;"
  ]
  node [
    id 150
    label "godzina"
  ]
  node [
    id 151
    label "t&#322;usty_czwartek"
  ]
  node [
    id 152
    label "wsta&#263;"
  ]
  node [
    id 153
    label "day"
  ]
  node [
    id 154
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 155
    label "przedwiecz&#243;r"
  ]
  node [
    id 156
    label "Sylwester"
  ]
  node [
    id 157
    label "po&#322;udnie"
  ]
  node [
    id 158
    label "wzej&#347;cie"
  ]
  node [
    id 159
    label "podwiecz&#243;r"
  ]
  node [
    id 160
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 161
    label "rano"
  ]
  node [
    id 162
    label "termin"
  ]
  node [
    id 163
    label "ranek"
  ]
  node [
    id 164
    label "doba"
  ]
  node [
    id 165
    label "wiecz&#243;r"
  ]
  node [
    id 166
    label "walentynki"
  ]
  node [
    id 167
    label "popo&#322;udnie"
  ]
  node [
    id 168
    label "noc"
  ]
  node [
    id 169
    label "wstanie"
  ]
  node [
    id 170
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 171
    label "miesi&#261;c"
  ]
  node [
    id 172
    label "Barb&#243;rka"
  ]
  node [
    id 173
    label "stulecie"
  ]
  node [
    id 174
    label "kalendarz"
  ]
  node [
    id 175
    label "pora_roku"
  ]
  node [
    id 176
    label "cykl_astronomiczny"
  ]
  node [
    id 177
    label "p&#243;&#322;rocze"
  ]
  node [
    id 178
    label "grupa"
  ]
  node [
    id 179
    label "kwarta&#322;"
  ]
  node [
    id 180
    label "kurs"
  ]
  node [
    id 181
    label "jubileusz"
  ]
  node [
    id 182
    label "lata"
  ]
  node [
    id 183
    label "martwy_sezon"
  ]
  node [
    id 184
    label "sprzeciwianie_si&#281;"
  ]
  node [
    id 185
    label "pokonywanie"
  ]
  node [
    id 186
    label "zdelegalizowanie"
  ]
  node [
    id 187
    label "nieoficjalny"
  ]
  node [
    id 188
    label "delegalizowanie"
  ]
  node [
    id 189
    label "nielegalnie"
  ]
  node [
    id 190
    label "turn"
  ]
  node [
    id 191
    label "ruch"
  ]
  node [
    id 192
    label "sprzeda&#380;"
  ]
  node [
    id 193
    label "proces_ekonomiczny"
  ]
  node [
    id 194
    label "zmiana"
  ]
  node [
    id 195
    label "wp&#322;yw"
  ]
  node [
    id 196
    label "obieg"
  ]
  node [
    id 197
    label "round"
  ]
  node [
    id 198
    label "miejsce"
  ]
  node [
    id 199
    label "abstrakcja"
  ]
  node [
    id 200
    label "punkt"
  ]
  node [
    id 201
    label "spos&#243;b"
  ]
  node [
    id 202
    label "chemikalia"
  ]
  node [
    id 203
    label "odurzaj&#261;co"
  ]
  node [
    id 204
    label "zniewalaj&#261;cy"
  ]
  node [
    id 205
    label "intensywny"
  ]
  node [
    id 206
    label "byt"
  ]
  node [
    id 207
    label "smolisty"
  ]
  node [
    id 208
    label "przenika&#263;"
  ]
  node [
    id 209
    label "cz&#261;steczka"
  ]
  node [
    id 210
    label "materia"
  ]
  node [
    id 211
    label "przenikanie"
  ]
  node [
    id 212
    label "temperatura_krytyczna"
  ]
  node [
    id 213
    label "psychoaktywny"
  ]
  node [
    id 214
    label "leczniczy"
  ]
  node [
    id 215
    label "wewn&#281;trznie"
  ]
  node [
    id 216
    label "wn&#281;trzny"
  ]
  node [
    id 217
    label "psychiczny"
  ]
  node [
    id 218
    label "numer"
  ]
  node [
    id 219
    label "oddzia&#322;"
  ]
  node [
    id 220
    label "cz&#322;owiek"
  ]
  node [
    id 221
    label "bli&#378;ni"
  ]
  node [
    id 222
    label "odpowiedni"
  ]
  node [
    id 223
    label "swojak"
  ]
  node [
    id 224
    label "samodzielny"
  ]
  node [
    id 225
    label "Rwanda"
  ]
  node [
    id 226
    label "Filipiny"
  ]
  node [
    id 227
    label "Monako"
  ]
  node [
    id 228
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 229
    label "Korea"
  ]
  node [
    id 230
    label "Czarnog&#243;ra"
  ]
  node [
    id 231
    label "Ghana"
  ]
  node [
    id 232
    label "Malawi"
  ]
  node [
    id 233
    label "Indonezja"
  ]
  node [
    id 234
    label "Bu&#322;garia"
  ]
  node [
    id 235
    label "Nauru"
  ]
  node [
    id 236
    label "Kenia"
  ]
  node [
    id 237
    label "Kambod&#380;a"
  ]
  node [
    id 238
    label "Mali"
  ]
  node [
    id 239
    label "Austria"
  ]
  node [
    id 240
    label "interior"
  ]
  node [
    id 241
    label "Armenia"
  ]
  node [
    id 242
    label "Fid&#380;i"
  ]
  node [
    id 243
    label "Tuwalu"
  ]
  node [
    id 244
    label "Etiopia"
  ]
  node [
    id 245
    label "Malezja"
  ]
  node [
    id 246
    label "Malta"
  ]
  node [
    id 247
    label "Tad&#380;ykistan"
  ]
  node [
    id 248
    label "Grenada"
  ]
  node [
    id 249
    label "Wehrlen"
  ]
  node [
    id 250
    label "para"
  ]
  node [
    id 251
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 252
    label "Rumunia"
  ]
  node [
    id 253
    label "Maroko"
  ]
  node [
    id 254
    label "Bhutan"
  ]
  node [
    id 255
    label "S&#322;owacja"
  ]
  node [
    id 256
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 257
    label "Seszele"
  ]
  node [
    id 258
    label "Kuwejt"
  ]
  node [
    id 259
    label "Arabia_Saudyjska"
  ]
  node [
    id 260
    label "Kanada"
  ]
  node [
    id 261
    label "Ekwador"
  ]
  node [
    id 262
    label "Japonia"
  ]
  node [
    id 263
    label "ziemia"
  ]
  node [
    id 264
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 265
    label "Hiszpania"
  ]
  node [
    id 266
    label "Wyspy_Marshalla"
  ]
  node [
    id 267
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 268
    label "D&#380;ibuti"
  ]
  node [
    id 269
    label "Botswana"
  ]
  node [
    id 270
    label "Wietnam"
  ]
  node [
    id 271
    label "Egipt"
  ]
  node [
    id 272
    label "Burkina_Faso"
  ]
  node [
    id 273
    label "Niemcy"
  ]
  node [
    id 274
    label "Khitai"
  ]
  node [
    id 275
    label "Macedonia"
  ]
  node [
    id 276
    label "Albania"
  ]
  node [
    id 277
    label "Madagaskar"
  ]
  node [
    id 278
    label "Bahrajn"
  ]
  node [
    id 279
    label "Jemen"
  ]
  node [
    id 280
    label "Lesoto"
  ]
  node [
    id 281
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 282
    label "Samoa"
  ]
  node [
    id 283
    label "Andora"
  ]
  node [
    id 284
    label "Chiny"
  ]
  node [
    id 285
    label "Cypr"
  ]
  node [
    id 286
    label "Wielka_Brytania"
  ]
  node [
    id 287
    label "Ukraina"
  ]
  node [
    id 288
    label "Paragwaj"
  ]
  node [
    id 289
    label "Trynidad_i_Tobago"
  ]
  node [
    id 290
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 291
    label "Libia"
  ]
  node [
    id 292
    label "Surinam"
  ]
  node [
    id 293
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 294
    label "Nigeria"
  ]
  node [
    id 295
    label "Australia"
  ]
  node [
    id 296
    label "Honduras"
  ]
  node [
    id 297
    label "Peru"
  ]
  node [
    id 298
    label "USA"
  ]
  node [
    id 299
    label "Bangladesz"
  ]
  node [
    id 300
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 301
    label "Kazachstan"
  ]
  node [
    id 302
    label "holoarktyka"
  ]
  node [
    id 303
    label "Nepal"
  ]
  node [
    id 304
    label "Sudan"
  ]
  node [
    id 305
    label "Irak"
  ]
  node [
    id 306
    label "San_Marino"
  ]
  node [
    id 307
    label "Burundi"
  ]
  node [
    id 308
    label "Dominikana"
  ]
  node [
    id 309
    label "Komory"
  ]
  node [
    id 310
    label "granica_pa&#324;stwa"
  ]
  node [
    id 311
    label "Gwatemala"
  ]
  node [
    id 312
    label "Antarktis"
  ]
  node [
    id 313
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 314
    label "Brunei"
  ]
  node [
    id 315
    label "Iran"
  ]
  node [
    id 316
    label "Zimbabwe"
  ]
  node [
    id 317
    label "Namibia"
  ]
  node [
    id 318
    label "Meksyk"
  ]
  node [
    id 319
    label "Kamerun"
  ]
  node [
    id 320
    label "zwrot"
  ]
  node [
    id 321
    label "Somalia"
  ]
  node [
    id 322
    label "Angola"
  ]
  node [
    id 323
    label "Gabon"
  ]
  node [
    id 324
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 325
    label "Nowa_Zelandia"
  ]
  node [
    id 326
    label "Mozambik"
  ]
  node [
    id 327
    label "Tunezja"
  ]
  node [
    id 328
    label "Tajwan"
  ]
  node [
    id 329
    label "Liban"
  ]
  node [
    id 330
    label "Jordania"
  ]
  node [
    id 331
    label "Tonga"
  ]
  node [
    id 332
    label "Czad"
  ]
  node [
    id 333
    label "Gwinea"
  ]
  node [
    id 334
    label "Liberia"
  ]
  node [
    id 335
    label "Belize"
  ]
  node [
    id 336
    label "Benin"
  ]
  node [
    id 337
    label "&#321;otwa"
  ]
  node [
    id 338
    label "Syria"
  ]
  node [
    id 339
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 340
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 341
    label "Dominika"
  ]
  node [
    id 342
    label "Antigua_i_Barbuda"
  ]
  node [
    id 343
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 344
    label "Hanower"
  ]
  node [
    id 345
    label "partia"
  ]
  node [
    id 346
    label "Afganistan"
  ]
  node [
    id 347
    label "W&#322;ochy"
  ]
  node [
    id 348
    label "Kiribati"
  ]
  node [
    id 349
    label "Szwajcaria"
  ]
  node [
    id 350
    label "Chorwacja"
  ]
  node [
    id 351
    label "Sahara_Zachodnia"
  ]
  node [
    id 352
    label "Tajlandia"
  ]
  node [
    id 353
    label "Salwador"
  ]
  node [
    id 354
    label "Bahamy"
  ]
  node [
    id 355
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 356
    label "S&#322;owenia"
  ]
  node [
    id 357
    label "Gambia"
  ]
  node [
    id 358
    label "Urugwaj"
  ]
  node [
    id 359
    label "Zair"
  ]
  node [
    id 360
    label "Erytrea"
  ]
  node [
    id 361
    label "Rosja"
  ]
  node [
    id 362
    label "Mauritius"
  ]
  node [
    id 363
    label "Niger"
  ]
  node [
    id 364
    label "Uganda"
  ]
  node [
    id 365
    label "Turkmenistan"
  ]
  node [
    id 366
    label "Turcja"
  ]
  node [
    id 367
    label "Irlandia"
  ]
  node [
    id 368
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 369
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 370
    label "Gwinea_Bissau"
  ]
  node [
    id 371
    label "Belgia"
  ]
  node [
    id 372
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 373
    label "Palau"
  ]
  node [
    id 374
    label "Barbados"
  ]
  node [
    id 375
    label "Wenezuela"
  ]
  node [
    id 376
    label "W&#281;gry"
  ]
  node [
    id 377
    label "Chile"
  ]
  node [
    id 378
    label "Argentyna"
  ]
  node [
    id 379
    label "Kolumbia"
  ]
  node [
    id 380
    label "Sierra_Leone"
  ]
  node [
    id 381
    label "Azerbejd&#380;an"
  ]
  node [
    id 382
    label "Kongo"
  ]
  node [
    id 383
    label "Pakistan"
  ]
  node [
    id 384
    label "Liechtenstein"
  ]
  node [
    id 385
    label "Nikaragua"
  ]
  node [
    id 386
    label "Senegal"
  ]
  node [
    id 387
    label "Indie"
  ]
  node [
    id 388
    label "Suazi"
  ]
  node [
    id 389
    label "Polska"
  ]
  node [
    id 390
    label "Algieria"
  ]
  node [
    id 391
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 392
    label "terytorium"
  ]
  node [
    id 393
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 394
    label "Jamajka"
  ]
  node [
    id 395
    label "Kostaryka"
  ]
  node [
    id 396
    label "Timor_Wschodni"
  ]
  node [
    id 397
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 398
    label "Kuba"
  ]
  node [
    id 399
    label "Mauretania"
  ]
  node [
    id 400
    label "Portoryko"
  ]
  node [
    id 401
    label "Brazylia"
  ]
  node [
    id 402
    label "Mo&#322;dawia"
  ]
  node [
    id 403
    label "organizacja"
  ]
  node [
    id 404
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 405
    label "Litwa"
  ]
  node [
    id 406
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 407
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 408
    label "Izrael"
  ]
  node [
    id 409
    label "Grecja"
  ]
  node [
    id 410
    label "Kirgistan"
  ]
  node [
    id 411
    label "Holandia"
  ]
  node [
    id 412
    label "Sri_Lanka"
  ]
  node [
    id 413
    label "Katar"
  ]
  node [
    id 414
    label "Mikronezja"
  ]
  node [
    id 415
    label "Laos"
  ]
  node [
    id 416
    label "Mongolia"
  ]
  node [
    id 417
    label "Malediwy"
  ]
  node [
    id 418
    label "Zambia"
  ]
  node [
    id 419
    label "Tanzania"
  ]
  node [
    id 420
    label "Gujana"
  ]
  node [
    id 421
    label "Uzbekistan"
  ]
  node [
    id 422
    label "Panama"
  ]
  node [
    id 423
    label "Czechy"
  ]
  node [
    id 424
    label "Gruzja"
  ]
  node [
    id 425
    label "Serbia"
  ]
  node [
    id 426
    label "Francja"
  ]
  node [
    id 427
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 428
    label "Togo"
  ]
  node [
    id 429
    label "Estonia"
  ]
  node [
    id 430
    label "Boliwia"
  ]
  node [
    id 431
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 432
    label "Oman"
  ]
  node [
    id 433
    label "Wyspy_Salomona"
  ]
  node [
    id 434
    label "Haiti"
  ]
  node [
    id 435
    label "Luksemburg"
  ]
  node [
    id 436
    label "Portugalia"
  ]
  node [
    id 437
    label "Birma"
  ]
  node [
    id 438
    label "Rodezja"
  ]
  node [
    id 439
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 440
    label "control"
  ]
  node [
    id 441
    label "ustawia&#263;"
  ]
  node [
    id 442
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 443
    label "motywowa&#263;"
  ]
  node [
    id 444
    label "order"
  ]
  node [
    id 445
    label "administrowa&#263;"
  ]
  node [
    id 446
    label "manipulate"
  ]
  node [
    id 447
    label "give"
  ]
  node [
    id 448
    label "indicate"
  ]
  node [
    id 449
    label "przeznacza&#263;"
  ]
  node [
    id 450
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 451
    label "match"
  ]
  node [
    id 452
    label "sterowa&#263;"
  ]
  node [
    id 453
    label "wysy&#322;a&#263;"
  ]
  node [
    id 454
    label "zwierzchnik"
  ]
  node [
    id 455
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 456
    label "obserwacja"
  ]
  node [
    id 457
    label "podstawa"
  ]
  node [
    id 458
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 459
    label "dominion"
  ]
  node [
    id 460
    label "qualification"
  ]
  node [
    id 461
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 462
    label "opis"
  ]
  node [
    id 463
    label "regu&#322;a_Allena"
  ]
  node [
    id 464
    label "normalizacja"
  ]
  node [
    id 465
    label "regu&#322;a_Glogera"
  ]
  node [
    id 466
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 467
    label "base"
  ]
  node [
    id 468
    label "prawo_Mendla"
  ]
  node [
    id 469
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 470
    label "twierdzenie"
  ]
  node [
    id 471
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 472
    label "prawo"
  ]
  node [
    id 473
    label "occupation"
  ]
  node [
    id 474
    label "zasada_d'Alemberta"
  ]
  node [
    id 475
    label "spok&#243;j"
  ]
  node [
    id 476
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 477
    label "g&#322;adko&#347;&#263;"
  ]
  node [
    id 478
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 479
    label "poj&#281;cie"
  ]
  node [
    id 480
    label "podobie&#324;stwo"
  ]
  node [
    id 481
    label "wsp&#243;lno&#347;&#263;"
  ]
  node [
    id 482
    label "dwustronnie"
  ]
  node [
    id 483
    label "wzajemny"
  ]
  node [
    id 484
    label "dobro"
  ]
  node [
    id 485
    label "zaleta"
  ]
  node [
    id 486
    label "agree"
  ]
  node [
    id 487
    label "porozumiewa&#263;_si&#281;"
  ]
  node [
    id 488
    label "kontaktowa&#263;"
  ]
  node [
    id 489
    label "ask"
  ]
  node [
    id 490
    label "skr&#281;canie"
  ]
  node [
    id 491
    label "voice"
  ]
  node [
    id 492
    label "forma"
  ]
  node [
    id 493
    label "internet"
  ]
  node [
    id 494
    label "skr&#281;ci&#263;"
  ]
  node [
    id 495
    label "kartka"
  ]
  node [
    id 496
    label "orientowa&#263;"
  ]
  node [
    id 497
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 498
    label "powierzchnia"
  ]
  node [
    id 499
    label "plik"
  ]
  node [
    id 500
    label "bok"
  ]
  node [
    id 501
    label "pagina"
  ]
  node [
    id 502
    label "orientowanie"
  ]
  node [
    id 503
    label "fragment"
  ]
  node [
    id 504
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 505
    label "s&#261;d"
  ]
  node [
    id 506
    label "skr&#281;ca&#263;"
  ]
  node [
    id 507
    label "g&#243;ra"
  ]
  node [
    id 508
    label "serwis_internetowy"
  ]
  node [
    id 509
    label "orientacja"
  ]
  node [
    id 510
    label "linia"
  ]
  node [
    id 511
    label "skr&#281;cenie"
  ]
  node [
    id 512
    label "layout"
  ]
  node [
    id 513
    label "zorientowa&#263;"
  ]
  node [
    id 514
    label "zorientowanie"
  ]
  node [
    id 515
    label "obiekt"
  ]
  node [
    id 516
    label "podmiot"
  ]
  node [
    id 517
    label "ty&#322;"
  ]
  node [
    id 518
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 519
    label "logowanie"
  ]
  node [
    id 520
    label "adres_internetowy"
  ]
  node [
    id 521
    label "uj&#281;cie"
  ]
  node [
    id 522
    label "prz&#243;d"
  ]
  node [
    id 523
    label "posta&#263;"
  ]
  node [
    id 524
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 525
    label "prosecute"
  ]
  node [
    id 526
    label "wsp&#243;&#322;pracownictwo"
  ]
  node [
    id 527
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 528
    label "organogeneza"
  ]
  node [
    id 529
    label "Komitet_Region&#243;w"
  ]
  node [
    id 530
    label "Izba_Konsyliarska"
  ]
  node [
    id 531
    label "budowa"
  ]
  node [
    id 532
    label "okolica"
  ]
  node [
    id 533
    label "zesp&#243;&#322;"
  ]
  node [
    id 534
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 535
    label "jednostka_organizacyjna"
  ]
  node [
    id 536
    label "dekortykacja"
  ]
  node [
    id 537
    label "struktura_anatomiczna"
  ]
  node [
    id 538
    label "tkanka"
  ]
  node [
    id 539
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 540
    label "stomia"
  ]
  node [
    id 541
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 542
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 543
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 544
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 545
    label "tw&#243;r"
  ]
  node [
    id 546
    label "chemical_bond"
  ]
  node [
    id 547
    label "tarcza"
  ]
  node [
    id 548
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 549
    label "borowiec"
  ]
  node [
    id 550
    label "obstawienie"
  ]
  node [
    id 551
    label "formacja"
  ]
  node [
    id 552
    label "ubezpieczenie"
  ]
  node [
    id 553
    label "obstawia&#263;"
  ]
  node [
    id 554
    label "obstawianie"
  ]
  node [
    id 555
    label "transportacja"
  ]
  node [
    id 556
    label "BHP"
  ]
  node [
    id 557
    label "katapultowa&#263;"
  ]
  node [
    id 558
    label "ubezpiecza&#263;"
  ]
  node [
    id 559
    label "stan"
  ]
  node [
    id 560
    label "cecha"
  ]
  node [
    id 561
    label "ubezpieczy&#263;"
  ]
  node [
    id 562
    label "safety"
  ]
  node [
    id 563
    label "katapultowanie"
  ]
  node [
    id 564
    label "ubezpieczanie"
  ]
  node [
    id 565
    label "test_zderzeniowy"
  ]
  node [
    id 566
    label "styl_architektoniczny"
  ]
  node [
    id 567
    label "relacja"
  ]
  node [
    id 568
    label "struktura"
  ]
  node [
    id 569
    label "jawny"
  ]
  node [
    id 570
    label "upublicznienie"
  ]
  node [
    id 571
    label "upublicznianie"
  ]
  node [
    id 572
    label "publicznie"
  ]
  node [
    id 573
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 574
    label "granica"
  ]
  node [
    id 575
    label "circle"
  ]
  node [
    id 576
    label "podzakres"
  ]
  node [
    id 577
    label "dziedzina"
  ]
  node [
    id 578
    label "desygnat"
  ]
  node [
    id 579
    label "sfera"
  ]
  node [
    id 580
    label "wielko&#347;&#263;"
  ]
  node [
    id 581
    label "robienie"
  ]
  node [
    id 582
    label "czynno&#347;&#263;"
  ]
  node [
    id 583
    label "chronienie_si&#281;"
  ]
  node [
    id 584
    label "kolejny"
  ]
  node [
    id 585
    label "inaczej"
  ]
  node [
    id 586
    label "r&#243;&#380;ny"
  ]
  node [
    id 587
    label "inszy"
  ]
  node [
    id 588
    label "osobno"
  ]
  node [
    id 589
    label "kategoria_gramatyczna"
  ]
  node [
    id 590
    label "autorament"
  ]
  node [
    id 591
    label "jednostka_systematyczna"
  ]
  node [
    id 592
    label "fashion"
  ]
  node [
    id 593
    label "rodzina"
  ]
  node [
    id 594
    label "variety"
  ]
  node [
    id 595
    label "gro&#378;ny"
  ]
  node [
    id 596
    label "trudny"
  ]
  node [
    id 597
    label "du&#380;y"
  ]
  node [
    id 598
    label "spowa&#380;nienie"
  ]
  node [
    id 599
    label "prawdziwy"
  ]
  node [
    id 600
    label "powa&#380;nienie"
  ]
  node [
    id 601
    label "powa&#380;nie"
  ]
  node [
    id 602
    label "ci&#281;&#380;ko"
  ]
  node [
    id 603
    label "ci&#281;&#380;ki"
  ]
  node [
    id 604
    label "okre&#347;lony"
  ]
  node [
    id 605
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 606
    label "brudny"
  ]
  node [
    id 607
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 608
    label "sprawstwo"
  ]
  node [
    id 609
    label "crime"
  ]
  node [
    id 610
    label "ojciec"
  ]
  node [
    id 611
    label "zwalcza&#263;"
  ]
  node [
    id 612
    label "&#347;rodki"
  ]
  node [
    id 613
    label "odurza&#263;"
  ]
  node [
    id 614
    label "i"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 5
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 47
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 610
  ]
  edge [
    source 19
    target 611
  ]
  edge [
    source 19
    target 612
  ]
  edge [
    source 19
    target 613
  ]
  edge [
    source 19
    target 614
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 610
  ]
  edge [
    source 20
    target 611
  ]
  edge [
    source 20
    target 612
  ]
  edge [
    source 20
    target 613
  ]
  edge [
    source 20
    target 614
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 144
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 201
  ]
  edge [
    source 21
    target 202
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 205
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 23
    target 211
  ]
  edge [
    source 23
    target 212
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 610
  ]
  edge [
    source 23
    target 611
  ]
  edge [
    source 23
    target 612
  ]
  edge [
    source 23
    target 613
  ]
  edge [
    source 23
    target 614
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 610
  ]
  edge [
    source 24
    target 611
  ]
  edge [
    source 24
    target 612
  ]
  edge [
    source 24
    target 613
  ]
  edge [
    source 24
    target 614
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 26
    target 217
  ]
  edge [
    source 26
    target 218
  ]
  edge [
    source 26
    target 219
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 225
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 28
    target 227
  ]
  edge [
    source 28
    target 228
  ]
  edge [
    source 28
    target 229
  ]
  edge [
    source 28
    target 230
  ]
  edge [
    source 28
    target 231
  ]
  edge [
    source 28
    target 232
  ]
  edge [
    source 28
    target 233
  ]
  edge [
    source 28
    target 234
  ]
  edge [
    source 28
    target 235
  ]
  edge [
    source 28
    target 236
  ]
  edge [
    source 28
    target 237
  ]
  edge [
    source 28
    target 238
  ]
  edge [
    source 28
    target 239
  ]
  edge [
    source 28
    target 240
  ]
  edge [
    source 28
    target 241
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 262
  ]
  edge [
    source 28
    target 263
  ]
  edge [
    source 28
    target 264
  ]
  edge [
    source 28
    target 265
  ]
  edge [
    source 28
    target 266
  ]
  edge [
    source 28
    target 267
  ]
  edge [
    source 28
    target 268
  ]
  edge [
    source 28
    target 269
  ]
  edge [
    source 28
    target 178
  ]
  edge [
    source 28
    target 270
  ]
  edge [
    source 28
    target 271
  ]
  edge [
    source 28
    target 272
  ]
  edge [
    source 28
    target 273
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 275
  ]
  edge [
    source 28
    target 276
  ]
  edge [
    source 28
    target 277
  ]
  edge [
    source 28
    target 278
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 281
  ]
  edge [
    source 28
    target 282
  ]
  edge [
    source 28
    target 283
  ]
  edge [
    source 28
    target 284
  ]
  edge [
    source 28
    target 285
  ]
  edge [
    source 28
    target 286
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 28
    target 288
  ]
  edge [
    source 28
    target 289
  ]
  edge [
    source 28
    target 290
  ]
  edge [
    source 28
    target 291
  ]
  edge [
    source 28
    target 292
  ]
  edge [
    source 28
    target 293
  ]
  edge [
    source 28
    target 294
  ]
  edge [
    source 28
    target 295
  ]
  edge [
    source 28
    target 296
  ]
  edge [
    source 28
    target 297
  ]
  edge [
    source 28
    target 298
  ]
  edge [
    source 28
    target 299
  ]
  edge [
    source 28
    target 300
  ]
  edge [
    source 28
    target 301
  ]
  edge [
    source 28
    target 302
  ]
  edge [
    source 28
    target 303
  ]
  edge [
    source 28
    target 304
  ]
  edge [
    source 28
    target 305
  ]
  edge [
    source 28
    target 306
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 28
    target 308
  ]
  edge [
    source 28
    target 309
  ]
  edge [
    source 28
    target 310
  ]
  edge [
    source 28
    target 311
  ]
  edge [
    source 28
    target 312
  ]
  edge [
    source 28
    target 313
  ]
  edge [
    source 28
    target 314
  ]
  edge [
    source 28
    target 315
  ]
  edge [
    source 28
    target 316
  ]
  edge [
    source 28
    target 317
  ]
  edge [
    source 28
    target 318
  ]
  edge [
    source 28
    target 319
  ]
  edge [
    source 28
    target 320
  ]
  edge [
    source 28
    target 321
  ]
  edge [
    source 28
    target 322
  ]
  edge [
    source 28
    target 323
  ]
  edge [
    source 28
    target 324
  ]
  edge [
    source 28
    target 325
  ]
  edge [
    source 28
    target 326
  ]
  edge [
    source 28
    target 327
  ]
  edge [
    source 28
    target 328
  ]
  edge [
    source 28
    target 329
  ]
  edge [
    source 28
    target 330
  ]
  edge [
    source 28
    target 331
  ]
  edge [
    source 28
    target 332
  ]
  edge [
    source 28
    target 333
  ]
  edge [
    source 28
    target 334
  ]
  edge [
    source 28
    target 335
  ]
  edge [
    source 28
    target 336
  ]
  edge [
    source 28
    target 337
  ]
  edge [
    source 28
    target 338
  ]
  edge [
    source 28
    target 339
  ]
  edge [
    source 28
    target 340
  ]
  edge [
    source 28
    target 341
  ]
  edge [
    source 28
    target 342
  ]
  edge [
    source 28
    target 343
  ]
  edge [
    source 28
    target 344
  ]
  edge [
    source 28
    target 345
  ]
  edge [
    source 28
    target 346
  ]
  edge [
    source 28
    target 347
  ]
  edge [
    source 28
    target 348
  ]
  edge [
    source 28
    target 349
  ]
  edge [
    source 28
    target 350
  ]
  edge [
    source 28
    target 351
  ]
  edge [
    source 28
    target 352
  ]
  edge [
    source 28
    target 353
  ]
  edge [
    source 28
    target 354
  ]
  edge [
    source 28
    target 355
  ]
  edge [
    source 28
    target 356
  ]
  edge [
    source 28
    target 357
  ]
  edge [
    source 28
    target 358
  ]
  edge [
    source 28
    target 359
  ]
  edge [
    source 28
    target 360
  ]
  edge [
    source 28
    target 361
  ]
  edge [
    source 28
    target 362
  ]
  edge [
    source 28
    target 363
  ]
  edge [
    source 28
    target 364
  ]
  edge [
    source 28
    target 365
  ]
  edge [
    source 28
    target 366
  ]
  edge [
    source 28
    target 367
  ]
  edge [
    source 28
    target 368
  ]
  edge [
    source 28
    target 369
  ]
  edge [
    source 28
    target 370
  ]
  edge [
    source 28
    target 371
  ]
  edge [
    source 28
    target 372
  ]
  edge [
    source 28
    target 373
  ]
  edge [
    source 28
    target 374
  ]
  edge [
    source 28
    target 375
  ]
  edge [
    source 28
    target 376
  ]
  edge [
    source 28
    target 377
  ]
  edge [
    source 28
    target 378
  ]
  edge [
    source 28
    target 379
  ]
  edge [
    source 28
    target 380
  ]
  edge [
    source 28
    target 381
  ]
  edge [
    source 28
    target 382
  ]
  edge [
    source 28
    target 383
  ]
  edge [
    source 28
    target 384
  ]
  edge [
    source 28
    target 385
  ]
  edge [
    source 28
    target 386
  ]
  edge [
    source 28
    target 387
  ]
  edge [
    source 28
    target 388
  ]
  edge [
    source 28
    target 389
  ]
  edge [
    source 28
    target 390
  ]
  edge [
    source 28
    target 391
  ]
  edge [
    source 28
    target 392
  ]
  edge [
    source 28
    target 393
  ]
  edge [
    source 28
    target 394
  ]
  edge [
    source 28
    target 395
  ]
  edge [
    source 28
    target 396
  ]
  edge [
    source 28
    target 397
  ]
  edge [
    source 28
    target 398
  ]
  edge [
    source 28
    target 399
  ]
  edge [
    source 28
    target 400
  ]
  edge [
    source 28
    target 401
  ]
  edge [
    source 28
    target 402
  ]
  edge [
    source 28
    target 403
  ]
  edge [
    source 28
    target 404
  ]
  edge [
    source 28
    target 405
  ]
  edge [
    source 28
    target 406
  ]
  edge [
    source 28
    target 407
  ]
  edge [
    source 28
    target 408
  ]
  edge [
    source 28
    target 409
  ]
  edge [
    source 28
    target 410
  ]
  edge [
    source 28
    target 411
  ]
  edge [
    source 28
    target 412
  ]
  edge [
    source 28
    target 413
  ]
  edge [
    source 28
    target 414
  ]
  edge [
    source 28
    target 415
  ]
  edge [
    source 28
    target 416
  ]
  edge [
    source 28
    target 417
  ]
  edge [
    source 28
    target 418
  ]
  edge [
    source 28
    target 419
  ]
  edge [
    source 28
    target 420
  ]
  edge [
    source 28
    target 421
  ]
  edge [
    source 28
    target 422
  ]
  edge [
    source 28
    target 423
  ]
  edge [
    source 28
    target 424
  ]
  edge [
    source 28
    target 425
  ]
  edge [
    source 28
    target 426
  ]
  edge [
    source 28
    target 427
  ]
  edge [
    source 28
    target 428
  ]
  edge [
    source 28
    target 429
  ]
  edge [
    source 28
    target 430
  ]
  edge [
    source 28
    target 431
  ]
  edge [
    source 28
    target 432
  ]
  edge [
    source 28
    target 433
  ]
  edge [
    source 28
    target 434
  ]
  edge [
    source 28
    target 435
  ]
  edge [
    source 28
    target 436
  ]
  edge [
    source 28
    target 437
  ]
  edge [
    source 28
    target 438
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 439
  ]
  edge [
    source 29
    target 440
  ]
  edge [
    source 29
    target 441
  ]
  edge [
    source 29
    target 442
  ]
  edge [
    source 29
    target 443
  ]
  edge [
    source 29
    target 444
  ]
  edge [
    source 29
    target 445
  ]
  edge [
    source 29
    target 446
  ]
  edge [
    source 29
    target 447
  ]
  edge [
    source 29
    target 448
  ]
  edge [
    source 29
    target 449
  ]
  edge [
    source 29
    target 450
  ]
  edge [
    source 29
    target 451
  ]
  edge [
    source 29
    target 452
  ]
  edge [
    source 29
    target 453
  ]
  edge [
    source 29
    target 454
  ]
  edge [
    source 29
    target 455
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 456
  ]
  edge [
    source 31
    target 80
  ]
  edge [
    source 31
    target 457
  ]
  edge [
    source 31
    target 458
  ]
  edge [
    source 31
    target 57
  ]
  edge [
    source 31
    target 459
  ]
  edge [
    source 31
    target 460
  ]
  edge [
    source 31
    target 461
  ]
  edge [
    source 31
    target 462
  ]
  edge [
    source 31
    target 463
  ]
  edge [
    source 31
    target 464
  ]
  edge [
    source 31
    target 465
  ]
  edge [
    source 31
    target 466
  ]
  edge [
    source 31
    target 86
  ]
  edge [
    source 31
    target 467
  ]
  edge [
    source 31
    target 201
  ]
  edge [
    source 31
    target 83
  ]
  edge [
    source 31
    target 468
  ]
  edge [
    source 31
    target 469
  ]
  edge [
    source 31
    target 84
  ]
  edge [
    source 31
    target 470
  ]
  edge [
    source 31
    target 471
  ]
  edge [
    source 31
    target 472
  ]
  edge [
    source 31
    target 473
  ]
  edge [
    source 31
    target 474
  ]
  edge [
    source 31
    target 44
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 475
  ]
  edge [
    source 32
    target 476
  ]
  edge [
    source 32
    target 477
  ]
  edge [
    source 32
    target 478
  ]
  edge [
    source 32
    target 479
  ]
  edge [
    source 32
    target 480
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 481
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 482
  ]
  edge [
    source 34
    target 483
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 34
    target 44
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 484
  ]
  edge [
    source 35
    target 485
  ]
  edge [
    source 36
    target 486
  ]
  edge [
    source 36
    target 487
  ]
  edge [
    source 36
    target 488
  ]
  edge [
    source 36
    target 489
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 490
  ]
  edge [
    source 37
    target 491
  ]
  edge [
    source 37
    target 492
  ]
  edge [
    source 37
    target 493
  ]
  edge [
    source 37
    target 494
  ]
  edge [
    source 37
    target 495
  ]
  edge [
    source 37
    target 496
  ]
  edge [
    source 37
    target 497
  ]
  edge [
    source 37
    target 498
  ]
  edge [
    source 37
    target 499
  ]
  edge [
    source 37
    target 500
  ]
  edge [
    source 37
    target 501
  ]
  edge [
    source 37
    target 502
  ]
  edge [
    source 37
    target 503
  ]
  edge [
    source 37
    target 504
  ]
  edge [
    source 37
    target 505
  ]
  edge [
    source 37
    target 506
  ]
  edge [
    source 37
    target 507
  ]
  edge [
    source 37
    target 508
  ]
  edge [
    source 37
    target 509
  ]
  edge [
    source 37
    target 510
  ]
  edge [
    source 37
    target 511
  ]
  edge [
    source 37
    target 512
  ]
  edge [
    source 37
    target 513
  ]
  edge [
    source 37
    target 514
  ]
  edge [
    source 37
    target 515
  ]
  edge [
    source 37
    target 516
  ]
  edge [
    source 37
    target 517
  ]
  edge [
    source 37
    target 518
  ]
  edge [
    source 37
    target 519
  ]
  edge [
    source 37
    target 520
  ]
  edge [
    source 37
    target 521
  ]
  edge [
    source 37
    target 522
  ]
  edge [
    source 37
    target 523
  ]
  edge [
    source 38
    target 450
  ]
  edge [
    source 38
    target 524
  ]
  edge [
    source 38
    target 525
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 526
  ]
  edge [
    source 39
    target 527
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 91
  ]
  edge [
    source 41
    target 528
  ]
  edge [
    source 41
    target 529
  ]
  edge [
    source 41
    target 530
  ]
  edge [
    source 41
    target 531
  ]
  edge [
    source 41
    target 532
  ]
  edge [
    source 41
    target 533
  ]
  edge [
    source 41
    target 534
  ]
  edge [
    source 41
    target 535
  ]
  edge [
    source 41
    target 536
  ]
  edge [
    source 41
    target 537
  ]
  edge [
    source 41
    target 538
  ]
  edge [
    source 41
    target 539
  ]
  edge [
    source 41
    target 518
  ]
  edge [
    source 41
    target 540
  ]
  edge [
    source 41
    target 541
  ]
  edge [
    source 41
    target 542
  ]
  edge [
    source 41
    target 543
  ]
  edge [
    source 41
    target 544
  ]
  edge [
    source 41
    target 545
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 546
  ]
  edge [
    source 42
    target 547
  ]
  edge [
    source 42
    target 527
  ]
  edge [
    source 42
    target 515
  ]
  edge [
    source 42
    target 548
  ]
  edge [
    source 42
    target 549
  ]
  edge [
    source 42
    target 550
  ]
  edge [
    source 42
    target 551
  ]
  edge [
    source 42
    target 552
  ]
  edge [
    source 42
    target 553
  ]
  edge [
    source 42
    target 554
  ]
  edge [
    source 42
    target 555
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 556
  ]
  edge [
    source 43
    target 557
  ]
  edge [
    source 43
    target 558
  ]
  edge [
    source 43
    target 559
  ]
  edge [
    source 43
    target 560
  ]
  edge [
    source 43
    target 552
  ]
  edge [
    source 43
    target 561
  ]
  edge [
    source 43
    target 562
  ]
  edge [
    source 43
    target 563
  ]
  edge [
    source 43
    target 564
  ]
  edge [
    source 43
    target 565
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 91
  ]
  edge [
    source 44
    target 566
  ]
  edge [
    source 44
    target 559
  ]
  edge [
    source 44
    target 464
  ]
  edge [
    source 44
    target 560
  ]
  edge [
    source 44
    target 567
  ]
  edge [
    source 44
    target 568
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 569
  ]
  edge [
    source 45
    target 570
  ]
  edge [
    source 45
    target 571
  ]
  edge [
    source 45
    target 572
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 573
  ]
  edge [
    source 46
    target 574
  ]
  edge [
    source 46
    target 575
  ]
  edge [
    source 46
    target 576
  ]
  edge [
    source 46
    target 95
  ]
  edge [
    source 46
    target 577
  ]
  edge [
    source 46
    target 578
  ]
  edge [
    source 46
    target 579
  ]
  edge [
    source 46
    target 580
  ]
  edge [
    source 47
    target 581
  ]
  edge [
    source 47
    target 582
  ]
  edge [
    source 47
    target 583
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 584
  ]
  edge [
    source 48
    target 585
  ]
  edge [
    source 48
    target 586
  ]
  edge [
    source 48
    target 587
  ]
  edge [
    source 48
    target 588
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 589
  ]
  edge [
    source 49
    target 590
  ]
  edge [
    source 49
    target 591
  ]
  edge [
    source 49
    target 592
  ]
  edge [
    source 49
    target 593
  ]
  edge [
    source 49
    target 594
  ]
  edge [
    source 50
    target 595
  ]
  edge [
    source 50
    target 596
  ]
  edge [
    source 50
    target 597
  ]
  edge [
    source 50
    target 598
  ]
  edge [
    source 50
    target 599
  ]
  edge [
    source 50
    target 600
  ]
  edge [
    source 50
    target 601
  ]
  edge [
    source 50
    target 602
  ]
  edge [
    source 50
    target 603
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 604
  ]
  edge [
    source 51
    target 605
  ]
  edge [
    source 52
    target 606
  ]
  edge [
    source 52
    target 607
  ]
  edge [
    source 52
    target 608
  ]
  edge [
    source 52
    target 609
  ]
  edge [
    source 610
    target 611
  ]
  edge [
    source 610
    target 612
  ]
  edge [
    source 610
    target 613
  ]
  edge [
    source 610
    target 614
  ]
  edge [
    source 611
    target 612
  ]
  edge [
    source 611
    target 613
  ]
  edge [
    source 611
    target 614
  ]
  edge [
    source 612
    target 613
  ]
  edge [
    source 612
    target 614
  ]
  edge [
    source 613
    target 614
  ]
]
