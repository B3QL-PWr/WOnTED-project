graph [
  maxDegree 16
  minDegree 1
  meanDegree 2
  density 0.03571428571428571
  graphCliqueNumber 2
  node [
    id 0
    label "wczoraj"
    origin "text"
  ]
  node [
    id 1
    label "apel"
    origin "text"
  ]
  node [
    id 2
    label "wykop"
    origin "text"
  ]
  node [
    id 3
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 4
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 5
    label "odpowiada&#263;"
    origin "text"
  ]
  node [
    id 6
    label "doba"
  ]
  node [
    id 7
    label "dawno"
  ]
  node [
    id 8
    label "niedawno"
  ]
  node [
    id 9
    label "uderzenie"
  ]
  node [
    id 10
    label "zdyscyplinowanie"
  ]
  node [
    id 11
    label "sygna&#322;"
  ]
  node [
    id 12
    label "bid"
  ]
  node [
    id 13
    label "recoil"
  ]
  node [
    id 14
    label "spotkanie"
  ]
  node [
    id 15
    label "zbi&#243;rka"
  ]
  node [
    id 16
    label "pro&#347;ba"
  ]
  node [
    id 17
    label "pies_my&#347;liwski"
  ]
  node [
    id 18
    label "znak"
  ]
  node [
    id 19
    label "szermierka"
  ]
  node [
    id 20
    label "odwa&#322;"
  ]
  node [
    id 21
    label "chody"
  ]
  node [
    id 22
    label "grodzisko"
  ]
  node [
    id 23
    label "budowa"
  ]
  node [
    id 24
    label "kopniak"
  ]
  node [
    id 25
    label "wyrobisko"
  ]
  node [
    id 26
    label "zrzutowy"
  ]
  node [
    id 27
    label "szaniec"
  ]
  node [
    id 28
    label "odk&#322;ad"
  ]
  node [
    id 29
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 30
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 31
    label "dzi&#347;"
  ]
  node [
    id 32
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 33
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 34
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 35
    label "ministerium"
  ]
  node [
    id 36
    label "resort"
  ]
  node [
    id 37
    label "urz&#261;d"
  ]
  node [
    id 38
    label "MSW"
  ]
  node [
    id 39
    label "departament"
  ]
  node [
    id 40
    label "NKWD"
  ]
  node [
    id 41
    label "report"
  ]
  node [
    id 42
    label "by&#263;"
  ]
  node [
    id 43
    label "dawa&#263;"
  ]
  node [
    id 44
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 45
    label "reagowa&#263;"
  ]
  node [
    id 46
    label "contend"
  ]
  node [
    id 47
    label "ponosi&#263;"
  ]
  node [
    id 48
    label "impart"
  ]
  node [
    id 49
    label "react"
  ]
  node [
    id 50
    label "tone"
  ]
  node [
    id 51
    label "equate"
  ]
  node [
    id 52
    label "pytanie"
  ]
  node [
    id 53
    label "powodowa&#263;"
  ]
  node [
    id 54
    label "answer"
  ]
  node [
    id 55
    label "puls"
  ]
  node [
    id 56
    label "biznes"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 55
    target 56
  ]
]
