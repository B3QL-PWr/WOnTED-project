graph [
  maxDegree 17
  minDegree 1
  meanDegree 2.080402010050251
  density 0.010507080858839652
  graphCliqueNumber 2
  node [
    id 0
    label "nasz"
    origin "text"
  ]
  node [
    id 1
    label "woodstock"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "totalnie"
    origin "text"
  ]
  node [
    id 4
    label "przer&#261;ba&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "media"
    origin "text"
  ]
  node [
    id 7
    label "dziennikarz"
    origin "text"
  ]
  node [
    id 8
    label "traktowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "nasa"
    origin "text"
  ]
  node [
    id 10
    label "jak"
    origin "text"
  ]
  node [
    id 11
    label "banda"
    origin "text"
  ]
  node [
    id 12
    label "oszo&#322;om"
    origin "text"
  ]
  node [
    id 13
    label "oburza&#263;"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;"
    origin "text"
  ]
  node [
    id 15
    label "ch&#322;opak"
    origin "text"
  ]
  node [
    id 16
    label "dred"
    origin "text"
  ]
  node [
    id 17
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 18
    label "teraz"
    origin "text"
  ]
  node [
    id 19
    label "sprostowa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "siada&#263;"
    origin "text"
  ]
  node [
    id 21
    label "pisz"
    origin "text"
  ]
  node [
    id 22
    label "gazeta"
    origin "text"
  ]
  node [
    id 23
    label "powiedzie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 25
    label "machn&#261;&#263;"
    origin "text"
  ]
  node [
    id 26
    label "fajny"
    origin "text"
  ]
  node [
    id 27
    label "tekst"
    origin "text"
  ]
  node [
    id 28
    label "opublikowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 29
    label "inny"
    origin "text"
  ]
  node [
    id 30
    label "qmamie"
    origin "text"
  ]
  node [
    id 31
    label "festiwalowy"
    origin "text"
  ]
  node [
    id 32
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 33
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 34
    label "serwis"
    origin "text"
  ]
  node [
    id 35
    label "mama"
    origin "text"
  ]
  node [
    id 36
    label "plac"
    origin "text"
  ]
  node [
    id 37
    label "czyj&#347;"
  ]
  node [
    id 38
    label "m&#261;&#380;"
  ]
  node [
    id 39
    label "totalitarny"
  ]
  node [
    id 40
    label "totalny"
  ]
  node [
    id 41
    label "absolutnie"
  ]
  node [
    id 42
    label "wyci&#261;&#263;"
  ]
  node [
    id 43
    label "wyr&#261;ba&#263;"
  ]
  node [
    id 44
    label "przeci&#261;&#263;"
  ]
  node [
    id 45
    label "majority"
  ]
  node [
    id 46
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 47
    label "przekazior"
  ]
  node [
    id 48
    label "mass-media"
  ]
  node [
    id 49
    label "uzbrajanie"
  ]
  node [
    id 50
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 51
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 52
    label "medium"
  ]
  node [
    id 53
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 54
    label "nowiniarz"
  ]
  node [
    id 55
    label "akredytowa&#263;"
  ]
  node [
    id 56
    label "akredytowanie"
  ]
  node [
    id 57
    label "bran&#380;owiec"
  ]
  node [
    id 58
    label "publicysta"
  ]
  node [
    id 59
    label "use"
  ]
  node [
    id 60
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 61
    label "robi&#263;"
  ]
  node [
    id 62
    label "dotyczy&#263;"
  ]
  node [
    id 63
    label "poddawa&#263;"
  ]
  node [
    id 64
    label "byd&#322;o"
  ]
  node [
    id 65
    label "zobo"
  ]
  node [
    id 66
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 67
    label "yakalo"
  ]
  node [
    id 68
    label "dzo"
  ]
  node [
    id 69
    label "band"
  ]
  node [
    id 70
    label "granda"
  ]
  node [
    id 71
    label "organizacja"
  ]
  node [
    id 72
    label "gromada"
  ]
  node [
    id 73
    label "towarzystwo"
  ]
  node [
    id 74
    label "kraw&#281;d&#378;"
  ]
  node [
    id 75
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 76
    label "gang"
  ]
  node [
    id 77
    label "crew"
  ]
  node [
    id 78
    label "st&#243;&#322;_bilardowy"
  ]
  node [
    id 79
    label "package"
  ]
  node [
    id 80
    label "ogrodzenie"
  ]
  node [
    id 81
    label "flight"
  ]
  node [
    id 82
    label "chory_na_g&#322;ow&#281;"
  ]
  node [
    id 83
    label "fanatyk"
  ]
  node [
    id 84
    label "disgust"
  ]
  node [
    id 85
    label "dzia&#322;a&#263;_na_nerwy"
  ]
  node [
    id 86
    label "porusza&#263;"
  ]
  node [
    id 87
    label "cz&#322;owiek"
  ]
  node [
    id 88
    label "pomocnik"
  ]
  node [
    id 89
    label "g&#243;wniarz"
  ]
  node [
    id 90
    label "&#347;l&#261;ski"
  ]
  node [
    id 91
    label "m&#322;odzieniec"
  ]
  node [
    id 92
    label "kajtek"
  ]
  node [
    id 93
    label "kawaler"
  ]
  node [
    id 94
    label "usynawianie"
  ]
  node [
    id 95
    label "dziecko"
  ]
  node [
    id 96
    label "okrzos"
  ]
  node [
    id 97
    label "usynowienie"
  ]
  node [
    id 98
    label "sympatia"
  ]
  node [
    id 99
    label "pederasta"
  ]
  node [
    id 100
    label "synek"
  ]
  node [
    id 101
    label "boyfriend"
  ]
  node [
    id 102
    label "dredy"
  ]
  node [
    id 103
    label "pasemko"
  ]
  node [
    id 104
    label "by&#263;"
  ]
  node [
    id 105
    label "uprawi&#263;"
  ]
  node [
    id 106
    label "gotowy"
  ]
  node [
    id 107
    label "might"
  ]
  node [
    id 108
    label "chwila"
  ]
  node [
    id 109
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 110
    label "correct"
  ]
  node [
    id 111
    label "skorygowa&#263;"
  ]
  node [
    id 112
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 113
    label "zajmowa&#263;"
  ]
  node [
    id 114
    label "l&#261;dowa&#263;"
  ]
  node [
    id 115
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 116
    label "bring"
  ]
  node [
    id 117
    label "psu&#263;_si&#281;"
  ]
  node [
    id 118
    label "perch"
  ]
  node [
    id 119
    label "plasowa&#263;_si&#281;"
  ]
  node [
    id 120
    label "sit"
  ]
  node [
    id 121
    label "prasa"
  ]
  node [
    id 122
    label "redakcja"
  ]
  node [
    id 123
    label "tytu&#322;"
  ]
  node [
    id 124
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 125
    label "czasopismo"
  ]
  node [
    id 126
    label "take_care"
  ]
  node [
    id 127
    label "troska&#263;_si&#281;"
  ]
  node [
    id 128
    label "zamierza&#263;"
  ]
  node [
    id 129
    label "os&#261;dza&#263;"
  ]
  node [
    id 130
    label "argue"
  ]
  node [
    id 131
    label "rozpatrywa&#263;"
  ]
  node [
    id 132
    label "deliver"
  ]
  node [
    id 133
    label "odpieprzy&#263;"
  ]
  node [
    id 134
    label "sway"
  ]
  node [
    id 135
    label "merdn&#261;&#263;"
  ]
  node [
    id 136
    label "rap"
  ]
  node [
    id 137
    label "tick"
  ]
  node [
    id 138
    label "uderzy&#263;"
  ]
  node [
    id 139
    label "ruszy&#263;"
  ]
  node [
    id 140
    label "waln&#261;&#263;"
  ]
  node [
    id 141
    label "fajnie"
  ]
  node [
    id 142
    label "dobry"
  ]
  node [
    id 143
    label "byczy"
  ]
  node [
    id 144
    label "klawy"
  ]
  node [
    id 145
    label "pisa&#263;"
  ]
  node [
    id 146
    label "odmianka"
  ]
  node [
    id 147
    label "opu&#347;ci&#263;"
  ]
  node [
    id 148
    label "wypowied&#378;"
  ]
  node [
    id 149
    label "wytw&#243;r"
  ]
  node [
    id 150
    label "koniektura"
  ]
  node [
    id 151
    label "preparacja"
  ]
  node [
    id 152
    label "ekscerpcja"
  ]
  node [
    id 153
    label "j&#281;zykowo"
  ]
  node [
    id 154
    label "obelga"
  ]
  node [
    id 155
    label "dzie&#322;o"
  ]
  node [
    id 156
    label "pomini&#281;cie"
  ]
  node [
    id 157
    label "kolejny"
  ]
  node [
    id 158
    label "inaczej"
  ]
  node [
    id 159
    label "r&#243;&#380;ny"
  ]
  node [
    id 160
    label "inszy"
  ]
  node [
    id 161
    label "osobno"
  ]
  node [
    id 162
    label "doba"
  ]
  node [
    id 163
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 164
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 165
    label "mecz"
  ]
  node [
    id 166
    label "service"
  ]
  node [
    id 167
    label "zak&#322;ad"
  ]
  node [
    id 168
    label "us&#322;uga"
  ]
  node [
    id 169
    label "uderzenie"
  ]
  node [
    id 170
    label "doniesienie"
  ]
  node [
    id 171
    label "zastawa"
  ]
  node [
    id 172
    label "YouTube"
  ]
  node [
    id 173
    label "punkt"
  ]
  node [
    id 174
    label "porcja"
  ]
  node [
    id 175
    label "strona"
  ]
  node [
    id 176
    label "matczysko"
  ]
  node [
    id 177
    label "macierz"
  ]
  node [
    id 178
    label "przodkini"
  ]
  node [
    id 179
    label "Matka_Boska"
  ]
  node [
    id 180
    label "macocha"
  ]
  node [
    id 181
    label "matka_zast&#281;pcza"
  ]
  node [
    id 182
    label "stara"
  ]
  node [
    id 183
    label "rodzice"
  ]
  node [
    id 184
    label "rodzic"
  ]
  node [
    id 185
    label "stoisko"
  ]
  node [
    id 186
    label "Majdan"
  ]
  node [
    id 187
    label "miejsce"
  ]
  node [
    id 188
    label "obszar"
  ]
  node [
    id 189
    label "kram"
  ]
  node [
    id 190
    label "pierzeja"
  ]
  node [
    id 191
    label "przestrze&#324;"
  ]
  node [
    id 192
    label "obiekt_handlowy"
  ]
  node [
    id 193
    label "targowica"
  ]
  node [
    id 194
    label "zgromadzenie"
  ]
  node [
    id 195
    label "miasto"
  ]
  node [
    id 196
    label "pole_bitwy"
  ]
  node [
    id 197
    label "&#321;ubianka"
  ]
  node [
    id 198
    label "area"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 17
    target 106
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 19
    target 111
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 112
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 121
  ]
  edge [
    source 22
    target 122
  ]
  edge [
    source 22
    target 123
  ]
  edge [
    source 22
    target 124
  ]
  edge [
    source 22
    target 125
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 126
  ]
  edge [
    source 24
    target 127
  ]
  edge [
    source 24
    target 128
  ]
  edge [
    source 24
    target 129
  ]
  edge [
    source 24
    target 61
  ]
  edge [
    source 24
    target 130
  ]
  edge [
    source 24
    target 131
  ]
  edge [
    source 24
    target 132
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 133
  ]
  edge [
    source 25
    target 134
  ]
  edge [
    source 25
    target 135
  ]
  edge [
    source 25
    target 136
  ]
  edge [
    source 25
    target 137
  ]
  edge [
    source 25
    target 138
  ]
  edge [
    source 25
    target 139
  ]
  edge [
    source 25
    target 140
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 141
  ]
  edge [
    source 26
    target 142
  ]
  edge [
    source 26
    target 143
  ]
  edge [
    source 26
    target 144
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 145
  ]
  edge [
    source 27
    target 146
  ]
  edge [
    source 27
    target 147
  ]
  edge [
    source 27
    target 148
  ]
  edge [
    source 27
    target 149
  ]
  edge [
    source 27
    target 150
  ]
  edge [
    source 27
    target 151
  ]
  edge [
    source 27
    target 152
  ]
  edge [
    source 27
    target 153
  ]
  edge [
    source 27
    target 154
  ]
  edge [
    source 27
    target 155
  ]
  edge [
    source 27
    target 122
  ]
  edge [
    source 27
    target 156
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 157
  ]
  edge [
    source 29
    target 158
  ]
  edge [
    source 29
    target 159
  ]
  edge [
    source 29
    target 160
  ]
  edge [
    source 29
    target 161
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 162
  ]
  edge [
    source 33
    target 163
  ]
  edge [
    source 33
    target 109
  ]
  edge [
    source 33
    target 164
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 165
  ]
  edge [
    source 34
    target 166
  ]
  edge [
    source 34
    target 149
  ]
  edge [
    source 34
    target 167
  ]
  edge [
    source 34
    target 168
  ]
  edge [
    source 34
    target 169
  ]
  edge [
    source 34
    target 170
  ]
  edge [
    source 34
    target 171
  ]
  edge [
    source 34
    target 172
  ]
  edge [
    source 34
    target 173
  ]
  edge [
    source 34
    target 174
  ]
  edge [
    source 34
    target 175
  ]
  edge [
    source 35
    target 176
  ]
  edge [
    source 35
    target 177
  ]
  edge [
    source 35
    target 178
  ]
  edge [
    source 35
    target 179
  ]
  edge [
    source 35
    target 180
  ]
  edge [
    source 35
    target 181
  ]
  edge [
    source 35
    target 182
  ]
  edge [
    source 35
    target 183
  ]
  edge [
    source 35
    target 184
  ]
  edge [
    source 36
    target 185
  ]
  edge [
    source 36
    target 186
  ]
  edge [
    source 36
    target 187
  ]
  edge [
    source 36
    target 188
  ]
  edge [
    source 36
    target 189
  ]
  edge [
    source 36
    target 190
  ]
  edge [
    source 36
    target 191
  ]
  edge [
    source 36
    target 192
  ]
  edge [
    source 36
    target 193
  ]
  edge [
    source 36
    target 194
  ]
  edge [
    source 36
    target 195
  ]
  edge [
    source 36
    target 196
  ]
  edge [
    source 36
    target 197
  ]
  edge [
    source 36
    target 198
  ]
]
