graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.539744847890088
  density 0.0024948377680649196
  graphCliqueNumber 11
  node [
    id 0
    label "ten"
    origin "text"
  ]
  node [
    id 1
    label "koszt"
    origin "text"
  ]
  node [
    id 2
    label "trudny"
    origin "text"
  ]
  node [
    id 3
    label "sprawa"
    origin "text"
  ]
  node [
    id 4
    label "gdyby"
    origin "text"
  ]
  node [
    id 5
    label "da&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 8
    label "jedynie"
    origin "text"
  ]
  node [
    id 9
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 10
    label "system"
    origin "text"
  ]
  node [
    id 11
    label "ekonomiczny"
    origin "text"
  ]
  node [
    id 12
    label "zawali&#263;"
    origin "text"
  ]
  node [
    id 13
    label "bud&#380;et"
    origin "text"
  ]
  node [
    id 14
    label "plan"
    origin "text"
  ]
  node [
    id 15
    label "biznesowy"
    origin "text"
  ]
  node [
    id 16
    label "kredyt"
    origin "text"
  ]
  node [
    id 17
    label "wszystko"
    origin "text"
  ]
  node [
    id 18
    label "opiera&#263;"
    origin "text"
  ]
  node [
    id 19
    label "przed"
    origin "text"
  ]
  node [
    id 20
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 21
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 22
    label "pomyli&#263;"
    origin "text"
  ]
  node [
    id 23
    label "por&#243;wnanie"
    origin "text"
  ]
  node [
    id 24
    label "wegetarianin"
    origin "text"
  ]
  node [
    id 25
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "chybi&#263;"
    origin "text"
  ]
  node [
    id 27
    label "jeden"
    origin "text"
  ]
  node [
    id 28
    label "knajpa"
    origin "text"
  ]
  node [
    id 29
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "dania"
    origin "text"
  ]
  node [
    id 31
    label "wegetaria&#324;ski"
    origin "text"
  ]
  node [
    id 32
    label "niewegetaria&#324;ski"
    origin "text"
  ]
  node [
    id 33
    label "stan"
    origin "text"
  ]
  node [
    id 34
    label "problem"
    origin "text"
  ]
  node [
    id 35
    label "dla"
    origin "text"
  ]
  node [
    id 36
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 37
    label "strona"
    origin "text"
  ]
  node [
    id 38
    label "chyba"
    origin "text"
  ]
  node [
    id 39
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 40
    label "by&#263;"
    origin "text"
  ]
  node [
    id 41
    label "fanatyk"
    origin "text"
  ]
  node [
    id 42
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 43
    label "nawet"
    origin "text"
  ]
  node [
    id 44
    label "patrze&#263;"
    origin "text"
  ]
  node [
    id 45
    label "sojowy"
    origin "text"
  ]
  node [
    id 46
    label "kotlet"
    origin "text"
  ]
  node [
    id 47
    label "przy"
    origin "text"
  ]
  node [
    id 48
    label "zwolennik"
    origin "text"
  ]
  node [
    id 49
    label "upiera&#263;"
    origin "text"
  ]
  node [
    id 50
    label "tak"
    origin "text"
  ]
  node [
    id 51
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 52
    label "wolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 53
    label "bardzo"
    origin "text"
  ]
  node [
    id 54
    label "czemu"
    origin "text"
  ]
  node [
    id 55
    label "u&#380;ytkownik"
    origin "text"
  ]
  node [
    id 56
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 57
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 58
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 59
    label "mo&#380;liwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 60
    label "p&#243;j&#347;cie"
    origin "text"
  ]
  node [
    id 61
    label "fajny"
    origin "text"
  ]
  node [
    id 62
    label "niepal&#261;cy"
    origin "text"
  ]
  node [
    id 63
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 64
    label "cierpie&#263;"
    origin "text"
  ]
  node [
    id 65
    label "dlatego"
    origin "text"
  ]
  node [
    id 66
    label "chroni&#263;"
    origin "text"
  ]
  node [
    id 67
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 68
    label "pub"
    origin "text"
  ]
  node [
    id 69
    label "rynek"
    origin "text"
  ]
  node [
    id 70
    label "zawie&#347;&#263;"
    origin "text"
  ]
  node [
    id 71
    label "norma"
    origin "text"
  ]
  node [
    id 72
    label "standard"
    origin "text"
  ]
  node [
    id 73
    label "pal&#261;cy"
    origin "text"
  ]
  node [
    id 74
    label "zwykle"
    origin "text"
  ]
  node [
    id 75
    label "decydowa&#263;"
    origin "text"
  ]
  node [
    id 76
    label "tym"
    origin "text"
  ]
  node [
    id 77
    label "pal"
    origin "text"
  ]
  node [
    id 78
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 79
    label "wi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 80
    label "kwestia"
    origin "text"
  ]
  node [
    id 81
    label "dost&#281;p"
    origin "text"
  ]
  node [
    id 82
    label "jak"
    origin "text"
  ]
  node [
    id 83
    label "traktowa&#263;"
    origin "text"
  ]
  node [
    id 84
    label "uznawa&#263;"
    origin "text"
  ]
  node [
    id 85
    label "chodzenie"
    origin "text"
  ]
  node [
    id 86
    label "ca&#322;kowicie"
    origin "text"
  ]
  node [
    id 87
    label "zb&#281;dny"
    origin "text"
  ]
  node [
    id 88
    label "wp&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 89
    label "pozycja"
    origin "text"
  ]
  node [
    id 90
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 91
    label "jak&#261;&#347;tam"
    origin "text"
  ]
  node [
    id 92
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 93
    label "zakaz"
    origin "text"
  ]
  node [
    id 94
    label "palenie"
    origin "text"
  ]
  node [
    id 95
    label "absurd"
    origin "text"
  ]
  node [
    id 96
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 97
    label "uzna&#263;"
    origin "text"
  ]
  node [
    id 98
    label "inaczej"
    origin "text"
  ]
  node [
    id 99
    label "komplikowa&#263;"
    origin "text"
  ]
  node [
    id 100
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 101
    label "pali&#263;"
    origin "text"
  ]
  node [
    id 102
    label "gdziekolwiek"
    origin "text"
  ]
  node [
    id 103
    label "komunikacja"
    origin "text"
  ]
  node [
    id 104
    label "poniewa&#380;"
    origin "text"
  ]
  node [
    id 105
    label "korzysta&#263;"
    origin "text"
  ]
  node [
    id 106
    label "miejski"
    origin "text"
  ]
  node [
    id 107
    label "koniec"
    origin "text"
  ]
  node [
    id 108
    label "wolny"
    origin "text"
  ]
  node [
    id 109
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 110
    label "z&#322;y"
    origin "text"
  ]
  node [
    id 111
    label "sytuacja"
    origin "text"
  ]
  node [
    id 112
    label "wprowadzi&#263;"
    origin "text"
  ]
  node [
    id 113
    label "te&#380;"
    origin "text"
  ]
  node [
    id 114
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 115
    label "mycie"
    origin "text"
  ]
  node [
    id 116
    label "dezodorant"
    origin "text"
  ]
  node [
    id 117
    label "rower"
    origin "text"
  ]
  node [
    id 118
    label "pomaga&#263;"
    origin "text"
  ]
  node [
    id 119
    label "ale"
    origin "text"
  ]
  node [
    id 120
    label "czas"
    origin "text"
  ]
  node [
    id 121
    label "trzeba"
    origin "text"
  ]
  node [
    id 122
    label "pojecha&#263;"
    origin "text"
  ]
  node [
    id 123
    label "autobus"
    origin "text"
  ]
  node [
    id 124
    label "stara"
    origin "text"
  ]
  node [
    id 125
    label "zasada"
    origin "text"
  ]
  node [
    id 126
    label "pisownia"
    origin "text"
  ]
  node [
    id 127
    label "imies&#322;&#243;w"
    origin "text"
  ]
  node [
    id 128
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 129
    label "nowa"
    origin "text"
  ]
  node [
    id 130
    label "&#322;atwy"
    origin "text"
  ]
  node [
    id 131
    label "okre&#347;lony"
  ]
  node [
    id 132
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 133
    label "nak&#322;ad"
  ]
  node [
    id 134
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 135
    label "sumpt"
  ]
  node [
    id 136
    label "wydatek"
  ]
  node [
    id 137
    label "wymagaj&#261;cy"
  ]
  node [
    id 138
    label "skomplikowany"
  ]
  node [
    id 139
    label "k&#322;opotliwy"
  ]
  node [
    id 140
    label "ci&#281;&#380;ko"
  ]
  node [
    id 141
    label "temat"
  ]
  node [
    id 142
    label "kognicja"
  ]
  node [
    id 143
    label "idea"
  ]
  node [
    id 144
    label "szczeg&#243;&#322;"
  ]
  node [
    id 145
    label "rzecz"
  ]
  node [
    id 146
    label "wydarzenie"
  ]
  node [
    id 147
    label "przes&#322;anka"
  ]
  node [
    id 148
    label "rozprawa"
  ]
  node [
    id 149
    label "object"
  ]
  node [
    id 150
    label "proposition"
  ]
  node [
    id 151
    label "dostarczy&#263;"
  ]
  node [
    id 152
    label "obieca&#263;"
  ]
  node [
    id 153
    label "pozwoli&#263;"
  ]
  node [
    id 154
    label "przeznaczy&#263;"
  ]
  node [
    id 155
    label "doda&#263;"
  ]
  node [
    id 156
    label "give"
  ]
  node [
    id 157
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 158
    label "wyrzec_si&#281;"
  ]
  node [
    id 159
    label "supply"
  ]
  node [
    id 160
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 161
    label "zada&#263;"
  ]
  node [
    id 162
    label "odst&#261;pi&#263;"
  ]
  node [
    id 163
    label "feed"
  ]
  node [
    id 164
    label "testify"
  ]
  node [
    id 165
    label "powierzy&#263;"
  ]
  node [
    id 166
    label "convey"
  ]
  node [
    id 167
    label "przekaza&#263;"
  ]
  node [
    id 168
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 169
    label "zap&#322;aci&#263;"
  ]
  node [
    id 170
    label "dress"
  ]
  node [
    id 171
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 172
    label "udost&#281;pni&#263;"
  ]
  node [
    id 173
    label "sztachn&#261;&#263;"
  ]
  node [
    id 174
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 175
    label "zrobi&#263;"
  ]
  node [
    id 176
    label "przywali&#263;"
  ]
  node [
    id 177
    label "rap"
  ]
  node [
    id 178
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 179
    label "picture"
  ]
  node [
    id 180
    label "report"
  ]
  node [
    id 181
    label "dodawa&#263;"
  ]
  node [
    id 182
    label "wymienia&#263;"
  ]
  node [
    id 183
    label "okre&#347;la&#263;"
  ]
  node [
    id 184
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 185
    label "dyskalkulia"
  ]
  node [
    id 186
    label "wynagrodzenie"
  ]
  node [
    id 187
    label "admit"
  ]
  node [
    id 188
    label "osi&#261;ga&#263;"
  ]
  node [
    id 189
    label "wyznacza&#263;"
  ]
  node [
    id 190
    label "posiada&#263;"
  ]
  node [
    id 191
    label "mierzy&#263;"
  ]
  node [
    id 192
    label "odlicza&#263;"
  ]
  node [
    id 193
    label "bra&#263;"
  ]
  node [
    id 194
    label "wycenia&#263;"
  ]
  node [
    id 195
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 196
    label "rachowa&#263;"
  ]
  node [
    id 197
    label "tell"
  ]
  node [
    id 198
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 199
    label "policza&#263;"
  ]
  node [
    id 200
    label "count"
  ]
  node [
    id 201
    label "du&#380;y"
  ]
  node [
    id 202
    label "jedyny"
  ]
  node [
    id 203
    label "kompletny"
  ]
  node [
    id 204
    label "zdr&#243;w"
  ]
  node [
    id 205
    label "&#380;ywy"
  ]
  node [
    id 206
    label "ca&#322;o"
  ]
  node [
    id 207
    label "pe&#322;ny"
  ]
  node [
    id 208
    label "calu&#347;ko"
  ]
  node [
    id 209
    label "podobny"
  ]
  node [
    id 210
    label "model"
  ]
  node [
    id 211
    label "sk&#322;ad"
  ]
  node [
    id 212
    label "zachowanie"
  ]
  node [
    id 213
    label "podstawa"
  ]
  node [
    id 214
    label "porz&#261;dek"
  ]
  node [
    id 215
    label "Android"
  ]
  node [
    id 216
    label "przyn&#281;ta"
  ]
  node [
    id 217
    label "jednostka_geologiczna"
  ]
  node [
    id 218
    label "metoda"
  ]
  node [
    id 219
    label "podsystem"
  ]
  node [
    id 220
    label "p&#322;&#243;d"
  ]
  node [
    id 221
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 222
    label "s&#261;d"
  ]
  node [
    id 223
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 224
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 225
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 226
    label "j&#261;dro"
  ]
  node [
    id 227
    label "eratem"
  ]
  node [
    id 228
    label "ryba"
  ]
  node [
    id 229
    label "pulpit"
  ]
  node [
    id 230
    label "struktura"
  ]
  node [
    id 231
    label "spos&#243;b"
  ]
  node [
    id 232
    label "oddzia&#322;"
  ]
  node [
    id 233
    label "usenet"
  ]
  node [
    id 234
    label "o&#347;"
  ]
  node [
    id 235
    label "oprogramowanie"
  ]
  node [
    id 236
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 237
    label "poj&#281;cie"
  ]
  node [
    id 238
    label "w&#281;dkarstwo"
  ]
  node [
    id 239
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 240
    label "Leopard"
  ]
  node [
    id 241
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 242
    label "systemik"
  ]
  node [
    id 243
    label "rozprz&#261;c"
  ]
  node [
    id 244
    label "cybernetyk"
  ]
  node [
    id 245
    label "konstelacja"
  ]
  node [
    id 246
    label "doktryna"
  ]
  node [
    id 247
    label "net"
  ]
  node [
    id 248
    label "zbi&#243;r"
  ]
  node [
    id 249
    label "method"
  ]
  node [
    id 250
    label "systemat"
  ]
  node [
    id 251
    label "ekonomicznie"
  ]
  node [
    id 252
    label "korzystny"
  ]
  node [
    id 253
    label "oszcz&#281;dny"
  ]
  node [
    id 254
    label "overcrowd"
  ]
  node [
    id 255
    label "stuff"
  ]
  node [
    id 256
    label "wype&#322;ni&#263;"
  ]
  node [
    id 257
    label "throw"
  ]
  node [
    id 258
    label "zablokowa&#263;"
  ]
  node [
    id 259
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 260
    label "zamkn&#261;&#263;"
  ]
  node [
    id 261
    label "wydatki_bud&#380;etowe"
  ]
  node [
    id 262
    label "etat"
  ]
  node [
    id 263
    label "portfel"
  ]
  node [
    id 264
    label "rezerwa_rewaluacyjna"
  ]
  node [
    id 265
    label "kwota"
  ]
  node [
    id 266
    label "device"
  ]
  node [
    id 267
    label "wytw&#243;r"
  ]
  node [
    id 268
    label "obraz"
  ]
  node [
    id 269
    label "przestrze&#324;"
  ]
  node [
    id 270
    label "dekoracja"
  ]
  node [
    id 271
    label "intencja"
  ]
  node [
    id 272
    label "agreement"
  ]
  node [
    id 273
    label "pomys&#322;"
  ]
  node [
    id 274
    label "punkt"
  ]
  node [
    id 275
    label "miejsce_pracy"
  ]
  node [
    id 276
    label "perspektywa"
  ]
  node [
    id 277
    label "rysunek"
  ]
  node [
    id 278
    label "reprezentacja"
  ]
  node [
    id 279
    label "zawodowy"
  ]
  node [
    id 280
    label "biznesowo"
  ]
  node [
    id 281
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 282
    label "aktywa"
  ]
  node [
    id 283
    label "borg"
  ]
  node [
    id 284
    label "odsetki"
  ]
  node [
    id 285
    label "konsolidacja"
  ]
  node [
    id 286
    label "arrozacja"
  ]
  node [
    id 287
    label "d&#322;ug"
  ]
  node [
    id 288
    label "sp&#322;ata"
  ]
  node [
    id 289
    label "rata"
  ]
  node [
    id 290
    label "zapa&#347;&#263;_kredytowa"
  ]
  node [
    id 291
    label "konto"
  ]
  node [
    id 292
    label "pasywa"
  ]
  node [
    id 293
    label "zobowi&#261;zanie"
  ]
  node [
    id 294
    label "linia_kredytowa"
  ]
  node [
    id 295
    label "lock"
  ]
  node [
    id 296
    label "absolut"
  ]
  node [
    id 297
    label "osnowywa&#263;"
  ]
  node [
    id 298
    label "back"
  ]
  node [
    id 299
    label "czerpa&#263;"
  ]
  node [
    id 300
    label "digest"
  ]
  node [
    id 301
    label "stawia&#263;"
  ]
  node [
    id 302
    label "free"
  ]
  node [
    id 303
    label "confuse"
  ]
  node [
    id 304
    label "pomi&#281;sza&#263;"
  ]
  node [
    id 305
    label "wykona&#263;"
  ]
  node [
    id 306
    label "popieprzy&#263;"
  ]
  node [
    id 307
    label "simile"
  ]
  node [
    id 308
    label "figura_stylistyczna"
  ]
  node [
    id 309
    label "zestawienie"
  ]
  node [
    id 310
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 311
    label "comparison"
  ]
  node [
    id 312
    label "zanalizowanie"
  ]
  node [
    id 313
    label "zjadacz"
  ]
  node [
    id 314
    label "impart"
  ]
  node [
    id 315
    label "panna_na_wydaniu"
  ]
  node [
    id 316
    label "surrender"
  ]
  node [
    id 317
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 318
    label "train"
  ]
  node [
    id 319
    label "wytwarza&#263;"
  ]
  node [
    id 320
    label "dawa&#263;"
  ]
  node [
    id 321
    label "zapach"
  ]
  node [
    id 322
    label "wprowadza&#263;"
  ]
  node [
    id 323
    label "ujawnia&#263;"
  ]
  node [
    id 324
    label "wydawnictwo"
  ]
  node [
    id 325
    label "powierza&#263;"
  ]
  node [
    id 326
    label "produkcja"
  ]
  node [
    id 327
    label "denuncjowa&#263;"
  ]
  node [
    id 328
    label "mie&#263;_miejsce"
  ]
  node [
    id 329
    label "plon"
  ]
  node [
    id 330
    label "reszta"
  ]
  node [
    id 331
    label "robi&#263;"
  ]
  node [
    id 332
    label "placard"
  ]
  node [
    id 333
    label "tajemnica"
  ]
  node [
    id 334
    label "wiano"
  ]
  node [
    id 335
    label "kojarzy&#263;"
  ]
  node [
    id 336
    label "d&#378;wi&#281;k"
  ]
  node [
    id 337
    label "miss"
  ]
  node [
    id 338
    label "kieliszek"
  ]
  node [
    id 339
    label "shot"
  ]
  node [
    id 340
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 341
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 342
    label "jaki&#347;"
  ]
  node [
    id 343
    label "jednolicie"
  ]
  node [
    id 344
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 345
    label "w&#243;dka"
  ]
  node [
    id 346
    label "ujednolicenie"
  ]
  node [
    id 347
    label "jednakowy"
  ]
  node [
    id 348
    label "zak&#322;ad"
  ]
  node [
    id 349
    label "grupa"
  ]
  node [
    id 350
    label "szynkownia"
  ]
  node [
    id 351
    label "gastronomia"
  ]
  node [
    id 352
    label "bar"
  ]
  node [
    id 353
    label "tenis"
  ]
  node [
    id 354
    label "cover"
  ]
  node [
    id 355
    label "siatk&#243;wka"
  ]
  node [
    id 356
    label "faszerowa&#263;"
  ]
  node [
    id 357
    label "informowa&#263;"
  ]
  node [
    id 358
    label "introduce"
  ]
  node [
    id 359
    label "jedzenie"
  ]
  node [
    id 360
    label "tender"
  ]
  node [
    id 361
    label "deal"
  ]
  node [
    id 362
    label "kelner"
  ]
  node [
    id 363
    label "serwowa&#263;"
  ]
  node [
    id 364
    label "rozgrywa&#263;"
  ]
  node [
    id 365
    label "bezmi&#281;sny"
  ]
  node [
    id 366
    label "wegetaria&#324;sko"
  ]
  node [
    id 367
    label "Arizona"
  ]
  node [
    id 368
    label "Georgia"
  ]
  node [
    id 369
    label "warstwa"
  ]
  node [
    id 370
    label "jednostka_administracyjna"
  ]
  node [
    id 371
    label "Goa"
  ]
  node [
    id 372
    label "Hawaje"
  ]
  node [
    id 373
    label "Floryda"
  ]
  node [
    id 374
    label "Oklahoma"
  ]
  node [
    id 375
    label "Alaska"
  ]
  node [
    id 376
    label "Alabama"
  ]
  node [
    id 377
    label "wci&#281;cie"
  ]
  node [
    id 378
    label "Oregon"
  ]
  node [
    id 379
    label "poziom"
  ]
  node [
    id 380
    label "Teksas"
  ]
  node [
    id 381
    label "Illinois"
  ]
  node [
    id 382
    label "Jukatan"
  ]
  node [
    id 383
    label "Waszyngton"
  ]
  node [
    id 384
    label "shape"
  ]
  node [
    id 385
    label "Nowy_Meksyk"
  ]
  node [
    id 386
    label "ilo&#347;&#263;"
  ]
  node [
    id 387
    label "state"
  ]
  node [
    id 388
    label "Nowy_York"
  ]
  node [
    id 389
    label "Arakan"
  ]
  node [
    id 390
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 391
    label "Kalifornia"
  ]
  node [
    id 392
    label "wektor"
  ]
  node [
    id 393
    label "Massachusetts"
  ]
  node [
    id 394
    label "miejsce"
  ]
  node [
    id 395
    label "Pensylwania"
  ]
  node [
    id 396
    label "Maryland"
  ]
  node [
    id 397
    label "Michigan"
  ]
  node [
    id 398
    label "Ohio"
  ]
  node [
    id 399
    label "Kansas"
  ]
  node [
    id 400
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 401
    label "Luizjana"
  ]
  node [
    id 402
    label "samopoczucie"
  ]
  node [
    id 403
    label "Wirginia"
  ]
  node [
    id 404
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 405
    label "trudno&#347;&#263;"
  ]
  node [
    id 406
    label "ambaras"
  ]
  node [
    id 407
    label "problemat"
  ]
  node [
    id 408
    label "pierepa&#322;ka"
  ]
  node [
    id 409
    label "obstruction"
  ]
  node [
    id 410
    label "problematyka"
  ]
  node [
    id 411
    label "jajko_Kolumba"
  ]
  node [
    id 412
    label "subiekcja"
  ]
  node [
    id 413
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 414
    label "nijaki"
  ]
  node [
    id 415
    label "skr&#281;canie"
  ]
  node [
    id 416
    label "voice"
  ]
  node [
    id 417
    label "forma"
  ]
  node [
    id 418
    label "internet"
  ]
  node [
    id 419
    label "skr&#281;ci&#263;"
  ]
  node [
    id 420
    label "kartka"
  ]
  node [
    id 421
    label "orientowa&#263;"
  ]
  node [
    id 422
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 423
    label "powierzchnia"
  ]
  node [
    id 424
    label "plik"
  ]
  node [
    id 425
    label "bok"
  ]
  node [
    id 426
    label "pagina"
  ]
  node [
    id 427
    label "orientowanie"
  ]
  node [
    id 428
    label "fragment"
  ]
  node [
    id 429
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 430
    label "skr&#281;ca&#263;"
  ]
  node [
    id 431
    label "g&#243;ra"
  ]
  node [
    id 432
    label "serwis_internetowy"
  ]
  node [
    id 433
    label "orientacja"
  ]
  node [
    id 434
    label "linia"
  ]
  node [
    id 435
    label "skr&#281;cenie"
  ]
  node [
    id 436
    label "layout"
  ]
  node [
    id 437
    label "zorientowa&#263;"
  ]
  node [
    id 438
    label "zorientowanie"
  ]
  node [
    id 439
    label "obiekt"
  ]
  node [
    id 440
    label "podmiot"
  ]
  node [
    id 441
    label "ty&#322;"
  ]
  node [
    id 442
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 443
    label "logowanie"
  ]
  node [
    id 444
    label "adres_internetowy"
  ]
  node [
    id 445
    label "uj&#281;cie"
  ]
  node [
    id 446
    label "prz&#243;d"
  ]
  node [
    id 447
    label "posta&#263;"
  ]
  node [
    id 448
    label "cz&#322;owiek"
  ]
  node [
    id 449
    label "znaczenie"
  ]
  node [
    id 450
    label "go&#347;&#263;"
  ]
  node [
    id 451
    label "osoba"
  ]
  node [
    id 452
    label "si&#281;ga&#263;"
  ]
  node [
    id 453
    label "trwa&#263;"
  ]
  node [
    id 454
    label "obecno&#347;&#263;"
  ]
  node [
    id 455
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 456
    label "stand"
  ]
  node [
    id 457
    label "uczestniczy&#263;"
  ]
  node [
    id 458
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 459
    label "equal"
  ]
  node [
    id 460
    label "oszo&#322;om"
  ]
  node [
    id 461
    label "pasjonat"
  ]
  node [
    id 462
    label "radyka&#322;"
  ]
  node [
    id 463
    label "obsesjonista"
  ]
  node [
    id 464
    label "koso"
  ]
  node [
    id 465
    label "szuka&#263;"
  ]
  node [
    id 466
    label "go_steady"
  ]
  node [
    id 467
    label "dba&#263;"
  ]
  node [
    id 468
    label "os&#261;dza&#263;"
  ]
  node [
    id 469
    label "punkt_widzenia"
  ]
  node [
    id 470
    label "uwa&#380;a&#263;"
  ]
  node [
    id 471
    label "look"
  ]
  node [
    id 472
    label "pogl&#261;da&#263;"
  ]
  node [
    id 473
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 474
    label "potrawa"
  ]
  node [
    id 475
    label "sma&#380;y&#263;"
  ]
  node [
    id 476
    label "postawi&#263;"
  ]
  node [
    id 477
    label "prasa"
  ]
  node [
    id 478
    label "stworzy&#263;"
  ]
  node [
    id 479
    label "donie&#347;&#263;"
  ]
  node [
    id 480
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 481
    label "write"
  ]
  node [
    id 482
    label "styl"
  ]
  node [
    id 483
    label "read"
  ]
  node [
    id 484
    label "absolutno&#347;&#263;"
  ]
  node [
    id 485
    label "uwi&#281;zi&#263;"
  ]
  node [
    id 486
    label "cecha"
  ]
  node [
    id 487
    label "freedom"
  ]
  node [
    id 488
    label "niezale&#380;no&#347;&#263;"
  ]
  node [
    id 489
    label "uwi&#281;zienie"
  ]
  node [
    id 490
    label "nieokre&#347;lono&#347;&#263;"
  ]
  node [
    id 491
    label "w_chuj"
  ]
  node [
    id 492
    label "j&#281;zykowo"
  ]
  node [
    id 493
    label "czu&#263;"
  ]
  node [
    id 494
    label "desire"
  ]
  node [
    id 495
    label "kcie&#263;"
  ]
  node [
    id 496
    label "need"
  ]
  node [
    id 497
    label "hide"
  ]
  node [
    id 498
    label "support"
  ]
  node [
    id 499
    label "ability"
  ]
  node [
    id 500
    label "prospect"
  ]
  node [
    id 501
    label "egzekutywa"
  ]
  node [
    id 502
    label "alternatywa"
  ]
  node [
    id 503
    label "potencja&#322;"
  ]
  node [
    id 504
    label "obliczeniowo"
  ]
  node [
    id 505
    label "operator_modalny"
  ]
  node [
    id 506
    label "zostanie"
  ]
  node [
    id 507
    label "sprzedanie_si&#281;"
  ]
  node [
    id 508
    label "podziewanie_si&#281;"
  ]
  node [
    id 509
    label "poruszenie_si&#281;"
  ]
  node [
    id 510
    label "exit"
  ]
  node [
    id 511
    label "zacz&#281;cie"
  ]
  node [
    id 512
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 513
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 514
    label "zdarzenie_si&#281;"
  ]
  node [
    id 515
    label "opuszczenie"
  ]
  node [
    id 516
    label "zrobienie"
  ]
  node [
    id 517
    label "podzianie_si&#281;"
  ]
  node [
    id 518
    label "popsucie_si&#281;"
  ]
  node [
    id 519
    label "kopni&#281;cie_si&#281;"
  ]
  node [
    id 520
    label "przes&#322;anie"
  ]
  node [
    id 521
    label "udanie_si&#281;"
  ]
  node [
    id 522
    label "wych&#243;d"
  ]
  node [
    id 523
    label "powychodzenie"
  ]
  node [
    id 524
    label "przeznaczenie"
  ]
  node [
    id 525
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 526
    label "fajnie"
  ]
  node [
    id 527
    label "dobry"
  ]
  node [
    id 528
    label "byczy"
  ]
  node [
    id 529
    label "klawy"
  ]
  node [
    id 530
    label "czyj&#347;"
  ]
  node [
    id 531
    label "m&#261;&#380;"
  ]
  node [
    id 532
    label "j&#281;cze&#263;"
  ]
  node [
    id 533
    label "martwi&#263;_si&#281;"
  ]
  node [
    id 534
    label "wytrzymywa&#263;"
  ]
  node [
    id 535
    label "represent"
  ]
  node [
    id 536
    label "pochyla&#263;_si&#281;"
  ]
  node [
    id 537
    label "traci&#263;"
  ]
  node [
    id 538
    label "narzeka&#263;"
  ]
  node [
    id 539
    label "sting"
  ]
  node [
    id 540
    label "doznawa&#263;"
  ]
  node [
    id 541
    label "hurt"
  ]
  node [
    id 542
    label "boryka&#263;_si&#281;"
  ]
  node [
    id 543
    label "kultywowa&#263;"
  ]
  node [
    id 544
    label "sprawowa&#263;"
  ]
  node [
    id 545
    label "czuwa&#263;"
  ]
  node [
    id 546
    label "wykupienie"
  ]
  node [
    id 547
    label "bycie_w_posiadaniu"
  ]
  node [
    id 548
    label "wykupywanie"
  ]
  node [
    id 549
    label "piwiarnia"
  ]
  node [
    id 550
    label "stoisko"
  ]
  node [
    id 551
    label "plac"
  ]
  node [
    id 552
    label "emitowanie"
  ]
  node [
    id 553
    label "targowica"
  ]
  node [
    id 554
    label "emitowa&#263;"
  ]
  node [
    id 555
    label "wprowadzanie"
  ]
  node [
    id 556
    label "rynek_wt&#243;rny"
  ]
  node [
    id 557
    label "wprowadzenie"
  ]
  node [
    id 558
    label "kram"
  ]
  node [
    id 559
    label "pojawienie_si&#281;"
  ]
  node [
    id 560
    label "rynek_podstawowy"
  ]
  node [
    id 561
    label "biznes"
  ]
  node [
    id 562
    label "gospodarka"
  ]
  node [
    id 563
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 564
    label "obiekt_handlowy"
  ]
  node [
    id 565
    label "konsument"
  ]
  node [
    id 566
    label "wytw&#243;rca"
  ]
  node [
    id 567
    label "segment_rynku"
  ]
  node [
    id 568
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 569
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 570
    label "wzbudzi&#263;"
  ]
  node [
    id 571
    label "moderate"
  ]
  node [
    id 572
    label "dokument"
  ]
  node [
    id 573
    label "pace"
  ]
  node [
    id 574
    label "moralno&#347;&#263;"
  ]
  node [
    id 575
    label "powszednio&#347;&#263;"
  ]
  node [
    id 576
    label "konstrukt"
  ]
  node [
    id 577
    label "prawid&#322;o"
  ]
  node [
    id 578
    label "criterion"
  ]
  node [
    id 579
    label "rozmiar"
  ]
  node [
    id 580
    label "zorganizowa&#263;"
  ]
  node [
    id 581
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 582
    label "taniec_towarzyski"
  ]
  node [
    id 583
    label "ordinariness"
  ]
  node [
    id 584
    label "organizowanie"
  ]
  node [
    id 585
    label "zorganizowanie"
  ]
  node [
    id 586
    label "instytucja"
  ]
  node [
    id 587
    label "organizowa&#263;"
  ]
  node [
    id 588
    label "pal&#261;co"
  ]
  node [
    id 589
    label "pilnie"
  ]
  node [
    id 590
    label "intensywny"
  ]
  node [
    id 591
    label "gor&#261;cy"
  ]
  node [
    id 592
    label "dojmuj&#261;co"
  ]
  node [
    id 593
    label "wyra&#378;ny"
  ]
  node [
    id 594
    label "zwyk&#322;y"
  ]
  node [
    id 595
    label "cz&#281;sto"
  ]
  node [
    id 596
    label "klasyfikator"
  ]
  node [
    id 597
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 598
    label "decide"
  ]
  node [
    id 599
    label "mean"
  ]
  node [
    id 600
    label "dalba"
  ]
  node [
    id 601
    label "picket"
  ]
  node [
    id 602
    label "paliszcze"
  ]
  node [
    id 603
    label "s&#322;up"
  ]
  node [
    id 604
    label "palisada"
  ]
  node [
    id 605
    label "tobo&#322;ek"
  ]
  node [
    id 606
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 607
    label "perpetrate"
  ]
  node [
    id 608
    label "pakowa&#263;"
  ]
  node [
    id 609
    label "bind"
  ]
  node [
    id 610
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 611
    label "krzepn&#261;&#263;"
  ]
  node [
    id 612
    label "obezw&#322;adnia&#263;"
  ]
  node [
    id 613
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 614
    label "consort"
  ]
  node [
    id 615
    label "ensnare"
  ]
  node [
    id 616
    label "anga&#380;owa&#263;"
  ]
  node [
    id 617
    label "powodowa&#263;"
  ]
  node [
    id 618
    label "zawi&#261;zek"
  ]
  node [
    id 619
    label "w&#281;ze&#322;"
  ]
  node [
    id 620
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 621
    label "tighten"
  ]
  node [
    id 622
    label "scala&#263;"
  ]
  node [
    id 623
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 624
    label "cement"
  ]
  node [
    id 625
    label "frame"
  ]
  node [
    id 626
    label "zaprawa"
  ]
  node [
    id 627
    label "relate"
  ]
  node [
    id 628
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 629
    label "wypowied&#378;"
  ]
  node [
    id 630
    label "dialog"
  ]
  node [
    id 631
    label "subject"
  ]
  node [
    id 632
    label "informatyka"
  ]
  node [
    id 633
    label "has&#322;o"
  ]
  node [
    id 634
    label "operacja"
  ]
  node [
    id 635
    label "byd&#322;o"
  ]
  node [
    id 636
    label "zobo"
  ]
  node [
    id 637
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 638
    label "yakalo"
  ]
  node [
    id 639
    label "dzo"
  ]
  node [
    id 640
    label "use"
  ]
  node [
    id 641
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 642
    label "dotyczy&#263;"
  ]
  node [
    id 643
    label "poddawa&#263;"
  ]
  node [
    id 644
    label "consider"
  ]
  node [
    id 645
    label "stwierdza&#263;"
  ]
  node [
    id 646
    label "notice"
  ]
  node [
    id 647
    label "przyznawa&#263;"
  ]
  node [
    id 648
    label "uchodzenie_si&#281;"
  ]
  node [
    id 649
    label "uruchamianie"
  ]
  node [
    id 650
    label "przechodzenie"
  ]
  node [
    id 651
    label "ubieranie"
  ]
  node [
    id 652
    label "w&#322;&#261;czanie"
  ]
  node [
    id 653
    label "ucz&#281;szczanie"
  ]
  node [
    id 654
    label "noszenie"
  ]
  node [
    id 655
    label "rozdeptywanie"
  ]
  node [
    id 656
    label "wydeptanie"
  ]
  node [
    id 657
    label "wydeptywanie"
  ]
  node [
    id 658
    label "ko&#322;atanie_si&#281;"
  ]
  node [
    id 659
    label "zdeptanie"
  ]
  node [
    id 660
    label "uganianie_si&#281;"
  ]
  node [
    id 661
    label "p&#322;ywanie"
  ]
  node [
    id 662
    label "jitter"
  ]
  node [
    id 663
    label "funkcja"
  ]
  node [
    id 664
    label "dzianie_si&#281;"
  ]
  node [
    id 665
    label "zmierzanie"
  ]
  node [
    id 666
    label "wear"
  ]
  node [
    id 667
    label "deptanie"
  ]
  node [
    id 668
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 669
    label "rozdeptanie"
  ]
  node [
    id 670
    label "ubieranie_si&#281;"
  ]
  node [
    id 671
    label "znoszenie"
  ]
  node [
    id 672
    label "staranie_si&#281;"
  ]
  node [
    id 673
    label "podtrzymywanie"
  ]
  node [
    id 674
    label "przenoszenie"
  ]
  node [
    id 675
    label "poruszanie_si&#281;"
  ]
  node [
    id 676
    label "nakr&#281;canie"
  ]
  node [
    id 677
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 678
    label "pochodzenie"
  ]
  node [
    id 679
    label "uruchomienie"
  ]
  node [
    id 680
    label "bywanie"
  ]
  node [
    id 681
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 682
    label "tr&#243;jstronny"
  ]
  node [
    id 683
    label "impact"
  ]
  node [
    id 684
    label "dochodzenie"
  ]
  node [
    id 685
    label "w&#322;&#261;czenie"
  ]
  node [
    id 686
    label "zatrzymanie"
  ]
  node [
    id 687
    label "obchodzenie"
  ]
  node [
    id 688
    label "donoszenie"
  ]
  node [
    id 689
    label "nakr&#281;cenie"
  ]
  node [
    id 690
    label "donaszanie"
  ]
  node [
    id 691
    label "wniwecz"
  ]
  node [
    id 692
    label "zupe&#322;ny"
  ]
  node [
    id 693
    label "odchodzenie"
  ]
  node [
    id 694
    label "odej&#347;cie"
  ]
  node [
    id 695
    label "nadmiarowy"
  ]
  node [
    id 696
    label "zb&#281;dnie"
  ]
  node [
    id 697
    label "zasila&#263;"
  ]
  node [
    id 698
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 699
    label "work"
  ]
  node [
    id 700
    label "przyp&#322;ywa&#263;"
  ]
  node [
    id 701
    label "kapita&#322;"
  ]
  node [
    id 702
    label "determine"
  ]
  node [
    id 703
    label "dochodzi&#263;"
  ]
  node [
    id 704
    label "pour"
  ]
  node [
    id 705
    label "ciek_wodny"
  ]
  node [
    id 706
    label "spis"
  ]
  node [
    id 707
    label "awansowanie"
  ]
  node [
    id 708
    label "po&#322;o&#380;enie"
  ]
  node [
    id 709
    label "rz&#261;d"
  ]
  node [
    id 710
    label "szermierka"
  ]
  node [
    id 711
    label "debit"
  ]
  node [
    id 712
    label "status"
  ]
  node [
    id 713
    label "adres"
  ]
  node [
    id 714
    label "redaktor"
  ]
  node [
    id 715
    label "poster"
  ]
  node [
    id 716
    label "le&#380;e&#263;"
  ]
  node [
    id 717
    label "wyda&#263;"
  ]
  node [
    id 718
    label "bearing"
  ]
  node [
    id 719
    label "wojsko"
  ]
  node [
    id 720
    label "druk"
  ]
  node [
    id 721
    label "awans"
  ]
  node [
    id 722
    label "ustawienie"
  ]
  node [
    id 723
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 724
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 725
    label "szata_graficzna"
  ]
  node [
    id 726
    label "awansowa&#263;"
  ]
  node [
    id 727
    label "rozmieszczenie"
  ]
  node [
    id 728
    label "publikacja"
  ]
  node [
    id 729
    label "niepubliczny"
  ]
  node [
    id 730
    label "spo&#322;ecznie"
  ]
  node [
    id 731
    label "publiczny"
  ]
  node [
    id 732
    label "proceed"
  ]
  node [
    id 733
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 734
    label "bangla&#263;"
  ]
  node [
    id 735
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 736
    label "run"
  ]
  node [
    id 737
    label "tryb"
  ]
  node [
    id 738
    label "p&#322;ywa&#263;"
  ]
  node [
    id 739
    label "continue"
  ]
  node [
    id 740
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 741
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 742
    label "przebiega&#263;"
  ]
  node [
    id 743
    label "wk&#322;ada&#263;"
  ]
  node [
    id 744
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 745
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 746
    label "para"
  ]
  node [
    id 747
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 748
    label "krok"
  ]
  node [
    id 749
    label "str&#243;j"
  ]
  node [
    id 750
    label "bywa&#263;"
  ]
  node [
    id 751
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 752
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 753
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 754
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 755
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 756
    label "dziama&#263;"
  ]
  node [
    id 757
    label "stara&#263;_si&#281;"
  ]
  node [
    id 758
    label "carry"
  ]
  node [
    id 759
    label "rozporz&#261;dzenie"
  ]
  node [
    id 760
    label "polecenie"
  ]
  node [
    id 761
    label "kadzenie"
  ]
  node [
    id 762
    label "robienie"
  ]
  node [
    id 763
    label "strzelanie"
  ]
  node [
    id 764
    label "wypalenie"
  ]
  node [
    id 765
    label "przygotowywanie"
  ]
  node [
    id 766
    label "wypalanie"
  ]
  node [
    id 767
    label "pykni&#281;cie"
  ]
  node [
    id 768
    label "popalenie"
  ]
  node [
    id 769
    label "emergency"
  ]
  node [
    id 770
    label "rozpalanie"
  ]
  node [
    id 771
    label "na&#322;&#243;g"
  ]
  node [
    id 772
    label "incineration"
  ]
  node [
    id 773
    label "dra&#380;nienie"
  ]
  node [
    id 774
    label "paliwo"
  ]
  node [
    id 775
    label "psucie"
  ]
  node [
    id 776
    label "jaranie"
  ]
  node [
    id 777
    label "zu&#380;ywanie"
  ]
  node [
    id 778
    label "podpalanie"
  ]
  node [
    id 779
    label "palenie_si&#281;"
  ]
  node [
    id 780
    label "papieros"
  ]
  node [
    id 781
    label "ogie&#324;"
  ]
  node [
    id 782
    label "czynno&#347;&#263;"
  ]
  node [
    id 783
    label "grzanie"
  ]
  node [
    id 784
    label "bolenie"
  ]
  node [
    id 785
    label "burn"
  ]
  node [
    id 786
    label "fajka"
  ]
  node [
    id 787
    label "dowcip"
  ]
  node [
    id 788
    label "dopalenie"
  ]
  node [
    id 789
    label "dokuczanie"
  ]
  node [
    id 790
    label "napalenie"
  ]
  node [
    id 791
    label "cygaro"
  ]
  node [
    id 792
    label "burning"
  ]
  node [
    id 793
    label "niszczenie"
  ]
  node [
    id 794
    label "powodowanie"
  ]
  node [
    id 795
    label "nieprawdopodobie&#324;stwo"
  ]
  node [
    id 796
    label "nieprawda"
  ]
  node [
    id 797
    label "rede"
  ]
  node [
    id 798
    label "assent"
  ]
  node [
    id 799
    label "oceni&#263;"
  ]
  node [
    id 800
    label "przyzna&#263;"
  ]
  node [
    id 801
    label "stwierdzi&#263;"
  ]
  node [
    id 802
    label "see"
  ]
  node [
    id 803
    label "niestandardowo"
  ]
  node [
    id 804
    label "inny"
  ]
  node [
    id 805
    label "involve"
  ]
  node [
    id 806
    label "utrudnia&#263;"
  ]
  node [
    id 807
    label "bole&#263;"
  ]
  node [
    id 808
    label "ridicule"
  ]
  node [
    id 809
    label "psu&#263;"
  ]
  node [
    id 810
    label "podtrzymywa&#263;"
  ]
  node [
    id 811
    label "doskwiera&#263;"
  ]
  node [
    id 812
    label "odstawia&#263;"
  ]
  node [
    id 813
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 814
    label "flash"
  ]
  node [
    id 815
    label "niszczy&#263;"
  ]
  node [
    id 816
    label "podra&#380;nia&#263;"
  ]
  node [
    id 817
    label "reek"
  ]
  node [
    id 818
    label "strzela&#263;"
  ]
  node [
    id 819
    label "blaze"
  ]
  node [
    id 820
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 821
    label "grza&#263;"
  ]
  node [
    id 822
    label "fire"
  ]
  node [
    id 823
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 824
    label "implicite"
  ]
  node [
    id 825
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 826
    label "transportation_system"
  ]
  node [
    id 827
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 828
    label "explicite"
  ]
  node [
    id 829
    label "ekspedytor"
  ]
  node [
    id 830
    label "uzyskiwa&#263;"
  ]
  node [
    id 831
    label "u&#380;ywa&#263;"
  ]
  node [
    id 832
    label "miejsko"
  ]
  node [
    id 833
    label "miastowy"
  ]
  node [
    id 834
    label "typowy"
  ]
  node [
    id 835
    label "defenestracja"
  ]
  node [
    id 836
    label "szereg"
  ]
  node [
    id 837
    label "dzia&#322;anie"
  ]
  node [
    id 838
    label "ostatnie_podrygi"
  ]
  node [
    id 839
    label "kres"
  ]
  node [
    id 840
    label "agonia"
  ]
  node [
    id 841
    label "visitation"
  ]
  node [
    id 842
    label "szeol"
  ]
  node [
    id 843
    label "mogi&#322;a"
  ]
  node [
    id 844
    label "chwila"
  ]
  node [
    id 845
    label "pogrzebanie"
  ]
  node [
    id 846
    label "&#380;a&#322;oba"
  ]
  node [
    id 847
    label "zabicie"
  ]
  node [
    id 848
    label "kres_&#380;ycia"
  ]
  node [
    id 849
    label "niezale&#380;ny"
  ]
  node [
    id 850
    label "swobodnie"
  ]
  node [
    id 851
    label "niespieszny"
  ]
  node [
    id 852
    label "rozrzedzanie"
  ]
  node [
    id 853
    label "zwolnienie_si&#281;"
  ]
  node [
    id 854
    label "wolno"
  ]
  node [
    id 855
    label "rozrzedzenie"
  ]
  node [
    id 856
    label "lu&#378;no"
  ]
  node [
    id 857
    label "zwalnianie_si&#281;"
  ]
  node [
    id 858
    label "wolnie"
  ]
  node [
    id 859
    label "strza&#322;"
  ]
  node [
    id 860
    label "rozwodnienie"
  ]
  node [
    id 861
    label "wakowa&#263;"
  ]
  node [
    id 862
    label "rozwadnianie"
  ]
  node [
    id 863
    label "rzedni&#281;cie"
  ]
  node [
    id 864
    label "zrzedni&#281;cie"
  ]
  node [
    id 865
    label "decyzja"
  ]
  node [
    id 866
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 867
    label "pick"
  ]
  node [
    id 868
    label "zdenerwowany"
  ]
  node [
    id 869
    label "zez&#322;oszczenie"
  ]
  node [
    id 870
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 871
    label "gniewanie"
  ]
  node [
    id 872
    label "niekorzystny"
  ]
  node [
    id 873
    label "niemoralny"
  ]
  node [
    id 874
    label "niegrzeczny"
  ]
  node [
    id 875
    label "pieski"
  ]
  node [
    id 876
    label "negatywny"
  ]
  node [
    id 877
    label "&#378;le"
  ]
  node [
    id 878
    label "syf"
  ]
  node [
    id 879
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 880
    label "sierdzisty"
  ]
  node [
    id 881
    label "z&#322;oszczenie"
  ]
  node [
    id 882
    label "rozgniewanie"
  ]
  node [
    id 883
    label "niepomy&#347;lny"
  ]
  node [
    id 884
    label "motyw"
  ]
  node [
    id 885
    label "realia"
  ]
  node [
    id 886
    label "warunki"
  ]
  node [
    id 887
    label "wej&#347;&#263;"
  ]
  node [
    id 888
    label "zacz&#261;&#263;"
  ]
  node [
    id 889
    label "zej&#347;&#263;"
  ]
  node [
    id 890
    label "spowodowa&#263;"
  ]
  node [
    id 891
    label "wpisa&#263;"
  ]
  node [
    id 892
    label "insert"
  ]
  node [
    id 893
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 894
    label "indicate"
  ]
  node [
    id 895
    label "zapozna&#263;"
  ]
  node [
    id 896
    label "umie&#347;ci&#263;"
  ]
  node [
    id 897
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 898
    label "doprowadzi&#263;"
  ]
  node [
    id 899
    label "ablucja"
  ]
  node [
    id 900
    label "czyszczenie"
  ]
  node [
    id 901
    label "toaleta"
  ]
  node [
    id 902
    label "czysty"
  ]
  node [
    id 903
    label "wymywanie"
  ]
  node [
    id 904
    label "wash"
  ]
  node [
    id 905
    label "wymycie"
  ]
  node [
    id 906
    label "przemycie"
  ]
  node [
    id 907
    label "przemywanie"
  ]
  node [
    id 908
    label "deodorant"
  ]
  node [
    id 909
    label "kosmetyk"
  ]
  node [
    id 910
    label "suport"
  ]
  node [
    id 911
    label "sztyca"
  ]
  node [
    id 912
    label "mostek"
  ]
  node [
    id 913
    label "cykloergometr"
  ]
  node [
    id 914
    label "&#322;a&#324;cuch"
  ]
  node [
    id 915
    label "przerzutka"
  ]
  node [
    id 916
    label "Romet"
  ]
  node [
    id 917
    label "torpedo"
  ]
  node [
    id 918
    label "dwuko&#322;owiec"
  ]
  node [
    id 919
    label "miska"
  ]
  node [
    id 920
    label "siode&#322;ko"
  ]
  node [
    id 921
    label "kierownica"
  ]
  node [
    id 922
    label "pojazd_niemechaniczny"
  ]
  node [
    id 923
    label "sprzyja&#263;"
  ]
  node [
    id 924
    label "skutkowa&#263;"
  ]
  node [
    id 925
    label "concur"
  ]
  node [
    id 926
    label "Warszawa"
  ]
  node [
    id 927
    label "aid"
  ]
  node [
    id 928
    label "u&#322;atwia&#263;"
  ]
  node [
    id 929
    label "piwo"
  ]
  node [
    id 930
    label "czasokres"
  ]
  node [
    id 931
    label "trawienie"
  ]
  node [
    id 932
    label "kategoria_gramatyczna"
  ]
  node [
    id 933
    label "period"
  ]
  node [
    id 934
    label "odczyt"
  ]
  node [
    id 935
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 936
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 937
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 938
    label "poprzedzenie"
  ]
  node [
    id 939
    label "koniugacja"
  ]
  node [
    id 940
    label "dzieje"
  ]
  node [
    id 941
    label "poprzedzi&#263;"
  ]
  node [
    id 942
    label "przep&#322;ywanie"
  ]
  node [
    id 943
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 944
    label "odwlekanie_si&#281;"
  ]
  node [
    id 945
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 946
    label "Zeitgeist"
  ]
  node [
    id 947
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 948
    label "okres_czasu"
  ]
  node [
    id 949
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 950
    label "pochodzi&#263;"
  ]
  node [
    id 951
    label "schy&#322;ek"
  ]
  node [
    id 952
    label "czwarty_wymiar"
  ]
  node [
    id 953
    label "chronometria"
  ]
  node [
    id 954
    label "poprzedzanie"
  ]
  node [
    id 955
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 956
    label "pogoda"
  ]
  node [
    id 957
    label "zegar"
  ]
  node [
    id 958
    label "trawi&#263;"
  ]
  node [
    id 959
    label "poprzedza&#263;"
  ]
  node [
    id 960
    label "time_period"
  ]
  node [
    id 961
    label "rachuba_czasu"
  ]
  node [
    id 962
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 963
    label "czasoprzestrze&#324;"
  ]
  node [
    id 964
    label "laba"
  ]
  node [
    id 965
    label "trza"
  ]
  node [
    id 966
    label "necessity"
  ]
  node [
    id 967
    label "odby&#263;"
  ]
  node [
    id 968
    label "przesun&#261;&#263;"
  ]
  node [
    id 969
    label "napa&#347;&#263;"
  ]
  node [
    id 970
    label "uda&#263;_si&#281;"
  ]
  node [
    id 971
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 972
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 973
    label "drive"
  ]
  node [
    id 974
    label "odjecha&#263;"
  ]
  node [
    id 975
    label "ride"
  ]
  node [
    id 976
    label "ruszy&#263;"
  ]
  node [
    id 977
    label "skorzysta&#263;"
  ]
  node [
    id 978
    label "samoch&#243;d"
  ]
  node [
    id 979
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 980
    label "matka"
  ]
  node [
    id 981
    label "kobieta"
  ]
  node [
    id 982
    label "partnerka"
  ]
  node [
    id 983
    label "&#380;ona"
  ]
  node [
    id 984
    label "starzy"
  ]
  node [
    id 985
    label "obserwacja"
  ]
  node [
    id 986
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 987
    label "umowa"
  ]
  node [
    id 988
    label "dominion"
  ]
  node [
    id 989
    label "qualification"
  ]
  node [
    id 990
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 991
    label "opis"
  ]
  node [
    id 992
    label "regu&#322;a_Allena"
  ]
  node [
    id 993
    label "normalizacja"
  ]
  node [
    id 994
    label "regu&#322;a_Glogera"
  ]
  node [
    id 995
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 996
    label "base"
  ]
  node [
    id 997
    label "substancja"
  ]
  node [
    id 998
    label "prawo_Mendla"
  ]
  node [
    id 999
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1000
    label "twierdzenie"
  ]
  node [
    id 1001
    label "prawo"
  ]
  node [
    id 1002
    label "occupation"
  ]
  node [
    id 1003
    label "zasada_d'Alemberta"
  ]
  node [
    id 1004
    label "zapis"
  ]
  node [
    id 1005
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 1006
    label "fonetyzacja"
  ]
  node [
    id 1007
    label "pismo"
  ]
  node [
    id 1008
    label "wyj&#261;tek"
  ]
  node [
    id 1009
    label "pisanie_si&#281;"
  ]
  node [
    id 1010
    label "czasownik"
  ]
  node [
    id 1011
    label "gwiazda"
  ]
  node [
    id 1012
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 1013
    label "letki"
  ]
  node [
    id 1014
    label "przyjemny"
  ]
  node [
    id 1015
    label "snadny"
  ]
  node [
    id 1016
    label "prosty"
  ]
  node [
    id 1017
    label "&#322;atwo"
  ]
  node [
    id 1018
    label "&#322;acny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 255
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 257
  ]
  edge [
    source 12
    target 258
  ]
  edge [
    source 12
    target 259
  ]
  edge [
    source 12
    target 260
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 262
  ]
  edge [
    source 13
    target 263
  ]
  edge [
    source 13
    target 264
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 266
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 14
    target 267
  ]
  edge [
    source 14
    target 268
  ]
  edge [
    source 14
    target 269
  ]
  edge [
    source 14
    target 270
  ]
  edge [
    source 14
    target 271
  ]
  edge [
    source 14
    target 272
  ]
  edge [
    source 14
    target 273
  ]
  edge [
    source 14
    target 274
  ]
  edge [
    source 14
    target 275
  ]
  edge [
    source 14
    target 276
  ]
  edge [
    source 14
    target 277
  ]
  edge [
    source 14
    target 278
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 279
  ]
  edge [
    source 15
    target 280
  ]
  edge [
    source 15
    target 281
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 282
  ]
  edge [
    source 16
    target 283
  ]
  edge [
    source 16
    target 284
  ]
  edge [
    source 16
    target 285
  ]
  edge [
    source 16
    target 286
  ]
  edge [
    source 16
    target 287
  ]
  edge [
    source 16
    target 288
  ]
  edge [
    source 16
    target 289
  ]
  edge [
    source 16
    target 290
  ]
  edge [
    source 16
    target 291
  ]
  edge [
    source 16
    target 292
  ]
  edge [
    source 16
    target 293
  ]
  edge [
    source 16
    target 294
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 17
    target 295
  ]
  edge [
    source 17
    target 296
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 62
  ]
  edge [
    source 17
    target 66
  ]
  edge [
    source 17
    target 70
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 18
    target 297
  ]
  edge [
    source 18
    target 298
  ]
  edge [
    source 18
    target 299
  ]
  edge [
    source 18
    target 300
  ]
  edge [
    source 18
    target 301
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 20
    target 29
  ]
  edge [
    source 20
    target 100
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 302
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 34
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 20
    target 66
  ]
  edge [
    source 20
    target 70
  ]
  edge [
    source 20
    target 104
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 40
  ]
  edge [
    source 21
    target 117
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 303
  ]
  edge [
    source 22
    target 304
  ]
  edge [
    source 22
    target 305
  ]
  edge [
    source 22
    target 306
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 22
    target 34
  ]
  edge [
    source 22
    target 62
  ]
  edge [
    source 22
    target 66
  ]
  edge [
    source 22
    target 70
  ]
  edge [
    source 22
    target 104
  ]
  edge [
    source 22
    target 126
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 307
  ]
  edge [
    source 23
    target 308
  ]
  edge [
    source 23
    target 309
  ]
  edge [
    source 23
    target 310
  ]
  edge [
    source 23
    target 311
  ]
  edge [
    source 23
    target 312
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 313
  ]
  edge [
    source 25
    target 314
  ]
  edge [
    source 25
    target 315
  ]
  edge [
    source 25
    target 316
  ]
  edge [
    source 25
    target 317
  ]
  edge [
    source 25
    target 318
  ]
  edge [
    source 25
    target 156
  ]
  edge [
    source 25
    target 319
  ]
  edge [
    source 25
    target 320
  ]
  edge [
    source 25
    target 321
  ]
  edge [
    source 25
    target 322
  ]
  edge [
    source 25
    target 323
  ]
  edge [
    source 25
    target 324
  ]
  edge [
    source 25
    target 325
  ]
  edge [
    source 25
    target 326
  ]
  edge [
    source 25
    target 327
  ]
  edge [
    source 25
    target 328
  ]
  edge [
    source 25
    target 329
  ]
  edge [
    source 25
    target 330
  ]
  edge [
    source 25
    target 331
  ]
  edge [
    source 25
    target 332
  ]
  edge [
    source 25
    target 333
  ]
  edge [
    source 25
    target 334
  ]
  edge [
    source 25
    target 335
  ]
  edge [
    source 25
    target 336
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 89
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 174
  ]
  edge [
    source 26
    target 337
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 338
  ]
  edge [
    source 27
    target 339
  ]
  edge [
    source 27
    target 340
  ]
  edge [
    source 27
    target 341
  ]
  edge [
    source 27
    target 342
  ]
  edge [
    source 27
    target 343
  ]
  edge [
    source 27
    target 344
  ]
  edge [
    source 27
    target 345
  ]
  edge [
    source 27
    target 346
  ]
  edge [
    source 27
    target 347
  ]
  edge [
    source 27
    target 41
  ]
  edge [
    source 27
    target 128
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 28
    target 56
  ]
  edge [
    source 28
    target 61
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 28
    target 70
  ]
  edge [
    source 28
    target 81
  ]
  edge [
    source 28
    target 82
  ]
  edge [
    source 28
    target 85
  ]
  edge [
    source 28
    target 40
  ]
  edge [
    source 28
    target 94
  ]
  edge [
    source 28
    target 105
  ]
  edge [
    source 28
    target 113
  ]
  edge [
    source 28
    target 348
  ]
  edge [
    source 28
    target 349
  ]
  edge [
    source 28
    target 350
  ]
  edge [
    source 28
    target 351
  ]
  edge [
    source 28
    target 352
  ]
  edge [
    source 28
    target 68
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 62
  ]
  edge [
    source 28
    target 66
  ]
  edge [
    source 28
    target 104
  ]
  edge [
    source 28
    target 126
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 28
    target 64
  ]
  edge [
    source 28
    target 73
  ]
  edge [
    source 28
    target 91
  ]
  edge [
    source 28
    target 103
  ]
  edge [
    source 28
    target 112
  ]
  edge [
    source 28
    target 117
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 353
  ]
  edge [
    source 29
    target 354
  ]
  edge [
    source 29
    target 355
  ]
  edge [
    source 29
    target 320
  ]
  edge [
    source 29
    target 356
  ]
  edge [
    source 29
    target 357
  ]
  edge [
    source 29
    target 358
  ]
  edge [
    source 29
    target 359
  ]
  edge [
    source 29
    target 360
  ]
  edge [
    source 29
    target 361
  ]
  edge [
    source 29
    target 362
  ]
  edge [
    source 29
    target 363
  ]
  edge [
    source 29
    target 364
  ]
  edge [
    source 29
    target 301
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 365
  ]
  edge [
    source 31
    target 366
  ]
  edge [
    source 31
    target 45
  ]
  edge [
    source 31
    target 46
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 367
  ]
  edge [
    source 33
    target 368
  ]
  edge [
    source 33
    target 369
  ]
  edge [
    source 33
    target 370
  ]
  edge [
    source 33
    target 371
  ]
  edge [
    source 33
    target 372
  ]
  edge [
    source 33
    target 373
  ]
  edge [
    source 33
    target 374
  ]
  edge [
    source 33
    target 274
  ]
  edge [
    source 33
    target 375
  ]
  edge [
    source 33
    target 376
  ]
  edge [
    source 33
    target 377
  ]
  edge [
    source 33
    target 378
  ]
  edge [
    source 33
    target 379
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 33
    target 380
  ]
  edge [
    source 33
    target 381
  ]
  edge [
    source 33
    target 382
  ]
  edge [
    source 33
    target 383
  ]
  edge [
    source 33
    target 384
  ]
  edge [
    source 33
    target 385
  ]
  edge [
    source 33
    target 386
  ]
  edge [
    source 33
    target 387
  ]
  edge [
    source 33
    target 388
  ]
  edge [
    source 33
    target 389
  ]
  edge [
    source 33
    target 390
  ]
  edge [
    source 33
    target 391
  ]
  edge [
    source 33
    target 392
  ]
  edge [
    source 33
    target 393
  ]
  edge [
    source 33
    target 394
  ]
  edge [
    source 33
    target 395
  ]
  edge [
    source 33
    target 396
  ]
  edge [
    source 33
    target 397
  ]
  edge [
    source 33
    target 398
  ]
  edge [
    source 33
    target 399
  ]
  edge [
    source 33
    target 400
  ]
  edge [
    source 33
    target 401
  ]
  edge [
    source 33
    target 402
  ]
  edge [
    source 33
    target 403
  ]
  edge [
    source 33
    target 404
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 405
  ]
  edge [
    source 34
    target 406
  ]
  edge [
    source 34
    target 407
  ]
  edge [
    source 34
    target 408
  ]
  edge [
    source 34
    target 409
  ]
  edge [
    source 34
    target 410
  ]
  edge [
    source 34
    target 411
  ]
  edge [
    source 34
    target 412
  ]
  edge [
    source 34
    target 413
  ]
  edge [
    source 34
    target 62
  ]
  edge [
    source 34
    target 66
  ]
  edge [
    source 34
    target 70
  ]
  edge [
    source 34
    target 104
  ]
  edge [
    source 34
    target 126
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 62
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 414
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 415
  ]
  edge [
    source 37
    target 416
  ]
  edge [
    source 37
    target 417
  ]
  edge [
    source 37
    target 418
  ]
  edge [
    source 37
    target 419
  ]
  edge [
    source 37
    target 420
  ]
  edge [
    source 37
    target 421
  ]
  edge [
    source 37
    target 422
  ]
  edge [
    source 37
    target 423
  ]
  edge [
    source 37
    target 424
  ]
  edge [
    source 37
    target 425
  ]
  edge [
    source 37
    target 426
  ]
  edge [
    source 37
    target 427
  ]
  edge [
    source 37
    target 428
  ]
  edge [
    source 37
    target 429
  ]
  edge [
    source 37
    target 222
  ]
  edge [
    source 37
    target 430
  ]
  edge [
    source 37
    target 431
  ]
  edge [
    source 37
    target 432
  ]
  edge [
    source 37
    target 433
  ]
  edge [
    source 37
    target 434
  ]
  edge [
    source 37
    target 435
  ]
  edge [
    source 37
    target 436
  ]
  edge [
    source 37
    target 437
  ]
  edge [
    source 37
    target 438
  ]
  edge [
    source 37
    target 439
  ]
  edge [
    source 37
    target 440
  ]
  edge [
    source 37
    target 441
  ]
  edge [
    source 37
    target 442
  ]
  edge [
    source 37
    target 443
  ]
  edge [
    source 37
    target 444
  ]
  edge [
    source 37
    target 445
  ]
  edge [
    source 37
    target 446
  ]
  edge [
    source 37
    target 447
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 73
  ]
  edge [
    source 38
    target 91
  ]
  edge [
    source 38
    target 103
  ]
  edge [
    source 38
    target 112
  ]
  edge [
    source 38
    target 117
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 448
  ]
  edge [
    source 39
    target 449
  ]
  edge [
    source 39
    target 450
  ]
  edge [
    source 39
    target 451
  ]
  edge [
    source 39
    target 447
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 72
  ]
  edge [
    source 40
    target 73
  ]
  edge [
    source 40
    target 81
  ]
  edge [
    source 40
    target 83
  ]
  edge [
    source 40
    target 86
  ]
  edge [
    source 40
    target 95
  ]
  edge [
    source 40
    target 101
  ]
  edge [
    source 40
    target 106
  ]
  edge [
    source 40
    target 107
  ]
  edge [
    source 40
    target 62
  ]
  edge [
    source 40
    target 110
  ]
  edge [
    source 40
    target 93
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 113
  ]
  edge [
    source 40
    target 114
  ]
  edge [
    source 40
    target 116
  ]
  edge [
    source 40
    target 129
  ]
  edge [
    source 40
    target 130
  ]
  edge [
    source 40
    target 452
  ]
  edge [
    source 40
    target 453
  ]
  edge [
    source 40
    target 454
  ]
  edge [
    source 40
    target 455
  ]
  edge [
    source 40
    target 456
  ]
  edge [
    source 40
    target 328
  ]
  edge [
    source 40
    target 457
  ]
  edge [
    source 40
    target 92
  ]
  edge [
    source 40
    target 458
  ]
  edge [
    source 40
    target 459
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 448
  ]
  edge [
    source 41
    target 48
  ]
  edge [
    source 41
    target 460
  ]
  edge [
    source 41
    target 461
  ]
  edge [
    source 41
    target 462
  ]
  edge [
    source 41
    target 463
  ]
  edge [
    source 41
    target 128
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 105
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 464
  ]
  edge [
    source 44
    target 465
  ]
  edge [
    source 44
    target 466
  ]
  edge [
    source 44
    target 467
  ]
  edge [
    source 44
    target 83
  ]
  edge [
    source 44
    target 468
  ]
  edge [
    source 44
    target 469
  ]
  edge [
    source 44
    target 331
  ]
  edge [
    source 44
    target 470
  ]
  edge [
    source 44
    target 471
  ]
  edge [
    source 44
    target 472
  ]
  edge [
    source 44
    target 473
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 474
  ]
  edge [
    source 46
    target 475
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 47
    target 52
  ]
  edge [
    source 48
    target 448
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 53
  ]
  edge [
    source 51
    target 476
  ]
  edge [
    source 51
    target 477
  ]
  edge [
    source 51
    target 478
  ]
  edge [
    source 51
    target 479
  ]
  edge [
    source 51
    target 480
  ]
  edge [
    source 51
    target 481
  ]
  edge [
    source 51
    target 482
  ]
  edge [
    source 51
    target 483
  ]
  edge [
    source 51
    target 81
  ]
  edge [
    source 51
    target 92
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 52
    target 66
  ]
  edge [
    source 52
    target 67
  ]
  edge [
    source 52
    target 484
  ]
  edge [
    source 52
    target 454
  ]
  edge [
    source 52
    target 485
  ]
  edge [
    source 52
    target 486
  ]
  edge [
    source 52
    target 487
  ]
  edge [
    source 52
    target 488
  ]
  edge [
    source 52
    target 489
  ]
  edge [
    source 52
    target 490
  ]
  edge [
    source 52
    target 97
  ]
  edge [
    source 52
    target 108
  ]
  edge [
    source 52
    target 93
  ]
  edge [
    source 52
    target 118
  ]
  edge [
    source 52
    target 130
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 491
  ]
  edge [
    source 55
    target 492
  ]
  edge [
    source 55
    target 440
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 493
  ]
  edge [
    source 57
    target 494
  ]
  edge [
    source 57
    target 495
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 455
  ]
  edge [
    source 58
    target 493
  ]
  edge [
    source 58
    target 496
  ]
  edge [
    source 58
    target 497
  ]
  edge [
    source 58
    target 498
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 499
  ]
  edge [
    source 59
    target 109
  ]
  edge [
    source 59
    target 500
  ]
  edge [
    source 59
    target 501
  ]
  edge [
    source 59
    target 502
  ]
  edge [
    source 59
    target 503
  ]
  edge [
    source 59
    target 486
  ]
  edge [
    source 59
    target 390
  ]
  edge [
    source 59
    target 504
  ]
  edge [
    source 59
    target 146
  ]
  edge [
    source 59
    target 505
  ]
  edge [
    source 59
    target 190
  ]
  edge [
    source 59
    target 81
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 506
  ]
  edge [
    source 60
    target 507
  ]
  edge [
    source 60
    target 508
  ]
  edge [
    source 60
    target 509
  ]
  edge [
    source 60
    target 85
  ]
  edge [
    source 60
    target 510
  ]
  edge [
    source 60
    target 511
  ]
  edge [
    source 60
    target 512
  ]
  edge [
    source 60
    target 513
  ]
  edge [
    source 60
    target 514
  ]
  edge [
    source 60
    target 515
  ]
  edge [
    source 60
    target 516
  ]
  edge [
    source 60
    target 517
  ]
  edge [
    source 60
    target 518
  ]
  edge [
    source 60
    target 519
  ]
  edge [
    source 60
    target 146
  ]
  edge [
    source 60
    target 520
  ]
  edge [
    source 60
    target 521
  ]
  edge [
    source 60
    target 522
  ]
  edge [
    source 60
    target 523
  ]
  edge [
    source 60
    target 524
  ]
  edge [
    source 60
    target 525
  ]
  edge [
    source 61
    target 526
  ]
  edge [
    source 61
    target 527
  ]
  edge [
    source 61
    target 528
  ]
  edge [
    source 61
    target 529
  ]
  edge [
    source 61
    target 67
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 109
  ]
  edge [
    source 62
    target 448
  ]
  edge [
    source 62
    target 66
  ]
  edge [
    source 62
    target 70
  ]
  edge [
    source 62
    target 104
  ]
  edge [
    source 62
    target 126
  ]
  edge [
    source 62
    target 123
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 71
  ]
  edge [
    source 63
    target 530
  ]
  edge [
    source 63
    target 531
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 532
  ]
  edge [
    source 64
    target 533
  ]
  edge [
    source 64
    target 534
  ]
  edge [
    source 64
    target 493
  ]
  edge [
    source 64
    target 535
  ]
  edge [
    source 64
    target 536
  ]
  edge [
    source 64
    target 537
  ]
  edge [
    source 64
    target 538
  ]
  edge [
    source 64
    target 539
  ]
  edge [
    source 64
    target 540
  ]
  edge [
    source 64
    target 541
  ]
  edge [
    source 64
    target 542
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 106
  ]
  edge [
    source 66
    target 180
  ]
  edge [
    source 66
    target 543
  ]
  edge [
    source 66
    target 544
  ]
  edge [
    source 66
    target 331
  ]
  edge [
    source 66
    target 545
  ]
  edge [
    source 66
    target 70
  ]
  edge [
    source 66
    target 104
  ]
  edge [
    source 66
    target 126
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 546
  ]
  edge [
    source 67
    target 547
  ]
  edge [
    source 67
    target 548
  ]
  edge [
    source 67
    target 440
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 549
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 550
  ]
  edge [
    source 69
    target 551
  ]
  edge [
    source 69
    target 552
  ]
  edge [
    source 69
    target 553
  ]
  edge [
    source 69
    target 554
  ]
  edge [
    source 69
    target 555
  ]
  edge [
    source 69
    target 112
  ]
  edge [
    source 69
    target 160
  ]
  edge [
    source 69
    target 556
  ]
  edge [
    source 69
    target 557
  ]
  edge [
    source 69
    target 558
  ]
  edge [
    source 69
    target 322
  ]
  edge [
    source 69
    target 559
  ]
  edge [
    source 69
    target 560
  ]
  edge [
    source 69
    target 561
  ]
  edge [
    source 69
    target 562
  ]
  edge [
    source 69
    target 563
  ]
  edge [
    source 69
    target 564
  ]
  edge [
    source 69
    target 565
  ]
  edge [
    source 69
    target 566
  ]
  edge [
    source 69
    target 567
  ]
  edge [
    source 69
    target 568
  ]
  edge [
    source 69
    target 569
  ]
  edge [
    source 69
    target 87
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 70
    target 570
  ]
  edge [
    source 70
    target 571
  ]
  edge [
    source 70
    target 104
  ]
  edge [
    source 70
    target 126
  ]
  edge [
    source 70
    target 87
  ]
  edge [
    source 71
    target 572
  ]
  edge [
    source 71
    target 573
  ]
  edge [
    source 71
    target 574
  ]
  edge [
    source 71
    target 575
  ]
  edge [
    source 71
    target 576
  ]
  edge [
    source 71
    target 577
  ]
  edge [
    source 71
    target 578
  ]
  edge [
    source 71
    target 579
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 72
    target 580
  ]
  edge [
    source 72
    target 210
  ]
  edge [
    source 72
    target 581
  ]
  edge [
    source 72
    target 582
  ]
  edge [
    source 72
    target 583
  ]
  edge [
    source 72
    target 584
  ]
  edge [
    source 72
    target 578
  ]
  edge [
    source 72
    target 585
  ]
  edge [
    source 72
    target 586
  ]
  edge [
    source 72
    target 587
  ]
  edge [
    source 72
    target 125
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 588
  ]
  edge [
    source 73
    target 589
  ]
  edge [
    source 73
    target 590
  ]
  edge [
    source 73
    target 205
  ]
  edge [
    source 73
    target 591
  ]
  edge [
    source 73
    target 592
  ]
  edge [
    source 73
    target 593
  ]
  edge [
    source 73
    target 91
  ]
  edge [
    source 73
    target 103
  ]
  edge [
    source 73
    target 112
  ]
  edge [
    source 73
    target 117
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 594
  ]
  edge [
    source 74
    target 595
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 596
  ]
  edge [
    source 75
    target 597
  ]
  edge [
    source 75
    target 598
  ]
  edge [
    source 75
    target 599
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 600
  ]
  edge [
    source 77
    target 601
  ]
  edge [
    source 77
    target 602
  ]
  edge [
    source 77
    target 603
  ]
  edge [
    source 77
    target 604
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 79
    target 605
  ]
  edge [
    source 79
    target 606
  ]
  edge [
    source 79
    target 607
  ]
  edge [
    source 79
    target 319
  ]
  edge [
    source 79
    target 608
  ]
  edge [
    source 79
    target 609
  ]
  edge [
    source 79
    target 610
  ]
  edge [
    source 79
    target 611
  ]
  edge [
    source 79
    target 612
  ]
  edge [
    source 79
    target 613
  ]
  edge [
    source 79
    target 614
  ]
  edge [
    source 79
    target 615
  ]
  edge [
    source 79
    target 616
  ]
  edge [
    source 79
    target 617
  ]
  edge [
    source 79
    target 618
  ]
  edge [
    source 79
    target 619
  ]
  edge [
    source 79
    target 620
  ]
  edge [
    source 79
    target 621
  ]
  edge [
    source 79
    target 622
  ]
  edge [
    source 79
    target 623
  ]
  edge [
    source 79
    target 624
  ]
  edge [
    source 79
    target 625
  ]
  edge [
    source 79
    target 626
  ]
  edge [
    source 79
    target 627
  ]
  edge [
    source 79
    target 628
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 407
  ]
  edge [
    source 80
    target 629
  ]
  edge [
    source 80
    target 630
  ]
  edge [
    source 80
    target 410
  ]
  edge [
    source 80
    target 413
  ]
  edge [
    source 80
    target 631
  ]
  edge [
    source 81
    target 394
  ]
  edge [
    source 81
    target 291
  ]
  edge [
    source 81
    target 632
  ]
  edge [
    source 81
    target 633
  ]
  edge [
    source 81
    target 634
  ]
  edge [
    source 81
    target 92
  ]
  edge [
    source 82
    target 635
  ]
  edge [
    source 82
    target 636
  ]
  edge [
    source 82
    target 637
  ]
  edge [
    source 82
    target 638
  ]
  edge [
    source 82
    target 639
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 640
  ]
  edge [
    source 83
    target 641
  ]
  edge [
    source 83
    target 331
  ]
  edge [
    source 83
    target 642
  ]
  edge [
    source 83
    target 643
  ]
  edge [
    source 83
    target 93
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 644
  ]
  edge [
    source 84
    target 468
  ]
  edge [
    source 84
    target 645
  ]
  edge [
    source 84
    target 646
  ]
  edge [
    source 84
    target 647
  ]
  edge [
    source 84
    target 94
  ]
  edge [
    source 85
    target 648
  ]
  edge [
    source 85
    target 649
  ]
  edge [
    source 85
    target 650
  ]
  edge [
    source 85
    target 651
  ]
  edge [
    source 85
    target 652
  ]
  edge [
    source 85
    target 653
  ]
  edge [
    source 85
    target 654
  ]
  edge [
    source 85
    target 655
  ]
  edge [
    source 85
    target 656
  ]
  edge [
    source 85
    target 657
  ]
  edge [
    source 85
    target 658
  ]
  edge [
    source 85
    target 659
  ]
  edge [
    source 85
    target 660
  ]
  edge [
    source 85
    target 661
  ]
  edge [
    source 85
    target 662
  ]
  edge [
    source 85
    target 663
  ]
  edge [
    source 85
    target 664
  ]
  edge [
    source 85
    target 665
  ]
  edge [
    source 85
    target 666
  ]
  edge [
    source 85
    target 667
  ]
  edge [
    source 85
    target 668
  ]
  edge [
    source 85
    target 669
  ]
  edge [
    source 85
    target 670
  ]
  edge [
    source 85
    target 671
  ]
  edge [
    source 85
    target 672
  ]
  edge [
    source 85
    target 673
  ]
  edge [
    source 85
    target 674
  ]
  edge [
    source 85
    target 675
  ]
  edge [
    source 85
    target 676
  ]
  edge [
    source 85
    target 677
  ]
  edge [
    source 85
    target 678
  ]
  edge [
    source 85
    target 679
  ]
  edge [
    source 85
    target 680
  ]
  edge [
    source 85
    target 681
  ]
  edge [
    source 85
    target 682
  ]
  edge [
    source 85
    target 683
  ]
  edge [
    source 85
    target 684
  ]
  edge [
    source 85
    target 685
  ]
  edge [
    source 85
    target 686
  ]
  edge [
    source 85
    target 687
  ]
  edge [
    source 85
    target 688
  ]
  edge [
    source 85
    target 689
  ]
  edge [
    source 85
    target 690
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 203
  ]
  edge [
    source 86
    target 691
  ]
  edge [
    source 86
    target 692
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 693
  ]
  edge [
    source 87
    target 694
  ]
  edge [
    source 87
    target 695
  ]
  edge [
    source 87
    target 696
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 697
  ]
  edge [
    source 88
    target 698
  ]
  edge [
    source 88
    target 699
  ]
  edge [
    source 88
    target 700
  ]
  edge [
    source 88
    target 701
  ]
  edge [
    source 88
    target 702
  ]
  edge [
    source 88
    target 703
  ]
  edge [
    source 88
    target 704
  ]
  edge [
    source 88
    target 705
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 706
  ]
  edge [
    source 89
    target 449
  ]
  edge [
    source 89
    target 707
  ]
  edge [
    source 89
    target 708
  ]
  edge [
    source 89
    target 709
  ]
  edge [
    source 89
    target 429
  ]
  edge [
    source 89
    target 710
  ]
  edge [
    source 89
    target 711
  ]
  edge [
    source 89
    target 712
  ]
  edge [
    source 89
    target 713
  ]
  edge [
    source 89
    target 714
  ]
  edge [
    source 89
    target 715
  ]
  edge [
    source 89
    target 716
  ]
  edge [
    source 89
    target 717
  ]
  edge [
    source 89
    target 718
  ]
  edge [
    source 89
    target 719
  ]
  edge [
    source 89
    target 720
  ]
  edge [
    source 89
    target 721
  ]
  edge [
    source 89
    target 722
  ]
  edge [
    source 89
    target 111
  ]
  edge [
    source 89
    target 723
  ]
  edge [
    source 89
    target 394
  ]
  edge [
    source 89
    target 724
  ]
  edge [
    source 89
    target 725
  ]
  edge [
    source 89
    target 726
  ]
  edge [
    source 89
    target 727
  ]
  edge [
    source 89
    target 728
  ]
  edge [
    source 90
    target 729
  ]
  edge [
    source 90
    target 730
  ]
  edge [
    source 90
    target 731
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 103
  ]
  edge [
    source 91
    target 112
  ]
  edge [
    source 91
    target 117
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 732
  ]
  edge [
    source 92
    target 733
  ]
  edge [
    source 92
    target 734
  ]
  edge [
    source 92
    target 429
  ]
  edge [
    source 92
    target 735
  ]
  edge [
    source 92
    target 736
  ]
  edge [
    source 92
    target 737
  ]
  edge [
    source 92
    target 738
  ]
  edge [
    source 92
    target 739
  ]
  edge [
    source 92
    target 740
  ]
  edge [
    source 92
    target 741
  ]
  edge [
    source 92
    target 742
  ]
  edge [
    source 92
    target 328
  ]
  edge [
    source 92
    target 743
  ]
  edge [
    source 92
    target 744
  ]
  edge [
    source 92
    target 745
  ]
  edge [
    source 92
    target 746
  ]
  edge [
    source 92
    target 747
  ]
  edge [
    source 92
    target 677
  ]
  edge [
    source 92
    target 748
  ]
  edge [
    source 92
    target 749
  ]
  edge [
    source 92
    target 750
  ]
  edge [
    source 92
    target 751
  ]
  edge [
    source 92
    target 752
  ]
  edge [
    source 92
    target 753
  ]
  edge [
    source 92
    target 754
  ]
  edge [
    source 92
    target 755
  ]
  edge [
    source 92
    target 756
  ]
  edge [
    source 92
    target 757
  ]
  edge [
    source 92
    target 758
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 112
  ]
  edge [
    source 93
    target 759
  ]
  edge [
    source 93
    target 760
  ]
  edge [
    source 93
    target 97
  ]
  edge [
    source 93
    target 108
  ]
  edge [
    source 93
    target 118
  ]
  edge [
    source 93
    target 130
  ]
  edge [
    source 94
    target 761
  ]
  edge [
    source 94
    target 762
  ]
  edge [
    source 94
    target 763
  ]
  edge [
    source 94
    target 764
  ]
  edge [
    source 94
    target 765
  ]
  edge [
    source 94
    target 766
  ]
  edge [
    source 94
    target 767
  ]
  edge [
    source 94
    target 768
  ]
  edge [
    source 94
    target 769
  ]
  edge [
    source 94
    target 770
  ]
  edge [
    source 94
    target 771
  ]
  edge [
    source 94
    target 772
  ]
  edge [
    source 94
    target 773
  ]
  edge [
    source 94
    target 774
  ]
  edge [
    source 94
    target 775
  ]
  edge [
    source 94
    target 776
  ]
  edge [
    source 94
    target 777
  ]
  edge [
    source 94
    target 778
  ]
  edge [
    source 94
    target 779
  ]
  edge [
    source 94
    target 780
  ]
  edge [
    source 94
    target 781
  ]
  edge [
    source 94
    target 782
  ]
  edge [
    source 94
    target 783
  ]
  edge [
    source 94
    target 784
  ]
  edge [
    source 94
    target 785
  ]
  edge [
    source 94
    target 786
  ]
  edge [
    source 94
    target 787
  ]
  edge [
    source 94
    target 788
  ]
  edge [
    source 94
    target 789
  ]
  edge [
    source 94
    target 673
  ]
  edge [
    source 94
    target 790
  ]
  edge [
    source 94
    target 791
  ]
  edge [
    source 94
    target 792
  ]
  edge [
    source 94
    target 793
  ]
  edge [
    source 94
    target 794
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 795
  ]
  edge [
    source 95
    target 255
  ]
  edge [
    source 95
    target 796
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 104
  ]
  edge [
    source 97
    target 105
  ]
  edge [
    source 97
    target 797
  ]
  edge [
    source 97
    target 798
  ]
  edge [
    source 97
    target 799
  ]
  edge [
    source 97
    target 800
  ]
  edge [
    source 97
    target 801
  ]
  edge [
    source 97
    target 802
  ]
  edge [
    source 97
    target 108
  ]
  edge [
    source 97
    target 118
  ]
  edge [
    source 97
    target 130
  ]
  edge [
    source 97
    target 125
  ]
  edge [
    source 98
    target 803
  ]
  edge [
    source 98
    target 804
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 805
  ]
  edge [
    source 99
    target 806
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 807
  ]
  edge [
    source 101
    target 808
  ]
  edge [
    source 101
    target 809
  ]
  edge [
    source 101
    target 810
  ]
  edge [
    source 101
    target 811
  ]
  edge [
    source 101
    target 812
  ]
  edge [
    source 101
    target 813
  ]
  edge [
    source 101
    target 814
  ]
  edge [
    source 101
    target 774
  ]
  edge [
    source 101
    target 780
  ]
  edge [
    source 101
    target 815
  ]
  edge [
    source 101
    target 816
  ]
  edge [
    source 101
    target 817
  ]
  edge [
    source 101
    target 818
  ]
  edge [
    source 101
    target 819
  ]
  edge [
    source 101
    target 820
  ]
  edge [
    source 101
    target 331
  ]
  edge [
    source 101
    target 821
  ]
  edge [
    source 101
    target 597
  ]
  edge [
    source 101
    target 822
  ]
  edge [
    source 101
    target 785
  ]
  edge [
    source 101
    target 786
  ]
  edge [
    source 101
    target 791
  ]
  edge [
    source 101
    target 643
  ]
  edge [
    source 101
    target 617
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 105
  ]
  edge [
    source 103
    target 106
  ]
  edge [
    source 103
    target 656
  ]
  edge [
    source 103
    target 657
  ]
  edge [
    source 103
    target 394
  ]
  edge [
    source 103
    target 823
  ]
  edge [
    source 103
    target 824
  ]
  edge [
    source 103
    target 825
  ]
  edge [
    source 103
    target 826
  ]
  edge [
    source 103
    target 827
  ]
  edge [
    source 103
    target 828
  ]
  edge [
    source 103
    target 829
  ]
  edge [
    source 103
    target 112
  ]
  edge [
    source 103
    target 117
  ]
  edge [
    source 104
    target 126
  ]
  edge [
    source 105
    target 640
  ]
  edge [
    source 105
    target 830
  ]
  edge [
    source 105
    target 831
  ]
  edge [
    source 106
    target 832
  ]
  edge [
    source 106
    target 833
  ]
  edge [
    source 106
    target 834
  ]
  edge [
    source 106
    target 731
  ]
  edge [
    source 107
    target 114
  ]
  edge [
    source 107
    target 835
  ]
  edge [
    source 107
    target 836
  ]
  edge [
    source 107
    target 837
  ]
  edge [
    source 107
    target 394
  ]
  edge [
    source 107
    target 838
  ]
  edge [
    source 107
    target 839
  ]
  edge [
    source 107
    target 840
  ]
  edge [
    source 107
    target 841
  ]
  edge [
    source 107
    target 842
  ]
  edge [
    source 107
    target 843
  ]
  edge [
    source 107
    target 844
  ]
  edge [
    source 107
    target 146
  ]
  edge [
    source 107
    target 442
  ]
  edge [
    source 107
    target 845
  ]
  edge [
    source 107
    target 274
  ]
  edge [
    source 107
    target 846
  ]
  edge [
    source 107
    target 847
  ]
  edge [
    source 107
    target 848
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 849
  ]
  edge [
    source 108
    target 850
  ]
  edge [
    source 108
    target 851
  ]
  edge [
    source 108
    target 852
  ]
  edge [
    source 108
    target 853
  ]
  edge [
    source 108
    target 854
  ]
  edge [
    source 108
    target 855
  ]
  edge [
    source 108
    target 856
  ]
  edge [
    source 108
    target 857
  ]
  edge [
    source 108
    target 858
  ]
  edge [
    source 108
    target 859
  ]
  edge [
    source 108
    target 860
  ]
  edge [
    source 108
    target 861
  ]
  edge [
    source 108
    target 862
  ]
  edge [
    source 108
    target 863
  ]
  edge [
    source 108
    target 864
  ]
  edge [
    source 108
    target 118
  ]
  edge [
    source 108
    target 130
  ]
  edge [
    source 109
    target 782
  ]
  edge [
    source 109
    target 225
  ]
  edge [
    source 109
    target 865
  ]
  edge [
    source 109
    target 866
  ]
  edge [
    source 109
    target 867
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 868
  ]
  edge [
    source 110
    target 869
  ]
  edge [
    source 110
    target 870
  ]
  edge [
    source 110
    target 871
  ]
  edge [
    source 110
    target 872
  ]
  edge [
    source 110
    target 873
  ]
  edge [
    source 110
    target 874
  ]
  edge [
    source 110
    target 875
  ]
  edge [
    source 110
    target 876
  ]
  edge [
    source 110
    target 877
  ]
  edge [
    source 110
    target 878
  ]
  edge [
    source 110
    target 879
  ]
  edge [
    source 110
    target 880
  ]
  edge [
    source 110
    target 881
  ]
  edge [
    source 110
    target 882
  ]
  edge [
    source 110
    target 883
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 144
  ]
  edge [
    source 111
    target 884
  ]
  edge [
    source 111
    target 390
  ]
  edge [
    source 111
    target 387
  ]
  edge [
    source 111
    target 885
  ]
  edge [
    source 111
    target 886
  ]
  edge [
    source 112
    target 887
  ]
  edge [
    source 112
    target 888
  ]
  edge [
    source 112
    target 889
  ]
  edge [
    source 112
    target 890
  ]
  edge [
    source 112
    target 891
  ]
  edge [
    source 112
    target 892
  ]
  edge [
    source 112
    target 893
  ]
  edge [
    source 112
    target 164
  ]
  edge [
    source 112
    target 894
  ]
  edge [
    source 112
    target 895
  ]
  edge [
    source 112
    target 896
  ]
  edge [
    source 112
    target 897
  ]
  edge [
    source 112
    target 175
  ]
  edge [
    source 112
    target 898
  ]
  edge [
    source 112
    target 179
  ]
  edge [
    source 112
    target 117
  ]
  edge [
    source 115
    target 899
  ]
  edge [
    source 115
    target 900
  ]
  edge [
    source 115
    target 901
  ]
  edge [
    source 115
    target 902
  ]
  edge [
    source 115
    target 903
  ]
  edge [
    source 115
    target 904
  ]
  edge [
    source 115
    target 905
  ]
  edge [
    source 115
    target 906
  ]
  edge [
    source 115
    target 907
  ]
  edge [
    source 116
    target 908
  ]
  edge [
    source 116
    target 909
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 910
  ]
  edge [
    source 117
    target 911
  ]
  edge [
    source 117
    target 912
  ]
  edge [
    source 117
    target 913
  ]
  edge [
    source 117
    target 914
  ]
  edge [
    source 117
    target 915
  ]
  edge [
    source 117
    target 916
  ]
  edge [
    source 117
    target 917
  ]
  edge [
    source 117
    target 918
  ]
  edge [
    source 117
    target 919
  ]
  edge [
    source 117
    target 920
  ]
  edge [
    source 117
    target 921
  ]
  edge [
    source 117
    target 922
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 923
  ]
  edge [
    source 118
    target 298
  ]
  edge [
    source 118
    target 924
  ]
  edge [
    source 118
    target 328
  ]
  edge [
    source 118
    target 925
  ]
  edge [
    source 118
    target 926
  ]
  edge [
    source 118
    target 331
  ]
  edge [
    source 118
    target 927
  ]
  edge [
    source 118
    target 928
  ]
  edge [
    source 118
    target 617
  ]
  edge [
    source 118
    target 300
  ]
  edge [
    source 118
    target 130
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 128
  ]
  edge [
    source 119
    target 129
  ]
  edge [
    source 119
    target 929
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 930
  ]
  edge [
    source 120
    target 931
  ]
  edge [
    source 120
    target 932
  ]
  edge [
    source 120
    target 933
  ]
  edge [
    source 120
    target 934
  ]
  edge [
    source 120
    target 935
  ]
  edge [
    source 120
    target 936
  ]
  edge [
    source 120
    target 844
  ]
  edge [
    source 120
    target 937
  ]
  edge [
    source 120
    target 938
  ]
  edge [
    source 120
    target 939
  ]
  edge [
    source 120
    target 940
  ]
  edge [
    source 120
    target 941
  ]
  edge [
    source 120
    target 942
  ]
  edge [
    source 120
    target 943
  ]
  edge [
    source 120
    target 944
  ]
  edge [
    source 120
    target 945
  ]
  edge [
    source 120
    target 946
  ]
  edge [
    source 120
    target 947
  ]
  edge [
    source 120
    target 948
  ]
  edge [
    source 120
    target 949
  ]
  edge [
    source 120
    target 950
  ]
  edge [
    source 120
    target 951
  ]
  edge [
    source 120
    target 952
  ]
  edge [
    source 120
    target 953
  ]
  edge [
    source 120
    target 954
  ]
  edge [
    source 120
    target 955
  ]
  edge [
    source 120
    target 956
  ]
  edge [
    source 120
    target 957
  ]
  edge [
    source 120
    target 958
  ]
  edge [
    source 120
    target 678
  ]
  edge [
    source 120
    target 959
  ]
  edge [
    source 120
    target 960
  ]
  edge [
    source 120
    target 961
  ]
  edge [
    source 120
    target 962
  ]
  edge [
    source 120
    target 963
  ]
  edge [
    source 120
    target 964
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 965
  ]
  edge [
    source 121
    target 966
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 967
  ]
  edge [
    source 122
    target 968
  ]
  edge [
    source 122
    target 969
  ]
  edge [
    source 122
    target 970
  ]
  edge [
    source 122
    target 971
  ]
  edge [
    source 122
    target 972
  ]
  edge [
    source 122
    target 973
  ]
  edge [
    source 122
    target 974
  ]
  edge [
    source 122
    target 975
  ]
  edge [
    source 122
    target 976
  ]
  edge [
    source 122
    target 977
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 978
  ]
  edge [
    source 123
    target 979
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 980
  ]
  edge [
    source 124
    target 981
  ]
  edge [
    source 124
    target 982
  ]
  edge [
    source 124
    target 983
  ]
  edge [
    source 124
    target 984
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 985
  ]
  edge [
    source 125
    target 574
  ]
  edge [
    source 125
    target 213
  ]
  edge [
    source 125
    target 986
  ]
  edge [
    source 125
    target 987
  ]
  edge [
    source 125
    target 988
  ]
  edge [
    source 125
    target 989
  ]
  edge [
    source 125
    target 990
  ]
  edge [
    source 125
    target 991
  ]
  edge [
    source 125
    target 992
  ]
  edge [
    source 125
    target 993
  ]
  edge [
    source 125
    target 994
  ]
  edge [
    source 125
    target 995
  ]
  edge [
    source 125
    target 996
  ]
  edge [
    source 125
    target 997
  ]
  edge [
    source 125
    target 231
  ]
  edge [
    source 125
    target 577
  ]
  edge [
    source 125
    target 998
  ]
  edge [
    source 125
    target 999
  ]
  edge [
    source 125
    target 578
  ]
  edge [
    source 125
    target 1000
  ]
  edge [
    source 125
    target 241
  ]
  edge [
    source 125
    target 1001
  ]
  edge [
    source 125
    target 1002
  ]
  edge [
    source 125
    target 1003
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 1004
  ]
  edge [
    source 126
    target 1005
  ]
  edge [
    source 126
    target 1006
  ]
  edge [
    source 126
    target 1007
  ]
  edge [
    source 126
    target 1008
  ]
  edge [
    source 126
    target 1009
  ]
  edge [
    source 127
    target 1010
  ]
  edge [
    source 128
    target 831
  ]
  edge [
    source 129
    target 1011
  ]
  edge [
    source 129
    target 1012
  ]
  edge [
    source 130
    target 1013
  ]
  edge [
    source 130
    target 1014
  ]
  edge [
    source 130
    target 1015
  ]
  edge [
    source 130
    target 1016
  ]
  edge [
    source 130
    target 1017
  ]
  edge [
    source 130
    target 1018
  ]
]
