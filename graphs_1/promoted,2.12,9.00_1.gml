graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "tried"
    origin "text"
  ]
  node [
    id 1
    label "somebody's"
    origin "text"
  ]
  node [
    id 2
    label "work"
    origin "text"
  ]
  node [
    id 3
    label "after"
    origin "text"
  ]
  node [
    id 4
    label "interview"
    origin "text"
  ]
  node [
    id 5
    label "rozmowa"
  ]
  node [
    id 6
    label "autoryzowanie"
  ]
  node [
    id 7
    label "consultation"
  ]
  node [
    id 8
    label "autoryzowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
]
