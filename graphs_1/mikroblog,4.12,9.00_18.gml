graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 2
  node [
    id 0
    label "taki"
    origin "text"
  ]
  node [
    id 1
    label "chop"
    origin "text"
  ]
  node [
    id 2
    label "klep"
    origin "text"
  ]
  node [
    id 3
    label "twoja"
    origin "text"
  ]
  node [
    id 4
    label "dziewczyne"
    origin "text"
  ]
  node [
    id 5
    label "ty&#322;ek"
    origin "text"
  ]
  node [
    id 6
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "okre&#347;lony"
  ]
  node [
    id 8
    label "jaki&#347;"
  ]
  node [
    id 9
    label "dupa"
  ]
  node [
    id 10
    label "tu&#322;&#243;w"
  ]
  node [
    id 11
    label "ty&#322;"
  ]
  node [
    id 12
    label "sempiterna"
  ]
  node [
    id 13
    label "tentegowa&#263;"
  ]
  node [
    id 14
    label "urz&#261;dza&#263;"
  ]
  node [
    id 15
    label "give"
  ]
  node [
    id 16
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 17
    label "czyni&#263;"
  ]
  node [
    id 18
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 19
    label "post&#281;powa&#263;"
  ]
  node [
    id 20
    label "wydala&#263;"
  ]
  node [
    id 21
    label "oszukiwa&#263;"
  ]
  node [
    id 22
    label "organizowa&#263;"
  ]
  node [
    id 23
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 24
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 25
    label "work"
  ]
  node [
    id 26
    label "przerabia&#263;"
  ]
  node [
    id 27
    label "stylizowa&#263;"
  ]
  node [
    id 28
    label "falowa&#263;"
  ]
  node [
    id 29
    label "act"
  ]
  node [
    id 30
    label "peddle"
  ]
  node [
    id 31
    label "ukazywa&#263;"
  ]
  node [
    id 32
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 33
    label "praca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
]
