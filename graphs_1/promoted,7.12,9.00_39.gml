graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.06436781609195402
  graphCliqueNumber 2
  node [
    id 0
    label "podj&#261;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wszystek"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;liwy"
    origin "text"
  ]
  node [
    id 3
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 4
    label "zmieni&#263;"
  ]
  node [
    id 5
    label "zacz&#261;&#263;"
  ]
  node [
    id 6
    label "zareagowa&#263;"
  ]
  node [
    id 7
    label "allude"
  ]
  node [
    id 8
    label "raise"
  ]
  node [
    id 9
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 10
    label "draw"
  ]
  node [
    id 11
    label "zrobi&#263;"
  ]
  node [
    id 12
    label "ca&#322;y"
  ]
  node [
    id 13
    label "zno&#347;ny"
  ]
  node [
    id 14
    label "mo&#380;liwie"
  ]
  node [
    id 15
    label "urealnianie"
  ]
  node [
    id 16
    label "umo&#380;liwienie"
  ]
  node [
    id 17
    label "mo&#380;ebny"
  ]
  node [
    id 18
    label "umo&#380;liwianie"
  ]
  node [
    id 19
    label "dost&#281;pny"
  ]
  node [
    id 20
    label "urealnienie"
  ]
  node [
    id 21
    label "miejsce"
  ]
  node [
    id 22
    label "czas"
  ]
  node [
    id 23
    label "abstrakcja"
  ]
  node [
    id 24
    label "punkt"
  ]
  node [
    id 25
    label "substancja"
  ]
  node [
    id 26
    label "spos&#243;b"
  ]
  node [
    id 27
    label "chemikalia"
  ]
  node [
    id 28
    label "Huawei"
  ]
  node [
    id 29
    label "Ltd"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 28
    target 29
  ]
]
