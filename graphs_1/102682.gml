graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9444444444444444
  density 0.05555555555555555
  graphCliqueNumber 2
  node [
    id 0
    label "dzienia"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "xxx"
    origin "text"
  ]
  node [
    id 3
    label "czym"
    origin "text"
  ]
  node [
    id 4
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 5
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 6
    label "pomy&#347;lny"
  ]
  node [
    id 7
    label "skuteczny"
  ]
  node [
    id 8
    label "moralny"
  ]
  node [
    id 9
    label "korzystny"
  ]
  node [
    id 10
    label "odpowiedni"
  ]
  node [
    id 11
    label "zwrot"
  ]
  node [
    id 12
    label "dobrze"
  ]
  node [
    id 13
    label "pozytywny"
  ]
  node [
    id 14
    label "grzeczny"
  ]
  node [
    id 15
    label "powitanie"
  ]
  node [
    id 16
    label "mi&#322;y"
  ]
  node [
    id 17
    label "dobroczynny"
  ]
  node [
    id 18
    label "pos&#322;uszny"
  ]
  node [
    id 19
    label "ca&#322;y"
  ]
  node [
    id 20
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 21
    label "czw&#243;rka"
  ]
  node [
    id 22
    label "spokojny"
  ]
  node [
    id 23
    label "&#347;mieszny"
  ]
  node [
    id 24
    label "drogi"
  ]
  node [
    id 25
    label "by&#263;"
  ]
  node [
    id 26
    label "uprawi&#263;"
  ]
  node [
    id 27
    label "gotowy"
  ]
  node [
    id 28
    label "might"
  ]
  node [
    id 29
    label "help"
  ]
  node [
    id 30
    label "aid"
  ]
  node [
    id 31
    label "u&#322;atwi&#263;"
  ]
  node [
    id 32
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 33
    label "concur"
  ]
  node [
    id 34
    label "zrobi&#263;"
  ]
  node [
    id 35
    label "zaskutkowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
]
