graph [
  maxDegree 14
  minDegree 1
  meanDegree 2.4827586206896552
  density 0.043557168784029036
  graphCliqueNumber 5
  node [
    id 0
    label "dywizja"
    origin "text"
  ]
  node [
    id 1
    label "piechota"
    origin "text"
  ]
  node [
    id 2
    label "psz"
    origin "text"
  ]
  node [
    id 3
    label "brygada"
  ]
  node [
    id 4
    label "zwi&#261;zek_taktyczny"
  ]
  node [
    id 5
    label "korpus"
  ]
  node [
    id 6
    label "armia"
  ]
  node [
    id 7
    label "kompania_honorowa"
  ]
  node [
    id 8
    label "formacja"
  ]
  node [
    id 9
    label "falanga"
  ]
  node [
    id 10
    label "infantry"
  ]
  node [
    id 11
    label "11"
  ]
  node [
    id 12
    label "polskie"
  ]
  node [
    id 13
    label "si&#322;y"
  ]
  node [
    id 14
    label "zbrojny"
  ]
  node [
    id 15
    label "DP"
  ]
  node [
    id 16
    label "o&#347;rodek"
  ]
  node [
    id 17
    label "organizacyjny"
  ]
  node [
    id 18
    label "Leona"
  ]
  node [
    id 19
    label "koc"
  ]
  node [
    id 20
    label "zapasowy"
  ]
  node [
    id 21
    label "19"
  ]
  node [
    id 22
    label "pu&#322;k"
  ]
  node [
    id 23
    label "7"
  ]
  node [
    id 24
    label "21"
  ]
  node [
    id 25
    label "artyleria"
  ]
  node [
    id 26
    label "batalion"
  ]
  node [
    id 27
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 28
    label "oddzia&#322;"
  ]
  node [
    id 29
    label "rozpoznawczy"
  ]
  node [
    id 30
    label "2"
  ]
  node [
    id 31
    label "kompania"
  ]
  node [
    id 32
    label "&#380;andarmeria"
  ]
  node [
    id 33
    label "sanitarny"
  ]
  node [
    id 34
    label "kolumna"
  ]
  node [
    id 35
    label "samochodowy"
  ]
  node [
    id 36
    label "park"
  ]
  node [
    id 37
    label "intendentura"
  ]
  node [
    id 38
    label "s&#261;d"
  ]
  node [
    id 39
    label "polowy"
  ]
  node [
    id 40
    label "szko&#322;a"
  ]
  node [
    id 41
    label "podchor&#261;&#380;y"
  ]
  node [
    id 42
    label "pomocniczy"
  ]
  node [
    id 43
    label "s&#322;u&#380;ba"
  ]
  node [
    id 44
    label "kobieta"
  ]
  node [
    id 45
    label "kierowca"
  ]
  node [
    id 46
    label "junak"
  ]
  node [
    id 47
    label "drogowy"
  ]
  node [
    id 48
    label "dyscyplinarny"
  ]
  node [
    id 49
    label "komenda"
  ]
  node [
    id 50
    label "uzupe&#322;nienie"
  ]
  node [
    id 51
    label "Nr"
  ]
  node [
    id 52
    label "oficerski"
  ]
  node [
    id 53
    label "stacja"
  ]
  node [
    id 54
    label "zborny"
  ]
  node [
    id 55
    label "pluton"
  ]
  node [
    id 56
    label "opieka"
  ]
  node [
    id 57
    label "spo&#322;eczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 42
  ]
  edge [
    source 16
    target 43
  ]
  edge [
    source 16
    target 44
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 20
    target 29
  ]
  edge [
    source 20
    target 33
  ]
  edge [
    source 20
    target 34
  ]
  edge [
    source 20
    target 35
  ]
  edge [
    source 20
    target 42
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 20
    target 44
  ]
  edge [
    source 20
    target 47
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 23
    target 49
  ]
  edge [
    source 23
    target 50
  ]
  edge [
    source 23
    target 51
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 47
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 48
  ]
  edge [
    source 31
    target 52
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 45
  ]
  edge [
    source 40
    target 46
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 56
    target 57
  ]
]
