graph [
  maxDegree 35
  minDegree 1
  meanDegree 2.018867924528302
  density 0.01922731356693621
  graphCliqueNumber 2
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pami&#261;tka"
    origin "text"
  ]
  node [
    id 3
    label "moi"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wi&#281;tej_pami&#281;ci"
    origin "text"
  ]
  node [
    id 5
    label "pradziadek"
    origin "text"
  ]
  node [
    id 6
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "lato"
    origin "text"
  ]
  node [
    id 10
    label "osiemdziesi&#261;ty"
    origin "text"
  ]
  node [
    id 11
    label "dok&#322;adnie"
    origin "text"
  ]
  node [
    id 12
    label "strona"
    origin "text"
  ]
  node [
    id 13
    label "matczysko"
  ]
  node [
    id 14
    label "macierz"
  ]
  node [
    id 15
    label "przodkini"
  ]
  node [
    id 16
    label "Matka_Boska"
  ]
  node [
    id 17
    label "macocha"
  ]
  node [
    id 18
    label "matka_zast&#281;pcza"
  ]
  node [
    id 19
    label "stara"
  ]
  node [
    id 20
    label "rodzice"
  ]
  node [
    id 21
    label "rodzic"
  ]
  node [
    id 22
    label "spowodowa&#263;"
  ]
  node [
    id 23
    label "wyrazi&#263;"
  ]
  node [
    id 24
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 25
    label "przedstawi&#263;"
  ]
  node [
    id 26
    label "testify"
  ]
  node [
    id 27
    label "indicate"
  ]
  node [
    id 28
    label "przeszkoli&#263;"
  ]
  node [
    id 29
    label "udowodni&#263;"
  ]
  node [
    id 30
    label "poinformowa&#263;"
  ]
  node [
    id 31
    label "poda&#263;"
  ]
  node [
    id 32
    label "point"
  ]
  node [
    id 33
    label "&#347;wiadectwo"
  ]
  node [
    id 34
    label "przedmiot"
  ]
  node [
    id 35
    label "pradziadkowie"
  ]
  node [
    id 36
    label "przodek"
  ]
  node [
    id 37
    label "pradziad"
  ]
  node [
    id 38
    label "ok&#322;adka"
  ]
  node [
    id 39
    label "zak&#322;adka"
  ]
  node [
    id 40
    label "ekslibris"
  ]
  node [
    id 41
    label "wk&#322;ad"
  ]
  node [
    id 42
    label "przek&#322;adacz"
  ]
  node [
    id 43
    label "wydawnictwo"
  ]
  node [
    id 44
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 45
    label "tytu&#322;"
  ]
  node [
    id 46
    label "bibliofilstwo"
  ]
  node [
    id 47
    label "falc"
  ]
  node [
    id 48
    label "nomina&#322;"
  ]
  node [
    id 49
    label "pagina"
  ]
  node [
    id 50
    label "rozdzia&#322;"
  ]
  node [
    id 51
    label "egzemplarz"
  ]
  node [
    id 52
    label "zw&#243;j"
  ]
  node [
    id 53
    label "tekst"
  ]
  node [
    id 54
    label "ozdabia&#263;"
  ]
  node [
    id 55
    label "dysgrafia"
  ]
  node [
    id 56
    label "prasa"
  ]
  node [
    id 57
    label "spell"
  ]
  node [
    id 58
    label "skryba"
  ]
  node [
    id 59
    label "donosi&#263;"
  ]
  node [
    id 60
    label "code"
  ]
  node [
    id 61
    label "dysortografia"
  ]
  node [
    id 62
    label "read"
  ]
  node [
    id 63
    label "tworzy&#263;"
  ]
  node [
    id 64
    label "formu&#322;owa&#263;"
  ]
  node [
    id 65
    label "styl"
  ]
  node [
    id 66
    label "stawia&#263;"
  ]
  node [
    id 67
    label "pora_roku"
  ]
  node [
    id 68
    label "punctiliously"
  ]
  node [
    id 69
    label "dok&#322;adny"
  ]
  node [
    id 70
    label "meticulously"
  ]
  node [
    id 71
    label "precyzyjnie"
  ]
  node [
    id 72
    label "rzetelnie"
  ]
  node [
    id 73
    label "skr&#281;canie"
  ]
  node [
    id 74
    label "voice"
  ]
  node [
    id 75
    label "forma"
  ]
  node [
    id 76
    label "internet"
  ]
  node [
    id 77
    label "skr&#281;ci&#263;"
  ]
  node [
    id 78
    label "kartka"
  ]
  node [
    id 79
    label "orientowa&#263;"
  ]
  node [
    id 80
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 81
    label "powierzchnia"
  ]
  node [
    id 82
    label "plik"
  ]
  node [
    id 83
    label "bok"
  ]
  node [
    id 84
    label "orientowanie"
  ]
  node [
    id 85
    label "fragment"
  ]
  node [
    id 86
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 87
    label "s&#261;d"
  ]
  node [
    id 88
    label "skr&#281;ca&#263;"
  ]
  node [
    id 89
    label "g&#243;ra"
  ]
  node [
    id 90
    label "serwis_internetowy"
  ]
  node [
    id 91
    label "orientacja"
  ]
  node [
    id 92
    label "linia"
  ]
  node [
    id 93
    label "skr&#281;cenie"
  ]
  node [
    id 94
    label "layout"
  ]
  node [
    id 95
    label "zorientowa&#263;"
  ]
  node [
    id 96
    label "zorientowanie"
  ]
  node [
    id 97
    label "obiekt"
  ]
  node [
    id 98
    label "podmiot"
  ]
  node [
    id 99
    label "ty&#322;"
  ]
  node [
    id 100
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 101
    label "logowanie"
  ]
  node [
    id 102
    label "adres_internetowy"
  ]
  node [
    id 103
    label "uj&#281;cie"
  ]
  node [
    id 104
    label "prz&#243;d"
  ]
  node [
    id 105
    label "posta&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 49
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
]
