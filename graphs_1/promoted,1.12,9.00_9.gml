graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.978021978021978
  density 0.02197802197802198
  graphCliqueNumber 2
  node [
    id 0
    label "chiny"
    origin "text"
  ]
  node [
    id 1
    label "wypu&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "sp&#281;dzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "lato"
    origin "text"
  ]
  node [
    id 7
    label "wi&#281;zienie"
    origin "text"
  ]
  node [
    id 8
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 9
    label "zarzut"
    origin "text"
  ]
  node [
    id 10
    label "gwa&#322;t"
    origin "text"
  ]
  node [
    id 11
    label "morderstwo"
    origin "text"
  ]
  node [
    id 12
    label "rynek"
  ]
  node [
    id 13
    label "pu&#347;ci&#263;"
  ]
  node [
    id 14
    label "wykszta&#322;ci&#263;"
  ]
  node [
    id 15
    label "zej&#347;&#263;"
  ]
  node [
    id 16
    label "issue"
  ]
  node [
    id 17
    label "pozwoli&#263;"
  ]
  node [
    id 18
    label "wyda&#263;"
  ]
  node [
    id 19
    label "leave"
  ]
  node [
    id 20
    label "release"
  ]
  node [
    id 21
    label "przesta&#263;"
  ]
  node [
    id 22
    label "picture"
  ]
  node [
    id 23
    label "zwolni&#263;"
  ]
  node [
    id 24
    label "zrobi&#263;"
  ]
  node [
    id 25
    label "publish"
  ]
  node [
    id 26
    label "absolutno&#347;&#263;"
  ]
  node [
    id 27
    label "obecno&#347;&#263;"
  ]
  node [
    id 28
    label "uwi&#281;zi&#263;"
  ]
  node [
    id 29
    label "cecha"
  ]
  node [
    id 30
    label "freedom"
  ]
  node [
    id 31
    label "niezale&#380;no&#347;&#263;"
  ]
  node [
    id 32
    label "uwi&#281;zienie"
  ]
  node [
    id 33
    label "nieokre&#347;lono&#347;&#263;"
  ]
  node [
    id 34
    label "ch&#322;opina"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "bratek"
  ]
  node [
    id 37
    label "jegomo&#347;&#263;"
  ]
  node [
    id 38
    label "doros&#322;y"
  ]
  node [
    id 39
    label "samiec"
  ]
  node [
    id 40
    label "ojciec"
  ]
  node [
    id 41
    label "twardziel"
  ]
  node [
    id 42
    label "androlog"
  ]
  node [
    id 43
    label "pa&#324;stwo"
  ]
  node [
    id 44
    label "m&#261;&#380;"
  ]
  node [
    id 45
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 46
    label "andropauza"
  ]
  node [
    id 47
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 48
    label "usun&#261;&#263;"
  ]
  node [
    id 49
    label "authorize"
  ]
  node [
    id 50
    label "base_on_balls"
  ]
  node [
    id 51
    label "skupi&#263;"
  ]
  node [
    id 52
    label "pora_roku"
  ]
  node [
    id 53
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 54
    label "ogranicza&#263;"
  ]
  node [
    id 55
    label "uniemo&#380;liwianie"
  ]
  node [
    id 56
    label "Butyrki"
  ]
  node [
    id 57
    label "ciupa"
  ]
  node [
    id 58
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 59
    label "reedukator"
  ]
  node [
    id 60
    label "miejsce_odosobnienia"
  ]
  node [
    id 61
    label "&#321;ubianka"
  ]
  node [
    id 62
    label "pierdel"
  ]
  node [
    id 63
    label "imprisonment"
  ]
  node [
    id 64
    label "sytuacja"
  ]
  node [
    id 65
    label "strona"
  ]
  node [
    id 66
    label "przyczyna"
  ]
  node [
    id 67
    label "matuszka"
  ]
  node [
    id 68
    label "geneza"
  ]
  node [
    id 69
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 70
    label "czynnik"
  ]
  node [
    id 71
    label "poci&#261;ganie"
  ]
  node [
    id 72
    label "rezultat"
  ]
  node [
    id 73
    label "uprz&#261;&#380;"
  ]
  node [
    id 74
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 75
    label "subject"
  ]
  node [
    id 76
    label "oskar&#380;enie"
  ]
  node [
    id 77
    label "pretensja"
  ]
  node [
    id 78
    label "oskar&#380;ycielstwo"
  ]
  node [
    id 79
    label "czyn_nierz&#261;dny"
  ]
  node [
    id 80
    label "przewaga"
  ]
  node [
    id 81
    label "violation"
  ]
  node [
    id 82
    label "patologia"
  ]
  node [
    id 83
    label "molestowanie_seksualne"
  ]
  node [
    id 84
    label "zamieszanie"
  ]
  node [
    id 85
    label "drastyczny"
  ]
  node [
    id 86
    label "post&#281;pek"
  ]
  node [
    id 87
    label "agresja"
  ]
  node [
    id 88
    label "po&#347;piech"
  ]
  node [
    id 89
    label "zabicie"
  ]
  node [
    id 90
    label "przest&#281;pstwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
]
