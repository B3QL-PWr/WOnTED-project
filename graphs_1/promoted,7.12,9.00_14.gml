graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.058139534883721
  density 0.012035903712770298
  graphCliqueNumber 3
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "gra"
    origin "text"
  ]
  node [
    id 2
    label "zr&#281;czno&#347;ciowy"
    origin "text"
  ]
  node [
    id 3
    label "zagadka"
    origin "text"
  ]
  node [
    id 4
    label "logiczny"
    origin "text"
  ]
  node [
    id 5
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 6
    label "g&#322;&#243;wny"
    origin "text"
  ]
  node [
    id 7
    label "bohater"
    origin "text"
  ]
  node [
    id 8
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "akcja"
    origin "text"
  ]
  node [
    id 10
    label "dzieje"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 13
    label "rozmawia&#263;"
    origin "text"
  ]
  node [
    id 14
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 15
    label "pod&#347;wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "si&#281;ga&#263;"
  ]
  node [
    id 17
    label "trwa&#263;"
  ]
  node [
    id 18
    label "obecno&#347;&#263;"
  ]
  node [
    id 19
    label "stan"
  ]
  node [
    id 20
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 21
    label "stand"
  ]
  node [
    id 22
    label "mie&#263;_miejsce"
  ]
  node [
    id 23
    label "uczestniczy&#263;"
  ]
  node [
    id 24
    label "chodzi&#263;"
  ]
  node [
    id 25
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 26
    label "equal"
  ]
  node [
    id 27
    label "zabawa"
  ]
  node [
    id 28
    label "rywalizacja"
  ]
  node [
    id 29
    label "czynno&#347;&#263;"
  ]
  node [
    id 30
    label "Pok&#233;mon"
  ]
  node [
    id 31
    label "synteza"
  ]
  node [
    id 32
    label "odtworzenie"
  ]
  node [
    id 33
    label "komplet"
  ]
  node [
    id 34
    label "rekwizyt_do_gry"
  ]
  node [
    id 35
    label "odg&#322;os"
  ]
  node [
    id 36
    label "rozgrywka"
  ]
  node [
    id 37
    label "post&#281;powanie"
  ]
  node [
    id 38
    label "wydarzenie"
  ]
  node [
    id 39
    label "apparent_motion"
  ]
  node [
    id 40
    label "game"
  ]
  node [
    id 41
    label "zmienno&#347;&#263;"
  ]
  node [
    id 42
    label "zasada"
  ]
  node [
    id 43
    label "play"
  ]
  node [
    id 44
    label "contest"
  ]
  node [
    id 45
    label "zbijany"
  ]
  node [
    id 46
    label "po&#322;udnica"
  ]
  node [
    id 47
    label "&#322;amig&#322;&#243;wka"
  ]
  node [
    id 48
    label "rzecz"
  ]
  node [
    id 49
    label "mystery"
  ]
  node [
    id 50
    label "enigmat"
  ]
  node [
    id 51
    label "taj&#324;"
  ]
  node [
    id 52
    label "uporz&#261;dkowany"
  ]
  node [
    id 53
    label "rozumowy"
  ]
  node [
    id 54
    label "sensowny"
  ]
  node [
    id 55
    label "rozs&#261;dny"
  ]
  node [
    id 56
    label "umotywowany"
  ]
  node [
    id 57
    label "logicznie"
  ]
  node [
    id 58
    label "kres_&#380;ycia"
  ]
  node [
    id 59
    label "defenestracja"
  ]
  node [
    id 60
    label "kres"
  ]
  node [
    id 61
    label "agonia"
  ]
  node [
    id 62
    label "&#380;ycie"
  ]
  node [
    id 63
    label "szeol"
  ]
  node [
    id 64
    label "mogi&#322;a"
  ]
  node [
    id 65
    label "pogrzeb"
  ]
  node [
    id 66
    label "istota_nadprzyrodzona"
  ]
  node [
    id 67
    label "pogrzebanie"
  ]
  node [
    id 68
    label "&#380;a&#322;oba"
  ]
  node [
    id 69
    label "zabicie"
  ]
  node [
    id 70
    label "upadek"
  ]
  node [
    id 71
    label "najwa&#380;niejszy"
  ]
  node [
    id 72
    label "g&#322;&#243;wnie"
  ]
  node [
    id 73
    label "bohaterski"
  ]
  node [
    id 74
    label "cz&#322;owiek"
  ]
  node [
    id 75
    label "Zgredek"
  ]
  node [
    id 76
    label "Herkules"
  ]
  node [
    id 77
    label "Casanova"
  ]
  node [
    id 78
    label "Borewicz"
  ]
  node [
    id 79
    label "Don_Juan"
  ]
  node [
    id 80
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 81
    label "Winnetou"
  ]
  node [
    id 82
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 83
    label "Messi"
  ]
  node [
    id 84
    label "Herkules_Poirot"
  ]
  node [
    id 85
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 86
    label "Szwejk"
  ]
  node [
    id 87
    label "Sherlock_Holmes"
  ]
  node [
    id 88
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 89
    label "Hamlet"
  ]
  node [
    id 90
    label "Asterix"
  ]
  node [
    id 91
    label "Quasimodo"
  ]
  node [
    id 92
    label "Don_Kiszot"
  ]
  node [
    id 93
    label "Wallenrod"
  ]
  node [
    id 94
    label "uczestnik"
  ]
  node [
    id 95
    label "&#347;mia&#322;ek"
  ]
  node [
    id 96
    label "Harry_Potter"
  ]
  node [
    id 97
    label "podmiot"
  ]
  node [
    id 98
    label "Achilles"
  ]
  node [
    id 99
    label "Werter"
  ]
  node [
    id 100
    label "Mario"
  ]
  node [
    id 101
    label "posta&#263;"
  ]
  node [
    id 102
    label "du&#380;y"
  ]
  node [
    id 103
    label "jedyny"
  ]
  node [
    id 104
    label "kompletny"
  ]
  node [
    id 105
    label "zdr&#243;w"
  ]
  node [
    id 106
    label "&#380;ywy"
  ]
  node [
    id 107
    label "ca&#322;o"
  ]
  node [
    id 108
    label "pe&#322;ny"
  ]
  node [
    id 109
    label "calu&#347;ko"
  ]
  node [
    id 110
    label "podobny"
  ]
  node [
    id 111
    label "zagrywka"
  ]
  node [
    id 112
    label "czyn"
  ]
  node [
    id 113
    label "wysoko&#347;&#263;"
  ]
  node [
    id 114
    label "stock"
  ]
  node [
    id 115
    label "w&#281;ze&#322;"
  ]
  node [
    id 116
    label "instrument_strunowy"
  ]
  node [
    id 117
    label "dywidenda"
  ]
  node [
    id 118
    label "przebieg"
  ]
  node [
    id 119
    label "udzia&#322;"
  ]
  node [
    id 120
    label "occupation"
  ]
  node [
    id 121
    label "jazda"
  ]
  node [
    id 122
    label "commotion"
  ]
  node [
    id 123
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 124
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 125
    label "operacja"
  ]
  node [
    id 126
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 127
    label "epoka"
  ]
  node [
    id 128
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 129
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 130
    label "ucho"
  ]
  node [
    id 131
    label "makrocefalia"
  ]
  node [
    id 132
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 133
    label "m&#243;zg"
  ]
  node [
    id 134
    label "kierownictwo"
  ]
  node [
    id 135
    label "czaszka"
  ]
  node [
    id 136
    label "dekiel"
  ]
  node [
    id 137
    label "umys&#322;"
  ]
  node [
    id 138
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 139
    label "&#347;ci&#281;cie"
  ]
  node [
    id 140
    label "sztuka"
  ]
  node [
    id 141
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 142
    label "g&#243;ra"
  ]
  node [
    id 143
    label "byd&#322;o"
  ]
  node [
    id 144
    label "alkohol"
  ]
  node [
    id 145
    label "wiedza"
  ]
  node [
    id 146
    label "ro&#347;lina"
  ]
  node [
    id 147
    label "&#347;ci&#281;gno"
  ]
  node [
    id 148
    label "pryncypa&#322;"
  ]
  node [
    id 149
    label "fryzura"
  ]
  node [
    id 150
    label "noosfera"
  ]
  node [
    id 151
    label "kierowa&#263;"
  ]
  node [
    id 152
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 153
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 154
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 155
    label "cecha"
  ]
  node [
    id 156
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 157
    label "zdolno&#347;&#263;"
  ]
  node [
    id 158
    label "kszta&#322;t"
  ]
  node [
    id 159
    label "cz&#322;onek"
  ]
  node [
    id 160
    label "cia&#322;o"
  ]
  node [
    id 161
    label "obiekt"
  ]
  node [
    id 162
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 163
    label "talk"
  ]
  node [
    id 164
    label "gaworzy&#263;"
  ]
  node [
    id 165
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 166
    label "bli&#378;ni"
  ]
  node [
    id 167
    label "odpowiedni"
  ]
  node [
    id 168
    label "swojak"
  ]
  node [
    id 169
    label "samodzielny"
  ]
  node [
    id 170
    label "_id"
  ]
  node [
    id 171
    label "psychika"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
]
