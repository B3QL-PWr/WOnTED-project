graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.96
  density 0.04
  graphCliqueNumber 2
  node [
    id 0
    label "zako&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "badanie"
    origin "text"
  ]
  node [
    id 2
    label "archeologiczny"
    origin "text"
  ]
  node [
    id 3
    label "cmentarzysko"
    origin "text"
  ]
  node [
    id 4
    label "woj"
    origin "text"
  ]
  node [
    id 5
    label "powiat"
    origin "text"
  ]
  node [
    id 6
    label "gorzowski"
    origin "text"
  ]
  node [
    id 7
    label "communicate"
  ]
  node [
    id 8
    label "cause"
  ]
  node [
    id 9
    label "zrezygnowa&#263;"
  ]
  node [
    id 10
    label "wytworzy&#263;"
  ]
  node [
    id 11
    label "przesta&#263;"
  ]
  node [
    id 12
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 13
    label "dispose"
  ]
  node [
    id 14
    label "zrobi&#263;"
  ]
  node [
    id 15
    label "usi&#322;owanie"
  ]
  node [
    id 16
    label "examination"
  ]
  node [
    id 17
    label "investigation"
  ]
  node [
    id 18
    label "ustalenie"
  ]
  node [
    id 19
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 20
    label "ustalanie"
  ]
  node [
    id 21
    label "bia&#322;a_niedziela"
  ]
  node [
    id 22
    label "analysis"
  ]
  node [
    id 23
    label "rozpatrywanie"
  ]
  node [
    id 24
    label "wziernikowanie"
  ]
  node [
    id 25
    label "obserwowanie"
  ]
  node [
    id 26
    label "omawianie"
  ]
  node [
    id 27
    label "sprawdzanie"
  ]
  node [
    id 28
    label "udowadnianie"
  ]
  node [
    id 29
    label "diagnostyka"
  ]
  node [
    id 30
    label "czynno&#347;&#263;"
  ]
  node [
    id 31
    label "macanie"
  ]
  node [
    id 32
    label "rektalny"
  ]
  node [
    id 33
    label "penetrowanie"
  ]
  node [
    id 34
    label "krytykowanie"
  ]
  node [
    id 35
    label "kontrola"
  ]
  node [
    id 36
    label "dociekanie"
  ]
  node [
    id 37
    label "zrecenzowanie"
  ]
  node [
    id 38
    label "praca"
  ]
  node [
    id 39
    label "rezultat"
  ]
  node [
    id 40
    label "archeologicznie"
  ]
  node [
    id 41
    label "naukowy"
  ]
  node [
    id 42
    label "charakterystyczny"
  ]
  node [
    id 43
    label "cmentarz"
  ]
  node [
    id 44
    label "&#380;o&#322;nierz"
  ]
  node [
    id 45
    label "wite&#378;"
  ]
  node [
    id 46
    label "wojownik"
  ]
  node [
    id 47
    label "gmina"
  ]
  node [
    id 48
    label "jednostka_administracyjna"
  ]
  node [
    id 49
    label "wojew&#243;dztwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
]
