graph [
  maxDegree 7
  minDegree 1
  meanDegree 2.1818181818181817
  density 0.1038961038961039
  graphCliqueNumber 3
  node [
    id 0
    label "chili"
    origin "text"
  ]
  node [
    id 1
    label "chilihead"
    origin "text"
  ]
  node [
    id 2
    label "papryczka"
    origin "text"
  ]
  node [
    id 3
    label "ostrezarcie"
    origin "text"
  ]
  node [
    id 4
    label "gotujzwykopem"
    origin "text"
  ]
  node [
    id 5
    label "foodporn"
    origin "text"
  ]
  node [
    id 6
    label "rozdajo"
    origin "text"
  ]
  node [
    id 7
    label "miko&#322;ajkowy"
    origin "text"
  ]
  node [
    id 8
    label "potr&#243;jny"
    origin "text"
  ]
  node [
    id 9
    label "papryka"
  ]
  node [
    id 10
    label "przyprawa"
  ]
  node [
    id 11
    label "sos"
  ]
  node [
    id 12
    label "trzykrotnie"
  ]
  node [
    id 13
    label "kilkukrotny"
  ]
  node [
    id 14
    label "parokrotny"
  ]
  node [
    id 15
    label "potr&#243;jnie"
  ]
  node [
    id 16
    label "ROZDAJO"
  ]
  node [
    id 17
    label "Jays"
  ]
  node [
    id 18
    label "Peach"
  ]
  node [
    id 19
    label "Ghost"
  ]
  node [
    id 20
    label "Bhutlah"
  ]
  node [
    id 21
    label "Chocolate"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
]
