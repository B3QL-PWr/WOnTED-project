graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.0952380952380953
  density 0.006672732787382469
  graphCliqueNumber 3
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wyzwanie"
    origin "text"
  ]
  node [
    id 2
    label "zadanie"
    origin "text"
  ]
  node [
    id 3
    label "nagroda"
    origin "text"
  ]
  node [
    id 4
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 5
    label "fundacja"
    origin "text"
  ]
  node [
    id 6
    label "nowa"
    origin "text"
  ]
  node [
    id 7
    label "media"
    origin "text"
  ]
  node [
    id 8
    label "og&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "nowy"
    origin "text"
  ]
  node [
    id 10
    label "konkurs"
    origin "text"
  ]
  node [
    id 11
    label "dla"
    origin "text"
  ]
  node [
    id 12
    label "gimnazjum"
    origin "text"
  ]
  node [
    id 13
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 14
    label "ponadgimnazjalnych"
    origin "text"
  ]
  node [
    id 15
    label "pire"
    origin "text"
  ]
  node [
    id 16
    label "informacyjny"
    origin "text"
  ]
  node [
    id 17
    label "unia"
    origin "text"
  ]
  node [
    id 18
    label "europejski"
    origin "text"
  ]
  node [
    id 19
    label "pozna&#263;"
    origin "text"
  ]
  node [
    id 20
    label "cztery"
    origin "text"
  ]
  node [
    id 21
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 22
    label "wydanie"
    origin "text"
  ]
  node [
    id 23
    label "qmama"
    origin "text"
  ]
  node [
    id 24
    label "wygranie"
    origin "text"
  ]
  node [
    id 25
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 26
    label "inny"
    origin "text"
  ]
  node [
    id 27
    label "laptop"
    origin "text"
  ]
  node [
    id 28
    label "rzutnik"
    origin "text"
  ]
  node [
    id 29
    label "multimedialny"
    origin "text"
  ]
  node [
    id 30
    label "warto"
    origin "text"
  ]
  node [
    id 31
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 32
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 33
    label "zajrze&#263;"
    origin "text"
  ]
  node [
    id 34
    label "strona"
    origin "text"
  ]
  node [
    id 35
    label "zg&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 36
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 37
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 38
    label "projekt"
    origin "text"
  ]
  node [
    id 39
    label "si&#281;ga&#263;"
  ]
  node [
    id 40
    label "trwa&#263;"
  ]
  node [
    id 41
    label "obecno&#347;&#263;"
  ]
  node [
    id 42
    label "stan"
  ]
  node [
    id 43
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 44
    label "stand"
  ]
  node [
    id 45
    label "mie&#263;_miejsce"
  ]
  node [
    id 46
    label "uczestniczy&#263;"
  ]
  node [
    id 47
    label "chodzi&#263;"
  ]
  node [
    id 48
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 49
    label "equal"
  ]
  node [
    id 50
    label "challenge"
  ]
  node [
    id 51
    label "wypowied&#378;"
  ]
  node [
    id 52
    label "zaproponowanie"
  ]
  node [
    id 53
    label "rzuci&#263;"
  ]
  node [
    id 54
    label "obra&#380;enie"
  ]
  node [
    id 55
    label "rzuca&#263;"
  ]
  node [
    id 56
    label "gauntlet"
  ]
  node [
    id 57
    label "pr&#243;ba"
  ]
  node [
    id 58
    label "pojedynek"
  ]
  node [
    id 59
    label "pojedynkowanie_si&#281;"
  ]
  node [
    id 60
    label "rzucenie"
  ]
  node [
    id 61
    label "rzucanie"
  ]
  node [
    id 62
    label "yield"
  ]
  node [
    id 63
    label "czynno&#347;&#263;"
  ]
  node [
    id 64
    label "problem"
  ]
  node [
    id 65
    label "przepisanie"
  ]
  node [
    id 66
    label "przepisa&#263;"
  ]
  node [
    id 67
    label "za&#322;o&#380;enie"
  ]
  node [
    id 68
    label "work"
  ]
  node [
    id 69
    label "nakarmienie"
  ]
  node [
    id 70
    label "duty"
  ]
  node [
    id 71
    label "zbi&#243;r"
  ]
  node [
    id 72
    label "powierzanie"
  ]
  node [
    id 73
    label "zaszkodzenie"
  ]
  node [
    id 74
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 75
    label "zaj&#281;cie"
  ]
  node [
    id 76
    label "zobowi&#261;zanie"
  ]
  node [
    id 77
    label "return"
  ]
  node [
    id 78
    label "konsekwencja"
  ]
  node [
    id 79
    label "oskar"
  ]
  node [
    id 80
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 81
    label "foundation"
  ]
  node [
    id 82
    label "instytucja"
  ]
  node [
    id 83
    label "dar"
  ]
  node [
    id 84
    label "darowizna"
  ]
  node [
    id 85
    label "pocz&#261;tek"
  ]
  node [
    id 86
    label "gwiazda"
  ]
  node [
    id 87
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 88
    label "przekazior"
  ]
  node [
    id 89
    label "mass-media"
  ]
  node [
    id 90
    label "uzbrajanie"
  ]
  node [
    id 91
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 92
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 93
    label "medium"
  ]
  node [
    id 94
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 95
    label "communicate"
  ]
  node [
    id 96
    label "opublikowa&#263;"
  ]
  node [
    id 97
    label "obwo&#322;a&#263;"
  ]
  node [
    id 98
    label "poda&#263;"
  ]
  node [
    id 99
    label "publish"
  ]
  node [
    id 100
    label "declare"
  ]
  node [
    id 101
    label "cz&#322;owiek"
  ]
  node [
    id 102
    label "nowotny"
  ]
  node [
    id 103
    label "drugi"
  ]
  node [
    id 104
    label "kolejny"
  ]
  node [
    id 105
    label "bie&#380;&#261;cy"
  ]
  node [
    id 106
    label "nowo"
  ]
  node [
    id 107
    label "narybek"
  ]
  node [
    id 108
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 109
    label "obcy"
  ]
  node [
    id 110
    label "eliminacje"
  ]
  node [
    id 111
    label "Interwizja"
  ]
  node [
    id 112
    label "emulation"
  ]
  node [
    id 113
    label "impreza"
  ]
  node [
    id 114
    label "casting"
  ]
  node [
    id 115
    label "Eurowizja"
  ]
  node [
    id 116
    label "nab&#243;r"
  ]
  node [
    id 117
    label "miejsce"
  ]
  node [
    id 118
    label "szko&#322;a_ponadpodstawowa"
  ]
  node [
    id 119
    label "Mickiewicz"
  ]
  node [
    id 120
    label "czas"
  ]
  node [
    id 121
    label "szkolenie"
  ]
  node [
    id 122
    label "lesson"
  ]
  node [
    id 123
    label "grupa"
  ]
  node [
    id 124
    label "praktyka"
  ]
  node [
    id 125
    label "metoda"
  ]
  node [
    id 126
    label "niepokalanki"
  ]
  node [
    id 127
    label "kara"
  ]
  node [
    id 128
    label "zda&#263;"
  ]
  node [
    id 129
    label "form"
  ]
  node [
    id 130
    label "kwalifikacje"
  ]
  node [
    id 131
    label "system"
  ]
  node [
    id 132
    label "sztuba"
  ]
  node [
    id 133
    label "wiedza"
  ]
  node [
    id 134
    label "stopek"
  ]
  node [
    id 135
    label "school"
  ]
  node [
    id 136
    label "absolwent"
  ]
  node [
    id 137
    label "urszulanki"
  ]
  node [
    id 138
    label "gabinet"
  ]
  node [
    id 139
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 140
    label "ideologia"
  ]
  node [
    id 141
    label "lekcja"
  ]
  node [
    id 142
    label "muzyka"
  ]
  node [
    id 143
    label "podr&#281;cznik"
  ]
  node [
    id 144
    label "zdanie"
  ]
  node [
    id 145
    label "siedziba"
  ]
  node [
    id 146
    label "sekretariat"
  ]
  node [
    id 147
    label "nauka"
  ]
  node [
    id 148
    label "do&#347;wiadczenie"
  ]
  node [
    id 149
    label "tablica"
  ]
  node [
    id 150
    label "teren_szko&#322;y"
  ]
  node [
    id 151
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 152
    label "skolaryzacja"
  ]
  node [
    id 153
    label "&#322;awa_szkolna"
  ]
  node [
    id 154
    label "klasa"
  ]
  node [
    id 155
    label "informacyjnie"
  ]
  node [
    id 156
    label "uk&#322;ad"
  ]
  node [
    id 157
    label "partia"
  ]
  node [
    id 158
    label "organizacja"
  ]
  node [
    id 159
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 160
    label "Unia_Europejska"
  ]
  node [
    id 161
    label "combination"
  ]
  node [
    id 162
    label "union"
  ]
  node [
    id 163
    label "Unia"
  ]
  node [
    id 164
    label "European"
  ]
  node [
    id 165
    label "po_europejsku"
  ]
  node [
    id 166
    label "charakterystyczny"
  ]
  node [
    id 167
    label "europejsko"
  ]
  node [
    id 168
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 169
    label "typowy"
  ]
  node [
    id 170
    label "przyswoi&#263;"
  ]
  node [
    id 171
    label "feel"
  ]
  node [
    id 172
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 173
    label "teach"
  ]
  node [
    id 174
    label "zrozumie&#263;"
  ]
  node [
    id 175
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 176
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 177
    label "topographic_point"
  ]
  node [
    id 178
    label "experience"
  ]
  node [
    id 179
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 180
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 181
    label "visualize"
  ]
  node [
    id 182
    label "zorganizowa&#263;"
  ]
  node [
    id 183
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 184
    label "przerobi&#263;"
  ]
  node [
    id 185
    label "wystylizowa&#263;"
  ]
  node [
    id 186
    label "cause"
  ]
  node [
    id 187
    label "wydali&#263;"
  ]
  node [
    id 188
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 189
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 190
    label "post&#261;pi&#263;"
  ]
  node [
    id 191
    label "appoint"
  ]
  node [
    id 192
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 193
    label "nabra&#263;"
  ]
  node [
    id 194
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 195
    label "make"
  ]
  node [
    id 196
    label "delivery"
  ]
  node [
    id 197
    label "podanie"
  ]
  node [
    id 198
    label "issue"
  ]
  node [
    id 199
    label "danie"
  ]
  node [
    id 200
    label "rendition"
  ]
  node [
    id 201
    label "egzemplarz"
  ]
  node [
    id 202
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 203
    label "impression"
  ]
  node [
    id 204
    label "odmiana"
  ]
  node [
    id 205
    label "zapach"
  ]
  node [
    id 206
    label "wytworzenie"
  ]
  node [
    id 207
    label "wprowadzenie"
  ]
  node [
    id 208
    label "zdarzenie_si&#281;"
  ]
  node [
    id 209
    label "zrobienie"
  ]
  node [
    id 210
    label "ujawnienie"
  ]
  node [
    id 211
    label "reszta"
  ]
  node [
    id 212
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 213
    label "zadenuncjowanie"
  ]
  node [
    id 214
    label "czasopismo"
  ]
  node [
    id 215
    label "urz&#261;dzenie"
  ]
  node [
    id 216
    label "d&#378;wi&#281;k"
  ]
  node [
    id 217
    label "publikacja"
  ]
  node [
    id 218
    label "zwojowanie"
  ]
  node [
    id 219
    label "beat"
  ]
  node [
    id 220
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 221
    label "zapanowanie"
  ]
  node [
    id 222
    label "zniesienie"
  ]
  node [
    id 223
    label "wygrywanie"
  ]
  node [
    id 224
    label "wr&#243;cenie_z_tarcz&#261;"
  ]
  node [
    id 225
    label "inaczej"
  ]
  node [
    id 226
    label "r&#243;&#380;ny"
  ]
  node [
    id 227
    label "inszy"
  ]
  node [
    id 228
    label "osobno"
  ]
  node [
    id 229
    label "touchpad"
  ]
  node [
    id 230
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 231
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 232
    label "Ultrabook"
  ]
  node [
    id 233
    label "projektor"
  ]
  node [
    id 234
    label "slide_projector"
  ]
  node [
    id 235
    label "multimedialnie"
  ]
  node [
    id 236
    label "przysparza&#263;"
  ]
  node [
    id 237
    label "give"
  ]
  node [
    id 238
    label "kali&#263;_si&#281;"
  ]
  node [
    id 239
    label "bonanza"
  ]
  node [
    id 240
    label "doba"
  ]
  node [
    id 241
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 242
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 243
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 244
    label "spojrze&#263;"
  ]
  node [
    id 245
    label "cheep"
  ]
  node [
    id 246
    label "odwiedzi&#263;"
  ]
  node [
    id 247
    label "pozazdro&#347;ci&#263;"
  ]
  node [
    id 248
    label "skr&#281;canie"
  ]
  node [
    id 249
    label "voice"
  ]
  node [
    id 250
    label "forma"
  ]
  node [
    id 251
    label "internet"
  ]
  node [
    id 252
    label "skr&#281;ci&#263;"
  ]
  node [
    id 253
    label "kartka"
  ]
  node [
    id 254
    label "orientowa&#263;"
  ]
  node [
    id 255
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 256
    label "powierzchnia"
  ]
  node [
    id 257
    label "plik"
  ]
  node [
    id 258
    label "bok"
  ]
  node [
    id 259
    label "pagina"
  ]
  node [
    id 260
    label "orientowanie"
  ]
  node [
    id 261
    label "fragment"
  ]
  node [
    id 262
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 263
    label "s&#261;d"
  ]
  node [
    id 264
    label "skr&#281;ca&#263;"
  ]
  node [
    id 265
    label "g&#243;ra"
  ]
  node [
    id 266
    label "serwis_internetowy"
  ]
  node [
    id 267
    label "orientacja"
  ]
  node [
    id 268
    label "linia"
  ]
  node [
    id 269
    label "skr&#281;cenie"
  ]
  node [
    id 270
    label "layout"
  ]
  node [
    id 271
    label "zorientowa&#263;"
  ]
  node [
    id 272
    label "zorientowanie"
  ]
  node [
    id 273
    label "obiekt"
  ]
  node [
    id 274
    label "podmiot"
  ]
  node [
    id 275
    label "ty&#322;"
  ]
  node [
    id 276
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 277
    label "logowanie"
  ]
  node [
    id 278
    label "adres_internetowy"
  ]
  node [
    id 279
    label "uj&#281;cie"
  ]
  node [
    id 280
    label "prz&#243;d"
  ]
  node [
    id 281
    label "posta&#263;"
  ]
  node [
    id 282
    label "report"
  ]
  node [
    id 283
    label "announce"
  ]
  node [
    id 284
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 285
    label "write"
  ]
  node [
    id 286
    label "poinformowa&#263;"
  ]
  node [
    id 287
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 288
    label "kwota"
  ]
  node [
    id 289
    label "ilo&#347;&#263;"
  ]
  node [
    id 290
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 291
    label "whole"
  ]
  node [
    id 292
    label "odm&#322;adza&#263;"
  ]
  node [
    id 293
    label "zabudowania"
  ]
  node [
    id 294
    label "odm&#322;odzenie"
  ]
  node [
    id 295
    label "zespolik"
  ]
  node [
    id 296
    label "skupienie"
  ]
  node [
    id 297
    label "schorzenie"
  ]
  node [
    id 298
    label "Depeche_Mode"
  ]
  node [
    id 299
    label "Mazowsze"
  ]
  node [
    id 300
    label "ro&#347;lina"
  ]
  node [
    id 301
    label "The_Beatles"
  ]
  node [
    id 302
    label "group"
  ]
  node [
    id 303
    label "&#346;wietliki"
  ]
  node [
    id 304
    label "odm&#322;adzanie"
  ]
  node [
    id 305
    label "batch"
  ]
  node [
    id 306
    label "dokument"
  ]
  node [
    id 307
    label "device"
  ]
  node [
    id 308
    label "program_u&#380;ytkowy"
  ]
  node [
    id 309
    label "intencja"
  ]
  node [
    id 310
    label "agreement"
  ]
  node [
    id 311
    label "pomys&#322;"
  ]
  node [
    id 312
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 313
    label "plan"
  ]
  node [
    id 314
    label "dokumentacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 205
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 22
    target 208
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 217
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 104
  ]
  edge [
    source 26
    target 225
  ]
  edge [
    source 26
    target 226
  ]
  edge [
    source 26
    target 227
  ]
  edge [
    source 26
    target 228
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 229
  ]
  edge [
    source 27
    target 230
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 232
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 233
  ]
  edge [
    source 28
    target 234
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 235
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 236
  ]
  edge [
    source 30
    target 237
  ]
  edge [
    source 30
    target 238
  ]
  edge [
    source 30
    target 239
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 32
    target 241
  ]
  edge [
    source 32
    target 242
  ]
  edge [
    source 32
    target 243
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 244
  ]
  edge [
    source 33
    target 245
  ]
  edge [
    source 33
    target 246
  ]
  edge [
    source 33
    target 247
  ]
  edge [
    source 34
    target 248
  ]
  edge [
    source 34
    target 249
  ]
  edge [
    source 34
    target 250
  ]
  edge [
    source 34
    target 251
  ]
  edge [
    source 34
    target 252
  ]
  edge [
    source 34
    target 253
  ]
  edge [
    source 34
    target 254
  ]
  edge [
    source 34
    target 255
  ]
  edge [
    source 34
    target 256
  ]
  edge [
    source 34
    target 257
  ]
  edge [
    source 34
    target 258
  ]
  edge [
    source 34
    target 259
  ]
  edge [
    source 34
    target 260
  ]
  edge [
    source 34
    target 261
  ]
  edge [
    source 34
    target 262
  ]
  edge [
    source 34
    target 263
  ]
  edge [
    source 34
    target 264
  ]
  edge [
    source 34
    target 265
  ]
  edge [
    source 34
    target 266
  ]
  edge [
    source 34
    target 267
  ]
  edge [
    source 34
    target 268
  ]
  edge [
    source 34
    target 269
  ]
  edge [
    source 34
    target 270
  ]
  edge [
    source 34
    target 271
  ]
  edge [
    source 34
    target 272
  ]
  edge [
    source 34
    target 273
  ]
  edge [
    source 34
    target 274
  ]
  edge [
    source 34
    target 275
  ]
  edge [
    source 34
    target 276
  ]
  edge [
    source 34
    target 277
  ]
  edge [
    source 34
    target 278
  ]
  edge [
    source 34
    target 279
  ]
  edge [
    source 34
    target 280
  ]
  edge [
    source 34
    target 281
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 282
  ]
  edge [
    source 35
    target 283
  ]
  edge [
    source 35
    target 284
  ]
  edge [
    source 35
    target 285
  ]
  edge [
    source 35
    target 286
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 41
  ]
  edge [
    source 36
    target 287
  ]
  edge [
    source 36
    target 288
  ]
  edge [
    source 36
    target 289
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 290
  ]
  edge [
    source 37
    target 291
  ]
  edge [
    source 37
    target 292
  ]
  edge [
    source 37
    target 293
  ]
  edge [
    source 37
    target 294
  ]
  edge [
    source 37
    target 295
  ]
  edge [
    source 37
    target 296
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 37
    target 123
  ]
  edge [
    source 37
    target 298
  ]
  edge [
    source 37
    target 299
  ]
  edge [
    source 37
    target 300
  ]
  edge [
    source 37
    target 71
  ]
  edge [
    source 37
    target 301
  ]
  edge [
    source 37
    target 302
  ]
  edge [
    source 37
    target 303
  ]
  edge [
    source 37
    target 304
  ]
  edge [
    source 37
    target 305
  ]
  edge [
    source 38
    target 306
  ]
  edge [
    source 38
    target 307
  ]
  edge [
    source 38
    target 308
  ]
  edge [
    source 38
    target 309
  ]
  edge [
    source 38
    target 310
  ]
  edge [
    source 38
    target 311
  ]
  edge [
    source 38
    target 312
  ]
  edge [
    source 38
    target 313
  ]
  edge [
    source 38
    target 314
  ]
]
