graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.4285714285714286
  density 0.23809523809523808
  graphCliqueNumber 2
  node [
    id 0
    label "audi"
    origin "text"
  ]
  node [
    id 1
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 2
    label "Audi"
  ]
  node [
    id 3
    label "samoch&#243;d"
  ]
  node [
    id 4
    label "50"
  ]
  node [
    id 5
    label "volkswagen"
  ]
  node [
    id 6
    label "polo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 5
    target 6
  ]
]
