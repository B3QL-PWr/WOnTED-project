graph [
  maxDegree 26
  minDegree 1
  meanDegree 2.055299539170507
  density 0.009515275644307902
  graphCliqueNumber 2
  node [
    id 0
    label "przychodzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "pomoc"
    origin "text"
  ]
  node [
    id 2
    label "logarytm"
    origin "text"
  ]
  node [
    id 3
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 4
    label "bardzo"
    origin "text"
  ]
  node [
    id 5
    label "algorytm"
    origin "text"
  ]
  node [
    id 6
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 7
    label "guzik"
    origin "text"
  ]
  node [
    id 8
    label "pilot"
    origin "text"
  ]
  node [
    id 9
    label "wcisn&#261;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pozwoli&#263;"
    origin "text"
  ]
  node [
    id 11
    label "siebie"
    origin "text"
  ]
  node [
    id 12
    label "za&#380;artowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "pr&#243;bowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 15
    label "t&#322;umaczy&#263;"
    origin "text"
  ]
  node [
    id 16
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 17
    label "nie"
    origin "text"
  ]
  node [
    id 18
    label "zdolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "matematyka"
    origin "text"
  ]
  node [
    id 20
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 21
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 22
    label "tak"
    origin "text"
  ]
  node [
    id 23
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 24
    label "pewne"
    origin "text"
  ]
  node [
    id 25
    label "do&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 26
    label "pedagogiczny"
    origin "text"
  ]
  node [
    id 27
    label "dla"
    origin "text"
  ]
  node [
    id 28
    label "jasny"
    origin "text"
  ]
  node [
    id 29
    label "u&#380;ywanie"
    origin "text"
  ]
  node [
    id 30
    label "termin"
    origin "text"
  ]
  node [
    id 31
    label "logarytmowa&#263;"
    origin "text"
  ]
  node [
    id 32
    label "dobre"
    origin "text"
  ]
  node [
    id 33
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 34
    label "przybywa&#263;"
  ]
  node [
    id 35
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 36
    label "dochodzi&#263;"
  ]
  node [
    id 37
    label "zgodzi&#263;"
  ]
  node [
    id 38
    label "pomocnik"
  ]
  node [
    id 39
    label "doch&#243;d"
  ]
  node [
    id 40
    label "property"
  ]
  node [
    id 41
    label "przedmiot"
  ]
  node [
    id 42
    label "grupa"
  ]
  node [
    id 43
    label "telefon_zaufania"
  ]
  node [
    id 44
    label "darowizna"
  ]
  node [
    id 45
    label "&#347;rodek"
  ]
  node [
    id 46
    label "liga"
  ]
  node [
    id 47
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 48
    label "logarithm"
  ]
  node [
    id 49
    label "mantysa"
  ]
  node [
    id 50
    label "cecha_logarytmu"
  ]
  node [
    id 51
    label "w_chuj"
  ]
  node [
    id 52
    label "algorithm"
  ]
  node [
    id 53
    label "formu&#322;a"
  ]
  node [
    id 54
    label "zero"
  ]
  node [
    id 55
    label "pasmanteria"
  ]
  node [
    id 56
    label "r&#243;g"
  ]
  node [
    id 57
    label "knob"
  ]
  node [
    id 58
    label "guzikarz"
  ]
  node [
    id 59
    label "filobutonista"
  ]
  node [
    id 60
    label "przycisk"
  ]
  node [
    id 61
    label "filobutonistyka"
  ]
  node [
    id 62
    label "zapi&#281;cie"
  ]
  node [
    id 63
    label "lotnik"
  ]
  node [
    id 64
    label "briefing"
  ]
  node [
    id 65
    label "zwiastun"
  ]
  node [
    id 66
    label "urz&#261;dzenie"
  ]
  node [
    id 67
    label "wycieczka"
  ]
  node [
    id 68
    label "delfin"
  ]
  node [
    id 69
    label "nawigator"
  ]
  node [
    id 70
    label "przewodnik"
  ]
  node [
    id 71
    label "delfinowate"
  ]
  node [
    id 72
    label "wci&#347;ni&#281;cie"
  ]
  node [
    id 73
    label "spowodowa&#263;"
  ]
  node [
    id 74
    label "wciska&#263;"
  ]
  node [
    id 75
    label "nadusi&#263;"
  ]
  node [
    id 76
    label "tug"
  ]
  node [
    id 77
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 78
    label "przekaza&#263;"
  ]
  node [
    id 79
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 80
    label "press"
  ]
  node [
    id 81
    label "pofolgowa&#263;"
  ]
  node [
    id 82
    label "assent"
  ]
  node [
    id 83
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 84
    label "leave"
  ]
  node [
    id 85
    label "uzna&#263;"
  ]
  node [
    id 86
    label "jeer"
  ]
  node [
    id 87
    label "zakomunikowa&#263;"
  ]
  node [
    id 88
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 89
    label "joke"
  ]
  node [
    id 90
    label "wystawi&#263;"
  ]
  node [
    id 91
    label "zrobi&#263;"
  ]
  node [
    id 92
    label "przek&#322;ada&#263;"
  ]
  node [
    id 93
    label "uzasadnia&#263;"
  ]
  node [
    id 94
    label "poja&#347;nia&#263;"
  ]
  node [
    id 95
    label "elaborate"
  ]
  node [
    id 96
    label "explain"
  ]
  node [
    id 97
    label "suplikowa&#263;"
  ]
  node [
    id 98
    label "j&#281;zyk"
  ]
  node [
    id 99
    label "sprawowa&#263;"
  ]
  node [
    id 100
    label "robi&#263;"
  ]
  node [
    id 101
    label "give"
  ]
  node [
    id 102
    label "przekonywa&#263;"
  ]
  node [
    id 103
    label "u&#322;atwia&#263;"
  ]
  node [
    id 104
    label "broni&#263;"
  ]
  node [
    id 105
    label "interpretowa&#263;"
  ]
  node [
    id 106
    label "przedstawia&#263;"
  ]
  node [
    id 107
    label "cz&#322;owiek"
  ]
  node [
    id 108
    label "znaczenie"
  ]
  node [
    id 109
    label "go&#347;&#263;"
  ]
  node [
    id 110
    label "osoba"
  ]
  node [
    id 111
    label "posta&#263;"
  ]
  node [
    id 112
    label "sprzeciw"
  ]
  node [
    id 113
    label "ability"
  ]
  node [
    id 114
    label "potencja&#322;"
  ]
  node [
    id 115
    label "cecha"
  ]
  node [
    id 116
    label "obliczeniowo"
  ]
  node [
    id 117
    label "posiada&#263;"
  ]
  node [
    id 118
    label "zapomnie&#263;"
  ]
  node [
    id 119
    label "zapomnienie"
  ]
  node [
    id 120
    label "zapominanie"
  ]
  node [
    id 121
    label "zapomina&#263;"
  ]
  node [
    id 122
    label "infimum"
  ]
  node [
    id 123
    label "matematyka_czysta"
  ]
  node [
    id 124
    label "fizyka_matematyczna"
  ]
  node [
    id 125
    label "matma"
  ]
  node [
    id 126
    label "rachunki"
  ]
  node [
    id 127
    label "kryptologia"
  ]
  node [
    id 128
    label "supremum"
  ]
  node [
    id 129
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 130
    label "rachunek_operatorowy"
  ]
  node [
    id 131
    label "funkcja"
  ]
  node [
    id 132
    label "rzut"
  ]
  node [
    id 133
    label "forsing"
  ]
  node [
    id 134
    label "teoria_graf&#243;w"
  ]
  node [
    id 135
    label "logicyzm"
  ]
  node [
    id 136
    label "logika"
  ]
  node [
    id 137
    label "topologia_algebraiczna"
  ]
  node [
    id 138
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 139
    label "matematyka_stosowana"
  ]
  node [
    id 140
    label "kierunek"
  ]
  node [
    id 141
    label "modelowanie_matematyczne"
  ]
  node [
    id 142
    label "teoria_katastrof"
  ]
  node [
    id 143
    label "jednostka"
  ]
  node [
    id 144
    label "mo&#380;liwie"
  ]
  node [
    id 145
    label "nieznaczny"
  ]
  node [
    id 146
    label "kr&#243;tko"
  ]
  node [
    id 147
    label "nieistotnie"
  ]
  node [
    id 148
    label "nieliczny"
  ]
  node [
    id 149
    label "mikroskopijnie"
  ]
  node [
    id 150
    label "pomiernie"
  ]
  node [
    id 151
    label "ma&#322;y"
  ]
  node [
    id 152
    label "du&#380;y"
  ]
  node [
    id 153
    label "cz&#281;sto"
  ]
  node [
    id 154
    label "mocno"
  ]
  node [
    id 155
    label "wiela"
  ]
  node [
    id 156
    label "proszek"
  ]
  node [
    id 157
    label "zbadanie"
  ]
  node [
    id 158
    label "skill"
  ]
  node [
    id 159
    label "wy&#347;wiadczenie"
  ]
  node [
    id 160
    label "znawstwo"
  ]
  node [
    id 161
    label "wiedza"
  ]
  node [
    id 162
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 163
    label "poczucie"
  ]
  node [
    id 164
    label "spotkanie"
  ]
  node [
    id 165
    label "do&#347;wiadczanie"
  ]
  node [
    id 166
    label "wydarzenie"
  ]
  node [
    id 167
    label "badanie"
  ]
  node [
    id 168
    label "assay"
  ]
  node [
    id 169
    label "obserwowanie"
  ]
  node [
    id 170
    label "checkup"
  ]
  node [
    id 171
    label "potraktowanie"
  ]
  node [
    id 172
    label "szko&#322;a"
  ]
  node [
    id 173
    label "eksperiencja"
  ]
  node [
    id 174
    label "dydaktycznie"
  ]
  node [
    id 175
    label "naukowy"
  ]
  node [
    id 176
    label "szczery"
  ]
  node [
    id 177
    label "o&#347;wietlenie"
  ]
  node [
    id 178
    label "klarowny"
  ]
  node [
    id 179
    label "przytomny"
  ]
  node [
    id 180
    label "jednoznaczny"
  ]
  node [
    id 181
    label "pogodny"
  ]
  node [
    id 182
    label "o&#347;wietlanie"
  ]
  node [
    id 183
    label "bia&#322;y"
  ]
  node [
    id 184
    label "niezm&#261;cony"
  ]
  node [
    id 185
    label "zrozumia&#322;y"
  ]
  node [
    id 186
    label "dobry"
  ]
  node [
    id 187
    label "jasno"
  ]
  node [
    id 188
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 189
    label "use"
  ]
  node [
    id 190
    label "robienie"
  ]
  node [
    id 191
    label "czynno&#347;&#263;"
  ]
  node [
    id 192
    label "exercise"
  ]
  node [
    id 193
    label "zniszczenie"
  ]
  node [
    id 194
    label "stosowanie"
  ]
  node [
    id 195
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 196
    label "przejaskrawianie"
  ]
  node [
    id 197
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 198
    label "zu&#380;ywanie"
  ]
  node [
    id 199
    label "u&#380;yteczny"
  ]
  node [
    id 200
    label "relish"
  ]
  node [
    id 201
    label "zaznawanie"
  ]
  node [
    id 202
    label "przypadni&#281;cie"
  ]
  node [
    id 203
    label "czas"
  ]
  node [
    id 204
    label "chronogram"
  ]
  node [
    id 205
    label "nazewnictwo"
  ]
  node [
    id 206
    label "ekspiracja"
  ]
  node [
    id 207
    label "nazwa"
  ]
  node [
    id 208
    label "przypa&#347;&#263;"
  ]
  node [
    id 209
    label "praktyka"
  ]
  node [
    id 210
    label "term"
  ]
  node [
    id 211
    label "system"
  ]
  node [
    id 212
    label "wytw&#243;r"
  ]
  node [
    id 213
    label "idea"
  ]
  node [
    id 214
    label "ukra&#347;&#263;"
  ]
  node [
    id 215
    label "ukradzenie"
  ]
  node [
    id 216
    label "pocz&#261;tki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 41
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 19
    target 135
  ]
  edge [
    source 19
    target 136
  ]
  edge [
    source 19
    target 137
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 156
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 157
  ]
  edge [
    source 25
    target 158
  ]
  edge [
    source 25
    target 159
  ]
  edge [
    source 25
    target 160
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 162
  ]
  edge [
    source 25
    target 163
  ]
  edge [
    source 25
    target 164
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 167
  ]
  edge [
    source 25
    target 168
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 25
    target 172
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 174
  ]
  edge [
    source 26
    target 175
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 176
  ]
  edge [
    source 28
    target 177
  ]
  edge [
    source 28
    target 178
  ]
  edge [
    source 28
    target 179
  ]
  edge [
    source 28
    target 180
  ]
  edge [
    source 28
    target 181
  ]
  edge [
    source 28
    target 182
  ]
  edge [
    source 28
    target 183
  ]
  edge [
    source 28
    target 184
  ]
  edge [
    source 28
    target 185
  ]
  edge [
    source 28
    target 186
  ]
  edge [
    source 28
    target 187
  ]
  edge [
    source 28
    target 188
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 189
  ]
  edge [
    source 29
    target 190
  ]
  edge [
    source 29
    target 191
  ]
  edge [
    source 29
    target 192
  ]
  edge [
    source 29
    target 193
  ]
  edge [
    source 29
    target 194
  ]
  edge [
    source 29
    target 195
  ]
  edge [
    source 29
    target 196
  ]
  edge [
    source 29
    target 197
  ]
  edge [
    source 29
    target 198
  ]
  edge [
    source 29
    target 199
  ]
  edge [
    source 29
    target 200
  ]
  edge [
    source 29
    target 201
  ]
  edge [
    source 30
    target 202
  ]
  edge [
    source 30
    target 203
  ]
  edge [
    source 30
    target 204
  ]
  edge [
    source 30
    target 205
  ]
  edge [
    source 30
    target 206
  ]
  edge [
    source 30
    target 207
  ]
  edge [
    source 30
    target 208
  ]
  edge [
    source 30
    target 209
  ]
  edge [
    source 30
    target 210
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 211
  ]
  edge [
    source 33
    target 212
  ]
  edge [
    source 33
    target 213
  ]
  edge [
    source 33
    target 214
  ]
  edge [
    source 33
    target 215
  ]
  edge [
    source 33
    target 216
  ]
]
