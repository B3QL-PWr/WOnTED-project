graph [
  maxDegree 17
  minDegree 1
  meanDegree 2.027027027027027
  density 0.027767493520918177
  graphCliqueNumber 3
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 2
    label "raz"
    origin "text"
  ]
  node [
    id 3
    label "sprawdzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 5
    label "element"
    origin "text"
  ]
  node [
    id 6
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 7
    label "wyko&#324;czenie"
    origin "text"
  ]
  node [
    id 8
    label "sko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "co&#347;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;ga&#263;"
  ]
  node [
    id 11
    label "zna&#263;"
  ]
  node [
    id 12
    label "troska&#263;_si&#281;"
  ]
  node [
    id 13
    label "zachowywa&#263;"
  ]
  node [
    id 14
    label "chowa&#263;"
  ]
  node [
    id 15
    label "think"
  ]
  node [
    id 16
    label "pilnowa&#263;"
  ]
  node [
    id 17
    label "robi&#263;"
  ]
  node [
    id 18
    label "recall"
  ]
  node [
    id 19
    label "echo"
  ]
  node [
    id 20
    label "take_care"
  ]
  node [
    id 21
    label "chwila"
  ]
  node [
    id 22
    label "uderzenie"
  ]
  node [
    id 23
    label "cios"
  ]
  node [
    id 24
    label "time"
  ]
  node [
    id 25
    label "examine"
  ]
  node [
    id 26
    label "zrobi&#263;"
  ]
  node [
    id 27
    label "jaki&#347;"
  ]
  node [
    id 28
    label "szkodnik"
  ]
  node [
    id 29
    label "&#347;rodowisko"
  ]
  node [
    id 30
    label "component"
  ]
  node [
    id 31
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 32
    label "r&#243;&#380;niczka"
  ]
  node [
    id 33
    label "przedmiot"
  ]
  node [
    id 34
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 35
    label "gangsterski"
  ]
  node [
    id 36
    label "szambo"
  ]
  node [
    id 37
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 38
    label "materia"
  ]
  node [
    id 39
    label "aspo&#322;eczny"
  ]
  node [
    id 40
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 41
    label "poj&#281;cie"
  ]
  node [
    id 42
    label "underworld"
  ]
  node [
    id 43
    label "stanie"
  ]
  node [
    id 44
    label "przebywanie"
  ]
  node [
    id 45
    label "panowanie"
  ]
  node [
    id 46
    label "zajmowanie"
  ]
  node [
    id 47
    label "pomieszkanie"
  ]
  node [
    id 48
    label "adjustment"
  ]
  node [
    id 49
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 50
    label "lokal"
  ]
  node [
    id 51
    label "kwadrat"
  ]
  node [
    id 52
    label "animation"
  ]
  node [
    id 53
    label "dom"
  ]
  node [
    id 54
    label "zrobienie"
  ]
  node [
    id 55
    label "murder"
  ]
  node [
    id 56
    label "zm&#281;czenie"
  ]
  node [
    id 57
    label "str&#243;j"
  ]
  node [
    id 58
    label "zu&#380;ycie"
  ]
  node [
    id 59
    label "znu&#380;enie"
  ]
  node [
    id 60
    label "zniszczenie"
  ]
  node [
    id 61
    label "pomordowanie"
  ]
  node [
    id 62
    label "ukszta&#322;towanie"
  ]
  node [
    id 63
    label "wymordowanie"
  ]
  node [
    id 64
    label "os&#322;abienie"
  ]
  node [
    id 65
    label "zabicie"
  ]
  node [
    id 66
    label "skonany"
  ]
  node [
    id 67
    label "communicate"
  ]
  node [
    id 68
    label "zako&#324;czy&#263;"
  ]
  node [
    id 69
    label "przesta&#263;"
  ]
  node [
    id 70
    label "end"
  ]
  node [
    id 71
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 72
    label "thing"
  ]
  node [
    id 73
    label "cosik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
]
