graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.88
  density 0.03836734693877551
  graphCliqueNumber 3
  node [
    id 0
    label "nauka"
    origin "text"
  ]
  node [
    id 1
    label "czytanie"
    origin "text"
  ]
  node [
    id 2
    label "nauki_o_Ziemi"
  ]
  node [
    id 3
    label "teoria_naukowa"
  ]
  node [
    id 4
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 5
    label "nauki_o_poznaniu"
  ]
  node [
    id 6
    label "nomotetyczny"
  ]
  node [
    id 7
    label "metodologia"
  ]
  node [
    id 8
    label "przem&#243;wienie"
  ]
  node [
    id 9
    label "wiedza"
  ]
  node [
    id 10
    label "kultura_duchowa"
  ]
  node [
    id 11
    label "nauki_penalne"
  ]
  node [
    id 12
    label "systematyka"
  ]
  node [
    id 13
    label "inwentyka"
  ]
  node [
    id 14
    label "dziedzina"
  ]
  node [
    id 15
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 16
    label "miasteczko_rowerowe"
  ]
  node [
    id 17
    label "fotowoltaika"
  ]
  node [
    id 18
    label "porada"
  ]
  node [
    id 19
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 20
    label "proces"
  ]
  node [
    id 21
    label "imagineskopia"
  ]
  node [
    id 22
    label "typologia"
  ]
  node [
    id 23
    label "&#322;awa_szkolna"
  ]
  node [
    id 24
    label "dysleksja"
  ]
  node [
    id 25
    label "wyczytywanie"
  ]
  node [
    id 26
    label "doczytywanie"
  ]
  node [
    id 27
    label "lektor"
  ]
  node [
    id 28
    label "przepowiadanie"
  ]
  node [
    id 29
    label "wczytywanie_si&#281;"
  ]
  node [
    id 30
    label "oczytywanie_si&#281;"
  ]
  node [
    id 31
    label "zaczytanie_si&#281;"
  ]
  node [
    id 32
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 33
    label "wczytywanie"
  ]
  node [
    id 34
    label "obrz&#261;dek"
  ]
  node [
    id 35
    label "czytywanie"
  ]
  node [
    id 36
    label "bycie_w_stanie"
  ]
  node [
    id 37
    label "pokazywanie"
  ]
  node [
    id 38
    label "poznawanie"
  ]
  node [
    id 39
    label "poczytanie"
  ]
  node [
    id 40
    label "Biblia"
  ]
  node [
    id 41
    label "reading"
  ]
  node [
    id 42
    label "recitation"
  ]
  node [
    id 43
    label "tom"
  ]
  node [
    id 44
    label "pozna&#324;ski"
  ]
  node [
    id 45
    label "hour"
  ]
  node [
    id 46
    label "Meterowa"
  ]
  node [
    id 47
    label "m&#281;ski"
  ]
  node [
    id 48
    label "albo"
  ]
  node [
    id 49
    label "Tinker"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 48
    target 49
  ]
]
