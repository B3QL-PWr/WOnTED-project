graph [
  maxDegree 48
  minDegree 1
  meanDegree 2.122448979591837
  density 0.02188091731537976
  graphCliqueNumber 6
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "j&#243;zef"
    origin "text"
  ]
  node [
    id 2
    label "rojek"
    origin "text"
  ]
  node [
    id 3
    label "klub"
    origin "text"
  ]
  node [
    id 4
    label "parlamentarny"
    origin "text"
  ]
  node [
    id 5
    label "prawo"
    origin "text"
  ]
  node [
    id 6
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "dyplomata"
  ]
  node [
    id 8
    label "wys&#322;annik"
  ]
  node [
    id 9
    label "przedstawiciel"
  ]
  node [
    id 10
    label "kurier_dyplomatyczny"
  ]
  node [
    id 11
    label "ablegat"
  ]
  node [
    id 12
    label "klubista"
  ]
  node [
    id 13
    label "Miko&#322;ajczyk"
  ]
  node [
    id 14
    label "Korwin"
  ]
  node [
    id 15
    label "parlamentarzysta"
  ]
  node [
    id 16
    label "dyscyplina_partyjna"
  ]
  node [
    id 17
    label "izba_ni&#380;sza"
  ]
  node [
    id 18
    label "poselstwo"
  ]
  node [
    id 19
    label "wir"
  ]
  node [
    id 20
    label "society"
  ]
  node [
    id 21
    label "jakobini"
  ]
  node [
    id 22
    label "stowarzyszenie"
  ]
  node [
    id 23
    label "lokal"
  ]
  node [
    id 24
    label "od&#322;am"
  ]
  node [
    id 25
    label "siedziba"
  ]
  node [
    id 26
    label "bar"
  ]
  node [
    id 27
    label "parlamentowy"
  ]
  node [
    id 28
    label "stosowny"
  ]
  node [
    id 29
    label "demokratyczny"
  ]
  node [
    id 30
    label "parlamentarnie"
  ]
  node [
    id 31
    label "obserwacja"
  ]
  node [
    id 32
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 33
    label "nauka_prawa"
  ]
  node [
    id 34
    label "dominion"
  ]
  node [
    id 35
    label "normatywizm"
  ]
  node [
    id 36
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 37
    label "qualification"
  ]
  node [
    id 38
    label "opis"
  ]
  node [
    id 39
    label "regu&#322;a_Allena"
  ]
  node [
    id 40
    label "normalizacja"
  ]
  node [
    id 41
    label "kazuistyka"
  ]
  node [
    id 42
    label "regu&#322;a_Glogera"
  ]
  node [
    id 43
    label "kultura_duchowa"
  ]
  node [
    id 44
    label "prawo_karne"
  ]
  node [
    id 45
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 46
    label "standard"
  ]
  node [
    id 47
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 48
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 49
    label "struktura"
  ]
  node [
    id 50
    label "szko&#322;a"
  ]
  node [
    id 51
    label "prawo_karne_procesowe"
  ]
  node [
    id 52
    label "prawo_Mendla"
  ]
  node [
    id 53
    label "przepis"
  ]
  node [
    id 54
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 55
    label "criterion"
  ]
  node [
    id 56
    label "kanonistyka"
  ]
  node [
    id 57
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 58
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 59
    label "wykonawczy"
  ]
  node [
    id 60
    label "twierdzenie"
  ]
  node [
    id 61
    label "judykatura"
  ]
  node [
    id 62
    label "legislacyjnie"
  ]
  node [
    id 63
    label "umocowa&#263;"
  ]
  node [
    id 64
    label "podmiot"
  ]
  node [
    id 65
    label "procesualistyka"
  ]
  node [
    id 66
    label "kierunek"
  ]
  node [
    id 67
    label "kryminologia"
  ]
  node [
    id 68
    label "kryminalistyka"
  ]
  node [
    id 69
    label "cywilistyka"
  ]
  node [
    id 70
    label "law"
  ]
  node [
    id 71
    label "zasada_d'Alemberta"
  ]
  node [
    id 72
    label "jurisprudence"
  ]
  node [
    id 73
    label "zasada"
  ]
  node [
    id 74
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 75
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 76
    label "konsekwencja"
  ]
  node [
    id 77
    label "punishment"
  ]
  node [
    id 78
    label "cecha"
  ]
  node [
    id 79
    label "roboty_przymusowe"
  ]
  node [
    id 80
    label "nemezis"
  ]
  node [
    id 81
    label "righteousness"
  ]
  node [
    id 82
    label "J&#243;zefa"
  ]
  node [
    id 83
    label "i"
  ]
  node [
    id 84
    label "platforma"
  ]
  node [
    id 85
    label "obywatelski"
  ]
  node [
    id 86
    label "afera"
  ]
  node [
    id 87
    label "hazardowy"
  ]
  node [
    id 88
    label "komisja"
  ]
  node [
    id 89
    label "nadzwyczajny"
  ]
  node [
    id 90
    label "&#8222;"
  ]
  node [
    id 91
    label "przyjazny"
  ]
  node [
    id 92
    label "pa&#324;stwo"
  ]
  node [
    id 93
    label "&#8221;"
  ]
  node [
    id 94
    label "TVP"
  ]
  node [
    id 95
    label "info"
  ]
  node [
    id 96
    label "Andrzej"
  ]
  node [
    id 97
    label "szlachta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 90
  ]
  edge [
    source 88
    target 91
  ]
  edge [
    source 88
    target 92
  ]
  edge [
    source 88
    target 93
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 89
    target 92
  ]
  edge [
    source 89
    target 93
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 92
  ]
  edge [
    source 90
    target 93
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 93
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 96
    target 97
  ]
]
