graph [
  maxDegree 8
  minDegree 1
  meanDegree 3
  density 0.13043478260869565
  graphCliqueNumber 4
  node [
    id 0
    label "bobrowo"
    origin "text"
  ]
  node [
    id 1
    label "powiat"
    origin "text"
  ]
  node [
    id 2
    label "drawski"
    origin "text"
  ]
  node [
    id 3
    label "gmina"
  ]
  node [
    id 4
    label "jednostka_administracyjna"
  ]
  node [
    id 5
    label "wojew&#243;dztwo"
  ]
  node [
    id 6
    label "Stargard"
  ]
  node [
    id 7
    label "szczeci&#324;ski"
  ]
  node [
    id 8
    label "linia"
  ]
  node [
    id 9
    label "kolejowy"
  ]
  node [
    id 10
    label "nr"
  ]
  node [
    id 11
    label "210"
  ]
  node [
    id 12
    label "drogi"
  ]
  node [
    id 13
    label "krajowy"
  ]
  node [
    id 14
    label "20"
  ]
  node [
    id 15
    label "Konstantyn"
  ]
  node [
    id 16
    label "von"
  ]
  node [
    id 17
    label "knebel"
  ]
  node [
    id 18
    label "Doeberitz"
  ]
  node [
    id 19
    label "ludwik"
  ]
  node [
    id 20
    label "Henriet&#261;"
  ]
  node [
    id 21
    label "Grunburg"
  ]
  node [
    id 22
    label "Artur"
  ]
  node [
    id 23
    label "hassa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 20
    target 21
  ]
]
