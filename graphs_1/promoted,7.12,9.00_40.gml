graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8095238095238095
  density 0.09047619047619047
  graphCliqueNumber 3
  node [
    id 0
    label "bethesda"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "problem"
    origin "text"
  ]
  node [
    id 3
    label "fallout"
    origin "text"
  ]
  node [
    id 4
    label "czyj&#347;"
  ]
  node [
    id 5
    label "m&#261;&#380;"
  ]
  node [
    id 6
    label "trudno&#347;&#263;"
  ]
  node [
    id 7
    label "sprawa"
  ]
  node [
    id 8
    label "ambaras"
  ]
  node [
    id 9
    label "problemat"
  ]
  node [
    id 10
    label "pierepa&#322;ka"
  ]
  node [
    id 11
    label "obstruction"
  ]
  node [
    id 12
    label "problematyka"
  ]
  node [
    id 13
    label "jajko_Kolumba"
  ]
  node [
    id 14
    label "subiekcja"
  ]
  node [
    id 15
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 16
    label "Fallout"
  ]
  node [
    id 17
    label "76"
  ]
  node [
    id 18
    label "Power"
  ]
  node [
    id 19
    label "Armor"
  ]
  node [
    id 20
    label "Edition"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
]
