graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9333333333333333
  density 0.06666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "sylwester"
    origin "text"
  ]
  node [
    id 1
    label "pies"
    origin "text"
  ]
  node [
    id 2
    label "heheszki"
    origin "text"
  ]
  node [
    id 3
    label "impreza"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "wy&#263;"
  ]
  node [
    id 6
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 7
    label "spragniony"
  ]
  node [
    id 8
    label "rakarz"
  ]
  node [
    id 9
    label "psowate"
  ]
  node [
    id 10
    label "istota_&#380;ywa"
  ]
  node [
    id 11
    label "kabanos"
  ]
  node [
    id 12
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 13
    label "&#322;ajdak"
  ]
  node [
    id 14
    label "czworon&#243;g"
  ]
  node [
    id 15
    label "policjant"
  ]
  node [
    id 16
    label "szczucie"
  ]
  node [
    id 17
    label "s&#322;u&#380;enie"
  ]
  node [
    id 18
    label "sobaka"
  ]
  node [
    id 19
    label "dogoterapia"
  ]
  node [
    id 20
    label "Cerber"
  ]
  node [
    id 21
    label "wyzwisko"
  ]
  node [
    id 22
    label "szczu&#263;"
  ]
  node [
    id 23
    label "wycie"
  ]
  node [
    id 24
    label "szczeka&#263;"
  ]
  node [
    id 25
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 26
    label "trufla"
  ]
  node [
    id 27
    label "samiec"
  ]
  node [
    id 28
    label "piese&#322;"
  ]
  node [
    id 29
    label "zawy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
]
