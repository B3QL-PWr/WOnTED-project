graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.022099447513812
  density 0.011233885819521179
  graphCliqueNumber 2
  node [
    id 0
    label "prezentowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "opis"
    origin "text"
  ]
  node [
    id 2
    label "budowa"
    origin "text"
  ]
  node [
    id 3
    label "model"
    origin "text"
  ]
  node [
    id 4
    label "axial"
    origin "text"
  ]
  node [
    id 5
    label "scorpion"
    origin "text"
  ]
  node [
    id 6
    label "wersja"
    origin "text"
  ]
  node [
    id 7
    label "kit"
    origin "text"
  ]
  node [
    id 8
    label "mama"
    origin "text"
  ]
  node [
    id 9
    label "nadzieja"
    origin "text"
  ]
  node [
    id 10
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 11
    label "dok&#322;adny"
    origin "text"
  ]
  node [
    id 12
    label "fotografia"
    origin "text"
  ]
  node [
    id 13
    label "montowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "element"
    origin "text"
  ]
  node [
    id 15
    label "by&#263;"
    origin "text"
  ]
  node [
    id 16
    label "przydatny"
    origin "text"
  ]
  node [
    id 17
    label "uzupe&#322;nienie"
    origin "text"
  ]
  node [
    id 18
    label "czarno"
    origin "text"
  ]
  node [
    id 19
    label "bia&#322;a"
    origin "text"
  ]
  node [
    id 20
    label "instrukcja"
    origin "text"
  ]
  node [
    id 21
    label "do&#322;&#261;cza&#263;"
    origin "text"
  ]
  node [
    id 22
    label "zapoznawa&#263;"
  ]
  node [
    id 23
    label "przedstawia&#263;"
  ]
  node [
    id 24
    label "present"
  ]
  node [
    id 25
    label "gra&#263;"
  ]
  node [
    id 26
    label "uprzedza&#263;"
  ]
  node [
    id 27
    label "represent"
  ]
  node [
    id 28
    label "program"
  ]
  node [
    id 29
    label "wyra&#380;a&#263;"
  ]
  node [
    id 30
    label "attest"
  ]
  node [
    id 31
    label "display"
  ]
  node [
    id 32
    label "exposition"
  ]
  node [
    id 33
    label "czynno&#347;&#263;"
  ]
  node [
    id 34
    label "wypowied&#378;"
  ]
  node [
    id 35
    label "obja&#347;nienie"
  ]
  node [
    id 36
    label "figura"
  ]
  node [
    id 37
    label "wjazd"
  ]
  node [
    id 38
    label "struktura"
  ]
  node [
    id 39
    label "konstrukcja"
  ]
  node [
    id 40
    label "r&#243;w"
  ]
  node [
    id 41
    label "kreacja"
  ]
  node [
    id 42
    label "posesja"
  ]
  node [
    id 43
    label "cecha"
  ]
  node [
    id 44
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 45
    label "organ"
  ]
  node [
    id 46
    label "mechanika"
  ]
  node [
    id 47
    label "zwierz&#281;"
  ]
  node [
    id 48
    label "miejsce_pracy"
  ]
  node [
    id 49
    label "praca"
  ]
  node [
    id 50
    label "constitution"
  ]
  node [
    id 51
    label "typ"
  ]
  node [
    id 52
    label "cz&#322;owiek"
  ]
  node [
    id 53
    label "pozowa&#263;"
  ]
  node [
    id 54
    label "ideal"
  ]
  node [
    id 55
    label "matryca"
  ]
  node [
    id 56
    label "imitacja"
  ]
  node [
    id 57
    label "ruch"
  ]
  node [
    id 58
    label "motif"
  ]
  node [
    id 59
    label "pozowanie"
  ]
  node [
    id 60
    label "wz&#243;r"
  ]
  node [
    id 61
    label "miniatura"
  ]
  node [
    id 62
    label "prezenter"
  ]
  node [
    id 63
    label "facet"
  ]
  node [
    id 64
    label "orygina&#322;"
  ]
  node [
    id 65
    label "mildew"
  ]
  node [
    id 66
    label "spos&#243;b"
  ]
  node [
    id 67
    label "zi&#243;&#322;ko"
  ]
  node [
    id 68
    label "adaptation"
  ]
  node [
    id 69
    label "posta&#263;"
  ]
  node [
    id 70
    label "wcisk"
  ]
  node [
    id 71
    label "masa"
  ]
  node [
    id 72
    label "&#347;ciema"
  ]
  node [
    id 73
    label "matczysko"
  ]
  node [
    id 74
    label "macierz"
  ]
  node [
    id 75
    label "przodkini"
  ]
  node [
    id 76
    label "Matka_Boska"
  ]
  node [
    id 77
    label "macocha"
  ]
  node [
    id 78
    label "matka_zast&#281;pcza"
  ]
  node [
    id 79
    label "stara"
  ]
  node [
    id 80
    label "rodzice"
  ]
  node [
    id 81
    label "rodzic"
  ]
  node [
    id 82
    label "&#347;wiate&#322;ko_w_tunelu"
  ]
  node [
    id 83
    label "wierzy&#263;"
  ]
  node [
    id 84
    label "szansa"
  ]
  node [
    id 85
    label "oczekiwanie"
  ]
  node [
    id 86
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 87
    label "spoczywa&#263;"
  ]
  node [
    id 88
    label "dok&#322;adnie"
  ]
  node [
    id 89
    label "precyzowanie"
  ]
  node [
    id 90
    label "rzetelny"
  ]
  node [
    id 91
    label "sprecyzowanie"
  ]
  node [
    id 92
    label "miliamperomierz"
  ]
  node [
    id 93
    label "precyzyjny"
  ]
  node [
    id 94
    label "bezcieniowy"
  ]
  node [
    id 95
    label "wyretuszowanie"
  ]
  node [
    id 96
    label "przedstawienie"
  ]
  node [
    id 97
    label "fota"
  ]
  node [
    id 98
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 99
    label "podlew"
  ]
  node [
    id 100
    label "ziarno"
  ]
  node [
    id 101
    label "obraz"
  ]
  node [
    id 102
    label "legitymacja"
  ]
  node [
    id 103
    label "przepa&#322;"
  ]
  node [
    id 104
    label "fotogaleria"
  ]
  node [
    id 105
    label "winieta"
  ]
  node [
    id 106
    label "retuszowanie"
  ]
  node [
    id 107
    label "monid&#322;o"
  ]
  node [
    id 108
    label "talbotypia"
  ]
  node [
    id 109
    label "wyretuszowa&#263;"
  ]
  node [
    id 110
    label "fototeka"
  ]
  node [
    id 111
    label "photograph"
  ]
  node [
    id 112
    label "ekspozycja"
  ]
  node [
    id 113
    label "retuszowa&#263;"
  ]
  node [
    id 114
    label "sk&#322;ada&#263;"
  ]
  node [
    id 115
    label "konstruowa&#263;"
  ]
  node [
    id 116
    label "umieszcza&#263;"
  ]
  node [
    id 117
    label "tworzy&#263;"
  ]
  node [
    id 118
    label "raise"
  ]
  node [
    id 119
    label "supply"
  ]
  node [
    id 120
    label "organizowa&#263;"
  ]
  node [
    id 121
    label "scala&#263;"
  ]
  node [
    id 122
    label "szkodnik"
  ]
  node [
    id 123
    label "&#347;rodowisko"
  ]
  node [
    id 124
    label "component"
  ]
  node [
    id 125
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 126
    label "r&#243;&#380;niczka"
  ]
  node [
    id 127
    label "przedmiot"
  ]
  node [
    id 128
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 129
    label "gangsterski"
  ]
  node [
    id 130
    label "szambo"
  ]
  node [
    id 131
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 132
    label "materia"
  ]
  node [
    id 133
    label "aspo&#322;eczny"
  ]
  node [
    id 134
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 135
    label "poj&#281;cie"
  ]
  node [
    id 136
    label "underworld"
  ]
  node [
    id 137
    label "si&#281;ga&#263;"
  ]
  node [
    id 138
    label "trwa&#263;"
  ]
  node [
    id 139
    label "obecno&#347;&#263;"
  ]
  node [
    id 140
    label "stan"
  ]
  node [
    id 141
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 142
    label "stand"
  ]
  node [
    id 143
    label "mie&#263;_miejsce"
  ]
  node [
    id 144
    label "uczestniczy&#263;"
  ]
  node [
    id 145
    label "chodzi&#263;"
  ]
  node [
    id 146
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 147
    label "equal"
  ]
  node [
    id 148
    label "przydatnie"
  ]
  node [
    id 149
    label "po&#380;&#261;dany"
  ]
  node [
    id 150
    label "potrzebny"
  ]
  node [
    id 151
    label "dodanie"
  ]
  node [
    id 152
    label "doj&#347;cie"
  ]
  node [
    id 153
    label "summation"
  ]
  node [
    id 154
    label "doj&#347;&#263;"
  ]
  node [
    id 155
    label "dochodzenie"
  ]
  node [
    id 156
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 157
    label "rzecz"
  ]
  node [
    id 158
    label "attachment"
  ]
  node [
    id 159
    label "rozprawa"
  ]
  node [
    id 160
    label "niepomy&#347;lnie"
  ]
  node [
    id 161
    label "pesymistyczny"
  ]
  node [
    id 162
    label "negatywnie"
  ]
  node [
    id 163
    label "pessimistically"
  ]
  node [
    id 164
    label "gorzko"
  ]
  node [
    id 165
    label "czarny"
  ]
  node [
    id 166
    label "brudno"
  ]
  node [
    id 167
    label "ponuro"
  ]
  node [
    id 168
    label "przewrotnie"
  ]
  node [
    id 169
    label "czarnosk&#243;ry"
  ]
  node [
    id 170
    label "granatowy"
  ]
  node [
    id 171
    label "ciemno"
  ]
  node [
    id 172
    label "ulotka"
  ]
  node [
    id 173
    label "instruktarz"
  ]
  node [
    id 174
    label "zbi&#243;r"
  ]
  node [
    id 175
    label "wskaz&#243;wka"
  ]
  node [
    id 176
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 177
    label "dodawa&#263;"
  ]
  node [
    id 178
    label "bind"
  ]
  node [
    id 179
    label "submit"
  ]
  node [
    id 180
    label "dokoptowywa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
]
