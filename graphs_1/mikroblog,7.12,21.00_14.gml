graph [
  maxDegree 29
  minDegree 1
  meanDegree 2
  density 0.029411764705882353
  graphCliqueNumber 3
  node [
    id 0
    label "mirka"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "podoba"
    origin "text"
  ]
  node [
    id 3
    label "pasek"
    origin "text"
  ]
  node [
    id 4
    label "sk&#243;rzany"
    origin "text"
  ]
  node [
    id 5
    label "nowy"
    origin "text"
  ]
  node [
    id 6
    label "kolekcja"
    origin "text"
  ]
  node [
    id 7
    label "nasz"
    origin "text"
  ]
  node [
    id 8
    label "firma"
    origin "text"
  ]
  node [
    id 9
    label "tomsk&#243;r"
    origin "text"
  ]
  node [
    id 10
    label "zwi&#261;zek"
  ]
  node [
    id 11
    label "dyktando"
  ]
  node [
    id 12
    label "dodatek"
  ]
  node [
    id 13
    label "obiekt"
  ]
  node [
    id 14
    label "oznaka"
  ]
  node [
    id 15
    label "prevention"
  ]
  node [
    id 16
    label "spekulacja"
  ]
  node [
    id 17
    label "handel"
  ]
  node [
    id 18
    label "zone"
  ]
  node [
    id 19
    label "naszywka"
  ]
  node [
    id 20
    label "us&#322;uga"
  ]
  node [
    id 21
    label "przewi&#261;zka"
  ]
  node [
    id 22
    label "nagrzbietnik"
  ]
  node [
    id 23
    label "naturalny"
  ]
  node [
    id 24
    label "przypominaj&#261;cy"
  ]
  node [
    id 25
    label "cz&#322;owiek"
  ]
  node [
    id 26
    label "nowotny"
  ]
  node [
    id 27
    label "drugi"
  ]
  node [
    id 28
    label "kolejny"
  ]
  node [
    id 29
    label "bie&#380;&#261;cy"
  ]
  node [
    id 30
    label "nowo"
  ]
  node [
    id 31
    label "narybek"
  ]
  node [
    id 32
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 33
    label "obcy"
  ]
  node [
    id 34
    label "stage_set"
  ]
  node [
    id 35
    label "linia"
  ]
  node [
    id 36
    label "album"
  ]
  node [
    id 37
    label "collection"
  ]
  node [
    id 38
    label "zbi&#243;r"
  ]
  node [
    id 39
    label "czyj&#347;"
  ]
  node [
    id 40
    label "Hortex"
  ]
  node [
    id 41
    label "MAC"
  ]
  node [
    id 42
    label "reengineering"
  ]
  node [
    id 43
    label "nazwa_w&#322;asna"
  ]
  node [
    id 44
    label "podmiot_gospodarczy"
  ]
  node [
    id 45
    label "Google"
  ]
  node [
    id 46
    label "zaufanie"
  ]
  node [
    id 47
    label "biurowiec"
  ]
  node [
    id 48
    label "networking"
  ]
  node [
    id 49
    label "zasoby_ludzkie"
  ]
  node [
    id 50
    label "interes"
  ]
  node [
    id 51
    label "paczkarnia"
  ]
  node [
    id 52
    label "Canon"
  ]
  node [
    id 53
    label "HP"
  ]
  node [
    id 54
    label "Baltona"
  ]
  node [
    id 55
    label "Pewex"
  ]
  node [
    id 56
    label "MAN_SE"
  ]
  node [
    id 57
    label "Apeks"
  ]
  node [
    id 58
    label "zasoby"
  ]
  node [
    id 59
    label "Orbis"
  ]
  node [
    id 60
    label "miejsce_pracy"
  ]
  node [
    id 61
    label "siedziba"
  ]
  node [
    id 62
    label "Spo&#322;em"
  ]
  node [
    id 63
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 64
    label "Orlen"
  ]
  node [
    id 65
    label "klasa"
  ]
  node [
    id 66
    label "P"
  ]
  node [
    id 67
    label "sekunda"
  ]
  node [
    id 68
    label "jak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 68
  ]
  edge [
    source 67
    target 68
  ]
]
