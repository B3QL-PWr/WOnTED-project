graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.0610687022900764
  density 0.015854374633000587
  graphCliqueNumber 2
  node [
    id 0
    label "szanowny"
    origin "text"
  ]
  node [
    id 1
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "spora"
    origin "text"
  ]
  node [
    id 4
    label "afera"
    origin "text"
  ]
  node [
    id 5
    label "bank"
    origin "text"
  ]
  node [
    id 6
    label "idea"
    origin "text"
  ]
  node [
    id 7
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ruski"
    origin "text"
  ]
  node [
    id 9
    label "zasada"
    origin "text"
  ]
  node [
    id 10
    label "znaczy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "jak"
    origin "text"
  ]
  node [
    id 12
    label "upomnie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "szacownie"
  ]
  node [
    id 14
    label "take_care"
  ]
  node [
    id 15
    label "troska&#263;_si&#281;"
  ]
  node [
    id 16
    label "zamierza&#263;"
  ]
  node [
    id 17
    label "os&#261;dza&#263;"
  ]
  node [
    id 18
    label "robi&#263;"
  ]
  node [
    id 19
    label "argue"
  ]
  node [
    id 20
    label "rozpatrywa&#263;"
  ]
  node [
    id 21
    label "deliver"
  ]
  node [
    id 22
    label "si&#281;ga&#263;"
  ]
  node [
    id 23
    label "trwa&#263;"
  ]
  node [
    id 24
    label "obecno&#347;&#263;"
  ]
  node [
    id 25
    label "stan"
  ]
  node [
    id 26
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 27
    label "stand"
  ]
  node [
    id 28
    label "mie&#263;_miejsce"
  ]
  node [
    id 29
    label "uczestniczy&#263;"
  ]
  node [
    id 30
    label "chodzi&#263;"
  ]
  node [
    id 31
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 32
    label "equal"
  ]
  node [
    id 33
    label "kom&#243;rka_ro&#347;linna"
  ]
  node [
    id 34
    label "egzyna"
  ]
  node [
    id 35
    label "spore"
  ]
  node [
    id 36
    label "intyna"
  ]
  node [
    id 37
    label "cyrk"
  ]
  node [
    id 38
    label "argument"
  ]
  node [
    id 39
    label "skandal"
  ]
  node [
    id 40
    label "sensacja"
  ]
  node [
    id 41
    label "przest&#281;pstwo"
  ]
  node [
    id 42
    label "smr&#243;d"
  ]
  node [
    id 43
    label "occupation"
  ]
  node [
    id 44
    label "wk&#322;adca"
  ]
  node [
    id 45
    label "agencja"
  ]
  node [
    id 46
    label "konto"
  ]
  node [
    id 47
    label "agent_rozliczeniowy"
  ]
  node [
    id 48
    label "eurorynek"
  ]
  node [
    id 49
    label "zbi&#243;r"
  ]
  node [
    id 50
    label "instytucja"
  ]
  node [
    id 51
    label "siedziba"
  ]
  node [
    id 52
    label "kwota"
  ]
  node [
    id 53
    label "byt"
  ]
  node [
    id 54
    label "istota"
  ]
  node [
    id 55
    label "ideologia"
  ]
  node [
    id 56
    label "intelekt"
  ]
  node [
    id 57
    label "Kant"
  ]
  node [
    id 58
    label "pomys&#322;"
  ]
  node [
    id 59
    label "poj&#281;cie"
  ]
  node [
    id 60
    label "cel"
  ]
  node [
    id 61
    label "p&#322;&#243;d"
  ]
  node [
    id 62
    label "ideacja"
  ]
  node [
    id 63
    label "work"
  ]
  node [
    id 64
    label "reakcja_chemiczna"
  ]
  node [
    id 65
    label "function"
  ]
  node [
    id 66
    label "commit"
  ]
  node [
    id 67
    label "bangla&#263;"
  ]
  node [
    id 68
    label "determine"
  ]
  node [
    id 69
    label "tryb"
  ]
  node [
    id 70
    label "powodowa&#263;"
  ]
  node [
    id 71
    label "dziama&#263;"
  ]
  node [
    id 72
    label "istnie&#263;"
  ]
  node [
    id 73
    label "po_rosyjsku"
  ]
  node [
    id 74
    label "toporny"
  ]
  node [
    id 75
    label "serowy"
  ]
  node [
    id 76
    label "zbytkowny"
  ]
  node [
    id 77
    label "j&#281;zyk"
  ]
  node [
    id 78
    label "ostentacyjny"
  ]
  node [
    id 79
    label "byle_jaki"
  ]
  node [
    id 80
    label "wielkoruski"
  ]
  node [
    id 81
    label "tandetny"
  ]
  node [
    id 82
    label "kacapski"
  ]
  node [
    id 83
    label "Russian"
  ]
  node [
    id 84
    label "wulgarny"
  ]
  node [
    id 85
    label "j&#281;zyki_wschodnios&#322;owia&#324;skie"
  ]
  node [
    id 86
    label "po_rusku"
  ]
  node [
    id 87
    label "ziemniaczany"
  ]
  node [
    id 88
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 89
    label "rusek"
  ]
  node [
    id 90
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 91
    label "obserwacja"
  ]
  node [
    id 92
    label "moralno&#347;&#263;"
  ]
  node [
    id 93
    label "podstawa"
  ]
  node [
    id 94
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 95
    label "umowa"
  ]
  node [
    id 96
    label "dominion"
  ]
  node [
    id 97
    label "qualification"
  ]
  node [
    id 98
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 99
    label "opis"
  ]
  node [
    id 100
    label "regu&#322;a_Allena"
  ]
  node [
    id 101
    label "normalizacja"
  ]
  node [
    id 102
    label "regu&#322;a_Glogera"
  ]
  node [
    id 103
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 104
    label "standard"
  ]
  node [
    id 105
    label "base"
  ]
  node [
    id 106
    label "substancja"
  ]
  node [
    id 107
    label "spos&#243;b"
  ]
  node [
    id 108
    label "prawid&#322;o"
  ]
  node [
    id 109
    label "prawo_Mendla"
  ]
  node [
    id 110
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 111
    label "criterion"
  ]
  node [
    id 112
    label "twierdzenie"
  ]
  node [
    id 113
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 114
    label "prawo"
  ]
  node [
    id 115
    label "zasada_d'Alemberta"
  ]
  node [
    id 116
    label "spell"
  ]
  node [
    id 117
    label "odgrywa&#263;_rol&#281;"
  ]
  node [
    id 118
    label "zostawia&#263;"
  ]
  node [
    id 119
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 120
    label "represent"
  ]
  node [
    id 121
    label "count"
  ]
  node [
    id 122
    label "wyraz"
  ]
  node [
    id 123
    label "zdobi&#263;"
  ]
  node [
    id 124
    label "byd&#322;o"
  ]
  node [
    id 125
    label "zobo"
  ]
  node [
    id 126
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 127
    label "yakalo"
  ]
  node [
    id 128
    label "dzo"
  ]
  node [
    id 129
    label "zakomunikowa&#263;"
  ]
  node [
    id 130
    label "caution"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
]
