graph [
  maxDegree 594
  minDegree 1
  meanDegree 2.1204819277108435
  density 0.0025578792855378086
  graphCliqueNumber 5
  node [
    id 0
    label "rejestr"
    origin "text"
  ]
  node [
    id 1
    label "wyborca"
    origin "text"
  ]
  node [
    id 2
    label "s&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 3
    label "sporz&#261;dza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "spis"
    origin "text"
  ]
  node [
    id 5
    label "dla"
    origin "text"
  ]
  node [
    id 6
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 7
    label "prezydent"
    origin "text"
  ]
  node [
    id 8
    label "rzeczpospolita"
    origin "text"
  ]
  node [
    id 9
    label "polski"
    origin "text"
  ]
  node [
    id 10
    label "wybory"
    origin "text"
  ]
  node [
    id 11
    label "sejm"
    origin "text"
  ]
  node [
    id 12
    label "senat"
    origin "text"
  ]
  node [
    id 13
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 14
    label "parlament"
    origin "text"
  ]
  node [
    id 15
    label "europejski"
    origin "text"
  ]
  node [
    id 16
    label "rad"
    origin "text"
  ]
  node [
    id 17
    label "gmin"
    origin "text"
  ]
  node [
    id 18
    label "powiat"
    origin "text"
  ]
  node [
    id 19
    label "sejmik"
    origin "text"
  ]
  node [
    id 20
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 21
    label "w&#243;jt"
    origin "text"
  ]
  node [
    id 22
    label "burmistrz"
    origin "text"
  ]
  node [
    id 23
    label "miasto"
    origin "text"
  ]
  node [
    id 24
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 25
    label "osoba"
    origin "text"
  ]
  node [
    id 26
    label "uprawniony"
    origin "text"
  ]
  node [
    id 27
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 28
    label "referendum"
    origin "text"
  ]
  node [
    id 29
    label "wyliczanka"
  ]
  node [
    id 30
    label "skala"
  ]
  node [
    id 31
    label "catalog"
  ]
  node [
    id 32
    label "organy"
  ]
  node [
    id 33
    label "brzmienie"
  ]
  node [
    id 34
    label "stock"
  ]
  node [
    id 35
    label "procesor"
  ]
  node [
    id 36
    label "figurowa&#263;"
  ]
  node [
    id 37
    label "przycisk"
  ]
  node [
    id 38
    label "zbi&#243;r"
  ]
  node [
    id 39
    label "book"
  ]
  node [
    id 40
    label "urz&#261;dzenie"
  ]
  node [
    id 41
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 42
    label "regestr"
  ]
  node [
    id 43
    label "pozycja"
  ]
  node [
    id 44
    label "tekst"
  ]
  node [
    id 45
    label "sumariusz"
  ]
  node [
    id 46
    label "elektorat"
  ]
  node [
    id 47
    label "obywatel"
  ]
  node [
    id 48
    label "use"
  ]
  node [
    id 49
    label "trwa&#263;"
  ]
  node [
    id 50
    label "by&#263;"
  ]
  node [
    id 51
    label "&#380;o&#322;nierz"
  ]
  node [
    id 52
    label "pies"
  ]
  node [
    id 53
    label "robi&#263;"
  ]
  node [
    id 54
    label "wait"
  ]
  node [
    id 55
    label "pomaga&#263;"
  ]
  node [
    id 56
    label "cel"
  ]
  node [
    id 57
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 58
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 59
    label "pracowa&#263;"
  ]
  node [
    id 60
    label "suffice"
  ]
  node [
    id 61
    label "match"
  ]
  node [
    id 62
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 63
    label "przygotowywa&#263;"
  ]
  node [
    id 64
    label "give"
  ]
  node [
    id 65
    label "dzieli&#263;"
  ]
  node [
    id 66
    label "oprawia&#263;"
  ]
  node [
    id 67
    label "opracowywa&#263;"
  ]
  node [
    id 68
    label "tworzy&#263;"
  ]
  node [
    id 69
    label "wytwarza&#263;"
  ]
  node [
    id 70
    label "czynno&#347;&#263;"
  ]
  node [
    id 71
    label "akt"
  ]
  node [
    id 72
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 73
    label "decyzja"
  ]
  node [
    id 74
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 75
    label "pick"
  ]
  node [
    id 76
    label "Jelcyn"
  ]
  node [
    id 77
    label "Roosevelt"
  ]
  node [
    id 78
    label "Clinton"
  ]
  node [
    id 79
    label "dostojnik"
  ]
  node [
    id 80
    label "Nixon"
  ]
  node [
    id 81
    label "Tito"
  ]
  node [
    id 82
    label "de_Gaulle"
  ]
  node [
    id 83
    label "gruba_ryba"
  ]
  node [
    id 84
    label "Gorbaczow"
  ]
  node [
    id 85
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 86
    label "Putin"
  ]
  node [
    id 87
    label "Naser"
  ]
  node [
    id 88
    label "samorz&#261;dowiec"
  ]
  node [
    id 89
    label "Kemal"
  ]
  node [
    id 90
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 91
    label "zwierzchnik"
  ]
  node [
    id 92
    label "Bierut"
  ]
  node [
    id 93
    label "Buriacja"
  ]
  node [
    id 94
    label "Abchazja"
  ]
  node [
    id 95
    label "Inguszetia"
  ]
  node [
    id 96
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 97
    label "Nachiczewan"
  ]
  node [
    id 98
    label "Karaka&#322;pacja"
  ]
  node [
    id 99
    label "Jakucja"
  ]
  node [
    id 100
    label "Singapur"
  ]
  node [
    id 101
    label "Komi"
  ]
  node [
    id 102
    label "Karelia"
  ]
  node [
    id 103
    label "Tatarstan"
  ]
  node [
    id 104
    label "Chakasja"
  ]
  node [
    id 105
    label "Dagestan"
  ]
  node [
    id 106
    label "Mordowia"
  ]
  node [
    id 107
    label "Ka&#322;mucja"
  ]
  node [
    id 108
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 109
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 110
    label "Baszkiria"
  ]
  node [
    id 111
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 112
    label "Mari_El"
  ]
  node [
    id 113
    label "Ad&#380;aria"
  ]
  node [
    id 114
    label "Czuwaszja"
  ]
  node [
    id 115
    label "Tuwa"
  ]
  node [
    id 116
    label "Czeczenia"
  ]
  node [
    id 117
    label "Udmurcja"
  ]
  node [
    id 118
    label "pa&#324;stwo"
  ]
  node [
    id 119
    label "lacki"
  ]
  node [
    id 120
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 121
    label "przedmiot"
  ]
  node [
    id 122
    label "sztajer"
  ]
  node [
    id 123
    label "drabant"
  ]
  node [
    id 124
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 125
    label "polak"
  ]
  node [
    id 126
    label "pierogi_ruskie"
  ]
  node [
    id 127
    label "krakowiak"
  ]
  node [
    id 128
    label "Polish"
  ]
  node [
    id 129
    label "j&#281;zyk"
  ]
  node [
    id 130
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 131
    label "oberek"
  ]
  node [
    id 132
    label "po_polsku"
  ]
  node [
    id 133
    label "mazur"
  ]
  node [
    id 134
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 135
    label "chodzony"
  ]
  node [
    id 136
    label "skoczny"
  ]
  node [
    id 137
    label "ryba_po_grecku"
  ]
  node [
    id 138
    label "goniony"
  ]
  node [
    id 139
    label "polsko"
  ]
  node [
    id 140
    label "skrutator"
  ]
  node [
    id 141
    label "g&#322;osowanie"
  ]
  node [
    id 142
    label "obrady"
  ]
  node [
    id 143
    label "grupa"
  ]
  node [
    id 144
    label "lewica"
  ]
  node [
    id 145
    label "zgromadzenie"
  ]
  node [
    id 146
    label "prawica"
  ]
  node [
    id 147
    label "centrum"
  ]
  node [
    id 148
    label "izba_ni&#380;sza"
  ]
  node [
    id 149
    label "siedziba"
  ]
  node [
    id 150
    label "parliament"
  ]
  node [
    id 151
    label "deputation"
  ]
  node [
    id 152
    label "izba_wy&#380;sza"
  ]
  node [
    id 153
    label "organ"
  ]
  node [
    id 154
    label "kolegium"
  ]
  node [
    id 155
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 156
    label "magistrat"
  ]
  node [
    id 157
    label "dyplomata"
  ]
  node [
    id 158
    label "wys&#322;annik"
  ]
  node [
    id 159
    label "przedstawiciel"
  ]
  node [
    id 160
    label "kurier_dyplomatyczny"
  ]
  node [
    id 161
    label "ablegat"
  ]
  node [
    id 162
    label "klubista"
  ]
  node [
    id 163
    label "Miko&#322;ajczyk"
  ]
  node [
    id 164
    label "Korwin"
  ]
  node [
    id 165
    label "parlamentarzysta"
  ]
  node [
    id 166
    label "dyscyplina_partyjna"
  ]
  node [
    id 167
    label "poselstwo"
  ]
  node [
    id 168
    label "plankton_polityczny"
  ]
  node [
    id 169
    label "ustawodawca"
  ]
  node [
    id 170
    label "urz&#261;d"
  ]
  node [
    id 171
    label "europarlament"
  ]
  node [
    id 172
    label "grupa_bilateralna"
  ]
  node [
    id 173
    label "European"
  ]
  node [
    id 174
    label "po_europejsku"
  ]
  node [
    id 175
    label "charakterystyczny"
  ]
  node [
    id 176
    label "europejsko"
  ]
  node [
    id 177
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 178
    label "typowy"
  ]
  node [
    id 179
    label "berylowiec"
  ]
  node [
    id 180
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 181
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 182
    label "mikroradian"
  ]
  node [
    id 183
    label "zadowolenie_si&#281;"
  ]
  node [
    id 184
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 185
    label "content"
  ]
  node [
    id 186
    label "jednostka_promieniowania"
  ]
  node [
    id 187
    label "miliradian"
  ]
  node [
    id 188
    label "jednostka"
  ]
  node [
    id 189
    label "stan_trzeci"
  ]
  node [
    id 190
    label "stan"
  ]
  node [
    id 191
    label "gminno&#347;&#263;"
  ]
  node [
    id 192
    label "gmina"
  ]
  node [
    id 193
    label "jednostka_administracyjna"
  ]
  node [
    id 194
    label "makroregion"
  ]
  node [
    id 195
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 196
    label "mikroregion"
  ]
  node [
    id 197
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 198
    label "ceklarz"
  ]
  node [
    id 199
    label "burmistrzyna"
  ]
  node [
    id 200
    label "Brac&#322;aw"
  ]
  node [
    id 201
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 202
    label "G&#322;uch&#243;w"
  ]
  node [
    id 203
    label "Hallstatt"
  ]
  node [
    id 204
    label "Zbara&#380;"
  ]
  node [
    id 205
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 206
    label "Suworow"
  ]
  node [
    id 207
    label "Halicz"
  ]
  node [
    id 208
    label "Gandawa"
  ]
  node [
    id 209
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 210
    label "Wismar"
  ]
  node [
    id 211
    label "Norymberga"
  ]
  node [
    id 212
    label "Ruciane-Nida"
  ]
  node [
    id 213
    label "Wia&#378;ma"
  ]
  node [
    id 214
    label "Sewilla"
  ]
  node [
    id 215
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 216
    label "Kobry&#324;"
  ]
  node [
    id 217
    label "Brno"
  ]
  node [
    id 218
    label "Tomsk"
  ]
  node [
    id 219
    label "Poniatowa"
  ]
  node [
    id 220
    label "Hadziacz"
  ]
  node [
    id 221
    label "Tiume&#324;"
  ]
  node [
    id 222
    label "Karlsbad"
  ]
  node [
    id 223
    label "Drohobycz"
  ]
  node [
    id 224
    label "Lyon"
  ]
  node [
    id 225
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 226
    label "K&#322;odawa"
  ]
  node [
    id 227
    label "Solikamsk"
  ]
  node [
    id 228
    label "Wolgast"
  ]
  node [
    id 229
    label "Saloniki"
  ]
  node [
    id 230
    label "Lw&#243;w"
  ]
  node [
    id 231
    label "Al-Kufa"
  ]
  node [
    id 232
    label "Hamburg"
  ]
  node [
    id 233
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 234
    label "Nampula"
  ]
  node [
    id 235
    label "D&#252;sseldorf"
  ]
  node [
    id 236
    label "Nowy_Orlean"
  ]
  node [
    id 237
    label "Bamberg"
  ]
  node [
    id 238
    label "Osaka"
  ]
  node [
    id 239
    label "Michalovce"
  ]
  node [
    id 240
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 241
    label "Fryburg"
  ]
  node [
    id 242
    label "Trabzon"
  ]
  node [
    id 243
    label "Wersal"
  ]
  node [
    id 244
    label "Swatowe"
  ]
  node [
    id 245
    label "Ka&#322;uga"
  ]
  node [
    id 246
    label "Dijon"
  ]
  node [
    id 247
    label "Cannes"
  ]
  node [
    id 248
    label "Borowsk"
  ]
  node [
    id 249
    label "Kursk"
  ]
  node [
    id 250
    label "Tyberiada"
  ]
  node [
    id 251
    label "Boden"
  ]
  node [
    id 252
    label "Dodona"
  ]
  node [
    id 253
    label "Vukovar"
  ]
  node [
    id 254
    label "Soleczniki"
  ]
  node [
    id 255
    label "Barcelona"
  ]
  node [
    id 256
    label "Oszmiana"
  ]
  node [
    id 257
    label "Stuttgart"
  ]
  node [
    id 258
    label "Nerczy&#324;sk"
  ]
  node [
    id 259
    label "Essen"
  ]
  node [
    id 260
    label "Bijsk"
  ]
  node [
    id 261
    label "Luboml"
  ]
  node [
    id 262
    label "Gr&#243;dek"
  ]
  node [
    id 263
    label "Orany"
  ]
  node [
    id 264
    label "Siedliszcze"
  ]
  node [
    id 265
    label "P&#322;owdiw"
  ]
  node [
    id 266
    label "A&#322;apajewsk"
  ]
  node [
    id 267
    label "Liverpool"
  ]
  node [
    id 268
    label "Ostrawa"
  ]
  node [
    id 269
    label "Penza"
  ]
  node [
    id 270
    label "Rudki"
  ]
  node [
    id 271
    label "Aktobe"
  ]
  node [
    id 272
    label "I&#322;awka"
  ]
  node [
    id 273
    label "Tolkmicko"
  ]
  node [
    id 274
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 275
    label "Sajgon"
  ]
  node [
    id 276
    label "Windawa"
  ]
  node [
    id 277
    label "Weimar"
  ]
  node [
    id 278
    label "Jekaterynburg"
  ]
  node [
    id 279
    label "Lejda"
  ]
  node [
    id 280
    label "Cremona"
  ]
  node [
    id 281
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 282
    label "Kordoba"
  ]
  node [
    id 283
    label "&#321;ohojsk"
  ]
  node [
    id 284
    label "Kalmar"
  ]
  node [
    id 285
    label "Akerman"
  ]
  node [
    id 286
    label "Locarno"
  ]
  node [
    id 287
    label "Bych&#243;w"
  ]
  node [
    id 288
    label "Toledo"
  ]
  node [
    id 289
    label "Minusi&#324;sk"
  ]
  node [
    id 290
    label "Szk&#322;&#243;w"
  ]
  node [
    id 291
    label "Wenecja"
  ]
  node [
    id 292
    label "Bazylea"
  ]
  node [
    id 293
    label "Peszt"
  ]
  node [
    id 294
    label "Piza"
  ]
  node [
    id 295
    label "Tanger"
  ]
  node [
    id 296
    label "Krzywi&#324;"
  ]
  node [
    id 297
    label "Eger"
  ]
  node [
    id 298
    label "Bogus&#322;aw"
  ]
  node [
    id 299
    label "Taganrog"
  ]
  node [
    id 300
    label "Oksford"
  ]
  node [
    id 301
    label "Gwardiejsk"
  ]
  node [
    id 302
    label "Tyraspol"
  ]
  node [
    id 303
    label "Kleczew"
  ]
  node [
    id 304
    label "Nowa_D&#281;ba"
  ]
  node [
    id 305
    label "Wilejka"
  ]
  node [
    id 306
    label "Modena"
  ]
  node [
    id 307
    label "Demmin"
  ]
  node [
    id 308
    label "Houston"
  ]
  node [
    id 309
    label "Rydu&#322;towy"
  ]
  node [
    id 310
    label "Bordeaux"
  ]
  node [
    id 311
    label "Schmalkalden"
  ]
  node [
    id 312
    label "O&#322;omuniec"
  ]
  node [
    id 313
    label "Tuluza"
  ]
  node [
    id 314
    label "tramwaj"
  ]
  node [
    id 315
    label "Nantes"
  ]
  node [
    id 316
    label "Debreczyn"
  ]
  node [
    id 317
    label "Kowel"
  ]
  node [
    id 318
    label "Witnica"
  ]
  node [
    id 319
    label "Stalingrad"
  ]
  node [
    id 320
    label "Drezno"
  ]
  node [
    id 321
    label "Perejas&#322;aw"
  ]
  node [
    id 322
    label "Luksor"
  ]
  node [
    id 323
    label "Ostaszk&#243;w"
  ]
  node [
    id 324
    label "Gettysburg"
  ]
  node [
    id 325
    label "Trydent"
  ]
  node [
    id 326
    label "Poczdam"
  ]
  node [
    id 327
    label "Mesyna"
  ]
  node [
    id 328
    label "Krasnogorsk"
  ]
  node [
    id 329
    label "Kars"
  ]
  node [
    id 330
    label "Darmstadt"
  ]
  node [
    id 331
    label "Rzg&#243;w"
  ]
  node [
    id 332
    label "Kar&#322;owice"
  ]
  node [
    id 333
    label "Czeskie_Budziejowice"
  ]
  node [
    id 334
    label "Buda"
  ]
  node [
    id 335
    label "Monako"
  ]
  node [
    id 336
    label "Pardubice"
  ]
  node [
    id 337
    label "Pas&#322;&#281;k"
  ]
  node [
    id 338
    label "Fatima"
  ]
  node [
    id 339
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 340
    label "Bir&#380;e"
  ]
  node [
    id 341
    label "Wi&#322;komierz"
  ]
  node [
    id 342
    label "Opawa"
  ]
  node [
    id 343
    label "Mantua"
  ]
  node [
    id 344
    label "ulica"
  ]
  node [
    id 345
    label "Tarragona"
  ]
  node [
    id 346
    label "Antwerpia"
  ]
  node [
    id 347
    label "Asuan"
  ]
  node [
    id 348
    label "Korynt"
  ]
  node [
    id 349
    label "Armenia"
  ]
  node [
    id 350
    label "Budionnowsk"
  ]
  node [
    id 351
    label "Lengyel"
  ]
  node [
    id 352
    label "Betlejem"
  ]
  node [
    id 353
    label "Asy&#380;"
  ]
  node [
    id 354
    label "Batumi"
  ]
  node [
    id 355
    label "Paczk&#243;w"
  ]
  node [
    id 356
    label "Grenada"
  ]
  node [
    id 357
    label "Suczawa"
  ]
  node [
    id 358
    label "Nowogard"
  ]
  node [
    id 359
    label "Tyr"
  ]
  node [
    id 360
    label "Bria&#324;sk"
  ]
  node [
    id 361
    label "Bar"
  ]
  node [
    id 362
    label "Czerkiesk"
  ]
  node [
    id 363
    label "Ja&#322;ta"
  ]
  node [
    id 364
    label "Mo&#347;ciska"
  ]
  node [
    id 365
    label "Medyna"
  ]
  node [
    id 366
    label "Tartu"
  ]
  node [
    id 367
    label "Pemba"
  ]
  node [
    id 368
    label "Lipawa"
  ]
  node [
    id 369
    label "Tyl&#380;a"
  ]
  node [
    id 370
    label "Dayton"
  ]
  node [
    id 371
    label "Lipsk"
  ]
  node [
    id 372
    label "Rohatyn"
  ]
  node [
    id 373
    label "Peszawar"
  ]
  node [
    id 374
    label "Adrianopol"
  ]
  node [
    id 375
    label "Azow"
  ]
  node [
    id 376
    label "Iwano-Frankowsk"
  ]
  node [
    id 377
    label "Czarnobyl"
  ]
  node [
    id 378
    label "Rakoniewice"
  ]
  node [
    id 379
    label "Obuch&#243;w"
  ]
  node [
    id 380
    label "Orneta"
  ]
  node [
    id 381
    label "Koszyce"
  ]
  node [
    id 382
    label "Czeski_Cieszyn"
  ]
  node [
    id 383
    label "Zagorsk"
  ]
  node [
    id 384
    label "Nieder_Selters"
  ]
  node [
    id 385
    label "Ko&#322;omna"
  ]
  node [
    id 386
    label "Rost&#243;w"
  ]
  node [
    id 387
    label "Bolonia"
  ]
  node [
    id 388
    label "Rajgr&#243;d"
  ]
  node [
    id 389
    label "L&#252;neburg"
  ]
  node [
    id 390
    label "Brack"
  ]
  node [
    id 391
    label "Konstancja"
  ]
  node [
    id 392
    label "Koluszki"
  ]
  node [
    id 393
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 394
    label "Suez"
  ]
  node [
    id 395
    label "Mrocza"
  ]
  node [
    id 396
    label "Triest"
  ]
  node [
    id 397
    label "Murma&#324;sk"
  ]
  node [
    id 398
    label "Tu&#322;a"
  ]
  node [
    id 399
    label "Tarnogr&#243;d"
  ]
  node [
    id 400
    label "Radziech&#243;w"
  ]
  node [
    id 401
    label "Kokand"
  ]
  node [
    id 402
    label "Kircholm"
  ]
  node [
    id 403
    label "Nowa_Ruda"
  ]
  node [
    id 404
    label "Huma&#324;"
  ]
  node [
    id 405
    label "Turkiestan"
  ]
  node [
    id 406
    label "Kani&#243;w"
  ]
  node [
    id 407
    label "Pilzno"
  ]
  node [
    id 408
    label "Korfant&#243;w"
  ]
  node [
    id 409
    label "Dubno"
  ]
  node [
    id 410
    label "Bras&#322;aw"
  ]
  node [
    id 411
    label "Choroszcz"
  ]
  node [
    id 412
    label "Nowogr&#243;d"
  ]
  node [
    id 413
    label "Konotop"
  ]
  node [
    id 414
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 415
    label "Jastarnia"
  ]
  node [
    id 416
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 417
    label "Omsk"
  ]
  node [
    id 418
    label "Troick"
  ]
  node [
    id 419
    label "Koper"
  ]
  node [
    id 420
    label "Jenisejsk"
  ]
  node [
    id 421
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 422
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 423
    label "Trenczyn"
  ]
  node [
    id 424
    label "Wormacja"
  ]
  node [
    id 425
    label "Wagram"
  ]
  node [
    id 426
    label "Lubeka"
  ]
  node [
    id 427
    label "Genewa"
  ]
  node [
    id 428
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 429
    label "Kleck"
  ]
  node [
    id 430
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 431
    label "Struga"
  ]
  node [
    id 432
    label "Izbica_Kujawska"
  ]
  node [
    id 433
    label "Stalinogorsk"
  ]
  node [
    id 434
    label "Izmir"
  ]
  node [
    id 435
    label "Dortmund"
  ]
  node [
    id 436
    label "Workuta"
  ]
  node [
    id 437
    label "Jerycho"
  ]
  node [
    id 438
    label "Brunszwik"
  ]
  node [
    id 439
    label "Aleksandria"
  ]
  node [
    id 440
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 441
    label "Borys&#322;aw"
  ]
  node [
    id 442
    label "Zaleszczyki"
  ]
  node [
    id 443
    label "Z&#322;oczew"
  ]
  node [
    id 444
    label "Piast&#243;w"
  ]
  node [
    id 445
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 446
    label "Bor"
  ]
  node [
    id 447
    label "Nazaret"
  ]
  node [
    id 448
    label "Sarat&#243;w"
  ]
  node [
    id 449
    label "Brasz&#243;w"
  ]
  node [
    id 450
    label "Malin"
  ]
  node [
    id 451
    label "Parma"
  ]
  node [
    id 452
    label "Wierchoja&#324;sk"
  ]
  node [
    id 453
    label "Tarent"
  ]
  node [
    id 454
    label "Mariampol"
  ]
  node [
    id 455
    label "Wuhan"
  ]
  node [
    id 456
    label "Split"
  ]
  node [
    id 457
    label "Baranowicze"
  ]
  node [
    id 458
    label "Marki"
  ]
  node [
    id 459
    label "Adana"
  ]
  node [
    id 460
    label "B&#322;aszki"
  ]
  node [
    id 461
    label "Lubecz"
  ]
  node [
    id 462
    label "Sulech&#243;w"
  ]
  node [
    id 463
    label "Borys&#243;w"
  ]
  node [
    id 464
    label "Homel"
  ]
  node [
    id 465
    label "Tours"
  ]
  node [
    id 466
    label "Zaporo&#380;e"
  ]
  node [
    id 467
    label "Edam"
  ]
  node [
    id 468
    label "Kamieniec_Podolski"
  ]
  node [
    id 469
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 470
    label "Konstantynopol"
  ]
  node [
    id 471
    label "Chocim"
  ]
  node [
    id 472
    label "Mohylew"
  ]
  node [
    id 473
    label "Merseburg"
  ]
  node [
    id 474
    label "Kapsztad"
  ]
  node [
    id 475
    label "Sambor"
  ]
  node [
    id 476
    label "Manchester"
  ]
  node [
    id 477
    label "Pi&#324;sk"
  ]
  node [
    id 478
    label "Ochryda"
  ]
  node [
    id 479
    label "Rybi&#324;sk"
  ]
  node [
    id 480
    label "Czadca"
  ]
  node [
    id 481
    label "Orenburg"
  ]
  node [
    id 482
    label "Krajowa"
  ]
  node [
    id 483
    label "Eleusis"
  ]
  node [
    id 484
    label "Awinion"
  ]
  node [
    id 485
    label "Rzeczyca"
  ]
  node [
    id 486
    label "Lozanna"
  ]
  node [
    id 487
    label "Barczewo"
  ]
  node [
    id 488
    label "&#379;migr&#243;d"
  ]
  node [
    id 489
    label "Chabarowsk"
  ]
  node [
    id 490
    label "Jena"
  ]
  node [
    id 491
    label "Xai-Xai"
  ]
  node [
    id 492
    label "Radk&#243;w"
  ]
  node [
    id 493
    label "Syrakuzy"
  ]
  node [
    id 494
    label "Zas&#322;aw"
  ]
  node [
    id 495
    label "Windsor"
  ]
  node [
    id 496
    label "Getynga"
  ]
  node [
    id 497
    label "Carrara"
  ]
  node [
    id 498
    label "Madras"
  ]
  node [
    id 499
    label "Nitra"
  ]
  node [
    id 500
    label "Kilonia"
  ]
  node [
    id 501
    label "Rawenna"
  ]
  node [
    id 502
    label "Stawropol"
  ]
  node [
    id 503
    label "Warna"
  ]
  node [
    id 504
    label "Ba&#322;tijsk"
  ]
  node [
    id 505
    label "Cumana"
  ]
  node [
    id 506
    label "Kostroma"
  ]
  node [
    id 507
    label "Bajonna"
  ]
  node [
    id 508
    label "Magadan"
  ]
  node [
    id 509
    label "Kercz"
  ]
  node [
    id 510
    label "Harbin"
  ]
  node [
    id 511
    label "Sankt_Florian"
  ]
  node [
    id 512
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 513
    label "Wo&#322;kowysk"
  ]
  node [
    id 514
    label "Norak"
  ]
  node [
    id 515
    label "S&#232;vres"
  ]
  node [
    id 516
    label "Barwice"
  ]
  node [
    id 517
    label "Sumy"
  ]
  node [
    id 518
    label "Jutrosin"
  ]
  node [
    id 519
    label "Canterbury"
  ]
  node [
    id 520
    label "Czerkasy"
  ]
  node [
    id 521
    label "Troki"
  ]
  node [
    id 522
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 523
    label "Turka"
  ]
  node [
    id 524
    label "Budziszyn"
  ]
  node [
    id 525
    label "A&#322;czewsk"
  ]
  node [
    id 526
    label "Chark&#243;w"
  ]
  node [
    id 527
    label "Go&#347;cino"
  ]
  node [
    id 528
    label "Ku&#378;nieck"
  ]
  node [
    id 529
    label "Wotki&#324;sk"
  ]
  node [
    id 530
    label "Symferopol"
  ]
  node [
    id 531
    label "Dmitrow"
  ]
  node [
    id 532
    label "Cherso&#324;"
  ]
  node [
    id 533
    label "zabudowa"
  ]
  node [
    id 534
    label "Orlean"
  ]
  node [
    id 535
    label "Nowogr&#243;dek"
  ]
  node [
    id 536
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 537
    label "Berdia&#324;sk"
  ]
  node [
    id 538
    label "Szumsk"
  ]
  node [
    id 539
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 540
    label "Orsza"
  ]
  node [
    id 541
    label "Cluny"
  ]
  node [
    id 542
    label "Aralsk"
  ]
  node [
    id 543
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 544
    label "Bogumin"
  ]
  node [
    id 545
    label "Antiochia"
  ]
  node [
    id 546
    label "Inhambane"
  ]
  node [
    id 547
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 548
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 549
    label "Trewir"
  ]
  node [
    id 550
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 551
    label "Siewieromorsk"
  ]
  node [
    id 552
    label "Calais"
  ]
  node [
    id 553
    label "Twer"
  ]
  node [
    id 554
    label "&#379;ytawa"
  ]
  node [
    id 555
    label "Eupatoria"
  ]
  node [
    id 556
    label "Stara_Zagora"
  ]
  node [
    id 557
    label "Jastrowie"
  ]
  node [
    id 558
    label "Piatigorsk"
  ]
  node [
    id 559
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 560
    label "Le&#324;sk"
  ]
  node [
    id 561
    label "Johannesburg"
  ]
  node [
    id 562
    label "Kaszyn"
  ]
  node [
    id 563
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 564
    label "&#379;ylina"
  ]
  node [
    id 565
    label "Sewastopol"
  ]
  node [
    id 566
    label "Pietrozawodsk"
  ]
  node [
    id 567
    label "Bobolice"
  ]
  node [
    id 568
    label "Mosty"
  ]
  node [
    id 569
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 570
    label "Karaganda"
  ]
  node [
    id 571
    label "Marsylia"
  ]
  node [
    id 572
    label "Buchara"
  ]
  node [
    id 573
    label "Dubrownik"
  ]
  node [
    id 574
    label "Be&#322;z"
  ]
  node [
    id 575
    label "Oran"
  ]
  node [
    id 576
    label "Regensburg"
  ]
  node [
    id 577
    label "Rotterdam"
  ]
  node [
    id 578
    label "Trembowla"
  ]
  node [
    id 579
    label "Woskriesiensk"
  ]
  node [
    id 580
    label "Po&#322;ock"
  ]
  node [
    id 581
    label "Poprad"
  ]
  node [
    id 582
    label "Kronsztad"
  ]
  node [
    id 583
    label "Los_Angeles"
  ]
  node [
    id 584
    label "U&#322;an_Ude"
  ]
  node [
    id 585
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 586
    label "W&#322;adywostok"
  ]
  node [
    id 587
    label "Kandahar"
  ]
  node [
    id 588
    label "Tobolsk"
  ]
  node [
    id 589
    label "Boston"
  ]
  node [
    id 590
    label "Hawana"
  ]
  node [
    id 591
    label "Kis&#322;owodzk"
  ]
  node [
    id 592
    label "Tulon"
  ]
  node [
    id 593
    label "Utrecht"
  ]
  node [
    id 594
    label "Oleszyce"
  ]
  node [
    id 595
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 596
    label "Katania"
  ]
  node [
    id 597
    label "Teby"
  ]
  node [
    id 598
    label "Paw&#322;owo"
  ]
  node [
    id 599
    label "W&#252;rzburg"
  ]
  node [
    id 600
    label "Podiebrady"
  ]
  node [
    id 601
    label "Uppsala"
  ]
  node [
    id 602
    label "Poniewie&#380;"
  ]
  node [
    id 603
    label "Niko&#322;ajewsk"
  ]
  node [
    id 604
    label "Aczy&#324;sk"
  ]
  node [
    id 605
    label "Berezyna"
  ]
  node [
    id 606
    label "Ostr&#243;g"
  ]
  node [
    id 607
    label "Brze&#347;&#263;"
  ]
  node [
    id 608
    label "Lancaster"
  ]
  node [
    id 609
    label "Stryj"
  ]
  node [
    id 610
    label "Kozielsk"
  ]
  node [
    id 611
    label "Loreto"
  ]
  node [
    id 612
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 613
    label "Hebron"
  ]
  node [
    id 614
    label "Kaspijsk"
  ]
  node [
    id 615
    label "Peczora"
  ]
  node [
    id 616
    label "Isfahan"
  ]
  node [
    id 617
    label "Chimoio"
  ]
  node [
    id 618
    label "Mory&#324;"
  ]
  node [
    id 619
    label "Kowno"
  ]
  node [
    id 620
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 621
    label "Opalenica"
  ]
  node [
    id 622
    label "Kolonia"
  ]
  node [
    id 623
    label "Stary_Sambor"
  ]
  node [
    id 624
    label "Kolkata"
  ]
  node [
    id 625
    label "Turkmenbaszy"
  ]
  node [
    id 626
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 627
    label "Nankin"
  ]
  node [
    id 628
    label "Krzanowice"
  ]
  node [
    id 629
    label "Efez"
  ]
  node [
    id 630
    label "Dobrodzie&#324;"
  ]
  node [
    id 631
    label "Neapol"
  ]
  node [
    id 632
    label "S&#322;uck"
  ]
  node [
    id 633
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 634
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 635
    label "Frydek-Mistek"
  ]
  node [
    id 636
    label "Korsze"
  ]
  node [
    id 637
    label "T&#322;uszcz"
  ]
  node [
    id 638
    label "Soligorsk"
  ]
  node [
    id 639
    label "Kie&#380;mark"
  ]
  node [
    id 640
    label "Mannheim"
  ]
  node [
    id 641
    label "Ulm"
  ]
  node [
    id 642
    label "Podhajce"
  ]
  node [
    id 643
    label "Dniepropetrowsk"
  ]
  node [
    id 644
    label "Szamocin"
  ]
  node [
    id 645
    label "Ko&#322;omyja"
  ]
  node [
    id 646
    label "Buczacz"
  ]
  node [
    id 647
    label "M&#252;nster"
  ]
  node [
    id 648
    label "Brema"
  ]
  node [
    id 649
    label "Delhi"
  ]
  node [
    id 650
    label "&#346;niatyn"
  ]
  node [
    id 651
    label "Nicea"
  ]
  node [
    id 652
    label "Szawle"
  ]
  node [
    id 653
    label "Czerniowce"
  ]
  node [
    id 654
    label "Mi&#347;nia"
  ]
  node [
    id 655
    label "Sydney"
  ]
  node [
    id 656
    label "Moguncja"
  ]
  node [
    id 657
    label "Narbona"
  ]
  node [
    id 658
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 659
    label "Wittenberga"
  ]
  node [
    id 660
    label "Uljanowsk"
  ]
  node [
    id 661
    label "&#321;uga&#324;sk"
  ]
  node [
    id 662
    label "Wyborg"
  ]
  node [
    id 663
    label "Trojan"
  ]
  node [
    id 664
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 665
    label "Brandenburg"
  ]
  node [
    id 666
    label "Kemerowo"
  ]
  node [
    id 667
    label "Kaszgar"
  ]
  node [
    id 668
    label "Lenzen"
  ]
  node [
    id 669
    label "Nanning"
  ]
  node [
    id 670
    label "Gotha"
  ]
  node [
    id 671
    label "Zurych"
  ]
  node [
    id 672
    label "Baltimore"
  ]
  node [
    id 673
    label "&#321;uck"
  ]
  node [
    id 674
    label "Bristol"
  ]
  node [
    id 675
    label "Ferrara"
  ]
  node [
    id 676
    label "Mariupol"
  ]
  node [
    id 677
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 678
    label "Lhasa"
  ]
  node [
    id 679
    label "Czerniejewo"
  ]
  node [
    id 680
    label "Filadelfia"
  ]
  node [
    id 681
    label "Kanton"
  ]
  node [
    id 682
    label "Milan&#243;wek"
  ]
  node [
    id 683
    label "Perwomajsk"
  ]
  node [
    id 684
    label "Nieftiegorsk"
  ]
  node [
    id 685
    label "Pittsburgh"
  ]
  node [
    id 686
    label "Greifswald"
  ]
  node [
    id 687
    label "Akwileja"
  ]
  node [
    id 688
    label "Norfolk"
  ]
  node [
    id 689
    label "Perm"
  ]
  node [
    id 690
    label "Detroit"
  ]
  node [
    id 691
    label "Fergana"
  ]
  node [
    id 692
    label "Starobielsk"
  ]
  node [
    id 693
    label "Wielsk"
  ]
  node [
    id 694
    label "Zaklik&#243;w"
  ]
  node [
    id 695
    label "Majsur"
  ]
  node [
    id 696
    label "Narwa"
  ]
  node [
    id 697
    label "Chicago"
  ]
  node [
    id 698
    label "Byczyna"
  ]
  node [
    id 699
    label "Mozyrz"
  ]
  node [
    id 700
    label "Konstantyn&#243;wka"
  ]
  node [
    id 701
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 702
    label "Megara"
  ]
  node [
    id 703
    label "Stralsund"
  ]
  node [
    id 704
    label "Wo&#322;gograd"
  ]
  node [
    id 705
    label "Lichinga"
  ]
  node [
    id 706
    label "Haga"
  ]
  node [
    id 707
    label "Tarnopol"
  ]
  node [
    id 708
    label "K&#322;ajpeda"
  ]
  node [
    id 709
    label "Nowomoskowsk"
  ]
  node [
    id 710
    label "Ussuryjsk"
  ]
  node [
    id 711
    label "Brugia"
  ]
  node [
    id 712
    label "Natal"
  ]
  node [
    id 713
    label "Kro&#347;niewice"
  ]
  node [
    id 714
    label "Edynburg"
  ]
  node [
    id 715
    label "Marburg"
  ]
  node [
    id 716
    label "&#346;wiebodzice"
  ]
  node [
    id 717
    label "S&#322;onim"
  ]
  node [
    id 718
    label "Dalton"
  ]
  node [
    id 719
    label "Smorgonie"
  ]
  node [
    id 720
    label "Orze&#322;"
  ]
  node [
    id 721
    label "Nowoku&#378;nieck"
  ]
  node [
    id 722
    label "Zadar"
  ]
  node [
    id 723
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 724
    label "Koprzywnica"
  ]
  node [
    id 725
    label "Angarsk"
  ]
  node [
    id 726
    label "Mo&#380;ajsk"
  ]
  node [
    id 727
    label "Akwizgran"
  ]
  node [
    id 728
    label "Norylsk"
  ]
  node [
    id 729
    label "Jawor&#243;w"
  ]
  node [
    id 730
    label "weduta"
  ]
  node [
    id 731
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 732
    label "Suzdal"
  ]
  node [
    id 733
    label "W&#322;odzimierz"
  ]
  node [
    id 734
    label "Bujnaksk"
  ]
  node [
    id 735
    label "Beresteczko"
  ]
  node [
    id 736
    label "Strzelno"
  ]
  node [
    id 737
    label "Siewsk"
  ]
  node [
    id 738
    label "Cymlansk"
  ]
  node [
    id 739
    label "Trzyniec"
  ]
  node [
    id 740
    label "Hanower"
  ]
  node [
    id 741
    label "Wuppertal"
  ]
  node [
    id 742
    label "Sura&#380;"
  ]
  node [
    id 743
    label "Winchester"
  ]
  node [
    id 744
    label "Samara"
  ]
  node [
    id 745
    label "Sydon"
  ]
  node [
    id 746
    label "Krasnodar"
  ]
  node [
    id 747
    label "Worone&#380;"
  ]
  node [
    id 748
    label "Paw&#322;odar"
  ]
  node [
    id 749
    label "Czelabi&#324;sk"
  ]
  node [
    id 750
    label "Reda"
  ]
  node [
    id 751
    label "Karwina"
  ]
  node [
    id 752
    label "Wyszehrad"
  ]
  node [
    id 753
    label "Sara&#324;sk"
  ]
  node [
    id 754
    label "Koby&#322;ka"
  ]
  node [
    id 755
    label "Winnica"
  ]
  node [
    id 756
    label "Tambow"
  ]
  node [
    id 757
    label "Pyskowice"
  ]
  node [
    id 758
    label "Heidelberg"
  ]
  node [
    id 759
    label "Maribor"
  ]
  node [
    id 760
    label "Werona"
  ]
  node [
    id 761
    label "G&#322;uszyca"
  ]
  node [
    id 762
    label "Rostock"
  ]
  node [
    id 763
    label "Mekka"
  ]
  node [
    id 764
    label "Liberec"
  ]
  node [
    id 765
    label "Bie&#322;gorod"
  ]
  node [
    id 766
    label "Berdycz&#243;w"
  ]
  node [
    id 767
    label "Sierdobsk"
  ]
  node [
    id 768
    label "Bobrujsk"
  ]
  node [
    id 769
    label "Padwa"
  ]
  node [
    id 770
    label "Pasawa"
  ]
  node [
    id 771
    label "Chanty-Mansyjsk"
  ]
  node [
    id 772
    label "&#379;ar&#243;w"
  ]
  node [
    id 773
    label "Poczaj&#243;w"
  ]
  node [
    id 774
    label "Barabi&#324;sk"
  ]
  node [
    id 775
    label "Gorycja"
  ]
  node [
    id 776
    label "Haarlem"
  ]
  node [
    id 777
    label "Kiejdany"
  ]
  node [
    id 778
    label "Chmielnicki"
  ]
  node [
    id 779
    label "Magnitogorsk"
  ]
  node [
    id 780
    label "Burgas"
  ]
  node [
    id 781
    label "Siena"
  ]
  node [
    id 782
    label "Korzec"
  ]
  node [
    id 783
    label "Bonn"
  ]
  node [
    id 784
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 785
    label "Walencja"
  ]
  node [
    id 786
    label "Mosina"
  ]
  node [
    id 787
    label "Zgredek"
  ]
  node [
    id 788
    label "kategoria_gramatyczna"
  ]
  node [
    id 789
    label "Casanova"
  ]
  node [
    id 790
    label "Don_Juan"
  ]
  node [
    id 791
    label "Gargantua"
  ]
  node [
    id 792
    label "Faust"
  ]
  node [
    id 793
    label "profanum"
  ]
  node [
    id 794
    label "Chocho&#322;"
  ]
  node [
    id 795
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 796
    label "koniugacja"
  ]
  node [
    id 797
    label "Winnetou"
  ]
  node [
    id 798
    label "Dwukwiat"
  ]
  node [
    id 799
    label "homo_sapiens"
  ]
  node [
    id 800
    label "Edyp"
  ]
  node [
    id 801
    label "Herkules_Poirot"
  ]
  node [
    id 802
    label "ludzko&#347;&#263;"
  ]
  node [
    id 803
    label "mikrokosmos"
  ]
  node [
    id 804
    label "person"
  ]
  node [
    id 805
    label "Sherlock_Holmes"
  ]
  node [
    id 806
    label "portrecista"
  ]
  node [
    id 807
    label "Szwejk"
  ]
  node [
    id 808
    label "Hamlet"
  ]
  node [
    id 809
    label "duch"
  ]
  node [
    id 810
    label "g&#322;owa"
  ]
  node [
    id 811
    label "oddzia&#322;ywanie"
  ]
  node [
    id 812
    label "Quasimodo"
  ]
  node [
    id 813
    label "Dulcynea"
  ]
  node [
    id 814
    label "Don_Kiszot"
  ]
  node [
    id 815
    label "Wallenrod"
  ]
  node [
    id 816
    label "Plastu&#347;"
  ]
  node [
    id 817
    label "Harry_Potter"
  ]
  node [
    id 818
    label "figura"
  ]
  node [
    id 819
    label "parali&#380;owa&#263;"
  ]
  node [
    id 820
    label "istota"
  ]
  node [
    id 821
    label "Werter"
  ]
  node [
    id 822
    label "antropochoria"
  ]
  node [
    id 823
    label "posta&#263;"
  ]
  node [
    id 824
    label "w&#322;ady"
  ]
  node [
    id 825
    label "obecno&#347;&#263;"
  ]
  node [
    id 826
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 827
    label "kwota"
  ]
  node [
    id 828
    label "ilo&#347;&#263;"
  ]
  node [
    id 829
    label "demokracja_bezpo&#347;rednia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 88
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 88
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 97
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 23
    target 211
  ]
  edge [
    source 23
    target 212
  ]
  edge [
    source 23
    target 213
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 23
    target 218
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 227
  ]
  edge [
    source 23
    target 228
  ]
  edge [
    source 23
    target 229
  ]
  edge [
    source 23
    target 230
  ]
  edge [
    source 23
    target 231
  ]
  edge [
    source 23
    target 232
  ]
  edge [
    source 23
    target 233
  ]
  edge [
    source 23
    target 234
  ]
  edge [
    source 23
    target 235
  ]
  edge [
    source 23
    target 236
  ]
  edge [
    source 23
    target 237
  ]
  edge [
    source 23
    target 238
  ]
  edge [
    source 23
    target 239
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 243
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 23
    target 250
  ]
  edge [
    source 23
    target 251
  ]
  edge [
    source 23
    target 252
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 254
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 256
  ]
  edge [
    source 23
    target 257
  ]
  edge [
    source 23
    target 258
  ]
  edge [
    source 23
    target 259
  ]
  edge [
    source 23
    target 260
  ]
  edge [
    source 23
    target 261
  ]
  edge [
    source 23
    target 262
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 23
    target 281
  ]
  edge [
    source 23
    target 282
  ]
  edge [
    source 23
    target 170
  ]
  edge [
    source 23
    target 283
  ]
  edge [
    source 23
    target 284
  ]
  edge [
    source 23
    target 285
  ]
  edge [
    source 23
    target 286
  ]
  edge [
    source 23
    target 287
  ]
  edge [
    source 23
    target 288
  ]
  edge [
    source 23
    target 289
  ]
  edge [
    source 23
    target 290
  ]
  edge [
    source 23
    target 291
  ]
  edge [
    source 23
    target 292
  ]
  edge [
    source 23
    target 293
  ]
  edge [
    source 23
    target 294
  ]
  edge [
    source 23
    target 295
  ]
  edge [
    source 23
    target 296
  ]
  edge [
    source 23
    target 297
  ]
  edge [
    source 23
    target 298
  ]
  edge [
    source 23
    target 299
  ]
  edge [
    source 23
    target 300
  ]
  edge [
    source 23
    target 301
  ]
  edge [
    source 23
    target 302
  ]
  edge [
    source 23
    target 303
  ]
  edge [
    source 23
    target 304
  ]
  edge [
    source 23
    target 305
  ]
  edge [
    source 23
    target 306
  ]
  edge [
    source 23
    target 307
  ]
  edge [
    source 23
    target 308
  ]
  edge [
    source 23
    target 309
  ]
  edge [
    source 23
    target 310
  ]
  edge [
    source 23
    target 311
  ]
  edge [
    source 23
    target 312
  ]
  edge [
    source 23
    target 313
  ]
  edge [
    source 23
    target 314
  ]
  edge [
    source 23
    target 315
  ]
  edge [
    source 23
    target 316
  ]
  edge [
    source 23
    target 317
  ]
  edge [
    source 23
    target 318
  ]
  edge [
    source 23
    target 319
  ]
  edge [
    source 23
    target 320
  ]
  edge [
    source 23
    target 321
  ]
  edge [
    source 23
    target 322
  ]
  edge [
    source 23
    target 323
  ]
  edge [
    source 23
    target 324
  ]
  edge [
    source 23
    target 325
  ]
  edge [
    source 23
    target 326
  ]
  edge [
    source 23
    target 327
  ]
  edge [
    source 23
    target 328
  ]
  edge [
    source 23
    target 329
  ]
  edge [
    source 23
    target 330
  ]
  edge [
    source 23
    target 331
  ]
  edge [
    source 23
    target 332
  ]
  edge [
    source 23
    target 333
  ]
  edge [
    source 23
    target 334
  ]
  edge [
    source 23
    target 335
  ]
  edge [
    source 23
    target 336
  ]
  edge [
    source 23
    target 337
  ]
  edge [
    source 23
    target 338
  ]
  edge [
    source 23
    target 339
  ]
  edge [
    source 23
    target 340
  ]
  edge [
    source 23
    target 341
  ]
  edge [
    source 23
    target 342
  ]
  edge [
    source 23
    target 343
  ]
  edge [
    source 23
    target 344
  ]
  edge [
    source 23
    target 345
  ]
  edge [
    source 23
    target 346
  ]
  edge [
    source 23
    target 347
  ]
  edge [
    source 23
    target 348
  ]
  edge [
    source 23
    target 349
  ]
  edge [
    source 23
    target 350
  ]
  edge [
    source 23
    target 351
  ]
  edge [
    source 23
    target 352
  ]
  edge [
    source 23
    target 353
  ]
  edge [
    source 23
    target 354
  ]
  edge [
    source 23
    target 355
  ]
  edge [
    source 23
    target 356
  ]
  edge [
    source 23
    target 357
  ]
  edge [
    source 23
    target 358
  ]
  edge [
    source 23
    target 359
  ]
  edge [
    source 23
    target 360
  ]
  edge [
    source 23
    target 361
  ]
  edge [
    source 23
    target 362
  ]
  edge [
    source 23
    target 363
  ]
  edge [
    source 23
    target 364
  ]
  edge [
    source 23
    target 365
  ]
  edge [
    source 23
    target 366
  ]
  edge [
    source 23
    target 367
  ]
  edge [
    source 23
    target 368
  ]
  edge [
    source 23
    target 369
  ]
  edge [
    source 23
    target 370
  ]
  edge [
    source 23
    target 371
  ]
  edge [
    source 23
    target 372
  ]
  edge [
    source 23
    target 373
  ]
  edge [
    source 23
    target 374
  ]
  edge [
    source 23
    target 375
  ]
  edge [
    source 23
    target 376
  ]
  edge [
    source 23
    target 377
  ]
  edge [
    source 23
    target 378
  ]
  edge [
    source 23
    target 379
  ]
  edge [
    source 23
    target 380
  ]
  edge [
    source 23
    target 381
  ]
  edge [
    source 23
    target 382
  ]
  edge [
    source 23
    target 383
  ]
  edge [
    source 23
    target 384
  ]
  edge [
    source 23
    target 385
  ]
  edge [
    source 23
    target 386
  ]
  edge [
    source 23
    target 387
  ]
  edge [
    source 23
    target 388
  ]
  edge [
    source 23
    target 389
  ]
  edge [
    source 23
    target 390
  ]
  edge [
    source 23
    target 391
  ]
  edge [
    source 23
    target 392
  ]
  edge [
    source 23
    target 393
  ]
  edge [
    source 23
    target 394
  ]
  edge [
    source 23
    target 395
  ]
  edge [
    source 23
    target 396
  ]
  edge [
    source 23
    target 397
  ]
  edge [
    source 23
    target 398
  ]
  edge [
    source 23
    target 399
  ]
  edge [
    source 23
    target 400
  ]
  edge [
    source 23
    target 401
  ]
  edge [
    source 23
    target 402
  ]
  edge [
    source 23
    target 403
  ]
  edge [
    source 23
    target 404
  ]
  edge [
    source 23
    target 405
  ]
  edge [
    source 23
    target 406
  ]
  edge [
    source 23
    target 407
  ]
  edge [
    source 23
    target 408
  ]
  edge [
    source 23
    target 409
  ]
  edge [
    source 23
    target 410
  ]
  edge [
    source 23
    target 411
  ]
  edge [
    source 23
    target 412
  ]
  edge [
    source 23
    target 413
  ]
  edge [
    source 23
    target 414
  ]
  edge [
    source 23
    target 415
  ]
  edge [
    source 23
    target 416
  ]
  edge [
    source 23
    target 417
  ]
  edge [
    source 23
    target 418
  ]
  edge [
    source 23
    target 419
  ]
  edge [
    source 23
    target 420
  ]
  edge [
    source 23
    target 421
  ]
  edge [
    source 23
    target 422
  ]
  edge [
    source 23
    target 423
  ]
  edge [
    source 23
    target 424
  ]
  edge [
    source 23
    target 425
  ]
  edge [
    source 23
    target 426
  ]
  edge [
    source 23
    target 427
  ]
  edge [
    source 23
    target 428
  ]
  edge [
    source 23
    target 429
  ]
  edge [
    source 23
    target 430
  ]
  edge [
    source 23
    target 431
  ]
  edge [
    source 23
    target 432
  ]
  edge [
    source 23
    target 433
  ]
  edge [
    source 23
    target 434
  ]
  edge [
    source 23
    target 435
  ]
  edge [
    source 23
    target 436
  ]
  edge [
    source 23
    target 437
  ]
  edge [
    source 23
    target 438
  ]
  edge [
    source 23
    target 439
  ]
  edge [
    source 23
    target 440
  ]
  edge [
    source 23
    target 441
  ]
  edge [
    source 23
    target 442
  ]
  edge [
    source 23
    target 443
  ]
  edge [
    source 23
    target 444
  ]
  edge [
    source 23
    target 445
  ]
  edge [
    source 23
    target 446
  ]
  edge [
    source 23
    target 447
  ]
  edge [
    source 23
    target 448
  ]
  edge [
    source 23
    target 449
  ]
  edge [
    source 23
    target 450
  ]
  edge [
    source 23
    target 451
  ]
  edge [
    source 23
    target 452
  ]
  edge [
    source 23
    target 453
  ]
  edge [
    source 23
    target 454
  ]
  edge [
    source 23
    target 455
  ]
  edge [
    source 23
    target 456
  ]
  edge [
    source 23
    target 457
  ]
  edge [
    source 23
    target 458
  ]
  edge [
    source 23
    target 459
  ]
  edge [
    source 23
    target 460
  ]
  edge [
    source 23
    target 461
  ]
  edge [
    source 23
    target 462
  ]
  edge [
    source 23
    target 463
  ]
  edge [
    source 23
    target 464
  ]
  edge [
    source 23
    target 465
  ]
  edge [
    source 23
    target 466
  ]
  edge [
    source 23
    target 467
  ]
  edge [
    source 23
    target 468
  ]
  edge [
    source 23
    target 469
  ]
  edge [
    source 23
    target 470
  ]
  edge [
    source 23
    target 471
  ]
  edge [
    source 23
    target 472
  ]
  edge [
    source 23
    target 473
  ]
  edge [
    source 23
    target 474
  ]
  edge [
    source 23
    target 475
  ]
  edge [
    source 23
    target 476
  ]
  edge [
    source 23
    target 477
  ]
  edge [
    source 23
    target 478
  ]
  edge [
    source 23
    target 479
  ]
  edge [
    source 23
    target 480
  ]
  edge [
    source 23
    target 481
  ]
  edge [
    source 23
    target 482
  ]
  edge [
    source 23
    target 483
  ]
  edge [
    source 23
    target 484
  ]
  edge [
    source 23
    target 485
  ]
  edge [
    source 23
    target 486
  ]
  edge [
    source 23
    target 487
  ]
  edge [
    source 23
    target 488
  ]
  edge [
    source 23
    target 489
  ]
  edge [
    source 23
    target 490
  ]
  edge [
    source 23
    target 491
  ]
  edge [
    source 23
    target 492
  ]
  edge [
    source 23
    target 493
  ]
  edge [
    source 23
    target 494
  ]
  edge [
    source 23
    target 495
  ]
  edge [
    source 23
    target 496
  ]
  edge [
    source 23
    target 497
  ]
  edge [
    source 23
    target 498
  ]
  edge [
    source 23
    target 499
  ]
  edge [
    source 23
    target 500
  ]
  edge [
    source 23
    target 501
  ]
  edge [
    source 23
    target 502
  ]
  edge [
    source 23
    target 503
  ]
  edge [
    source 23
    target 504
  ]
  edge [
    source 23
    target 505
  ]
  edge [
    source 23
    target 506
  ]
  edge [
    source 23
    target 507
  ]
  edge [
    source 23
    target 508
  ]
  edge [
    source 23
    target 509
  ]
  edge [
    source 23
    target 510
  ]
  edge [
    source 23
    target 511
  ]
  edge [
    source 23
    target 512
  ]
  edge [
    source 23
    target 513
  ]
  edge [
    source 23
    target 514
  ]
  edge [
    source 23
    target 515
  ]
  edge [
    source 23
    target 516
  ]
  edge [
    source 23
    target 517
  ]
  edge [
    source 23
    target 518
  ]
  edge [
    source 23
    target 519
  ]
  edge [
    source 23
    target 520
  ]
  edge [
    source 23
    target 521
  ]
  edge [
    source 23
    target 522
  ]
  edge [
    source 23
    target 523
  ]
  edge [
    source 23
    target 524
  ]
  edge [
    source 23
    target 525
  ]
  edge [
    source 23
    target 526
  ]
  edge [
    source 23
    target 527
  ]
  edge [
    source 23
    target 528
  ]
  edge [
    source 23
    target 529
  ]
  edge [
    source 23
    target 530
  ]
  edge [
    source 23
    target 531
  ]
  edge [
    source 23
    target 532
  ]
  edge [
    source 23
    target 533
  ]
  edge [
    source 23
    target 534
  ]
  edge [
    source 23
    target 535
  ]
  edge [
    source 23
    target 536
  ]
  edge [
    source 23
    target 537
  ]
  edge [
    source 23
    target 538
  ]
  edge [
    source 23
    target 539
  ]
  edge [
    source 23
    target 540
  ]
  edge [
    source 23
    target 541
  ]
  edge [
    source 23
    target 542
  ]
  edge [
    source 23
    target 543
  ]
  edge [
    source 23
    target 544
  ]
  edge [
    source 23
    target 545
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 23
    target 546
  ]
  edge [
    source 23
    target 547
  ]
  edge [
    source 23
    target 548
  ]
  edge [
    source 23
    target 549
  ]
  edge [
    source 23
    target 550
  ]
  edge [
    source 23
    target 551
  ]
  edge [
    source 23
    target 552
  ]
  edge [
    source 23
    target 553
  ]
  edge [
    source 23
    target 554
  ]
  edge [
    source 23
    target 555
  ]
  edge [
    source 23
    target 556
  ]
  edge [
    source 23
    target 557
  ]
  edge [
    source 23
    target 558
  ]
  edge [
    source 23
    target 559
  ]
  edge [
    source 23
    target 560
  ]
  edge [
    source 23
    target 561
  ]
  edge [
    source 23
    target 562
  ]
  edge [
    source 23
    target 563
  ]
  edge [
    source 23
    target 564
  ]
  edge [
    source 23
    target 565
  ]
  edge [
    source 23
    target 566
  ]
  edge [
    source 23
    target 567
  ]
  edge [
    source 23
    target 568
  ]
  edge [
    source 23
    target 569
  ]
  edge [
    source 23
    target 570
  ]
  edge [
    source 23
    target 571
  ]
  edge [
    source 23
    target 572
  ]
  edge [
    source 23
    target 573
  ]
  edge [
    source 23
    target 574
  ]
  edge [
    source 23
    target 575
  ]
  edge [
    source 23
    target 576
  ]
  edge [
    source 23
    target 577
  ]
  edge [
    source 23
    target 578
  ]
  edge [
    source 23
    target 579
  ]
  edge [
    source 23
    target 580
  ]
  edge [
    source 23
    target 581
  ]
  edge [
    source 23
    target 582
  ]
  edge [
    source 23
    target 583
  ]
  edge [
    source 23
    target 584
  ]
  edge [
    source 23
    target 585
  ]
  edge [
    source 23
    target 586
  ]
  edge [
    source 23
    target 587
  ]
  edge [
    source 23
    target 588
  ]
  edge [
    source 23
    target 589
  ]
  edge [
    source 23
    target 590
  ]
  edge [
    source 23
    target 591
  ]
  edge [
    source 23
    target 592
  ]
  edge [
    source 23
    target 593
  ]
  edge [
    source 23
    target 594
  ]
  edge [
    source 23
    target 595
  ]
  edge [
    source 23
    target 596
  ]
  edge [
    source 23
    target 597
  ]
  edge [
    source 23
    target 598
  ]
  edge [
    source 23
    target 599
  ]
  edge [
    source 23
    target 600
  ]
  edge [
    source 23
    target 601
  ]
  edge [
    source 23
    target 602
  ]
  edge [
    source 23
    target 603
  ]
  edge [
    source 23
    target 604
  ]
  edge [
    source 23
    target 605
  ]
  edge [
    source 23
    target 606
  ]
  edge [
    source 23
    target 607
  ]
  edge [
    source 23
    target 608
  ]
  edge [
    source 23
    target 609
  ]
  edge [
    source 23
    target 610
  ]
  edge [
    source 23
    target 611
  ]
  edge [
    source 23
    target 612
  ]
  edge [
    source 23
    target 613
  ]
  edge [
    source 23
    target 614
  ]
  edge [
    source 23
    target 615
  ]
  edge [
    source 23
    target 616
  ]
  edge [
    source 23
    target 617
  ]
  edge [
    source 23
    target 618
  ]
  edge [
    source 23
    target 619
  ]
  edge [
    source 23
    target 620
  ]
  edge [
    source 23
    target 621
  ]
  edge [
    source 23
    target 622
  ]
  edge [
    source 23
    target 623
  ]
  edge [
    source 23
    target 624
  ]
  edge [
    source 23
    target 625
  ]
  edge [
    source 23
    target 626
  ]
  edge [
    source 23
    target 627
  ]
  edge [
    source 23
    target 628
  ]
  edge [
    source 23
    target 629
  ]
  edge [
    source 23
    target 630
  ]
  edge [
    source 23
    target 631
  ]
  edge [
    source 23
    target 632
  ]
  edge [
    source 23
    target 633
  ]
  edge [
    source 23
    target 634
  ]
  edge [
    source 23
    target 635
  ]
  edge [
    source 23
    target 636
  ]
  edge [
    source 23
    target 637
  ]
  edge [
    source 23
    target 638
  ]
  edge [
    source 23
    target 639
  ]
  edge [
    source 23
    target 640
  ]
  edge [
    source 23
    target 641
  ]
  edge [
    source 23
    target 642
  ]
  edge [
    source 23
    target 643
  ]
  edge [
    source 23
    target 644
  ]
  edge [
    source 23
    target 645
  ]
  edge [
    source 23
    target 646
  ]
  edge [
    source 23
    target 647
  ]
  edge [
    source 23
    target 648
  ]
  edge [
    source 23
    target 649
  ]
  edge [
    source 23
    target 650
  ]
  edge [
    source 23
    target 651
  ]
  edge [
    source 23
    target 652
  ]
  edge [
    source 23
    target 653
  ]
  edge [
    source 23
    target 654
  ]
  edge [
    source 23
    target 655
  ]
  edge [
    source 23
    target 656
  ]
  edge [
    source 23
    target 657
  ]
  edge [
    source 23
    target 658
  ]
  edge [
    source 23
    target 659
  ]
  edge [
    source 23
    target 660
  ]
  edge [
    source 23
    target 661
  ]
  edge [
    source 23
    target 662
  ]
  edge [
    source 23
    target 663
  ]
  edge [
    source 23
    target 664
  ]
  edge [
    source 23
    target 665
  ]
  edge [
    source 23
    target 666
  ]
  edge [
    source 23
    target 667
  ]
  edge [
    source 23
    target 668
  ]
  edge [
    source 23
    target 669
  ]
  edge [
    source 23
    target 670
  ]
  edge [
    source 23
    target 671
  ]
  edge [
    source 23
    target 672
  ]
  edge [
    source 23
    target 673
  ]
  edge [
    source 23
    target 674
  ]
  edge [
    source 23
    target 675
  ]
  edge [
    source 23
    target 676
  ]
  edge [
    source 23
    target 677
  ]
  edge [
    source 23
    target 678
  ]
  edge [
    source 23
    target 679
  ]
  edge [
    source 23
    target 680
  ]
  edge [
    source 23
    target 681
  ]
  edge [
    source 23
    target 682
  ]
  edge [
    source 23
    target 683
  ]
  edge [
    source 23
    target 684
  ]
  edge [
    source 23
    target 685
  ]
  edge [
    source 23
    target 686
  ]
  edge [
    source 23
    target 687
  ]
  edge [
    source 23
    target 688
  ]
  edge [
    source 23
    target 689
  ]
  edge [
    source 23
    target 690
  ]
  edge [
    source 23
    target 691
  ]
  edge [
    source 23
    target 692
  ]
  edge [
    source 23
    target 693
  ]
  edge [
    source 23
    target 694
  ]
  edge [
    source 23
    target 695
  ]
  edge [
    source 23
    target 696
  ]
  edge [
    source 23
    target 697
  ]
  edge [
    source 23
    target 698
  ]
  edge [
    source 23
    target 699
  ]
  edge [
    source 23
    target 700
  ]
  edge [
    source 23
    target 701
  ]
  edge [
    source 23
    target 702
  ]
  edge [
    source 23
    target 703
  ]
  edge [
    source 23
    target 704
  ]
  edge [
    source 23
    target 705
  ]
  edge [
    source 23
    target 706
  ]
  edge [
    source 23
    target 707
  ]
  edge [
    source 23
    target 708
  ]
  edge [
    source 23
    target 709
  ]
  edge [
    source 23
    target 710
  ]
  edge [
    source 23
    target 711
  ]
  edge [
    source 23
    target 712
  ]
  edge [
    source 23
    target 713
  ]
  edge [
    source 23
    target 714
  ]
  edge [
    source 23
    target 715
  ]
  edge [
    source 23
    target 716
  ]
  edge [
    source 23
    target 717
  ]
  edge [
    source 23
    target 718
  ]
  edge [
    source 23
    target 719
  ]
  edge [
    source 23
    target 720
  ]
  edge [
    source 23
    target 721
  ]
  edge [
    source 23
    target 722
  ]
  edge [
    source 23
    target 723
  ]
  edge [
    source 23
    target 724
  ]
  edge [
    source 23
    target 725
  ]
  edge [
    source 23
    target 726
  ]
  edge [
    source 23
    target 727
  ]
  edge [
    source 23
    target 728
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 730
  ]
  edge [
    source 23
    target 731
  ]
  edge [
    source 23
    target 732
  ]
  edge [
    source 23
    target 733
  ]
  edge [
    source 23
    target 734
  ]
  edge [
    source 23
    target 735
  ]
  edge [
    source 23
    target 736
  ]
  edge [
    source 23
    target 737
  ]
  edge [
    source 23
    target 738
  ]
  edge [
    source 23
    target 739
  ]
  edge [
    source 23
    target 740
  ]
  edge [
    source 23
    target 741
  ]
  edge [
    source 23
    target 742
  ]
  edge [
    source 23
    target 743
  ]
  edge [
    source 23
    target 744
  ]
  edge [
    source 23
    target 745
  ]
  edge [
    source 23
    target 746
  ]
  edge [
    source 23
    target 747
  ]
  edge [
    source 23
    target 748
  ]
  edge [
    source 23
    target 749
  ]
  edge [
    source 23
    target 750
  ]
  edge [
    source 23
    target 751
  ]
  edge [
    source 23
    target 752
  ]
  edge [
    source 23
    target 753
  ]
  edge [
    source 23
    target 754
  ]
  edge [
    source 23
    target 755
  ]
  edge [
    source 23
    target 756
  ]
  edge [
    source 23
    target 757
  ]
  edge [
    source 23
    target 758
  ]
  edge [
    source 23
    target 759
  ]
  edge [
    source 23
    target 760
  ]
  edge [
    source 23
    target 761
  ]
  edge [
    source 23
    target 762
  ]
  edge [
    source 23
    target 763
  ]
  edge [
    source 23
    target 764
  ]
  edge [
    source 23
    target 765
  ]
  edge [
    source 23
    target 766
  ]
  edge [
    source 23
    target 767
  ]
  edge [
    source 23
    target 768
  ]
  edge [
    source 23
    target 769
  ]
  edge [
    source 23
    target 770
  ]
  edge [
    source 23
    target 771
  ]
  edge [
    source 23
    target 772
  ]
  edge [
    source 23
    target 773
  ]
  edge [
    source 23
    target 774
  ]
  edge [
    source 23
    target 775
  ]
  edge [
    source 23
    target 776
  ]
  edge [
    source 23
    target 777
  ]
  edge [
    source 23
    target 778
  ]
  edge [
    source 23
    target 779
  ]
  edge [
    source 23
    target 780
  ]
  edge [
    source 23
    target 781
  ]
  edge [
    source 23
    target 782
  ]
  edge [
    source 23
    target 783
  ]
  edge [
    source 23
    target 784
  ]
  edge [
    source 23
    target 785
  ]
  edge [
    source 23
    target 786
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 787
  ]
  edge [
    source 25
    target 788
  ]
  edge [
    source 25
    target 789
  ]
  edge [
    source 25
    target 790
  ]
  edge [
    source 25
    target 791
  ]
  edge [
    source 25
    target 792
  ]
  edge [
    source 25
    target 793
  ]
  edge [
    source 25
    target 794
  ]
  edge [
    source 25
    target 795
  ]
  edge [
    source 25
    target 796
  ]
  edge [
    source 25
    target 797
  ]
  edge [
    source 25
    target 798
  ]
  edge [
    source 25
    target 799
  ]
  edge [
    source 25
    target 800
  ]
  edge [
    source 25
    target 801
  ]
  edge [
    source 25
    target 802
  ]
  edge [
    source 25
    target 803
  ]
  edge [
    source 25
    target 804
  ]
  edge [
    source 25
    target 805
  ]
  edge [
    source 25
    target 806
  ]
  edge [
    source 25
    target 807
  ]
  edge [
    source 25
    target 808
  ]
  edge [
    source 25
    target 809
  ]
  edge [
    source 25
    target 810
  ]
  edge [
    source 25
    target 811
  ]
  edge [
    source 25
    target 812
  ]
  edge [
    source 25
    target 813
  ]
  edge [
    source 25
    target 814
  ]
  edge [
    source 25
    target 815
  ]
  edge [
    source 25
    target 816
  ]
  edge [
    source 25
    target 817
  ]
  edge [
    source 25
    target 818
  ]
  edge [
    source 25
    target 819
  ]
  edge [
    source 25
    target 820
  ]
  edge [
    source 25
    target 821
  ]
  edge [
    source 25
    target 822
  ]
  edge [
    source 25
    target 823
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 824
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 825
  ]
  edge [
    source 27
    target 826
  ]
  edge [
    source 27
    target 827
  ]
  edge [
    source 27
    target 828
  ]
  edge [
    source 28
    target 141
  ]
  edge [
    source 28
    target 829
  ]
]
