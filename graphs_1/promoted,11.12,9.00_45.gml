graph [
  maxDegree 36
  minDegree 1
  meanDegree 1.9791666666666667
  density 0.020833333333333332
  graphCliqueNumber 2
  node [
    id 0
    label "chiny"
    origin "text"
  ]
  node [
    id 1
    label "urodzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "pierwsza"
    origin "text"
  ]
  node [
    id 4
    label "dziecko"
    origin "text"
  ]
  node [
    id 5
    label "modyfikowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "genetycznie"
    origin "text"
  ]
  node [
    id 7
    label "znaczy&#263;"
    origin "text"
  ]
  node [
    id 8
    label "strona"
    origin "text"
  ]
  node [
    id 9
    label "naukowy"
    origin "text"
  ]
  node [
    id 10
    label "bioetyczny"
    origin "text"
  ]
  node [
    id 11
    label "narodzi&#263;"
  ]
  node [
    id 12
    label "zlec"
  ]
  node [
    id 13
    label "engender"
  ]
  node [
    id 14
    label "powi&#263;"
  ]
  node [
    id 15
    label "zrobi&#263;"
  ]
  node [
    id 16
    label "porodzi&#263;"
  ]
  node [
    id 17
    label "godzina"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "potomstwo"
  ]
  node [
    id 20
    label "organizm"
  ]
  node [
    id 21
    label "sraluch"
  ]
  node [
    id 22
    label "utulanie"
  ]
  node [
    id 23
    label "pediatra"
  ]
  node [
    id 24
    label "dzieciarnia"
  ]
  node [
    id 25
    label "m&#322;odziak"
  ]
  node [
    id 26
    label "dzieciak"
  ]
  node [
    id 27
    label "utula&#263;"
  ]
  node [
    id 28
    label "potomek"
  ]
  node [
    id 29
    label "entliczek-pentliczek"
  ]
  node [
    id 30
    label "pedofil"
  ]
  node [
    id 31
    label "m&#322;odzik"
  ]
  node [
    id 32
    label "cz&#322;owieczek"
  ]
  node [
    id 33
    label "zwierz&#281;"
  ]
  node [
    id 34
    label "niepe&#322;noletni"
  ]
  node [
    id 35
    label "fledgling"
  ]
  node [
    id 36
    label "utuli&#263;"
  ]
  node [
    id 37
    label "utulenie"
  ]
  node [
    id 38
    label "ingerowa&#263;"
  ]
  node [
    id 39
    label "zmienia&#263;"
  ]
  node [
    id 40
    label "dziedzicznie"
  ]
  node [
    id 41
    label "genetyczny"
  ]
  node [
    id 42
    label "by&#263;"
  ]
  node [
    id 43
    label "spell"
  ]
  node [
    id 44
    label "odgrywa&#263;_rol&#281;"
  ]
  node [
    id 45
    label "zostawia&#263;"
  ]
  node [
    id 46
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 47
    label "represent"
  ]
  node [
    id 48
    label "count"
  ]
  node [
    id 49
    label "wyraz"
  ]
  node [
    id 50
    label "zdobi&#263;"
  ]
  node [
    id 51
    label "skr&#281;canie"
  ]
  node [
    id 52
    label "voice"
  ]
  node [
    id 53
    label "forma"
  ]
  node [
    id 54
    label "internet"
  ]
  node [
    id 55
    label "skr&#281;ci&#263;"
  ]
  node [
    id 56
    label "kartka"
  ]
  node [
    id 57
    label "orientowa&#263;"
  ]
  node [
    id 58
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 59
    label "powierzchnia"
  ]
  node [
    id 60
    label "plik"
  ]
  node [
    id 61
    label "bok"
  ]
  node [
    id 62
    label "pagina"
  ]
  node [
    id 63
    label "orientowanie"
  ]
  node [
    id 64
    label "fragment"
  ]
  node [
    id 65
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 66
    label "s&#261;d"
  ]
  node [
    id 67
    label "skr&#281;ca&#263;"
  ]
  node [
    id 68
    label "g&#243;ra"
  ]
  node [
    id 69
    label "serwis_internetowy"
  ]
  node [
    id 70
    label "orientacja"
  ]
  node [
    id 71
    label "linia"
  ]
  node [
    id 72
    label "skr&#281;cenie"
  ]
  node [
    id 73
    label "layout"
  ]
  node [
    id 74
    label "zorientowa&#263;"
  ]
  node [
    id 75
    label "zorientowanie"
  ]
  node [
    id 76
    label "obiekt"
  ]
  node [
    id 77
    label "podmiot"
  ]
  node [
    id 78
    label "ty&#322;"
  ]
  node [
    id 79
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 80
    label "logowanie"
  ]
  node [
    id 81
    label "adres_internetowy"
  ]
  node [
    id 82
    label "uj&#281;cie"
  ]
  node [
    id 83
    label "prz&#243;d"
  ]
  node [
    id 84
    label "posta&#263;"
  ]
  node [
    id 85
    label "specjalny"
  ]
  node [
    id 86
    label "edukacyjnie"
  ]
  node [
    id 87
    label "intelektualny"
  ]
  node [
    id 88
    label "skomplikowany"
  ]
  node [
    id 89
    label "zgodny"
  ]
  node [
    id 90
    label "naukowo"
  ]
  node [
    id 91
    label "scjentyficzny"
  ]
  node [
    id 92
    label "teoretyczny"
  ]
  node [
    id 93
    label "specjalistyczny"
  ]
  node [
    id 94
    label "moralny"
  ]
  node [
    id 95
    label "bioetycznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
]
