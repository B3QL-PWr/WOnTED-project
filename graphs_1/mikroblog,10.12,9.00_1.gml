graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.027972027972028
  density 0.01428149315473259
  graphCliqueNumber 3
  node [
    id 0
    label "dzienia"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "chyba"
    origin "text"
  ]
  node [
    id 3
    label "musza"
    origin "text"
  ]
  node [
    id 4
    label "t&#322;umaczy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "klasycznie"
    origin "text"
  ]
  node [
    id 6
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pod"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 9
    label "tag"
    origin "text"
  ]
  node [
    id 10
    label "pixelemirka"
    origin "text"
  ]
  node [
    id 11
    label "mi&#322;y"
    origin "text"
  ]
  node [
    id 12
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 13
    label "&#380;yczy&#263;"
    origin "text"
  ]
  node [
    id 14
    label "tworczoscwlasna"
    origin "text"
  ]
  node [
    id 15
    label "grafik"
    origin "text"
  ]
  node [
    id 16
    label "film"
    origin "text"
  ]
  node [
    id 17
    label "miodowelata"
    origin "text"
  ]
  node [
    id 18
    label "pomy&#347;lny"
  ]
  node [
    id 19
    label "skuteczny"
  ]
  node [
    id 20
    label "moralny"
  ]
  node [
    id 21
    label "korzystny"
  ]
  node [
    id 22
    label "odpowiedni"
  ]
  node [
    id 23
    label "zwrot"
  ]
  node [
    id 24
    label "dobrze"
  ]
  node [
    id 25
    label "pozytywny"
  ]
  node [
    id 26
    label "grzeczny"
  ]
  node [
    id 27
    label "powitanie"
  ]
  node [
    id 28
    label "dobroczynny"
  ]
  node [
    id 29
    label "pos&#322;uszny"
  ]
  node [
    id 30
    label "ca&#322;y"
  ]
  node [
    id 31
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 32
    label "czw&#243;rka"
  ]
  node [
    id 33
    label "spokojny"
  ]
  node [
    id 34
    label "&#347;mieszny"
  ]
  node [
    id 35
    label "drogi"
  ]
  node [
    id 36
    label "przek&#322;ada&#263;"
  ]
  node [
    id 37
    label "uzasadnia&#263;"
  ]
  node [
    id 38
    label "poja&#347;nia&#263;"
  ]
  node [
    id 39
    label "elaborate"
  ]
  node [
    id 40
    label "explain"
  ]
  node [
    id 41
    label "suplikowa&#263;"
  ]
  node [
    id 42
    label "j&#281;zyk"
  ]
  node [
    id 43
    label "sprawowa&#263;"
  ]
  node [
    id 44
    label "robi&#263;"
  ]
  node [
    id 45
    label "give"
  ]
  node [
    id 46
    label "przekonywa&#263;"
  ]
  node [
    id 47
    label "u&#322;atwia&#263;"
  ]
  node [
    id 48
    label "broni&#263;"
  ]
  node [
    id 49
    label "interpretowa&#263;"
  ]
  node [
    id 50
    label "przedstawia&#263;"
  ]
  node [
    id 51
    label "zwyczajnie"
  ]
  node [
    id 52
    label "klasyczny"
  ]
  node [
    id 53
    label "klasyczno"
  ]
  node [
    id 54
    label "typowo"
  ]
  node [
    id 55
    label "modelowo"
  ]
  node [
    id 56
    label "tradycyjnie"
  ]
  node [
    id 57
    label "normatywnie"
  ]
  node [
    id 58
    label "invite"
  ]
  node [
    id 59
    label "ask"
  ]
  node [
    id 60
    label "oferowa&#263;"
  ]
  node [
    id 61
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 62
    label "czyj&#347;"
  ]
  node [
    id 63
    label "m&#261;&#380;"
  ]
  node [
    id 64
    label "identyfikator"
  ]
  node [
    id 65
    label "napis"
  ]
  node [
    id 66
    label "nerd"
  ]
  node [
    id 67
    label "komnatowy"
  ]
  node [
    id 68
    label "sport_elektroniczny"
  ]
  node [
    id 69
    label "znacznik"
  ]
  node [
    id 70
    label "dyplomata"
  ]
  node [
    id 71
    label "kochanek"
  ]
  node [
    id 72
    label "wybranek"
  ]
  node [
    id 73
    label "mi&#322;o"
  ]
  node [
    id 74
    label "kochanie"
  ]
  node [
    id 75
    label "przyjemnie"
  ]
  node [
    id 76
    label "sk&#322;onny"
  ]
  node [
    id 77
    label "umi&#322;owany"
  ]
  node [
    id 78
    label "s&#322;o&#324;ce"
  ]
  node [
    id 79
    label "czynienie_si&#281;"
  ]
  node [
    id 80
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 81
    label "czas"
  ]
  node [
    id 82
    label "long_time"
  ]
  node [
    id 83
    label "przedpo&#322;udnie"
  ]
  node [
    id 84
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 85
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 86
    label "tydzie&#324;"
  ]
  node [
    id 87
    label "godzina"
  ]
  node [
    id 88
    label "t&#322;usty_czwartek"
  ]
  node [
    id 89
    label "wsta&#263;"
  ]
  node [
    id 90
    label "day"
  ]
  node [
    id 91
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 92
    label "przedwiecz&#243;r"
  ]
  node [
    id 93
    label "Sylwester"
  ]
  node [
    id 94
    label "po&#322;udnie"
  ]
  node [
    id 95
    label "wzej&#347;cie"
  ]
  node [
    id 96
    label "podwiecz&#243;r"
  ]
  node [
    id 97
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 98
    label "rano"
  ]
  node [
    id 99
    label "termin"
  ]
  node [
    id 100
    label "ranek"
  ]
  node [
    id 101
    label "doba"
  ]
  node [
    id 102
    label "wiecz&#243;r"
  ]
  node [
    id 103
    label "walentynki"
  ]
  node [
    id 104
    label "popo&#322;udnie"
  ]
  node [
    id 105
    label "noc"
  ]
  node [
    id 106
    label "wstanie"
  ]
  node [
    id 107
    label "preen"
  ]
  node [
    id 108
    label "sk&#322;ada&#263;"
  ]
  node [
    id 109
    label "plastyk"
  ]
  node [
    id 110
    label "informatyk"
  ]
  node [
    id 111
    label "rozk&#322;ad"
  ]
  node [
    id 112
    label "diagram"
  ]
  node [
    id 113
    label "rozbieg&#243;wka"
  ]
  node [
    id 114
    label "block"
  ]
  node [
    id 115
    label "odczula&#263;"
  ]
  node [
    id 116
    label "blik"
  ]
  node [
    id 117
    label "rola"
  ]
  node [
    id 118
    label "trawiarnia"
  ]
  node [
    id 119
    label "b&#322;ona"
  ]
  node [
    id 120
    label "filmoteka"
  ]
  node [
    id 121
    label "sztuka"
  ]
  node [
    id 122
    label "muza"
  ]
  node [
    id 123
    label "odczuli&#263;"
  ]
  node [
    id 124
    label "klatka"
  ]
  node [
    id 125
    label "odczulenie"
  ]
  node [
    id 126
    label "emulsja_fotograficzna"
  ]
  node [
    id 127
    label "animatronika"
  ]
  node [
    id 128
    label "dorobek"
  ]
  node [
    id 129
    label "odczulanie"
  ]
  node [
    id 130
    label "scena"
  ]
  node [
    id 131
    label "czo&#322;&#243;wka"
  ]
  node [
    id 132
    label "ty&#322;&#243;wka"
  ]
  node [
    id 133
    label "napisy"
  ]
  node [
    id 134
    label "photograph"
  ]
  node [
    id 135
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 136
    label "postprodukcja"
  ]
  node [
    id 137
    label "sklejarka"
  ]
  node [
    id 138
    label "anamorfoza"
  ]
  node [
    id 139
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 140
    label "ta&#347;ma"
  ]
  node [
    id 141
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 142
    label "uj&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
]
