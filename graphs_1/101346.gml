graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.6923076923076923
  density 0.14102564102564102
  graphCliqueNumber 2
  node [
    id 0
    label "micha&#322;"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "opolskie"
    origin "text"
  ]
  node [
    id 3
    label "przedmiot"
  ]
  node [
    id 4
    label "jednostka_administracyjna"
  ]
  node [
    id 5
    label "makroregion"
  ]
  node [
    id 6
    label "powiat"
  ]
  node [
    id 7
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 8
    label "mikroregion"
  ]
  node [
    id 9
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 10
    label "pa&#324;stwo"
  ]
  node [
    id 11
    label "kresy"
  ]
  node [
    id 12
    label "wschodni"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 11
    target 12
  ]
]
