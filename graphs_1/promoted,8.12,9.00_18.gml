graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.013333333333333
  density 0.013512304250559284
  graphCliqueNumber 3
  node [
    id 0
    label "naczelny"
    origin "text"
  ]
  node [
    id 1
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 2
    label "administracyjny"
    origin "text"
  ]
  node [
    id 3
    label "podtrzyma&#263;"
    origin "text"
  ]
  node [
    id 4
    label "uchyli&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ponad"
    origin "text"
  ]
  node [
    id 6
    label "zarz&#261;dzenie"
    origin "text"
  ]
  node [
    id 7
    label "wojewoda"
    origin "text"
  ]
  node [
    id 8
    label "zmiana"
    origin "text"
  ]
  node [
    id 9
    label "nazwa"
    origin "text"
  ]
  node [
    id 10
    label "ulica"
    origin "text"
  ]
  node [
    id 11
    label "warszawa"
    origin "text"
  ]
  node [
    id 12
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 13
    label "ustawa"
    origin "text"
  ]
  node [
    id 14
    label "dekomunizacyjny"
    origin "text"
  ]
  node [
    id 15
    label "nadrz&#281;dny"
  ]
  node [
    id 16
    label "jeneralny"
  ]
  node [
    id 17
    label "zawo&#322;any"
  ]
  node [
    id 18
    label "naczelnie"
  ]
  node [
    id 19
    label "g&#322;&#243;wny"
  ]
  node [
    id 20
    label "Michnik"
  ]
  node [
    id 21
    label "znany"
  ]
  node [
    id 22
    label "redaktor"
  ]
  node [
    id 23
    label "zwierzchnik"
  ]
  node [
    id 24
    label "procesowicz"
  ]
  node [
    id 25
    label "wypowied&#378;"
  ]
  node [
    id 26
    label "pods&#261;dny"
  ]
  node [
    id 27
    label "podejrzany"
  ]
  node [
    id 28
    label "broni&#263;"
  ]
  node [
    id 29
    label "bronienie"
  ]
  node [
    id 30
    label "system"
  ]
  node [
    id 31
    label "my&#347;l"
  ]
  node [
    id 32
    label "wytw&#243;r"
  ]
  node [
    id 33
    label "urz&#261;d"
  ]
  node [
    id 34
    label "konektyw"
  ]
  node [
    id 35
    label "court"
  ]
  node [
    id 36
    label "obrona"
  ]
  node [
    id 37
    label "s&#261;downictwo"
  ]
  node [
    id 38
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 39
    label "forum"
  ]
  node [
    id 40
    label "zesp&#243;&#322;"
  ]
  node [
    id 41
    label "post&#281;powanie"
  ]
  node [
    id 42
    label "skazany"
  ]
  node [
    id 43
    label "wydarzenie"
  ]
  node [
    id 44
    label "&#347;wiadek"
  ]
  node [
    id 45
    label "antylogizm"
  ]
  node [
    id 46
    label "strona"
  ]
  node [
    id 47
    label "oskar&#380;yciel"
  ]
  node [
    id 48
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 49
    label "biuro"
  ]
  node [
    id 50
    label "instytucja"
  ]
  node [
    id 51
    label "administracyjnie"
  ]
  node [
    id 52
    label "administrative"
  ]
  node [
    id 53
    label "prawny"
  ]
  node [
    id 54
    label "urz&#281;dowy"
  ]
  node [
    id 55
    label "spowodowa&#263;"
  ]
  node [
    id 56
    label "foster"
  ]
  node [
    id 57
    label "support"
  ]
  node [
    id 58
    label "pocieszy&#263;"
  ]
  node [
    id 59
    label "zrobi&#263;"
  ]
  node [
    id 60
    label "utrzyma&#263;"
  ]
  node [
    id 61
    label "unie&#347;&#263;"
  ]
  node [
    id 62
    label "rise"
  ]
  node [
    id 63
    label "zniwelowa&#263;"
  ]
  node [
    id 64
    label "przesun&#261;&#263;"
  ]
  node [
    id 65
    label "retract"
  ]
  node [
    id 66
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 67
    label "rule"
  ]
  node [
    id 68
    label "polecenie"
  ]
  node [
    id 69
    label "stipulation"
  ]
  node [
    id 70
    label "akt"
  ]
  node [
    id 71
    label "danie"
  ]
  node [
    id 72
    label "commission"
  ]
  node [
    id 73
    label "ordonans"
  ]
  node [
    id 74
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 75
    label "dostojnik"
  ]
  node [
    id 76
    label "urz&#281;dnik"
  ]
  node [
    id 77
    label "organ"
  ]
  node [
    id 78
    label "palatyn"
  ]
  node [
    id 79
    label "urz&#281;dnik_ziemski"
  ]
  node [
    id 80
    label "anatomopatolog"
  ]
  node [
    id 81
    label "rewizja"
  ]
  node [
    id 82
    label "oznaka"
  ]
  node [
    id 83
    label "czas"
  ]
  node [
    id 84
    label "ferment"
  ]
  node [
    id 85
    label "komplet"
  ]
  node [
    id 86
    label "tura"
  ]
  node [
    id 87
    label "amendment"
  ]
  node [
    id 88
    label "zmianka"
  ]
  node [
    id 89
    label "odmienianie"
  ]
  node [
    id 90
    label "passage"
  ]
  node [
    id 91
    label "zjawisko"
  ]
  node [
    id 92
    label "change"
  ]
  node [
    id 93
    label "praca"
  ]
  node [
    id 94
    label "term"
  ]
  node [
    id 95
    label "wezwanie"
  ]
  node [
    id 96
    label "leksem"
  ]
  node [
    id 97
    label "patron"
  ]
  node [
    id 98
    label "&#347;rodowisko"
  ]
  node [
    id 99
    label "miasteczko"
  ]
  node [
    id 100
    label "streetball"
  ]
  node [
    id 101
    label "pierzeja"
  ]
  node [
    id 102
    label "grupa"
  ]
  node [
    id 103
    label "pas_ruchu"
  ]
  node [
    id 104
    label "pas_rozdzielczy"
  ]
  node [
    id 105
    label "jezdnia"
  ]
  node [
    id 106
    label "droga"
  ]
  node [
    id 107
    label "korona_drogi"
  ]
  node [
    id 108
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 109
    label "chodnik"
  ]
  node [
    id 110
    label "arteria"
  ]
  node [
    id 111
    label "Broadway"
  ]
  node [
    id 112
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 113
    label "wysepka"
  ]
  node [
    id 114
    label "autostrada"
  ]
  node [
    id 115
    label "Warszawa"
  ]
  node [
    id 116
    label "samoch&#243;d"
  ]
  node [
    id 117
    label "fastback"
  ]
  node [
    id 118
    label "odwodnienie"
  ]
  node [
    id 119
    label "konstytucja"
  ]
  node [
    id 120
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 121
    label "substancja_chemiczna"
  ]
  node [
    id 122
    label "bratnia_dusza"
  ]
  node [
    id 123
    label "zwi&#261;zanie"
  ]
  node [
    id 124
    label "lokant"
  ]
  node [
    id 125
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 126
    label "zwi&#261;za&#263;"
  ]
  node [
    id 127
    label "organizacja"
  ]
  node [
    id 128
    label "odwadnia&#263;"
  ]
  node [
    id 129
    label "marriage"
  ]
  node [
    id 130
    label "marketing_afiliacyjny"
  ]
  node [
    id 131
    label "bearing"
  ]
  node [
    id 132
    label "wi&#261;zanie"
  ]
  node [
    id 133
    label "odwadnianie"
  ]
  node [
    id 134
    label "koligacja"
  ]
  node [
    id 135
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 136
    label "odwodni&#263;"
  ]
  node [
    id 137
    label "azeotrop"
  ]
  node [
    id 138
    label "powi&#261;zanie"
  ]
  node [
    id 139
    label "Karta_Nauczyciela"
  ]
  node [
    id 140
    label "marc&#243;wka"
  ]
  node [
    id 141
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 142
    label "przej&#347;&#263;"
  ]
  node [
    id 143
    label "charter"
  ]
  node [
    id 144
    label "przej&#347;cie"
  ]
  node [
    id 145
    label "AL"
  ]
  node [
    id 146
    label "armia"
  ]
  node [
    id 147
    label "ludowy"
  ]
  node [
    id 148
    label "Lech"
  ]
  node [
    id 149
    label "Kaczy&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 145
    target 146
  ]
  edge [
    source 145
    target 147
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 148
    target 149
  ]
]
