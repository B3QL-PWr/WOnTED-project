graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "o&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 1
    label "poselski"
    origin "text"
  ]
  node [
    id 2
    label "resolution"
  ]
  node [
    id 3
    label "wypowied&#378;"
  ]
  node [
    id 4
    label "poinformowanie"
  ]
  node [
    id 5
    label "announcement"
  ]
  node [
    id 6
    label "zwiastowanie"
  ]
  node [
    id 7
    label "komunikat"
  ]
  node [
    id 8
    label "statement"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
]
