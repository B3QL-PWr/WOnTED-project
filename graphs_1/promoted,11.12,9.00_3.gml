graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.95
  density 0.024683544303797468
  graphCliqueNumber 2
  node [
    id 0
    label "lato"
    origin "text"
  ]
  node [
    id 1
    label "sonda"
    origin "text"
  ]
  node [
    id 2
    label "voyager"
    origin "text"
  ]
  node [
    id 3
    label "opu&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 4
    label "heliosfera"
    origin "text"
  ]
  node [
    id 5
    label "dotrze&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przestrze&#324;"
    origin "text"
  ]
  node [
    id 7
    label "mi&#281;dzygwiezdny"
    origin "text"
  ]
  node [
    id 8
    label "tak"
    origin "text"
  ]
  node [
    id 9
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pora_roku"
  ]
  node [
    id 11
    label "sonda&#380;"
  ]
  node [
    id 12
    label "przyrz&#261;d"
  ]
  node [
    id 13
    label "leash"
  ]
  node [
    id 14
    label "badanie"
  ]
  node [
    id 15
    label "narz&#281;dzie"
  ]
  node [
    id 16
    label "lead"
  ]
  node [
    id 17
    label "statek_kosmiczny"
  ]
  node [
    id 18
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 19
    label "omin&#261;&#263;"
  ]
  node [
    id 20
    label "humiliate"
  ]
  node [
    id 21
    label "pozostawi&#263;"
  ]
  node [
    id 22
    label "potani&#263;"
  ]
  node [
    id 23
    label "obni&#380;y&#263;"
  ]
  node [
    id 24
    label "evacuate"
  ]
  node [
    id 25
    label "authorize"
  ]
  node [
    id 26
    label "leave"
  ]
  node [
    id 27
    label "przesta&#263;"
  ]
  node [
    id 28
    label "straci&#263;"
  ]
  node [
    id 29
    label "zostawi&#263;"
  ]
  node [
    id 30
    label "drop"
  ]
  node [
    id 31
    label "tekst"
  ]
  node [
    id 32
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 33
    label "strefa"
  ]
  node [
    id 34
    label "S&#322;o&#324;ce"
  ]
  node [
    id 35
    label "heliopauza"
  ]
  node [
    id 36
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 37
    label "get"
  ]
  node [
    id 38
    label "utrze&#263;"
  ]
  node [
    id 39
    label "spowodowa&#263;"
  ]
  node [
    id 40
    label "catch"
  ]
  node [
    id 41
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 42
    label "become"
  ]
  node [
    id 43
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 44
    label "znale&#378;&#263;"
  ]
  node [
    id 45
    label "dorobi&#263;"
  ]
  node [
    id 46
    label "advance"
  ]
  node [
    id 47
    label "dopasowa&#263;"
  ]
  node [
    id 48
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 49
    label "silnik"
  ]
  node [
    id 50
    label "oktant"
  ]
  node [
    id 51
    label "niezmierzony"
  ]
  node [
    id 52
    label "miejsce"
  ]
  node [
    id 53
    label "bezbrze&#380;e"
  ]
  node [
    id 54
    label "przedzieli&#263;"
  ]
  node [
    id 55
    label "rozdzielanie"
  ]
  node [
    id 56
    label "rozdziela&#263;"
  ]
  node [
    id 57
    label "zbi&#243;r"
  ]
  node [
    id 58
    label "punkt"
  ]
  node [
    id 59
    label "przestw&#243;r"
  ]
  node [
    id 60
    label "przedzielenie"
  ]
  node [
    id 61
    label "nielito&#347;ciwy"
  ]
  node [
    id 62
    label "czasoprzestrze&#324;"
  ]
  node [
    id 63
    label "zorganizowa&#263;"
  ]
  node [
    id 64
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 65
    label "przerobi&#263;"
  ]
  node [
    id 66
    label "wystylizowa&#263;"
  ]
  node [
    id 67
    label "cause"
  ]
  node [
    id 68
    label "wydali&#263;"
  ]
  node [
    id 69
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 70
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 71
    label "post&#261;pi&#263;"
  ]
  node [
    id 72
    label "appoint"
  ]
  node [
    id 73
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 74
    label "nabra&#263;"
  ]
  node [
    id 75
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 76
    label "make"
  ]
  node [
    id 77
    label "Voyager"
  ]
  node [
    id 78
    label "2"
  ]
  node [
    id 79
    label "1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 79
  ]
]
