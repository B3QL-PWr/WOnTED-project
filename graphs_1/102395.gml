graph [
  maxDegree 25
  minDegree 1
  meanDegree 2.1052631578947367
  density 0.01238390092879257
  graphCliqueNumber 3
  node [
    id 0
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 1
    label "opisa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "taka"
    origin "text"
  ]
  node [
    id 3
    label "przer&#243;bka"
    origin "text"
  ]
  node [
    id 4
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "jak"
    origin "text"
  ]
  node [
    id 6
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "prosto"
    origin "text"
  ]
  node [
    id 8
    label "spr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "po&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 10
    label "nacisk"
    origin "text"
  ]
  node [
    id 11
    label "wykona&#263;"
    origin "text"
  ]
  node [
    id 12
    label "taki"
    origin "text"
  ]
  node [
    id 13
    label "zasilacz"
    origin "text"
  ]
  node [
    id 14
    label "rozumie&#263;"
    origin "text"
  ]
  node [
    id 15
    label "si&#281;"
    origin "text"
  ]
  node [
    id 16
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 17
    label "przy"
    origin "text"
  ]
  node [
    id 18
    label "okazja"
    origin "text"
  ]
  node [
    id 19
    label "wykorzysta&#263;"
    origin "text"
  ]
  node [
    id 20
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 21
    label "funkcja"
    origin "text"
  ]
  node [
    id 22
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 23
    label "oferowa&#263;"
    origin "text"
  ]
  node [
    id 24
    label "zinterpretowa&#263;"
  ]
  node [
    id 25
    label "relate"
  ]
  node [
    id 26
    label "zapozna&#263;"
  ]
  node [
    id 27
    label "delineate"
  ]
  node [
    id 28
    label "Bangladesz"
  ]
  node [
    id 29
    label "jednostka_monetarna"
  ]
  node [
    id 30
    label "zamiana"
  ]
  node [
    id 31
    label "odmiana"
  ]
  node [
    id 32
    label "alteration"
  ]
  node [
    id 33
    label "zmiana"
  ]
  node [
    id 34
    label "produkcja"
  ]
  node [
    id 35
    label "zapoznawa&#263;"
  ]
  node [
    id 36
    label "represent"
  ]
  node [
    id 37
    label "byd&#322;o"
  ]
  node [
    id 38
    label "zobo"
  ]
  node [
    id 39
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 40
    label "yakalo"
  ]
  node [
    id 41
    label "dzo"
  ]
  node [
    id 42
    label "zorganizowa&#263;"
  ]
  node [
    id 43
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 44
    label "przerobi&#263;"
  ]
  node [
    id 45
    label "wystylizowa&#263;"
  ]
  node [
    id 46
    label "cause"
  ]
  node [
    id 47
    label "wydali&#263;"
  ]
  node [
    id 48
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 49
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 50
    label "post&#261;pi&#263;"
  ]
  node [
    id 51
    label "appoint"
  ]
  node [
    id 52
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 53
    label "nabra&#263;"
  ]
  node [
    id 54
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 55
    label "make"
  ]
  node [
    id 56
    label "elementarily"
  ]
  node [
    id 57
    label "bezpo&#347;rednio"
  ]
  node [
    id 58
    label "naturalnie"
  ]
  node [
    id 59
    label "skromnie"
  ]
  node [
    id 60
    label "prosty"
  ]
  node [
    id 61
    label "&#322;atwo"
  ]
  node [
    id 62
    label "niepozornie"
  ]
  node [
    id 63
    label "sprawdzi&#263;"
  ]
  node [
    id 64
    label "spo&#380;y&#263;"
  ]
  node [
    id 65
    label "pos&#322;u&#380;y&#263;_si&#281;"
  ]
  node [
    id 66
    label "try"
  ]
  node [
    id 67
    label "zakosztowa&#263;"
  ]
  node [
    id 68
    label "podj&#261;&#263;"
  ]
  node [
    id 69
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 70
    label "przygotowa&#263;"
  ]
  node [
    id 71
    label "zmieni&#263;"
  ]
  node [
    id 72
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 73
    label "pokry&#263;"
  ]
  node [
    id 74
    label "pozostawi&#263;"
  ]
  node [
    id 75
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 76
    label "zacz&#261;&#263;"
  ]
  node [
    id 77
    label "znak"
  ]
  node [
    id 78
    label "return"
  ]
  node [
    id 79
    label "stagger"
  ]
  node [
    id 80
    label "raise"
  ]
  node [
    id 81
    label "plant"
  ]
  node [
    id 82
    label "umie&#347;ci&#263;"
  ]
  node [
    id 83
    label "wear"
  ]
  node [
    id 84
    label "zepsu&#263;"
  ]
  node [
    id 85
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 86
    label "wygra&#263;"
  ]
  node [
    id 87
    label "zjawisko"
  ]
  node [
    id 88
    label "force"
  ]
  node [
    id 89
    label "uwaga"
  ]
  node [
    id 90
    label "wp&#322;yw"
  ]
  node [
    id 91
    label "wytworzy&#263;"
  ]
  node [
    id 92
    label "manufacture"
  ]
  node [
    id 93
    label "picture"
  ]
  node [
    id 94
    label "okre&#347;lony"
  ]
  node [
    id 95
    label "jaki&#347;"
  ]
  node [
    id 96
    label "p&#281;tla_ociekowa"
  ]
  node [
    id 97
    label "urz&#261;dzenie"
  ]
  node [
    id 98
    label "wiedzie&#263;"
  ]
  node [
    id 99
    label "zna&#263;"
  ]
  node [
    id 100
    label "czu&#263;"
  ]
  node [
    id 101
    label "j&#281;zyk"
  ]
  node [
    id 102
    label "kuma&#263;"
  ]
  node [
    id 103
    label "give"
  ]
  node [
    id 104
    label "odbiera&#263;"
  ]
  node [
    id 105
    label "empatia"
  ]
  node [
    id 106
    label "see"
  ]
  node [
    id 107
    label "match"
  ]
  node [
    id 108
    label "dziama&#263;"
  ]
  node [
    id 109
    label "tentegowa&#263;"
  ]
  node [
    id 110
    label "urz&#261;dza&#263;"
  ]
  node [
    id 111
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 112
    label "czyni&#263;"
  ]
  node [
    id 113
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 114
    label "post&#281;powa&#263;"
  ]
  node [
    id 115
    label "wydala&#263;"
  ]
  node [
    id 116
    label "oszukiwa&#263;"
  ]
  node [
    id 117
    label "organizowa&#263;"
  ]
  node [
    id 118
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 119
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 120
    label "work"
  ]
  node [
    id 121
    label "przerabia&#263;"
  ]
  node [
    id 122
    label "stylizowa&#263;"
  ]
  node [
    id 123
    label "falowa&#263;"
  ]
  node [
    id 124
    label "act"
  ]
  node [
    id 125
    label "peddle"
  ]
  node [
    id 126
    label "ukazywa&#263;"
  ]
  node [
    id 127
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 128
    label "praca"
  ]
  node [
    id 129
    label "atrakcyjny"
  ]
  node [
    id 130
    label "oferta"
  ]
  node [
    id 131
    label "adeptness"
  ]
  node [
    id 132
    label "okazka"
  ]
  node [
    id 133
    label "wydarzenie"
  ]
  node [
    id 134
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 135
    label "podw&#243;zka"
  ]
  node [
    id 136
    label "autostop"
  ]
  node [
    id 137
    label "sytuacja"
  ]
  node [
    id 138
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 139
    label "u&#380;y&#263;"
  ]
  node [
    id 140
    label "seize"
  ]
  node [
    id 141
    label "skorzysta&#263;"
  ]
  node [
    id 142
    label "du&#380;y"
  ]
  node [
    id 143
    label "cz&#281;sto"
  ]
  node [
    id 144
    label "bardzo"
  ]
  node [
    id 145
    label "mocno"
  ]
  node [
    id 146
    label "wiela"
  ]
  node [
    id 147
    label "infimum"
  ]
  node [
    id 148
    label "znaczenie"
  ]
  node [
    id 149
    label "awansowanie"
  ]
  node [
    id 150
    label "zastosowanie"
  ]
  node [
    id 151
    label "function"
  ]
  node [
    id 152
    label "funkcjonowanie"
  ]
  node [
    id 153
    label "cel"
  ]
  node [
    id 154
    label "supremum"
  ]
  node [
    id 155
    label "powierzanie"
  ]
  node [
    id 156
    label "rzut"
  ]
  node [
    id 157
    label "addytywno&#347;&#263;"
  ]
  node [
    id 158
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 159
    label "wakowa&#263;"
  ]
  node [
    id 160
    label "dziedzina"
  ]
  node [
    id 161
    label "postawi&#263;"
  ]
  node [
    id 162
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 163
    label "czyn"
  ]
  node [
    id 164
    label "przeciwdziedzina"
  ]
  node [
    id 165
    label "matematyka"
  ]
  node [
    id 166
    label "awansowa&#263;"
  ]
  node [
    id 167
    label "stawia&#263;"
  ]
  node [
    id 168
    label "jednostka"
  ]
  node [
    id 169
    label "volunteer"
  ]
  node [
    id 170
    label "zach&#281;ca&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 147
  ]
  edge [
    source 21
    target 148
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 21
    target 150
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 21
    target 161
  ]
  edge [
    source 21
    target 162
  ]
  edge [
    source 21
    target 163
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 23
    target 169
  ]
  edge [
    source 23
    target 170
  ]
]
