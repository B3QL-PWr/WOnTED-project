graph [
  maxDegree 48
  minDegree 1
  meanDegree 2.122448979591837
  density 0.01453732177802628
  graphCliqueNumber 3
  node [
    id 0
    label "g&#322;&#243;wnia"
    origin "text"
  ]
  node [
    id 1
    label "pe&#322;nomocnik"
    origin "text"
  ]
  node [
    id 2
    label "graniczny"
    origin "text"
  ]
  node [
    id 3
    label "zast&#281;pca"
    origin "text"
  ]
  node [
    id 4
    label "pomocnica"
    origin "text"
  ]
  node [
    id 5
    label "cela"
    origin "text"
  ]
  node [
    id 6
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "swoje"
    origin "text"
  ]
  node [
    id 8
    label "funkcja"
    origin "text"
  ]
  node [
    id 9
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pisemny"
    origin "text"
  ]
  node [
    id 11
    label "pe&#322;nomocnictwo"
    origin "text"
  ]
  node [
    id 12
    label "sporz&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 14
    label "polski"
    origin "text"
  ]
  node [
    id 15
    label "litewski"
    origin "text"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "substytuowanie"
  ]
  node [
    id 18
    label "substytuowa&#263;"
  ]
  node [
    id 19
    label "przyleg&#322;y"
  ]
  node [
    id 20
    label "skrajny"
  ]
  node [
    id 21
    label "wa&#380;ny"
  ]
  node [
    id 22
    label "granicznie"
  ]
  node [
    id 23
    label "ostateczny"
  ]
  node [
    id 24
    label "pomieszczenie"
  ]
  node [
    id 25
    label "klasztor"
  ]
  node [
    id 26
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 27
    label "work"
  ]
  node [
    id 28
    label "robi&#263;"
  ]
  node [
    id 29
    label "muzyka"
  ]
  node [
    id 30
    label "rola"
  ]
  node [
    id 31
    label "create"
  ]
  node [
    id 32
    label "wytwarza&#263;"
  ]
  node [
    id 33
    label "praca"
  ]
  node [
    id 34
    label "infimum"
  ]
  node [
    id 35
    label "znaczenie"
  ]
  node [
    id 36
    label "awansowanie"
  ]
  node [
    id 37
    label "zastosowanie"
  ]
  node [
    id 38
    label "function"
  ]
  node [
    id 39
    label "funkcjonowanie"
  ]
  node [
    id 40
    label "cel"
  ]
  node [
    id 41
    label "supremum"
  ]
  node [
    id 42
    label "powierzanie"
  ]
  node [
    id 43
    label "rzut"
  ]
  node [
    id 44
    label "addytywno&#347;&#263;"
  ]
  node [
    id 45
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 46
    label "wakowa&#263;"
  ]
  node [
    id 47
    label "dziedzina"
  ]
  node [
    id 48
    label "postawi&#263;"
  ]
  node [
    id 49
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 50
    label "czyn"
  ]
  node [
    id 51
    label "przeciwdziedzina"
  ]
  node [
    id 52
    label "matematyka"
  ]
  node [
    id 53
    label "awansowa&#263;"
  ]
  node [
    id 54
    label "stawia&#263;"
  ]
  node [
    id 55
    label "jednostka"
  ]
  node [
    id 56
    label "take"
  ]
  node [
    id 57
    label "dostawa&#263;"
  ]
  node [
    id 58
    label "return"
  ]
  node [
    id 59
    label "pi&#347;mienny"
  ]
  node [
    id 60
    label "pisemnie"
  ]
  node [
    id 61
    label "power_of_attorney"
  ]
  node [
    id 62
    label "authority"
  ]
  node [
    id 63
    label "umocowa&#263;"
  ]
  node [
    id 64
    label "prawo"
  ]
  node [
    id 65
    label "za&#347;wiadczenie"
  ]
  node [
    id 66
    label "przygotowa&#263;"
  ]
  node [
    id 67
    label "stworzy&#263;"
  ]
  node [
    id 68
    label "wytworzy&#263;"
  ]
  node [
    id 69
    label "opracowa&#263;"
  ]
  node [
    id 70
    label "draw"
  ]
  node [
    id 71
    label "oprawi&#263;"
  ]
  node [
    id 72
    label "zrobi&#263;"
  ]
  node [
    id 73
    label "podzieli&#263;"
  ]
  node [
    id 74
    label "pisa&#263;"
  ]
  node [
    id 75
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 76
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 77
    label "ssanie"
  ]
  node [
    id 78
    label "po_koroniarsku"
  ]
  node [
    id 79
    label "przedmiot"
  ]
  node [
    id 80
    label "but"
  ]
  node [
    id 81
    label "m&#243;wienie"
  ]
  node [
    id 82
    label "rozumie&#263;"
  ]
  node [
    id 83
    label "formacja_geologiczna"
  ]
  node [
    id 84
    label "rozumienie"
  ]
  node [
    id 85
    label "m&#243;wi&#263;"
  ]
  node [
    id 86
    label "gramatyka"
  ]
  node [
    id 87
    label "pype&#263;"
  ]
  node [
    id 88
    label "makroglosja"
  ]
  node [
    id 89
    label "kawa&#322;ek"
  ]
  node [
    id 90
    label "artykulator"
  ]
  node [
    id 91
    label "kultura_duchowa"
  ]
  node [
    id 92
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 93
    label "jama_ustna"
  ]
  node [
    id 94
    label "spos&#243;b"
  ]
  node [
    id 95
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 96
    label "przet&#322;umaczenie"
  ]
  node [
    id 97
    label "t&#322;umaczenie"
  ]
  node [
    id 98
    label "language"
  ]
  node [
    id 99
    label "jeniec"
  ]
  node [
    id 100
    label "organ"
  ]
  node [
    id 101
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 102
    label "pismo"
  ]
  node [
    id 103
    label "formalizowanie"
  ]
  node [
    id 104
    label "fonetyka"
  ]
  node [
    id 105
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 106
    label "wokalizm"
  ]
  node [
    id 107
    label "liza&#263;"
  ]
  node [
    id 108
    label "s&#322;ownictwo"
  ]
  node [
    id 109
    label "napisa&#263;"
  ]
  node [
    id 110
    label "formalizowa&#263;"
  ]
  node [
    id 111
    label "natural_language"
  ]
  node [
    id 112
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 113
    label "stylik"
  ]
  node [
    id 114
    label "konsonantyzm"
  ]
  node [
    id 115
    label "urz&#261;dzenie"
  ]
  node [
    id 116
    label "ssa&#263;"
  ]
  node [
    id 117
    label "kod"
  ]
  node [
    id 118
    label "lizanie"
  ]
  node [
    id 119
    label "lacki"
  ]
  node [
    id 120
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 121
    label "sztajer"
  ]
  node [
    id 122
    label "drabant"
  ]
  node [
    id 123
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 124
    label "polak"
  ]
  node [
    id 125
    label "pierogi_ruskie"
  ]
  node [
    id 126
    label "krakowiak"
  ]
  node [
    id 127
    label "Polish"
  ]
  node [
    id 128
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 129
    label "oberek"
  ]
  node [
    id 130
    label "po_polsku"
  ]
  node [
    id 131
    label "mazur"
  ]
  node [
    id 132
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 133
    label "chodzony"
  ]
  node [
    id 134
    label "skoczny"
  ]
  node [
    id 135
    label "ryba_po_grecku"
  ]
  node [
    id 136
    label "goniony"
  ]
  node [
    id 137
    label "polsko"
  ]
  node [
    id 138
    label "po_litewsku"
  ]
  node [
    id 139
    label "soczewiak"
  ]
  node [
    id 140
    label "p&#243;&#322;nocnoeuropejski"
  ]
  node [
    id 141
    label "europejski"
  ]
  node [
    id 142
    label "Lithuanian"
  ]
  node [
    id 143
    label "ser_jab&#322;kowy"
  ]
  node [
    id 144
    label "szpekucha"
  ]
  node [
    id 145
    label "j&#281;zyk_ba&#322;tycki"
  ]
  node [
    id 146
    label "blin_&#380;mudzki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
]
