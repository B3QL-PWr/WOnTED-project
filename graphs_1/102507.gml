graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.0828402366863905
  density 0.012397858551704706
  graphCliqueNumber 3
  node [
    id 0
    label "rozegrana"
    origin "text"
  ]
  node [
    id 1
    label "luty"
    origin "text"
  ]
  node [
    id 2
    label "katowice"
    origin "text"
  ]
  node [
    id 3
    label "zawody"
    origin "text"
  ]
  node [
    id 4
    label "model"
    origin "text"
  ]
  node [
    id 5
    label "skala"
    origin "text"
  ]
  node [
    id 6
    label "rozpocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "cykl"
    origin "text"
  ]
  node [
    id 8
    label "impreza"
    origin "text"
  ]
  node [
    id 9
    label "nazwa"
    origin "text"
  ]
  node [
    id 10
    label "micromania"
    origin "text"
  ]
  node [
    id 11
    label "pod"
    origin "text"
  ]
  node [
    id 12
    label "patronat"
    origin "text"
  ]
  node [
    id 13
    label "firma"
    origin "text"
  ]
  node [
    id 14
    label "proxima"
    origin "text"
  ]
  node [
    id 15
    label "stawi&#263;"
    origin "text"
  ]
  node [
    id 16
    label "si&#281;"
    origin "text"
  ]
  node [
    id 17
    label "czo&#322;&#243;wka"
    origin "text"
  ]
  node [
    id 18
    label "polski"
    origin "text"
  ]
  node [
    id 19
    label "zawodnik"
    origin "text"
  ]
  node [
    id 20
    label "go&#347;cia"
    origin "text"
  ]
  node [
    id 21
    label "czech"
    origin "text"
  ]
  node [
    id 22
    label "miesi&#261;c"
  ]
  node [
    id 23
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 24
    label "walentynki"
  ]
  node [
    id 25
    label "spadochroniarstwo"
  ]
  node [
    id 26
    label "champion"
  ]
  node [
    id 27
    label "walczenie"
  ]
  node [
    id 28
    label "tysi&#281;cznik"
  ]
  node [
    id 29
    label "kategoria_open"
  ]
  node [
    id 30
    label "walczy&#263;"
  ]
  node [
    id 31
    label "rywalizacja"
  ]
  node [
    id 32
    label "contest"
  ]
  node [
    id 33
    label "typ"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "pozowa&#263;"
  ]
  node [
    id 36
    label "ideal"
  ]
  node [
    id 37
    label "matryca"
  ]
  node [
    id 38
    label "imitacja"
  ]
  node [
    id 39
    label "ruch"
  ]
  node [
    id 40
    label "motif"
  ]
  node [
    id 41
    label "pozowanie"
  ]
  node [
    id 42
    label "wz&#243;r"
  ]
  node [
    id 43
    label "miniatura"
  ]
  node [
    id 44
    label "prezenter"
  ]
  node [
    id 45
    label "facet"
  ]
  node [
    id 46
    label "orygina&#322;"
  ]
  node [
    id 47
    label "mildew"
  ]
  node [
    id 48
    label "spos&#243;b"
  ]
  node [
    id 49
    label "zi&#243;&#322;ko"
  ]
  node [
    id 50
    label "adaptation"
  ]
  node [
    id 51
    label "przedzia&#322;"
  ]
  node [
    id 52
    label "przymiar"
  ]
  node [
    id 53
    label "podzia&#322;ka"
  ]
  node [
    id 54
    label "proporcja"
  ]
  node [
    id 55
    label "tetrachord"
  ]
  node [
    id 56
    label "scale"
  ]
  node [
    id 57
    label "dominanta"
  ]
  node [
    id 58
    label "rejestr"
  ]
  node [
    id 59
    label "sfera"
  ]
  node [
    id 60
    label "struktura"
  ]
  node [
    id 61
    label "kreska"
  ]
  node [
    id 62
    label "zero"
  ]
  node [
    id 63
    label "interwa&#322;"
  ]
  node [
    id 64
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 65
    label "subdominanta"
  ]
  node [
    id 66
    label "dziedzina"
  ]
  node [
    id 67
    label "masztab"
  ]
  node [
    id 68
    label "part"
  ]
  node [
    id 69
    label "podzakres"
  ]
  node [
    id 70
    label "zbi&#243;r"
  ]
  node [
    id 71
    label "wielko&#347;&#263;"
  ]
  node [
    id 72
    label "jednostka"
  ]
  node [
    id 73
    label "cause"
  ]
  node [
    id 74
    label "zacz&#261;&#263;"
  ]
  node [
    id 75
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 76
    label "do"
  ]
  node [
    id 77
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 78
    label "zrobi&#263;"
  ]
  node [
    id 79
    label "sekwencja"
  ]
  node [
    id 80
    label "czas"
  ]
  node [
    id 81
    label "edycja"
  ]
  node [
    id 82
    label "przebieg"
  ]
  node [
    id 83
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 84
    label "okres"
  ]
  node [
    id 85
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 86
    label "cycle"
  ]
  node [
    id 87
    label "owulacja"
  ]
  node [
    id 88
    label "miesi&#261;czka"
  ]
  node [
    id 89
    label "set"
  ]
  node [
    id 90
    label "party"
  ]
  node [
    id 91
    label "rozrywka"
  ]
  node [
    id 92
    label "przyj&#281;cie"
  ]
  node [
    id 93
    label "okazja"
  ]
  node [
    id 94
    label "impra"
  ]
  node [
    id 95
    label "term"
  ]
  node [
    id 96
    label "wezwanie"
  ]
  node [
    id 97
    label "leksem"
  ]
  node [
    id 98
    label "patron"
  ]
  node [
    id 99
    label "licencja"
  ]
  node [
    id 100
    label "opieka"
  ]
  node [
    id 101
    label "nadz&#243;r"
  ]
  node [
    id 102
    label "sponsorship"
  ]
  node [
    id 103
    label "MAC"
  ]
  node [
    id 104
    label "Hortex"
  ]
  node [
    id 105
    label "reengineering"
  ]
  node [
    id 106
    label "nazwa_w&#322;asna"
  ]
  node [
    id 107
    label "podmiot_gospodarczy"
  ]
  node [
    id 108
    label "Google"
  ]
  node [
    id 109
    label "zaufanie"
  ]
  node [
    id 110
    label "biurowiec"
  ]
  node [
    id 111
    label "interes"
  ]
  node [
    id 112
    label "zasoby_ludzkie"
  ]
  node [
    id 113
    label "networking"
  ]
  node [
    id 114
    label "paczkarnia"
  ]
  node [
    id 115
    label "Canon"
  ]
  node [
    id 116
    label "HP"
  ]
  node [
    id 117
    label "Baltona"
  ]
  node [
    id 118
    label "Pewex"
  ]
  node [
    id 119
    label "MAN_SE"
  ]
  node [
    id 120
    label "Apeks"
  ]
  node [
    id 121
    label "zasoby"
  ]
  node [
    id 122
    label "Orbis"
  ]
  node [
    id 123
    label "miejsce_pracy"
  ]
  node [
    id 124
    label "siedziba"
  ]
  node [
    id 125
    label "Spo&#322;em"
  ]
  node [
    id 126
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 127
    label "Orlen"
  ]
  node [
    id 128
    label "klasa"
  ]
  node [
    id 129
    label "obstawi&#263;"
  ]
  node [
    id 130
    label "alpinizm"
  ]
  node [
    id 131
    label "materia&#322;"
  ]
  node [
    id 132
    label "&#347;ciana"
  ]
  node [
    id 133
    label "grupa"
  ]
  node [
    id 134
    label "film"
  ]
  node [
    id 135
    label "rajd"
  ]
  node [
    id 136
    label "bieg"
  ]
  node [
    id 137
    label "poligrafia"
  ]
  node [
    id 138
    label "front"
  ]
  node [
    id 139
    label "rz&#261;d"
  ]
  node [
    id 140
    label "pododdzia&#322;"
  ]
  node [
    id 141
    label "wst&#281;p"
  ]
  node [
    id 142
    label "zderzenie"
  ]
  node [
    id 143
    label "elita"
  ]
  node [
    id 144
    label "latarka_czo&#322;owa"
  ]
  node [
    id 145
    label "lacki"
  ]
  node [
    id 146
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 147
    label "przedmiot"
  ]
  node [
    id 148
    label "sztajer"
  ]
  node [
    id 149
    label "drabant"
  ]
  node [
    id 150
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 151
    label "polak"
  ]
  node [
    id 152
    label "pierogi_ruskie"
  ]
  node [
    id 153
    label "krakowiak"
  ]
  node [
    id 154
    label "Polish"
  ]
  node [
    id 155
    label "j&#281;zyk"
  ]
  node [
    id 156
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 157
    label "oberek"
  ]
  node [
    id 158
    label "po_polsku"
  ]
  node [
    id 159
    label "mazur"
  ]
  node [
    id 160
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 161
    label "chodzony"
  ]
  node [
    id 162
    label "skoczny"
  ]
  node [
    id 163
    label "ryba_po_grecku"
  ]
  node [
    id 164
    label "goniony"
  ]
  node [
    id 165
    label "polsko"
  ]
  node [
    id 166
    label "lista_startowa"
  ]
  node [
    id 167
    label "uczestnik"
  ]
  node [
    id 168
    label "sportowiec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 34
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 45
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 46
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 49
  ]
  edge [
    source 20
    target 21
  ]
]
