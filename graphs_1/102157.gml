graph [
  maxDegree 25
  minDegree 1
  meanDegree 4.061538461538461
  density 0.06346153846153846
  graphCliqueNumber 13
  node [
    id 0
    label "organizacja"
    origin "text"
  ]
  node [
    id 1
    label "praca"
    origin "text"
  ]
  node [
    id 2
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 3
    label "endecki"
  ]
  node [
    id 4
    label "komitet_koordynacyjny"
  ]
  node [
    id 5
    label "przybud&#243;wka"
  ]
  node [
    id 6
    label "ZOMO"
  ]
  node [
    id 7
    label "podmiot"
  ]
  node [
    id 8
    label "boj&#243;wka"
  ]
  node [
    id 9
    label "zesp&#243;&#322;"
  ]
  node [
    id 10
    label "organization"
  ]
  node [
    id 11
    label "TOPR"
  ]
  node [
    id 12
    label "jednostka_organizacyjna"
  ]
  node [
    id 13
    label "przedstawicielstwo"
  ]
  node [
    id 14
    label "Cepelia"
  ]
  node [
    id 15
    label "GOPR"
  ]
  node [
    id 16
    label "ZMP"
  ]
  node [
    id 17
    label "ZBoWiD"
  ]
  node [
    id 18
    label "struktura"
  ]
  node [
    id 19
    label "od&#322;am"
  ]
  node [
    id 20
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 21
    label "centrala"
  ]
  node [
    id 22
    label "stosunek_pracy"
  ]
  node [
    id 23
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 24
    label "benedykty&#324;ski"
  ]
  node [
    id 25
    label "pracowanie"
  ]
  node [
    id 26
    label "zaw&#243;d"
  ]
  node [
    id 27
    label "kierownictwo"
  ]
  node [
    id 28
    label "zmiana"
  ]
  node [
    id 29
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 30
    label "wytw&#243;r"
  ]
  node [
    id 31
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 32
    label "tynkarski"
  ]
  node [
    id 33
    label "czynnik_produkcji"
  ]
  node [
    id 34
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 35
    label "zobowi&#261;zanie"
  ]
  node [
    id 36
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 37
    label "czynno&#347;&#263;"
  ]
  node [
    id 38
    label "tyrka"
  ]
  node [
    id 39
    label "pracowa&#263;"
  ]
  node [
    id 40
    label "siedziba"
  ]
  node [
    id 41
    label "poda&#380;_pracy"
  ]
  node [
    id 42
    label "miejsce"
  ]
  node [
    id 43
    label "zak&#322;ad"
  ]
  node [
    id 44
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 45
    label "najem"
  ]
  node [
    id 46
    label "prezydent"
  ]
  node [
    id 47
    label "miasto"
  ]
  node [
    id 48
    label "Bydgoszcz"
  ]
  node [
    id 49
    label "aktualizacja"
  ]
  node [
    id 50
    label "strategia"
  ]
  node [
    id 51
    label "rozw&#243;j"
  ]
  node [
    id 52
    label "diagnoza"
  ]
  node [
    id 53
    label "prospektywny"
  ]
  node [
    id 54
    label "do"
  ]
  node [
    id 55
    label "2015"
  ]
  node [
    id 56
    label "rok"
  ]
  node [
    id 57
    label "spo&#322;eczny"
  ]
  node [
    id 58
    label "rada"
  ]
  node [
    id 59
    label "konsultacyjny"
  ]
  node [
    id 60
    label "do&#160;spraw"
  ]
  node [
    id 61
    label "&#8222;"
  ]
  node [
    id 62
    label "&#8221;"
  ]
  node [
    id 63
    label "Lidia"
  ]
  node [
    id 64
    label "Wilniewczyc"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 47
    target 58
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 48
    target 54
  ]
  edge [
    source 48
    target 55
  ]
  edge [
    source 48
    target 56
  ]
  edge [
    source 48
    target 57
  ]
  edge [
    source 48
    target 58
  ]
  edge [
    source 48
    target 59
  ]
  edge [
    source 48
    target 60
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 61
  ]
  edge [
    source 48
    target 62
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 49
    target 57
  ]
  edge [
    source 49
    target 58
  ]
  edge [
    source 49
    target 59
  ]
  edge [
    source 49
    target 60
  ]
  edge [
    source 49
    target 61
  ]
  edge [
    source 49
    target 54
  ]
  edge [
    source 49
    target 55
  ]
  edge [
    source 49
    target 56
  ]
  edge [
    source 49
    target 62
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 50
    target 55
  ]
  edge [
    source 50
    target 56
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 50
    target 58
  ]
  edge [
    source 50
    target 59
  ]
  edge [
    source 50
    target 60
  ]
  edge [
    source 50
    target 61
  ]
  edge [
    source 50
    target 62
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 51
    target 56
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 51
    target 58
  ]
  edge [
    source 51
    target 59
  ]
  edge [
    source 51
    target 60
  ]
  edge [
    source 51
    target 61
  ]
  edge [
    source 51
    target 62
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 54
    target 59
  ]
  edge [
    source 54
    target 60
  ]
  edge [
    source 54
    target 61
  ]
  edge [
    source 54
    target 62
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 55
    target 59
  ]
  edge [
    source 55
    target 60
  ]
  edge [
    source 55
    target 61
  ]
  edge [
    source 55
    target 62
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 59
  ]
  edge [
    source 56
    target 60
  ]
  edge [
    source 56
    target 61
  ]
  edge [
    source 56
    target 62
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 57
    target 61
  ]
  edge [
    source 57
    target 62
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 58
    target 61
  ]
  edge [
    source 58
    target 62
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 59
    target 62
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 62
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 63
    target 64
  ]
]
