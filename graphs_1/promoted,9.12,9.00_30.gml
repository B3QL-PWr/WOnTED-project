graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dzienia"
    origin "text"
  ]
  node [
    id 2
    label "zorganizowa&#263;"
  ]
  node [
    id 3
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 4
    label "przerobi&#263;"
  ]
  node [
    id 5
    label "wystylizowa&#263;"
  ]
  node [
    id 6
    label "cause"
  ]
  node [
    id 7
    label "wydali&#263;"
  ]
  node [
    id 8
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 9
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 10
    label "post&#261;pi&#263;"
  ]
  node [
    id 11
    label "appoint"
  ]
  node [
    id 12
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 13
    label "nabra&#263;"
  ]
  node [
    id 14
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 15
    label "make"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
]
