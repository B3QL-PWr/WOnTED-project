graph [
  maxDegree 66
  minDegree 1
  meanDegree 1.9865771812080537
  density 0.013422818791946308
  graphCliqueNumber 2
  node [
    id 0
    label "powsta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "inspiracja"
    origin "text"
  ]
  node [
    id 2
    label "afryka&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "street"
    origin "text"
  ]
  node [
    id 4
    label "foodem"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "pozna&#263;"
    origin "text"
  ]
  node [
    id 7
    label "posmakowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "polski"
    origin "text"
  ]
  node [
    id 9
    label "technolog"
    origin "text"
  ]
  node [
    id 10
    label "p&#322;ywaj&#261;cy"
    origin "text"
  ]
  node [
    id 11
    label "wielki"
    origin "text"
  ]
  node [
    id 12
    label "statek"
    origin "text"
  ]
  node [
    id 13
    label "ch&#322;odnia"
    origin "text"
  ]
  node [
    id 14
    label "zaistnie&#263;"
  ]
  node [
    id 15
    label "sprzeciwi&#263;_si&#281;"
  ]
  node [
    id 16
    label "originate"
  ]
  node [
    id 17
    label "rise"
  ]
  node [
    id 18
    label "zbuntowa&#263;_si&#281;"
  ]
  node [
    id 19
    label "mount"
  ]
  node [
    id 20
    label "stan&#261;&#263;"
  ]
  node [
    id 21
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 22
    label "kuca&#263;"
  ]
  node [
    id 23
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 24
    label "wypowied&#378;"
  ]
  node [
    id 25
    label "porada"
  ]
  node [
    id 26
    label "wp&#322;yw"
  ]
  node [
    id 27
    label "inspiration"
  ]
  node [
    id 28
    label "zach&#281;ta"
  ]
  node [
    id 29
    label "upalny"
  ]
  node [
    id 30
    label "charakterystyczny"
  ]
  node [
    id 31
    label "po_afryka&#324;sku"
  ]
  node [
    id 32
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 33
    label "typowy"
  ]
  node [
    id 34
    label "etnolekt"
  ]
  node [
    id 35
    label "przyswoi&#263;"
  ]
  node [
    id 36
    label "feel"
  ]
  node [
    id 37
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 38
    label "teach"
  ]
  node [
    id 39
    label "zrozumie&#263;"
  ]
  node [
    id 40
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 41
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 42
    label "topographic_point"
  ]
  node [
    id 43
    label "experience"
  ]
  node [
    id 44
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 45
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 46
    label "visualize"
  ]
  node [
    id 47
    label "spo&#380;y&#263;"
  ]
  node [
    id 48
    label "try"
  ]
  node [
    id 49
    label "lacki"
  ]
  node [
    id 50
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 51
    label "przedmiot"
  ]
  node [
    id 52
    label "sztajer"
  ]
  node [
    id 53
    label "drabant"
  ]
  node [
    id 54
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 55
    label "polak"
  ]
  node [
    id 56
    label "pierogi_ruskie"
  ]
  node [
    id 57
    label "krakowiak"
  ]
  node [
    id 58
    label "Polish"
  ]
  node [
    id 59
    label "j&#281;zyk"
  ]
  node [
    id 60
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 61
    label "oberek"
  ]
  node [
    id 62
    label "po_polsku"
  ]
  node [
    id 63
    label "mazur"
  ]
  node [
    id 64
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 65
    label "chodzony"
  ]
  node [
    id 66
    label "skoczny"
  ]
  node [
    id 67
    label "ryba_po_grecku"
  ]
  node [
    id 68
    label "goniony"
  ]
  node [
    id 69
    label "polsko"
  ]
  node [
    id 70
    label "specjalista"
  ]
  node [
    id 71
    label "asekuracyjny"
  ]
  node [
    id 72
    label "wyporno&#347;ciowy"
  ]
  node [
    id 73
    label "dupny"
  ]
  node [
    id 74
    label "wysoce"
  ]
  node [
    id 75
    label "wyj&#261;tkowy"
  ]
  node [
    id 76
    label "wybitny"
  ]
  node [
    id 77
    label "znaczny"
  ]
  node [
    id 78
    label "prawdziwy"
  ]
  node [
    id 79
    label "wa&#380;ny"
  ]
  node [
    id 80
    label "nieprzeci&#281;tny"
  ]
  node [
    id 81
    label "korab"
  ]
  node [
    id 82
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 83
    label "zr&#281;bnica"
  ]
  node [
    id 84
    label "odkotwiczenie"
  ]
  node [
    id 85
    label "cumowanie"
  ]
  node [
    id 86
    label "zadokowanie"
  ]
  node [
    id 87
    label "bumsztak"
  ]
  node [
    id 88
    label "zacumowanie"
  ]
  node [
    id 89
    label "dobi&#263;"
  ]
  node [
    id 90
    label "odkotwiczanie"
  ]
  node [
    id 91
    label "kotwica"
  ]
  node [
    id 92
    label "zwodowa&#263;"
  ]
  node [
    id 93
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 94
    label "zakotwiczenie"
  ]
  node [
    id 95
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 96
    label "dzi&#243;b"
  ]
  node [
    id 97
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 98
    label "armada"
  ]
  node [
    id 99
    label "grobla"
  ]
  node [
    id 100
    label "kad&#322;ub"
  ]
  node [
    id 101
    label "dobijanie"
  ]
  node [
    id 102
    label "odkotwicza&#263;"
  ]
  node [
    id 103
    label "proporczyk"
  ]
  node [
    id 104
    label "luk"
  ]
  node [
    id 105
    label "odcumowanie"
  ]
  node [
    id 106
    label "kabina"
  ]
  node [
    id 107
    label "skrajnik"
  ]
  node [
    id 108
    label "kotwiczenie"
  ]
  node [
    id 109
    label "zwodowanie"
  ]
  node [
    id 110
    label "szkutnictwo"
  ]
  node [
    id 111
    label "pojazd"
  ]
  node [
    id 112
    label "wodowanie"
  ]
  node [
    id 113
    label "zacumowa&#263;"
  ]
  node [
    id 114
    label "sterownik_automatyczny"
  ]
  node [
    id 115
    label "p&#322;ywa&#263;"
  ]
  node [
    id 116
    label "zadokowa&#263;"
  ]
  node [
    id 117
    label "zakotwiczy&#263;"
  ]
  node [
    id 118
    label "sztormtrap"
  ]
  node [
    id 119
    label "pok&#322;ad"
  ]
  node [
    id 120
    label "kotwiczy&#263;"
  ]
  node [
    id 121
    label "&#380;yroskop"
  ]
  node [
    id 122
    label "odcumowa&#263;"
  ]
  node [
    id 123
    label "dobicie"
  ]
  node [
    id 124
    label "armator"
  ]
  node [
    id 125
    label "odbijacz"
  ]
  node [
    id 126
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 127
    label "reling"
  ]
  node [
    id 128
    label "flota"
  ]
  node [
    id 129
    label "kabestan"
  ]
  node [
    id 130
    label "nadbud&#243;wka"
  ]
  node [
    id 131
    label "dokowa&#263;"
  ]
  node [
    id 132
    label "cumowa&#263;"
  ]
  node [
    id 133
    label "odkotwiczy&#263;"
  ]
  node [
    id 134
    label "dobija&#263;"
  ]
  node [
    id 135
    label "odcumowywanie"
  ]
  node [
    id 136
    label "ster"
  ]
  node [
    id 137
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 138
    label "odcumowywa&#263;"
  ]
  node [
    id 139
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 140
    label "futr&#243;wka"
  ]
  node [
    id 141
    label "dokowanie"
  ]
  node [
    id 142
    label "trap"
  ]
  node [
    id 143
    label "zaw&#243;r_denny"
  ]
  node [
    id 144
    label "rostra"
  ]
  node [
    id 145
    label "refrigerator"
  ]
  node [
    id 146
    label "magazyn"
  ]
  node [
    id 147
    label "ch&#322;odnicowiec"
  ]
  node [
    id 148
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
]
