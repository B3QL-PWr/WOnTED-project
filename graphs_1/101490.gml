graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 3
  node [
    id 0
    label "rad"
    origin "text"
  ]
  node [
    id 1
    label "vasile"
    origin "text"
  ]
  node [
    id 2
    label "berylowiec"
  ]
  node [
    id 3
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 4
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 5
    label "mikroradian"
  ]
  node [
    id 6
    label "zadowolenie_si&#281;"
  ]
  node [
    id 7
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 8
    label "content"
  ]
  node [
    id 9
    label "jednostka_promieniowania"
  ]
  node [
    id 10
    label "miliradian"
  ]
  node [
    id 11
    label "jednostka"
  ]
  node [
    id 12
    label "Vasile"
  ]
  node [
    id 13
    label "Mischiu"
  ]
  node [
    id 14
    label "narodowy"
  ]
  node [
    id 15
    label "partia"
  ]
  node [
    id 16
    label "ch&#322;op"
  ]
  node [
    id 17
    label "demokratyczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
]
