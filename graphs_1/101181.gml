graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "zdoby&#263;"
    origin "text"
  ]
  node [
    id 1
    label "flaga"
    origin "text"
  ]
  node [
    id 2
    label "uzyska&#263;"
  ]
  node [
    id 3
    label "stage"
  ]
  node [
    id 4
    label "dosta&#263;"
  ]
  node [
    id 5
    label "manipulate"
  ]
  node [
    id 6
    label "realize"
  ]
  node [
    id 7
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 8
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 9
    label "flag"
  ]
  node [
    id 10
    label "transparent"
  ]
  node [
    id 11
    label "oznaka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
]
