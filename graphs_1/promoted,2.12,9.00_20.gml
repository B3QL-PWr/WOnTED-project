graph [
  maxDegree 32
  minDegree 1
  meanDegree 1.9726027397260273
  density 0.0273972602739726
  graphCliqueNumber 2
  node [
    id 0
    label "dramatyczny"
    origin "text"
  ]
  node [
    id 1
    label "starcie"
    origin "text"
  ]
  node [
    id 2
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "hebron"
    origin "text"
  ]
  node [
    id 4
    label "czerwiec"
    origin "text"
  ]
  node [
    id 5
    label "przej&#281;ty"
  ]
  node [
    id 6
    label "tragicznie"
  ]
  node [
    id 7
    label "dramatycznie"
  ]
  node [
    id 8
    label "wstrz&#261;saj&#261;cy"
  ]
  node [
    id 9
    label "powa&#380;ny"
  ]
  node [
    id 10
    label "emocjonuj&#261;cy"
  ]
  node [
    id 11
    label "straszny"
  ]
  node [
    id 12
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 13
    label "dynamiczny"
  ]
  node [
    id 14
    label "zranienie"
  ]
  node [
    id 15
    label "konfrontacyjny"
  ]
  node [
    id 16
    label "usuni&#281;cie"
  ]
  node [
    id 17
    label "military_action"
  ]
  node [
    id 18
    label "trafienie"
  ]
  node [
    id 19
    label "oczyszczenie"
  ]
  node [
    id 20
    label "sambo"
  ]
  node [
    id 21
    label "euroliga"
  ]
  node [
    id 22
    label "zaatakowanie"
  ]
  node [
    id 23
    label "interliga"
  ]
  node [
    id 24
    label "runda"
  ]
  node [
    id 25
    label "contest"
  ]
  node [
    id 26
    label "discord"
  ]
  node [
    id 27
    label "zagrywka"
  ]
  node [
    id 28
    label "obrona"
  ]
  node [
    id 29
    label "expunction"
  ]
  node [
    id 30
    label "rub"
  ]
  node [
    id 31
    label "wydarzenie"
  ]
  node [
    id 32
    label "rewan&#380;owy"
  ]
  node [
    id 33
    label "otarcie"
  ]
  node [
    id 34
    label "faza"
  ]
  node [
    id 35
    label "rozdrobnienie"
  ]
  node [
    id 36
    label "k&#322;&#243;tnia"
  ]
  node [
    id 37
    label "scramble"
  ]
  node [
    id 38
    label "get"
  ]
  node [
    id 39
    label "zaj&#347;&#263;"
  ]
  node [
    id 40
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 41
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 42
    label "dop&#322;ata"
  ]
  node [
    id 43
    label "supervene"
  ]
  node [
    id 44
    label "heed"
  ]
  node [
    id 45
    label "dodatek"
  ]
  node [
    id 46
    label "catch"
  ]
  node [
    id 47
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 48
    label "uzyska&#263;"
  ]
  node [
    id 49
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 50
    label "orgazm"
  ]
  node [
    id 51
    label "dozna&#263;"
  ]
  node [
    id 52
    label "sta&#263;_si&#281;"
  ]
  node [
    id 53
    label "bodziec"
  ]
  node [
    id 54
    label "drive"
  ]
  node [
    id 55
    label "informacja"
  ]
  node [
    id 56
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 57
    label "spowodowa&#263;"
  ]
  node [
    id 58
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 59
    label "dotrze&#263;"
  ]
  node [
    id 60
    label "postrzega&#263;"
  ]
  node [
    id 61
    label "become"
  ]
  node [
    id 62
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 63
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 64
    label "przesy&#322;ka"
  ]
  node [
    id 65
    label "dolecie&#263;"
  ]
  node [
    id 66
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 67
    label "dokoptowa&#263;"
  ]
  node [
    id 68
    label "ro&#347;lina_zielna"
  ]
  node [
    id 69
    label "go&#378;dzikowate"
  ]
  node [
    id 70
    label "miesi&#261;c"
  ]
  node [
    id 71
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 72
    label "&#347;wi&#281;ty_Jan"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
]
