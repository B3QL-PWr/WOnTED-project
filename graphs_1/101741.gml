graph [
  maxDegree 70
  minDegree 1
  meanDegree 2.0825242718446604
  density 0.0050669690312522145
  graphCliqueNumber 3
  node [
    id 0
    label "tworzy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "okres"
    origin "text"
  ]
  node [
    id 3
    label "przelot"
    origin "text"
  ]
  node [
    id 4
    label "konstruowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "przer&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 6
    label "figura"
    origin "text"
  ]
  node [
    id 7
    label "niebo"
    origin "text"
  ]
  node [
    id 8
    label "niekiedy"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "popularny"
    origin "text"
  ]
  node [
    id 11
    label "klucz"
    origin "text"
  ]
  node [
    id 12
    label "inny"
    origin "text"
  ]
  node [
    id 13
    label "razem"
    origin "text"
  ]
  node [
    id 14
    label "znana"
    origin "text"
  ]
  node [
    id 15
    label "laik"
    origin "text"
  ]
  node [
    id 16
    label "tylko"
    origin "text"
  ]
  node [
    id 17
    label "powiedzenie"
    origin "text"
  ]
  node [
    id 18
    label "szyk"
    origin "text"
  ]
  node [
    id 19
    label "g&#281;si"
    origin "text"
  ]
  node [
    id 20
    label "naukowiec"
    origin "text"
  ]
  node [
    id 21
    label "lata"
    origin "text"
  ]
  node [
    id 22
    label "bada&#263;"
    origin "text"
  ]
  node [
    id 23
    label "fenomen"
    origin "text"
  ]
  node [
    id 24
    label "zjawisko"
    origin "text"
  ]
  node [
    id 25
    label "bardzo"
    origin "text"
  ]
  node [
    id 26
    label "skrajny"
    origin "text"
  ]
  node [
    id 27
    label "opinia"
    origin "text"
  ]
  node [
    id 28
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 29
    label "przypadek"
    origin "text"
  ]
  node [
    id 30
    label "druga"
    origin "text"
  ]
  node [
    id 31
    label "zamontowa&#263;"
    origin "text"
  ]
  node [
    id 32
    label "system"
    origin "text"
  ]
  node [
    id 33
    label "nawigacja"
    origin "text"
  ]
  node [
    id 34
    label "gps"
    origin "text"
  ]
  node [
    id 35
    label "jak"
    origin "text"
  ]
  node [
    id 36
    label "naprawd&#281;"
    origin "text"
  ]
  node [
    id 37
    label "ptasi"
    origin "text"
  ]
  node [
    id 38
    label "formacja"
    origin "text"
  ]
  node [
    id 39
    label "dlaczego"
    origin "text"
  ]
  node [
    id 40
    label "kiedy"
    origin "text"
  ]
  node [
    id 41
    label "w&#281;drowa&#263;"
    origin "text"
  ]
  node [
    id 42
    label "get"
  ]
  node [
    id 43
    label "consist"
  ]
  node [
    id 44
    label "raise"
  ]
  node [
    id 45
    label "robi&#263;"
  ]
  node [
    id 46
    label "pope&#322;nia&#263;"
  ]
  node [
    id 47
    label "wytwarza&#263;"
  ]
  node [
    id 48
    label "stanowi&#263;"
  ]
  node [
    id 49
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 50
    label "paleogen"
  ]
  node [
    id 51
    label "spell"
  ]
  node [
    id 52
    label "czas"
  ]
  node [
    id 53
    label "period"
  ]
  node [
    id 54
    label "prekambr"
  ]
  node [
    id 55
    label "jura"
  ]
  node [
    id 56
    label "interstadia&#322;"
  ]
  node [
    id 57
    label "jednostka_geologiczna"
  ]
  node [
    id 58
    label "izochronizm"
  ]
  node [
    id 59
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 60
    label "okres_noachijski"
  ]
  node [
    id 61
    label "orosir"
  ]
  node [
    id 62
    label "kreda"
  ]
  node [
    id 63
    label "sten"
  ]
  node [
    id 64
    label "drugorz&#281;d"
  ]
  node [
    id 65
    label "semester"
  ]
  node [
    id 66
    label "trzeciorz&#281;d"
  ]
  node [
    id 67
    label "ton"
  ]
  node [
    id 68
    label "dzieje"
  ]
  node [
    id 69
    label "poprzednik"
  ]
  node [
    id 70
    label "ordowik"
  ]
  node [
    id 71
    label "karbon"
  ]
  node [
    id 72
    label "trias"
  ]
  node [
    id 73
    label "kalim"
  ]
  node [
    id 74
    label "stater"
  ]
  node [
    id 75
    label "era"
  ]
  node [
    id 76
    label "cykl"
  ]
  node [
    id 77
    label "p&#243;&#322;okres"
  ]
  node [
    id 78
    label "czwartorz&#281;d"
  ]
  node [
    id 79
    label "pulsacja"
  ]
  node [
    id 80
    label "okres_amazo&#324;ski"
  ]
  node [
    id 81
    label "kambr"
  ]
  node [
    id 82
    label "Zeitgeist"
  ]
  node [
    id 83
    label "nast&#281;pnik"
  ]
  node [
    id 84
    label "kriogen"
  ]
  node [
    id 85
    label "glacja&#322;"
  ]
  node [
    id 86
    label "fala"
  ]
  node [
    id 87
    label "okres_czasu"
  ]
  node [
    id 88
    label "riak"
  ]
  node [
    id 89
    label "schy&#322;ek"
  ]
  node [
    id 90
    label "okres_hesperyjski"
  ]
  node [
    id 91
    label "sylur"
  ]
  node [
    id 92
    label "dewon"
  ]
  node [
    id 93
    label "ciota"
  ]
  node [
    id 94
    label "epoka"
  ]
  node [
    id 95
    label "pierwszorz&#281;d"
  ]
  node [
    id 96
    label "okres_halsztacki"
  ]
  node [
    id 97
    label "ektas"
  ]
  node [
    id 98
    label "zdanie"
  ]
  node [
    id 99
    label "condition"
  ]
  node [
    id 100
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 101
    label "rok_akademicki"
  ]
  node [
    id 102
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 103
    label "postglacja&#322;"
  ]
  node [
    id 104
    label "faza"
  ]
  node [
    id 105
    label "proces_fizjologiczny"
  ]
  node [
    id 106
    label "ediakar"
  ]
  node [
    id 107
    label "time_period"
  ]
  node [
    id 108
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 109
    label "perm"
  ]
  node [
    id 110
    label "rok_szkolny"
  ]
  node [
    id 111
    label "neogen"
  ]
  node [
    id 112
    label "sider"
  ]
  node [
    id 113
    label "flow"
  ]
  node [
    id 114
    label "podokres"
  ]
  node [
    id 115
    label "preglacja&#322;"
  ]
  node [
    id 116
    label "retoryka"
  ]
  node [
    id 117
    label "choroba_przyrodzona"
  ]
  node [
    id 118
    label "bobowate_w&#322;a&#347;ciwe"
  ]
  node [
    id 119
    label "prze&#347;wit"
  ]
  node [
    id 120
    label "bylina"
  ]
  node [
    id 121
    label "lot"
  ]
  node [
    id 122
    label "passage"
  ]
  node [
    id 123
    label "train"
  ]
  node [
    id 124
    label "jaki&#347;"
  ]
  node [
    id 125
    label "przer&#243;&#380;nie"
  ]
  node [
    id 126
    label "cz&#322;owiek"
  ]
  node [
    id 127
    label "Aspazja"
  ]
  node [
    id 128
    label "charakterystyka"
  ]
  node [
    id 129
    label "gestaltyzm"
  ]
  node [
    id 130
    label "przedmiot"
  ]
  node [
    id 131
    label "rzecz"
  ]
  node [
    id 132
    label "figure"
  ]
  node [
    id 133
    label "kompleksja"
  ]
  node [
    id 134
    label "podzbi&#243;r"
  ]
  node [
    id 135
    label "Osjan"
  ]
  node [
    id 136
    label "sztuka"
  ]
  node [
    id 137
    label "antycypacja"
  ]
  node [
    id 138
    label "ornamentyka"
  ]
  node [
    id 139
    label "wytw&#243;r"
  ]
  node [
    id 140
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 141
    label "bierka_szachowa"
  ]
  node [
    id 142
    label "obraz"
  ]
  node [
    id 143
    label "popis"
  ]
  node [
    id 144
    label "budowa"
  ]
  node [
    id 145
    label "symetria"
  ]
  node [
    id 146
    label "shape"
  ]
  node [
    id 147
    label "stylistyka"
  ]
  node [
    id 148
    label "styl"
  ]
  node [
    id 149
    label "point"
  ]
  node [
    id 150
    label "cecha"
  ]
  node [
    id 151
    label "informacja"
  ]
  node [
    id 152
    label "facet"
  ]
  node [
    id 153
    label "karta"
  ]
  node [
    id 154
    label "obiekt_matematyczny"
  ]
  node [
    id 155
    label "character"
  ]
  node [
    id 156
    label "wygl&#261;d"
  ]
  node [
    id 157
    label "przedstawienie"
  ]
  node [
    id 158
    label "miejsce"
  ]
  node [
    id 159
    label "lingwistyka_kognitywna"
  ]
  node [
    id 160
    label "rze&#378;ba"
  ]
  node [
    id 161
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 162
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 163
    label "wiersz"
  ]
  node [
    id 164
    label "d&#378;wi&#281;k"
  ]
  node [
    id 165
    label "perspektywa"
  ]
  node [
    id 166
    label "kto&#347;"
  ]
  node [
    id 167
    label "p&#322;aszczyzna"
  ]
  node [
    id 168
    label "Waruna"
  ]
  node [
    id 169
    label "przestrze&#324;"
  ]
  node [
    id 170
    label "za&#347;wiaty"
  ]
  node [
    id 171
    label "zodiak"
  ]
  node [
    id 172
    label "znak_zodiaku"
  ]
  node [
    id 173
    label "bezchmurno&#347;&#263;"
  ]
  node [
    id 174
    label "Kr&#243;lestwo_Niebieskie"
  ]
  node [
    id 175
    label "czasami"
  ]
  node [
    id 176
    label "si&#281;ga&#263;"
  ]
  node [
    id 177
    label "trwa&#263;"
  ]
  node [
    id 178
    label "obecno&#347;&#263;"
  ]
  node [
    id 179
    label "stan"
  ]
  node [
    id 180
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 181
    label "stand"
  ]
  node [
    id 182
    label "mie&#263;_miejsce"
  ]
  node [
    id 183
    label "uczestniczy&#263;"
  ]
  node [
    id 184
    label "chodzi&#263;"
  ]
  node [
    id 185
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 186
    label "equal"
  ]
  node [
    id 187
    label "przyst&#281;pny"
  ]
  node [
    id 188
    label "&#322;atwy"
  ]
  node [
    id 189
    label "popularnie"
  ]
  node [
    id 190
    label "znany"
  ]
  node [
    id 191
    label "spis"
  ]
  node [
    id 192
    label "kompleks"
  ]
  node [
    id 193
    label "podstawa"
  ]
  node [
    id 194
    label "obja&#347;nienie"
  ]
  node [
    id 195
    label "narz&#281;dzie"
  ]
  node [
    id 196
    label "ochrona"
  ]
  node [
    id 197
    label "za&#322;&#261;cznik"
  ]
  node [
    id 198
    label "znak_muzyczny"
  ]
  node [
    id 199
    label "przycisk"
  ]
  node [
    id 200
    label "nakr&#281;ca&#263;"
  ]
  node [
    id 201
    label "spos&#243;b"
  ]
  node [
    id 202
    label "kliniec"
  ]
  node [
    id 203
    label "code"
  ]
  node [
    id 204
    label "szyfr"
  ]
  node [
    id 205
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 206
    label "instrument_strunowy"
  ]
  node [
    id 207
    label "z&#322;&#261;czenie"
  ]
  node [
    id 208
    label "zbi&#243;r"
  ]
  node [
    id 209
    label "radical"
  ]
  node [
    id 210
    label "kod"
  ]
  node [
    id 211
    label "kolejny"
  ]
  node [
    id 212
    label "inaczej"
  ]
  node [
    id 213
    label "r&#243;&#380;ny"
  ]
  node [
    id 214
    label "inszy"
  ]
  node [
    id 215
    label "osobno"
  ]
  node [
    id 216
    label "&#322;&#261;cznie"
  ]
  node [
    id 217
    label "nieprofesjonalista"
  ]
  node [
    id 218
    label "wyznawca"
  ]
  node [
    id 219
    label "laikat"
  ]
  node [
    id 220
    label "wyznanie"
  ]
  node [
    id 221
    label "dodanie"
  ]
  node [
    id 222
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 223
    label "wypowied&#378;"
  ]
  node [
    id 224
    label "podanie"
  ]
  node [
    id 225
    label "nazwanie"
  ]
  node [
    id 226
    label "ozwanie_si&#281;"
  ]
  node [
    id 227
    label "wydanie"
  ]
  node [
    id 228
    label "notification"
  ]
  node [
    id 229
    label "doprowadzenie"
  ]
  node [
    id 230
    label "wyra&#380;enie"
  ]
  node [
    id 231
    label "statement"
  ]
  node [
    id 232
    label "wypowiedzenie"
  ]
  node [
    id 233
    label "proverb"
  ]
  node [
    id 234
    label "wydobycie"
  ]
  node [
    id 235
    label "przepowiedzenie"
  ]
  node [
    id 236
    label "zapeszenie"
  ]
  node [
    id 237
    label "rozwleczenie"
  ]
  node [
    id 238
    label "whole"
  ]
  node [
    id 239
    label "skrzyd&#322;o"
  ]
  node [
    id 240
    label "sznyt"
  ]
  node [
    id 241
    label "kolejno&#347;&#263;"
  ]
  node [
    id 242
    label "kaczkowate"
  ]
  node [
    id 243
    label "Miczurin"
  ]
  node [
    id 244
    label "&#347;ledziciel"
  ]
  node [
    id 245
    label "uczony"
  ]
  node [
    id 246
    label "summer"
  ]
  node [
    id 247
    label "sondowa&#263;"
  ]
  node [
    id 248
    label "examine"
  ]
  node [
    id 249
    label "wypytywa&#263;"
  ]
  node [
    id 250
    label "decydowa&#263;"
  ]
  node [
    id 251
    label "poznawa&#263;"
  ]
  node [
    id 252
    label "sprawdza&#263;"
  ]
  node [
    id 253
    label "talent"
  ]
  node [
    id 254
    label "nadcz&#322;owiek"
  ]
  node [
    id 255
    label "m&#243;zg"
  ]
  node [
    id 256
    label "rzadko&#347;&#263;"
  ]
  node [
    id 257
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 258
    label "krajobraz"
  ]
  node [
    id 259
    label "przywidzenie"
  ]
  node [
    id 260
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 261
    label "boski"
  ]
  node [
    id 262
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 263
    label "proces"
  ]
  node [
    id 264
    label "presence"
  ]
  node [
    id 265
    label "charakter"
  ]
  node [
    id 266
    label "w_chuj"
  ]
  node [
    id 267
    label "okrajkowy"
  ]
  node [
    id 268
    label "skrajnie"
  ]
  node [
    id 269
    label "dokument"
  ]
  node [
    id 270
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 271
    label "reputacja"
  ]
  node [
    id 272
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 273
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 274
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 275
    label "sofcik"
  ]
  node [
    id 276
    label "appraisal"
  ]
  node [
    id 277
    label "ekspertyza"
  ]
  node [
    id 278
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 279
    label "pogl&#261;d"
  ]
  node [
    id 280
    label "kryterium"
  ]
  node [
    id 281
    label "wielko&#347;&#263;"
  ]
  node [
    id 282
    label "remark"
  ]
  node [
    id 283
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 284
    label "u&#380;ywa&#263;"
  ]
  node [
    id 285
    label "okre&#347;la&#263;"
  ]
  node [
    id 286
    label "j&#281;zyk"
  ]
  node [
    id 287
    label "say"
  ]
  node [
    id 288
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 289
    label "formu&#322;owa&#263;"
  ]
  node [
    id 290
    label "talk"
  ]
  node [
    id 291
    label "powiada&#263;"
  ]
  node [
    id 292
    label "informowa&#263;"
  ]
  node [
    id 293
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 294
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 295
    label "wydobywa&#263;"
  ]
  node [
    id 296
    label "express"
  ]
  node [
    id 297
    label "chew_the_fat"
  ]
  node [
    id 298
    label "dysfonia"
  ]
  node [
    id 299
    label "umie&#263;"
  ]
  node [
    id 300
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 301
    label "tell"
  ]
  node [
    id 302
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 303
    label "wyra&#380;a&#263;"
  ]
  node [
    id 304
    label "gaworzy&#263;"
  ]
  node [
    id 305
    label "rozmawia&#263;"
  ]
  node [
    id 306
    label "dziama&#263;"
  ]
  node [
    id 307
    label "prawi&#263;"
  ]
  node [
    id 308
    label "pacjent"
  ]
  node [
    id 309
    label "kategoria_gramatyczna"
  ]
  node [
    id 310
    label "schorzenie"
  ]
  node [
    id 311
    label "przeznaczenie"
  ]
  node [
    id 312
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 313
    label "wydarzenie"
  ]
  node [
    id 314
    label "happening"
  ]
  node [
    id 315
    label "przyk&#322;ad"
  ]
  node [
    id 316
    label "godzina"
  ]
  node [
    id 317
    label "install"
  ]
  node [
    id 318
    label "umie&#347;ci&#263;"
  ]
  node [
    id 319
    label "model"
  ]
  node [
    id 320
    label "sk&#322;ad"
  ]
  node [
    id 321
    label "zachowanie"
  ]
  node [
    id 322
    label "porz&#261;dek"
  ]
  node [
    id 323
    label "Android"
  ]
  node [
    id 324
    label "przyn&#281;ta"
  ]
  node [
    id 325
    label "metoda"
  ]
  node [
    id 326
    label "podsystem"
  ]
  node [
    id 327
    label "p&#322;&#243;d"
  ]
  node [
    id 328
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 329
    label "s&#261;d"
  ]
  node [
    id 330
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 331
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 332
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 333
    label "j&#261;dro"
  ]
  node [
    id 334
    label "eratem"
  ]
  node [
    id 335
    label "ryba"
  ]
  node [
    id 336
    label "pulpit"
  ]
  node [
    id 337
    label "struktura"
  ]
  node [
    id 338
    label "oddzia&#322;"
  ]
  node [
    id 339
    label "usenet"
  ]
  node [
    id 340
    label "o&#347;"
  ]
  node [
    id 341
    label "oprogramowanie"
  ]
  node [
    id 342
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 343
    label "poj&#281;cie"
  ]
  node [
    id 344
    label "w&#281;dkarstwo"
  ]
  node [
    id 345
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 346
    label "Leopard"
  ]
  node [
    id 347
    label "systemik"
  ]
  node [
    id 348
    label "rozprz&#261;c"
  ]
  node [
    id 349
    label "cybernetyk"
  ]
  node [
    id 350
    label "konstelacja"
  ]
  node [
    id 351
    label "doktryna"
  ]
  node [
    id 352
    label "net"
  ]
  node [
    id 353
    label "method"
  ]
  node [
    id 354
    label "systemat"
  ]
  node [
    id 355
    label "nautyka"
  ]
  node [
    id 356
    label "czynno&#347;&#263;"
  ]
  node [
    id 357
    label "dziedzina"
  ]
  node [
    id 358
    label "lotnictwo"
  ]
  node [
    id 359
    label "byd&#322;o"
  ]
  node [
    id 360
    label "zobo"
  ]
  node [
    id 361
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 362
    label "yakalo"
  ]
  node [
    id 363
    label "dzo"
  ]
  node [
    id 364
    label "prawdziwy"
  ]
  node [
    id 365
    label "ptasio"
  ]
  node [
    id 366
    label "avian"
  ]
  node [
    id 367
    label "zwierz&#281;cy"
  ]
  node [
    id 368
    label "podobny"
  ]
  node [
    id 369
    label "SLD"
  ]
  node [
    id 370
    label "forma"
  ]
  node [
    id 371
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 372
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 373
    label "ZChN"
  ]
  node [
    id 374
    label "Wigowie"
  ]
  node [
    id 375
    label "zespolik"
  ]
  node [
    id 376
    label "egzekutywa"
  ]
  node [
    id 377
    label "unit"
  ]
  node [
    id 378
    label "Mazowsze"
  ]
  node [
    id 379
    label "blok"
  ]
  node [
    id 380
    label "The_Beatles"
  ]
  node [
    id 381
    label "szko&#322;a"
  ]
  node [
    id 382
    label "Razem"
  ]
  node [
    id 383
    label "partia"
  ]
  node [
    id 384
    label "organizacja"
  ]
  node [
    id 385
    label "si&#322;a"
  ]
  node [
    id 386
    label "PiS"
  ]
  node [
    id 387
    label "zesp&#243;&#322;"
  ]
  node [
    id 388
    label "Depeche_Mode"
  ]
  node [
    id 389
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 390
    label "Bund"
  ]
  node [
    id 391
    label "AWS"
  ]
  node [
    id 392
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 393
    label "wojsko"
  ]
  node [
    id 394
    label "Kuomintang"
  ]
  node [
    id 395
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 396
    label "leksem"
  ]
  node [
    id 397
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 398
    label "Jakobici"
  ]
  node [
    id 399
    label "rocznik"
  ]
  node [
    id 400
    label "PSL"
  ]
  node [
    id 401
    label "Federali&#347;ci"
  ]
  node [
    id 402
    label "ZSL"
  ]
  node [
    id 403
    label "PPR"
  ]
  node [
    id 404
    label "rugby"
  ]
  node [
    id 405
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 406
    label "PO"
  ]
  node [
    id 407
    label "posta&#263;"
  ]
  node [
    id 408
    label "jednostka"
  ]
  node [
    id 409
    label "ramble_on"
  ]
  node [
    id 410
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 411
    label "i&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 200
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 206
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 208
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 209
  ]
  edge [
    source 11
    target 210
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 212
  ]
  edge [
    source 12
    target 213
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 215
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 216
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 229
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 232
  ]
  edge [
    source 17
    target 233
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 237
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 238
  ]
  edge [
    source 18
    target 239
  ]
  edge [
    source 18
    target 240
  ]
  edge [
    source 18
    target 241
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 242
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 243
  ]
  edge [
    source 20
    target 244
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 246
  ]
  edge [
    source 21
    target 52
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 247
  ]
  edge [
    source 22
    target 248
  ]
  edge [
    source 22
    target 249
  ]
  edge [
    source 22
    target 45
  ]
  edge [
    source 22
    target 250
  ]
  edge [
    source 22
    target 251
  ]
  edge [
    source 22
    target 252
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 254
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 256
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 257
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 259
  ]
  edge [
    source 24
    target 260
  ]
  edge [
    source 24
    target 261
  ]
  edge [
    source 24
    target 262
  ]
  edge [
    source 24
    target 263
  ]
  edge [
    source 24
    target 264
  ]
  edge [
    source 24
    target 265
  ]
  edge [
    source 24
    target 38
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 266
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 267
  ]
  edge [
    source 26
    target 268
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 269
  ]
  edge [
    source 27
    target 270
  ]
  edge [
    source 27
    target 271
  ]
  edge [
    source 27
    target 272
  ]
  edge [
    source 27
    target 273
  ]
  edge [
    source 27
    target 150
  ]
  edge [
    source 27
    target 274
  ]
  edge [
    source 27
    target 151
  ]
  edge [
    source 27
    target 275
  ]
  edge [
    source 27
    target 276
  ]
  edge [
    source 27
    target 277
  ]
  edge [
    source 27
    target 278
  ]
  edge [
    source 27
    target 279
  ]
  edge [
    source 27
    target 280
  ]
  edge [
    source 27
    target 281
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 282
  ]
  edge [
    source 28
    target 283
  ]
  edge [
    source 28
    target 284
  ]
  edge [
    source 28
    target 285
  ]
  edge [
    source 28
    target 286
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 28
    target 288
  ]
  edge [
    source 28
    target 289
  ]
  edge [
    source 28
    target 290
  ]
  edge [
    source 28
    target 291
  ]
  edge [
    source 28
    target 292
  ]
  edge [
    source 28
    target 293
  ]
  edge [
    source 28
    target 294
  ]
  edge [
    source 28
    target 295
  ]
  edge [
    source 28
    target 296
  ]
  edge [
    source 28
    target 297
  ]
  edge [
    source 28
    target 298
  ]
  edge [
    source 28
    target 299
  ]
  edge [
    source 28
    target 300
  ]
  edge [
    source 28
    target 301
  ]
  edge [
    source 28
    target 302
  ]
  edge [
    source 28
    target 303
  ]
  edge [
    source 28
    target 304
  ]
  edge [
    source 28
    target 305
  ]
  edge [
    source 28
    target 306
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 308
  ]
  edge [
    source 29
    target 309
  ]
  edge [
    source 29
    target 310
  ]
  edge [
    source 29
    target 311
  ]
  edge [
    source 29
    target 312
  ]
  edge [
    source 29
    target 313
  ]
  edge [
    source 29
    target 314
  ]
  edge [
    source 29
    target 315
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 316
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 317
  ]
  edge [
    source 31
    target 318
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 32
    target 193
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 32
    target 323
  ]
  edge [
    source 32
    target 324
  ]
  edge [
    source 32
    target 57
  ]
  edge [
    source 32
    target 325
  ]
  edge [
    source 32
    target 326
  ]
  edge [
    source 32
    target 327
  ]
  edge [
    source 32
    target 328
  ]
  edge [
    source 32
    target 329
  ]
  edge [
    source 32
    target 330
  ]
  edge [
    source 32
    target 331
  ]
  edge [
    source 32
    target 332
  ]
  edge [
    source 32
    target 333
  ]
  edge [
    source 32
    target 334
  ]
  edge [
    source 32
    target 335
  ]
  edge [
    source 32
    target 336
  ]
  edge [
    source 32
    target 337
  ]
  edge [
    source 32
    target 201
  ]
  edge [
    source 32
    target 338
  ]
  edge [
    source 32
    target 339
  ]
  edge [
    source 32
    target 340
  ]
  edge [
    source 32
    target 341
  ]
  edge [
    source 32
    target 342
  ]
  edge [
    source 32
    target 343
  ]
  edge [
    source 32
    target 344
  ]
  edge [
    source 32
    target 345
  ]
  edge [
    source 32
    target 346
  ]
  edge [
    source 32
    target 205
  ]
  edge [
    source 32
    target 347
  ]
  edge [
    source 32
    target 348
  ]
  edge [
    source 32
    target 349
  ]
  edge [
    source 32
    target 350
  ]
  edge [
    source 32
    target 351
  ]
  edge [
    source 32
    target 352
  ]
  edge [
    source 32
    target 208
  ]
  edge [
    source 32
    target 353
  ]
  edge [
    source 32
    target 354
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 355
  ]
  edge [
    source 33
    target 356
  ]
  edge [
    source 33
    target 357
  ]
  edge [
    source 33
    target 358
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 359
  ]
  edge [
    source 35
    target 360
  ]
  edge [
    source 35
    target 361
  ]
  edge [
    source 35
    target 362
  ]
  edge [
    source 35
    target 363
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 364
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 365
  ]
  edge [
    source 37
    target 366
  ]
  edge [
    source 37
    target 367
  ]
  edge [
    source 37
    target 368
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 369
  ]
  edge [
    source 38
    target 370
  ]
  edge [
    source 38
    target 371
  ]
  edge [
    source 38
    target 372
  ]
  edge [
    source 38
    target 373
  ]
  edge [
    source 38
    target 374
  ]
  edge [
    source 38
    target 375
  ]
  edge [
    source 38
    target 376
  ]
  edge [
    source 38
    target 377
  ]
  edge [
    source 38
    target 378
  ]
  edge [
    source 38
    target 379
  ]
  edge [
    source 38
    target 380
  ]
  edge [
    source 38
    target 381
  ]
  edge [
    source 38
    target 382
  ]
  edge [
    source 38
    target 383
  ]
  edge [
    source 38
    target 356
  ]
  edge [
    source 38
    target 384
  ]
  edge [
    source 38
    target 385
  ]
  edge [
    source 38
    target 386
  ]
  edge [
    source 38
    target 387
  ]
  edge [
    source 38
    target 388
  ]
  edge [
    source 38
    target 389
  ]
  edge [
    source 38
    target 390
  ]
  edge [
    source 38
    target 391
  ]
  edge [
    source 38
    target 392
  ]
  edge [
    source 38
    target 393
  ]
  edge [
    source 38
    target 394
  ]
  edge [
    source 38
    target 395
  ]
  edge [
    source 38
    target 396
  ]
  edge [
    source 38
    target 397
  ]
  edge [
    source 38
    target 398
  ]
  edge [
    source 38
    target 399
  ]
  edge [
    source 38
    target 400
  ]
  edge [
    source 38
    target 401
  ]
  edge [
    source 38
    target 402
  ]
  edge [
    source 38
    target 403
  ]
  edge [
    source 38
    target 404
  ]
  edge [
    source 38
    target 405
  ]
  edge [
    source 38
    target 263
  ]
  edge [
    source 38
    target 406
  ]
  edge [
    source 38
    target 407
  ]
  edge [
    source 38
    target 408
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 409
  ]
  edge [
    source 41
    target 410
  ]
  edge [
    source 41
    target 411
  ]
]
