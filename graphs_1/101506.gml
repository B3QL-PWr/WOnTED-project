graph [
  maxDegree 65
  minDegree 1
  meanDegree 2.0829493087557602
  density 0.009643283836832224
  graphCliqueNumber 3
  node [
    id 0
    label "gen"
    origin "text"
  ]
  node [
    id 1
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 2
    label "islandia"
    origin "text"
  ]
  node [
    id 3
    label "genetyk"
    origin "text"
  ]
  node [
    id 4
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "linia"
    origin "text"
  ]
  node [
    id 6
    label "dna"
    origin "text"
  ]
  node [
    id 7
    label "mitochondrialnego"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "wyst&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 11
    label "rodowity"
    origin "text"
  ]
  node [
    id 12
    label "ameryka"
    origin "text"
  ]
  node [
    id 13
    label "azja"
    origin "text"
  ]
  node [
    id 14
    label "wschodni"
    origin "text"
  ]
  node [
    id 15
    label "hiszpa&#324;ski"
    origin "text"
  ]
  node [
    id 16
    label "islandzki"
    origin "text"
  ]
  node [
    id 17
    label "naukowiec"
    origin "text"
  ]
  node [
    id 18
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 19
    label "pocz&#261;tkowo"
    origin "text"
  ]
  node [
    id 20
    label "by&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wsp&#243;&#322;czesny"
    origin "text"
  ]
  node [
    id 22
    label "domieszka"
    origin "text"
  ]
  node [
    id 23
    label "genetyczny"
    origin "text"
  ]
  node [
    id 24
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 25
    label "migracja"
    origin "text"
  ]
  node [
    id 26
    label "dziedziczenie"
  ]
  node [
    id 27
    label "gene"
  ]
  node [
    id 28
    label "operon"
  ]
  node [
    id 29
    label "transdukcja"
  ]
  node [
    id 30
    label "ekson"
  ]
  node [
    id 31
    label "odziedziczenie"
  ]
  node [
    id 32
    label "odziedziczy&#263;"
  ]
  node [
    id 33
    label "genom"
  ]
  node [
    id 34
    label "genotyp"
  ]
  node [
    id 35
    label "dziedziczy&#263;"
  ]
  node [
    id 36
    label "mutacja"
  ]
  node [
    id 37
    label "cz&#322;owiek"
  ]
  node [
    id 38
    label "ludno&#347;&#263;"
  ]
  node [
    id 39
    label "zwierz&#281;"
  ]
  node [
    id 40
    label "biolog"
  ]
  node [
    id 41
    label "odzyska&#263;"
  ]
  node [
    id 42
    label "devise"
  ]
  node [
    id 43
    label "oceni&#263;"
  ]
  node [
    id 44
    label "znaj&#347;&#263;"
  ]
  node [
    id 45
    label "wymy&#347;li&#263;"
  ]
  node [
    id 46
    label "invent"
  ]
  node [
    id 47
    label "pozyska&#263;"
  ]
  node [
    id 48
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 49
    label "wykry&#263;"
  ]
  node [
    id 50
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 51
    label "dozna&#263;"
  ]
  node [
    id 52
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 53
    label "koniec"
  ]
  node [
    id 54
    label "uporz&#261;dkowanie"
  ]
  node [
    id 55
    label "coalescence"
  ]
  node [
    id 56
    label "rz&#261;d"
  ]
  node [
    id 57
    label "grupa_organizm&#243;w"
  ]
  node [
    id 58
    label "curve"
  ]
  node [
    id 59
    label "kompleksja"
  ]
  node [
    id 60
    label "access"
  ]
  node [
    id 61
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 62
    label "tekst"
  ]
  node [
    id 63
    label "fragment"
  ]
  node [
    id 64
    label "cord"
  ]
  node [
    id 65
    label "przewo&#378;nik"
  ]
  node [
    id 66
    label "budowa"
  ]
  node [
    id 67
    label "granica"
  ]
  node [
    id 68
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 69
    label "szpaler"
  ]
  node [
    id 70
    label "phreaker"
  ]
  node [
    id 71
    label "tract"
  ]
  node [
    id 72
    label "sztrych"
  ]
  node [
    id 73
    label "kontakt"
  ]
  node [
    id 74
    label "spos&#243;b"
  ]
  node [
    id 75
    label "przystanek"
  ]
  node [
    id 76
    label "prowadzi&#263;"
  ]
  node [
    id 77
    label "point"
  ]
  node [
    id 78
    label "materia&#322;_zecerski"
  ]
  node [
    id 79
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 80
    label "linijka"
  ]
  node [
    id 81
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 82
    label "po&#322;&#261;czenie"
  ]
  node [
    id 83
    label "cecha"
  ]
  node [
    id 84
    label "transporter"
  ]
  node [
    id 85
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 86
    label "przeorientowywa&#263;"
  ]
  node [
    id 87
    label "bearing"
  ]
  node [
    id 88
    label "line"
  ]
  node [
    id 89
    label "trasa"
  ]
  node [
    id 90
    label "przew&#243;d"
  ]
  node [
    id 91
    label "figura_geometryczna"
  ]
  node [
    id 92
    label "kszta&#322;t"
  ]
  node [
    id 93
    label "drzewo_genealogiczne"
  ]
  node [
    id 94
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 95
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 96
    label "wygl&#261;d"
  ]
  node [
    id 97
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 98
    label "poprowadzi&#263;"
  ]
  node [
    id 99
    label "armia"
  ]
  node [
    id 100
    label "szczep"
  ]
  node [
    id 101
    label "Ural"
  ]
  node [
    id 102
    label "przeorientowa&#263;"
  ]
  node [
    id 103
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 104
    label "granice"
  ]
  node [
    id 105
    label "przeorientowanie"
  ]
  node [
    id 106
    label "billing"
  ]
  node [
    id 107
    label "prowadzenie"
  ]
  node [
    id 108
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 109
    label "zbi&#243;r"
  ]
  node [
    id 110
    label "przeorientowywanie"
  ]
  node [
    id 111
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 112
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 113
    label "jard"
  ]
  node [
    id 114
    label "podagra"
  ]
  node [
    id 115
    label "probenecyd"
  ]
  node [
    id 116
    label "schorzenie"
  ]
  node [
    id 117
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 118
    label "wychodzi&#263;"
  ]
  node [
    id 119
    label "mie&#263;_miejsce"
  ]
  node [
    id 120
    label "dzia&#322;a&#263;"
  ]
  node [
    id 121
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 122
    label "uczestniczy&#263;"
  ]
  node [
    id 123
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 124
    label "act"
  ]
  node [
    id 125
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 126
    label "unwrap"
  ]
  node [
    id 127
    label "seclude"
  ]
  node [
    id 128
    label "perform"
  ]
  node [
    id 129
    label "odst&#281;powa&#263;"
  ]
  node [
    id 130
    label "rezygnowa&#263;"
  ]
  node [
    id 131
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 132
    label "overture"
  ]
  node [
    id 133
    label "nak&#322;ania&#263;"
  ]
  node [
    id 134
    label "appear"
  ]
  node [
    id 135
    label "wy&#322;&#261;czny"
  ]
  node [
    id 136
    label "syrniki"
  ]
  node [
    id 137
    label "&#347;r&#243;dziemnomorski"
  ]
  node [
    id 138
    label "po_hiszpa&#324;sku"
  ]
  node [
    id 139
    label "fandango"
  ]
  node [
    id 140
    label "nami&#281;tny"
  ]
  node [
    id 141
    label "j&#281;zyk"
  ]
  node [
    id 142
    label "europejski"
  ]
  node [
    id 143
    label "hispanoj&#281;zyczny"
  ]
  node [
    id 144
    label "po&#322;udniowoeuropejski"
  ]
  node [
    id 145
    label "paso_doble"
  ]
  node [
    id 146
    label "hispanistyka"
  ]
  node [
    id 147
    label "Spanish"
  ]
  node [
    id 148
    label "j&#281;zyk_roma&#324;ski"
  ]
  node [
    id 149
    label "sarabanda"
  ]
  node [
    id 150
    label "pawana"
  ]
  node [
    id 151
    label "hiszpan"
  ]
  node [
    id 152
    label "p&#243;&#322;nocnoeuropejski"
  ]
  node [
    id 153
    label "Icelandic"
  ]
  node [
    id 154
    label "po_islandzku"
  ]
  node [
    id 155
    label "j&#281;zyk_germa&#324;ski"
  ]
  node [
    id 156
    label "Miczurin"
  ]
  node [
    id 157
    label "&#347;ledziciel"
  ]
  node [
    id 158
    label "uczony"
  ]
  node [
    id 159
    label "take_care"
  ]
  node [
    id 160
    label "troska&#263;_si&#281;"
  ]
  node [
    id 161
    label "zamierza&#263;"
  ]
  node [
    id 162
    label "os&#261;dza&#263;"
  ]
  node [
    id 163
    label "robi&#263;"
  ]
  node [
    id 164
    label "argue"
  ]
  node [
    id 165
    label "rozpatrywa&#263;"
  ]
  node [
    id 166
    label "deliver"
  ]
  node [
    id 167
    label "pocz&#261;tkowy"
  ]
  node [
    id 168
    label "dzieci&#281;co"
  ]
  node [
    id 169
    label "zrazu"
  ]
  node [
    id 170
    label "si&#281;ga&#263;"
  ]
  node [
    id 171
    label "trwa&#263;"
  ]
  node [
    id 172
    label "obecno&#347;&#263;"
  ]
  node [
    id 173
    label "stan"
  ]
  node [
    id 174
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 175
    label "stand"
  ]
  node [
    id 176
    label "chodzi&#263;"
  ]
  node [
    id 177
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 178
    label "equal"
  ]
  node [
    id 179
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 180
    label "tera&#378;niejszy"
  ]
  node [
    id 181
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 182
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 183
    label "unowocze&#347;nianie"
  ]
  node [
    id 184
    label "jednoczesny"
  ]
  node [
    id 185
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 186
    label "odcie&#324;"
  ]
  node [
    id 187
    label "dodatek"
  ]
  node [
    id 188
    label "sk&#322;adnik"
  ]
  node [
    id 189
    label "porcja"
  ]
  node [
    id 190
    label "dziedziczny"
  ]
  node [
    id 191
    label "genetycznie"
  ]
  node [
    id 192
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 193
    label "tobo&#322;ek"
  ]
  node [
    id 194
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 195
    label "scali&#263;"
  ]
  node [
    id 196
    label "zawi&#261;za&#263;"
  ]
  node [
    id 197
    label "zatrzyma&#263;"
  ]
  node [
    id 198
    label "form"
  ]
  node [
    id 199
    label "bind"
  ]
  node [
    id 200
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 201
    label "unify"
  ]
  node [
    id 202
    label "consort"
  ]
  node [
    id 203
    label "incorporate"
  ]
  node [
    id 204
    label "wi&#281;&#378;"
  ]
  node [
    id 205
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 206
    label "w&#281;ze&#322;"
  ]
  node [
    id 207
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 208
    label "powi&#261;za&#263;"
  ]
  node [
    id 209
    label "opakowa&#263;"
  ]
  node [
    id 210
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 211
    label "cement"
  ]
  node [
    id 212
    label "zaprawa"
  ]
  node [
    id 213
    label "relate"
  ]
  node [
    id 214
    label "exodus"
  ]
  node [
    id 215
    label "ruch"
  ]
  node [
    id 216
    label "Azja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 216
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 192
  ]
  edge [
    source 24
    target 193
  ]
  edge [
    source 24
    target 194
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 196
  ]
  edge [
    source 24
    target 197
  ]
  edge [
    source 24
    target 198
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 24
    target 201
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 79
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
]
