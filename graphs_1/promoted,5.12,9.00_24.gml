graph [
  maxDegree 63
  minDegree 1
  meanDegree 1.9705882352941178
  density 0.029411764705882353
  graphCliqueNumber 2
  node [
    id 0
    label "serce"
    origin "text"
  ]
  node [
    id 1
    label "p&#281;ka&#263;"
    origin "text"
  ]
  node [
    id 2
    label "cz&#322;owiek"
  ]
  node [
    id 3
    label "courage"
  ]
  node [
    id 4
    label "mi&#281;sie&#324;"
  ]
  node [
    id 5
    label "kompleks"
  ]
  node [
    id 6
    label "sfera_afektywna"
  ]
  node [
    id 7
    label "heart"
  ]
  node [
    id 8
    label "sumienie"
  ]
  node [
    id 9
    label "przedsionek"
  ]
  node [
    id 10
    label "entity"
  ]
  node [
    id 11
    label "nastawienie"
  ]
  node [
    id 12
    label "punkt"
  ]
  node [
    id 13
    label "kompleksja"
  ]
  node [
    id 14
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 15
    label "kier"
  ]
  node [
    id 16
    label "systol"
  ]
  node [
    id 17
    label "pikawa"
  ]
  node [
    id 18
    label "power"
  ]
  node [
    id 19
    label "zastawka"
  ]
  node [
    id 20
    label "psychika"
  ]
  node [
    id 21
    label "wola"
  ]
  node [
    id 22
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 23
    label "komora"
  ]
  node [
    id 24
    label "zapalno&#347;&#263;"
  ]
  node [
    id 25
    label "wsierdzie"
  ]
  node [
    id 26
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 27
    label "podekscytowanie"
  ]
  node [
    id 28
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 29
    label "strunowiec"
  ]
  node [
    id 30
    label "favor"
  ]
  node [
    id 31
    label "dusza"
  ]
  node [
    id 32
    label "fizjonomia"
  ]
  node [
    id 33
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 34
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 35
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 36
    label "charakter"
  ]
  node [
    id 37
    label "mikrokosmos"
  ]
  node [
    id 38
    label "dzwon"
  ]
  node [
    id 39
    label "koniuszek_serca"
  ]
  node [
    id 40
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 41
    label "passion"
  ]
  node [
    id 42
    label "cecha"
  ]
  node [
    id 43
    label "pulsowa&#263;"
  ]
  node [
    id 44
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 45
    label "organ"
  ]
  node [
    id 46
    label "ego"
  ]
  node [
    id 47
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 48
    label "kardiografia"
  ]
  node [
    id 49
    label "osobowo&#347;&#263;"
  ]
  node [
    id 50
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 51
    label "kszta&#322;t"
  ]
  node [
    id 52
    label "karta"
  ]
  node [
    id 53
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 54
    label "dobro&#263;"
  ]
  node [
    id 55
    label "podroby"
  ]
  node [
    id 56
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 57
    label "pulsowanie"
  ]
  node [
    id 58
    label "deformowa&#263;"
  ]
  node [
    id 59
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 60
    label "seksualno&#347;&#263;"
  ]
  node [
    id 61
    label "deformowanie"
  ]
  node [
    id 62
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 63
    label "elektrokardiografia"
  ]
  node [
    id 64
    label "collapse"
  ]
  node [
    id 65
    label "break"
  ]
  node [
    id 66
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 67
    label "ba&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
]
