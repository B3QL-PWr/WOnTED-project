graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "sos"
    origin "text"
  ]
  node [
    id 1
    label "bearne&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "mieszanina"
  ]
  node [
    id 3
    label "jedzenie"
  ]
  node [
    id 4
    label "gulasz"
  ]
  node [
    id 5
    label "zaklepka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
]
