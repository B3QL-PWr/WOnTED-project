graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "komisja"
    origin "text"
  ]
  node [
    id 1
    label "europejski"
    origin "text"
  ]
  node [
    id 2
    label "obrady"
  ]
  node [
    id 3
    label "zesp&#243;&#322;"
  ]
  node [
    id 4
    label "organ"
  ]
  node [
    id 5
    label "Komisja_Europejska"
  ]
  node [
    id 6
    label "podkomisja"
  ]
  node [
    id 7
    label "European"
  ]
  node [
    id 8
    label "po_europejsku"
  ]
  node [
    id 9
    label "charakterystyczny"
  ]
  node [
    id 10
    label "europejsko"
  ]
  node [
    id 11
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 12
    label "typowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
]
