graph [
  maxDegree 16
  minDegree 1
  meanDegree 2.032258064516129
  density 0.033315705975674244
  graphCliqueNumber 2
  node [
    id 0
    label "daleki"
    origin "text"
  ]
  node [
    id 1
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wczesno"
    origin "text"
  ]
  node [
    id 3
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sprawa"
    origin "text"
  ]
  node [
    id 5
    label "molestowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "latki"
    origin "text"
  ]
  node [
    id 7
    label "komenda"
    origin "text"
  ]
  node [
    id 8
    label "sto&#322;eczny"
    origin "text"
  ]
  node [
    id 9
    label "policja"
    origin "text"
  ]
  node [
    id 10
    label "dawny"
  ]
  node [
    id 11
    label "du&#380;y"
  ]
  node [
    id 12
    label "s&#322;aby"
  ]
  node [
    id 13
    label "oddalony"
  ]
  node [
    id 14
    label "daleko"
  ]
  node [
    id 15
    label "przysz&#322;y"
  ]
  node [
    id 16
    label "ogl&#281;dny"
  ]
  node [
    id 17
    label "r&#243;&#380;ny"
  ]
  node [
    id 18
    label "g&#322;&#281;boki"
  ]
  node [
    id 19
    label "odlegle"
  ]
  node [
    id 20
    label "nieobecny"
  ]
  node [
    id 21
    label "odleg&#322;y"
  ]
  node [
    id 22
    label "d&#322;ugi"
  ]
  node [
    id 23
    label "zwi&#261;zany"
  ]
  node [
    id 24
    label "obcy"
  ]
  node [
    id 25
    label "whole"
  ]
  node [
    id 26
    label "Rzym_Zachodni"
  ]
  node [
    id 27
    label "element"
  ]
  node [
    id 28
    label "ilo&#347;&#263;"
  ]
  node [
    id 29
    label "urz&#261;dzenie"
  ]
  node [
    id 30
    label "Rzym_Wschodni"
  ]
  node [
    id 31
    label "wcze&#347;nie"
  ]
  node [
    id 32
    label "zapoznawa&#263;"
  ]
  node [
    id 33
    label "represent"
  ]
  node [
    id 34
    label "temat"
  ]
  node [
    id 35
    label "kognicja"
  ]
  node [
    id 36
    label "idea"
  ]
  node [
    id 37
    label "szczeg&#243;&#322;"
  ]
  node [
    id 38
    label "rzecz"
  ]
  node [
    id 39
    label "wydarzenie"
  ]
  node [
    id 40
    label "przes&#322;anka"
  ]
  node [
    id 41
    label "rozprawa"
  ]
  node [
    id 42
    label "object"
  ]
  node [
    id 43
    label "proposition"
  ]
  node [
    id 44
    label "wykorzystywa&#263;"
  ]
  node [
    id 45
    label "zmusza&#263;"
  ]
  node [
    id 46
    label "nudzi&#263;"
  ]
  node [
    id 47
    label "trouble_oneself"
  ]
  node [
    id 48
    label "prosi&#263;"
  ]
  node [
    id 49
    label "psiarnia"
  ]
  node [
    id 50
    label "formu&#322;a"
  ]
  node [
    id 51
    label "posterunek"
  ]
  node [
    id 52
    label "sygna&#322;"
  ]
  node [
    id 53
    label "komender&#243;wka"
  ]
  node [
    id 54
    label "polecenie"
  ]
  node [
    id 55
    label "direction"
  ]
  node [
    id 56
    label "miejski"
  ]
  node [
    id 57
    label "sto&#322;ecznie"
  ]
  node [
    id 58
    label "komisariat"
  ]
  node [
    id 59
    label "grupa"
  ]
  node [
    id 60
    label "organ"
  ]
  node [
    id 61
    label "s&#322;u&#380;ba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
]
