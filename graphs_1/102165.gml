graph [
  maxDegree 16
  minDegree 1
  meanDegree 2.5
  density 0.08064516129032258
  graphCliqueNumber 6
  node [
    id 0
    label "wymaga&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dokument"
    origin "text"
  ]
  node [
    id 2
    label "zmusza&#263;"
  ]
  node [
    id 3
    label "by&#263;"
  ]
  node [
    id 4
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 5
    label "claim"
  ]
  node [
    id 6
    label "force"
  ]
  node [
    id 7
    label "take"
  ]
  node [
    id 8
    label "record"
  ]
  node [
    id 9
    label "wytw&#243;r"
  ]
  node [
    id 10
    label "&#347;wiadectwo"
  ]
  node [
    id 11
    label "zapis"
  ]
  node [
    id 12
    label "fascyku&#322;"
  ]
  node [
    id 13
    label "raport&#243;wka"
  ]
  node [
    id 14
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 15
    label "artyku&#322;"
  ]
  node [
    id 16
    label "plik"
  ]
  node [
    id 17
    label "writing"
  ]
  node [
    id 18
    label "utw&#243;r"
  ]
  node [
    id 19
    label "dokumentacja"
  ]
  node [
    id 20
    label "parafa"
  ]
  node [
    id 21
    label "sygnatariusz"
  ]
  node [
    id 22
    label "registratura"
  ]
  node [
    id 23
    label "urz&#261;d"
  ]
  node [
    id 24
    label "m&#281;ski"
  ]
  node [
    id 25
    label "starszy"
  ]
  node [
    id 26
    label "warszawa"
  ]
  node [
    id 27
    label "wydzia&#322;"
  ]
  node [
    id 28
    label "obs&#322;uga"
  ]
  node [
    id 29
    label "mieszkaniec"
  ]
  node [
    id 30
    label "stan"
  ]
  node [
    id 31
    label "cywilny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
]
