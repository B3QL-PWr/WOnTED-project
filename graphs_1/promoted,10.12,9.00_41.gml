graph [
  maxDegree 152
  minDegree 1
  meanDegree 1.9881656804733727
  density 0.011834319526627219
  graphCliqueNumber 2
  node [
    id 0
    label "przep&#281;dzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "aktywista"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;asny"
    origin "text"
  ]
  node [
    id 3
    label "ziemia"
    origin "text"
  ]
  node [
    id 4
    label "tug"
  ]
  node [
    id 5
    label "usun&#261;&#263;"
  ]
  node [
    id 6
    label "zmusi&#263;"
  ]
  node [
    id 7
    label "authorize"
  ]
  node [
    id 8
    label "zwolennik"
  ]
  node [
    id 9
    label "cz&#322;onek"
  ]
  node [
    id 10
    label "Owsiak"
  ]
  node [
    id 11
    label "Michnik"
  ]
  node [
    id 12
    label "Asnyk"
  ]
  node [
    id 13
    label "swoisty"
  ]
  node [
    id 14
    label "czyj&#347;"
  ]
  node [
    id 15
    label "osobny"
  ]
  node [
    id 16
    label "zwi&#261;zany"
  ]
  node [
    id 17
    label "samodzielny"
  ]
  node [
    id 18
    label "Skandynawia"
  ]
  node [
    id 19
    label "Yorkshire"
  ]
  node [
    id 20
    label "Kaukaz"
  ]
  node [
    id 21
    label "Kaszmir"
  ]
  node [
    id 22
    label "Toskania"
  ]
  node [
    id 23
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 24
    label "&#321;emkowszczyzna"
  ]
  node [
    id 25
    label "obszar"
  ]
  node [
    id 26
    label "Amhara"
  ]
  node [
    id 27
    label "Lombardia"
  ]
  node [
    id 28
    label "Podbeskidzie"
  ]
  node [
    id 29
    label "Kalabria"
  ]
  node [
    id 30
    label "kort"
  ]
  node [
    id 31
    label "Tyrol"
  ]
  node [
    id 32
    label "Pamir"
  ]
  node [
    id 33
    label "Lubelszczyzna"
  ]
  node [
    id 34
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 35
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 36
    label "&#379;ywiecczyzna"
  ]
  node [
    id 37
    label "ryzosfera"
  ]
  node [
    id 38
    label "Europa_Wschodnia"
  ]
  node [
    id 39
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 40
    label "Zabajkale"
  ]
  node [
    id 41
    label "Kaszuby"
  ]
  node [
    id 42
    label "Bo&#347;nia"
  ]
  node [
    id 43
    label "Noworosja"
  ]
  node [
    id 44
    label "Ba&#322;kany"
  ]
  node [
    id 45
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 46
    label "Anglia"
  ]
  node [
    id 47
    label "Kielecczyzna"
  ]
  node [
    id 48
    label "Pomorze_Zachodnie"
  ]
  node [
    id 49
    label "Opolskie"
  ]
  node [
    id 50
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 51
    label "skorupa_ziemska"
  ]
  node [
    id 52
    label "Ko&#322;yma"
  ]
  node [
    id 53
    label "Oksytania"
  ]
  node [
    id 54
    label "Syjon"
  ]
  node [
    id 55
    label "posadzka"
  ]
  node [
    id 56
    label "pa&#324;stwo"
  ]
  node [
    id 57
    label "Kociewie"
  ]
  node [
    id 58
    label "Huculszczyzna"
  ]
  node [
    id 59
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 60
    label "budynek"
  ]
  node [
    id 61
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 62
    label "Bawaria"
  ]
  node [
    id 63
    label "pomieszczenie"
  ]
  node [
    id 64
    label "pr&#243;chnica"
  ]
  node [
    id 65
    label "glinowanie"
  ]
  node [
    id 66
    label "Maghreb"
  ]
  node [
    id 67
    label "Bory_Tucholskie"
  ]
  node [
    id 68
    label "Europa_Zachodnia"
  ]
  node [
    id 69
    label "Kerala"
  ]
  node [
    id 70
    label "Podhale"
  ]
  node [
    id 71
    label "Kabylia"
  ]
  node [
    id 72
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 73
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 74
    label "Ma&#322;opolska"
  ]
  node [
    id 75
    label "Polesie"
  ]
  node [
    id 76
    label "Liguria"
  ]
  node [
    id 77
    label "&#321;&#243;dzkie"
  ]
  node [
    id 78
    label "geosystem"
  ]
  node [
    id 79
    label "Palestyna"
  ]
  node [
    id 80
    label "Bojkowszczyzna"
  ]
  node [
    id 81
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 82
    label "Karaiby"
  ]
  node [
    id 83
    label "S&#261;decczyzna"
  ]
  node [
    id 84
    label "Sand&#380;ak"
  ]
  node [
    id 85
    label "Nadrenia"
  ]
  node [
    id 86
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 87
    label "Zakarpacie"
  ]
  node [
    id 88
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 89
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 90
    label "Zag&#243;rze"
  ]
  node [
    id 91
    label "Andaluzja"
  ]
  node [
    id 92
    label "Turkiestan"
  ]
  node [
    id 93
    label "Naddniestrze"
  ]
  node [
    id 94
    label "Hercegowina"
  ]
  node [
    id 95
    label "p&#322;aszczyzna"
  ]
  node [
    id 96
    label "Opolszczyzna"
  ]
  node [
    id 97
    label "jednostka_administracyjna"
  ]
  node [
    id 98
    label "Lotaryngia"
  ]
  node [
    id 99
    label "Afryka_Wschodnia"
  ]
  node [
    id 100
    label "Szlezwik"
  ]
  node [
    id 101
    label "powierzchnia"
  ]
  node [
    id 102
    label "glinowa&#263;"
  ]
  node [
    id 103
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 104
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 105
    label "podglebie"
  ]
  node [
    id 106
    label "Mazowsze"
  ]
  node [
    id 107
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 108
    label "teren"
  ]
  node [
    id 109
    label "Afryka_Zachodnia"
  ]
  node [
    id 110
    label "czynnik_produkcji"
  ]
  node [
    id 111
    label "Galicja"
  ]
  node [
    id 112
    label "Szkocja"
  ]
  node [
    id 113
    label "Walia"
  ]
  node [
    id 114
    label "Powi&#347;le"
  ]
  node [
    id 115
    label "penetrator"
  ]
  node [
    id 116
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 117
    label "kompleks_sorpcyjny"
  ]
  node [
    id 118
    label "Zamojszczyzna"
  ]
  node [
    id 119
    label "Kujawy"
  ]
  node [
    id 120
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 121
    label "Podlasie"
  ]
  node [
    id 122
    label "Laponia"
  ]
  node [
    id 123
    label "Umbria"
  ]
  node [
    id 124
    label "plantowa&#263;"
  ]
  node [
    id 125
    label "Mezoameryka"
  ]
  node [
    id 126
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 127
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 128
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 129
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 130
    label "Kurdystan"
  ]
  node [
    id 131
    label "Kampania"
  ]
  node [
    id 132
    label "Armagnac"
  ]
  node [
    id 133
    label "Polinezja"
  ]
  node [
    id 134
    label "Warmia"
  ]
  node [
    id 135
    label "Wielkopolska"
  ]
  node [
    id 136
    label "litosfera"
  ]
  node [
    id 137
    label "Bordeaux"
  ]
  node [
    id 138
    label "Lauda"
  ]
  node [
    id 139
    label "Mazury"
  ]
  node [
    id 140
    label "Podkarpacie"
  ]
  node [
    id 141
    label "Oceania"
  ]
  node [
    id 142
    label "Lasko"
  ]
  node [
    id 143
    label "Amazonia"
  ]
  node [
    id 144
    label "pojazd"
  ]
  node [
    id 145
    label "glej"
  ]
  node [
    id 146
    label "martwica"
  ]
  node [
    id 147
    label "zapadnia"
  ]
  node [
    id 148
    label "przestrze&#324;"
  ]
  node [
    id 149
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 150
    label "dotleni&#263;"
  ]
  node [
    id 151
    label "Kurpie"
  ]
  node [
    id 152
    label "Tonkin"
  ]
  node [
    id 153
    label "Azja_Wschodnia"
  ]
  node [
    id 154
    label "Mikronezja"
  ]
  node [
    id 155
    label "Ukraina_Zachodnia"
  ]
  node [
    id 156
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 157
    label "Turyngia"
  ]
  node [
    id 158
    label "Baszkiria"
  ]
  node [
    id 159
    label "Apulia"
  ]
  node [
    id 160
    label "miejsce"
  ]
  node [
    id 161
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 162
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 163
    label "Indochiny"
  ]
  node [
    id 164
    label "Biskupizna"
  ]
  node [
    id 165
    label "Lubuskie"
  ]
  node [
    id 166
    label "domain"
  ]
  node [
    id 167
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 168
    label "Ameryka_&#321;aci&#324;ska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
]
