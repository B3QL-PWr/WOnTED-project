graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.945945945945946
  density 0.05405405405405406
  graphCliqueNumber 2
  node [
    id 0
    label "zmiana"
    origin "text"
  ]
  node [
    id 1
    label "prze&#322;o&#380;enie"
    origin "text"
  ]
  node [
    id 2
    label "anatomopatolog"
  ]
  node [
    id 3
    label "rewizja"
  ]
  node [
    id 4
    label "oznaka"
  ]
  node [
    id 5
    label "czas"
  ]
  node [
    id 6
    label "ferment"
  ]
  node [
    id 7
    label "komplet"
  ]
  node [
    id 8
    label "tura"
  ]
  node [
    id 9
    label "amendment"
  ]
  node [
    id 10
    label "zmianka"
  ]
  node [
    id 11
    label "odmienianie"
  ]
  node [
    id 12
    label "passage"
  ]
  node [
    id 13
    label "zjawisko"
  ]
  node [
    id 14
    label "change"
  ]
  node [
    id 15
    label "praca"
  ]
  node [
    id 16
    label "zmienienie"
  ]
  node [
    id 17
    label "po&#322;o&#380;enie"
  ]
  node [
    id 18
    label "proporcja"
  ]
  node [
    id 19
    label "prym"
  ]
  node [
    id 20
    label "rendition"
  ]
  node [
    id 21
    label "j&#281;zyk"
  ]
  node [
    id 22
    label "uznanie"
  ]
  node [
    id 23
    label "ratio"
  ]
  node [
    id 24
    label "zrobienie"
  ]
  node [
    id 25
    label "czynno&#347;&#263;"
  ]
  node [
    id 26
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 27
    label "marriage"
  ]
  node [
    id 28
    label "marketing_afiliacyjny"
  ]
  node [
    id 29
    label "zekranizowanie"
  ]
  node [
    id 30
    label "przeniesienie"
  ]
  node [
    id 31
    label "w&#322;o&#380;enie"
  ]
  node [
    id 32
    label "przedstawienie"
  ]
  node [
    id 33
    label "przemieszczenie"
  ]
  node [
    id 34
    label "move"
  ]
  node [
    id 35
    label "transfer"
  ]
  node [
    id 36
    label "wyra&#380;enie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
]
