graph [
  maxDegree 326
  minDegree 1
  meanDegree 2.047531992687386
  density 0.0037500585946655418
  graphCliqueNumber 3
  node [
    id 0
    label "g&#322;owi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "taki"
    origin "text"
  ]
  node [
    id 3
    label "benkler"
    origin "text"
  ]
  node [
    id 4
    label "jak"
    origin "text"
  ]
  node [
    id 5
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 6
    label "biedny"
    origin "text"
  ]
  node [
    id 7
    label "kraj"
    origin "text"
  ]
  node [
    id 8
    label "tworzenie"
    origin "text"
  ]
  node [
    id 9
    label "odpowiedni"
    origin "text"
  ]
  node [
    id 10
    label "ro&#347;lina"
    origin "text"
  ]
  node [
    id 11
    label "wymy&#347;la&#263;"
    origin "text"
  ]
  node [
    id 12
    label "peer"
    origin "text"
  ]
  node [
    id 13
    label "production"
    origin "text"
  ]
  node [
    id 14
    label "dzielenie"
    origin "text"
  ]
  node [
    id 15
    label "licencja"
    origin "text"
  ]
  node [
    id 16
    label "przez"
    origin "text"
  ]
  node [
    id 17
    label "uniwersytet"
    origin "text"
  ]
  node [
    id 18
    label "udost&#281;pnia&#263;"
    origin "text"
  ]
  node [
    id 19
    label "nasienie"
    origin "text"
  ]
  node [
    id 20
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 21
    label "tak"
    origin "text"
  ]
  node [
    id 22
    label "nic"
    origin "text"
  ]
  node [
    id 23
    label "rozwini&#281;ta"
    origin "text"
  ]
  node [
    id 24
    label "przyda&#263;"
    origin "text"
  ]
  node [
    id 25
    label "tymczasem"
    origin "text"
  ]
  node [
    id 26
    label "w&#322;adza"
    origin "text"
  ]
  node [
    id 27
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 28
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 29
    label "nowy"
    origin "text"
  ]
  node [
    id 30
    label "dobry"
    origin "text"
  ]
  node [
    id 31
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 32
    label "okre&#347;lony"
  ]
  node [
    id 33
    label "jaki&#347;"
  ]
  node [
    id 34
    label "byd&#322;o"
  ]
  node [
    id 35
    label "zobo"
  ]
  node [
    id 36
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 37
    label "yakalo"
  ]
  node [
    id 38
    label "dzo"
  ]
  node [
    id 39
    label "help"
  ]
  node [
    id 40
    label "aid"
  ]
  node [
    id 41
    label "u&#322;atwi&#263;"
  ]
  node [
    id 42
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 43
    label "concur"
  ]
  node [
    id 44
    label "zrobi&#263;"
  ]
  node [
    id 45
    label "zaskutkowa&#263;"
  ]
  node [
    id 46
    label "cz&#322;owiek"
  ]
  node [
    id 47
    label "ho&#322;ysz"
  ]
  node [
    id 48
    label "s&#322;aby"
  ]
  node [
    id 49
    label "ubo&#380;enie"
  ]
  node [
    id 50
    label "nieszcz&#281;&#347;liwy"
  ]
  node [
    id 51
    label "zubo&#380;anie"
  ]
  node [
    id 52
    label "biedota"
  ]
  node [
    id 53
    label "zubo&#380;enie"
  ]
  node [
    id 54
    label "n&#281;dzny"
  ]
  node [
    id 55
    label "bankrutowanie"
  ]
  node [
    id 56
    label "proletariusz"
  ]
  node [
    id 57
    label "go&#322;odupiec"
  ]
  node [
    id 58
    label "biednie"
  ]
  node [
    id 59
    label "zbiednienie"
  ]
  node [
    id 60
    label "sytuowany"
  ]
  node [
    id 61
    label "raw_material"
  ]
  node [
    id 62
    label "Skandynawia"
  ]
  node [
    id 63
    label "Filipiny"
  ]
  node [
    id 64
    label "Rwanda"
  ]
  node [
    id 65
    label "Kaukaz"
  ]
  node [
    id 66
    label "Kaszmir"
  ]
  node [
    id 67
    label "Toskania"
  ]
  node [
    id 68
    label "Yorkshire"
  ]
  node [
    id 69
    label "&#321;emkowszczyzna"
  ]
  node [
    id 70
    label "obszar"
  ]
  node [
    id 71
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 72
    label "Monako"
  ]
  node [
    id 73
    label "Amhara"
  ]
  node [
    id 74
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 75
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 76
    label "Lombardia"
  ]
  node [
    id 77
    label "Korea"
  ]
  node [
    id 78
    label "Kalabria"
  ]
  node [
    id 79
    label "Ghana"
  ]
  node [
    id 80
    label "Czarnog&#243;ra"
  ]
  node [
    id 81
    label "Tyrol"
  ]
  node [
    id 82
    label "Malawi"
  ]
  node [
    id 83
    label "Indonezja"
  ]
  node [
    id 84
    label "Bu&#322;garia"
  ]
  node [
    id 85
    label "Nauru"
  ]
  node [
    id 86
    label "Kenia"
  ]
  node [
    id 87
    label "Pamir"
  ]
  node [
    id 88
    label "Kambod&#380;a"
  ]
  node [
    id 89
    label "Lubelszczyzna"
  ]
  node [
    id 90
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 91
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 92
    label "Mali"
  ]
  node [
    id 93
    label "&#379;ywiecczyzna"
  ]
  node [
    id 94
    label "Austria"
  ]
  node [
    id 95
    label "interior"
  ]
  node [
    id 96
    label "Europa_Wschodnia"
  ]
  node [
    id 97
    label "Armenia"
  ]
  node [
    id 98
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 99
    label "Fid&#380;i"
  ]
  node [
    id 100
    label "Tuwalu"
  ]
  node [
    id 101
    label "Zabajkale"
  ]
  node [
    id 102
    label "Etiopia"
  ]
  node [
    id 103
    label "Malta"
  ]
  node [
    id 104
    label "Malezja"
  ]
  node [
    id 105
    label "Kaszuby"
  ]
  node [
    id 106
    label "Bo&#347;nia"
  ]
  node [
    id 107
    label "Noworosja"
  ]
  node [
    id 108
    label "Grenada"
  ]
  node [
    id 109
    label "Tad&#380;ykistan"
  ]
  node [
    id 110
    label "Ba&#322;kany"
  ]
  node [
    id 111
    label "Wehrlen"
  ]
  node [
    id 112
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 113
    label "Anglia"
  ]
  node [
    id 114
    label "Kielecczyzna"
  ]
  node [
    id 115
    label "Rumunia"
  ]
  node [
    id 116
    label "Pomorze_Zachodnie"
  ]
  node [
    id 117
    label "Maroko"
  ]
  node [
    id 118
    label "Bhutan"
  ]
  node [
    id 119
    label "Opolskie"
  ]
  node [
    id 120
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 121
    label "Ko&#322;yma"
  ]
  node [
    id 122
    label "Oksytania"
  ]
  node [
    id 123
    label "S&#322;owacja"
  ]
  node [
    id 124
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 125
    label "Seszele"
  ]
  node [
    id 126
    label "Syjon"
  ]
  node [
    id 127
    label "Kuwejt"
  ]
  node [
    id 128
    label "Arabia_Saudyjska"
  ]
  node [
    id 129
    label "Kociewie"
  ]
  node [
    id 130
    label "Ekwador"
  ]
  node [
    id 131
    label "Kanada"
  ]
  node [
    id 132
    label "ziemia"
  ]
  node [
    id 133
    label "Japonia"
  ]
  node [
    id 134
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 135
    label "Hiszpania"
  ]
  node [
    id 136
    label "Wyspy_Marshalla"
  ]
  node [
    id 137
    label "Botswana"
  ]
  node [
    id 138
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 139
    label "D&#380;ibuti"
  ]
  node [
    id 140
    label "Huculszczyzna"
  ]
  node [
    id 141
    label "Wietnam"
  ]
  node [
    id 142
    label "Egipt"
  ]
  node [
    id 143
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 144
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 145
    label "Burkina_Faso"
  ]
  node [
    id 146
    label "Bawaria"
  ]
  node [
    id 147
    label "Niemcy"
  ]
  node [
    id 148
    label "Khitai"
  ]
  node [
    id 149
    label "Macedonia"
  ]
  node [
    id 150
    label "Albania"
  ]
  node [
    id 151
    label "Madagaskar"
  ]
  node [
    id 152
    label "Bahrajn"
  ]
  node [
    id 153
    label "Jemen"
  ]
  node [
    id 154
    label "Lesoto"
  ]
  node [
    id 155
    label "Maghreb"
  ]
  node [
    id 156
    label "Samoa"
  ]
  node [
    id 157
    label "Andora"
  ]
  node [
    id 158
    label "Bory_Tucholskie"
  ]
  node [
    id 159
    label "Chiny"
  ]
  node [
    id 160
    label "Europa_Zachodnia"
  ]
  node [
    id 161
    label "Cypr"
  ]
  node [
    id 162
    label "Wielka_Brytania"
  ]
  node [
    id 163
    label "Kerala"
  ]
  node [
    id 164
    label "Podhale"
  ]
  node [
    id 165
    label "Kabylia"
  ]
  node [
    id 166
    label "Ukraina"
  ]
  node [
    id 167
    label "Paragwaj"
  ]
  node [
    id 168
    label "Trynidad_i_Tobago"
  ]
  node [
    id 169
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 170
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 171
    label "Ma&#322;opolska"
  ]
  node [
    id 172
    label "Polesie"
  ]
  node [
    id 173
    label "Liguria"
  ]
  node [
    id 174
    label "Libia"
  ]
  node [
    id 175
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 176
    label "&#321;&#243;dzkie"
  ]
  node [
    id 177
    label "Surinam"
  ]
  node [
    id 178
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 179
    label "Palestyna"
  ]
  node [
    id 180
    label "Australia"
  ]
  node [
    id 181
    label "Nigeria"
  ]
  node [
    id 182
    label "Honduras"
  ]
  node [
    id 183
    label "Bojkowszczyzna"
  ]
  node [
    id 184
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 185
    label "Karaiby"
  ]
  node [
    id 186
    label "Bangladesz"
  ]
  node [
    id 187
    label "Peru"
  ]
  node [
    id 188
    label "Kazachstan"
  ]
  node [
    id 189
    label "USA"
  ]
  node [
    id 190
    label "Irak"
  ]
  node [
    id 191
    label "Nepal"
  ]
  node [
    id 192
    label "S&#261;decczyzna"
  ]
  node [
    id 193
    label "Sudan"
  ]
  node [
    id 194
    label "Sand&#380;ak"
  ]
  node [
    id 195
    label "Nadrenia"
  ]
  node [
    id 196
    label "San_Marino"
  ]
  node [
    id 197
    label "Burundi"
  ]
  node [
    id 198
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 199
    label "Dominikana"
  ]
  node [
    id 200
    label "Komory"
  ]
  node [
    id 201
    label "Zakarpacie"
  ]
  node [
    id 202
    label "Gwatemala"
  ]
  node [
    id 203
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 204
    label "Zag&#243;rze"
  ]
  node [
    id 205
    label "Andaluzja"
  ]
  node [
    id 206
    label "granica_pa&#324;stwa"
  ]
  node [
    id 207
    label "Turkiestan"
  ]
  node [
    id 208
    label "Naddniestrze"
  ]
  node [
    id 209
    label "Hercegowina"
  ]
  node [
    id 210
    label "Brunei"
  ]
  node [
    id 211
    label "Iran"
  ]
  node [
    id 212
    label "jednostka_administracyjna"
  ]
  node [
    id 213
    label "Zimbabwe"
  ]
  node [
    id 214
    label "Namibia"
  ]
  node [
    id 215
    label "Meksyk"
  ]
  node [
    id 216
    label "Lotaryngia"
  ]
  node [
    id 217
    label "Kamerun"
  ]
  node [
    id 218
    label "Opolszczyzna"
  ]
  node [
    id 219
    label "Afryka_Wschodnia"
  ]
  node [
    id 220
    label "Szlezwik"
  ]
  node [
    id 221
    label "Somalia"
  ]
  node [
    id 222
    label "Angola"
  ]
  node [
    id 223
    label "Gabon"
  ]
  node [
    id 224
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 225
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 226
    label "Mozambik"
  ]
  node [
    id 227
    label "Tajwan"
  ]
  node [
    id 228
    label "Tunezja"
  ]
  node [
    id 229
    label "Nowa_Zelandia"
  ]
  node [
    id 230
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 231
    label "Podbeskidzie"
  ]
  node [
    id 232
    label "Liban"
  ]
  node [
    id 233
    label "Jordania"
  ]
  node [
    id 234
    label "Tonga"
  ]
  node [
    id 235
    label "Czad"
  ]
  node [
    id 236
    label "Liberia"
  ]
  node [
    id 237
    label "Gwinea"
  ]
  node [
    id 238
    label "Belize"
  ]
  node [
    id 239
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 240
    label "Mazowsze"
  ]
  node [
    id 241
    label "&#321;otwa"
  ]
  node [
    id 242
    label "Syria"
  ]
  node [
    id 243
    label "Benin"
  ]
  node [
    id 244
    label "Afryka_Zachodnia"
  ]
  node [
    id 245
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 246
    label "Dominika"
  ]
  node [
    id 247
    label "Antigua_i_Barbuda"
  ]
  node [
    id 248
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 249
    label "Hanower"
  ]
  node [
    id 250
    label "Galicja"
  ]
  node [
    id 251
    label "Szkocja"
  ]
  node [
    id 252
    label "Walia"
  ]
  node [
    id 253
    label "Afganistan"
  ]
  node [
    id 254
    label "Kiribati"
  ]
  node [
    id 255
    label "W&#322;ochy"
  ]
  node [
    id 256
    label "Szwajcaria"
  ]
  node [
    id 257
    label "Powi&#347;le"
  ]
  node [
    id 258
    label "Sahara_Zachodnia"
  ]
  node [
    id 259
    label "Chorwacja"
  ]
  node [
    id 260
    label "Tajlandia"
  ]
  node [
    id 261
    label "Salwador"
  ]
  node [
    id 262
    label "Bahamy"
  ]
  node [
    id 263
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 264
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 265
    label "Zamojszczyzna"
  ]
  node [
    id 266
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 267
    label "S&#322;owenia"
  ]
  node [
    id 268
    label "Gambia"
  ]
  node [
    id 269
    label "Kujawy"
  ]
  node [
    id 270
    label "Urugwaj"
  ]
  node [
    id 271
    label "Podlasie"
  ]
  node [
    id 272
    label "Zair"
  ]
  node [
    id 273
    label "Erytrea"
  ]
  node [
    id 274
    label "Laponia"
  ]
  node [
    id 275
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 276
    label "Umbria"
  ]
  node [
    id 277
    label "Rosja"
  ]
  node [
    id 278
    label "Uganda"
  ]
  node [
    id 279
    label "Niger"
  ]
  node [
    id 280
    label "Mauritius"
  ]
  node [
    id 281
    label "Turkmenistan"
  ]
  node [
    id 282
    label "Turcja"
  ]
  node [
    id 283
    label "Mezoameryka"
  ]
  node [
    id 284
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 285
    label "Irlandia"
  ]
  node [
    id 286
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 287
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 288
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 289
    label "Gwinea_Bissau"
  ]
  node [
    id 290
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 291
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 292
    label "Kurdystan"
  ]
  node [
    id 293
    label "Belgia"
  ]
  node [
    id 294
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 295
    label "Palau"
  ]
  node [
    id 296
    label "Barbados"
  ]
  node [
    id 297
    label "Chile"
  ]
  node [
    id 298
    label "Wenezuela"
  ]
  node [
    id 299
    label "W&#281;gry"
  ]
  node [
    id 300
    label "Argentyna"
  ]
  node [
    id 301
    label "Kolumbia"
  ]
  node [
    id 302
    label "Kampania"
  ]
  node [
    id 303
    label "Armagnac"
  ]
  node [
    id 304
    label "Sierra_Leone"
  ]
  node [
    id 305
    label "Azerbejd&#380;an"
  ]
  node [
    id 306
    label "Kongo"
  ]
  node [
    id 307
    label "Polinezja"
  ]
  node [
    id 308
    label "Warmia"
  ]
  node [
    id 309
    label "Pakistan"
  ]
  node [
    id 310
    label "Liechtenstein"
  ]
  node [
    id 311
    label "Wielkopolska"
  ]
  node [
    id 312
    label "Nikaragua"
  ]
  node [
    id 313
    label "Senegal"
  ]
  node [
    id 314
    label "brzeg"
  ]
  node [
    id 315
    label "Bordeaux"
  ]
  node [
    id 316
    label "Lauda"
  ]
  node [
    id 317
    label "Indie"
  ]
  node [
    id 318
    label "Mazury"
  ]
  node [
    id 319
    label "Suazi"
  ]
  node [
    id 320
    label "Polska"
  ]
  node [
    id 321
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 322
    label "Algieria"
  ]
  node [
    id 323
    label "Jamajka"
  ]
  node [
    id 324
    label "Timor_Wschodni"
  ]
  node [
    id 325
    label "Oceania"
  ]
  node [
    id 326
    label "Kostaryka"
  ]
  node [
    id 327
    label "Podkarpacie"
  ]
  node [
    id 328
    label "Lasko"
  ]
  node [
    id 329
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 330
    label "Kuba"
  ]
  node [
    id 331
    label "Mauretania"
  ]
  node [
    id 332
    label "Amazonia"
  ]
  node [
    id 333
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 334
    label "Portoryko"
  ]
  node [
    id 335
    label "Brazylia"
  ]
  node [
    id 336
    label "Mo&#322;dawia"
  ]
  node [
    id 337
    label "organizacja"
  ]
  node [
    id 338
    label "Litwa"
  ]
  node [
    id 339
    label "Kirgistan"
  ]
  node [
    id 340
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 341
    label "Izrael"
  ]
  node [
    id 342
    label "Grecja"
  ]
  node [
    id 343
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 344
    label "Kurpie"
  ]
  node [
    id 345
    label "Holandia"
  ]
  node [
    id 346
    label "Sri_Lanka"
  ]
  node [
    id 347
    label "Tonkin"
  ]
  node [
    id 348
    label "Katar"
  ]
  node [
    id 349
    label "Azja_Wschodnia"
  ]
  node [
    id 350
    label "Mikronezja"
  ]
  node [
    id 351
    label "Ukraina_Zachodnia"
  ]
  node [
    id 352
    label "Laos"
  ]
  node [
    id 353
    label "Mongolia"
  ]
  node [
    id 354
    label "Turyngia"
  ]
  node [
    id 355
    label "Malediwy"
  ]
  node [
    id 356
    label "Zambia"
  ]
  node [
    id 357
    label "Baszkiria"
  ]
  node [
    id 358
    label "Tanzania"
  ]
  node [
    id 359
    label "Gujana"
  ]
  node [
    id 360
    label "Apulia"
  ]
  node [
    id 361
    label "Czechy"
  ]
  node [
    id 362
    label "Panama"
  ]
  node [
    id 363
    label "Uzbekistan"
  ]
  node [
    id 364
    label "Gruzja"
  ]
  node [
    id 365
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 366
    label "Serbia"
  ]
  node [
    id 367
    label "Francja"
  ]
  node [
    id 368
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 369
    label "Togo"
  ]
  node [
    id 370
    label "Estonia"
  ]
  node [
    id 371
    label "Indochiny"
  ]
  node [
    id 372
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 373
    label "Oman"
  ]
  node [
    id 374
    label "Boliwia"
  ]
  node [
    id 375
    label "Portugalia"
  ]
  node [
    id 376
    label "Wyspy_Salomona"
  ]
  node [
    id 377
    label "Luksemburg"
  ]
  node [
    id 378
    label "Haiti"
  ]
  node [
    id 379
    label "Biskupizna"
  ]
  node [
    id 380
    label "Lubuskie"
  ]
  node [
    id 381
    label "Birma"
  ]
  node [
    id 382
    label "Rodezja"
  ]
  node [
    id 383
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 384
    label "robienie"
  ]
  node [
    id 385
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 386
    label "pope&#322;nianie"
  ]
  node [
    id 387
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 388
    label "development"
  ]
  node [
    id 389
    label "stanowienie"
  ]
  node [
    id 390
    label "exploitation"
  ]
  node [
    id 391
    label "structure"
  ]
  node [
    id 392
    label "specjalny"
  ]
  node [
    id 393
    label "nale&#380;yty"
  ]
  node [
    id 394
    label "stosownie"
  ]
  node [
    id 395
    label "zdarzony"
  ]
  node [
    id 396
    label "odpowiednio"
  ]
  node [
    id 397
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 398
    label "odpowiadanie"
  ]
  node [
    id 399
    label "nale&#380;ny"
  ]
  node [
    id 400
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 401
    label "strzy&#380;enie"
  ]
  node [
    id 402
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 403
    label "wegetowa&#263;"
  ]
  node [
    id 404
    label "nieuleczalnie_chory"
  ]
  node [
    id 405
    label "flawonoid"
  ]
  node [
    id 406
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 407
    label "strzyc"
  ]
  node [
    id 408
    label "g&#322;uszy&#263;"
  ]
  node [
    id 409
    label "bulwka"
  ]
  node [
    id 410
    label "fitotron"
  ]
  node [
    id 411
    label "pochewka"
  ]
  node [
    id 412
    label "ro&#347;liny"
  ]
  node [
    id 413
    label "wegetowanie"
  ]
  node [
    id 414
    label "zadziorek"
  ]
  node [
    id 415
    label "epiderma"
  ]
  node [
    id 416
    label "zawi&#261;zek"
  ]
  node [
    id 417
    label "odn&#243;&#380;ka"
  ]
  node [
    id 418
    label "fitocenoza"
  ]
  node [
    id 419
    label "g&#322;uszenie"
  ]
  node [
    id 420
    label "fotoautotrof"
  ]
  node [
    id 421
    label "wypotnik"
  ]
  node [
    id 422
    label "gumoza"
  ]
  node [
    id 423
    label "wyro&#347;le"
  ]
  node [
    id 424
    label "owoc"
  ]
  node [
    id 425
    label "zbiorowisko"
  ]
  node [
    id 426
    label "p&#281;d"
  ]
  node [
    id 427
    label "hodowla"
  ]
  node [
    id 428
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 429
    label "sok"
  ]
  node [
    id 430
    label "wegetacja"
  ]
  node [
    id 431
    label "do&#322;owanie"
  ]
  node [
    id 432
    label "pora&#380;a&#263;"
  ]
  node [
    id 433
    label "w&#322;&#243;kno"
  ]
  node [
    id 434
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 435
    label "system_korzeniowy"
  ]
  node [
    id 436
    label "do&#322;owa&#263;"
  ]
  node [
    id 437
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 438
    label "obra&#380;a&#263;"
  ]
  node [
    id 439
    label "mistreat"
  ]
  node [
    id 440
    label "dzielna"
  ]
  node [
    id 441
    label "liczenie"
  ]
  node [
    id 442
    label "rozdawanie"
  ]
  node [
    id 443
    label "czynno&#347;&#263;"
  ]
  node [
    id 444
    label "stosowanie"
  ]
  node [
    id 445
    label "rozprowadzanie"
  ]
  node [
    id 446
    label "separation"
  ]
  node [
    id 447
    label "dzielnik"
  ]
  node [
    id 448
    label "sk&#322;&#243;canie"
  ]
  node [
    id 449
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 450
    label "division"
  ]
  node [
    id 451
    label "wyodr&#281;bnianie"
  ]
  node [
    id 452
    label "powodowanie"
  ]
  node [
    id 453
    label "contribution"
  ]
  node [
    id 454
    label "iloraz"
  ]
  node [
    id 455
    label "przeszkoda"
  ]
  node [
    id 456
    label "dzielenie_si&#281;"
  ]
  node [
    id 457
    label "prawo"
  ]
  node [
    id 458
    label "licencjonowa&#263;"
  ]
  node [
    id 459
    label "pozwolenie"
  ]
  node [
    id 460
    label "rasowy"
  ]
  node [
    id 461
    label "license"
  ]
  node [
    id 462
    label "zezwolenie"
  ]
  node [
    id 463
    label "za&#347;wiadczenie"
  ]
  node [
    id 464
    label "ku&#378;nia"
  ]
  node [
    id 465
    label "Harvard"
  ]
  node [
    id 466
    label "uczelnia"
  ]
  node [
    id 467
    label "Sorbona"
  ]
  node [
    id 468
    label "Uniwersytet_Oksfordzki"
  ]
  node [
    id 469
    label "Stanford"
  ]
  node [
    id 470
    label "Princeton"
  ]
  node [
    id 471
    label "academy"
  ]
  node [
    id 472
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 473
    label "wydzia&#322;"
  ]
  node [
    id 474
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 475
    label "&#322;upina"
  ]
  node [
    id 476
    label "zarodek"
  ]
  node [
    id 477
    label "plemnik"
  ]
  node [
    id 478
    label "p&#322;yn_nasienny"
  ]
  node [
    id 479
    label "spierdolina"
  ]
  node [
    id 480
    label "elajosom"
  ]
  node [
    id 481
    label "organ"
  ]
  node [
    id 482
    label "seed"
  ]
  node [
    id 483
    label "przyczepka"
  ]
  node [
    id 484
    label "bielmo"
  ]
  node [
    id 485
    label "miernota"
  ]
  node [
    id 486
    label "g&#243;wno"
  ]
  node [
    id 487
    label "love"
  ]
  node [
    id 488
    label "ilo&#347;&#263;"
  ]
  node [
    id 489
    label "brak"
  ]
  node [
    id 490
    label "ciura"
  ]
  node [
    id 491
    label "krzywa"
  ]
  node [
    id 492
    label "doda&#263;"
  ]
  node [
    id 493
    label "articulation"
  ]
  node [
    id 494
    label "czasowo"
  ]
  node [
    id 495
    label "wtedy"
  ]
  node [
    id 496
    label "panowanie"
  ]
  node [
    id 497
    label "Kreml"
  ]
  node [
    id 498
    label "wydolno&#347;&#263;"
  ]
  node [
    id 499
    label "grupa"
  ]
  node [
    id 500
    label "rz&#261;dzenie"
  ]
  node [
    id 501
    label "rz&#261;d"
  ]
  node [
    id 502
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 503
    label "struktura"
  ]
  node [
    id 504
    label "odzyska&#263;"
  ]
  node [
    id 505
    label "devise"
  ]
  node [
    id 506
    label "oceni&#263;"
  ]
  node [
    id 507
    label "znaj&#347;&#263;"
  ]
  node [
    id 508
    label "wymy&#347;li&#263;"
  ]
  node [
    id 509
    label "invent"
  ]
  node [
    id 510
    label "pozyska&#263;"
  ]
  node [
    id 511
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 512
    label "wykry&#263;"
  ]
  node [
    id 513
    label "dozna&#263;"
  ]
  node [
    id 514
    label "nowotny"
  ]
  node [
    id 515
    label "drugi"
  ]
  node [
    id 516
    label "kolejny"
  ]
  node [
    id 517
    label "bie&#380;&#261;cy"
  ]
  node [
    id 518
    label "nowo"
  ]
  node [
    id 519
    label "narybek"
  ]
  node [
    id 520
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 521
    label "obcy"
  ]
  node [
    id 522
    label "pomy&#347;lny"
  ]
  node [
    id 523
    label "skuteczny"
  ]
  node [
    id 524
    label "moralny"
  ]
  node [
    id 525
    label "korzystny"
  ]
  node [
    id 526
    label "zwrot"
  ]
  node [
    id 527
    label "dobrze"
  ]
  node [
    id 528
    label "pozytywny"
  ]
  node [
    id 529
    label "grzeczny"
  ]
  node [
    id 530
    label "powitanie"
  ]
  node [
    id 531
    label "mi&#322;y"
  ]
  node [
    id 532
    label "dobroczynny"
  ]
  node [
    id 533
    label "pos&#322;uszny"
  ]
  node [
    id 534
    label "ca&#322;y"
  ]
  node [
    id 535
    label "czw&#243;rka"
  ]
  node [
    id 536
    label "spokojny"
  ]
  node [
    id 537
    label "&#347;mieszny"
  ]
  node [
    id 538
    label "drogi"
  ]
  node [
    id 539
    label "model"
  ]
  node [
    id 540
    label "zbi&#243;r"
  ]
  node [
    id 541
    label "tryb"
  ]
  node [
    id 542
    label "narz&#281;dzie"
  ]
  node [
    id 543
    label "nature"
  ]
  node [
    id 544
    label "expo"
  ]
  node [
    id 545
    label "2012"
  ]
  node [
    id 546
    label "przyrodniczy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 384
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 457
  ]
  edge [
    source 15
    target 458
  ]
  edge [
    source 15
    target 459
  ]
  edge [
    source 15
    target 427
  ]
  edge [
    source 15
    target 460
  ]
  edge [
    source 15
    target 461
  ]
  edge [
    source 15
    target 462
  ]
  edge [
    source 15
    target 463
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 464
  ]
  edge [
    source 17
    target 465
  ]
  edge [
    source 17
    target 466
  ]
  edge [
    source 17
    target 467
  ]
  edge [
    source 17
    target 468
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 470
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 472
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 17
    target 546
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 474
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 475
  ]
  edge [
    source 19
    target 476
  ]
  edge [
    source 19
    target 477
  ]
  edge [
    source 19
    target 478
  ]
  edge [
    source 19
    target 479
  ]
  edge [
    source 19
    target 480
  ]
  edge [
    source 19
    target 481
  ]
  edge [
    source 19
    target 482
  ]
  edge [
    source 19
    target 483
  ]
  edge [
    source 19
    target 484
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 31
  ]
  edge [
    source 22
    target 485
  ]
  edge [
    source 22
    target 486
  ]
  edge [
    source 22
    target 487
  ]
  edge [
    source 22
    target 488
  ]
  edge [
    source 22
    target 489
  ]
  edge [
    source 22
    target 490
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 491
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 492
  ]
  edge [
    source 24
    target 493
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 494
  ]
  edge [
    source 25
    target 495
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 46
  ]
  edge [
    source 26
    target 496
  ]
  edge [
    source 26
    target 497
  ]
  edge [
    source 26
    target 498
  ]
  edge [
    source 26
    target 499
  ]
  edge [
    source 26
    target 457
  ]
  edge [
    source 26
    target 500
  ]
  edge [
    source 26
    target 501
  ]
  edge [
    source 26
    target 502
  ]
  edge [
    source 26
    target 503
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 504
  ]
  edge [
    source 28
    target 505
  ]
  edge [
    source 28
    target 506
  ]
  edge [
    source 28
    target 507
  ]
  edge [
    source 28
    target 508
  ]
  edge [
    source 28
    target 509
  ]
  edge [
    source 28
    target 510
  ]
  edge [
    source 28
    target 511
  ]
  edge [
    source 28
    target 512
  ]
  edge [
    source 28
    target 42
  ]
  edge [
    source 28
    target 513
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 46
  ]
  edge [
    source 29
    target 514
  ]
  edge [
    source 29
    target 515
  ]
  edge [
    source 29
    target 516
  ]
  edge [
    source 29
    target 517
  ]
  edge [
    source 29
    target 518
  ]
  edge [
    source 29
    target 519
  ]
  edge [
    source 29
    target 520
  ]
  edge [
    source 29
    target 521
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 522
  ]
  edge [
    source 30
    target 523
  ]
  edge [
    source 30
    target 524
  ]
  edge [
    source 30
    target 525
  ]
  edge [
    source 30
    target 526
  ]
  edge [
    source 30
    target 527
  ]
  edge [
    source 30
    target 528
  ]
  edge [
    source 30
    target 529
  ]
  edge [
    source 30
    target 530
  ]
  edge [
    source 30
    target 531
  ]
  edge [
    source 30
    target 532
  ]
  edge [
    source 30
    target 533
  ]
  edge [
    source 30
    target 534
  ]
  edge [
    source 30
    target 397
  ]
  edge [
    source 30
    target 535
  ]
  edge [
    source 30
    target 536
  ]
  edge [
    source 30
    target 537
  ]
  edge [
    source 30
    target 538
  ]
  edge [
    source 31
    target 539
  ]
  edge [
    source 31
    target 540
  ]
  edge [
    source 31
    target 541
  ]
  edge [
    source 31
    target 542
  ]
  edge [
    source 31
    target 543
  ]
  edge [
    source 544
    target 545
  ]
]
