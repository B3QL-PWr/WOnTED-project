graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9666666666666666
  density 0.03333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "dzienia"
    origin "text"
  ]
  node [
    id 2
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 3
    label "wsp&#243;&#322;lokator"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "powiesi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wroclaw"
    origin "text"
  ]
  node [
    id 7
    label "du&#380;y"
  ]
  node [
    id 8
    label "jedyny"
  ]
  node [
    id 9
    label "kompletny"
  ]
  node [
    id 10
    label "zdr&#243;w"
  ]
  node [
    id 11
    label "&#380;ywy"
  ]
  node [
    id 12
    label "ca&#322;o"
  ]
  node [
    id 13
    label "pe&#322;ny"
  ]
  node [
    id 14
    label "calu&#347;ko"
  ]
  node [
    id 15
    label "podobny"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 18
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 19
    label "ucho"
  ]
  node [
    id 20
    label "makrocefalia"
  ]
  node [
    id 21
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 22
    label "m&#243;zg"
  ]
  node [
    id 23
    label "kierownictwo"
  ]
  node [
    id 24
    label "czaszka"
  ]
  node [
    id 25
    label "dekiel"
  ]
  node [
    id 26
    label "umys&#322;"
  ]
  node [
    id 27
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 28
    label "&#347;ci&#281;cie"
  ]
  node [
    id 29
    label "sztuka"
  ]
  node [
    id 30
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 31
    label "g&#243;ra"
  ]
  node [
    id 32
    label "byd&#322;o"
  ]
  node [
    id 33
    label "alkohol"
  ]
  node [
    id 34
    label "wiedza"
  ]
  node [
    id 35
    label "ro&#347;lina"
  ]
  node [
    id 36
    label "&#347;ci&#281;gno"
  ]
  node [
    id 37
    label "&#380;ycie"
  ]
  node [
    id 38
    label "pryncypa&#322;"
  ]
  node [
    id 39
    label "fryzura"
  ]
  node [
    id 40
    label "noosfera"
  ]
  node [
    id 41
    label "kierowa&#263;"
  ]
  node [
    id 42
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 43
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 44
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 45
    label "cecha"
  ]
  node [
    id 46
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 47
    label "zdolno&#347;&#263;"
  ]
  node [
    id 48
    label "kszta&#322;t"
  ]
  node [
    id 49
    label "cz&#322;onek"
  ]
  node [
    id 50
    label "cia&#322;o"
  ]
  node [
    id 51
    label "obiekt"
  ]
  node [
    id 52
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 53
    label "wsp&#243;&#322;mieszkaniec"
  ]
  node [
    id 54
    label "stryczek"
  ]
  node [
    id 55
    label "szubienica"
  ]
  node [
    id 56
    label "bent"
  ]
  node [
    id 57
    label "suspend"
  ]
  node [
    id 58
    label "umie&#347;ci&#263;"
  ]
  node [
    id 59
    label "zabi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
]
