graph [
  maxDegree 17
  minDegree 1
  meanDegree 2
  density 0.03508771929824561
  graphCliqueNumber 3
  node [
    id 0
    label "bliscy"
    origin "text"
  ]
  node [
    id 1
    label "policja"
    origin "text"
  ]
  node [
    id 2
    label "poszukiwa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "dwa"
    origin "text"
  ]
  node [
    id 4
    label "letni"
    origin "text"
  ]
  node [
    id 5
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 6
    label "nowy"
    origin "text"
  ]
  node [
    id 7
    label "targ"
    origin "text"
  ]
  node [
    id 8
    label "grono"
  ]
  node [
    id 9
    label "komisariat"
  ]
  node [
    id 10
    label "psiarnia"
  ]
  node [
    id 11
    label "posterunek"
  ]
  node [
    id 12
    label "grupa"
  ]
  node [
    id 13
    label "organ"
  ]
  node [
    id 14
    label "s&#322;u&#380;ba"
  ]
  node [
    id 15
    label "ask"
  ]
  node [
    id 16
    label "stara&#263;_si&#281;"
  ]
  node [
    id 17
    label "szuka&#263;"
  ]
  node [
    id 18
    label "look"
  ]
  node [
    id 19
    label "nijaki"
  ]
  node [
    id 20
    label "sezonowy"
  ]
  node [
    id 21
    label "letnio"
  ]
  node [
    id 22
    label "s&#322;oneczny"
  ]
  node [
    id 23
    label "weso&#322;y"
  ]
  node [
    id 24
    label "oboj&#281;tny"
  ]
  node [
    id 25
    label "latowy"
  ]
  node [
    id 26
    label "ciep&#322;y"
  ]
  node [
    id 27
    label "typowy"
  ]
  node [
    id 28
    label "cz&#322;owiek"
  ]
  node [
    id 29
    label "pomocnik"
  ]
  node [
    id 30
    label "g&#243;wniarz"
  ]
  node [
    id 31
    label "&#347;l&#261;ski"
  ]
  node [
    id 32
    label "m&#322;odzieniec"
  ]
  node [
    id 33
    label "kajtek"
  ]
  node [
    id 34
    label "kawaler"
  ]
  node [
    id 35
    label "usynawianie"
  ]
  node [
    id 36
    label "dziecko"
  ]
  node [
    id 37
    label "okrzos"
  ]
  node [
    id 38
    label "usynowienie"
  ]
  node [
    id 39
    label "sympatia"
  ]
  node [
    id 40
    label "pederasta"
  ]
  node [
    id 41
    label "synek"
  ]
  node [
    id 42
    label "boyfriend"
  ]
  node [
    id 43
    label "nowotny"
  ]
  node [
    id 44
    label "drugi"
  ]
  node [
    id 45
    label "kolejny"
  ]
  node [
    id 46
    label "bie&#380;&#261;cy"
  ]
  node [
    id 47
    label "nowo"
  ]
  node [
    id 48
    label "narybek"
  ]
  node [
    id 49
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 50
    label "obcy"
  ]
  node [
    id 51
    label "stoisko"
  ]
  node [
    id 52
    label "plac"
  ]
  node [
    id 53
    label "kram"
  ]
  node [
    id 54
    label "market"
  ]
  node [
    id 55
    label "obiekt_handlowy"
  ]
  node [
    id 56
    label "targowica"
  ]
  node [
    id 57
    label "sprzeda&#380;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
]
