graph [
  maxDegree 39
  minDegree 1
  meanDegree 2
  density 0.0273972602739726
  graphCliqueNumber 2
  node [
    id 0
    label "komunikat"
    origin "text"
  ]
  node [
    id 1
    label "dla"
    origin "text"
  ]
  node [
    id 2
    label "osoba"
    origin "text"
  ]
  node [
    id 3
    label "ubiega&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "praca"
    origin "text"
  ]
  node [
    id 6
    label "ump"
    origin "text"
  ]
  node [
    id 7
    label "kreacjonista"
  ]
  node [
    id 8
    label "wytw&#243;r"
  ]
  node [
    id 9
    label "roi&#263;_si&#281;"
  ]
  node [
    id 10
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 11
    label "communication"
  ]
  node [
    id 12
    label "Zgredek"
  ]
  node [
    id 13
    label "kategoria_gramatyczna"
  ]
  node [
    id 14
    label "Casanova"
  ]
  node [
    id 15
    label "Don_Juan"
  ]
  node [
    id 16
    label "Gargantua"
  ]
  node [
    id 17
    label "Faust"
  ]
  node [
    id 18
    label "profanum"
  ]
  node [
    id 19
    label "Chocho&#322;"
  ]
  node [
    id 20
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 21
    label "koniugacja"
  ]
  node [
    id 22
    label "Winnetou"
  ]
  node [
    id 23
    label "Dwukwiat"
  ]
  node [
    id 24
    label "homo_sapiens"
  ]
  node [
    id 25
    label "Edyp"
  ]
  node [
    id 26
    label "Herkules_Poirot"
  ]
  node [
    id 27
    label "ludzko&#347;&#263;"
  ]
  node [
    id 28
    label "mikrokosmos"
  ]
  node [
    id 29
    label "person"
  ]
  node [
    id 30
    label "Sherlock_Holmes"
  ]
  node [
    id 31
    label "portrecista"
  ]
  node [
    id 32
    label "Szwejk"
  ]
  node [
    id 33
    label "Hamlet"
  ]
  node [
    id 34
    label "duch"
  ]
  node [
    id 35
    label "g&#322;owa"
  ]
  node [
    id 36
    label "oddzia&#322;ywanie"
  ]
  node [
    id 37
    label "Quasimodo"
  ]
  node [
    id 38
    label "Dulcynea"
  ]
  node [
    id 39
    label "Don_Kiszot"
  ]
  node [
    id 40
    label "Wallenrod"
  ]
  node [
    id 41
    label "Plastu&#347;"
  ]
  node [
    id 42
    label "Harry_Potter"
  ]
  node [
    id 43
    label "figura"
  ]
  node [
    id 44
    label "parali&#380;owa&#263;"
  ]
  node [
    id 45
    label "istota"
  ]
  node [
    id 46
    label "Werter"
  ]
  node [
    id 47
    label "antropochoria"
  ]
  node [
    id 48
    label "posta&#263;"
  ]
  node [
    id 49
    label "robi&#263;"
  ]
  node [
    id 50
    label "anticipate"
  ]
  node [
    id 51
    label "stosunek_pracy"
  ]
  node [
    id 52
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 53
    label "benedykty&#324;ski"
  ]
  node [
    id 54
    label "pracowanie"
  ]
  node [
    id 55
    label "zaw&#243;d"
  ]
  node [
    id 56
    label "kierownictwo"
  ]
  node [
    id 57
    label "zmiana"
  ]
  node [
    id 58
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 59
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 60
    label "tynkarski"
  ]
  node [
    id 61
    label "czynnik_produkcji"
  ]
  node [
    id 62
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 63
    label "zobowi&#261;zanie"
  ]
  node [
    id 64
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 65
    label "czynno&#347;&#263;"
  ]
  node [
    id 66
    label "tyrka"
  ]
  node [
    id 67
    label "pracowa&#263;"
  ]
  node [
    id 68
    label "siedziba"
  ]
  node [
    id 69
    label "poda&#380;_pracy"
  ]
  node [
    id 70
    label "miejsce"
  ]
  node [
    id 71
    label "zak&#322;ad"
  ]
  node [
    id 72
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 73
    label "najem"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
]
