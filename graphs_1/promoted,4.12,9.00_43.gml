graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 1
    label "przera&#380;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wzbudza&#263;"
  ]
  node [
    id 3
    label "alarm"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
]
