graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.2560975609756095
  density 0.01384108933113871
  graphCliqueNumber 3
  node [
    id 0
    label "zakres"
    origin "text"
  ]
  node [
    id 1
    label "zatrudnia&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pracownik"
    origin "text"
  ]
  node [
    id 3
    label "cela"
    origin "text"
  ]
  node [
    id 4
    label "skierowanie"
    origin "text"
  ]
  node [
    id 5
    label "pracodawca"
    origin "text"
  ]
  node [
    id 6
    label "u&#380;ytkownik"
    origin "text"
  ]
  node [
    id 7
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "lub"
    origin "text"
  ]
  node [
    id 10
    label "podmiot"
    origin "text"
  ]
  node [
    id 11
    label "rozumienie"
    origin "text"
  ]
  node [
    id 12
    label "kodeks"
    origin "text"
  ]
  node [
    id 13
    label "praca"
    origin "text"
  ]
  node [
    id 14
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 15
    label "agencja"
    origin "text"
  ]
  node [
    id 16
    label "tymczasowy"
    origin "text"
  ]
  node [
    id 17
    label "wyznacza&#263;"
    origin "text"
  ]
  node [
    id 18
    label "zadanie"
    origin "text"
  ]
  node [
    id 19
    label "wykonanie"
    origin "text"
  ]
  node [
    id 20
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 21
    label "granica"
  ]
  node [
    id 22
    label "circle"
  ]
  node [
    id 23
    label "podzakres"
  ]
  node [
    id 24
    label "zbi&#243;r"
  ]
  node [
    id 25
    label "dziedzina"
  ]
  node [
    id 26
    label "desygnat"
  ]
  node [
    id 27
    label "sfera"
  ]
  node [
    id 28
    label "wielko&#347;&#263;"
  ]
  node [
    id 29
    label "zatrudni&#263;"
  ]
  node [
    id 30
    label "zatrudnianie"
  ]
  node [
    id 31
    label "bra&#263;"
  ]
  node [
    id 32
    label "undertake"
  ]
  node [
    id 33
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "delegowa&#263;"
  ]
  node [
    id 36
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 37
    label "pracu&#347;"
  ]
  node [
    id 38
    label "delegowanie"
  ]
  node [
    id 39
    label "r&#281;ka"
  ]
  node [
    id 40
    label "salariat"
  ]
  node [
    id 41
    label "pomieszczenie"
  ]
  node [
    id 42
    label "klasztor"
  ]
  node [
    id 43
    label "turn"
  ]
  node [
    id 44
    label "przemieszczenie"
  ]
  node [
    id 45
    label "przeznaczenie"
  ]
  node [
    id 46
    label "podpowiedzenie"
  ]
  node [
    id 47
    label "mission"
  ]
  node [
    id 48
    label "pismo"
  ]
  node [
    id 49
    label "wys&#322;anie"
  ]
  node [
    id 50
    label "referral"
  ]
  node [
    id 51
    label "ustawienie"
  ]
  node [
    id 52
    label "p&#322;atnik"
  ]
  node [
    id 53
    label "zwierzchnik"
  ]
  node [
    id 54
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 55
    label "j&#281;zykowo"
  ]
  node [
    id 56
    label "si&#281;ga&#263;"
  ]
  node [
    id 57
    label "trwa&#263;"
  ]
  node [
    id 58
    label "obecno&#347;&#263;"
  ]
  node [
    id 59
    label "stan"
  ]
  node [
    id 60
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 61
    label "stand"
  ]
  node [
    id 62
    label "mie&#263;_miejsce"
  ]
  node [
    id 63
    label "uczestniczy&#263;"
  ]
  node [
    id 64
    label "chodzi&#263;"
  ]
  node [
    id 65
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 66
    label "equal"
  ]
  node [
    id 67
    label "byt"
  ]
  node [
    id 68
    label "organizacja"
  ]
  node [
    id 69
    label "prawo"
  ]
  node [
    id 70
    label "nauka_prawa"
  ]
  node [
    id 71
    label "osobowo&#347;&#263;"
  ]
  node [
    id 72
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 73
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 74
    label "robienie"
  ]
  node [
    id 75
    label "czynno&#347;&#263;"
  ]
  node [
    id 76
    label "czucie"
  ]
  node [
    id 77
    label "wytw&#243;r"
  ]
  node [
    id 78
    label "wnioskowanie"
  ]
  node [
    id 79
    label "bycie"
  ]
  node [
    id 80
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 81
    label "kontekst"
  ]
  node [
    id 82
    label "apprehension"
  ]
  node [
    id 83
    label "j&#281;zyk"
  ]
  node [
    id 84
    label "kumanie"
  ]
  node [
    id 85
    label "obja&#347;nienie"
  ]
  node [
    id 86
    label "hermeneutyka"
  ]
  node [
    id 87
    label "interpretation"
  ]
  node [
    id 88
    label "realization"
  ]
  node [
    id 89
    label "r&#281;kopis"
  ]
  node [
    id 90
    label "kodeks_morski"
  ]
  node [
    id 91
    label "Justynian"
  ]
  node [
    id 92
    label "code"
  ]
  node [
    id 93
    label "przepis"
  ]
  node [
    id 94
    label "obwiniony"
  ]
  node [
    id 95
    label "kodeks_karny"
  ]
  node [
    id 96
    label "kodeks_drogowy"
  ]
  node [
    id 97
    label "zasada"
  ]
  node [
    id 98
    label "kodeks_pracy"
  ]
  node [
    id 99
    label "kodeks_cywilny"
  ]
  node [
    id 100
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 101
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 102
    label "kodeks_rodzinny"
  ]
  node [
    id 103
    label "stosunek_pracy"
  ]
  node [
    id 104
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 105
    label "benedykty&#324;ski"
  ]
  node [
    id 106
    label "pracowanie"
  ]
  node [
    id 107
    label "zaw&#243;d"
  ]
  node [
    id 108
    label "kierownictwo"
  ]
  node [
    id 109
    label "zmiana"
  ]
  node [
    id 110
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 111
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 112
    label "tynkarski"
  ]
  node [
    id 113
    label "czynnik_produkcji"
  ]
  node [
    id 114
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 115
    label "zobowi&#261;zanie"
  ]
  node [
    id 116
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 117
    label "tyrka"
  ]
  node [
    id 118
    label "pracowa&#263;"
  ]
  node [
    id 119
    label "siedziba"
  ]
  node [
    id 120
    label "poda&#380;_pracy"
  ]
  node [
    id 121
    label "miejsce"
  ]
  node [
    id 122
    label "zak&#322;ad"
  ]
  node [
    id 123
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 124
    label "najem"
  ]
  node [
    id 125
    label "wy&#322;&#261;czny"
  ]
  node [
    id 126
    label "bank"
  ]
  node [
    id 127
    label "whole"
  ]
  node [
    id 128
    label "NASA"
  ]
  node [
    id 129
    label "firma"
  ]
  node [
    id 130
    label "przedstawicielstwo"
  ]
  node [
    id 131
    label "ajencja"
  ]
  node [
    id 132
    label "instytucja"
  ]
  node [
    id 133
    label "filia"
  ]
  node [
    id 134
    label "dzia&#322;"
  ]
  node [
    id 135
    label "oddzia&#322;"
  ]
  node [
    id 136
    label "przepustka"
  ]
  node [
    id 137
    label "czasowo"
  ]
  node [
    id 138
    label "zaznacza&#263;"
  ]
  node [
    id 139
    label "wybiera&#263;"
  ]
  node [
    id 140
    label "inflict"
  ]
  node [
    id 141
    label "okre&#347;la&#263;"
  ]
  node [
    id 142
    label "set"
  ]
  node [
    id 143
    label "ustala&#263;"
  ]
  node [
    id 144
    label "yield"
  ]
  node [
    id 145
    label "problem"
  ]
  node [
    id 146
    label "przepisanie"
  ]
  node [
    id 147
    label "przepisa&#263;"
  ]
  node [
    id 148
    label "za&#322;o&#380;enie"
  ]
  node [
    id 149
    label "work"
  ]
  node [
    id 150
    label "nakarmienie"
  ]
  node [
    id 151
    label "duty"
  ]
  node [
    id 152
    label "powierzanie"
  ]
  node [
    id 153
    label "zaszkodzenie"
  ]
  node [
    id 154
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 155
    label "zaj&#281;cie"
  ]
  node [
    id 156
    label "zrobienie"
  ]
  node [
    id 157
    label "fabrication"
  ]
  node [
    id 158
    label "ziszczenie_si&#281;"
  ]
  node [
    id 159
    label "pojawienie_si&#281;"
  ]
  node [
    id 160
    label "dzie&#322;o"
  ]
  node [
    id 161
    label "production"
  ]
  node [
    id 162
    label "completion"
  ]
  node [
    id 163
    label "realizacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 75
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 75
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
]
