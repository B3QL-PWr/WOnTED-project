graph [
  maxDegree 27
  minDegree 1
  meanDegree 2
  density 0.05555555555555555
  graphCliqueNumber 3
  node [
    id 0
    label "droga"
    origin "text"
  ]
  node [
    id 1
    label "krajowy"
    origin "text"
  ]
  node [
    id 2
    label "niemcy"
    origin "text"
  ]
  node [
    id 3
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 4
    label "journey"
  ]
  node [
    id 5
    label "podbieg"
  ]
  node [
    id 6
    label "bezsilnikowy"
  ]
  node [
    id 7
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 8
    label "wylot"
  ]
  node [
    id 9
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 10
    label "drogowskaz"
  ]
  node [
    id 11
    label "nawierzchnia"
  ]
  node [
    id 12
    label "turystyka"
  ]
  node [
    id 13
    label "budowla"
  ]
  node [
    id 14
    label "spos&#243;b"
  ]
  node [
    id 15
    label "passage"
  ]
  node [
    id 16
    label "marszrutyzacja"
  ]
  node [
    id 17
    label "zbior&#243;wka"
  ]
  node [
    id 18
    label "rajza"
  ]
  node [
    id 19
    label "ekskursja"
  ]
  node [
    id 20
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 21
    label "ruch"
  ]
  node [
    id 22
    label "trasa"
  ]
  node [
    id 23
    label "wyb&#243;j"
  ]
  node [
    id 24
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "ekwipunek"
  ]
  node [
    id 26
    label "korona_drogi"
  ]
  node [
    id 27
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 28
    label "pobocze"
  ]
  node [
    id 29
    label "rodzimy"
  ]
  node [
    id 30
    label "drogi"
  ]
  node [
    id 31
    label "B210"
  ]
  node [
    id 32
    label "210"
  ]
  node [
    id 33
    label "by&#322;y"
  ]
  node [
    id 34
    label "Bundesstra&#223;e"
  ]
  node [
    id 35
    label "dolny"
  ]
  node [
    id 36
    label "Saksonia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 35
    target 36
  ]
]
