graph [
  maxDegree 11
  minDegree 1
  meanDegree 2
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "facet"
    origin "text"
  ]
  node [
    id 3
    label "przy&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 4
    label "robot"
    origin "text"
  ]
  node [
    id 5
    label "kolega"
    origin "text"
  ]
  node [
    id 6
    label "wy&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "bratek"
  ]
  node [
    id 9
    label "dopieprzy&#263;"
  ]
  node [
    id 10
    label "catch"
  ]
  node [
    id 11
    label "uderzy&#263;"
  ]
  node [
    id 12
    label "przypalantowa&#263;"
  ]
  node [
    id 13
    label "set"
  ]
  node [
    id 14
    label "zbli&#380;y&#263;"
  ]
  node [
    id 15
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 16
    label "maszyna"
  ]
  node [
    id 17
    label "sprz&#281;t_AGD"
  ]
  node [
    id 18
    label "automat"
  ]
  node [
    id 19
    label "kumplowanie_si&#281;"
  ]
  node [
    id 20
    label "znajomy"
  ]
  node [
    id 21
    label "konfrater"
  ]
  node [
    id 22
    label "towarzysz"
  ]
  node [
    id 23
    label "zakolegowanie_si&#281;"
  ]
  node [
    id 24
    label "partner"
  ]
  node [
    id 25
    label "ziom"
  ]
  node [
    id 26
    label "range"
  ]
  node [
    id 27
    label "translate"
  ]
  node [
    id 28
    label "sheathe"
  ]
  node [
    id 29
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 30
    label "give"
  ]
  node [
    id 31
    label "pieni&#261;dze"
  ]
  node [
    id 32
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 33
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 34
    label "zrobi&#263;"
  ]
  node [
    id 35
    label "wyj&#261;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
]
