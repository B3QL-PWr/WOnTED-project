graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.2
  density 0.3
  graphCliqueNumber 2
  node [
    id 0
    label "niesamowity"
    origin "text"
  ]
  node [
    id 1
    label "niesamowicie"
  ]
  node [
    id 2
    label "niezwyk&#322;y"
  ]
  node [
    id 3
    label "stara"
  ]
  node [
    id 4
    label "miasto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 3
    target 4
  ]
]
