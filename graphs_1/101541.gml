graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9473684210526316
  density 0.05263157894736842
  graphCliqueNumber 2
  node [
    id 0
    label "kod"
    origin "text"
  ]
  node [
    id 1
    label "powr&#243;t"
    origin "text"
  ]
  node [
    id 2
    label "ostanio"
    origin "text"
  ]
  node [
    id 3
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "polecenie"
    origin "text"
  ]
  node [
    id 5
    label "language"
  ]
  node [
    id 6
    label "code"
  ]
  node [
    id 7
    label "ci&#261;g"
  ]
  node [
    id 8
    label "szablon"
  ]
  node [
    id 9
    label "szyfrowanie"
  ]
  node [
    id 10
    label "struktura"
  ]
  node [
    id 11
    label "return"
  ]
  node [
    id 12
    label "odyseja"
  ]
  node [
    id 13
    label "para"
  ]
  node [
    id 14
    label "wydarzenie"
  ]
  node [
    id 15
    label "rektyfikacja"
  ]
  node [
    id 16
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "work"
  ]
  node [
    id 18
    label "robi&#263;"
  ]
  node [
    id 19
    label "muzyka"
  ]
  node [
    id 20
    label "rola"
  ]
  node [
    id 21
    label "create"
  ]
  node [
    id 22
    label "wytwarza&#263;"
  ]
  node [
    id 23
    label "praca"
  ]
  node [
    id 24
    label "zadanie"
  ]
  node [
    id 25
    label "wypowied&#378;"
  ]
  node [
    id 26
    label "education"
  ]
  node [
    id 27
    label "zaordynowanie"
  ]
  node [
    id 28
    label "rekomendacja"
  ]
  node [
    id 29
    label "ukaz"
  ]
  node [
    id 30
    label "przesadzenie"
  ]
  node [
    id 31
    label "statement"
  ]
  node [
    id 32
    label "recommendation"
  ]
  node [
    id 33
    label "powierzenie"
  ]
  node [
    id 34
    label "pobiegni&#281;cie"
  ]
  node [
    id 35
    label "consign"
  ]
  node [
    id 36
    label "pognanie"
  ]
  node [
    id 37
    label "doradzenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
]
