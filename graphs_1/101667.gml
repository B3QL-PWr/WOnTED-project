graph [
  maxDegree 51
  minDegree 1
  meanDegree 2.1608040201005023
  density 0.010913151616669205
  graphCliqueNumber 5
  node [
    id 0
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 1
    label "komisja"
    origin "text"
  ]
  node [
    id 2
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "prawy"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 5
    label "stanowisko"
    origin "text"
  ]
  node [
    id 6
    label "senat"
    origin "text"
  ]
  node [
    id 7
    label "sprawa"
    origin "text"
  ]
  node [
    id 8
    label "ustawa"
    origin "text"
  ]
  node [
    id 9
    label "zmiana"
    origin "text"
  ]
  node [
    id 10
    label "prawo"
    origin "text"
  ]
  node [
    id 11
    label "adwokatura"
    origin "text"
  ]
  node [
    id 12
    label "radca"
    origin "text"
  ]
  node [
    id 13
    label "prawny"
    origin "text"
  ]
  node [
    id 14
    label "notariat"
    origin "text"
  ]
  node [
    id 15
    label "druk"
    origin "text"
  ]
  node [
    id 16
    label "message"
  ]
  node [
    id 17
    label "korespondent"
  ]
  node [
    id 18
    label "wypowied&#378;"
  ]
  node [
    id 19
    label "sprawko"
  ]
  node [
    id 20
    label "obrady"
  ]
  node [
    id 21
    label "zesp&#243;&#322;"
  ]
  node [
    id 22
    label "organ"
  ]
  node [
    id 23
    label "Komisja_Europejska"
  ]
  node [
    id 24
    label "podkomisja"
  ]
  node [
    id 25
    label "konsekwencja"
  ]
  node [
    id 26
    label "punishment"
  ]
  node [
    id 27
    label "cecha"
  ]
  node [
    id 28
    label "roboty_przymusowe"
  ]
  node [
    id 29
    label "nemezis"
  ]
  node [
    id 30
    label "righteousness"
  ]
  node [
    id 31
    label "z_prawa"
  ]
  node [
    id 32
    label "cnotliwy"
  ]
  node [
    id 33
    label "moralny"
  ]
  node [
    id 34
    label "zgodnie_z_prawem"
  ]
  node [
    id 35
    label "naturalny"
  ]
  node [
    id 36
    label "legalny"
  ]
  node [
    id 37
    label "na_prawo"
  ]
  node [
    id 38
    label "s&#322;uszny"
  ]
  node [
    id 39
    label "w_prawo"
  ]
  node [
    id 40
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 41
    label "prawicowy"
  ]
  node [
    id 42
    label "chwalebny"
  ]
  node [
    id 43
    label "zacny"
  ]
  node [
    id 44
    label "asymilowa&#263;"
  ]
  node [
    id 45
    label "wapniak"
  ]
  node [
    id 46
    label "dwun&#243;g"
  ]
  node [
    id 47
    label "polifag"
  ]
  node [
    id 48
    label "wz&#243;r"
  ]
  node [
    id 49
    label "profanum"
  ]
  node [
    id 50
    label "hominid"
  ]
  node [
    id 51
    label "homo_sapiens"
  ]
  node [
    id 52
    label "nasada"
  ]
  node [
    id 53
    label "podw&#322;adny"
  ]
  node [
    id 54
    label "ludzko&#347;&#263;"
  ]
  node [
    id 55
    label "os&#322;abianie"
  ]
  node [
    id 56
    label "mikrokosmos"
  ]
  node [
    id 57
    label "portrecista"
  ]
  node [
    id 58
    label "duch"
  ]
  node [
    id 59
    label "oddzia&#322;ywanie"
  ]
  node [
    id 60
    label "g&#322;owa"
  ]
  node [
    id 61
    label "asymilowanie"
  ]
  node [
    id 62
    label "osoba"
  ]
  node [
    id 63
    label "os&#322;abia&#263;"
  ]
  node [
    id 64
    label "figura"
  ]
  node [
    id 65
    label "Adam"
  ]
  node [
    id 66
    label "senior"
  ]
  node [
    id 67
    label "antropochoria"
  ]
  node [
    id 68
    label "posta&#263;"
  ]
  node [
    id 69
    label "postawi&#263;"
  ]
  node [
    id 70
    label "miejsce"
  ]
  node [
    id 71
    label "awansowanie"
  ]
  node [
    id 72
    label "po&#322;o&#380;enie"
  ]
  node [
    id 73
    label "awansowa&#263;"
  ]
  node [
    id 74
    label "uprawianie"
  ]
  node [
    id 75
    label "powierzanie"
  ]
  node [
    id 76
    label "punkt"
  ]
  node [
    id 77
    label "pogl&#261;d"
  ]
  node [
    id 78
    label "wojsko"
  ]
  node [
    id 79
    label "praca"
  ]
  node [
    id 80
    label "wakowa&#263;"
  ]
  node [
    id 81
    label "stawia&#263;"
  ]
  node [
    id 82
    label "parlament"
  ]
  node [
    id 83
    label "deputation"
  ]
  node [
    id 84
    label "grupa"
  ]
  node [
    id 85
    label "izba_wy&#380;sza"
  ]
  node [
    id 86
    label "kolegium"
  ]
  node [
    id 87
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 88
    label "siedziba"
  ]
  node [
    id 89
    label "magistrat"
  ]
  node [
    id 90
    label "temat"
  ]
  node [
    id 91
    label "kognicja"
  ]
  node [
    id 92
    label "idea"
  ]
  node [
    id 93
    label "szczeg&#243;&#322;"
  ]
  node [
    id 94
    label "rzecz"
  ]
  node [
    id 95
    label "wydarzenie"
  ]
  node [
    id 96
    label "przes&#322;anka"
  ]
  node [
    id 97
    label "rozprawa"
  ]
  node [
    id 98
    label "object"
  ]
  node [
    id 99
    label "proposition"
  ]
  node [
    id 100
    label "Karta_Nauczyciela"
  ]
  node [
    id 101
    label "marc&#243;wka"
  ]
  node [
    id 102
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 103
    label "akt"
  ]
  node [
    id 104
    label "przej&#347;&#263;"
  ]
  node [
    id 105
    label "charter"
  ]
  node [
    id 106
    label "przej&#347;cie"
  ]
  node [
    id 107
    label "anatomopatolog"
  ]
  node [
    id 108
    label "rewizja"
  ]
  node [
    id 109
    label "oznaka"
  ]
  node [
    id 110
    label "czas"
  ]
  node [
    id 111
    label "ferment"
  ]
  node [
    id 112
    label "komplet"
  ]
  node [
    id 113
    label "tura"
  ]
  node [
    id 114
    label "amendment"
  ]
  node [
    id 115
    label "zmianka"
  ]
  node [
    id 116
    label "odmienianie"
  ]
  node [
    id 117
    label "passage"
  ]
  node [
    id 118
    label "zjawisko"
  ]
  node [
    id 119
    label "change"
  ]
  node [
    id 120
    label "obserwacja"
  ]
  node [
    id 121
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 122
    label "nauka_prawa"
  ]
  node [
    id 123
    label "dominion"
  ]
  node [
    id 124
    label "normatywizm"
  ]
  node [
    id 125
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 126
    label "qualification"
  ]
  node [
    id 127
    label "opis"
  ]
  node [
    id 128
    label "regu&#322;a_Allena"
  ]
  node [
    id 129
    label "normalizacja"
  ]
  node [
    id 130
    label "kazuistyka"
  ]
  node [
    id 131
    label "regu&#322;a_Glogera"
  ]
  node [
    id 132
    label "kultura_duchowa"
  ]
  node [
    id 133
    label "prawo_karne"
  ]
  node [
    id 134
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 135
    label "standard"
  ]
  node [
    id 136
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 137
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 138
    label "struktura"
  ]
  node [
    id 139
    label "szko&#322;a"
  ]
  node [
    id 140
    label "prawo_karne_procesowe"
  ]
  node [
    id 141
    label "prawo_Mendla"
  ]
  node [
    id 142
    label "przepis"
  ]
  node [
    id 143
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 144
    label "criterion"
  ]
  node [
    id 145
    label "kanonistyka"
  ]
  node [
    id 146
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 147
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 148
    label "wykonawczy"
  ]
  node [
    id 149
    label "twierdzenie"
  ]
  node [
    id 150
    label "judykatura"
  ]
  node [
    id 151
    label "legislacyjnie"
  ]
  node [
    id 152
    label "umocowa&#263;"
  ]
  node [
    id 153
    label "podmiot"
  ]
  node [
    id 154
    label "procesualistyka"
  ]
  node [
    id 155
    label "kierunek"
  ]
  node [
    id 156
    label "kryminologia"
  ]
  node [
    id 157
    label "kryminalistyka"
  ]
  node [
    id 158
    label "cywilistyka"
  ]
  node [
    id 159
    label "law"
  ]
  node [
    id 160
    label "zasada_d'Alemberta"
  ]
  node [
    id 161
    label "jurisprudence"
  ]
  node [
    id 162
    label "zasada"
  ]
  node [
    id 163
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 164
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 165
    label "prawnicy"
  ]
  node [
    id 166
    label "legal_profession"
  ]
  node [
    id 167
    label "urz&#281;dnik"
  ]
  node [
    id 168
    label "prawniczo"
  ]
  node [
    id 169
    label "prawnie"
  ]
  node [
    id 170
    label "konstytucyjnoprawny"
  ]
  node [
    id 171
    label "jurydyczny"
  ]
  node [
    id 172
    label "kancelaria"
  ]
  node [
    id 173
    label "urz&#261;d"
  ]
  node [
    id 174
    label "prohibita"
  ]
  node [
    id 175
    label "impression"
  ]
  node [
    id 176
    label "wytw&#243;r"
  ]
  node [
    id 177
    label "tkanina"
  ]
  node [
    id 178
    label "glif"
  ]
  node [
    id 179
    label "formatowanie"
  ]
  node [
    id 180
    label "printing"
  ]
  node [
    id 181
    label "technika"
  ]
  node [
    id 182
    label "formatowa&#263;"
  ]
  node [
    id 183
    label "pismo"
  ]
  node [
    id 184
    label "cymelium"
  ]
  node [
    id 185
    label "zdobnik"
  ]
  node [
    id 186
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 187
    label "tekst"
  ]
  node [
    id 188
    label "publikacja"
  ]
  node [
    id 189
    label "zaproszenie"
  ]
  node [
    id 190
    label "dese&#324;"
  ]
  node [
    id 191
    label "character"
  ]
  node [
    id 192
    label "ojciec"
  ]
  node [
    id 193
    label "i"
  ]
  node [
    id 194
    label "J&#243;zefa"
  ]
  node [
    id 195
    label "Zych"
  ]
  node [
    id 196
    label "polskie"
  ]
  node [
    id 197
    label "stronnictwo"
  ]
  node [
    id 198
    label "ludowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 8
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 10
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 36
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 194
    target 195
  ]
  edge [
    source 196
    target 197
  ]
  edge [
    source 196
    target 198
  ]
  edge [
    source 197
    target 198
  ]
]
