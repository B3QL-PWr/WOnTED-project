graph [
  maxDegree 21
  minDegree 1
  meanDegree 2.3969465648854964
  density 0.018438050499119202
  graphCliqueNumber 7
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "czytanie"
    origin "text"
  ]
  node [
    id 2
    label "poselski"
    origin "text"
  ]
  node [
    id 3
    label "projekt"
    origin "text"
  ]
  node [
    id 4
    label "ustawa"
    origin "text"
  ]
  node [
    id 5
    label "zmiana"
    origin "text"
  ]
  node [
    id 6
    label "kodeks"
    origin "text"
  ]
  node [
    id 7
    label "sp&#243;&#322;ka"
    origin "text"
  ]
  node [
    id 8
    label "handlowy"
    origin "text"
  ]
  node [
    id 9
    label "koszt"
    origin "text"
  ]
  node [
    id 10
    label "s&#261;dowy"
    origin "text"
  ]
  node [
    id 11
    label "sprawa"
    origin "text"
  ]
  node [
    id 12
    label "cywilny"
    origin "text"
  ]
  node [
    id 13
    label "druk"
    origin "text"
  ]
  node [
    id 14
    label "godzina"
  ]
  node [
    id 15
    label "dysleksja"
  ]
  node [
    id 16
    label "wyczytywanie"
  ]
  node [
    id 17
    label "doczytywanie"
  ]
  node [
    id 18
    label "lektor"
  ]
  node [
    id 19
    label "przepowiadanie"
  ]
  node [
    id 20
    label "wczytywanie_si&#281;"
  ]
  node [
    id 21
    label "oczytywanie_si&#281;"
  ]
  node [
    id 22
    label "zaczytanie_si&#281;"
  ]
  node [
    id 23
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 24
    label "wczytywanie"
  ]
  node [
    id 25
    label "obrz&#261;dek"
  ]
  node [
    id 26
    label "czytywanie"
  ]
  node [
    id 27
    label "bycie_w_stanie"
  ]
  node [
    id 28
    label "pokazywanie"
  ]
  node [
    id 29
    label "poznawanie"
  ]
  node [
    id 30
    label "poczytanie"
  ]
  node [
    id 31
    label "Biblia"
  ]
  node [
    id 32
    label "reading"
  ]
  node [
    id 33
    label "recitation"
  ]
  node [
    id 34
    label "dokument"
  ]
  node [
    id 35
    label "device"
  ]
  node [
    id 36
    label "program_u&#380;ytkowy"
  ]
  node [
    id 37
    label "intencja"
  ]
  node [
    id 38
    label "agreement"
  ]
  node [
    id 39
    label "pomys&#322;"
  ]
  node [
    id 40
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 41
    label "plan"
  ]
  node [
    id 42
    label "dokumentacja"
  ]
  node [
    id 43
    label "Karta_Nauczyciela"
  ]
  node [
    id 44
    label "marc&#243;wka"
  ]
  node [
    id 45
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 46
    label "akt"
  ]
  node [
    id 47
    label "przej&#347;&#263;"
  ]
  node [
    id 48
    label "charter"
  ]
  node [
    id 49
    label "przej&#347;cie"
  ]
  node [
    id 50
    label "anatomopatolog"
  ]
  node [
    id 51
    label "rewizja"
  ]
  node [
    id 52
    label "oznaka"
  ]
  node [
    id 53
    label "czas"
  ]
  node [
    id 54
    label "ferment"
  ]
  node [
    id 55
    label "komplet"
  ]
  node [
    id 56
    label "tura"
  ]
  node [
    id 57
    label "amendment"
  ]
  node [
    id 58
    label "zmianka"
  ]
  node [
    id 59
    label "odmienianie"
  ]
  node [
    id 60
    label "passage"
  ]
  node [
    id 61
    label "zjawisko"
  ]
  node [
    id 62
    label "change"
  ]
  node [
    id 63
    label "praca"
  ]
  node [
    id 64
    label "r&#281;kopis"
  ]
  node [
    id 65
    label "Justynian"
  ]
  node [
    id 66
    label "kodeks_morski"
  ]
  node [
    id 67
    label "code"
  ]
  node [
    id 68
    label "przepis"
  ]
  node [
    id 69
    label "obwiniony"
  ]
  node [
    id 70
    label "kodeks_karny"
  ]
  node [
    id 71
    label "zbi&#243;r"
  ]
  node [
    id 72
    label "kodeks_drogowy"
  ]
  node [
    id 73
    label "zasada"
  ]
  node [
    id 74
    label "kodeks_pracy"
  ]
  node [
    id 75
    label "kodeks_cywilny"
  ]
  node [
    id 76
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 77
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 78
    label "kodeks_rodzinny"
  ]
  node [
    id 79
    label "podmiot_gospodarczy"
  ]
  node [
    id 80
    label "organizacja"
  ]
  node [
    id 81
    label "zesp&#243;&#322;"
  ]
  node [
    id 82
    label "wsp&#243;lnictwo"
  ]
  node [
    id 83
    label "handlowo"
  ]
  node [
    id 84
    label "nak&#322;ad"
  ]
  node [
    id 85
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 86
    label "sumpt"
  ]
  node [
    id 87
    label "wydatek"
  ]
  node [
    id 88
    label "s&#261;downie"
  ]
  node [
    id 89
    label "urz&#281;dowy"
  ]
  node [
    id 90
    label "temat"
  ]
  node [
    id 91
    label "kognicja"
  ]
  node [
    id 92
    label "idea"
  ]
  node [
    id 93
    label "szczeg&#243;&#322;"
  ]
  node [
    id 94
    label "rzecz"
  ]
  node [
    id 95
    label "wydarzenie"
  ]
  node [
    id 96
    label "przes&#322;anka"
  ]
  node [
    id 97
    label "rozprawa"
  ]
  node [
    id 98
    label "object"
  ]
  node [
    id 99
    label "proposition"
  ]
  node [
    id 100
    label "cywilnie"
  ]
  node [
    id 101
    label "nieoficjalny"
  ]
  node [
    id 102
    label "prohibita"
  ]
  node [
    id 103
    label "impression"
  ]
  node [
    id 104
    label "wytw&#243;r"
  ]
  node [
    id 105
    label "tkanina"
  ]
  node [
    id 106
    label "glif"
  ]
  node [
    id 107
    label "formatowanie"
  ]
  node [
    id 108
    label "printing"
  ]
  node [
    id 109
    label "technika"
  ]
  node [
    id 110
    label "formatowa&#263;"
  ]
  node [
    id 111
    label "pismo"
  ]
  node [
    id 112
    label "cymelium"
  ]
  node [
    id 113
    label "zdobnik"
  ]
  node [
    id 114
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 115
    label "tekst"
  ]
  node [
    id 116
    label "publikacja"
  ]
  node [
    id 117
    label "zaproszenie"
  ]
  node [
    id 118
    label "dese&#324;"
  ]
  node [
    id 119
    label "character"
  ]
  node [
    id 120
    label "ojciec"
  ]
  node [
    id 121
    label "wyspa"
  ]
  node [
    id 122
    label "ministerstwo"
  ]
  node [
    id 123
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 124
    label "&#321;ukasz"
  ]
  node [
    id 125
    label "R&#281;dziniaka"
  ]
  node [
    id 126
    label "R&#281;dziniak"
  ]
  node [
    id 127
    label "gospodarka"
  ]
  node [
    id 128
    label "pakiet"
  ]
  node [
    id 129
    label "na"
  ]
  node [
    id 130
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 94
    target 128
  ]
  edge [
    source 94
    target 129
  ]
  edge [
    source 94
    target 130
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 127
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 126
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 130
  ]
  edge [
    source 129
    target 130
  ]
]
