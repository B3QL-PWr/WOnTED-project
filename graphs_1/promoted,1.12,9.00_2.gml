graph [
  maxDegree 37
  minDegree 1
  meanDegree 2.062015503875969
  density 0.01610949612403101
  graphCliqueNumber 2
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "przedsi&#281;biorca"
    origin "text"
  ]
  node [
    id 2
    label "brakowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 4
    label "praca"
    origin "text"
  ]
  node [
    id 5
    label "samo"
    origin "text"
  ]
  node [
    id 6
    label "polski"
    origin "text"
  ]
  node [
    id 7
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 8
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 9
    label "wolne"
    origin "text"
  ]
  node [
    id 10
    label "kobieta"
    origin "text"
  ]
  node [
    id 11
    label "wydawca"
  ]
  node [
    id 12
    label "kapitalista"
  ]
  node [
    id 13
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 14
    label "osoba_fizyczna"
  ]
  node [
    id 15
    label "wsp&#243;lnik"
  ]
  node [
    id 16
    label "klasa_&#347;rednia"
  ]
  node [
    id 17
    label "przebiera&#263;"
  ]
  node [
    id 18
    label "lack"
  ]
  node [
    id 19
    label "przegl&#261;da&#263;"
  ]
  node [
    id 20
    label "sprawdza&#263;"
  ]
  node [
    id 21
    label "utylizowa&#263;"
  ]
  node [
    id 22
    label "krzy&#380;"
  ]
  node [
    id 23
    label "paw"
  ]
  node [
    id 24
    label "rami&#281;"
  ]
  node [
    id 25
    label "gestykulowanie"
  ]
  node [
    id 26
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 27
    label "pracownik"
  ]
  node [
    id 28
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 29
    label "bramkarz"
  ]
  node [
    id 30
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 31
    label "handwriting"
  ]
  node [
    id 32
    label "hasta"
  ]
  node [
    id 33
    label "pi&#322;ka"
  ]
  node [
    id 34
    label "&#322;okie&#263;"
  ]
  node [
    id 35
    label "spos&#243;b"
  ]
  node [
    id 36
    label "zagrywka"
  ]
  node [
    id 37
    label "obietnica"
  ]
  node [
    id 38
    label "przedrami&#281;"
  ]
  node [
    id 39
    label "chwyta&#263;"
  ]
  node [
    id 40
    label "r&#261;czyna"
  ]
  node [
    id 41
    label "cecha"
  ]
  node [
    id 42
    label "wykroczenie"
  ]
  node [
    id 43
    label "kroki"
  ]
  node [
    id 44
    label "palec"
  ]
  node [
    id 45
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 46
    label "graba"
  ]
  node [
    id 47
    label "hand"
  ]
  node [
    id 48
    label "nadgarstek"
  ]
  node [
    id 49
    label "pomocnik"
  ]
  node [
    id 50
    label "k&#322;&#261;b"
  ]
  node [
    id 51
    label "hazena"
  ]
  node [
    id 52
    label "gestykulowa&#263;"
  ]
  node [
    id 53
    label "cmoknonsens"
  ]
  node [
    id 54
    label "d&#322;o&#324;"
  ]
  node [
    id 55
    label "chwytanie"
  ]
  node [
    id 56
    label "czerwona_kartka"
  ]
  node [
    id 57
    label "stosunek_pracy"
  ]
  node [
    id 58
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 59
    label "benedykty&#324;ski"
  ]
  node [
    id 60
    label "pracowanie"
  ]
  node [
    id 61
    label "zaw&#243;d"
  ]
  node [
    id 62
    label "kierownictwo"
  ]
  node [
    id 63
    label "zmiana"
  ]
  node [
    id 64
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 65
    label "wytw&#243;r"
  ]
  node [
    id 66
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 67
    label "tynkarski"
  ]
  node [
    id 68
    label "czynnik_produkcji"
  ]
  node [
    id 69
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 70
    label "zobowi&#261;zanie"
  ]
  node [
    id 71
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 72
    label "czynno&#347;&#263;"
  ]
  node [
    id 73
    label "tyrka"
  ]
  node [
    id 74
    label "pracowa&#263;"
  ]
  node [
    id 75
    label "siedziba"
  ]
  node [
    id 76
    label "poda&#380;_pracy"
  ]
  node [
    id 77
    label "miejsce"
  ]
  node [
    id 78
    label "zak&#322;ad"
  ]
  node [
    id 79
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 80
    label "najem"
  ]
  node [
    id 81
    label "lacki"
  ]
  node [
    id 82
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 83
    label "przedmiot"
  ]
  node [
    id 84
    label "sztajer"
  ]
  node [
    id 85
    label "drabant"
  ]
  node [
    id 86
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 87
    label "polak"
  ]
  node [
    id 88
    label "pierogi_ruskie"
  ]
  node [
    id 89
    label "krakowiak"
  ]
  node [
    id 90
    label "Polish"
  ]
  node [
    id 91
    label "j&#281;zyk"
  ]
  node [
    id 92
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 93
    label "oberek"
  ]
  node [
    id 94
    label "po_polsku"
  ]
  node [
    id 95
    label "mazur"
  ]
  node [
    id 96
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 97
    label "chodzony"
  ]
  node [
    id 98
    label "skoczny"
  ]
  node [
    id 99
    label "ryba_po_grecku"
  ]
  node [
    id 100
    label "goniony"
  ]
  node [
    id 101
    label "polsko"
  ]
  node [
    id 102
    label "ch&#322;opina"
  ]
  node [
    id 103
    label "cz&#322;owiek"
  ]
  node [
    id 104
    label "bratek"
  ]
  node [
    id 105
    label "jegomo&#347;&#263;"
  ]
  node [
    id 106
    label "doros&#322;y"
  ]
  node [
    id 107
    label "samiec"
  ]
  node [
    id 108
    label "ojciec"
  ]
  node [
    id 109
    label "twardziel"
  ]
  node [
    id 110
    label "androlog"
  ]
  node [
    id 111
    label "pa&#324;stwo"
  ]
  node [
    id 112
    label "m&#261;&#380;"
  ]
  node [
    id 113
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 114
    label "andropauza"
  ]
  node [
    id 115
    label "czas_wolny"
  ]
  node [
    id 116
    label "przekwitanie"
  ]
  node [
    id 117
    label "m&#281;&#380;yna"
  ]
  node [
    id 118
    label "babka"
  ]
  node [
    id 119
    label "samica"
  ]
  node [
    id 120
    label "ulec"
  ]
  node [
    id 121
    label "uleganie"
  ]
  node [
    id 122
    label "partnerka"
  ]
  node [
    id 123
    label "&#380;ona"
  ]
  node [
    id 124
    label "ulega&#263;"
  ]
  node [
    id 125
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 126
    label "ulegni&#281;cie"
  ]
  node [
    id 127
    label "menopauza"
  ]
  node [
    id 128
    label "&#322;ono"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
]
