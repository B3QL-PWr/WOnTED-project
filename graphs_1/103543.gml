graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9166666666666667
  density 0.08333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "niech"
    origin "text"
  ]
  node [
    id 1
    label "wszystko"
    origin "text"
  ]
  node [
    id 2
    label "stoa"
    origin "text"
  ]
  node [
    id 3
    label "naprzeciw"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "lock"
  ]
  node [
    id 6
    label "absolut"
  ]
  node [
    id 7
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 8
    label "kolumna"
  ]
  node [
    id 9
    label "budowla"
  ]
  node [
    id 10
    label "z_naprzeciwka"
  ]
  node [
    id 11
    label "wprost"
  ]
  node [
    id 12
    label "po_przeciwnej_stronie"
  ]
  node [
    id 13
    label "si&#281;ga&#263;"
  ]
  node [
    id 14
    label "trwa&#263;"
  ]
  node [
    id 15
    label "obecno&#347;&#263;"
  ]
  node [
    id 16
    label "stan"
  ]
  node [
    id 17
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 18
    label "stand"
  ]
  node [
    id 19
    label "mie&#263;_miejsce"
  ]
  node [
    id 20
    label "uczestniczy&#263;"
  ]
  node [
    id 21
    label "chodzi&#263;"
  ]
  node [
    id 22
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 23
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
]
