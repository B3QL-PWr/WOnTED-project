graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.192982456140351
  density 0.003854099219930318
  graphCliqueNumber 3
  node [
    id 0
    label "mks"
    origin "text"
  ]
  node [
    id 1
    label "pewnie"
    origin "text"
  ]
  node [
    id 2
    label "wygra&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 4
    label "brzesko"
    origin "text"
  ]
  node [
    id 5
    label "set"
    origin "text"
  ]
  node [
    id 6
    label "pierwszy"
    origin "text"
  ]
  node [
    id 7
    label "zapowiada&#263;"
    origin "text"
  ]
  node [
    id 8
    label "taki"
    origin "text"
  ]
  node [
    id 9
    label "przebieg"
    origin "text"
  ]
  node [
    id 10
    label "spotkanie"
    origin "text"
  ]
  node [
    id 11
    label "go&#347;cia"
    origin "text"
  ]
  node [
    id 12
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "pi&#322;ka"
    origin "text"
  ]
  node [
    id 14
    label "setowy"
    origin "text"
  ]
  node [
    id 15
    label "przy"
    origin "text"
  ]
  node [
    id 16
    label "puntkach"
    origin "text"
  ]
  node [
    id 17
    label "dla"
    origin "text"
  ]
  node [
    id 18
    label "bzura"
    origin "text"
  ]
  node [
    id 19
    label "nasi"
    origin "text"
  ]
  node [
    id 20
    label "walczy&#263;"
    origin "text"
  ]
  node [
    id 21
    label "koniec"
    origin "text"
  ]
  node [
    id 22
    label "ostatecznie"
    origin "text"
  ]
  node [
    id 23
    label "przegra&#263;"
    origin "text"
  ]
  node [
    id 24
    label "pierwsza"
    origin "text"
  ]
  node [
    id 25
    label "partia"
    origin "text"
  ]
  node [
    id 26
    label "wygranie"
    origin "text"
  ]
  node [
    id 27
    label "drugi"
    origin "text"
  ]
  node [
    id 28
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 29
    label "oba"
    origin "text"
  ]
  node [
    id 30
    label "gra&#263;"
    origin "text"
  ]
  node [
    id 31
    label "punkt"
    origin "text"
  ]
  node [
    id 32
    label "ozorkowianie"
    origin "text"
  ]
  node [
    id 33
    label "uwierzy&#263;"
    origin "text"
  ]
  node [
    id 34
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 35
    label "pokona&#263;"
    origin "text"
  ]
  node [
    id 36
    label "rywal"
    origin "text"
  ]
  node [
    id 37
    label "dos&#322;ownie"
    origin "text"
  ]
  node [
    id 38
    label "roznie&#347;&#263;"
    origin "text"
  ]
  node [
    id 39
    label "przyjezdny"
    origin "text"
  ]
  node [
    id 40
    label "&#347;wietnie"
    origin "text"
  ]
  node [
    id 41
    label "tym"
    origin "text"
  ]
  node [
    id 42
    label "okres"
    origin "text"
  ]
  node [
    id 43
    label "gra"
    origin "text"
  ]
  node [
    id 44
    label "spisywa&#263;"
    origin "text"
  ]
  node [
    id 45
    label "si&#281;"
    origin "text"
  ]
  node [
    id 46
    label "nasz"
    origin "text"
  ]
  node [
    id 47
    label "blok"
    origin "text"
  ]
  node [
    id 48
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 49
    label "ma&#322;y"
    origin "text"
  ]
  node [
    id 50
    label "szansa"
    origin "text"
  ]
  node [
    id 51
    label "sko&#324;czenie"
    origin "text"
  ]
  node [
    id 52
    label "akcja"
    origin "text"
  ]
  node [
    id 53
    label "atakowa&#263;"
    origin "text"
  ]
  node [
    id 54
    label "okocimski"
    origin "text"
  ]
  node [
    id 55
    label "decydyj&#261;cym"
    origin "text"
  ]
  node [
    id 56
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 57
    label "wyr&#243;wnany"
    origin "text"
  ]
  node [
    id 58
    label "stan"
    origin "text"
  ]
  node [
    id 59
    label "zwinnie"
  ]
  node [
    id 60
    label "pewniej"
  ]
  node [
    id 61
    label "bezpiecznie"
  ]
  node [
    id 62
    label "wiarygodnie"
  ]
  node [
    id 63
    label "pewny"
  ]
  node [
    id 64
    label "mocno"
  ]
  node [
    id 65
    label "najpewniej"
  ]
  node [
    id 66
    label "score"
  ]
  node [
    id 67
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 68
    label "zwojowa&#263;"
  ]
  node [
    id 69
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 70
    label "leave"
  ]
  node [
    id 71
    label "znie&#347;&#263;"
  ]
  node [
    id 72
    label "zagwarantowa&#263;"
  ]
  node [
    id 73
    label "instrument_muzyczny"
  ]
  node [
    id 74
    label "zagra&#263;"
  ]
  node [
    id 75
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 76
    label "zrobi&#263;"
  ]
  node [
    id 77
    label "net_income"
  ]
  node [
    id 78
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 79
    label "whole"
  ]
  node [
    id 80
    label "odm&#322;adza&#263;"
  ]
  node [
    id 81
    label "zabudowania"
  ]
  node [
    id 82
    label "odm&#322;odzenie"
  ]
  node [
    id 83
    label "zespolik"
  ]
  node [
    id 84
    label "skupienie"
  ]
  node [
    id 85
    label "schorzenie"
  ]
  node [
    id 86
    label "grupa"
  ]
  node [
    id 87
    label "Depeche_Mode"
  ]
  node [
    id 88
    label "Mazowsze"
  ]
  node [
    id 89
    label "ro&#347;lina"
  ]
  node [
    id 90
    label "zbi&#243;r"
  ]
  node [
    id 91
    label "The_Beatles"
  ]
  node [
    id 92
    label "group"
  ]
  node [
    id 93
    label "&#346;wietliki"
  ]
  node [
    id 94
    label "odm&#322;adzanie"
  ]
  node [
    id 95
    label "batch"
  ]
  node [
    id 96
    label "zestaw"
  ]
  node [
    id 97
    label "kompozycja"
  ]
  node [
    id 98
    label "gem"
  ]
  node [
    id 99
    label "muzyka"
  ]
  node [
    id 100
    label "runda"
  ]
  node [
    id 101
    label "najwa&#380;niejszy"
  ]
  node [
    id 102
    label "pocz&#261;tkowy"
  ]
  node [
    id 103
    label "dobry"
  ]
  node [
    id 104
    label "ch&#281;tny"
  ]
  node [
    id 105
    label "dzie&#324;"
  ]
  node [
    id 106
    label "pr&#281;dki"
  ]
  node [
    id 107
    label "informowa&#263;"
  ]
  node [
    id 108
    label "og&#322;asza&#263;"
  ]
  node [
    id 109
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 110
    label "bode"
  ]
  node [
    id 111
    label "post"
  ]
  node [
    id 112
    label "ostrzega&#263;"
  ]
  node [
    id 113
    label "harbinger"
  ]
  node [
    id 114
    label "okre&#347;lony"
  ]
  node [
    id 115
    label "jaki&#347;"
  ]
  node [
    id 116
    label "linia"
  ]
  node [
    id 117
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 118
    label "procedura"
  ]
  node [
    id 119
    label "sequence"
  ]
  node [
    id 120
    label "cycle"
  ]
  node [
    id 121
    label "ilo&#347;&#263;"
  ]
  node [
    id 122
    label "proces"
  ]
  node [
    id 123
    label "room"
  ]
  node [
    id 124
    label "praca"
  ]
  node [
    id 125
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 126
    label "po&#380;egnanie"
  ]
  node [
    id 127
    label "spowodowanie"
  ]
  node [
    id 128
    label "znalezienie"
  ]
  node [
    id 129
    label "znajomy"
  ]
  node [
    id 130
    label "doznanie"
  ]
  node [
    id 131
    label "employment"
  ]
  node [
    id 132
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 133
    label "gather"
  ]
  node [
    id 134
    label "powitanie"
  ]
  node [
    id 135
    label "spotykanie"
  ]
  node [
    id 136
    label "wydarzenie"
  ]
  node [
    id 137
    label "gathering"
  ]
  node [
    id 138
    label "spotkanie_si&#281;"
  ]
  node [
    id 139
    label "zdarzenie_si&#281;"
  ]
  node [
    id 140
    label "match"
  ]
  node [
    id 141
    label "zawarcie"
  ]
  node [
    id 142
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 143
    label "czu&#263;"
  ]
  node [
    id 144
    label "need"
  ]
  node [
    id 145
    label "hide"
  ]
  node [
    id 146
    label "support"
  ]
  node [
    id 147
    label "zagrywka"
  ]
  node [
    id 148
    label "serwowanie"
  ]
  node [
    id 149
    label "sport"
  ]
  node [
    id 150
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 151
    label "sport_zespo&#322;owy"
  ]
  node [
    id 152
    label "odbicie"
  ]
  node [
    id 153
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 154
    label "rzucanka"
  ]
  node [
    id 155
    label "aut"
  ]
  node [
    id 156
    label "orb"
  ]
  node [
    id 157
    label "zaserwowanie"
  ]
  node [
    id 158
    label "do&#347;rodkowywanie"
  ]
  node [
    id 159
    label "zaserwowa&#263;"
  ]
  node [
    id 160
    label "kula"
  ]
  node [
    id 161
    label "&#347;wieca"
  ]
  node [
    id 162
    label "serwowa&#263;"
  ]
  node [
    id 163
    label "musket_ball"
  ]
  node [
    id 164
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 165
    label "cope"
  ]
  node [
    id 166
    label "contend"
  ]
  node [
    id 167
    label "zawody"
  ]
  node [
    id 168
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 169
    label "dzia&#322;a&#263;"
  ]
  node [
    id 170
    label "wrestle"
  ]
  node [
    id 171
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 172
    label "robi&#263;"
  ]
  node [
    id 173
    label "my&#347;lenie"
  ]
  node [
    id 174
    label "argue"
  ]
  node [
    id 175
    label "stara&#263;_si&#281;"
  ]
  node [
    id 176
    label "fight"
  ]
  node [
    id 177
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 178
    label "defenestracja"
  ]
  node [
    id 179
    label "szereg"
  ]
  node [
    id 180
    label "dzia&#322;anie"
  ]
  node [
    id 181
    label "miejsce"
  ]
  node [
    id 182
    label "ostatnie_podrygi"
  ]
  node [
    id 183
    label "kres"
  ]
  node [
    id 184
    label "agonia"
  ]
  node [
    id 185
    label "visitation"
  ]
  node [
    id 186
    label "szeol"
  ]
  node [
    id 187
    label "mogi&#322;a"
  ]
  node [
    id 188
    label "chwila"
  ]
  node [
    id 189
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 190
    label "pogrzebanie"
  ]
  node [
    id 191
    label "&#380;a&#322;oba"
  ]
  node [
    id 192
    label "zabicie"
  ]
  node [
    id 193
    label "kres_&#380;ycia"
  ]
  node [
    id 194
    label "ostateczny"
  ]
  node [
    id 195
    label "finalnie"
  ]
  node [
    id 196
    label "zupe&#322;nie"
  ]
  node [
    id 197
    label "play"
  ]
  node [
    id 198
    label "ponie&#347;&#263;"
  ]
  node [
    id 199
    label "godzina"
  ]
  node [
    id 200
    label "SLD"
  ]
  node [
    id 201
    label "niedoczas"
  ]
  node [
    id 202
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 203
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 204
    label "game"
  ]
  node [
    id 205
    label "ZChN"
  ]
  node [
    id 206
    label "wybranka"
  ]
  node [
    id 207
    label "Wigowie"
  ]
  node [
    id 208
    label "egzekutywa"
  ]
  node [
    id 209
    label "unit"
  ]
  node [
    id 210
    label "Razem"
  ]
  node [
    id 211
    label "si&#322;a"
  ]
  node [
    id 212
    label "organizacja"
  ]
  node [
    id 213
    label "wybranek"
  ]
  node [
    id 214
    label "materia&#322;"
  ]
  node [
    id 215
    label "PiS"
  ]
  node [
    id 216
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 217
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 218
    label "AWS"
  ]
  node [
    id 219
    label "package"
  ]
  node [
    id 220
    label "Bund"
  ]
  node [
    id 221
    label "Kuomintang"
  ]
  node [
    id 222
    label "aktyw"
  ]
  node [
    id 223
    label "Jakobici"
  ]
  node [
    id 224
    label "PSL"
  ]
  node [
    id 225
    label "Federali&#347;ci"
  ]
  node [
    id 226
    label "ZSL"
  ]
  node [
    id 227
    label "PPR"
  ]
  node [
    id 228
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 229
    label "PO"
  ]
  node [
    id 230
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 231
    label "zrobienie"
  ]
  node [
    id 232
    label "zwojowanie"
  ]
  node [
    id 233
    label "beat"
  ]
  node [
    id 234
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 235
    label "zapanowanie"
  ]
  node [
    id 236
    label "zniesienie"
  ]
  node [
    id 237
    label "wygrywanie"
  ]
  node [
    id 238
    label "wr&#243;cenie_z_tarcz&#261;"
  ]
  node [
    id 239
    label "cz&#322;owiek"
  ]
  node [
    id 240
    label "inny"
  ]
  node [
    id 241
    label "kolejny"
  ]
  node [
    id 242
    label "przeciwny"
  ]
  node [
    id 243
    label "sw&#243;j"
  ]
  node [
    id 244
    label "odwrotnie"
  ]
  node [
    id 245
    label "podobny"
  ]
  node [
    id 246
    label "wt&#243;ry"
  ]
  node [
    id 247
    label "&#347;wieci&#263;"
  ]
  node [
    id 248
    label "typify"
  ]
  node [
    id 249
    label "majaczy&#263;"
  ]
  node [
    id 250
    label "rola"
  ]
  node [
    id 251
    label "wykonywa&#263;"
  ]
  node [
    id 252
    label "tokowa&#263;"
  ]
  node [
    id 253
    label "prezentowa&#263;"
  ]
  node [
    id 254
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 255
    label "rozgrywa&#263;"
  ]
  node [
    id 256
    label "przedstawia&#263;"
  ]
  node [
    id 257
    label "wykorzystywa&#263;"
  ]
  node [
    id 258
    label "wida&#263;"
  ]
  node [
    id 259
    label "brzmie&#263;"
  ]
  node [
    id 260
    label "dally"
  ]
  node [
    id 261
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 262
    label "do"
  ]
  node [
    id 263
    label "otwarcie"
  ]
  node [
    id 264
    label "szczeka&#263;"
  ]
  node [
    id 265
    label "pasowa&#263;"
  ]
  node [
    id 266
    label "napierdziela&#263;"
  ]
  node [
    id 267
    label "sound"
  ]
  node [
    id 268
    label "muzykowa&#263;"
  ]
  node [
    id 269
    label "i&#347;&#263;"
  ]
  node [
    id 270
    label "prosta"
  ]
  node [
    id 271
    label "po&#322;o&#380;enie"
  ]
  node [
    id 272
    label "ust&#281;p"
  ]
  node [
    id 273
    label "problemat"
  ]
  node [
    id 274
    label "mark"
  ]
  node [
    id 275
    label "pozycja"
  ]
  node [
    id 276
    label "point"
  ]
  node [
    id 277
    label "stopie&#324;_pisma"
  ]
  node [
    id 278
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 279
    label "przestrze&#324;"
  ]
  node [
    id 280
    label "wojsko"
  ]
  node [
    id 281
    label "problematyka"
  ]
  node [
    id 282
    label "zapunktowa&#263;"
  ]
  node [
    id 283
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 284
    label "obiekt_matematyczny"
  ]
  node [
    id 285
    label "sprawa"
  ]
  node [
    id 286
    label "plamka"
  ]
  node [
    id 287
    label "obiekt"
  ]
  node [
    id 288
    label "plan"
  ]
  node [
    id 289
    label "podpunkt"
  ]
  node [
    id 290
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 291
    label "jednostka"
  ]
  node [
    id 292
    label "trust"
  ]
  node [
    id 293
    label "zacz&#261;&#263;"
  ]
  node [
    id 294
    label "uzna&#263;"
  ]
  node [
    id 295
    label "free"
  ]
  node [
    id 296
    label "z&#322;oi&#263;"
  ]
  node [
    id 297
    label "zaatakowa&#263;"
  ]
  node [
    id 298
    label "zapobiec"
  ]
  node [
    id 299
    label "poradzi&#263;_sobie"
  ]
  node [
    id 300
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 301
    label "overwhelm"
  ]
  node [
    id 302
    label "przeciwnik"
  ]
  node [
    id 303
    label "zalotnik"
  ]
  node [
    id 304
    label "konkurencja"
  ]
  node [
    id 305
    label "sp&#243;&#322;zawodnik"
  ]
  node [
    id 306
    label "bezpo&#347;rednio"
  ]
  node [
    id 307
    label "dos&#322;owny"
  ]
  node [
    id 308
    label "literally"
  ]
  node [
    id 309
    label "prawdziwie"
  ]
  node [
    id 310
    label "wiernie"
  ]
  node [
    id 311
    label "rozpowszechni&#263;"
  ]
  node [
    id 312
    label "zanie&#347;&#263;"
  ]
  node [
    id 313
    label "zniszczy&#263;"
  ]
  node [
    id 314
    label "deliver"
  ]
  node [
    id 315
    label "przybysz"
  ]
  node [
    id 316
    label "nietutejszy"
  ]
  node [
    id 317
    label "zajebi&#347;cie"
  ]
  node [
    id 318
    label "dobrze"
  ]
  node [
    id 319
    label "pomy&#347;lnie"
  ]
  node [
    id 320
    label "pozytywnie"
  ]
  node [
    id 321
    label "wspania&#322;y"
  ]
  node [
    id 322
    label "&#347;wietny"
  ]
  node [
    id 323
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 324
    label "skutecznie"
  ]
  node [
    id 325
    label "paleogen"
  ]
  node [
    id 326
    label "spell"
  ]
  node [
    id 327
    label "czas"
  ]
  node [
    id 328
    label "period"
  ]
  node [
    id 329
    label "prekambr"
  ]
  node [
    id 330
    label "jura"
  ]
  node [
    id 331
    label "interstadia&#322;"
  ]
  node [
    id 332
    label "jednostka_geologiczna"
  ]
  node [
    id 333
    label "izochronizm"
  ]
  node [
    id 334
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 335
    label "okres_noachijski"
  ]
  node [
    id 336
    label "orosir"
  ]
  node [
    id 337
    label "kreda"
  ]
  node [
    id 338
    label "sten"
  ]
  node [
    id 339
    label "drugorz&#281;d"
  ]
  node [
    id 340
    label "semester"
  ]
  node [
    id 341
    label "trzeciorz&#281;d"
  ]
  node [
    id 342
    label "ton"
  ]
  node [
    id 343
    label "dzieje"
  ]
  node [
    id 344
    label "poprzednik"
  ]
  node [
    id 345
    label "ordowik"
  ]
  node [
    id 346
    label "karbon"
  ]
  node [
    id 347
    label "trias"
  ]
  node [
    id 348
    label "kalim"
  ]
  node [
    id 349
    label "stater"
  ]
  node [
    id 350
    label "era"
  ]
  node [
    id 351
    label "cykl"
  ]
  node [
    id 352
    label "p&#243;&#322;okres"
  ]
  node [
    id 353
    label "czwartorz&#281;d"
  ]
  node [
    id 354
    label "pulsacja"
  ]
  node [
    id 355
    label "okres_amazo&#324;ski"
  ]
  node [
    id 356
    label "kambr"
  ]
  node [
    id 357
    label "Zeitgeist"
  ]
  node [
    id 358
    label "nast&#281;pnik"
  ]
  node [
    id 359
    label "kriogen"
  ]
  node [
    id 360
    label "glacja&#322;"
  ]
  node [
    id 361
    label "fala"
  ]
  node [
    id 362
    label "okres_czasu"
  ]
  node [
    id 363
    label "riak"
  ]
  node [
    id 364
    label "schy&#322;ek"
  ]
  node [
    id 365
    label "okres_hesperyjski"
  ]
  node [
    id 366
    label "sylur"
  ]
  node [
    id 367
    label "dewon"
  ]
  node [
    id 368
    label "ciota"
  ]
  node [
    id 369
    label "epoka"
  ]
  node [
    id 370
    label "pierwszorz&#281;d"
  ]
  node [
    id 371
    label "okres_halsztacki"
  ]
  node [
    id 372
    label "ektas"
  ]
  node [
    id 373
    label "zdanie"
  ]
  node [
    id 374
    label "condition"
  ]
  node [
    id 375
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 376
    label "rok_akademicki"
  ]
  node [
    id 377
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 378
    label "postglacja&#322;"
  ]
  node [
    id 379
    label "faza"
  ]
  node [
    id 380
    label "proces_fizjologiczny"
  ]
  node [
    id 381
    label "ediakar"
  ]
  node [
    id 382
    label "time_period"
  ]
  node [
    id 383
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 384
    label "perm"
  ]
  node [
    id 385
    label "rok_szkolny"
  ]
  node [
    id 386
    label "neogen"
  ]
  node [
    id 387
    label "sider"
  ]
  node [
    id 388
    label "flow"
  ]
  node [
    id 389
    label "podokres"
  ]
  node [
    id 390
    label "preglacja&#322;"
  ]
  node [
    id 391
    label "retoryka"
  ]
  node [
    id 392
    label "choroba_przyrodzona"
  ]
  node [
    id 393
    label "zabawa"
  ]
  node [
    id 394
    label "rywalizacja"
  ]
  node [
    id 395
    label "czynno&#347;&#263;"
  ]
  node [
    id 396
    label "Pok&#233;mon"
  ]
  node [
    id 397
    label "synteza"
  ]
  node [
    id 398
    label "odtworzenie"
  ]
  node [
    id 399
    label "komplet"
  ]
  node [
    id 400
    label "rekwizyt_do_gry"
  ]
  node [
    id 401
    label "odg&#322;os"
  ]
  node [
    id 402
    label "rozgrywka"
  ]
  node [
    id 403
    label "post&#281;powanie"
  ]
  node [
    id 404
    label "apparent_motion"
  ]
  node [
    id 405
    label "zmienno&#347;&#263;"
  ]
  node [
    id 406
    label "zasada"
  ]
  node [
    id 407
    label "contest"
  ]
  node [
    id 408
    label "zbijany"
  ]
  node [
    id 409
    label "przepisywa&#263;"
  ]
  node [
    id 410
    label "sporz&#261;dza&#263;"
  ]
  node [
    id 411
    label "sprawdzian"
  ]
  node [
    id 412
    label "clamp"
  ]
  node [
    id 413
    label "redagowa&#263;"
  ]
  node [
    id 414
    label "write"
  ]
  node [
    id 415
    label "czyj&#347;"
  ]
  node [
    id 416
    label "square"
  ]
  node [
    id 417
    label "zamek"
  ]
  node [
    id 418
    label "block"
  ]
  node [
    id 419
    label "budynek"
  ]
  node [
    id 420
    label "blokowisko"
  ]
  node [
    id 421
    label "barak"
  ]
  node [
    id 422
    label "stok_kontynentalny"
  ]
  node [
    id 423
    label "blokada"
  ]
  node [
    id 424
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 425
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 426
    label "siatk&#243;wka"
  ]
  node [
    id 427
    label "kr&#261;g"
  ]
  node [
    id 428
    label "bloking"
  ]
  node [
    id 429
    label "dom_wielorodzinny"
  ]
  node [
    id 430
    label "dzia&#322;"
  ]
  node [
    id 431
    label "przeszkoda"
  ]
  node [
    id 432
    label "bie&#380;nia"
  ]
  node [
    id 433
    label "start"
  ]
  node [
    id 434
    label "blockage"
  ]
  node [
    id 435
    label "obrona"
  ]
  node [
    id 436
    label "bajt"
  ]
  node [
    id 437
    label "artyku&#322;"
  ]
  node [
    id 438
    label "bry&#322;a"
  ]
  node [
    id 439
    label "zeszyt"
  ]
  node [
    id 440
    label "ok&#322;adka"
  ]
  node [
    id 441
    label "kontynent"
  ]
  node [
    id 442
    label "referat"
  ]
  node [
    id 443
    label "nastawnia"
  ]
  node [
    id 444
    label "skorupa_ziemska"
  ]
  node [
    id 445
    label "program"
  ]
  node [
    id 446
    label "ram&#243;wka"
  ]
  node [
    id 447
    label "j&#261;kanie"
  ]
  node [
    id 448
    label "render"
  ]
  node [
    id 449
    label "hold"
  ]
  node [
    id 450
    label "surrender"
  ]
  node [
    id 451
    label "traktowa&#263;"
  ]
  node [
    id 452
    label "dostarcza&#263;"
  ]
  node [
    id 453
    label "tender"
  ]
  node [
    id 454
    label "train"
  ]
  node [
    id 455
    label "give"
  ]
  node [
    id 456
    label "umieszcza&#263;"
  ]
  node [
    id 457
    label "nalewa&#263;"
  ]
  node [
    id 458
    label "przeznacza&#263;"
  ]
  node [
    id 459
    label "p&#322;aci&#263;"
  ]
  node [
    id 460
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 461
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 462
    label "powierza&#263;"
  ]
  node [
    id 463
    label "hold_out"
  ]
  node [
    id 464
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 465
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 466
    label "mie&#263;_miejsce"
  ]
  node [
    id 467
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 468
    label "t&#322;uc"
  ]
  node [
    id 469
    label "wpiernicza&#263;"
  ]
  node [
    id 470
    label "przekazywa&#263;"
  ]
  node [
    id 471
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 472
    label "zezwala&#263;"
  ]
  node [
    id 473
    label "rap"
  ]
  node [
    id 474
    label "obiecywa&#263;"
  ]
  node [
    id 475
    label "&#322;adowa&#263;"
  ]
  node [
    id 476
    label "odst&#281;powa&#263;"
  ]
  node [
    id 477
    label "exsert"
  ]
  node [
    id 478
    label "marny"
  ]
  node [
    id 479
    label "nieznaczny"
  ]
  node [
    id 480
    label "s&#322;aby"
  ]
  node [
    id 481
    label "ch&#322;opiec"
  ]
  node [
    id 482
    label "ma&#322;o"
  ]
  node [
    id 483
    label "n&#281;dznie"
  ]
  node [
    id 484
    label "niewa&#380;ny"
  ]
  node [
    id 485
    label "przeci&#281;tny"
  ]
  node [
    id 486
    label "nieliczny"
  ]
  node [
    id 487
    label "wstydliwy"
  ]
  node [
    id 488
    label "szybki"
  ]
  node [
    id 489
    label "m&#322;ody"
  ]
  node [
    id 490
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 491
    label "umarcie"
  ]
  node [
    id 492
    label "uczenie_si&#281;"
  ]
  node [
    id 493
    label "sko&#324;czony"
  ]
  node [
    id 494
    label "zako&#324;czenie"
  ]
  node [
    id 495
    label "kompletnie"
  ]
  node [
    id 496
    label "termination"
  ]
  node [
    id 497
    label "completion"
  ]
  node [
    id 498
    label "czyn"
  ]
  node [
    id 499
    label "wysoko&#347;&#263;"
  ]
  node [
    id 500
    label "stock"
  ]
  node [
    id 501
    label "w&#281;ze&#322;"
  ]
  node [
    id 502
    label "instrument_strunowy"
  ]
  node [
    id 503
    label "dywidenda"
  ]
  node [
    id 504
    label "udzia&#322;"
  ]
  node [
    id 505
    label "occupation"
  ]
  node [
    id 506
    label "jazda"
  ]
  node [
    id 507
    label "commotion"
  ]
  node [
    id 508
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 509
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 510
    label "operacja"
  ]
  node [
    id 511
    label "napada&#263;"
  ]
  node [
    id 512
    label "nast&#281;powa&#263;"
  ]
  node [
    id 513
    label "strike"
  ]
  node [
    id 514
    label "trouble_oneself"
  ]
  node [
    id 515
    label "m&#243;wi&#263;"
  ]
  node [
    id 516
    label "usi&#322;owa&#263;"
  ]
  node [
    id 517
    label "aim"
  ]
  node [
    id 518
    label "przewaga"
  ]
  node [
    id 519
    label "attack"
  ]
  node [
    id 520
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 521
    label "epidemia"
  ]
  node [
    id 522
    label "krytykowa&#263;"
  ]
  node [
    id 523
    label "ofensywny"
  ]
  node [
    id 524
    label "partnerka"
  ]
  node [
    id 525
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 526
    label "Arizona"
  ]
  node [
    id 527
    label "Georgia"
  ]
  node [
    id 528
    label "warstwa"
  ]
  node [
    id 529
    label "jednostka_administracyjna"
  ]
  node [
    id 530
    label "Goa"
  ]
  node [
    id 531
    label "Hawaje"
  ]
  node [
    id 532
    label "Floryda"
  ]
  node [
    id 533
    label "Oklahoma"
  ]
  node [
    id 534
    label "Alaska"
  ]
  node [
    id 535
    label "Alabama"
  ]
  node [
    id 536
    label "wci&#281;cie"
  ]
  node [
    id 537
    label "Oregon"
  ]
  node [
    id 538
    label "poziom"
  ]
  node [
    id 539
    label "by&#263;"
  ]
  node [
    id 540
    label "Teksas"
  ]
  node [
    id 541
    label "Illinois"
  ]
  node [
    id 542
    label "Jukatan"
  ]
  node [
    id 543
    label "Waszyngton"
  ]
  node [
    id 544
    label "shape"
  ]
  node [
    id 545
    label "Nowy_Meksyk"
  ]
  node [
    id 546
    label "state"
  ]
  node [
    id 547
    label "Nowy_York"
  ]
  node [
    id 548
    label "Arakan"
  ]
  node [
    id 549
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 550
    label "Kalifornia"
  ]
  node [
    id 551
    label "wektor"
  ]
  node [
    id 552
    label "Massachusetts"
  ]
  node [
    id 553
    label "Pensylwania"
  ]
  node [
    id 554
    label "Maryland"
  ]
  node [
    id 555
    label "Michigan"
  ]
  node [
    id 556
    label "Ohio"
  ]
  node [
    id 557
    label "Kansas"
  ]
  node [
    id 558
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 559
    label "Luizjana"
  ]
  node [
    id 560
    label "samopoczucie"
  ]
  node [
    id 561
    label "Wirginia"
  ]
  node [
    id 562
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 563
    label "tomka"
  ]
  node [
    id 564
    label "wojciechowski"
  ]
  node [
    id 565
    label "Marcin"
  ]
  node [
    id 566
    label "Sadecki"
  ]
  node [
    id 567
    label "Wawel"
  ]
  node [
    id 568
    label "AGH"
  ]
  node [
    id 569
    label "Krak&#243;w"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 53
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 136
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 200
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 25
    target 202
  ]
  edge [
    source 25
    target 203
  ]
  edge [
    source 25
    target 86
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 47
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 43
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 189
  ]
  edge [
    source 25
    target 228
  ]
  edge [
    source 25
    target 229
  ]
  edge [
    source 25
    target 230
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 139
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 27
    target 239
  ]
  edge [
    source 27
    target 240
  ]
  edge [
    source 27
    target 241
  ]
  edge [
    source 27
    target 242
  ]
  edge [
    source 27
    target 243
  ]
  edge [
    source 27
    target 244
  ]
  edge [
    source 27
    target 105
  ]
  edge [
    source 27
    target 245
  ]
  edge [
    source 27
    target 246
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 247
  ]
  edge [
    source 30
    target 248
  ]
  edge [
    source 30
    target 249
  ]
  edge [
    source 30
    target 169
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 259
  ]
  edge [
    source 30
    target 260
  ]
  edge [
    source 30
    target 261
  ]
  edge [
    source 30
    target 172
  ]
  edge [
    source 30
    target 262
  ]
  edge [
    source 30
    target 73
  ]
  edge [
    source 30
    target 197
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 264
  ]
  edge [
    source 30
    target 165
  ]
  edge [
    source 30
    target 265
  ]
  edge [
    source 30
    target 266
  ]
  edge [
    source 30
    target 267
  ]
  edge [
    source 30
    target 268
  ]
  edge [
    source 30
    target 175
  ]
  edge [
    source 30
    target 269
  ]
  edge [
    source 31
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 270
  ]
  edge [
    source 31
    target 271
  ]
  edge [
    source 31
    target 188
  ]
  edge [
    source 31
    target 272
  ]
  edge [
    source 31
    target 273
  ]
  edge [
    source 31
    target 183
  ]
  edge [
    source 31
    target 274
  ]
  edge [
    source 31
    target 275
  ]
  edge [
    source 31
    target 276
  ]
  edge [
    source 31
    target 277
  ]
  edge [
    source 31
    target 278
  ]
  edge [
    source 31
    target 279
  ]
  edge [
    source 31
    target 280
  ]
  edge [
    source 31
    target 281
  ]
  edge [
    source 31
    target 282
  ]
  edge [
    source 31
    target 283
  ]
  edge [
    source 31
    target 284
  ]
  edge [
    source 31
    target 285
  ]
  edge [
    source 31
    target 286
  ]
  edge [
    source 31
    target 181
  ]
  edge [
    source 31
    target 287
  ]
  edge [
    source 31
    target 288
  ]
  edge [
    source 31
    target 289
  ]
  edge [
    source 31
    target 290
  ]
  edge [
    source 31
    target 291
  ]
  edge [
    source 31
    target 58
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 292
  ]
  edge [
    source 33
    target 293
  ]
  edge [
    source 33
    target 294
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 295
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 296
  ]
  edge [
    source 35
    target 297
  ]
  edge [
    source 35
    target 298
  ]
  edge [
    source 35
    target 299
  ]
  edge [
    source 35
    target 300
  ]
  edge [
    source 35
    target 301
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 302
  ]
  edge [
    source 36
    target 303
  ]
  edge [
    source 36
    target 304
  ]
  edge [
    source 36
    target 305
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 306
  ]
  edge [
    source 37
    target 307
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 309
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 311
  ]
  edge [
    source 38
    target 312
  ]
  edge [
    source 38
    target 313
  ]
  edge [
    source 38
    target 314
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 315
  ]
  edge [
    source 39
    target 316
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 317
  ]
  edge [
    source 40
    target 318
  ]
  edge [
    source 40
    target 319
  ]
  edge [
    source 40
    target 320
  ]
  edge [
    source 40
    target 321
  ]
  edge [
    source 40
    target 322
  ]
  edge [
    source 40
    target 323
  ]
  edge [
    source 40
    target 324
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 325
  ]
  edge [
    source 42
    target 326
  ]
  edge [
    source 42
    target 327
  ]
  edge [
    source 42
    target 328
  ]
  edge [
    source 42
    target 329
  ]
  edge [
    source 42
    target 330
  ]
  edge [
    source 42
    target 331
  ]
  edge [
    source 42
    target 332
  ]
  edge [
    source 42
    target 333
  ]
  edge [
    source 42
    target 334
  ]
  edge [
    source 42
    target 335
  ]
  edge [
    source 42
    target 336
  ]
  edge [
    source 42
    target 337
  ]
  edge [
    source 42
    target 338
  ]
  edge [
    source 42
    target 339
  ]
  edge [
    source 42
    target 340
  ]
  edge [
    source 42
    target 341
  ]
  edge [
    source 42
    target 342
  ]
  edge [
    source 42
    target 343
  ]
  edge [
    source 42
    target 344
  ]
  edge [
    source 42
    target 345
  ]
  edge [
    source 42
    target 346
  ]
  edge [
    source 42
    target 347
  ]
  edge [
    source 42
    target 348
  ]
  edge [
    source 42
    target 349
  ]
  edge [
    source 42
    target 350
  ]
  edge [
    source 42
    target 351
  ]
  edge [
    source 42
    target 352
  ]
  edge [
    source 42
    target 353
  ]
  edge [
    source 42
    target 354
  ]
  edge [
    source 42
    target 355
  ]
  edge [
    source 42
    target 356
  ]
  edge [
    source 42
    target 357
  ]
  edge [
    source 42
    target 358
  ]
  edge [
    source 42
    target 359
  ]
  edge [
    source 42
    target 360
  ]
  edge [
    source 42
    target 361
  ]
  edge [
    source 42
    target 362
  ]
  edge [
    source 42
    target 363
  ]
  edge [
    source 42
    target 364
  ]
  edge [
    source 42
    target 365
  ]
  edge [
    source 42
    target 366
  ]
  edge [
    source 42
    target 367
  ]
  edge [
    source 42
    target 368
  ]
  edge [
    source 42
    target 369
  ]
  edge [
    source 42
    target 370
  ]
  edge [
    source 42
    target 371
  ]
  edge [
    source 42
    target 372
  ]
  edge [
    source 42
    target 373
  ]
  edge [
    source 42
    target 374
  ]
  edge [
    source 42
    target 375
  ]
  edge [
    source 42
    target 376
  ]
  edge [
    source 42
    target 377
  ]
  edge [
    source 42
    target 378
  ]
  edge [
    source 42
    target 379
  ]
  edge [
    source 42
    target 380
  ]
  edge [
    source 42
    target 381
  ]
  edge [
    source 42
    target 382
  ]
  edge [
    source 42
    target 383
  ]
  edge [
    source 42
    target 384
  ]
  edge [
    source 42
    target 385
  ]
  edge [
    source 42
    target 386
  ]
  edge [
    source 42
    target 387
  ]
  edge [
    source 42
    target 388
  ]
  edge [
    source 42
    target 389
  ]
  edge [
    source 42
    target 390
  ]
  edge [
    source 42
    target 391
  ]
  edge [
    source 42
    target 392
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 56
  ]
  edge [
    source 43
    target 393
  ]
  edge [
    source 43
    target 394
  ]
  edge [
    source 43
    target 395
  ]
  edge [
    source 43
    target 396
  ]
  edge [
    source 43
    target 397
  ]
  edge [
    source 43
    target 398
  ]
  edge [
    source 43
    target 399
  ]
  edge [
    source 43
    target 400
  ]
  edge [
    source 43
    target 401
  ]
  edge [
    source 43
    target 402
  ]
  edge [
    source 43
    target 403
  ]
  edge [
    source 43
    target 136
  ]
  edge [
    source 43
    target 404
  ]
  edge [
    source 43
    target 204
  ]
  edge [
    source 43
    target 405
  ]
  edge [
    source 43
    target 406
  ]
  edge [
    source 43
    target 52
  ]
  edge [
    source 43
    target 197
  ]
  edge [
    source 43
    target 407
  ]
  edge [
    source 43
    target 408
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 409
  ]
  edge [
    source 44
    target 410
  ]
  edge [
    source 44
    target 411
  ]
  edge [
    source 44
    target 412
  ]
  edge [
    source 44
    target 413
  ]
  edge [
    source 44
    target 414
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 415
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 416
  ]
  edge [
    source 47
    target 79
  ]
  edge [
    source 47
    target 417
  ]
  edge [
    source 47
    target 418
  ]
  edge [
    source 47
    target 86
  ]
  edge [
    source 47
    target 419
  ]
  edge [
    source 47
    target 420
  ]
  edge [
    source 47
    target 421
  ]
  edge [
    source 47
    target 422
  ]
  edge [
    source 47
    target 423
  ]
  edge [
    source 47
    target 424
  ]
  edge [
    source 47
    target 425
  ]
  edge [
    source 47
    target 426
  ]
  edge [
    source 47
    target 427
  ]
  edge [
    source 47
    target 428
  ]
  edge [
    source 47
    target 429
  ]
  edge [
    source 47
    target 430
  ]
  edge [
    source 47
    target 431
  ]
  edge [
    source 47
    target 432
  ]
  edge [
    source 47
    target 212
  ]
  edge [
    source 47
    target 433
  ]
  edge [
    source 47
    target 434
  ]
  edge [
    source 47
    target 435
  ]
  edge [
    source 47
    target 436
  ]
  edge [
    source 47
    target 437
  ]
  edge [
    source 47
    target 438
  ]
  edge [
    source 47
    target 439
  ]
  edge [
    source 47
    target 440
  ]
  edge [
    source 47
    target 441
  ]
  edge [
    source 47
    target 442
  ]
  edge [
    source 47
    target 443
  ]
  edge [
    source 47
    target 444
  ]
  edge [
    source 47
    target 445
  ]
  edge [
    source 47
    target 90
  ]
  edge [
    source 47
    target 446
  ]
  edge [
    source 47
    target 447
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 448
  ]
  edge [
    source 48
    target 449
  ]
  edge [
    source 48
    target 450
  ]
  edge [
    source 48
    target 451
  ]
  edge [
    source 48
    target 452
  ]
  edge [
    source 48
    target 453
  ]
  edge [
    source 48
    target 454
  ]
  edge [
    source 48
    target 455
  ]
  edge [
    source 48
    target 456
  ]
  edge [
    source 48
    target 457
  ]
  edge [
    source 48
    target 458
  ]
  edge [
    source 48
    target 459
  ]
  edge [
    source 48
    target 460
  ]
  edge [
    source 48
    target 461
  ]
  edge [
    source 48
    target 462
  ]
  edge [
    source 48
    target 463
  ]
  edge [
    source 48
    target 464
  ]
  edge [
    source 48
    target 465
  ]
  edge [
    source 48
    target 466
  ]
  edge [
    source 48
    target 467
  ]
  edge [
    source 48
    target 172
  ]
  edge [
    source 48
    target 468
  ]
  edge [
    source 48
    target 469
  ]
  edge [
    source 48
    target 470
  ]
  edge [
    source 48
    target 471
  ]
  edge [
    source 48
    target 472
  ]
  edge [
    source 48
    target 473
  ]
  edge [
    source 48
    target 474
  ]
  edge [
    source 48
    target 475
  ]
  edge [
    source 48
    target 476
  ]
  edge [
    source 48
    target 477
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 478
  ]
  edge [
    source 49
    target 479
  ]
  edge [
    source 49
    target 480
  ]
  edge [
    source 49
    target 481
  ]
  edge [
    source 49
    target 482
  ]
  edge [
    source 49
    target 483
  ]
  edge [
    source 49
    target 484
  ]
  edge [
    source 49
    target 485
  ]
  edge [
    source 49
    target 486
  ]
  edge [
    source 49
    target 487
  ]
  edge [
    source 49
    target 488
  ]
  edge [
    source 49
    target 489
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 490
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 180
  ]
  edge [
    source 51
    target 231
  ]
  edge [
    source 51
    target 491
  ]
  edge [
    source 51
    target 492
  ]
  edge [
    source 51
    target 493
  ]
  edge [
    source 51
    target 494
  ]
  edge [
    source 51
    target 495
  ]
  edge [
    source 51
    target 496
  ]
  edge [
    source 51
    target 497
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 147
  ]
  edge [
    source 52
    target 498
  ]
  edge [
    source 52
    target 395
  ]
  edge [
    source 52
    target 499
  ]
  edge [
    source 52
    target 500
  ]
  edge [
    source 52
    target 501
  ]
  edge [
    source 52
    target 502
  ]
  edge [
    source 52
    target 503
  ]
  edge [
    source 52
    target 504
  ]
  edge [
    source 52
    target 505
  ]
  edge [
    source 52
    target 506
  ]
  edge [
    source 52
    target 136
  ]
  edge [
    source 52
    target 507
  ]
  edge [
    source 52
    target 508
  ]
  edge [
    source 52
    target 509
  ]
  edge [
    source 52
    target 510
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 511
  ]
  edge [
    source 53
    target 169
  ]
  edge [
    source 53
    target 512
  ]
  edge [
    source 53
    target 513
  ]
  edge [
    source 53
    target 514
  ]
  edge [
    source 53
    target 515
  ]
  edge [
    source 53
    target 516
  ]
  edge [
    source 53
    target 255
  ]
  edge [
    source 53
    target 517
  ]
  edge [
    source 53
    target 149
  ]
  edge [
    source 53
    target 518
  ]
  edge [
    source 53
    target 519
  ]
  edge [
    source 53
    target 467
  ]
  edge [
    source 53
    target 172
  ]
  edge [
    source 53
    target 520
  ]
  edge [
    source 53
    target 85
  ]
  edge [
    source 53
    target 521
  ]
  edge [
    source 53
    target 522
  ]
  edge [
    source 53
    target 523
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 524
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 525
  ]
  edge [
    source 58
    target 526
  ]
  edge [
    source 58
    target 527
  ]
  edge [
    source 58
    target 528
  ]
  edge [
    source 58
    target 529
  ]
  edge [
    source 58
    target 530
  ]
  edge [
    source 58
    target 531
  ]
  edge [
    source 58
    target 532
  ]
  edge [
    source 58
    target 533
  ]
  edge [
    source 58
    target 534
  ]
  edge [
    source 58
    target 535
  ]
  edge [
    source 58
    target 536
  ]
  edge [
    source 58
    target 537
  ]
  edge [
    source 58
    target 538
  ]
  edge [
    source 58
    target 539
  ]
  edge [
    source 58
    target 540
  ]
  edge [
    source 58
    target 541
  ]
  edge [
    source 58
    target 542
  ]
  edge [
    source 58
    target 543
  ]
  edge [
    source 58
    target 544
  ]
  edge [
    source 58
    target 545
  ]
  edge [
    source 58
    target 121
  ]
  edge [
    source 58
    target 546
  ]
  edge [
    source 58
    target 547
  ]
  edge [
    source 58
    target 548
  ]
  edge [
    source 58
    target 549
  ]
  edge [
    source 58
    target 550
  ]
  edge [
    source 58
    target 551
  ]
  edge [
    source 58
    target 552
  ]
  edge [
    source 58
    target 181
  ]
  edge [
    source 58
    target 553
  ]
  edge [
    source 58
    target 554
  ]
  edge [
    source 58
    target 555
  ]
  edge [
    source 58
    target 556
  ]
  edge [
    source 58
    target 557
  ]
  edge [
    source 58
    target 558
  ]
  edge [
    source 58
    target 559
  ]
  edge [
    source 58
    target 560
  ]
  edge [
    source 58
    target 561
  ]
  edge [
    source 58
    target 562
  ]
  edge [
    source 563
    target 564
  ]
  edge [
    source 565
    target 566
  ]
  edge [
    source 567
    target 568
  ]
  edge [
    source 567
    target 569
  ]
  edge [
    source 568
    target 569
  ]
]
