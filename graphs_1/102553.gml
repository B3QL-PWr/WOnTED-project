graph [
  maxDegree 36
  minDegree 1
  meanDegree 2
  density 0.01652892561983471
  graphCliqueNumber 2
  node [
    id 0
    label "lista"
    origin "text"
  ]
  node [
    id 1
    label "s&#322;aby"
    origin "text"
  ]
  node [
    id 2
    label "strona"
    origin "text"
  ]
  node [
    id 3
    label "odczuwa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "bariera"
    origin "text"
  ]
  node [
    id 5
    label "rozwojowy"
    origin "text"
  ]
  node [
    id 6
    label "organizacja"
    origin "text"
  ]
  node [
    id 7
    label "pozarz&#261;dowy"
    origin "text"
  ]
  node [
    id 8
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 9
    label "w&#322;asny"
    origin "text"
  ]
  node [
    id 10
    label "ocena"
    origin "text"
  ]
  node [
    id 11
    label "wyliczanka"
  ]
  node [
    id 12
    label "catalog"
  ]
  node [
    id 13
    label "stock"
  ]
  node [
    id 14
    label "figurowa&#263;"
  ]
  node [
    id 15
    label "zbi&#243;r"
  ]
  node [
    id 16
    label "book"
  ]
  node [
    id 17
    label "pozycja"
  ]
  node [
    id 18
    label "tekst"
  ]
  node [
    id 19
    label "sumariusz"
  ]
  node [
    id 20
    label "nieznaczny"
  ]
  node [
    id 21
    label "nieumiej&#281;tny"
  ]
  node [
    id 22
    label "marnie"
  ]
  node [
    id 23
    label "md&#322;y"
  ]
  node [
    id 24
    label "przemijaj&#261;cy"
  ]
  node [
    id 25
    label "zawodny"
  ]
  node [
    id 26
    label "delikatny"
  ]
  node [
    id 27
    label "&#322;agodny"
  ]
  node [
    id 28
    label "niedoskona&#322;y"
  ]
  node [
    id 29
    label "nietrwa&#322;y"
  ]
  node [
    id 30
    label "po&#347;ledni"
  ]
  node [
    id 31
    label "s&#322;abowity"
  ]
  node [
    id 32
    label "niefajny"
  ]
  node [
    id 33
    label "z&#322;y"
  ]
  node [
    id 34
    label "niemocny"
  ]
  node [
    id 35
    label "kiepsko"
  ]
  node [
    id 36
    label "niezdrowy"
  ]
  node [
    id 37
    label "lura"
  ]
  node [
    id 38
    label "s&#322;abo"
  ]
  node [
    id 39
    label "nieudany"
  ]
  node [
    id 40
    label "mizerny"
  ]
  node [
    id 41
    label "skr&#281;canie"
  ]
  node [
    id 42
    label "voice"
  ]
  node [
    id 43
    label "forma"
  ]
  node [
    id 44
    label "internet"
  ]
  node [
    id 45
    label "skr&#281;ci&#263;"
  ]
  node [
    id 46
    label "kartka"
  ]
  node [
    id 47
    label "orientowa&#263;"
  ]
  node [
    id 48
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 49
    label "powierzchnia"
  ]
  node [
    id 50
    label "plik"
  ]
  node [
    id 51
    label "bok"
  ]
  node [
    id 52
    label "pagina"
  ]
  node [
    id 53
    label "orientowanie"
  ]
  node [
    id 54
    label "fragment"
  ]
  node [
    id 55
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 56
    label "s&#261;d"
  ]
  node [
    id 57
    label "skr&#281;ca&#263;"
  ]
  node [
    id 58
    label "g&#243;ra"
  ]
  node [
    id 59
    label "serwis_internetowy"
  ]
  node [
    id 60
    label "orientacja"
  ]
  node [
    id 61
    label "linia"
  ]
  node [
    id 62
    label "skr&#281;cenie"
  ]
  node [
    id 63
    label "layout"
  ]
  node [
    id 64
    label "zorientowa&#263;"
  ]
  node [
    id 65
    label "zorientowanie"
  ]
  node [
    id 66
    label "obiekt"
  ]
  node [
    id 67
    label "podmiot"
  ]
  node [
    id 68
    label "ty&#322;"
  ]
  node [
    id 69
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 70
    label "logowanie"
  ]
  node [
    id 71
    label "adres_internetowy"
  ]
  node [
    id 72
    label "uj&#281;cie"
  ]
  node [
    id 73
    label "prz&#243;d"
  ]
  node [
    id 74
    label "posta&#263;"
  ]
  node [
    id 75
    label "feel"
  ]
  node [
    id 76
    label "widzie&#263;"
  ]
  node [
    id 77
    label "uczuwa&#263;"
  ]
  node [
    id 78
    label "notice"
  ]
  node [
    id 79
    label "konsekwencja"
  ]
  node [
    id 80
    label "smell"
  ]
  node [
    id 81
    label "postrzega&#263;"
  ]
  node [
    id 82
    label "doznawa&#263;"
  ]
  node [
    id 83
    label "ochrona"
  ]
  node [
    id 84
    label "trudno&#347;&#263;"
  ]
  node [
    id 85
    label "parapet"
  ]
  node [
    id 86
    label "obstruction"
  ]
  node [
    id 87
    label "pasmo"
  ]
  node [
    id 88
    label "przeszkoda"
  ]
  node [
    id 89
    label "zmienny"
  ]
  node [
    id 90
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 91
    label "endecki"
  ]
  node [
    id 92
    label "komitet_koordynacyjny"
  ]
  node [
    id 93
    label "przybud&#243;wka"
  ]
  node [
    id 94
    label "ZOMO"
  ]
  node [
    id 95
    label "boj&#243;wka"
  ]
  node [
    id 96
    label "zesp&#243;&#322;"
  ]
  node [
    id 97
    label "organization"
  ]
  node [
    id 98
    label "TOPR"
  ]
  node [
    id 99
    label "jednostka_organizacyjna"
  ]
  node [
    id 100
    label "przedstawicielstwo"
  ]
  node [
    id 101
    label "Cepelia"
  ]
  node [
    id 102
    label "GOPR"
  ]
  node [
    id 103
    label "ZMP"
  ]
  node [
    id 104
    label "ZBoWiD"
  ]
  node [
    id 105
    label "struktura"
  ]
  node [
    id 106
    label "od&#322;am"
  ]
  node [
    id 107
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 108
    label "centrala"
  ]
  node [
    id 109
    label "pozainstytucjonalny"
  ]
  node [
    id 110
    label "pozarz&#261;dowo"
  ]
  node [
    id 111
    label "swoisty"
  ]
  node [
    id 112
    label "czyj&#347;"
  ]
  node [
    id 113
    label "osobny"
  ]
  node [
    id 114
    label "zwi&#261;zany"
  ]
  node [
    id 115
    label "samodzielny"
  ]
  node [
    id 116
    label "informacja"
  ]
  node [
    id 117
    label "sofcik"
  ]
  node [
    id 118
    label "appraisal"
  ]
  node [
    id 119
    label "decyzja"
  ]
  node [
    id 120
    label "pogl&#261;d"
  ]
  node [
    id 121
    label "kryterium"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
]
