graph [
  maxDegree 12
  minDegree 1
  meanDegree 2
  density 0.03773584905660377
  graphCliqueNumber 2
  node [
    id 0
    label "chorowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "nowotw&#243;r"
    origin "text"
  ]
  node [
    id 2
    label "niedowidzie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 4
    label "poruszaj&#261;cy"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "w&#243;zek"
    origin "text"
  ]
  node [
    id 7
    label "inwalidzki"
    origin "text"
  ]
  node [
    id 8
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 9
    label "okra&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "swoje"
    origin "text"
  ]
  node [
    id 11
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 12
    label "cierpie&#263;"
  ]
  node [
    id 13
    label "pain"
  ]
  node [
    id 14
    label "garlic"
  ]
  node [
    id 15
    label "pragn&#261;&#263;"
  ]
  node [
    id 16
    label "badanie_histopatologiczne"
  ]
  node [
    id 17
    label "dysplazja"
  ]
  node [
    id 18
    label "tkanka"
  ]
  node [
    id 19
    label "rozrost"
  ]
  node [
    id 20
    label "zmiana"
  ]
  node [
    id 21
    label "profesor"
  ]
  node [
    id 22
    label "kszta&#322;ciciel"
  ]
  node [
    id 23
    label "szkolnik"
  ]
  node [
    id 24
    label "preceptor"
  ]
  node [
    id 25
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 26
    label "pedagog"
  ]
  node [
    id 27
    label "popularyzator"
  ]
  node [
    id 28
    label "belfer"
  ]
  node [
    id 29
    label "pojazd_niemechaniczny"
  ]
  node [
    id 30
    label "proceed"
  ]
  node [
    id 31
    label "catch"
  ]
  node [
    id 32
    label "pozosta&#263;"
  ]
  node [
    id 33
    label "osta&#263;_si&#281;"
  ]
  node [
    id 34
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 35
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 36
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 37
    label "change"
  ]
  node [
    id 38
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 39
    label "obskoczy&#263;"
  ]
  node [
    id 40
    label "zabra&#263;"
  ]
  node [
    id 41
    label "treat"
  ]
  node [
    id 42
    label "obedrze&#263;"
  ]
  node [
    id 43
    label "stanie"
  ]
  node [
    id 44
    label "przebywanie"
  ]
  node [
    id 45
    label "panowanie"
  ]
  node [
    id 46
    label "zajmowanie"
  ]
  node [
    id 47
    label "pomieszkanie"
  ]
  node [
    id 48
    label "adjustment"
  ]
  node [
    id 49
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 50
    label "lokal"
  ]
  node [
    id 51
    label "kwadrat"
  ]
  node [
    id 52
    label "animation"
  ]
  node [
    id 53
    label "dom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 11
    target 44
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 53
  ]
]
