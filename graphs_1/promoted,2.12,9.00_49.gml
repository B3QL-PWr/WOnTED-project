graph [
  maxDegree 48
  minDegree 1
  meanDegree 2.0555555555555554
  density 0.014374514374514374
  graphCliqueNumber 4
  node [
    id 0
    label "dok&#322;adnie"
    origin "text"
  ]
  node [
    id 1
    label "rok"
    origin "text"
  ]
  node [
    id 2
    label "karabinek"
    origin "text"
  ]
  node [
    id 3
    label "grot"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "u&#380;ywa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przez"
    origin "text"
  ]
  node [
    id 7
    label "&#380;o&#322;nierz"
    origin "text"
  ]
  node [
    id 8
    label "wojsko"
    origin "text"
  ]
  node [
    id 9
    label "obrona"
    origin "text"
  ]
  node [
    id 10
    label "terytorialny"
    origin "text"
  ]
  node [
    id 11
    label "punctiliously"
  ]
  node [
    id 12
    label "dok&#322;adny"
  ]
  node [
    id 13
    label "meticulously"
  ]
  node [
    id 14
    label "precyzyjnie"
  ]
  node [
    id 15
    label "rzetelnie"
  ]
  node [
    id 16
    label "stulecie"
  ]
  node [
    id 17
    label "kalendarz"
  ]
  node [
    id 18
    label "czas"
  ]
  node [
    id 19
    label "pora_roku"
  ]
  node [
    id 20
    label "cykl_astronomiczny"
  ]
  node [
    id 21
    label "p&#243;&#322;rocze"
  ]
  node [
    id 22
    label "grupa"
  ]
  node [
    id 23
    label "kwarta&#322;"
  ]
  node [
    id 24
    label "kurs"
  ]
  node [
    id 25
    label "jubileusz"
  ]
  node [
    id 26
    label "miesi&#261;c"
  ]
  node [
    id 27
    label "lata"
  ]
  node [
    id 28
    label "martwy_sezon"
  ]
  node [
    id 29
    label "pier&#347;cie&#324;"
  ]
  node [
    id 30
    label "carbine"
  ]
  node [
    id 31
    label "przyrz&#261;d"
  ]
  node [
    id 32
    label "karabin"
  ]
  node [
    id 33
    label "smycz"
  ]
  node [
    id 34
    label "sprz&#281;t_wspinaczkowy"
  ]
  node [
    id 35
    label "zaczep"
  ]
  node [
    id 36
    label "ekspres"
  ]
  node [
    id 37
    label "p&#322;oszczyk"
  ]
  node [
    id 38
    label "zadzior"
  ]
  node [
    id 39
    label "haczyk"
  ]
  node [
    id 40
    label "ostrze"
  ]
  node [
    id 41
    label "&#380;agiel"
  ]
  node [
    id 42
    label "&#380;ele&#378;ce"
  ]
  node [
    id 43
    label "si&#281;ga&#263;"
  ]
  node [
    id 44
    label "trwa&#263;"
  ]
  node [
    id 45
    label "obecno&#347;&#263;"
  ]
  node [
    id 46
    label "stan"
  ]
  node [
    id 47
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 48
    label "stand"
  ]
  node [
    id 49
    label "mie&#263;_miejsce"
  ]
  node [
    id 50
    label "uczestniczy&#263;"
  ]
  node [
    id 51
    label "chodzi&#263;"
  ]
  node [
    id 52
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 53
    label "equal"
  ]
  node [
    id 54
    label "bash"
  ]
  node [
    id 55
    label "distribute"
  ]
  node [
    id 56
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 57
    label "give"
  ]
  node [
    id 58
    label "korzysta&#263;"
  ]
  node [
    id 59
    label "doznawa&#263;"
  ]
  node [
    id 60
    label "cz&#322;owiek"
  ]
  node [
    id 61
    label "demobilizowa&#263;"
  ]
  node [
    id 62
    label "rota"
  ]
  node [
    id 63
    label "walcz&#261;cy"
  ]
  node [
    id 64
    label "demobilizowanie"
  ]
  node [
    id 65
    label "harcap"
  ]
  node [
    id 66
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 67
    label "&#380;o&#322;dowy"
  ]
  node [
    id 68
    label "zdemobilizowanie"
  ]
  node [
    id 69
    label "elew"
  ]
  node [
    id 70
    label "mundurowy"
  ]
  node [
    id 71
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 72
    label "so&#322;dat"
  ]
  node [
    id 73
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 74
    label "zdemobilizowa&#263;"
  ]
  node [
    id 75
    label "Gurkha"
  ]
  node [
    id 76
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 77
    label "dryl"
  ]
  node [
    id 78
    label "przedmiot"
  ]
  node [
    id 79
    label "ods&#322;ugiwanie"
  ]
  node [
    id 80
    label "korpus"
  ]
  node [
    id 81
    label "s&#322;u&#380;ba"
  ]
  node [
    id 82
    label "wojo"
  ]
  node [
    id 83
    label "zrejterowanie"
  ]
  node [
    id 84
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 85
    label "werbowanie_si&#281;"
  ]
  node [
    id 86
    label "zmobilizowa&#263;"
  ]
  node [
    id 87
    label "struktura"
  ]
  node [
    id 88
    label "szko&#322;a"
  ]
  node [
    id 89
    label "oddzia&#322;"
  ]
  node [
    id 90
    label "mobilizowa&#263;"
  ]
  node [
    id 91
    label "Armia_Krajowa"
  ]
  node [
    id 92
    label "pozycja"
  ]
  node [
    id 93
    label "mobilizowanie"
  ]
  node [
    id 94
    label "si&#322;a"
  ]
  node [
    id 95
    label "fala"
  ]
  node [
    id 96
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 97
    label "pospolite_ruszenie"
  ]
  node [
    id 98
    label "Armia_Czerwona"
  ]
  node [
    id 99
    label "rejterowanie"
  ]
  node [
    id 100
    label "cofni&#281;cie"
  ]
  node [
    id 101
    label "Eurokorpus"
  ]
  node [
    id 102
    label "tabor"
  ]
  node [
    id 103
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 104
    label "petarda"
  ]
  node [
    id 105
    label "zrejterowa&#263;"
  ]
  node [
    id 106
    label "zmobilizowanie"
  ]
  node [
    id 107
    label "rejterowa&#263;"
  ]
  node [
    id 108
    label "Czerwona_Gwardia"
  ]
  node [
    id 109
    label "Legia_Cudzoziemska"
  ]
  node [
    id 110
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 111
    label "wermacht"
  ]
  node [
    id 112
    label "soldateska"
  ]
  node [
    id 113
    label "oddzia&#322;_karny"
  ]
  node [
    id 114
    label "rezerwa"
  ]
  node [
    id 115
    label "or&#281;&#380;"
  ]
  node [
    id 116
    label "dezerter"
  ]
  node [
    id 117
    label "potencja"
  ]
  node [
    id 118
    label "sztabslekarz"
  ]
  node [
    id 119
    label "manewr"
  ]
  node [
    id 120
    label "reakcja"
  ]
  node [
    id 121
    label "auspices"
  ]
  node [
    id 122
    label "mecz"
  ]
  node [
    id 123
    label "poparcie"
  ]
  node [
    id 124
    label "ochrona"
  ]
  node [
    id 125
    label "s&#261;d"
  ]
  node [
    id 126
    label "defensive_structure"
  ]
  node [
    id 127
    label "liga"
  ]
  node [
    id 128
    label "egzamin"
  ]
  node [
    id 129
    label "gracz"
  ]
  node [
    id 130
    label "defense"
  ]
  node [
    id 131
    label "walka"
  ]
  node [
    id 132
    label "post&#281;powanie"
  ]
  node [
    id 133
    label "protection"
  ]
  node [
    id 134
    label "poj&#281;cie"
  ]
  node [
    id 135
    label "guard_duty"
  ]
  node [
    id 136
    label "strona"
  ]
  node [
    id 137
    label "sp&#243;r"
  ]
  node [
    id 138
    label "gra"
  ]
  node [
    id 139
    label "terytorialnie"
  ]
  node [
    id 140
    label "Grot"
  ]
  node [
    id 141
    label "C16"
  ]
  node [
    id 142
    label "FB"
  ]
  node [
    id 143
    label "M1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 140
    target 142
  ]
  edge [
    source 140
    target 143
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 141
    target 143
  ]
  edge [
    source 142
    target 143
  ]
]
