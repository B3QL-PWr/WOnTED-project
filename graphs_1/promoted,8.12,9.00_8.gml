graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.75
  density 0.11666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "nietypowy"
    origin "text"
  ]
  node [
    id 1
    label "przypadek"
    origin "text"
  ]
  node [
    id 2
    label "jemm'y"
    origin "text"
  ]
  node [
    id 3
    label "beale"
    origin "text"
  ]
  node [
    id 4
    label "inny"
  ]
  node [
    id 5
    label "nietypowo"
  ]
  node [
    id 6
    label "pacjent"
  ]
  node [
    id 7
    label "kategoria_gramatyczna"
  ]
  node [
    id 8
    label "schorzenie"
  ]
  node [
    id 9
    label "przeznaczenie"
  ]
  node [
    id 10
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 11
    label "wydarzenie"
  ]
  node [
    id 12
    label "happening"
  ]
  node [
    id 13
    label "przyk&#322;ad"
  ]
  node [
    id 14
    label "Jemmy"
  ]
  node [
    id 15
    label "Beale"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 14
    target 15
  ]
]
