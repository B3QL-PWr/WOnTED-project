graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.9696969696969697
  density 0.030303030303030304
  graphCliqueNumber 2
  node [
    id 0
    label "akcja"
    origin "text"
  ]
  node [
    id 1
    label "zach&#281;caj&#261;cy"
    origin "text"
  ]
  node [
    id 2
    label "nosi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "odblask"
    origin "text"
  ]
  node [
    id 4
    label "droga"
    origin "text"
  ]
  node [
    id 5
    label "zagrywka"
  ]
  node [
    id 6
    label "czyn"
  ]
  node [
    id 7
    label "czynno&#347;&#263;"
  ]
  node [
    id 8
    label "wysoko&#347;&#263;"
  ]
  node [
    id 9
    label "stock"
  ]
  node [
    id 10
    label "gra"
  ]
  node [
    id 11
    label "w&#281;ze&#322;"
  ]
  node [
    id 12
    label "instrument_strunowy"
  ]
  node [
    id 13
    label "dywidenda"
  ]
  node [
    id 14
    label "przebieg"
  ]
  node [
    id 15
    label "udzia&#322;"
  ]
  node [
    id 16
    label "occupation"
  ]
  node [
    id 17
    label "jazda"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "commotion"
  ]
  node [
    id 20
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 21
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 22
    label "operacja"
  ]
  node [
    id 23
    label "mobilizuj&#261;cy"
  ]
  node [
    id 24
    label "przyjazny"
  ]
  node [
    id 25
    label "przyjemny"
  ]
  node [
    id 26
    label "zach&#281;caj&#261;co"
  ]
  node [
    id 27
    label "dobry"
  ]
  node [
    id 28
    label "mi&#322;y"
  ]
  node [
    id 29
    label "str&#243;j"
  ]
  node [
    id 30
    label "mie&#263;"
  ]
  node [
    id 31
    label "wk&#322;ada&#263;"
  ]
  node [
    id 32
    label "przemieszcza&#263;"
  ]
  node [
    id 33
    label "posiada&#263;"
  ]
  node [
    id 34
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 35
    label "wear"
  ]
  node [
    id 36
    label "carry"
  ]
  node [
    id 37
    label "reflection"
  ]
  node [
    id 38
    label "blask"
  ]
  node [
    id 39
    label "znaczek"
  ]
  node [
    id 40
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 41
    label "journey"
  ]
  node [
    id 42
    label "podbieg"
  ]
  node [
    id 43
    label "bezsilnikowy"
  ]
  node [
    id 44
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 45
    label "wylot"
  ]
  node [
    id 46
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 47
    label "drogowskaz"
  ]
  node [
    id 48
    label "nawierzchnia"
  ]
  node [
    id 49
    label "turystyka"
  ]
  node [
    id 50
    label "budowla"
  ]
  node [
    id 51
    label "spos&#243;b"
  ]
  node [
    id 52
    label "passage"
  ]
  node [
    id 53
    label "marszrutyzacja"
  ]
  node [
    id 54
    label "zbior&#243;wka"
  ]
  node [
    id 55
    label "ekskursja"
  ]
  node [
    id 56
    label "rajza"
  ]
  node [
    id 57
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 58
    label "ruch"
  ]
  node [
    id 59
    label "trasa"
  ]
  node [
    id 60
    label "wyb&#243;j"
  ]
  node [
    id 61
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 62
    label "ekwipunek"
  ]
  node [
    id 63
    label "korona_drogi"
  ]
  node [
    id 64
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 65
    label "pobocze"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
]
