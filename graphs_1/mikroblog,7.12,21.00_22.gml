graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.07407407407407407
  graphCliqueNumber 2
  node [
    id 0
    label "um&#243;wi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "wczoraj"
    origin "text"
  ]
  node [
    id 3
    label "galeria"
    origin "text"
  ]
  node [
    id 4
    label "krakowski"
    origin "text"
  ]
  node [
    id 5
    label "pizzahut"
    origin "text"
  ]
  node [
    id 6
    label "moja"
    origin "text"
  ]
  node [
    id 7
    label "kumpela"
    origin "text"
  ]
  node [
    id 8
    label "doba"
  ]
  node [
    id 9
    label "dawno"
  ]
  node [
    id 10
    label "niedawno"
  ]
  node [
    id 11
    label "sklep"
  ]
  node [
    id 12
    label "eskalator"
  ]
  node [
    id 13
    label "wystawa"
  ]
  node [
    id 14
    label "balkon"
  ]
  node [
    id 15
    label "centrum_handlowe"
  ]
  node [
    id 16
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 17
    label "Galeria_Arsena&#322;"
  ]
  node [
    id 18
    label "publiczno&#347;&#263;"
  ]
  node [
    id 19
    label "zbi&#243;r"
  ]
  node [
    id 20
    label "sala"
  ]
  node [
    id 21
    label "&#322;&#261;cznik"
  ]
  node [
    id 22
    label "muzeum"
  ]
  node [
    id 23
    label "g&#322;&#243;g"
  ]
  node [
    id 24
    label "ma&#322;opolski"
  ]
  node [
    id 25
    label "po_krakowsku"
  ]
  node [
    id 26
    label "kuma"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 26
  ]
]
