graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.07407407407407407
  graphCliqueNumber 2
  node [
    id 0
    label "nawi&#261;zanie"
    origin "text"
  ]
  node [
    id 1
    label "zdarzenie"
    origin "text"
  ]
  node [
    id 2
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "przyczepienie"
  ]
  node [
    id 4
    label "zaczerpni&#281;cie"
  ]
  node [
    id 5
    label "sugerowa&#263;"
  ]
  node [
    id 6
    label "prequel"
  ]
  node [
    id 7
    label "zacz&#281;cie"
  ]
  node [
    id 8
    label "zasugerowanie"
  ]
  node [
    id 9
    label "connection"
  ]
  node [
    id 10
    label "wzmianka"
  ]
  node [
    id 11
    label "wskaz&#243;wka"
  ]
  node [
    id 12
    label "spin-off"
  ]
  node [
    id 13
    label "zasugerowa&#263;"
  ]
  node [
    id 14
    label "sugerowanie"
  ]
  node [
    id 15
    label "zawi&#261;zanie_si&#281;"
  ]
  node [
    id 16
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 17
    label "czynno&#347;&#263;"
  ]
  node [
    id 18
    label "motyw"
  ]
  node [
    id 19
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 20
    label "fabu&#322;a"
  ]
  node [
    id 21
    label "przebiec"
  ]
  node [
    id 22
    label "przebiegni&#281;cie"
  ]
  node [
    id 23
    label "charakter"
  ]
  node [
    id 24
    label "komunikowa&#263;"
  ]
  node [
    id 25
    label "powiada&#263;"
  ]
  node [
    id 26
    label "inform"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
]
