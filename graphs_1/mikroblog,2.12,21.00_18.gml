graph [
  maxDegree 41
  minDegree 1
  meanDegree 1.975
  density 0.025
  graphCliqueNumber 2
  node [
    id 0
    label "zaraz"
    origin "text"
  ]
  node [
    id 1
    label "rozegra&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "walka"
    origin "text"
  ]
  node [
    id 4
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 5
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 6
    label "kupon"
    origin "text"
  ]
  node [
    id 7
    label "blisko"
  ]
  node [
    id 8
    label "zara"
  ]
  node [
    id 9
    label "nied&#322;ugo"
  ]
  node [
    id 10
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 11
    label "przeprowadzi&#263;"
  ]
  node [
    id 12
    label "play"
  ]
  node [
    id 13
    label "spowodowa&#263;"
  ]
  node [
    id 14
    label "czyn"
  ]
  node [
    id 15
    label "trudno&#347;&#263;"
  ]
  node [
    id 16
    label "obrona"
  ]
  node [
    id 17
    label "zaatakowanie"
  ]
  node [
    id 18
    label "konfrontacyjny"
  ]
  node [
    id 19
    label "military_action"
  ]
  node [
    id 20
    label "wrestle"
  ]
  node [
    id 21
    label "action"
  ]
  node [
    id 22
    label "wydarzenie"
  ]
  node [
    id 23
    label "rywalizacja"
  ]
  node [
    id 24
    label "sambo"
  ]
  node [
    id 25
    label "contest"
  ]
  node [
    id 26
    label "sp&#243;r"
  ]
  node [
    id 27
    label "kres_&#380;ycia"
  ]
  node [
    id 28
    label "defenestracja"
  ]
  node [
    id 29
    label "kres"
  ]
  node [
    id 30
    label "agonia"
  ]
  node [
    id 31
    label "szeol"
  ]
  node [
    id 32
    label "mogi&#322;a"
  ]
  node [
    id 33
    label "pogrzeb"
  ]
  node [
    id 34
    label "istota_nadprzyrodzona"
  ]
  node [
    id 35
    label "pogrzebanie"
  ]
  node [
    id 36
    label "&#380;a&#322;oba"
  ]
  node [
    id 37
    label "zabicie"
  ]
  node [
    id 38
    label "upadek"
  ]
  node [
    id 39
    label "energy"
  ]
  node [
    id 40
    label "czas"
  ]
  node [
    id 41
    label "bycie"
  ]
  node [
    id 42
    label "zegar_biologiczny"
  ]
  node [
    id 43
    label "okres_noworodkowy"
  ]
  node [
    id 44
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 45
    label "entity"
  ]
  node [
    id 46
    label "prze&#380;ywanie"
  ]
  node [
    id 47
    label "prze&#380;ycie"
  ]
  node [
    id 48
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 49
    label "wiek_matuzalemowy"
  ]
  node [
    id 50
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 51
    label "dzieci&#324;stwo"
  ]
  node [
    id 52
    label "power"
  ]
  node [
    id 53
    label "szwung"
  ]
  node [
    id 54
    label "menopauza"
  ]
  node [
    id 55
    label "umarcie"
  ]
  node [
    id 56
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 57
    label "life"
  ]
  node [
    id 58
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 59
    label "&#380;ywy"
  ]
  node [
    id 60
    label "rozw&#243;j"
  ]
  node [
    id 61
    label "po&#322;&#243;g"
  ]
  node [
    id 62
    label "byt"
  ]
  node [
    id 63
    label "przebywanie"
  ]
  node [
    id 64
    label "subsistence"
  ]
  node [
    id 65
    label "koleje_losu"
  ]
  node [
    id 66
    label "raj_utracony"
  ]
  node [
    id 67
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 68
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 69
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 70
    label "andropauza"
  ]
  node [
    id 71
    label "warunki"
  ]
  node [
    id 72
    label "do&#380;ywanie"
  ]
  node [
    id 73
    label "niemowl&#281;ctwo"
  ]
  node [
    id 74
    label "umieranie"
  ]
  node [
    id 75
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 76
    label "staro&#347;&#263;"
  ]
  node [
    id 77
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 78
    label "formularz"
  ]
  node [
    id 79
    label "odcinek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
]
