graph [
  maxDegree 13
  minDegree 1
  meanDegree 2
  density 0.06060606060606061
  graphCliqueNumber 3
  node [
    id 0
    label "pami&#281;tnik"
    origin "text"
  ]
  node [
    id 1
    label "wi&#281;zie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "xiii"
    origin "text"
  ]
  node [
    id 3
    label "fort"
    origin "text"
  ]
  node [
    id 4
    label "twierdza"
    origin "text"
  ]
  node [
    id 5
    label "przemy&#347;l"
    origin "text"
  ]
  node [
    id 6
    label "zapiski"
  ]
  node [
    id 7
    label "pami&#261;tka"
  ]
  node [
    id 8
    label "album"
  ]
  node [
    id 9
    label "notes"
  ]
  node [
    id 10
    label "utw&#243;r_epicki"
  ]
  node [
    id 11
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 12
    label "raptularz"
  ]
  node [
    id 13
    label "cz&#322;owiek"
  ]
  node [
    id 14
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 15
    label "kiciarz"
  ]
  node [
    id 16
    label "Butyrki"
  ]
  node [
    id 17
    label "ciupa"
  ]
  node [
    id 18
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 19
    label "pasiak"
  ]
  node [
    id 20
    label "reedukator"
  ]
  node [
    id 21
    label "miejsce_odosobnienia"
  ]
  node [
    id 22
    label "&#321;ubianka"
  ]
  node [
    id 23
    label "pierdel"
  ]
  node [
    id 24
    label "gra_MMORPG"
  ]
  node [
    id 25
    label "budowla"
  ]
  node [
    id 26
    label "fortyfikacja"
  ]
  node [
    id 27
    label "Brenna"
  ]
  node [
    id 28
    label "Szlisselburg"
  ]
  node [
    id 29
    label "Dyjament"
  ]
  node [
    id 30
    label "flanka"
  ]
  node [
    id 31
    label "schronienie"
  ]
  node [
    id 32
    label "bastion"
  ]
  node [
    id 33
    label "przemy&#347;le&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
]
