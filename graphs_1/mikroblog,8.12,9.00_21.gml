graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8571428571428572
  density 0.14285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "bawi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "zawsze"
    origin "text"
  ]
  node [
    id 2
    label "ubawia&#263;"
  ]
  node [
    id 3
    label "amuse"
  ]
  node [
    id 4
    label "zajmowa&#263;"
  ]
  node [
    id 5
    label "wzbudza&#263;"
  ]
  node [
    id 6
    label "przebywa&#263;"
  ]
  node [
    id 7
    label "sprawia&#263;"
  ]
  node [
    id 8
    label "zabawia&#263;"
  ]
  node [
    id 9
    label "play"
  ]
  node [
    id 10
    label "zaw&#380;dy"
  ]
  node [
    id 11
    label "ci&#261;gle"
  ]
  node [
    id 12
    label "na_zawsze"
  ]
  node [
    id 13
    label "cz&#281;sto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
]
