graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.050632911392405
  density 0.02629016553067186
  graphCliqueNumber 3
  node [
    id 0
    label "bochum"
    origin "text"
  ]
  node [
    id 1
    label "jarmark"
    origin "text"
  ]
  node [
    id 2
    label "bo&#380;onarodzeniowy"
    origin "text"
  ]
  node [
    id 3
    label "opisa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wraz"
    origin "text"
  ]
  node [
    id 5
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 6
    label "informacja"
    origin "text"
  ]
  node [
    id 7
    label "praktyczny"
    origin "text"
  ]
  node [
    id 8
    label "taki"
    origin "text"
  ]
  node [
    id 9
    label "termin"
    origin "text"
  ]
  node [
    id 10
    label "cena"
    origin "text"
  ]
  node [
    id 11
    label "stoisko"
    origin "text"
  ]
  node [
    id 12
    label "miejsce"
    origin "text"
  ]
  node [
    id 13
    label "gdzie"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;"
    origin "text"
  ]
  node [
    id 15
    label "odbywa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "plac"
  ]
  node [
    id 17
    label "kram"
  ]
  node [
    id 18
    label "market"
  ]
  node [
    id 19
    label "obiekt_handlowy"
  ]
  node [
    id 20
    label "targowica"
  ]
  node [
    id 21
    label "targ"
  ]
  node [
    id 22
    label "zinterpretowa&#263;"
  ]
  node [
    id 23
    label "relate"
  ]
  node [
    id 24
    label "zapozna&#263;"
  ]
  node [
    id 25
    label "delineate"
  ]
  node [
    id 26
    label "doj&#347;cie"
  ]
  node [
    id 27
    label "doj&#347;&#263;"
  ]
  node [
    id 28
    label "powzi&#261;&#263;"
  ]
  node [
    id 29
    label "wiedza"
  ]
  node [
    id 30
    label "sygna&#322;"
  ]
  node [
    id 31
    label "obiegni&#281;cie"
  ]
  node [
    id 32
    label "obieganie"
  ]
  node [
    id 33
    label "obiec"
  ]
  node [
    id 34
    label "dane"
  ]
  node [
    id 35
    label "obiega&#263;"
  ]
  node [
    id 36
    label "punkt"
  ]
  node [
    id 37
    label "publikacja"
  ]
  node [
    id 38
    label "powzi&#281;cie"
  ]
  node [
    id 39
    label "u&#380;yteczny"
  ]
  node [
    id 40
    label "praktycznie"
  ]
  node [
    id 41
    label "racjonalny"
  ]
  node [
    id 42
    label "okre&#347;lony"
  ]
  node [
    id 43
    label "jaki&#347;"
  ]
  node [
    id 44
    label "przypadni&#281;cie"
  ]
  node [
    id 45
    label "czas"
  ]
  node [
    id 46
    label "chronogram"
  ]
  node [
    id 47
    label "nazewnictwo"
  ]
  node [
    id 48
    label "ekspiracja"
  ]
  node [
    id 49
    label "nazwa"
  ]
  node [
    id 50
    label "przypa&#347;&#263;"
  ]
  node [
    id 51
    label "praktyka"
  ]
  node [
    id 52
    label "term"
  ]
  node [
    id 53
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 54
    label "warto&#347;&#263;"
  ]
  node [
    id 55
    label "wycenienie"
  ]
  node [
    id 56
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 57
    label "dyskryminacja_cenowa"
  ]
  node [
    id 58
    label "inflacja"
  ]
  node [
    id 59
    label "kosztowa&#263;"
  ]
  node [
    id 60
    label "kupowanie"
  ]
  node [
    id 61
    label "wyceni&#263;"
  ]
  node [
    id 62
    label "worth"
  ]
  node [
    id 63
    label "kosztowanie"
  ]
  node [
    id 64
    label "cia&#322;o"
  ]
  node [
    id 65
    label "cecha"
  ]
  node [
    id 66
    label "uwaga"
  ]
  node [
    id 67
    label "przestrze&#324;"
  ]
  node [
    id 68
    label "status"
  ]
  node [
    id 69
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 70
    label "chwila"
  ]
  node [
    id 71
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 72
    label "rz&#261;d"
  ]
  node [
    id 73
    label "praca"
  ]
  node [
    id 74
    label "location"
  ]
  node [
    id 75
    label "warunek_lokalowy"
  ]
  node [
    id 76
    label "uczestniczy&#263;"
  ]
  node [
    id 77
    label "przechodzi&#263;"
  ]
  node [
    id 78
    label "hold"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 78
  ]
]
