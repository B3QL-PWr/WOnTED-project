graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "hrabstwo"
    origin "text"
  ]
  node [
    id 1
    label "van"
    origin "text"
  ]
  node [
    id 2
    label "buren"
    origin "text"
  ]
  node [
    id 3
    label "tennessee"
    origin "text"
  ]
  node [
    id 4
    label "Yorkshire"
  ]
  node [
    id 5
    label "jednostka_administracyjna"
  ]
  node [
    id 6
    label "Norfolk"
  ]
  node [
    id 7
    label "Kornwalia"
  ]
  node [
    id 8
    label "maj&#261;tek"
  ]
  node [
    id 9
    label "samoch&#243;d"
  ]
  node [
    id 10
    label "nadwozie"
  ]
  node [
    id 11
    label "Buren"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
]
