graph [
  maxDegree 23
  minDegree 1
  meanDegree 2.017543859649123
  density 0.01785437043937277
  graphCliqueNumber 3
  node [
    id 0
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 2
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 3
    label "zobaczy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "jak"
    origin "text"
  ]
  node [
    id 5
    label "wewn&#261;trz"
    origin "text"
  ]
  node [
    id 6
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "chyba"
    origin "text"
  ]
  node [
    id 8
    label "najbardziej"
    origin "text"
  ]
  node [
    id 9
    label "traumatyczny"
    origin "text"
  ]
  node [
    id 10
    label "do&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 11
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 12
    label "ostatni"
    origin "text"
  ]
  node [
    id 13
    label "dwa"
    origin "text"
  ]
  node [
    id 14
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 15
    label "doba"
  ]
  node [
    id 16
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 17
    label "dzi&#347;"
  ]
  node [
    id 18
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 19
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 20
    label "spotka&#263;"
  ]
  node [
    id 21
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 22
    label "peek"
  ]
  node [
    id 23
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 24
    label "spoziera&#263;"
  ]
  node [
    id 25
    label "obejrze&#263;"
  ]
  node [
    id 26
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 27
    label "postrzec"
  ]
  node [
    id 28
    label "spot"
  ]
  node [
    id 29
    label "go_steady"
  ]
  node [
    id 30
    label "pojrze&#263;"
  ]
  node [
    id 31
    label "popatrze&#263;"
  ]
  node [
    id 32
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 33
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 34
    label "see"
  ]
  node [
    id 35
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 36
    label "dostrzec"
  ]
  node [
    id 37
    label "zinterpretowa&#263;"
  ]
  node [
    id 38
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 39
    label "znale&#378;&#263;"
  ]
  node [
    id 40
    label "cognizance"
  ]
  node [
    id 41
    label "byd&#322;o"
  ]
  node [
    id 42
    label "zobo"
  ]
  node [
    id 43
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 44
    label "yakalo"
  ]
  node [
    id 45
    label "dzo"
  ]
  node [
    id 46
    label "psychicznie"
  ]
  node [
    id 47
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 48
    label "express"
  ]
  node [
    id 49
    label "rzekn&#261;&#263;"
  ]
  node [
    id 50
    label "okre&#347;li&#263;"
  ]
  node [
    id 51
    label "wyrazi&#263;"
  ]
  node [
    id 52
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 53
    label "unwrap"
  ]
  node [
    id 54
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 55
    label "convey"
  ]
  node [
    id 56
    label "discover"
  ]
  node [
    id 57
    label "wydoby&#263;"
  ]
  node [
    id 58
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 59
    label "poda&#263;"
  ]
  node [
    id 60
    label "bolesny"
  ]
  node [
    id 61
    label "dramatyczny"
  ]
  node [
    id 62
    label "traumatycznie"
  ]
  node [
    id 63
    label "pourazowy"
  ]
  node [
    id 64
    label "zbadanie"
  ]
  node [
    id 65
    label "skill"
  ]
  node [
    id 66
    label "wy&#347;wiadczenie"
  ]
  node [
    id 67
    label "znawstwo"
  ]
  node [
    id 68
    label "wiedza"
  ]
  node [
    id 69
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 70
    label "poczucie"
  ]
  node [
    id 71
    label "spotkanie"
  ]
  node [
    id 72
    label "do&#347;wiadczanie"
  ]
  node [
    id 73
    label "wydarzenie"
  ]
  node [
    id 74
    label "badanie"
  ]
  node [
    id 75
    label "assay"
  ]
  node [
    id 76
    label "obserwowanie"
  ]
  node [
    id 77
    label "checkup"
  ]
  node [
    id 78
    label "potraktowanie"
  ]
  node [
    id 79
    label "szko&#322;a"
  ]
  node [
    id 80
    label "eksperiencja"
  ]
  node [
    id 81
    label "si&#322;a"
  ]
  node [
    id 82
    label "stan"
  ]
  node [
    id 83
    label "lina"
  ]
  node [
    id 84
    label "way"
  ]
  node [
    id 85
    label "cable"
  ]
  node [
    id 86
    label "przebieg"
  ]
  node [
    id 87
    label "zbi&#243;r"
  ]
  node [
    id 88
    label "ch&#243;d"
  ]
  node [
    id 89
    label "trasa"
  ]
  node [
    id 90
    label "rz&#261;d"
  ]
  node [
    id 91
    label "k&#322;us"
  ]
  node [
    id 92
    label "progression"
  ]
  node [
    id 93
    label "current"
  ]
  node [
    id 94
    label "pr&#261;d"
  ]
  node [
    id 95
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 96
    label "lot"
  ]
  node [
    id 97
    label "cz&#322;owiek"
  ]
  node [
    id 98
    label "kolejny"
  ]
  node [
    id 99
    label "istota_&#380;ywa"
  ]
  node [
    id 100
    label "najgorszy"
  ]
  node [
    id 101
    label "aktualny"
  ]
  node [
    id 102
    label "ostatnio"
  ]
  node [
    id 103
    label "niedawno"
  ]
  node [
    id 104
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 105
    label "sko&#324;czony"
  ]
  node [
    id 106
    label "poprzedni"
  ]
  node [
    id 107
    label "pozosta&#322;y"
  ]
  node [
    id 108
    label "w&#261;tpliwy"
  ]
  node [
    id 109
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 110
    label "czas"
  ]
  node [
    id 111
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 112
    label "weekend"
  ]
  node [
    id 113
    label "miesi&#261;c"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
]
