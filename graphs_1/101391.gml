graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.7037037037037037
  density 0.06552706552706553
  graphCliqueNumber 2
  node [
    id 0
    label "amerykanin"
    origin "text"
  ]
  node [
    id 1
    label "rosyjski"
    origin "text"
  ]
  node [
    id 2
    label "pochodzenie"
    origin "text"
  ]
  node [
    id 3
    label "po_rosyjsku"
  ]
  node [
    id 4
    label "j&#281;zyk"
  ]
  node [
    id 5
    label "wielkoruski"
  ]
  node [
    id 6
    label "kacapski"
  ]
  node [
    id 7
    label "Russian"
  ]
  node [
    id 8
    label "rusek"
  ]
  node [
    id 9
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 10
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 11
    label "str&#243;j"
  ]
  node [
    id 12
    label "origin"
  ]
  node [
    id 13
    label "czas"
  ]
  node [
    id 14
    label "geneza"
  ]
  node [
    id 15
    label "beginning"
  ]
  node [
    id 16
    label "wynikanie"
  ]
  node [
    id 17
    label "zaczynanie_si&#281;"
  ]
  node [
    id 18
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 19
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 20
    label "background"
  ]
  node [
    id 21
    label "stanowi&#263;"
  ]
  node [
    id 22
    label "zjednoczy&#263;"
  ]
  node [
    id 23
    label "zwi&#261;zek"
  ]
  node [
    id 24
    label "radziecki"
  ]
  node [
    id 25
    label "nowy"
  ]
  node [
    id 26
    label "Jork"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
]
