graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.0851063829787235
  density 0.011150301513255205
  graphCliqueNumber 2
  node [
    id 0
    label "upalny"
    origin "text"
  ]
  node [
    id 1
    label "pi&#261;tkowy"
    origin "text"
  ]
  node [
    id 2
    label "popo&#322;udnie"
    origin "text"
  ]
  node [
    id 3
    label "spotkanie"
    origin "text"
  ]
  node [
    id 4
    label "opr&#243;cz"
    origin "text"
  ]
  node [
    id 5
    label "sta&#322;a"
    origin "text"
  ]
  node [
    id 6
    label "ekipa"
    origin "text"
  ]
  node [
    id 7
    label "przyjecha&#263;"
    origin "text"
  ]
  node [
    id 8
    label "pawe&#322;"
    origin "text"
  ]
  node [
    id 9
    label "kuba"
    origin "text"
  ]
  node [
    id 10
    label "umila&#263;"
    origin "text"
  ]
  node [
    id 11
    label "jazda"
    origin "text"
  ]
  node [
    id 12
    label "&#347;ciga&#263;"
    origin "text"
  ]
  node [
    id 13
    label "nasze"
    origin "text"
  ]
  node [
    id 14
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 15
    label "helikopter"
    origin "text"
  ]
  node [
    id 16
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 17
    label "przygotowywa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;"
    origin "text"
  ]
  node [
    id 19
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 20
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 21
    label "polska"
    origin "text"
  ]
  node [
    id 22
    label "klasa"
    origin "text"
  ]
  node [
    id 23
    label "buggy"
    origin "text"
  ]
  node [
    id 24
    label "swoje"
    origin "text"
  ]
  node [
    id 25
    label "xrayem"
    origin "text"
  ]
  node [
    id 26
    label "tragusami"
    origin "text"
  ]
  node [
    id 27
    label "bazyl"
    origin "text"
  ]
  node [
    id 28
    label "adam"
    origin "text"
  ]
  node [
    id 29
    label "pojedynek"
    origin "text"
  ]
  node [
    id 30
    label "spalin&#243;wek"
    origin "text"
  ]
  node [
    id 31
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 32
    label "naprawd&#281;"
    origin "text"
  ]
  node [
    id 33
    label "spektakularny"
    origin "text"
  ]
  node [
    id 34
    label "gor&#261;cy"
  ]
  node [
    id 35
    label "upalnie"
  ]
  node [
    id 36
    label "dzie&#324;"
  ]
  node [
    id 37
    label "pora"
  ]
  node [
    id 38
    label "odwieczerz"
  ]
  node [
    id 39
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 40
    label "po&#380;egnanie"
  ]
  node [
    id 41
    label "spowodowanie"
  ]
  node [
    id 42
    label "znalezienie"
  ]
  node [
    id 43
    label "znajomy"
  ]
  node [
    id 44
    label "doznanie"
  ]
  node [
    id 45
    label "employment"
  ]
  node [
    id 46
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 47
    label "gather"
  ]
  node [
    id 48
    label "powitanie"
  ]
  node [
    id 49
    label "spotykanie"
  ]
  node [
    id 50
    label "wydarzenie"
  ]
  node [
    id 51
    label "gathering"
  ]
  node [
    id 52
    label "spotkanie_si&#281;"
  ]
  node [
    id 53
    label "zdarzenie_si&#281;"
  ]
  node [
    id 54
    label "match"
  ]
  node [
    id 55
    label "zawarcie"
  ]
  node [
    id 56
    label "wielko&#347;&#263;"
  ]
  node [
    id 57
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 58
    label "element"
  ]
  node [
    id 59
    label "constant"
  ]
  node [
    id 60
    label "zesp&#243;&#322;"
  ]
  node [
    id 61
    label "grupa"
  ]
  node [
    id 62
    label "dublet"
  ]
  node [
    id 63
    label "force"
  ]
  node [
    id 64
    label "get"
  ]
  node [
    id 65
    label "dotrze&#263;"
  ]
  node [
    id 66
    label "stawi&#263;_si&#281;"
  ]
  node [
    id 67
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 68
    label "sport"
  ]
  node [
    id 69
    label "szwadron"
  ]
  node [
    id 70
    label "formacja"
  ]
  node [
    id 71
    label "chor&#261;giew"
  ]
  node [
    id 72
    label "ruch"
  ]
  node [
    id 73
    label "wykrzyknik"
  ]
  node [
    id 74
    label "heca"
  ]
  node [
    id 75
    label "journey"
  ]
  node [
    id 76
    label "cavalry"
  ]
  node [
    id 77
    label "szale&#324;stwo"
  ]
  node [
    id 78
    label "awantura"
  ]
  node [
    id 79
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 80
    label "straszy&#263;"
  ]
  node [
    id 81
    label "prosecute"
  ]
  node [
    id 82
    label "kara&#263;"
  ]
  node [
    id 83
    label "usi&#322;owa&#263;"
  ]
  node [
    id 84
    label "poszukiwa&#263;"
  ]
  node [
    id 85
    label "baga&#380;nik"
  ]
  node [
    id 86
    label "immobilizer"
  ]
  node [
    id 87
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 88
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 89
    label "poduszka_powietrzna"
  ]
  node [
    id 90
    label "dachowanie"
  ]
  node [
    id 91
    label "dwu&#347;lad"
  ]
  node [
    id 92
    label "deska_rozdzielcza"
  ]
  node [
    id 93
    label "poci&#261;g_drogowy"
  ]
  node [
    id 94
    label "kierownica"
  ]
  node [
    id 95
    label "pojazd_drogowy"
  ]
  node [
    id 96
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 97
    label "pompa_wodna"
  ]
  node [
    id 98
    label "silnik"
  ]
  node [
    id 99
    label "wycieraczka"
  ]
  node [
    id 100
    label "bak"
  ]
  node [
    id 101
    label "ABS"
  ]
  node [
    id 102
    label "most"
  ]
  node [
    id 103
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 104
    label "spryskiwacz"
  ]
  node [
    id 105
    label "t&#322;umik"
  ]
  node [
    id 106
    label "tempomat"
  ]
  node [
    id 107
    label "wirop&#322;at"
  ]
  node [
    id 108
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 109
    label "&#347;mig&#322;o"
  ]
  node [
    id 110
    label "wykonywa&#263;"
  ]
  node [
    id 111
    label "sposobi&#263;"
  ]
  node [
    id 112
    label "arrange"
  ]
  node [
    id 113
    label "pryczy&#263;"
  ]
  node [
    id 114
    label "train"
  ]
  node [
    id 115
    label "robi&#263;"
  ]
  node [
    id 116
    label "wytwarza&#263;"
  ]
  node [
    id 117
    label "szkoli&#263;"
  ]
  node [
    id 118
    label "usposabia&#263;"
  ]
  node [
    id 119
    label "koniec"
  ]
  node [
    id 120
    label "conclusion"
  ]
  node [
    id 121
    label "coating"
  ]
  node [
    id 122
    label "runda"
  ]
  node [
    id 123
    label "championship"
  ]
  node [
    id 124
    label "Formu&#322;a_1"
  ]
  node [
    id 125
    label "zawody"
  ]
  node [
    id 126
    label "typ"
  ]
  node [
    id 127
    label "warstwa"
  ]
  node [
    id 128
    label "znak_jako&#347;ci"
  ]
  node [
    id 129
    label "przedmiot"
  ]
  node [
    id 130
    label "przepisa&#263;"
  ]
  node [
    id 131
    label "pomoc"
  ]
  node [
    id 132
    label "arrangement"
  ]
  node [
    id 133
    label "wagon"
  ]
  node [
    id 134
    label "form"
  ]
  node [
    id 135
    label "zaleta"
  ]
  node [
    id 136
    label "poziom"
  ]
  node [
    id 137
    label "dziennik_lekcyjny"
  ]
  node [
    id 138
    label "&#347;rodowisko"
  ]
  node [
    id 139
    label "atak"
  ]
  node [
    id 140
    label "przepisanie"
  ]
  node [
    id 141
    label "szko&#322;a"
  ]
  node [
    id 142
    label "class"
  ]
  node [
    id 143
    label "organizacja"
  ]
  node [
    id 144
    label "obrona"
  ]
  node [
    id 145
    label "type"
  ]
  node [
    id 146
    label "promocja"
  ]
  node [
    id 147
    label "&#322;awka"
  ]
  node [
    id 148
    label "kurs"
  ]
  node [
    id 149
    label "botanika"
  ]
  node [
    id 150
    label "sala"
  ]
  node [
    id 151
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 152
    label "gromada"
  ]
  node [
    id 153
    label "obiekt"
  ]
  node [
    id 154
    label "Ekwici"
  ]
  node [
    id 155
    label "fakcja"
  ]
  node [
    id 156
    label "tablica"
  ]
  node [
    id 157
    label "programowanie_obiektowe"
  ]
  node [
    id 158
    label "jednostka_systematyczna"
  ]
  node [
    id 159
    label "mecz_mistrzowski"
  ]
  node [
    id 160
    label "zbi&#243;r"
  ]
  node [
    id 161
    label "jako&#347;&#263;"
  ]
  node [
    id 162
    label "rezerwa"
  ]
  node [
    id 163
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 164
    label "wyzwanie"
  ]
  node [
    id 165
    label "wyzywanie"
  ]
  node [
    id 166
    label "engagement"
  ]
  node [
    id 167
    label "walka"
  ]
  node [
    id 168
    label "wyzywa&#263;"
  ]
  node [
    id 169
    label "turniej"
  ]
  node [
    id 170
    label "wyzwa&#263;"
  ]
  node [
    id 171
    label "competitiveness"
  ]
  node [
    id 172
    label "bout"
  ]
  node [
    id 173
    label "odyniec"
  ]
  node [
    id 174
    label "sekundant"
  ]
  node [
    id 175
    label "sp&#243;r"
  ]
  node [
    id 176
    label "dawny"
  ]
  node [
    id 177
    label "rozw&#243;d"
  ]
  node [
    id 178
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 179
    label "eksprezydent"
  ]
  node [
    id 180
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 181
    label "partner"
  ]
  node [
    id 182
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 183
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 184
    label "wcze&#347;niejszy"
  ]
  node [
    id 185
    label "prawdziwy"
  ]
  node [
    id 186
    label "widowiskowo"
  ]
  node [
    id 187
    label "efektowny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 26
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 126
  ]
  edge [
    source 22
    target 127
  ]
  edge [
    source 22
    target 128
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 130
  ]
  edge [
    source 22
    target 61
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 22
    target 133
  ]
  edge [
    source 22
    target 134
  ]
  edge [
    source 22
    target 135
  ]
  edge [
    source 22
    target 136
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 138
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 22
    target 140
  ]
  edge [
    source 22
    target 141
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 22
    target 143
  ]
  edge [
    source 22
    target 144
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 147
  ]
  edge [
    source 22
    target 148
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 22
    target 150
  ]
  edge [
    source 22
    target 151
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 22
    target 155
  ]
  edge [
    source 22
    target 156
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 73
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 164
  ]
  edge [
    source 29
    target 165
  ]
  edge [
    source 29
    target 166
  ]
  edge [
    source 29
    target 167
  ]
  edge [
    source 29
    target 168
  ]
  edge [
    source 29
    target 169
  ]
  edge [
    source 29
    target 170
  ]
  edge [
    source 29
    target 171
  ]
  edge [
    source 29
    target 172
  ]
  edge [
    source 29
    target 173
  ]
  edge [
    source 29
    target 174
  ]
  edge [
    source 29
    target 175
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 176
  ]
  edge [
    source 31
    target 177
  ]
  edge [
    source 31
    target 178
  ]
  edge [
    source 31
    target 179
  ]
  edge [
    source 31
    target 180
  ]
  edge [
    source 31
    target 181
  ]
  edge [
    source 31
    target 182
  ]
  edge [
    source 31
    target 183
  ]
  edge [
    source 31
    target 184
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 185
  ]
  edge [
    source 33
    target 186
  ]
  edge [
    source 33
    target 187
  ]
]
