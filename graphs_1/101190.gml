graph [
  maxDegree 3
  minDegree 1
  meanDegree 2
  density 0.2857142857142857
  graphCliqueNumber 4
  node [
    id 0
    label "harri"
    origin "text"
  ]
  node [
    id 1
    label "haatainen"
    origin "text"
  ]
  node [
    id 2
    label "Harri"
  ]
  node [
    id 3
    label "Haatainen"
  ]
  node [
    id 4
    label "igrzysko"
  ]
  node [
    id 5
    label "olimpijski"
  ]
  node [
    id 6
    label "wyspa"
  ]
  node [
    id 7
    label "Sydney"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
]
