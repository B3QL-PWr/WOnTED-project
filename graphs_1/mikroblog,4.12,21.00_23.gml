graph [
  maxDegree 25
  minDegree 1
  meanDegree 1.9285714285714286
  density 0.07142857142857142
  graphCliqueNumber 2
  node [
    id 0
    label "praca"
    origin "text"
  ]
  node [
    id 1
    label "pracbaza"
    origin "text"
  ]
  node [
    id 2
    label "wpolscejakwlesie"
    origin "text"
  ]
  node [
    id 3
    label "januszebiznesu"
    origin "text"
  ]
  node [
    id 4
    label "stosunek_pracy"
  ]
  node [
    id 5
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 6
    label "benedykty&#324;ski"
  ]
  node [
    id 7
    label "pracowanie"
  ]
  node [
    id 8
    label "zaw&#243;d"
  ]
  node [
    id 9
    label "kierownictwo"
  ]
  node [
    id 10
    label "zmiana"
  ]
  node [
    id 11
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 12
    label "wytw&#243;r"
  ]
  node [
    id 13
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 14
    label "tynkarski"
  ]
  node [
    id 15
    label "czynnik_produkcji"
  ]
  node [
    id 16
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 17
    label "zobowi&#261;zanie"
  ]
  node [
    id 18
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 19
    label "czynno&#347;&#263;"
  ]
  node [
    id 20
    label "tyrka"
  ]
  node [
    id 21
    label "pracowa&#263;"
  ]
  node [
    id 22
    label "siedziba"
  ]
  node [
    id 23
    label "poda&#380;_pracy"
  ]
  node [
    id 24
    label "miejsce"
  ]
  node [
    id 25
    label "zak&#322;ad"
  ]
  node [
    id 26
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 27
    label "najem"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
