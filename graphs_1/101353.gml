graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9
  density 0.03220338983050847
  graphCliqueNumber 3
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "cmentarz"
    origin "text"
  ]
  node [
    id 2
    label "&#380;ydowski"
    origin "text"
  ]
  node [
    id 3
    label "cieszyn"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "nowotny"
  ]
  node [
    id 6
    label "drugi"
  ]
  node [
    id 7
    label "kolejny"
  ]
  node [
    id 8
    label "bie&#380;&#261;cy"
  ]
  node [
    id 9
    label "nowo"
  ]
  node [
    id 10
    label "narybek"
  ]
  node [
    id 11
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 12
    label "obcy"
  ]
  node [
    id 13
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 14
    label "pogrobowisko"
  ]
  node [
    id 15
    label "&#380;alnik"
  ]
  node [
    id 16
    label "sm&#281;tarz"
  ]
  node [
    id 17
    label "park_sztywnych"
  ]
  node [
    id 18
    label "smentarz"
  ]
  node [
    id 19
    label "cinerarium"
  ]
  node [
    id 20
    label "parchaty"
  ]
  node [
    id 21
    label "pejsaty"
  ]
  node [
    id 22
    label "semicki"
  ]
  node [
    id 23
    label "charakterystyczny"
  ]
  node [
    id 24
    label "&#380;ydowsko"
  ]
  node [
    id 25
    label "moszaw"
  ]
  node [
    id 26
    label "stary"
  ]
  node [
    id 27
    label "Andrzej"
  ]
  node [
    id 28
    label "folwarczny"
  ]
  node [
    id 29
    label "ministerstwo"
  ]
  node [
    id 30
    label "sprawi&#263;"
  ]
  node [
    id 31
    label "wewn&#281;trzny"
  ]
  node [
    id 32
    label "naczelny"
  ]
  node [
    id 33
    label "s&#261;d"
  ]
  node [
    id 34
    label "administracyjny"
  ]
  node [
    id 35
    label "Jan"
  ]
  node [
    id 36
    label "Glesingera"
  ]
  node [
    id 37
    label "Alojzy"
  ]
  node [
    id 38
    label "Jedek"
  ]
  node [
    id 39
    label "Jakoba"
  ]
  node [
    id 40
    label "Gartnera"
  ]
  node [
    id 41
    label "starzy"
  ]
  node [
    id 42
    label "ii"
  ]
  node [
    id 43
    label "wojna"
  ]
  node [
    id 44
    label "&#347;wiatowy"
  ]
  node [
    id 45
    label "iii"
  ]
  node [
    id 46
    label "rzesza"
  ]
  node [
    id 47
    label "kongregacja"
  ]
  node [
    id 48
    label "wyzna&#263;"
  ]
  node [
    id 49
    label "moj&#380;eszowy"
  ]
  node [
    id 50
    label "bielski"
  ]
  node [
    id 51
    label "bia&#322;y"
  ]
  node [
    id 52
    label "Edwarda"
  ]
  node [
    id 53
    label "weber"
  ]
  node [
    id 54
    label "gmina"
  ]
  node [
    id 55
    label "wyznaniowy"
  ]
  node [
    id 56
    label "J"
  ]
  node [
    id 57
    label "Phillips"
  ]
  node [
    id 58
    label "Tadeusz"
  ]
  node [
    id 59
    label "Dordy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 52
    target 57
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 59
  ]
  edge [
    source 58
    target 59
  ]
]
