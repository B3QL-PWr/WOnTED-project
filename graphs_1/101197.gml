graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "samo&#347;wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "self-consciousness"
  ]
  node [
    id 2
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 3
    label "depersonalizacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
]
