graph [
  maxDegree 16
  minDegree 1
  meanDegree 2
  density 0.03278688524590164
  graphCliqueNumber 2
  node [
    id 0
    label "lew"
    origin "text"
  ]
  node [
    id 1
    label "zako&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "letni"
    origin "text"
  ]
  node [
    id 4
    label "ob&#243;z"
    origin "text"
  ]
  node [
    id 5
    label "dziennikarski"
    origin "text"
  ]
  node [
    id 6
    label "mama"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "lygrys"
  ]
  node [
    id 9
    label "liard"
  ]
  node [
    id 10
    label "stotinka"
  ]
  node [
    id 11
    label "kot"
  ]
  node [
    id 12
    label "jednostka_monetarna"
  ]
  node [
    id 13
    label "Bu&#322;garia"
  ]
  node [
    id 14
    label "communicate"
  ]
  node [
    id 15
    label "cause"
  ]
  node [
    id 16
    label "zrezygnowa&#263;"
  ]
  node [
    id 17
    label "wytworzy&#263;"
  ]
  node [
    id 18
    label "przesta&#263;"
  ]
  node [
    id 19
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 20
    label "dispose"
  ]
  node [
    id 21
    label "zrobi&#263;"
  ]
  node [
    id 22
    label "nijaki"
  ]
  node [
    id 23
    label "sezonowy"
  ]
  node [
    id 24
    label "letnio"
  ]
  node [
    id 25
    label "s&#322;oneczny"
  ]
  node [
    id 26
    label "weso&#322;y"
  ]
  node [
    id 27
    label "oboj&#281;tny"
  ]
  node [
    id 28
    label "latowy"
  ]
  node [
    id 29
    label "ciep&#322;y"
  ]
  node [
    id 30
    label "typowy"
  ]
  node [
    id 31
    label "zwi&#261;zek"
  ]
  node [
    id 32
    label "namiot"
  ]
  node [
    id 33
    label "ONZ"
  ]
  node [
    id 34
    label "grupa"
  ]
  node [
    id 35
    label "podob&#243;z"
  ]
  node [
    id 36
    label "blok"
  ]
  node [
    id 37
    label "odpoczynek"
  ]
  node [
    id 38
    label "alianci"
  ]
  node [
    id 39
    label "Paneuropa"
  ]
  node [
    id 40
    label "NATO"
  ]
  node [
    id 41
    label "miejsce_odosobnienia"
  ]
  node [
    id 42
    label "schronienie"
  ]
  node [
    id 43
    label "confederation"
  ]
  node [
    id 44
    label "obozowisko"
  ]
  node [
    id 45
    label "zawodowy"
  ]
  node [
    id 46
    label "rzetelny"
  ]
  node [
    id 47
    label "tre&#347;ciwy"
  ]
  node [
    id 48
    label "po_dziennikarsku"
  ]
  node [
    id 49
    label "dziennikarsko"
  ]
  node [
    id 50
    label "obiektywny"
  ]
  node [
    id 51
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 52
    label "wzorowy"
  ]
  node [
    id 53
    label "matczysko"
  ]
  node [
    id 54
    label "macierz"
  ]
  node [
    id 55
    label "przodkini"
  ]
  node [
    id 56
    label "Matka_Boska"
  ]
  node [
    id 57
    label "macocha"
  ]
  node [
    id 58
    label "matka_zast&#281;pcza"
  ]
  node [
    id 59
    label "stara"
  ]
  node [
    id 60
    label "rodzice"
  ]
  node [
    id 61
    label "rodzic"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
]
