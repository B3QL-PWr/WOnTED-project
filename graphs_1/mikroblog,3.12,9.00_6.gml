graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "wirus"
    origin "text"
  ]
  node [
    id 1
    label "wirusy"
  ]
  node [
    id 2
    label "botnet"
  ]
  node [
    id 3
    label "trojan"
  ]
  node [
    id 4
    label "cz&#261;steczka"
  ]
  node [
    id 5
    label "prokariont"
  ]
  node [
    id 6
    label "program"
  ]
  node [
    id 7
    label "kr&#243;lik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
]
