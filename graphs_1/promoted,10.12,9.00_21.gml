graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.0489510489510487
  density 0.007189301926144032
  graphCliqueNumber 3
  node [
    id 0
    label "strategia"
    origin "text"
  ]
  node [
    id 1
    label "reprodukcyjny"
    origin "text"
  ]
  node [
    id 2
    label "rodzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "zjawisko"
    origin "text"
  ]
  node [
    id 5
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 6
    label "jaki"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "samotny"
    origin "text"
  ]
  node [
    id 9
    label "matka"
    origin "text"
  ]
  node [
    id 10
    label "brak"
    origin "text"
  ]
  node [
    id 11
    label "ojciec"
    origin "text"
  ]
  node [
    id 12
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 13
    label "prywatny"
    origin "text"
  ]
  node [
    id 14
    label "problem"
    origin "text"
  ]
  node [
    id 15
    label "dziecko"
    origin "text"
  ]
  node [
    id 16
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 17
    label "prawo"
    origin "text"
  ]
  node [
    id 18
    label "m&#281;ski"
    origin "text"
  ]
  node [
    id 19
    label "wzorzec"
    origin "text"
  ]
  node [
    id 20
    label "dokument"
  ]
  node [
    id 21
    label "gra"
  ]
  node [
    id 22
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 23
    label "wzorzec_projektowy"
  ]
  node [
    id 24
    label "pocz&#261;tki"
  ]
  node [
    id 25
    label "doktryna"
  ]
  node [
    id 26
    label "program"
  ]
  node [
    id 27
    label "plan"
  ]
  node [
    id 28
    label "metoda"
  ]
  node [
    id 29
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 30
    label "operacja"
  ]
  node [
    id 31
    label "dziedzina"
  ]
  node [
    id 32
    label "wrinkle"
  ]
  node [
    id 33
    label "give"
  ]
  node [
    id 34
    label "create"
  ]
  node [
    id 35
    label "plon"
  ]
  node [
    id 36
    label "wytwarza&#263;"
  ]
  node [
    id 37
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 38
    label "krajobraz"
  ]
  node [
    id 39
    label "przywidzenie"
  ]
  node [
    id 40
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 41
    label "boski"
  ]
  node [
    id 42
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 43
    label "proces"
  ]
  node [
    id 44
    label "presence"
  ]
  node [
    id 45
    label "charakter"
  ]
  node [
    id 46
    label "niepubliczny"
  ]
  node [
    id 47
    label "spo&#322;ecznie"
  ]
  node [
    id 48
    label "publiczny"
  ]
  node [
    id 49
    label "si&#281;ga&#263;"
  ]
  node [
    id 50
    label "trwa&#263;"
  ]
  node [
    id 51
    label "obecno&#347;&#263;"
  ]
  node [
    id 52
    label "stan"
  ]
  node [
    id 53
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 54
    label "stand"
  ]
  node [
    id 55
    label "mie&#263;_miejsce"
  ]
  node [
    id 56
    label "uczestniczy&#263;"
  ]
  node [
    id 57
    label "chodzi&#263;"
  ]
  node [
    id 58
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 59
    label "equal"
  ]
  node [
    id 60
    label "pojedynczy"
  ]
  node [
    id 61
    label "wolny"
  ]
  node [
    id 62
    label "samotnie"
  ]
  node [
    id 63
    label "osamotnianie"
  ]
  node [
    id 64
    label "odosobniony"
  ]
  node [
    id 65
    label "osamotnienie"
  ]
  node [
    id 66
    label "samodzielny"
  ]
  node [
    id 67
    label "sam"
  ]
  node [
    id 68
    label "Matka_Boska"
  ]
  node [
    id 69
    label "matka_zast&#281;pcza"
  ]
  node [
    id 70
    label "stara"
  ]
  node [
    id 71
    label "rodzic"
  ]
  node [
    id 72
    label "matczysko"
  ]
  node [
    id 73
    label "ro&#347;lina"
  ]
  node [
    id 74
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 75
    label "gracz"
  ]
  node [
    id 76
    label "zawodnik"
  ]
  node [
    id 77
    label "macierz"
  ]
  node [
    id 78
    label "owad"
  ]
  node [
    id 79
    label "przyczyna"
  ]
  node [
    id 80
    label "macocha"
  ]
  node [
    id 81
    label "dwa_ognie"
  ]
  node [
    id 82
    label "staruszka"
  ]
  node [
    id 83
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 84
    label "rozsadnik"
  ]
  node [
    id 85
    label "zakonnica"
  ]
  node [
    id 86
    label "obiekt"
  ]
  node [
    id 87
    label "samica"
  ]
  node [
    id 88
    label "przodkini"
  ]
  node [
    id 89
    label "rodzice"
  ]
  node [
    id 90
    label "prywatywny"
  ]
  node [
    id 91
    label "defect"
  ]
  node [
    id 92
    label "odej&#347;cie"
  ]
  node [
    id 93
    label "gap"
  ]
  node [
    id 94
    label "kr&#243;tki"
  ]
  node [
    id 95
    label "wyr&#243;b"
  ]
  node [
    id 96
    label "nieistnienie"
  ]
  node [
    id 97
    label "wada"
  ]
  node [
    id 98
    label "odej&#347;&#263;"
  ]
  node [
    id 99
    label "odchodzenie"
  ]
  node [
    id 100
    label "odchodzi&#263;"
  ]
  node [
    id 101
    label "pomys&#322;odawca"
  ]
  node [
    id 102
    label "kszta&#322;ciciel"
  ]
  node [
    id 103
    label "tworzyciel"
  ]
  node [
    id 104
    label "ojczym"
  ]
  node [
    id 105
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 106
    label "stary"
  ]
  node [
    id 107
    label "samiec"
  ]
  node [
    id 108
    label "papa"
  ]
  node [
    id 109
    label "&#347;w"
  ]
  node [
    id 110
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 111
    label "zakonnik"
  ]
  node [
    id 112
    label "kuwada"
  ]
  node [
    id 113
    label "przodek"
  ]
  node [
    id 114
    label "wykonawca"
  ]
  node [
    id 115
    label "energy"
  ]
  node [
    id 116
    label "czas"
  ]
  node [
    id 117
    label "bycie"
  ]
  node [
    id 118
    label "zegar_biologiczny"
  ]
  node [
    id 119
    label "okres_noworodkowy"
  ]
  node [
    id 120
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 121
    label "entity"
  ]
  node [
    id 122
    label "prze&#380;ywanie"
  ]
  node [
    id 123
    label "prze&#380;ycie"
  ]
  node [
    id 124
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 125
    label "wiek_matuzalemowy"
  ]
  node [
    id 126
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 127
    label "dzieci&#324;stwo"
  ]
  node [
    id 128
    label "power"
  ]
  node [
    id 129
    label "szwung"
  ]
  node [
    id 130
    label "menopauza"
  ]
  node [
    id 131
    label "umarcie"
  ]
  node [
    id 132
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 133
    label "life"
  ]
  node [
    id 134
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 135
    label "&#380;ywy"
  ]
  node [
    id 136
    label "rozw&#243;j"
  ]
  node [
    id 137
    label "po&#322;&#243;g"
  ]
  node [
    id 138
    label "byt"
  ]
  node [
    id 139
    label "przebywanie"
  ]
  node [
    id 140
    label "subsistence"
  ]
  node [
    id 141
    label "koleje_losu"
  ]
  node [
    id 142
    label "raj_utracony"
  ]
  node [
    id 143
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 144
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 145
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 146
    label "andropauza"
  ]
  node [
    id 147
    label "warunki"
  ]
  node [
    id 148
    label "do&#380;ywanie"
  ]
  node [
    id 149
    label "niemowl&#281;ctwo"
  ]
  node [
    id 150
    label "umieranie"
  ]
  node [
    id 151
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 152
    label "staro&#347;&#263;"
  ]
  node [
    id 153
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 154
    label "&#347;mier&#263;"
  ]
  node [
    id 155
    label "czyj&#347;"
  ]
  node [
    id 156
    label "nieformalny"
  ]
  node [
    id 157
    label "personalny"
  ]
  node [
    id 158
    label "w&#322;asny"
  ]
  node [
    id 159
    label "prywatnie"
  ]
  node [
    id 160
    label "trudno&#347;&#263;"
  ]
  node [
    id 161
    label "sprawa"
  ]
  node [
    id 162
    label "ambaras"
  ]
  node [
    id 163
    label "problemat"
  ]
  node [
    id 164
    label "pierepa&#322;ka"
  ]
  node [
    id 165
    label "obstruction"
  ]
  node [
    id 166
    label "problematyka"
  ]
  node [
    id 167
    label "jajko_Kolumba"
  ]
  node [
    id 168
    label "subiekcja"
  ]
  node [
    id 169
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 170
    label "cz&#322;owiek"
  ]
  node [
    id 171
    label "potomstwo"
  ]
  node [
    id 172
    label "organizm"
  ]
  node [
    id 173
    label "sraluch"
  ]
  node [
    id 174
    label "utulanie"
  ]
  node [
    id 175
    label "pediatra"
  ]
  node [
    id 176
    label "dzieciarnia"
  ]
  node [
    id 177
    label "m&#322;odziak"
  ]
  node [
    id 178
    label "dzieciak"
  ]
  node [
    id 179
    label "utula&#263;"
  ]
  node [
    id 180
    label "potomek"
  ]
  node [
    id 181
    label "pedofil"
  ]
  node [
    id 182
    label "entliczek-pentliczek"
  ]
  node [
    id 183
    label "m&#322;odzik"
  ]
  node [
    id 184
    label "cz&#322;owieczek"
  ]
  node [
    id 185
    label "zwierz&#281;"
  ]
  node [
    id 186
    label "niepe&#322;noletni"
  ]
  node [
    id 187
    label "fledgling"
  ]
  node [
    id 188
    label "utuli&#263;"
  ]
  node [
    id 189
    label "utulenie"
  ]
  node [
    id 190
    label "Mickiewicz"
  ]
  node [
    id 191
    label "szkolenie"
  ]
  node [
    id 192
    label "przepisa&#263;"
  ]
  node [
    id 193
    label "lesson"
  ]
  node [
    id 194
    label "grupa"
  ]
  node [
    id 195
    label "praktyka"
  ]
  node [
    id 196
    label "niepokalanki"
  ]
  node [
    id 197
    label "kara"
  ]
  node [
    id 198
    label "zda&#263;"
  ]
  node [
    id 199
    label "form"
  ]
  node [
    id 200
    label "kwalifikacje"
  ]
  node [
    id 201
    label "system"
  ]
  node [
    id 202
    label "przepisanie"
  ]
  node [
    id 203
    label "sztuba"
  ]
  node [
    id 204
    label "wiedza"
  ]
  node [
    id 205
    label "stopek"
  ]
  node [
    id 206
    label "school"
  ]
  node [
    id 207
    label "absolwent"
  ]
  node [
    id 208
    label "urszulanki"
  ]
  node [
    id 209
    label "gabinet"
  ]
  node [
    id 210
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 211
    label "ideologia"
  ]
  node [
    id 212
    label "lekcja"
  ]
  node [
    id 213
    label "muzyka"
  ]
  node [
    id 214
    label "podr&#281;cznik"
  ]
  node [
    id 215
    label "zdanie"
  ]
  node [
    id 216
    label "siedziba"
  ]
  node [
    id 217
    label "sekretariat"
  ]
  node [
    id 218
    label "nauka"
  ]
  node [
    id 219
    label "do&#347;wiadczenie"
  ]
  node [
    id 220
    label "tablica"
  ]
  node [
    id 221
    label "teren_szko&#322;y"
  ]
  node [
    id 222
    label "instytucja"
  ]
  node [
    id 223
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 224
    label "skolaryzacja"
  ]
  node [
    id 225
    label "&#322;awa_szkolna"
  ]
  node [
    id 226
    label "klasa"
  ]
  node [
    id 227
    label "obserwacja"
  ]
  node [
    id 228
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 229
    label "nauka_prawa"
  ]
  node [
    id 230
    label "dominion"
  ]
  node [
    id 231
    label "normatywizm"
  ]
  node [
    id 232
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 233
    label "qualification"
  ]
  node [
    id 234
    label "opis"
  ]
  node [
    id 235
    label "regu&#322;a_Allena"
  ]
  node [
    id 236
    label "normalizacja"
  ]
  node [
    id 237
    label "kazuistyka"
  ]
  node [
    id 238
    label "regu&#322;a_Glogera"
  ]
  node [
    id 239
    label "kultura_duchowa"
  ]
  node [
    id 240
    label "prawo_karne"
  ]
  node [
    id 241
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 242
    label "standard"
  ]
  node [
    id 243
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 244
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 245
    label "struktura"
  ]
  node [
    id 246
    label "prawo_karne_procesowe"
  ]
  node [
    id 247
    label "prawo_Mendla"
  ]
  node [
    id 248
    label "przepis"
  ]
  node [
    id 249
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 250
    label "criterion"
  ]
  node [
    id 251
    label "kanonistyka"
  ]
  node [
    id 252
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 253
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 254
    label "wykonawczy"
  ]
  node [
    id 255
    label "twierdzenie"
  ]
  node [
    id 256
    label "judykatura"
  ]
  node [
    id 257
    label "legislacyjnie"
  ]
  node [
    id 258
    label "umocowa&#263;"
  ]
  node [
    id 259
    label "podmiot"
  ]
  node [
    id 260
    label "procesualistyka"
  ]
  node [
    id 261
    label "kierunek"
  ]
  node [
    id 262
    label "kryminologia"
  ]
  node [
    id 263
    label "kryminalistyka"
  ]
  node [
    id 264
    label "cywilistyka"
  ]
  node [
    id 265
    label "law"
  ]
  node [
    id 266
    label "zasada_d'Alemberta"
  ]
  node [
    id 267
    label "jurisprudence"
  ]
  node [
    id 268
    label "zasada"
  ]
  node [
    id 269
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 270
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 271
    label "toaleta"
  ]
  node [
    id 272
    label "zdecydowany"
  ]
  node [
    id 273
    label "doros&#322;y"
  ]
  node [
    id 274
    label "stosowny"
  ]
  node [
    id 275
    label "prawdziwy"
  ]
  node [
    id 276
    label "odr&#281;bny"
  ]
  node [
    id 277
    label "m&#281;sko"
  ]
  node [
    id 278
    label "typowy"
  ]
  node [
    id 279
    label "podobny"
  ]
  node [
    id 280
    label "po_m&#281;sku"
  ]
  node [
    id 281
    label "ruch"
  ]
  node [
    id 282
    label "punkt_odniesienia"
  ]
  node [
    id 283
    label "ideal"
  ]
  node [
    id 284
    label "mildew"
  ]
  node [
    id 285
    label "spos&#243;b"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 46
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 28
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 197
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 16
    target 203
  ]
  edge [
    source 16
    target 204
  ]
  edge [
    source 16
    target 205
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 207
  ]
  edge [
    source 16
    target 208
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 16
    target 213
  ]
  edge [
    source 16
    target 214
  ]
  edge [
    source 16
    target 215
  ]
  edge [
    source 16
    target 216
  ]
  edge [
    source 16
    target 217
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 224
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 16
    target 226
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 229
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 232
  ]
  edge [
    source 17
    target 233
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 237
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 239
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 241
  ]
  edge [
    source 17
    target 242
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 245
  ]
  edge [
    source 17
    target 246
  ]
  edge [
    source 17
    target 247
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 249
  ]
  edge [
    source 17
    target 250
  ]
  edge [
    source 17
    target 251
  ]
  edge [
    source 17
    target 252
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 17
    target 254
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 256
  ]
  edge [
    source 17
    target 257
  ]
  edge [
    source 17
    target 258
  ]
  edge [
    source 17
    target 259
  ]
  edge [
    source 17
    target 260
  ]
  edge [
    source 17
    target 261
  ]
  edge [
    source 17
    target 262
  ]
  edge [
    source 17
    target 263
  ]
  edge [
    source 17
    target 264
  ]
  edge [
    source 17
    target 265
  ]
  edge [
    source 17
    target 266
  ]
  edge [
    source 17
    target 267
  ]
  edge [
    source 17
    target 268
  ]
  edge [
    source 17
    target 269
  ]
  edge [
    source 17
    target 270
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 271
  ]
  edge [
    source 18
    target 272
  ]
  edge [
    source 18
    target 273
  ]
  edge [
    source 18
    target 274
  ]
  edge [
    source 18
    target 275
  ]
  edge [
    source 18
    target 276
  ]
  edge [
    source 18
    target 277
  ]
  edge [
    source 18
    target 278
  ]
  edge [
    source 18
    target 279
  ]
  edge [
    source 18
    target 280
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 281
  ]
  edge [
    source 19
    target 282
  ]
  edge [
    source 19
    target 283
  ]
  edge [
    source 19
    target 284
  ]
  edge [
    source 19
    target 285
  ]
]
