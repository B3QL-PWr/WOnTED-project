graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.066420664206642
  density 0.007653409867432008
  graphCliqueNumber 4
  node [
    id 0
    label "gdybyby&#263;"
    origin "text"
  ]
  node [
    id 1
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "moja"
    origin "text"
  ]
  node [
    id 3
    label "c&#243;rka"
    origin "text"
  ]
  node [
    id 4
    label "wyros&#322;y"
    origin "text"
  ]
  node [
    id 5
    label "geeka"
    origin "text"
  ]
  node [
    id 6
    label "sen"
    origin "text"
  ]
  node [
    id 7
    label "cora"
    origin "text"
  ]
  node [
    id 8
    label "ego"
    origin "text"
  ]
  node [
    id 9
    label "doctorowa"
    origin "text"
  ]
  node [
    id 10
    label "ten"
    origin "text"
  ]
  node [
    id 11
    label "nieca&#322;y"
    origin "text"
  ]
  node [
    id 12
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 13
    label "temu"
    origin "text"
  ]
  node [
    id 14
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "information"
    origin "text"
  ]
  node [
    id 16
    label "week"
    origin "text"
  ]
  node [
    id 17
    label "jeden"
    origin "text"
  ]
  node [
    id 18
    label "ale"
    origin "text"
  ]
  node [
    id 19
    label "dwa"
    origin "text"
  ]
  node [
    id 20
    label "&#347;wietny"
    origin "text"
  ]
  node [
    id 21
    label "felieton"
    origin "text"
  ]
  node [
    id 22
    label "drm"
    origin "text"
  ]
  node [
    id 23
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 24
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 25
    label "broadcast"
    origin "text"
  ]
  node [
    id 26
    label "flaga"
    origin "text"
  ]
  node [
    id 27
    label "drugi"
    origin "text"
  ]
  node [
    id 28
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 29
    label "rozrywkowy"
    origin "text"
  ]
  node [
    id 30
    label "z&#322;y"
    origin "text"
  ]
  node [
    id 31
    label "wplata&#263;"
    origin "text"
  ]
  node [
    id 32
    label "techno"
    origin "text"
  ]
  node [
    id 33
    label "polityczny"
    origin "text"
  ]
  node [
    id 34
    label "eseistyka"
    origin "text"
  ]
  node [
    id 35
    label "zwrot"
    origin "text"
  ]
  node [
    id 36
    label "ca&#322;kiem"
    origin "text"
  ]
  node [
    id 37
    label "poetycki"
    origin "text"
  ]
  node [
    id 38
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 39
    label "urok"
    origin "text"
  ]
  node [
    id 40
    label "oddawa&#263;"
    origin "text"
  ]
  node [
    id 41
    label "pe&#322;nia"
    origin "text"
  ]
  node [
    id 42
    label "robiony"
    origin "text"
  ]
  node [
    id 43
    label "komputerowy"
    origin "text"
  ]
  node [
    id 44
    label "kolano"
    origin "text"
  ]
  node [
    id 45
    label "t&#322;umaczenie"
    origin "text"
  ]
  node [
    id 46
    label "czu&#263;"
  ]
  node [
    id 47
    label "desire"
  ]
  node [
    id 48
    label "kcie&#263;"
  ]
  node [
    id 49
    label "cz&#322;owiek"
  ]
  node [
    id 50
    label "dziewka"
  ]
  node [
    id 51
    label "potomkini"
  ]
  node [
    id 52
    label "dziewoja"
  ]
  node [
    id 53
    label "dziunia"
  ]
  node [
    id 54
    label "dziecko"
  ]
  node [
    id 55
    label "dziewczynina"
  ]
  node [
    id 56
    label "siksa"
  ]
  node [
    id 57
    label "dziewcz&#281;"
  ]
  node [
    id 58
    label "kora"
  ]
  node [
    id 59
    label "m&#322;&#243;dka"
  ]
  node [
    id 60
    label "dziecina"
  ]
  node [
    id 61
    label "sikorka"
  ]
  node [
    id 62
    label "wysoki"
  ]
  node [
    id 63
    label "du&#380;y"
  ]
  node [
    id 64
    label "kima"
  ]
  node [
    id 65
    label "wytw&#243;r"
  ]
  node [
    id 66
    label "proces_fizjologiczny"
  ]
  node [
    id 67
    label "relaxation"
  ]
  node [
    id 68
    label "marzenie_senne"
  ]
  node [
    id 69
    label "sen_wolnofalowy"
  ]
  node [
    id 70
    label "odpoczynek"
  ]
  node [
    id 71
    label "hipersomnia"
  ]
  node [
    id 72
    label "jen"
  ]
  node [
    id 73
    label "fun"
  ]
  node [
    id 74
    label "wymys&#322;"
  ]
  node [
    id 75
    label "nokturn"
  ]
  node [
    id 76
    label "sen_paradoksalny"
  ]
  node [
    id 77
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 78
    label "Freud"
  ]
  node [
    id 79
    label "poczucie_w&#322;asnej_warto&#347;ci"
  ]
  node [
    id 80
    label "psychoanaliza"
  ]
  node [
    id 81
    label "psychika"
  ]
  node [
    id 82
    label "okre&#347;lony"
  ]
  node [
    id 83
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 84
    label "nieca&#322;kowicie"
  ]
  node [
    id 85
    label "zdekompletowanie_si&#281;"
  ]
  node [
    id 86
    label "dekompletowanie_si&#281;"
  ]
  node [
    id 87
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 88
    label "doba"
  ]
  node [
    id 89
    label "czas"
  ]
  node [
    id 90
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 91
    label "weekend"
  ]
  node [
    id 92
    label "miesi&#261;c"
  ]
  node [
    id 93
    label "upubliczni&#263;"
  ]
  node [
    id 94
    label "wydawnictwo"
  ]
  node [
    id 95
    label "wprowadzi&#263;"
  ]
  node [
    id 96
    label "picture"
  ]
  node [
    id 97
    label "kieliszek"
  ]
  node [
    id 98
    label "shot"
  ]
  node [
    id 99
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 100
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 101
    label "jaki&#347;"
  ]
  node [
    id 102
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 103
    label "jednolicie"
  ]
  node [
    id 104
    label "w&#243;dka"
  ]
  node [
    id 105
    label "ujednolicenie"
  ]
  node [
    id 106
    label "jednakowy"
  ]
  node [
    id 107
    label "piwo"
  ]
  node [
    id 108
    label "pomy&#347;lny"
  ]
  node [
    id 109
    label "pozytywny"
  ]
  node [
    id 110
    label "wspaniale"
  ]
  node [
    id 111
    label "dobry"
  ]
  node [
    id 112
    label "superancki"
  ]
  node [
    id 113
    label "arcydzielny"
  ]
  node [
    id 114
    label "zajebisty"
  ]
  node [
    id 115
    label "wa&#380;ny"
  ]
  node [
    id 116
    label "&#347;wietnie"
  ]
  node [
    id 117
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 118
    label "skuteczny"
  ]
  node [
    id 119
    label "spania&#322;y"
  ]
  node [
    id 120
    label "felietonistyka"
  ]
  node [
    id 121
    label "artyku&#322;"
  ]
  node [
    id 122
    label "gatunek_literacki"
  ]
  node [
    id 123
    label "tekst_prasowy"
  ]
  node [
    id 124
    label "contribution"
  ]
  node [
    id 125
    label "czyn"
  ]
  node [
    id 126
    label "przedstawiciel"
  ]
  node [
    id 127
    label "ilustracja"
  ]
  node [
    id 128
    label "fakt"
  ]
  node [
    id 129
    label "nowoczesny"
  ]
  node [
    id 130
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 131
    label "po_ameryka&#324;sku"
  ]
  node [
    id 132
    label "boston"
  ]
  node [
    id 133
    label "cake-walk"
  ]
  node [
    id 134
    label "charakterystyczny"
  ]
  node [
    id 135
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 136
    label "fajny"
  ]
  node [
    id 137
    label "j&#281;zyk_angielski"
  ]
  node [
    id 138
    label "Princeton"
  ]
  node [
    id 139
    label "pepperoni"
  ]
  node [
    id 140
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 141
    label "zachodni"
  ]
  node [
    id 142
    label "anglosaski"
  ]
  node [
    id 143
    label "typowy"
  ]
  node [
    id 144
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 145
    label "flag"
  ]
  node [
    id 146
    label "transparent"
  ]
  node [
    id 147
    label "oznaka"
  ]
  node [
    id 148
    label "inny"
  ]
  node [
    id 149
    label "kolejny"
  ]
  node [
    id 150
    label "przeciwny"
  ]
  node [
    id 151
    label "sw&#243;j"
  ]
  node [
    id 152
    label "odwrotnie"
  ]
  node [
    id 153
    label "dzie&#324;"
  ]
  node [
    id 154
    label "podobny"
  ]
  node [
    id 155
    label "wt&#243;ry"
  ]
  node [
    id 156
    label "gospodarka"
  ]
  node [
    id 157
    label "przechowalnictwo"
  ]
  node [
    id 158
    label "uprzemys&#322;owienie"
  ]
  node [
    id 159
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 160
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 161
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 162
    label "uprzemys&#322;awianie"
  ]
  node [
    id 163
    label "towarzyski"
  ]
  node [
    id 164
    label "weso&#322;y"
  ]
  node [
    id 165
    label "rozrywkowo"
  ]
  node [
    id 166
    label "lu&#378;ny"
  ]
  node [
    id 167
    label "zdenerwowany"
  ]
  node [
    id 168
    label "zez&#322;oszczenie"
  ]
  node [
    id 169
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 170
    label "gniewanie"
  ]
  node [
    id 171
    label "niekorzystny"
  ]
  node [
    id 172
    label "niemoralny"
  ]
  node [
    id 173
    label "niegrzeczny"
  ]
  node [
    id 174
    label "pieski"
  ]
  node [
    id 175
    label "negatywny"
  ]
  node [
    id 176
    label "&#378;le"
  ]
  node [
    id 177
    label "syf"
  ]
  node [
    id 178
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 179
    label "sierdzisty"
  ]
  node [
    id 180
    label "z&#322;oszczenie"
  ]
  node [
    id 181
    label "rozgniewanie"
  ]
  node [
    id 182
    label "niepomy&#347;lny"
  ]
  node [
    id 183
    label "przeplata&#263;"
  ]
  node [
    id 184
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 185
    label "wrench"
  ]
  node [
    id 186
    label "techniawa"
  ]
  node [
    id 187
    label "dance"
  ]
  node [
    id 188
    label "subkultura"
  ]
  node [
    id 189
    label "muzyka_elektroniczna"
  ]
  node [
    id 190
    label "internowanie"
  ]
  node [
    id 191
    label "prorz&#261;dowy"
  ]
  node [
    id 192
    label "wi&#281;zie&#324;"
  ]
  node [
    id 193
    label "politycznie"
  ]
  node [
    id 194
    label "internowa&#263;"
  ]
  node [
    id 195
    label "ideologiczny"
  ]
  node [
    id 196
    label "publicystyka"
  ]
  node [
    id 197
    label "turn"
  ]
  node [
    id 198
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 199
    label "skr&#281;t"
  ]
  node [
    id 200
    label "jednostka_leksykalna"
  ]
  node [
    id 201
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 202
    label "obr&#243;t"
  ]
  node [
    id 203
    label "fraza_czasownikowa"
  ]
  node [
    id 204
    label "wyra&#380;enie"
  ]
  node [
    id 205
    label "punkt"
  ]
  node [
    id 206
    label "zmiana"
  ]
  node [
    id 207
    label "turning"
  ]
  node [
    id 208
    label "wniwecz"
  ]
  node [
    id 209
    label "zupe&#322;ny"
  ]
  node [
    id 210
    label "rymotw&#243;rczy"
  ]
  node [
    id 211
    label "poetycko"
  ]
  node [
    id 212
    label "literacki"
  ]
  node [
    id 213
    label "attraction"
  ]
  node [
    id 214
    label "agreeableness"
  ]
  node [
    id 215
    label "czar"
  ]
  node [
    id 216
    label "cecha"
  ]
  node [
    id 217
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 218
    label "render"
  ]
  node [
    id 219
    label "dawa&#263;"
  ]
  node [
    id 220
    label "przedstawia&#263;"
  ]
  node [
    id 221
    label "blurt_out"
  ]
  node [
    id 222
    label "impart"
  ]
  node [
    id 223
    label "sacrifice"
  ]
  node [
    id 224
    label "surrender"
  ]
  node [
    id 225
    label "reflect"
  ]
  node [
    id 226
    label "dostarcza&#263;"
  ]
  node [
    id 227
    label "give"
  ]
  node [
    id 228
    label "umieszcza&#263;"
  ]
  node [
    id 229
    label "deliver"
  ]
  node [
    id 230
    label "odst&#281;powa&#263;"
  ]
  node [
    id 231
    label "sprzedawa&#263;"
  ]
  node [
    id 232
    label "odpowiada&#263;"
  ]
  node [
    id 233
    label "przekazywa&#263;"
  ]
  node [
    id 234
    label "zakres"
  ]
  node [
    id 235
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 236
    label "szczyt"
  ]
  node [
    id 237
    label "sztuczny"
  ]
  node [
    id 238
    label "komputerowo"
  ]
  node [
    id 239
    label "nogawka"
  ]
  node [
    id 240
    label "mi&#281;sie&#324;_stawowy_kolana"
  ]
  node [
    id 241
    label "rura"
  ]
  node [
    id 242
    label "rzepka_kolanowa"
  ]
  node [
    id 243
    label "struktura_anatomiczna"
  ]
  node [
    id 244
    label "zakr&#281;t"
  ]
  node [
    id 245
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 246
    label "kszta&#322;tka_rurowa"
  ]
  node [
    id 247
    label "&#322;&#281;kotka"
  ]
  node [
    id 248
    label "staw_kolanowy"
  ]
  node [
    id 249
    label "noga"
  ]
  node [
    id 250
    label "remark"
  ]
  node [
    id 251
    label "robienie"
  ]
  node [
    id 252
    label "kr&#281;ty"
  ]
  node [
    id 253
    label "rozwianie"
  ]
  node [
    id 254
    label "j&#281;zyk"
  ]
  node [
    id 255
    label "przekonywanie"
  ]
  node [
    id 256
    label "uzasadnianie"
  ]
  node [
    id 257
    label "przedstawianie"
  ]
  node [
    id 258
    label "przek&#322;adanie"
  ]
  node [
    id 259
    label "zrozumia&#322;y"
  ]
  node [
    id 260
    label "explanation"
  ]
  node [
    id 261
    label "rozwiewanie"
  ]
  node [
    id 262
    label "gossip"
  ]
  node [
    id 263
    label "rendition"
  ]
  node [
    id 264
    label "bronienie"
  ]
  node [
    id 265
    label "tekst"
  ]
  node [
    id 266
    label "Cora"
  ]
  node [
    id 267
    label "&#8217;"
  ]
  node [
    id 268
    label "Doctorowa"
  ]
  node [
    id 269
    label "Information"
  ]
  node [
    id 270
    label "Week"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 97
  ]
  edge [
    source 17
    target 98
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 17
    target 106
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 107
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 108
  ]
  edge [
    source 20
    target 109
  ]
  edge [
    source 20
    target 110
  ]
  edge [
    source 20
    target 111
  ]
  edge [
    source 20
    target 112
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 21
    target 120
  ]
  edge [
    source 21
    target 121
  ]
  edge [
    source 21
    target 122
  ]
  edge [
    source 21
    target 123
  ]
  edge [
    source 21
    target 124
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 49
  ]
  edge [
    source 23
    target 125
  ]
  edge [
    source 23
    target 126
  ]
  edge [
    source 23
    target 127
  ]
  edge [
    source 23
    target 128
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 129
  ]
  edge [
    source 24
    target 130
  ]
  edge [
    source 24
    target 131
  ]
  edge [
    source 24
    target 132
  ]
  edge [
    source 24
    target 133
  ]
  edge [
    source 24
    target 134
  ]
  edge [
    source 24
    target 135
  ]
  edge [
    source 24
    target 136
  ]
  edge [
    source 24
    target 137
  ]
  edge [
    source 24
    target 138
  ]
  edge [
    source 24
    target 139
  ]
  edge [
    source 24
    target 140
  ]
  edge [
    source 24
    target 141
  ]
  edge [
    source 24
    target 117
  ]
  edge [
    source 24
    target 142
  ]
  edge [
    source 24
    target 143
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 144
  ]
  edge [
    source 26
    target 145
  ]
  edge [
    source 26
    target 146
  ]
  edge [
    source 26
    target 147
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 27
    target 49
  ]
  edge [
    source 27
    target 148
  ]
  edge [
    source 27
    target 149
  ]
  edge [
    source 27
    target 150
  ]
  edge [
    source 27
    target 151
  ]
  edge [
    source 27
    target 152
  ]
  edge [
    source 27
    target 153
  ]
  edge [
    source 27
    target 154
  ]
  edge [
    source 27
    target 155
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 156
  ]
  edge [
    source 28
    target 157
  ]
  edge [
    source 28
    target 158
  ]
  edge [
    source 28
    target 159
  ]
  edge [
    source 28
    target 160
  ]
  edge [
    source 28
    target 161
  ]
  edge [
    source 28
    target 162
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 163
  ]
  edge [
    source 29
    target 164
  ]
  edge [
    source 29
    target 165
  ]
  edge [
    source 29
    target 166
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 167
  ]
  edge [
    source 30
    target 168
  ]
  edge [
    source 30
    target 169
  ]
  edge [
    source 30
    target 170
  ]
  edge [
    source 30
    target 171
  ]
  edge [
    source 30
    target 172
  ]
  edge [
    source 30
    target 173
  ]
  edge [
    source 30
    target 174
  ]
  edge [
    source 30
    target 175
  ]
  edge [
    source 30
    target 176
  ]
  edge [
    source 30
    target 177
  ]
  edge [
    source 30
    target 178
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 30
    target 181
  ]
  edge [
    source 30
    target 182
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 183
  ]
  edge [
    source 31
    target 184
  ]
  edge [
    source 31
    target 185
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 186
  ]
  edge [
    source 32
    target 187
  ]
  edge [
    source 32
    target 188
  ]
  edge [
    source 32
    target 189
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 190
  ]
  edge [
    source 33
    target 191
  ]
  edge [
    source 33
    target 192
  ]
  edge [
    source 33
    target 193
  ]
  edge [
    source 33
    target 194
  ]
  edge [
    source 33
    target 195
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 196
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 197
  ]
  edge [
    source 35
    target 198
  ]
  edge [
    source 35
    target 199
  ]
  edge [
    source 35
    target 200
  ]
  edge [
    source 35
    target 201
  ]
  edge [
    source 35
    target 202
  ]
  edge [
    source 35
    target 203
  ]
  edge [
    source 35
    target 204
  ]
  edge [
    source 35
    target 205
  ]
  edge [
    source 35
    target 206
  ]
  edge [
    source 35
    target 207
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 208
  ]
  edge [
    source 36
    target 209
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 210
  ]
  edge [
    source 37
    target 211
  ]
  edge [
    source 37
    target 212
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 213
  ]
  edge [
    source 39
    target 214
  ]
  edge [
    source 39
    target 215
  ]
  edge [
    source 39
    target 216
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 217
  ]
  edge [
    source 40
    target 218
  ]
  edge [
    source 40
    target 219
  ]
  edge [
    source 40
    target 220
  ]
  edge [
    source 40
    target 221
  ]
  edge [
    source 40
    target 222
  ]
  edge [
    source 40
    target 223
  ]
  edge [
    source 40
    target 224
  ]
  edge [
    source 40
    target 225
  ]
  edge [
    source 40
    target 226
  ]
  edge [
    source 40
    target 227
  ]
  edge [
    source 40
    target 228
  ]
  edge [
    source 40
    target 229
  ]
  edge [
    source 40
    target 230
  ]
  edge [
    source 40
    target 231
  ]
  edge [
    source 40
    target 232
  ]
  edge [
    source 40
    target 233
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 234
  ]
  edge [
    source 41
    target 235
  ]
  edge [
    source 41
    target 236
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 237
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 238
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 239
  ]
  edge [
    source 44
    target 240
  ]
  edge [
    source 44
    target 241
  ]
  edge [
    source 44
    target 242
  ]
  edge [
    source 44
    target 243
  ]
  edge [
    source 44
    target 244
  ]
  edge [
    source 44
    target 245
  ]
  edge [
    source 44
    target 246
  ]
  edge [
    source 44
    target 247
  ]
  edge [
    source 44
    target 248
  ]
  edge [
    source 44
    target 249
  ]
  edge [
    source 45
    target 250
  ]
  edge [
    source 45
    target 251
  ]
  edge [
    source 45
    target 252
  ]
  edge [
    source 45
    target 253
  ]
  edge [
    source 45
    target 254
  ]
  edge [
    source 45
    target 255
  ]
  edge [
    source 45
    target 256
  ]
  edge [
    source 45
    target 257
  ]
  edge [
    source 45
    target 258
  ]
  edge [
    source 45
    target 259
  ]
  edge [
    source 45
    target 260
  ]
  edge [
    source 45
    target 261
  ]
  edge [
    source 45
    target 262
  ]
  edge [
    source 45
    target 263
  ]
  edge [
    source 45
    target 264
  ]
  edge [
    source 45
    target 265
  ]
  edge [
    source 266
    target 267
  ]
  edge [
    source 266
    target 268
  ]
  edge [
    source 267
    target 268
  ]
  edge [
    source 269
    target 270
  ]
]
