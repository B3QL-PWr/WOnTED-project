graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "kto"
    origin "text"
  ]
  node [
    id 1
    label "zje&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 3
    label "wszystek"
    origin "text"
  ]
  node [
    id 4
    label "czekoladka"
    origin "text"
  ]
  node [
    id 5
    label "raz"
    origin "text"
  ]
  node [
    id 6
    label "ze&#380;re&#263;"
  ]
  node [
    id 7
    label "spo&#380;y&#263;"
  ]
  node [
    id 8
    label "zrobi&#263;"
  ]
  node [
    id 9
    label "absorb"
  ]
  node [
    id 10
    label "ca&#322;y"
  ]
  node [
    id 11
    label "wyr&#243;b_cukierniczy"
  ]
  node [
    id 12
    label "brunetka"
  ]
  node [
    id 13
    label "&#322;ako&#263;"
  ]
  node [
    id 14
    label "chwila"
  ]
  node [
    id 15
    label "uderzenie"
  ]
  node [
    id 16
    label "cios"
  ]
  node [
    id 17
    label "time"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
]
