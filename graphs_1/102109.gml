graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.3
  density 0.011557788944723618
  graphCliqueNumber 3
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "decyzja"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "trzeba"
    origin "text"
  ]
  node [
    id 4
    label "podj&#261;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wybiera&#263;"
    origin "text"
  ]
  node [
    id 6
    label "model"
    origin "text"
  ]
  node [
    id 7
    label "uzale&#380;ni&#263;"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "rodzaj"
    origin "text"
  ]
  node [
    id 10
    label "nawierzchnia"
    origin "text"
  ]
  node [
    id 11
    label "je&#378;dzi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "musie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "wybra&#263;"
    origin "text"
  ]
  node [
    id 14
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 15
    label "road"
    origin "text"
  ]
  node [
    id 16
    label "torowy"
    origin "text"
  ]
  node [
    id 17
    label "off"
    origin "text"
  ]
  node [
    id 18
    label "terenowy"
    origin "text"
  ]
  node [
    id 19
    label "podkre&#347;li&#263;"
    origin "text"
  ]
  node [
    id 20
    label "wymaga&#263;"
    origin "text"
  ]
  node [
    id 21
    label "raczej"
    origin "text"
  ]
  node [
    id 22
    label "g&#322;adki"
    origin "text"
  ]
  node [
    id 23
    label "wyk&#322;adzina"
    origin "text"
  ]
  node [
    id 24
    label "lub"
    origin "text"
  ]
  node [
    id 25
    label "asfalt"
    origin "text"
  ]
  node [
    id 26
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 27
    label "parking"
    origin "text"
  ]
  node [
    id 28
    label "przed"
    origin "text"
  ]
  node [
    id 29
    label "sklep"
    origin "text"
  ]
  node [
    id 30
    label "alejka"
    origin "text"
  ]
  node [
    id 31
    label "park"
    origin "text"
  ]
  node [
    id 32
    label "ale"
    origin "text"
  ]
  node [
    id 33
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 34
    label "placyk"
    origin "text"
  ]
  node [
    id 35
    label "kruszy&#263;"
    origin "text"
  ]
  node [
    id 36
    label "si&#281;"
    origin "text"
  ]
  node [
    id 37
    label "betonowy"
    origin "text"
  ]
  node [
    id 38
    label "p&#322;yt"
    origin "text"
  ]
  node [
    id 39
    label "sprawdzi&#263;"
    origin "text"
  ]
  node [
    id 40
    label "jako"
    origin "text"
  ]
  node [
    id 41
    label "dla"
    origin "text"
  ]
  node [
    id 42
    label "inny"
  ]
  node [
    id 43
    label "nast&#281;pnie"
  ]
  node [
    id 44
    label "kt&#243;ry&#347;"
  ]
  node [
    id 45
    label "kolejno"
  ]
  node [
    id 46
    label "nastopny"
  ]
  node [
    id 47
    label "dokument"
  ]
  node [
    id 48
    label "resolution"
  ]
  node [
    id 49
    label "zdecydowanie"
  ]
  node [
    id 50
    label "wytw&#243;r"
  ]
  node [
    id 51
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 52
    label "management"
  ]
  node [
    id 53
    label "trza"
  ]
  node [
    id 54
    label "necessity"
  ]
  node [
    id 55
    label "zmieni&#263;"
  ]
  node [
    id 56
    label "zacz&#261;&#263;"
  ]
  node [
    id 57
    label "zareagowa&#263;"
  ]
  node [
    id 58
    label "allude"
  ]
  node [
    id 59
    label "raise"
  ]
  node [
    id 60
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 61
    label "draw"
  ]
  node [
    id 62
    label "zrobi&#263;"
  ]
  node [
    id 63
    label "sie&#263;_rybacka"
  ]
  node [
    id 64
    label "wyjmowa&#263;"
  ]
  node [
    id 65
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 66
    label "powo&#322;ywa&#263;"
  ]
  node [
    id 67
    label "kotwica"
  ]
  node [
    id 68
    label "take"
  ]
  node [
    id 69
    label "ustala&#263;"
  ]
  node [
    id 70
    label "typ"
  ]
  node [
    id 71
    label "cz&#322;owiek"
  ]
  node [
    id 72
    label "pozowa&#263;"
  ]
  node [
    id 73
    label "ideal"
  ]
  node [
    id 74
    label "matryca"
  ]
  node [
    id 75
    label "imitacja"
  ]
  node [
    id 76
    label "ruch"
  ]
  node [
    id 77
    label "motif"
  ]
  node [
    id 78
    label "pozowanie"
  ]
  node [
    id 79
    label "wz&#243;r"
  ]
  node [
    id 80
    label "miniatura"
  ]
  node [
    id 81
    label "prezenter"
  ]
  node [
    id 82
    label "facet"
  ]
  node [
    id 83
    label "orygina&#322;"
  ]
  node [
    id 84
    label "mildew"
  ]
  node [
    id 85
    label "spos&#243;b"
  ]
  node [
    id 86
    label "zi&#243;&#322;ko"
  ]
  node [
    id 87
    label "adaptation"
  ]
  node [
    id 88
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 89
    label "hyponym"
  ]
  node [
    id 90
    label "spowodowa&#263;"
  ]
  node [
    id 91
    label "si&#281;ga&#263;"
  ]
  node [
    id 92
    label "trwa&#263;"
  ]
  node [
    id 93
    label "obecno&#347;&#263;"
  ]
  node [
    id 94
    label "stan"
  ]
  node [
    id 95
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 96
    label "stand"
  ]
  node [
    id 97
    label "mie&#263;_miejsce"
  ]
  node [
    id 98
    label "uczestniczy&#263;"
  ]
  node [
    id 99
    label "chodzi&#263;"
  ]
  node [
    id 100
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 101
    label "equal"
  ]
  node [
    id 102
    label "kategoria_gramatyczna"
  ]
  node [
    id 103
    label "autorament"
  ]
  node [
    id 104
    label "jednostka_systematyczna"
  ]
  node [
    id 105
    label "fashion"
  ]
  node [
    id 106
    label "rodzina"
  ]
  node [
    id 107
    label "variety"
  ]
  node [
    id 108
    label "droga"
  ]
  node [
    id 109
    label "warstwa"
  ]
  node [
    id 110
    label "pokrycie"
  ]
  node [
    id 111
    label "continue"
  ]
  node [
    id 112
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 113
    label "umie&#263;"
  ]
  node [
    id 114
    label "napada&#263;"
  ]
  node [
    id 115
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 116
    label "proceed"
  ]
  node [
    id 117
    label "przybywa&#263;"
  ]
  node [
    id 118
    label "uprawia&#263;"
  ]
  node [
    id 119
    label "drive"
  ]
  node [
    id 120
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 121
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 122
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 123
    label "ride"
  ]
  node [
    id 124
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 125
    label "carry"
  ]
  node [
    id 126
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 127
    label "prowadzi&#263;"
  ]
  node [
    id 128
    label "need"
  ]
  node [
    id 129
    label "pragn&#261;&#263;"
  ]
  node [
    id 130
    label "zu&#380;y&#263;"
  ]
  node [
    id 131
    label "distill"
  ]
  node [
    id 132
    label "wyj&#261;&#263;"
  ]
  node [
    id 133
    label "powo&#322;a&#263;"
  ]
  node [
    id 134
    label "ustali&#263;"
  ]
  node [
    id 135
    label "pick"
  ]
  node [
    id 136
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 137
    label "specjalny"
  ]
  node [
    id 138
    label "terenowo"
  ]
  node [
    id 139
    label "kreska"
  ]
  node [
    id 140
    label "narysowa&#263;"
  ]
  node [
    id 141
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 142
    label "try"
  ]
  node [
    id 143
    label "zmusza&#263;"
  ]
  node [
    id 144
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 145
    label "claim"
  ]
  node [
    id 146
    label "force"
  ]
  node [
    id 147
    label "elegancki"
  ]
  node [
    id 148
    label "&#322;adny"
  ]
  node [
    id 149
    label "kulturalny"
  ]
  node [
    id 150
    label "wyg&#322;adzanie_si&#281;"
  ]
  node [
    id 151
    label "prosty"
  ]
  node [
    id 152
    label "g&#322;adzenie"
  ]
  node [
    id 153
    label "r&#243;wny"
  ]
  node [
    id 154
    label "og&#243;lnikowy"
  ]
  node [
    id 155
    label "g&#322;adko"
  ]
  node [
    id 156
    label "przyg&#322;adzanie"
  ]
  node [
    id 157
    label "jednobarwny"
  ]
  node [
    id 158
    label "cisza"
  ]
  node [
    id 159
    label "obtaczanie"
  ]
  node [
    id 160
    label "wyg&#322;adzenie"
  ]
  node [
    id 161
    label "&#322;atwy"
  ]
  node [
    id 162
    label "wyr&#243;wnanie"
  ]
  node [
    id 163
    label "atrakcyjny"
  ]
  node [
    id 164
    label "wyg&#322;adzenie_si&#281;"
  ]
  node [
    id 165
    label "okr&#261;g&#322;y"
  ]
  node [
    id 166
    label "bezproblemowy"
  ]
  node [
    id 167
    label "grzeczny"
  ]
  node [
    id 168
    label "przyg&#322;adzenie"
  ]
  node [
    id 169
    label "nieruchomy"
  ]
  node [
    id 170
    label "materia&#322;_budowlany"
  ]
  node [
    id 171
    label "tkanina"
  ]
  node [
    id 172
    label "wyposa&#380;enie_wn&#281;trz"
  ]
  node [
    id 173
    label "asfalt&#243;wka"
  ]
  node [
    id 174
    label "czarnosk&#243;ry"
  ]
  node [
    id 175
    label "plac"
  ]
  node [
    id 176
    label "stoisko"
  ]
  node [
    id 177
    label "sk&#322;ad"
  ]
  node [
    id 178
    label "firma"
  ]
  node [
    id 179
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 180
    label "witryna"
  ]
  node [
    id 181
    label "obiekt_handlowy"
  ]
  node [
    id 182
    label "zaplecze"
  ]
  node [
    id 183
    label "p&#243;&#322;ka"
  ]
  node [
    id 184
    label "&#347;cie&#380;ka"
  ]
  node [
    id 185
    label "teren_przemys&#322;owy"
  ]
  node [
    id 186
    label "miejsce"
  ]
  node [
    id 187
    label "ballpark"
  ]
  node [
    id 188
    label "obszar"
  ]
  node [
    id 189
    label "sprz&#281;t"
  ]
  node [
    id 190
    label "tabor"
  ]
  node [
    id 191
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 192
    label "teren_zielony"
  ]
  node [
    id 193
    label "piwo"
  ]
  node [
    id 194
    label "transgress"
  ]
  node [
    id 195
    label "rozdrabnia&#263;"
  ]
  node [
    id 196
    label "t&#281;py"
  ]
  node [
    id 197
    label "uparty"
  ]
  node [
    id 198
    label "oddany"
  ]
  node [
    id 199
    label "examine"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 68
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 147
  ]
  edge [
    source 22
    target 148
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 22
    target 150
  ]
  edge [
    source 22
    target 151
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 22
    target 155
  ]
  edge [
    source 22
    target 156
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 170
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 175
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 176
  ]
  edge [
    source 29
    target 177
  ]
  edge [
    source 29
    target 178
  ]
  edge [
    source 29
    target 179
  ]
  edge [
    source 29
    target 180
  ]
  edge [
    source 29
    target 181
  ]
  edge [
    source 29
    target 182
  ]
  edge [
    source 29
    target 183
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 184
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 185
  ]
  edge [
    source 31
    target 186
  ]
  edge [
    source 31
    target 187
  ]
  edge [
    source 31
    target 188
  ]
  edge [
    source 31
    target 189
  ]
  edge [
    source 31
    target 190
  ]
  edge [
    source 31
    target 191
  ]
  edge [
    source 31
    target 192
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 193
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 194
  ]
  edge [
    source 35
    target 195
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 196
  ]
  edge [
    source 37
    target 197
  ]
  edge [
    source 37
    target 198
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 199
  ]
  edge [
    source 39
    target 62
  ]
]
