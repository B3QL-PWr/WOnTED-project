graph [
  maxDegree 24
  minDegree 1
  meanDegree 1.9545454545454546
  density 0.045454545454545456
  graphCliqueNumber 3
  node [
    id 0
    label "zakonnica"
    origin "text"
  ]
  node [
    id 1
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 3
    label "wynikn&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "nic"
    origin "text"
  ]
  node [
    id 5
    label "dobre"
    origin "text"
  ]
  node [
    id 6
    label "pingwin"
  ]
  node [
    id 7
    label "kornet"
  ]
  node [
    id 8
    label "wyznawczyni"
  ]
  node [
    id 9
    label "baga&#380;nik"
  ]
  node [
    id 10
    label "immobilizer"
  ]
  node [
    id 11
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 12
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 13
    label "poduszka_powietrzna"
  ]
  node [
    id 14
    label "dachowanie"
  ]
  node [
    id 15
    label "dwu&#347;lad"
  ]
  node [
    id 16
    label "deska_rozdzielcza"
  ]
  node [
    id 17
    label "poci&#261;g_drogowy"
  ]
  node [
    id 18
    label "kierownica"
  ]
  node [
    id 19
    label "pojazd_drogowy"
  ]
  node [
    id 20
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 21
    label "pompa_wodna"
  ]
  node [
    id 22
    label "silnik"
  ]
  node [
    id 23
    label "wycieraczka"
  ]
  node [
    id 24
    label "bak"
  ]
  node [
    id 25
    label "ABS"
  ]
  node [
    id 26
    label "most"
  ]
  node [
    id 27
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 28
    label "spryskiwacz"
  ]
  node [
    id 29
    label "t&#322;umik"
  ]
  node [
    id 30
    label "tempomat"
  ]
  node [
    id 31
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 32
    label "rise"
  ]
  node [
    id 33
    label "appear"
  ]
  node [
    id 34
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 35
    label "miernota"
  ]
  node [
    id 36
    label "g&#243;wno"
  ]
  node [
    id 37
    label "love"
  ]
  node [
    id 38
    label "ilo&#347;&#263;"
  ]
  node [
    id 39
    label "brak"
  ]
  node [
    id 40
    label "ciura"
  ]
  node [
    id 41
    label "Louis"
  ]
  node [
    id 42
    label "de"
  ]
  node [
    id 43
    label "Funes"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 42
    target 43
  ]
]
