graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "zagadka"
    origin "text"
  ]
  node [
    id 1
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 2
    label "po&#322;udnica"
  ]
  node [
    id 3
    label "&#322;amig&#322;&#243;wka"
  ]
  node [
    id 4
    label "rzecz"
  ]
  node [
    id 5
    label "mystery"
  ]
  node [
    id 6
    label "enigmat"
  ]
  node [
    id 7
    label "taj&#324;"
  ]
  node [
    id 8
    label "doba"
  ]
  node [
    id 9
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 10
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 11
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
]
