graph [
  maxDegree 14
  minDegree 1
  meanDegree 2.135593220338983
  density 0.03682057276446522
  graphCliqueNumber 5
  node [
    id 0
    label "tyson"
    origin "text"
  ]
  node [
    id 1
    label "fura"
    origin "text"
  ]
  node [
    id 2
    label "emocjonuj&#261;cy"
    origin "text"
  ]
  node [
    id 3
    label "pojedynek"
    origin "text"
  ]
  node [
    id 4
    label "ostatecznie"
    origin "text"
  ]
  node [
    id 5
    label "zremisowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "deontayem"
    origin "text"
  ]
  node [
    id 7
    label "wilder"
    origin "text"
  ]
  node [
    id 8
    label "podczas"
    origin "text"
  ]
  node [
    id 9
    label "gal"
    origin "text"
  ]
  node [
    id 10
    label "los"
    origin "text"
  ]
  node [
    id 11
    label "angeles"
    origin "text"
  ]
  node [
    id 12
    label "uk&#322;ad"
  ]
  node [
    id 13
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 14
    label "bryczka"
  ]
  node [
    id 15
    label "w&#243;z"
  ]
  node [
    id 16
    label "&#322;adunek"
  ]
  node [
    id 17
    label "lu&#347;nia"
  ]
  node [
    id 18
    label "kupa"
  ]
  node [
    id 19
    label "emocjonuj&#261;co"
  ]
  node [
    id 20
    label "pe&#322;ny"
  ]
  node [
    id 21
    label "ciekawy"
  ]
  node [
    id 22
    label "wyzwanie"
  ]
  node [
    id 23
    label "wyzywanie"
  ]
  node [
    id 24
    label "engagement"
  ]
  node [
    id 25
    label "walka"
  ]
  node [
    id 26
    label "wyzywa&#263;"
  ]
  node [
    id 27
    label "turniej"
  ]
  node [
    id 28
    label "wyzwa&#263;"
  ]
  node [
    id 29
    label "competitiveness"
  ]
  node [
    id 30
    label "bout"
  ]
  node [
    id 31
    label "odyniec"
  ]
  node [
    id 32
    label "sekundant"
  ]
  node [
    id 33
    label "sp&#243;r"
  ]
  node [
    id 34
    label "ostateczny"
  ]
  node [
    id 35
    label "finalnie"
  ]
  node [
    id 36
    label "zupe&#322;nie"
  ]
  node [
    id 37
    label "tie"
  ]
  node [
    id 38
    label "sko&#324;czy&#263;"
  ]
  node [
    id 39
    label "metal"
  ]
  node [
    id 40
    label "jednostka_przy&#347;pieszenia"
  ]
  node [
    id 41
    label "borowiec"
  ]
  node [
    id 42
    label "si&#322;a"
  ]
  node [
    id 43
    label "przymus"
  ]
  node [
    id 44
    label "rzuci&#263;"
  ]
  node [
    id 45
    label "hazard"
  ]
  node [
    id 46
    label "destiny"
  ]
  node [
    id 47
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 48
    label "bilet"
  ]
  node [
    id 49
    label "przebieg_&#380;ycia"
  ]
  node [
    id 50
    label "&#380;ycie"
  ]
  node [
    id 51
    label "rzucenie"
  ]
  node [
    id 52
    label "Deontayem"
  ]
  node [
    id 53
    label "Wilder"
  ]
  node [
    id 54
    label "Angeles"
  ]
  node [
    id 55
    label "&#8217;"
  ]
  node [
    id 56
    label "ego"
  ]
  node [
    id 57
    label "114"
  ]
  node [
    id 58
    label "110"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 57
    target 58
  ]
]
