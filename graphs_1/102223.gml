graph [
  maxDegree 44
  minDegree 1
  meanDegree 2.2857142857142856
  density 0.014285714285714285
  graphCliqueNumber 3
  node [
    id 0
    label "warto&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dzia&#322;ka"
    origin "text"
  ]
  node [
    id 2
    label "legalizacyjny"
    origin "text"
  ]
  node [
    id 3
    label "umowny"
    origin "text"
  ]
  node [
    id 4
    label "wyrazi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "jednostka"
    origin "text"
  ]
  node [
    id 6
    label "miara"
    origin "text"
  ]
  node [
    id 7
    label "masa"
    origin "text"
  ]
  node [
    id 8
    label "stosowany"
    origin "text"
  ]
  node [
    id 9
    label "badanie"
    origin "text"
  ]
  node [
    id 10
    label "kontrola"
    origin "text"
  ]
  node [
    id 11
    label "klasyfikacja"
    origin "text"
  ]
  node [
    id 12
    label "waga"
    origin "text"
  ]
  node [
    id 13
    label "wabik"
  ]
  node [
    id 14
    label "rewaluowa&#263;"
  ]
  node [
    id 15
    label "wskazywanie"
  ]
  node [
    id 16
    label "korzy&#347;&#263;"
  ]
  node [
    id 17
    label "worth"
  ]
  node [
    id 18
    label "rewaluowanie"
  ]
  node [
    id 19
    label "cecha"
  ]
  node [
    id 20
    label "zmienna"
  ]
  node [
    id 21
    label "zrewaluowa&#263;"
  ]
  node [
    id 22
    label "rozmiar"
  ]
  node [
    id 23
    label "poj&#281;cie"
  ]
  node [
    id 24
    label "cel"
  ]
  node [
    id 25
    label "wskazywa&#263;"
  ]
  node [
    id 26
    label "strona"
  ]
  node [
    id 27
    label "zrewaluowanie"
  ]
  node [
    id 28
    label "dawka"
  ]
  node [
    id 29
    label "obszar"
  ]
  node [
    id 30
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 31
    label "kielich"
  ]
  node [
    id 32
    label "podzia&#322;ka"
  ]
  node [
    id 33
    label "odst&#281;p"
  ]
  node [
    id 34
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 35
    label "package"
  ]
  node [
    id 36
    label "room"
  ]
  node [
    id 37
    label "dziedzina"
  ]
  node [
    id 38
    label "okre&#347;lony"
  ]
  node [
    id 39
    label "konwencjonalnie"
  ]
  node [
    id 40
    label "symbolicznie"
  ]
  node [
    id 41
    label "nieprawdziwy"
  ]
  node [
    id 42
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 43
    label "zakomunikowa&#263;"
  ]
  node [
    id 44
    label "vent"
  ]
  node [
    id 45
    label "oznaczy&#263;"
  ]
  node [
    id 46
    label "testify"
  ]
  node [
    id 47
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 48
    label "infimum"
  ]
  node [
    id 49
    label "przyswoi&#263;"
  ]
  node [
    id 50
    label "ewoluowanie"
  ]
  node [
    id 51
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 52
    label "reakcja"
  ]
  node [
    id 53
    label "wyewoluowanie"
  ]
  node [
    id 54
    label "individual"
  ]
  node [
    id 55
    label "profanum"
  ]
  node [
    id 56
    label "starzenie_si&#281;"
  ]
  node [
    id 57
    label "homo_sapiens"
  ]
  node [
    id 58
    label "skala"
  ]
  node [
    id 59
    label "supremum"
  ]
  node [
    id 60
    label "przyswaja&#263;"
  ]
  node [
    id 61
    label "ludzko&#347;&#263;"
  ]
  node [
    id 62
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 63
    label "one"
  ]
  node [
    id 64
    label "funkcja"
  ]
  node [
    id 65
    label "przeliczenie"
  ]
  node [
    id 66
    label "przeliczanie"
  ]
  node [
    id 67
    label "mikrokosmos"
  ]
  node [
    id 68
    label "rzut"
  ]
  node [
    id 69
    label "portrecista"
  ]
  node [
    id 70
    label "przelicza&#263;"
  ]
  node [
    id 71
    label "przyswajanie"
  ]
  node [
    id 72
    label "duch"
  ]
  node [
    id 73
    label "wyewoluowa&#263;"
  ]
  node [
    id 74
    label "ewoluowa&#263;"
  ]
  node [
    id 75
    label "g&#322;owa"
  ]
  node [
    id 76
    label "oddzia&#322;ywanie"
  ]
  node [
    id 77
    label "liczba_naturalna"
  ]
  node [
    id 78
    label "osoba"
  ]
  node [
    id 79
    label "figura"
  ]
  node [
    id 80
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 81
    label "obiekt"
  ]
  node [
    id 82
    label "matematyka"
  ]
  node [
    id 83
    label "przyswojenie"
  ]
  node [
    id 84
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 85
    label "czynnik_biotyczny"
  ]
  node [
    id 86
    label "przeliczy&#263;"
  ]
  node [
    id 87
    label "antropochoria"
  ]
  node [
    id 88
    label "dymensja"
  ]
  node [
    id 89
    label "proportion"
  ]
  node [
    id 90
    label "granica"
  ]
  node [
    id 91
    label "ilo&#347;&#263;"
  ]
  node [
    id 92
    label "wielko&#347;&#263;"
  ]
  node [
    id 93
    label "continence"
  ]
  node [
    id 94
    label "odwiedziny"
  ]
  node [
    id 95
    label "liczba"
  ]
  node [
    id 96
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 97
    label "warunek_lokalowy"
  ]
  node [
    id 98
    label "zakres"
  ]
  node [
    id 99
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 100
    label "enormousness"
  ]
  node [
    id 101
    label "sum"
  ]
  node [
    id 102
    label "mass"
  ]
  node [
    id 103
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 104
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 105
    label "masa_relatywistyczna"
  ]
  node [
    id 106
    label "praktyczny"
  ]
  node [
    id 107
    label "usi&#322;owanie"
  ]
  node [
    id 108
    label "examination"
  ]
  node [
    id 109
    label "investigation"
  ]
  node [
    id 110
    label "ustalenie"
  ]
  node [
    id 111
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 112
    label "ustalanie"
  ]
  node [
    id 113
    label "bia&#322;a_niedziela"
  ]
  node [
    id 114
    label "analysis"
  ]
  node [
    id 115
    label "rozpatrywanie"
  ]
  node [
    id 116
    label "wziernikowanie"
  ]
  node [
    id 117
    label "obserwowanie"
  ]
  node [
    id 118
    label "omawianie"
  ]
  node [
    id 119
    label "sprawdzanie"
  ]
  node [
    id 120
    label "udowadnianie"
  ]
  node [
    id 121
    label "diagnostyka"
  ]
  node [
    id 122
    label "czynno&#347;&#263;"
  ]
  node [
    id 123
    label "macanie"
  ]
  node [
    id 124
    label "rektalny"
  ]
  node [
    id 125
    label "penetrowanie"
  ]
  node [
    id 126
    label "krytykowanie"
  ]
  node [
    id 127
    label "dociekanie"
  ]
  node [
    id 128
    label "zrecenzowanie"
  ]
  node [
    id 129
    label "praca"
  ]
  node [
    id 130
    label "rezultat"
  ]
  node [
    id 131
    label "legalizacja_pierwotna"
  ]
  node [
    id 132
    label "w&#322;adza"
  ]
  node [
    id 133
    label "perlustracja"
  ]
  node [
    id 134
    label "instytucja"
  ]
  node [
    id 135
    label "legalizacja_ponowna"
  ]
  node [
    id 136
    label "wytw&#243;r"
  ]
  node [
    id 137
    label "uplasowa&#263;_si&#281;"
  ]
  node [
    id 138
    label "podzia&#322;"
  ]
  node [
    id 139
    label "competence"
  ]
  node [
    id 140
    label "stopie&#324;"
  ]
  node [
    id 141
    label "ocena"
  ]
  node [
    id 142
    label "kolejno&#347;&#263;"
  ]
  node [
    id 143
    label "plasowanie_si&#281;"
  ]
  node [
    id 144
    label "uplasowanie_si&#281;"
  ]
  node [
    id 145
    label "plasowa&#263;_si&#281;"
  ]
  node [
    id 146
    label "distribution"
  ]
  node [
    id 147
    label "division"
  ]
  node [
    id 148
    label "report"
  ]
  node [
    id 149
    label "cz&#322;owiek"
  ]
  node [
    id 150
    label "odwa&#380;nik"
  ]
  node [
    id 151
    label "kategoria"
  ]
  node [
    id 152
    label "weight"
  ]
  node [
    id 153
    label "zawa&#380;enie"
  ]
  node [
    id 154
    label "zawa&#380;y&#263;"
  ]
  node [
    id 155
    label "j&#281;zyczek_u_wagi"
  ]
  node [
    id 156
    label "szala"
  ]
  node [
    id 157
    label "&#263;wiczenie"
  ]
  node [
    id 158
    label "urz&#261;dzenie"
  ]
  node [
    id 159
    label "pomiar"
  ]
  node [
    id 160
    label "load"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
]
