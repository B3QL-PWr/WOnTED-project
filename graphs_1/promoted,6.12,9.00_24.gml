graph [
  maxDegree 37
  minDegree 1
  meanDegree 2.05
  density 0.01289308176100629
  graphCliqueNumber 3
  node [
    id 0
    label "francja"
    origin "text"
  ]
  node [
    id 1
    label "grozi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ca&#322;kowity"
    origin "text"
  ]
  node [
    id 3
    label "parali&#380;"
    origin "text"
  ]
  node [
    id 4
    label "przed"
    origin "text"
  ]
  node [
    id 5
    label "&#347;wi&#281;to"
    origin "text"
  ]
  node [
    id 6
    label "tak"
    origin "text"
  ]
  node [
    id 7
    label "nadsekwa&#324;ski"
    origin "text"
  ]
  node [
    id 8
    label "prasa"
    origin "text"
  ]
  node [
    id 9
    label "komentowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "zapowied&#378;"
    origin "text"
  ]
  node [
    id 11
    label "strajk"
    origin "text"
  ]
  node [
    id 12
    label "kierowca"
    origin "text"
  ]
  node [
    id 13
    label "ci&#281;&#380;ar&#243;wka"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 16
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 17
    label "maj"
    origin "text"
  ]
  node [
    id 18
    label "przy&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "ruch"
    origin "text"
  ]
  node [
    id 21
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 22
    label "kamizelka"
    origin "text"
  ]
  node [
    id 23
    label "strajkuj&#261;cy"
    origin "text"
  ]
  node [
    id 24
    label "przeciwko"
    origin "text"
  ]
  node [
    id 25
    label "polityka"
    origin "text"
  ]
  node [
    id 26
    label "prezydent"
    origin "text"
  ]
  node [
    id 27
    label "macrona"
    origin "text"
  ]
  node [
    id 28
    label "zapowiada&#263;"
  ]
  node [
    id 29
    label "boast"
  ]
  node [
    id 30
    label "hazard"
  ]
  node [
    id 31
    label "w_pizdu"
  ]
  node [
    id 32
    label "&#322;&#261;czny"
  ]
  node [
    id 33
    label "og&#243;lnie"
  ]
  node [
    id 34
    label "ca&#322;y"
  ]
  node [
    id 35
    label "pe&#322;ny"
  ]
  node [
    id 36
    label "zupe&#322;nie"
  ]
  node [
    id 37
    label "kompletnie"
  ]
  node [
    id 38
    label "kryzys"
  ]
  node [
    id 39
    label "bierno&#347;&#263;"
  ]
  node [
    id 40
    label "oznaka"
  ]
  node [
    id 41
    label "trema"
  ]
  node [
    id 42
    label "paralysis"
  ]
  node [
    id 43
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 44
    label "czas"
  ]
  node [
    id 45
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 46
    label "Nowy_Rok"
  ]
  node [
    id 47
    label "Godowe_&#346;wi&#281;to"
  ]
  node [
    id 48
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 49
    label "Barb&#243;rka"
  ]
  node [
    id 50
    label "ramadan"
  ]
  node [
    id 51
    label "pisa&#263;"
  ]
  node [
    id 52
    label "dziennikarz_prasowy"
  ]
  node [
    id 53
    label "gazeta"
  ]
  node [
    id 54
    label "napisa&#263;"
  ]
  node [
    id 55
    label "maszyna_rolnicza"
  ]
  node [
    id 56
    label "zesp&#243;&#322;"
  ]
  node [
    id 57
    label "t&#322;oczysko"
  ]
  node [
    id 58
    label "depesza"
  ]
  node [
    id 59
    label "maszyna"
  ]
  node [
    id 60
    label "czasopismo"
  ]
  node [
    id 61
    label "media"
  ]
  node [
    id 62
    label "kiosk"
  ]
  node [
    id 63
    label "krytykowa&#263;"
  ]
  node [
    id 64
    label "gloss"
  ]
  node [
    id 65
    label "interpretowa&#263;"
  ]
  node [
    id 66
    label "przewidywanie"
  ]
  node [
    id 67
    label "declaration"
  ]
  node [
    id 68
    label "zawiadomienie"
  ]
  node [
    id 69
    label "signal"
  ]
  node [
    id 70
    label "bunt"
  ]
  node [
    id 71
    label "stop"
  ]
  node [
    id 72
    label "pogotowie_strajkowe"
  ]
  node [
    id 73
    label "Sierpie&#324;"
  ]
  node [
    id 74
    label "cz&#322;owiek"
  ]
  node [
    id 75
    label "transportowiec"
  ]
  node [
    id 76
    label "szoferka"
  ]
  node [
    id 77
    label "kabina"
  ]
  node [
    id 78
    label "samoch&#243;d"
  ]
  node [
    id 79
    label "ci&#281;&#380;arowy"
  ]
  node [
    id 80
    label "kolejny"
  ]
  node [
    id 81
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 82
    label "doba"
  ]
  node [
    id 83
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 84
    label "weekend"
  ]
  node [
    id 85
    label "miesi&#261;c"
  ]
  node [
    id 86
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 87
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 88
    label "doprowadzi&#263;"
  ]
  node [
    id 89
    label "submit"
  ]
  node [
    id 90
    label "impersonate"
  ]
  node [
    id 91
    label "umie&#347;ci&#263;"
  ]
  node [
    id 92
    label "incorporate"
  ]
  node [
    id 93
    label "dokoptowa&#263;"
  ]
  node [
    id 94
    label "zwi&#261;za&#263;"
  ]
  node [
    id 95
    label "manewr"
  ]
  node [
    id 96
    label "model"
  ]
  node [
    id 97
    label "movement"
  ]
  node [
    id 98
    label "apraksja"
  ]
  node [
    id 99
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 100
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 101
    label "poruszenie"
  ]
  node [
    id 102
    label "commercial_enterprise"
  ]
  node [
    id 103
    label "dyssypacja_energii"
  ]
  node [
    id 104
    label "zmiana"
  ]
  node [
    id 105
    label "utrzymanie"
  ]
  node [
    id 106
    label "utrzyma&#263;"
  ]
  node [
    id 107
    label "komunikacja"
  ]
  node [
    id 108
    label "tumult"
  ]
  node [
    id 109
    label "kr&#243;tki"
  ]
  node [
    id 110
    label "drift"
  ]
  node [
    id 111
    label "utrzymywa&#263;"
  ]
  node [
    id 112
    label "stopek"
  ]
  node [
    id 113
    label "kanciasty"
  ]
  node [
    id 114
    label "d&#322;ugi"
  ]
  node [
    id 115
    label "zjawisko"
  ]
  node [
    id 116
    label "utrzymywanie"
  ]
  node [
    id 117
    label "czynno&#347;&#263;"
  ]
  node [
    id 118
    label "myk"
  ]
  node [
    id 119
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 120
    label "wydarzenie"
  ]
  node [
    id 121
    label "taktyka"
  ]
  node [
    id 122
    label "move"
  ]
  node [
    id 123
    label "natural_process"
  ]
  node [
    id 124
    label "lokomocja"
  ]
  node [
    id 125
    label "mechanika"
  ]
  node [
    id 126
    label "proces"
  ]
  node [
    id 127
    label "strumie&#324;"
  ]
  node [
    id 128
    label "aktywno&#347;&#263;"
  ]
  node [
    id 129
    label "travel"
  ]
  node [
    id 130
    label "jasny"
  ]
  node [
    id 131
    label "typ_mongoloidalny"
  ]
  node [
    id 132
    label "kolorowy"
  ]
  node [
    id 133
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 134
    label "ciep&#322;y"
  ]
  node [
    id 135
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 136
    label "westa"
  ]
  node [
    id 137
    label "g&#243;ra"
  ]
  node [
    id 138
    label "uczestnik"
  ]
  node [
    id 139
    label "policy"
  ]
  node [
    id 140
    label "dyplomacja"
  ]
  node [
    id 141
    label "Europejska_Polityka_S&#261;siedztwa"
  ]
  node [
    id 142
    label "metoda"
  ]
  node [
    id 143
    label "Jelcyn"
  ]
  node [
    id 144
    label "Roosevelt"
  ]
  node [
    id 145
    label "Clinton"
  ]
  node [
    id 146
    label "dostojnik"
  ]
  node [
    id 147
    label "Tito"
  ]
  node [
    id 148
    label "de_Gaulle"
  ]
  node [
    id 149
    label "Nixon"
  ]
  node [
    id 150
    label "gruba_ryba"
  ]
  node [
    id 151
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 152
    label "Putin"
  ]
  node [
    id 153
    label "Gorbaczow"
  ]
  node [
    id 154
    label "Naser"
  ]
  node [
    id 155
    label "samorz&#261;dowiec"
  ]
  node [
    id 156
    label "Kemal"
  ]
  node [
    id 157
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 158
    label "zwierzchnik"
  ]
  node [
    id 159
    label "Bierut"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 44
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 43
  ]
  edge [
    source 17
    target 85
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 86
  ]
  edge [
    source 18
    target 87
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 89
  ]
  edge [
    source 18
    target 90
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 92
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 94
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 95
  ]
  edge [
    source 20
    target 96
  ]
  edge [
    source 20
    target 97
  ]
  edge [
    source 20
    target 98
  ]
  edge [
    source 20
    target 99
  ]
  edge [
    source 20
    target 100
  ]
  edge [
    source 20
    target 101
  ]
  edge [
    source 20
    target 102
  ]
  edge [
    source 20
    target 103
  ]
  edge [
    source 20
    target 104
  ]
  edge [
    source 20
    target 105
  ]
  edge [
    source 20
    target 106
  ]
  edge [
    source 20
    target 107
  ]
  edge [
    source 20
    target 108
  ]
  edge [
    source 20
    target 109
  ]
  edge [
    source 20
    target 110
  ]
  edge [
    source 20
    target 111
  ]
  edge [
    source 20
    target 112
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 130
  ]
  edge [
    source 21
    target 131
  ]
  edge [
    source 21
    target 132
  ]
  edge [
    source 21
    target 133
  ]
  edge [
    source 21
    target 134
  ]
  edge [
    source 21
    target 135
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 136
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 138
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 99
  ]
  edge [
    source 25
    target 139
  ]
  edge [
    source 25
    target 140
  ]
  edge [
    source 25
    target 141
  ]
  edge [
    source 25
    target 142
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 143
  ]
  edge [
    source 26
    target 144
  ]
  edge [
    source 26
    target 145
  ]
  edge [
    source 26
    target 146
  ]
  edge [
    source 26
    target 147
  ]
  edge [
    source 26
    target 148
  ]
  edge [
    source 26
    target 149
  ]
  edge [
    source 26
    target 150
  ]
  edge [
    source 26
    target 151
  ]
  edge [
    source 26
    target 152
  ]
  edge [
    source 26
    target 153
  ]
  edge [
    source 26
    target 154
  ]
  edge [
    source 26
    target 155
  ]
  edge [
    source 26
    target 156
  ]
  edge [
    source 26
    target 157
  ]
  edge [
    source 26
    target 158
  ]
  edge [
    source 26
    target 159
  ]
]
