graph [
  maxDegree 31
  minDegree 1
  meanDegree 1.9692307692307693
  density 0.03076923076923077
  graphCliqueNumber 2
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "mireczka"
    origin "text"
  ]
  node [
    id 2
    label "prawdziwy"
    origin "text"
  ]
  node [
    id 3
    label "bohater"
    origin "text"
  ]
  node [
    id 4
    label "nosi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "peleryna"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;ga&#263;"
  ]
  node [
    id 7
    label "zna&#263;"
  ]
  node [
    id 8
    label "troska&#263;_si&#281;"
  ]
  node [
    id 9
    label "zachowywa&#263;"
  ]
  node [
    id 10
    label "chowa&#263;"
  ]
  node [
    id 11
    label "think"
  ]
  node [
    id 12
    label "pilnowa&#263;"
  ]
  node [
    id 13
    label "robi&#263;"
  ]
  node [
    id 14
    label "recall"
  ]
  node [
    id 15
    label "echo"
  ]
  node [
    id 16
    label "take_care"
  ]
  node [
    id 17
    label "szczery"
  ]
  node [
    id 18
    label "naprawd&#281;"
  ]
  node [
    id 19
    label "zgodny"
  ]
  node [
    id 20
    label "naturalny"
  ]
  node [
    id 21
    label "realnie"
  ]
  node [
    id 22
    label "prawdziwie"
  ]
  node [
    id 23
    label "m&#261;dry"
  ]
  node [
    id 24
    label "&#380;ywny"
  ]
  node [
    id 25
    label "podobny"
  ]
  node [
    id 26
    label "bohaterski"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "Zgredek"
  ]
  node [
    id 29
    label "Herkules"
  ]
  node [
    id 30
    label "Casanova"
  ]
  node [
    id 31
    label "Borewicz"
  ]
  node [
    id 32
    label "Don_Juan"
  ]
  node [
    id 33
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 34
    label "Winnetou"
  ]
  node [
    id 35
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 36
    label "Messi"
  ]
  node [
    id 37
    label "Herkules_Poirot"
  ]
  node [
    id 38
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 39
    label "Szwejk"
  ]
  node [
    id 40
    label "Sherlock_Holmes"
  ]
  node [
    id 41
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 42
    label "Hamlet"
  ]
  node [
    id 43
    label "Asterix"
  ]
  node [
    id 44
    label "Quasimodo"
  ]
  node [
    id 45
    label "Don_Kiszot"
  ]
  node [
    id 46
    label "Wallenrod"
  ]
  node [
    id 47
    label "uczestnik"
  ]
  node [
    id 48
    label "&#347;mia&#322;ek"
  ]
  node [
    id 49
    label "Harry_Potter"
  ]
  node [
    id 50
    label "podmiot"
  ]
  node [
    id 51
    label "Achilles"
  ]
  node [
    id 52
    label "Werter"
  ]
  node [
    id 53
    label "Mario"
  ]
  node [
    id 54
    label "posta&#263;"
  ]
  node [
    id 55
    label "str&#243;j"
  ]
  node [
    id 56
    label "mie&#263;"
  ]
  node [
    id 57
    label "wk&#322;ada&#263;"
  ]
  node [
    id 58
    label "przemieszcza&#263;"
  ]
  node [
    id 59
    label "posiada&#263;"
  ]
  node [
    id 60
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 61
    label "wear"
  ]
  node [
    id 62
    label "carry"
  ]
  node [
    id 63
    label "p&#322;aszcz"
  ]
  node [
    id 64
    label "okrycie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
]
