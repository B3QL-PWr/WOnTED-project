graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.0555555555555554
  density 0.05873015873015873
  graphCliqueNumber 3
  node [
    id 0
    label "klasowy"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 2
    label "wjecha&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 4
    label "p&#243;&#322;metek"
    origin "text"
  ]
  node [
    id 5
    label "niejednolity"
  ]
  node [
    id 6
    label "klasowo"
  ]
  node [
    id 7
    label "przychylny"
  ]
  node [
    id 8
    label "wspania&#322;y"
  ]
  node [
    id 9
    label "dok&#322;adnie"
  ]
  node [
    id 10
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 11
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 12
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 13
    label "wkroczy&#263;"
  ]
  node [
    id 14
    label "zacz&#261;&#263;"
  ]
  node [
    id 15
    label "wsun&#261;&#263;_si&#281;"
  ]
  node [
    id 16
    label "skrytykowa&#263;"
  ]
  node [
    id 17
    label "wpa&#347;&#263;"
  ]
  node [
    id 18
    label "powiedzie&#263;"
  ]
  node [
    id 19
    label "doj&#347;&#263;"
  ]
  node [
    id 20
    label "move"
  ]
  node [
    id 21
    label "wsun&#261;&#263;"
  ]
  node [
    id 22
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 23
    label "spell"
  ]
  node [
    id 24
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 25
    label "przekroczy&#263;"
  ]
  node [
    id 26
    label "intervene"
  ]
  node [
    id 27
    label "impreza"
  ]
  node [
    id 28
    label "&#347;rodek"
  ]
  node [
    id 29
    label "Gaba"
  ]
  node [
    id 30
    label "z"
  ]
  node [
    id 31
    label "Dominik"
  ]
  node [
    id 32
    label "Barbara"
  ]
  node [
    id 33
    label "Mateusz"
  ]
  node [
    id 34
    label "Emilia"
  ]
  node [
    id 35
    label "Seweryn"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
]
