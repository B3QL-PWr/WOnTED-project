graph [
  maxDegree 154
  minDegree 1
  meanDegree 1.9745222929936306
  density 0.012657194185856606
  graphCliqueNumber 2
  node [
    id 0
    label "rejon"
    origin "text"
  ]
  node [
    id 1
    label "abzieli&#322;owski"
    origin "text"
  ]
  node [
    id 2
    label "Skandynawia"
  ]
  node [
    id 3
    label "Yorkshire"
  ]
  node [
    id 4
    label "Kaukaz"
  ]
  node [
    id 5
    label "Kaszmir"
  ]
  node [
    id 6
    label "Toskania"
  ]
  node [
    id 7
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 8
    label "&#321;emkowszczyzna"
  ]
  node [
    id 9
    label "obszar"
  ]
  node [
    id 10
    label "Amhara"
  ]
  node [
    id 11
    label "Lombardia"
  ]
  node [
    id 12
    label "Podbeskidzie"
  ]
  node [
    id 13
    label "Kalabria"
  ]
  node [
    id 14
    label "Tyrol"
  ]
  node [
    id 15
    label "Neogea"
  ]
  node [
    id 16
    label "Pamir"
  ]
  node [
    id 17
    label "Lubelszczyzna"
  ]
  node [
    id 18
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 19
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 20
    label "&#379;ywiecczyzna"
  ]
  node [
    id 21
    label "Europa_Wschodnia"
  ]
  node [
    id 22
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 23
    label "Zabajkale"
  ]
  node [
    id 24
    label "Kaszuby"
  ]
  node [
    id 25
    label "Bo&#347;nia"
  ]
  node [
    id 26
    label "Noworosja"
  ]
  node [
    id 27
    label "Ba&#322;kany"
  ]
  node [
    id 28
    label "Antarktyka"
  ]
  node [
    id 29
    label "Anglia"
  ]
  node [
    id 30
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 31
    label "Kielecczyzna"
  ]
  node [
    id 32
    label "Pomorze_Zachodnie"
  ]
  node [
    id 33
    label "Opolskie"
  ]
  node [
    id 34
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 35
    label "Ko&#322;yma"
  ]
  node [
    id 36
    label "Oksytania"
  ]
  node [
    id 37
    label "Arktyka"
  ]
  node [
    id 38
    label "Syjon"
  ]
  node [
    id 39
    label "Kresy_Zachodnie"
  ]
  node [
    id 40
    label "Kociewie"
  ]
  node [
    id 41
    label "Huculszczyzna"
  ]
  node [
    id 42
    label "wsch&#243;d"
  ]
  node [
    id 43
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 44
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 45
    label "Bawaria"
  ]
  node [
    id 46
    label "Rakowice"
  ]
  node [
    id 47
    label "Syberia_Wschodnia"
  ]
  node [
    id 48
    label "Maghreb"
  ]
  node [
    id 49
    label "Bory_Tucholskie"
  ]
  node [
    id 50
    label "Europa_Zachodnia"
  ]
  node [
    id 51
    label "antroposfera"
  ]
  node [
    id 52
    label "Kerala"
  ]
  node [
    id 53
    label "Podhale"
  ]
  node [
    id 54
    label "Kabylia"
  ]
  node [
    id 55
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 56
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 57
    label "Ma&#322;opolska"
  ]
  node [
    id 58
    label "Polesie"
  ]
  node [
    id 59
    label "Liguria"
  ]
  node [
    id 60
    label "&#321;&#243;dzkie"
  ]
  node [
    id 61
    label "Syberia_Zachodnia"
  ]
  node [
    id 62
    label "Notogea"
  ]
  node [
    id 63
    label "Palestyna"
  ]
  node [
    id 64
    label "&#321;&#281;g"
  ]
  node [
    id 65
    label "Bojkowszczyzna"
  ]
  node [
    id 66
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 67
    label "Karaiby"
  ]
  node [
    id 68
    label "S&#261;decczyzna"
  ]
  node [
    id 69
    label "Zab&#322;ocie"
  ]
  node [
    id 70
    label "Sand&#380;ak"
  ]
  node [
    id 71
    label "Nadrenia"
  ]
  node [
    id 72
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 73
    label "Zakarpacie"
  ]
  node [
    id 74
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 75
    label "Zag&#243;rze"
  ]
  node [
    id 76
    label "Andaluzja"
  ]
  node [
    id 77
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 78
    label "Turkiestan"
  ]
  node [
    id 79
    label "Zabu&#380;e"
  ]
  node [
    id 80
    label "Naddniestrze"
  ]
  node [
    id 81
    label "Hercegowina"
  ]
  node [
    id 82
    label "Opolszczyzna"
  ]
  node [
    id 83
    label "Lotaryngia"
  ]
  node [
    id 84
    label "pas_planetoid"
  ]
  node [
    id 85
    label "Afryka_Wschodnia"
  ]
  node [
    id 86
    label "Szlezwik"
  ]
  node [
    id 87
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 88
    label "holarktyka"
  ]
  node [
    id 89
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 90
    label "akrecja"
  ]
  node [
    id 91
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 92
    label "Mazowsze"
  ]
  node [
    id 93
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 94
    label "Afryka_Zachodnia"
  ]
  node [
    id 95
    label "Galicja"
  ]
  node [
    id 96
    label "Szkocja"
  ]
  node [
    id 97
    label "po&#322;udnie"
  ]
  node [
    id 98
    label "Walia"
  ]
  node [
    id 99
    label "Powi&#347;le"
  ]
  node [
    id 100
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 101
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 102
    label "Ruda_Pabianicka"
  ]
  node [
    id 103
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 104
    label "Zamojszczyzna"
  ]
  node [
    id 105
    label "Kujawy"
  ]
  node [
    id 106
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 107
    label "Podlasie"
  ]
  node [
    id 108
    label "Laponia"
  ]
  node [
    id 109
    label "Umbria"
  ]
  node [
    id 110
    label "Mezoameryka"
  ]
  node [
    id 111
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 112
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 113
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 114
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 115
    label "Kurdystan"
  ]
  node [
    id 116
    label "Kampania"
  ]
  node [
    id 117
    label "Armagnac"
  ]
  node [
    id 118
    label "Polinezja"
  ]
  node [
    id 119
    label "Warmia"
  ]
  node [
    id 120
    label "Wielkopolska"
  ]
  node [
    id 121
    label "Kosowo"
  ]
  node [
    id 122
    label "Bordeaux"
  ]
  node [
    id 123
    label "Lauda"
  ]
  node [
    id 124
    label "p&#243;&#322;noc"
  ]
  node [
    id 125
    label "Mazury"
  ]
  node [
    id 126
    label "Podkarpacie"
  ]
  node [
    id 127
    label "Oceania"
  ]
  node [
    id 128
    label "Lasko"
  ]
  node [
    id 129
    label "Amazonia"
  ]
  node [
    id 130
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 131
    label "zach&#243;d"
  ]
  node [
    id 132
    label "Olszanica"
  ]
  node [
    id 133
    label "przestrze&#324;"
  ]
  node [
    id 134
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 135
    label "Kurpie"
  ]
  node [
    id 136
    label "Tonkin"
  ]
  node [
    id 137
    label "Piotrowo"
  ]
  node [
    id 138
    label "Azja_Wschodnia"
  ]
  node [
    id 139
    label "Mikronezja"
  ]
  node [
    id 140
    label "Ukraina_Zachodnia"
  ]
  node [
    id 141
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 142
    label "Turyngia"
  ]
  node [
    id 143
    label "Baszkiria"
  ]
  node [
    id 144
    label "Apulia"
  ]
  node [
    id 145
    label "Pow&#261;zki"
  ]
  node [
    id 146
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 147
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 148
    label "Indochiny"
  ]
  node [
    id 149
    label "Biskupizna"
  ]
  node [
    id 150
    label "Lubuskie"
  ]
  node [
    id 151
    label "Ludwin&#243;w"
  ]
  node [
    id 152
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 153
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 154
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 155
    label "&#1040;&#1073;&#1079;&#1077;&#1083;&#1080;&#1083;&#1086;&#1074;&#1089;&#1082;&#1080;&#1081;"
  ]
  node [
    id 156
    label "&#1088;&#1072;&#1081;&#1086;&#1085;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 155
    target 156
  ]
]
