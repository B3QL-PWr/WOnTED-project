graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.0434782608695654
  density 0.022455805064500716
  graphCliqueNumber 2
  node [
    id 0
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 1
    label "konsultacja"
    origin "text"
  ]
  node [
    id 2
    label "przed"
    origin "text"
  ]
  node [
    id 3
    label "wydanie"
    origin "text"
  ]
  node [
    id 4
    label "decyzja"
    origin "text"
  ]
  node [
    id 5
    label "zgodnie"
    origin "text"
  ]
  node [
    id 6
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 7
    label "robienie"
  ]
  node [
    id 8
    label "przywodzenie"
  ]
  node [
    id 9
    label "prowadzanie"
  ]
  node [
    id 10
    label "ukierunkowywanie"
  ]
  node [
    id 11
    label "kszta&#322;towanie"
  ]
  node [
    id 12
    label "poprowadzenie"
  ]
  node [
    id 13
    label "wprowadzanie"
  ]
  node [
    id 14
    label "dysponowanie"
  ]
  node [
    id 15
    label "przeci&#261;ganie"
  ]
  node [
    id 16
    label "doprowadzanie"
  ]
  node [
    id 17
    label "wprowadzenie"
  ]
  node [
    id 18
    label "eksponowanie"
  ]
  node [
    id 19
    label "oprowadzenie"
  ]
  node [
    id 20
    label "trzymanie"
  ]
  node [
    id 21
    label "ta&#324;czenie"
  ]
  node [
    id 22
    label "przeci&#281;cie"
  ]
  node [
    id 23
    label "przewy&#380;szanie"
  ]
  node [
    id 24
    label "prowadzi&#263;"
  ]
  node [
    id 25
    label "aim"
  ]
  node [
    id 26
    label "czynno&#347;&#263;"
  ]
  node [
    id 27
    label "zwracanie"
  ]
  node [
    id 28
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 29
    label "przecinanie"
  ]
  node [
    id 30
    label "sterowanie"
  ]
  node [
    id 31
    label "drive"
  ]
  node [
    id 32
    label "kre&#347;lenie"
  ]
  node [
    id 33
    label "management"
  ]
  node [
    id 34
    label "dawanie"
  ]
  node [
    id 35
    label "oprowadzanie"
  ]
  node [
    id 36
    label "pozarz&#261;dzanie"
  ]
  node [
    id 37
    label "g&#243;rowanie"
  ]
  node [
    id 38
    label "linia_melodyczna"
  ]
  node [
    id 39
    label "granie"
  ]
  node [
    id 40
    label "doprowadzenie"
  ]
  node [
    id 41
    label "kierowanie"
  ]
  node [
    id 42
    label "zaprowadzanie"
  ]
  node [
    id 43
    label "lead"
  ]
  node [
    id 44
    label "powodowanie"
  ]
  node [
    id 45
    label "krzywa"
  ]
  node [
    id 46
    label "ocena"
  ]
  node [
    id 47
    label "narada"
  ]
  node [
    id 48
    label "porada"
  ]
  node [
    id 49
    label "delivery"
  ]
  node [
    id 50
    label "podanie"
  ]
  node [
    id 51
    label "issue"
  ]
  node [
    id 52
    label "danie"
  ]
  node [
    id 53
    label "rendition"
  ]
  node [
    id 54
    label "egzemplarz"
  ]
  node [
    id 55
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 56
    label "impression"
  ]
  node [
    id 57
    label "odmiana"
  ]
  node [
    id 58
    label "zapach"
  ]
  node [
    id 59
    label "wytworzenie"
  ]
  node [
    id 60
    label "zdarzenie_si&#281;"
  ]
  node [
    id 61
    label "zrobienie"
  ]
  node [
    id 62
    label "ujawnienie"
  ]
  node [
    id 63
    label "reszta"
  ]
  node [
    id 64
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 65
    label "zadenuncjowanie"
  ]
  node [
    id 66
    label "czasopismo"
  ]
  node [
    id 67
    label "urz&#261;dzenie"
  ]
  node [
    id 68
    label "d&#378;wi&#281;k"
  ]
  node [
    id 69
    label "publikacja"
  ]
  node [
    id 70
    label "dokument"
  ]
  node [
    id 71
    label "resolution"
  ]
  node [
    id 72
    label "zdecydowanie"
  ]
  node [
    id 73
    label "wytw&#243;r"
  ]
  node [
    id 74
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 75
    label "jednakowo"
  ]
  node [
    id 76
    label "spokojnie"
  ]
  node [
    id 77
    label "zgodny"
  ]
  node [
    id 78
    label "dobrze"
  ]
  node [
    id 79
    label "zbie&#380;nie"
  ]
  node [
    id 80
    label "towar"
  ]
  node [
    id 81
    label "nag&#322;&#243;wek"
  ]
  node [
    id 82
    label "znak_j&#281;zykowy"
  ]
  node [
    id 83
    label "wyr&#243;b"
  ]
  node [
    id 84
    label "blok"
  ]
  node [
    id 85
    label "line"
  ]
  node [
    id 86
    label "paragraf"
  ]
  node [
    id 87
    label "rodzajnik"
  ]
  node [
    id 88
    label "prawda"
  ]
  node [
    id 89
    label "szkic"
  ]
  node [
    id 90
    label "tekst"
  ]
  node [
    id 91
    label "fragment"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
]
