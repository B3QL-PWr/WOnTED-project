graph [
  maxDegree 38
  minDegree 1
  meanDegree 1.9487179487179487
  density 0.05128205128205128
  graphCliqueNumber 2
  node [
    id 0
    label "akt"
    origin "text"
  ]
  node [
    id 1
    label "certificate"
  ]
  node [
    id 2
    label "erotyka"
  ]
  node [
    id 3
    label "podniecanie"
  ]
  node [
    id 4
    label "wzw&#243;d"
  ]
  node [
    id 5
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 6
    label "rozmna&#380;anie"
  ]
  node [
    id 7
    label "ontologia"
  ]
  node [
    id 8
    label "fascyku&#322;"
  ]
  node [
    id 9
    label "fragment"
  ]
  node [
    id 10
    label "po&#380;&#261;danie"
  ]
  node [
    id 11
    label "imisja"
  ]
  node [
    id 12
    label "po&#380;ycie"
  ]
  node [
    id 13
    label "pozycja_misjonarska"
  ]
  node [
    id 14
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 15
    label "podnieci&#263;"
  ]
  node [
    id 16
    label "podnieca&#263;"
  ]
  node [
    id 17
    label "funkcja"
  ]
  node [
    id 18
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 19
    label "czynno&#347;&#263;"
  ]
  node [
    id 20
    label "urzeczywistnienie"
  ]
  node [
    id 21
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 22
    label "gra_wst&#281;pna"
  ]
  node [
    id 23
    label "scena"
  ]
  node [
    id 24
    label "nago&#347;&#263;"
  ]
  node [
    id 25
    label "wydarzenie"
  ]
  node [
    id 26
    label "poj&#281;cie"
  ]
  node [
    id 27
    label "numer"
  ]
  node [
    id 28
    label "ruch_frykcyjny"
  ]
  node [
    id 29
    label "baraszki"
  ]
  node [
    id 30
    label "dokument"
  ]
  node [
    id 31
    label "na_pieska"
  ]
  node [
    id 32
    label "arystotelizm"
  ]
  node [
    id 33
    label "z&#322;&#261;czenie"
  ]
  node [
    id 34
    label "act"
  ]
  node [
    id 35
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 36
    label "seks"
  ]
  node [
    id 37
    label "zwyczaj"
  ]
  node [
    id 38
    label "podniecenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
]
