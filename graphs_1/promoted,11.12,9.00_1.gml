graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.044943820224719
  density 0.02323799795709908
  graphCliqueNumber 4
  node [
    id 0
    label "fizyk"
    origin "text"
  ]
  node [
    id 1
    label "belgia"
    origin "text"
  ]
  node [
    id 2
    label "stany"
    origin "text"
  ]
  node [
    id 3
    label "zjednoczy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zbudowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "miniaturowy"
    origin "text"
  ]
  node [
    id 6
    label "model"
    origin "text"
  ]
  node [
    id 7
    label "elektrownia"
    origin "text"
  ]
  node [
    id 8
    label "wiatrowy"
    origin "text"
  ]
  node [
    id 9
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "sto"
    origin "text"
  ]
  node [
    id 12
    label "turbina"
    origin "text"
  ]
  node [
    id 13
    label "Doppler"
  ]
  node [
    id 14
    label "Galvani"
  ]
  node [
    id 15
    label "Maxwell"
  ]
  node [
    id 16
    label "William_Nicol"
  ]
  node [
    id 17
    label "Gilbert"
  ]
  node [
    id 18
    label "Weber"
  ]
  node [
    id 19
    label "Pascal"
  ]
  node [
    id 20
    label "Faraday"
  ]
  node [
    id 21
    label "Newton"
  ]
  node [
    id 22
    label "Kartezjusz"
  ]
  node [
    id 23
    label "naukowiec"
  ]
  node [
    id 24
    label "nauczyciel"
  ]
  node [
    id 25
    label "Lorentz"
  ]
  node [
    id 26
    label "Culomb"
  ]
  node [
    id 27
    label "Einstein"
  ]
  node [
    id 28
    label "Biot"
  ]
  node [
    id 29
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 30
    label "consort"
  ]
  node [
    id 31
    label "zaplanowa&#263;"
  ]
  node [
    id 32
    label "establish"
  ]
  node [
    id 33
    label "stworzy&#263;"
  ]
  node [
    id 34
    label "wytworzy&#263;"
  ]
  node [
    id 35
    label "evolve"
  ]
  node [
    id 36
    label "budowla"
  ]
  node [
    id 37
    label "miniaturowo"
  ]
  node [
    id 38
    label "male&#324;ki"
  ]
  node [
    id 39
    label "ma&#322;y"
  ]
  node [
    id 40
    label "typ"
  ]
  node [
    id 41
    label "cz&#322;owiek"
  ]
  node [
    id 42
    label "pozowa&#263;"
  ]
  node [
    id 43
    label "ideal"
  ]
  node [
    id 44
    label "matryca"
  ]
  node [
    id 45
    label "imitacja"
  ]
  node [
    id 46
    label "ruch"
  ]
  node [
    id 47
    label "motif"
  ]
  node [
    id 48
    label "pozowanie"
  ]
  node [
    id 49
    label "wz&#243;r"
  ]
  node [
    id 50
    label "miniatura"
  ]
  node [
    id 51
    label "prezenter"
  ]
  node [
    id 52
    label "facet"
  ]
  node [
    id 53
    label "orygina&#322;"
  ]
  node [
    id 54
    label "mildew"
  ]
  node [
    id 55
    label "spos&#243;b"
  ]
  node [
    id 56
    label "zi&#243;&#322;ko"
  ]
  node [
    id 57
    label "adaptation"
  ]
  node [
    id 58
    label "zak&#322;ad_komunalny"
  ]
  node [
    id 59
    label "render"
  ]
  node [
    id 60
    label "zmienia&#263;"
  ]
  node [
    id 61
    label "zestaw"
  ]
  node [
    id 62
    label "train"
  ]
  node [
    id 63
    label "uk&#322;ada&#263;"
  ]
  node [
    id 64
    label "dzieli&#263;"
  ]
  node [
    id 65
    label "set"
  ]
  node [
    id 66
    label "przywraca&#263;"
  ]
  node [
    id 67
    label "dawa&#263;"
  ]
  node [
    id 68
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 69
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 70
    label "zbiera&#263;"
  ]
  node [
    id 71
    label "convey"
  ]
  node [
    id 72
    label "opracowywa&#263;"
  ]
  node [
    id 73
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 74
    label "publicize"
  ]
  node [
    id 75
    label "przekazywa&#263;"
  ]
  node [
    id 76
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 77
    label "scala&#263;"
  ]
  node [
    id 78
    label "oddawa&#263;"
  ]
  node [
    id 79
    label "maszyna_hydrauliczna"
  ]
  node [
    id 80
    label "turbozesp&#243;&#322;"
  ]
  node [
    id 81
    label "silnik_przep&#322;ywowy"
  ]
  node [
    id 82
    label "stopie&#324;_turbiny"
  ]
  node [
    id 83
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 84
    label "Stany"
  ]
  node [
    id 85
    label "Physical"
  ]
  node [
    id 86
    label "Review"
  ]
  node [
    id 87
    label "Fluids"
  ]
  node [
    id 88
    label "en"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 87
  ]
  edge [
    source 85
    target 88
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 88
  ]
  edge [
    source 87
    target 88
  ]
]
