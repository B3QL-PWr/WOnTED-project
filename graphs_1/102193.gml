graph [
  maxDegree 25
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.07692307692307693
  graphCliqueNumber 2
  node [
    id 0
    label "sygna&#322;"
    origin "text"
  ]
  node [
    id 1
    label "doj&#347;cie"
  ]
  node [
    id 2
    label "przewodzi&#263;"
  ]
  node [
    id 3
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 4
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 5
    label "znak"
  ]
  node [
    id 6
    label "drift"
  ]
  node [
    id 7
    label "medium_transmisyjne"
  ]
  node [
    id 8
    label "doj&#347;&#263;"
  ]
  node [
    id 9
    label "pulsation"
  ]
  node [
    id 10
    label "przekaza&#263;"
  ]
  node [
    id 11
    label "point"
  ]
  node [
    id 12
    label "demodulacja"
  ]
  node [
    id 13
    label "fala"
  ]
  node [
    id 14
    label "zapowied&#378;"
  ]
  node [
    id 15
    label "po&#322;&#261;czenie"
  ]
  node [
    id 16
    label "czynnik"
  ]
  node [
    id 17
    label "aliasing"
  ]
  node [
    id 18
    label "przekazywanie"
  ]
  node [
    id 19
    label "przewodzenie"
  ]
  node [
    id 20
    label "przekazywa&#263;"
  ]
  node [
    id 21
    label "wizja"
  ]
  node [
    id 22
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 23
    label "przekazanie"
  ]
  node [
    id 24
    label "modulacja"
  ]
  node [
    id 25
    label "d&#378;wi&#281;k"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
]
