graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9787234042553192
  density 0.02127659574468085
  graphCliqueNumber 3
  node [
    id 0
    label "budowa"
    origin "text"
  ]
  node [
    id 1
    label "muszy"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "weso&#322;o"
    origin "text"
  ]
  node [
    id 4
    label "s&#322;ysze&#263;"
    origin "text"
  ]
  node [
    id 5
    label "nagranie"
    origin "text"
  ]
  node [
    id 6
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "facebook"
    origin "text"
  ]
  node [
    id 8
    label "przez"
    origin "text"
  ]
  node [
    id 9
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 10
    label "zawodowy"
    origin "text"
  ]
  node [
    id 11
    label "solidaritet"
    origin "text"
  ]
  node [
    id 12
    label "ekte"
    origin "text"
  ]
  node [
    id 13
    label "fagforening"
    origin "text"
  ]
  node [
    id 14
    label "figura"
  ]
  node [
    id 15
    label "wjazd"
  ]
  node [
    id 16
    label "struktura"
  ]
  node [
    id 17
    label "konstrukcja"
  ]
  node [
    id 18
    label "r&#243;w"
  ]
  node [
    id 19
    label "kreacja"
  ]
  node [
    id 20
    label "posesja"
  ]
  node [
    id 21
    label "cecha"
  ]
  node [
    id 22
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 23
    label "organ"
  ]
  node [
    id 24
    label "mechanika"
  ]
  node [
    id 25
    label "zwierz&#281;"
  ]
  node [
    id 26
    label "miejsce_pracy"
  ]
  node [
    id 27
    label "praca"
  ]
  node [
    id 28
    label "constitution"
  ]
  node [
    id 29
    label "si&#281;ga&#263;"
  ]
  node [
    id 30
    label "trwa&#263;"
  ]
  node [
    id 31
    label "obecno&#347;&#263;"
  ]
  node [
    id 32
    label "stan"
  ]
  node [
    id 33
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 34
    label "stand"
  ]
  node [
    id 35
    label "mie&#263;_miejsce"
  ]
  node [
    id 36
    label "uczestniczy&#263;"
  ]
  node [
    id 37
    label "chodzi&#263;"
  ]
  node [
    id 38
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 39
    label "equal"
  ]
  node [
    id 40
    label "dobrze"
  ]
  node [
    id 41
    label "pozytywnie"
  ]
  node [
    id 42
    label "weso&#322;y"
  ]
  node [
    id 43
    label "przyjemnie"
  ]
  node [
    id 44
    label "beztrosko"
  ]
  node [
    id 45
    label "listen"
  ]
  node [
    id 46
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 47
    label "postrzega&#263;"
  ]
  node [
    id 48
    label "s&#322;ycha&#263;"
  ]
  node [
    id 49
    label "read"
  ]
  node [
    id 50
    label "wys&#322;uchanie"
  ]
  node [
    id 51
    label "wytw&#243;r"
  ]
  node [
    id 52
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 53
    label "recording"
  ]
  node [
    id 54
    label "utrwalenie"
  ]
  node [
    id 55
    label "upubliczni&#263;"
  ]
  node [
    id 56
    label "wydawnictwo"
  ]
  node [
    id 57
    label "wprowadzi&#263;"
  ]
  node [
    id 58
    label "picture"
  ]
  node [
    id 59
    label "wall"
  ]
  node [
    id 60
    label "konto"
  ]
  node [
    id 61
    label "odwodnienie"
  ]
  node [
    id 62
    label "konstytucja"
  ]
  node [
    id 63
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 64
    label "substancja_chemiczna"
  ]
  node [
    id 65
    label "bratnia_dusza"
  ]
  node [
    id 66
    label "zwi&#261;zanie"
  ]
  node [
    id 67
    label "lokant"
  ]
  node [
    id 68
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 69
    label "zwi&#261;za&#263;"
  ]
  node [
    id 70
    label "organizacja"
  ]
  node [
    id 71
    label "odwadnia&#263;"
  ]
  node [
    id 72
    label "marriage"
  ]
  node [
    id 73
    label "marketing_afiliacyjny"
  ]
  node [
    id 74
    label "bearing"
  ]
  node [
    id 75
    label "wi&#261;zanie"
  ]
  node [
    id 76
    label "odwadnianie"
  ]
  node [
    id 77
    label "koligacja"
  ]
  node [
    id 78
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 79
    label "odwodni&#263;"
  ]
  node [
    id 80
    label "azeotrop"
  ]
  node [
    id 81
    label "powi&#261;zanie"
  ]
  node [
    id 82
    label "formalny"
  ]
  node [
    id 83
    label "zawodowo"
  ]
  node [
    id 84
    label "zawo&#322;any"
  ]
  node [
    id 85
    label "profesjonalny"
  ]
  node [
    id 86
    label "czadowy"
  ]
  node [
    id 87
    label "fajny"
  ]
  node [
    id 88
    label "fachowy"
  ]
  node [
    id 89
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 90
    label "klawy"
  ]
  node [
    id 91
    label "Solidaritet"
  ]
  node [
    id 92
    label "Ekte"
  ]
  node [
    id 93
    label "Fagforening"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 93
  ]
  edge [
    source 92
    target 93
  ]
]
