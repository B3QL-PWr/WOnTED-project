graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.028225806451613
  density 0.00409742587161942
  graphCliqueNumber 4
  node [
    id 0
    label "przyj&#281;ty"
    origin "text"
  ]
  node [
    id 1
    label "ustawa"
    origin "text"
  ]
  node [
    id 2
    label "the"
    origin "text"
  ]
  node [
    id 3
    label "occupied"
    origin "text"
  ]
  node [
    id 4
    label "territories"
    origin "text"
  ]
  node [
    id 5
    label "bill"
    origin "text"
  ]
  node [
    id 6
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 7
    label "sprawi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "irlandia"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pierwsza"
    origin "text"
  ]
  node [
    id 11
    label "kraj"
    origin "text"
  ]
  node [
    id 12
    label "europa"
    origin "text"
  ]
  node [
    id 13
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 14
    label "zako&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 15
    label "wymiana"
    origin "text"
  ]
  node [
    id 16
    label "handlowy"
    origin "text"
  ]
  node [
    id 17
    label "nielegalny"
    origin "text"
  ]
  node [
    id 18
    label "&#380;ydowski"
    origin "text"
  ]
  node [
    id 19
    label "osiedle"
    origin "text"
  ]
  node [
    id 20
    label "okupowa&#263;"
    origin "text"
  ]
  node [
    id 21
    label "terytorium"
    origin "text"
  ]
  node [
    id 22
    label "palestyna"
    origin "text"
  ]
  node [
    id 23
    label "obowi&#261;zuj&#261;cy"
  ]
  node [
    id 24
    label "znajomy"
  ]
  node [
    id 25
    label "powszechny"
  ]
  node [
    id 26
    label "Karta_Nauczyciela"
  ]
  node [
    id 27
    label "marc&#243;wka"
  ]
  node [
    id 28
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 29
    label "akt"
  ]
  node [
    id 30
    label "przej&#347;&#263;"
  ]
  node [
    id 31
    label "charter"
  ]
  node [
    id 32
    label "przej&#347;cie"
  ]
  node [
    id 33
    label "wyrobi&#263;"
  ]
  node [
    id 34
    label "przygotowa&#263;"
  ]
  node [
    id 35
    label "wzi&#261;&#263;"
  ]
  node [
    id 36
    label "catch"
  ]
  node [
    id 37
    label "spowodowa&#263;"
  ]
  node [
    id 38
    label "frame"
  ]
  node [
    id 39
    label "si&#281;ga&#263;"
  ]
  node [
    id 40
    label "trwa&#263;"
  ]
  node [
    id 41
    label "obecno&#347;&#263;"
  ]
  node [
    id 42
    label "stan"
  ]
  node [
    id 43
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 44
    label "stand"
  ]
  node [
    id 45
    label "mie&#263;_miejsce"
  ]
  node [
    id 46
    label "uczestniczy&#263;"
  ]
  node [
    id 47
    label "chodzi&#263;"
  ]
  node [
    id 48
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 49
    label "equal"
  ]
  node [
    id 50
    label "godzina"
  ]
  node [
    id 51
    label "Skandynawia"
  ]
  node [
    id 52
    label "Rwanda"
  ]
  node [
    id 53
    label "Filipiny"
  ]
  node [
    id 54
    label "Yorkshire"
  ]
  node [
    id 55
    label "Kaukaz"
  ]
  node [
    id 56
    label "Podbeskidzie"
  ]
  node [
    id 57
    label "Toskania"
  ]
  node [
    id 58
    label "&#321;emkowszczyzna"
  ]
  node [
    id 59
    label "obszar"
  ]
  node [
    id 60
    label "Monako"
  ]
  node [
    id 61
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 62
    label "Amhara"
  ]
  node [
    id 63
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 64
    label "Lombardia"
  ]
  node [
    id 65
    label "Korea"
  ]
  node [
    id 66
    label "Kalabria"
  ]
  node [
    id 67
    label "Czarnog&#243;ra"
  ]
  node [
    id 68
    label "Ghana"
  ]
  node [
    id 69
    label "Tyrol"
  ]
  node [
    id 70
    label "Malawi"
  ]
  node [
    id 71
    label "Indonezja"
  ]
  node [
    id 72
    label "Bu&#322;garia"
  ]
  node [
    id 73
    label "Nauru"
  ]
  node [
    id 74
    label "Kenia"
  ]
  node [
    id 75
    label "Pamir"
  ]
  node [
    id 76
    label "Kambod&#380;a"
  ]
  node [
    id 77
    label "Lubelszczyzna"
  ]
  node [
    id 78
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 79
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 80
    label "Mali"
  ]
  node [
    id 81
    label "&#379;ywiecczyzna"
  ]
  node [
    id 82
    label "Austria"
  ]
  node [
    id 83
    label "interior"
  ]
  node [
    id 84
    label "Europa_Wschodnia"
  ]
  node [
    id 85
    label "Armenia"
  ]
  node [
    id 86
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 87
    label "Fid&#380;i"
  ]
  node [
    id 88
    label "Tuwalu"
  ]
  node [
    id 89
    label "Zabajkale"
  ]
  node [
    id 90
    label "Etiopia"
  ]
  node [
    id 91
    label "Malezja"
  ]
  node [
    id 92
    label "Malta"
  ]
  node [
    id 93
    label "Kaszuby"
  ]
  node [
    id 94
    label "Noworosja"
  ]
  node [
    id 95
    label "Bo&#347;nia"
  ]
  node [
    id 96
    label "Tad&#380;ykistan"
  ]
  node [
    id 97
    label "Grenada"
  ]
  node [
    id 98
    label "Ba&#322;kany"
  ]
  node [
    id 99
    label "Wehrlen"
  ]
  node [
    id 100
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 101
    label "Anglia"
  ]
  node [
    id 102
    label "Kielecczyzna"
  ]
  node [
    id 103
    label "Rumunia"
  ]
  node [
    id 104
    label "Pomorze_Zachodnie"
  ]
  node [
    id 105
    label "Maroko"
  ]
  node [
    id 106
    label "Bhutan"
  ]
  node [
    id 107
    label "Opolskie"
  ]
  node [
    id 108
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 109
    label "Ko&#322;yma"
  ]
  node [
    id 110
    label "Oksytania"
  ]
  node [
    id 111
    label "S&#322;owacja"
  ]
  node [
    id 112
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 113
    label "Seszele"
  ]
  node [
    id 114
    label "Syjon"
  ]
  node [
    id 115
    label "Kuwejt"
  ]
  node [
    id 116
    label "Arabia_Saudyjska"
  ]
  node [
    id 117
    label "Kociewie"
  ]
  node [
    id 118
    label "Kanada"
  ]
  node [
    id 119
    label "Ekwador"
  ]
  node [
    id 120
    label "ziemia"
  ]
  node [
    id 121
    label "Japonia"
  ]
  node [
    id 122
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 123
    label "Hiszpania"
  ]
  node [
    id 124
    label "Wyspy_Marshalla"
  ]
  node [
    id 125
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 126
    label "D&#380;ibuti"
  ]
  node [
    id 127
    label "Botswana"
  ]
  node [
    id 128
    label "Huculszczyzna"
  ]
  node [
    id 129
    label "Wietnam"
  ]
  node [
    id 130
    label "Egipt"
  ]
  node [
    id 131
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 132
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 133
    label "Burkina_Faso"
  ]
  node [
    id 134
    label "Bawaria"
  ]
  node [
    id 135
    label "Niemcy"
  ]
  node [
    id 136
    label "Khitai"
  ]
  node [
    id 137
    label "Macedonia"
  ]
  node [
    id 138
    label "Albania"
  ]
  node [
    id 139
    label "Madagaskar"
  ]
  node [
    id 140
    label "Bahrajn"
  ]
  node [
    id 141
    label "Jemen"
  ]
  node [
    id 142
    label "Lesoto"
  ]
  node [
    id 143
    label "Maghreb"
  ]
  node [
    id 144
    label "Samoa"
  ]
  node [
    id 145
    label "Andora"
  ]
  node [
    id 146
    label "Bory_Tucholskie"
  ]
  node [
    id 147
    label "Chiny"
  ]
  node [
    id 148
    label "Europa_Zachodnia"
  ]
  node [
    id 149
    label "Cypr"
  ]
  node [
    id 150
    label "Wielka_Brytania"
  ]
  node [
    id 151
    label "Kerala"
  ]
  node [
    id 152
    label "Podhale"
  ]
  node [
    id 153
    label "Kabylia"
  ]
  node [
    id 154
    label "Ukraina"
  ]
  node [
    id 155
    label "Paragwaj"
  ]
  node [
    id 156
    label "Trynidad_i_Tobago"
  ]
  node [
    id 157
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 158
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 159
    label "Ma&#322;opolska"
  ]
  node [
    id 160
    label "Polesie"
  ]
  node [
    id 161
    label "Liguria"
  ]
  node [
    id 162
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 163
    label "Libia"
  ]
  node [
    id 164
    label "&#321;&#243;dzkie"
  ]
  node [
    id 165
    label "Surinam"
  ]
  node [
    id 166
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 167
    label "Palestyna"
  ]
  node [
    id 168
    label "Nigeria"
  ]
  node [
    id 169
    label "Australia"
  ]
  node [
    id 170
    label "Honduras"
  ]
  node [
    id 171
    label "Bojkowszczyzna"
  ]
  node [
    id 172
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 173
    label "Karaiby"
  ]
  node [
    id 174
    label "Peru"
  ]
  node [
    id 175
    label "USA"
  ]
  node [
    id 176
    label "Bangladesz"
  ]
  node [
    id 177
    label "Kazachstan"
  ]
  node [
    id 178
    label "Nepal"
  ]
  node [
    id 179
    label "Irak"
  ]
  node [
    id 180
    label "Nadrenia"
  ]
  node [
    id 181
    label "Sudan"
  ]
  node [
    id 182
    label "S&#261;decczyzna"
  ]
  node [
    id 183
    label "Sand&#380;ak"
  ]
  node [
    id 184
    label "San_Marino"
  ]
  node [
    id 185
    label "Burundi"
  ]
  node [
    id 186
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 187
    label "Dominikana"
  ]
  node [
    id 188
    label "Komory"
  ]
  node [
    id 189
    label "Zakarpacie"
  ]
  node [
    id 190
    label "Gwatemala"
  ]
  node [
    id 191
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 192
    label "Zag&#243;rze"
  ]
  node [
    id 193
    label "Andaluzja"
  ]
  node [
    id 194
    label "granica_pa&#324;stwa"
  ]
  node [
    id 195
    label "Turkiestan"
  ]
  node [
    id 196
    label "Naddniestrze"
  ]
  node [
    id 197
    label "Hercegowina"
  ]
  node [
    id 198
    label "Brunei"
  ]
  node [
    id 199
    label "Iran"
  ]
  node [
    id 200
    label "jednostka_administracyjna"
  ]
  node [
    id 201
    label "Zimbabwe"
  ]
  node [
    id 202
    label "Namibia"
  ]
  node [
    id 203
    label "Meksyk"
  ]
  node [
    id 204
    label "Opolszczyzna"
  ]
  node [
    id 205
    label "Kamerun"
  ]
  node [
    id 206
    label "Afryka_Wschodnia"
  ]
  node [
    id 207
    label "Szlezwik"
  ]
  node [
    id 208
    label "Lotaryngia"
  ]
  node [
    id 209
    label "Somalia"
  ]
  node [
    id 210
    label "Angola"
  ]
  node [
    id 211
    label "Gabon"
  ]
  node [
    id 212
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 213
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 214
    label "Nowa_Zelandia"
  ]
  node [
    id 215
    label "Mozambik"
  ]
  node [
    id 216
    label "Tunezja"
  ]
  node [
    id 217
    label "Tajwan"
  ]
  node [
    id 218
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 219
    label "Liban"
  ]
  node [
    id 220
    label "Jordania"
  ]
  node [
    id 221
    label "Tonga"
  ]
  node [
    id 222
    label "Czad"
  ]
  node [
    id 223
    label "Gwinea"
  ]
  node [
    id 224
    label "Liberia"
  ]
  node [
    id 225
    label "Belize"
  ]
  node [
    id 226
    label "Mazowsze"
  ]
  node [
    id 227
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 228
    label "Benin"
  ]
  node [
    id 229
    label "&#321;otwa"
  ]
  node [
    id 230
    label "Syria"
  ]
  node [
    id 231
    label "Afryka_Zachodnia"
  ]
  node [
    id 232
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 233
    label "Dominika"
  ]
  node [
    id 234
    label "Antigua_i_Barbuda"
  ]
  node [
    id 235
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 236
    label "Hanower"
  ]
  node [
    id 237
    label "Galicja"
  ]
  node [
    id 238
    label "Szkocja"
  ]
  node [
    id 239
    label "Walia"
  ]
  node [
    id 240
    label "Afganistan"
  ]
  node [
    id 241
    label "W&#322;ochy"
  ]
  node [
    id 242
    label "Kiribati"
  ]
  node [
    id 243
    label "Szwajcaria"
  ]
  node [
    id 244
    label "Powi&#347;le"
  ]
  node [
    id 245
    label "Chorwacja"
  ]
  node [
    id 246
    label "Sahara_Zachodnia"
  ]
  node [
    id 247
    label "Tajlandia"
  ]
  node [
    id 248
    label "Salwador"
  ]
  node [
    id 249
    label "Bahamy"
  ]
  node [
    id 250
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 251
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 252
    label "Zamojszczyzna"
  ]
  node [
    id 253
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 254
    label "S&#322;owenia"
  ]
  node [
    id 255
    label "Gambia"
  ]
  node [
    id 256
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 257
    label "Urugwaj"
  ]
  node [
    id 258
    label "Podlasie"
  ]
  node [
    id 259
    label "Zair"
  ]
  node [
    id 260
    label "Erytrea"
  ]
  node [
    id 261
    label "Laponia"
  ]
  node [
    id 262
    label "Kujawy"
  ]
  node [
    id 263
    label "Umbria"
  ]
  node [
    id 264
    label "Rosja"
  ]
  node [
    id 265
    label "Mauritius"
  ]
  node [
    id 266
    label "Niger"
  ]
  node [
    id 267
    label "Uganda"
  ]
  node [
    id 268
    label "Turkmenistan"
  ]
  node [
    id 269
    label "Turcja"
  ]
  node [
    id 270
    label "Mezoameryka"
  ]
  node [
    id 271
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 272
    label "Irlandia"
  ]
  node [
    id 273
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 274
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 275
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 276
    label "Gwinea_Bissau"
  ]
  node [
    id 277
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 278
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 279
    label "Kurdystan"
  ]
  node [
    id 280
    label "Belgia"
  ]
  node [
    id 281
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 282
    label "Palau"
  ]
  node [
    id 283
    label "Barbados"
  ]
  node [
    id 284
    label "Wenezuela"
  ]
  node [
    id 285
    label "W&#281;gry"
  ]
  node [
    id 286
    label "Chile"
  ]
  node [
    id 287
    label "Argentyna"
  ]
  node [
    id 288
    label "Kolumbia"
  ]
  node [
    id 289
    label "Armagnac"
  ]
  node [
    id 290
    label "Kampania"
  ]
  node [
    id 291
    label "Sierra_Leone"
  ]
  node [
    id 292
    label "Azerbejd&#380;an"
  ]
  node [
    id 293
    label "Kongo"
  ]
  node [
    id 294
    label "Polinezja"
  ]
  node [
    id 295
    label "Warmia"
  ]
  node [
    id 296
    label "Pakistan"
  ]
  node [
    id 297
    label "Liechtenstein"
  ]
  node [
    id 298
    label "Wielkopolska"
  ]
  node [
    id 299
    label "Nikaragua"
  ]
  node [
    id 300
    label "Senegal"
  ]
  node [
    id 301
    label "brzeg"
  ]
  node [
    id 302
    label "Bordeaux"
  ]
  node [
    id 303
    label "Lauda"
  ]
  node [
    id 304
    label "Indie"
  ]
  node [
    id 305
    label "Mazury"
  ]
  node [
    id 306
    label "Suazi"
  ]
  node [
    id 307
    label "Polska"
  ]
  node [
    id 308
    label "Algieria"
  ]
  node [
    id 309
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 310
    label "Jamajka"
  ]
  node [
    id 311
    label "Timor_Wschodni"
  ]
  node [
    id 312
    label "Oceania"
  ]
  node [
    id 313
    label "Kostaryka"
  ]
  node [
    id 314
    label "Lasko"
  ]
  node [
    id 315
    label "Podkarpacie"
  ]
  node [
    id 316
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 317
    label "Kuba"
  ]
  node [
    id 318
    label "Mauretania"
  ]
  node [
    id 319
    label "Amazonia"
  ]
  node [
    id 320
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 321
    label "Portoryko"
  ]
  node [
    id 322
    label "Brazylia"
  ]
  node [
    id 323
    label "Mo&#322;dawia"
  ]
  node [
    id 324
    label "organizacja"
  ]
  node [
    id 325
    label "Litwa"
  ]
  node [
    id 326
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 327
    label "Kirgistan"
  ]
  node [
    id 328
    label "Izrael"
  ]
  node [
    id 329
    label "Grecja"
  ]
  node [
    id 330
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 331
    label "Kurpie"
  ]
  node [
    id 332
    label "Holandia"
  ]
  node [
    id 333
    label "Sri_Lanka"
  ]
  node [
    id 334
    label "Tonkin"
  ]
  node [
    id 335
    label "Katar"
  ]
  node [
    id 336
    label "Azja_Wschodnia"
  ]
  node [
    id 337
    label "Kaszmir"
  ]
  node [
    id 338
    label "Mikronezja"
  ]
  node [
    id 339
    label "Ukraina_Zachodnia"
  ]
  node [
    id 340
    label "Laos"
  ]
  node [
    id 341
    label "Mongolia"
  ]
  node [
    id 342
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 343
    label "Malediwy"
  ]
  node [
    id 344
    label "Zambia"
  ]
  node [
    id 345
    label "Turyngia"
  ]
  node [
    id 346
    label "Tanzania"
  ]
  node [
    id 347
    label "Gujana"
  ]
  node [
    id 348
    label "Apulia"
  ]
  node [
    id 349
    label "Uzbekistan"
  ]
  node [
    id 350
    label "Panama"
  ]
  node [
    id 351
    label "Czechy"
  ]
  node [
    id 352
    label "Gruzja"
  ]
  node [
    id 353
    label "Baszkiria"
  ]
  node [
    id 354
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 355
    label "Francja"
  ]
  node [
    id 356
    label "Serbia"
  ]
  node [
    id 357
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 358
    label "Togo"
  ]
  node [
    id 359
    label "Estonia"
  ]
  node [
    id 360
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 361
    label "Indochiny"
  ]
  node [
    id 362
    label "Boliwia"
  ]
  node [
    id 363
    label "Oman"
  ]
  node [
    id 364
    label "Portugalia"
  ]
  node [
    id 365
    label "Wyspy_Salomona"
  ]
  node [
    id 366
    label "Haiti"
  ]
  node [
    id 367
    label "Luksemburg"
  ]
  node [
    id 368
    label "Lubuskie"
  ]
  node [
    id 369
    label "Biskupizna"
  ]
  node [
    id 370
    label "Birma"
  ]
  node [
    id 371
    label "Rodezja"
  ]
  node [
    id 372
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 373
    label "communicate"
  ]
  node [
    id 374
    label "cause"
  ]
  node [
    id 375
    label "zrezygnowa&#263;"
  ]
  node [
    id 376
    label "wytworzy&#263;"
  ]
  node [
    id 377
    label "przesta&#263;"
  ]
  node [
    id 378
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 379
    label "dispose"
  ]
  node [
    id 380
    label "zrobi&#263;"
  ]
  node [
    id 381
    label "zjawisko_fonetyczne"
  ]
  node [
    id 382
    label "zamiana"
  ]
  node [
    id 383
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 384
    label "implicite"
  ]
  node [
    id 385
    label "ruch"
  ]
  node [
    id 386
    label "handel"
  ]
  node [
    id 387
    label "deal"
  ]
  node [
    id 388
    label "exchange"
  ]
  node [
    id 389
    label "wydarzenie"
  ]
  node [
    id 390
    label "explicite"
  ]
  node [
    id 391
    label "szachy"
  ]
  node [
    id 392
    label "handlowo"
  ]
  node [
    id 393
    label "zdelegalizowanie"
  ]
  node [
    id 394
    label "nieoficjalny"
  ]
  node [
    id 395
    label "delegalizowanie"
  ]
  node [
    id 396
    label "nielegalnie"
  ]
  node [
    id 397
    label "parchaty"
  ]
  node [
    id 398
    label "pejsaty"
  ]
  node [
    id 399
    label "semicki"
  ]
  node [
    id 400
    label "charakterystyczny"
  ]
  node [
    id 401
    label "&#380;ydowsko"
  ]
  node [
    id 402
    label "moszaw"
  ]
  node [
    id 403
    label "G&#243;rce"
  ]
  node [
    id 404
    label "Powsin"
  ]
  node [
    id 405
    label "Kar&#322;owice"
  ]
  node [
    id 406
    label "Rakowiec"
  ]
  node [
    id 407
    label "Dojlidy"
  ]
  node [
    id 408
    label "Horodyszcze"
  ]
  node [
    id 409
    label "Kujbyszewe"
  ]
  node [
    id 410
    label "Kabaty"
  ]
  node [
    id 411
    label "jednostka_osadnicza"
  ]
  node [
    id 412
    label "Ujazd&#243;w"
  ]
  node [
    id 413
    label "Kaw&#281;czyn"
  ]
  node [
    id 414
    label "Siersza"
  ]
  node [
    id 415
    label "Groch&#243;w"
  ]
  node [
    id 416
    label "Paw&#322;owice"
  ]
  node [
    id 417
    label "Bielice"
  ]
  node [
    id 418
    label "siedziba"
  ]
  node [
    id 419
    label "Tarchomin"
  ]
  node [
    id 420
    label "Br&#243;dno"
  ]
  node [
    id 421
    label "Jelcz"
  ]
  node [
    id 422
    label "Mariensztat"
  ]
  node [
    id 423
    label "Falenica"
  ]
  node [
    id 424
    label "Izborsk"
  ]
  node [
    id 425
    label "Wi&#347;niewo"
  ]
  node [
    id 426
    label "Marymont"
  ]
  node [
    id 427
    label "Solec"
  ]
  node [
    id 428
    label "Zakrz&#243;w"
  ]
  node [
    id 429
    label "Wi&#347;niowiec"
  ]
  node [
    id 430
    label "Natolin"
  ]
  node [
    id 431
    label "grupa"
  ]
  node [
    id 432
    label "Grabiszyn"
  ]
  node [
    id 433
    label "Anin"
  ]
  node [
    id 434
    label "Orunia"
  ]
  node [
    id 435
    label "Gronik"
  ]
  node [
    id 436
    label "Boryszew"
  ]
  node [
    id 437
    label "Bogucice"
  ]
  node [
    id 438
    label "&#379;era&#324;"
  ]
  node [
    id 439
    label "zesp&#243;&#322;"
  ]
  node [
    id 440
    label "Jasienica"
  ]
  node [
    id 441
    label "Laskowice_O&#322;awskie"
  ]
  node [
    id 442
    label "Salwator"
  ]
  node [
    id 443
    label "Zerze&#324;"
  ]
  node [
    id 444
    label "M&#322;ociny"
  ]
  node [
    id 445
    label "Branice"
  ]
  node [
    id 446
    label "Chojny"
  ]
  node [
    id 447
    label "Wad&#243;w"
  ]
  node [
    id 448
    label "Miedzeszyn"
  ]
  node [
    id 449
    label "Ok&#281;cie"
  ]
  node [
    id 450
    label "Lewin&#243;w"
  ]
  node [
    id 451
    label "Broch&#243;w"
  ]
  node [
    id 452
    label "Marysin"
  ]
  node [
    id 453
    label "Szack"
  ]
  node [
    id 454
    label "Wielopole"
  ]
  node [
    id 455
    label "Opor&#243;w"
  ]
  node [
    id 456
    label "Osobowice"
  ]
  node [
    id 457
    label "Lubiesz&#243;w"
  ]
  node [
    id 458
    label "&#379;erniki"
  ]
  node [
    id 459
    label "osadnictwo"
  ]
  node [
    id 460
    label "Wojn&#243;w"
  ]
  node [
    id 461
    label "Latycz&#243;w"
  ]
  node [
    id 462
    label "Kortowo"
  ]
  node [
    id 463
    label "Rej&#243;w"
  ]
  node [
    id 464
    label "Arsk"
  ]
  node [
    id 465
    label "&#321;agiewniki"
  ]
  node [
    id 466
    label "Azory"
  ]
  node [
    id 467
    label "Imielin"
  ]
  node [
    id 468
    label "Rataje"
  ]
  node [
    id 469
    label "Nadodrze"
  ]
  node [
    id 470
    label "Szczytniki"
  ]
  node [
    id 471
    label "Gr&#281;ba&#322;&#243;w"
  ]
  node [
    id 472
    label "dzielnica"
  ]
  node [
    id 473
    label "S&#281;polno"
  ]
  node [
    id 474
    label "G&#243;rczyn"
  ]
  node [
    id 475
    label "Szcz&#281;&#347;liwice"
  ]
  node [
    id 476
    label "Zalesie"
  ]
  node [
    id 477
    label "Ochock"
  ]
  node [
    id 478
    label "Gutkowo"
  ]
  node [
    id 479
    label "G&#322;uszyna"
  ]
  node [
    id 480
    label "Le&#347;nica"
  ]
  node [
    id 481
    label "Micha&#322;owo"
  ]
  node [
    id 482
    label "Jelonki"
  ]
  node [
    id 483
    label "Marysin_Wawerski"
  ]
  node [
    id 484
    label "Biskupin"
  ]
  node [
    id 485
    label "Goc&#322;aw"
  ]
  node [
    id 486
    label "Wawrzyszew"
  ]
  node [
    id 487
    label "zajmowa&#263;"
  ]
  node [
    id 488
    label "buntowa&#263;_si&#281;"
  ]
  node [
    id 489
    label "absorb"
  ]
  node [
    id 490
    label "Wile&#324;szczyzna"
  ]
  node [
    id 491
    label "Jukon"
  ]
  node [
    id 492
    label "The"
  ]
  node [
    id 493
    label "Occupied"
  ]
  node [
    id 494
    label "Territories"
  ]
  node [
    id 495
    label "Bill"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 200
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 206
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 208
  ]
  edge [
    source 11
    target 209
  ]
  edge [
    source 11
    target 210
  ]
  edge [
    source 11
    target 211
  ]
  edge [
    source 11
    target 212
  ]
  edge [
    source 11
    target 213
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 219
  ]
  edge [
    source 11
    target 220
  ]
  edge [
    source 11
    target 221
  ]
  edge [
    source 11
    target 222
  ]
  edge [
    source 11
    target 223
  ]
  edge [
    source 11
    target 224
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 226
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 247
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 250
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 290
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 292
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 296
  ]
  edge [
    source 11
    target 297
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 373
  ]
  edge [
    source 14
    target 374
  ]
  edge [
    source 14
    target 375
  ]
  edge [
    source 14
    target 376
  ]
  edge [
    source 14
    target 377
  ]
  edge [
    source 14
    target 378
  ]
  edge [
    source 14
    target 379
  ]
  edge [
    source 14
    target 380
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 382
  ]
  edge [
    source 15
    target 383
  ]
  edge [
    source 15
    target 384
  ]
  edge [
    source 15
    target 385
  ]
  edge [
    source 15
    target 386
  ]
  edge [
    source 15
    target 387
  ]
  edge [
    source 15
    target 388
  ]
  edge [
    source 15
    target 389
  ]
  edge [
    source 15
    target 390
  ]
  edge [
    source 15
    target 391
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 392
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 393
  ]
  edge [
    source 17
    target 394
  ]
  edge [
    source 17
    target 395
  ]
  edge [
    source 17
    target 396
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 397
  ]
  edge [
    source 18
    target 398
  ]
  edge [
    source 18
    target 399
  ]
  edge [
    source 18
    target 400
  ]
  edge [
    source 18
    target 401
  ]
  edge [
    source 18
    target 402
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 403
  ]
  edge [
    source 19
    target 404
  ]
  edge [
    source 19
    target 405
  ]
  edge [
    source 19
    target 406
  ]
  edge [
    source 19
    target 407
  ]
  edge [
    source 19
    target 408
  ]
  edge [
    source 19
    target 409
  ]
  edge [
    source 19
    target 410
  ]
  edge [
    source 19
    target 411
  ]
  edge [
    source 19
    target 412
  ]
  edge [
    source 19
    target 413
  ]
  edge [
    source 19
    target 414
  ]
  edge [
    source 19
    target 415
  ]
  edge [
    source 19
    target 416
  ]
  edge [
    source 19
    target 417
  ]
  edge [
    source 19
    target 418
  ]
  edge [
    source 19
    target 419
  ]
  edge [
    source 19
    target 420
  ]
  edge [
    source 19
    target 421
  ]
  edge [
    source 19
    target 422
  ]
  edge [
    source 19
    target 423
  ]
  edge [
    source 19
    target 424
  ]
  edge [
    source 19
    target 425
  ]
  edge [
    source 19
    target 426
  ]
  edge [
    source 19
    target 427
  ]
  edge [
    source 19
    target 428
  ]
  edge [
    source 19
    target 429
  ]
  edge [
    source 19
    target 430
  ]
  edge [
    source 19
    target 431
  ]
  edge [
    source 19
    target 432
  ]
  edge [
    source 19
    target 433
  ]
  edge [
    source 19
    target 434
  ]
  edge [
    source 19
    target 435
  ]
  edge [
    source 19
    target 436
  ]
  edge [
    source 19
    target 437
  ]
  edge [
    source 19
    target 438
  ]
  edge [
    source 19
    target 439
  ]
  edge [
    source 19
    target 440
  ]
  edge [
    source 19
    target 441
  ]
  edge [
    source 19
    target 442
  ]
  edge [
    source 19
    target 443
  ]
  edge [
    source 19
    target 444
  ]
  edge [
    source 19
    target 445
  ]
  edge [
    source 19
    target 446
  ]
  edge [
    source 19
    target 447
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 448
  ]
  edge [
    source 19
    target 449
  ]
  edge [
    source 19
    target 450
  ]
  edge [
    source 19
    target 451
  ]
  edge [
    source 19
    target 452
  ]
  edge [
    source 19
    target 453
  ]
  edge [
    source 19
    target 454
  ]
  edge [
    source 19
    target 455
  ]
  edge [
    source 19
    target 456
  ]
  edge [
    source 19
    target 457
  ]
  edge [
    source 19
    target 458
  ]
  edge [
    source 19
    target 244
  ]
  edge [
    source 19
    target 459
  ]
  edge [
    source 19
    target 460
  ]
  edge [
    source 19
    target 461
  ]
  edge [
    source 19
    target 462
  ]
  edge [
    source 19
    target 463
  ]
  edge [
    source 19
    target 464
  ]
  edge [
    source 19
    target 465
  ]
  edge [
    source 19
    target 466
  ]
  edge [
    source 19
    target 467
  ]
  edge [
    source 19
    target 468
  ]
  edge [
    source 19
    target 469
  ]
  edge [
    source 19
    target 470
  ]
  edge [
    source 19
    target 471
  ]
  edge [
    source 19
    target 472
  ]
  edge [
    source 19
    target 473
  ]
  edge [
    source 19
    target 474
  ]
  edge [
    source 19
    target 475
  ]
  edge [
    source 19
    target 476
  ]
  edge [
    source 19
    target 477
  ]
  edge [
    source 19
    target 478
  ]
  edge [
    source 19
    target 479
  ]
  edge [
    source 19
    target 480
  ]
  edge [
    source 19
    target 481
  ]
  edge [
    source 19
    target 482
  ]
  edge [
    source 19
    target 483
  ]
  edge [
    source 19
    target 484
  ]
  edge [
    source 19
    target 485
  ]
  edge [
    source 19
    target 486
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 487
  ]
  edge [
    source 20
    target 488
  ]
  edge [
    source 20
    target 489
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 59
  ]
  edge [
    source 21
    target 490
  ]
  edge [
    source 21
    target 118
  ]
  edge [
    source 21
    target 491
  ]
  edge [
    source 492
    target 493
  ]
  edge [
    source 492
    target 494
  ]
  edge [
    source 492
    target 495
  ]
  edge [
    source 493
    target 494
  ]
  edge [
    source 493
    target 495
  ]
  edge [
    source 494
    target 495
  ]
]
