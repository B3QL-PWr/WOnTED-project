graph [
  maxDegree 152
  minDegree 1
  meanDegree 2.100334448160535
  density 0.007048102175035353
  graphCliqueNumber 3
  node [
    id 0
    label "oparcie"
    origin "text"
  ]
  node [
    id 1
    label "zr&#243;&#380;nicowanie"
    origin "text"
  ]
  node [
    id 2
    label "k&#261;t"
    origin "text"
  ]
  node [
    id 3
    label "padanie"
    origin "text"
  ]
  node [
    id 4
    label "promie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "s&#322;oneczny"
    origin "text"
  ]
  node [
    id 6
    label "powierzchnia"
    origin "text"
  ]
  node [
    id 7
    label "ziemia"
    origin "text"
  ]
  node [
    id 8
    label "wyznaczy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 10
    label "strefa"
    origin "text"
  ]
  node [
    id 11
    label "o&#347;wietlenie"
    origin "text"
  ]
  node [
    id 12
    label "jeden"
    origin "text"
  ]
  node [
    id 13
    label "mi&#281;dzyzwrotnikowy"
    origin "text"
  ]
  node [
    id 14
    label "dwa"
    origin "text"
  ]
  node [
    id 15
    label "symetrycznie"
    origin "text"
  ]
  node [
    id 16
    label "po&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 17
    label "umiarkowany"
    origin "text"
  ]
  node [
    id 18
    label "szeroko&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "oko&#322;obiegunowy"
    origin "text"
  ]
  node [
    id 20
    label "back"
  ]
  node [
    id 21
    label "zaczerpni&#281;cie"
  ]
  node [
    id 22
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 23
    label "podstawa"
  ]
  node [
    id 24
    label "anchor"
  ]
  node [
    id 25
    label "podpora"
  ]
  node [
    id 26
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 27
    label "ustawienie"
  ]
  node [
    id 28
    label "zrobienie"
  ]
  node [
    id 29
    label "rozdzielenie"
  ]
  node [
    id 30
    label "diverseness"
  ]
  node [
    id 31
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 32
    label "discrimination"
  ]
  node [
    id 33
    label "bogactwo"
  ]
  node [
    id 34
    label "multikulturalizm"
  ]
  node [
    id 35
    label "cecha"
  ]
  node [
    id 36
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 37
    label "rozproszenie_si&#281;"
  ]
  node [
    id 38
    label "eklektyk"
  ]
  node [
    id 39
    label "podzielenie"
  ]
  node [
    id 40
    label "differentiation"
  ]
  node [
    id 41
    label "nadanie"
  ]
  node [
    id 42
    label "miejsce"
  ]
  node [
    id 43
    label "p&#322;aszczyzna"
  ]
  node [
    id 44
    label "garderoba"
  ]
  node [
    id 45
    label "rami&#281;_k&#261;ta"
  ]
  node [
    id 46
    label "siedziba"
  ]
  node [
    id 47
    label "ubocze"
  ]
  node [
    id 48
    label "obiekt_matematyczny"
  ]
  node [
    id 49
    label "dom"
  ]
  node [
    id 50
    label "spieprzanie_si&#281;"
  ]
  node [
    id 51
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 52
    label "zdychanie"
  ]
  node [
    id 53
    label "zawalenie"
  ]
  node [
    id 54
    label "wyt&#322;uczenie"
  ]
  node [
    id 55
    label "umieranie"
  ]
  node [
    id 56
    label "kozio&#322;kowanie"
  ]
  node [
    id 57
    label "popadanie"
  ]
  node [
    id 58
    label "spill"
  ]
  node [
    id 59
    label "przelecenie"
  ]
  node [
    id 60
    label "dzianie_si&#281;"
  ]
  node [
    id 61
    label "fall"
  ]
  node [
    id 62
    label "odrobina"
  ]
  node [
    id 63
    label "&#347;wiat&#322;o"
  ]
  node [
    id 64
    label "zapowied&#378;"
  ]
  node [
    id 65
    label "wyrostek"
  ]
  node [
    id 66
    label "odcinek"
  ]
  node [
    id 67
    label "pi&#243;rko"
  ]
  node [
    id 68
    label "strumie&#324;"
  ]
  node [
    id 69
    label "rozeta"
  ]
  node [
    id 70
    label "bezchmurny"
  ]
  node [
    id 71
    label "bezdeszczowy"
  ]
  node [
    id 72
    label "s&#322;onecznie"
  ]
  node [
    id 73
    label "jasny"
  ]
  node [
    id 74
    label "fotowoltaiczny"
  ]
  node [
    id 75
    label "pogodny"
  ]
  node [
    id 76
    label "weso&#322;y"
  ]
  node [
    id 77
    label "ciep&#322;y"
  ]
  node [
    id 78
    label "letni"
  ]
  node [
    id 79
    label "capacity"
  ]
  node [
    id 80
    label "obszar"
  ]
  node [
    id 81
    label "zwierciad&#322;o"
  ]
  node [
    id 82
    label "rozmiar"
  ]
  node [
    id 83
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 84
    label "poj&#281;cie"
  ]
  node [
    id 85
    label "plane"
  ]
  node [
    id 86
    label "Skandynawia"
  ]
  node [
    id 87
    label "Yorkshire"
  ]
  node [
    id 88
    label "Kaukaz"
  ]
  node [
    id 89
    label "Kaszmir"
  ]
  node [
    id 90
    label "Toskania"
  ]
  node [
    id 91
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 92
    label "&#321;emkowszczyzna"
  ]
  node [
    id 93
    label "Amhara"
  ]
  node [
    id 94
    label "Lombardia"
  ]
  node [
    id 95
    label "Podbeskidzie"
  ]
  node [
    id 96
    label "Kalabria"
  ]
  node [
    id 97
    label "kort"
  ]
  node [
    id 98
    label "Tyrol"
  ]
  node [
    id 99
    label "Pamir"
  ]
  node [
    id 100
    label "Lubelszczyzna"
  ]
  node [
    id 101
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 102
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 103
    label "&#379;ywiecczyzna"
  ]
  node [
    id 104
    label "ryzosfera"
  ]
  node [
    id 105
    label "Europa_Wschodnia"
  ]
  node [
    id 106
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 107
    label "Zabajkale"
  ]
  node [
    id 108
    label "Kaszuby"
  ]
  node [
    id 109
    label "Bo&#347;nia"
  ]
  node [
    id 110
    label "Noworosja"
  ]
  node [
    id 111
    label "Ba&#322;kany"
  ]
  node [
    id 112
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 113
    label "Anglia"
  ]
  node [
    id 114
    label "Kielecczyzna"
  ]
  node [
    id 115
    label "Pomorze_Zachodnie"
  ]
  node [
    id 116
    label "Opolskie"
  ]
  node [
    id 117
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 118
    label "skorupa_ziemska"
  ]
  node [
    id 119
    label "Ko&#322;yma"
  ]
  node [
    id 120
    label "Oksytania"
  ]
  node [
    id 121
    label "Syjon"
  ]
  node [
    id 122
    label "posadzka"
  ]
  node [
    id 123
    label "pa&#324;stwo"
  ]
  node [
    id 124
    label "Kociewie"
  ]
  node [
    id 125
    label "Huculszczyzna"
  ]
  node [
    id 126
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 127
    label "budynek"
  ]
  node [
    id 128
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 129
    label "Bawaria"
  ]
  node [
    id 130
    label "pomieszczenie"
  ]
  node [
    id 131
    label "pr&#243;chnica"
  ]
  node [
    id 132
    label "glinowanie"
  ]
  node [
    id 133
    label "Maghreb"
  ]
  node [
    id 134
    label "Bory_Tucholskie"
  ]
  node [
    id 135
    label "Europa_Zachodnia"
  ]
  node [
    id 136
    label "Kerala"
  ]
  node [
    id 137
    label "Podhale"
  ]
  node [
    id 138
    label "Kabylia"
  ]
  node [
    id 139
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 140
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 141
    label "Ma&#322;opolska"
  ]
  node [
    id 142
    label "Polesie"
  ]
  node [
    id 143
    label "Liguria"
  ]
  node [
    id 144
    label "&#321;&#243;dzkie"
  ]
  node [
    id 145
    label "geosystem"
  ]
  node [
    id 146
    label "Palestyna"
  ]
  node [
    id 147
    label "Bojkowszczyzna"
  ]
  node [
    id 148
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 149
    label "Karaiby"
  ]
  node [
    id 150
    label "S&#261;decczyzna"
  ]
  node [
    id 151
    label "Sand&#380;ak"
  ]
  node [
    id 152
    label "Nadrenia"
  ]
  node [
    id 153
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 154
    label "Zakarpacie"
  ]
  node [
    id 155
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 156
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 157
    label "Zag&#243;rze"
  ]
  node [
    id 158
    label "Andaluzja"
  ]
  node [
    id 159
    label "Turkiestan"
  ]
  node [
    id 160
    label "Naddniestrze"
  ]
  node [
    id 161
    label "Hercegowina"
  ]
  node [
    id 162
    label "Opolszczyzna"
  ]
  node [
    id 163
    label "jednostka_administracyjna"
  ]
  node [
    id 164
    label "Lotaryngia"
  ]
  node [
    id 165
    label "Afryka_Wschodnia"
  ]
  node [
    id 166
    label "Szlezwik"
  ]
  node [
    id 167
    label "glinowa&#263;"
  ]
  node [
    id 168
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 169
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 170
    label "podglebie"
  ]
  node [
    id 171
    label "Mazowsze"
  ]
  node [
    id 172
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 173
    label "teren"
  ]
  node [
    id 174
    label "Afryka_Zachodnia"
  ]
  node [
    id 175
    label "czynnik_produkcji"
  ]
  node [
    id 176
    label "Galicja"
  ]
  node [
    id 177
    label "Szkocja"
  ]
  node [
    id 178
    label "Walia"
  ]
  node [
    id 179
    label "Powi&#347;le"
  ]
  node [
    id 180
    label "penetrator"
  ]
  node [
    id 181
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 182
    label "kompleks_sorpcyjny"
  ]
  node [
    id 183
    label "Zamojszczyzna"
  ]
  node [
    id 184
    label "Kujawy"
  ]
  node [
    id 185
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 186
    label "Podlasie"
  ]
  node [
    id 187
    label "Laponia"
  ]
  node [
    id 188
    label "Umbria"
  ]
  node [
    id 189
    label "plantowa&#263;"
  ]
  node [
    id 190
    label "Mezoameryka"
  ]
  node [
    id 191
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 192
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 193
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 194
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 195
    label "Kurdystan"
  ]
  node [
    id 196
    label "Kampania"
  ]
  node [
    id 197
    label "Armagnac"
  ]
  node [
    id 198
    label "Polinezja"
  ]
  node [
    id 199
    label "Warmia"
  ]
  node [
    id 200
    label "Wielkopolska"
  ]
  node [
    id 201
    label "litosfera"
  ]
  node [
    id 202
    label "Bordeaux"
  ]
  node [
    id 203
    label "Lauda"
  ]
  node [
    id 204
    label "Mazury"
  ]
  node [
    id 205
    label "Podkarpacie"
  ]
  node [
    id 206
    label "Oceania"
  ]
  node [
    id 207
    label "Lasko"
  ]
  node [
    id 208
    label "Amazonia"
  ]
  node [
    id 209
    label "pojazd"
  ]
  node [
    id 210
    label "glej"
  ]
  node [
    id 211
    label "martwica"
  ]
  node [
    id 212
    label "zapadnia"
  ]
  node [
    id 213
    label "przestrze&#324;"
  ]
  node [
    id 214
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 215
    label "dotleni&#263;"
  ]
  node [
    id 216
    label "Kurpie"
  ]
  node [
    id 217
    label "Tonkin"
  ]
  node [
    id 218
    label "Azja_Wschodnia"
  ]
  node [
    id 219
    label "Mikronezja"
  ]
  node [
    id 220
    label "Ukraina_Zachodnia"
  ]
  node [
    id 221
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 222
    label "Turyngia"
  ]
  node [
    id 223
    label "Baszkiria"
  ]
  node [
    id 224
    label "Apulia"
  ]
  node [
    id 225
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 226
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 227
    label "Indochiny"
  ]
  node [
    id 228
    label "Biskupizna"
  ]
  node [
    id 229
    label "Lubuskie"
  ]
  node [
    id 230
    label "domain"
  ]
  node [
    id 231
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 232
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 233
    label "aim"
  ]
  node [
    id 234
    label "okre&#347;li&#263;"
  ]
  node [
    id 235
    label "wybra&#263;"
  ]
  node [
    id 236
    label "sign"
  ]
  node [
    id 237
    label "position"
  ]
  node [
    id 238
    label "zaznaczy&#263;"
  ]
  node [
    id 239
    label "ustali&#263;"
  ]
  node [
    id 240
    label "set"
  ]
  node [
    id 241
    label "proceed"
  ]
  node [
    id 242
    label "catch"
  ]
  node [
    id 243
    label "pozosta&#263;"
  ]
  node [
    id 244
    label "osta&#263;_si&#281;"
  ]
  node [
    id 245
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 246
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 247
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 248
    label "change"
  ]
  node [
    id 249
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 250
    label "obrona_strefowa"
  ]
  node [
    id 251
    label "czynno&#347;&#263;"
  ]
  node [
    id 252
    label "spowodowanie"
  ]
  node [
    id 253
    label "nat&#281;&#380;enie"
  ]
  node [
    id 254
    label "lighting"
  ]
  node [
    id 255
    label "light"
  ]
  node [
    id 256
    label "instalacja"
  ]
  node [
    id 257
    label "punkt_widzenia"
  ]
  node [
    id 258
    label "o&#347;wietlanie"
  ]
  node [
    id 259
    label "lighter"
  ]
  node [
    id 260
    label "interpretacja"
  ]
  node [
    id 261
    label "kieliszek"
  ]
  node [
    id 262
    label "shot"
  ]
  node [
    id 263
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 264
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 265
    label "jaki&#347;"
  ]
  node [
    id 266
    label "jednolicie"
  ]
  node [
    id 267
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 268
    label "w&#243;dka"
  ]
  node [
    id 269
    label "ten"
  ]
  node [
    id 270
    label "ujednolicenie"
  ]
  node [
    id 271
    label "jednakowy"
  ]
  node [
    id 272
    label "jednocze&#347;nie"
  ]
  node [
    id 273
    label "symetryczny"
  ]
  node [
    id 274
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 275
    label "przygotowa&#263;"
  ]
  node [
    id 276
    label "zmieni&#263;"
  ]
  node [
    id 277
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 278
    label "pokry&#263;"
  ]
  node [
    id 279
    label "pozostawi&#263;"
  ]
  node [
    id 280
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 281
    label "zacz&#261;&#263;"
  ]
  node [
    id 282
    label "znak"
  ]
  node [
    id 283
    label "return"
  ]
  node [
    id 284
    label "stagger"
  ]
  node [
    id 285
    label "raise"
  ]
  node [
    id 286
    label "plant"
  ]
  node [
    id 287
    label "umie&#347;ci&#263;"
  ]
  node [
    id 288
    label "wear"
  ]
  node [
    id 289
    label "zepsu&#263;"
  ]
  node [
    id 290
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 291
    label "wygra&#263;"
  ]
  node [
    id 292
    label "umiarkowanie"
  ]
  node [
    id 293
    label "&#347;rednio"
  ]
  node [
    id 294
    label "ostro&#380;ny"
  ]
  node [
    id 295
    label "rozwa&#380;ny"
  ]
  node [
    id 296
    label "zakres"
  ]
  node [
    id 297
    label "largeness"
  ]
  node [
    id 298
    label "polarny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 262
  ]
  edge [
    source 12
    target 263
  ]
  edge [
    source 12
    target 264
  ]
  edge [
    source 12
    target 265
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 272
  ]
  edge [
    source 15
    target 273
  ]
  edge [
    source 16
    target 274
  ]
  edge [
    source 16
    target 275
  ]
  edge [
    source 16
    target 276
  ]
  edge [
    source 16
    target 277
  ]
  edge [
    source 16
    target 278
  ]
  edge [
    source 16
    target 279
  ]
  edge [
    source 16
    target 280
  ]
  edge [
    source 16
    target 281
  ]
  edge [
    source 16
    target 282
  ]
  edge [
    source 16
    target 283
  ]
  edge [
    source 16
    target 284
  ]
  edge [
    source 16
    target 285
  ]
  edge [
    source 16
    target 286
  ]
  edge [
    source 16
    target 287
  ]
  edge [
    source 16
    target 288
  ]
  edge [
    source 16
    target 289
  ]
  edge [
    source 16
    target 290
  ]
  edge [
    source 16
    target 291
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 292
  ]
  edge [
    source 17
    target 293
  ]
  edge [
    source 17
    target 294
  ]
  edge [
    source 17
    target 295
  ]
  edge [
    source 18
    target 296
  ]
  edge [
    source 18
    target 297
  ]
  edge [
    source 18
    target 82
  ]
  edge [
    source 19
    target 298
  ]
]
