graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.0344827586206895
  density 0.007039732728791314
  graphCliqueNumber 2
  node [
    id 0
    label "szkoda"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 2
    label "straci&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 4
    label "ciotka"
    origin "text"
  ]
  node [
    id 5
    label "tonika"
    origin "text"
  ]
  node [
    id 6
    label "lista"
    origin "text"
  ]
  node [
    id 7
    label "oczekuj&#261;cy"
    origin "text"
  ]
  node [
    id 8
    label "przydzia&#322;"
    origin "text"
  ]
  node [
    id 9
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 10
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 11
    label "czas"
    origin "text"
  ]
  node [
    id 12
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 13
    label "niespokojny"
    origin "text"
  ]
  node [
    id 14
    label "budowa"
    origin "text"
  ]
  node [
    id 15
    label "dom"
    origin "text"
  ]
  node [
    id 16
    label "ryzykowny"
    origin "text"
  ]
  node [
    id 17
    label "wiadomo"
    origin "text"
  ]
  node [
    id 18
    label "by&#263;"
    origin "text"
  ]
  node [
    id 19
    label "przynie&#347;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "przysz&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 22
    label "tonik"
    origin "text"
  ]
  node [
    id 23
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 24
    label "blisko"
    origin "text"
  ]
  node [
    id 25
    label "kopalnia"
    origin "text"
  ]
  node [
    id 26
    label "te&#380;"
    origin "text"
  ]
  node [
    id 27
    label "si&#281;"
    origin "text"
  ]
  node [
    id 28
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 29
    label "zofija"
    origin "text"
  ]
  node [
    id 30
    label "matka"
    origin "text"
  ]
  node [
    id 31
    label "aniela"
    origin "text"
  ]
  node [
    id 32
    label "naciska&#263;"
    origin "text"
  ]
  node [
    id 33
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 34
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 35
    label "swoje"
    origin "text"
  ]
  node [
    id 36
    label "pole"
  ]
  node [
    id 37
    label "commiseration"
  ]
  node [
    id 38
    label "czu&#263;"
  ]
  node [
    id 39
    label "zniszczenie"
  ]
  node [
    id 40
    label "niepowodzenie"
  ]
  node [
    id 41
    label "ubytek"
  ]
  node [
    id 42
    label "&#380;erowisko"
  ]
  node [
    id 43
    label "szwank"
  ]
  node [
    id 44
    label "omin&#261;&#263;"
  ]
  node [
    id 45
    label "spowodowa&#263;"
  ]
  node [
    id 46
    label "leave_office"
  ]
  node [
    id 47
    label "forfeit"
  ]
  node [
    id 48
    label "stracenie"
  ]
  node [
    id 49
    label "execute"
  ]
  node [
    id 50
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 51
    label "zabi&#263;"
  ]
  node [
    id 52
    label "wytraci&#263;"
  ]
  node [
    id 53
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 54
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 55
    label "przegra&#263;"
  ]
  node [
    id 56
    label "waste"
  ]
  node [
    id 57
    label "stanie"
  ]
  node [
    id 58
    label "przebywanie"
  ]
  node [
    id 59
    label "panowanie"
  ]
  node [
    id 60
    label "zajmowanie"
  ]
  node [
    id 61
    label "pomieszkanie"
  ]
  node [
    id 62
    label "adjustment"
  ]
  node [
    id 63
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 64
    label "lokal"
  ]
  node [
    id 65
    label "kwadrat"
  ]
  node [
    id 66
    label "animation"
  ]
  node [
    id 67
    label "krewna"
  ]
  node [
    id 68
    label "miesi&#261;czka"
  ]
  node [
    id 69
    label "tourist"
  ]
  node [
    id 70
    label "ciotczysko"
  ]
  node [
    id 71
    label "system_dur-moll"
  ]
  node [
    id 72
    label "tr&#243;jd&#378;wi&#281;k"
  ]
  node [
    id 73
    label "gama"
  ]
  node [
    id 74
    label "tonic"
  ]
  node [
    id 75
    label "d&#378;wi&#281;k"
  ]
  node [
    id 76
    label "wyliczanka"
  ]
  node [
    id 77
    label "catalog"
  ]
  node [
    id 78
    label "stock"
  ]
  node [
    id 79
    label "figurowa&#263;"
  ]
  node [
    id 80
    label "zbi&#243;r"
  ]
  node [
    id 81
    label "book"
  ]
  node [
    id 82
    label "pozycja"
  ]
  node [
    id 83
    label "tekst"
  ]
  node [
    id 84
    label "sumariusz"
  ]
  node [
    id 85
    label "czynno&#347;&#263;"
  ]
  node [
    id 86
    label "za&#347;wiadczenie"
  ]
  node [
    id 87
    label "assignment"
  ]
  node [
    id 88
    label "towar"
  ]
  node [
    id 89
    label "partnerka"
  ]
  node [
    id 90
    label "daleki"
  ]
  node [
    id 91
    label "d&#322;ugo"
  ]
  node [
    id 92
    label "ruch"
  ]
  node [
    id 93
    label "czasokres"
  ]
  node [
    id 94
    label "trawienie"
  ]
  node [
    id 95
    label "kategoria_gramatyczna"
  ]
  node [
    id 96
    label "period"
  ]
  node [
    id 97
    label "odczyt"
  ]
  node [
    id 98
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 99
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 100
    label "chwila"
  ]
  node [
    id 101
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 102
    label "poprzedzenie"
  ]
  node [
    id 103
    label "koniugacja"
  ]
  node [
    id 104
    label "dzieje"
  ]
  node [
    id 105
    label "poprzedzi&#263;"
  ]
  node [
    id 106
    label "przep&#322;ywanie"
  ]
  node [
    id 107
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 108
    label "odwlekanie_si&#281;"
  ]
  node [
    id 109
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 110
    label "Zeitgeist"
  ]
  node [
    id 111
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 112
    label "okres_czasu"
  ]
  node [
    id 113
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 114
    label "pochodzi&#263;"
  ]
  node [
    id 115
    label "schy&#322;ek"
  ]
  node [
    id 116
    label "czwarty_wymiar"
  ]
  node [
    id 117
    label "chronometria"
  ]
  node [
    id 118
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 119
    label "poprzedzanie"
  ]
  node [
    id 120
    label "pogoda"
  ]
  node [
    id 121
    label "zegar"
  ]
  node [
    id 122
    label "pochodzenie"
  ]
  node [
    id 123
    label "poprzedza&#263;"
  ]
  node [
    id 124
    label "trawi&#263;"
  ]
  node [
    id 125
    label "time_period"
  ]
  node [
    id 126
    label "rachuba_czasu"
  ]
  node [
    id 127
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 128
    label "czasoprzestrze&#324;"
  ]
  node [
    id 129
    label "laba"
  ]
  node [
    id 130
    label "dawny"
  ]
  node [
    id 131
    label "rozw&#243;d"
  ]
  node [
    id 132
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 133
    label "eksprezydent"
  ]
  node [
    id 134
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 135
    label "partner"
  ]
  node [
    id 136
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 137
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 138
    label "wcze&#347;niejszy"
  ]
  node [
    id 139
    label "niespokojnie"
  ]
  node [
    id 140
    label "nerwowo"
  ]
  node [
    id 141
    label "figura"
  ]
  node [
    id 142
    label "wjazd"
  ]
  node [
    id 143
    label "struktura"
  ]
  node [
    id 144
    label "konstrukcja"
  ]
  node [
    id 145
    label "r&#243;w"
  ]
  node [
    id 146
    label "kreacja"
  ]
  node [
    id 147
    label "posesja"
  ]
  node [
    id 148
    label "cecha"
  ]
  node [
    id 149
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 150
    label "organ"
  ]
  node [
    id 151
    label "mechanika"
  ]
  node [
    id 152
    label "zwierz&#281;"
  ]
  node [
    id 153
    label "miejsce_pracy"
  ]
  node [
    id 154
    label "praca"
  ]
  node [
    id 155
    label "constitution"
  ]
  node [
    id 156
    label "garderoba"
  ]
  node [
    id 157
    label "wiecha"
  ]
  node [
    id 158
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 159
    label "grupa"
  ]
  node [
    id 160
    label "budynek"
  ]
  node [
    id 161
    label "fratria"
  ]
  node [
    id 162
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 163
    label "poj&#281;cie"
  ]
  node [
    id 164
    label "rodzina"
  ]
  node [
    id 165
    label "substancja_mieszkaniowa"
  ]
  node [
    id 166
    label "instytucja"
  ]
  node [
    id 167
    label "dom_rodzinny"
  ]
  node [
    id 168
    label "stead"
  ]
  node [
    id 169
    label "siedziba"
  ]
  node [
    id 170
    label "ryzykownie"
  ]
  node [
    id 171
    label "niebezpieczny"
  ]
  node [
    id 172
    label "si&#281;ga&#263;"
  ]
  node [
    id 173
    label "trwa&#263;"
  ]
  node [
    id 174
    label "obecno&#347;&#263;"
  ]
  node [
    id 175
    label "stan"
  ]
  node [
    id 176
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 177
    label "stand"
  ]
  node [
    id 178
    label "mie&#263;_miejsce"
  ]
  node [
    id 179
    label "uczestniczy&#263;"
  ]
  node [
    id 180
    label "chodzi&#263;"
  ]
  node [
    id 181
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 182
    label "equal"
  ]
  node [
    id 183
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 184
    label "da&#263;"
  ]
  node [
    id 185
    label "ponie&#347;&#263;"
  ]
  node [
    id 186
    label "get"
  ]
  node [
    id 187
    label "przytacha&#263;"
  ]
  node [
    id 188
    label "increase"
  ]
  node [
    id 189
    label "doda&#263;"
  ]
  node [
    id 190
    label "zanie&#347;&#263;"
  ]
  node [
    id 191
    label "poda&#263;"
  ]
  node [
    id 192
    label "carry"
  ]
  node [
    id 193
    label "jutro"
  ]
  node [
    id 194
    label "cel"
  ]
  node [
    id 195
    label "nap&#243;j_gazowany"
  ]
  node [
    id 196
    label "kosmetyk"
  ]
  node [
    id 197
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 198
    label "ciecz"
  ]
  node [
    id 199
    label "proszek"
  ]
  node [
    id 200
    label "dok&#322;adnie"
  ]
  node [
    id 201
    label "bliski"
  ]
  node [
    id 202
    label "silnie"
  ]
  node [
    id 203
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 204
    label "hala"
  ]
  node [
    id 205
    label "klatka"
  ]
  node [
    id 206
    label "g&#243;rnik"
  ]
  node [
    id 207
    label "gmina_g&#243;rnicza"
  ]
  node [
    id 208
    label "zag&#322;&#281;bie"
  ]
  node [
    id 209
    label "bicie"
  ]
  node [
    id 210
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 211
    label "lutnioci&#261;g"
  ]
  node [
    id 212
    label "w&#281;giel_kopalny"
  ]
  node [
    id 213
    label "cechownia"
  ]
  node [
    id 214
    label "mina"
  ]
  node [
    id 215
    label "za&#322;adownia"
  ]
  node [
    id 216
    label "ucinka"
  ]
  node [
    id 217
    label "wyrobisko"
  ]
  node [
    id 218
    label "report"
  ]
  node [
    id 219
    label "dodawa&#263;"
  ]
  node [
    id 220
    label "wymienia&#263;"
  ]
  node [
    id 221
    label "okre&#347;la&#263;"
  ]
  node [
    id 222
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 223
    label "dyskalkulia"
  ]
  node [
    id 224
    label "wynagrodzenie"
  ]
  node [
    id 225
    label "admit"
  ]
  node [
    id 226
    label "osi&#261;ga&#263;"
  ]
  node [
    id 227
    label "wyznacza&#263;"
  ]
  node [
    id 228
    label "posiada&#263;"
  ]
  node [
    id 229
    label "mierzy&#263;"
  ]
  node [
    id 230
    label "odlicza&#263;"
  ]
  node [
    id 231
    label "bra&#263;"
  ]
  node [
    id 232
    label "wycenia&#263;"
  ]
  node [
    id 233
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 234
    label "rachowa&#263;"
  ]
  node [
    id 235
    label "tell"
  ]
  node [
    id 236
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 237
    label "policza&#263;"
  ]
  node [
    id 238
    label "count"
  ]
  node [
    id 239
    label "Matka_Boska"
  ]
  node [
    id 240
    label "matka_zast&#281;pcza"
  ]
  node [
    id 241
    label "stara"
  ]
  node [
    id 242
    label "rodzic"
  ]
  node [
    id 243
    label "matczysko"
  ]
  node [
    id 244
    label "ro&#347;lina"
  ]
  node [
    id 245
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 246
    label "gracz"
  ]
  node [
    id 247
    label "zawodnik"
  ]
  node [
    id 248
    label "macierz"
  ]
  node [
    id 249
    label "owad"
  ]
  node [
    id 250
    label "przyczyna"
  ]
  node [
    id 251
    label "macocha"
  ]
  node [
    id 252
    label "dwa_ognie"
  ]
  node [
    id 253
    label "staruszka"
  ]
  node [
    id 254
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 255
    label "rozsadnik"
  ]
  node [
    id 256
    label "zakonnica"
  ]
  node [
    id 257
    label "obiekt"
  ]
  node [
    id 258
    label "samica"
  ]
  node [
    id 259
    label "przodkini"
  ]
  node [
    id 260
    label "rodzice"
  ]
  node [
    id 261
    label "crowd"
  ]
  node [
    id 262
    label "napierdziela&#263;"
  ]
  node [
    id 263
    label "wk&#322;ada&#263;"
  ]
  node [
    id 264
    label "rush"
  ]
  node [
    id 265
    label "force"
  ]
  node [
    id 266
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 267
    label "przekonywa&#263;"
  ]
  node [
    id 268
    label "opu&#347;ci&#263;"
  ]
  node [
    id 269
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 270
    label "proceed"
  ]
  node [
    id 271
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 272
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 273
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 274
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 275
    label "zacz&#261;&#263;"
  ]
  node [
    id 276
    label "zmieni&#263;"
  ]
  node [
    id 277
    label "zosta&#263;"
  ]
  node [
    id 278
    label "sail"
  ]
  node [
    id 279
    label "leave"
  ]
  node [
    id 280
    label "uda&#263;_si&#281;"
  ]
  node [
    id 281
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 282
    label "zrobi&#263;"
  ]
  node [
    id 283
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 284
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 285
    label "przyj&#261;&#263;"
  ]
  node [
    id 286
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 287
    label "become"
  ]
  node [
    id 288
    label "play_along"
  ]
  node [
    id 289
    label "travel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 24
    target 201
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 153
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 218
  ]
  edge [
    source 28
    target 219
  ]
  edge [
    source 28
    target 220
  ]
  edge [
    source 28
    target 221
  ]
  edge [
    source 28
    target 222
  ]
  edge [
    source 28
    target 223
  ]
  edge [
    source 28
    target 224
  ]
  edge [
    source 28
    target 225
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 28
    target 227
  ]
  edge [
    source 28
    target 228
  ]
  edge [
    source 28
    target 229
  ]
  edge [
    source 28
    target 230
  ]
  edge [
    source 28
    target 231
  ]
  edge [
    source 28
    target 232
  ]
  edge [
    source 28
    target 233
  ]
  edge [
    source 28
    target 234
  ]
  edge [
    source 28
    target 235
  ]
  edge [
    source 28
    target 236
  ]
  edge [
    source 28
    target 237
  ]
  edge [
    source 28
    target 238
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 239
  ]
  edge [
    source 30
    target 240
  ]
  edge [
    source 30
    target 241
  ]
  edge [
    source 30
    target 242
  ]
  edge [
    source 30
    target 243
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 30
    target 245
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 247
  ]
  edge [
    source 30
    target 248
  ]
  edge [
    source 30
    target 249
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 259
  ]
  edge [
    source 30
    target 260
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 261
  ]
  edge [
    source 32
    target 262
  ]
  edge [
    source 32
    target 263
  ]
  edge [
    source 32
    target 264
  ]
  edge [
    source 32
    target 265
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 268
  ]
  edge [
    source 34
    target 269
  ]
  edge [
    source 34
    target 270
  ]
  edge [
    source 34
    target 271
  ]
  edge [
    source 34
    target 272
  ]
  edge [
    source 34
    target 273
  ]
  edge [
    source 34
    target 274
  ]
  edge [
    source 34
    target 275
  ]
  edge [
    source 34
    target 276
  ]
  edge [
    source 34
    target 277
  ]
  edge [
    source 34
    target 278
  ]
  edge [
    source 34
    target 279
  ]
  edge [
    source 34
    target 280
  ]
  edge [
    source 34
    target 281
  ]
  edge [
    source 34
    target 53
  ]
  edge [
    source 34
    target 282
  ]
  edge [
    source 34
    target 283
  ]
  edge [
    source 34
    target 284
  ]
  edge [
    source 34
    target 285
  ]
  edge [
    source 34
    target 286
  ]
  edge [
    source 34
    target 287
  ]
  edge [
    source 34
    target 288
  ]
  edge [
    source 34
    target 289
  ]
]
