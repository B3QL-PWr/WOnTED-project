graph [
  maxDegree 70
  minDegree 1
  meanDegree 2.1186440677966103
  density 0.006001824554664618
  graphCliqueNumber 3
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "zobaczy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nie"
    origin "text"
  ]
  node [
    id 3
    label "p&#322;aka&#263;"
    origin "text"
  ]
  node [
    id 4
    label "powiada&#263;"
    origin "text"
  ]
  node [
    id 5
    label "judce"
    origin "text"
  ]
  node [
    id 6
    label "rycze&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ten"
    origin "text"
  ]
  node [
    id 8
    label "w&#243;&#322;"
    origin "text"
  ]
  node [
    id 9
    label "siostra"
    origin "text"
  ]
  node [
    id 10
    label "zbieg&#322;y"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "hoffowie"
    origin "text"
  ]
  node [
    id 13
    label "czeladnik"
    origin "text"
  ]
  node [
    id 14
    label "terminator"
    origin "text"
  ]
  node [
    id 15
    label "daleko"
    origin "text"
  ]
  node [
    id 16
    label "kl&#261;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "skar&#380;y&#263;"
    origin "text"
  ]
  node [
    id 18
    label "zabi&#263;"
    origin "text"
  ]
  node [
    id 19
    label "znowu"
    origin "text"
  ]
  node [
    id 20
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wmawia&#263;"
    origin "text"
  ]
  node [
    id 22
    label "sam"
    origin "text"
  ]
  node [
    id 23
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 24
    label "sko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 25
    label "tema"
    origin "text"
  ]
  node [
    id 26
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 27
    label "zeszed&#322;"
    origin "text"
  ]
  node [
    id 28
    label "nieboszczka"
    origin "text"
  ]
  node [
    id 29
    label "pochowa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "nasz"
    origin "text"
  ]
  node [
    id 31
    label "ch&#322;opak"
    origin "text"
  ]
  node [
    id 32
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 33
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "profesor"
  ]
  node [
    id 36
    label "kszta&#322;ciciel"
  ]
  node [
    id 37
    label "jegomo&#347;&#263;"
  ]
  node [
    id 38
    label "zwrot"
  ]
  node [
    id 39
    label "pracodawca"
  ]
  node [
    id 40
    label "rz&#261;dzenie"
  ]
  node [
    id 41
    label "m&#261;&#380;"
  ]
  node [
    id 42
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 43
    label "ch&#322;opina"
  ]
  node [
    id 44
    label "bratek"
  ]
  node [
    id 45
    label "opiekun"
  ]
  node [
    id 46
    label "doros&#322;y"
  ]
  node [
    id 47
    label "preceptor"
  ]
  node [
    id 48
    label "Midas"
  ]
  node [
    id 49
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 50
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 51
    label "murza"
  ]
  node [
    id 52
    label "ojciec"
  ]
  node [
    id 53
    label "androlog"
  ]
  node [
    id 54
    label "pupil"
  ]
  node [
    id 55
    label "efendi"
  ]
  node [
    id 56
    label "nabab"
  ]
  node [
    id 57
    label "w&#322;odarz"
  ]
  node [
    id 58
    label "szkolnik"
  ]
  node [
    id 59
    label "pedagog"
  ]
  node [
    id 60
    label "popularyzator"
  ]
  node [
    id 61
    label "andropauza"
  ]
  node [
    id 62
    label "gra_w_karty"
  ]
  node [
    id 63
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 64
    label "Mieszko_I"
  ]
  node [
    id 65
    label "bogaty"
  ]
  node [
    id 66
    label "samiec"
  ]
  node [
    id 67
    label "przyw&#243;dca"
  ]
  node [
    id 68
    label "pa&#324;stwo"
  ]
  node [
    id 69
    label "belfer"
  ]
  node [
    id 70
    label "spotka&#263;"
  ]
  node [
    id 71
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 72
    label "peek"
  ]
  node [
    id 73
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 74
    label "spoziera&#263;"
  ]
  node [
    id 75
    label "obejrze&#263;"
  ]
  node [
    id 76
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 77
    label "postrzec"
  ]
  node [
    id 78
    label "spot"
  ]
  node [
    id 79
    label "go_steady"
  ]
  node [
    id 80
    label "pojrze&#263;"
  ]
  node [
    id 81
    label "popatrze&#263;"
  ]
  node [
    id 82
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 83
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 84
    label "see"
  ]
  node [
    id 85
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 86
    label "dostrzec"
  ]
  node [
    id 87
    label "zinterpretowa&#263;"
  ]
  node [
    id 88
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 89
    label "znale&#378;&#263;"
  ]
  node [
    id 90
    label "cognizance"
  ]
  node [
    id 91
    label "sprzeciw"
  ]
  node [
    id 92
    label "wy&#263;"
  ]
  node [
    id 93
    label "cudowa&#263;"
  ]
  node [
    id 94
    label "reagowa&#263;"
  ]
  node [
    id 95
    label "snivel"
  ]
  node [
    id 96
    label "backfire"
  ]
  node [
    id 97
    label "robi&#263;"
  ]
  node [
    id 98
    label "narzeka&#263;"
  ]
  node [
    id 99
    label "szkoda"
  ]
  node [
    id 100
    label "sorrow"
  ]
  node [
    id 101
    label "wydziela&#263;"
  ]
  node [
    id 102
    label "pokrywa&#263;_si&#281;"
  ]
  node [
    id 103
    label "sting"
  ]
  node [
    id 104
    label "mawia&#263;"
  ]
  node [
    id 105
    label "m&#243;wi&#263;"
  ]
  node [
    id 106
    label "informowa&#263;"
  ]
  node [
    id 107
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 108
    label "hula&#263;"
  ]
  node [
    id 109
    label "mika&#263;"
  ]
  node [
    id 110
    label "zia&#263;"
  ]
  node [
    id 111
    label "&#347;mia&#263;_si&#281;"
  ]
  node [
    id 112
    label "bawl"
  ]
  node [
    id 113
    label "wrzeszcze&#263;"
  ]
  node [
    id 114
    label "hucze&#263;"
  ]
  node [
    id 115
    label "okre&#347;lony"
  ]
  node [
    id 116
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 117
    label "bukranion"
  ]
  node [
    id 118
    label "czaban"
  ]
  node [
    id 119
    label "kastrat"
  ]
  node [
    id 120
    label "pracownik"
  ]
  node [
    id 121
    label "bydl&#281;"
  ]
  node [
    id 122
    label "kornet"
  ]
  node [
    id 123
    label "wyznawczyni"
  ]
  node [
    id 124
    label "pigu&#322;a"
  ]
  node [
    id 125
    label "rodze&#324;stwo"
  ]
  node [
    id 126
    label "czepek"
  ]
  node [
    id 127
    label "krewna"
  ]
  node [
    id 128
    label "siostrzyca"
  ]
  node [
    id 129
    label "pingwin"
  ]
  node [
    id 130
    label "siora"
  ]
  node [
    id 131
    label "anestetysta"
  ]
  node [
    id 132
    label "rzemie&#347;lniczek"
  ]
  node [
    id 133
    label "rzemie&#347;lnik"
  ]
  node [
    id 134
    label "ucze&#324;"
  ]
  node [
    id 135
    label "dawno"
  ]
  node [
    id 136
    label "nisko"
  ]
  node [
    id 137
    label "nieobecnie"
  ]
  node [
    id 138
    label "daleki"
  ]
  node [
    id 139
    label "het"
  ]
  node [
    id 140
    label "wysoko"
  ]
  node [
    id 141
    label "du&#380;o"
  ]
  node [
    id 142
    label "znacznie"
  ]
  node [
    id 143
    label "g&#322;&#281;boko"
  ]
  node [
    id 144
    label "blu&#378;ni&#263;"
  ]
  node [
    id 145
    label "curse"
  ]
  node [
    id 146
    label "oskar&#380;a&#263;"
  ]
  node [
    id 147
    label "sobaczy&#263;"
  ]
  node [
    id 148
    label "mistreat"
  ]
  node [
    id 149
    label "donosi&#263;"
  ]
  node [
    id 150
    label "spill_the_beans"
  ]
  node [
    id 151
    label "chatter"
  ]
  node [
    id 152
    label "powodowa&#263;"
  ]
  node [
    id 153
    label "skrzywdzi&#263;"
  ]
  node [
    id 154
    label "kill"
  ]
  node [
    id 155
    label "poder&#380;n&#261;&#263;"
  ]
  node [
    id 156
    label "skarci&#263;"
  ]
  node [
    id 157
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 158
    label "przybi&#263;"
  ]
  node [
    id 159
    label "zakry&#263;"
  ]
  node [
    id 160
    label "rozbroi&#263;"
  ]
  node [
    id 161
    label "zmordowa&#263;"
  ]
  node [
    id 162
    label "break"
  ]
  node [
    id 163
    label "zastrzeli&#263;"
  ]
  node [
    id 164
    label "u&#347;mierci&#263;"
  ]
  node [
    id 165
    label "zadzwoni&#263;"
  ]
  node [
    id 166
    label "pomacha&#263;"
  ]
  node [
    id 167
    label "po&#322;o&#380;y&#263;_na_kolana"
  ]
  node [
    id 168
    label "zwalczy&#263;"
  ]
  node [
    id 169
    label "zniszczy&#263;"
  ]
  node [
    id 170
    label "os&#322;oni&#263;"
  ]
  node [
    id 171
    label "dispatch"
  ]
  node [
    id 172
    label "uderzy&#263;"
  ]
  node [
    id 173
    label "zapulsowa&#263;"
  ]
  node [
    id 174
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 175
    label "skrzywi&#263;"
  ]
  node [
    id 176
    label "zako&#324;czy&#263;"
  ]
  node [
    id 177
    label "zbi&#263;"
  ]
  node [
    id 178
    label "cause"
  ]
  node [
    id 179
    label "introduce"
  ]
  node [
    id 180
    label "begin"
  ]
  node [
    id 181
    label "odj&#261;&#263;"
  ]
  node [
    id 182
    label "post&#261;pi&#263;"
  ]
  node [
    id 183
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 184
    label "do"
  ]
  node [
    id 185
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 186
    label "zrobi&#263;"
  ]
  node [
    id 187
    label "przekonywa&#263;"
  ]
  node [
    id 188
    label "sklep"
  ]
  node [
    id 189
    label "render"
  ]
  node [
    id 190
    label "zmienia&#263;"
  ]
  node [
    id 191
    label "zestaw"
  ]
  node [
    id 192
    label "train"
  ]
  node [
    id 193
    label "uk&#322;ada&#263;"
  ]
  node [
    id 194
    label "dzieli&#263;"
  ]
  node [
    id 195
    label "set"
  ]
  node [
    id 196
    label "przywraca&#263;"
  ]
  node [
    id 197
    label "dawa&#263;"
  ]
  node [
    id 198
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 199
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 200
    label "zbiera&#263;"
  ]
  node [
    id 201
    label "convey"
  ]
  node [
    id 202
    label "opracowywa&#263;"
  ]
  node [
    id 203
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 204
    label "publicize"
  ]
  node [
    id 205
    label "przekazywa&#263;"
  ]
  node [
    id 206
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 207
    label "scala&#263;"
  ]
  node [
    id 208
    label "oddawa&#263;"
  ]
  node [
    id 209
    label "communicate"
  ]
  node [
    id 210
    label "przesta&#263;"
  ]
  node [
    id 211
    label "end"
  ]
  node [
    id 212
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 213
    label "Cesarstwo_Bizanty&#324;skie"
  ]
  node [
    id 214
    label "jednostka_administracyjna"
  ]
  node [
    id 215
    label "procesowicz"
  ]
  node [
    id 216
    label "wypowied&#378;"
  ]
  node [
    id 217
    label "pods&#261;dny"
  ]
  node [
    id 218
    label "podejrzany"
  ]
  node [
    id 219
    label "broni&#263;"
  ]
  node [
    id 220
    label "bronienie"
  ]
  node [
    id 221
    label "system"
  ]
  node [
    id 222
    label "my&#347;l"
  ]
  node [
    id 223
    label "wytw&#243;r"
  ]
  node [
    id 224
    label "urz&#261;d"
  ]
  node [
    id 225
    label "konektyw"
  ]
  node [
    id 226
    label "court"
  ]
  node [
    id 227
    label "obrona"
  ]
  node [
    id 228
    label "s&#261;downictwo"
  ]
  node [
    id 229
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 230
    label "forum"
  ]
  node [
    id 231
    label "zesp&#243;&#322;"
  ]
  node [
    id 232
    label "post&#281;powanie"
  ]
  node [
    id 233
    label "skazany"
  ]
  node [
    id 234
    label "wydarzenie"
  ]
  node [
    id 235
    label "&#347;wiadek"
  ]
  node [
    id 236
    label "antylogizm"
  ]
  node [
    id 237
    label "strona"
  ]
  node [
    id 238
    label "oskar&#380;yciel"
  ]
  node [
    id 239
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 240
    label "biuro"
  ]
  node [
    id 241
    label "instytucja"
  ]
  node [
    id 242
    label "zw&#322;oki"
  ]
  node [
    id 243
    label "bury"
  ]
  node [
    id 244
    label "znie&#347;&#263;"
  ]
  node [
    id 245
    label "hide"
  ]
  node [
    id 246
    label "straci&#263;"
  ]
  node [
    id 247
    label "powk&#322;ada&#263;"
  ]
  node [
    id 248
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 249
    label "poumieszcza&#263;"
  ]
  node [
    id 250
    label "czyj&#347;"
  ]
  node [
    id 251
    label "pomocnik"
  ]
  node [
    id 252
    label "g&#243;wniarz"
  ]
  node [
    id 253
    label "&#347;l&#261;ski"
  ]
  node [
    id 254
    label "m&#322;odzieniec"
  ]
  node [
    id 255
    label "kajtek"
  ]
  node [
    id 256
    label "kawaler"
  ]
  node [
    id 257
    label "usynawianie"
  ]
  node [
    id 258
    label "dziecko"
  ]
  node [
    id 259
    label "okrzos"
  ]
  node [
    id 260
    label "usynowienie"
  ]
  node [
    id 261
    label "sympatia"
  ]
  node [
    id 262
    label "pederasta"
  ]
  node [
    id 263
    label "synek"
  ]
  node [
    id 264
    label "boyfriend"
  ]
  node [
    id 265
    label "opu&#347;ci&#263;"
  ]
  node [
    id 266
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 267
    label "proceed"
  ]
  node [
    id 268
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 269
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 270
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 271
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 272
    label "zmieni&#263;"
  ]
  node [
    id 273
    label "zosta&#263;"
  ]
  node [
    id 274
    label "sail"
  ]
  node [
    id 275
    label "leave"
  ]
  node [
    id 276
    label "uda&#263;_si&#281;"
  ]
  node [
    id 277
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 278
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 279
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 280
    label "przyj&#261;&#263;"
  ]
  node [
    id 281
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 282
    label "become"
  ]
  node [
    id 283
    label "play_along"
  ]
  node [
    id 284
    label "travel"
  ]
  node [
    id 285
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 286
    label "obszar"
  ]
  node [
    id 287
    label "obiekt_naturalny"
  ]
  node [
    id 288
    label "przedmiot"
  ]
  node [
    id 289
    label "Stary_&#346;wiat"
  ]
  node [
    id 290
    label "grupa"
  ]
  node [
    id 291
    label "stw&#243;r"
  ]
  node [
    id 292
    label "biosfera"
  ]
  node [
    id 293
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 294
    label "rzecz"
  ]
  node [
    id 295
    label "magnetosfera"
  ]
  node [
    id 296
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 297
    label "environment"
  ]
  node [
    id 298
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 299
    label "geosfera"
  ]
  node [
    id 300
    label "Nowy_&#346;wiat"
  ]
  node [
    id 301
    label "planeta"
  ]
  node [
    id 302
    label "przejmowa&#263;"
  ]
  node [
    id 303
    label "litosfera"
  ]
  node [
    id 304
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 305
    label "makrokosmos"
  ]
  node [
    id 306
    label "barysfera"
  ]
  node [
    id 307
    label "biota"
  ]
  node [
    id 308
    label "p&#243;&#322;noc"
  ]
  node [
    id 309
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 310
    label "fauna"
  ]
  node [
    id 311
    label "wszechstworzenie"
  ]
  node [
    id 312
    label "geotermia"
  ]
  node [
    id 313
    label "biegun"
  ]
  node [
    id 314
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 315
    label "ekosystem"
  ]
  node [
    id 316
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 317
    label "teren"
  ]
  node [
    id 318
    label "zjawisko"
  ]
  node [
    id 319
    label "p&#243;&#322;kula"
  ]
  node [
    id 320
    label "atmosfera"
  ]
  node [
    id 321
    label "mikrokosmos"
  ]
  node [
    id 322
    label "class"
  ]
  node [
    id 323
    label "po&#322;udnie"
  ]
  node [
    id 324
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 325
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 326
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 327
    label "przejmowanie"
  ]
  node [
    id 328
    label "przestrze&#324;"
  ]
  node [
    id 329
    label "asymilowanie_si&#281;"
  ]
  node [
    id 330
    label "przej&#261;&#263;"
  ]
  node [
    id 331
    label "ekosfera"
  ]
  node [
    id 332
    label "przyroda"
  ]
  node [
    id 333
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 334
    label "ciemna_materia"
  ]
  node [
    id 335
    label "geoida"
  ]
  node [
    id 336
    label "Wsch&#243;d"
  ]
  node [
    id 337
    label "populace"
  ]
  node [
    id 338
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 339
    label "huczek"
  ]
  node [
    id 340
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 341
    label "Ziemia"
  ]
  node [
    id 342
    label "universe"
  ]
  node [
    id 343
    label "ozonosfera"
  ]
  node [
    id 344
    label "rze&#378;ba"
  ]
  node [
    id 345
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 346
    label "zagranica"
  ]
  node [
    id 347
    label "hydrosfera"
  ]
  node [
    id 348
    label "woda"
  ]
  node [
    id 349
    label "kuchnia"
  ]
  node [
    id 350
    label "przej&#281;cie"
  ]
  node [
    id 351
    label "czarna_dziura"
  ]
  node [
    id 352
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 353
    label "morze"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 97
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 26
    target 217
  ]
  edge [
    source 26
    target 218
  ]
  edge [
    source 26
    target 219
  ]
  edge [
    source 26
    target 220
  ]
  edge [
    source 26
    target 221
  ]
  edge [
    source 26
    target 222
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 26
    target 224
  ]
  edge [
    source 26
    target 225
  ]
  edge [
    source 26
    target 226
  ]
  edge [
    source 26
    target 227
  ]
  edge [
    source 26
    target 228
  ]
  edge [
    source 26
    target 229
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 239
  ]
  edge [
    source 26
    target 240
  ]
  edge [
    source 26
    target 241
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 242
  ]
  edge [
    source 29
    target 243
  ]
  edge [
    source 29
    target 244
  ]
  edge [
    source 29
    target 245
  ]
  edge [
    source 29
    target 246
  ]
  edge [
    source 29
    target 247
  ]
  edge [
    source 29
    target 248
  ]
  edge [
    source 29
    target 249
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 251
  ]
  edge [
    source 31
    target 252
  ]
  edge [
    source 31
    target 253
  ]
  edge [
    source 31
    target 254
  ]
  edge [
    source 31
    target 255
  ]
  edge [
    source 31
    target 256
  ]
  edge [
    source 31
    target 257
  ]
  edge [
    source 31
    target 258
  ]
  edge [
    source 31
    target 259
  ]
  edge [
    source 31
    target 260
  ]
  edge [
    source 31
    target 261
  ]
  edge [
    source 31
    target 262
  ]
  edge [
    source 31
    target 263
  ]
  edge [
    source 31
    target 264
  ]
  edge [
    source 32
    target 265
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 32
    target 271
  ]
  edge [
    source 32
    target 272
  ]
  edge [
    source 32
    target 273
  ]
  edge [
    source 32
    target 274
  ]
  edge [
    source 32
    target 275
  ]
  edge [
    source 32
    target 276
  ]
  edge [
    source 32
    target 277
  ]
  edge [
    source 32
    target 174
  ]
  edge [
    source 32
    target 186
  ]
  edge [
    source 32
    target 278
  ]
  edge [
    source 32
    target 279
  ]
  edge [
    source 32
    target 280
  ]
  edge [
    source 32
    target 281
  ]
  edge [
    source 32
    target 282
  ]
  edge [
    source 32
    target 283
  ]
  edge [
    source 32
    target 284
  ]
  edge [
    source 33
    target 285
  ]
  edge [
    source 33
    target 286
  ]
  edge [
    source 33
    target 287
  ]
  edge [
    source 33
    target 288
  ]
  edge [
    source 33
    target 289
  ]
  edge [
    source 33
    target 290
  ]
  edge [
    source 33
    target 291
  ]
  edge [
    source 33
    target 292
  ]
  edge [
    source 33
    target 293
  ]
  edge [
    source 33
    target 294
  ]
  edge [
    source 33
    target 295
  ]
  edge [
    source 33
    target 296
  ]
  edge [
    source 33
    target 297
  ]
  edge [
    source 33
    target 298
  ]
  edge [
    source 33
    target 299
  ]
  edge [
    source 33
    target 300
  ]
  edge [
    source 33
    target 301
  ]
  edge [
    source 33
    target 302
  ]
  edge [
    source 33
    target 303
  ]
  edge [
    source 33
    target 304
  ]
  edge [
    source 33
    target 305
  ]
  edge [
    source 33
    target 306
  ]
  edge [
    source 33
    target 307
  ]
  edge [
    source 33
    target 308
  ]
  edge [
    source 33
    target 309
  ]
  edge [
    source 33
    target 310
  ]
  edge [
    source 33
    target 311
  ]
  edge [
    source 33
    target 312
  ]
  edge [
    source 33
    target 313
  ]
  edge [
    source 33
    target 314
  ]
  edge [
    source 33
    target 315
  ]
  edge [
    source 33
    target 316
  ]
  edge [
    source 33
    target 317
  ]
  edge [
    source 33
    target 318
  ]
  edge [
    source 33
    target 319
  ]
  edge [
    source 33
    target 320
  ]
  edge [
    source 33
    target 321
  ]
  edge [
    source 33
    target 322
  ]
  edge [
    source 33
    target 323
  ]
  edge [
    source 33
    target 324
  ]
  edge [
    source 33
    target 325
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 33
    target 328
  ]
  edge [
    source 33
    target 329
  ]
  edge [
    source 33
    target 330
  ]
  edge [
    source 33
    target 331
  ]
  edge [
    source 33
    target 332
  ]
  edge [
    source 33
    target 333
  ]
  edge [
    source 33
    target 334
  ]
  edge [
    source 33
    target 335
  ]
  edge [
    source 33
    target 336
  ]
  edge [
    source 33
    target 337
  ]
  edge [
    source 33
    target 338
  ]
  edge [
    source 33
    target 339
  ]
  edge [
    source 33
    target 340
  ]
  edge [
    source 33
    target 341
  ]
  edge [
    source 33
    target 342
  ]
  edge [
    source 33
    target 343
  ]
  edge [
    source 33
    target 344
  ]
  edge [
    source 33
    target 345
  ]
  edge [
    source 33
    target 346
  ]
  edge [
    source 33
    target 347
  ]
  edge [
    source 33
    target 348
  ]
  edge [
    source 33
    target 349
  ]
  edge [
    source 33
    target 350
  ]
  edge [
    source 33
    target 351
  ]
  edge [
    source 33
    target 352
  ]
  edge [
    source 33
    target 353
  ]
]
