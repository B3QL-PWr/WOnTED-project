graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.0428790199081166
  density 0.0031332500305339206
  graphCliqueNumber 3
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "dyplomacja"
    origin "text"
  ]
  node [
    id 2
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zmobilizowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "obrzuca&#263;"
    origin "text"
  ]
  node [
    id 5
    label "z&#322;y"
    origin "text"
  ]
  node [
    id 6
    label "inwektywa"
    origin "text"
  ]
  node [
    id 7
    label "niemiecki"
    origin "text"
  ]
  node [
    id 8
    label "prasa"
    origin "text"
  ]
  node [
    id 9
    label "domaga&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "sankcja"
    origin "text"
  ]
  node [
    id 12
    label "wobec"
    origin "text"
  ]
  node [
    id 13
    label "dziennikarz"
    origin "text"
  ]
  node [
    id 14
    label "jak"
    origin "text"
  ]
  node [
    id 15
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 16
    label "kraj"
    origin "text"
  ]
  node [
    id 17
    label "cz&#322;onkowski"
    origin "text"
  ]
  node [
    id 18
    label "unia"
    origin "text"
  ]
  node [
    id 19
    label "europejski"
    origin "text"
  ]
  node [
    id 20
    label "powa&#380;nie"
    origin "text"
  ]
  node [
    id 21
    label "grozi&#263;"
    origin "text"
  ]
  node [
    id 22
    label "wi&#281;zienie"
    origin "text"
  ]
  node [
    id 23
    label "krytyka"
    origin "text"
  ]
  node [
    id 24
    label "osobisto&#347;&#263;"
    origin "text"
  ]
  node [
    id 25
    label "polityczny"
    origin "text"
  ]
  node [
    id 26
    label "polski"
    origin "text"
  ]
  node [
    id 27
    label "prawo"
    origin "text"
  ]
  node [
    id 28
    label "przewiduj&#261;cy"
    origin "text"
  ]
  node [
    id 29
    label "kar"
    origin "text"
  ]
  node [
    id 30
    label "wykroczenie"
    origin "text"
  ]
  node [
    id 31
    label "prasowy"
    origin "text"
  ]
  node [
    id 32
    label "by&#263;"
    origin "text"
  ]
  node [
    id 33
    label "sprzeczny"
    origin "text"
  ]
  node [
    id 34
    label "standard"
    origin "text"
  ]
  node [
    id 35
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 36
    label "znowelizowa&#263;"
    origin "text"
  ]
  node [
    id 37
    label "zacofany"
    origin "text"
  ]
  node [
    id 38
    label "przepis"
    origin "text"
  ]
  node [
    id 39
    label "bez"
    origin "text"
  ]
  node [
    id 40
    label "granica"
    origin "text"
  ]
  node [
    id 41
    label "korpus_dyplomatyczny"
  ]
  node [
    id 42
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 43
    label "grupa"
  ]
  node [
    id 44
    label "statesmanship"
  ]
  node [
    id 45
    label "nastawienie"
  ]
  node [
    id 46
    label "notyfikowanie"
  ]
  node [
    id 47
    label "notyfikowa&#263;"
  ]
  node [
    id 48
    label "corps"
  ]
  node [
    id 49
    label "polityka"
  ]
  node [
    id 50
    label "proceed"
  ]
  node [
    id 51
    label "catch"
  ]
  node [
    id 52
    label "pozosta&#263;"
  ]
  node [
    id 53
    label "osta&#263;_si&#281;"
  ]
  node [
    id 54
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 55
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 56
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 57
    label "change"
  ]
  node [
    id 58
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 59
    label "przygotowa&#263;"
  ]
  node [
    id 60
    label "postawi&#263;_w_stan_pogotowia"
  ]
  node [
    id 61
    label "wake_up"
  ]
  node [
    id 62
    label "nastawi&#263;"
  ]
  node [
    id 63
    label "pool"
  ]
  node [
    id 64
    label "wojsko"
  ]
  node [
    id 65
    label "powo&#322;a&#263;"
  ]
  node [
    id 66
    label "pobudzi&#263;"
  ]
  node [
    id 67
    label "napada&#263;"
  ]
  node [
    id 68
    label "pipe"
  ]
  node [
    id 69
    label "hide"
  ]
  node [
    id 70
    label "obr&#281;bia&#263;"
  ]
  node [
    id 71
    label "obsypywa&#263;"
  ]
  node [
    id 72
    label "rzuca&#263;"
  ]
  node [
    id 73
    label "pepper"
  ]
  node [
    id 74
    label "bombardowa&#263;"
  ]
  node [
    id 75
    label "zdenerwowany"
  ]
  node [
    id 76
    label "zez&#322;oszczenie"
  ]
  node [
    id 77
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 78
    label "gniewanie"
  ]
  node [
    id 79
    label "niekorzystny"
  ]
  node [
    id 80
    label "niemoralny"
  ]
  node [
    id 81
    label "niegrzeczny"
  ]
  node [
    id 82
    label "pieski"
  ]
  node [
    id 83
    label "negatywny"
  ]
  node [
    id 84
    label "&#378;le"
  ]
  node [
    id 85
    label "syf"
  ]
  node [
    id 86
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 87
    label "sierdzisty"
  ]
  node [
    id 88
    label "z&#322;oszczenie"
  ]
  node [
    id 89
    label "rozgniewanie"
  ]
  node [
    id 90
    label "niepomy&#347;lny"
  ]
  node [
    id 91
    label "cholera"
  ]
  node [
    id 92
    label "wypowied&#378;"
  ]
  node [
    id 93
    label "pies"
  ]
  node [
    id 94
    label "szmata"
  ]
  node [
    id 95
    label "bluzg"
  ]
  node [
    id 96
    label "obelga"
  ]
  node [
    id 97
    label "chujowy"
  ]
  node [
    id 98
    label "chuj"
  ]
  node [
    id 99
    label "szwabski"
  ]
  node [
    id 100
    label "po_niemiecku"
  ]
  node [
    id 101
    label "niemiec"
  ]
  node [
    id 102
    label "cenar"
  ]
  node [
    id 103
    label "j&#281;zyk"
  ]
  node [
    id 104
    label "German"
  ]
  node [
    id 105
    label "pionier"
  ]
  node [
    id 106
    label "niemiecko"
  ]
  node [
    id 107
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 108
    label "zachodnioeuropejski"
  ]
  node [
    id 109
    label "strudel"
  ]
  node [
    id 110
    label "junkers"
  ]
  node [
    id 111
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 112
    label "pisa&#263;"
  ]
  node [
    id 113
    label "dziennikarz_prasowy"
  ]
  node [
    id 114
    label "gazeta"
  ]
  node [
    id 115
    label "napisa&#263;"
  ]
  node [
    id 116
    label "maszyna_rolnicza"
  ]
  node [
    id 117
    label "zesp&#243;&#322;"
  ]
  node [
    id 118
    label "t&#322;oczysko"
  ]
  node [
    id 119
    label "depesza"
  ]
  node [
    id 120
    label "maszyna"
  ]
  node [
    id 121
    label "czasopismo"
  ]
  node [
    id 122
    label "media"
  ]
  node [
    id 123
    label "kiosk"
  ]
  node [
    id 124
    label "authority"
  ]
  node [
    id 125
    label "konsekwencja"
  ]
  node [
    id 126
    label "punishment"
  ]
  node [
    id 127
    label "roboty_przymusowe"
  ]
  node [
    id 128
    label "nemezis"
  ]
  node [
    id 129
    label "restrykcja"
  ]
  node [
    id 130
    label "kara"
  ]
  node [
    id 131
    label "zatwierdzenie"
  ]
  node [
    id 132
    label "nowiniarz"
  ]
  node [
    id 133
    label "akredytowa&#263;"
  ]
  node [
    id 134
    label "akredytowanie"
  ]
  node [
    id 135
    label "bran&#380;owiec"
  ]
  node [
    id 136
    label "publicysta"
  ]
  node [
    id 137
    label "byd&#322;o"
  ]
  node [
    id 138
    label "zobo"
  ]
  node [
    id 139
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 140
    label "yakalo"
  ]
  node [
    id 141
    label "dzo"
  ]
  node [
    id 142
    label "free"
  ]
  node [
    id 143
    label "Skandynawia"
  ]
  node [
    id 144
    label "Filipiny"
  ]
  node [
    id 145
    label "Rwanda"
  ]
  node [
    id 146
    label "Kaukaz"
  ]
  node [
    id 147
    label "Kaszmir"
  ]
  node [
    id 148
    label "Toskania"
  ]
  node [
    id 149
    label "Yorkshire"
  ]
  node [
    id 150
    label "&#321;emkowszczyzna"
  ]
  node [
    id 151
    label "obszar"
  ]
  node [
    id 152
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 153
    label "Monako"
  ]
  node [
    id 154
    label "Amhara"
  ]
  node [
    id 155
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 156
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 157
    label "Lombardia"
  ]
  node [
    id 158
    label "Korea"
  ]
  node [
    id 159
    label "Kalabria"
  ]
  node [
    id 160
    label "Ghana"
  ]
  node [
    id 161
    label "Czarnog&#243;ra"
  ]
  node [
    id 162
    label "Tyrol"
  ]
  node [
    id 163
    label "Malawi"
  ]
  node [
    id 164
    label "Indonezja"
  ]
  node [
    id 165
    label "Bu&#322;garia"
  ]
  node [
    id 166
    label "Nauru"
  ]
  node [
    id 167
    label "Kenia"
  ]
  node [
    id 168
    label "Pamir"
  ]
  node [
    id 169
    label "Kambod&#380;a"
  ]
  node [
    id 170
    label "Lubelszczyzna"
  ]
  node [
    id 171
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 172
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 173
    label "Mali"
  ]
  node [
    id 174
    label "&#379;ywiecczyzna"
  ]
  node [
    id 175
    label "Austria"
  ]
  node [
    id 176
    label "interior"
  ]
  node [
    id 177
    label "Europa_Wschodnia"
  ]
  node [
    id 178
    label "Armenia"
  ]
  node [
    id 179
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 180
    label "Fid&#380;i"
  ]
  node [
    id 181
    label "Tuwalu"
  ]
  node [
    id 182
    label "Zabajkale"
  ]
  node [
    id 183
    label "Etiopia"
  ]
  node [
    id 184
    label "Malta"
  ]
  node [
    id 185
    label "Malezja"
  ]
  node [
    id 186
    label "Kaszuby"
  ]
  node [
    id 187
    label "Bo&#347;nia"
  ]
  node [
    id 188
    label "Noworosja"
  ]
  node [
    id 189
    label "Grenada"
  ]
  node [
    id 190
    label "Tad&#380;ykistan"
  ]
  node [
    id 191
    label "Ba&#322;kany"
  ]
  node [
    id 192
    label "Wehrlen"
  ]
  node [
    id 193
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 194
    label "Anglia"
  ]
  node [
    id 195
    label "Kielecczyzna"
  ]
  node [
    id 196
    label "Rumunia"
  ]
  node [
    id 197
    label "Pomorze_Zachodnie"
  ]
  node [
    id 198
    label "Maroko"
  ]
  node [
    id 199
    label "Bhutan"
  ]
  node [
    id 200
    label "Opolskie"
  ]
  node [
    id 201
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 202
    label "Ko&#322;yma"
  ]
  node [
    id 203
    label "Oksytania"
  ]
  node [
    id 204
    label "S&#322;owacja"
  ]
  node [
    id 205
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 206
    label "Seszele"
  ]
  node [
    id 207
    label "Syjon"
  ]
  node [
    id 208
    label "Kuwejt"
  ]
  node [
    id 209
    label "Arabia_Saudyjska"
  ]
  node [
    id 210
    label "Kociewie"
  ]
  node [
    id 211
    label "Ekwador"
  ]
  node [
    id 212
    label "Kanada"
  ]
  node [
    id 213
    label "ziemia"
  ]
  node [
    id 214
    label "Japonia"
  ]
  node [
    id 215
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 216
    label "Hiszpania"
  ]
  node [
    id 217
    label "Wyspy_Marshalla"
  ]
  node [
    id 218
    label "Botswana"
  ]
  node [
    id 219
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 220
    label "D&#380;ibuti"
  ]
  node [
    id 221
    label "Huculszczyzna"
  ]
  node [
    id 222
    label "Wietnam"
  ]
  node [
    id 223
    label "Egipt"
  ]
  node [
    id 224
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 225
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 226
    label "Burkina_Faso"
  ]
  node [
    id 227
    label "Bawaria"
  ]
  node [
    id 228
    label "Niemcy"
  ]
  node [
    id 229
    label "Khitai"
  ]
  node [
    id 230
    label "Macedonia"
  ]
  node [
    id 231
    label "Albania"
  ]
  node [
    id 232
    label "Madagaskar"
  ]
  node [
    id 233
    label "Bahrajn"
  ]
  node [
    id 234
    label "Jemen"
  ]
  node [
    id 235
    label "Lesoto"
  ]
  node [
    id 236
    label "Maghreb"
  ]
  node [
    id 237
    label "Samoa"
  ]
  node [
    id 238
    label "Andora"
  ]
  node [
    id 239
    label "Bory_Tucholskie"
  ]
  node [
    id 240
    label "Chiny"
  ]
  node [
    id 241
    label "Europa_Zachodnia"
  ]
  node [
    id 242
    label "Cypr"
  ]
  node [
    id 243
    label "Wielka_Brytania"
  ]
  node [
    id 244
    label "Kerala"
  ]
  node [
    id 245
    label "Podhale"
  ]
  node [
    id 246
    label "Kabylia"
  ]
  node [
    id 247
    label "Ukraina"
  ]
  node [
    id 248
    label "Paragwaj"
  ]
  node [
    id 249
    label "Trynidad_i_Tobago"
  ]
  node [
    id 250
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 251
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 252
    label "Ma&#322;opolska"
  ]
  node [
    id 253
    label "Polesie"
  ]
  node [
    id 254
    label "Liguria"
  ]
  node [
    id 255
    label "Libia"
  ]
  node [
    id 256
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 257
    label "&#321;&#243;dzkie"
  ]
  node [
    id 258
    label "Surinam"
  ]
  node [
    id 259
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 260
    label "Palestyna"
  ]
  node [
    id 261
    label "Australia"
  ]
  node [
    id 262
    label "Nigeria"
  ]
  node [
    id 263
    label "Honduras"
  ]
  node [
    id 264
    label "Bojkowszczyzna"
  ]
  node [
    id 265
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 266
    label "Karaiby"
  ]
  node [
    id 267
    label "Bangladesz"
  ]
  node [
    id 268
    label "Peru"
  ]
  node [
    id 269
    label "Kazachstan"
  ]
  node [
    id 270
    label "USA"
  ]
  node [
    id 271
    label "Irak"
  ]
  node [
    id 272
    label "Nepal"
  ]
  node [
    id 273
    label "S&#261;decczyzna"
  ]
  node [
    id 274
    label "Sudan"
  ]
  node [
    id 275
    label "Sand&#380;ak"
  ]
  node [
    id 276
    label "Nadrenia"
  ]
  node [
    id 277
    label "San_Marino"
  ]
  node [
    id 278
    label "Burundi"
  ]
  node [
    id 279
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 280
    label "Dominikana"
  ]
  node [
    id 281
    label "Komory"
  ]
  node [
    id 282
    label "Zakarpacie"
  ]
  node [
    id 283
    label "Gwatemala"
  ]
  node [
    id 284
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 285
    label "Zag&#243;rze"
  ]
  node [
    id 286
    label "Andaluzja"
  ]
  node [
    id 287
    label "granica_pa&#324;stwa"
  ]
  node [
    id 288
    label "Turkiestan"
  ]
  node [
    id 289
    label "Naddniestrze"
  ]
  node [
    id 290
    label "Hercegowina"
  ]
  node [
    id 291
    label "Brunei"
  ]
  node [
    id 292
    label "Iran"
  ]
  node [
    id 293
    label "jednostka_administracyjna"
  ]
  node [
    id 294
    label "Zimbabwe"
  ]
  node [
    id 295
    label "Namibia"
  ]
  node [
    id 296
    label "Meksyk"
  ]
  node [
    id 297
    label "Lotaryngia"
  ]
  node [
    id 298
    label "Kamerun"
  ]
  node [
    id 299
    label "Opolszczyzna"
  ]
  node [
    id 300
    label "Afryka_Wschodnia"
  ]
  node [
    id 301
    label "Szlezwik"
  ]
  node [
    id 302
    label "Somalia"
  ]
  node [
    id 303
    label "Angola"
  ]
  node [
    id 304
    label "Gabon"
  ]
  node [
    id 305
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 306
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 307
    label "Mozambik"
  ]
  node [
    id 308
    label "Tajwan"
  ]
  node [
    id 309
    label "Tunezja"
  ]
  node [
    id 310
    label "Nowa_Zelandia"
  ]
  node [
    id 311
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 312
    label "Podbeskidzie"
  ]
  node [
    id 313
    label "Liban"
  ]
  node [
    id 314
    label "Jordania"
  ]
  node [
    id 315
    label "Tonga"
  ]
  node [
    id 316
    label "Czad"
  ]
  node [
    id 317
    label "Liberia"
  ]
  node [
    id 318
    label "Gwinea"
  ]
  node [
    id 319
    label "Belize"
  ]
  node [
    id 320
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 321
    label "Mazowsze"
  ]
  node [
    id 322
    label "&#321;otwa"
  ]
  node [
    id 323
    label "Syria"
  ]
  node [
    id 324
    label "Benin"
  ]
  node [
    id 325
    label "Afryka_Zachodnia"
  ]
  node [
    id 326
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 327
    label "Dominika"
  ]
  node [
    id 328
    label "Antigua_i_Barbuda"
  ]
  node [
    id 329
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 330
    label "Hanower"
  ]
  node [
    id 331
    label "Galicja"
  ]
  node [
    id 332
    label "Szkocja"
  ]
  node [
    id 333
    label "Walia"
  ]
  node [
    id 334
    label "Afganistan"
  ]
  node [
    id 335
    label "Kiribati"
  ]
  node [
    id 336
    label "W&#322;ochy"
  ]
  node [
    id 337
    label "Szwajcaria"
  ]
  node [
    id 338
    label "Powi&#347;le"
  ]
  node [
    id 339
    label "Sahara_Zachodnia"
  ]
  node [
    id 340
    label "Chorwacja"
  ]
  node [
    id 341
    label "Tajlandia"
  ]
  node [
    id 342
    label "Salwador"
  ]
  node [
    id 343
    label "Bahamy"
  ]
  node [
    id 344
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 345
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 346
    label "Zamojszczyzna"
  ]
  node [
    id 347
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 348
    label "S&#322;owenia"
  ]
  node [
    id 349
    label "Gambia"
  ]
  node [
    id 350
    label "Kujawy"
  ]
  node [
    id 351
    label "Urugwaj"
  ]
  node [
    id 352
    label "Podlasie"
  ]
  node [
    id 353
    label "Zair"
  ]
  node [
    id 354
    label "Erytrea"
  ]
  node [
    id 355
    label "Laponia"
  ]
  node [
    id 356
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 357
    label "Umbria"
  ]
  node [
    id 358
    label "Rosja"
  ]
  node [
    id 359
    label "Uganda"
  ]
  node [
    id 360
    label "Niger"
  ]
  node [
    id 361
    label "Mauritius"
  ]
  node [
    id 362
    label "Turkmenistan"
  ]
  node [
    id 363
    label "Turcja"
  ]
  node [
    id 364
    label "Mezoameryka"
  ]
  node [
    id 365
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 366
    label "Irlandia"
  ]
  node [
    id 367
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 368
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 369
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 370
    label "Gwinea_Bissau"
  ]
  node [
    id 371
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 372
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 373
    label "Kurdystan"
  ]
  node [
    id 374
    label "Belgia"
  ]
  node [
    id 375
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 376
    label "Palau"
  ]
  node [
    id 377
    label "Barbados"
  ]
  node [
    id 378
    label "Chile"
  ]
  node [
    id 379
    label "Wenezuela"
  ]
  node [
    id 380
    label "W&#281;gry"
  ]
  node [
    id 381
    label "Argentyna"
  ]
  node [
    id 382
    label "Kolumbia"
  ]
  node [
    id 383
    label "Kampania"
  ]
  node [
    id 384
    label "Armagnac"
  ]
  node [
    id 385
    label "Sierra_Leone"
  ]
  node [
    id 386
    label "Azerbejd&#380;an"
  ]
  node [
    id 387
    label "Kongo"
  ]
  node [
    id 388
    label "Polinezja"
  ]
  node [
    id 389
    label "Warmia"
  ]
  node [
    id 390
    label "Pakistan"
  ]
  node [
    id 391
    label "Liechtenstein"
  ]
  node [
    id 392
    label "Wielkopolska"
  ]
  node [
    id 393
    label "Nikaragua"
  ]
  node [
    id 394
    label "Senegal"
  ]
  node [
    id 395
    label "brzeg"
  ]
  node [
    id 396
    label "Bordeaux"
  ]
  node [
    id 397
    label "Lauda"
  ]
  node [
    id 398
    label "Indie"
  ]
  node [
    id 399
    label "Mazury"
  ]
  node [
    id 400
    label "Suazi"
  ]
  node [
    id 401
    label "Polska"
  ]
  node [
    id 402
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 403
    label "Algieria"
  ]
  node [
    id 404
    label "Jamajka"
  ]
  node [
    id 405
    label "Timor_Wschodni"
  ]
  node [
    id 406
    label "Oceania"
  ]
  node [
    id 407
    label "Kostaryka"
  ]
  node [
    id 408
    label "Podkarpacie"
  ]
  node [
    id 409
    label "Lasko"
  ]
  node [
    id 410
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 411
    label "Kuba"
  ]
  node [
    id 412
    label "Mauretania"
  ]
  node [
    id 413
    label "Amazonia"
  ]
  node [
    id 414
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 415
    label "Portoryko"
  ]
  node [
    id 416
    label "Brazylia"
  ]
  node [
    id 417
    label "Mo&#322;dawia"
  ]
  node [
    id 418
    label "organizacja"
  ]
  node [
    id 419
    label "Litwa"
  ]
  node [
    id 420
    label "Kirgistan"
  ]
  node [
    id 421
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 422
    label "Izrael"
  ]
  node [
    id 423
    label "Grecja"
  ]
  node [
    id 424
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 425
    label "Kurpie"
  ]
  node [
    id 426
    label "Holandia"
  ]
  node [
    id 427
    label "Sri_Lanka"
  ]
  node [
    id 428
    label "Tonkin"
  ]
  node [
    id 429
    label "Katar"
  ]
  node [
    id 430
    label "Azja_Wschodnia"
  ]
  node [
    id 431
    label "Mikronezja"
  ]
  node [
    id 432
    label "Ukraina_Zachodnia"
  ]
  node [
    id 433
    label "Laos"
  ]
  node [
    id 434
    label "Mongolia"
  ]
  node [
    id 435
    label "Turyngia"
  ]
  node [
    id 436
    label "Malediwy"
  ]
  node [
    id 437
    label "Zambia"
  ]
  node [
    id 438
    label "Baszkiria"
  ]
  node [
    id 439
    label "Tanzania"
  ]
  node [
    id 440
    label "Gujana"
  ]
  node [
    id 441
    label "Apulia"
  ]
  node [
    id 442
    label "Czechy"
  ]
  node [
    id 443
    label "Panama"
  ]
  node [
    id 444
    label "Uzbekistan"
  ]
  node [
    id 445
    label "Gruzja"
  ]
  node [
    id 446
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 447
    label "Serbia"
  ]
  node [
    id 448
    label "Francja"
  ]
  node [
    id 449
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 450
    label "Togo"
  ]
  node [
    id 451
    label "Estonia"
  ]
  node [
    id 452
    label "Indochiny"
  ]
  node [
    id 453
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 454
    label "Oman"
  ]
  node [
    id 455
    label "Boliwia"
  ]
  node [
    id 456
    label "Portugalia"
  ]
  node [
    id 457
    label "Wyspy_Salomona"
  ]
  node [
    id 458
    label "Luksemburg"
  ]
  node [
    id 459
    label "Haiti"
  ]
  node [
    id 460
    label "Biskupizna"
  ]
  node [
    id 461
    label "Lubuskie"
  ]
  node [
    id 462
    label "Birma"
  ]
  node [
    id 463
    label "Rodezja"
  ]
  node [
    id 464
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 465
    label "uk&#322;ad"
  ]
  node [
    id 466
    label "partia"
  ]
  node [
    id 467
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 468
    label "Unia_Europejska"
  ]
  node [
    id 469
    label "combination"
  ]
  node [
    id 470
    label "union"
  ]
  node [
    id 471
    label "Unia"
  ]
  node [
    id 472
    label "European"
  ]
  node [
    id 473
    label "po_europejsku"
  ]
  node [
    id 474
    label "charakterystyczny"
  ]
  node [
    id 475
    label "europejsko"
  ]
  node [
    id 476
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 477
    label "typowy"
  ]
  node [
    id 478
    label "powa&#380;ny"
  ]
  node [
    id 479
    label "bardzo"
  ]
  node [
    id 480
    label "niema&#322;o"
  ]
  node [
    id 481
    label "gro&#378;nie"
  ]
  node [
    id 482
    label "ci&#281;&#380;ko"
  ]
  node [
    id 483
    label "ci&#281;&#380;ki"
  ]
  node [
    id 484
    label "zapowiada&#263;"
  ]
  node [
    id 485
    label "boast"
  ]
  node [
    id 486
    label "hazard"
  ]
  node [
    id 487
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 488
    label "ogranicza&#263;"
  ]
  node [
    id 489
    label "uniemo&#380;liwianie"
  ]
  node [
    id 490
    label "Butyrki"
  ]
  node [
    id 491
    label "ciupa"
  ]
  node [
    id 492
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 493
    label "reedukator"
  ]
  node [
    id 494
    label "miejsce_odosobnienia"
  ]
  node [
    id 495
    label "&#321;ubianka"
  ]
  node [
    id 496
    label "pierdel"
  ]
  node [
    id 497
    label "imprisonment"
  ]
  node [
    id 498
    label "sytuacja"
  ]
  node [
    id 499
    label "krytyka_literacka"
  ]
  node [
    id 500
    label "cenzura"
  ]
  node [
    id 501
    label "review"
  ]
  node [
    id 502
    label "ocena"
  ]
  node [
    id 503
    label "publiczno&#347;&#263;"
  ]
  node [
    id 504
    label "streszczenie"
  ]
  node [
    id 505
    label "diatryba"
  ]
  node [
    id 506
    label "criticism"
  ]
  node [
    id 507
    label "publicystyka"
  ]
  node [
    id 508
    label "tekst"
  ]
  node [
    id 509
    label "kto&#347;"
  ]
  node [
    id 510
    label "internowanie"
  ]
  node [
    id 511
    label "prorz&#261;dowy"
  ]
  node [
    id 512
    label "wi&#281;zie&#324;"
  ]
  node [
    id 513
    label "politycznie"
  ]
  node [
    id 514
    label "internowa&#263;"
  ]
  node [
    id 515
    label "ideologiczny"
  ]
  node [
    id 516
    label "lacki"
  ]
  node [
    id 517
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 518
    label "przedmiot"
  ]
  node [
    id 519
    label "sztajer"
  ]
  node [
    id 520
    label "drabant"
  ]
  node [
    id 521
    label "polak"
  ]
  node [
    id 522
    label "pierogi_ruskie"
  ]
  node [
    id 523
    label "krakowiak"
  ]
  node [
    id 524
    label "Polish"
  ]
  node [
    id 525
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 526
    label "oberek"
  ]
  node [
    id 527
    label "po_polsku"
  ]
  node [
    id 528
    label "mazur"
  ]
  node [
    id 529
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 530
    label "chodzony"
  ]
  node [
    id 531
    label "skoczny"
  ]
  node [
    id 532
    label "ryba_po_grecku"
  ]
  node [
    id 533
    label "goniony"
  ]
  node [
    id 534
    label "polsko"
  ]
  node [
    id 535
    label "obserwacja"
  ]
  node [
    id 536
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 537
    label "nauka_prawa"
  ]
  node [
    id 538
    label "dominion"
  ]
  node [
    id 539
    label "normatywizm"
  ]
  node [
    id 540
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 541
    label "qualification"
  ]
  node [
    id 542
    label "opis"
  ]
  node [
    id 543
    label "regu&#322;a_Allena"
  ]
  node [
    id 544
    label "normalizacja"
  ]
  node [
    id 545
    label "kazuistyka"
  ]
  node [
    id 546
    label "regu&#322;a_Glogera"
  ]
  node [
    id 547
    label "kultura_duchowa"
  ]
  node [
    id 548
    label "prawo_karne"
  ]
  node [
    id 549
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 550
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 551
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 552
    label "struktura"
  ]
  node [
    id 553
    label "szko&#322;a"
  ]
  node [
    id 554
    label "prawo_karne_procesowe"
  ]
  node [
    id 555
    label "prawo_Mendla"
  ]
  node [
    id 556
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 557
    label "criterion"
  ]
  node [
    id 558
    label "kanonistyka"
  ]
  node [
    id 559
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 560
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 561
    label "wykonawczy"
  ]
  node [
    id 562
    label "twierdzenie"
  ]
  node [
    id 563
    label "judykatura"
  ]
  node [
    id 564
    label "legislacyjnie"
  ]
  node [
    id 565
    label "umocowa&#263;"
  ]
  node [
    id 566
    label "podmiot"
  ]
  node [
    id 567
    label "procesualistyka"
  ]
  node [
    id 568
    label "kierunek"
  ]
  node [
    id 569
    label "kryminologia"
  ]
  node [
    id 570
    label "kryminalistyka"
  ]
  node [
    id 571
    label "cywilistyka"
  ]
  node [
    id 572
    label "law"
  ]
  node [
    id 573
    label "zasada_d'Alemberta"
  ]
  node [
    id 574
    label "jurisprudence"
  ]
  node [
    id 575
    label "zasada"
  ]
  node [
    id 576
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 577
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 578
    label "zabiegliwy"
  ]
  node [
    id 579
    label "provident"
  ]
  node [
    id 580
    label "opatrzny"
  ]
  node [
    id 581
    label "rozwa&#380;ny"
  ]
  node [
    id 582
    label "wn&#281;ka"
  ]
  node [
    id 583
    label "zrobienie"
  ]
  node [
    id 584
    label "post&#281;pek"
  ]
  node [
    id 585
    label "transgresja"
  ]
  node [
    id 586
    label "discourtesy"
  ]
  node [
    id 587
    label "medialny"
  ]
  node [
    id 588
    label "medialnie"
  ]
  node [
    id 589
    label "si&#281;ga&#263;"
  ]
  node [
    id 590
    label "trwa&#263;"
  ]
  node [
    id 591
    label "obecno&#347;&#263;"
  ]
  node [
    id 592
    label "stan"
  ]
  node [
    id 593
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 594
    label "stand"
  ]
  node [
    id 595
    label "mie&#263;_miejsce"
  ]
  node [
    id 596
    label "uczestniczy&#263;"
  ]
  node [
    id 597
    label "chodzi&#263;"
  ]
  node [
    id 598
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 599
    label "equal"
  ]
  node [
    id 600
    label "sprzecznie"
  ]
  node [
    id 601
    label "niezgodny"
  ]
  node [
    id 602
    label "zorganizowa&#263;"
  ]
  node [
    id 603
    label "model"
  ]
  node [
    id 604
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 605
    label "taniec_towarzyski"
  ]
  node [
    id 606
    label "ordinariness"
  ]
  node [
    id 607
    label "organizowanie"
  ]
  node [
    id 608
    label "zorganizowanie"
  ]
  node [
    id 609
    label "instytucja"
  ]
  node [
    id 610
    label "organizowa&#263;"
  ]
  node [
    id 611
    label "trza"
  ]
  node [
    id 612
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 613
    label "para"
  ]
  node [
    id 614
    label "necessity"
  ]
  node [
    id 615
    label "uaktualni&#263;"
  ]
  node [
    id 616
    label "spowodowa&#263;"
  ]
  node [
    id 617
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 618
    label "zatabaczony"
  ]
  node [
    id 619
    label "nienowoczesny"
  ]
  node [
    id 620
    label "przedawnienie_si&#281;"
  ]
  node [
    id 621
    label "recepta"
  ]
  node [
    id 622
    label "norma_prawna"
  ]
  node [
    id 623
    label "kodeks"
  ]
  node [
    id 624
    label "regulation"
  ]
  node [
    id 625
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 626
    label "porada"
  ]
  node [
    id 627
    label "przedawnianie_si&#281;"
  ]
  node [
    id 628
    label "spos&#243;b"
  ]
  node [
    id 629
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 630
    label "ki&#347;&#263;"
  ]
  node [
    id 631
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 632
    label "krzew"
  ]
  node [
    id 633
    label "pi&#380;maczkowate"
  ]
  node [
    id 634
    label "pestkowiec"
  ]
  node [
    id 635
    label "kwiat"
  ]
  node [
    id 636
    label "owoc"
  ]
  node [
    id 637
    label "oliwkowate"
  ]
  node [
    id 638
    label "ro&#347;lina"
  ]
  node [
    id 639
    label "hy&#263;ka"
  ]
  node [
    id 640
    label "lilac"
  ]
  node [
    id 641
    label "delfinidyna"
  ]
  node [
    id 642
    label "zakres"
  ]
  node [
    id 643
    label "Ural"
  ]
  node [
    id 644
    label "koniec"
  ]
  node [
    id 645
    label "kres"
  ]
  node [
    id 646
    label "granice"
  ]
  node [
    id 647
    label "pu&#322;ap"
  ]
  node [
    id 648
    label "frontier"
  ]
  node [
    id 649
    label "end"
  ]
  node [
    id 650
    label "miara"
  ]
  node [
    id 651
    label "poj&#281;cie"
  ]
  node [
    id 652
    label "przej&#347;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 197
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 16
    target 203
  ]
  edge [
    source 16
    target 204
  ]
  edge [
    source 16
    target 205
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 207
  ]
  edge [
    source 16
    target 208
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 16
    target 213
  ]
  edge [
    source 16
    target 214
  ]
  edge [
    source 16
    target 215
  ]
  edge [
    source 16
    target 216
  ]
  edge [
    source 16
    target 217
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 224
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 16
    target 226
  ]
  edge [
    source 16
    target 227
  ]
  edge [
    source 16
    target 228
  ]
  edge [
    source 16
    target 229
  ]
  edge [
    source 16
    target 230
  ]
  edge [
    source 16
    target 231
  ]
  edge [
    source 16
    target 232
  ]
  edge [
    source 16
    target 233
  ]
  edge [
    source 16
    target 234
  ]
  edge [
    source 16
    target 235
  ]
  edge [
    source 16
    target 236
  ]
  edge [
    source 16
    target 237
  ]
  edge [
    source 16
    target 238
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 240
  ]
  edge [
    source 16
    target 241
  ]
  edge [
    source 16
    target 242
  ]
  edge [
    source 16
    target 243
  ]
  edge [
    source 16
    target 244
  ]
  edge [
    source 16
    target 245
  ]
  edge [
    source 16
    target 246
  ]
  edge [
    source 16
    target 247
  ]
  edge [
    source 16
    target 248
  ]
  edge [
    source 16
    target 249
  ]
  edge [
    source 16
    target 250
  ]
  edge [
    source 16
    target 251
  ]
  edge [
    source 16
    target 252
  ]
  edge [
    source 16
    target 253
  ]
  edge [
    source 16
    target 254
  ]
  edge [
    source 16
    target 255
  ]
  edge [
    source 16
    target 256
  ]
  edge [
    source 16
    target 257
  ]
  edge [
    source 16
    target 258
  ]
  edge [
    source 16
    target 259
  ]
  edge [
    source 16
    target 260
  ]
  edge [
    source 16
    target 261
  ]
  edge [
    source 16
    target 262
  ]
  edge [
    source 16
    target 263
  ]
  edge [
    source 16
    target 264
  ]
  edge [
    source 16
    target 265
  ]
  edge [
    source 16
    target 266
  ]
  edge [
    source 16
    target 267
  ]
  edge [
    source 16
    target 268
  ]
  edge [
    source 16
    target 269
  ]
  edge [
    source 16
    target 270
  ]
  edge [
    source 16
    target 271
  ]
  edge [
    source 16
    target 272
  ]
  edge [
    source 16
    target 273
  ]
  edge [
    source 16
    target 274
  ]
  edge [
    source 16
    target 275
  ]
  edge [
    source 16
    target 276
  ]
  edge [
    source 16
    target 277
  ]
  edge [
    source 16
    target 278
  ]
  edge [
    source 16
    target 279
  ]
  edge [
    source 16
    target 280
  ]
  edge [
    source 16
    target 281
  ]
  edge [
    source 16
    target 282
  ]
  edge [
    source 16
    target 283
  ]
  edge [
    source 16
    target 284
  ]
  edge [
    source 16
    target 285
  ]
  edge [
    source 16
    target 286
  ]
  edge [
    source 16
    target 287
  ]
  edge [
    source 16
    target 288
  ]
  edge [
    source 16
    target 289
  ]
  edge [
    source 16
    target 290
  ]
  edge [
    source 16
    target 291
  ]
  edge [
    source 16
    target 292
  ]
  edge [
    source 16
    target 293
  ]
  edge [
    source 16
    target 294
  ]
  edge [
    source 16
    target 295
  ]
  edge [
    source 16
    target 296
  ]
  edge [
    source 16
    target 297
  ]
  edge [
    source 16
    target 298
  ]
  edge [
    source 16
    target 299
  ]
  edge [
    source 16
    target 300
  ]
  edge [
    source 16
    target 301
  ]
  edge [
    source 16
    target 302
  ]
  edge [
    source 16
    target 303
  ]
  edge [
    source 16
    target 304
  ]
  edge [
    source 16
    target 305
  ]
  edge [
    source 16
    target 306
  ]
  edge [
    source 16
    target 307
  ]
  edge [
    source 16
    target 308
  ]
  edge [
    source 16
    target 309
  ]
  edge [
    source 16
    target 310
  ]
  edge [
    source 16
    target 311
  ]
  edge [
    source 16
    target 312
  ]
  edge [
    source 16
    target 313
  ]
  edge [
    source 16
    target 314
  ]
  edge [
    source 16
    target 315
  ]
  edge [
    source 16
    target 316
  ]
  edge [
    source 16
    target 317
  ]
  edge [
    source 16
    target 318
  ]
  edge [
    source 16
    target 319
  ]
  edge [
    source 16
    target 320
  ]
  edge [
    source 16
    target 321
  ]
  edge [
    source 16
    target 322
  ]
  edge [
    source 16
    target 323
  ]
  edge [
    source 16
    target 324
  ]
  edge [
    source 16
    target 325
  ]
  edge [
    source 16
    target 326
  ]
  edge [
    source 16
    target 327
  ]
  edge [
    source 16
    target 328
  ]
  edge [
    source 16
    target 329
  ]
  edge [
    source 16
    target 330
  ]
  edge [
    source 16
    target 331
  ]
  edge [
    source 16
    target 332
  ]
  edge [
    source 16
    target 333
  ]
  edge [
    source 16
    target 334
  ]
  edge [
    source 16
    target 335
  ]
  edge [
    source 16
    target 336
  ]
  edge [
    source 16
    target 337
  ]
  edge [
    source 16
    target 338
  ]
  edge [
    source 16
    target 339
  ]
  edge [
    source 16
    target 340
  ]
  edge [
    source 16
    target 341
  ]
  edge [
    source 16
    target 342
  ]
  edge [
    source 16
    target 343
  ]
  edge [
    source 16
    target 344
  ]
  edge [
    source 16
    target 345
  ]
  edge [
    source 16
    target 346
  ]
  edge [
    source 16
    target 347
  ]
  edge [
    source 16
    target 348
  ]
  edge [
    source 16
    target 349
  ]
  edge [
    source 16
    target 350
  ]
  edge [
    source 16
    target 351
  ]
  edge [
    source 16
    target 352
  ]
  edge [
    source 16
    target 353
  ]
  edge [
    source 16
    target 354
  ]
  edge [
    source 16
    target 355
  ]
  edge [
    source 16
    target 356
  ]
  edge [
    source 16
    target 357
  ]
  edge [
    source 16
    target 358
  ]
  edge [
    source 16
    target 359
  ]
  edge [
    source 16
    target 360
  ]
  edge [
    source 16
    target 361
  ]
  edge [
    source 16
    target 362
  ]
  edge [
    source 16
    target 363
  ]
  edge [
    source 16
    target 364
  ]
  edge [
    source 16
    target 365
  ]
  edge [
    source 16
    target 366
  ]
  edge [
    source 16
    target 367
  ]
  edge [
    source 16
    target 368
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 387
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 389
  ]
  edge [
    source 16
    target 390
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 392
  ]
  edge [
    source 16
    target 393
  ]
  edge [
    source 16
    target 394
  ]
  edge [
    source 16
    target 395
  ]
  edge [
    source 16
    target 396
  ]
  edge [
    source 16
    target 397
  ]
  edge [
    source 16
    target 398
  ]
  edge [
    source 16
    target 399
  ]
  edge [
    source 16
    target 400
  ]
  edge [
    source 16
    target 401
  ]
  edge [
    source 16
    target 402
  ]
  edge [
    source 16
    target 403
  ]
  edge [
    source 16
    target 404
  ]
  edge [
    source 16
    target 405
  ]
  edge [
    source 16
    target 406
  ]
  edge [
    source 16
    target 407
  ]
  edge [
    source 16
    target 408
  ]
  edge [
    source 16
    target 409
  ]
  edge [
    source 16
    target 410
  ]
  edge [
    source 16
    target 411
  ]
  edge [
    source 16
    target 412
  ]
  edge [
    source 16
    target 413
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 416
  ]
  edge [
    source 16
    target 417
  ]
  edge [
    source 16
    target 418
  ]
  edge [
    source 16
    target 419
  ]
  edge [
    source 16
    target 420
  ]
  edge [
    source 16
    target 421
  ]
  edge [
    source 16
    target 422
  ]
  edge [
    source 16
    target 423
  ]
  edge [
    source 16
    target 424
  ]
  edge [
    source 16
    target 425
  ]
  edge [
    source 16
    target 426
  ]
  edge [
    source 16
    target 427
  ]
  edge [
    source 16
    target 428
  ]
  edge [
    source 16
    target 429
  ]
  edge [
    source 16
    target 430
  ]
  edge [
    source 16
    target 431
  ]
  edge [
    source 16
    target 432
  ]
  edge [
    source 16
    target 433
  ]
  edge [
    source 16
    target 434
  ]
  edge [
    source 16
    target 435
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 437
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 16
    target 439
  ]
  edge [
    source 16
    target 440
  ]
  edge [
    source 16
    target 441
  ]
  edge [
    source 16
    target 442
  ]
  edge [
    source 16
    target 443
  ]
  edge [
    source 16
    target 444
  ]
  edge [
    source 16
    target 445
  ]
  edge [
    source 16
    target 446
  ]
  edge [
    source 16
    target 447
  ]
  edge [
    source 16
    target 448
  ]
  edge [
    source 16
    target 449
  ]
  edge [
    source 16
    target 450
  ]
  edge [
    source 16
    target 451
  ]
  edge [
    source 16
    target 452
  ]
  edge [
    source 16
    target 453
  ]
  edge [
    source 16
    target 454
  ]
  edge [
    source 16
    target 455
  ]
  edge [
    source 16
    target 456
  ]
  edge [
    source 16
    target 457
  ]
  edge [
    source 16
    target 458
  ]
  edge [
    source 16
    target 459
  ]
  edge [
    source 16
    target 460
  ]
  edge [
    source 16
    target 461
  ]
  edge [
    source 16
    target 462
  ]
  edge [
    source 16
    target 463
  ]
  edge [
    source 16
    target 464
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 418
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 468
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 470
  ]
  edge [
    source 18
    target 471
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 34
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 472
  ]
  edge [
    source 19
    target 473
  ]
  edge [
    source 19
    target 474
  ]
  edge [
    source 19
    target 475
  ]
  edge [
    source 19
    target 476
  ]
  edge [
    source 19
    target 477
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 478
  ]
  edge [
    source 20
    target 479
  ]
  edge [
    source 20
    target 480
  ]
  edge [
    source 20
    target 481
  ]
  edge [
    source 20
    target 482
  ]
  edge [
    source 20
    target 483
  ]
  edge [
    source 21
    target 484
  ]
  edge [
    source 21
    target 485
  ]
  edge [
    source 21
    target 486
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 22
    target 487
  ]
  edge [
    source 22
    target 488
  ]
  edge [
    source 22
    target 489
  ]
  edge [
    source 22
    target 490
  ]
  edge [
    source 22
    target 491
  ]
  edge [
    source 22
    target 492
  ]
  edge [
    source 22
    target 493
  ]
  edge [
    source 22
    target 494
  ]
  edge [
    source 22
    target 495
  ]
  edge [
    source 22
    target 496
  ]
  edge [
    source 22
    target 497
  ]
  edge [
    source 22
    target 498
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 499
  ]
  edge [
    source 23
    target 500
  ]
  edge [
    source 23
    target 501
  ]
  edge [
    source 23
    target 502
  ]
  edge [
    source 23
    target 503
  ]
  edge [
    source 23
    target 504
  ]
  edge [
    source 23
    target 505
  ]
  edge [
    source 23
    target 506
  ]
  edge [
    source 23
    target 507
  ]
  edge [
    source 23
    target 508
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 509
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 510
  ]
  edge [
    source 25
    target 511
  ]
  edge [
    source 25
    target 512
  ]
  edge [
    source 25
    target 513
  ]
  edge [
    source 25
    target 514
  ]
  edge [
    source 25
    target 515
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 516
  ]
  edge [
    source 26
    target 517
  ]
  edge [
    source 26
    target 518
  ]
  edge [
    source 26
    target 519
  ]
  edge [
    source 26
    target 520
  ]
  edge [
    source 26
    target 111
  ]
  edge [
    source 26
    target 521
  ]
  edge [
    source 26
    target 522
  ]
  edge [
    source 26
    target 523
  ]
  edge [
    source 26
    target 524
  ]
  edge [
    source 26
    target 103
  ]
  edge [
    source 26
    target 525
  ]
  edge [
    source 26
    target 526
  ]
  edge [
    source 26
    target 527
  ]
  edge [
    source 26
    target 528
  ]
  edge [
    source 26
    target 529
  ]
  edge [
    source 26
    target 530
  ]
  edge [
    source 26
    target 531
  ]
  edge [
    source 26
    target 532
  ]
  edge [
    source 26
    target 533
  ]
  edge [
    source 26
    target 534
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 535
  ]
  edge [
    source 27
    target 536
  ]
  edge [
    source 27
    target 537
  ]
  edge [
    source 27
    target 538
  ]
  edge [
    source 27
    target 539
  ]
  edge [
    source 27
    target 540
  ]
  edge [
    source 27
    target 541
  ]
  edge [
    source 27
    target 542
  ]
  edge [
    source 27
    target 543
  ]
  edge [
    source 27
    target 544
  ]
  edge [
    source 27
    target 545
  ]
  edge [
    source 27
    target 546
  ]
  edge [
    source 27
    target 547
  ]
  edge [
    source 27
    target 548
  ]
  edge [
    source 27
    target 549
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 27
    target 550
  ]
  edge [
    source 27
    target 551
  ]
  edge [
    source 27
    target 552
  ]
  edge [
    source 27
    target 553
  ]
  edge [
    source 27
    target 554
  ]
  edge [
    source 27
    target 555
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 27
    target 556
  ]
  edge [
    source 27
    target 557
  ]
  edge [
    source 27
    target 558
  ]
  edge [
    source 27
    target 559
  ]
  edge [
    source 27
    target 560
  ]
  edge [
    source 27
    target 561
  ]
  edge [
    source 27
    target 562
  ]
  edge [
    source 27
    target 563
  ]
  edge [
    source 27
    target 564
  ]
  edge [
    source 27
    target 565
  ]
  edge [
    source 27
    target 566
  ]
  edge [
    source 27
    target 567
  ]
  edge [
    source 27
    target 568
  ]
  edge [
    source 27
    target 569
  ]
  edge [
    source 27
    target 570
  ]
  edge [
    source 27
    target 571
  ]
  edge [
    source 27
    target 572
  ]
  edge [
    source 27
    target 573
  ]
  edge [
    source 27
    target 574
  ]
  edge [
    source 27
    target 575
  ]
  edge [
    source 27
    target 576
  ]
  edge [
    source 27
    target 577
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 578
  ]
  edge [
    source 28
    target 579
  ]
  edge [
    source 28
    target 580
  ]
  edge [
    source 28
    target 581
  ]
  edge [
    source 29
    target 582
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 583
  ]
  edge [
    source 30
    target 584
  ]
  edge [
    source 30
    target 585
  ]
  edge [
    source 30
    target 586
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 587
  ]
  edge [
    source 31
    target 588
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 589
  ]
  edge [
    source 32
    target 590
  ]
  edge [
    source 32
    target 591
  ]
  edge [
    source 32
    target 592
  ]
  edge [
    source 32
    target 593
  ]
  edge [
    source 32
    target 594
  ]
  edge [
    source 32
    target 595
  ]
  edge [
    source 32
    target 596
  ]
  edge [
    source 32
    target 597
  ]
  edge [
    source 32
    target 598
  ]
  edge [
    source 32
    target 599
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 600
  ]
  edge [
    source 33
    target 601
  ]
  edge [
    source 34
    target 602
  ]
  edge [
    source 34
    target 603
  ]
  edge [
    source 34
    target 604
  ]
  edge [
    source 34
    target 605
  ]
  edge [
    source 34
    target 606
  ]
  edge [
    source 34
    target 607
  ]
  edge [
    source 34
    target 557
  ]
  edge [
    source 34
    target 608
  ]
  edge [
    source 34
    target 609
  ]
  edge [
    source 34
    target 610
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 611
  ]
  edge [
    source 35
    target 596
  ]
  edge [
    source 35
    target 612
  ]
  edge [
    source 35
    target 613
  ]
  edge [
    source 35
    target 614
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 615
  ]
  edge [
    source 36
    target 616
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 617
  ]
  edge [
    source 37
    target 618
  ]
  edge [
    source 37
    target 619
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 620
  ]
  edge [
    source 38
    target 621
  ]
  edge [
    source 38
    target 622
  ]
  edge [
    source 38
    target 623
  ]
  edge [
    source 38
    target 624
  ]
  edge [
    source 38
    target 625
  ]
  edge [
    source 38
    target 626
  ]
  edge [
    source 38
    target 627
  ]
  edge [
    source 38
    target 628
  ]
  edge [
    source 38
    target 629
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 630
  ]
  edge [
    source 39
    target 631
  ]
  edge [
    source 39
    target 632
  ]
  edge [
    source 39
    target 633
  ]
  edge [
    source 39
    target 634
  ]
  edge [
    source 39
    target 635
  ]
  edge [
    source 39
    target 636
  ]
  edge [
    source 39
    target 637
  ]
  edge [
    source 39
    target 638
  ]
  edge [
    source 39
    target 639
  ]
  edge [
    source 39
    target 640
  ]
  edge [
    source 39
    target 641
  ]
  edge [
    source 40
    target 642
  ]
  edge [
    source 40
    target 643
  ]
  edge [
    source 40
    target 644
  ]
  edge [
    source 40
    target 645
  ]
  edge [
    source 40
    target 646
  ]
  edge [
    source 40
    target 287
  ]
  edge [
    source 40
    target 647
  ]
  edge [
    source 40
    target 648
  ]
  edge [
    source 40
    target 649
  ]
  edge [
    source 40
    target 650
  ]
  edge [
    source 40
    target 651
  ]
  edge [
    source 40
    target 652
  ]
]
