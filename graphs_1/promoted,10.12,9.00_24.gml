graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.0161290322580645
  density 0.016391292945187517
  graphCliqueNumber 2
  node [
    id 0
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "akcja"
    origin "text"
  ]
  node [
    id 2
    label "zasypa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kartka"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wi&#261;teczny"
    origin "text"
  ]
  node [
    id 5
    label "pozytywny"
    origin "text"
  ]
  node [
    id 6
    label "przekaz"
    origin "text"
  ]
  node [
    id 7
    label "zaproszony"
    origin "text"
  ]
  node [
    id 8
    label "zabawa"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 11
    label "szczeg&#243;&#322;"
    origin "text"
  ]
  node [
    id 12
    label "wpis"
    origin "text"
  ]
  node [
    id 13
    label "planowa&#263;"
  ]
  node [
    id 14
    label "dostosowywa&#263;"
  ]
  node [
    id 15
    label "pozyskiwa&#263;"
  ]
  node [
    id 16
    label "wprowadza&#263;"
  ]
  node [
    id 17
    label "treat"
  ]
  node [
    id 18
    label "przygotowywa&#263;"
  ]
  node [
    id 19
    label "create"
  ]
  node [
    id 20
    label "ensnare"
  ]
  node [
    id 21
    label "tworzy&#263;"
  ]
  node [
    id 22
    label "standard"
  ]
  node [
    id 23
    label "skupia&#263;"
  ]
  node [
    id 24
    label "zagrywka"
  ]
  node [
    id 25
    label "czyn"
  ]
  node [
    id 26
    label "czynno&#347;&#263;"
  ]
  node [
    id 27
    label "wysoko&#347;&#263;"
  ]
  node [
    id 28
    label "stock"
  ]
  node [
    id 29
    label "gra"
  ]
  node [
    id 30
    label "w&#281;ze&#322;"
  ]
  node [
    id 31
    label "instrument_strunowy"
  ]
  node [
    id 32
    label "dywidenda"
  ]
  node [
    id 33
    label "przebieg"
  ]
  node [
    id 34
    label "udzia&#322;"
  ]
  node [
    id 35
    label "occupation"
  ]
  node [
    id 36
    label "jazda"
  ]
  node [
    id 37
    label "wydarzenie"
  ]
  node [
    id 38
    label "commotion"
  ]
  node [
    id 39
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 40
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 41
    label "operacja"
  ]
  node [
    id 42
    label "przykry&#263;"
  ]
  node [
    id 43
    label "stack"
  ]
  node [
    id 44
    label "wype&#322;ni&#263;"
  ]
  node [
    id 45
    label "przeznaczy&#263;"
  ]
  node [
    id 46
    label "throw"
  ]
  node [
    id 47
    label "wygada&#263;_si&#281;"
  ]
  node [
    id 48
    label "overwhelm"
  ]
  node [
    id 49
    label "przygnie&#347;&#263;"
  ]
  node [
    id 50
    label "carry"
  ]
  node [
    id 51
    label "ticket"
  ]
  node [
    id 52
    label "kartonik"
  ]
  node [
    id 53
    label "arkusz"
  ]
  node [
    id 54
    label "bon"
  ]
  node [
    id 55
    label "wk&#322;ad"
  ]
  node [
    id 56
    label "faul"
  ]
  node [
    id 57
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 58
    label "s&#281;dzia"
  ]
  node [
    id 59
    label "kara"
  ]
  node [
    id 60
    label "strona"
  ]
  node [
    id 61
    label "dzie&#324;_wolny"
  ]
  node [
    id 62
    label "&#347;wi&#281;tny"
  ]
  node [
    id 63
    label "&#347;wi&#261;tecznie"
  ]
  node [
    id 64
    label "wyj&#261;tkowy"
  ]
  node [
    id 65
    label "od&#347;wi&#281;tny"
  ]
  node [
    id 66
    label "od&#347;wi&#281;tnie"
  ]
  node [
    id 67
    label "obrz&#281;dowy"
  ]
  node [
    id 68
    label "uroczysty"
  ]
  node [
    id 69
    label "przyjemny"
  ]
  node [
    id 70
    label "po&#380;&#261;dany"
  ]
  node [
    id 71
    label "dobrze"
  ]
  node [
    id 72
    label "fajny"
  ]
  node [
    id 73
    label "pozytywnie"
  ]
  node [
    id 74
    label "dodatnio"
  ]
  node [
    id 75
    label "dokument"
  ]
  node [
    id 76
    label "znaczenie"
  ]
  node [
    id 77
    label "implicite"
  ]
  node [
    id 78
    label "transakcja"
  ]
  node [
    id 79
    label "order"
  ]
  node [
    id 80
    label "explicite"
  ]
  node [
    id 81
    label "draft"
  ]
  node [
    id 82
    label "proces"
  ]
  node [
    id 83
    label "kwota"
  ]
  node [
    id 84
    label "blankiet"
  ]
  node [
    id 85
    label "tekst"
  ]
  node [
    id 86
    label "uczestnik"
  ]
  node [
    id 87
    label "wodzirej"
  ]
  node [
    id 88
    label "rozrywka"
  ]
  node [
    id 89
    label "nabawienie_si&#281;"
  ]
  node [
    id 90
    label "cecha"
  ]
  node [
    id 91
    label "gambling"
  ]
  node [
    id 92
    label "taniec"
  ]
  node [
    id 93
    label "impreza"
  ]
  node [
    id 94
    label "igraszka"
  ]
  node [
    id 95
    label "igra"
  ]
  node [
    id 96
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 97
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 98
    label "game"
  ]
  node [
    id 99
    label "ubaw"
  ]
  node [
    id 100
    label "chwyt"
  ]
  node [
    id 101
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 102
    label "si&#281;ga&#263;"
  ]
  node [
    id 103
    label "trwa&#263;"
  ]
  node [
    id 104
    label "obecno&#347;&#263;"
  ]
  node [
    id 105
    label "stan"
  ]
  node [
    id 106
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 107
    label "stand"
  ]
  node [
    id 108
    label "mie&#263;_miejsce"
  ]
  node [
    id 109
    label "uczestniczy&#263;"
  ]
  node [
    id 110
    label "chodzi&#263;"
  ]
  node [
    id 111
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 112
    label "equal"
  ]
  node [
    id 113
    label "jaki&#347;"
  ]
  node [
    id 114
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 115
    label "niuansowa&#263;"
  ]
  node [
    id 116
    label "zniuansowa&#263;"
  ]
  node [
    id 117
    label "element"
  ]
  node [
    id 118
    label "sk&#322;adnik"
  ]
  node [
    id 119
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 120
    label "entrance"
  ]
  node [
    id 121
    label "inscription"
  ]
  node [
    id 122
    label "akt"
  ]
  node [
    id 123
    label "op&#322;ata"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 85
  ]
]
