graph [
  maxDegree 75
  minDegree 1
  meanDegree 2.282003710575139
  density 0.002118852098955561
  graphCliqueNumber 5
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "nie&#378;le"
    origin "text"
  ]
  node [
    id 2
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "upowszechnienie"
    origin "text"
  ]
  node [
    id 4
    label "odkrycie"
    origin "text"
  ]
  node [
    id 5
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 6
    label "murchison"
    origin "text"
  ]
  node [
    id 7
    label "cios"
    origin "text"
  ]
  node [
    id 8
    label "kto"
    origin "text"
  ]
  node [
    id 9
    label "za&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 10
    label "by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "poda&#263;"
    origin "text"
  ]
  node [
    id 12
    label "jako"
    origin "text"
  ]
  node [
    id 13
    label "pierwsza"
    origin "text"
  ]
  node [
    id 14
    label "polski"
    origin "text"
  ]
  node [
    id 15
    label "media"
    origin "text"
  ]
  node [
    id 16
    label "ale"
    origin "text"
  ]
  node [
    id 17
    label "najpierw"
    origin "text"
  ]
  node [
    id 18
    label "dwa"
    origin "text"
  ]
  node [
    id 19
    label "s&#322;owo"
    origin "text"
  ]
  node [
    id 20
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 21
    label "s&#322;awny"
    origin "text"
  ]
  node [
    id 22
    label "w&#322;adca"
    origin "text"
  ]
  node [
    id 23
    label "meksyk"
    origin "text"
  ]
  node [
    id 24
    label "montezuma"
    origin "text"
  ]
  node [
    id 25
    label "g&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 26
    label "legenda"
    origin "text"
  ]
  node [
    id 27
    label "zem&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 28
    label "si&#281;"
    origin "text"
  ]
  node [
    id 29
    label "konkwistador"
    origin "text"
  ]
  node [
    id 30
    label "umiera&#263;"
    origin "text"
  ]
  node [
    id 31
    label "seryjnie"
    origin "text"
  ]
  node [
    id 32
    label "m&#281;czarnia"
    origin "text"
  ]
  node [
    id 33
    label "potworny"
    origin "text"
  ]
  node [
    id 34
    label "biegunka"
    origin "text"
  ]
  node [
    id 35
    label "wymiot"
    origin "text"
  ]
  node [
    id 36
    label "choroba"
    origin "text"
  ]
  node [
    id 37
    label "znana"
    origin "text"
  ]
  node [
    id 38
    label "turysta"
    origin "text"
  ]
  node [
    id 39
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 40
    label "tylko"
    origin "text"
  ]
  node [
    id 41
    label "tym"
    origin "text"
  ]
  node [
    id 42
    label "odwiedza&#263;"
    origin "text"
  ]
  node [
    id 43
    label "nosi&#263;"
    origin "text"
  ]
  node [
    id 44
    label "popularny"
    origin "text"
  ]
  node [
    id 45
    label "nazwa"
    origin "text"
  ]
  node [
    id 46
    label "rozpowszechni&#263;"
    origin "text"
  ]
  node [
    id 47
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 48
    label "zaka&#378;ny"
    origin "text"
  ]
  node [
    id 49
    label "malari"
    origin "text"
  ]
  node [
    id 50
    label "rok"
    origin "text"
  ]
  node [
    id 51
    label "milion"
    origin "text"
  ]
  node [
    id 52
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 53
    label "te&#380;"
    origin "text"
  ]
  node [
    id 54
    label "gruby"
    origin "text"
  ]
  node [
    id 55
    label "przyczyna"
    origin "text"
  ]
  node [
    id 56
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 57
    label "maja"
    origin "text"
  ]
  node [
    id 58
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 59
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 60
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 61
    label "aztek"
    origin "text"
  ]
  node [
    id 62
    label "bakteryjny"
    origin "text"
  ]
  node [
    id 63
    label "zanieczyszczenie"
    origin "text"
  ]
  node [
    id 64
    label "woda"
    origin "text"
  ]
  node [
    id 65
    label "niemniej"
    origin "text"
  ]
  node [
    id 66
    label "sam"
    origin "text"
  ]
  node [
    id 67
    label "istota"
    origin "text"
  ]
  node [
    id 68
    label "zachorowa&#263;"
    origin "text"
  ]
  node [
    id 69
    label "mechanizm"
    origin "text"
  ]
  node [
    id 70
    label "pokonywa&#263;"
    origin "text"
  ]
  node [
    id 71
    label "przez"
    origin "text"
  ]
  node [
    id 72
    label "bakteria"
    origin "text"
  ]
  node [
    id 73
    label "uk&#322;ad"
    origin "text"
  ]
  node [
    id 74
    label "odporno&#347;ciowy"
    origin "text"
  ]
  node [
    id 75
    label "pozostawa&#263;"
    origin "text"
  ]
  node [
    id 76
    label "tajemnica"
    origin "text"
  ]
  node [
    id 77
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 78
    label "donosi&#263;"
    origin "text"
  ]
  node [
    id 79
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 80
    label "dzia&#322;"
    origin "text"
  ]
  node [
    id 81
    label "news"
    origin "text"
  ]
  node [
    id 82
    label "przywo&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 83
    label "niejaki"
    origin "text"
  ]
  node [
    id 84
    label "powodzenie"
    origin "text"
  ]
  node [
    id 85
    label "serwis"
    origin "text"
  ]
  node [
    id 86
    label "eurekalert"
    origin "text"
  ]
  node [
    id 87
    label "uczony"
    origin "text"
  ]
  node [
    id 88
    label "johns"
    origin "text"
  ]
  node [
    id 89
    label "hopkins"
    origin "text"
  ]
  node [
    id 90
    label "university"
    origin "text"
  ]
  node [
    id 91
    label "odkry&#263;"
    origin "text"
  ]
  node [
    id 92
    label "enzym"
    origin "text"
  ]
  node [
    id 93
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 94
    label "umo&#380;liwia&#263;"
    origin "text"
  ]
  node [
    id 95
    label "mikroorganizm"
    origin "text"
  ]
  node [
    id 96
    label "os&#322;abienie"
    origin "text"
  ]
  node [
    id 97
    label "ludzki"
    origin "text"
  ]
  node [
    id 98
    label "bariera"
    origin "text"
  ]
  node [
    id 99
    label "nale&#380;&#261;cy"
    origin "text"
  ]
  node [
    id 100
    label "pewien"
    origin "text"
  ]
  node [
    id 101
    label "specjalny"
    origin "text"
  ]
  node [
    id 102
    label "dobrze"
    origin "text"
  ]
  node [
    id 103
    label "nauka"
    origin "text"
  ]
  node [
    id 104
    label "grupa"
    origin "text"
  ]
  node [
    id 105
    label "wyst&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 106
    label "organizm"
    origin "text"
  ]
  node [
    id 107
    label "wszyscy"
    origin "text"
  ]
  node [
    id 108
    label "&#380;ywy"
    origin "text"
  ]
  node [
    id 109
    label "kilka"
    origin "text"
  ]
  node [
    id 110
    label "lata"
    origin "text"
  ]
  node [
    id 111
    label "temu"
    origin "text"
  ]
  node [
    id 112
    label "sin"
    origin "text"
  ]
  node [
    id 113
    label "urban"
    origin "text"
  ]
  node [
    id 114
    label "adiunkt"
    origin "text"
  ]
  node [
    id 115
    label "ten"
    origin "text"
  ]
  node [
    id 116
    label "odpowiada&#263;"
    origin "text"
  ]
  node [
    id 117
    label "u&#322;atwia&#263;"
    origin "text"
  ]
  node [
    id 118
    label "zachorowa&#324;"
    origin "text"
  ]
  node [
    id 119
    label "malaria"
    origin "text"
  ]
  node [
    id 120
    label "zdopingowa&#263;"
    origin "text"
  ]
  node [
    id 121
    label "poszukiwania"
    origin "text"
  ]
  node [
    id 122
    label "podobny"
    origin "text"
  ]
  node [
    id 123
    label "sytuacja"
    origin "text"
  ]
  node [
    id 124
    label "przy"
    origin "text"
  ]
  node [
    id 125
    label "inny"
    origin "text"
  ]
  node [
    id 126
    label "tak"
    origin "text"
  ]
  node [
    id 127
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 128
    label "dotrze&#263;"
    origin "text"
  ]
  node [
    id 129
    label "montezumy"
    origin "text"
  ]
  node [
    id 130
    label "byd&#322;o"
  ]
  node [
    id 131
    label "zobo"
  ]
  node [
    id 132
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 133
    label "yakalo"
  ]
  node [
    id 134
    label "dzo"
  ]
  node [
    id 135
    label "sporo"
  ]
  node [
    id 136
    label "pozytywnie"
  ]
  node [
    id 137
    label "intensywnie"
  ]
  node [
    id 138
    label "nieszpetnie"
  ]
  node [
    id 139
    label "niez&#322;y"
  ]
  node [
    id 140
    label "skutecznie"
  ]
  node [
    id 141
    label "opu&#347;ci&#263;"
  ]
  node [
    id 142
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 143
    label "proceed"
  ]
  node [
    id 144
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 145
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 146
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 147
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 148
    label "zacz&#261;&#263;"
  ]
  node [
    id 149
    label "zmieni&#263;"
  ]
  node [
    id 150
    label "zosta&#263;"
  ]
  node [
    id 151
    label "sail"
  ]
  node [
    id 152
    label "leave"
  ]
  node [
    id 153
    label "uda&#263;_si&#281;"
  ]
  node [
    id 154
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 155
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 156
    label "zrobi&#263;"
  ]
  node [
    id 157
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 158
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 159
    label "przyj&#261;&#263;"
  ]
  node [
    id 160
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 161
    label "become"
  ]
  node [
    id 162
    label "play_along"
  ]
  node [
    id 163
    label "travel"
  ]
  node [
    id 164
    label "zrobienie"
  ]
  node [
    id 165
    label "spowodowanie"
  ]
  node [
    id 166
    label "doj&#347;cie"
  ]
  node [
    id 167
    label "propagation"
  ]
  node [
    id 168
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 169
    label "objawienie"
  ]
  node [
    id 170
    label "jawny"
  ]
  node [
    id 171
    label "zsuni&#281;cie"
  ]
  node [
    id 172
    label "znalezienie"
  ]
  node [
    id 173
    label "poinformowanie"
  ]
  node [
    id 174
    label "novum"
  ]
  node [
    id 175
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 176
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 177
    label "ukazanie"
  ]
  node [
    id 178
    label "disclosure"
  ]
  node [
    id 179
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 180
    label "poznanie"
  ]
  node [
    id 181
    label "podniesienie"
  ]
  node [
    id 182
    label "detection"
  ]
  node [
    id 183
    label "discovery"
  ]
  node [
    id 184
    label "niespodzianka"
  ]
  node [
    id 185
    label "energy"
  ]
  node [
    id 186
    label "czas"
  ]
  node [
    id 187
    label "bycie"
  ]
  node [
    id 188
    label "zegar_biologiczny"
  ]
  node [
    id 189
    label "okres_noworodkowy"
  ]
  node [
    id 190
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 191
    label "entity"
  ]
  node [
    id 192
    label "prze&#380;ywanie"
  ]
  node [
    id 193
    label "prze&#380;ycie"
  ]
  node [
    id 194
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 195
    label "wiek_matuzalemowy"
  ]
  node [
    id 196
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 197
    label "dzieci&#324;stwo"
  ]
  node [
    id 198
    label "power"
  ]
  node [
    id 199
    label "szwung"
  ]
  node [
    id 200
    label "menopauza"
  ]
  node [
    id 201
    label "umarcie"
  ]
  node [
    id 202
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 203
    label "life"
  ]
  node [
    id 204
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 205
    label "rozw&#243;j"
  ]
  node [
    id 206
    label "po&#322;&#243;g"
  ]
  node [
    id 207
    label "byt"
  ]
  node [
    id 208
    label "przebywanie"
  ]
  node [
    id 209
    label "subsistence"
  ]
  node [
    id 210
    label "koleje_losu"
  ]
  node [
    id 211
    label "raj_utracony"
  ]
  node [
    id 212
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 213
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 214
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 215
    label "andropauza"
  ]
  node [
    id 216
    label "warunki"
  ]
  node [
    id 217
    label "do&#380;ywanie"
  ]
  node [
    id 218
    label "niemowl&#281;ctwo"
  ]
  node [
    id 219
    label "umieranie"
  ]
  node [
    id 220
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 221
    label "staro&#347;&#263;"
  ]
  node [
    id 222
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 223
    label "&#347;mier&#263;"
  ]
  node [
    id 224
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 225
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 226
    label "shot"
  ]
  node [
    id 227
    label "struktura_geologiczna"
  ]
  node [
    id 228
    label "uderzenie"
  ]
  node [
    id 229
    label "siekacz"
  ]
  node [
    id 230
    label "blok"
  ]
  node [
    id 231
    label "pr&#243;ba"
  ]
  node [
    id 232
    label "coup"
  ]
  node [
    id 233
    label "time"
  ]
  node [
    id 234
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 235
    label "insert"
  ]
  node [
    id 236
    label "utworzy&#263;"
  ]
  node [
    id 237
    label "ubra&#263;"
  ]
  node [
    id 238
    label "set"
  ]
  node [
    id 239
    label "invest"
  ]
  node [
    id 240
    label "pokry&#263;"
  ]
  node [
    id 241
    label "przewidzie&#263;"
  ]
  node [
    id 242
    label "umie&#347;ci&#263;"
  ]
  node [
    id 243
    label "map"
  ]
  node [
    id 244
    label "load"
  ]
  node [
    id 245
    label "zap&#322;aci&#263;"
  ]
  node [
    id 246
    label "oblec_si&#281;"
  ]
  node [
    id 247
    label "podwin&#261;&#263;"
  ]
  node [
    id 248
    label "plant"
  ]
  node [
    id 249
    label "create"
  ]
  node [
    id 250
    label "str&#243;j"
  ]
  node [
    id 251
    label "jell"
  ]
  node [
    id 252
    label "spowodowa&#263;"
  ]
  node [
    id 253
    label "oblec"
  ]
  node [
    id 254
    label "przyodzia&#263;"
  ]
  node [
    id 255
    label "install"
  ]
  node [
    id 256
    label "si&#281;ga&#263;"
  ]
  node [
    id 257
    label "trwa&#263;"
  ]
  node [
    id 258
    label "obecno&#347;&#263;"
  ]
  node [
    id 259
    label "stan"
  ]
  node [
    id 260
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 261
    label "stand"
  ]
  node [
    id 262
    label "mie&#263;_miejsce"
  ]
  node [
    id 263
    label "uczestniczy&#263;"
  ]
  node [
    id 264
    label "chodzi&#263;"
  ]
  node [
    id 265
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 266
    label "equal"
  ]
  node [
    id 267
    label "tenis"
  ]
  node [
    id 268
    label "da&#263;"
  ]
  node [
    id 269
    label "siatk&#243;wka"
  ]
  node [
    id 270
    label "introduce"
  ]
  node [
    id 271
    label "jedzenie"
  ]
  node [
    id 272
    label "zaserwowa&#263;"
  ]
  node [
    id 273
    label "give"
  ]
  node [
    id 274
    label "ustawi&#263;"
  ]
  node [
    id 275
    label "zagra&#263;"
  ]
  node [
    id 276
    label "supply"
  ]
  node [
    id 277
    label "nafaszerowa&#263;"
  ]
  node [
    id 278
    label "poinformowa&#263;"
  ]
  node [
    id 279
    label "godzina"
  ]
  node [
    id 280
    label "lacki"
  ]
  node [
    id 281
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 282
    label "przedmiot"
  ]
  node [
    id 283
    label "sztajer"
  ]
  node [
    id 284
    label "drabant"
  ]
  node [
    id 285
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 286
    label "polak"
  ]
  node [
    id 287
    label "pierogi_ruskie"
  ]
  node [
    id 288
    label "krakowiak"
  ]
  node [
    id 289
    label "Polish"
  ]
  node [
    id 290
    label "j&#281;zyk"
  ]
  node [
    id 291
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 292
    label "oberek"
  ]
  node [
    id 293
    label "po_polsku"
  ]
  node [
    id 294
    label "mazur"
  ]
  node [
    id 295
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 296
    label "chodzony"
  ]
  node [
    id 297
    label "skoczny"
  ]
  node [
    id 298
    label "ryba_po_grecku"
  ]
  node [
    id 299
    label "goniony"
  ]
  node [
    id 300
    label "polsko"
  ]
  node [
    id 301
    label "przekazior"
  ]
  node [
    id 302
    label "mass-media"
  ]
  node [
    id 303
    label "uzbrajanie"
  ]
  node [
    id 304
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 305
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 306
    label "medium"
  ]
  node [
    id 307
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 308
    label "piwo"
  ]
  node [
    id 309
    label "wcze&#347;niej"
  ]
  node [
    id 310
    label "nasamprz&#243;d"
  ]
  node [
    id 311
    label "pocz&#261;tkowo"
  ]
  node [
    id 312
    label "pierwiej"
  ]
  node [
    id 313
    label "pierw"
  ]
  node [
    id 314
    label "obietnica"
  ]
  node [
    id 315
    label "bit"
  ]
  node [
    id 316
    label "s&#322;ownictwo"
  ]
  node [
    id 317
    label "jednostka_leksykalna"
  ]
  node [
    id 318
    label "pisanie_si&#281;"
  ]
  node [
    id 319
    label "wykrzyknik"
  ]
  node [
    id 320
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 321
    label "pole_semantyczne"
  ]
  node [
    id 322
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 323
    label "komunikat"
  ]
  node [
    id 324
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 325
    label "wypowiedzenie"
  ]
  node [
    id 326
    label "nag&#322;os"
  ]
  node [
    id 327
    label "wordnet"
  ]
  node [
    id 328
    label "morfem"
  ]
  node [
    id 329
    label "czasownik"
  ]
  node [
    id 330
    label "wyg&#322;os"
  ]
  node [
    id 331
    label "jednostka_informacji"
  ]
  node [
    id 332
    label "rynek"
  ]
  node [
    id 333
    label "issue"
  ]
  node [
    id 334
    label "evocation"
  ]
  node [
    id 335
    label "wst&#281;p"
  ]
  node [
    id 336
    label "nuklearyzacja"
  ]
  node [
    id 337
    label "umo&#380;liwienie"
  ]
  node [
    id 338
    label "zacz&#281;cie"
  ]
  node [
    id 339
    label "wpisanie"
  ]
  node [
    id 340
    label "zapoznanie"
  ]
  node [
    id 341
    label "czynno&#347;&#263;"
  ]
  node [
    id 342
    label "entrance"
  ]
  node [
    id 343
    label "wej&#347;cie"
  ]
  node [
    id 344
    label "podstawy"
  ]
  node [
    id 345
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 346
    label "w&#322;&#261;czenie"
  ]
  node [
    id 347
    label "doprowadzenie"
  ]
  node [
    id 348
    label "przewietrzenie"
  ]
  node [
    id 349
    label "deduction"
  ]
  node [
    id 350
    label "umieszczenie"
  ]
  node [
    id 351
    label "ws&#322;awianie_si&#281;"
  ]
  node [
    id 352
    label "ws&#322;awienie_si&#281;"
  ]
  node [
    id 353
    label "ws&#322;awienie"
  ]
  node [
    id 354
    label "znany"
  ]
  node [
    id 355
    label "os&#322;awiony"
  ]
  node [
    id 356
    label "ws&#322;awianie"
  ]
  node [
    id 357
    label "w&#322;odarz"
  ]
  node [
    id 358
    label "Midas"
  ]
  node [
    id 359
    label "rz&#261;dzenie"
  ]
  node [
    id 360
    label "przyw&#243;dca"
  ]
  node [
    id 361
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 362
    label "Mieszko_I"
  ]
  node [
    id 363
    label "ba&#322;agan"
  ]
  node [
    id 364
    label "wypowiada&#263;"
  ]
  node [
    id 365
    label "szczeka&#263;"
  ]
  node [
    id 366
    label "rozpowszechnia&#263;"
  ]
  node [
    id 367
    label "publicize"
  ]
  node [
    id 368
    label "rumor"
  ]
  node [
    id 369
    label "talk"
  ]
  node [
    id 370
    label "pies_my&#347;liwski"
  ]
  node [
    id 371
    label "legend"
  ]
  node [
    id 372
    label "napis"
  ]
  node [
    id 373
    label "utw&#243;r_programowy"
  ]
  node [
    id 374
    label "obja&#347;nienie"
  ]
  node [
    id 375
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 376
    label "mapa"
  ]
  node [
    id 377
    label "opowie&#347;&#263;"
  ]
  node [
    id 378
    label "miniatura"
  ]
  node [
    id 379
    label "pogl&#261;d"
  ]
  node [
    id 380
    label "s&#322;awa"
  ]
  node [
    id 381
    label "narrative"
  ]
  node [
    id 382
    label "fantastyka"
  ]
  node [
    id 383
    label "Ma&#322;ysz"
  ]
  node [
    id 384
    label "luminarz"
  ]
  node [
    id 385
    label "zdobywca"
  ]
  node [
    id 386
    label "pada&#263;"
  ]
  node [
    id 387
    label "gasn&#261;&#263;"
  ]
  node [
    id 388
    label "zanika&#263;"
  ]
  node [
    id 389
    label "przestawa&#263;"
  ]
  node [
    id 390
    label "die"
  ]
  node [
    id 391
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 392
    label "naprawd&#281;"
  ]
  node [
    id 393
    label "masowo"
  ]
  node [
    id 394
    label "consecutive"
  ]
  node [
    id 395
    label "successively"
  ]
  node [
    id 396
    label "seryjny"
  ]
  node [
    id 397
    label "nieoryginalnie"
  ]
  node [
    id 398
    label "powtarzalnie"
  ]
  node [
    id 399
    label "powa&#380;nie"
  ]
  node [
    id 400
    label "czy&#347;ciec"
  ]
  node [
    id 401
    label "trud"
  ]
  node [
    id 402
    label "drudgery"
  ]
  node [
    id 403
    label "cierpienie"
  ]
  node [
    id 404
    label "okropny"
  ]
  node [
    id 405
    label "z&#322;y"
  ]
  node [
    id 406
    label "straszny"
  ]
  node [
    id 407
    label "potwornie"
  ]
  node [
    id 408
    label "straszliwie"
  ]
  node [
    id 409
    label "przera&#378;liwie"
  ]
  node [
    id 410
    label "przeczyszczanie"
  ]
  node [
    id 411
    label "sraczka"
  ]
  node [
    id 412
    label "przeczy&#347;ci&#263;"
  ]
  node [
    id 413
    label "oznaka"
  ]
  node [
    id 414
    label "katar_kiszek"
  ]
  node [
    id 415
    label "rotawirus"
  ]
  node [
    id 416
    label "biega&#263;"
  ]
  node [
    id 417
    label "przeczyszcza&#263;"
  ]
  node [
    id 418
    label "przeczyszczenie"
  ]
  node [
    id 419
    label "ognisko"
  ]
  node [
    id 420
    label "odezwanie_si&#281;"
  ]
  node [
    id 421
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 422
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 423
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 424
    label "przypadek"
  ]
  node [
    id 425
    label "zajmowa&#263;"
  ]
  node [
    id 426
    label "zajmowanie"
  ]
  node [
    id 427
    label "badanie_histopatologiczne"
  ]
  node [
    id 428
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 429
    label "atakowanie"
  ]
  node [
    id 430
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 431
    label "bol&#261;czka"
  ]
  node [
    id 432
    label "remisja"
  ]
  node [
    id 433
    label "grupa_ryzyka"
  ]
  node [
    id 434
    label "atakowa&#263;"
  ]
  node [
    id 435
    label "kryzys"
  ]
  node [
    id 436
    label "nabawienie_si&#281;"
  ]
  node [
    id 437
    label "chor&#243;bka"
  ]
  node [
    id 438
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 439
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 440
    label "inkubacja"
  ]
  node [
    id 441
    label "powalenie"
  ]
  node [
    id 442
    label "cholera"
  ]
  node [
    id 443
    label "nabawianie_si&#281;"
  ]
  node [
    id 444
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 445
    label "odzywanie_si&#281;"
  ]
  node [
    id 446
    label "diagnoza"
  ]
  node [
    id 447
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 448
    label "powali&#263;"
  ]
  node [
    id 449
    label "zaburzenie"
  ]
  node [
    id 450
    label "podr&#243;&#380;nik"
  ]
  node [
    id 451
    label "doba"
  ]
  node [
    id 452
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 453
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 454
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 455
    label "inflict"
  ]
  node [
    id 456
    label "przybywa&#263;"
  ]
  node [
    id 457
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 458
    label "mie&#263;"
  ]
  node [
    id 459
    label "wk&#322;ada&#263;"
  ]
  node [
    id 460
    label "przemieszcza&#263;"
  ]
  node [
    id 461
    label "posiada&#263;"
  ]
  node [
    id 462
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 463
    label "wear"
  ]
  node [
    id 464
    label "carry"
  ]
  node [
    id 465
    label "przyst&#281;pny"
  ]
  node [
    id 466
    label "&#322;atwy"
  ]
  node [
    id 467
    label "popularnie"
  ]
  node [
    id 468
    label "term"
  ]
  node [
    id 469
    label "wezwanie"
  ]
  node [
    id 470
    label "leksem"
  ]
  node [
    id 471
    label "patron"
  ]
  node [
    id 472
    label "exsert"
  ]
  node [
    id 473
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 474
    label "obszar"
  ]
  node [
    id 475
    label "obiekt_naturalny"
  ]
  node [
    id 476
    label "Stary_&#346;wiat"
  ]
  node [
    id 477
    label "stw&#243;r"
  ]
  node [
    id 478
    label "biosfera"
  ]
  node [
    id 479
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 480
    label "rzecz"
  ]
  node [
    id 481
    label "magnetosfera"
  ]
  node [
    id 482
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 483
    label "environment"
  ]
  node [
    id 484
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 485
    label "geosfera"
  ]
  node [
    id 486
    label "Nowy_&#346;wiat"
  ]
  node [
    id 487
    label "planeta"
  ]
  node [
    id 488
    label "przejmowa&#263;"
  ]
  node [
    id 489
    label "litosfera"
  ]
  node [
    id 490
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 491
    label "makrokosmos"
  ]
  node [
    id 492
    label "barysfera"
  ]
  node [
    id 493
    label "biota"
  ]
  node [
    id 494
    label "p&#243;&#322;noc"
  ]
  node [
    id 495
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 496
    label "fauna"
  ]
  node [
    id 497
    label "wszechstworzenie"
  ]
  node [
    id 498
    label "geotermia"
  ]
  node [
    id 499
    label "biegun"
  ]
  node [
    id 500
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 501
    label "ekosystem"
  ]
  node [
    id 502
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 503
    label "teren"
  ]
  node [
    id 504
    label "zjawisko"
  ]
  node [
    id 505
    label "p&#243;&#322;kula"
  ]
  node [
    id 506
    label "atmosfera"
  ]
  node [
    id 507
    label "mikrokosmos"
  ]
  node [
    id 508
    label "class"
  ]
  node [
    id 509
    label "po&#322;udnie"
  ]
  node [
    id 510
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 511
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 512
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 513
    label "przejmowanie"
  ]
  node [
    id 514
    label "przestrze&#324;"
  ]
  node [
    id 515
    label "asymilowanie_si&#281;"
  ]
  node [
    id 516
    label "przej&#261;&#263;"
  ]
  node [
    id 517
    label "ekosfera"
  ]
  node [
    id 518
    label "przyroda"
  ]
  node [
    id 519
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 520
    label "ciemna_materia"
  ]
  node [
    id 521
    label "geoida"
  ]
  node [
    id 522
    label "Wsch&#243;d"
  ]
  node [
    id 523
    label "populace"
  ]
  node [
    id 524
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 525
    label "huczek"
  ]
  node [
    id 526
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 527
    label "Ziemia"
  ]
  node [
    id 528
    label "universe"
  ]
  node [
    id 529
    label "ozonosfera"
  ]
  node [
    id 530
    label "rze&#378;ba"
  ]
  node [
    id 531
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 532
    label "zagranica"
  ]
  node [
    id 533
    label "hydrosfera"
  ]
  node [
    id 534
    label "kuchnia"
  ]
  node [
    id 535
    label "przej&#281;cie"
  ]
  node [
    id 536
    label "czarna_dziura"
  ]
  node [
    id 537
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 538
    label "morze"
  ]
  node [
    id 539
    label "zara&#378;liwie"
  ]
  node [
    id 540
    label "stulecie"
  ]
  node [
    id 541
    label "kalendarz"
  ]
  node [
    id 542
    label "pora_roku"
  ]
  node [
    id 543
    label "cykl_astronomiczny"
  ]
  node [
    id 544
    label "p&#243;&#322;rocze"
  ]
  node [
    id 545
    label "kwarta&#322;"
  ]
  node [
    id 546
    label "kurs"
  ]
  node [
    id 547
    label "jubileusz"
  ]
  node [
    id 548
    label "miesi&#261;c"
  ]
  node [
    id 549
    label "martwy_sezon"
  ]
  node [
    id 550
    label "liczba"
  ]
  node [
    id 551
    label "miljon"
  ]
  node [
    id 552
    label "ba&#324;ka"
  ]
  node [
    id 553
    label "asymilowa&#263;"
  ]
  node [
    id 554
    label "wapniak"
  ]
  node [
    id 555
    label "dwun&#243;g"
  ]
  node [
    id 556
    label "polifag"
  ]
  node [
    id 557
    label "wz&#243;r"
  ]
  node [
    id 558
    label "profanum"
  ]
  node [
    id 559
    label "hominid"
  ]
  node [
    id 560
    label "homo_sapiens"
  ]
  node [
    id 561
    label "nasada"
  ]
  node [
    id 562
    label "podw&#322;adny"
  ]
  node [
    id 563
    label "ludzko&#347;&#263;"
  ]
  node [
    id 564
    label "os&#322;abianie"
  ]
  node [
    id 565
    label "portrecista"
  ]
  node [
    id 566
    label "duch"
  ]
  node [
    id 567
    label "oddzia&#322;ywanie"
  ]
  node [
    id 568
    label "g&#322;owa"
  ]
  node [
    id 569
    label "asymilowanie"
  ]
  node [
    id 570
    label "osoba"
  ]
  node [
    id 571
    label "os&#322;abia&#263;"
  ]
  node [
    id 572
    label "figura"
  ]
  node [
    id 573
    label "Adam"
  ]
  node [
    id 574
    label "senior"
  ]
  node [
    id 575
    label "antropochoria"
  ]
  node [
    id 576
    label "posta&#263;"
  ]
  node [
    id 577
    label "du&#380;y"
  ]
  node [
    id 578
    label "pasienie"
  ]
  node [
    id 579
    label "g&#243;ra_t&#322;uszczu"
  ]
  node [
    id 580
    label "prostacki"
  ]
  node [
    id 581
    label "na_t&#322;usto"
  ]
  node [
    id 582
    label "niski"
  ]
  node [
    id 583
    label "niegrzeczny"
  ]
  node [
    id 584
    label "grubo"
  ]
  node [
    id 585
    label "tubalnie"
  ]
  node [
    id 586
    label "grubienie"
  ]
  node [
    id 587
    label "fajny"
  ]
  node [
    id 588
    label "grubia&#324;sko"
  ]
  node [
    id 589
    label "beka"
  ]
  node [
    id 590
    label "oblany"
  ]
  node [
    id 591
    label "spasienie"
  ]
  node [
    id 592
    label "ciep&#322;y"
  ]
  node [
    id 593
    label "liczny"
  ]
  node [
    id 594
    label "zgrubienie"
  ]
  node [
    id 595
    label "matuszka"
  ]
  node [
    id 596
    label "geneza"
  ]
  node [
    id 597
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 598
    label "czynnik"
  ]
  node [
    id 599
    label "poci&#261;ganie"
  ]
  node [
    id 600
    label "rezultat"
  ]
  node [
    id 601
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 602
    label "subject"
  ]
  node [
    id 603
    label "wedyzm"
  ]
  node [
    id 604
    label "energia"
  ]
  node [
    id 605
    label "buddyzm"
  ]
  node [
    id 606
    label "nijaki"
  ]
  node [
    id 607
    label "odwodnienie"
  ]
  node [
    id 608
    label "konstytucja"
  ]
  node [
    id 609
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 610
    label "substancja_chemiczna"
  ]
  node [
    id 611
    label "bratnia_dusza"
  ]
  node [
    id 612
    label "zwi&#261;zanie"
  ]
  node [
    id 613
    label "lokant"
  ]
  node [
    id 614
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 615
    label "zwi&#261;za&#263;"
  ]
  node [
    id 616
    label "organizacja"
  ]
  node [
    id 617
    label "odwadnia&#263;"
  ]
  node [
    id 618
    label "marriage"
  ]
  node [
    id 619
    label "marketing_afiliacyjny"
  ]
  node [
    id 620
    label "bearing"
  ]
  node [
    id 621
    label "wi&#261;zanie"
  ]
  node [
    id 622
    label "odwadnianie"
  ]
  node [
    id 623
    label "koligacja"
  ]
  node [
    id 624
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 625
    label "odwodni&#263;"
  ]
  node [
    id 626
    label "azeotrop"
  ]
  node [
    id 627
    label "powi&#261;zanie"
  ]
  node [
    id 628
    label "jako&#347;"
  ]
  node [
    id 629
    label "charakterystyczny"
  ]
  node [
    id 630
    label "ciekawy"
  ]
  node [
    id 631
    label "jako_tako"
  ]
  node [
    id 632
    label "dziwny"
  ]
  node [
    id 633
    label "przyzwoity"
  ]
  node [
    id 634
    label "bakteryjnie"
  ]
  node [
    id 635
    label "mieszanina"
  ]
  node [
    id 636
    label "nieprzejrzysty"
  ]
  node [
    id 637
    label "domieszka"
  ]
  node [
    id 638
    label "kwa&#347;ny_deszcz"
  ]
  node [
    id 639
    label "cecha"
  ]
  node [
    id 640
    label "pozwolenie"
  ]
  node [
    id 641
    label "truciciel"
  ]
  node [
    id 642
    label "impurity"
  ]
  node [
    id 643
    label "wypowied&#378;"
  ]
  node [
    id 644
    label "bicie"
  ]
  node [
    id 645
    label "wysi&#281;k"
  ]
  node [
    id 646
    label "pustka"
  ]
  node [
    id 647
    label "woda_s&#322;odka"
  ]
  node [
    id 648
    label "p&#322;ycizna"
  ]
  node [
    id 649
    label "ciecz"
  ]
  node [
    id 650
    label "spi&#281;trza&#263;"
  ]
  node [
    id 651
    label "uj&#281;cie_wody"
  ]
  node [
    id 652
    label "chlasta&#263;"
  ]
  node [
    id 653
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 654
    label "nap&#243;j"
  ]
  node [
    id 655
    label "bombast"
  ]
  node [
    id 656
    label "water"
  ]
  node [
    id 657
    label "kryptodepresja"
  ]
  node [
    id 658
    label "wodnik"
  ]
  node [
    id 659
    label "pojazd"
  ]
  node [
    id 660
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 661
    label "fala"
  ]
  node [
    id 662
    label "Waruna"
  ]
  node [
    id 663
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 664
    label "zrzut"
  ]
  node [
    id 665
    label "dotleni&#263;"
  ]
  node [
    id 666
    label "utylizator"
  ]
  node [
    id 667
    label "uci&#261;g"
  ]
  node [
    id 668
    label "wybrze&#380;e"
  ]
  node [
    id 669
    label "nabranie"
  ]
  node [
    id 670
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 671
    label "klarownik"
  ]
  node [
    id 672
    label "chlastanie"
  ]
  node [
    id 673
    label "przybrze&#380;e"
  ]
  node [
    id 674
    label "deklamacja"
  ]
  node [
    id 675
    label "spi&#281;trzenie"
  ]
  node [
    id 676
    label "przybieranie"
  ]
  node [
    id 677
    label "nabra&#263;"
  ]
  node [
    id 678
    label "tlenek"
  ]
  node [
    id 679
    label "spi&#281;trzanie"
  ]
  node [
    id 680
    label "l&#243;d"
  ]
  node [
    id 681
    label "sklep"
  ]
  node [
    id 682
    label "znaczenie"
  ]
  node [
    id 683
    label "wn&#281;trze"
  ]
  node [
    id 684
    label "psychika"
  ]
  node [
    id 685
    label "superego"
  ]
  node [
    id 686
    label "charakter"
  ]
  node [
    id 687
    label "mentalno&#347;&#263;"
  ]
  node [
    id 688
    label "fall"
  ]
  node [
    id 689
    label "zapragn&#261;&#263;"
  ]
  node [
    id 690
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 691
    label "maszyneria"
  ]
  node [
    id 692
    label "podstawa"
  ]
  node [
    id 693
    label "maszyna"
  ]
  node [
    id 694
    label "urz&#261;dzenie"
  ]
  node [
    id 695
    label "spos&#243;b"
  ]
  node [
    id 696
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 697
    label "wygrywa&#263;"
  ]
  node [
    id 698
    label "&#322;oi&#263;"
  ]
  node [
    id 699
    label "hesitate"
  ]
  node [
    id 700
    label "przeciwdzia&#322;a&#263;"
  ]
  node [
    id 701
    label "fight"
  ]
  node [
    id 702
    label "radzi&#263;_sobie"
  ]
  node [
    id 703
    label "istota_&#380;ywa"
  ]
  node [
    id 704
    label "prokariont"
  ]
  node [
    id 705
    label "wirus"
  ]
  node [
    id 706
    label "program"
  ]
  node [
    id 707
    label "plechowiec"
  ]
  node [
    id 708
    label "bakterie"
  ]
  node [
    id 709
    label "ryzosfera"
  ]
  node [
    id 710
    label "beta-laktamaza"
  ]
  node [
    id 711
    label "enterotoksyna"
  ]
  node [
    id 712
    label "sk&#322;ad"
  ]
  node [
    id 713
    label "zachowanie"
  ]
  node [
    id 714
    label "umowa"
  ]
  node [
    id 715
    label "podsystem"
  ]
  node [
    id 716
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 717
    label "system"
  ]
  node [
    id 718
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 719
    label "struktura"
  ]
  node [
    id 720
    label "wi&#281;&#378;"
  ]
  node [
    id 721
    label "zawarcie"
  ]
  node [
    id 722
    label "systemat"
  ]
  node [
    id 723
    label "usenet"
  ]
  node [
    id 724
    label "ONZ"
  ]
  node [
    id 725
    label "o&#347;"
  ]
  node [
    id 726
    label "organ"
  ]
  node [
    id 727
    label "przestawi&#263;"
  ]
  node [
    id 728
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 729
    label "traktat_wersalski"
  ]
  node [
    id 730
    label "rozprz&#261;c"
  ]
  node [
    id 731
    label "cybernetyk"
  ]
  node [
    id 732
    label "cia&#322;o"
  ]
  node [
    id 733
    label "zawrze&#263;"
  ]
  node [
    id 734
    label "konstelacja"
  ]
  node [
    id 735
    label "alliance"
  ]
  node [
    id 736
    label "zbi&#243;r"
  ]
  node [
    id 737
    label "NATO"
  ]
  node [
    id 738
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 739
    label "treaty"
  ]
  node [
    id 740
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 741
    label "stop"
  ]
  node [
    id 742
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 743
    label "przebywa&#263;"
  ]
  node [
    id 744
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 745
    label "support"
  ]
  node [
    id 746
    label "blend"
  ]
  node [
    id 747
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 748
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 749
    label "dyskrecja"
  ]
  node [
    id 750
    label "secret"
  ]
  node [
    id 751
    label "zachowa&#263;"
  ]
  node [
    id 752
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 753
    label "zachowywa&#263;"
  ]
  node [
    id 754
    label "wiedza"
  ]
  node [
    id 755
    label "wyda&#263;"
  ]
  node [
    id 756
    label "informacja"
  ]
  node [
    id 757
    label "enigmat"
  ]
  node [
    id 758
    label "zachowywanie"
  ]
  node [
    id 759
    label "wydawa&#263;"
  ]
  node [
    id 760
    label "wypaplanie"
  ]
  node [
    id 761
    label "taj&#324;"
  ]
  node [
    id 762
    label "obowi&#261;zek"
  ]
  node [
    id 763
    label "zu&#380;y&#263;"
  ]
  node [
    id 764
    label "get"
  ]
  node [
    id 765
    label "render"
  ]
  node [
    id 766
    label "ci&#261;&#380;a"
  ]
  node [
    id 767
    label "informowa&#263;"
  ]
  node [
    id 768
    label "zanosi&#263;"
  ]
  node [
    id 769
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 770
    label "spill_the_beans"
  ]
  node [
    id 771
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 772
    label "inform"
  ]
  node [
    id 773
    label "przeby&#263;"
  ]
  node [
    id 774
    label "bli&#378;ni"
  ]
  node [
    id 775
    label "odpowiedni"
  ]
  node [
    id 776
    label "swojak"
  ]
  node [
    id 777
    label "samodzielny"
  ]
  node [
    id 778
    label "zakres"
  ]
  node [
    id 779
    label "whole"
  ]
  node [
    id 780
    label "wytw&#243;r"
  ]
  node [
    id 781
    label "column"
  ]
  node [
    id 782
    label "distribution"
  ]
  node [
    id 783
    label "bezdro&#380;e"
  ]
  node [
    id 784
    label "competence"
  ]
  node [
    id 785
    label "zesp&#243;&#322;"
  ]
  node [
    id 786
    label "urz&#261;d"
  ]
  node [
    id 787
    label "jednostka_organizacyjna"
  ]
  node [
    id 788
    label "stopie&#324;"
  ]
  node [
    id 789
    label "insourcing"
  ]
  node [
    id 790
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 791
    label "sfera"
  ]
  node [
    id 792
    label "miejsce_pracy"
  ]
  node [
    id 793
    label "poddzia&#322;"
  ]
  node [
    id 794
    label "nowostka"
  ]
  node [
    id 795
    label "doniesienie"
  ]
  node [
    id 796
    label "message"
  ]
  node [
    id 797
    label "nius"
  ]
  node [
    id 798
    label "nakazywa&#263;"
  ]
  node [
    id 799
    label "przywraca&#263;"
  ]
  node [
    id 800
    label "call"
  ]
  node [
    id 801
    label "preferans"
  ]
  node [
    id 802
    label "order"
  ]
  node [
    id 803
    label "adduce"
  ]
  node [
    id 804
    label "upomina&#263;"
  ]
  node [
    id 805
    label "nawi&#261;zywa&#263;"
  ]
  node [
    id 806
    label "przypomina&#263;"
  ]
  node [
    id 807
    label "success"
  ]
  node [
    id 808
    label "prosperity"
  ]
  node [
    id 809
    label "passa"
  ]
  node [
    id 810
    label "popularno&#347;&#263;"
  ]
  node [
    id 811
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 812
    label "popularity"
  ]
  node [
    id 813
    label "opinia"
  ]
  node [
    id 814
    label "mecz"
  ]
  node [
    id 815
    label "service"
  ]
  node [
    id 816
    label "zak&#322;ad"
  ]
  node [
    id 817
    label "us&#322;uga"
  ]
  node [
    id 818
    label "zastawa"
  ]
  node [
    id 819
    label "YouTube"
  ]
  node [
    id 820
    label "punkt"
  ]
  node [
    id 821
    label "porcja"
  ]
  node [
    id 822
    label "strona"
  ]
  node [
    id 823
    label "inteligent"
  ]
  node [
    id 824
    label "nauczny"
  ]
  node [
    id 825
    label "uczenie"
  ]
  node [
    id 826
    label "wykszta&#322;cony"
  ]
  node [
    id 827
    label "m&#261;dry"
  ]
  node [
    id 828
    label "Awerroes"
  ]
  node [
    id 829
    label "intelektualista"
  ]
  node [
    id 830
    label "pozna&#263;"
  ]
  node [
    id 831
    label "ukaza&#263;"
  ]
  node [
    id 832
    label "denounce"
  ]
  node [
    id 833
    label "podnie&#347;&#263;"
  ]
  node [
    id 834
    label "znale&#378;&#263;"
  ]
  node [
    id 835
    label "unwrap"
  ]
  node [
    id 836
    label "&#322;owca_talent&#243;w"
  ]
  node [
    id 837
    label "expose"
  ]
  node [
    id 838
    label "discover"
  ]
  node [
    id 839
    label "zsun&#261;&#263;"
  ]
  node [
    id 840
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 841
    label "objawi&#263;"
  ]
  node [
    id 842
    label "bia&#322;ko"
  ]
  node [
    id 843
    label "immobilizowanie"
  ]
  node [
    id 844
    label "apoenzym"
  ]
  node [
    id 845
    label "immobilizowa&#263;"
  ]
  node [
    id 846
    label "enzyme"
  ]
  node [
    id 847
    label "immobilizacja"
  ]
  node [
    id 848
    label "biokatalizator"
  ]
  node [
    id 849
    label "zymaza"
  ]
  node [
    id 850
    label "powodowa&#263;"
  ]
  node [
    id 851
    label "metylotrofia"
  ]
  node [
    id 852
    label "wyja&#322;owi&#263;"
  ]
  node [
    id 853
    label "wyja&#322;awia&#263;"
  ]
  node [
    id 854
    label "wyja&#322;owi&#263;_si&#281;"
  ]
  node [
    id 855
    label "wyja&#322;awia&#263;_si&#281;"
  ]
  node [
    id 856
    label "doznanie"
  ]
  node [
    id 857
    label "zmniejszenie"
  ]
  node [
    id 858
    label "os&#322;abi&#263;"
  ]
  node [
    id 859
    label "s&#322;abszy"
  ]
  node [
    id 860
    label "zdrowie"
  ]
  node [
    id 861
    label "fatigue_duty"
  ]
  node [
    id 862
    label "kondycja_fizyczna"
  ]
  node [
    id 863
    label "pogorszenie"
  ]
  node [
    id 864
    label "infirmity"
  ]
  node [
    id 865
    label "empatyczny"
  ]
  node [
    id 866
    label "naturalny"
  ]
  node [
    id 867
    label "prawdziwy"
  ]
  node [
    id 868
    label "ludzko"
  ]
  node [
    id 869
    label "po_ludzku"
  ]
  node [
    id 870
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 871
    label "normalny"
  ]
  node [
    id 872
    label "ochrona"
  ]
  node [
    id 873
    label "trudno&#347;&#263;"
  ]
  node [
    id 874
    label "parapet"
  ]
  node [
    id 875
    label "obstruction"
  ]
  node [
    id 876
    label "pasmo"
  ]
  node [
    id 877
    label "przeszkoda"
  ]
  node [
    id 878
    label "upewnienie_si&#281;"
  ]
  node [
    id 879
    label "wierzenie"
  ]
  node [
    id 880
    label "mo&#380;liwy"
  ]
  node [
    id 881
    label "ufanie"
  ]
  node [
    id 882
    label "spokojny"
  ]
  node [
    id 883
    label "upewnianie_si&#281;"
  ]
  node [
    id 884
    label "specjalnie"
  ]
  node [
    id 885
    label "nieetatowy"
  ]
  node [
    id 886
    label "intencjonalny"
  ]
  node [
    id 887
    label "szczeg&#243;lny"
  ]
  node [
    id 888
    label "niedorozw&#243;j"
  ]
  node [
    id 889
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 890
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 891
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 892
    label "nienormalny"
  ]
  node [
    id 893
    label "umy&#347;lnie"
  ]
  node [
    id 894
    label "moralnie"
  ]
  node [
    id 895
    label "wiele"
  ]
  node [
    id 896
    label "lepiej"
  ]
  node [
    id 897
    label "korzystnie"
  ]
  node [
    id 898
    label "pomy&#347;lnie"
  ]
  node [
    id 899
    label "dobry"
  ]
  node [
    id 900
    label "dobroczynnie"
  ]
  node [
    id 901
    label "odpowiednio"
  ]
  node [
    id 902
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 903
    label "nauki_o_Ziemi"
  ]
  node [
    id 904
    label "teoria_naukowa"
  ]
  node [
    id 905
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 906
    label "nauki_o_poznaniu"
  ]
  node [
    id 907
    label "nomotetyczny"
  ]
  node [
    id 908
    label "metodologia"
  ]
  node [
    id 909
    label "przem&#243;wienie"
  ]
  node [
    id 910
    label "kultura_duchowa"
  ]
  node [
    id 911
    label "nauki_penalne"
  ]
  node [
    id 912
    label "systematyka"
  ]
  node [
    id 913
    label "inwentyka"
  ]
  node [
    id 914
    label "dziedzina"
  ]
  node [
    id 915
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 916
    label "miasteczko_rowerowe"
  ]
  node [
    id 917
    label "fotowoltaika"
  ]
  node [
    id 918
    label "porada"
  ]
  node [
    id 919
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 920
    label "proces"
  ]
  node [
    id 921
    label "imagineskopia"
  ]
  node [
    id 922
    label "typologia"
  ]
  node [
    id 923
    label "&#322;awa_szkolna"
  ]
  node [
    id 924
    label "odm&#322;adza&#263;"
  ]
  node [
    id 925
    label "cz&#261;steczka"
  ]
  node [
    id 926
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 927
    label "egzemplarz"
  ]
  node [
    id 928
    label "formacja_geologiczna"
  ]
  node [
    id 929
    label "harcerze_starsi"
  ]
  node [
    id 930
    label "liga"
  ]
  node [
    id 931
    label "Terranie"
  ]
  node [
    id 932
    label "&#346;wietliki"
  ]
  node [
    id 933
    label "pakiet_klimatyczny"
  ]
  node [
    id 934
    label "oddzia&#322;"
  ]
  node [
    id 935
    label "stage_set"
  ]
  node [
    id 936
    label "Entuzjastki"
  ]
  node [
    id 937
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 938
    label "odm&#322;odzenie"
  ]
  node [
    id 939
    label "type"
  ]
  node [
    id 940
    label "category"
  ]
  node [
    id 941
    label "specgrupa"
  ]
  node [
    id 942
    label "odm&#322;adzanie"
  ]
  node [
    id 943
    label "gromada"
  ]
  node [
    id 944
    label "Eurogrupa"
  ]
  node [
    id 945
    label "jednostka_systematyczna"
  ]
  node [
    id 946
    label "kompozycja"
  ]
  node [
    id 947
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 948
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 949
    label "wychodzi&#263;"
  ]
  node [
    id 950
    label "dzia&#322;a&#263;"
  ]
  node [
    id 951
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 952
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 953
    label "act"
  ]
  node [
    id 954
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 955
    label "seclude"
  ]
  node [
    id 956
    label "perform"
  ]
  node [
    id 957
    label "odst&#281;powa&#263;"
  ]
  node [
    id 958
    label "rezygnowa&#263;"
  ]
  node [
    id 959
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 960
    label "overture"
  ]
  node [
    id 961
    label "nak&#322;ania&#263;"
  ]
  node [
    id 962
    label "appear"
  ]
  node [
    id 963
    label "ewoluowanie"
  ]
  node [
    id 964
    label "przyswoi&#263;"
  ]
  node [
    id 965
    label "reakcja"
  ]
  node [
    id 966
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 967
    label "wyewoluowanie"
  ]
  node [
    id 968
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 969
    label "individual"
  ]
  node [
    id 970
    label "otworzenie"
  ]
  node [
    id 971
    label "starzenie_si&#281;"
  ]
  node [
    id 972
    label "biorytm"
  ]
  node [
    id 973
    label "otwieranie"
  ]
  node [
    id 974
    label "l&#281;d&#378;wie"
  ]
  node [
    id 975
    label "sk&#243;ra"
  ]
  node [
    id 976
    label "unerwienie"
  ]
  node [
    id 977
    label "ow&#322;osienie"
  ]
  node [
    id 978
    label "przyswajanie"
  ]
  node [
    id 979
    label "wyewoluowa&#263;"
  ]
  node [
    id 980
    label "ewoluowa&#263;"
  ]
  node [
    id 981
    label "otworzy&#263;"
  ]
  node [
    id 982
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 983
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 984
    label "cz&#322;onek"
  ]
  node [
    id 985
    label "miejsce"
  ]
  node [
    id 986
    label "obiekt"
  ]
  node [
    id 987
    label "szkielet"
  ]
  node [
    id 988
    label "przyswojenie"
  ]
  node [
    id 989
    label "ty&#322;"
  ]
  node [
    id 990
    label "temperatura"
  ]
  node [
    id 991
    label "czynnik_biotyczny"
  ]
  node [
    id 992
    label "staw"
  ]
  node [
    id 993
    label "przyswaja&#263;"
  ]
  node [
    id 994
    label "prz&#243;d"
  ]
  node [
    id 995
    label "otwiera&#263;"
  ]
  node [
    id 996
    label "p&#322;aszczyzna"
  ]
  node [
    id 997
    label "czynny"
  ]
  node [
    id 998
    label "aktualny"
  ]
  node [
    id 999
    label "realistyczny"
  ]
  node [
    id 1000
    label "silny"
  ]
  node [
    id 1001
    label "&#380;ywotny"
  ]
  node [
    id 1002
    label "g&#322;&#281;boki"
  ]
  node [
    id 1003
    label "&#380;ywo"
  ]
  node [
    id 1004
    label "zgrabny"
  ]
  node [
    id 1005
    label "o&#380;ywianie"
  ]
  node [
    id 1006
    label "szybki"
  ]
  node [
    id 1007
    label "wyra&#378;ny"
  ]
  node [
    id 1008
    label "energiczny"
  ]
  node [
    id 1009
    label "&#347;ledziowate"
  ]
  node [
    id 1010
    label "ryba"
  ]
  node [
    id 1011
    label "summer"
  ]
  node [
    id 1012
    label "wstawa"
  ]
  node [
    id 1013
    label "funkcja_trygonometryczna"
  ]
  node [
    id 1014
    label "sine"
  ]
  node [
    id 1015
    label "arcus_sinus"
  ]
  node [
    id 1016
    label "nauczyciel_akademicki"
  ]
  node [
    id 1017
    label "tytu&#322;"
  ]
  node [
    id 1018
    label "okre&#347;lony"
  ]
  node [
    id 1019
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1020
    label "report"
  ]
  node [
    id 1021
    label "dawa&#263;"
  ]
  node [
    id 1022
    label "reagowa&#263;"
  ]
  node [
    id 1023
    label "contend"
  ]
  node [
    id 1024
    label "ponosi&#263;"
  ]
  node [
    id 1025
    label "impart"
  ]
  node [
    id 1026
    label "react"
  ]
  node [
    id 1027
    label "tone"
  ]
  node [
    id 1028
    label "equate"
  ]
  node [
    id 1029
    label "pytanie"
  ]
  node [
    id 1030
    label "answer"
  ]
  node [
    id 1031
    label "ease"
  ]
  node [
    id 1032
    label "robi&#263;"
  ]
  node [
    id 1033
    label "&#322;atwi&#263;"
  ]
  node [
    id 1034
    label "mucha_tse-tse"
  ]
  node [
    id 1035
    label "febra"
  ]
  node [
    id 1036
    label "zarodziec"
  ]
  node [
    id 1037
    label "choroba_paso&#380;ytnicza"
  ]
  node [
    id 1038
    label "cenocyt"
  ]
  node [
    id 1039
    label "zach&#281;ci&#263;"
  ]
  node [
    id 1040
    label "heating_system"
  ]
  node [
    id 1041
    label "podobnie"
  ]
  node [
    id 1042
    label "upodabnianie_si&#281;"
  ]
  node [
    id 1043
    label "zasymilowanie"
  ]
  node [
    id 1044
    label "drugi"
  ]
  node [
    id 1045
    label "taki"
  ]
  node [
    id 1046
    label "upodobnienie"
  ]
  node [
    id 1047
    label "przypominanie"
  ]
  node [
    id 1048
    label "upodobnienie_si&#281;"
  ]
  node [
    id 1049
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1050
    label "motyw"
  ]
  node [
    id 1051
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1052
    label "state"
  ]
  node [
    id 1053
    label "realia"
  ]
  node [
    id 1054
    label "kolejny"
  ]
  node [
    id 1055
    label "inaczej"
  ]
  node [
    id 1056
    label "r&#243;&#380;ny"
  ]
  node [
    id 1057
    label "inszy"
  ]
  node [
    id 1058
    label "osobno"
  ]
  node [
    id 1059
    label "dok&#322;adnie"
  ]
  node [
    id 1060
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1061
    label "utrze&#263;"
  ]
  node [
    id 1062
    label "catch"
  ]
  node [
    id 1063
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 1064
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 1065
    label "dorobi&#263;"
  ]
  node [
    id 1066
    label "advance"
  ]
  node [
    id 1067
    label "dopasowa&#263;"
  ]
  node [
    id 1068
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 1069
    label "silnik"
  ]
  node [
    id 1070
    label "Montezuma"
  ]
  node [
    id 1071
    label "ii"
  ]
  node [
    id 1072
    label "Breaking"
  ]
  node [
    id 1073
    label "Johns"
  ]
  node [
    id 1074
    label "Hopkins"
  ]
  node [
    id 1075
    label "University"
  ]
  node [
    id 1076
    label "Sin"
  ]
  node [
    id 1077
    label "Urban"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 279
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 280
  ]
  edge [
    source 14
    target 281
  ]
  edge [
    source 14
    target 282
  ]
  edge [
    source 14
    target 283
  ]
  edge [
    source 14
    target 284
  ]
  edge [
    source 14
    target 285
  ]
  edge [
    source 14
    target 286
  ]
  edge [
    source 14
    target 287
  ]
  edge [
    source 14
    target 288
  ]
  edge [
    source 14
    target 289
  ]
  edge [
    source 14
    target 290
  ]
  edge [
    source 14
    target 291
  ]
  edge [
    source 14
    target 292
  ]
  edge [
    source 14
    target 293
  ]
  edge [
    source 14
    target 294
  ]
  edge [
    source 14
    target 295
  ]
  edge [
    source 14
    target 296
  ]
  edge [
    source 14
    target 297
  ]
  edge [
    source 14
    target 298
  ]
  edge [
    source 14
    target 299
  ]
  edge [
    source 14
    target 300
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 301
  ]
  edge [
    source 15
    target 302
  ]
  edge [
    source 15
    target 303
  ]
  edge [
    source 15
    target 304
  ]
  edge [
    source 15
    target 305
  ]
  edge [
    source 15
    target 306
  ]
  edge [
    source 15
    target 307
  ]
  edge [
    source 15
    target 47
  ]
  edge [
    source 15
    target 70
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 61
  ]
  edge [
    source 16
    target 62
  ]
  edge [
    source 16
    target 308
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 309
  ]
  edge [
    source 17
    target 310
  ]
  edge [
    source 17
    target 311
  ]
  edge [
    source 17
    target 312
  ]
  edge [
    source 17
    target 313
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 314
  ]
  edge [
    source 19
    target 315
  ]
  edge [
    source 19
    target 316
  ]
  edge [
    source 19
    target 317
  ]
  edge [
    source 19
    target 318
  ]
  edge [
    source 19
    target 319
  ]
  edge [
    source 19
    target 320
  ]
  edge [
    source 19
    target 321
  ]
  edge [
    source 19
    target 322
  ]
  edge [
    source 19
    target 323
  ]
  edge [
    source 19
    target 324
  ]
  edge [
    source 19
    target 325
  ]
  edge [
    source 19
    target 326
  ]
  edge [
    source 19
    target 327
  ]
  edge [
    source 19
    target 328
  ]
  edge [
    source 19
    target 329
  ]
  edge [
    source 19
    target 330
  ]
  edge [
    source 19
    target 331
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 332
  ]
  edge [
    source 20
    target 333
  ]
  edge [
    source 20
    target 334
  ]
  edge [
    source 20
    target 335
  ]
  edge [
    source 20
    target 336
  ]
  edge [
    source 20
    target 337
  ]
  edge [
    source 20
    target 338
  ]
  edge [
    source 20
    target 339
  ]
  edge [
    source 20
    target 340
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 341
  ]
  edge [
    source 20
    target 342
  ]
  edge [
    source 20
    target 343
  ]
  edge [
    source 20
    target 344
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 345
  ]
  edge [
    source 20
    target 346
  ]
  edge [
    source 20
    target 347
  ]
  edge [
    source 20
    target 348
  ]
  edge [
    source 20
    target 349
  ]
  edge [
    source 20
    target 350
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 87
  ]
  edge [
    source 21
    target 88
  ]
  edge [
    source 21
    target 351
  ]
  edge [
    source 21
    target 352
  ]
  edge [
    source 21
    target 353
  ]
  edge [
    source 21
    target 354
  ]
  edge [
    source 21
    target 355
  ]
  edge [
    source 21
    target 356
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 357
  ]
  edge [
    source 22
    target 358
  ]
  edge [
    source 22
    target 359
  ]
  edge [
    source 22
    target 360
  ]
  edge [
    source 22
    target 361
  ]
  edge [
    source 22
    target 362
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 42
  ]
  edge [
    source 23
    target 43
  ]
  edge [
    source 23
    target 363
  ]
  edge [
    source 23
    target 49
  ]
  edge [
    source 23
    target 82
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 364
  ]
  edge [
    source 25
    target 365
  ]
  edge [
    source 25
    target 366
  ]
  edge [
    source 25
    target 367
  ]
  edge [
    source 25
    target 368
  ]
  edge [
    source 25
    target 369
  ]
  edge [
    source 25
    target 370
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 371
  ]
  edge [
    source 26
    target 372
  ]
  edge [
    source 26
    target 373
  ]
  edge [
    source 26
    target 374
  ]
  edge [
    source 26
    target 375
  ]
  edge [
    source 26
    target 376
  ]
  edge [
    source 26
    target 377
  ]
  edge [
    source 26
    target 378
  ]
  edge [
    source 26
    target 379
  ]
  edge [
    source 26
    target 380
  ]
  edge [
    source 26
    target 381
  ]
  edge [
    source 26
    target 382
  ]
  edge [
    source 26
    target 383
  ]
  edge [
    source 26
    target 384
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 385
  ]
  edge [
    source 29
    target 67
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 386
  ]
  edge [
    source 30
    target 387
  ]
  edge [
    source 30
    target 143
  ]
  edge [
    source 30
    target 388
  ]
  edge [
    source 30
    target 389
  ]
  edge [
    source 30
    target 152
  ]
  edge [
    source 30
    target 390
  ]
  edge [
    source 30
    target 391
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 392
  ]
  edge [
    source 31
    target 393
  ]
  edge [
    source 31
    target 394
  ]
  edge [
    source 31
    target 395
  ]
  edge [
    source 31
    target 396
  ]
  edge [
    source 31
    target 397
  ]
  edge [
    source 31
    target 398
  ]
  edge [
    source 31
    target 399
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 400
  ]
  edge [
    source 32
    target 401
  ]
  edge [
    source 32
    target 402
  ]
  edge [
    source 32
    target 403
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 404
  ]
  edge [
    source 33
    target 405
  ]
  edge [
    source 33
    target 406
  ]
  edge [
    source 33
    target 407
  ]
  edge [
    source 33
    target 408
  ]
  edge [
    source 33
    target 409
  ]
  edge [
    source 33
    target 45
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 410
  ]
  edge [
    source 34
    target 411
  ]
  edge [
    source 34
    target 412
  ]
  edge [
    source 34
    target 413
  ]
  edge [
    source 34
    target 414
  ]
  edge [
    source 34
    target 415
  ]
  edge [
    source 34
    target 416
  ]
  edge [
    source 34
    target 417
  ]
  edge [
    source 34
    target 418
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 47
  ]
  edge [
    source 36
    target 48
  ]
  edge [
    source 36
    target 125
  ]
  edge [
    source 36
    target 419
  ]
  edge [
    source 36
    target 420
  ]
  edge [
    source 36
    target 421
  ]
  edge [
    source 36
    target 422
  ]
  edge [
    source 36
    target 423
  ]
  edge [
    source 36
    target 424
  ]
  edge [
    source 36
    target 425
  ]
  edge [
    source 36
    target 426
  ]
  edge [
    source 36
    target 427
  ]
  edge [
    source 36
    target 428
  ]
  edge [
    source 36
    target 429
  ]
  edge [
    source 36
    target 430
  ]
  edge [
    source 36
    target 431
  ]
  edge [
    source 36
    target 432
  ]
  edge [
    source 36
    target 433
  ]
  edge [
    source 36
    target 434
  ]
  edge [
    source 36
    target 435
  ]
  edge [
    source 36
    target 436
  ]
  edge [
    source 36
    target 437
  ]
  edge [
    source 36
    target 438
  ]
  edge [
    source 36
    target 439
  ]
  edge [
    source 36
    target 440
  ]
  edge [
    source 36
    target 441
  ]
  edge [
    source 36
    target 442
  ]
  edge [
    source 36
    target 443
  ]
  edge [
    source 36
    target 444
  ]
  edge [
    source 36
    target 445
  ]
  edge [
    source 36
    target 446
  ]
  edge [
    source 36
    target 447
  ]
  edge [
    source 36
    target 448
  ]
  edge [
    source 36
    target 449
  ]
  edge [
    source 36
    target 46
  ]
  edge [
    source 36
    target 58
  ]
  edge [
    source 36
    target 69
  ]
  edge [
    source 37
    target 52
  ]
  edge [
    source 37
    target 102
  ]
  edge [
    source 37
    target 103
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 45
  ]
  edge [
    source 38
    target 46
  ]
  edge [
    source 38
    target 52
  ]
  edge [
    source 38
    target 450
  ]
  edge [
    source 38
    target 61
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 75
  ]
  edge [
    source 39
    target 76
  ]
  edge [
    source 39
    target 77
  ]
  edge [
    source 39
    target 451
  ]
  edge [
    source 39
    target 452
  ]
  edge [
    source 39
    target 453
  ]
  edge [
    source 39
    target 454
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 455
  ]
  edge [
    source 42
    target 456
  ]
  edge [
    source 42
    target 457
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 250
  ]
  edge [
    source 43
    target 458
  ]
  edge [
    source 43
    target 459
  ]
  edge [
    source 43
    target 460
  ]
  edge [
    source 43
    target 461
  ]
  edge [
    source 43
    target 462
  ]
  edge [
    source 43
    target 463
  ]
  edge [
    source 43
    target 464
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 465
  ]
  edge [
    source 44
    target 466
  ]
  edge [
    source 44
    target 467
  ]
  edge [
    source 44
    target 354
  ]
  edge [
    source 45
    target 92
  ]
  edge [
    source 45
    target 99
  ]
  edge [
    source 45
    target 468
  ]
  edge [
    source 45
    target 469
  ]
  edge [
    source 45
    target 470
  ]
  edge [
    source 45
    target 471
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 472
  ]
  edge [
    source 46
    target 252
  ]
  edge [
    source 46
    target 69
  ]
  edge [
    source 47
    target 473
  ]
  edge [
    source 47
    target 474
  ]
  edge [
    source 47
    target 475
  ]
  edge [
    source 47
    target 282
  ]
  edge [
    source 47
    target 476
  ]
  edge [
    source 47
    target 104
  ]
  edge [
    source 47
    target 477
  ]
  edge [
    source 47
    target 478
  ]
  edge [
    source 47
    target 479
  ]
  edge [
    source 47
    target 480
  ]
  edge [
    source 47
    target 481
  ]
  edge [
    source 47
    target 482
  ]
  edge [
    source 47
    target 483
  ]
  edge [
    source 47
    target 484
  ]
  edge [
    source 47
    target 485
  ]
  edge [
    source 47
    target 486
  ]
  edge [
    source 47
    target 487
  ]
  edge [
    source 47
    target 488
  ]
  edge [
    source 47
    target 489
  ]
  edge [
    source 47
    target 490
  ]
  edge [
    source 47
    target 491
  ]
  edge [
    source 47
    target 492
  ]
  edge [
    source 47
    target 493
  ]
  edge [
    source 47
    target 494
  ]
  edge [
    source 47
    target 495
  ]
  edge [
    source 47
    target 496
  ]
  edge [
    source 47
    target 497
  ]
  edge [
    source 47
    target 498
  ]
  edge [
    source 47
    target 499
  ]
  edge [
    source 47
    target 500
  ]
  edge [
    source 47
    target 501
  ]
  edge [
    source 47
    target 502
  ]
  edge [
    source 47
    target 503
  ]
  edge [
    source 47
    target 504
  ]
  edge [
    source 47
    target 505
  ]
  edge [
    source 47
    target 506
  ]
  edge [
    source 47
    target 507
  ]
  edge [
    source 47
    target 508
  ]
  edge [
    source 47
    target 509
  ]
  edge [
    source 47
    target 510
  ]
  edge [
    source 47
    target 511
  ]
  edge [
    source 47
    target 512
  ]
  edge [
    source 47
    target 513
  ]
  edge [
    source 47
    target 514
  ]
  edge [
    source 47
    target 515
  ]
  edge [
    source 47
    target 516
  ]
  edge [
    source 47
    target 517
  ]
  edge [
    source 47
    target 518
  ]
  edge [
    source 47
    target 519
  ]
  edge [
    source 47
    target 520
  ]
  edge [
    source 47
    target 521
  ]
  edge [
    source 47
    target 522
  ]
  edge [
    source 47
    target 523
  ]
  edge [
    source 47
    target 524
  ]
  edge [
    source 47
    target 525
  ]
  edge [
    source 47
    target 526
  ]
  edge [
    source 47
    target 527
  ]
  edge [
    source 47
    target 528
  ]
  edge [
    source 47
    target 529
  ]
  edge [
    source 47
    target 530
  ]
  edge [
    source 47
    target 531
  ]
  edge [
    source 47
    target 532
  ]
  edge [
    source 47
    target 533
  ]
  edge [
    source 47
    target 64
  ]
  edge [
    source 47
    target 534
  ]
  edge [
    source 47
    target 535
  ]
  edge [
    source 47
    target 536
  ]
  edge [
    source 47
    target 537
  ]
  edge [
    source 47
    target 538
  ]
  edge [
    source 47
    target 70
  ]
  edge [
    source 47
    target 92
  ]
  edge [
    source 47
    target 118
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 126
  ]
  edge [
    source 48
    target 539
  ]
  edge [
    source 48
    target 96
  ]
  edge [
    source 48
    target 98
  ]
  edge [
    source 48
    target 74
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 82
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 540
  ]
  edge [
    source 50
    target 541
  ]
  edge [
    source 50
    target 186
  ]
  edge [
    source 50
    target 542
  ]
  edge [
    source 50
    target 543
  ]
  edge [
    source 50
    target 544
  ]
  edge [
    source 50
    target 104
  ]
  edge [
    source 50
    target 545
  ]
  edge [
    source 50
    target 546
  ]
  edge [
    source 50
    target 547
  ]
  edge [
    source 50
    target 548
  ]
  edge [
    source 50
    target 110
  ]
  edge [
    source 50
    target 549
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 550
  ]
  edge [
    source 51
    target 551
  ]
  edge [
    source 51
    target 552
  ]
  edge [
    source 52
    target 74
  ]
  edge [
    source 52
    target 75
  ]
  edge [
    source 52
    target 72
  ]
  edge [
    source 52
    target 109
  ]
  edge [
    source 52
    target 553
  ]
  edge [
    source 52
    target 554
  ]
  edge [
    source 52
    target 555
  ]
  edge [
    source 52
    target 556
  ]
  edge [
    source 52
    target 557
  ]
  edge [
    source 52
    target 558
  ]
  edge [
    source 52
    target 559
  ]
  edge [
    source 52
    target 560
  ]
  edge [
    source 52
    target 561
  ]
  edge [
    source 52
    target 562
  ]
  edge [
    source 52
    target 563
  ]
  edge [
    source 52
    target 564
  ]
  edge [
    source 52
    target 507
  ]
  edge [
    source 52
    target 565
  ]
  edge [
    source 52
    target 566
  ]
  edge [
    source 52
    target 567
  ]
  edge [
    source 52
    target 568
  ]
  edge [
    source 52
    target 569
  ]
  edge [
    source 52
    target 570
  ]
  edge [
    source 52
    target 571
  ]
  edge [
    source 52
    target 572
  ]
  edge [
    source 52
    target 573
  ]
  edge [
    source 52
    target 574
  ]
  edge [
    source 52
    target 575
  ]
  edge [
    source 52
    target 576
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 79
  ]
  edge [
    source 52
    target 87
  ]
  edge [
    source 52
    target 108
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 577
  ]
  edge [
    source 54
    target 578
  ]
  edge [
    source 54
    target 579
  ]
  edge [
    source 54
    target 580
  ]
  edge [
    source 54
    target 581
  ]
  edge [
    source 54
    target 582
  ]
  edge [
    source 54
    target 583
  ]
  edge [
    source 54
    target 584
  ]
  edge [
    source 54
    target 585
  ]
  edge [
    source 54
    target 586
  ]
  edge [
    source 54
    target 587
  ]
  edge [
    source 54
    target 588
  ]
  edge [
    source 54
    target 589
  ]
  edge [
    source 54
    target 590
  ]
  edge [
    source 54
    target 591
  ]
  edge [
    source 54
    target 592
  ]
  edge [
    source 54
    target 593
  ]
  edge [
    source 54
    target 594
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 595
  ]
  edge [
    source 55
    target 596
  ]
  edge [
    source 55
    target 597
  ]
  edge [
    source 55
    target 598
  ]
  edge [
    source 55
    target 599
  ]
  edge [
    source 55
    target 600
  ]
  edge [
    source 55
    target 601
  ]
  edge [
    source 55
    target 602
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 603
  ]
  edge [
    source 57
    target 604
  ]
  edge [
    source 57
    target 605
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 606
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 607
  ]
  edge [
    source 59
    target 608
  ]
  edge [
    source 59
    target 609
  ]
  edge [
    source 59
    target 610
  ]
  edge [
    source 59
    target 611
  ]
  edge [
    source 59
    target 612
  ]
  edge [
    source 59
    target 613
  ]
  edge [
    source 59
    target 614
  ]
  edge [
    source 59
    target 615
  ]
  edge [
    source 59
    target 616
  ]
  edge [
    source 59
    target 617
  ]
  edge [
    source 59
    target 618
  ]
  edge [
    source 59
    target 619
  ]
  edge [
    source 59
    target 620
  ]
  edge [
    source 59
    target 621
  ]
  edge [
    source 59
    target 622
  ]
  edge [
    source 59
    target 623
  ]
  edge [
    source 59
    target 624
  ]
  edge [
    source 59
    target 625
  ]
  edge [
    source 59
    target 626
  ]
  edge [
    source 59
    target 627
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 628
  ]
  edge [
    source 60
    target 629
  ]
  edge [
    source 60
    target 630
  ]
  edge [
    source 60
    target 631
  ]
  edge [
    source 60
    target 632
  ]
  edge [
    source 60
    target 139
  ]
  edge [
    source 60
    target 633
  ]
  edge [
    source 60
    target 83
  ]
  edge [
    source 60
    target 100
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 634
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 635
  ]
  edge [
    source 63
    target 164
  ]
  edge [
    source 63
    target 165
  ]
  edge [
    source 63
    target 636
  ]
  edge [
    source 63
    target 637
  ]
  edge [
    source 63
    target 638
  ]
  edge [
    source 63
    target 639
  ]
  edge [
    source 63
    target 640
  ]
  edge [
    source 63
    target 641
  ]
  edge [
    source 63
    target 642
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 643
  ]
  edge [
    source 64
    target 475
  ]
  edge [
    source 64
    target 644
  ]
  edge [
    source 64
    target 645
  ]
  edge [
    source 64
    target 646
  ]
  edge [
    source 64
    target 647
  ]
  edge [
    source 64
    target 648
  ]
  edge [
    source 64
    target 649
  ]
  edge [
    source 64
    target 650
  ]
  edge [
    source 64
    target 651
  ]
  edge [
    source 64
    target 652
  ]
  edge [
    source 64
    target 653
  ]
  edge [
    source 64
    target 654
  ]
  edge [
    source 64
    target 655
  ]
  edge [
    source 64
    target 656
  ]
  edge [
    source 64
    target 657
  ]
  edge [
    source 64
    target 658
  ]
  edge [
    source 64
    target 659
  ]
  edge [
    source 64
    target 660
  ]
  edge [
    source 64
    target 661
  ]
  edge [
    source 64
    target 662
  ]
  edge [
    source 64
    target 663
  ]
  edge [
    source 64
    target 664
  ]
  edge [
    source 64
    target 665
  ]
  edge [
    source 64
    target 666
  ]
  edge [
    source 64
    target 518
  ]
  edge [
    source 64
    target 667
  ]
  edge [
    source 64
    target 668
  ]
  edge [
    source 64
    target 669
  ]
  edge [
    source 64
    target 670
  ]
  edge [
    source 64
    target 671
  ]
  edge [
    source 64
    target 672
  ]
  edge [
    source 64
    target 673
  ]
  edge [
    source 64
    target 674
  ]
  edge [
    source 64
    target 675
  ]
  edge [
    source 64
    target 676
  ]
  edge [
    source 64
    target 677
  ]
  edge [
    source 64
    target 678
  ]
  edge [
    source 64
    target 679
  ]
  edge [
    source 64
    target 680
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 681
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 107
  ]
  edge [
    source 67
    target 108
  ]
  edge [
    source 67
    target 682
  ]
  edge [
    source 67
    target 683
  ]
  edge [
    source 67
    target 684
  ]
  edge [
    source 67
    target 639
  ]
  edge [
    source 67
    target 685
  ]
  edge [
    source 67
    target 686
  ]
  edge [
    source 67
    target 687
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 688
  ]
  edge [
    source 68
    target 438
  ]
  edge [
    source 68
    target 689
  ]
  edge [
    source 68
    target 94
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 116
  ]
  edge [
    source 69
    target 129
  ]
  edge [
    source 69
    target 690
  ]
  edge [
    source 69
    target 691
  ]
  edge [
    source 69
    target 692
  ]
  edge [
    source 69
    target 693
  ]
  edge [
    source 69
    target 694
  ]
  edge [
    source 69
    target 307
  ]
  edge [
    source 69
    target 695
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 696
  ]
  edge [
    source 70
    target 697
  ]
  edge [
    source 70
    target 698
  ]
  edge [
    source 70
    target 699
  ]
  edge [
    source 70
    target 700
  ]
  edge [
    source 70
    target 701
  ]
  edge [
    source 70
    target 702
  ]
  edge [
    source 70
    target 92
  ]
  edge [
    source 70
    target 118
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 82
  ]
  edge [
    source 71
    target 83
  ]
  edge [
    source 71
    target 74
  ]
  edge [
    source 71
    target 91
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 108
  ]
  edge [
    source 72
    target 703
  ]
  edge [
    source 72
    target 704
  ]
  edge [
    source 72
    target 705
  ]
  edge [
    source 72
    target 706
  ]
  edge [
    source 72
    target 707
  ]
  edge [
    source 72
    target 708
  ]
  edge [
    source 72
    target 709
  ]
  edge [
    source 72
    target 710
  ]
  edge [
    source 72
    target 711
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 712
  ]
  edge [
    source 73
    target 713
  ]
  edge [
    source 73
    target 714
  ]
  edge [
    source 73
    target 715
  ]
  edge [
    source 73
    target 307
  ]
  edge [
    source 73
    target 716
  ]
  edge [
    source 73
    target 717
  ]
  edge [
    source 73
    target 718
  ]
  edge [
    source 73
    target 500
  ]
  edge [
    source 73
    target 719
  ]
  edge [
    source 73
    target 720
  ]
  edge [
    source 73
    target 721
  ]
  edge [
    source 73
    target 722
  ]
  edge [
    source 73
    target 723
  ]
  edge [
    source 73
    target 724
  ]
  edge [
    source 73
    target 725
  ]
  edge [
    source 73
    target 726
  ]
  edge [
    source 73
    target 727
  ]
  edge [
    source 73
    target 728
  ]
  edge [
    source 73
    target 729
  ]
  edge [
    source 73
    target 730
  ]
  edge [
    source 73
    target 731
  ]
  edge [
    source 73
    target 732
  ]
  edge [
    source 73
    target 733
  ]
  edge [
    source 73
    target 734
  ]
  edge [
    source 73
    target 735
  ]
  edge [
    source 73
    target 736
  ]
  edge [
    source 73
    target 737
  ]
  edge [
    source 73
    target 738
  ]
  edge [
    source 73
    target 739
  ]
  edge [
    source 73
    target 106
  ]
  edge [
    source 74
    target 98
  ]
  edge [
    source 74
    target 96
  ]
  edge [
    source 75
    target 740
  ]
  edge [
    source 75
    target 741
  ]
  edge [
    source 75
    target 742
  ]
  edge [
    source 75
    target 743
  ]
  edge [
    source 75
    target 744
  ]
  edge [
    source 75
    target 745
  ]
  edge [
    source 75
    target 746
  ]
  edge [
    source 75
    target 747
  ]
  edge [
    source 75
    target 748
  ]
  edge [
    source 76
    target 749
  ]
  edge [
    source 76
    target 750
  ]
  edge [
    source 76
    target 751
  ]
  edge [
    source 76
    target 713
  ]
  edge [
    source 76
    target 752
  ]
  edge [
    source 76
    target 753
  ]
  edge [
    source 76
    target 754
  ]
  edge [
    source 76
    target 755
  ]
  edge [
    source 76
    target 756
  ]
  edge [
    source 76
    target 480
  ]
  edge [
    source 76
    target 757
  ]
  edge [
    source 76
    target 758
  ]
  edge [
    source 76
    target 759
  ]
  edge [
    source 76
    target 760
  ]
  edge [
    source 76
    target 761
  ]
  edge [
    source 76
    target 695
  ]
  edge [
    source 76
    target 762
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 763
  ]
  edge [
    source 78
    target 764
  ]
  edge [
    source 78
    target 765
  ]
  edge [
    source 78
    target 766
  ]
  edge [
    source 78
    target 767
  ]
  edge [
    source 78
    target 768
  ]
  edge [
    source 78
    target 270
  ]
  edge [
    source 78
    target 769
  ]
  edge [
    source 78
    target 770
  ]
  edge [
    source 78
    target 273
  ]
  edge [
    source 78
    target 771
  ]
  edge [
    source 78
    target 772
  ]
  edge [
    source 78
    target 773
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 774
  ]
  edge [
    source 79
    target 775
  ]
  edge [
    source 79
    target 776
  ]
  edge [
    source 79
    target 777
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 473
  ]
  edge [
    source 80
    target 778
  ]
  edge [
    source 80
    target 779
  ]
  edge [
    source 80
    target 780
  ]
  edge [
    source 80
    target 781
  ]
  edge [
    source 80
    target 782
  ]
  edge [
    source 80
    target 783
  ]
  edge [
    source 80
    target 784
  ]
  edge [
    source 80
    target 785
  ]
  edge [
    source 80
    target 786
  ]
  edge [
    source 80
    target 787
  ]
  edge [
    source 80
    target 788
  ]
  edge [
    source 80
    target 789
  ]
  edge [
    source 80
    target 790
  ]
  edge [
    source 80
    target 791
  ]
  edge [
    source 80
    target 792
  ]
  edge [
    source 80
    target 793
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 717
  ]
  edge [
    source 81
    target 794
  ]
  edge [
    source 81
    target 752
  ]
  edge [
    source 81
    target 756
  ]
  edge [
    source 81
    target 795
  ]
  edge [
    source 81
    target 796
  ]
  edge [
    source 81
    target 797
  ]
  edge [
    source 81
    target 1072
  ]
  edge [
    source 82
    target 798
  ]
  edge [
    source 82
    target 799
  ]
  edge [
    source 82
    target 800
  ]
  edge [
    source 82
    target 801
  ]
  edge [
    source 82
    target 802
  ]
  edge [
    source 82
    target 803
  ]
  edge [
    source 82
    target 804
  ]
  edge [
    source 82
    target 805
  ]
  edge [
    source 82
    target 806
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 100
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 807
  ]
  edge [
    source 84
    target 808
  ]
  edge [
    source 84
    target 809
  ]
  edge [
    source 84
    target 600
  ]
  edge [
    source 84
    target 810
  ]
  edge [
    source 84
    target 811
  ]
  edge [
    source 84
    target 812
  ]
  edge [
    source 84
    target 813
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 814
  ]
  edge [
    source 85
    target 815
  ]
  edge [
    source 85
    target 780
  ]
  edge [
    source 85
    target 816
  ]
  edge [
    source 85
    target 817
  ]
  edge [
    source 85
    target 228
  ]
  edge [
    source 85
    target 795
  ]
  edge [
    source 85
    target 818
  ]
  edge [
    source 85
    target 819
  ]
  edge [
    source 85
    target 820
  ]
  edge [
    source 85
    target 821
  ]
  edge [
    source 85
    target 822
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 87
    target 823
  ]
  edge [
    source 87
    target 824
  ]
  edge [
    source 87
    target 825
  ]
  edge [
    source 87
    target 826
  ]
  edge [
    source 87
    target 827
  ]
  edge [
    source 87
    target 828
  ]
  edge [
    source 87
    target 829
  ]
  edge [
    source 87
    target 129
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 114
  ]
  edge [
    source 88
    target 128
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 103
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 830
  ]
  edge [
    source 91
    target 831
  ]
  edge [
    source 91
    target 832
  ]
  edge [
    source 91
    target 833
  ]
  edge [
    source 91
    target 834
  ]
  edge [
    source 91
    target 835
  ]
  edge [
    source 91
    target 836
  ]
  edge [
    source 91
    target 837
  ]
  edge [
    source 91
    target 838
  ]
  edge [
    source 91
    target 839
  ]
  edge [
    source 91
    target 840
  ]
  edge [
    source 91
    target 841
  ]
  edge [
    source 91
    target 278
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 115
  ]
  edge [
    source 92
    target 842
  ]
  edge [
    source 92
    target 843
  ]
  edge [
    source 92
    target 844
  ]
  edge [
    source 92
    target 845
  ]
  edge [
    source 92
    target 846
  ]
  edge [
    source 92
    target 847
  ]
  edge [
    source 92
    target 848
  ]
  edge [
    source 92
    target 849
  ]
  edge [
    source 92
    target 118
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 104
  ]
  edge [
    source 93
    target 105
  ]
  edge [
    source 93
    target 128
  ]
  edge [
    source 93
    target 116
  ]
  edge [
    source 93
    target 117
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 850
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 106
  ]
  edge [
    source 95
    target 851
  ]
  edge [
    source 95
    target 852
  ]
  edge [
    source 95
    target 853
  ]
  edge [
    source 95
    target 854
  ]
  edge [
    source 95
    target 855
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 571
  ]
  edge [
    source 96
    target 165
  ]
  edge [
    source 96
    target 341
  ]
  edge [
    source 96
    target 856
  ]
  edge [
    source 96
    target 857
  ]
  edge [
    source 96
    target 858
  ]
  edge [
    source 96
    target 859
  ]
  edge [
    source 96
    target 860
  ]
  edge [
    source 96
    target 861
  ]
  edge [
    source 96
    target 862
  ]
  edge [
    source 96
    target 863
  ]
  edge [
    source 96
    target 564
  ]
  edge [
    source 96
    target 864
  ]
  edge [
    source 96
    target 98
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 865
  ]
  edge [
    source 97
    target 866
  ]
  edge [
    source 97
    target 867
  ]
  edge [
    source 97
    target 868
  ]
  edge [
    source 97
    target 869
  ]
  edge [
    source 97
    target 870
  ]
  edge [
    source 97
    target 871
  ]
  edge [
    source 97
    target 633
  ]
  edge [
    source 98
    target 872
  ]
  edge [
    source 98
    target 873
  ]
  edge [
    source 98
    target 874
  ]
  edge [
    source 98
    target 875
  ]
  edge [
    source 98
    target 876
  ]
  edge [
    source 98
    target 877
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 878
  ]
  edge [
    source 100
    target 879
  ]
  edge [
    source 100
    target 880
  ]
  edge [
    source 100
    target 881
  ]
  edge [
    source 100
    target 882
  ]
  edge [
    source 100
    target 883
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 884
  ]
  edge [
    source 101
    target 885
  ]
  edge [
    source 101
    target 886
  ]
  edge [
    source 101
    target 887
  ]
  edge [
    source 101
    target 775
  ]
  edge [
    source 101
    target 888
  ]
  edge [
    source 101
    target 889
  ]
  edge [
    source 101
    target 890
  ]
  edge [
    source 101
    target 891
  ]
  edge [
    source 101
    target 892
  ]
  edge [
    source 101
    target 893
  ]
  edge [
    source 102
    target 894
  ]
  edge [
    source 102
    target 895
  ]
  edge [
    source 102
    target 896
  ]
  edge [
    source 102
    target 897
  ]
  edge [
    source 102
    target 898
  ]
  edge [
    source 102
    target 136
  ]
  edge [
    source 102
    target 899
  ]
  edge [
    source 102
    target 900
  ]
  edge [
    source 102
    target 901
  ]
  edge [
    source 102
    target 902
  ]
  edge [
    source 102
    target 140
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 903
  ]
  edge [
    source 103
    target 904
  ]
  edge [
    source 103
    target 905
  ]
  edge [
    source 103
    target 906
  ]
  edge [
    source 103
    target 907
  ]
  edge [
    source 103
    target 908
  ]
  edge [
    source 103
    target 909
  ]
  edge [
    source 103
    target 754
  ]
  edge [
    source 103
    target 910
  ]
  edge [
    source 103
    target 911
  ]
  edge [
    source 103
    target 912
  ]
  edge [
    source 103
    target 913
  ]
  edge [
    source 103
    target 914
  ]
  edge [
    source 103
    target 915
  ]
  edge [
    source 103
    target 916
  ]
  edge [
    source 103
    target 917
  ]
  edge [
    source 103
    target 918
  ]
  edge [
    source 103
    target 919
  ]
  edge [
    source 103
    target 920
  ]
  edge [
    source 103
    target 921
  ]
  edge [
    source 103
    target 922
  ]
  edge [
    source 103
    target 923
  ]
  edge [
    source 104
    target 115
  ]
  edge [
    source 104
    target 116
  ]
  edge [
    source 104
    target 924
  ]
  edge [
    source 104
    target 553
  ]
  edge [
    source 104
    target 925
  ]
  edge [
    source 104
    target 926
  ]
  edge [
    source 104
    target 927
  ]
  edge [
    source 104
    target 928
  ]
  edge [
    source 104
    target 500
  ]
  edge [
    source 104
    target 929
  ]
  edge [
    source 104
    target 930
  ]
  edge [
    source 104
    target 931
  ]
  edge [
    source 104
    target 932
  ]
  edge [
    source 104
    target 933
  ]
  edge [
    source 104
    target 934
  ]
  edge [
    source 104
    target 935
  ]
  edge [
    source 104
    target 936
  ]
  edge [
    source 104
    target 937
  ]
  edge [
    source 104
    target 938
  ]
  edge [
    source 104
    target 939
  ]
  edge [
    source 104
    target 940
  ]
  edge [
    source 104
    target 569
  ]
  edge [
    source 104
    target 941
  ]
  edge [
    source 104
    target 942
  ]
  edge [
    source 104
    target 943
  ]
  edge [
    source 104
    target 944
  ]
  edge [
    source 104
    target 945
  ]
  edge [
    source 104
    target 946
  ]
  edge [
    source 104
    target 947
  ]
  edge [
    source 104
    target 736
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 948
  ]
  edge [
    source 105
    target 949
  ]
  edge [
    source 105
    target 262
  ]
  edge [
    source 105
    target 950
  ]
  edge [
    source 105
    target 951
  ]
  edge [
    source 105
    target 263
  ]
  edge [
    source 105
    target 952
  ]
  edge [
    source 105
    target 953
  ]
  edge [
    source 105
    target 954
  ]
  edge [
    source 105
    target 835
  ]
  edge [
    source 105
    target 955
  ]
  edge [
    source 105
    target 956
  ]
  edge [
    source 105
    target 957
  ]
  edge [
    source 105
    target 958
  ]
  edge [
    source 105
    target 959
  ]
  edge [
    source 105
    target 960
  ]
  edge [
    source 105
    target 961
  ]
  edge [
    source 105
    target 962
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 963
  ]
  edge [
    source 106
    target 964
  ]
  edge [
    source 106
    target 965
  ]
  edge [
    source 106
    target 966
  ]
  edge [
    source 106
    target 967
  ]
  edge [
    source 106
    target 968
  ]
  edge [
    source 106
    target 607
  ]
  edge [
    source 106
    target 969
  ]
  edge [
    source 106
    target 970
  ]
  edge [
    source 106
    target 971
  ]
  edge [
    source 106
    target 972
  ]
  edge [
    source 106
    target 703
  ]
  edge [
    source 106
    target 500
  ]
  edge [
    source 106
    target 973
  ]
  edge [
    source 106
    target 974
  ]
  edge [
    source 106
    target 975
  ]
  edge [
    source 106
    target 976
  ]
  edge [
    source 106
    target 977
  ]
  edge [
    source 106
    target 978
  ]
  edge [
    source 106
    target 979
  ]
  edge [
    source 106
    target 980
  ]
  edge [
    source 106
    target 617
  ]
  edge [
    source 106
    target 981
  ]
  edge [
    source 106
    target 982
  ]
  edge [
    source 106
    target 983
  ]
  edge [
    source 106
    target 622
  ]
  edge [
    source 106
    target 984
  ]
  edge [
    source 106
    target 732
  ]
  edge [
    source 106
    target 985
  ]
  edge [
    source 106
    target 986
  ]
  edge [
    source 106
    target 987
  ]
  edge [
    source 106
    target 988
  ]
  edge [
    source 106
    target 625
  ]
  edge [
    source 106
    target 989
  ]
  edge [
    source 106
    target 990
  ]
  edge [
    source 106
    target 991
  ]
  edge [
    source 106
    target 992
  ]
  edge [
    source 106
    target 993
  ]
  edge [
    source 106
    target 994
  ]
  edge [
    source 106
    target 995
  ]
  edge [
    source 106
    target 996
  ]
  edge [
    source 108
    target 997
  ]
  edge [
    source 108
    target 998
  ]
  edge [
    source 108
    target 999
  ]
  edge [
    source 108
    target 1000
  ]
  edge [
    source 108
    target 1001
  ]
  edge [
    source 108
    target 1002
  ]
  edge [
    source 108
    target 866
  ]
  edge [
    source 108
    target 630
  ]
  edge [
    source 108
    target 1003
  ]
  edge [
    source 108
    target 867
  ]
  edge [
    source 108
    target 1004
  ]
  edge [
    source 108
    target 1005
  ]
  edge [
    source 108
    target 1006
  ]
  edge [
    source 108
    target 1007
  ]
  edge [
    source 108
    target 1008
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 1009
  ]
  edge [
    source 109
    target 1010
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 1011
  ]
  edge [
    source 110
    target 186
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 1012
  ]
  edge [
    source 112
    target 1013
  ]
  edge [
    source 112
    target 1014
  ]
  edge [
    source 112
    target 1015
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 114
    target 1016
  ]
  edge [
    source 114
    target 1017
  ]
  edge [
    source 114
    target 119
  ]
  edge [
    source 115
    target 1018
  ]
  edge [
    source 115
    target 1019
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 1020
  ]
  edge [
    source 116
    target 1021
  ]
  edge [
    source 116
    target 959
  ]
  edge [
    source 116
    target 1022
  ]
  edge [
    source 116
    target 1023
  ]
  edge [
    source 116
    target 1024
  ]
  edge [
    source 116
    target 1025
  ]
  edge [
    source 116
    target 1026
  ]
  edge [
    source 116
    target 1027
  ]
  edge [
    source 116
    target 1028
  ]
  edge [
    source 116
    target 1029
  ]
  edge [
    source 116
    target 850
  ]
  edge [
    source 116
    target 1030
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 1031
  ]
  edge [
    source 117
    target 1032
  ]
  edge [
    source 117
    target 850
  ]
  edge [
    source 117
    target 1033
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 119
    target 1034
  ]
  edge [
    source 119
    target 1035
  ]
  edge [
    source 119
    target 1036
  ]
  edge [
    source 119
    target 1037
  ]
  edge [
    source 119
    target 1038
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 1039
  ]
  edge [
    source 120
    target 1040
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 1041
  ]
  edge [
    source 122
    target 1042
  ]
  edge [
    source 122
    target 1043
  ]
  edge [
    source 122
    target 1044
  ]
  edge [
    source 122
    target 1045
  ]
  edge [
    source 122
    target 1046
  ]
  edge [
    source 122
    target 629
  ]
  edge [
    source 122
    target 1047
  ]
  edge [
    source 122
    target 569
  ]
  edge [
    source 122
    target 1048
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 1049
  ]
  edge [
    source 123
    target 1050
  ]
  edge [
    source 123
    target 1051
  ]
  edge [
    source 123
    target 1052
  ]
  edge [
    source 123
    target 1053
  ]
  edge [
    source 123
    target 216
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 125
    target 1054
  ]
  edge [
    source 125
    target 1055
  ]
  edge [
    source 125
    target 1056
  ]
  edge [
    source 125
    target 1057
  ]
  edge [
    source 125
    target 1058
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 1059
  ]
  edge [
    source 128
    target 1060
  ]
  edge [
    source 128
    target 764
  ]
  edge [
    source 128
    target 1061
  ]
  edge [
    source 128
    target 252
  ]
  edge [
    source 128
    target 1062
  ]
  edge [
    source 128
    target 1063
  ]
  edge [
    source 128
    target 161
  ]
  edge [
    source 128
    target 1064
  ]
  edge [
    source 128
    target 834
  ]
  edge [
    source 128
    target 1065
  ]
  edge [
    source 128
    target 1066
  ]
  edge [
    source 128
    target 1067
  ]
  edge [
    source 128
    target 1068
  ]
  edge [
    source 128
    target 1069
  ]
  edge [
    source 1070
    target 1071
  ]
  edge [
    source 1073
    target 1074
  ]
  edge [
    source 1073
    target 1075
  ]
  edge [
    source 1074
    target 1075
  ]
  edge [
    source 1076
    target 1077
  ]
]
