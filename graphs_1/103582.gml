graph [
  maxDegree 12
  minDegree 1
  meanDegree 2
  density 0.07407407407407407
  graphCliqueNumber 3
  node [
    id 0
    label "czu&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "s&#322;ysze&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 3
    label "cover"
    origin "text"
  ]
  node [
    id 4
    label "swoje"
    origin "text"
  ]
  node [
    id 5
    label "utw&#243;r"
    origin "text"
  ]
  node [
    id 6
    label "listen"
  ]
  node [
    id 7
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 8
    label "postrzega&#263;"
  ]
  node [
    id 9
    label "s&#322;ycha&#263;"
  ]
  node [
    id 10
    label "read"
  ]
  node [
    id 11
    label "doba"
  ]
  node [
    id 12
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 13
    label "dzi&#347;"
  ]
  node [
    id 14
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 15
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 16
    label "aran&#380;acja"
  ]
  node [
    id 17
    label "kawa&#322;ek"
  ]
  node [
    id 18
    label "tre&#347;&#263;"
  ]
  node [
    id 19
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 20
    label "obrazowanie"
  ]
  node [
    id 21
    label "part"
  ]
  node [
    id 22
    label "organ"
  ]
  node [
    id 23
    label "komunikat"
  ]
  node [
    id 24
    label "tekst"
  ]
  node [
    id 25
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 26
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 27
    label "element_anatomiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
]
