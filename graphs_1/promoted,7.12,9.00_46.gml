graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9047619047619047
  density 0.04645760743321719
  graphCliqueNumber 3
  node [
    id 0
    label "powstawa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "zielono"
    origin "text"
  ]
  node [
    id 2
    label "czarna"
    origin "text"
  ]
  node [
    id 3
    label "koalicja"
    origin "text"
  ]
  node [
    id 4
    label "przeciwko"
    origin "text"
  ]
  node [
    id 5
    label "nord"
    origin "text"
  ]
  node [
    id 6
    label "stream"
    origin "text"
  ]
  node [
    id 7
    label "niemcy"
    origin "text"
  ]
  node [
    id 8
    label "dera"
    origin "text"
  ]
  node [
    id 9
    label "tagesspiegel"
    origin "text"
  ]
  node [
    id 10
    label "buntowa&#263;_si&#281;"
  ]
  node [
    id 11
    label "rise"
  ]
  node [
    id 12
    label "spring"
  ]
  node [
    id 13
    label "stawa&#263;"
  ]
  node [
    id 14
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 15
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 16
    label "plot"
  ]
  node [
    id 17
    label "podnosi&#263;_si&#281;"
  ]
  node [
    id 18
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 19
    label "publish"
  ]
  node [
    id 20
    label "blado"
  ]
  node [
    id 21
    label "&#347;wie&#380;o"
  ]
  node [
    id 22
    label "&#380;ywo"
  ]
  node [
    id 23
    label "zielony"
  ]
  node [
    id 24
    label "ch&#322;odno"
  ]
  node [
    id 25
    label "kawa"
  ]
  node [
    id 26
    label "czarny"
  ]
  node [
    id 27
    label "murzynek"
  ]
  node [
    id 28
    label "zwi&#261;zek"
  ]
  node [
    id 29
    label "ONZ"
  ]
  node [
    id 30
    label "alianci"
  ]
  node [
    id 31
    label "blok"
  ]
  node [
    id 32
    label "Paneuropa"
  ]
  node [
    id 33
    label "NATO"
  ]
  node [
    id 34
    label "confederation"
  ]
  node [
    id 35
    label "przesy&#322;"
  ]
  node [
    id 36
    label "Nord"
  ]
  node [
    id 37
    label "Stream"
  ]
  node [
    id 38
    label "2"
  ]
  node [
    id 39
    label "Tagesspiegel"
  ]
  node [
    id 40
    label "Angela"
  ]
  node [
    id 41
    label "Merkel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 40
    target 41
  ]
]
