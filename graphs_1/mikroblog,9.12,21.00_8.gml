graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "&#347;liczny"
    origin "text"
  ]
  node [
    id 1
    label "przyjemny"
  ]
  node [
    id 2
    label "z&#322;y"
  ]
  node [
    id 3
    label "dobry"
  ]
  node [
    id 4
    label "wspania&#322;y"
  ]
  node [
    id 5
    label "&#347;licznie"
  ]
  node [
    id 6
    label "pi&#281;knie"
  ]
  node [
    id 7
    label "skandaliczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
]
