graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.0727272727272728
  density 0.01901584653878232
  graphCliqueNumber 2
  node [
    id 0
    label "moja"
    origin "text"
  ]
  node [
    id 1
    label "przygoda"
    origin "text"
  ]
  node [
    id 2
    label "mma"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "ko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "koniec"
    origin "text"
  ]
  node [
    id 6
    label "zaj&#261;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "kocha&#263;"
    origin "text"
  ]
  node [
    id 8
    label "czecze&#324;ski"
    origin "text"
  ]
  node [
    id 9
    label "gangsterka"
    origin "text"
  ]
  node [
    id 10
    label "przemyt"
    origin "text"
  ]
  node [
    id 11
    label "zorganizowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "przest&#281;pczo&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "ksw"
    origin "text"
  ]
  node [
    id 15
    label "wydarzenie"
  ]
  node [
    id 16
    label "awanturnik"
  ]
  node [
    id 17
    label "fascynacja"
  ]
  node [
    id 18
    label "romans"
  ]
  node [
    id 19
    label "awantura"
  ]
  node [
    id 20
    label "affair"
  ]
  node [
    id 21
    label "zako&#324;cza&#263;"
  ]
  node [
    id 22
    label "przestawa&#263;"
  ]
  node [
    id 23
    label "robi&#263;"
  ]
  node [
    id 24
    label "satisfy"
  ]
  node [
    id 25
    label "close"
  ]
  node [
    id 26
    label "determine"
  ]
  node [
    id 27
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 28
    label "stanowi&#263;"
  ]
  node [
    id 29
    label "defenestracja"
  ]
  node [
    id 30
    label "szereg"
  ]
  node [
    id 31
    label "dzia&#322;anie"
  ]
  node [
    id 32
    label "miejsce"
  ]
  node [
    id 33
    label "ostatnie_podrygi"
  ]
  node [
    id 34
    label "kres"
  ]
  node [
    id 35
    label "agonia"
  ]
  node [
    id 36
    label "visitation"
  ]
  node [
    id 37
    label "szeol"
  ]
  node [
    id 38
    label "mogi&#322;a"
  ]
  node [
    id 39
    label "chwila"
  ]
  node [
    id 40
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 41
    label "pogrzebanie"
  ]
  node [
    id 42
    label "punkt"
  ]
  node [
    id 43
    label "&#380;a&#322;oba"
  ]
  node [
    id 44
    label "zabicie"
  ]
  node [
    id 45
    label "kres_&#380;ycia"
  ]
  node [
    id 46
    label "dostarczy&#263;"
  ]
  node [
    id 47
    label "wype&#322;ni&#263;"
  ]
  node [
    id 48
    label "anektowa&#263;"
  ]
  node [
    id 49
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 50
    label "obj&#261;&#263;"
  ]
  node [
    id 51
    label "zada&#263;"
  ]
  node [
    id 52
    label "sorb"
  ]
  node [
    id 53
    label "interest"
  ]
  node [
    id 54
    label "skorzysta&#263;"
  ]
  node [
    id 55
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 56
    label "wzi&#261;&#263;"
  ]
  node [
    id 57
    label "employment"
  ]
  node [
    id 58
    label "zapanowa&#263;"
  ]
  node [
    id 59
    label "do"
  ]
  node [
    id 60
    label "klasyfikacja"
  ]
  node [
    id 61
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 62
    label "bankrupt"
  ]
  node [
    id 63
    label "zabra&#263;"
  ]
  node [
    id 64
    label "spowodowa&#263;"
  ]
  node [
    id 65
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 66
    label "komornik"
  ]
  node [
    id 67
    label "prosecute"
  ]
  node [
    id 68
    label "seize"
  ]
  node [
    id 69
    label "topographic_point"
  ]
  node [
    id 70
    label "wzbudzi&#263;"
  ]
  node [
    id 71
    label "rozciekawi&#263;"
  ]
  node [
    id 72
    label "like"
  ]
  node [
    id 73
    label "czu&#263;"
  ]
  node [
    id 74
    label "chowa&#263;"
  ]
  node [
    id 75
    label "mi&#322;owa&#263;"
  ]
  node [
    id 76
    label "j&#281;zyk_ergatywny"
  ]
  node [
    id 77
    label "po_czecze&#324;sku"
  ]
  node [
    id 78
    label "kaukaski"
  ]
  node [
    id 79
    label "przest&#281;pstwo"
  ]
  node [
    id 80
    label "towar"
  ]
  node [
    id 81
    label "zaplanowa&#263;"
  ]
  node [
    id 82
    label "dostosowa&#263;"
  ]
  node [
    id 83
    label "przygotowa&#263;"
  ]
  node [
    id 84
    label "stworzy&#263;"
  ]
  node [
    id 85
    label "stage"
  ]
  node [
    id 86
    label "pozyska&#263;"
  ]
  node [
    id 87
    label "urobi&#263;"
  ]
  node [
    id 88
    label "plan"
  ]
  node [
    id 89
    label "ensnare"
  ]
  node [
    id 90
    label "standard"
  ]
  node [
    id 91
    label "skupi&#263;"
  ]
  node [
    id 92
    label "wprowadzi&#263;"
  ]
  node [
    id 93
    label "gang"
  ]
  node [
    id 94
    label "bezprawie"
  ]
  node [
    id 95
    label "patologia"
  ]
  node [
    id 96
    label "criminalism"
  ]
  node [
    id 97
    label "banda"
  ]
  node [
    id 98
    label "problem_spo&#322;eczny"
  ]
  node [
    id 99
    label "si&#281;ga&#263;"
  ]
  node [
    id 100
    label "trwa&#263;"
  ]
  node [
    id 101
    label "obecno&#347;&#263;"
  ]
  node [
    id 102
    label "stan"
  ]
  node [
    id 103
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 104
    label "stand"
  ]
  node [
    id 105
    label "mie&#263;_miejsce"
  ]
  node [
    id 106
    label "uczestniczy&#263;"
  ]
  node [
    id 107
    label "chodzi&#263;"
  ]
  node [
    id 108
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 109
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
]
