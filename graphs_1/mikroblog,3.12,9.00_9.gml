graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "reda"
    origin "text"
  ]
  node [
    id 1
    label "dead"
    origin "text"
  ]
  node [
    id 2
    label "redemption"
    origin "text"
  ]
  node [
    id 3
    label "koloryzowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "akwatorium"
  ]
  node [
    id 5
    label "morze"
  ]
  node [
    id 6
    label "falochron"
  ]
  node [
    id 7
    label "barwi&#263;"
  ]
  node [
    id 8
    label "ubarwia&#263;"
  ]
  node [
    id 9
    label "przesadza&#263;"
  ]
  node [
    id 10
    label "color"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
]
