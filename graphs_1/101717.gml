graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.0232558139534884
  density 0.023803009575923392
  graphCliqueNumber 2
  node [
    id 0
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "tym"
    origin "text"
  ]
  node [
    id 2
    label "wszyscy"
    origin "text"
  ]
  node [
    id 3
    label "zastanawia&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "ile"
    origin "text"
  ]
  node [
    id 6
    label "powszechny"
    origin "text"
  ]
  node [
    id 7
    label "przyzwyczajenie"
    origin "text"
  ]
  node [
    id 8
    label "sprzeciw"
    origin "text"
  ]
  node [
    id 9
    label "wp&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "przykleja&#263;"
    origin "text"
  ]
  node [
    id 13
    label "blog"
    origin "text"
  ]
  node [
    id 14
    label "etykietka"
    origin "text"
  ]
  node [
    id 15
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 16
    label "dzielenie"
    origin "text"
  ]
  node [
    id 17
    label "ozdabia&#263;"
  ]
  node [
    id 18
    label "dysgrafia"
  ]
  node [
    id 19
    label "prasa"
  ]
  node [
    id 20
    label "spell"
  ]
  node [
    id 21
    label "skryba"
  ]
  node [
    id 22
    label "donosi&#263;"
  ]
  node [
    id 23
    label "code"
  ]
  node [
    id 24
    label "tekst"
  ]
  node [
    id 25
    label "dysortografia"
  ]
  node [
    id 26
    label "read"
  ]
  node [
    id 27
    label "tworzy&#263;"
  ]
  node [
    id 28
    label "formu&#322;owa&#263;"
  ]
  node [
    id 29
    label "styl"
  ]
  node [
    id 30
    label "stawia&#263;"
  ]
  node [
    id 31
    label "strike"
  ]
  node [
    id 32
    label "interesowa&#263;"
  ]
  node [
    id 33
    label "zbiorowy"
  ]
  node [
    id 34
    label "generalny"
  ]
  node [
    id 35
    label "cz&#281;sty"
  ]
  node [
    id 36
    label "znany"
  ]
  node [
    id 37
    label "powszechnie"
  ]
  node [
    id 38
    label "zwyczaj"
  ]
  node [
    id 39
    label "oddzia&#322;anie"
  ]
  node [
    id 40
    label "nawyknienie"
  ]
  node [
    id 41
    label "use"
  ]
  node [
    id 42
    label "reakcja"
  ]
  node [
    id 43
    label "protestacja"
  ]
  node [
    id 44
    label "czerwona_kartka"
  ]
  node [
    id 45
    label "zasila&#263;"
  ]
  node [
    id 46
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 47
    label "work"
  ]
  node [
    id 48
    label "przyp&#322;ywa&#263;"
  ]
  node [
    id 49
    label "kapita&#322;"
  ]
  node [
    id 50
    label "determine"
  ]
  node [
    id 51
    label "dochodzi&#263;"
  ]
  node [
    id 52
    label "pour"
  ]
  node [
    id 53
    label "ciek_wodny"
  ]
  node [
    id 54
    label "strategia"
  ]
  node [
    id 55
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 56
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 57
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 58
    label "stick"
  ]
  node [
    id 59
    label "przymocowywa&#263;"
  ]
  node [
    id 60
    label "komcio"
  ]
  node [
    id 61
    label "strona"
  ]
  node [
    id 62
    label "blogosfera"
  ]
  node [
    id 63
    label "pami&#281;tnik"
  ]
  node [
    id 64
    label "tab"
  ]
  node [
    id 65
    label "tabliczka"
  ]
  node [
    id 66
    label "naklejka"
  ]
  node [
    id 67
    label "wsp&#243;&#322;pracownictwo"
  ]
  node [
    id 68
    label "dzielna"
  ]
  node [
    id 69
    label "liczenie"
  ]
  node [
    id 70
    label "robienie"
  ]
  node [
    id 71
    label "rozdawanie"
  ]
  node [
    id 72
    label "czynno&#347;&#263;"
  ]
  node [
    id 73
    label "stosowanie"
  ]
  node [
    id 74
    label "rozprowadzanie"
  ]
  node [
    id 75
    label "separation"
  ]
  node [
    id 76
    label "dzielnik"
  ]
  node [
    id 77
    label "sk&#322;&#243;canie"
  ]
  node [
    id 78
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 79
    label "division"
  ]
  node [
    id 80
    label "wyodr&#281;bnianie"
  ]
  node [
    id 81
    label "powodowanie"
  ]
  node [
    id 82
    label "contribution"
  ]
  node [
    id 83
    label "iloraz"
  ]
  node [
    id 84
    label "przeszkoda"
  ]
  node [
    id 85
    label "dzielenie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 60
  ]
  edge [
    source 13
    target 61
  ]
  edge [
    source 13
    target 62
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 65
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 67
  ]
  edge [
    source 15
    target 56
  ]
  edge [
    source 16
    target 68
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 16
    target 70
  ]
  edge [
    source 16
    target 71
  ]
  edge [
    source 16
    target 72
  ]
  edge [
    source 16
    target 73
  ]
  edge [
    source 16
    target 74
  ]
  edge [
    source 16
    target 75
  ]
  edge [
    source 16
    target 76
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 16
    target 79
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
]
