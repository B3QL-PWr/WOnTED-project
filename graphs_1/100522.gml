graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.10457516339869281
  graphCliqueNumber 4
  node [
    id 0
    label "krzysztof"
    origin "text"
  ]
  node [
    id 1
    label "mehlich"
    origin "text"
  ]
  node [
    id 2
    label "Krzysztofa"
  ]
  node [
    id 3
    label "Mehlich"
  ]
  node [
    id 4
    label "strzelec"
  ]
  node [
    id 5
    label "opolski"
  ]
  node [
    id 6
    label "Jan"
  ]
  node [
    id 7
    label "Victoria"
  ]
  node [
    id 8
    label "Racib&#243;rz"
  ]
  node [
    id 9
    label "AZS"
  ]
  node [
    id 10
    label "AWF"
  ]
  node [
    id 11
    label "Katowice"
  ]
  node [
    id 12
    label "san"
  ]
  node [
    id 13
    label "Sebastiana"
  ]
  node [
    id 14
    label "puchar"
  ]
  node [
    id 15
    label "europ"
  ]
  node [
    id 16
    label "wyspa"
  ]
  node [
    id 17
    label "lekkoatletyka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
]
