graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.0388349514563107
  density 0.019988577955454025
  graphCliqueNumber 3
  node [
    id 0
    label "mimo"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "ogl&#261;dalno&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "tvp"
    origin "text"
  ]
  node [
    id 4
    label "stacja"
    origin "text"
  ]
  node [
    id 5
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "zdj&#261;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "program"
    origin "text"
  ]
  node [
    id 8
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 9
    label "antenty"
    origin "text"
  ]
  node [
    id 10
    label "pomy&#347;lny"
  ]
  node [
    id 11
    label "skuteczny"
  ]
  node [
    id 12
    label "moralny"
  ]
  node [
    id 13
    label "korzystny"
  ]
  node [
    id 14
    label "odpowiedni"
  ]
  node [
    id 15
    label "zwrot"
  ]
  node [
    id 16
    label "dobrze"
  ]
  node [
    id 17
    label "pozytywny"
  ]
  node [
    id 18
    label "grzeczny"
  ]
  node [
    id 19
    label "powitanie"
  ]
  node [
    id 20
    label "mi&#322;y"
  ]
  node [
    id 21
    label "dobroczynny"
  ]
  node [
    id 22
    label "pos&#322;uszny"
  ]
  node [
    id 23
    label "ca&#322;y"
  ]
  node [
    id 24
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 25
    label "czw&#243;rka"
  ]
  node [
    id 26
    label "spokojny"
  ]
  node [
    id 27
    label "&#347;mieszny"
  ]
  node [
    id 28
    label "drogi"
  ]
  node [
    id 29
    label "hearing"
  ]
  node [
    id 30
    label "liczba"
  ]
  node [
    id 31
    label "miejsce"
  ]
  node [
    id 32
    label "instytucja"
  ]
  node [
    id 33
    label "droga_krzy&#380;owa"
  ]
  node [
    id 34
    label "punkt"
  ]
  node [
    id 35
    label "urz&#261;dzenie"
  ]
  node [
    id 36
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 37
    label "siedziba"
  ]
  node [
    id 38
    label "sta&#263;_si&#281;"
  ]
  node [
    id 39
    label "zrobi&#263;"
  ]
  node [
    id 40
    label "podj&#261;&#263;"
  ]
  node [
    id 41
    label "determine"
  ]
  node [
    id 42
    label "uwolni&#263;"
  ]
  node [
    id 43
    label "wzi&#261;&#263;"
  ]
  node [
    id 44
    label "cenzura"
  ]
  node [
    id 45
    label "abolicjonista"
  ]
  node [
    id 46
    label "pull"
  ]
  node [
    id 47
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 48
    label "draw"
  ]
  node [
    id 49
    label "odsun&#261;&#263;"
  ]
  node [
    id 50
    label "zabroni&#263;"
  ]
  node [
    id 51
    label "wyuzda&#263;"
  ]
  node [
    id 52
    label "lift"
  ]
  node [
    id 53
    label "spis"
  ]
  node [
    id 54
    label "odinstalowanie"
  ]
  node [
    id 55
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 56
    label "za&#322;o&#380;enie"
  ]
  node [
    id 57
    label "podstawa"
  ]
  node [
    id 58
    label "emitowanie"
  ]
  node [
    id 59
    label "odinstalowywanie"
  ]
  node [
    id 60
    label "instrukcja"
  ]
  node [
    id 61
    label "teleferie"
  ]
  node [
    id 62
    label "emitowa&#263;"
  ]
  node [
    id 63
    label "wytw&#243;r"
  ]
  node [
    id 64
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 65
    label "sekcja_krytyczna"
  ]
  node [
    id 66
    label "oferta"
  ]
  node [
    id 67
    label "prezentowa&#263;"
  ]
  node [
    id 68
    label "blok"
  ]
  node [
    id 69
    label "podprogram"
  ]
  node [
    id 70
    label "tryb"
  ]
  node [
    id 71
    label "dzia&#322;"
  ]
  node [
    id 72
    label "broszura"
  ]
  node [
    id 73
    label "deklaracja"
  ]
  node [
    id 74
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 75
    label "struktura_organizacyjna"
  ]
  node [
    id 76
    label "zaprezentowanie"
  ]
  node [
    id 77
    label "informatyka"
  ]
  node [
    id 78
    label "booklet"
  ]
  node [
    id 79
    label "menu"
  ]
  node [
    id 80
    label "oprogramowanie"
  ]
  node [
    id 81
    label "instalowanie"
  ]
  node [
    id 82
    label "furkacja"
  ]
  node [
    id 83
    label "odinstalowa&#263;"
  ]
  node [
    id 84
    label "instalowa&#263;"
  ]
  node [
    id 85
    label "pirat"
  ]
  node [
    id 86
    label "zainstalowanie"
  ]
  node [
    id 87
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 88
    label "ogranicznik_referencyjny"
  ]
  node [
    id 89
    label "zainstalowa&#263;"
  ]
  node [
    id 90
    label "kana&#322;"
  ]
  node [
    id 91
    label "zaprezentowa&#263;"
  ]
  node [
    id 92
    label "interfejs"
  ]
  node [
    id 93
    label "odinstalowywa&#263;"
  ]
  node [
    id 94
    label "folder"
  ]
  node [
    id 95
    label "course_of_study"
  ]
  node [
    id 96
    label "ram&#243;wka"
  ]
  node [
    id 97
    label "prezentowanie"
  ]
  node [
    id 98
    label "okno"
  ]
  node [
    id 99
    label "cz&#322;owiek"
  ]
  node [
    id 100
    label "bli&#378;ni"
  ]
  node [
    id 101
    label "swojak"
  ]
  node [
    id 102
    label "samodzielny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
]
