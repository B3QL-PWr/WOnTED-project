graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.1827160493827162
  density 0.005402762498472069
  graphCliqueNumber 4
  node [
    id 0
    label "taki"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiadek"
    origin "text"
  ]
  node [
    id 2
    label "nikt"
    origin "text"
  ]
  node [
    id 3
    label "uwierzy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pani"
    origin "text"
  ]
  node [
    id 6
    label "wsp&#243;lnik"
    origin "text"
  ]
  node [
    id 7
    label "szpieg"
    origin "text"
  ]
  node [
    id 8
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "&#347;lub"
    origin "text"
  ]
  node [
    id 10
    label "zainscenizowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "komedia"
    origin "text"
  ]
  node [
    id 12
    label "&#378;le"
    origin "text"
  ]
  node [
    id 13
    label "trafi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "podejrzewa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "polecenie"
    origin "text"
  ]
  node [
    id 16
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 17
    label "&#347;ledzi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 18
    label "krynica"
    origin "text"
  ]
  node [
    id 19
    label "ot&#243;&#380;"
    origin "text"
  ]
  node [
    id 20
    label "rzeczywi&#347;cie"
    origin "text"
  ]
  node [
    id 21
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 22
    label "nic"
    origin "text"
  ]
  node [
    id 23
    label "tym"
    origin "text"
  ]
  node [
    id 24
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 25
    label "tak"
    origin "text"
  ]
  node [
    id 26
    label "droga"
    origin "text"
  ]
  node [
    id 27
    label "robi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 28
    label "wszystko"
    origin "text"
  ]
  node [
    id 29
    label "w&#322;asny"
    origin "text"
  ]
  node [
    id 30
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 31
    label "my&#263;"
    origin "text"
  ]
  node [
    id 32
    label "si&#281;"
    origin "text"
  ]
  node [
    id 33
    label "s&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 34
    label "zdoby&#263;by&#263;"
    origin "text"
  ]
  node [
    id 35
    label "fotografia"
    origin "text"
  ]
  node [
    id 36
    label "odbitka"
    origin "text"
  ]
  node [
    id 37
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 38
    label "ten"
    origin "text"
  ]
  node [
    id 39
    label "podstawa"
    origin "text"
  ]
  node [
    id 40
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 41
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "kima"
    origin "text"
  ]
  node [
    id 43
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 44
    label "okre&#347;lony"
  ]
  node [
    id 45
    label "jaki&#347;"
  ]
  node [
    id 46
    label "cz&#322;owiek"
  ]
  node [
    id 47
    label "s&#261;d"
  ]
  node [
    id 48
    label "osoba_fizyczna"
  ]
  node [
    id 49
    label "uczestnik"
  ]
  node [
    id 50
    label "obserwator"
  ]
  node [
    id 51
    label "dru&#380;ba"
  ]
  node [
    id 52
    label "miernota"
  ]
  node [
    id 53
    label "ciura"
  ]
  node [
    id 54
    label "trust"
  ]
  node [
    id 55
    label "zacz&#261;&#263;"
  ]
  node [
    id 56
    label "uzna&#263;"
  ]
  node [
    id 57
    label "si&#281;ga&#263;"
  ]
  node [
    id 58
    label "trwa&#263;"
  ]
  node [
    id 59
    label "obecno&#347;&#263;"
  ]
  node [
    id 60
    label "stan"
  ]
  node [
    id 61
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 62
    label "stand"
  ]
  node [
    id 63
    label "mie&#263;_miejsce"
  ]
  node [
    id 64
    label "uczestniczy&#263;"
  ]
  node [
    id 65
    label "chodzi&#263;"
  ]
  node [
    id 66
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 67
    label "equal"
  ]
  node [
    id 68
    label "przekwitanie"
  ]
  node [
    id 69
    label "zwrot"
  ]
  node [
    id 70
    label "uleganie"
  ]
  node [
    id 71
    label "ulega&#263;"
  ]
  node [
    id 72
    label "partner"
  ]
  node [
    id 73
    label "doros&#322;y"
  ]
  node [
    id 74
    label "przyw&#243;dczyni"
  ]
  node [
    id 75
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 76
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 77
    label "ulec"
  ]
  node [
    id 78
    label "kobita"
  ]
  node [
    id 79
    label "&#322;ono"
  ]
  node [
    id 80
    label "kobieta"
  ]
  node [
    id 81
    label "w&#322;a&#347;cicielka"
  ]
  node [
    id 82
    label "m&#281;&#380;yna"
  ]
  node [
    id 83
    label "babka"
  ]
  node [
    id 84
    label "samica"
  ]
  node [
    id 85
    label "pa&#324;stwo"
  ]
  node [
    id 86
    label "ulegni&#281;cie"
  ]
  node [
    id 87
    label "menopauza"
  ]
  node [
    id 88
    label "sp&#243;lnik"
  ]
  node [
    id 89
    label "uczestniczenie"
  ]
  node [
    id 90
    label "przedsi&#281;biorca"
  ]
  node [
    id 91
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 92
    label "agentura"
  ]
  node [
    id 93
    label "informator"
  ]
  node [
    id 94
    label "wywiad"
  ]
  node [
    id 95
    label "donosiciel"
  ]
  node [
    id 96
    label "szpicel"
  ]
  node [
    id 97
    label "wojsko"
  ]
  node [
    id 98
    label "&#347;ledziciel"
  ]
  node [
    id 99
    label "du&#380;y"
  ]
  node [
    id 100
    label "jedyny"
  ]
  node [
    id 101
    label "kompletny"
  ]
  node [
    id 102
    label "zdr&#243;w"
  ]
  node [
    id 103
    label "&#380;ywy"
  ]
  node [
    id 104
    label "ca&#322;o"
  ]
  node [
    id 105
    label "pe&#322;ny"
  ]
  node [
    id 106
    label "calu&#347;ko"
  ]
  node [
    id 107
    label "podobny"
  ]
  node [
    id 108
    label "przysi&#281;ga"
  ]
  node [
    id 109
    label "wedding"
  ]
  node [
    id 110
    label "&#347;lubowanie"
  ]
  node [
    id 111
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 112
    label "chupa"
  ]
  node [
    id 113
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 114
    label "po&#347;lubiny"
  ]
  node [
    id 115
    label "zapowiedzi"
  ]
  node [
    id 116
    label "vow"
  ]
  node [
    id 117
    label "zorganizowa&#263;"
  ]
  node [
    id 118
    label "zaadaptowa&#263;"
  ]
  node [
    id 119
    label "produce"
  ]
  node [
    id 120
    label "grow"
  ]
  node [
    id 121
    label "bulwar&#243;wka"
  ]
  node [
    id 122
    label "cyrk"
  ]
  node [
    id 123
    label "drollery"
  ]
  node [
    id 124
    label "Allen"
  ]
  node [
    id 125
    label "film"
  ]
  node [
    id 126
    label "dramat"
  ]
  node [
    id 127
    label "stand-up"
  ]
  node [
    id 128
    label "tragikomedia"
  ]
  node [
    id 129
    label "niezgodnie"
  ]
  node [
    id 130
    label "niepomy&#347;lnie"
  ]
  node [
    id 131
    label "negatywnie"
  ]
  node [
    id 132
    label "piesko"
  ]
  node [
    id 133
    label "z&#322;y"
  ]
  node [
    id 134
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 135
    label "gorzej"
  ]
  node [
    id 136
    label "niekorzystnie"
  ]
  node [
    id 137
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 138
    label "przypasowa&#263;"
  ]
  node [
    id 139
    label "wpa&#347;&#263;"
  ]
  node [
    id 140
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 141
    label "spotka&#263;"
  ]
  node [
    id 142
    label "dotrze&#263;"
  ]
  node [
    id 143
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 144
    label "happen"
  ]
  node [
    id 145
    label "znale&#378;&#263;"
  ]
  node [
    id 146
    label "hit"
  ]
  node [
    id 147
    label "pocisk"
  ]
  node [
    id 148
    label "stumble"
  ]
  node [
    id 149
    label "dolecie&#263;"
  ]
  node [
    id 150
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 151
    label "przewidywa&#263;"
  ]
  node [
    id 152
    label "intuition"
  ]
  node [
    id 153
    label "os&#261;dza&#263;"
  ]
  node [
    id 154
    label "inflict"
  ]
  node [
    id 155
    label "przypuszcza&#263;"
  ]
  node [
    id 156
    label "zadanie"
  ]
  node [
    id 157
    label "wypowied&#378;"
  ]
  node [
    id 158
    label "education"
  ]
  node [
    id 159
    label "zaordynowanie"
  ]
  node [
    id 160
    label "rekomendacja"
  ]
  node [
    id 161
    label "ukaz"
  ]
  node [
    id 162
    label "przesadzenie"
  ]
  node [
    id 163
    label "statement"
  ]
  node [
    id 164
    label "recommendation"
  ]
  node [
    id 165
    label "powierzenie"
  ]
  node [
    id 166
    label "pobiegni&#281;cie"
  ]
  node [
    id 167
    label "consign"
  ]
  node [
    id 168
    label "pognanie"
  ]
  node [
    id 169
    label "doradzenie"
  ]
  node [
    id 170
    label "pan_domu"
  ]
  node [
    id 171
    label "ch&#322;op"
  ]
  node [
    id 172
    label "ma&#322;&#380;onek"
  ]
  node [
    id 173
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 174
    label "stary"
  ]
  node [
    id 175
    label "&#347;lubny"
  ]
  node [
    id 176
    label "pan_i_w&#322;adca"
  ]
  node [
    id 177
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 178
    label "pan_m&#322;ody"
  ]
  node [
    id 179
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 180
    label "strumie&#324;"
  ]
  node [
    id 181
    label "realny"
  ]
  node [
    id 182
    label "naprawd&#281;"
  ]
  node [
    id 183
    label "czyj&#347;"
  ]
  node [
    id 184
    label "g&#243;wno"
  ]
  node [
    id 185
    label "love"
  ]
  node [
    id 186
    label "ilo&#347;&#263;"
  ]
  node [
    id 187
    label "brak"
  ]
  node [
    id 188
    label "cognizance"
  ]
  node [
    id 189
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 190
    label "journey"
  ]
  node [
    id 191
    label "podbieg"
  ]
  node [
    id 192
    label "bezsilnikowy"
  ]
  node [
    id 193
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 194
    label "wylot"
  ]
  node [
    id 195
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 196
    label "drogowskaz"
  ]
  node [
    id 197
    label "nawierzchnia"
  ]
  node [
    id 198
    label "turystyka"
  ]
  node [
    id 199
    label "budowla"
  ]
  node [
    id 200
    label "spos&#243;b"
  ]
  node [
    id 201
    label "passage"
  ]
  node [
    id 202
    label "marszrutyzacja"
  ]
  node [
    id 203
    label "zbior&#243;wka"
  ]
  node [
    id 204
    label "ekskursja"
  ]
  node [
    id 205
    label "rajza"
  ]
  node [
    id 206
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 207
    label "ruch"
  ]
  node [
    id 208
    label "trasa"
  ]
  node [
    id 209
    label "wyb&#243;j"
  ]
  node [
    id 210
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 211
    label "ekwipunek"
  ]
  node [
    id 212
    label "korona_drogi"
  ]
  node [
    id 213
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 214
    label "pobocze"
  ]
  node [
    id 215
    label "lock"
  ]
  node [
    id 216
    label "absolut"
  ]
  node [
    id 217
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 218
    label "swoisty"
  ]
  node [
    id 219
    label "osobny"
  ]
  node [
    id 220
    label "zwi&#261;zany"
  ]
  node [
    id 221
    label "samodzielny"
  ]
  node [
    id 222
    label "krzy&#380;"
  ]
  node [
    id 223
    label "paw"
  ]
  node [
    id 224
    label "rami&#281;"
  ]
  node [
    id 225
    label "gestykulowanie"
  ]
  node [
    id 226
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 227
    label "pracownik"
  ]
  node [
    id 228
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 229
    label "bramkarz"
  ]
  node [
    id 230
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 231
    label "handwriting"
  ]
  node [
    id 232
    label "hasta"
  ]
  node [
    id 233
    label "pi&#322;ka"
  ]
  node [
    id 234
    label "&#322;okie&#263;"
  ]
  node [
    id 235
    label "zagrywka"
  ]
  node [
    id 236
    label "obietnica"
  ]
  node [
    id 237
    label "przedrami&#281;"
  ]
  node [
    id 238
    label "chwyta&#263;"
  ]
  node [
    id 239
    label "r&#261;czyna"
  ]
  node [
    id 240
    label "cecha"
  ]
  node [
    id 241
    label "wykroczenie"
  ]
  node [
    id 242
    label "kroki"
  ]
  node [
    id 243
    label "palec"
  ]
  node [
    id 244
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 245
    label "graba"
  ]
  node [
    id 246
    label "hand"
  ]
  node [
    id 247
    label "nadgarstek"
  ]
  node [
    id 248
    label "pomocnik"
  ]
  node [
    id 249
    label "k&#322;&#261;b"
  ]
  node [
    id 250
    label "hazena"
  ]
  node [
    id 251
    label "gestykulowa&#263;"
  ]
  node [
    id 252
    label "cmoknonsens"
  ]
  node [
    id 253
    label "d&#322;o&#324;"
  ]
  node [
    id 254
    label "chwytanie"
  ]
  node [
    id 255
    label "czerwona_kartka"
  ]
  node [
    id 256
    label "wash"
  ]
  node [
    id 257
    label "oczyszcza&#263;"
  ]
  node [
    id 258
    label "wsp&#243;&#322;rz&#261;dzi&#263;"
  ]
  node [
    id 259
    label "hold"
  ]
  node [
    id 260
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 261
    label "my&#347;le&#263;"
  ]
  node [
    id 262
    label "sprawowa&#263;"
  ]
  node [
    id 263
    label "zas&#261;dza&#263;"
  ]
  node [
    id 264
    label "powodowa&#263;"
  ]
  node [
    id 265
    label "deliver"
  ]
  node [
    id 266
    label "bezcieniowy"
  ]
  node [
    id 267
    label "wyretuszowanie"
  ]
  node [
    id 268
    label "przedstawienie"
  ]
  node [
    id 269
    label "fota"
  ]
  node [
    id 270
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 271
    label "podlew"
  ]
  node [
    id 272
    label "ziarno"
  ]
  node [
    id 273
    label "obraz"
  ]
  node [
    id 274
    label "legitymacja"
  ]
  node [
    id 275
    label "przepa&#322;"
  ]
  node [
    id 276
    label "fotogaleria"
  ]
  node [
    id 277
    label "winieta"
  ]
  node [
    id 278
    label "retuszowanie"
  ]
  node [
    id 279
    label "monid&#322;o"
  ]
  node [
    id 280
    label "talbotypia"
  ]
  node [
    id 281
    label "wyretuszowa&#263;"
  ]
  node [
    id 282
    label "fototeka"
  ]
  node [
    id 283
    label "photograph"
  ]
  node [
    id 284
    label "ekspozycja"
  ]
  node [
    id 285
    label "retuszowa&#263;"
  ]
  node [
    id 286
    label "nuta"
  ]
  node [
    id 287
    label "print"
  ]
  node [
    id 288
    label "sylaba"
  ]
  node [
    id 289
    label "kopia"
  ]
  node [
    id 290
    label "wers"
  ]
  node [
    id 291
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 292
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 293
    label "obszar"
  ]
  node [
    id 294
    label "obiekt_naturalny"
  ]
  node [
    id 295
    label "przedmiot"
  ]
  node [
    id 296
    label "biosfera"
  ]
  node [
    id 297
    label "grupa"
  ]
  node [
    id 298
    label "stw&#243;r"
  ]
  node [
    id 299
    label "Stary_&#346;wiat"
  ]
  node [
    id 300
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 301
    label "rzecz"
  ]
  node [
    id 302
    label "magnetosfera"
  ]
  node [
    id 303
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 304
    label "environment"
  ]
  node [
    id 305
    label "Nowy_&#346;wiat"
  ]
  node [
    id 306
    label "geosfera"
  ]
  node [
    id 307
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 308
    label "planeta"
  ]
  node [
    id 309
    label "przejmowa&#263;"
  ]
  node [
    id 310
    label "litosfera"
  ]
  node [
    id 311
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 312
    label "makrokosmos"
  ]
  node [
    id 313
    label "barysfera"
  ]
  node [
    id 314
    label "biota"
  ]
  node [
    id 315
    label "p&#243;&#322;noc"
  ]
  node [
    id 316
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 317
    label "fauna"
  ]
  node [
    id 318
    label "wszechstworzenie"
  ]
  node [
    id 319
    label "geotermia"
  ]
  node [
    id 320
    label "biegun"
  ]
  node [
    id 321
    label "ekosystem"
  ]
  node [
    id 322
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 323
    label "teren"
  ]
  node [
    id 324
    label "zjawisko"
  ]
  node [
    id 325
    label "p&#243;&#322;kula"
  ]
  node [
    id 326
    label "atmosfera"
  ]
  node [
    id 327
    label "mikrokosmos"
  ]
  node [
    id 328
    label "class"
  ]
  node [
    id 329
    label "po&#322;udnie"
  ]
  node [
    id 330
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 331
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 332
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 333
    label "przejmowanie"
  ]
  node [
    id 334
    label "przestrze&#324;"
  ]
  node [
    id 335
    label "asymilowanie_si&#281;"
  ]
  node [
    id 336
    label "przej&#261;&#263;"
  ]
  node [
    id 337
    label "ekosfera"
  ]
  node [
    id 338
    label "przyroda"
  ]
  node [
    id 339
    label "ciemna_materia"
  ]
  node [
    id 340
    label "geoida"
  ]
  node [
    id 341
    label "Wsch&#243;d"
  ]
  node [
    id 342
    label "populace"
  ]
  node [
    id 343
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 344
    label "huczek"
  ]
  node [
    id 345
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 346
    label "Ziemia"
  ]
  node [
    id 347
    label "universe"
  ]
  node [
    id 348
    label "ozonosfera"
  ]
  node [
    id 349
    label "rze&#378;ba"
  ]
  node [
    id 350
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 351
    label "zagranica"
  ]
  node [
    id 352
    label "hydrosfera"
  ]
  node [
    id 353
    label "woda"
  ]
  node [
    id 354
    label "kuchnia"
  ]
  node [
    id 355
    label "przej&#281;cie"
  ]
  node [
    id 356
    label "czarna_dziura"
  ]
  node [
    id 357
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 358
    label "morze"
  ]
  node [
    id 359
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 360
    label "podstawowy"
  ]
  node [
    id 361
    label "strategia"
  ]
  node [
    id 362
    label "pot&#281;ga"
  ]
  node [
    id 363
    label "zasadzenie"
  ]
  node [
    id 364
    label "za&#322;o&#380;enie"
  ]
  node [
    id 365
    label "&#347;ciana"
  ]
  node [
    id 366
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 367
    label "documentation"
  ]
  node [
    id 368
    label "dzieci&#281;ctwo"
  ]
  node [
    id 369
    label "pomys&#322;"
  ]
  node [
    id 370
    label "bok"
  ]
  node [
    id 371
    label "d&#243;&#322;"
  ]
  node [
    id 372
    label "punkt_odniesienia"
  ]
  node [
    id 373
    label "column"
  ]
  node [
    id 374
    label "zasadzi&#263;"
  ]
  node [
    id 375
    label "background"
  ]
  node [
    id 376
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 377
    label "represent"
  ]
  node [
    id 378
    label "powiedzie&#263;"
  ]
  node [
    id 379
    label "testify"
  ]
  node [
    id 380
    label "oznajmi&#263;"
  ]
  node [
    id 381
    label "declare"
  ]
  node [
    id 382
    label "sen"
  ]
  node [
    id 383
    label "kszta&#322;townik"
  ]
  node [
    id 384
    label "tentegowa&#263;"
  ]
  node [
    id 385
    label "urz&#261;dza&#263;"
  ]
  node [
    id 386
    label "give"
  ]
  node [
    id 387
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 388
    label "czyni&#263;"
  ]
  node [
    id 389
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 390
    label "post&#281;powa&#263;"
  ]
  node [
    id 391
    label "wydala&#263;"
  ]
  node [
    id 392
    label "oszukiwa&#263;"
  ]
  node [
    id 393
    label "organizowa&#263;"
  ]
  node [
    id 394
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 395
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 396
    label "work"
  ]
  node [
    id 397
    label "przerabia&#263;"
  ]
  node [
    id 398
    label "stylizowa&#263;"
  ]
  node [
    id 399
    label "falowa&#263;"
  ]
  node [
    id 400
    label "act"
  ]
  node [
    id 401
    label "peddle"
  ]
  node [
    id 402
    label "ukazywa&#263;"
  ]
  node [
    id 403
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 404
    label "praca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 34
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 46
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 52
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 53
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 189
  ]
  edge [
    source 26
    target 190
  ]
  edge [
    source 26
    target 191
  ]
  edge [
    source 26
    target 192
  ]
  edge [
    source 26
    target 193
  ]
  edge [
    source 26
    target 194
  ]
  edge [
    source 26
    target 195
  ]
  edge [
    source 26
    target 196
  ]
  edge [
    source 26
    target 197
  ]
  edge [
    source 26
    target 198
  ]
  edge [
    source 26
    target 199
  ]
  edge [
    source 26
    target 200
  ]
  edge [
    source 26
    target 201
  ]
  edge [
    source 26
    target 202
  ]
  edge [
    source 26
    target 203
  ]
  edge [
    source 26
    target 204
  ]
  edge [
    source 26
    target 205
  ]
  edge [
    source 26
    target 206
  ]
  edge [
    source 26
    target 207
  ]
  edge [
    source 26
    target 208
  ]
  edge [
    source 26
    target 209
  ]
  edge [
    source 26
    target 210
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 213
  ]
  edge [
    source 26
    target 214
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 215
  ]
  edge [
    source 28
    target 216
  ]
  edge [
    source 28
    target 217
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 218
  ]
  edge [
    source 29
    target 183
  ]
  edge [
    source 29
    target 219
  ]
  edge [
    source 29
    target 220
  ]
  edge [
    source 29
    target 221
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 222
  ]
  edge [
    source 30
    target 223
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 30
    target 227
  ]
  edge [
    source 30
    target 228
  ]
  edge [
    source 30
    target 229
  ]
  edge [
    source 30
    target 230
  ]
  edge [
    source 30
    target 231
  ]
  edge [
    source 30
    target 232
  ]
  edge [
    source 30
    target 233
  ]
  edge [
    source 30
    target 234
  ]
  edge [
    source 30
    target 200
  ]
  edge [
    source 30
    target 235
  ]
  edge [
    source 30
    target 236
  ]
  edge [
    source 30
    target 237
  ]
  edge [
    source 30
    target 238
  ]
  edge [
    source 30
    target 239
  ]
  edge [
    source 30
    target 240
  ]
  edge [
    source 30
    target 241
  ]
  edge [
    source 30
    target 242
  ]
  edge [
    source 30
    target 243
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 30
    target 245
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 247
  ]
  edge [
    source 30
    target 248
  ]
  edge [
    source 30
    target 249
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 256
  ]
  edge [
    source 31
    target 257
  ]
  edge [
    source 32
    target 40
  ]
  edge [
    source 32
    target 41
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 258
  ]
  edge [
    source 33
    target 259
  ]
  edge [
    source 33
    target 260
  ]
  edge [
    source 33
    target 261
  ]
  edge [
    source 33
    target 262
  ]
  edge [
    source 33
    target 153
  ]
  edge [
    source 33
    target 43
  ]
  edge [
    source 33
    target 263
  ]
  edge [
    source 33
    target 264
  ]
  edge [
    source 33
    target 265
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 43
  ]
  edge [
    source 35
    target 266
  ]
  edge [
    source 35
    target 267
  ]
  edge [
    source 35
    target 268
  ]
  edge [
    source 35
    target 269
  ]
  edge [
    source 35
    target 270
  ]
  edge [
    source 35
    target 271
  ]
  edge [
    source 35
    target 272
  ]
  edge [
    source 35
    target 273
  ]
  edge [
    source 35
    target 274
  ]
  edge [
    source 35
    target 275
  ]
  edge [
    source 35
    target 276
  ]
  edge [
    source 35
    target 277
  ]
  edge [
    source 35
    target 278
  ]
  edge [
    source 35
    target 279
  ]
  edge [
    source 35
    target 280
  ]
  edge [
    source 35
    target 281
  ]
  edge [
    source 35
    target 282
  ]
  edge [
    source 35
    target 283
  ]
  edge [
    source 35
    target 284
  ]
  edge [
    source 35
    target 285
  ]
  edge [
    source 35
    target 41
  ]
  edge [
    source 36
    target 286
  ]
  edge [
    source 36
    target 287
  ]
  edge [
    source 36
    target 288
  ]
  edge [
    source 36
    target 289
  ]
  edge [
    source 36
    target 290
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 291
  ]
  edge [
    source 37
    target 292
  ]
  edge [
    source 37
    target 293
  ]
  edge [
    source 37
    target 294
  ]
  edge [
    source 37
    target 295
  ]
  edge [
    source 37
    target 296
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 37
    target 298
  ]
  edge [
    source 37
    target 299
  ]
  edge [
    source 37
    target 300
  ]
  edge [
    source 37
    target 301
  ]
  edge [
    source 37
    target 302
  ]
  edge [
    source 37
    target 303
  ]
  edge [
    source 37
    target 304
  ]
  edge [
    source 37
    target 305
  ]
  edge [
    source 37
    target 306
  ]
  edge [
    source 37
    target 307
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 309
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 37
    target 311
  ]
  edge [
    source 37
    target 312
  ]
  edge [
    source 37
    target 313
  ]
  edge [
    source 37
    target 314
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 37
    target 317
  ]
  edge [
    source 37
    target 318
  ]
  edge [
    source 37
    target 319
  ]
  edge [
    source 37
    target 320
  ]
  edge [
    source 37
    target 217
  ]
  edge [
    source 37
    target 321
  ]
  edge [
    source 37
    target 322
  ]
  edge [
    source 37
    target 323
  ]
  edge [
    source 37
    target 324
  ]
  edge [
    source 37
    target 325
  ]
  edge [
    source 37
    target 326
  ]
  edge [
    source 37
    target 327
  ]
  edge [
    source 37
    target 328
  ]
  edge [
    source 37
    target 329
  ]
  edge [
    source 37
    target 330
  ]
  edge [
    source 37
    target 331
  ]
  edge [
    source 37
    target 332
  ]
  edge [
    source 37
    target 333
  ]
  edge [
    source 37
    target 334
  ]
  edge [
    source 37
    target 335
  ]
  edge [
    source 37
    target 336
  ]
  edge [
    source 37
    target 337
  ]
  edge [
    source 37
    target 338
  ]
  edge [
    source 37
    target 339
  ]
  edge [
    source 37
    target 340
  ]
  edge [
    source 37
    target 341
  ]
  edge [
    source 37
    target 342
  ]
  edge [
    source 37
    target 343
  ]
  edge [
    source 37
    target 344
  ]
  edge [
    source 37
    target 345
  ]
  edge [
    source 37
    target 346
  ]
  edge [
    source 37
    target 347
  ]
  edge [
    source 37
    target 348
  ]
  edge [
    source 37
    target 349
  ]
  edge [
    source 37
    target 350
  ]
  edge [
    source 37
    target 351
  ]
  edge [
    source 37
    target 352
  ]
  edge [
    source 37
    target 353
  ]
  edge [
    source 37
    target 354
  ]
  edge [
    source 37
    target 355
  ]
  edge [
    source 37
    target 356
  ]
  edge [
    source 37
    target 357
  ]
  edge [
    source 37
    target 358
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 44
  ]
  edge [
    source 38
    target 359
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 360
  ]
  edge [
    source 39
    target 361
  ]
  edge [
    source 39
    target 362
  ]
  edge [
    source 39
    target 363
  ]
  edge [
    source 39
    target 364
  ]
  edge [
    source 39
    target 365
  ]
  edge [
    source 39
    target 366
  ]
  edge [
    source 39
    target 295
  ]
  edge [
    source 39
    target 367
  ]
  edge [
    source 39
    target 368
  ]
  edge [
    source 39
    target 369
  ]
  edge [
    source 39
    target 370
  ]
  edge [
    source 39
    target 371
  ]
  edge [
    source 39
    target 372
  ]
  edge [
    source 39
    target 373
  ]
  edge [
    source 39
    target 374
  ]
  edge [
    source 39
    target 375
  ]
  edge [
    source 40
    target 376
  ]
  edge [
    source 40
    target 377
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 378
  ]
  edge [
    source 41
    target 379
  ]
  edge [
    source 41
    target 56
  ]
  edge [
    source 41
    target 380
  ]
  edge [
    source 41
    target 381
  ]
  edge [
    source 42
    target 382
  ]
  edge [
    source 42
    target 383
  ]
  edge [
    source 43
    target 384
  ]
  edge [
    source 43
    target 385
  ]
  edge [
    source 43
    target 386
  ]
  edge [
    source 43
    target 387
  ]
  edge [
    source 43
    target 388
  ]
  edge [
    source 43
    target 389
  ]
  edge [
    source 43
    target 390
  ]
  edge [
    source 43
    target 391
  ]
  edge [
    source 43
    target 392
  ]
  edge [
    source 43
    target 393
  ]
  edge [
    source 43
    target 394
  ]
  edge [
    source 43
    target 395
  ]
  edge [
    source 43
    target 396
  ]
  edge [
    source 43
    target 397
  ]
  edge [
    source 43
    target 398
  ]
  edge [
    source 43
    target 399
  ]
  edge [
    source 43
    target 400
  ]
  edge [
    source 43
    target 401
  ]
  edge [
    source 43
    target 402
  ]
  edge [
    source 43
    target 403
  ]
  edge [
    source 43
    target 404
  ]
]
