graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.0549019607843135
  density 0.008090165199938243
  graphCliqueNumber 3
  node [
    id 0
    label "mecz"
    origin "text"
  ]
  node [
    id 1
    label "reprezentacja"
    origin "text"
  ]
  node [
    id 2
    label "pi&#322;karski"
    origin "text"
  ]
  node [
    id 3
    label "polska"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "tekst"
    origin "text"
  ]
  node [
    id 6
    label "popkulturowym"
    origin "text"
  ]
  node [
    id 7
    label "pozwala&#263;"
    origin "text"
  ]
  node [
    id 8
    label "konstruowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "polak"
    origin "text"
  ]
  node [
    id 10
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 11
    label "to&#380;samo&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "narodowy"
    origin "text"
  ]
  node [
    id 13
    label "podtrzymywa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "umacnia&#263;"
    origin "text"
  ]
  node [
    id 15
    label "jak"
    origin "text"
  ]
  node [
    id 16
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 17
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 18
    label "wydarzenie"
    origin "text"
  ]
  node [
    id 19
    label "podkre&#347;li&#263;"
    origin "text"
  ]
  node [
    id 20
    label "warto"
    origin "text"
  ]
  node [
    id 21
    label "lepsze"
    origin "text"
  ]
  node [
    id 22
    label "wynik"
    origin "text"
  ]
  node [
    id 23
    label "nasz"
    origin "text"
  ]
  node [
    id 24
    label "pi&#322;ka"
    origin "text"
  ]
  node [
    id 25
    label "kopa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "tym"
    origin "text"
  ]
  node [
    id 27
    label "silnie"
    origin "text"
  ]
  node [
    id 28
    label "spektakl"
    origin "text"
  ]
  node [
    id 29
    label "identyfikowa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "inny"
    origin "text"
  ]
  node [
    id 31
    label "cz&#322;onek"
    origin "text"
  ]
  node [
    id 32
    label "wsp&#243;lnota"
    origin "text"
  ]
  node [
    id 33
    label "wyobrazi&#263;"
    origin "text"
  ]
  node [
    id 34
    label "obrona"
  ]
  node [
    id 35
    label "gra"
  ]
  node [
    id 36
    label "dwumecz"
  ]
  node [
    id 37
    label "game"
  ]
  node [
    id 38
    label "serw"
  ]
  node [
    id 39
    label "dru&#380;yna"
  ]
  node [
    id 40
    label "zesp&#243;&#322;"
  ]
  node [
    id 41
    label "emblemat"
  ]
  node [
    id 42
    label "deputation"
  ]
  node [
    id 43
    label "specjalny"
  ]
  node [
    id 44
    label "sportowy"
  ]
  node [
    id 45
    label "po_pi&#322;karsku"
  ]
  node [
    id 46
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 47
    label "typowy"
  ]
  node [
    id 48
    label "pi&#322;karsko"
  ]
  node [
    id 49
    label "si&#281;ga&#263;"
  ]
  node [
    id 50
    label "trwa&#263;"
  ]
  node [
    id 51
    label "obecno&#347;&#263;"
  ]
  node [
    id 52
    label "stan"
  ]
  node [
    id 53
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 54
    label "stand"
  ]
  node [
    id 55
    label "mie&#263;_miejsce"
  ]
  node [
    id 56
    label "uczestniczy&#263;"
  ]
  node [
    id 57
    label "chodzi&#263;"
  ]
  node [
    id 58
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 59
    label "equal"
  ]
  node [
    id 60
    label "pisa&#263;"
  ]
  node [
    id 61
    label "odmianka"
  ]
  node [
    id 62
    label "opu&#347;ci&#263;"
  ]
  node [
    id 63
    label "wypowied&#378;"
  ]
  node [
    id 64
    label "wytw&#243;r"
  ]
  node [
    id 65
    label "koniektura"
  ]
  node [
    id 66
    label "preparacja"
  ]
  node [
    id 67
    label "ekscerpcja"
  ]
  node [
    id 68
    label "redakcja"
  ]
  node [
    id 69
    label "obelga"
  ]
  node [
    id 70
    label "dzie&#322;o"
  ]
  node [
    id 71
    label "j&#281;zykowo"
  ]
  node [
    id 72
    label "pomini&#281;cie"
  ]
  node [
    id 73
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 74
    label "authorize"
  ]
  node [
    id 75
    label "uznawa&#263;"
  ]
  node [
    id 76
    label "consent"
  ]
  node [
    id 77
    label "train"
  ]
  node [
    id 78
    label "tworzy&#263;"
  ]
  node [
    id 79
    label "polski"
  ]
  node [
    id 80
    label "cz&#322;owiek"
  ]
  node [
    id 81
    label "bli&#378;ni"
  ]
  node [
    id 82
    label "odpowiedni"
  ]
  node [
    id 83
    label "swojak"
  ]
  node [
    id 84
    label "samodzielny"
  ]
  node [
    id 85
    label "identity"
  ]
  node [
    id 86
    label "pesel"
  ]
  node [
    id 87
    label "uniformizm"
  ]
  node [
    id 88
    label "imi&#281;"
  ]
  node [
    id 89
    label "depersonalizacja"
  ]
  node [
    id 90
    label "to&#380;samo&#347;&#263;_osobista"
  ]
  node [
    id 91
    label "dane"
  ]
  node [
    id 92
    label "adres"
  ]
  node [
    id 93
    label "nazwisko"
  ]
  node [
    id 94
    label "self-consciousness"
  ]
  node [
    id 95
    label "poj&#281;cie"
  ]
  node [
    id 96
    label "r&#243;wno&#347;&#263;"
  ]
  node [
    id 97
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 98
    label "NN"
  ]
  node [
    id 99
    label "nacjonalistyczny"
  ]
  node [
    id 100
    label "narodowo"
  ]
  node [
    id 101
    label "wa&#380;ny"
  ]
  node [
    id 102
    label "back"
  ]
  node [
    id 103
    label "patronize"
  ]
  node [
    id 104
    label "pociesza&#263;"
  ]
  node [
    id 105
    label "utrzymywa&#263;"
  ]
  node [
    id 106
    label "corroborate"
  ]
  node [
    id 107
    label "sprawowa&#263;"
  ]
  node [
    id 108
    label "reinforce"
  ]
  node [
    id 109
    label "zmienia&#263;"
  ]
  node [
    id 110
    label "wzmacnia&#263;"
  ]
  node [
    id 111
    label "utrwala&#263;"
  ]
  node [
    id 112
    label "confirm"
  ]
  node [
    id 113
    label "zabezpiecza&#263;"
  ]
  node [
    id 114
    label "umocnienie"
  ]
  node [
    id 115
    label "stabilizowa&#263;"
  ]
  node [
    id 116
    label "podnosi&#263;"
  ]
  node [
    id 117
    label "byd&#322;o"
  ]
  node [
    id 118
    label "zobo"
  ]
  node [
    id 119
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 120
    label "yakalo"
  ]
  node [
    id 121
    label "dzo"
  ]
  node [
    id 122
    label "mo&#380;liwie"
  ]
  node [
    id 123
    label "nieznaczny"
  ]
  node [
    id 124
    label "kr&#243;tko"
  ]
  node [
    id 125
    label "nieistotnie"
  ]
  node [
    id 126
    label "nieliczny"
  ]
  node [
    id 127
    label "mikroskopijnie"
  ]
  node [
    id 128
    label "pomiernie"
  ]
  node [
    id 129
    label "ma&#322;y"
  ]
  node [
    id 130
    label "czynno&#347;&#263;"
  ]
  node [
    id 131
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 132
    label "motyw"
  ]
  node [
    id 133
    label "fabu&#322;a"
  ]
  node [
    id 134
    label "przebiec"
  ]
  node [
    id 135
    label "przebiegni&#281;cie"
  ]
  node [
    id 136
    label "charakter"
  ]
  node [
    id 137
    label "kreska"
  ]
  node [
    id 138
    label "narysowa&#263;"
  ]
  node [
    id 139
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 140
    label "try"
  ]
  node [
    id 141
    label "przysparza&#263;"
  ]
  node [
    id 142
    label "give"
  ]
  node [
    id 143
    label "kali&#263;_si&#281;"
  ]
  node [
    id 144
    label "bonanza"
  ]
  node [
    id 145
    label "typ"
  ]
  node [
    id 146
    label "dzia&#322;anie"
  ]
  node [
    id 147
    label "przyczyna"
  ]
  node [
    id 148
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 149
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 150
    label "zaokr&#261;glenie"
  ]
  node [
    id 151
    label "event"
  ]
  node [
    id 152
    label "rezultat"
  ]
  node [
    id 153
    label "czyj&#347;"
  ]
  node [
    id 154
    label "zagrywka"
  ]
  node [
    id 155
    label "serwowanie"
  ]
  node [
    id 156
    label "sport"
  ]
  node [
    id 157
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 158
    label "sport_zespo&#322;owy"
  ]
  node [
    id 159
    label "odbicie"
  ]
  node [
    id 160
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 161
    label "rzucanka"
  ]
  node [
    id 162
    label "aut"
  ]
  node [
    id 163
    label "orb"
  ]
  node [
    id 164
    label "do&#347;rodkowywanie"
  ]
  node [
    id 165
    label "zaserwowanie"
  ]
  node [
    id 166
    label "zaserwowa&#263;"
  ]
  node [
    id 167
    label "kula"
  ]
  node [
    id 168
    label "&#347;wieca"
  ]
  node [
    id 169
    label "serwowa&#263;"
  ]
  node [
    id 170
    label "musket_ball"
  ]
  node [
    id 171
    label "spulchnia&#263;"
  ]
  node [
    id 172
    label "wykopywa&#263;"
  ]
  node [
    id 173
    label "chow"
  ]
  node [
    id 174
    label "pora&#380;a&#263;"
  ]
  node [
    id 175
    label "hack"
  ]
  node [
    id 176
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 177
    label "robi&#263;"
  ]
  node [
    id 178
    label "d&#243;&#322;"
  ]
  node [
    id 179
    label "krytykowa&#263;"
  ]
  node [
    id 180
    label "uderza&#263;"
  ]
  node [
    id 181
    label "dig"
  ]
  node [
    id 182
    label "przeszukiwa&#263;"
  ]
  node [
    id 183
    label "zdecydowanie"
  ]
  node [
    id 184
    label "zajebi&#347;cie"
  ]
  node [
    id 185
    label "dusznie"
  ]
  node [
    id 186
    label "silny"
  ]
  node [
    id 187
    label "niepodwa&#380;alnie"
  ]
  node [
    id 188
    label "konkretnie"
  ]
  node [
    id 189
    label "przekonuj&#261;co"
  ]
  node [
    id 190
    label "strongly"
  ]
  node [
    id 191
    label "intensywnie"
  ]
  node [
    id 192
    label "mocny"
  ]
  node [
    id 193
    label "powerfully"
  ]
  node [
    id 194
    label "theatrical_performance"
  ]
  node [
    id 195
    label "przedstawienie"
  ]
  node [
    id 196
    label "scenografia"
  ]
  node [
    id 197
    label "ods&#322;ona"
  ]
  node [
    id 198
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 199
    label "pr&#243;bowanie"
  ]
  node [
    id 200
    label "scena"
  ]
  node [
    id 201
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 202
    label "przedstawi&#263;"
  ]
  node [
    id 203
    label "przedstawianie"
  ]
  node [
    id 204
    label "rola"
  ]
  node [
    id 205
    label "pokaz"
  ]
  node [
    id 206
    label "przedstawia&#263;"
  ]
  node [
    id 207
    label "realizacja"
  ]
  node [
    id 208
    label "equate"
  ]
  node [
    id 209
    label "rozpoznawa&#263;"
  ]
  node [
    id 210
    label "ujednolica&#263;"
  ]
  node [
    id 211
    label "kolejny"
  ]
  node [
    id 212
    label "inaczej"
  ]
  node [
    id 213
    label "r&#243;&#380;ny"
  ]
  node [
    id 214
    label "inszy"
  ]
  node [
    id 215
    label "osobno"
  ]
  node [
    id 216
    label "cia&#322;o"
  ]
  node [
    id 217
    label "organizacja"
  ]
  node [
    id 218
    label "przedstawiciel"
  ]
  node [
    id 219
    label "shaft"
  ]
  node [
    id 220
    label "podmiot"
  ]
  node [
    id 221
    label "fiut"
  ]
  node [
    id 222
    label "przyrodzenie"
  ]
  node [
    id 223
    label "wchodzenie"
  ]
  node [
    id 224
    label "grupa"
  ]
  node [
    id 225
    label "ptaszek"
  ]
  node [
    id 226
    label "organ"
  ]
  node [
    id 227
    label "wej&#347;cie"
  ]
  node [
    id 228
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 229
    label "element_anatomiczny"
  ]
  node [
    id 230
    label "Skandynawia"
  ]
  node [
    id 231
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 232
    label "partnership"
  ]
  node [
    id 233
    label "zwi&#261;zek"
  ]
  node [
    id 234
    label "zwi&#261;za&#263;"
  ]
  node [
    id 235
    label "Walencja"
  ]
  node [
    id 236
    label "society"
  ]
  node [
    id 237
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 238
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 239
    label "bratnia_dusza"
  ]
  node [
    id 240
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 241
    label "marriage"
  ]
  node [
    id 242
    label "zwi&#261;zanie"
  ]
  node [
    id 243
    label "Ba&#322;kany"
  ]
  node [
    id 244
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 245
    label "wi&#261;zanie"
  ]
  node [
    id 246
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 247
    label "podobie&#324;stwo"
  ]
  node [
    id 248
    label "Grzegorz"
  ]
  node [
    id 249
    label "Rasiak"
  ]
  node [
    id 250
    label "van"
  ]
  node [
    id 251
    label "dera"
  ]
  node [
    id 252
    label "drwal"
  ]
  node [
    id 253
    label "drewniany"
  ]
  node [
    id 254
    label "noga"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 46
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 137
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 147
  ]
  edge [
    source 22
    target 148
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 22
    target 150
  ]
  edge [
    source 22
    target 151
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 153
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 154
  ]
  edge [
    source 24
    target 155
  ]
  edge [
    source 24
    target 156
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 158
  ]
  edge [
    source 24
    target 159
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 24
    target 161
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 162
  ]
  edge [
    source 24
    target 163
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 24
    target 167
  ]
  edge [
    source 24
    target 168
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 170
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 25
    target 172
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 25
    target 181
  ]
  edge [
    source 25
    target 182
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 183
  ]
  edge [
    source 27
    target 184
  ]
  edge [
    source 27
    target 185
  ]
  edge [
    source 27
    target 186
  ]
  edge [
    source 27
    target 187
  ]
  edge [
    source 27
    target 188
  ]
  edge [
    source 27
    target 189
  ]
  edge [
    source 27
    target 190
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 27
    target 192
  ]
  edge [
    source 27
    target 193
  ]
  edge [
    source 28
    target 194
  ]
  edge [
    source 28
    target 195
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 197
  ]
  edge [
    source 28
    target 198
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 200
  ]
  edge [
    source 28
    target 201
  ]
  edge [
    source 28
    target 202
  ]
  edge [
    source 28
    target 203
  ]
  edge [
    source 28
    target 204
  ]
  edge [
    source 28
    target 205
  ]
  edge [
    source 28
    target 206
  ]
  edge [
    source 28
    target 207
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 208
  ]
  edge [
    source 29
    target 209
  ]
  edge [
    source 29
    target 210
  ]
  edge [
    source 29
    target 59
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 211
  ]
  edge [
    source 30
    target 212
  ]
  edge [
    source 30
    target 213
  ]
  edge [
    source 30
    target 214
  ]
  edge [
    source 30
    target 215
  ]
  edge [
    source 31
    target 80
  ]
  edge [
    source 31
    target 216
  ]
  edge [
    source 31
    target 217
  ]
  edge [
    source 31
    target 218
  ]
  edge [
    source 31
    target 219
  ]
  edge [
    source 31
    target 220
  ]
  edge [
    source 31
    target 221
  ]
  edge [
    source 31
    target 222
  ]
  edge [
    source 31
    target 223
  ]
  edge [
    source 31
    target 224
  ]
  edge [
    source 31
    target 225
  ]
  edge [
    source 31
    target 226
  ]
  edge [
    source 31
    target 227
  ]
  edge [
    source 31
    target 228
  ]
  edge [
    source 31
    target 229
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 230
  ]
  edge [
    source 32
    target 231
  ]
  edge [
    source 32
    target 232
  ]
  edge [
    source 32
    target 233
  ]
  edge [
    source 32
    target 234
  ]
  edge [
    source 32
    target 235
  ]
  edge [
    source 32
    target 236
  ]
  edge [
    source 32
    target 237
  ]
  edge [
    source 32
    target 238
  ]
  edge [
    source 32
    target 239
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 32
    target 241
  ]
  edge [
    source 32
    target 242
  ]
  edge [
    source 32
    target 243
  ]
  edge [
    source 32
    target 244
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 246
  ]
  edge [
    source 32
    target 247
  ]
  edge [
    source 248
    target 249
  ]
  edge [
    source 250
    target 251
  ]
  edge [
    source 250
    target 252
  ]
  edge [
    source 251
    target 252
  ]
  edge [
    source 253
    target 254
  ]
]
