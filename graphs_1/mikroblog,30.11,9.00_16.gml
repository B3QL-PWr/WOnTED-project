graph [
  maxDegree 13
  minDegree 1
  meanDegree 2
  density 0.08
  graphCliqueNumber 3
  node [
    id 0
    label "ogunie"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "juz"
    origin "text"
  ]
  node [
    id 3
    label "zielonka"
    origin "text"
  ]
  node [
    id 4
    label "wiec"
    origin "text"
  ]
  node [
    id 5
    label "moge"
    origin "text"
  ]
  node [
    id 6
    label "robic"
    origin "text"
  ]
  node [
    id 7
    label "pokazmorde"
    origin "text"
  ]
  node [
    id 8
    label "legitnie"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;ga&#263;"
  ]
  node [
    id 10
    label "trwa&#263;"
  ]
  node [
    id 11
    label "obecno&#347;&#263;"
  ]
  node [
    id 12
    label "stan"
  ]
  node [
    id 13
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 14
    label "stand"
  ]
  node [
    id 15
    label "mie&#263;_miejsce"
  ]
  node [
    id 16
    label "uczestniczy&#263;"
  ]
  node [
    id 17
    label "chodzi&#263;"
  ]
  node [
    id 18
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 19
    label "equal"
  ]
  node [
    id 20
    label "chru&#347;ciele"
  ]
  node [
    id 21
    label "g&#261;ska_zielonka"
  ]
  node [
    id 22
    label "ptak_w&#281;drowny"
  ]
  node [
    id 23
    label "karma"
  ]
  node [
    id 24
    label "ptak_wodny"
  ]
  node [
    id 25
    label "mityng"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
]
