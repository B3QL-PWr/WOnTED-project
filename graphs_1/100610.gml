graph [
  maxDegree 14
  minDegree 1
  meanDegree 2.0681818181818183
  density 0.023772204806687566
  graphCliqueNumber 5
  node [
    id 0
    label "kilka"
    origin "text"
  ]
  node [
    id 1
    label "dni"
    origin "text"
  ]
  node [
    id 2
    label "mama"
    origin "text"
  ]
  node [
    id 3
    label "logo"
    origin "text"
  ]
  node [
    id 4
    label "projekt"
    origin "text"
  ]
  node [
    id 5
    label "przygotowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przez"
    origin "text"
  ]
  node [
    id 7
    label "kama"
    origin "text"
  ]
  node [
    id 8
    label "jackowska"
    origin "text"
  ]
  node [
    id 9
    label "studio"
    origin "text"
  ]
  node [
    id 10
    label "&#347;ledziowate"
  ]
  node [
    id 11
    label "ryba"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "matczysko"
  ]
  node [
    id 14
    label "macierz"
  ]
  node [
    id 15
    label "przodkini"
  ]
  node [
    id 16
    label "Matka_Boska"
  ]
  node [
    id 17
    label "macocha"
  ]
  node [
    id 18
    label "matka_zast&#281;pcza"
  ]
  node [
    id 19
    label "stara"
  ]
  node [
    id 20
    label "rodzice"
  ]
  node [
    id 21
    label "rodzic"
  ]
  node [
    id 22
    label "naszywka"
  ]
  node [
    id 23
    label "symbol"
  ]
  node [
    id 24
    label "dokument"
  ]
  node [
    id 25
    label "device"
  ]
  node [
    id 26
    label "program_u&#380;ytkowy"
  ]
  node [
    id 27
    label "intencja"
  ]
  node [
    id 28
    label "agreement"
  ]
  node [
    id 29
    label "pomys&#322;"
  ]
  node [
    id 30
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 31
    label "plan"
  ]
  node [
    id 32
    label "dokumentacja"
  ]
  node [
    id 33
    label "arrange"
  ]
  node [
    id 34
    label "spowodowa&#263;"
  ]
  node [
    id 35
    label "dress"
  ]
  node [
    id 36
    label "wyszkoli&#263;"
  ]
  node [
    id 37
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 38
    label "wytworzy&#263;"
  ]
  node [
    id 39
    label "ukierunkowa&#263;"
  ]
  node [
    id 40
    label "train"
  ]
  node [
    id 41
    label "wykona&#263;"
  ]
  node [
    id 42
    label "zrobi&#263;"
  ]
  node [
    id 43
    label "cook"
  ]
  node [
    id 44
    label "set"
  ]
  node [
    id 45
    label "bro&#324;_sieczna"
  ]
  node [
    id 46
    label "antylopa"
  ]
  node [
    id 47
    label "bro&#324;"
  ]
  node [
    id 48
    label "telewizja"
  ]
  node [
    id 49
    label "pomieszczenie"
  ]
  node [
    id 50
    label "radio"
  ]
  node [
    id 51
    label "Jackowska"
  ]
  node [
    id 52
    label "otworzy&#263;"
  ]
  node [
    id 53
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 54
    label "instytut"
  ]
  node [
    id 55
    label "kultura"
  ]
  node [
    id 56
    label "polski"
  ]
  node [
    id 57
    label "etnologia"
  ]
  node [
    id 58
    label "i"
  ]
  node [
    id 59
    label "antropolog"
  ]
  node [
    id 60
    label "kulturowy"
  ]
  node [
    id 61
    label "wydzia&#322;"
  ]
  node [
    id 62
    label "psychologia"
  ]
  node [
    id 63
    label "politechnika"
  ]
  node [
    id 64
    label "warszawski"
  ]
  node [
    id 65
    label "Zofia"
  ]
  node [
    id 66
    label "Dworakowska"
  ]
  node [
    id 67
    label "Micha&#322;"
  ]
  node [
    id 68
    label "Piotr"
  ]
  node [
    id 69
    label "Pr&#281;gowski"
  ]
  node [
    id 70
    label "Marta"
  ]
  node [
    id 71
    label "zimniak"
  ]
  node [
    id 72
    label "Ha&#322;ajko"
  ]
  node [
    id 73
    label "pawe&#322;"
  ]
  node [
    id 74
    label "Krzyworzeka"
  ]
  node [
    id 75
    label "Jan"
  ]
  node [
    id 76
    label "zaj&#261;c"
  ]
  node [
    id 77
    label "Creative"
  ]
  node [
    id 78
    label "Commons"
  ]
  node [
    id 79
    label "polskie"
  ]
  node [
    id 80
    label "antropologia"
  ]
  node [
    id 81
    label "biblioteka"
  ]
  node [
    id 82
    label "wirtualny"
  ]
  node [
    id 83
    label "nauka"
  ]
  node [
    id 84
    label "interdyscyplinarny"
  ]
  node [
    id 85
    label "centrum"
  ]
  node [
    id 86
    label "modelowa&#263;"
  ]
  node [
    id 87
    label "uniwersytet"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 54
    target 59
  ]
  edge [
    source 54
    target 60
  ]
  edge [
    source 54
    target 79
  ]
  edge [
    source 54
    target 80
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 77
  ]
  edge [
    source 56
    target 78
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 64
    target 87
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 69
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 83
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 86
  ]
  edge [
    source 85
    target 86
  ]
]
