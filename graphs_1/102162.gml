graph [
  maxDegree 595
  minDegree 1
  meanDegree 2.2965367965367967
  density 0.0024881222064320656
  graphCliqueNumber 11
  node [
    id 0
    label "uchwa&#322;a"
    origin "text"
  ]
  node [
    id 1
    label "rad"
    origin "text"
  ]
  node [
    id 2
    label "miejski"
    origin "text"
  ]
  node [
    id 3
    label "radom"
    origin "text"
  ]
  node [
    id 4
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "sprawa"
    origin "text"
  ]
  node [
    id 7
    label "finansowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "przez"
    origin "text"
  ]
  node [
    id 9
    label "gmina"
    origin "text"
  ]
  node [
    id 10
    label "miasto"
    origin "text"
  ]
  node [
    id 11
    label "prawo"
    origin "text"
  ]
  node [
    id 12
    label "powiat"
    origin "text"
  ]
  node [
    id 13
    label "program"
    origin "text"
  ]
  node [
    id 14
    label "rewitalizacja"
    origin "text"
  ]
  node [
    id 15
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 16
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 17
    label "nasz"
    origin "text"
  ]
  node [
    id 18
    label "przysz&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "rama"
    origin "text"
  ]
  node [
    id 20
    label "priorytet"
    origin "text"
  ]
  node [
    id 21
    label "zatrudnienie"
    origin "text"
  ]
  node [
    id 22
    label "systemowy"
    origin "text"
  ]
  node [
    id 23
    label "instytucja"
    origin "text"
  ]
  node [
    id 24
    label "operacyjny"
    origin "text"
  ]
  node [
    id 25
    label "kapita&#322;"
    origin "text"
  ]
  node [
    id 26
    label "resolution"
  ]
  node [
    id 27
    label "akt"
  ]
  node [
    id 28
    label "berylowiec"
  ]
  node [
    id 29
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 30
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 31
    label "mikroradian"
  ]
  node [
    id 32
    label "zadowolenie_si&#281;"
  ]
  node [
    id 33
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 34
    label "content"
  ]
  node [
    id 35
    label "jednostka_promieniowania"
  ]
  node [
    id 36
    label "miliradian"
  ]
  node [
    id 37
    label "jednostka"
  ]
  node [
    id 38
    label "miejsko"
  ]
  node [
    id 39
    label "miastowy"
  ]
  node [
    id 40
    label "typowy"
  ]
  node [
    id 41
    label "publiczny"
  ]
  node [
    id 42
    label "s&#322;o&#324;ce"
  ]
  node [
    id 43
    label "czynienie_si&#281;"
  ]
  node [
    id 44
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 45
    label "czas"
  ]
  node [
    id 46
    label "long_time"
  ]
  node [
    id 47
    label "przedpo&#322;udnie"
  ]
  node [
    id 48
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 49
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 50
    label "tydzie&#324;"
  ]
  node [
    id 51
    label "godzina"
  ]
  node [
    id 52
    label "t&#322;usty_czwartek"
  ]
  node [
    id 53
    label "wsta&#263;"
  ]
  node [
    id 54
    label "day"
  ]
  node [
    id 55
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 56
    label "przedwiecz&#243;r"
  ]
  node [
    id 57
    label "Sylwester"
  ]
  node [
    id 58
    label "po&#322;udnie"
  ]
  node [
    id 59
    label "wzej&#347;cie"
  ]
  node [
    id 60
    label "podwiecz&#243;r"
  ]
  node [
    id 61
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 62
    label "rano"
  ]
  node [
    id 63
    label "termin"
  ]
  node [
    id 64
    label "ranek"
  ]
  node [
    id 65
    label "doba"
  ]
  node [
    id 66
    label "wiecz&#243;r"
  ]
  node [
    id 67
    label "walentynki"
  ]
  node [
    id 68
    label "popo&#322;udnie"
  ]
  node [
    id 69
    label "noc"
  ]
  node [
    id 70
    label "wstanie"
  ]
  node [
    id 71
    label "stulecie"
  ]
  node [
    id 72
    label "kalendarz"
  ]
  node [
    id 73
    label "pora_roku"
  ]
  node [
    id 74
    label "cykl_astronomiczny"
  ]
  node [
    id 75
    label "p&#243;&#322;rocze"
  ]
  node [
    id 76
    label "grupa"
  ]
  node [
    id 77
    label "kwarta&#322;"
  ]
  node [
    id 78
    label "kurs"
  ]
  node [
    id 79
    label "jubileusz"
  ]
  node [
    id 80
    label "miesi&#261;c"
  ]
  node [
    id 81
    label "lata"
  ]
  node [
    id 82
    label "martwy_sezon"
  ]
  node [
    id 83
    label "temat"
  ]
  node [
    id 84
    label "kognicja"
  ]
  node [
    id 85
    label "idea"
  ]
  node [
    id 86
    label "szczeg&#243;&#322;"
  ]
  node [
    id 87
    label "rzecz"
  ]
  node [
    id 88
    label "wydarzenie"
  ]
  node [
    id 89
    label "przes&#322;anka"
  ]
  node [
    id 90
    label "rozprawa"
  ]
  node [
    id 91
    label "object"
  ]
  node [
    id 92
    label "proposition"
  ]
  node [
    id 93
    label "finance"
  ]
  node [
    id 94
    label "p&#322;aci&#263;"
  ]
  node [
    id 95
    label "rada_gminy"
  ]
  node [
    id 96
    label "Wielka_Wie&#347;"
  ]
  node [
    id 97
    label "jednostka_administracyjna"
  ]
  node [
    id 98
    label "Dobro&#324;"
  ]
  node [
    id 99
    label "Karlsbad"
  ]
  node [
    id 100
    label "urz&#261;d"
  ]
  node [
    id 101
    label "Biskupice"
  ]
  node [
    id 102
    label "radny"
  ]
  node [
    id 103
    label "organizacja_religijna"
  ]
  node [
    id 104
    label "Brac&#322;aw"
  ]
  node [
    id 105
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 106
    label "G&#322;uch&#243;w"
  ]
  node [
    id 107
    label "Hallstatt"
  ]
  node [
    id 108
    label "Zbara&#380;"
  ]
  node [
    id 109
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 110
    label "Nachiczewan"
  ]
  node [
    id 111
    label "Suworow"
  ]
  node [
    id 112
    label "Halicz"
  ]
  node [
    id 113
    label "Gandawa"
  ]
  node [
    id 114
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 115
    label "Wismar"
  ]
  node [
    id 116
    label "Norymberga"
  ]
  node [
    id 117
    label "Ruciane-Nida"
  ]
  node [
    id 118
    label "Wia&#378;ma"
  ]
  node [
    id 119
    label "Sewilla"
  ]
  node [
    id 120
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 121
    label "Kobry&#324;"
  ]
  node [
    id 122
    label "Brno"
  ]
  node [
    id 123
    label "Tomsk"
  ]
  node [
    id 124
    label "Poniatowa"
  ]
  node [
    id 125
    label "Hadziacz"
  ]
  node [
    id 126
    label "Tiume&#324;"
  ]
  node [
    id 127
    label "Drohobycz"
  ]
  node [
    id 128
    label "Lyon"
  ]
  node [
    id 129
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 130
    label "K&#322;odawa"
  ]
  node [
    id 131
    label "Solikamsk"
  ]
  node [
    id 132
    label "Wolgast"
  ]
  node [
    id 133
    label "Saloniki"
  ]
  node [
    id 134
    label "Lw&#243;w"
  ]
  node [
    id 135
    label "Al-Kufa"
  ]
  node [
    id 136
    label "Hamburg"
  ]
  node [
    id 137
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 138
    label "Nampula"
  ]
  node [
    id 139
    label "burmistrz"
  ]
  node [
    id 140
    label "D&#252;sseldorf"
  ]
  node [
    id 141
    label "Nowy_Orlean"
  ]
  node [
    id 142
    label "Bamberg"
  ]
  node [
    id 143
    label "Osaka"
  ]
  node [
    id 144
    label "Michalovce"
  ]
  node [
    id 145
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 146
    label "Fryburg"
  ]
  node [
    id 147
    label "Trabzon"
  ]
  node [
    id 148
    label "Wersal"
  ]
  node [
    id 149
    label "Swatowe"
  ]
  node [
    id 150
    label "Ka&#322;uga"
  ]
  node [
    id 151
    label "Dijon"
  ]
  node [
    id 152
    label "Cannes"
  ]
  node [
    id 153
    label "Borowsk"
  ]
  node [
    id 154
    label "Kursk"
  ]
  node [
    id 155
    label "Tyberiada"
  ]
  node [
    id 156
    label "Boden"
  ]
  node [
    id 157
    label "Dodona"
  ]
  node [
    id 158
    label "Vukovar"
  ]
  node [
    id 159
    label "Soleczniki"
  ]
  node [
    id 160
    label "Barcelona"
  ]
  node [
    id 161
    label "Oszmiana"
  ]
  node [
    id 162
    label "Stuttgart"
  ]
  node [
    id 163
    label "Nerczy&#324;sk"
  ]
  node [
    id 164
    label "Essen"
  ]
  node [
    id 165
    label "Bijsk"
  ]
  node [
    id 166
    label "Luboml"
  ]
  node [
    id 167
    label "Gr&#243;dek"
  ]
  node [
    id 168
    label "Orany"
  ]
  node [
    id 169
    label "Siedliszcze"
  ]
  node [
    id 170
    label "P&#322;owdiw"
  ]
  node [
    id 171
    label "A&#322;apajewsk"
  ]
  node [
    id 172
    label "Liverpool"
  ]
  node [
    id 173
    label "Ostrawa"
  ]
  node [
    id 174
    label "Penza"
  ]
  node [
    id 175
    label "Rudki"
  ]
  node [
    id 176
    label "Aktobe"
  ]
  node [
    id 177
    label "I&#322;awka"
  ]
  node [
    id 178
    label "Tolkmicko"
  ]
  node [
    id 179
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 180
    label "Sajgon"
  ]
  node [
    id 181
    label "Windawa"
  ]
  node [
    id 182
    label "Weimar"
  ]
  node [
    id 183
    label "Jekaterynburg"
  ]
  node [
    id 184
    label "Lejda"
  ]
  node [
    id 185
    label "Cremona"
  ]
  node [
    id 186
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 187
    label "Kordoba"
  ]
  node [
    id 188
    label "&#321;ohojsk"
  ]
  node [
    id 189
    label "Kalmar"
  ]
  node [
    id 190
    label "Akerman"
  ]
  node [
    id 191
    label "Locarno"
  ]
  node [
    id 192
    label "Bych&#243;w"
  ]
  node [
    id 193
    label "Toledo"
  ]
  node [
    id 194
    label "Minusi&#324;sk"
  ]
  node [
    id 195
    label "Szk&#322;&#243;w"
  ]
  node [
    id 196
    label "Wenecja"
  ]
  node [
    id 197
    label "Bazylea"
  ]
  node [
    id 198
    label "Peszt"
  ]
  node [
    id 199
    label "Piza"
  ]
  node [
    id 200
    label "Tanger"
  ]
  node [
    id 201
    label "Krzywi&#324;"
  ]
  node [
    id 202
    label "Eger"
  ]
  node [
    id 203
    label "Bogus&#322;aw"
  ]
  node [
    id 204
    label "Taganrog"
  ]
  node [
    id 205
    label "Oksford"
  ]
  node [
    id 206
    label "Gwardiejsk"
  ]
  node [
    id 207
    label "Tyraspol"
  ]
  node [
    id 208
    label "Kleczew"
  ]
  node [
    id 209
    label "Nowa_D&#281;ba"
  ]
  node [
    id 210
    label "Wilejka"
  ]
  node [
    id 211
    label "Modena"
  ]
  node [
    id 212
    label "Demmin"
  ]
  node [
    id 213
    label "Houston"
  ]
  node [
    id 214
    label "Rydu&#322;towy"
  ]
  node [
    id 215
    label "Bordeaux"
  ]
  node [
    id 216
    label "Schmalkalden"
  ]
  node [
    id 217
    label "O&#322;omuniec"
  ]
  node [
    id 218
    label "Tuluza"
  ]
  node [
    id 219
    label "tramwaj"
  ]
  node [
    id 220
    label "Nantes"
  ]
  node [
    id 221
    label "Debreczyn"
  ]
  node [
    id 222
    label "Kowel"
  ]
  node [
    id 223
    label "Witnica"
  ]
  node [
    id 224
    label "Stalingrad"
  ]
  node [
    id 225
    label "Drezno"
  ]
  node [
    id 226
    label "Perejas&#322;aw"
  ]
  node [
    id 227
    label "Luksor"
  ]
  node [
    id 228
    label "Ostaszk&#243;w"
  ]
  node [
    id 229
    label "Gettysburg"
  ]
  node [
    id 230
    label "Trydent"
  ]
  node [
    id 231
    label "Poczdam"
  ]
  node [
    id 232
    label "Mesyna"
  ]
  node [
    id 233
    label "Krasnogorsk"
  ]
  node [
    id 234
    label "Kars"
  ]
  node [
    id 235
    label "Darmstadt"
  ]
  node [
    id 236
    label "Rzg&#243;w"
  ]
  node [
    id 237
    label "Kar&#322;owice"
  ]
  node [
    id 238
    label "Czeskie_Budziejowice"
  ]
  node [
    id 239
    label "Buda"
  ]
  node [
    id 240
    label "Monako"
  ]
  node [
    id 241
    label "Pardubice"
  ]
  node [
    id 242
    label "Pas&#322;&#281;k"
  ]
  node [
    id 243
    label "Fatima"
  ]
  node [
    id 244
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 245
    label "Bir&#380;e"
  ]
  node [
    id 246
    label "Wi&#322;komierz"
  ]
  node [
    id 247
    label "Opawa"
  ]
  node [
    id 248
    label "Mantua"
  ]
  node [
    id 249
    label "ulica"
  ]
  node [
    id 250
    label "Tarragona"
  ]
  node [
    id 251
    label "Antwerpia"
  ]
  node [
    id 252
    label "Asuan"
  ]
  node [
    id 253
    label "Korynt"
  ]
  node [
    id 254
    label "Armenia"
  ]
  node [
    id 255
    label "Budionnowsk"
  ]
  node [
    id 256
    label "Lengyel"
  ]
  node [
    id 257
    label "Betlejem"
  ]
  node [
    id 258
    label "Asy&#380;"
  ]
  node [
    id 259
    label "Batumi"
  ]
  node [
    id 260
    label "Paczk&#243;w"
  ]
  node [
    id 261
    label "Grenada"
  ]
  node [
    id 262
    label "Suczawa"
  ]
  node [
    id 263
    label "Nowogard"
  ]
  node [
    id 264
    label "Tyr"
  ]
  node [
    id 265
    label "Bria&#324;sk"
  ]
  node [
    id 266
    label "Bar"
  ]
  node [
    id 267
    label "Czerkiesk"
  ]
  node [
    id 268
    label "Ja&#322;ta"
  ]
  node [
    id 269
    label "Mo&#347;ciska"
  ]
  node [
    id 270
    label "Medyna"
  ]
  node [
    id 271
    label "Tartu"
  ]
  node [
    id 272
    label "Pemba"
  ]
  node [
    id 273
    label "Lipawa"
  ]
  node [
    id 274
    label "Tyl&#380;a"
  ]
  node [
    id 275
    label "Dayton"
  ]
  node [
    id 276
    label "Lipsk"
  ]
  node [
    id 277
    label "Rohatyn"
  ]
  node [
    id 278
    label "Peszawar"
  ]
  node [
    id 279
    label "Adrianopol"
  ]
  node [
    id 280
    label "Azow"
  ]
  node [
    id 281
    label "Iwano-Frankowsk"
  ]
  node [
    id 282
    label "Czarnobyl"
  ]
  node [
    id 283
    label "Rakoniewice"
  ]
  node [
    id 284
    label "Obuch&#243;w"
  ]
  node [
    id 285
    label "Orneta"
  ]
  node [
    id 286
    label "Koszyce"
  ]
  node [
    id 287
    label "Czeski_Cieszyn"
  ]
  node [
    id 288
    label "Zagorsk"
  ]
  node [
    id 289
    label "Nieder_Selters"
  ]
  node [
    id 290
    label "Ko&#322;omna"
  ]
  node [
    id 291
    label "Rost&#243;w"
  ]
  node [
    id 292
    label "Bolonia"
  ]
  node [
    id 293
    label "Rajgr&#243;d"
  ]
  node [
    id 294
    label "L&#252;neburg"
  ]
  node [
    id 295
    label "Brack"
  ]
  node [
    id 296
    label "Konstancja"
  ]
  node [
    id 297
    label "Koluszki"
  ]
  node [
    id 298
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 299
    label "Suez"
  ]
  node [
    id 300
    label "Mrocza"
  ]
  node [
    id 301
    label "Triest"
  ]
  node [
    id 302
    label "Murma&#324;sk"
  ]
  node [
    id 303
    label "Tu&#322;a"
  ]
  node [
    id 304
    label "Tarnogr&#243;d"
  ]
  node [
    id 305
    label "Radziech&#243;w"
  ]
  node [
    id 306
    label "Kokand"
  ]
  node [
    id 307
    label "Kircholm"
  ]
  node [
    id 308
    label "Nowa_Ruda"
  ]
  node [
    id 309
    label "Huma&#324;"
  ]
  node [
    id 310
    label "Turkiestan"
  ]
  node [
    id 311
    label "Kani&#243;w"
  ]
  node [
    id 312
    label "Pilzno"
  ]
  node [
    id 313
    label "Korfant&#243;w"
  ]
  node [
    id 314
    label "Dubno"
  ]
  node [
    id 315
    label "Bras&#322;aw"
  ]
  node [
    id 316
    label "Choroszcz"
  ]
  node [
    id 317
    label "Nowogr&#243;d"
  ]
  node [
    id 318
    label "Konotop"
  ]
  node [
    id 319
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 320
    label "Jastarnia"
  ]
  node [
    id 321
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 322
    label "Omsk"
  ]
  node [
    id 323
    label "Troick"
  ]
  node [
    id 324
    label "Koper"
  ]
  node [
    id 325
    label "Jenisejsk"
  ]
  node [
    id 326
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 327
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 328
    label "Trenczyn"
  ]
  node [
    id 329
    label "Wormacja"
  ]
  node [
    id 330
    label "Wagram"
  ]
  node [
    id 331
    label "Lubeka"
  ]
  node [
    id 332
    label "Genewa"
  ]
  node [
    id 333
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 334
    label "Kleck"
  ]
  node [
    id 335
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 336
    label "Struga"
  ]
  node [
    id 337
    label "Izbica_Kujawska"
  ]
  node [
    id 338
    label "Stalinogorsk"
  ]
  node [
    id 339
    label "Izmir"
  ]
  node [
    id 340
    label "Dortmund"
  ]
  node [
    id 341
    label "Workuta"
  ]
  node [
    id 342
    label "Jerycho"
  ]
  node [
    id 343
    label "Brunszwik"
  ]
  node [
    id 344
    label "Aleksandria"
  ]
  node [
    id 345
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 346
    label "Borys&#322;aw"
  ]
  node [
    id 347
    label "Zaleszczyki"
  ]
  node [
    id 348
    label "Z&#322;oczew"
  ]
  node [
    id 349
    label "Piast&#243;w"
  ]
  node [
    id 350
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 351
    label "Bor"
  ]
  node [
    id 352
    label "Nazaret"
  ]
  node [
    id 353
    label "Sarat&#243;w"
  ]
  node [
    id 354
    label "Brasz&#243;w"
  ]
  node [
    id 355
    label "Malin"
  ]
  node [
    id 356
    label "Parma"
  ]
  node [
    id 357
    label "Wierchoja&#324;sk"
  ]
  node [
    id 358
    label "Tarent"
  ]
  node [
    id 359
    label "Mariampol"
  ]
  node [
    id 360
    label "Wuhan"
  ]
  node [
    id 361
    label "Split"
  ]
  node [
    id 362
    label "Baranowicze"
  ]
  node [
    id 363
    label "Marki"
  ]
  node [
    id 364
    label "Adana"
  ]
  node [
    id 365
    label "B&#322;aszki"
  ]
  node [
    id 366
    label "Lubecz"
  ]
  node [
    id 367
    label "Sulech&#243;w"
  ]
  node [
    id 368
    label "Borys&#243;w"
  ]
  node [
    id 369
    label "Homel"
  ]
  node [
    id 370
    label "Tours"
  ]
  node [
    id 371
    label "Zaporo&#380;e"
  ]
  node [
    id 372
    label "Edam"
  ]
  node [
    id 373
    label "Kamieniec_Podolski"
  ]
  node [
    id 374
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 375
    label "Konstantynopol"
  ]
  node [
    id 376
    label "Chocim"
  ]
  node [
    id 377
    label "Mohylew"
  ]
  node [
    id 378
    label "Merseburg"
  ]
  node [
    id 379
    label "Kapsztad"
  ]
  node [
    id 380
    label "Sambor"
  ]
  node [
    id 381
    label "Manchester"
  ]
  node [
    id 382
    label "Pi&#324;sk"
  ]
  node [
    id 383
    label "Ochryda"
  ]
  node [
    id 384
    label "Rybi&#324;sk"
  ]
  node [
    id 385
    label "Czadca"
  ]
  node [
    id 386
    label "Orenburg"
  ]
  node [
    id 387
    label "Krajowa"
  ]
  node [
    id 388
    label "Eleusis"
  ]
  node [
    id 389
    label "Awinion"
  ]
  node [
    id 390
    label "Rzeczyca"
  ]
  node [
    id 391
    label "Lozanna"
  ]
  node [
    id 392
    label "Barczewo"
  ]
  node [
    id 393
    label "&#379;migr&#243;d"
  ]
  node [
    id 394
    label "Chabarowsk"
  ]
  node [
    id 395
    label "Jena"
  ]
  node [
    id 396
    label "Xai-Xai"
  ]
  node [
    id 397
    label "Radk&#243;w"
  ]
  node [
    id 398
    label "Syrakuzy"
  ]
  node [
    id 399
    label "Zas&#322;aw"
  ]
  node [
    id 400
    label "Windsor"
  ]
  node [
    id 401
    label "Getynga"
  ]
  node [
    id 402
    label "Carrara"
  ]
  node [
    id 403
    label "Madras"
  ]
  node [
    id 404
    label "Nitra"
  ]
  node [
    id 405
    label "Kilonia"
  ]
  node [
    id 406
    label "Rawenna"
  ]
  node [
    id 407
    label "Stawropol"
  ]
  node [
    id 408
    label "Warna"
  ]
  node [
    id 409
    label "Ba&#322;tijsk"
  ]
  node [
    id 410
    label "Cumana"
  ]
  node [
    id 411
    label "Kostroma"
  ]
  node [
    id 412
    label "Bajonna"
  ]
  node [
    id 413
    label "Magadan"
  ]
  node [
    id 414
    label "Kercz"
  ]
  node [
    id 415
    label "Harbin"
  ]
  node [
    id 416
    label "Sankt_Florian"
  ]
  node [
    id 417
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 418
    label "Wo&#322;kowysk"
  ]
  node [
    id 419
    label "Norak"
  ]
  node [
    id 420
    label "S&#232;vres"
  ]
  node [
    id 421
    label "Barwice"
  ]
  node [
    id 422
    label "Sumy"
  ]
  node [
    id 423
    label "Jutrosin"
  ]
  node [
    id 424
    label "Canterbury"
  ]
  node [
    id 425
    label "Czerkasy"
  ]
  node [
    id 426
    label "Troki"
  ]
  node [
    id 427
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 428
    label "Turka"
  ]
  node [
    id 429
    label "Budziszyn"
  ]
  node [
    id 430
    label "A&#322;czewsk"
  ]
  node [
    id 431
    label "Chark&#243;w"
  ]
  node [
    id 432
    label "Go&#347;cino"
  ]
  node [
    id 433
    label "Ku&#378;nieck"
  ]
  node [
    id 434
    label "Wotki&#324;sk"
  ]
  node [
    id 435
    label "Symferopol"
  ]
  node [
    id 436
    label "Dmitrow"
  ]
  node [
    id 437
    label "Cherso&#324;"
  ]
  node [
    id 438
    label "zabudowa"
  ]
  node [
    id 439
    label "Orlean"
  ]
  node [
    id 440
    label "Nowogr&#243;dek"
  ]
  node [
    id 441
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 442
    label "Berdia&#324;sk"
  ]
  node [
    id 443
    label "Szumsk"
  ]
  node [
    id 444
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 445
    label "Orsza"
  ]
  node [
    id 446
    label "Cluny"
  ]
  node [
    id 447
    label "Aralsk"
  ]
  node [
    id 448
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 449
    label "Bogumin"
  ]
  node [
    id 450
    label "Antiochia"
  ]
  node [
    id 451
    label "Inhambane"
  ]
  node [
    id 452
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 453
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 454
    label "Trewir"
  ]
  node [
    id 455
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 456
    label "Siewieromorsk"
  ]
  node [
    id 457
    label "Calais"
  ]
  node [
    id 458
    label "Twer"
  ]
  node [
    id 459
    label "&#379;ytawa"
  ]
  node [
    id 460
    label "Eupatoria"
  ]
  node [
    id 461
    label "Stara_Zagora"
  ]
  node [
    id 462
    label "Jastrowie"
  ]
  node [
    id 463
    label "Piatigorsk"
  ]
  node [
    id 464
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 465
    label "Le&#324;sk"
  ]
  node [
    id 466
    label "Johannesburg"
  ]
  node [
    id 467
    label "Kaszyn"
  ]
  node [
    id 468
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 469
    label "&#379;ylina"
  ]
  node [
    id 470
    label "Sewastopol"
  ]
  node [
    id 471
    label "Pietrozawodsk"
  ]
  node [
    id 472
    label "Bobolice"
  ]
  node [
    id 473
    label "Mosty"
  ]
  node [
    id 474
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 475
    label "Karaganda"
  ]
  node [
    id 476
    label "Marsylia"
  ]
  node [
    id 477
    label "Buchara"
  ]
  node [
    id 478
    label "Dubrownik"
  ]
  node [
    id 479
    label "Be&#322;z"
  ]
  node [
    id 480
    label "Oran"
  ]
  node [
    id 481
    label "Regensburg"
  ]
  node [
    id 482
    label "Rotterdam"
  ]
  node [
    id 483
    label "Trembowla"
  ]
  node [
    id 484
    label "Woskriesiensk"
  ]
  node [
    id 485
    label "Po&#322;ock"
  ]
  node [
    id 486
    label "Poprad"
  ]
  node [
    id 487
    label "Kronsztad"
  ]
  node [
    id 488
    label "Los_Angeles"
  ]
  node [
    id 489
    label "U&#322;an_Ude"
  ]
  node [
    id 490
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 491
    label "W&#322;adywostok"
  ]
  node [
    id 492
    label "Kandahar"
  ]
  node [
    id 493
    label "Tobolsk"
  ]
  node [
    id 494
    label "Boston"
  ]
  node [
    id 495
    label "Hawana"
  ]
  node [
    id 496
    label "Kis&#322;owodzk"
  ]
  node [
    id 497
    label "Tulon"
  ]
  node [
    id 498
    label "Utrecht"
  ]
  node [
    id 499
    label "Oleszyce"
  ]
  node [
    id 500
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 501
    label "Katania"
  ]
  node [
    id 502
    label "Teby"
  ]
  node [
    id 503
    label "Paw&#322;owo"
  ]
  node [
    id 504
    label "W&#252;rzburg"
  ]
  node [
    id 505
    label "Podiebrady"
  ]
  node [
    id 506
    label "Uppsala"
  ]
  node [
    id 507
    label "Poniewie&#380;"
  ]
  node [
    id 508
    label "Niko&#322;ajewsk"
  ]
  node [
    id 509
    label "Aczy&#324;sk"
  ]
  node [
    id 510
    label "Berezyna"
  ]
  node [
    id 511
    label "Ostr&#243;g"
  ]
  node [
    id 512
    label "Brze&#347;&#263;"
  ]
  node [
    id 513
    label "Lancaster"
  ]
  node [
    id 514
    label "Stryj"
  ]
  node [
    id 515
    label "Kozielsk"
  ]
  node [
    id 516
    label "Loreto"
  ]
  node [
    id 517
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 518
    label "Hebron"
  ]
  node [
    id 519
    label "Kaspijsk"
  ]
  node [
    id 520
    label "Peczora"
  ]
  node [
    id 521
    label "Isfahan"
  ]
  node [
    id 522
    label "Chimoio"
  ]
  node [
    id 523
    label "Mory&#324;"
  ]
  node [
    id 524
    label "Kowno"
  ]
  node [
    id 525
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 526
    label "Opalenica"
  ]
  node [
    id 527
    label "Kolonia"
  ]
  node [
    id 528
    label "Stary_Sambor"
  ]
  node [
    id 529
    label "Kolkata"
  ]
  node [
    id 530
    label "Turkmenbaszy"
  ]
  node [
    id 531
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 532
    label "Nankin"
  ]
  node [
    id 533
    label "Krzanowice"
  ]
  node [
    id 534
    label "Efez"
  ]
  node [
    id 535
    label "Dobrodzie&#324;"
  ]
  node [
    id 536
    label "Neapol"
  ]
  node [
    id 537
    label "S&#322;uck"
  ]
  node [
    id 538
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 539
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 540
    label "Frydek-Mistek"
  ]
  node [
    id 541
    label "Korsze"
  ]
  node [
    id 542
    label "T&#322;uszcz"
  ]
  node [
    id 543
    label "Soligorsk"
  ]
  node [
    id 544
    label "Kie&#380;mark"
  ]
  node [
    id 545
    label "Mannheim"
  ]
  node [
    id 546
    label "Ulm"
  ]
  node [
    id 547
    label "Podhajce"
  ]
  node [
    id 548
    label "Dniepropetrowsk"
  ]
  node [
    id 549
    label "Szamocin"
  ]
  node [
    id 550
    label "Ko&#322;omyja"
  ]
  node [
    id 551
    label "Buczacz"
  ]
  node [
    id 552
    label "M&#252;nster"
  ]
  node [
    id 553
    label "Brema"
  ]
  node [
    id 554
    label "Delhi"
  ]
  node [
    id 555
    label "&#346;niatyn"
  ]
  node [
    id 556
    label "Nicea"
  ]
  node [
    id 557
    label "Szawle"
  ]
  node [
    id 558
    label "Czerniowce"
  ]
  node [
    id 559
    label "Mi&#347;nia"
  ]
  node [
    id 560
    label "Sydney"
  ]
  node [
    id 561
    label "Moguncja"
  ]
  node [
    id 562
    label "Narbona"
  ]
  node [
    id 563
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 564
    label "Wittenberga"
  ]
  node [
    id 565
    label "Uljanowsk"
  ]
  node [
    id 566
    label "&#321;uga&#324;sk"
  ]
  node [
    id 567
    label "Wyborg"
  ]
  node [
    id 568
    label "Trojan"
  ]
  node [
    id 569
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 570
    label "Brandenburg"
  ]
  node [
    id 571
    label "Kemerowo"
  ]
  node [
    id 572
    label "Kaszgar"
  ]
  node [
    id 573
    label "Lenzen"
  ]
  node [
    id 574
    label "Nanning"
  ]
  node [
    id 575
    label "Gotha"
  ]
  node [
    id 576
    label "Zurych"
  ]
  node [
    id 577
    label "Baltimore"
  ]
  node [
    id 578
    label "&#321;uck"
  ]
  node [
    id 579
    label "Bristol"
  ]
  node [
    id 580
    label "Ferrara"
  ]
  node [
    id 581
    label "Mariupol"
  ]
  node [
    id 582
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 583
    label "Lhasa"
  ]
  node [
    id 584
    label "Czerniejewo"
  ]
  node [
    id 585
    label "Filadelfia"
  ]
  node [
    id 586
    label "Kanton"
  ]
  node [
    id 587
    label "Milan&#243;wek"
  ]
  node [
    id 588
    label "Perwomajsk"
  ]
  node [
    id 589
    label "Nieftiegorsk"
  ]
  node [
    id 590
    label "Pittsburgh"
  ]
  node [
    id 591
    label "Greifswald"
  ]
  node [
    id 592
    label "Akwileja"
  ]
  node [
    id 593
    label "Norfolk"
  ]
  node [
    id 594
    label "Perm"
  ]
  node [
    id 595
    label "Detroit"
  ]
  node [
    id 596
    label "Fergana"
  ]
  node [
    id 597
    label "Starobielsk"
  ]
  node [
    id 598
    label "Wielsk"
  ]
  node [
    id 599
    label "Zaklik&#243;w"
  ]
  node [
    id 600
    label "Majsur"
  ]
  node [
    id 601
    label "Narwa"
  ]
  node [
    id 602
    label "Chicago"
  ]
  node [
    id 603
    label "Byczyna"
  ]
  node [
    id 604
    label "Mozyrz"
  ]
  node [
    id 605
    label "Konstantyn&#243;wka"
  ]
  node [
    id 606
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 607
    label "Megara"
  ]
  node [
    id 608
    label "Stralsund"
  ]
  node [
    id 609
    label "Wo&#322;gograd"
  ]
  node [
    id 610
    label "Lichinga"
  ]
  node [
    id 611
    label "Haga"
  ]
  node [
    id 612
    label "Tarnopol"
  ]
  node [
    id 613
    label "K&#322;ajpeda"
  ]
  node [
    id 614
    label "Nowomoskowsk"
  ]
  node [
    id 615
    label "Ussuryjsk"
  ]
  node [
    id 616
    label "Brugia"
  ]
  node [
    id 617
    label "Natal"
  ]
  node [
    id 618
    label "Kro&#347;niewice"
  ]
  node [
    id 619
    label "Edynburg"
  ]
  node [
    id 620
    label "Marburg"
  ]
  node [
    id 621
    label "&#346;wiebodzice"
  ]
  node [
    id 622
    label "S&#322;onim"
  ]
  node [
    id 623
    label "Dalton"
  ]
  node [
    id 624
    label "Smorgonie"
  ]
  node [
    id 625
    label "Orze&#322;"
  ]
  node [
    id 626
    label "Nowoku&#378;nieck"
  ]
  node [
    id 627
    label "Zadar"
  ]
  node [
    id 628
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 629
    label "Koprzywnica"
  ]
  node [
    id 630
    label "Angarsk"
  ]
  node [
    id 631
    label "Mo&#380;ajsk"
  ]
  node [
    id 632
    label "Akwizgran"
  ]
  node [
    id 633
    label "Norylsk"
  ]
  node [
    id 634
    label "Jawor&#243;w"
  ]
  node [
    id 635
    label "weduta"
  ]
  node [
    id 636
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 637
    label "Suzdal"
  ]
  node [
    id 638
    label "W&#322;odzimierz"
  ]
  node [
    id 639
    label "Bujnaksk"
  ]
  node [
    id 640
    label "Beresteczko"
  ]
  node [
    id 641
    label "Strzelno"
  ]
  node [
    id 642
    label "Siewsk"
  ]
  node [
    id 643
    label "Cymlansk"
  ]
  node [
    id 644
    label "Trzyniec"
  ]
  node [
    id 645
    label "Hanower"
  ]
  node [
    id 646
    label "Wuppertal"
  ]
  node [
    id 647
    label "Sura&#380;"
  ]
  node [
    id 648
    label "Winchester"
  ]
  node [
    id 649
    label "Samara"
  ]
  node [
    id 650
    label "Sydon"
  ]
  node [
    id 651
    label "Krasnodar"
  ]
  node [
    id 652
    label "Worone&#380;"
  ]
  node [
    id 653
    label "Paw&#322;odar"
  ]
  node [
    id 654
    label "Czelabi&#324;sk"
  ]
  node [
    id 655
    label "Reda"
  ]
  node [
    id 656
    label "Karwina"
  ]
  node [
    id 657
    label "Wyszehrad"
  ]
  node [
    id 658
    label "Sara&#324;sk"
  ]
  node [
    id 659
    label "Koby&#322;ka"
  ]
  node [
    id 660
    label "Winnica"
  ]
  node [
    id 661
    label "Tambow"
  ]
  node [
    id 662
    label "Pyskowice"
  ]
  node [
    id 663
    label "Heidelberg"
  ]
  node [
    id 664
    label "Maribor"
  ]
  node [
    id 665
    label "Werona"
  ]
  node [
    id 666
    label "G&#322;uszyca"
  ]
  node [
    id 667
    label "Rostock"
  ]
  node [
    id 668
    label "Mekka"
  ]
  node [
    id 669
    label "Liberec"
  ]
  node [
    id 670
    label "Bie&#322;gorod"
  ]
  node [
    id 671
    label "Berdycz&#243;w"
  ]
  node [
    id 672
    label "Sierdobsk"
  ]
  node [
    id 673
    label "Bobrujsk"
  ]
  node [
    id 674
    label "Padwa"
  ]
  node [
    id 675
    label "Pasawa"
  ]
  node [
    id 676
    label "Chanty-Mansyjsk"
  ]
  node [
    id 677
    label "&#379;ar&#243;w"
  ]
  node [
    id 678
    label "Poczaj&#243;w"
  ]
  node [
    id 679
    label "Barabi&#324;sk"
  ]
  node [
    id 680
    label "Gorycja"
  ]
  node [
    id 681
    label "Haarlem"
  ]
  node [
    id 682
    label "Kiejdany"
  ]
  node [
    id 683
    label "Chmielnicki"
  ]
  node [
    id 684
    label "Magnitogorsk"
  ]
  node [
    id 685
    label "Burgas"
  ]
  node [
    id 686
    label "Siena"
  ]
  node [
    id 687
    label "Korzec"
  ]
  node [
    id 688
    label "Bonn"
  ]
  node [
    id 689
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 690
    label "Walencja"
  ]
  node [
    id 691
    label "Mosina"
  ]
  node [
    id 692
    label "obserwacja"
  ]
  node [
    id 693
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 694
    label "nauka_prawa"
  ]
  node [
    id 695
    label "dominion"
  ]
  node [
    id 696
    label "normatywizm"
  ]
  node [
    id 697
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 698
    label "qualification"
  ]
  node [
    id 699
    label "opis"
  ]
  node [
    id 700
    label "regu&#322;a_Allena"
  ]
  node [
    id 701
    label "normalizacja"
  ]
  node [
    id 702
    label "kazuistyka"
  ]
  node [
    id 703
    label "regu&#322;a_Glogera"
  ]
  node [
    id 704
    label "kultura_duchowa"
  ]
  node [
    id 705
    label "prawo_karne"
  ]
  node [
    id 706
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 707
    label "standard"
  ]
  node [
    id 708
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 709
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 710
    label "struktura"
  ]
  node [
    id 711
    label "szko&#322;a"
  ]
  node [
    id 712
    label "prawo_karne_procesowe"
  ]
  node [
    id 713
    label "prawo_Mendla"
  ]
  node [
    id 714
    label "przepis"
  ]
  node [
    id 715
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 716
    label "criterion"
  ]
  node [
    id 717
    label "kanonistyka"
  ]
  node [
    id 718
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 719
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 720
    label "wykonawczy"
  ]
  node [
    id 721
    label "twierdzenie"
  ]
  node [
    id 722
    label "judykatura"
  ]
  node [
    id 723
    label "legislacyjnie"
  ]
  node [
    id 724
    label "umocowa&#263;"
  ]
  node [
    id 725
    label "podmiot"
  ]
  node [
    id 726
    label "procesualistyka"
  ]
  node [
    id 727
    label "kierunek"
  ]
  node [
    id 728
    label "kryminologia"
  ]
  node [
    id 729
    label "kryminalistyka"
  ]
  node [
    id 730
    label "cywilistyka"
  ]
  node [
    id 731
    label "law"
  ]
  node [
    id 732
    label "zasada_d'Alemberta"
  ]
  node [
    id 733
    label "jurisprudence"
  ]
  node [
    id 734
    label "zasada"
  ]
  node [
    id 735
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 736
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 737
    label "wojew&#243;dztwo"
  ]
  node [
    id 738
    label "spis"
  ]
  node [
    id 739
    label "odinstalowanie"
  ]
  node [
    id 740
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 741
    label "za&#322;o&#380;enie"
  ]
  node [
    id 742
    label "podstawa"
  ]
  node [
    id 743
    label "emitowanie"
  ]
  node [
    id 744
    label "odinstalowywanie"
  ]
  node [
    id 745
    label "instrukcja"
  ]
  node [
    id 746
    label "punkt"
  ]
  node [
    id 747
    label "teleferie"
  ]
  node [
    id 748
    label "emitowa&#263;"
  ]
  node [
    id 749
    label "wytw&#243;r"
  ]
  node [
    id 750
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 751
    label "sekcja_krytyczna"
  ]
  node [
    id 752
    label "prezentowa&#263;"
  ]
  node [
    id 753
    label "blok"
  ]
  node [
    id 754
    label "podprogram"
  ]
  node [
    id 755
    label "tryb"
  ]
  node [
    id 756
    label "dzia&#322;"
  ]
  node [
    id 757
    label "broszura"
  ]
  node [
    id 758
    label "deklaracja"
  ]
  node [
    id 759
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 760
    label "struktura_organizacyjna"
  ]
  node [
    id 761
    label "zaprezentowanie"
  ]
  node [
    id 762
    label "informatyka"
  ]
  node [
    id 763
    label "booklet"
  ]
  node [
    id 764
    label "menu"
  ]
  node [
    id 765
    label "oprogramowanie"
  ]
  node [
    id 766
    label "instalowanie"
  ]
  node [
    id 767
    label "furkacja"
  ]
  node [
    id 768
    label "odinstalowa&#263;"
  ]
  node [
    id 769
    label "instalowa&#263;"
  ]
  node [
    id 770
    label "okno"
  ]
  node [
    id 771
    label "pirat"
  ]
  node [
    id 772
    label "zainstalowanie"
  ]
  node [
    id 773
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 774
    label "ogranicznik_referencyjny"
  ]
  node [
    id 775
    label "zainstalowa&#263;"
  ]
  node [
    id 776
    label "kana&#322;"
  ]
  node [
    id 777
    label "zaprezentowa&#263;"
  ]
  node [
    id 778
    label "interfejs"
  ]
  node [
    id 779
    label "odinstalowywa&#263;"
  ]
  node [
    id 780
    label "folder"
  ]
  node [
    id 781
    label "course_of_study"
  ]
  node [
    id 782
    label "ram&#243;wka"
  ]
  node [
    id 783
    label "prezentowanie"
  ]
  node [
    id 784
    label "oferta"
  ]
  node [
    id 785
    label "odnowa"
  ]
  node [
    id 786
    label "odbudowa"
  ]
  node [
    id 787
    label "niepubliczny"
  ]
  node [
    id 788
    label "spo&#322;ecznie"
  ]
  node [
    id 789
    label "asymilowa&#263;"
  ]
  node [
    id 790
    label "wapniak"
  ]
  node [
    id 791
    label "dwun&#243;g"
  ]
  node [
    id 792
    label "polifag"
  ]
  node [
    id 793
    label "wz&#243;r"
  ]
  node [
    id 794
    label "profanum"
  ]
  node [
    id 795
    label "hominid"
  ]
  node [
    id 796
    label "homo_sapiens"
  ]
  node [
    id 797
    label "nasada"
  ]
  node [
    id 798
    label "podw&#322;adny"
  ]
  node [
    id 799
    label "ludzko&#347;&#263;"
  ]
  node [
    id 800
    label "os&#322;abianie"
  ]
  node [
    id 801
    label "mikrokosmos"
  ]
  node [
    id 802
    label "portrecista"
  ]
  node [
    id 803
    label "duch"
  ]
  node [
    id 804
    label "g&#322;owa"
  ]
  node [
    id 805
    label "oddzia&#322;ywanie"
  ]
  node [
    id 806
    label "asymilowanie"
  ]
  node [
    id 807
    label "osoba"
  ]
  node [
    id 808
    label "os&#322;abia&#263;"
  ]
  node [
    id 809
    label "figura"
  ]
  node [
    id 810
    label "Adam"
  ]
  node [
    id 811
    label "senior"
  ]
  node [
    id 812
    label "antropochoria"
  ]
  node [
    id 813
    label "posta&#263;"
  ]
  node [
    id 814
    label "czyj&#347;"
  ]
  node [
    id 815
    label "jutro"
  ]
  node [
    id 816
    label "cel"
  ]
  node [
    id 817
    label "zakres"
  ]
  node [
    id 818
    label "dodatek"
  ]
  node [
    id 819
    label "stela&#380;"
  ]
  node [
    id 820
    label "human_body"
  ]
  node [
    id 821
    label "szablon"
  ]
  node [
    id 822
    label "oprawa"
  ]
  node [
    id 823
    label "paczka"
  ]
  node [
    id 824
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 825
    label "obramowanie"
  ]
  node [
    id 826
    label "pojazd"
  ]
  node [
    id 827
    label "postawa"
  ]
  node [
    id 828
    label "element_konstrukcyjny"
  ]
  node [
    id 829
    label "dzia&#322;anie"
  ]
  node [
    id 830
    label "znaczenie"
  ]
  node [
    id 831
    label "przesy&#322;ka"
  ]
  node [
    id 832
    label "us&#322;uga"
  ]
  node [
    id 833
    label "pierwszy_plan"
  ]
  node [
    id 834
    label "wzi&#281;cie"
  ]
  node [
    id 835
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 836
    label "stosunek_pracy"
  ]
  node [
    id 837
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 838
    label "zatrudni&#263;"
  ]
  node [
    id 839
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 840
    label "agregat_ekonomiczny"
  ]
  node [
    id 841
    label "function"
  ]
  node [
    id 842
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 843
    label "pracowanie"
  ]
  node [
    id 844
    label "najem"
  ]
  node [
    id 845
    label "zaw&#243;d"
  ]
  node [
    id 846
    label "kierownictwo"
  ]
  node [
    id 847
    label "pracowa&#263;"
  ]
  node [
    id 848
    label "zobowi&#261;zanie"
  ]
  node [
    id 849
    label "poda&#380;_pracy"
  ]
  node [
    id 850
    label "affair"
  ]
  node [
    id 851
    label "zgodny"
  ]
  node [
    id 852
    label "globalny"
  ]
  node [
    id 853
    label "systemowo"
  ]
  node [
    id 854
    label "zale&#380;ny"
  ]
  node [
    id 855
    label "metodyczny"
  ]
  node [
    id 856
    label "afiliowa&#263;"
  ]
  node [
    id 857
    label "osoba_prawna"
  ]
  node [
    id 858
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 859
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 860
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 861
    label "establishment"
  ]
  node [
    id 862
    label "organizacja"
  ]
  node [
    id 863
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 864
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 865
    label "zamykanie"
  ]
  node [
    id 866
    label "zamyka&#263;"
  ]
  node [
    id 867
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 868
    label "poj&#281;cie"
  ]
  node [
    id 869
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 870
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 871
    label "Fundusze_Unijne"
  ]
  node [
    id 872
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 873
    label "biuro"
  ]
  node [
    id 874
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 875
    label "mo&#380;liwy"
  ]
  node [
    id 876
    label "medyczny"
  ]
  node [
    id 877
    label "operacyjnie"
  ]
  node [
    id 878
    label "uruchomienie"
  ]
  node [
    id 879
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 880
    label "&#347;rodowisko"
  ]
  node [
    id 881
    label "supernadz&#243;r"
  ]
  node [
    id 882
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 883
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 884
    label "absolutorium"
  ]
  node [
    id 885
    label "podupada&#263;"
  ]
  node [
    id 886
    label "nap&#322;ywanie"
  ]
  node [
    id 887
    label "kapitalista"
  ]
  node [
    id 888
    label "podupadanie"
  ]
  node [
    id 889
    label "kwestor"
  ]
  node [
    id 890
    label "uruchamia&#263;"
  ]
  node [
    id 891
    label "mienie"
  ]
  node [
    id 892
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 893
    label "uruchamianie"
  ]
  node [
    id 894
    label "czynnik_produkcji"
  ]
  node [
    id 895
    label "zaleta"
  ]
  node [
    id 896
    label "zas&#243;b"
  ]
  node [
    id 897
    label "rada"
  ]
  node [
    id 898
    label "wyspa"
  ]
  node [
    id 899
    label "Radom"
  ]
  node [
    id 900
    label "p&#243;&#322;nocny"
  ]
  node [
    id 901
    label "&#8222;"
  ]
  node [
    id 902
    label "m&#322;odzi"
  ]
  node [
    id 903
    label "ludzie"
  ]
  node [
    id 904
    label "&#8211;"
  ]
  node [
    id 905
    label "nasza&#263;"
  ]
  node [
    id 906
    label "&#8221;"
  ]
  node [
    id 907
    label "kapita&#322;a"
  ]
  node [
    id 908
    label "ludzki"
  ]
  node [
    id 909
    label "ustawa"
  ]
  node [
    id 910
    label "zeszyt"
  ]
  node [
    id 911
    label "27"
  ]
  node [
    id 912
    label "sierpie&#324;"
  ]
  node [
    id 913
    label "2009"
  ]
  node [
    id 914
    label "ojciec"
  ]
  node [
    id 915
    label "finanse"
  ]
  node [
    id 916
    label "8"
  ]
  node [
    id 917
    label "marzec"
  ]
  node [
    id 918
    label "1990"
  ]
  node [
    id 919
    label "samorz&#261;d"
  ]
  node [
    id 920
    label "gminny"
  ]
  node [
    id 921
    label "dziennik"
  ]
  node [
    id 922
    label "u"
  ]
  node [
    id 923
    label "prezydent"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 897
  ]
  edge [
    source 2
    target 898
  ]
  edge [
    source 2
    target 899
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 909
  ]
  edge [
    source 4
    target 910
  ]
  edge [
    source 4
    target 911
  ]
  edge [
    source 4
    target 912
  ]
  edge [
    source 4
    target 913
  ]
  edge [
    source 4
    target 914
  ]
  edge [
    source 4
    target 915
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 916
  ]
  edge [
    source 4
    target 917
  ]
  edge [
    source 4
    target 918
  ]
  edge [
    source 4
    target 919
  ]
  edge [
    source 4
    target 920
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 909
  ]
  edge [
    source 5
    target 910
  ]
  edge [
    source 5
    target 911
  ]
  edge [
    source 5
    target 912
  ]
  edge [
    source 5
    target 913
  ]
  edge [
    source 5
    target 914
  ]
  edge [
    source 5
    target 915
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 916
  ]
  edge [
    source 5
    target 917
  ]
  edge [
    source 5
    target 918
  ]
  edge [
    source 5
    target 919
  ]
  edge [
    source 5
    target 920
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 923
  ]
  edge [
    source 10
    target 899
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 785
  ]
  edge [
    source 14
    target 786
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 901
  ]
  edge [
    source 14
    target 14
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 14
    target 903
  ]
  edge [
    source 14
    target 904
  ]
  edge [
    source 14
    target 905
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 906
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 41
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 906
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 816
  ]
  edge [
    source 18
    target 45
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 817
  ]
  edge [
    source 19
    target 818
  ]
  edge [
    source 19
    target 710
  ]
  edge [
    source 19
    target 819
  ]
  edge [
    source 19
    target 741
  ]
  edge [
    source 19
    target 820
  ]
  edge [
    source 19
    target 821
  ]
  edge [
    source 19
    target 822
  ]
  edge [
    source 19
    target 823
  ]
  edge [
    source 19
    target 824
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 19
    target 826
  ]
  edge [
    source 19
    target 827
  ]
  edge [
    source 19
    target 828
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 829
  ]
  edge [
    source 20
    target 830
  ]
  edge [
    source 20
    target 831
  ]
  edge [
    source 20
    target 832
  ]
  edge [
    source 20
    target 816
  ]
  edge [
    source 20
    target 833
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 834
  ]
  edge [
    source 21
    target 835
  ]
  edge [
    source 21
    target 836
  ]
  edge [
    source 21
    target 837
  ]
  edge [
    source 21
    target 838
  ]
  edge [
    source 21
    target 839
  ]
  edge [
    source 21
    target 840
  ]
  edge [
    source 21
    target 841
  ]
  edge [
    source 21
    target 842
  ]
  edge [
    source 21
    target 843
  ]
  edge [
    source 21
    target 844
  ]
  edge [
    source 21
    target 845
  ]
  edge [
    source 21
    target 846
  ]
  edge [
    source 21
    target 847
  ]
  edge [
    source 21
    target 848
  ]
  edge [
    source 21
    target 849
  ]
  edge [
    source 21
    target 850
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 851
  ]
  edge [
    source 22
    target 852
  ]
  edge [
    source 22
    target 853
  ]
  edge [
    source 22
    target 854
  ]
  edge [
    source 22
    target 855
  ]
  edge [
    source 23
    target 856
  ]
  edge [
    source 23
    target 857
  ]
  edge [
    source 23
    target 858
  ]
  edge [
    source 23
    target 100
  ]
  edge [
    source 23
    target 859
  ]
  edge [
    source 23
    target 860
  ]
  edge [
    source 23
    target 861
  ]
  edge [
    source 23
    target 707
  ]
  edge [
    source 23
    target 862
  ]
  edge [
    source 23
    target 863
  ]
  edge [
    source 23
    target 864
  ]
  edge [
    source 23
    target 865
  ]
  edge [
    source 23
    target 866
  ]
  edge [
    source 23
    target 867
  ]
  edge [
    source 23
    target 868
  ]
  edge [
    source 23
    target 869
  ]
  edge [
    source 23
    target 870
  ]
  edge [
    source 23
    target 871
  ]
  edge [
    source 23
    target 872
  ]
  edge [
    source 23
    target 873
  ]
  edge [
    source 23
    target 874
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 875
  ]
  edge [
    source 24
    target 876
  ]
  edge [
    source 24
    target 877
  ]
  edge [
    source 24
    target 907
  ]
  edge [
    source 24
    target 908
  ]
  edge [
    source 25
    target 878
  ]
  edge [
    source 25
    target 879
  ]
  edge [
    source 25
    target 880
  ]
  edge [
    source 25
    target 881
  ]
  edge [
    source 25
    target 882
  ]
  edge [
    source 25
    target 883
  ]
  edge [
    source 25
    target 884
  ]
  edge [
    source 25
    target 885
  ]
  edge [
    source 25
    target 886
  ]
  edge [
    source 25
    target 887
  ]
  edge [
    source 25
    target 888
  ]
  edge [
    source 25
    target 889
  ]
  edge [
    source 25
    target 890
  ]
  edge [
    source 25
    target 891
  ]
  edge [
    source 25
    target 892
  ]
  edge [
    source 25
    target 893
  ]
  edge [
    source 25
    target 894
  ]
  edge [
    source 25
    target 895
  ]
  edge [
    source 25
    target 896
  ]
  edge [
    source 41
    target 909
  ]
  edge [
    source 41
    target 910
  ]
  edge [
    source 41
    target 911
  ]
  edge [
    source 41
    target 912
  ]
  edge [
    source 41
    target 913
  ]
  edge [
    source 41
    target 914
  ]
  edge [
    source 41
    target 915
  ]
  edge [
    source 897
    target 898
  ]
  edge [
    source 897
    target 899
  ]
  edge [
    source 898
    target 899
  ]
  edge [
    source 899
    target 923
  ]
  edge [
    source 900
    target 901
  ]
  edge [
    source 900
    target 902
  ]
  edge [
    source 900
    target 903
  ]
  edge [
    source 900
    target 904
  ]
  edge [
    source 900
    target 905
  ]
  edge [
    source 900
    target 906
  ]
  edge [
    source 901
    target 902
  ]
  edge [
    source 901
    target 903
  ]
  edge [
    source 901
    target 904
  ]
  edge [
    source 901
    target 905
  ]
  edge [
    source 901
    target 906
  ]
  edge [
    source 902
    target 903
  ]
  edge [
    source 902
    target 904
  ]
  edge [
    source 902
    target 905
  ]
  edge [
    source 902
    target 906
  ]
  edge [
    source 903
    target 904
  ]
  edge [
    source 903
    target 905
  ]
  edge [
    source 903
    target 906
  ]
  edge [
    source 904
    target 905
  ]
  edge [
    source 904
    target 906
  ]
  edge [
    source 905
    target 906
  ]
  edge [
    source 907
    target 908
  ]
  edge [
    source 909
    target 910
  ]
  edge [
    source 909
    target 911
  ]
  edge [
    source 909
    target 912
  ]
  edge [
    source 909
    target 913
  ]
  edge [
    source 909
    target 914
  ]
  edge [
    source 909
    target 915
  ]
  edge [
    source 909
    target 916
  ]
  edge [
    source 909
    target 917
  ]
  edge [
    source 909
    target 918
  ]
  edge [
    source 909
    target 919
  ]
  edge [
    source 909
    target 920
  ]
  edge [
    source 910
    target 911
  ]
  edge [
    source 910
    target 912
  ]
  edge [
    source 910
    target 913
  ]
  edge [
    source 910
    target 914
  ]
  edge [
    source 910
    target 915
  ]
  edge [
    source 910
    target 916
  ]
  edge [
    source 910
    target 917
  ]
  edge [
    source 910
    target 918
  ]
  edge [
    source 910
    target 919
  ]
  edge [
    source 910
    target 920
  ]
  edge [
    source 911
    target 912
  ]
  edge [
    source 911
    target 913
  ]
  edge [
    source 911
    target 914
  ]
  edge [
    source 911
    target 915
  ]
  edge [
    source 912
    target 913
  ]
  edge [
    source 912
    target 914
  ]
  edge [
    source 912
    target 915
  ]
  edge [
    source 913
    target 914
  ]
  edge [
    source 913
    target 915
  ]
  edge [
    source 914
    target 915
  ]
  edge [
    source 914
    target 916
  ]
  edge [
    source 914
    target 917
  ]
  edge [
    source 914
    target 918
  ]
  edge [
    source 914
    target 919
  ]
  edge [
    source 914
    target 920
  ]
  edge [
    source 916
    target 917
  ]
  edge [
    source 916
    target 918
  ]
  edge [
    source 916
    target 919
  ]
  edge [
    source 916
    target 920
  ]
  edge [
    source 917
    target 918
  ]
  edge [
    source 917
    target 919
  ]
  edge [
    source 917
    target 920
  ]
  edge [
    source 918
    target 919
  ]
  edge [
    source 918
    target 920
  ]
  edge [
    source 919
    target 920
  ]
  edge [
    source 921
    target 922
  ]
]
