graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.411764705882353
  density 0.08823529411764706
  graphCliqueNumber 2
  node [
    id 0
    label "zielonka"
    origin "text"
  ]
  node [
    id 1
    label "wiesia"
    origin "text"
  ]
  node [
    id 2
    label "chru&#347;ciele"
  ]
  node [
    id 3
    label "g&#261;ska_zielonka"
  ]
  node [
    id 4
    label "ptak_w&#281;drowny"
  ]
  node [
    id 5
    label "karma"
  ]
  node [
    id 6
    label "ptak_wodny"
  ]
  node [
    id 7
    label "wie&#347;"
  ]
  node [
    id 8
    label "stary"
  ]
  node [
    id 9
    label "Babice"
  ]
  node [
    id 10
    label "warszawski"
  ]
  node [
    id 11
    label "zachodni"
  ]
  node [
    id 12
    label "parcela"
  ]
  node [
    id 13
    label "&#380;elazowy"
  ]
  node [
    id 14
    label "woli"
  ]
  node [
    id 15
    label "trakt"
  ]
  node [
    id 16
    label "kr&#243;lewski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
]
