graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8857142857142857
  density 0.05546218487394958
  graphCliqueNumber 2
  node [
    id 0
    label "policjant"
    origin "text"
  ]
  node [
    id 1
    label "poszukiwa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "letni"
    origin "text"
  ]
  node [
    id 3
    label "daniel"
    origin "text"
  ]
  node [
    id 4
    label "nadolski"
    origin "text"
  ]
  node [
    id 5
    label "policja"
  ]
  node [
    id 6
    label "blacharz"
  ]
  node [
    id 7
    label "pa&#322;a"
  ]
  node [
    id 8
    label "mundurowy"
  ]
  node [
    id 9
    label "str&#243;&#380;"
  ]
  node [
    id 10
    label "glina"
  ]
  node [
    id 11
    label "ask"
  ]
  node [
    id 12
    label "stara&#263;_si&#281;"
  ]
  node [
    id 13
    label "szuka&#263;"
  ]
  node [
    id 14
    label "look"
  ]
  node [
    id 15
    label "nijaki"
  ]
  node [
    id 16
    label "sezonowy"
  ]
  node [
    id 17
    label "letnio"
  ]
  node [
    id 18
    label "s&#322;oneczny"
  ]
  node [
    id 19
    label "weso&#322;y"
  ]
  node [
    id 20
    label "oboj&#281;tny"
  ]
  node [
    id 21
    label "latowy"
  ]
  node [
    id 22
    label "ciep&#322;y"
  ]
  node [
    id 23
    label "typowy"
  ]
  node [
    id 24
    label "becze&#263;"
  ]
  node [
    id 25
    label "zabecze&#263;"
  ]
  node [
    id 26
    label "&#347;wiece"
  ]
  node [
    id 27
    label "kwiat"
  ]
  node [
    id 28
    label "zwierz&#281;_&#322;owne"
  ]
  node [
    id 29
    label "zwierzyna_p&#322;owa"
  ]
  node [
    id 30
    label "prze&#380;uwacz"
  ]
  node [
    id 31
    label "jeleniowate"
  ]
  node [
    id 32
    label "badyl"
  ]
  node [
    id 33
    label "Daniel"
  ]
  node [
    id 34
    label "Nadolski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 33
    target 34
  ]
]
