graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "polan"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "lubelskie"
    origin "text"
  ]
  node [
    id 3
    label "w&#322;&#243;kno"
  ]
  node [
    id 4
    label "jednostka_administracyjna"
  ]
  node [
    id 5
    label "makroregion"
  ]
  node [
    id 6
    label "powiat"
  ]
  node [
    id 7
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 8
    label "mikroregion"
  ]
  node [
    id 9
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 10
    label "pa&#324;stwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
]
