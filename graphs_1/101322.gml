graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.5
  density 0.07894736842105263
  graphCliqueNumber 2
  node [
    id 0
    label "orze&#322;"
    origin "text"
  ]
  node [
    id 1
    label "murowany"
    origin "text"
  ]
  node [
    id 2
    label "ptak_drapie&#380;ny"
  ]
  node [
    id 3
    label "gapa"
  ]
  node [
    id 4
    label "or&#322;y"
  ]
  node [
    id 5
    label "eagle"
  ]
  node [
    id 6
    label "talent"
  ]
  node [
    id 7
    label "awers"
  ]
  node [
    id 8
    label "bystrzak"
  ]
  node [
    id 9
    label "murowanie"
  ]
  node [
    id 10
    label "pewny"
  ]
  node [
    id 11
    label "murowa&#263;"
  ]
  node [
    id 12
    label "poczta"
  ]
  node [
    id 13
    label "polski"
  ]
  node [
    id 14
    label "powsta&#263;"
  ]
  node [
    id 15
    label "Chmielnicki"
  ]
  node [
    id 16
    label "&#347;wi&#281;ty"
  ]
  node [
    id 17
    label "Jan"
  ]
  node [
    id 18
    label "i"
  ]
  node [
    id 19
    label "W&#346;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
]
