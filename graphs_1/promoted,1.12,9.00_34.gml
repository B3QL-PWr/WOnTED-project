graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.11764705882352941
  graphCliqueNumber 2
  node [
    id 0
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 1
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 2
    label "zewn&#261;trz"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
  ]
  node [
    id 4
    label "czeka&#263;"
  ]
  node [
    id 5
    label "lookout"
  ]
  node [
    id 6
    label "wyziera&#263;"
  ]
  node [
    id 7
    label "peep"
  ]
  node [
    id 8
    label "look"
  ]
  node [
    id 9
    label "patrze&#263;"
  ]
  node [
    id 10
    label "miejsce"
  ]
  node [
    id 11
    label "czas"
  ]
  node [
    id 12
    label "abstrakcja"
  ]
  node [
    id 13
    label "punkt"
  ]
  node [
    id 14
    label "substancja"
  ]
  node [
    id 15
    label "spos&#243;b"
  ]
  node [
    id 16
    label "chemikalia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
]
