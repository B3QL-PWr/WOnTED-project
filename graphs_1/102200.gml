graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.0826446280991737
  density 0.017355371900826446
  graphCliqueNumber 3
  node [
    id 0
    label "przyby&#263;"
    origin "text"
  ]
  node [
    id 1
    label "jaskinia"
    origin "text"
  ]
  node [
    id 2
    label "zasta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wszyscy"
    origin "text"
  ]
  node [
    id 4
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 5
    label "zebra&#263;"
    origin "text"
  ]
  node [
    id 6
    label "&#347;niadanie"
    origin "text"
  ]
  node [
    id 7
    label "dla"
    origin "text"
  ]
  node [
    id 8
    label "jedno"
    origin "text"
  ]
  node [
    id 9
    label "miejsce"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "nieznajomy"
    origin "text"
  ]
  node [
    id 12
    label "wcale"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "przybywa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "taki"
    origin "text"
  ]
  node [
    id 16
    label "zwyczaj"
    origin "text"
  ]
  node [
    id 17
    label "go&#347;cinno&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "hiszpa&#324;ski"
    origin "text"
  ]
  node [
    id 19
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 20
    label "nigdy"
    origin "text"
  ]
  node [
    id 21
    label "nikt"
    origin "text"
  ]
  node [
    id 22
    label "powa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 23
    label "uchybia&#263;"
    origin "text"
  ]
  node [
    id 24
    label "zyska&#263;"
  ]
  node [
    id 25
    label "dotrze&#263;"
  ]
  node [
    id 26
    label "get"
  ]
  node [
    id 27
    label "strop"
  ]
  node [
    id 28
    label "korytarz"
  ]
  node [
    id 29
    label "komora"
  ]
  node [
    id 30
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 31
    label "wzi&#261;&#263;"
  ]
  node [
    id 32
    label "spowodowa&#263;"
  ]
  node [
    id 33
    label "sprz&#261;tn&#261;&#263;"
  ]
  node [
    id 34
    label "przyci&#261;gn&#261;&#263;"
  ]
  node [
    id 35
    label "wezbra&#263;"
  ]
  node [
    id 36
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 37
    label "congregate"
  ]
  node [
    id 38
    label "oszcz&#281;dzi&#263;"
  ]
  node [
    id 39
    label "dosta&#263;"
  ]
  node [
    id 40
    label "pozyska&#263;"
  ]
  node [
    id 41
    label "zgromadzi&#263;"
  ]
  node [
    id 42
    label "przej&#261;&#263;"
  ]
  node [
    id 43
    label "umie&#347;ci&#263;"
  ]
  node [
    id 44
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 45
    label "raise"
  ]
  node [
    id 46
    label "skupi&#263;"
  ]
  node [
    id 47
    label "nat&#281;&#380;y&#263;"
  ]
  node [
    id 48
    label "plane"
  ]
  node [
    id 49
    label "posi&#322;ek"
  ]
  node [
    id 50
    label "cia&#322;o"
  ]
  node [
    id 51
    label "plac"
  ]
  node [
    id 52
    label "cecha"
  ]
  node [
    id 53
    label "uwaga"
  ]
  node [
    id 54
    label "przestrze&#324;"
  ]
  node [
    id 55
    label "status"
  ]
  node [
    id 56
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 57
    label "chwila"
  ]
  node [
    id 58
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 59
    label "rz&#261;d"
  ]
  node [
    id 60
    label "praca"
  ]
  node [
    id 61
    label "location"
  ]
  node [
    id 62
    label "warunek_lokalowy"
  ]
  node [
    id 63
    label "obcy"
  ]
  node [
    id 64
    label "nieznajomo"
  ]
  node [
    id 65
    label "ksenofil"
  ]
  node [
    id 66
    label "ni_chuja"
  ]
  node [
    id 67
    label "ca&#322;kiem"
  ]
  node [
    id 68
    label "zupe&#322;nie"
  ]
  node [
    id 69
    label "si&#281;ga&#263;"
  ]
  node [
    id 70
    label "trwa&#263;"
  ]
  node [
    id 71
    label "obecno&#347;&#263;"
  ]
  node [
    id 72
    label "stan"
  ]
  node [
    id 73
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 74
    label "stand"
  ]
  node [
    id 75
    label "mie&#263;_miejsce"
  ]
  node [
    id 76
    label "uczestniczy&#263;"
  ]
  node [
    id 77
    label "chodzi&#263;"
  ]
  node [
    id 78
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 79
    label "equal"
  ]
  node [
    id 80
    label "zyskiwa&#263;"
  ]
  node [
    id 81
    label "dociera&#263;"
  ]
  node [
    id 82
    label "okre&#347;lony"
  ]
  node [
    id 83
    label "jaki&#347;"
  ]
  node [
    id 84
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 85
    label "zachowanie"
  ]
  node [
    id 86
    label "kultura"
  ]
  node [
    id 87
    label "kultura_duchowa"
  ]
  node [
    id 88
    label "ceremony"
  ]
  node [
    id 89
    label "hospitableness"
  ]
  node [
    id 90
    label "towarzysko&#347;&#263;"
  ]
  node [
    id 91
    label "uczynno&#347;&#263;"
  ]
  node [
    id 92
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 93
    label "&#347;r&#243;dziemnomorski"
  ]
  node [
    id 94
    label "po_hiszpa&#324;sku"
  ]
  node [
    id 95
    label "fandango"
  ]
  node [
    id 96
    label "nami&#281;tny"
  ]
  node [
    id 97
    label "j&#281;zyk"
  ]
  node [
    id 98
    label "europejski"
  ]
  node [
    id 99
    label "hispanoj&#281;zyczny"
  ]
  node [
    id 100
    label "po&#322;udniowoeuropejski"
  ]
  node [
    id 101
    label "paso_doble"
  ]
  node [
    id 102
    label "hispanistyka"
  ]
  node [
    id 103
    label "Spanish"
  ]
  node [
    id 104
    label "j&#281;zyk_roma&#324;ski"
  ]
  node [
    id 105
    label "sarabanda"
  ]
  node [
    id 106
    label "pawana"
  ]
  node [
    id 107
    label "hiszpan"
  ]
  node [
    id 108
    label "kompletnie"
  ]
  node [
    id 109
    label "miernota"
  ]
  node [
    id 110
    label "ciura"
  ]
  node [
    id 111
    label "czu&#263;"
  ]
  node [
    id 112
    label "chowa&#263;"
  ]
  node [
    id 113
    label "treasure"
  ]
  node [
    id 114
    label "respektowa&#263;"
  ]
  node [
    id 115
    label "powa&#380;anie"
  ]
  node [
    id 116
    label "wyra&#380;a&#263;"
  ]
  node [
    id 117
    label "transgress"
  ]
  node [
    id 118
    label "robi&#263;"
  ]
  node [
    id 119
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 120
    label "mistreat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 86
  ]
  edge [
    source 16
    target 87
  ]
  edge [
    source 16
    target 88
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 17
    target 91
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 94
  ]
  edge [
    source 18
    target 95
  ]
  edge [
    source 18
    target 96
  ]
  edge [
    source 18
    target 97
  ]
  edge [
    source 18
    target 98
  ]
  edge [
    source 18
    target 99
  ]
  edge [
    source 18
    target 100
  ]
  edge [
    source 18
    target 101
  ]
  edge [
    source 18
    target 102
  ]
  edge [
    source 18
    target 103
  ]
  edge [
    source 18
    target 104
  ]
  edge [
    source 18
    target 105
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 18
    target 107
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 108
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 109
  ]
  edge [
    source 21
    target 110
  ]
  edge [
    source 22
    target 111
  ]
  edge [
    source 22
    target 112
  ]
  edge [
    source 22
    target 113
  ]
  edge [
    source 22
    target 114
  ]
  edge [
    source 22
    target 115
  ]
  edge [
    source 22
    target 116
  ]
  edge [
    source 23
    target 117
  ]
  edge [
    source 23
    target 118
  ]
  edge [
    source 23
    target 119
  ]
  edge [
    source 23
    target 120
  ]
]
