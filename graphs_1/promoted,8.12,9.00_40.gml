graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.046511627906977
  density 0.048726467331118496
  graphCliqueNumber 4
  node [
    id 0
    label "cztery"
    origin "text"
  ]
  node [
    id 1
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "areszt"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;o&#324;ce"
  ]
  node [
    id 4
    label "czynienie_si&#281;"
  ]
  node [
    id 5
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 6
    label "czas"
  ]
  node [
    id 7
    label "long_time"
  ]
  node [
    id 8
    label "przedpo&#322;udnie"
  ]
  node [
    id 9
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 10
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 11
    label "tydzie&#324;"
  ]
  node [
    id 12
    label "godzina"
  ]
  node [
    id 13
    label "t&#322;usty_czwartek"
  ]
  node [
    id 14
    label "wsta&#263;"
  ]
  node [
    id 15
    label "day"
  ]
  node [
    id 16
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 17
    label "przedwiecz&#243;r"
  ]
  node [
    id 18
    label "Sylwester"
  ]
  node [
    id 19
    label "po&#322;udnie"
  ]
  node [
    id 20
    label "wzej&#347;cie"
  ]
  node [
    id 21
    label "podwiecz&#243;r"
  ]
  node [
    id 22
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 23
    label "rano"
  ]
  node [
    id 24
    label "termin"
  ]
  node [
    id 25
    label "ranek"
  ]
  node [
    id 26
    label "doba"
  ]
  node [
    id 27
    label "wiecz&#243;r"
  ]
  node [
    id 28
    label "walentynki"
  ]
  node [
    id 29
    label "popo&#322;udnie"
  ]
  node [
    id 30
    label "noc"
  ]
  node [
    id 31
    label "wstanie"
  ]
  node [
    id 32
    label "ul"
  ]
  node [
    id 33
    label "zarz&#261;d"
  ]
  node [
    id 34
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 35
    label "procedura"
  ]
  node [
    id 36
    label "miejsce_odosobnienia"
  ]
  node [
    id 37
    label "ograniczenie"
  ]
  node [
    id 38
    label "poj&#281;cie"
  ]
  node [
    id 39
    label "s&#261;d"
  ]
  node [
    id 40
    label "rejonowy"
  ]
  node [
    id 41
    label "w"
  ]
  node [
    id 42
    label "Tczew"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 41
    target 42
  ]
]
