graph [
  maxDegree 37
  minDegree 1
  meanDegree 2.0259740259740258
  density 0.01324166030048383
  graphCliqueNumber 2
  node [
    id 0
    label "oskar"
    origin "text"
  ]
  node [
    id 1
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "&#347;mia&#263;"
    origin "text"
  ]
  node [
    id 4
    label "irytowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "jarek"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "nagle"
    origin "text"
  ]
  node [
    id 8
    label "podchodzi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "fachowy"
    origin "text"
  ]
  node [
    id 10
    label "ruch"
    origin "text"
  ]
  node [
    id 11
    label "wykr&#281;ca&#263;"
    origin "text"
  ]
  node [
    id 12
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 13
    label "zmusza&#263;"
    origin "text"
  ]
  node [
    id 14
    label "ukl&#281;kn&#261;&#263;"
    origin "text"
  ]
  node [
    id 15
    label "pod&#322;oga"
    origin "text"
  ]
  node [
    id 16
    label "mimo"
    origin "text"
  ]
  node [
    id 17
    label "przestawa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "open"
  ]
  node [
    id 19
    label "odejmowa&#263;"
  ]
  node [
    id 20
    label "mie&#263;_miejsce"
  ]
  node [
    id 21
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 22
    label "set_about"
  ]
  node [
    id 23
    label "begin"
  ]
  node [
    id 24
    label "post&#281;powa&#263;"
  ]
  node [
    id 25
    label "bankrupt"
  ]
  node [
    id 26
    label "dzia&#322;a&#263;_na_nerwy"
  ]
  node [
    id 27
    label "harass"
  ]
  node [
    id 28
    label "szybko"
  ]
  node [
    id 29
    label "raptowny"
  ]
  node [
    id 30
    label "nieprzewidzianie"
  ]
  node [
    id 31
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 32
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 33
    label "approach"
  ]
  node [
    id 34
    label "sprawdzian"
  ]
  node [
    id 35
    label "traktowa&#263;"
  ]
  node [
    id 36
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 37
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 38
    label "wspina&#263;_si&#281;"
  ]
  node [
    id 39
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 40
    label "wype&#322;nia&#263;"
  ]
  node [
    id 41
    label "dochodzi&#263;"
  ]
  node [
    id 42
    label "dociera&#263;"
  ]
  node [
    id 43
    label "oszukiwa&#263;"
  ]
  node [
    id 44
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 45
    label "odpowiada&#263;"
  ]
  node [
    id 46
    label "ciecz"
  ]
  node [
    id 47
    label "zawodowy"
  ]
  node [
    id 48
    label "profesjonalny"
  ]
  node [
    id 49
    label "fachowo"
  ]
  node [
    id 50
    label "dobry"
  ]
  node [
    id 51
    label "umiej&#281;tny"
  ]
  node [
    id 52
    label "specjalistyczny"
  ]
  node [
    id 53
    label "manewr"
  ]
  node [
    id 54
    label "model"
  ]
  node [
    id 55
    label "movement"
  ]
  node [
    id 56
    label "apraksja"
  ]
  node [
    id 57
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 58
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 59
    label "poruszenie"
  ]
  node [
    id 60
    label "commercial_enterprise"
  ]
  node [
    id 61
    label "dyssypacja_energii"
  ]
  node [
    id 62
    label "zmiana"
  ]
  node [
    id 63
    label "utrzymanie"
  ]
  node [
    id 64
    label "utrzyma&#263;"
  ]
  node [
    id 65
    label "komunikacja"
  ]
  node [
    id 66
    label "tumult"
  ]
  node [
    id 67
    label "kr&#243;tki"
  ]
  node [
    id 68
    label "drift"
  ]
  node [
    id 69
    label "utrzymywa&#263;"
  ]
  node [
    id 70
    label "stopek"
  ]
  node [
    id 71
    label "kanciasty"
  ]
  node [
    id 72
    label "d&#322;ugi"
  ]
  node [
    id 73
    label "zjawisko"
  ]
  node [
    id 74
    label "utrzymywanie"
  ]
  node [
    id 75
    label "czynno&#347;&#263;"
  ]
  node [
    id 76
    label "myk"
  ]
  node [
    id 77
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 78
    label "wydarzenie"
  ]
  node [
    id 79
    label "taktyka"
  ]
  node [
    id 80
    label "move"
  ]
  node [
    id 81
    label "natural_process"
  ]
  node [
    id 82
    label "lokomocja"
  ]
  node [
    id 83
    label "mechanika"
  ]
  node [
    id 84
    label "proces"
  ]
  node [
    id 85
    label "strumie&#324;"
  ]
  node [
    id 86
    label "aktywno&#347;&#263;"
  ]
  node [
    id 87
    label "travel"
  ]
  node [
    id 88
    label "odwraca&#263;"
  ]
  node [
    id 89
    label "wyjmowa&#263;"
  ]
  node [
    id 90
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 91
    label "corrupt"
  ]
  node [
    id 92
    label "deformowa&#263;"
  ]
  node [
    id 93
    label "kierunek"
  ]
  node [
    id 94
    label "wrench"
  ]
  node [
    id 95
    label "wy&#380;yma&#263;"
  ]
  node [
    id 96
    label "krzy&#380;"
  ]
  node [
    id 97
    label "paw"
  ]
  node [
    id 98
    label "rami&#281;"
  ]
  node [
    id 99
    label "gestykulowanie"
  ]
  node [
    id 100
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 101
    label "pracownik"
  ]
  node [
    id 102
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 103
    label "bramkarz"
  ]
  node [
    id 104
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 105
    label "handwriting"
  ]
  node [
    id 106
    label "hasta"
  ]
  node [
    id 107
    label "pi&#322;ka"
  ]
  node [
    id 108
    label "&#322;okie&#263;"
  ]
  node [
    id 109
    label "spos&#243;b"
  ]
  node [
    id 110
    label "zagrywka"
  ]
  node [
    id 111
    label "obietnica"
  ]
  node [
    id 112
    label "przedrami&#281;"
  ]
  node [
    id 113
    label "chwyta&#263;"
  ]
  node [
    id 114
    label "r&#261;czyna"
  ]
  node [
    id 115
    label "cecha"
  ]
  node [
    id 116
    label "wykroczenie"
  ]
  node [
    id 117
    label "kroki"
  ]
  node [
    id 118
    label "palec"
  ]
  node [
    id 119
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 120
    label "graba"
  ]
  node [
    id 121
    label "hand"
  ]
  node [
    id 122
    label "nadgarstek"
  ]
  node [
    id 123
    label "pomocnik"
  ]
  node [
    id 124
    label "k&#322;&#261;b"
  ]
  node [
    id 125
    label "hazena"
  ]
  node [
    id 126
    label "gestykulowa&#263;"
  ]
  node [
    id 127
    label "cmoknonsens"
  ]
  node [
    id 128
    label "d&#322;o&#324;"
  ]
  node [
    id 129
    label "chwytanie"
  ]
  node [
    id 130
    label "czerwona_kartka"
  ]
  node [
    id 131
    label "powodowa&#263;"
  ]
  node [
    id 132
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 133
    label "sandbag"
  ]
  node [
    id 134
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 135
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 136
    label "kneel"
  ]
  node [
    id 137
    label "przyj&#261;&#263;"
  ]
  node [
    id 138
    label "pokl&#281;kn&#261;&#263;"
  ]
  node [
    id 139
    label "zmieni&#263;"
  ]
  node [
    id 140
    label "zapadnia"
  ]
  node [
    id 141
    label "pomieszczenie"
  ]
  node [
    id 142
    label "budynek"
  ]
  node [
    id 143
    label "posadzka"
  ]
  node [
    id 144
    label "pojazd"
  ]
  node [
    id 145
    label "p&#322;aszczyzna"
  ]
  node [
    id 146
    label "ko&#324;czy&#263;"
  ]
  node [
    id 147
    label "&#380;y&#263;"
  ]
  node [
    id 148
    label "przebywa&#263;"
  ]
  node [
    id 149
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 150
    label "determine"
  ]
  node [
    id 151
    label "coating"
  ]
  node [
    id 152
    label "finish_up"
  ]
  node [
    id 153
    label "ko&#324;czy&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
]
