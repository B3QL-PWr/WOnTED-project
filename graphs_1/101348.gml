graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.5
  density 0.21428571428571427
  graphCliqueNumber 2
  node [
    id 0
    label "pogorzel"
    origin "text"
  ]
  node [
    id 1
    label "powiat"
    origin "text"
  ]
  node [
    id 2
    label "m&#322;awski"
    origin "text"
  ]
  node [
    id 3
    label "gmina"
  ]
  node [
    id 4
    label "jednostka_administracyjna"
  ]
  node [
    id 5
    label "wojew&#243;dztwo"
  ]
  node [
    id 6
    label "Wieczfnia"
  ]
  node [
    id 7
    label "ko&#347;cielny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 6
    target 7
  ]
]
