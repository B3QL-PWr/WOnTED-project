graph [
  maxDegree 47
  minDegree 1
  meanDegree 2.184357541899441
  density 0.006118648576749135
  graphCliqueNumber 3
  node [
    id 0
    label "forum"
    origin "text"
  ]
  node [
    id 1
    label "pismak"
    origin "text"
  ]
  node [
    id 2
    label "najstarszy"
    origin "text"
  ]
  node [
    id 3
    label "konkurs"
    origin "text"
  ]
  node [
    id 4
    label "gazetka"
    origin "text"
  ]
  node [
    id 5
    label "szkolny"
    origin "text"
  ]
  node [
    id 6
    label "polska"
    origin "text"
  ]
  node [
    id 7
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wa&#322;brzych"
    origin "text"
  ]
  node [
    id 10
    label "rok"
    origin "text"
  ]
  node [
    id 11
    label "pomys&#322;odawczyni"
    origin "text"
  ]
  node [
    id 12
    label "nauczycielka"
    origin "text"
  ]
  node [
    id 13
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 14
    label "polski"
    origin "text"
  ]
  node [
    id 15
    label "wieloletni"
    origin "text"
  ]
  node [
    id 16
    label "animatorka"
    origin "text"
  ]
  node [
    id 17
    label "media"
    origin "text"
  ]
  node [
    id 18
    label "el&#380;bieta"
    origin "text"
  ]
  node [
    id 19
    label "sura"
    origin "text"
  ]
  node [
    id 20
    label "opiekunka"
    origin "text"
  ]
  node [
    id 21
    label "obok"
    origin "text"
  ]
  node [
    id 22
    label "przez"
    origin "text"
  ]
  node [
    id 23
    label "lata"
    origin "text"
  ]
  node [
    id 24
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 25
    label "wymiar"
    origin "text"
  ]
  node [
    id 26
    label "regionalny"
    origin "text"
  ]
  node [
    id 27
    label "najpierw"
    origin "text"
  ]
  node [
    id 28
    label "woj"
    origin "text"
  ]
  node [
    id 29
    label "wa&#322;brzyskie"
    origin "text"
  ]
  node [
    id 30
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 31
    label "dolno&#347;l&#261;ski"
    origin "text"
  ]
  node [
    id 32
    label "si&#243;dma"
    origin "text"
  ]
  node [
    id 33
    label "edycja"
    origin "text"
  ]
  node [
    id 34
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 35
    label "zasi&#281;g"
    origin "text"
  ]
  node [
    id 36
    label "og&#243;lnopolski"
    origin "text"
  ]
  node [
    id 37
    label "jedenasta"
    origin "text"
  ]
  node [
    id 38
    label "poza"
    origin "text"
  ]
  node [
    id 39
    label "gala"
    origin "text"
  ]
  node [
    id 40
    label "poprzedza&#263;"
    origin "text"
  ]
  node [
    id 41
    label "dwudniowy"
    origin "text"
  ]
  node [
    id 42
    label "warsztat"
    origin "text"
  ]
  node [
    id 43
    label "dziennikarski"
    origin "text"
  ]
  node [
    id 44
    label "dla"
    origin "text"
  ]
  node [
    id 45
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 46
    label "dobry"
    origin "text"
  ]
  node [
    id 47
    label "redakcja"
    origin "text"
  ]
  node [
    id 48
    label "czternasta"
    origin "text"
  ]
  node [
    id 49
    label "wsp&#243;&#322;organizowa&#263;"
    origin "text"
  ]
  node [
    id 50
    label "fundacja"
    origin "text"
  ]
  node [
    id 51
    label "nowa"
    origin "text"
  ]
  node [
    id 52
    label "raz"
    origin "text"
  ]
  node [
    id 53
    label "kolejny"
    origin "text"
  ]
  node [
    id 54
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 55
    label "patronat"
    origin "text"
  ]
  node [
    id 56
    label "obj&#261;&#263;"
    origin "text"
  ]
  node [
    id 57
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 58
    label "edukacja"
    origin "text"
  ]
  node [
    id 59
    label "narodowy"
    origin "text"
  ]
  node [
    id 60
    label "s&#261;d"
  ]
  node [
    id 61
    label "plac"
  ]
  node [
    id 62
    label "miejsce"
  ]
  node [
    id 63
    label "grupa_dyskusyjna"
  ]
  node [
    id 64
    label "grupa"
  ]
  node [
    id 65
    label "przestrze&#324;"
  ]
  node [
    id 66
    label "agora"
  ]
  node [
    id 67
    label "bazylika"
  ]
  node [
    id 68
    label "konferencja"
  ]
  node [
    id 69
    label "strona"
  ]
  node [
    id 70
    label "portal"
  ]
  node [
    id 71
    label "gryzipi&#243;rek"
  ]
  node [
    id 72
    label "eliminacje"
  ]
  node [
    id 73
    label "Interwizja"
  ]
  node [
    id 74
    label "emulation"
  ]
  node [
    id 75
    label "impreza"
  ]
  node [
    id 76
    label "casting"
  ]
  node [
    id 77
    label "Eurowizja"
  ]
  node [
    id 78
    label "nab&#243;r"
  ]
  node [
    id 79
    label "czasopismo"
  ]
  node [
    id 80
    label "szkolnie"
  ]
  node [
    id 81
    label "szkoleniowy"
  ]
  node [
    id 82
    label "podstawowy"
  ]
  node [
    id 83
    label "prosty"
  ]
  node [
    id 84
    label "planowa&#263;"
  ]
  node [
    id 85
    label "dostosowywa&#263;"
  ]
  node [
    id 86
    label "pozyskiwa&#263;"
  ]
  node [
    id 87
    label "wprowadza&#263;"
  ]
  node [
    id 88
    label "treat"
  ]
  node [
    id 89
    label "przygotowywa&#263;"
  ]
  node [
    id 90
    label "create"
  ]
  node [
    id 91
    label "ensnare"
  ]
  node [
    id 92
    label "tworzy&#263;"
  ]
  node [
    id 93
    label "standard"
  ]
  node [
    id 94
    label "skupia&#263;"
  ]
  node [
    id 95
    label "si&#281;ga&#263;"
  ]
  node [
    id 96
    label "trwa&#263;"
  ]
  node [
    id 97
    label "obecno&#347;&#263;"
  ]
  node [
    id 98
    label "stan"
  ]
  node [
    id 99
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 100
    label "stand"
  ]
  node [
    id 101
    label "mie&#263;_miejsce"
  ]
  node [
    id 102
    label "uczestniczy&#263;"
  ]
  node [
    id 103
    label "chodzi&#263;"
  ]
  node [
    id 104
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 105
    label "equal"
  ]
  node [
    id 106
    label "stulecie"
  ]
  node [
    id 107
    label "kalendarz"
  ]
  node [
    id 108
    label "czas"
  ]
  node [
    id 109
    label "pora_roku"
  ]
  node [
    id 110
    label "cykl_astronomiczny"
  ]
  node [
    id 111
    label "p&#243;&#322;rocze"
  ]
  node [
    id 112
    label "kwarta&#322;"
  ]
  node [
    id 113
    label "kurs"
  ]
  node [
    id 114
    label "jubileusz"
  ]
  node [
    id 115
    label "miesi&#261;c"
  ]
  node [
    id 116
    label "martwy_sezon"
  ]
  node [
    id 117
    label "pisa&#263;"
  ]
  node [
    id 118
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 119
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 120
    label "ssanie"
  ]
  node [
    id 121
    label "po_koroniarsku"
  ]
  node [
    id 122
    label "przedmiot"
  ]
  node [
    id 123
    label "but"
  ]
  node [
    id 124
    label "m&#243;wienie"
  ]
  node [
    id 125
    label "rozumie&#263;"
  ]
  node [
    id 126
    label "formacja_geologiczna"
  ]
  node [
    id 127
    label "rozumienie"
  ]
  node [
    id 128
    label "m&#243;wi&#263;"
  ]
  node [
    id 129
    label "gramatyka"
  ]
  node [
    id 130
    label "pype&#263;"
  ]
  node [
    id 131
    label "makroglosja"
  ]
  node [
    id 132
    label "kawa&#322;ek"
  ]
  node [
    id 133
    label "artykulator"
  ]
  node [
    id 134
    label "kultura_duchowa"
  ]
  node [
    id 135
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 136
    label "jama_ustna"
  ]
  node [
    id 137
    label "spos&#243;b"
  ]
  node [
    id 138
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 139
    label "przet&#322;umaczenie"
  ]
  node [
    id 140
    label "t&#322;umaczenie"
  ]
  node [
    id 141
    label "language"
  ]
  node [
    id 142
    label "jeniec"
  ]
  node [
    id 143
    label "organ"
  ]
  node [
    id 144
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 145
    label "pismo"
  ]
  node [
    id 146
    label "formalizowanie"
  ]
  node [
    id 147
    label "fonetyka"
  ]
  node [
    id 148
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 149
    label "wokalizm"
  ]
  node [
    id 150
    label "liza&#263;"
  ]
  node [
    id 151
    label "s&#322;ownictwo"
  ]
  node [
    id 152
    label "napisa&#263;"
  ]
  node [
    id 153
    label "formalizowa&#263;"
  ]
  node [
    id 154
    label "natural_language"
  ]
  node [
    id 155
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 156
    label "stylik"
  ]
  node [
    id 157
    label "konsonantyzm"
  ]
  node [
    id 158
    label "urz&#261;dzenie"
  ]
  node [
    id 159
    label "ssa&#263;"
  ]
  node [
    id 160
    label "kod"
  ]
  node [
    id 161
    label "lizanie"
  ]
  node [
    id 162
    label "lacki"
  ]
  node [
    id 163
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 164
    label "sztajer"
  ]
  node [
    id 165
    label "drabant"
  ]
  node [
    id 166
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 167
    label "polak"
  ]
  node [
    id 168
    label "pierogi_ruskie"
  ]
  node [
    id 169
    label "krakowiak"
  ]
  node [
    id 170
    label "Polish"
  ]
  node [
    id 171
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 172
    label "oberek"
  ]
  node [
    id 173
    label "po_polsku"
  ]
  node [
    id 174
    label "mazur"
  ]
  node [
    id 175
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 176
    label "chodzony"
  ]
  node [
    id 177
    label "skoczny"
  ]
  node [
    id 178
    label "ryba_po_grecku"
  ]
  node [
    id 179
    label "goniony"
  ]
  node [
    id 180
    label "polsko"
  ]
  node [
    id 181
    label "d&#322;ugi"
  ]
  node [
    id 182
    label "d&#322;ugotrwa&#322;y"
  ]
  node [
    id 183
    label "przekazior"
  ]
  node [
    id 184
    label "mass-media"
  ]
  node [
    id 185
    label "uzbrajanie"
  ]
  node [
    id 186
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 187
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 188
    label "medium"
  ]
  node [
    id 189
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 190
    label "rozdzia&#322;"
  ]
  node [
    id 191
    label "Koran"
  ]
  node [
    id 192
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 193
    label "bliski"
  ]
  node [
    id 194
    label "summer"
  ]
  node [
    id 195
    label "proszek"
  ]
  node [
    id 196
    label "poziom"
  ]
  node [
    id 197
    label "znaczenie"
  ]
  node [
    id 198
    label "dymensja"
  ]
  node [
    id 199
    label "cecha"
  ]
  node [
    id 200
    label "parametr"
  ]
  node [
    id 201
    label "dane"
  ]
  node [
    id 202
    label "liczba"
  ]
  node [
    id 203
    label "ilo&#347;&#263;"
  ]
  node [
    id 204
    label "wielko&#347;&#263;"
  ]
  node [
    id 205
    label "warunek_lokalowy"
  ]
  node [
    id 206
    label "lokalny"
  ]
  node [
    id 207
    label "typowy"
  ]
  node [
    id 208
    label "tradycyjny"
  ]
  node [
    id 209
    label "regionalnie"
  ]
  node [
    id 210
    label "wcze&#347;niej"
  ]
  node [
    id 211
    label "nasamprz&#243;d"
  ]
  node [
    id 212
    label "pocz&#261;tkowo"
  ]
  node [
    id 213
    label "pierwiej"
  ]
  node [
    id 214
    label "pierw"
  ]
  node [
    id 215
    label "&#380;o&#322;nierz"
  ]
  node [
    id 216
    label "wite&#378;"
  ]
  node [
    id 217
    label "wojownik"
  ]
  node [
    id 218
    label "p&#243;&#378;ny"
  ]
  node [
    id 219
    label "po_dolno&#347;l&#261;sku"
  ]
  node [
    id 220
    label "&#347;l&#261;ski"
  ]
  node [
    id 221
    label "godzina"
  ]
  node [
    id 222
    label "impression"
  ]
  node [
    id 223
    label "odmiana"
  ]
  node [
    id 224
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 225
    label "notification"
  ]
  node [
    id 226
    label "cykl"
  ]
  node [
    id 227
    label "zmiana"
  ]
  node [
    id 228
    label "produkcja"
  ]
  node [
    id 229
    label "egzemplarz"
  ]
  node [
    id 230
    label "czyj&#347;"
  ]
  node [
    id 231
    label "m&#261;&#380;"
  ]
  node [
    id 232
    label "obszar"
  ]
  node [
    id 233
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 234
    label "og&#243;lnopolsko"
  ]
  node [
    id 235
    label "og&#243;lnokrajowy"
  ]
  node [
    id 236
    label "mode"
  ]
  node [
    id 237
    label "gra"
  ]
  node [
    id 238
    label "przesada"
  ]
  node [
    id 239
    label "ustawienie"
  ]
  node [
    id 240
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 241
    label "jab&#322;ko"
  ]
  node [
    id 242
    label "ceremonia"
  ]
  node [
    id 243
    label "jab&#322;o&#324;_domowa"
  ]
  node [
    id 244
    label "opatrywa&#263;"
  ]
  node [
    id 245
    label "kilkudniowy"
  ]
  node [
    id 246
    label "sprawno&#347;&#263;"
  ]
  node [
    id 247
    label "spotkanie"
  ]
  node [
    id 248
    label "wyposa&#380;enie"
  ]
  node [
    id 249
    label "st&#243;&#322;_roboczy"
  ]
  node [
    id 250
    label "pracownia"
  ]
  node [
    id 251
    label "zawodowy"
  ]
  node [
    id 252
    label "rzetelny"
  ]
  node [
    id 253
    label "tre&#347;ciwy"
  ]
  node [
    id 254
    label "po_dziennikarsku"
  ]
  node [
    id 255
    label "dziennikarsko"
  ]
  node [
    id 256
    label "obiektywny"
  ]
  node [
    id 257
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 258
    label "wzorowy"
  ]
  node [
    id 259
    label "cz&#322;owiek"
  ]
  node [
    id 260
    label "cz&#322;onek"
  ]
  node [
    id 261
    label "substytuowanie"
  ]
  node [
    id 262
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 263
    label "przyk&#322;ad"
  ]
  node [
    id 264
    label "zast&#281;pca"
  ]
  node [
    id 265
    label "substytuowa&#263;"
  ]
  node [
    id 266
    label "pomy&#347;lny"
  ]
  node [
    id 267
    label "skuteczny"
  ]
  node [
    id 268
    label "moralny"
  ]
  node [
    id 269
    label "korzystny"
  ]
  node [
    id 270
    label "odpowiedni"
  ]
  node [
    id 271
    label "zwrot"
  ]
  node [
    id 272
    label "dobrze"
  ]
  node [
    id 273
    label "pozytywny"
  ]
  node [
    id 274
    label "grzeczny"
  ]
  node [
    id 275
    label "powitanie"
  ]
  node [
    id 276
    label "mi&#322;y"
  ]
  node [
    id 277
    label "dobroczynny"
  ]
  node [
    id 278
    label "pos&#322;uszny"
  ]
  node [
    id 279
    label "ca&#322;y"
  ]
  node [
    id 280
    label "czw&#243;rka"
  ]
  node [
    id 281
    label "spokojny"
  ]
  node [
    id 282
    label "&#347;mieszny"
  ]
  node [
    id 283
    label "drogi"
  ]
  node [
    id 284
    label "telewizja"
  ]
  node [
    id 285
    label "redaction"
  ]
  node [
    id 286
    label "obr&#243;bka"
  ]
  node [
    id 287
    label "radio"
  ]
  node [
    id 288
    label "wydawnictwo"
  ]
  node [
    id 289
    label "zesp&#243;&#322;"
  ]
  node [
    id 290
    label "redaktor"
  ]
  node [
    id 291
    label "siedziba"
  ]
  node [
    id 292
    label "composition"
  ]
  node [
    id 293
    label "tekst"
  ]
  node [
    id 294
    label "foundation"
  ]
  node [
    id 295
    label "instytucja"
  ]
  node [
    id 296
    label "dar"
  ]
  node [
    id 297
    label "darowizna"
  ]
  node [
    id 298
    label "pocz&#261;tek"
  ]
  node [
    id 299
    label "gwiazda"
  ]
  node [
    id 300
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 301
    label "chwila"
  ]
  node [
    id 302
    label "uderzenie"
  ]
  node [
    id 303
    label "cios"
  ]
  node [
    id 304
    label "time"
  ]
  node [
    id 305
    label "inny"
  ]
  node [
    id 306
    label "nast&#281;pnie"
  ]
  node [
    id 307
    label "kt&#243;ry&#347;"
  ]
  node [
    id 308
    label "kolejno"
  ]
  node [
    id 309
    label "nastopny"
  ]
  node [
    id 310
    label "licencja"
  ]
  node [
    id 311
    label "opieka"
  ]
  node [
    id 312
    label "nadz&#243;r"
  ]
  node [
    id 313
    label "sponsorship"
  ]
  node [
    id 314
    label "obejmowa&#263;"
  ]
  node [
    id 315
    label "zacz&#261;&#263;"
  ]
  node [
    id 316
    label "embrace"
  ]
  node [
    id 317
    label "spowodowa&#263;"
  ]
  node [
    id 318
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 319
    label "obj&#281;cie"
  ]
  node [
    id 320
    label "skuma&#263;"
  ]
  node [
    id 321
    label "manipulate"
  ]
  node [
    id 322
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 323
    label "podj&#261;&#263;"
  ]
  node [
    id 324
    label "do"
  ]
  node [
    id 325
    label "dotkn&#261;&#263;"
  ]
  node [
    id 326
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 327
    label "assume"
  ]
  node [
    id 328
    label "involve"
  ]
  node [
    id 329
    label "zagarn&#261;&#263;"
  ]
  node [
    id 330
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 331
    label "ministerium"
  ]
  node [
    id 332
    label "resort"
  ]
  node [
    id 333
    label "urz&#261;d"
  ]
  node [
    id 334
    label "MSW"
  ]
  node [
    id 335
    label "departament"
  ]
  node [
    id 336
    label "NKWD"
  ]
  node [
    id 337
    label "kwalifikacje"
  ]
  node [
    id 338
    label "Karta_Nauczyciela"
  ]
  node [
    id 339
    label "szkolnictwo"
  ]
  node [
    id 340
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 341
    label "program_nauczania"
  ]
  node [
    id 342
    label "formation"
  ]
  node [
    id 343
    label "miasteczko_rowerowe"
  ]
  node [
    id 344
    label "gospodarka"
  ]
  node [
    id 345
    label "urszulanki"
  ]
  node [
    id 346
    label "wiedza"
  ]
  node [
    id 347
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 348
    label "skolaryzacja"
  ]
  node [
    id 349
    label "proces"
  ]
  node [
    id 350
    label "niepokalanki"
  ]
  node [
    id 351
    label "heureza"
  ]
  node [
    id 352
    label "form"
  ]
  node [
    id 353
    label "nauka"
  ]
  node [
    id 354
    label "&#322;awa_szkolna"
  ]
  node [
    id 355
    label "nacjonalistyczny"
  ]
  node [
    id 356
    label "narodowo"
  ]
  node [
    id 357
    label "wa&#380;ny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 51
  ]
  edge [
    source 17
    target 52
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 25
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 108
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 196
  ]
  edge [
    source 25
    target 197
  ]
  edge [
    source 25
    target 198
  ]
  edge [
    source 25
    target 69
  ]
  edge [
    source 25
    target 199
  ]
  edge [
    source 25
    target 200
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 25
    target 202
  ]
  edge [
    source 25
    target 203
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 206
  ]
  edge [
    source 26
    target 207
  ]
  edge [
    source 26
    target 208
  ]
  edge [
    source 26
    target 209
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 210
  ]
  edge [
    source 27
    target 211
  ]
  edge [
    source 27
    target 212
  ]
  edge [
    source 27
    target 213
  ]
  edge [
    source 27
    target 214
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 215
  ]
  edge [
    source 28
    target 216
  ]
  edge [
    source 28
    target 217
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 218
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 219
  ]
  edge [
    source 31
    target 220
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 221
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 48
  ]
  edge [
    source 33
    target 222
  ]
  edge [
    source 33
    target 223
  ]
  edge [
    source 33
    target 224
  ]
  edge [
    source 33
    target 225
  ]
  edge [
    source 33
    target 226
  ]
  edge [
    source 33
    target 227
  ]
  edge [
    source 33
    target 228
  ]
  edge [
    source 33
    target 229
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 230
  ]
  edge [
    source 34
    target 231
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 232
  ]
  edge [
    source 35
    target 233
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 234
  ]
  edge [
    source 36
    target 235
  ]
  edge [
    source 37
    target 221
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 236
  ]
  edge [
    source 38
    target 237
  ]
  edge [
    source 38
    target 238
  ]
  edge [
    source 38
    target 239
  ]
  edge [
    source 38
    target 240
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 241
  ]
  edge [
    source 39
    target 242
  ]
  edge [
    source 39
    target 243
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 244
  ]
  edge [
    source 40
    target 108
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 245
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 58
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 62
  ]
  edge [
    source 42
    target 246
  ]
  edge [
    source 42
    target 247
  ]
  edge [
    source 42
    target 248
  ]
  edge [
    source 42
    target 249
  ]
  edge [
    source 42
    target 250
  ]
  edge [
    source 42
    target 48
  ]
  edge [
    source 42
    target 59
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 251
  ]
  edge [
    source 43
    target 252
  ]
  edge [
    source 43
    target 253
  ]
  edge [
    source 43
    target 254
  ]
  edge [
    source 43
    target 255
  ]
  edge [
    source 43
    target 256
  ]
  edge [
    source 43
    target 257
  ]
  edge [
    source 43
    target 258
  ]
  edge [
    source 43
    target 207
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 259
  ]
  edge [
    source 45
    target 260
  ]
  edge [
    source 45
    target 261
  ]
  edge [
    source 45
    target 262
  ]
  edge [
    source 45
    target 263
  ]
  edge [
    source 45
    target 264
  ]
  edge [
    source 45
    target 265
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 266
  ]
  edge [
    source 46
    target 267
  ]
  edge [
    source 46
    target 268
  ]
  edge [
    source 46
    target 269
  ]
  edge [
    source 46
    target 270
  ]
  edge [
    source 46
    target 271
  ]
  edge [
    source 46
    target 272
  ]
  edge [
    source 46
    target 273
  ]
  edge [
    source 46
    target 274
  ]
  edge [
    source 46
    target 275
  ]
  edge [
    source 46
    target 276
  ]
  edge [
    source 46
    target 277
  ]
  edge [
    source 46
    target 278
  ]
  edge [
    source 46
    target 279
  ]
  edge [
    source 46
    target 257
  ]
  edge [
    source 46
    target 280
  ]
  edge [
    source 46
    target 281
  ]
  edge [
    source 46
    target 282
  ]
  edge [
    source 46
    target 283
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 284
  ]
  edge [
    source 47
    target 285
  ]
  edge [
    source 47
    target 286
  ]
  edge [
    source 47
    target 287
  ]
  edge [
    source 47
    target 288
  ]
  edge [
    source 47
    target 289
  ]
  edge [
    source 47
    target 290
  ]
  edge [
    source 47
    target 291
  ]
  edge [
    source 47
    target 292
  ]
  edge [
    source 47
    target 293
  ]
  edge [
    source 47
    target 58
  ]
  edge [
    source 48
    target 221
  ]
  edge [
    source 48
    target 59
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 294
  ]
  edge [
    source 50
    target 295
  ]
  edge [
    source 50
    target 296
  ]
  edge [
    source 50
    target 297
  ]
  edge [
    source 50
    target 298
  ]
  edge [
    source 51
    target 299
  ]
  edge [
    source 51
    target 300
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 301
  ]
  edge [
    source 52
    target 302
  ]
  edge [
    source 52
    target 303
  ]
  edge [
    source 52
    target 304
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 305
  ]
  edge [
    source 53
    target 306
  ]
  edge [
    source 53
    target 307
  ]
  edge [
    source 53
    target 308
  ]
  edge [
    source 53
    target 309
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 310
  ]
  edge [
    source 55
    target 311
  ]
  edge [
    source 55
    target 312
  ]
  edge [
    source 55
    target 313
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 314
  ]
  edge [
    source 56
    target 315
  ]
  edge [
    source 56
    target 316
  ]
  edge [
    source 56
    target 317
  ]
  edge [
    source 56
    target 318
  ]
  edge [
    source 56
    target 319
  ]
  edge [
    source 56
    target 320
  ]
  edge [
    source 56
    target 321
  ]
  edge [
    source 56
    target 322
  ]
  edge [
    source 56
    target 323
  ]
  edge [
    source 56
    target 324
  ]
  edge [
    source 56
    target 325
  ]
  edge [
    source 56
    target 326
  ]
  edge [
    source 56
    target 327
  ]
  edge [
    source 56
    target 328
  ]
  edge [
    source 56
    target 329
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 330
  ]
  edge [
    source 57
    target 331
  ]
  edge [
    source 57
    target 332
  ]
  edge [
    source 57
    target 333
  ]
  edge [
    source 57
    target 334
  ]
  edge [
    source 57
    target 335
  ]
  edge [
    source 57
    target 336
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 337
  ]
  edge [
    source 58
    target 338
  ]
  edge [
    source 58
    target 339
  ]
  edge [
    source 58
    target 340
  ]
  edge [
    source 58
    target 341
  ]
  edge [
    source 58
    target 342
  ]
  edge [
    source 58
    target 343
  ]
  edge [
    source 58
    target 344
  ]
  edge [
    source 58
    target 345
  ]
  edge [
    source 58
    target 346
  ]
  edge [
    source 58
    target 347
  ]
  edge [
    source 58
    target 348
  ]
  edge [
    source 58
    target 349
  ]
  edge [
    source 58
    target 350
  ]
  edge [
    source 58
    target 351
  ]
  edge [
    source 58
    target 352
  ]
  edge [
    source 58
    target 353
  ]
  edge [
    source 58
    target 354
  ]
  edge [
    source 59
    target 355
  ]
  edge [
    source 59
    target 257
  ]
  edge [
    source 59
    target 356
  ]
  edge [
    source 59
    target 357
  ]
]
