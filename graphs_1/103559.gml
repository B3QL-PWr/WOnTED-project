graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9655172413793103
  density 0.034482758620689655
  graphCliqueNumber 2
  node [
    id 0
    label "anna"
    origin "text"
  ]
  node [
    id 1
    label "mat"
    origin "text"
  ]
  node [
    id 2
    label "nawet"
    origin "text"
  ]
  node [
    id 3
    label "czas"
    origin "text"
  ]
  node [
    id 4
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "spotka&#263;"
    origin "text"
  ]
  node [
    id 7
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 8
    label "ruch"
  ]
  node [
    id 9
    label "podoficer_marynarki"
  ]
  node [
    id 10
    label "szach"
  ]
  node [
    id 11
    label "szachy"
  ]
  node [
    id 12
    label "czasokres"
  ]
  node [
    id 13
    label "trawienie"
  ]
  node [
    id 14
    label "kategoria_gramatyczna"
  ]
  node [
    id 15
    label "period"
  ]
  node [
    id 16
    label "odczyt"
  ]
  node [
    id 17
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 18
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 19
    label "chwila"
  ]
  node [
    id 20
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 21
    label "poprzedzenie"
  ]
  node [
    id 22
    label "koniugacja"
  ]
  node [
    id 23
    label "dzieje"
  ]
  node [
    id 24
    label "poprzedzi&#263;"
  ]
  node [
    id 25
    label "przep&#322;ywanie"
  ]
  node [
    id 26
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 27
    label "odwlekanie_si&#281;"
  ]
  node [
    id 28
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 29
    label "Zeitgeist"
  ]
  node [
    id 30
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 31
    label "okres_czasu"
  ]
  node [
    id 32
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 33
    label "pochodzi&#263;"
  ]
  node [
    id 34
    label "schy&#322;ek"
  ]
  node [
    id 35
    label "czwarty_wymiar"
  ]
  node [
    id 36
    label "chronometria"
  ]
  node [
    id 37
    label "poprzedzanie"
  ]
  node [
    id 38
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 39
    label "pogoda"
  ]
  node [
    id 40
    label "zegar"
  ]
  node [
    id 41
    label "trawi&#263;"
  ]
  node [
    id 42
    label "pochodzenie"
  ]
  node [
    id 43
    label "poprzedza&#263;"
  ]
  node [
    id 44
    label "time_period"
  ]
  node [
    id 45
    label "rachuba_czasu"
  ]
  node [
    id 46
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 47
    label "czasoprzestrze&#324;"
  ]
  node [
    id 48
    label "laba"
  ]
  node [
    id 49
    label "befall"
  ]
  node [
    id 50
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 51
    label "pozna&#263;"
  ]
  node [
    id 52
    label "spowodowa&#263;"
  ]
  node [
    id 53
    label "go_steady"
  ]
  node [
    id 54
    label "insert"
  ]
  node [
    id 55
    label "znale&#378;&#263;"
  ]
  node [
    id 56
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 57
    label "visualize"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
]
