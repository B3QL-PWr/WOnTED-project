graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.202020202020202
  density 0.022469593898165326
  graphCliqueNumber 5
  node [
    id 0
    label "sp&#243;&#322;ka"
    origin "text"
  ]
  node [
    id 1
    label "cegielski"
    origin "text"
  ]
  node [
    id 2
    label "pozna&#324;"
    origin "text"
  ]
  node [
    id 3
    label "podj&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 5
    label "korea&#324;ski"
    origin "text"
  ]
  node [
    id 6
    label "hyundai"
    origin "text"
  ]
  node [
    id 7
    label "rotem"
    origin "text"
  ]
  node [
    id 8
    label "company"
    origin "text"
  ]
  node [
    id 9
    label "cel"
    origin "text"
  ]
  node [
    id 10
    label "oferowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 12
    label "zbrojny"
    origin "text"
  ]
  node [
    id 13
    label "polsko"
    origin "text"
  ]
  node [
    id 14
    label "czo&#322;g"
    origin "text"
  ]
  node [
    id 15
    label "nowa"
    origin "text"
  ]
  node [
    id 16
    label "generacja"
    origin "text"
  ]
  node [
    id 17
    label "podmiot_gospodarczy"
  ]
  node [
    id 18
    label "organizacja"
  ]
  node [
    id 19
    label "zesp&#243;&#322;"
  ]
  node [
    id 20
    label "wsp&#243;lnictwo"
  ]
  node [
    id 21
    label "zmieni&#263;"
  ]
  node [
    id 22
    label "zacz&#261;&#263;"
  ]
  node [
    id 23
    label "zareagowa&#263;"
  ]
  node [
    id 24
    label "allude"
  ]
  node [
    id 25
    label "raise"
  ]
  node [
    id 26
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 27
    label "draw"
  ]
  node [
    id 28
    label "zrobi&#263;"
  ]
  node [
    id 29
    label "wsp&#243;&#322;pracownictwo"
  ]
  node [
    id 30
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 31
    label "po_korea&#324;sku"
  ]
  node [
    id 32
    label "bulgogi"
  ]
  node [
    id 33
    label "j&#281;zyk"
  ]
  node [
    id 34
    label "j&#281;zyk_izolowany"
  ]
  node [
    id 35
    label "marchew_po_korea&#324;sku"
  ]
  node [
    id 36
    label "bibimbap"
  ]
  node [
    id 37
    label "Korean"
  ]
  node [
    id 38
    label "azjatycki"
  ]
  node [
    id 39
    label "kimbap"
  ]
  node [
    id 40
    label "dalekowschodni"
  ]
  node [
    id 41
    label "miejsce"
  ]
  node [
    id 42
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 43
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 44
    label "rzecz"
  ]
  node [
    id 45
    label "punkt"
  ]
  node [
    id 46
    label "thing"
  ]
  node [
    id 47
    label "rezultat"
  ]
  node [
    id 48
    label "volunteer"
  ]
  node [
    id 49
    label "zach&#281;ca&#263;"
  ]
  node [
    id 50
    label "wojsko"
  ]
  node [
    id 51
    label "magnitude"
  ]
  node [
    id 52
    label "energia"
  ]
  node [
    id 53
    label "capacity"
  ]
  node [
    id 54
    label "wuchta"
  ]
  node [
    id 55
    label "cecha"
  ]
  node [
    id 56
    label "parametr"
  ]
  node [
    id 57
    label "moment_si&#322;y"
  ]
  node [
    id 58
    label "przemoc"
  ]
  node [
    id 59
    label "zdolno&#347;&#263;"
  ]
  node [
    id 60
    label "mn&#243;stwo"
  ]
  node [
    id 61
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 62
    label "rozwi&#261;zanie"
  ]
  node [
    id 63
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 64
    label "potencja"
  ]
  node [
    id 65
    label "zjawisko"
  ]
  node [
    id 66
    label "zaleta"
  ]
  node [
    id 67
    label "zbrojnie"
  ]
  node [
    id 68
    label "ostry"
  ]
  node [
    id 69
    label "uzbrojony"
  ]
  node [
    id 70
    label "przyodziany"
  ]
  node [
    id 71
    label "po_polsku"
  ]
  node [
    id 72
    label "polski"
  ]
  node [
    id 73
    label "europejsko"
  ]
  node [
    id 74
    label "ci&#281;&#380;ki_sprz&#281;t"
  ]
  node [
    id 75
    label "opancerzony_pojazd_bojowy"
  ]
  node [
    id 76
    label "armata"
  ]
  node [
    id 77
    label "ekran_przeciwkumulacyjny"
  ]
  node [
    id 78
    label "tra&#322;"
  ]
  node [
    id 79
    label "pojazd_g&#261;sienicowy"
  ]
  node [
    id 80
    label "luk"
  ]
  node [
    id 81
    label "wojska_pancerne"
  ]
  node [
    id 82
    label "bro&#324;"
  ]
  node [
    id 83
    label "peryskop"
  ]
  node [
    id 84
    label "bro&#324;_pancerna"
  ]
  node [
    id 85
    label "gwiazda"
  ]
  node [
    id 86
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 87
    label "rocznik"
  ]
  node [
    id 88
    label "zbi&#243;r"
  ]
  node [
    id 89
    label "praca"
  ]
  node [
    id 90
    label "proces_fizyczny"
  ]
  node [
    id 91
    label "coevals"
  ]
  node [
    id 92
    label "H"
  ]
  node [
    id 93
    label "Cegielski"
  ]
  node [
    id 94
    label "Rotem"
  ]
  node [
    id 95
    label "Company"
  ]
  node [
    id 96
    label "RP"
  ]
  node [
    id 97
    label "Pozna&#324;"
  ]
  node [
    id 98
    label "SA"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 75
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 14
    target 78
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 85
  ]
  edge [
    source 15
    target 86
  ]
  edge [
    source 16
    target 87
  ]
  edge [
    source 16
    target 88
  ]
  edge [
    source 16
    target 63
  ]
  edge [
    source 16
    target 89
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 91
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 97
  ]
  edge [
    source 92
    target 98
  ]
  edge [
    source 93
    target 97
  ]
  edge [
    source 93
    target 98
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 97
    target 98
  ]
]
