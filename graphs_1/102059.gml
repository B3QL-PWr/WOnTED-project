graph [
  maxDegree 21
  minDegree 1
  meanDegree 2.026315789473684
  density 0.013419309864064134
  graphCliqueNumber 2
  node [
    id 0
    label "jura"
    origin "text"
  ]
  node [
    id 1
    label "konkurs"
    origin "text"
  ]
  node [
    id 2
    label "qmam"
    origin "text"
  ]
  node [
    id 3
    label "media"
    origin "text"
  ]
  node [
    id 4
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 6
    label "dobry"
    origin "text"
  ]
  node [
    id 7
    label "qmam&#243;w"
    origin "text"
  ]
  node [
    id 8
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "luty"
    origin "text"
  ]
  node [
    id 10
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 11
    label "lista"
    origin "text"
  ]
  node [
    id 12
    label "laureat"
    origin "text"
  ]
  node [
    id 13
    label "dwa"
    origin "text"
  ]
  node [
    id 14
    label "kategoria"
    origin "text"
  ]
  node [
    id 15
    label "cykliczny"
    origin "text"
  ]
  node [
    id 16
    label "warsztatowy"
    origin "text"
  ]
  node [
    id 17
    label "bez"
    origin "text"
  ]
  node [
    id 18
    label "kit"
    origin "text"
  ]
  node [
    id 19
    label "harcerski"
    origin "text"
  ]
  node [
    id 20
    label "eskulap"
    origin "text"
  ]
  node [
    id 21
    label "rzep"
    origin "text"
  ]
  node [
    id 22
    label "read"
    origin "text"
  ]
  node [
    id 23
    label "melchior"
    origin "text"
  ]
  node [
    id 24
    label "the"
    origin "text"
  ]
  node [
    id 25
    label "times"
    origin "text"
  ]
  node [
    id 26
    label "life"
    origin "text"
  ]
  node [
    id 27
    label "school"
    origin "text"
  ]
  node [
    id 28
    label "gratulowa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "formacja_geologiczna"
  ]
  node [
    id 30
    label "jura_&#347;rodkowa"
  ]
  node [
    id 31
    label "dogger"
  ]
  node [
    id 32
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 33
    label "era_mezozoiczna"
  ]
  node [
    id 34
    label "plezjozaur"
  ]
  node [
    id 35
    label "euoplocefal"
  ]
  node [
    id 36
    label "jura_wczesna"
  ]
  node [
    id 37
    label "eliminacje"
  ]
  node [
    id 38
    label "Interwizja"
  ]
  node [
    id 39
    label "emulation"
  ]
  node [
    id 40
    label "impreza"
  ]
  node [
    id 41
    label "casting"
  ]
  node [
    id 42
    label "Eurowizja"
  ]
  node [
    id 43
    label "nab&#243;r"
  ]
  node [
    id 44
    label "przekazior"
  ]
  node [
    id 45
    label "mass-media"
  ]
  node [
    id 46
    label "uzbrajanie"
  ]
  node [
    id 47
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 48
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 49
    label "medium"
  ]
  node [
    id 50
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 51
    label "przesta&#263;"
  ]
  node [
    id 52
    label "zrobi&#263;"
  ]
  node [
    id 53
    label "cause"
  ]
  node [
    id 54
    label "communicate"
  ]
  node [
    id 55
    label "czynno&#347;&#263;"
  ]
  node [
    id 56
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 57
    label "decyzja"
  ]
  node [
    id 58
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 59
    label "pick"
  ]
  node [
    id 60
    label "pomy&#347;lny"
  ]
  node [
    id 61
    label "skuteczny"
  ]
  node [
    id 62
    label "moralny"
  ]
  node [
    id 63
    label "korzystny"
  ]
  node [
    id 64
    label "odpowiedni"
  ]
  node [
    id 65
    label "zwrot"
  ]
  node [
    id 66
    label "dobrze"
  ]
  node [
    id 67
    label "pozytywny"
  ]
  node [
    id 68
    label "grzeczny"
  ]
  node [
    id 69
    label "powitanie"
  ]
  node [
    id 70
    label "mi&#322;y"
  ]
  node [
    id 71
    label "dobroczynny"
  ]
  node [
    id 72
    label "pos&#322;uszny"
  ]
  node [
    id 73
    label "ca&#322;y"
  ]
  node [
    id 74
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 75
    label "czw&#243;rka"
  ]
  node [
    id 76
    label "spokojny"
  ]
  node [
    id 77
    label "&#347;mieszny"
  ]
  node [
    id 78
    label "drogi"
  ]
  node [
    id 79
    label "upubliczni&#263;"
  ]
  node [
    id 80
    label "wydawnictwo"
  ]
  node [
    id 81
    label "wprowadzi&#263;"
  ]
  node [
    id 82
    label "picture"
  ]
  node [
    id 83
    label "miesi&#261;c"
  ]
  node [
    id 84
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 85
    label "walentynki"
  ]
  node [
    id 86
    label "przedstawienie"
  ]
  node [
    id 87
    label "pokazywa&#263;"
  ]
  node [
    id 88
    label "zapoznawa&#263;"
  ]
  node [
    id 89
    label "typify"
  ]
  node [
    id 90
    label "opisywa&#263;"
  ]
  node [
    id 91
    label "teatr"
  ]
  node [
    id 92
    label "podawa&#263;"
  ]
  node [
    id 93
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 94
    label "demonstrowa&#263;"
  ]
  node [
    id 95
    label "represent"
  ]
  node [
    id 96
    label "ukazywa&#263;"
  ]
  node [
    id 97
    label "attest"
  ]
  node [
    id 98
    label "exhibit"
  ]
  node [
    id 99
    label "stanowi&#263;"
  ]
  node [
    id 100
    label "zg&#322;asza&#263;"
  ]
  node [
    id 101
    label "display"
  ]
  node [
    id 102
    label "wyliczanka"
  ]
  node [
    id 103
    label "catalog"
  ]
  node [
    id 104
    label "stock"
  ]
  node [
    id 105
    label "figurowa&#263;"
  ]
  node [
    id 106
    label "zbi&#243;r"
  ]
  node [
    id 107
    label "book"
  ]
  node [
    id 108
    label "pozycja"
  ]
  node [
    id 109
    label "tekst"
  ]
  node [
    id 110
    label "sumariusz"
  ]
  node [
    id 111
    label "zdobywca"
  ]
  node [
    id 112
    label "forma"
  ]
  node [
    id 113
    label "wytw&#243;r"
  ]
  node [
    id 114
    label "type"
  ]
  node [
    id 115
    label "teoria"
  ]
  node [
    id 116
    label "poj&#281;cie"
  ]
  node [
    id 117
    label "klasa"
  ]
  node [
    id 118
    label "tyrocydyna"
  ]
  node [
    id 119
    label "zwi&#261;zek_cykliczny"
  ]
  node [
    id 120
    label "zwi&#261;zek_heterocykliczny"
  ]
  node [
    id 121
    label "cyklicznie"
  ]
  node [
    id 122
    label "regularny"
  ]
  node [
    id 123
    label "technicznie"
  ]
  node [
    id 124
    label "ki&#347;&#263;"
  ]
  node [
    id 125
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 126
    label "krzew"
  ]
  node [
    id 127
    label "pi&#380;maczkowate"
  ]
  node [
    id 128
    label "pestkowiec"
  ]
  node [
    id 129
    label "kwiat"
  ]
  node [
    id 130
    label "owoc"
  ]
  node [
    id 131
    label "oliwkowate"
  ]
  node [
    id 132
    label "ro&#347;lina"
  ]
  node [
    id 133
    label "hy&#263;ka"
  ]
  node [
    id 134
    label "lilac"
  ]
  node [
    id 135
    label "delfinidyna"
  ]
  node [
    id 136
    label "wcisk"
  ]
  node [
    id 137
    label "masa"
  ]
  node [
    id 138
    label "&#347;ciema"
  ]
  node [
    id 139
    label "po_harcersku"
  ]
  node [
    id 140
    label "stosowny"
  ]
  node [
    id 141
    label "typowy"
  ]
  node [
    id 142
    label "sprawno&#347;&#263;"
  ]
  node [
    id 143
    label "lekarz"
  ]
  node [
    id 144
    label "Aesculapius"
  ]
  node [
    id 145
    label "medyk"
  ]
  node [
    id 146
    label "nie&#322;upka"
  ]
  node [
    id 147
    label "bur"
  ]
  node [
    id 148
    label "zapi&#281;cie"
  ]
  node [
    id 149
    label "mosi&#261;dz"
  ]
  node [
    id 150
    label "compliment"
  ]
  node [
    id 151
    label "kierowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 146
  ]
  edge [
    source 21
    target 147
  ]
  edge [
    source 21
    target 148
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 150
  ]
  edge [
    source 28
    target 151
  ]
]
