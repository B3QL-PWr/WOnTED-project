graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "wie&#380;a"
    origin "text"
  ]
  node [
    id 1
    label "pancerny"
    origin "text"
  ]
  node [
    id 2
    label "zestaw"
  ]
  node [
    id 3
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 4
    label "odtwarzacz"
  ]
  node [
    id 5
    label "hejnalica"
  ]
  node [
    id 6
    label "budynek"
  ]
  node [
    id 7
    label "strzelec"
  ]
  node [
    id 8
    label "tuner"
  ]
  node [
    id 9
    label "sprz&#281;t_audio"
  ]
  node [
    id 10
    label "korektor"
  ]
  node [
    id 11
    label "wzmacniacz"
  ]
  node [
    id 12
    label "odporny"
  ]
  node [
    id 13
    label "ci&#281;&#380;kozbrojny"
  ]
  node [
    id 14
    label "mocny"
  ]
  node [
    id 15
    label "ochronny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
]
