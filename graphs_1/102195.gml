graph [
  maxDegree 57
  minDegree 1
  meanDegree 2.0098039215686274
  density 0.009900511928909495
  graphCliqueNumber 3
  node [
    id 0
    label "daleko"
    origin "text"
  ]
  node [
    id 1
    label "omarz&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "noga"
    origin "text"
  ]
  node [
    id 3
    label "martwy"
    origin "text"
  ]
  node [
    id 4
    label "szkieletowy"
    origin "text"
  ]
  node [
    id 5
    label "wola"
    origin "text"
  ]
  node [
    id 6
    label "raczej"
    origin "text"
  ]
  node [
    id 7
    label "niepoj&#281;ty"
    origin "text"
  ]
  node [
    id 8
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 9
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 10
    label "prowadzony"
    origin "text"
  ]
  node [
    id 11
    label "st&#261;pa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "&#347;lad"
    origin "text"
  ]
  node [
    id 13
    label "skostnia&#322;y"
    origin "text"
  ]
  node [
    id 14
    label "dziecko"
    origin "text"
  ]
  node [
    id 15
    label "otuli&#263;"
    origin "text"
  ]
  node [
    id 16
    label "paruch&#261;"
    origin "text"
  ]
  node [
    id 17
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 18
    label "matka"
    origin "text"
  ]
  node [
    id 19
    label "zdj&#261;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "siebie"
    origin "text"
  ]
  node [
    id 21
    label "dawno"
  ]
  node [
    id 22
    label "nisko"
  ]
  node [
    id 23
    label "nieobecnie"
  ]
  node [
    id 24
    label "daleki"
  ]
  node [
    id 25
    label "het"
  ]
  node [
    id 26
    label "wysoko"
  ]
  node [
    id 27
    label "du&#380;o"
  ]
  node [
    id 28
    label "znacznie"
  ]
  node [
    id 29
    label "g&#322;&#281;boko"
  ]
  node [
    id 30
    label "&#322;amaga"
  ]
  node [
    id 31
    label "kopa&#263;"
  ]
  node [
    id 32
    label "jedenastka"
  ]
  node [
    id 33
    label "mi&#281;sie&#324;_krawiecki"
  ]
  node [
    id 34
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 35
    label "sfaulowanie"
  ]
  node [
    id 36
    label "kopn&#261;&#263;"
  ]
  node [
    id 37
    label "czpas"
  ]
  node [
    id 38
    label "Wis&#322;a"
  ]
  node [
    id 39
    label "depta&#263;"
  ]
  node [
    id 40
    label "ekstraklasa"
  ]
  node [
    id 41
    label "kopni&#281;cie"
  ]
  node [
    id 42
    label "bramkarz"
  ]
  node [
    id 43
    label "mato&#322;"
  ]
  node [
    id 44
    label "r&#281;ka"
  ]
  node [
    id 45
    label "zamurowywa&#263;"
  ]
  node [
    id 46
    label "dogranie"
  ]
  node [
    id 47
    label "catenaccio"
  ]
  node [
    id 48
    label "lobowanie"
  ]
  node [
    id 49
    label "mi&#281;sie&#324;_po&#347;ladkowy_ma&#322;y"
  ]
  node [
    id 50
    label "tackle"
  ]
  node [
    id 51
    label "ko&#324;czyna_dolna"
  ]
  node [
    id 52
    label "interliga"
  ]
  node [
    id 53
    label "lobowa&#263;"
  ]
  node [
    id 54
    label "mi&#281;czak"
  ]
  node [
    id 55
    label "podpora"
  ]
  node [
    id 56
    label "stopa"
  ]
  node [
    id 57
    label "pi&#322;ka"
  ]
  node [
    id 58
    label "nerw_udowo-goleniowy"
  ]
  node [
    id 59
    label "bezbramkowy"
  ]
  node [
    id 60
    label "zamurowywanie"
  ]
  node [
    id 61
    label "przelobowa&#263;"
  ]
  node [
    id 62
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 63
    label "dogrywanie"
  ]
  node [
    id 64
    label "zamurowanie"
  ]
  node [
    id 65
    label "ko&#324;czyna"
  ]
  node [
    id 66
    label "faulowa&#263;"
  ]
  node [
    id 67
    label "narz&#261;d_ruchu"
  ]
  node [
    id 68
    label "napinacz"
  ]
  node [
    id 69
    label "mi&#281;sie&#324;_po&#347;ladkowy_&#347;redni"
  ]
  node [
    id 70
    label "dublet"
  ]
  node [
    id 71
    label "gira"
  ]
  node [
    id 72
    label "przelobowanie"
  ]
  node [
    id 73
    label "dogra&#263;"
  ]
  node [
    id 74
    label "zamurowa&#263;"
  ]
  node [
    id 75
    label "kopanie"
  ]
  node [
    id 76
    label "mundial"
  ]
  node [
    id 77
    label "mi&#281;sie&#324;_po&#347;ladkowy_wielki"
  ]
  node [
    id 78
    label "&#322;&#261;czyna"
  ]
  node [
    id 79
    label "dogrywa&#263;"
  ]
  node [
    id 80
    label "s&#322;abeusz"
  ]
  node [
    id 81
    label "faulowanie"
  ]
  node [
    id 82
    label "sfaulowa&#263;"
  ]
  node [
    id 83
    label "nerw_udowy"
  ]
  node [
    id 84
    label "czerwona_kartka"
  ]
  node [
    id 85
    label "duch"
  ]
  node [
    id 86
    label "cz&#322;owiek"
  ]
  node [
    id 87
    label "obumarcie"
  ]
  node [
    id 88
    label "umarlak"
  ]
  node [
    id 89
    label "nieaktualny"
  ]
  node [
    id 90
    label "obumieranie"
  ]
  node [
    id 91
    label "wiszenie"
  ]
  node [
    id 92
    label "umarcie"
  ]
  node [
    id 93
    label "zw&#322;oki"
  ]
  node [
    id 94
    label "bezmy&#347;lny"
  ]
  node [
    id 95
    label "umieranie"
  ]
  node [
    id 96
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 97
    label "chowanie"
  ]
  node [
    id 98
    label "martwo"
  ]
  node [
    id 99
    label "nieumar&#322;y"
  ]
  node [
    id 100
    label "trwa&#322;y"
  ]
  node [
    id 101
    label "niesprawny"
  ]
  node [
    id 102
    label "oskoma"
  ]
  node [
    id 103
    label "wish"
  ]
  node [
    id 104
    label "emocja"
  ]
  node [
    id 105
    label "mniemanie"
  ]
  node [
    id 106
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 107
    label "inclination"
  ]
  node [
    id 108
    label "zajawka"
  ]
  node [
    id 109
    label "niezrozumia&#322;y"
  ]
  node [
    id 110
    label "tajemniczy"
  ]
  node [
    id 111
    label "olbrzymi"
  ]
  node [
    id 112
    label "niepoj&#281;cie"
  ]
  node [
    id 113
    label "poziom"
  ]
  node [
    id 114
    label "faza"
  ]
  node [
    id 115
    label "depression"
  ]
  node [
    id 116
    label "zjawisko"
  ]
  node [
    id 117
    label "nizina"
  ]
  node [
    id 118
    label "wojsko"
  ]
  node [
    id 119
    label "magnitude"
  ]
  node [
    id 120
    label "energia"
  ]
  node [
    id 121
    label "capacity"
  ]
  node [
    id 122
    label "wuchta"
  ]
  node [
    id 123
    label "cecha"
  ]
  node [
    id 124
    label "parametr"
  ]
  node [
    id 125
    label "moment_si&#322;y"
  ]
  node [
    id 126
    label "przemoc"
  ]
  node [
    id 127
    label "zdolno&#347;&#263;"
  ]
  node [
    id 128
    label "mn&#243;stwo"
  ]
  node [
    id 129
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 130
    label "rozwi&#261;zanie"
  ]
  node [
    id 131
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 132
    label "potencja"
  ]
  node [
    id 133
    label "zaleta"
  ]
  node [
    id 134
    label "pace"
  ]
  node [
    id 135
    label "chodzi&#263;"
  ]
  node [
    id 136
    label "sznurowanie"
  ]
  node [
    id 137
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 138
    label "odrobina"
  ]
  node [
    id 139
    label "sznurowa&#263;"
  ]
  node [
    id 140
    label "attribute"
  ]
  node [
    id 141
    label "wp&#322;yw"
  ]
  node [
    id 142
    label "odcisk"
  ]
  node [
    id 143
    label "skutek"
  ]
  node [
    id 144
    label "stwardnia&#322;y"
  ]
  node [
    id 145
    label "zmarzni&#281;ty"
  ]
  node [
    id 146
    label "zastyg&#322;y"
  ]
  node [
    id 147
    label "przestarza&#322;y"
  ]
  node [
    id 148
    label "kostnienie"
  ]
  node [
    id 149
    label "potomstwo"
  ]
  node [
    id 150
    label "organizm"
  ]
  node [
    id 151
    label "sraluch"
  ]
  node [
    id 152
    label "utulanie"
  ]
  node [
    id 153
    label "pediatra"
  ]
  node [
    id 154
    label "dzieciarnia"
  ]
  node [
    id 155
    label "m&#322;odziak"
  ]
  node [
    id 156
    label "dzieciak"
  ]
  node [
    id 157
    label "utula&#263;"
  ]
  node [
    id 158
    label "potomek"
  ]
  node [
    id 159
    label "pedofil"
  ]
  node [
    id 160
    label "entliczek-pentliczek"
  ]
  node [
    id 161
    label "m&#322;odzik"
  ]
  node [
    id 162
    label "cz&#322;owieczek"
  ]
  node [
    id 163
    label "zwierz&#281;"
  ]
  node [
    id 164
    label "niepe&#322;noletni"
  ]
  node [
    id 165
    label "fledgling"
  ]
  node [
    id 166
    label "utuli&#263;"
  ]
  node [
    id 167
    label "utulenie"
  ]
  node [
    id 168
    label "muffle"
  ]
  node [
    id 169
    label "otoczy&#263;"
  ]
  node [
    id 170
    label "Matka_Boska"
  ]
  node [
    id 171
    label "matka_zast&#281;pcza"
  ]
  node [
    id 172
    label "stara"
  ]
  node [
    id 173
    label "rodzic"
  ]
  node [
    id 174
    label "matczysko"
  ]
  node [
    id 175
    label "ro&#347;lina"
  ]
  node [
    id 176
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 177
    label "gracz"
  ]
  node [
    id 178
    label "zawodnik"
  ]
  node [
    id 179
    label "macierz"
  ]
  node [
    id 180
    label "owad"
  ]
  node [
    id 181
    label "przyczyna"
  ]
  node [
    id 182
    label "macocha"
  ]
  node [
    id 183
    label "dwa_ognie"
  ]
  node [
    id 184
    label "staruszka"
  ]
  node [
    id 185
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 186
    label "rozsadnik"
  ]
  node [
    id 187
    label "zakonnica"
  ]
  node [
    id 188
    label "obiekt"
  ]
  node [
    id 189
    label "samica"
  ]
  node [
    id 190
    label "przodkini"
  ]
  node [
    id 191
    label "rodzice"
  ]
  node [
    id 192
    label "uwolni&#263;"
  ]
  node [
    id 193
    label "wzi&#261;&#263;"
  ]
  node [
    id 194
    label "zrobi&#263;"
  ]
  node [
    id 195
    label "cenzura"
  ]
  node [
    id 196
    label "abolicjonista"
  ]
  node [
    id 197
    label "pull"
  ]
  node [
    id 198
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 199
    label "draw"
  ]
  node [
    id 200
    label "odsun&#261;&#263;"
  ]
  node [
    id 201
    label "zabroni&#263;"
  ]
  node [
    id 202
    label "wyuzda&#263;"
  ]
  node [
    id 203
    label "lift"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
]
