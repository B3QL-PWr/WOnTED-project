graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9473684210526316
  density 0.05263157894736842
  graphCliqueNumber 2
  node [
    id 0
    label "&#347;wietny"
    origin "text"
  ]
  node [
    id 1
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 2
    label "rolling"
    origin "text"
  ]
  node [
    id 3
    label "stone"
    origin "text"
  ]
  node [
    id 4
    label "t&#322;umacz"
    origin "text"
  ]
  node [
    id 5
    label "pomy&#347;lny"
  ]
  node [
    id 6
    label "pozytywny"
  ]
  node [
    id 7
    label "wspaniale"
  ]
  node [
    id 8
    label "dobry"
  ]
  node [
    id 9
    label "superancki"
  ]
  node [
    id 10
    label "arcydzielny"
  ]
  node [
    id 11
    label "zajebisty"
  ]
  node [
    id 12
    label "wa&#380;ny"
  ]
  node [
    id 13
    label "&#347;wietnie"
  ]
  node [
    id 14
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 15
    label "skuteczny"
  ]
  node [
    id 16
    label "spania&#322;y"
  ]
  node [
    id 17
    label "dokument"
  ]
  node [
    id 18
    label "towar"
  ]
  node [
    id 19
    label "nag&#322;&#243;wek"
  ]
  node [
    id 20
    label "znak_j&#281;zykowy"
  ]
  node [
    id 21
    label "wyr&#243;b"
  ]
  node [
    id 22
    label "blok"
  ]
  node [
    id 23
    label "line"
  ]
  node [
    id 24
    label "paragraf"
  ]
  node [
    id 25
    label "rodzajnik"
  ]
  node [
    id 26
    label "prawda"
  ]
  node [
    id 27
    label "szkic"
  ]
  node [
    id 28
    label "tekst"
  ]
  node [
    id 29
    label "fragment"
  ]
  node [
    id 30
    label "tennis"
  ]
  node [
    id 31
    label "przek&#322;adowca"
  ]
  node [
    id 32
    label "przek&#322;adacz"
  ]
  node [
    id 33
    label "Jan_Czeczot"
  ]
  node [
    id 34
    label "aplikacja"
  ]
  node [
    id 35
    label "interpretator"
  ]
  node [
    id 36
    label "Jakub_Wujek"
  ]
  node [
    id 37
    label "pracownik_umys&#322;owy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
]
