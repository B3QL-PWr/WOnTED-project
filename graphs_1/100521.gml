graph [
  maxDegree 6
  minDegree 1
  meanDegree 3.142857142857143
  density 0.15714285714285714
  graphCliqueNumber 7
  node [
    id 0
    label "gwiezdny"
    origin "text"
  ]
  node [
    id 1
    label "ku&#378;nia"
    origin "text"
  ]
  node [
    id 2
    label "jasny"
  ]
  node [
    id 3
    label "instytucja"
  ]
  node [
    id 4
    label "warsztat"
  ]
  node [
    id 5
    label "fabryka"
  ]
  node [
    id 6
    label "wojna"
  ]
  node [
    id 7
    label "bezkresny"
  ]
  node [
    id 8
    label "imperium"
  ]
  node [
    id 9
    label "mandaloria&#324;skich"
  ]
  node [
    id 10
    label "republika"
  ]
  node [
    id 11
    label "galaktyczny"
  ]
  node [
    id 12
    label "domowy"
  ]
  node [
    id 13
    label "Jedi"
  ]
  node [
    id 14
    label "star"
  ]
  node [
    id 15
    label "Wars"
  ]
  node [
    id 16
    label "Knights"
  ]
  node [
    id 17
    label "of"
  ]
  node [
    id 18
    label "the"
  ]
  node [
    id 19
    label "Old"
  ]
  node [
    id 20
    label "Republic"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
]
