graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.9775280898876404
  density 0.02247191011235955
  graphCliqueNumber 2
  node [
    id 0
    label "dana"
    origin "text"
  ]
  node [
    id 1
    label "uzyska&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 3
    label "badanie"
    origin "text"
  ]
  node [
    id 4
    label "anemia"
    origin "text"
  ]
  node [
    id 5
    label "sierpowaty"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "sprawdza&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "francja"
    origin "text"
  ]
  node [
    id 10
    label "prawie"
    origin "text"
  ]
  node [
    id 11
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 12
    label "dziecko"
    origin "text"
  ]
  node [
    id 13
    label "pochodzenie"
    origin "text"
  ]
  node [
    id 14
    label "pozaeuropejski"
    origin "text"
  ]
  node [
    id 15
    label "dar"
  ]
  node [
    id 16
    label "cnota"
  ]
  node [
    id 17
    label "buddyzm"
  ]
  node [
    id 18
    label "promocja"
  ]
  node [
    id 19
    label "give_birth"
  ]
  node [
    id 20
    label "wytworzy&#263;"
  ]
  node [
    id 21
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 22
    label "realize"
  ]
  node [
    id 23
    label "zrobi&#263;"
  ]
  node [
    id 24
    label "make"
  ]
  node [
    id 25
    label "usi&#322;owanie"
  ]
  node [
    id 26
    label "examination"
  ]
  node [
    id 27
    label "investigation"
  ]
  node [
    id 28
    label "ustalenie"
  ]
  node [
    id 29
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 30
    label "ustalanie"
  ]
  node [
    id 31
    label "bia&#322;a_niedziela"
  ]
  node [
    id 32
    label "analysis"
  ]
  node [
    id 33
    label "rozpatrywanie"
  ]
  node [
    id 34
    label "wziernikowanie"
  ]
  node [
    id 35
    label "obserwowanie"
  ]
  node [
    id 36
    label "omawianie"
  ]
  node [
    id 37
    label "sprawdzanie"
  ]
  node [
    id 38
    label "udowadnianie"
  ]
  node [
    id 39
    label "diagnostyka"
  ]
  node [
    id 40
    label "czynno&#347;&#263;"
  ]
  node [
    id 41
    label "macanie"
  ]
  node [
    id 42
    label "rektalny"
  ]
  node [
    id 43
    label "penetrowanie"
  ]
  node [
    id 44
    label "krytykowanie"
  ]
  node [
    id 45
    label "kontrola"
  ]
  node [
    id 46
    label "dociekanie"
  ]
  node [
    id 47
    label "zrecenzowanie"
  ]
  node [
    id 48
    label "praca"
  ]
  node [
    id 49
    label "rezultat"
  ]
  node [
    id 50
    label "hipersplenizm"
  ]
  node [
    id 51
    label "schorzenie"
  ]
  node [
    id 52
    label "wygi&#281;ty"
  ]
  node [
    id 53
    label "sierpowato"
  ]
  node [
    id 54
    label "robi&#263;"
  ]
  node [
    id 55
    label "examine"
  ]
  node [
    id 56
    label "szpiegowa&#263;"
  ]
  node [
    id 57
    label "wy&#322;&#261;czny"
  ]
  node [
    id 58
    label "cz&#322;owiek"
  ]
  node [
    id 59
    label "potomstwo"
  ]
  node [
    id 60
    label "organizm"
  ]
  node [
    id 61
    label "sraluch"
  ]
  node [
    id 62
    label "utulanie"
  ]
  node [
    id 63
    label "pediatra"
  ]
  node [
    id 64
    label "dzieciarnia"
  ]
  node [
    id 65
    label "m&#322;odziak"
  ]
  node [
    id 66
    label "dzieciak"
  ]
  node [
    id 67
    label "utula&#263;"
  ]
  node [
    id 68
    label "potomek"
  ]
  node [
    id 69
    label "pedofil"
  ]
  node [
    id 70
    label "entliczek-pentliczek"
  ]
  node [
    id 71
    label "m&#322;odzik"
  ]
  node [
    id 72
    label "cz&#322;owieczek"
  ]
  node [
    id 73
    label "zwierz&#281;"
  ]
  node [
    id 74
    label "niepe&#322;noletni"
  ]
  node [
    id 75
    label "fledgling"
  ]
  node [
    id 76
    label "utuli&#263;"
  ]
  node [
    id 77
    label "utulenie"
  ]
  node [
    id 78
    label "str&#243;j"
  ]
  node [
    id 79
    label "origin"
  ]
  node [
    id 80
    label "czas"
  ]
  node [
    id 81
    label "geneza"
  ]
  node [
    id 82
    label "beginning"
  ]
  node [
    id 83
    label "wynikanie"
  ]
  node [
    id 84
    label "zaczynanie_si&#281;"
  ]
  node [
    id 85
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 86
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 87
    label "background"
  ]
  node [
    id 88
    label "nieeuropejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 14
    target 88
  ]
]
