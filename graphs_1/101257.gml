graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.064516129032258
  density 0.033844526705446853
  graphCliqueNumber 4
  node [
    id 0
    label "droga"
    origin "text"
  ]
  node [
    id 1
    label "krajowy"
    origin "text"
  ]
  node [
    id 2
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 3
    label "journey"
  ]
  node [
    id 4
    label "podbieg"
  ]
  node [
    id 5
    label "bezsilnikowy"
  ]
  node [
    id 6
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 7
    label "wylot"
  ]
  node [
    id 8
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 9
    label "drogowskaz"
  ]
  node [
    id 10
    label "nawierzchnia"
  ]
  node [
    id 11
    label "turystyka"
  ]
  node [
    id 12
    label "budowla"
  ]
  node [
    id 13
    label "spos&#243;b"
  ]
  node [
    id 14
    label "passage"
  ]
  node [
    id 15
    label "marszrutyzacja"
  ]
  node [
    id 16
    label "zbior&#243;wka"
  ]
  node [
    id 17
    label "ekskursja"
  ]
  node [
    id 18
    label "rajza"
  ]
  node [
    id 19
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 20
    label "ruch"
  ]
  node [
    id 21
    label "trasa"
  ]
  node [
    id 22
    label "wyb&#243;j"
  ]
  node [
    id 23
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 24
    label "ekwipunek"
  ]
  node [
    id 25
    label "korona_drogi"
  ]
  node [
    id 26
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 27
    label "pobocze"
  ]
  node [
    id 28
    label "rodzimy"
  ]
  node [
    id 29
    label "drogi"
  ]
  node [
    id 30
    label "nr"
  ]
  node [
    id 31
    label "44"
  ]
  node [
    id 32
    label "g&#243;rny"
  ]
  node [
    id 33
    label "&#346;l&#261;sk"
  ]
  node [
    id 34
    label "konurbacja"
  ]
  node [
    id 35
    label "katowicki"
  ]
  node [
    id 36
    label "g&#243;rno&#347;l&#261;ski"
  ]
  node [
    id 37
    label "okr&#261;g"
  ]
  node [
    id 38
    label "przemys&#322;owy"
  ]
  node [
    id 39
    label "kompania"
  ]
  node [
    id 40
    label "piwowarski"
  ]
  node [
    id 41
    label "tyski"
  ]
  node [
    id 42
    label "browar"
  ]
  node [
    id 43
    label "ksi&#261;&#380;&#281;cy"
  ]
  node [
    id 44
    label "fiat"
  ]
  node [
    id 45
    label "auto"
  ]
  node [
    id 46
    label "Poland"
  ]
  node [
    id 47
    label "&#347;l&#261;ski"
  ]
  node [
    id 48
    label "gie&#322;da"
  ]
  node [
    id 49
    label "kwiatowy"
  ]
  node [
    id 50
    label "81"
  ]
  node [
    id 51
    label "28"
  ]
  node [
    id 52
    label "1"
  ]
  node [
    id 53
    label "Bierunia"
  ]
  node [
    id 54
    label "stary"
  ]
  node [
    id 55
    label "Tychy"
  ]
  node [
    id 56
    label "wilkowyj"
  ]
  node [
    id 57
    label "Bieru&#324;"
  ]
  node [
    id 58
    label "nowy"
  ]
  node [
    id 59
    label "bielsko"
  ]
  node [
    id 60
    label "bia&#322;y"
  ]
  node [
    id 61
    label "Urbanowic"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 50
  ]
  edge [
    source 29
    target 51
  ]
  edge [
    source 29
    target 52
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 50
  ]
  edge [
    source 30
    target 51
  ]
  edge [
    source 30
    target 52
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 58
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 61
  ]
  edge [
    source 59
    target 60
  ]
]
