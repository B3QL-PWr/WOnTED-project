graph [
  maxDegree 25
  minDegree 1
  meanDegree 2.073170731707317
  density 0.025594700391448358
  graphCliqueNumber 2
  node [
    id 0
    label "op&#322;ata"
    origin "text"
  ]
  node [
    id 1
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 3
    label "wrzuci&#263;"
    origin "text"
  ]
  node [
    id 4
    label "koszt"
    origin "text"
  ]
  node [
    id 5
    label "nawet"
    origin "text"
  ]
  node [
    id 6
    label "gdy"
    origin "text"
  ]
  node [
    id 7
    label "lokal"
    origin "text"
  ]
  node [
    id 8
    label "wyodr&#281;bni&#263;"
    origin "text"
  ]
  node [
    id 9
    label "specjalny"
    origin "text"
  ]
  node [
    id 10
    label "pomieszczenie"
    origin "text"
  ]
  node [
    id 11
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 12
    label "praca"
    origin "text"
  ]
  node [
    id 13
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 14
    label "kwota"
  ]
  node [
    id 15
    label "stanie"
  ]
  node [
    id 16
    label "przebywanie"
  ]
  node [
    id 17
    label "panowanie"
  ]
  node [
    id 18
    label "zajmowanie"
  ]
  node [
    id 19
    label "pomieszkanie"
  ]
  node [
    id 20
    label "adjustment"
  ]
  node [
    id 21
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 22
    label "kwadrat"
  ]
  node [
    id 23
    label "animation"
  ]
  node [
    id 24
    label "dom"
  ]
  node [
    id 25
    label "free"
  ]
  node [
    id 26
    label "insert"
  ]
  node [
    id 27
    label "umie&#347;ci&#263;"
  ]
  node [
    id 28
    label "nak&#322;ad"
  ]
  node [
    id 29
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 30
    label "sumpt"
  ]
  node [
    id 31
    label "wydatek"
  ]
  node [
    id 32
    label "gastronomia"
  ]
  node [
    id 33
    label "miejsce"
  ]
  node [
    id 34
    label "zak&#322;ad"
  ]
  node [
    id 35
    label "wyznaczy&#263;"
  ]
  node [
    id 36
    label "signalize"
  ]
  node [
    id 37
    label "oddzieli&#263;"
  ]
  node [
    id 38
    label "wykroi&#263;"
  ]
  node [
    id 39
    label "specjalnie"
  ]
  node [
    id 40
    label "nieetatowy"
  ]
  node [
    id 41
    label "intencjonalny"
  ]
  node [
    id 42
    label "szczeg&#243;lny"
  ]
  node [
    id 43
    label "odpowiedni"
  ]
  node [
    id 44
    label "niedorozw&#243;j"
  ]
  node [
    id 45
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 46
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 47
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 48
    label "nienormalny"
  ]
  node [
    id 49
    label "umy&#347;lnie"
  ]
  node [
    id 50
    label "zakamarek"
  ]
  node [
    id 51
    label "udost&#281;pnienie"
  ]
  node [
    id 52
    label "sufit"
  ]
  node [
    id 53
    label "apartment"
  ]
  node [
    id 54
    label "pod&#322;oga"
  ]
  node [
    id 55
    label "sklepienie"
  ]
  node [
    id 56
    label "front"
  ]
  node [
    id 57
    label "amfilada"
  ]
  node [
    id 58
    label "umieszczenie"
  ]
  node [
    id 59
    label "wy&#322;&#261;czny"
  ]
  node [
    id 60
    label "stosunek_pracy"
  ]
  node [
    id 61
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 62
    label "benedykty&#324;ski"
  ]
  node [
    id 63
    label "pracowanie"
  ]
  node [
    id 64
    label "zaw&#243;d"
  ]
  node [
    id 65
    label "kierownictwo"
  ]
  node [
    id 66
    label "zmiana"
  ]
  node [
    id 67
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 68
    label "wytw&#243;r"
  ]
  node [
    id 69
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 70
    label "tynkarski"
  ]
  node [
    id 71
    label "czynnik_produkcji"
  ]
  node [
    id 72
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 73
    label "zobowi&#261;zanie"
  ]
  node [
    id 74
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 75
    label "czynno&#347;&#263;"
  ]
  node [
    id 76
    label "tyrka"
  ]
  node [
    id 77
    label "pracowa&#263;"
  ]
  node [
    id 78
    label "siedziba"
  ]
  node [
    id 79
    label "poda&#380;_pracy"
  ]
  node [
    id 80
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 81
    label "najem"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
]
