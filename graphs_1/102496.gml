graph [
  maxDegree 63
  minDegree 1
  meanDegree 2.022222222222222
  density 0.011297330850403476
  graphCliqueNumber 2
  node [
    id 0
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 1
    label "oficjalnie"
    origin "text"
  ]
  node [
    id 2
    label "gra"
    origin "text"
  ]
  node [
    id 3
    label "sztab"
    origin "text"
  ]
  node [
    id 4
    label "xiv"
    origin "text"
  ]
  node [
    id 5
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 6
    label "wo&#347;p"
    origin "text"
  ]
  node [
    id 7
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 8
    label "lista"
    origin "text"
  ]
  node [
    id 9
    label "serdecznie"
    origin "text"
  ]
  node [
    id 10
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 12
    label "wszystek"
    origin "text"
  ]
  node [
    id 13
    label "osoba"
    origin "text"
  ]
  node [
    id 14
    label "wielki"
    origin "text"
  ]
  node [
    id 15
    label "serce"
    origin "text"
  ]
  node [
    id 16
    label "doba"
  ]
  node [
    id 17
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 18
    label "dzi&#347;"
  ]
  node [
    id 19
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 20
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 21
    label "regularly"
  ]
  node [
    id 22
    label "legalnie"
  ]
  node [
    id 23
    label "oficjalny"
  ]
  node [
    id 24
    label "pow&#347;ci&#261;gliwie"
  ]
  node [
    id 25
    label "formalny"
  ]
  node [
    id 26
    label "zabawa"
  ]
  node [
    id 27
    label "rywalizacja"
  ]
  node [
    id 28
    label "czynno&#347;&#263;"
  ]
  node [
    id 29
    label "Pok&#233;mon"
  ]
  node [
    id 30
    label "synteza"
  ]
  node [
    id 31
    label "odtworzenie"
  ]
  node [
    id 32
    label "komplet"
  ]
  node [
    id 33
    label "rekwizyt_do_gry"
  ]
  node [
    id 34
    label "odg&#322;os"
  ]
  node [
    id 35
    label "rozgrywka"
  ]
  node [
    id 36
    label "post&#281;powanie"
  ]
  node [
    id 37
    label "wydarzenie"
  ]
  node [
    id 38
    label "apparent_motion"
  ]
  node [
    id 39
    label "game"
  ]
  node [
    id 40
    label "zmienno&#347;&#263;"
  ]
  node [
    id 41
    label "zasada"
  ]
  node [
    id 42
    label "akcja"
  ]
  node [
    id 43
    label "play"
  ]
  node [
    id 44
    label "contest"
  ]
  node [
    id 45
    label "zbijany"
  ]
  node [
    id 46
    label "zast&#281;p"
  ]
  node [
    id 47
    label "dow&#243;dztwo"
  ]
  node [
    id 48
    label "siedziba"
  ]
  node [
    id 49
    label "centrala"
  ]
  node [
    id 50
    label "koniec"
  ]
  node [
    id 51
    label "conclusion"
  ]
  node [
    id 52
    label "coating"
  ]
  node [
    id 53
    label "runda"
  ]
  node [
    id 54
    label "wyliczanka"
  ]
  node [
    id 55
    label "catalog"
  ]
  node [
    id 56
    label "stock"
  ]
  node [
    id 57
    label "figurowa&#263;"
  ]
  node [
    id 58
    label "zbi&#243;r"
  ]
  node [
    id 59
    label "book"
  ]
  node [
    id 60
    label "pozycja"
  ]
  node [
    id 61
    label "tekst"
  ]
  node [
    id 62
    label "sumariusz"
  ]
  node [
    id 63
    label "siarczy&#347;cie"
  ]
  node [
    id 64
    label "serdeczny"
  ]
  node [
    id 65
    label "szczerze"
  ]
  node [
    id 66
    label "mi&#322;o"
  ]
  node [
    id 67
    label "invite"
  ]
  node [
    id 68
    label "ask"
  ]
  node [
    id 69
    label "oferowa&#263;"
  ]
  node [
    id 70
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 71
    label "wsp&#243;&#322;pracownictwo"
  ]
  node [
    id 72
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 73
    label "ca&#322;y"
  ]
  node [
    id 74
    label "Zgredek"
  ]
  node [
    id 75
    label "kategoria_gramatyczna"
  ]
  node [
    id 76
    label "Casanova"
  ]
  node [
    id 77
    label "Don_Juan"
  ]
  node [
    id 78
    label "Gargantua"
  ]
  node [
    id 79
    label "Faust"
  ]
  node [
    id 80
    label "profanum"
  ]
  node [
    id 81
    label "Chocho&#322;"
  ]
  node [
    id 82
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 83
    label "koniugacja"
  ]
  node [
    id 84
    label "Winnetou"
  ]
  node [
    id 85
    label "Dwukwiat"
  ]
  node [
    id 86
    label "homo_sapiens"
  ]
  node [
    id 87
    label "Edyp"
  ]
  node [
    id 88
    label "Herkules_Poirot"
  ]
  node [
    id 89
    label "ludzko&#347;&#263;"
  ]
  node [
    id 90
    label "mikrokosmos"
  ]
  node [
    id 91
    label "person"
  ]
  node [
    id 92
    label "Sherlock_Holmes"
  ]
  node [
    id 93
    label "portrecista"
  ]
  node [
    id 94
    label "Szwejk"
  ]
  node [
    id 95
    label "Hamlet"
  ]
  node [
    id 96
    label "duch"
  ]
  node [
    id 97
    label "g&#322;owa"
  ]
  node [
    id 98
    label "oddzia&#322;ywanie"
  ]
  node [
    id 99
    label "Quasimodo"
  ]
  node [
    id 100
    label "Dulcynea"
  ]
  node [
    id 101
    label "Don_Kiszot"
  ]
  node [
    id 102
    label "Wallenrod"
  ]
  node [
    id 103
    label "Plastu&#347;"
  ]
  node [
    id 104
    label "Harry_Potter"
  ]
  node [
    id 105
    label "figura"
  ]
  node [
    id 106
    label "parali&#380;owa&#263;"
  ]
  node [
    id 107
    label "istota"
  ]
  node [
    id 108
    label "Werter"
  ]
  node [
    id 109
    label "antropochoria"
  ]
  node [
    id 110
    label "posta&#263;"
  ]
  node [
    id 111
    label "dupny"
  ]
  node [
    id 112
    label "wysoce"
  ]
  node [
    id 113
    label "wyj&#261;tkowy"
  ]
  node [
    id 114
    label "wybitny"
  ]
  node [
    id 115
    label "znaczny"
  ]
  node [
    id 116
    label "prawdziwy"
  ]
  node [
    id 117
    label "wa&#380;ny"
  ]
  node [
    id 118
    label "nieprzeci&#281;tny"
  ]
  node [
    id 119
    label "cz&#322;owiek"
  ]
  node [
    id 120
    label "courage"
  ]
  node [
    id 121
    label "mi&#281;sie&#324;"
  ]
  node [
    id 122
    label "kompleks"
  ]
  node [
    id 123
    label "sfera_afektywna"
  ]
  node [
    id 124
    label "heart"
  ]
  node [
    id 125
    label "sumienie"
  ]
  node [
    id 126
    label "przedsionek"
  ]
  node [
    id 127
    label "entity"
  ]
  node [
    id 128
    label "nastawienie"
  ]
  node [
    id 129
    label "punkt"
  ]
  node [
    id 130
    label "kompleksja"
  ]
  node [
    id 131
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 132
    label "kier"
  ]
  node [
    id 133
    label "systol"
  ]
  node [
    id 134
    label "pikawa"
  ]
  node [
    id 135
    label "power"
  ]
  node [
    id 136
    label "zastawka"
  ]
  node [
    id 137
    label "psychika"
  ]
  node [
    id 138
    label "wola"
  ]
  node [
    id 139
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 140
    label "komora"
  ]
  node [
    id 141
    label "zapalno&#347;&#263;"
  ]
  node [
    id 142
    label "wsierdzie"
  ]
  node [
    id 143
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 144
    label "podekscytowanie"
  ]
  node [
    id 145
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 146
    label "strunowiec"
  ]
  node [
    id 147
    label "favor"
  ]
  node [
    id 148
    label "dusza"
  ]
  node [
    id 149
    label "fizjonomia"
  ]
  node [
    id 150
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 151
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 152
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 153
    label "charakter"
  ]
  node [
    id 154
    label "dzwon"
  ]
  node [
    id 155
    label "koniuszek_serca"
  ]
  node [
    id 156
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 157
    label "passion"
  ]
  node [
    id 158
    label "cecha"
  ]
  node [
    id 159
    label "pulsowa&#263;"
  ]
  node [
    id 160
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 161
    label "organ"
  ]
  node [
    id 162
    label "ego"
  ]
  node [
    id 163
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 164
    label "kardiografia"
  ]
  node [
    id 165
    label "osobowo&#347;&#263;"
  ]
  node [
    id 166
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 167
    label "kszta&#322;t"
  ]
  node [
    id 168
    label "karta"
  ]
  node [
    id 169
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 170
    label "dobro&#263;"
  ]
  node [
    id 171
    label "podroby"
  ]
  node [
    id 172
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 173
    label "pulsowanie"
  ]
  node [
    id 174
    label "deformowa&#263;"
  ]
  node [
    id 175
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 176
    label "seksualno&#347;&#263;"
  ]
  node [
    id 177
    label "deformowanie"
  ]
  node [
    id 178
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 179
    label "elektrokardiografia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
]
