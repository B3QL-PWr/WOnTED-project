graph [
  maxDegree 25
  minDegree 1
  meanDegree 2.382978723404255
  density 0.05180388529139685
  graphCliqueNumber 6
  node [
    id 0
    label "test"
    origin "text"
  ]
  node [
    id 1
    label "silnik"
    origin "text"
  ]
  node [
    id 2
    label "odrzutowy"
    origin "text"
  ]
  node [
    id 3
    label "eagle"
    origin "text"
  ]
  node [
    id 4
    label "fighting"
    origin "text"
  ]
  node [
    id 5
    label "jet"
    origin "text"
  ]
  node [
    id 6
    label "do&#347;wiadczenie"
  ]
  node [
    id 7
    label "arkusz"
  ]
  node [
    id 8
    label "sprawdzian"
  ]
  node [
    id 9
    label "quiz"
  ]
  node [
    id 10
    label "przechodzi&#263;"
  ]
  node [
    id 11
    label "przechodzenie"
  ]
  node [
    id 12
    label "badanie"
  ]
  node [
    id 13
    label "narz&#281;dzie"
  ]
  node [
    id 14
    label "sytuacja"
  ]
  node [
    id 15
    label "dotarcie"
  ]
  node [
    id 16
    label "wyci&#261;garka"
  ]
  node [
    id 17
    label "biblioteka"
  ]
  node [
    id 18
    label "aerosanie"
  ]
  node [
    id 19
    label "podgrzewacz"
  ]
  node [
    id 20
    label "bombowiec"
  ]
  node [
    id 21
    label "dociera&#263;"
  ]
  node [
    id 22
    label "gniazdo_zaworowe"
  ]
  node [
    id 23
    label "motor&#243;wka"
  ]
  node [
    id 24
    label "nap&#281;d"
  ]
  node [
    id 25
    label "perpetuum_mobile"
  ]
  node [
    id 26
    label "rz&#281;&#380;enie"
  ]
  node [
    id 27
    label "mechanizm"
  ]
  node [
    id 28
    label "gondola_silnikowa"
  ]
  node [
    id 29
    label "docieranie"
  ]
  node [
    id 30
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 31
    label "rz&#281;zi&#263;"
  ]
  node [
    id 32
    label "motoszybowiec"
  ]
  node [
    id 33
    label "motogodzina"
  ]
  node [
    id 34
    label "samoch&#243;d"
  ]
  node [
    id 35
    label "dotrze&#263;"
  ]
  node [
    id 36
    label "radiator"
  ]
  node [
    id 37
    label "program"
  ]
  node [
    id 38
    label "ultraszybki"
  ]
  node [
    id 39
    label "technologiczny"
  ]
  node [
    id 40
    label "silnikowy"
  ]
  node [
    id 41
    label "silnik_odrzutowy"
  ]
  node [
    id 42
    label "F"
  ]
  node [
    id 43
    label "15"
  ]
  node [
    id 44
    label "Eagle"
  ]
  node [
    id 45
    label "Fighting"
  ]
  node [
    id 46
    label "Jet"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 45
    target 46
  ]
]
