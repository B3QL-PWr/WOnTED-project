graph [
  maxDegree 38
  minDegree 1
  meanDegree 1.981818181818182
  density 0.01818181818181818
  graphCliqueNumber 2
  node [
    id 0
    label "felek"
    origin "text"
  ]
  node [
    id 1
    label "zawo&#322;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "matka"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;aby"
    origin "text"
  ]
  node [
    id 4
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 5
    label "&#322;&#243;&#380;ko"
    origin "text"
  ]
  node [
    id 6
    label "cry"
  ]
  node [
    id 7
    label "powiedzie&#263;"
  ]
  node [
    id 8
    label "invite"
  ]
  node [
    id 9
    label "krzykn&#261;&#263;"
  ]
  node [
    id 10
    label "przewo&#322;a&#263;"
  ]
  node [
    id 11
    label "nakaza&#263;"
  ]
  node [
    id 12
    label "Matka_Boska"
  ]
  node [
    id 13
    label "matka_zast&#281;pcza"
  ]
  node [
    id 14
    label "stara"
  ]
  node [
    id 15
    label "rodzic"
  ]
  node [
    id 16
    label "matczysko"
  ]
  node [
    id 17
    label "ro&#347;lina"
  ]
  node [
    id 18
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 19
    label "gracz"
  ]
  node [
    id 20
    label "zawodnik"
  ]
  node [
    id 21
    label "macierz"
  ]
  node [
    id 22
    label "owad"
  ]
  node [
    id 23
    label "przyczyna"
  ]
  node [
    id 24
    label "macocha"
  ]
  node [
    id 25
    label "dwa_ognie"
  ]
  node [
    id 26
    label "staruszka"
  ]
  node [
    id 27
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 28
    label "rozsadnik"
  ]
  node [
    id 29
    label "zakonnica"
  ]
  node [
    id 30
    label "obiekt"
  ]
  node [
    id 31
    label "samica"
  ]
  node [
    id 32
    label "przodkini"
  ]
  node [
    id 33
    label "rodzice"
  ]
  node [
    id 34
    label "nieznaczny"
  ]
  node [
    id 35
    label "nieumiej&#281;tny"
  ]
  node [
    id 36
    label "marnie"
  ]
  node [
    id 37
    label "md&#322;y"
  ]
  node [
    id 38
    label "przemijaj&#261;cy"
  ]
  node [
    id 39
    label "zawodny"
  ]
  node [
    id 40
    label "delikatny"
  ]
  node [
    id 41
    label "&#322;agodny"
  ]
  node [
    id 42
    label "niedoskona&#322;y"
  ]
  node [
    id 43
    label "nietrwa&#322;y"
  ]
  node [
    id 44
    label "po&#347;ledni"
  ]
  node [
    id 45
    label "s&#322;abowity"
  ]
  node [
    id 46
    label "niefajny"
  ]
  node [
    id 47
    label "z&#322;y"
  ]
  node [
    id 48
    label "niemocny"
  ]
  node [
    id 49
    label "kiepsko"
  ]
  node [
    id 50
    label "niezdrowy"
  ]
  node [
    id 51
    label "lura"
  ]
  node [
    id 52
    label "s&#322;abo"
  ]
  node [
    id 53
    label "nieudany"
  ]
  node [
    id 54
    label "mizerny"
  ]
  node [
    id 55
    label "opinion"
  ]
  node [
    id 56
    label "wypowied&#378;"
  ]
  node [
    id 57
    label "zmatowienie"
  ]
  node [
    id 58
    label "wpa&#347;&#263;"
  ]
  node [
    id 59
    label "grupa"
  ]
  node [
    id 60
    label "wokal"
  ]
  node [
    id 61
    label "note"
  ]
  node [
    id 62
    label "wydawa&#263;"
  ]
  node [
    id 63
    label "nakaz"
  ]
  node [
    id 64
    label "regestr"
  ]
  node [
    id 65
    label "&#347;piewak_operowy"
  ]
  node [
    id 66
    label "matowie&#263;"
  ]
  node [
    id 67
    label "wpada&#263;"
  ]
  node [
    id 68
    label "stanowisko"
  ]
  node [
    id 69
    label "zjawisko"
  ]
  node [
    id 70
    label "mutacja"
  ]
  node [
    id 71
    label "partia"
  ]
  node [
    id 72
    label "&#347;piewak"
  ]
  node [
    id 73
    label "emisja"
  ]
  node [
    id 74
    label "brzmienie"
  ]
  node [
    id 75
    label "zmatowie&#263;"
  ]
  node [
    id 76
    label "wydanie"
  ]
  node [
    id 77
    label "zesp&#243;&#322;"
  ]
  node [
    id 78
    label "wyda&#263;"
  ]
  node [
    id 79
    label "zdolno&#347;&#263;"
  ]
  node [
    id 80
    label "decyzja"
  ]
  node [
    id 81
    label "wpadni&#281;cie"
  ]
  node [
    id 82
    label "linia_melodyczna"
  ]
  node [
    id 83
    label "wpadanie"
  ]
  node [
    id 84
    label "onomatopeja"
  ]
  node [
    id 85
    label "sound"
  ]
  node [
    id 86
    label "matowienie"
  ]
  node [
    id 87
    label "ch&#243;rzysta"
  ]
  node [
    id 88
    label "d&#378;wi&#281;k"
  ]
  node [
    id 89
    label "foniatra"
  ]
  node [
    id 90
    label "&#347;piewaczka"
  ]
  node [
    id 91
    label "zag&#322;&#243;wek"
  ]
  node [
    id 92
    label "dopasowanie_seksualne"
  ]
  node [
    id 93
    label "promiskuityzm"
  ]
  node [
    id 94
    label "roz&#347;cielenie"
  ]
  node [
    id 95
    label "niedopasowanie_seksualne"
  ]
  node [
    id 96
    label "roz&#347;cieli&#263;"
  ]
  node [
    id 97
    label "mebel"
  ]
  node [
    id 98
    label "zas&#322;a&#263;"
  ]
  node [
    id 99
    label "s&#322;anie"
  ]
  node [
    id 100
    label "roz&#347;cie&#322;anie"
  ]
  node [
    id 101
    label "s&#322;a&#263;"
  ]
  node [
    id 102
    label "rama_&#322;&#243;&#380;ka"
  ]
  node [
    id 103
    label "petting"
  ]
  node [
    id 104
    label "wyrko"
  ]
  node [
    id 105
    label "sexual_activity"
  ]
  node [
    id 106
    label "wezg&#322;owie"
  ]
  node [
    id 107
    label "zas&#322;anie"
  ]
  node [
    id 108
    label "roz&#347;cie&#322;a&#263;"
  ]
  node [
    id 109
    label "materac"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
]
