graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "zabawa"
    origin "text"
  ]
  node [
    id 1
    label "blokowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wodzirej"
  ]
  node [
    id 3
    label "rozrywka"
  ]
  node [
    id 4
    label "nabawienie_si&#281;"
  ]
  node [
    id 5
    label "cecha"
  ]
  node [
    id 6
    label "gambling"
  ]
  node [
    id 7
    label "taniec"
  ]
  node [
    id 8
    label "impreza"
  ]
  node [
    id 9
    label "igraszka"
  ]
  node [
    id 10
    label "igra"
  ]
  node [
    id 11
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 12
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 13
    label "game"
  ]
  node [
    id 14
    label "ubaw"
  ]
  node [
    id 15
    label "chwyt"
  ]
  node [
    id 16
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 17
    label "throng"
  ]
  node [
    id 18
    label "zajmowa&#263;"
  ]
  node [
    id 19
    label "przeszkadza&#263;"
  ]
  node [
    id 20
    label "zablokowywa&#263;"
  ]
  node [
    id 21
    label "sk&#322;ada&#263;"
  ]
  node [
    id 22
    label "walczy&#263;"
  ]
  node [
    id 23
    label "interlock"
  ]
  node [
    id 24
    label "wstrzymywa&#263;"
  ]
  node [
    id 25
    label "unieruchamia&#263;"
  ]
  node [
    id 26
    label "kiblowa&#263;"
  ]
  node [
    id 27
    label "zatrzymywa&#263;"
  ]
  node [
    id 28
    label "parry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
]
