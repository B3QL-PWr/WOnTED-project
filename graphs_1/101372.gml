graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "retencja"
    origin "text"
  ]
  node [
    id 1
    label "wodny"
    origin "text"
  ]
  node [
    id 2
    label "prawo"
  ]
  node [
    id 3
    label "retentiveness"
  ]
  node [
    id 4
    label "zapas"
  ]
  node [
    id 5
    label "retention"
  ]
  node [
    id 6
    label "wierzyciel"
  ]
  node [
    id 7
    label "specjalny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 7
  ]
]
