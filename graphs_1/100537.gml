graph [
  maxDegree 1
  minDegree 0
  meanDegree 0.6666666666666666
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "framlingham"
    origin "text"
  ]
  node [
    id 1
    label "wielki"
  ]
  node [
    id 2
    label "brytania"
  ]
  edge [
    source 1
    target 2
  ]
]
