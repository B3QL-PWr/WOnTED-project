graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9770114942528736
  density 0.022988505747126436
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "tak"
    origin "text"
  ]
  node [
    id 2
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 3
    label "asertywny"
    origin "text"
  ]
  node [
    id 4
    label "zawsze"
    origin "text"
  ]
  node [
    id 5
    label "klika&#263;"
    origin "text"
  ]
  node [
    id 6
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 7
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 8
    label "zsmiast"
    origin "text"
  ]
  node [
    id 9
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pro&#347;ba"
    origin "text"
  ]
  node [
    id 11
    label "ocena"
    origin "text"
  ]
  node [
    id 12
    label "aplikacja"
    origin "text"
  ]
  node [
    id 13
    label "mimo"
    origin "text"
  ]
  node [
    id 14
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 15
    label "nigdy"
    origin "text"
  ]
  node [
    id 16
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 17
    label "si&#281;ga&#263;"
  ]
  node [
    id 18
    label "trwa&#263;"
  ]
  node [
    id 19
    label "obecno&#347;&#263;"
  ]
  node [
    id 20
    label "stan"
  ]
  node [
    id 21
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 22
    label "stand"
  ]
  node [
    id 23
    label "mie&#263;_miejsce"
  ]
  node [
    id 24
    label "uczestniczy&#263;"
  ]
  node [
    id 25
    label "chodzi&#263;"
  ]
  node [
    id 26
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 27
    label "equal"
  ]
  node [
    id 28
    label "mo&#380;liwie"
  ]
  node [
    id 29
    label "nieznaczny"
  ]
  node [
    id 30
    label "kr&#243;tko"
  ]
  node [
    id 31
    label "nieistotnie"
  ]
  node [
    id 32
    label "nieliczny"
  ]
  node [
    id 33
    label "mikroskopijnie"
  ]
  node [
    id 34
    label "pomiernie"
  ]
  node [
    id 35
    label "ma&#322;y"
  ]
  node [
    id 36
    label "assertive"
  ]
  node [
    id 37
    label "asertywnie"
  ]
  node [
    id 38
    label "stanowczy"
  ]
  node [
    id 39
    label "konstruktywny"
  ]
  node [
    id 40
    label "zaw&#380;dy"
  ]
  node [
    id 41
    label "ci&#261;gle"
  ]
  node [
    id 42
    label "na_zawsze"
  ]
  node [
    id 43
    label "cz&#281;sto"
  ]
  node [
    id 44
    label "naciska&#263;"
  ]
  node [
    id 45
    label "wybiera&#263;"
  ]
  node [
    id 46
    label "buton"
  ]
  node [
    id 47
    label "p&#243;&#378;ny"
  ]
  node [
    id 48
    label "odmawia&#263;"
  ]
  node [
    id 49
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 50
    label "sk&#322;ada&#263;"
  ]
  node [
    id 51
    label "thank"
  ]
  node [
    id 52
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 53
    label "wyra&#380;a&#263;"
  ]
  node [
    id 54
    label "etykieta"
  ]
  node [
    id 55
    label "solicitation"
  ]
  node [
    id 56
    label "wypowied&#378;"
  ]
  node [
    id 57
    label "informacja"
  ]
  node [
    id 58
    label "sofcik"
  ]
  node [
    id 59
    label "appraisal"
  ]
  node [
    id 60
    label "decyzja"
  ]
  node [
    id 61
    label "pogl&#261;d"
  ]
  node [
    id 62
    label "kryterium"
  ]
  node [
    id 63
    label "application"
  ]
  node [
    id 64
    label "ozdoba"
  ]
  node [
    id 65
    label "program"
  ]
  node [
    id 66
    label "naszycie"
  ]
  node [
    id 67
    label "pismo"
  ]
  node [
    id 68
    label "wz&#243;r"
  ]
  node [
    id 69
    label "praktyka"
  ]
  node [
    id 70
    label "applique"
  ]
  node [
    id 71
    label "cognizance"
  ]
  node [
    id 72
    label "kompletnie"
  ]
  node [
    id 73
    label "zorganizowa&#263;"
  ]
  node [
    id 74
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 75
    label "przerobi&#263;"
  ]
  node [
    id 76
    label "wystylizowa&#263;"
  ]
  node [
    id 77
    label "cause"
  ]
  node [
    id 78
    label "wydali&#263;"
  ]
  node [
    id 79
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 80
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 81
    label "post&#261;pi&#263;"
  ]
  node [
    id 82
    label "appoint"
  ]
  node [
    id 83
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 84
    label "nabra&#263;"
  ]
  node [
    id 85
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 86
    label "make"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 71
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 72
  ]
  edge [
    source 16
    target 73
  ]
  edge [
    source 16
    target 74
  ]
  edge [
    source 16
    target 75
  ]
  edge [
    source 16
    target 76
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 16
    target 79
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 86
  ]
]
