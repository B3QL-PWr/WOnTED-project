graph [
  maxDegree 15
  minDegree 1
  meanDegree 2
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "w&#261;tpliwy"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "autor"
    origin "text"
  ]
  node [
    id 4
    label "pseudo"
    origin "text"
  ]
  node [
    id 5
    label "graffiti"
    origin "text"
  ]
  node [
    id 6
    label "polak"
    origin "text"
  ]
  node [
    id 7
    label "pozorny"
  ]
  node [
    id 8
    label "w&#261;tpliwie"
  ]
  node [
    id 9
    label "si&#281;ga&#263;"
  ]
  node [
    id 10
    label "trwa&#263;"
  ]
  node [
    id 11
    label "obecno&#347;&#263;"
  ]
  node [
    id 12
    label "stan"
  ]
  node [
    id 13
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 14
    label "stand"
  ]
  node [
    id 15
    label "mie&#263;_miejsce"
  ]
  node [
    id 16
    label "uczestniczy&#263;"
  ]
  node [
    id 17
    label "chodzi&#263;"
  ]
  node [
    id 18
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 19
    label "equal"
  ]
  node [
    id 20
    label "pomys&#322;odawca"
  ]
  node [
    id 21
    label "kszta&#322;ciciel"
  ]
  node [
    id 22
    label "tworzyciel"
  ]
  node [
    id 23
    label "&#347;w"
  ]
  node [
    id 24
    label "wykonawca"
  ]
  node [
    id 25
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 26
    label "mural"
  ]
  node [
    id 27
    label "graffito"
  ]
  node [
    id 28
    label "malowid&#322;o"
  ]
  node [
    id 29
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 6
    target 29
  ]
]
