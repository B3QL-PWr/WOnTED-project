graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.3852813852813854
  density 0.0051741461719769745
  graphCliqueNumber 6
  node [
    id 0
    label "zaleta"
    origin "text"
  ]
  node [
    id 1
    label "quanty"
    origin "text"
  ]
  node [
    id 2
    label "plus"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "mo&#380;liwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wykorzystanie"
    origin "text"
  ]
  node [
    id 6
    label "tagowania"
    origin "text"
  ]
  node [
    id 7
    label "toolbar&#243;w"
    origin "text"
  ]
  node [
    id 8
    label "pasek"
    origin "text"
  ]
  node [
    id 9
    label "narz&#281;dziowy"
    origin "text"
  ]
  node [
    id 10
    label "przycisk"
    origin "text"
  ]
  node [
    id 11
    label "tag"
    origin "text"
  ]
  node [
    id 12
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 13
    label "tym"
    origin "text"
  ]
  node [
    id 14
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 15
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "myszka"
    origin "text"
  ]
  node [
    id 17
    label "zaznaczy&#263;"
    origin "text"
  ]
  node [
    id 18
    label "fragment"
    origin "text"
  ]
  node [
    id 19
    label "tekst"
    origin "text"
  ]
  node [
    id 20
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 21
    label "klikn&#261;&#263;"
    origin "text"
  ]
  node [
    id 22
    label "kt&#243;ry&#347;"
    origin "text"
  ]
  node [
    id 23
    label "oznaczony"
    origin "text"
  ]
  node [
    id 24
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 25
    label "odpowiedni"
    origin "text"
  ]
  node [
    id 26
    label "para"
    origin "text"
  ]
  node [
    id 27
    label "otwiera&#263;"
    origin "text"
  ]
  node [
    id 28
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 29
    label "si&#281;"
    origin "text"
  ]
  node [
    id 30
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 31
    label "zaznaczenie"
    origin "text"
  ]
  node [
    id 32
    label "ko&#324;cowy"
    origin "text"
  ]
  node [
    id 33
    label "koniec"
    origin "text"
  ]
  node [
    id 34
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 35
    label "bez"
    origin "text"
  ]
  node [
    id 36
    label "uprzedni"
    origin "text"
  ]
  node [
    id 37
    label "partia"
    origin "text"
  ]
  node [
    id 38
    label "par"
    origin "text"
  ]
  node [
    id 39
    label "miejsce"
    origin "text"
  ]
  node [
    id 40
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 41
    label "kursor"
    origin "text"
  ]
  node [
    id 42
    label "por&#243;wnanie"
    origin "text"
  ]
  node [
    id 43
    label "r&#281;czny"
    origin "text"
  ]
  node [
    id 44
    label "wpisywa&#263;"
    origin "text"
  ]
  node [
    id 45
    label "metoda"
    origin "text"
  ]
  node [
    id 46
    label "taka"
    origin "text"
  ]
  node [
    id 47
    label "zwi&#281;ksza&#263;"
    origin "text"
  ]
  node [
    id 48
    label "szybko&#347;&#263;"
    origin "text"
  ]
  node [
    id 49
    label "zmniejsza&#263;"
    origin "text"
  ]
  node [
    id 50
    label "te&#380;"
    origin "text"
  ]
  node [
    id 51
    label "ryzyka"
    origin "text"
  ]
  node [
    id 52
    label "pope&#322;nie&#263;"
    origin "text"
  ]
  node [
    id 53
    label "b&#322;&#261;d"
    origin "text"
  ]
  node [
    id 54
    label "wstawia&#263;"
    origin "text"
  ]
  node [
    id 55
    label "przez"
    origin "text"
  ]
  node [
    id 56
    label "program"
    origin "text"
  ]
  node [
    id 57
    label "zawiera&#263;"
    origin "text"
  ]
  node [
    id 58
    label "liter&#243;wka"
    origin "text"
  ]
  node [
    id 59
    label "taki"
    origin "text"
  ]
  node [
    id 60
    label "przypadek"
    origin "text"
  ]
  node [
    id 61
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 62
    label "utw&#243;r"
    origin "text"
  ]
  node [
    id 63
    label "duha"
    origin "text"
  ]
  node [
    id 64
    label "warto&#347;&#263;"
  ]
  node [
    id 65
    label "wabik"
  ]
  node [
    id 66
    label "rewaluowa&#263;"
  ]
  node [
    id 67
    label "korzy&#347;&#263;"
  ]
  node [
    id 68
    label "rewaluowanie"
  ]
  node [
    id 69
    label "zrewaluowa&#263;"
  ]
  node [
    id 70
    label "strona"
  ]
  node [
    id 71
    label "zrewaluowanie"
  ]
  node [
    id 72
    label "dodawanie"
  ]
  node [
    id 73
    label "stopie&#324;"
  ]
  node [
    id 74
    label "ocena"
  ]
  node [
    id 75
    label "liczba"
  ]
  node [
    id 76
    label "znak_matematyczny"
  ]
  node [
    id 77
    label "si&#281;ga&#263;"
  ]
  node [
    id 78
    label "trwa&#263;"
  ]
  node [
    id 79
    label "obecno&#347;&#263;"
  ]
  node [
    id 80
    label "stan"
  ]
  node [
    id 81
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 82
    label "stand"
  ]
  node [
    id 83
    label "mie&#263;_miejsce"
  ]
  node [
    id 84
    label "uczestniczy&#263;"
  ]
  node [
    id 85
    label "chodzi&#263;"
  ]
  node [
    id 86
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 87
    label "equal"
  ]
  node [
    id 88
    label "ability"
  ]
  node [
    id 89
    label "wyb&#243;r"
  ]
  node [
    id 90
    label "prospect"
  ]
  node [
    id 91
    label "egzekutywa"
  ]
  node [
    id 92
    label "alternatywa"
  ]
  node [
    id 93
    label "potencja&#322;"
  ]
  node [
    id 94
    label "cecha"
  ]
  node [
    id 95
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 96
    label "obliczeniowo"
  ]
  node [
    id 97
    label "wydarzenie"
  ]
  node [
    id 98
    label "operator_modalny"
  ]
  node [
    id 99
    label "posiada&#263;"
  ]
  node [
    id 100
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 101
    label "zrobienie"
  ]
  node [
    id 102
    label "use"
  ]
  node [
    id 103
    label "u&#380;ycie"
  ]
  node [
    id 104
    label "stosowanie"
  ]
  node [
    id 105
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 106
    label "u&#380;yteczny"
  ]
  node [
    id 107
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 108
    label "exploitation"
  ]
  node [
    id 109
    label "zwi&#261;zek"
  ]
  node [
    id 110
    label "dyktando"
  ]
  node [
    id 111
    label "dodatek"
  ]
  node [
    id 112
    label "obiekt"
  ]
  node [
    id 113
    label "oznaka"
  ]
  node [
    id 114
    label "prevention"
  ]
  node [
    id 115
    label "spekulacja"
  ]
  node [
    id 116
    label "handel"
  ]
  node [
    id 117
    label "zone"
  ]
  node [
    id 118
    label "naszywka"
  ]
  node [
    id 119
    label "us&#322;uga"
  ]
  node [
    id 120
    label "przewi&#261;zka"
  ]
  node [
    id 121
    label "pole"
  ]
  node [
    id 122
    label "prze&#322;&#261;cznik"
  ]
  node [
    id 123
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 124
    label "interfejs"
  ]
  node [
    id 125
    label "wymowa"
  ]
  node [
    id 126
    label "d&#378;wi&#281;k"
  ]
  node [
    id 127
    label "nacisk"
  ]
  node [
    id 128
    label "identyfikator"
  ]
  node [
    id 129
    label "napis"
  ]
  node [
    id 130
    label "nerd"
  ]
  node [
    id 131
    label "komnatowy"
  ]
  node [
    id 132
    label "sport_elektroniczny"
  ]
  node [
    id 133
    label "znacznik"
  ]
  node [
    id 134
    label "uprawi&#263;"
  ]
  node [
    id 135
    label "gotowy"
  ]
  node [
    id 136
    label "might"
  ]
  node [
    id 137
    label "endeavor"
  ]
  node [
    id 138
    label "funkcjonowa&#263;"
  ]
  node [
    id 139
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 140
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 141
    label "dzia&#322;a&#263;"
  ]
  node [
    id 142
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 143
    label "work"
  ]
  node [
    id 144
    label "bangla&#263;"
  ]
  node [
    id 145
    label "do"
  ]
  node [
    id 146
    label "maszyna"
  ]
  node [
    id 147
    label "tryb"
  ]
  node [
    id 148
    label "dziama&#263;"
  ]
  node [
    id 149
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 150
    label "praca"
  ]
  node [
    id 151
    label "podejmowa&#263;"
  ]
  node [
    id 152
    label "srom"
  ]
  node [
    id 153
    label "kobieta"
  ]
  node [
    id 154
    label "urz&#261;dzenie_wskazuj&#261;ce"
  ]
  node [
    id 155
    label "znami&#281;"
  ]
  node [
    id 156
    label "klawisz_myszki"
  ]
  node [
    id 157
    label "bouquet"
  ]
  node [
    id 158
    label "komputer"
  ]
  node [
    id 159
    label "flag"
  ]
  node [
    id 160
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 161
    label "podkre&#347;li&#263;"
  ]
  node [
    id 162
    label "uwydatni&#263;"
  ]
  node [
    id 163
    label "wskaza&#263;"
  ]
  node [
    id 164
    label "appoint"
  ]
  node [
    id 165
    label "set"
  ]
  node [
    id 166
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 167
    label "pisa&#263;"
  ]
  node [
    id 168
    label "odmianka"
  ]
  node [
    id 169
    label "opu&#347;ci&#263;"
  ]
  node [
    id 170
    label "wypowied&#378;"
  ]
  node [
    id 171
    label "wytw&#243;r"
  ]
  node [
    id 172
    label "koniektura"
  ]
  node [
    id 173
    label "preparacja"
  ]
  node [
    id 174
    label "ekscerpcja"
  ]
  node [
    id 175
    label "j&#281;zykowo"
  ]
  node [
    id 176
    label "obelga"
  ]
  node [
    id 177
    label "dzie&#322;o"
  ]
  node [
    id 178
    label "redakcja"
  ]
  node [
    id 179
    label "pomini&#281;cie"
  ]
  node [
    id 180
    label "nacisn&#261;&#263;"
  ]
  node [
    id 181
    label "wybra&#263;"
  ]
  node [
    id 182
    label "chink"
  ]
  node [
    id 183
    label "proceed"
  ]
  node [
    id 184
    label "catch"
  ]
  node [
    id 185
    label "pozosta&#263;"
  ]
  node [
    id 186
    label "osta&#263;_si&#281;"
  ]
  node [
    id 187
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 188
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 189
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 190
    label "change"
  ]
  node [
    id 191
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 192
    label "specjalny"
  ]
  node [
    id 193
    label "nale&#380;yty"
  ]
  node [
    id 194
    label "stosownie"
  ]
  node [
    id 195
    label "zdarzony"
  ]
  node [
    id 196
    label "odpowiednio"
  ]
  node [
    id 197
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 198
    label "odpowiadanie"
  ]
  node [
    id 199
    label "nale&#380;ny"
  ]
  node [
    id 200
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 201
    label "gaz_cieplarniany"
  ]
  node [
    id 202
    label "grupa"
  ]
  node [
    id 203
    label "smoke"
  ]
  node [
    id 204
    label "pair"
  ]
  node [
    id 205
    label "sztuka"
  ]
  node [
    id 206
    label "Albania"
  ]
  node [
    id 207
    label "odparowanie"
  ]
  node [
    id 208
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 209
    label "odparowywa&#263;"
  ]
  node [
    id 210
    label "nale&#380;e&#263;"
  ]
  node [
    id 211
    label "wyparowanie"
  ]
  node [
    id 212
    label "zesp&#243;&#322;"
  ]
  node [
    id 213
    label "parowanie"
  ]
  node [
    id 214
    label "damp"
  ]
  node [
    id 215
    label "odparowywanie"
  ]
  node [
    id 216
    label "poker"
  ]
  node [
    id 217
    label "moneta"
  ]
  node [
    id 218
    label "odparowa&#263;"
  ]
  node [
    id 219
    label "jednostka_monetarna"
  ]
  node [
    id 220
    label "uk&#322;ad"
  ]
  node [
    id 221
    label "gaz"
  ]
  node [
    id 222
    label "zbi&#243;r"
  ]
  node [
    id 223
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 224
    label "cia&#322;o"
  ]
  node [
    id 225
    label "begin"
  ]
  node [
    id 226
    label "train"
  ]
  node [
    id 227
    label "uruchamia&#263;"
  ]
  node [
    id 228
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 229
    label "robi&#263;"
  ]
  node [
    id 230
    label "zaczyna&#263;"
  ]
  node [
    id 231
    label "unboxing"
  ]
  node [
    id 232
    label "przecina&#263;"
  ]
  node [
    id 233
    label "powodowa&#263;"
  ]
  node [
    id 234
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 235
    label "faza"
  ]
  node [
    id 236
    label "upgrade"
  ]
  node [
    id 237
    label "pierworodztwo"
  ]
  node [
    id 238
    label "nast&#281;pstwo"
  ]
  node [
    id 239
    label "podkre&#347;lenie"
  ]
  node [
    id 240
    label "bell_ringer"
  ]
  node [
    id 241
    label "zastrzeganie"
  ]
  node [
    id 242
    label "marker"
  ]
  node [
    id 243
    label "wskazanie"
  ]
  node [
    id 244
    label "zastrze&#380;enie"
  ]
  node [
    id 245
    label "note"
  ]
  node [
    id 246
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 247
    label "uwydatnienie"
  ]
  node [
    id 248
    label "ostatni"
  ]
  node [
    id 249
    label "ko&#324;cowo"
  ]
  node [
    id 250
    label "defenestracja"
  ]
  node [
    id 251
    label "szereg"
  ]
  node [
    id 252
    label "dzia&#322;anie"
  ]
  node [
    id 253
    label "ostatnie_podrygi"
  ]
  node [
    id 254
    label "kres"
  ]
  node [
    id 255
    label "agonia"
  ]
  node [
    id 256
    label "visitation"
  ]
  node [
    id 257
    label "szeol"
  ]
  node [
    id 258
    label "mogi&#322;a"
  ]
  node [
    id 259
    label "chwila"
  ]
  node [
    id 260
    label "pogrzebanie"
  ]
  node [
    id 261
    label "punkt"
  ]
  node [
    id 262
    label "&#380;a&#322;oba"
  ]
  node [
    id 263
    label "zabicie"
  ]
  node [
    id 264
    label "kres_&#380;ycia"
  ]
  node [
    id 265
    label "ki&#347;&#263;"
  ]
  node [
    id 266
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 267
    label "krzew"
  ]
  node [
    id 268
    label "pi&#380;maczkowate"
  ]
  node [
    id 269
    label "pestkowiec"
  ]
  node [
    id 270
    label "kwiat"
  ]
  node [
    id 271
    label "owoc"
  ]
  node [
    id 272
    label "oliwkowate"
  ]
  node [
    id 273
    label "ro&#347;lina"
  ]
  node [
    id 274
    label "hy&#263;ka"
  ]
  node [
    id 275
    label "lilac"
  ]
  node [
    id 276
    label "delfinidyna"
  ]
  node [
    id 277
    label "poprzednio"
  ]
  node [
    id 278
    label "przesz&#322;y"
  ]
  node [
    id 279
    label "uprzednio"
  ]
  node [
    id 280
    label "wcze&#347;niejszy"
  ]
  node [
    id 281
    label "SLD"
  ]
  node [
    id 282
    label "niedoczas"
  ]
  node [
    id 283
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 284
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 285
    label "game"
  ]
  node [
    id 286
    label "ZChN"
  ]
  node [
    id 287
    label "wybranka"
  ]
  node [
    id 288
    label "Wigowie"
  ]
  node [
    id 289
    label "unit"
  ]
  node [
    id 290
    label "blok"
  ]
  node [
    id 291
    label "Razem"
  ]
  node [
    id 292
    label "organizacja"
  ]
  node [
    id 293
    label "si&#322;a"
  ]
  node [
    id 294
    label "wybranek"
  ]
  node [
    id 295
    label "materia&#322;"
  ]
  node [
    id 296
    label "PiS"
  ]
  node [
    id 297
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 298
    label "Bund"
  ]
  node [
    id 299
    label "AWS"
  ]
  node [
    id 300
    label "package"
  ]
  node [
    id 301
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 302
    label "Kuomintang"
  ]
  node [
    id 303
    label "aktyw"
  ]
  node [
    id 304
    label "Jakobici"
  ]
  node [
    id 305
    label "PSL"
  ]
  node [
    id 306
    label "Federali&#347;ci"
  ]
  node [
    id 307
    label "gra"
  ]
  node [
    id 308
    label "ZSL"
  ]
  node [
    id 309
    label "PPR"
  ]
  node [
    id 310
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 311
    label "PO"
  ]
  node [
    id 312
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 313
    label "Izba_Par&#243;w"
  ]
  node [
    id 314
    label "lord"
  ]
  node [
    id 315
    label "Izba_Lord&#243;w"
  ]
  node [
    id 316
    label "parlamentarzysta"
  ]
  node [
    id 317
    label "lennik"
  ]
  node [
    id 318
    label "plac"
  ]
  node [
    id 319
    label "uwaga"
  ]
  node [
    id 320
    label "przestrze&#324;"
  ]
  node [
    id 321
    label "status"
  ]
  node [
    id 322
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 323
    label "rz&#261;d"
  ]
  node [
    id 324
    label "location"
  ]
  node [
    id 325
    label "warunek_lokalowy"
  ]
  node [
    id 326
    label "doznawa&#263;"
  ]
  node [
    id 327
    label "znachodzi&#263;"
  ]
  node [
    id 328
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 329
    label "pozyskiwa&#263;"
  ]
  node [
    id 330
    label "odzyskiwa&#263;"
  ]
  node [
    id 331
    label "os&#261;dza&#263;"
  ]
  node [
    id 332
    label "wykrywa&#263;"
  ]
  node [
    id 333
    label "unwrap"
  ]
  node [
    id 334
    label "detect"
  ]
  node [
    id 335
    label "wymy&#347;la&#263;"
  ]
  node [
    id 336
    label "strza&#322;ka"
  ]
  node [
    id 337
    label "struktura"
  ]
  node [
    id 338
    label "simile"
  ]
  node [
    id 339
    label "figura_stylistyczna"
  ]
  node [
    id 340
    label "zestawienie"
  ]
  node [
    id 341
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 342
    label "comparison"
  ]
  node [
    id 343
    label "zanalizowanie"
  ]
  node [
    id 344
    label "r&#281;cznie"
  ]
  node [
    id 345
    label "wprowadza&#263;"
  ]
  node [
    id 346
    label "pull"
  ]
  node [
    id 347
    label "write"
  ]
  node [
    id 348
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 349
    label "read"
  ]
  node [
    id 350
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 351
    label "method"
  ]
  node [
    id 352
    label "spos&#243;b"
  ]
  node [
    id 353
    label "doktryna"
  ]
  node [
    id 354
    label "Bangladesz"
  ]
  node [
    id 355
    label "zmienia&#263;"
  ]
  node [
    id 356
    label "increase"
  ]
  node [
    id 357
    label "sprawno&#347;&#263;"
  ]
  node [
    id 358
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 359
    label "celerity"
  ]
  node [
    id 360
    label "kr&#243;tko&#347;&#263;"
  ]
  node [
    id 361
    label "tempo"
  ]
  node [
    id 362
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 363
    label "pr&#281;dko&#347;&#263;"
  ]
  node [
    id 364
    label "control"
  ]
  node [
    id 365
    label "baseball"
  ]
  node [
    id 366
    label "czyn"
  ]
  node [
    id 367
    label "error"
  ]
  node [
    id 368
    label "pomyli&#263;_si&#281;"
  ]
  node [
    id 369
    label "pomylenie_si&#281;"
  ]
  node [
    id 370
    label "mniemanie"
  ]
  node [
    id 371
    label "byk"
  ]
  node [
    id 372
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 373
    label "rezultat"
  ]
  node [
    id 374
    label "umieszcza&#263;"
  ]
  node [
    id 375
    label "intervene"
  ]
  node [
    id 376
    label "deliver"
  ]
  node [
    id 377
    label "spis"
  ]
  node [
    id 378
    label "odinstalowanie"
  ]
  node [
    id 379
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 380
    label "za&#322;o&#380;enie"
  ]
  node [
    id 381
    label "podstawa"
  ]
  node [
    id 382
    label "emitowanie"
  ]
  node [
    id 383
    label "odinstalowywanie"
  ]
  node [
    id 384
    label "instrukcja"
  ]
  node [
    id 385
    label "teleferie"
  ]
  node [
    id 386
    label "emitowa&#263;"
  ]
  node [
    id 387
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 388
    label "sekcja_krytyczna"
  ]
  node [
    id 389
    label "prezentowa&#263;"
  ]
  node [
    id 390
    label "podprogram"
  ]
  node [
    id 391
    label "dzia&#322;"
  ]
  node [
    id 392
    label "broszura"
  ]
  node [
    id 393
    label "deklaracja"
  ]
  node [
    id 394
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 395
    label "struktura_organizacyjna"
  ]
  node [
    id 396
    label "zaprezentowanie"
  ]
  node [
    id 397
    label "informatyka"
  ]
  node [
    id 398
    label "booklet"
  ]
  node [
    id 399
    label "menu"
  ]
  node [
    id 400
    label "oprogramowanie"
  ]
  node [
    id 401
    label "instalowanie"
  ]
  node [
    id 402
    label "furkacja"
  ]
  node [
    id 403
    label "odinstalowa&#263;"
  ]
  node [
    id 404
    label "instalowa&#263;"
  ]
  node [
    id 405
    label "okno"
  ]
  node [
    id 406
    label "pirat"
  ]
  node [
    id 407
    label "zainstalowanie"
  ]
  node [
    id 408
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 409
    label "ogranicznik_referencyjny"
  ]
  node [
    id 410
    label "zainstalowa&#263;"
  ]
  node [
    id 411
    label "kana&#322;"
  ]
  node [
    id 412
    label "zaprezentowa&#263;"
  ]
  node [
    id 413
    label "odinstalowywa&#263;"
  ]
  node [
    id 414
    label "folder"
  ]
  node [
    id 415
    label "course_of_study"
  ]
  node [
    id 416
    label "ram&#243;wka"
  ]
  node [
    id 417
    label "prezentowanie"
  ]
  node [
    id 418
    label "oferta"
  ]
  node [
    id 419
    label "obejmowa&#263;"
  ]
  node [
    id 420
    label "mie&#263;"
  ]
  node [
    id 421
    label "zamyka&#263;"
  ]
  node [
    id 422
    label "lock"
  ]
  node [
    id 423
    label "poznawa&#263;"
  ]
  node [
    id 424
    label "fold"
  ]
  node [
    id 425
    label "make"
  ]
  node [
    id 426
    label "ustala&#263;"
  ]
  node [
    id 427
    label "pomy&#322;ka"
  ]
  node [
    id 428
    label "misprint"
  ]
  node [
    id 429
    label "okre&#347;lony"
  ]
  node [
    id 430
    label "jaki&#347;"
  ]
  node [
    id 431
    label "pacjent"
  ]
  node [
    id 432
    label "kategoria_gramatyczna"
  ]
  node [
    id 433
    label "schorzenie"
  ]
  node [
    id 434
    label "przeznaczenie"
  ]
  node [
    id 435
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 436
    label "happening"
  ]
  node [
    id 437
    label "przyk&#322;ad"
  ]
  node [
    id 438
    label "doros&#322;y"
  ]
  node [
    id 439
    label "wiele"
  ]
  node [
    id 440
    label "dorodny"
  ]
  node [
    id 441
    label "znaczny"
  ]
  node [
    id 442
    label "du&#380;o"
  ]
  node [
    id 443
    label "prawdziwy"
  ]
  node [
    id 444
    label "niema&#322;o"
  ]
  node [
    id 445
    label "wa&#380;ny"
  ]
  node [
    id 446
    label "rozwini&#281;ty"
  ]
  node [
    id 447
    label "tre&#347;&#263;"
  ]
  node [
    id 448
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 449
    label "obrazowanie"
  ]
  node [
    id 450
    label "part"
  ]
  node [
    id 451
    label "organ"
  ]
  node [
    id 452
    label "komunikat"
  ]
  node [
    id 453
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 454
    label "element_anatomiczny"
  ]
  node [
    id 455
    label "Quanty"
  ]
  node [
    id 456
    label "quantum"
  ]
  node [
    id 457
    label "weba"
  ]
  node [
    id 458
    label "Dev"
  ]
  node [
    id 459
    label "grafika"
  ]
  node [
    id 460
    label "WL"
  ]
  node [
    id 461
    label "toolbarstgz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 11
  ]
  edge [
    source 11
    target 27
  ]
  edge [
    source 11
    target 31
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 11
    target 38
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 44
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 57
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 34
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 49
  ]
  edge [
    source 15
    target 62
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 62
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 42
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 37
  ]
  edge [
    source 19
    target 38
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 62
  ]
  edge [
    source 19
    target 45
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 49
  ]
  edge [
    source 21
    target 62
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 48
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 24
    target 189
  ]
  edge [
    source 24
    target 190
  ]
  edge [
    source 24
    target 191
  ]
  edge [
    source 24
    target 55
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 192
  ]
  edge [
    source 25
    target 193
  ]
  edge [
    source 25
    target 194
  ]
  edge [
    source 25
    target 195
  ]
  edge [
    source 25
    target 196
  ]
  edge [
    source 25
    target 197
  ]
  edge [
    source 25
    target 198
  ]
  edge [
    source 25
    target 199
  ]
  edge [
    source 25
    target 200
  ]
  edge [
    source 25
    target 41
  ]
  edge [
    source 26
    target 201
  ]
  edge [
    source 26
    target 202
  ]
  edge [
    source 26
    target 203
  ]
  edge [
    source 26
    target 204
  ]
  edge [
    source 26
    target 205
  ]
  edge [
    source 26
    target 206
  ]
  edge [
    source 26
    target 111
  ]
  edge [
    source 26
    target 207
  ]
  edge [
    source 26
    target 208
  ]
  edge [
    source 26
    target 209
  ]
  edge [
    source 26
    target 210
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 213
  ]
  edge [
    source 26
    target 214
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 26
    target 217
  ]
  edge [
    source 26
    target 218
  ]
  edge [
    source 26
    target 219
  ]
  edge [
    source 26
    target 220
  ]
  edge [
    source 26
    target 221
  ]
  edge [
    source 26
    target 85
  ]
  edge [
    source 26
    target 222
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 27
    target 228
  ]
  edge [
    source 27
    target 229
  ]
  edge [
    source 27
    target 230
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 232
  ]
  edge [
    source 27
    target 233
  ]
  edge [
    source 27
    target 234
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 42
  ]
  edge [
    source 28
    target 58
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 49
  ]
  edge [
    source 28
    target 62
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 39
  ]
  edge [
    source 29
    target 40
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 30
    target 235
  ]
  edge [
    source 30
    target 236
  ]
  edge [
    source 30
    target 166
  ]
  edge [
    source 30
    target 237
  ]
  edge [
    source 30
    target 238
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 239
  ]
  edge [
    source 31
    target 240
  ]
  edge [
    source 31
    target 241
  ]
  edge [
    source 31
    target 242
  ]
  edge [
    source 31
    target 243
  ]
  edge [
    source 31
    target 244
  ]
  edge [
    source 31
    target 245
  ]
  edge [
    source 31
    target 246
  ]
  edge [
    source 31
    target 247
  ]
  edge [
    source 31
    target 52
  ]
  edge [
    source 31
    target 57
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 248
  ]
  edge [
    source 32
    target 249
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 250
  ]
  edge [
    source 33
    target 251
  ]
  edge [
    source 33
    target 252
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 33
    target 253
  ]
  edge [
    source 33
    target 254
  ]
  edge [
    source 33
    target 255
  ]
  edge [
    source 33
    target 256
  ]
  edge [
    source 33
    target 257
  ]
  edge [
    source 33
    target 258
  ]
  edge [
    source 33
    target 259
  ]
  edge [
    source 33
    target 97
  ]
  edge [
    source 33
    target 166
  ]
  edge [
    source 33
    target 260
  ]
  edge [
    source 33
    target 261
  ]
  edge [
    source 33
    target 262
  ]
  edge [
    source 33
    target 263
  ]
  edge [
    source 33
    target 264
  ]
  edge [
    source 34
    target 49
  ]
  edge [
    source 34
    target 62
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 265
  ]
  edge [
    source 35
    target 266
  ]
  edge [
    source 35
    target 267
  ]
  edge [
    source 35
    target 268
  ]
  edge [
    source 35
    target 269
  ]
  edge [
    source 35
    target 270
  ]
  edge [
    source 35
    target 271
  ]
  edge [
    source 35
    target 272
  ]
  edge [
    source 35
    target 273
  ]
  edge [
    source 35
    target 274
  ]
  edge [
    source 35
    target 275
  ]
  edge [
    source 35
    target 276
  ]
  edge [
    source 36
    target 277
  ]
  edge [
    source 36
    target 278
  ]
  edge [
    source 36
    target 279
  ]
  edge [
    source 36
    target 280
  ]
  edge [
    source 36
    target 51
  ]
  edge [
    source 37
    target 281
  ]
  edge [
    source 37
    target 282
  ]
  edge [
    source 37
    target 283
  ]
  edge [
    source 37
    target 284
  ]
  edge [
    source 37
    target 202
  ]
  edge [
    source 37
    target 285
  ]
  edge [
    source 37
    target 286
  ]
  edge [
    source 37
    target 287
  ]
  edge [
    source 37
    target 288
  ]
  edge [
    source 37
    target 91
  ]
  edge [
    source 37
    target 289
  ]
  edge [
    source 37
    target 290
  ]
  edge [
    source 37
    target 291
  ]
  edge [
    source 37
    target 292
  ]
  edge [
    source 37
    target 293
  ]
  edge [
    source 37
    target 294
  ]
  edge [
    source 37
    target 295
  ]
  edge [
    source 37
    target 296
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 37
    target 298
  ]
  edge [
    source 37
    target 299
  ]
  edge [
    source 37
    target 300
  ]
  edge [
    source 37
    target 301
  ]
  edge [
    source 37
    target 302
  ]
  edge [
    source 37
    target 303
  ]
  edge [
    source 37
    target 304
  ]
  edge [
    source 37
    target 305
  ]
  edge [
    source 37
    target 306
  ]
  edge [
    source 37
    target 307
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 309
  ]
  edge [
    source 37
    target 166
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 37
    target 311
  ]
  edge [
    source 37
    target 312
  ]
  edge [
    source 38
    target 313
  ]
  edge [
    source 38
    target 314
  ]
  edge [
    source 38
    target 315
  ]
  edge [
    source 38
    target 316
  ]
  edge [
    source 38
    target 317
  ]
  edge [
    source 39
    target 224
  ]
  edge [
    source 39
    target 318
  ]
  edge [
    source 39
    target 94
  ]
  edge [
    source 39
    target 319
  ]
  edge [
    source 39
    target 320
  ]
  edge [
    source 39
    target 321
  ]
  edge [
    source 39
    target 322
  ]
  edge [
    source 39
    target 259
  ]
  edge [
    source 39
    target 166
  ]
  edge [
    source 39
    target 323
  ]
  edge [
    source 39
    target 150
  ]
  edge [
    source 39
    target 324
  ]
  edge [
    source 39
    target 325
  ]
  edge [
    source 40
    target 326
  ]
  edge [
    source 40
    target 327
  ]
  edge [
    source 40
    target 328
  ]
  edge [
    source 40
    target 329
  ]
  edge [
    source 40
    target 330
  ]
  edge [
    source 40
    target 331
  ]
  edge [
    source 40
    target 332
  ]
  edge [
    source 40
    target 333
  ]
  edge [
    source 40
    target 334
  ]
  edge [
    source 40
    target 335
  ]
  edge [
    source 40
    target 233
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 336
  ]
  edge [
    source 41
    target 337
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 338
  ]
  edge [
    source 42
    target 339
  ]
  edge [
    source 42
    target 340
  ]
  edge [
    source 42
    target 341
  ]
  edge [
    source 42
    target 342
  ]
  edge [
    source 42
    target 343
  ]
  edge [
    source 42
    target 58
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 344
  ]
  edge [
    source 44
    target 167
  ]
  edge [
    source 44
    target 345
  ]
  edge [
    source 44
    target 346
  ]
  edge [
    source 44
    target 347
  ]
  edge [
    source 44
    target 348
  ]
  edge [
    source 44
    target 349
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 350
  ]
  edge [
    source 45
    target 351
  ]
  edge [
    source 45
    target 352
  ]
  edge [
    source 45
    target 353
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 354
  ]
  edge [
    source 46
    target 219
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 355
  ]
  edge [
    source 47
    target 356
  ]
  edge [
    source 48
    target 357
  ]
  edge [
    source 48
    target 358
  ]
  edge [
    source 48
    target 359
  ]
  edge [
    source 48
    target 360
  ]
  edge [
    source 48
    target 361
  ]
  edge [
    source 48
    target 362
  ]
  edge [
    source 48
    target 363
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 364
  ]
  edge [
    source 49
    target 355
  ]
  edge [
    source 49
    target 62
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 59
  ]
  edge [
    source 51
    target 60
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 57
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 365
  ]
  edge [
    source 53
    target 366
  ]
  edge [
    source 53
    target 367
  ]
  edge [
    source 53
    target 368
  ]
  edge [
    source 53
    target 369
  ]
  edge [
    source 53
    target 370
  ]
  edge [
    source 53
    target 371
  ]
  edge [
    source 53
    target 372
  ]
  edge [
    source 53
    target 373
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 374
  ]
  edge [
    source 54
    target 375
  ]
  edge [
    source 54
    target 376
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 377
  ]
  edge [
    source 56
    target 378
  ]
  edge [
    source 56
    target 379
  ]
  edge [
    source 56
    target 380
  ]
  edge [
    source 56
    target 381
  ]
  edge [
    source 56
    target 382
  ]
  edge [
    source 56
    target 383
  ]
  edge [
    source 56
    target 384
  ]
  edge [
    source 56
    target 261
  ]
  edge [
    source 56
    target 385
  ]
  edge [
    source 56
    target 386
  ]
  edge [
    source 56
    target 171
  ]
  edge [
    source 56
    target 387
  ]
  edge [
    source 56
    target 388
  ]
  edge [
    source 56
    target 389
  ]
  edge [
    source 56
    target 290
  ]
  edge [
    source 56
    target 390
  ]
  edge [
    source 56
    target 147
  ]
  edge [
    source 56
    target 391
  ]
  edge [
    source 56
    target 392
  ]
  edge [
    source 56
    target 393
  ]
  edge [
    source 56
    target 394
  ]
  edge [
    source 56
    target 395
  ]
  edge [
    source 56
    target 396
  ]
  edge [
    source 56
    target 397
  ]
  edge [
    source 56
    target 398
  ]
  edge [
    source 56
    target 399
  ]
  edge [
    source 56
    target 400
  ]
  edge [
    source 56
    target 401
  ]
  edge [
    source 56
    target 402
  ]
  edge [
    source 56
    target 403
  ]
  edge [
    source 56
    target 404
  ]
  edge [
    source 56
    target 405
  ]
  edge [
    source 56
    target 406
  ]
  edge [
    source 56
    target 407
  ]
  edge [
    source 56
    target 408
  ]
  edge [
    source 56
    target 409
  ]
  edge [
    source 56
    target 410
  ]
  edge [
    source 56
    target 411
  ]
  edge [
    source 56
    target 412
  ]
  edge [
    source 56
    target 124
  ]
  edge [
    source 56
    target 413
  ]
  edge [
    source 56
    target 414
  ]
  edge [
    source 56
    target 415
  ]
  edge [
    source 56
    target 416
  ]
  edge [
    source 56
    target 417
  ]
  edge [
    source 56
    target 418
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 419
  ]
  edge [
    source 57
    target 420
  ]
  edge [
    source 57
    target 421
  ]
  edge [
    source 57
    target 422
  ]
  edge [
    source 57
    target 423
  ]
  edge [
    source 57
    target 424
  ]
  edge [
    source 57
    target 425
  ]
  edge [
    source 57
    target 426
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 427
  ]
  edge [
    source 58
    target 428
  ]
  edge [
    source 59
    target 429
  ]
  edge [
    source 59
    target 430
  ]
  edge [
    source 60
    target 431
  ]
  edge [
    source 60
    target 432
  ]
  edge [
    source 60
    target 433
  ]
  edge [
    source 60
    target 434
  ]
  edge [
    source 60
    target 435
  ]
  edge [
    source 60
    target 97
  ]
  edge [
    source 60
    target 436
  ]
  edge [
    source 60
    target 437
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 438
  ]
  edge [
    source 61
    target 439
  ]
  edge [
    source 61
    target 440
  ]
  edge [
    source 61
    target 441
  ]
  edge [
    source 61
    target 442
  ]
  edge [
    source 61
    target 443
  ]
  edge [
    source 61
    target 444
  ]
  edge [
    source 61
    target 445
  ]
  edge [
    source 61
    target 446
  ]
  edge [
    source 62
    target 447
  ]
  edge [
    source 62
    target 448
  ]
  edge [
    source 62
    target 449
  ]
  edge [
    source 62
    target 450
  ]
  edge [
    source 62
    target 451
  ]
  edge [
    source 62
    target 452
  ]
  edge [
    source 62
    target 107
  ]
  edge [
    source 62
    target 453
  ]
  edge [
    source 62
    target 454
  ]
  edge [
    source 457
    target 458
  ]
  edge [
    source 459
    target 460
  ]
  edge [
    source 459
    target 461
  ]
  edge [
    source 460
    target 461
  ]
]
