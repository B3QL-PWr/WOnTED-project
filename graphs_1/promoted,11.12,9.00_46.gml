graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.07407407407407407
  graphCliqueNumber 2
  node [
    id 0
    label "&#347;widnicki"
    origin "text"
  ]
  node [
    id 1
    label "policjant"
    origin "text"
  ]
  node [
    id 2
    label "poszukiwa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zaginiona"
    origin "text"
  ]
  node [
    id 4
    label "letni"
    origin "text"
  ]
  node [
    id 5
    label "kamila"
    origin "text"
  ]
  node [
    id 6
    label "siedlecki"
    origin "text"
  ]
  node [
    id 7
    label "policja"
  ]
  node [
    id 8
    label "blacharz"
  ]
  node [
    id 9
    label "pa&#322;a"
  ]
  node [
    id 10
    label "mundurowy"
  ]
  node [
    id 11
    label "str&#243;&#380;"
  ]
  node [
    id 12
    label "glina"
  ]
  node [
    id 13
    label "ask"
  ]
  node [
    id 14
    label "stara&#263;_si&#281;"
  ]
  node [
    id 15
    label "szuka&#263;"
  ]
  node [
    id 16
    label "look"
  ]
  node [
    id 17
    label "nijaki"
  ]
  node [
    id 18
    label "sezonowy"
  ]
  node [
    id 19
    label "letnio"
  ]
  node [
    id 20
    label "s&#322;oneczny"
  ]
  node [
    id 21
    label "weso&#322;y"
  ]
  node [
    id 22
    label "oboj&#281;tny"
  ]
  node [
    id 23
    label "latowy"
  ]
  node [
    id 24
    label "ciep&#322;y"
  ]
  node [
    id 25
    label "typowy"
  ]
  node [
    id 26
    label "Kamila"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 26
  ]
]
