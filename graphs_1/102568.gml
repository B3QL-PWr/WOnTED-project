graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.155844155844156
  density 0.02836637047163363
  graphCliqueNumber 3
  node [
    id 0
    label "cena"
    origin "text"
  ]
  node [
    id 1
    label "wywo&#322;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pierwsza"
    origin "text"
  ]
  node [
    id 3
    label "termin"
    origin "text"
  ]
  node [
    id 4
    label "licytacja"
    origin "text"
  ]
  node [
    id 5
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "warto&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "szacunkowy"
    origin "text"
  ]
  node [
    id 8
    label "ruchomo&#347;ci"
    origin "text"
  ]
  node [
    id 9
    label "druga"
    origin "text"
  ]
  node [
    id 10
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 11
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 12
    label "dyskryminacja_cenowa"
  ]
  node [
    id 13
    label "inflacja"
  ]
  node [
    id 14
    label "kupowanie"
  ]
  node [
    id 15
    label "kosztowa&#263;"
  ]
  node [
    id 16
    label "kosztowanie"
  ]
  node [
    id 17
    label "worth"
  ]
  node [
    id 18
    label "wyceni&#263;"
  ]
  node [
    id 19
    label "wycenienie"
  ]
  node [
    id 20
    label "trip"
  ]
  node [
    id 21
    label "spowodowa&#263;"
  ]
  node [
    id 22
    label "przetworzy&#263;"
  ]
  node [
    id 23
    label "wydali&#263;"
  ]
  node [
    id 24
    label "wezwa&#263;"
  ]
  node [
    id 25
    label "revolutionize"
  ]
  node [
    id 26
    label "arouse"
  ]
  node [
    id 27
    label "train"
  ]
  node [
    id 28
    label "oznajmi&#263;"
  ]
  node [
    id 29
    label "poleci&#263;"
  ]
  node [
    id 30
    label "godzina"
  ]
  node [
    id 31
    label "przypadni&#281;cie"
  ]
  node [
    id 32
    label "czas"
  ]
  node [
    id 33
    label "chronogram"
  ]
  node [
    id 34
    label "nazewnictwo"
  ]
  node [
    id 35
    label "ekspiracja"
  ]
  node [
    id 36
    label "nazwa"
  ]
  node [
    id 37
    label "przypa&#347;&#263;"
  ]
  node [
    id 38
    label "praktyka"
  ]
  node [
    id 39
    label "term"
  ]
  node [
    id 40
    label "bryd&#380;"
  ]
  node [
    id 41
    label "rozdanie"
  ]
  node [
    id 42
    label "faza"
  ]
  node [
    id 43
    label "tysi&#261;c"
  ]
  node [
    id 44
    label "pas"
  ]
  node [
    id 45
    label "przetarg"
  ]
  node [
    id 46
    label "skat"
  ]
  node [
    id 47
    label "sprzeda&#380;"
  ]
  node [
    id 48
    label "gra_w_karty"
  ]
  node [
    id 49
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 50
    label "odsuwa&#263;"
  ]
  node [
    id 51
    label "zanosi&#263;"
  ]
  node [
    id 52
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 53
    label "otrzymywa&#263;"
  ]
  node [
    id 54
    label "rozpowszechnia&#263;"
  ]
  node [
    id 55
    label "ujawnia&#263;"
  ]
  node [
    id 56
    label "kra&#347;&#263;"
  ]
  node [
    id 57
    label "kwota"
  ]
  node [
    id 58
    label "liczy&#263;"
  ]
  node [
    id 59
    label "raise"
  ]
  node [
    id 60
    label "podnosi&#263;"
  ]
  node [
    id 61
    label "rewaluowa&#263;"
  ]
  node [
    id 62
    label "wabik"
  ]
  node [
    id 63
    label "wskazywanie"
  ]
  node [
    id 64
    label "korzy&#347;&#263;"
  ]
  node [
    id 65
    label "rewaluowanie"
  ]
  node [
    id 66
    label "cecha"
  ]
  node [
    id 67
    label "zmienna"
  ]
  node [
    id 68
    label "zrewaluowa&#263;"
  ]
  node [
    id 69
    label "rozmiar"
  ]
  node [
    id 70
    label "poj&#281;cie"
  ]
  node [
    id 71
    label "cel"
  ]
  node [
    id 72
    label "wskazywa&#263;"
  ]
  node [
    id 73
    label "strona"
  ]
  node [
    id 74
    label "zrewaluowanie"
  ]
  node [
    id 75
    label "szacunkowo"
  ]
  node [
    id 76
    label "przybli&#380;ony"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 9
    target 30
  ]
]
