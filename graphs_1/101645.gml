graph [
  maxDegree 43
  minDegree 1
  meanDegree 2.2209302325581395
  density 0.012987896096831226
  graphCliqueNumber 6
  node [
    id 0
    label "zmiana"
    origin "text"
  ]
  node [
    id 1
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "wprowadzi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 4
    label "demokratyzacja"
    origin "text"
  ]
  node [
    id 5
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 6
    label "system"
    origin "text"
  ]
  node [
    id 7
    label "skok"
    origin "text"
  ]
  node [
    id 8
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 9
    label "swoboda"
    origin "text"
  ]
  node [
    id 10
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "tzw"
    origin "text"
  ]
  node [
    id 12
    label "parterowy"
    origin "text"
  ]
  node [
    id 13
    label "sp&#243;&#322;dzielczy"
    origin "text"
  ]
  node [
    id 14
    label "kasa"
    origin "text"
  ]
  node [
    id 15
    label "oszcz&#281;dno&#347;ciowo"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "sukces"
    origin "text"
  ]
  node [
    id 18
    label "zapewnia&#263;"
    origin "text"
  ]
  node [
    id 19
    label "ten"
    origin "text"
  ]
  node [
    id 20
    label "ustawa"
    origin "text"
  ]
  node [
    id 21
    label "anatomopatolog"
  ]
  node [
    id 22
    label "rewizja"
  ]
  node [
    id 23
    label "oznaka"
  ]
  node [
    id 24
    label "czas"
  ]
  node [
    id 25
    label "ferment"
  ]
  node [
    id 26
    label "komplet"
  ]
  node [
    id 27
    label "tura"
  ]
  node [
    id 28
    label "amendment"
  ]
  node [
    id 29
    label "zmianka"
  ]
  node [
    id 30
    label "odmienianie"
  ]
  node [
    id 31
    label "passage"
  ]
  node [
    id 32
    label "zjawisko"
  ]
  node [
    id 33
    label "change"
  ]
  node [
    id 34
    label "praca"
  ]
  node [
    id 35
    label "doros&#322;y"
  ]
  node [
    id 36
    label "wiele"
  ]
  node [
    id 37
    label "dorodny"
  ]
  node [
    id 38
    label "znaczny"
  ]
  node [
    id 39
    label "prawdziwy"
  ]
  node [
    id 40
    label "niema&#322;o"
  ]
  node [
    id 41
    label "wa&#380;ny"
  ]
  node [
    id 42
    label "rozwini&#281;ty"
  ]
  node [
    id 43
    label "proces"
  ]
  node [
    id 44
    label "strategia"
  ]
  node [
    id 45
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 46
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 47
    label "model"
  ]
  node [
    id 48
    label "sk&#322;ad"
  ]
  node [
    id 49
    label "zachowanie"
  ]
  node [
    id 50
    label "podstawa"
  ]
  node [
    id 51
    label "porz&#261;dek"
  ]
  node [
    id 52
    label "Android"
  ]
  node [
    id 53
    label "przyn&#281;ta"
  ]
  node [
    id 54
    label "jednostka_geologiczna"
  ]
  node [
    id 55
    label "metoda"
  ]
  node [
    id 56
    label "podsystem"
  ]
  node [
    id 57
    label "p&#322;&#243;d"
  ]
  node [
    id 58
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 59
    label "s&#261;d"
  ]
  node [
    id 60
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 61
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 62
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 63
    label "j&#261;dro"
  ]
  node [
    id 64
    label "eratem"
  ]
  node [
    id 65
    label "ryba"
  ]
  node [
    id 66
    label "pulpit"
  ]
  node [
    id 67
    label "struktura"
  ]
  node [
    id 68
    label "spos&#243;b"
  ]
  node [
    id 69
    label "oddzia&#322;"
  ]
  node [
    id 70
    label "usenet"
  ]
  node [
    id 71
    label "o&#347;"
  ]
  node [
    id 72
    label "oprogramowanie"
  ]
  node [
    id 73
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 74
    label "poj&#281;cie"
  ]
  node [
    id 75
    label "w&#281;dkarstwo"
  ]
  node [
    id 76
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 77
    label "Leopard"
  ]
  node [
    id 78
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 79
    label "systemik"
  ]
  node [
    id 80
    label "rozprz&#261;c"
  ]
  node [
    id 81
    label "cybernetyk"
  ]
  node [
    id 82
    label "konstelacja"
  ]
  node [
    id 83
    label "doktryna"
  ]
  node [
    id 84
    label "net"
  ]
  node [
    id 85
    label "zbi&#243;r"
  ]
  node [
    id 86
    label "method"
  ]
  node [
    id 87
    label "systemat"
  ]
  node [
    id 88
    label "wybicie"
  ]
  node [
    id 89
    label "konkurencja"
  ]
  node [
    id 90
    label "derail"
  ]
  node [
    id 91
    label "ptak"
  ]
  node [
    id 92
    label "ruch"
  ]
  node [
    id 93
    label "l&#261;dowanie"
  ]
  node [
    id 94
    label "&#322;apa"
  ]
  node [
    id 95
    label "struktura_anatomiczna"
  ]
  node [
    id 96
    label "stroke"
  ]
  node [
    id 97
    label "gimnastyka_przyrz&#261;dowa"
  ]
  node [
    id 98
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 99
    label "caper"
  ]
  node [
    id 100
    label "zaj&#261;c"
  ]
  node [
    id 101
    label "naskok"
  ]
  node [
    id 102
    label "napad"
  ]
  node [
    id 103
    label "ko&#347;&#263;_skokowa"
  ]
  node [
    id 104
    label "noga"
  ]
  node [
    id 105
    label "cz&#281;sto"
  ]
  node [
    id 106
    label "bardzo"
  ]
  node [
    id 107
    label "mocno"
  ]
  node [
    id 108
    label "wiela"
  ]
  node [
    id 109
    label "naturalno&#347;&#263;"
  ]
  node [
    id 110
    label "cecha"
  ]
  node [
    id 111
    label "zdolno&#347;&#263;"
  ]
  node [
    id 112
    label "freedom"
  ]
  node [
    id 113
    label "dzia&#322;anie"
  ]
  node [
    id 114
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 115
    label "absolutorium"
  ]
  node [
    id 116
    label "activity"
  ]
  node [
    id 117
    label "niski"
  ]
  node [
    id 118
    label "uspo&#322;ecznianie"
  ]
  node [
    id 119
    label "uspo&#322;ecznienie"
  ]
  node [
    id 120
    label "skrzynia"
  ]
  node [
    id 121
    label "miejsce"
  ]
  node [
    id 122
    label "instytucja"
  ]
  node [
    id 123
    label "szafa"
  ]
  node [
    id 124
    label "pieni&#261;dze"
  ]
  node [
    id 125
    label "urz&#261;dzenie"
  ]
  node [
    id 126
    label "si&#281;ga&#263;"
  ]
  node [
    id 127
    label "trwa&#263;"
  ]
  node [
    id 128
    label "obecno&#347;&#263;"
  ]
  node [
    id 129
    label "stan"
  ]
  node [
    id 130
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 131
    label "stand"
  ]
  node [
    id 132
    label "mie&#263;_miejsce"
  ]
  node [
    id 133
    label "uczestniczy&#263;"
  ]
  node [
    id 134
    label "chodzi&#263;"
  ]
  node [
    id 135
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 136
    label "equal"
  ]
  node [
    id 137
    label "success"
  ]
  node [
    id 138
    label "passa"
  ]
  node [
    id 139
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 140
    label "kobieta_sukcesu"
  ]
  node [
    id 141
    label "rezultat"
  ]
  node [
    id 142
    label "utrzymywa&#263;"
  ]
  node [
    id 143
    label "dostarcza&#263;"
  ]
  node [
    id 144
    label "informowa&#263;"
  ]
  node [
    id 145
    label "deliver"
  ]
  node [
    id 146
    label "okre&#347;lony"
  ]
  node [
    id 147
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 148
    label "Karta_Nauczyciela"
  ]
  node [
    id 149
    label "marc&#243;wka"
  ]
  node [
    id 150
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 151
    label "akt"
  ]
  node [
    id 152
    label "przej&#347;&#263;"
  ]
  node [
    id 153
    label "charter"
  ]
  node [
    id 154
    label "przej&#347;cie"
  ]
  node [
    id 155
    label "Stefan"
  ]
  node [
    id 156
    label "Niesio&#322;owski"
  ]
  node [
    id 157
    label "Jaros&#322;awa"
  ]
  node [
    id 158
    label "Urbaniak"
  ]
  node [
    id 159
    label "urz&#261;d"
  ]
  node [
    id 160
    label "ochrona"
  ]
  node [
    id 161
    label "i"
  ]
  node [
    id 162
    label "konsument"
  ]
  node [
    id 163
    label "platforma"
  ]
  node [
    id 164
    label "obywatelski"
  ]
  node [
    id 165
    label "senat"
  ]
  node [
    id 166
    label "rzeczpospolita"
  ]
  node [
    id 167
    label "Stawiarski"
  ]
  node [
    id 168
    label "prawo"
  ]
  node [
    id 169
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 170
    label "ojciec"
  ]
  node [
    id 171
    label "kredytowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 89
    target 159
  ]
  edge [
    source 89
    target 160
  ]
  edge [
    source 89
    target 161
  ]
  edge [
    source 89
    target 162
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 157
    target 167
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 159
    target 161
  ]
  edge [
    source 159
    target 162
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 160
    target 162
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 161
    target 168
  ]
  edge [
    source 161
    target 169
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 170
    target 171
  ]
]
