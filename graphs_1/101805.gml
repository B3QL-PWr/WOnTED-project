graph [
  maxDegree 36
  minDegree 1
  meanDegree 2.316702819956616
  density 0.005036310478166556
  graphCliqueNumber 6
  node [
    id 0
    label "diagnoza"
    origin "text"
  ]
  node [
    id 1
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 2
    label "pozostawia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "w&#261;tpliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "internet"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "polska"
    origin "text"
  ]
  node [
    id 7
    label "g&#322;&#243;wnie"
    origin "text"
  ]
  node [
    id 8
    label "tam"
    origin "text"
  ]
  node [
    id 9
    label "gdzie"
    origin "text"
  ]
  node [
    id 10
    label "uczy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "dziecko"
    origin "text"
  ]
  node [
    id 13
    label "dysproporocje"
    origin "text"
  ]
  node [
    id 14
    label "wykorzystanie"
    origin "text"
  ]
  node [
    id 15
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 16
    label "grupa"
    origin "text"
  ]
  node [
    id 17
    label "wiekowy"
    origin "text"
  ]
  node [
    id 18
    label "pora&#380;a&#263;"
    origin "text"
  ]
  node [
    id 19
    label "w&#347;r&#243;d"
    origin "text"
  ]
  node [
    id 20
    label "polak"
    origin "text"
  ]
  node [
    id 21
    label "wiek"
    origin "text"
  ]
  node [
    id 22
    label "lata"
    origin "text"
  ]
  node [
    id 23
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 24
    label "korzysta&#263;"
    origin "text"
  ]
  node [
    id 25
    label "populacja"
    origin "text"
  ]
  node [
    id 26
    label "emeryt"
    origin "text"
  ]
  node [
    id 27
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 28
    label "starsi"
    origin "text"
  ]
  node [
    id 29
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 30
    label "ofiara"
    origin "text"
  ]
  node [
    id 31
    label "cyfrowy"
    origin "text"
  ]
  node [
    id 32
    label "wykluczy&#263;"
    origin "text"
  ]
  node [
    id 33
    label "pewno"
    origin "text"
  ]
  node [
    id 34
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 35
    label "czynnik"
    origin "text"
  ]
  node [
    id 36
    label "finanse"
    origin "text"
  ]
  node [
    id 37
    label "ale"
    origin "text"
  ]
  node [
    id 38
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 39
    label "ca&#322;kiem"
    origin "text"
  ]
  node [
    id 40
    label "bez"
    origin "text"
  ]
  node [
    id 41
    label "znaczenie"
    origin "text"
  ]
  node [
    id 42
    label "te&#380;"
    origin "text"
  ]
  node [
    id 43
    label "poczucie"
    origin "text"
  ]
  node [
    id 44
    label "dla"
    origin "text"
  ]
  node [
    id 45
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 46
    label "miejsce"
    origin "text"
  ]
  node [
    id 47
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 48
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 49
    label "zainteresowa&#263;"
    origin "text"
  ]
  node [
    id 50
    label "polski"
    origin "text"
  ]
  node [
    id 51
    label "radio"
    origin "text"
  ]
  node [
    id 52
    label "maryja"
    origin "text"
  ]
  node [
    id 53
    label "taki"
    origin "text"
  ]
  node [
    id 54
    label "projekt"
    origin "text"
  ]
  node [
    id 55
    label "internetowy"
    origin "text"
  ]
  node [
    id 56
    label "jak"
    origin "text"
  ]
  node [
    id 57
    label "saga"
    origin "text"
  ]
  node [
    id 58
    label "przyci&#261;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 59
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 60
    label "tychy"
    origin "text"
  ]
  node [
    id 61
    label "dokument"
  ]
  node [
    id 62
    label "diagnosis"
  ]
  node [
    id 63
    label "sprawdzian"
  ]
  node [
    id 64
    label "schorzenie"
  ]
  node [
    id 65
    label "rozpoznanie"
  ]
  node [
    id 66
    label "ocena"
  ]
  node [
    id 67
    label "niepubliczny"
  ]
  node [
    id 68
    label "spo&#322;ecznie"
  ]
  node [
    id 69
    label "publiczny"
  ]
  node [
    id 70
    label "impart"
  ]
  node [
    id 71
    label "liszy&#263;"
  ]
  node [
    id 72
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 73
    label "wydawa&#263;"
  ]
  node [
    id 74
    label "doprowadza&#263;"
  ]
  node [
    id 75
    label "yield"
  ]
  node [
    id 76
    label "dawa&#263;"
  ]
  node [
    id 77
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 78
    label "zamierza&#263;"
  ]
  node [
    id 79
    label "wyznacza&#263;"
  ]
  node [
    id 80
    label "permit"
  ]
  node [
    id 81
    label "bequeath"
  ]
  node [
    id 82
    label "porzuca&#263;"
  ]
  node [
    id 83
    label "zachowywa&#263;"
  ]
  node [
    id 84
    label "robi&#263;"
  ]
  node [
    id 85
    label "pomija&#263;"
  ]
  node [
    id 86
    label "przekazywa&#263;"
  ]
  node [
    id 87
    label "opuszcza&#263;"
  ]
  node [
    id 88
    label "krzywdzi&#263;"
  ]
  node [
    id 89
    label "zabiera&#263;"
  ]
  node [
    id 90
    label "zrywa&#263;"
  ]
  node [
    id 91
    label "tworzy&#263;"
  ]
  node [
    id 92
    label "rezygnowa&#263;"
  ]
  node [
    id 93
    label "powodowa&#263;"
  ]
  node [
    id 94
    label "w&#261;tpienie"
  ]
  node [
    id 95
    label "wypowied&#378;"
  ]
  node [
    id 96
    label "wytw&#243;r"
  ]
  node [
    id 97
    label "question"
  ]
  node [
    id 98
    label "us&#322;uga_internetowa"
  ]
  node [
    id 99
    label "biznes_elektroniczny"
  ]
  node [
    id 100
    label "punkt_dost&#281;pu"
  ]
  node [
    id 101
    label "hipertekst"
  ]
  node [
    id 102
    label "gra_sieciowa"
  ]
  node [
    id 103
    label "mem"
  ]
  node [
    id 104
    label "e-hazard"
  ]
  node [
    id 105
    label "sie&#263;_komputerowa"
  ]
  node [
    id 106
    label "media"
  ]
  node [
    id 107
    label "podcast"
  ]
  node [
    id 108
    label "netbook"
  ]
  node [
    id 109
    label "provider"
  ]
  node [
    id 110
    label "cyberprzestrze&#324;"
  ]
  node [
    id 111
    label "grooming"
  ]
  node [
    id 112
    label "strona"
  ]
  node [
    id 113
    label "si&#281;ga&#263;"
  ]
  node [
    id 114
    label "trwa&#263;"
  ]
  node [
    id 115
    label "obecno&#347;&#263;"
  ]
  node [
    id 116
    label "stan"
  ]
  node [
    id 117
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 118
    label "stand"
  ]
  node [
    id 119
    label "mie&#263;_miejsce"
  ]
  node [
    id 120
    label "uczestniczy&#263;"
  ]
  node [
    id 121
    label "chodzi&#263;"
  ]
  node [
    id 122
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 123
    label "equal"
  ]
  node [
    id 124
    label "g&#322;&#243;wny"
  ]
  node [
    id 125
    label "tu"
  ]
  node [
    id 126
    label "zapoznawa&#263;"
  ]
  node [
    id 127
    label "teach"
  ]
  node [
    id 128
    label "train"
  ]
  node [
    id 129
    label "rozwija&#263;"
  ]
  node [
    id 130
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 131
    label "pracowa&#263;"
  ]
  node [
    id 132
    label "szkoli&#263;"
  ]
  node [
    id 133
    label "potomstwo"
  ]
  node [
    id 134
    label "organizm"
  ]
  node [
    id 135
    label "sraluch"
  ]
  node [
    id 136
    label "utulanie"
  ]
  node [
    id 137
    label "pediatra"
  ]
  node [
    id 138
    label "dzieciarnia"
  ]
  node [
    id 139
    label "m&#322;odziak"
  ]
  node [
    id 140
    label "dzieciak"
  ]
  node [
    id 141
    label "utula&#263;"
  ]
  node [
    id 142
    label "potomek"
  ]
  node [
    id 143
    label "entliczek-pentliczek"
  ]
  node [
    id 144
    label "pedofil"
  ]
  node [
    id 145
    label "m&#322;odzik"
  ]
  node [
    id 146
    label "cz&#322;owieczek"
  ]
  node [
    id 147
    label "zwierz&#281;"
  ]
  node [
    id 148
    label "niepe&#322;noletni"
  ]
  node [
    id 149
    label "fledgling"
  ]
  node [
    id 150
    label "utuli&#263;"
  ]
  node [
    id 151
    label "utulenie"
  ]
  node [
    id 152
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 153
    label "zrobienie"
  ]
  node [
    id 154
    label "use"
  ]
  node [
    id 155
    label "u&#380;ycie"
  ]
  node [
    id 156
    label "stosowanie"
  ]
  node [
    id 157
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 158
    label "u&#380;yteczny"
  ]
  node [
    id 159
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 160
    label "exploitation"
  ]
  node [
    id 161
    label "r&#243;&#380;nie"
  ]
  node [
    id 162
    label "inny"
  ]
  node [
    id 163
    label "jaki&#347;"
  ]
  node [
    id 164
    label "odm&#322;adza&#263;"
  ]
  node [
    id 165
    label "asymilowa&#263;"
  ]
  node [
    id 166
    label "cz&#261;steczka"
  ]
  node [
    id 167
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 168
    label "egzemplarz"
  ]
  node [
    id 169
    label "formacja_geologiczna"
  ]
  node [
    id 170
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 171
    label "harcerze_starsi"
  ]
  node [
    id 172
    label "liga"
  ]
  node [
    id 173
    label "Terranie"
  ]
  node [
    id 174
    label "&#346;wietliki"
  ]
  node [
    id 175
    label "pakiet_klimatyczny"
  ]
  node [
    id 176
    label "oddzia&#322;"
  ]
  node [
    id 177
    label "stage_set"
  ]
  node [
    id 178
    label "Entuzjastki"
  ]
  node [
    id 179
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 180
    label "odm&#322;odzenie"
  ]
  node [
    id 181
    label "type"
  ]
  node [
    id 182
    label "category"
  ]
  node [
    id 183
    label "asymilowanie"
  ]
  node [
    id 184
    label "specgrupa"
  ]
  node [
    id 185
    label "odm&#322;adzanie"
  ]
  node [
    id 186
    label "gromada"
  ]
  node [
    id 187
    label "Eurogrupa"
  ]
  node [
    id 188
    label "jednostka_systematyczna"
  ]
  node [
    id 189
    label "kompozycja"
  ]
  node [
    id 190
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 191
    label "zbi&#243;r"
  ]
  node [
    id 192
    label "stary"
  ]
  node [
    id 193
    label "wiekowo"
  ]
  node [
    id 194
    label "paralyze"
  ]
  node [
    id 195
    label "ro&#347;lina"
  ]
  node [
    id 196
    label "strike"
  ]
  node [
    id 197
    label "zachwyca&#263;"
  ]
  node [
    id 198
    label "porusza&#263;"
  ]
  node [
    id 199
    label "uszkadza&#263;"
  ]
  node [
    id 200
    label "zaskakiwa&#263;"
  ]
  node [
    id 201
    label "uderza&#263;"
  ]
  node [
    id 202
    label "atakowa&#263;"
  ]
  node [
    id 203
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 204
    label "czas"
  ]
  node [
    id 205
    label "period"
  ]
  node [
    id 206
    label "cecha"
  ]
  node [
    id 207
    label "rok"
  ]
  node [
    id 208
    label "long_time"
  ]
  node [
    id 209
    label "choroba_wieku"
  ]
  node [
    id 210
    label "jednostka_geologiczna"
  ]
  node [
    id 211
    label "chron"
  ]
  node [
    id 212
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 213
    label "summer"
  ]
  node [
    id 214
    label "gauze"
  ]
  node [
    id 215
    label "nitka"
  ]
  node [
    id 216
    label "mesh"
  ]
  node [
    id 217
    label "snu&#263;"
  ]
  node [
    id 218
    label "organization"
  ]
  node [
    id 219
    label "zasadzka"
  ]
  node [
    id 220
    label "web"
  ]
  node [
    id 221
    label "struktura"
  ]
  node [
    id 222
    label "organizacja"
  ]
  node [
    id 223
    label "vane"
  ]
  node [
    id 224
    label "kszta&#322;t"
  ]
  node [
    id 225
    label "obiekt"
  ]
  node [
    id 226
    label "wysnu&#263;"
  ]
  node [
    id 227
    label "instalacja"
  ]
  node [
    id 228
    label "net"
  ]
  node [
    id 229
    label "plecionka"
  ]
  node [
    id 230
    label "rozmieszczenie"
  ]
  node [
    id 231
    label "uzyskiwa&#263;"
  ]
  node [
    id 232
    label "u&#380;ywa&#263;"
  ]
  node [
    id 233
    label "ch&#322;opstwo"
  ]
  node [
    id 234
    label "innowierstwo"
  ]
  node [
    id 235
    label "grupa_organizm&#243;w"
  ]
  node [
    id 236
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 237
    label "oldboj"
  ]
  node [
    id 238
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 239
    label "niepracuj&#261;cy"
  ]
  node [
    id 240
    label "&#347;wiadczeniobiorca"
  ]
  node [
    id 241
    label "wapniak"
  ]
  node [
    id 242
    label "dwun&#243;g"
  ]
  node [
    id 243
    label "polifag"
  ]
  node [
    id 244
    label "wz&#243;r"
  ]
  node [
    id 245
    label "profanum"
  ]
  node [
    id 246
    label "hominid"
  ]
  node [
    id 247
    label "homo_sapiens"
  ]
  node [
    id 248
    label "nasada"
  ]
  node [
    id 249
    label "podw&#322;adny"
  ]
  node [
    id 250
    label "ludzko&#347;&#263;"
  ]
  node [
    id 251
    label "os&#322;abianie"
  ]
  node [
    id 252
    label "mikrokosmos"
  ]
  node [
    id 253
    label "portrecista"
  ]
  node [
    id 254
    label "duch"
  ]
  node [
    id 255
    label "oddzia&#322;ywanie"
  ]
  node [
    id 256
    label "g&#322;owa"
  ]
  node [
    id 257
    label "osoba"
  ]
  node [
    id 258
    label "os&#322;abia&#263;"
  ]
  node [
    id 259
    label "figura"
  ]
  node [
    id 260
    label "Adam"
  ]
  node [
    id 261
    label "senior"
  ]
  node [
    id 262
    label "antropochoria"
  ]
  node [
    id 263
    label "posta&#263;"
  ]
  node [
    id 264
    label "stara"
  ]
  node [
    id 265
    label "rodzice"
  ]
  node [
    id 266
    label "doros&#322;y"
  ]
  node [
    id 267
    label "wiele"
  ]
  node [
    id 268
    label "dorodny"
  ]
  node [
    id 269
    label "znaczny"
  ]
  node [
    id 270
    label "du&#380;o"
  ]
  node [
    id 271
    label "prawdziwy"
  ]
  node [
    id 272
    label "niema&#322;o"
  ]
  node [
    id 273
    label "rozwini&#281;ty"
  ]
  node [
    id 274
    label "gamo&#324;"
  ]
  node [
    id 275
    label "istota_&#380;ywa"
  ]
  node [
    id 276
    label "dupa_wo&#322;owa"
  ]
  node [
    id 277
    label "przedmiot"
  ]
  node [
    id 278
    label "pastwa"
  ]
  node [
    id 279
    label "rzecz"
  ]
  node [
    id 280
    label "zbi&#243;rka"
  ]
  node [
    id 281
    label "dar"
  ]
  node [
    id 282
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 283
    label "nastawienie"
  ]
  node [
    id 284
    label "crack"
  ]
  node [
    id 285
    label "ko&#347;cielny"
  ]
  node [
    id 286
    label "po&#347;miewisko"
  ]
  node [
    id 287
    label "cyfrowo"
  ]
  node [
    id 288
    label "sygna&#322;_cyfrowy"
  ]
  node [
    id 289
    label "elektroniczny"
  ]
  node [
    id 290
    label "cyfryzacja"
  ]
  node [
    id 291
    label "challenge"
  ]
  node [
    id 292
    label "odrzuci&#263;"
  ]
  node [
    id 293
    label "spowodowa&#263;"
  ]
  node [
    id 294
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 295
    label "nieprawdopodobie&#324;stwo"
  ]
  node [
    id 296
    label "wykluczenie"
  ]
  node [
    id 297
    label "barroom"
  ]
  node [
    id 298
    label "silny"
  ]
  node [
    id 299
    label "wa&#380;nie"
  ]
  node [
    id 300
    label "eksponowany"
  ]
  node [
    id 301
    label "istotnie"
  ]
  node [
    id 302
    label "dobry"
  ]
  node [
    id 303
    label "wynios&#322;y"
  ]
  node [
    id 304
    label "dono&#347;ny"
  ]
  node [
    id 305
    label "iloczyn"
  ]
  node [
    id 306
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 307
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 308
    label "ekspozycja"
  ]
  node [
    id 309
    label "agent"
  ]
  node [
    id 310
    label "divisor"
  ]
  node [
    id 311
    label "faktor"
  ]
  node [
    id 312
    label "uruchomienie"
  ]
  node [
    id 313
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 314
    label "nauka_ekonomiczna"
  ]
  node [
    id 315
    label "absolutorium"
  ]
  node [
    id 316
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 317
    label "podupada&#263;"
  ]
  node [
    id 318
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 319
    label "supernadz&#243;r"
  ]
  node [
    id 320
    label "nap&#322;ywanie"
  ]
  node [
    id 321
    label "podupadanie"
  ]
  node [
    id 322
    label "kwestor"
  ]
  node [
    id 323
    label "uruchamia&#263;"
  ]
  node [
    id 324
    label "mienie"
  ]
  node [
    id 325
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 326
    label "uruchamianie"
  ]
  node [
    id 327
    label "czynnik_produkcji"
  ]
  node [
    id 328
    label "piwo"
  ]
  node [
    id 329
    label "take_care"
  ]
  node [
    id 330
    label "troska&#263;_si&#281;"
  ]
  node [
    id 331
    label "os&#261;dza&#263;"
  ]
  node [
    id 332
    label "argue"
  ]
  node [
    id 333
    label "rozpatrywa&#263;"
  ]
  node [
    id 334
    label "deliver"
  ]
  node [
    id 335
    label "wniwecz"
  ]
  node [
    id 336
    label "zupe&#322;ny"
  ]
  node [
    id 337
    label "ki&#347;&#263;"
  ]
  node [
    id 338
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 339
    label "krzew"
  ]
  node [
    id 340
    label "pi&#380;maczkowate"
  ]
  node [
    id 341
    label "pestkowiec"
  ]
  node [
    id 342
    label "kwiat"
  ]
  node [
    id 343
    label "owoc"
  ]
  node [
    id 344
    label "oliwkowate"
  ]
  node [
    id 345
    label "hy&#263;ka"
  ]
  node [
    id 346
    label "lilac"
  ]
  node [
    id 347
    label "delfinidyna"
  ]
  node [
    id 348
    label "gravity"
  ]
  node [
    id 349
    label "okre&#347;lanie"
  ]
  node [
    id 350
    label "liczenie"
  ]
  node [
    id 351
    label "odgrywanie_roli"
  ]
  node [
    id 352
    label "wskazywanie"
  ]
  node [
    id 353
    label "bycie"
  ]
  node [
    id 354
    label "weight"
  ]
  node [
    id 355
    label "command"
  ]
  node [
    id 356
    label "istota"
  ]
  node [
    id 357
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 358
    label "informacja"
  ]
  node [
    id 359
    label "odk&#322;adanie"
  ]
  node [
    id 360
    label "wyra&#380;enie"
  ]
  node [
    id 361
    label "wyraz"
  ]
  node [
    id 362
    label "assay"
  ]
  node [
    id 363
    label "condition"
  ]
  node [
    id 364
    label "kto&#347;"
  ]
  node [
    id 365
    label "stawianie"
  ]
  node [
    id 366
    label "zareagowanie"
  ]
  node [
    id 367
    label "opanowanie"
  ]
  node [
    id 368
    label "doznanie"
  ]
  node [
    id 369
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 370
    label "intuition"
  ]
  node [
    id 371
    label "wiedza"
  ]
  node [
    id 372
    label "ekstraspekcja"
  ]
  node [
    id 373
    label "os&#322;upienie"
  ]
  node [
    id 374
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 375
    label "smell"
  ]
  node [
    id 376
    label "zdarzenie_si&#281;"
  ]
  node [
    id 377
    label "feeling"
  ]
  node [
    id 378
    label "czyj&#347;"
  ]
  node [
    id 379
    label "m&#261;&#380;"
  ]
  node [
    id 380
    label "cia&#322;o"
  ]
  node [
    id 381
    label "plac"
  ]
  node [
    id 382
    label "uwaga"
  ]
  node [
    id 383
    label "przestrze&#324;"
  ]
  node [
    id 384
    label "status"
  ]
  node [
    id 385
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 386
    label "chwila"
  ]
  node [
    id 387
    label "rz&#261;d"
  ]
  node [
    id 388
    label "praca"
  ]
  node [
    id 389
    label "location"
  ]
  node [
    id 390
    label "warunek_lokalowy"
  ]
  node [
    id 391
    label "wzbudzi&#263;"
  ]
  node [
    id 392
    label "interest"
  ]
  node [
    id 393
    label "rozciekawi&#263;"
  ]
  node [
    id 394
    label "lacki"
  ]
  node [
    id 395
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 396
    label "sztajer"
  ]
  node [
    id 397
    label "drabant"
  ]
  node [
    id 398
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 399
    label "pierogi_ruskie"
  ]
  node [
    id 400
    label "krakowiak"
  ]
  node [
    id 401
    label "Polish"
  ]
  node [
    id 402
    label "j&#281;zyk"
  ]
  node [
    id 403
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 404
    label "oberek"
  ]
  node [
    id 405
    label "po_polsku"
  ]
  node [
    id 406
    label "mazur"
  ]
  node [
    id 407
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 408
    label "chodzony"
  ]
  node [
    id 409
    label "skoczny"
  ]
  node [
    id 410
    label "ryba_po_grecku"
  ]
  node [
    id 411
    label "goniony"
  ]
  node [
    id 412
    label "polsko"
  ]
  node [
    id 413
    label "uk&#322;ad"
  ]
  node [
    id 414
    label "paj&#281;czarz"
  ]
  node [
    id 415
    label "fala_radiowa"
  ]
  node [
    id 416
    label "spot"
  ]
  node [
    id 417
    label "programowiec"
  ]
  node [
    id 418
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 419
    label "eliminator"
  ]
  node [
    id 420
    label "studio"
  ]
  node [
    id 421
    label "radiola"
  ]
  node [
    id 422
    label "redakcja"
  ]
  node [
    id 423
    label "odbieranie"
  ]
  node [
    id 424
    label "dyskryminator"
  ]
  node [
    id 425
    label "odbiera&#263;"
  ]
  node [
    id 426
    label "odbiornik"
  ]
  node [
    id 427
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 428
    label "stacja"
  ]
  node [
    id 429
    label "radiolinia"
  ]
  node [
    id 430
    label "radiofonia"
  ]
  node [
    id 431
    label "okre&#347;lony"
  ]
  node [
    id 432
    label "device"
  ]
  node [
    id 433
    label "program_u&#380;ytkowy"
  ]
  node [
    id 434
    label "intencja"
  ]
  node [
    id 435
    label "agreement"
  ]
  node [
    id 436
    label "pomys&#322;"
  ]
  node [
    id 437
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 438
    label "plan"
  ]
  node [
    id 439
    label "dokumentacja"
  ]
  node [
    id 440
    label "nowoczesny"
  ]
  node [
    id 441
    label "sieciowo"
  ]
  node [
    id 442
    label "netowy"
  ]
  node [
    id 443
    label "internetowo"
  ]
  node [
    id 444
    label "byd&#322;o"
  ]
  node [
    id 445
    label "zobo"
  ]
  node [
    id 446
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 447
    label "yakalo"
  ]
  node [
    id 448
    label "dzo"
  ]
  node [
    id 449
    label "powie&#347;&#263;"
  ]
  node [
    id 450
    label "opowie&#347;&#263;"
  ]
  node [
    id 451
    label "skusi&#263;"
  ]
  node [
    id 452
    label "draw"
  ]
  node [
    id 453
    label "zbli&#380;y&#263;"
  ]
  node [
    id 454
    label "whole"
  ]
  node [
    id 455
    label "Rzym_Zachodni"
  ]
  node [
    id 456
    label "element"
  ]
  node [
    id 457
    label "ilo&#347;&#263;"
  ]
  node [
    id 458
    label "urz&#261;dzenie"
  ]
  node [
    id 459
    label "Rzym_Wschodni"
  ]
  node [
    id 460
    label "Maryja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 13
    target 39
  ]
  edge [
    source 13
    target 48
  ]
  edge [
    source 13
    target 55
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 18
    target 195
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 50
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 21
    target 206
  ]
  edge [
    source 21
    target 207
  ]
  edge [
    source 21
    target 208
  ]
  edge [
    source 21
    target 209
  ]
  edge [
    source 21
    target 210
  ]
  edge [
    source 21
    target 211
  ]
  edge [
    source 21
    target 212
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 101
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 23
    target 104
  ]
  edge [
    source 23
    target 108
  ]
  edge [
    source 23
    target 110
  ]
  edge [
    source 23
    target 99
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 23
    target 218
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 23
    target 170
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 109
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 23
    target 98
  ]
  edge [
    source 23
    target 100
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 103
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 107
  ]
  edge [
    source 23
    target 111
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 112
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 102
  ]
  edge [
    source 23
    target 227
  ]
  edge [
    source 23
    target 105
  ]
  edge [
    source 23
    target 228
  ]
  edge [
    source 23
    target 229
  ]
  edge [
    source 23
    target 106
  ]
  edge [
    source 23
    target 230
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 154
  ]
  edge [
    source 24
    target 231
  ]
  edge [
    source 24
    target 232
  ]
  edge [
    source 25
    target 233
  ]
  edge [
    source 25
    target 234
  ]
  edge [
    source 25
    target 191
  ]
  edge [
    source 25
    target 235
  ]
  edge [
    source 25
    target 236
  ]
  edge [
    source 25
    target 39
  ]
  edge [
    source 25
    target 48
  ]
  edge [
    source 25
    target 55
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 50
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 239
  ]
  edge [
    source 26
    target 240
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 60
  ]
  edge [
    source 27
    target 165
  ]
  edge [
    source 27
    target 241
  ]
  edge [
    source 27
    target 242
  ]
  edge [
    source 27
    target 243
  ]
  edge [
    source 27
    target 244
  ]
  edge [
    source 27
    target 245
  ]
  edge [
    source 27
    target 246
  ]
  edge [
    source 27
    target 247
  ]
  edge [
    source 27
    target 248
  ]
  edge [
    source 27
    target 249
  ]
  edge [
    source 27
    target 250
  ]
  edge [
    source 27
    target 251
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 253
  ]
  edge [
    source 27
    target 254
  ]
  edge [
    source 27
    target 255
  ]
  edge [
    source 27
    target 256
  ]
  edge [
    source 27
    target 183
  ]
  edge [
    source 27
    target 257
  ]
  edge [
    source 27
    target 258
  ]
  edge [
    source 27
    target 259
  ]
  edge [
    source 27
    target 260
  ]
  edge [
    source 27
    target 261
  ]
  edge [
    source 27
    target 262
  ]
  edge [
    source 27
    target 263
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 28
    target 43
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 28
    target 264
  ]
  edge [
    source 28
    target 265
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 266
  ]
  edge [
    source 29
    target 267
  ]
  edge [
    source 29
    target 268
  ]
  edge [
    source 29
    target 269
  ]
  edge [
    source 29
    target 270
  ]
  edge [
    source 29
    target 271
  ]
  edge [
    source 29
    target 272
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 29
    target 273
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 274
  ]
  edge [
    source 30
    target 275
  ]
  edge [
    source 30
    target 276
  ]
  edge [
    source 30
    target 277
  ]
  edge [
    source 30
    target 278
  ]
  edge [
    source 30
    target 279
  ]
  edge [
    source 30
    target 280
  ]
  edge [
    source 30
    target 281
  ]
  edge [
    source 30
    target 282
  ]
  edge [
    source 30
    target 283
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 147
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 287
  ]
  edge [
    source 31
    target 288
  ]
  edge [
    source 31
    target 289
  ]
  edge [
    source 31
    target 290
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 291
  ]
  edge [
    source 32
    target 292
  ]
  edge [
    source 32
    target 293
  ]
  edge [
    source 32
    target 294
  ]
  edge [
    source 32
    target 295
  ]
  edge [
    source 32
    target 196
  ]
  edge [
    source 32
    target 296
  ]
  edge [
    source 32
    target 297
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 298
  ]
  edge [
    source 34
    target 299
  ]
  edge [
    source 34
    target 300
  ]
  edge [
    source 34
    target 301
  ]
  edge [
    source 34
    target 269
  ]
  edge [
    source 34
    target 302
  ]
  edge [
    source 34
    target 303
  ]
  edge [
    source 34
    target 304
  ]
  edge [
    source 35
    target 305
  ]
  edge [
    source 35
    target 306
  ]
  edge [
    source 35
    target 307
  ]
  edge [
    source 35
    target 308
  ]
  edge [
    source 35
    target 309
  ]
  edge [
    source 35
    target 310
  ]
  edge [
    source 35
    target 311
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 312
  ]
  edge [
    source 36
    target 313
  ]
  edge [
    source 36
    target 314
  ]
  edge [
    source 36
    target 315
  ]
  edge [
    source 36
    target 316
  ]
  edge [
    source 36
    target 317
  ]
  edge [
    source 36
    target 318
  ]
  edge [
    source 36
    target 319
  ]
  edge [
    source 36
    target 320
  ]
  edge [
    source 36
    target 321
  ]
  edge [
    source 36
    target 322
  ]
  edge [
    source 36
    target 323
  ]
  edge [
    source 36
    target 324
  ]
  edge [
    source 36
    target 325
  ]
  edge [
    source 36
    target 326
  ]
  edge [
    source 36
    target 327
  ]
  edge [
    source 36
    target 60
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 52
  ]
  edge [
    source 37
    target 53
  ]
  edge [
    source 37
    target 328
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 329
  ]
  edge [
    source 38
    target 330
  ]
  edge [
    source 38
    target 78
  ]
  edge [
    source 38
    target 331
  ]
  edge [
    source 38
    target 84
  ]
  edge [
    source 38
    target 332
  ]
  edge [
    source 38
    target 333
  ]
  edge [
    source 38
    target 334
  ]
  edge [
    source 38
    target 59
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 335
  ]
  edge [
    source 39
    target 336
  ]
  edge [
    source 39
    target 48
  ]
  edge [
    source 39
    target 55
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 337
  ]
  edge [
    source 40
    target 338
  ]
  edge [
    source 40
    target 339
  ]
  edge [
    source 40
    target 340
  ]
  edge [
    source 40
    target 341
  ]
  edge [
    source 40
    target 342
  ]
  edge [
    source 40
    target 343
  ]
  edge [
    source 40
    target 344
  ]
  edge [
    source 40
    target 195
  ]
  edge [
    source 40
    target 345
  ]
  edge [
    source 40
    target 346
  ]
  edge [
    source 40
    target 347
  ]
  edge [
    source 41
    target 348
  ]
  edge [
    source 41
    target 349
  ]
  edge [
    source 41
    target 350
  ]
  edge [
    source 41
    target 351
  ]
  edge [
    source 41
    target 352
  ]
  edge [
    source 41
    target 353
  ]
  edge [
    source 41
    target 354
  ]
  edge [
    source 41
    target 355
  ]
  edge [
    source 41
    target 356
  ]
  edge [
    source 41
    target 206
  ]
  edge [
    source 41
    target 357
  ]
  edge [
    source 41
    target 358
  ]
  edge [
    source 41
    target 359
  ]
  edge [
    source 41
    target 360
  ]
  edge [
    source 41
    target 361
  ]
  edge [
    source 41
    target 362
  ]
  edge [
    source 41
    target 363
  ]
  edge [
    source 41
    target 364
  ]
  edge [
    source 41
    target 365
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 366
  ]
  edge [
    source 43
    target 367
  ]
  edge [
    source 43
    target 368
  ]
  edge [
    source 43
    target 369
  ]
  edge [
    source 43
    target 370
  ]
  edge [
    source 43
    target 371
  ]
  edge [
    source 43
    target 372
  ]
  edge [
    source 43
    target 373
  ]
  edge [
    source 43
    target 374
  ]
  edge [
    source 43
    target 375
  ]
  edge [
    source 43
    target 376
  ]
  edge [
    source 43
    target 377
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 378
  ]
  edge [
    source 45
    target 379
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 380
  ]
  edge [
    source 46
    target 381
  ]
  edge [
    source 46
    target 206
  ]
  edge [
    source 46
    target 382
  ]
  edge [
    source 46
    target 383
  ]
  edge [
    source 46
    target 384
  ]
  edge [
    source 46
    target 385
  ]
  edge [
    source 46
    target 386
  ]
  edge [
    source 46
    target 59
  ]
  edge [
    source 46
    target 387
  ]
  edge [
    source 46
    target 388
  ]
  edge [
    source 46
    target 389
  ]
  edge [
    source 46
    target 390
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 57
  ]
  edge [
    source 48
    target 58
  ]
  edge [
    source 48
    target 55
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 391
  ]
  edge [
    source 49
    target 392
  ]
  edge [
    source 49
    target 393
  ]
  edge [
    source 50
    target 394
  ]
  edge [
    source 50
    target 395
  ]
  edge [
    source 50
    target 277
  ]
  edge [
    source 50
    target 396
  ]
  edge [
    source 50
    target 397
  ]
  edge [
    source 50
    target 398
  ]
  edge [
    source 50
    target 399
  ]
  edge [
    source 50
    target 400
  ]
  edge [
    source 50
    target 401
  ]
  edge [
    source 50
    target 402
  ]
  edge [
    source 50
    target 403
  ]
  edge [
    source 50
    target 404
  ]
  edge [
    source 50
    target 405
  ]
  edge [
    source 50
    target 406
  ]
  edge [
    source 50
    target 407
  ]
  edge [
    source 50
    target 408
  ]
  edge [
    source 50
    target 409
  ]
  edge [
    source 50
    target 410
  ]
  edge [
    source 50
    target 411
  ]
  edge [
    source 50
    target 412
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 413
  ]
  edge [
    source 51
    target 414
  ]
  edge [
    source 51
    target 415
  ]
  edge [
    source 51
    target 416
  ]
  edge [
    source 51
    target 417
  ]
  edge [
    source 51
    target 418
  ]
  edge [
    source 51
    target 419
  ]
  edge [
    source 51
    target 420
  ]
  edge [
    source 51
    target 421
  ]
  edge [
    source 51
    target 422
  ]
  edge [
    source 51
    target 423
  ]
  edge [
    source 51
    target 424
  ]
  edge [
    source 51
    target 425
  ]
  edge [
    source 51
    target 426
  ]
  edge [
    source 51
    target 106
  ]
  edge [
    source 51
    target 427
  ]
  edge [
    source 51
    target 428
  ]
  edge [
    source 51
    target 429
  ]
  edge [
    source 51
    target 430
  ]
  edge [
    source 51
    target 460
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 431
  ]
  edge [
    source 53
    target 163
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 61
  ]
  edge [
    source 54
    target 432
  ]
  edge [
    source 54
    target 433
  ]
  edge [
    source 54
    target 434
  ]
  edge [
    source 54
    target 435
  ]
  edge [
    source 54
    target 436
  ]
  edge [
    source 54
    target 437
  ]
  edge [
    source 54
    target 438
  ]
  edge [
    source 54
    target 439
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 440
  ]
  edge [
    source 55
    target 289
  ]
  edge [
    source 55
    target 441
  ]
  edge [
    source 55
    target 442
  ]
  edge [
    source 55
    target 443
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 444
  ]
  edge [
    source 56
    target 445
  ]
  edge [
    source 56
    target 446
  ]
  edge [
    source 56
    target 447
  ]
  edge [
    source 56
    target 448
  ]
  edge [
    source 57
    target 449
  ]
  edge [
    source 57
    target 450
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 451
  ]
  edge [
    source 58
    target 452
  ]
  edge [
    source 58
    target 453
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 454
  ]
  edge [
    source 59
    target 455
  ]
  edge [
    source 59
    target 456
  ]
  edge [
    source 59
    target 457
  ]
  edge [
    source 59
    target 458
  ]
  edge [
    source 59
    target 459
  ]
]
