graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "mirka"
    origin "text"
  ]
  node [
    id 1
    label "syn"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "rodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "usynowienie"
  ]
  node [
    id 5
    label "usynawianie"
  ]
  node [
    id 6
    label "dziecko"
  ]
  node [
    id 7
    label "give"
  ]
  node [
    id 8
    label "create"
  ]
  node [
    id 9
    label "plon"
  ]
  node [
    id 10
    label "wytwarza&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
]
