graph [
  maxDegree 324
  minDegree 1
  meanDegree 1.994186046511628
  density 0.005813953488372093
  graphCliqueNumber 2
  node [
    id 0
    label "przewodzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kraj"
    origin "text"
  ]
  node [
    id 2
    label "bit"
    origin "text"
  ]
  node [
    id 3
    label "zsrr"
    origin "text"
  ]
  node [
    id 4
    label "neuron"
  ]
  node [
    id 5
    label "control"
  ]
  node [
    id 6
    label "bodziec"
  ]
  node [
    id 7
    label "prowadzi&#263;"
  ]
  node [
    id 8
    label "manipulate"
  ]
  node [
    id 9
    label "przepuszcza&#263;"
  ]
  node [
    id 10
    label "preside"
  ]
  node [
    id 11
    label "&#322;yko"
  ]
  node [
    id 12
    label "doprowadza&#263;"
  ]
  node [
    id 13
    label "przekazywa&#263;"
  ]
  node [
    id 14
    label "Skandynawia"
  ]
  node [
    id 15
    label "Filipiny"
  ]
  node [
    id 16
    label "Rwanda"
  ]
  node [
    id 17
    label "Kaukaz"
  ]
  node [
    id 18
    label "Kaszmir"
  ]
  node [
    id 19
    label "Toskania"
  ]
  node [
    id 20
    label "Yorkshire"
  ]
  node [
    id 21
    label "&#321;emkowszczyzna"
  ]
  node [
    id 22
    label "obszar"
  ]
  node [
    id 23
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 24
    label "Monako"
  ]
  node [
    id 25
    label "Amhara"
  ]
  node [
    id 26
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 27
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 28
    label "Lombardia"
  ]
  node [
    id 29
    label "Korea"
  ]
  node [
    id 30
    label "Kalabria"
  ]
  node [
    id 31
    label "Ghana"
  ]
  node [
    id 32
    label "Czarnog&#243;ra"
  ]
  node [
    id 33
    label "Tyrol"
  ]
  node [
    id 34
    label "Malawi"
  ]
  node [
    id 35
    label "Indonezja"
  ]
  node [
    id 36
    label "Bu&#322;garia"
  ]
  node [
    id 37
    label "Nauru"
  ]
  node [
    id 38
    label "Kenia"
  ]
  node [
    id 39
    label "Pamir"
  ]
  node [
    id 40
    label "Kambod&#380;a"
  ]
  node [
    id 41
    label "Lubelszczyzna"
  ]
  node [
    id 42
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 43
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 44
    label "Mali"
  ]
  node [
    id 45
    label "&#379;ywiecczyzna"
  ]
  node [
    id 46
    label "Austria"
  ]
  node [
    id 47
    label "interior"
  ]
  node [
    id 48
    label "Europa_Wschodnia"
  ]
  node [
    id 49
    label "Armenia"
  ]
  node [
    id 50
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 51
    label "Fid&#380;i"
  ]
  node [
    id 52
    label "Tuwalu"
  ]
  node [
    id 53
    label "Zabajkale"
  ]
  node [
    id 54
    label "Etiopia"
  ]
  node [
    id 55
    label "Malta"
  ]
  node [
    id 56
    label "Malezja"
  ]
  node [
    id 57
    label "Kaszuby"
  ]
  node [
    id 58
    label "Bo&#347;nia"
  ]
  node [
    id 59
    label "Noworosja"
  ]
  node [
    id 60
    label "Grenada"
  ]
  node [
    id 61
    label "Tad&#380;ykistan"
  ]
  node [
    id 62
    label "Ba&#322;kany"
  ]
  node [
    id 63
    label "Wehrlen"
  ]
  node [
    id 64
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 65
    label "Anglia"
  ]
  node [
    id 66
    label "Kielecczyzna"
  ]
  node [
    id 67
    label "Rumunia"
  ]
  node [
    id 68
    label "Pomorze_Zachodnie"
  ]
  node [
    id 69
    label "Maroko"
  ]
  node [
    id 70
    label "Bhutan"
  ]
  node [
    id 71
    label "Opolskie"
  ]
  node [
    id 72
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 73
    label "Ko&#322;yma"
  ]
  node [
    id 74
    label "Oksytania"
  ]
  node [
    id 75
    label "S&#322;owacja"
  ]
  node [
    id 76
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 77
    label "Seszele"
  ]
  node [
    id 78
    label "Syjon"
  ]
  node [
    id 79
    label "Kuwejt"
  ]
  node [
    id 80
    label "Arabia_Saudyjska"
  ]
  node [
    id 81
    label "Kociewie"
  ]
  node [
    id 82
    label "Ekwador"
  ]
  node [
    id 83
    label "Kanada"
  ]
  node [
    id 84
    label "ziemia"
  ]
  node [
    id 85
    label "Japonia"
  ]
  node [
    id 86
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 87
    label "Hiszpania"
  ]
  node [
    id 88
    label "Wyspy_Marshalla"
  ]
  node [
    id 89
    label "Botswana"
  ]
  node [
    id 90
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 91
    label "D&#380;ibuti"
  ]
  node [
    id 92
    label "Huculszczyzna"
  ]
  node [
    id 93
    label "Wietnam"
  ]
  node [
    id 94
    label "Egipt"
  ]
  node [
    id 95
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 96
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 97
    label "Burkina_Faso"
  ]
  node [
    id 98
    label "Bawaria"
  ]
  node [
    id 99
    label "Niemcy"
  ]
  node [
    id 100
    label "Khitai"
  ]
  node [
    id 101
    label "Macedonia"
  ]
  node [
    id 102
    label "Albania"
  ]
  node [
    id 103
    label "Madagaskar"
  ]
  node [
    id 104
    label "Bahrajn"
  ]
  node [
    id 105
    label "Jemen"
  ]
  node [
    id 106
    label "Lesoto"
  ]
  node [
    id 107
    label "Maghreb"
  ]
  node [
    id 108
    label "Samoa"
  ]
  node [
    id 109
    label "Andora"
  ]
  node [
    id 110
    label "Bory_Tucholskie"
  ]
  node [
    id 111
    label "Chiny"
  ]
  node [
    id 112
    label "Europa_Zachodnia"
  ]
  node [
    id 113
    label "Cypr"
  ]
  node [
    id 114
    label "Wielka_Brytania"
  ]
  node [
    id 115
    label "Kerala"
  ]
  node [
    id 116
    label "Podhale"
  ]
  node [
    id 117
    label "Kabylia"
  ]
  node [
    id 118
    label "Ukraina"
  ]
  node [
    id 119
    label "Paragwaj"
  ]
  node [
    id 120
    label "Trynidad_i_Tobago"
  ]
  node [
    id 121
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 122
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 123
    label "Ma&#322;opolska"
  ]
  node [
    id 124
    label "Polesie"
  ]
  node [
    id 125
    label "Liguria"
  ]
  node [
    id 126
    label "Libia"
  ]
  node [
    id 127
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 128
    label "&#321;&#243;dzkie"
  ]
  node [
    id 129
    label "Surinam"
  ]
  node [
    id 130
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 131
    label "Palestyna"
  ]
  node [
    id 132
    label "Australia"
  ]
  node [
    id 133
    label "Nigeria"
  ]
  node [
    id 134
    label "Honduras"
  ]
  node [
    id 135
    label "Bojkowszczyzna"
  ]
  node [
    id 136
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 137
    label "Karaiby"
  ]
  node [
    id 138
    label "Bangladesz"
  ]
  node [
    id 139
    label "Peru"
  ]
  node [
    id 140
    label "Kazachstan"
  ]
  node [
    id 141
    label "USA"
  ]
  node [
    id 142
    label "Irak"
  ]
  node [
    id 143
    label "Nepal"
  ]
  node [
    id 144
    label "S&#261;decczyzna"
  ]
  node [
    id 145
    label "Sudan"
  ]
  node [
    id 146
    label "Sand&#380;ak"
  ]
  node [
    id 147
    label "Nadrenia"
  ]
  node [
    id 148
    label "San_Marino"
  ]
  node [
    id 149
    label "Burundi"
  ]
  node [
    id 150
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 151
    label "Dominikana"
  ]
  node [
    id 152
    label "Komory"
  ]
  node [
    id 153
    label "Zakarpacie"
  ]
  node [
    id 154
    label "Gwatemala"
  ]
  node [
    id 155
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 156
    label "Zag&#243;rze"
  ]
  node [
    id 157
    label "Andaluzja"
  ]
  node [
    id 158
    label "granica_pa&#324;stwa"
  ]
  node [
    id 159
    label "Turkiestan"
  ]
  node [
    id 160
    label "Naddniestrze"
  ]
  node [
    id 161
    label "Hercegowina"
  ]
  node [
    id 162
    label "Brunei"
  ]
  node [
    id 163
    label "Iran"
  ]
  node [
    id 164
    label "jednostka_administracyjna"
  ]
  node [
    id 165
    label "Zimbabwe"
  ]
  node [
    id 166
    label "Namibia"
  ]
  node [
    id 167
    label "Meksyk"
  ]
  node [
    id 168
    label "Lotaryngia"
  ]
  node [
    id 169
    label "Kamerun"
  ]
  node [
    id 170
    label "Opolszczyzna"
  ]
  node [
    id 171
    label "Afryka_Wschodnia"
  ]
  node [
    id 172
    label "Szlezwik"
  ]
  node [
    id 173
    label "Somalia"
  ]
  node [
    id 174
    label "Angola"
  ]
  node [
    id 175
    label "Gabon"
  ]
  node [
    id 176
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 177
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 178
    label "Mozambik"
  ]
  node [
    id 179
    label "Tajwan"
  ]
  node [
    id 180
    label "Tunezja"
  ]
  node [
    id 181
    label "Nowa_Zelandia"
  ]
  node [
    id 182
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 183
    label "Podbeskidzie"
  ]
  node [
    id 184
    label "Liban"
  ]
  node [
    id 185
    label "Jordania"
  ]
  node [
    id 186
    label "Tonga"
  ]
  node [
    id 187
    label "Czad"
  ]
  node [
    id 188
    label "Liberia"
  ]
  node [
    id 189
    label "Gwinea"
  ]
  node [
    id 190
    label "Belize"
  ]
  node [
    id 191
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 192
    label "Mazowsze"
  ]
  node [
    id 193
    label "&#321;otwa"
  ]
  node [
    id 194
    label "Syria"
  ]
  node [
    id 195
    label "Benin"
  ]
  node [
    id 196
    label "Afryka_Zachodnia"
  ]
  node [
    id 197
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 198
    label "Dominika"
  ]
  node [
    id 199
    label "Antigua_i_Barbuda"
  ]
  node [
    id 200
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 201
    label "Hanower"
  ]
  node [
    id 202
    label "Galicja"
  ]
  node [
    id 203
    label "Szkocja"
  ]
  node [
    id 204
    label "Walia"
  ]
  node [
    id 205
    label "Afganistan"
  ]
  node [
    id 206
    label "Kiribati"
  ]
  node [
    id 207
    label "W&#322;ochy"
  ]
  node [
    id 208
    label "Szwajcaria"
  ]
  node [
    id 209
    label "Powi&#347;le"
  ]
  node [
    id 210
    label "Sahara_Zachodnia"
  ]
  node [
    id 211
    label "Chorwacja"
  ]
  node [
    id 212
    label "Tajlandia"
  ]
  node [
    id 213
    label "Salwador"
  ]
  node [
    id 214
    label "Bahamy"
  ]
  node [
    id 215
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 216
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 217
    label "Zamojszczyzna"
  ]
  node [
    id 218
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 219
    label "S&#322;owenia"
  ]
  node [
    id 220
    label "Gambia"
  ]
  node [
    id 221
    label "Kujawy"
  ]
  node [
    id 222
    label "Urugwaj"
  ]
  node [
    id 223
    label "Podlasie"
  ]
  node [
    id 224
    label "Zair"
  ]
  node [
    id 225
    label "Erytrea"
  ]
  node [
    id 226
    label "Laponia"
  ]
  node [
    id 227
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 228
    label "Umbria"
  ]
  node [
    id 229
    label "Rosja"
  ]
  node [
    id 230
    label "Uganda"
  ]
  node [
    id 231
    label "Niger"
  ]
  node [
    id 232
    label "Mauritius"
  ]
  node [
    id 233
    label "Turkmenistan"
  ]
  node [
    id 234
    label "Turcja"
  ]
  node [
    id 235
    label "Mezoameryka"
  ]
  node [
    id 236
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 237
    label "Irlandia"
  ]
  node [
    id 238
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 239
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 240
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 241
    label "Gwinea_Bissau"
  ]
  node [
    id 242
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 243
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 244
    label "Kurdystan"
  ]
  node [
    id 245
    label "Belgia"
  ]
  node [
    id 246
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 247
    label "Palau"
  ]
  node [
    id 248
    label "Barbados"
  ]
  node [
    id 249
    label "Chile"
  ]
  node [
    id 250
    label "Wenezuela"
  ]
  node [
    id 251
    label "W&#281;gry"
  ]
  node [
    id 252
    label "Argentyna"
  ]
  node [
    id 253
    label "Kolumbia"
  ]
  node [
    id 254
    label "Kampania"
  ]
  node [
    id 255
    label "Armagnac"
  ]
  node [
    id 256
    label "Sierra_Leone"
  ]
  node [
    id 257
    label "Azerbejd&#380;an"
  ]
  node [
    id 258
    label "Kongo"
  ]
  node [
    id 259
    label "Polinezja"
  ]
  node [
    id 260
    label "Warmia"
  ]
  node [
    id 261
    label "Pakistan"
  ]
  node [
    id 262
    label "Liechtenstein"
  ]
  node [
    id 263
    label "Wielkopolska"
  ]
  node [
    id 264
    label "Nikaragua"
  ]
  node [
    id 265
    label "Senegal"
  ]
  node [
    id 266
    label "brzeg"
  ]
  node [
    id 267
    label "Bordeaux"
  ]
  node [
    id 268
    label "Lauda"
  ]
  node [
    id 269
    label "Indie"
  ]
  node [
    id 270
    label "Mazury"
  ]
  node [
    id 271
    label "Suazi"
  ]
  node [
    id 272
    label "Polska"
  ]
  node [
    id 273
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 274
    label "Algieria"
  ]
  node [
    id 275
    label "Jamajka"
  ]
  node [
    id 276
    label "Timor_Wschodni"
  ]
  node [
    id 277
    label "Oceania"
  ]
  node [
    id 278
    label "Kostaryka"
  ]
  node [
    id 279
    label "Podkarpacie"
  ]
  node [
    id 280
    label "Lasko"
  ]
  node [
    id 281
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 282
    label "Kuba"
  ]
  node [
    id 283
    label "Mauretania"
  ]
  node [
    id 284
    label "Amazonia"
  ]
  node [
    id 285
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 286
    label "Portoryko"
  ]
  node [
    id 287
    label "Brazylia"
  ]
  node [
    id 288
    label "Mo&#322;dawia"
  ]
  node [
    id 289
    label "organizacja"
  ]
  node [
    id 290
    label "Litwa"
  ]
  node [
    id 291
    label "Kirgistan"
  ]
  node [
    id 292
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 293
    label "Izrael"
  ]
  node [
    id 294
    label "Grecja"
  ]
  node [
    id 295
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 296
    label "Kurpie"
  ]
  node [
    id 297
    label "Holandia"
  ]
  node [
    id 298
    label "Sri_Lanka"
  ]
  node [
    id 299
    label "Tonkin"
  ]
  node [
    id 300
    label "Katar"
  ]
  node [
    id 301
    label "Azja_Wschodnia"
  ]
  node [
    id 302
    label "Mikronezja"
  ]
  node [
    id 303
    label "Ukraina_Zachodnia"
  ]
  node [
    id 304
    label "Laos"
  ]
  node [
    id 305
    label "Mongolia"
  ]
  node [
    id 306
    label "Turyngia"
  ]
  node [
    id 307
    label "Malediwy"
  ]
  node [
    id 308
    label "Zambia"
  ]
  node [
    id 309
    label "Baszkiria"
  ]
  node [
    id 310
    label "Tanzania"
  ]
  node [
    id 311
    label "Gujana"
  ]
  node [
    id 312
    label "Apulia"
  ]
  node [
    id 313
    label "Czechy"
  ]
  node [
    id 314
    label "Panama"
  ]
  node [
    id 315
    label "Uzbekistan"
  ]
  node [
    id 316
    label "Gruzja"
  ]
  node [
    id 317
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 318
    label "Serbia"
  ]
  node [
    id 319
    label "Francja"
  ]
  node [
    id 320
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 321
    label "Togo"
  ]
  node [
    id 322
    label "Estonia"
  ]
  node [
    id 323
    label "Indochiny"
  ]
  node [
    id 324
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 325
    label "Oman"
  ]
  node [
    id 326
    label "Boliwia"
  ]
  node [
    id 327
    label "Portugalia"
  ]
  node [
    id 328
    label "Wyspy_Salomona"
  ]
  node [
    id 329
    label "Luksemburg"
  ]
  node [
    id 330
    label "Haiti"
  ]
  node [
    id 331
    label "Biskupizna"
  ]
  node [
    id 332
    label "Lubuskie"
  ]
  node [
    id 333
    label "Birma"
  ]
  node [
    id 334
    label "Rodezja"
  ]
  node [
    id 335
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 336
    label "s&#322;owo_maszynowe"
  ]
  node [
    id 337
    label "system_dw&#243;jkowy"
  ]
  node [
    id 338
    label "rytm"
  ]
  node [
    id 339
    label "bajt"
  ]
  node [
    id 340
    label "p&#243;&#322;bajt"
  ]
  node [
    id 341
    label "cyfra"
  ]
  node [
    id 342
    label "oktet"
  ]
  node [
    id 343
    label "jednostka_informacji"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
]
