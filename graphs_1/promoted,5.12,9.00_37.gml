graph [
  maxDegree 29
  minDegree 1
  meanDegree 2
  density 0.015748031496062992
  graphCliqueNumber 2
  node [
    id 0
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 1
    label "ostatni"
    origin "text"
  ]
  node [
    id 2
    label "trzy"
    origin "text"
  ]
  node [
    id 3
    label "lato"
    origin "text"
  ]
  node [
    id 4
    label "liczba"
    origin "text"
  ]
  node [
    id 5
    label "firma"
    origin "text"
  ]
  node [
    id 6
    label "polski"
    origin "text"
  ]
  node [
    id 7
    label "kapita&#322;"
    origin "text"
  ]
  node [
    id 8
    label "wzros&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "blisko"
    origin "text"
  ]
  node [
    id 10
    label "proca"
    origin "text"
  ]
  node [
    id 11
    label "si&#322;a"
  ]
  node [
    id 12
    label "stan"
  ]
  node [
    id 13
    label "lina"
  ]
  node [
    id 14
    label "way"
  ]
  node [
    id 15
    label "cable"
  ]
  node [
    id 16
    label "przebieg"
  ]
  node [
    id 17
    label "zbi&#243;r"
  ]
  node [
    id 18
    label "ch&#243;d"
  ]
  node [
    id 19
    label "trasa"
  ]
  node [
    id 20
    label "rz&#261;d"
  ]
  node [
    id 21
    label "k&#322;us"
  ]
  node [
    id 22
    label "progression"
  ]
  node [
    id 23
    label "current"
  ]
  node [
    id 24
    label "pr&#261;d"
  ]
  node [
    id 25
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 26
    label "wydarzenie"
  ]
  node [
    id 27
    label "lot"
  ]
  node [
    id 28
    label "cz&#322;owiek"
  ]
  node [
    id 29
    label "kolejny"
  ]
  node [
    id 30
    label "istota_&#380;ywa"
  ]
  node [
    id 31
    label "najgorszy"
  ]
  node [
    id 32
    label "aktualny"
  ]
  node [
    id 33
    label "ostatnio"
  ]
  node [
    id 34
    label "niedawno"
  ]
  node [
    id 35
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 36
    label "sko&#324;czony"
  ]
  node [
    id 37
    label "poprzedni"
  ]
  node [
    id 38
    label "pozosta&#322;y"
  ]
  node [
    id 39
    label "w&#261;tpliwy"
  ]
  node [
    id 40
    label "pora_roku"
  ]
  node [
    id 41
    label "kategoria"
  ]
  node [
    id 42
    label "kategoria_gramatyczna"
  ]
  node [
    id 43
    label "kwadrat_magiczny"
  ]
  node [
    id 44
    label "grupa"
  ]
  node [
    id 45
    label "cecha"
  ]
  node [
    id 46
    label "wyra&#380;enie"
  ]
  node [
    id 47
    label "pierwiastek"
  ]
  node [
    id 48
    label "rozmiar"
  ]
  node [
    id 49
    label "number"
  ]
  node [
    id 50
    label "poj&#281;cie"
  ]
  node [
    id 51
    label "koniugacja"
  ]
  node [
    id 52
    label "MAC"
  ]
  node [
    id 53
    label "Hortex"
  ]
  node [
    id 54
    label "reengineering"
  ]
  node [
    id 55
    label "nazwa_w&#322;asna"
  ]
  node [
    id 56
    label "podmiot_gospodarczy"
  ]
  node [
    id 57
    label "Google"
  ]
  node [
    id 58
    label "zaufanie"
  ]
  node [
    id 59
    label "biurowiec"
  ]
  node [
    id 60
    label "interes"
  ]
  node [
    id 61
    label "zasoby_ludzkie"
  ]
  node [
    id 62
    label "networking"
  ]
  node [
    id 63
    label "paczkarnia"
  ]
  node [
    id 64
    label "Canon"
  ]
  node [
    id 65
    label "HP"
  ]
  node [
    id 66
    label "Baltona"
  ]
  node [
    id 67
    label "Pewex"
  ]
  node [
    id 68
    label "MAN_SE"
  ]
  node [
    id 69
    label "Apeks"
  ]
  node [
    id 70
    label "zasoby"
  ]
  node [
    id 71
    label "Orbis"
  ]
  node [
    id 72
    label "miejsce_pracy"
  ]
  node [
    id 73
    label "siedziba"
  ]
  node [
    id 74
    label "Spo&#322;em"
  ]
  node [
    id 75
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 76
    label "Orlen"
  ]
  node [
    id 77
    label "klasa"
  ]
  node [
    id 78
    label "lacki"
  ]
  node [
    id 79
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 80
    label "przedmiot"
  ]
  node [
    id 81
    label "sztajer"
  ]
  node [
    id 82
    label "drabant"
  ]
  node [
    id 83
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 84
    label "polak"
  ]
  node [
    id 85
    label "pierogi_ruskie"
  ]
  node [
    id 86
    label "krakowiak"
  ]
  node [
    id 87
    label "Polish"
  ]
  node [
    id 88
    label "j&#281;zyk"
  ]
  node [
    id 89
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 90
    label "oberek"
  ]
  node [
    id 91
    label "po_polsku"
  ]
  node [
    id 92
    label "mazur"
  ]
  node [
    id 93
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 94
    label "chodzony"
  ]
  node [
    id 95
    label "skoczny"
  ]
  node [
    id 96
    label "ryba_po_grecku"
  ]
  node [
    id 97
    label "goniony"
  ]
  node [
    id 98
    label "polsko"
  ]
  node [
    id 99
    label "uruchomienie"
  ]
  node [
    id 100
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 101
    label "&#347;rodowisko"
  ]
  node [
    id 102
    label "supernadz&#243;r"
  ]
  node [
    id 103
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 104
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 105
    label "absolutorium"
  ]
  node [
    id 106
    label "podupada&#263;"
  ]
  node [
    id 107
    label "nap&#322;ywanie"
  ]
  node [
    id 108
    label "kapitalista"
  ]
  node [
    id 109
    label "podupadanie"
  ]
  node [
    id 110
    label "kwestor"
  ]
  node [
    id 111
    label "uruchamia&#263;"
  ]
  node [
    id 112
    label "mienie"
  ]
  node [
    id 113
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 114
    label "uruchamianie"
  ]
  node [
    id 115
    label "czynnik_produkcji"
  ]
  node [
    id 116
    label "zaleta"
  ]
  node [
    id 117
    label "zas&#243;b"
  ]
  node [
    id 118
    label "dok&#322;adnie"
  ]
  node [
    id 119
    label "bliski"
  ]
  node [
    id 120
    label "silnie"
  ]
  node [
    id 121
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 122
    label "bro&#324;_miotaj&#261;ca"
  ]
  node [
    id 123
    label "zabawka"
  ]
  node [
    id 124
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 125
    label "bro&#324;"
  ]
  node [
    id 126
    label "catapult"
  ]
  node [
    id 127
    label "urwis"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
]
