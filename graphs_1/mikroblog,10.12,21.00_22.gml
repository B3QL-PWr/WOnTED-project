graph [
  maxDegree 16
  minDegree 1
  meanDegree 2
  density 0.044444444444444446
  graphCliqueNumber 2
  node [
    id 0
    label "gdy"
    origin "text"
  ]
  node [
    id 1
    label "przychodzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nowa"
    origin "text"
  ]
  node [
    id 3
    label "obrabiarka"
    origin "text"
  ]
  node [
    id 4
    label "korea"
    origin "text"
  ]
  node [
    id 5
    label "dokumentacja"
    origin "text"
  ]
  node [
    id 6
    label "spakowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pudlo"
    origin "text"
  ]
  node [
    id 8
    label "papier"
    origin "text"
  ]
  node [
    id 9
    label "drukarka"
    origin "text"
  ]
  node [
    id 10
    label "przybywa&#263;"
  ]
  node [
    id 11
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 12
    label "dochodzi&#263;"
  ]
  node [
    id 13
    label "gwiazda"
  ]
  node [
    id 14
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 15
    label "suport"
  ]
  node [
    id 16
    label "wrzeciennik"
  ]
  node [
    id 17
    label "imak"
  ]
  node [
    id 18
    label "wrzeciono"
  ]
  node [
    id 19
    label "maszyna"
  ]
  node [
    id 20
    label "sto&#380;ek_Morse'a"
  ]
  node [
    id 21
    label "ekscerpcja"
  ]
  node [
    id 22
    label "kosztorys"
  ]
  node [
    id 23
    label "materia&#322;"
  ]
  node [
    id 24
    label "operat"
  ]
  node [
    id 25
    label "throng"
  ]
  node [
    id 26
    label "skonwertowa&#263;"
  ]
  node [
    id 27
    label "pack"
  ]
  node [
    id 28
    label "dane"
  ]
  node [
    id 29
    label "owin&#261;&#263;"
  ]
  node [
    id 30
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 31
    label "tworzywo"
  ]
  node [
    id 32
    label "nak&#322;uwacz"
  ]
  node [
    id 33
    label "wytw&#243;r"
  ]
  node [
    id 34
    label "libra"
  ]
  node [
    id 35
    label "fascyku&#322;"
  ]
  node [
    id 36
    label "raport&#243;wka"
  ]
  node [
    id 37
    label "artyku&#322;"
  ]
  node [
    id 38
    label "writing"
  ]
  node [
    id 39
    label "format_arkusza"
  ]
  node [
    id 40
    label "registratura"
  ]
  node [
    id 41
    label "parafa"
  ]
  node [
    id 42
    label "sygnatariusz"
  ]
  node [
    id 43
    label "papeteria"
  ]
  node [
    id 44
    label "dupleks"
  ]
  node [
    id 45
    label "urz&#261;dzenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
]
