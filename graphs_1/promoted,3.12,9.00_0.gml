graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9733333333333334
  density 0.02666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "informacja"
    origin "text"
  ]
  node [
    id 1
    label "ile"
    origin "text"
  ]
  node [
    id 2
    label "sprzeda&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "&#347;ci&#347;le"
    origin "text"
  ]
  node [
    id 7
    label "strzec"
    origin "text"
  ]
  node [
    id 8
    label "doj&#347;cie"
  ]
  node [
    id 9
    label "doj&#347;&#263;"
  ]
  node [
    id 10
    label "powzi&#261;&#263;"
  ]
  node [
    id 11
    label "wiedza"
  ]
  node [
    id 12
    label "sygna&#322;"
  ]
  node [
    id 13
    label "obiegni&#281;cie"
  ]
  node [
    id 14
    label "obieganie"
  ]
  node [
    id 15
    label "obiec"
  ]
  node [
    id 16
    label "dane"
  ]
  node [
    id 17
    label "obiega&#263;"
  ]
  node [
    id 18
    label "punkt"
  ]
  node [
    id 19
    label "publikacja"
  ]
  node [
    id 20
    label "powzi&#281;cie"
  ]
  node [
    id 21
    label "zhandlowa&#263;"
  ]
  node [
    id 22
    label "odda&#263;"
  ]
  node [
    id 23
    label "zach&#281;ci&#263;"
  ]
  node [
    id 24
    label "give_birth"
  ]
  node [
    id 25
    label "zdradzi&#263;"
  ]
  node [
    id 26
    label "op&#281;dzi&#263;"
  ]
  node [
    id 27
    label "sell"
  ]
  node [
    id 28
    label "ok&#322;adka"
  ]
  node [
    id 29
    label "zak&#322;adka"
  ]
  node [
    id 30
    label "ekslibris"
  ]
  node [
    id 31
    label "wk&#322;ad"
  ]
  node [
    id 32
    label "przek&#322;adacz"
  ]
  node [
    id 33
    label "wydawnictwo"
  ]
  node [
    id 34
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 35
    label "tytu&#322;"
  ]
  node [
    id 36
    label "bibliofilstwo"
  ]
  node [
    id 37
    label "falc"
  ]
  node [
    id 38
    label "nomina&#322;"
  ]
  node [
    id 39
    label "pagina"
  ]
  node [
    id 40
    label "rozdzia&#322;"
  ]
  node [
    id 41
    label "egzemplarz"
  ]
  node [
    id 42
    label "zw&#243;j"
  ]
  node [
    id 43
    label "tekst"
  ]
  node [
    id 44
    label "si&#281;ga&#263;"
  ]
  node [
    id 45
    label "trwa&#263;"
  ]
  node [
    id 46
    label "obecno&#347;&#263;"
  ]
  node [
    id 47
    label "stan"
  ]
  node [
    id 48
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 49
    label "stand"
  ]
  node [
    id 50
    label "mie&#263;_miejsce"
  ]
  node [
    id 51
    label "uczestniczy&#263;"
  ]
  node [
    id 52
    label "chodzi&#263;"
  ]
  node [
    id 53
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 54
    label "equal"
  ]
  node [
    id 55
    label "przylegle"
  ]
  node [
    id 56
    label "dok&#322;adnie"
  ]
  node [
    id 57
    label "blisko"
  ]
  node [
    id 58
    label "zwarty"
  ]
  node [
    id 59
    label "zwarto"
  ]
  node [
    id 60
    label "g&#281;sto"
  ]
  node [
    id 61
    label "surowo"
  ]
  node [
    id 62
    label "rygorystyczny"
  ]
  node [
    id 63
    label "&#347;cis&#322;y"
  ]
  node [
    id 64
    label "logicznie"
  ]
  node [
    id 65
    label "continue"
  ]
  node [
    id 66
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 67
    label "zachowywa&#263;"
  ]
  node [
    id 68
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 69
    label "sprawowa&#263;"
  ]
  node [
    id 70
    label "chroni&#263;"
  ]
  node [
    id 71
    label "robi&#263;"
  ]
  node [
    id 72
    label "czuwa&#263;"
  ]
  node [
    id 73
    label "broni&#263;"
  ]
  node [
    id 74
    label "rebuff"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
]
