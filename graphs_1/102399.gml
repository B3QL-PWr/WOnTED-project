graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.046728971962617
  density 0.009609056206397262
  graphCliqueNumber 3
  node [
    id 0
    label "dawny"
    origin "text"
  ]
  node [
    id 1
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "niedu&#380;y"
    origin "text"
  ]
  node [
    id 5
    label "amatorski"
    origin "text"
  ]
  node [
    id 6
    label "filmik"
    origin "text"
  ]
  node [
    id 7
    label "wideo"
    origin "text"
  ]
  node [
    id 8
    label "po&#347;wi&#281;cony"
    origin "text"
  ]
  node [
    id 9
    label "ciekawy"
    origin "text"
  ]
  node [
    id 10
    label "problem"
    origin "text"
  ]
  node [
    id 11
    label "matematyczny"
    origin "text"
  ]
  node [
    id 12
    label "wszystek"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "dost&#281;pny"
    origin "text"
  ]
  node [
    id 15
    label "najpierw"
    origin "text"
  ]
  node [
    id 16
    label "witryna"
    origin "text"
  ]
  node [
    id 17
    label "opinia"
    origin "text"
  ]
  node [
    id 18
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 19
    label "tutaj"
    origin "text"
  ]
  node [
    id 20
    label "oto"
    origin "text"
  ]
  node [
    id 21
    label "pierwszy"
    origin "text"
  ]
  node [
    id 22
    label "przeprasza&#263;"
    origin "text"
  ]
  node [
    id 23
    label "jako&#347;&#263;"
    origin "text"
  ]
  node [
    id 24
    label "w&#322;a&#347;ciwie"
    origin "text"
  ]
  node [
    id 25
    label "kiepsko&#347;&#263;"
    origin "text"
  ]
  node [
    id 26
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 27
    label "przesz&#322;y"
  ]
  node [
    id 28
    label "dawno"
  ]
  node [
    id 29
    label "dawniej"
  ]
  node [
    id 30
    label "kombatant"
  ]
  node [
    id 31
    label "stary"
  ]
  node [
    id 32
    label "odleg&#322;y"
  ]
  node [
    id 33
    label "anachroniczny"
  ]
  node [
    id 34
    label "przestarza&#322;y"
  ]
  node [
    id 35
    label "od_dawna"
  ]
  node [
    id 36
    label "poprzedni"
  ]
  node [
    id 37
    label "d&#322;ugoletni"
  ]
  node [
    id 38
    label "wcze&#347;niejszy"
  ]
  node [
    id 39
    label "niegdysiejszy"
  ]
  node [
    id 40
    label "proceed"
  ]
  node [
    id 41
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 42
    label "bangla&#263;"
  ]
  node [
    id 43
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 44
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 45
    label "run"
  ]
  node [
    id 46
    label "tryb"
  ]
  node [
    id 47
    label "p&#322;ywa&#263;"
  ]
  node [
    id 48
    label "continue"
  ]
  node [
    id 49
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 50
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 51
    label "przebiega&#263;"
  ]
  node [
    id 52
    label "mie&#263;_miejsce"
  ]
  node [
    id 53
    label "wk&#322;ada&#263;"
  ]
  node [
    id 54
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 55
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 56
    label "para"
  ]
  node [
    id 57
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 58
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 59
    label "krok"
  ]
  node [
    id 60
    label "str&#243;j"
  ]
  node [
    id 61
    label "bywa&#263;"
  ]
  node [
    id 62
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 63
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 64
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 65
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 66
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 67
    label "dziama&#263;"
  ]
  node [
    id 68
    label "stara&#263;_si&#281;"
  ]
  node [
    id 69
    label "carry"
  ]
  node [
    id 70
    label "cause"
  ]
  node [
    id 71
    label "introduce"
  ]
  node [
    id 72
    label "begin"
  ]
  node [
    id 73
    label "odj&#261;&#263;"
  ]
  node [
    id 74
    label "post&#261;pi&#263;"
  ]
  node [
    id 75
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 76
    label "do"
  ]
  node [
    id 77
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 78
    label "zrobi&#263;"
  ]
  node [
    id 79
    label "tentegowa&#263;"
  ]
  node [
    id 80
    label "urz&#261;dza&#263;"
  ]
  node [
    id 81
    label "give"
  ]
  node [
    id 82
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 83
    label "czyni&#263;"
  ]
  node [
    id 84
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 85
    label "post&#281;powa&#263;"
  ]
  node [
    id 86
    label "wydala&#263;"
  ]
  node [
    id 87
    label "oszukiwa&#263;"
  ]
  node [
    id 88
    label "organizowa&#263;"
  ]
  node [
    id 89
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 90
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 91
    label "work"
  ]
  node [
    id 92
    label "przerabia&#263;"
  ]
  node [
    id 93
    label "stylizowa&#263;"
  ]
  node [
    id 94
    label "falowa&#263;"
  ]
  node [
    id 95
    label "act"
  ]
  node [
    id 96
    label "peddle"
  ]
  node [
    id 97
    label "ukazywa&#263;"
  ]
  node [
    id 98
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 99
    label "praca"
  ]
  node [
    id 100
    label "nieznacznie"
  ]
  node [
    id 101
    label "ma&#322;o"
  ]
  node [
    id 102
    label "nielicznie"
  ]
  node [
    id 103
    label "ma&#322;y"
  ]
  node [
    id 104
    label "m&#322;ody"
  ]
  node [
    id 105
    label "s&#322;aby"
  ]
  node [
    id 106
    label "po_laicku"
  ]
  node [
    id 107
    label "niezawodowy"
  ]
  node [
    id 108
    label "nierzetelny"
  ]
  node [
    id 109
    label "amatorsko"
  ]
  node [
    id 110
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 111
    label "po_amatorsku"
  ]
  node [
    id 112
    label "niezawodowo"
  ]
  node [
    id 113
    label "gif"
  ]
  node [
    id 114
    label "odtwarzacz"
  ]
  node [
    id 115
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 116
    label "film"
  ]
  node [
    id 117
    label "technika"
  ]
  node [
    id 118
    label "wideokaseta"
  ]
  node [
    id 119
    label "oddany"
  ]
  node [
    id 120
    label "swoisty"
  ]
  node [
    id 121
    label "cz&#322;owiek"
  ]
  node [
    id 122
    label "interesowanie"
  ]
  node [
    id 123
    label "nietuzinkowy"
  ]
  node [
    id 124
    label "ciekawie"
  ]
  node [
    id 125
    label "indagator"
  ]
  node [
    id 126
    label "interesuj&#261;cy"
  ]
  node [
    id 127
    label "dziwny"
  ]
  node [
    id 128
    label "intryguj&#261;cy"
  ]
  node [
    id 129
    label "ch&#281;tny"
  ]
  node [
    id 130
    label "trudno&#347;&#263;"
  ]
  node [
    id 131
    label "sprawa"
  ]
  node [
    id 132
    label "ambaras"
  ]
  node [
    id 133
    label "problemat"
  ]
  node [
    id 134
    label "pierepa&#322;ka"
  ]
  node [
    id 135
    label "obstruction"
  ]
  node [
    id 136
    label "problematyka"
  ]
  node [
    id 137
    label "jajko_Kolumba"
  ]
  node [
    id 138
    label "subiekcja"
  ]
  node [
    id 139
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 140
    label "dok&#322;adny"
  ]
  node [
    id 141
    label "matematycznie"
  ]
  node [
    id 142
    label "ca&#322;y"
  ]
  node [
    id 143
    label "si&#281;ga&#263;"
  ]
  node [
    id 144
    label "trwa&#263;"
  ]
  node [
    id 145
    label "obecno&#347;&#263;"
  ]
  node [
    id 146
    label "stan"
  ]
  node [
    id 147
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 148
    label "stand"
  ]
  node [
    id 149
    label "uczestniczy&#263;"
  ]
  node [
    id 150
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 151
    label "equal"
  ]
  node [
    id 152
    label "&#322;atwy"
  ]
  node [
    id 153
    label "mo&#380;liwy"
  ]
  node [
    id 154
    label "dost&#281;pnie"
  ]
  node [
    id 155
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 156
    label "przyst&#281;pnie"
  ]
  node [
    id 157
    label "zrozumia&#322;y"
  ]
  node [
    id 158
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 159
    label "odblokowanie_si&#281;"
  ]
  node [
    id 160
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 161
    label "wcze&#347;niej"
  ]
  node [
    id 162
    label "nasamprz&#243;d"
  ]
  node [
    id 163
    label "pocz&#261;tkowo"
  ]
  node [
    id 164
    label "pierwiej"
  ]
  node [
    id 165
    label "pierw"
  ]
  node [
    id 166
    label "wytw&#243;r"
  ]
  node [
    id 167
    label "sklep"
  ]
  node [
    id 168
    label "gablota"
  ]
  node [
    id 169
    label "szyba"
  ]
  node [
    id 170
    label "YouTube"
  ]
  node [
    id 171
    label "strona"
  ]
  node [
    id 172
    label "okno"
  ]
  node [
    id 173
    label "dokument"
  ]
  node [
    id 174
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 175
    label "reputacja"
  ]
  node [
    id 176
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 177
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 178
    label "cecha"
  ]
  node [
    id 179
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 180
    label "informacja"
  ]
  node [
    id 181
    label "sofcik"
  ]
  node [
    id 182
    label "appraisal"
  ]
  node [
    id 183
    label "ekspertyza"
  ]
  node [
    id 184
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 185
    label "pogl&#261;d"
  ]
  node [
    id 186
    label "kryterium"
  ]
  node [
    id 187
    label "wielko&#347;&#263;"
  ]
  node [
    id 188
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 189
    label "doba"
  ]
  node [
    id 190
    label "czas"
  ]
  node [
    id 191
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 192
    label "weekend"
  ]
  node [
    id 193
    label "miesi&#261;c"
  ]
  node [
    id 194
    label "tam"
  ]
  node [
    id 195
    label "najwa&#380;niejszy"
  ]
  node [
    id 196
    label "pocz&#261;tkowy"
  ]
  node [
    id 197
    label "dobry"
  ]
  node [
    id 198
    label "dzie&#324;"
  ]
  node [
    id 199
    label "pr&#281;dki"
  ]
  node [
    id 200
    label "sk&#322;ada&#263;"
  ]
  node [
    id 201
    label "warto&#347;&#263;"
  ]
  node [
    id 202
    label "co&#347;"
  ]
  node [
    id 203
    label "syf"
  ]
  node [
    id 204
    label "state"
  ]
  node [
    id 205
    label "quality"
  ]
  node [
    id 206
    label "dobrze"
  ]
  node [
    id 207
    label "stosowny"
  ]
  node [
    id 208
    label "nale&#380;ycie"
  ]
  node [
    id 209
    label "charakterystycznie"
  ]
  node [
    id 210
    label "prawdziwie"
  ]
  node [
    id 211
    label "nale&#380;nie"
  ]
  node [
    id 212
    label "denno&#347;&#263;"
  ]
  node [
    id 213
    label "dok&#322;adnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 52
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 129
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 110
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 26
    target 213
  ]
]
