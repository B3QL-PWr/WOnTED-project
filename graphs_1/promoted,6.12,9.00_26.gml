graph [
  maxDegree 47
  minDegree 1
  meanDegree 2.06
  density 0.020808080808080807
  graphCliqueNumber 3
  node [
    id 0
    label "cacka"
    origin "text"
  ]
  node [
    id 1
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ko&#324;"
    origin "text"
  ]
  node [
    id 3
    label "mechaniczny"
    origin "text"
  ]
  node [
    id 4
    label "automatyczny"
    origin "text"
  ]
  node [
    id 5
    label "skrzynia"
    origin "text"
  ]
  node [
    id 6
    label "bieg"
    origin "text"
  ]
  node [
    id 7
    label "nap&#281;d"
    origin "text"
  ]
  node [
    id 8
    label "wideorejestrator"
    origin "text"
  ]
  node [
    id 9
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 10
    label "czu&#263;"
  ]
  node [
    id 11
    label "need"
  ]
  node [
    id 12
    label "hide"
  ]
  node [
    id 13
    label "support"
  ]
  node [
    id 14
    label "pok&#322;usowa&#263;"
  ]
  node [
    id 15
    label "czo&#322;dar"
  ]
  node [
    id 16
    label "os&#322;omu&#322;"
  ]
  node [
    id 17
    label "kawalerzysta"
  ]
  node [
    id 18
    label "k&#322;usowanie"
  ]
  node [
    id 19
    label "pok&#322;usowanie"
  ]
  node [
    id 20
    label "zwierz&#281;_wierzchowe"
  ]
  node [
    id 21
    label "zar&#380;e&#263;"
  ]
  node [
    id 22
    label "karmiak"
  ]
  node [
    id 23
    label "galopowa&#263;"
  ]
  node [
    id 24
    label "k&#322;usowa&#263;"
  ]
  node [
    id 25
    label "przegalopowa&#263;"
  ]
  node [
    id 26
    label "hipoterapeuta"
  ]
  node [
    id 27
    label "osadzenie_si&#281;"
  ]
  node [
    id 28
    label "remuda"
  ]
  node [
    id 29
    label "znarowienie"
  ]
  node [
    id 30
    label "r&#380;e&#263;"
  ]
  node [
    id 31
    label "zebroid"
  ]
  node [
    id 32
    label "r&#380;enie"
  ]
  node [
    id 33
    label "podkuwanie"
  ]
  node [
    id 34
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 35
    label "podkuwa&#263;"
  ]
  node [
    id 36
    label "narowi&#263;"
  ]
  node [
    id 37
    label "zebrula"
  ]
  node [
    id 38
    label "&#322;ykawo&#347;&#263;"
  ]
  node [
    id 39
    label "zaci&#261;&#263;"
  ]
  node [
    id 40
    label "nar&#243;w"
  ]
  node [
    id 41
    label "przegalopowanie"
  ]
  node [
    id 42
    label "figura"
  ]
  node [
    id 43
    label "lansada"
  ]
  node [
    id 44
    label "dosiad"
  ]
  node [
    id 45
    label "narowienie"
  ]
  node [
    id 46
    label "zaci&#281;cie"
  ]
  node [
    id 47
    label "koniowate"
  ]
  node [
    id 48
    label "pogalopowanie"
  ]
  node [
    id 49
    label "uje&#380;d&#380;anie"
  ]
  node [
    id 50
    label "ko&#324;_dziki"
  ]
  node [
    id 51
    label "pogalopowa&#263;"
  ]
  node [
    id 52
    label "penis"
  ]
  node [
    id 53
    label "hipoterapia"
  ]
  node [
    id 54
    label "osadza&#263;_si&#281;"
  ]
  node [
    id 55
    label "osadzi&#263;_si&#281;"
  ]
  node [
    id 56
    label "galopowanie"
  ]
  node [
    id 57
    label "osadzanie_si&#281;"
  ]
  node [
    id 58
    label "znarowi&#263;"
  ]
  node [
    id 59
    label "mechanicznie"
  ]
  node [
    id 60
    label "bezwiednie"
  ]
  node [
    id 61
    label "nie&#347;wiadomy"
  ]
  node [
    id 62
    label "samoistny"
  ]
  node [
    id 63
    label "typowy"
  ]
  node [
    id 64
    label "poniewolny"
  ]
  node [
    id 65
    label "automatycznie"
  ]
  node [
    id 66
    label "zapewnianie"
  ]
  node [
    id 67
    label "pewny"
  ]
  node [
    id 68
    label "zapewnienie"
  ]
  node [
    id 69
    label "kanapa"
  ]
  node [
    id 70
    label "case"
  ]
  node [
    id 71
    label "pojemnik"
  ]
  node [
    id 72
    label "mebel"
  ]
  node [
    id 73
    label "tapczan"
  ]
  node [
    id 74
    label "wy&#347;cig"
  ]
  node [
    id 75
    label "parametr"
  ]
  node [
    id 76
    label "ciek_wodny"
  ]
  node [
    id 77
    label "roll"
  ]
  node [
    id 78
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 79
    label "przedbieg"
  ]
  node [
    id 80
    label "konkurencja"
  ]
  node [
    id 81
    label "tryb"
  ]
  node [
    id 82
    label "bystrzyca"
  ]
  node [
    id 83
    label "pr&#261;d"
  ]
  node [
    id 84
    label "pozycja"
  ]
  node [
    id 85
    label "linia"
  ]
  node [
    id 86
    label "procedura"
  ]
  node [
    id 87
    label "ruch"
  ]
  node [
    id 88
    label "czo&#322;&#243;wka"
  ]
  node [
    id 89
    label "kurs"
  ]
  node [
    id 90
    label "syfon"
  ]
  node [
    id 91
    label "kierunek"
  ]
  node [
    id 92
    label "cycle"
  ]
  node [
    id 93
    label "proces"
  ]
  node [
    id 94
    label "d&#261;&#380;enie"
  ]
  node [
    id 95
    label "energia"
  ]
  node [
    id 96
    label "most"
  ]
  node [
    id 97
    label "urz&#261;dzenie"
  ]
  node [
    id 98
    label "propulsion"
  ]
  node [
    id 99
    label "rejestrator"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 8
    target 99
  ]
]
