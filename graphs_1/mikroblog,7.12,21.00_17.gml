graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9393939393939394
  density 0.06060606060606061
  graphCliqueNumber 2
  node [
    id 0
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 1
    label "tutaj"
    origin "text"
  ]
  node [
    id 2
    label "obserwowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "profil"
    origin "text"
  ]
  node [
    id 4
    label "stachurski"
    origin "text"
  ]
  node [
    id 5
    label "instagramie"
    origin "text"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "znaczenie"
  ]
  node [
    id 8
    label "go&#347;&#263;"
  ]
  node [
    id 9
    label "osoba"
  ]
  node [
    id 10
    label "posta&#263;"
  ]
  node [
    id 11
    label "tam"
  ]
  node [
    id 12
    label "patrze&#263;"
  ]
  node [
    id 13
    label "dostrzega&#263;"
  ]
  node [
    id 14
    label "look"
  ]
  node [
    id 15
    label "rzeczywisto&#347;&#263;_wirtualna"
  ]
  node [
    id 16
    label "seria"
  ]
  node [
    id 17
    label "twarz"
  ]
  node [
    id 18
    label "section"
  ]
  node [
    id 19
    label "listwa"
  ]
  node [
    id 20
    label "podgl&#261;d"
  ]
  node [
    id 21
    label "ozdoba"
  ]
  node [
    id 22
    label "dominanta"
  ]
  node [
    id 23
    label "obw&#243;dka"
  ]
  node [
    id 24
    label "faseta"
  ]
  node [
    id 25
    label "kontur"
  ]
  node [
    id 26
    label "profile"
  ]
  node [
    id 27
    label "konto"
  ]
  node [
    id 28
    label "przekr&#243;j"
  ]
  node [
    id 29
    label "awatar"
  ]
  node [
    id 30
    label "charakter"
  ]
  node [
    id 31
    label "element_konstrukcyjny"
  ]
  node [
    id 32
    label "sylwetka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
]
