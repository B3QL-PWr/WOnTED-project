graph [
  maxDegree 4
  minDegree 1
  meanDegree 2.2777777777777777
  density 0.06507936507936508
  graphCliqueNumber 5
  node [
    id 0
    label "konstanty"
    origin "text"
  ]
  node [
    id 1
    label "drucki"
    origin "text"
  ]
  node [
    id 2
    label "lubecki"
    origin "text"
  ]
  node [
    id 3
    label "niemiecki"
  ]
  node [
    id 4
    label "konstanta"
  ]
  node [
    id 5
    label "Drucki"
  ]
  node [
    id 6
    label "Lubecki"
  ]
  node [
    id 7
    label "Maria"
  ]
  node [
    id 8
    label "J&#243;zefa"
  ]
  node [
    id 9
    label "wojsko"
  ]
  node [
    id 10
    label "polskie"
  ]
  node [
    id 11
    label "liceum"
  ]
  node [
    id 12
    label "wyspa"
  ]
  node [
    id 13
    label "carski"
  ]
  node [
    id 14
    label "sio&#322;o"
  ]
  node [
    id 15
    label "i"
  ]
  node [
    id 16
    label "wojna"
  ]
  node [
    id 17
    label "&#347;wiatowy"
  ]
  node [
    id 18
    label "12"
  ]
  node [
    id 19
    label "pu&#322;k"
  ]
  node [
    id 20
    label "huzar"
  ]
  node [
    id 21
    label "Konstanty"
  ]
  node [
    id 22
    label "Plisowskiego"
  ]
  node [
    id 23
    label "samoobrona"
  ]
  node [
    id 24
    label "wile&#324;ski"
  ]
  node [
    id 25
    label "order"
  ]
  node [
    id 26
    label "Virtuti"
  ]
  node [
    id 27
    label "Militari"
  ]
  node [
    id 28
    label "centrum"
  ]
  node [
    id 29
    label "wyszkoli&#263;"
  ]
  node [
    id 30
    label "kawaleria"
  ]
  node [
    id 31
    label "brygada"
  ]
  node [
    id 32
    label "2"
  ]
  node [
    id 33
    label "szwole&#380;er"
  ]
  node [
    id 34
    label "W&#322;adys&#322;awa"
  ]
  node [
    id 35
    label "Anders"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
]
