graph [
  maxDegree 33
  minDegree 1
  meanDegree 1.9473684210526316
  density 0.05263157894736842
  graphCliqueNumber 2
  node [
    id 0
    label "wtem"
    origin "text"
  ]
  node [
    id 1
    label "wpada&#263;"
    origin "text"
  ]
  node [
    id 2
    label "tw&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "ziomeczek"
    origin "text"
  ]
  node [
    id 4
    label "niespodziewanie"
  ]
  node [
    id 5
    label "raptownie"
  ]
  node [
    id 6
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 7
    label "wpa&#347;&#263;"
  ]
  node [
    id 8
    label "chowa&#263;"
  ]
  node [
    id 9
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 10
    label "strike"
  ]
  node [
    id 11
    label "rzecz"
  ]
  node [
    id 12
    label "ulega&#263;"
  ]
  node [
    id 13
    label "przypomina&#263;"
  ]
  node [
    id 14
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 15
    label "popada&#263;"
  ]
  node [
    id 16
    label "zapach"
  ]
  node [
    id 17
    label "czu&#263;"
  ]
  node [
    id 18
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 19
    label "pogo"
  ]
  node [
    id 20
    label "flatten"
  ]
  node [
    id 21
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 22
    label "odwiedza&#263;"
  ]
  node [
    id 23
    label "fall"
  ]
  node [
    id 24
    label "ujmowa&#263;"
  ]
  node [
    id 25
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 26
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 27
    label "&#347;wiat&#322;o"
  ]
  node [
    id 28
    label "wymy&#347;la&#263;"
  ]
  node [
    id 29
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 30
    label "emocja"
  ]
  node [
    id 31
    label "demaskowa&#263;"
  ]
  node [
    id 32
    label "zaziera&#263;"
  ]
  node [
    id 33
    label "ogrom"
  ]
  node [
    id 34
    label "spotyka&#263;"
  ]
  node [
    id 35
    label "d&#378;wi&#281;k"
  ]
  node [
    id 36
    label "drop"
  ]
  node [
    id 37
    label "czyj&#347;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
]
