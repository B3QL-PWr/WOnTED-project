graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.8974358974358974
  density 0.04993252361673414
  graphCliqueNumber 2
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "licencja"
    origin "text"
  ]
  node [
    id 4
    label "byd&#322;o"
  ]
  node [
    id 5
    label "zobo"
  ]
  node [
    id 6
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 7
    label "yakalo"
  ]
  node [
    id 8
    label "dzo"
  ]
  node [
    id 9
    label "use"
  ]
  node [
    id 10
    label "krzywdzi&#263;"
  ]
  node [
    id 11
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 12
    label "distribute"
  ]
  node [
    id 13
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 14
    label "give"
  ]
  node [
    id 15
    label "liga&#263;"
  ]
  node [
    id 16
    label "korzysta&#263;"
  ]
  node [
    id 17
    label "u&#380;ywa&#263;"
  ]
  node [
    id 18
    label "si&#281;ga&#263;"
  ]
  node [
    id 19
    label "trwa&#263;"
  ]
  node [
    id 20
    label "obecno&#347;&#263;"
  ]
  node [
    id 21
    label "stan"
  ]
  node [
    id 22
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 23
    label "stand"
  ]
  node [
    id 24
    label "mie&#263;_miejsce"
  ]
  node [
    id 25
    label "uczestniczy&#263;"
  ]
  node [
    id 26
    label "chodzi&#263;"
  ]
  node [
    id 27
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 28
    label "equal"
  ]
  node [
    id 29
    label "prawo"
  ]
  node [
    id 30
    label "licencjonowa&#263;"
  ]
  node [
    id 31
    label "pozwolenie"
  ]
  node [
    id 32
    label "hodowla"
  ]
  node [
    id 33
    label "rasowy"
  ]
  node [
    id 34
    label "license"
  ]
  node [
    id 35
    label "zezwolenie"
  ]
  node [
    id 36
    label "za&#347;wiadczenie"
  ]
  node [
    id 37
    label "unia"
  ]
  node [
    id 38
    label "europejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 37
    target 38
  ]
]
