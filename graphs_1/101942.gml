graph [
  maxDegree 221
  minDegree 1
  meanDegree 2.1067285382830625
  density 0.004899368693681541
  graphCliqueNumber 3
  node [
    id 0
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 1
    label "wezwa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 3
    label "ustanowi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "dodatkowy"
    origin "text"
  ]
  node [
    id 5
    label "warunek"
    origin "text"
  ]
  node [
    id 6
    label "ograniczaj&#261;cy"
    origin "text"
  ]
  node [
    id 7
    label "wykorzystanie"
    origin "text"
  ]
  node [
    id 8
    label "dana"
    origin "text"
  ]
  node [
    id 9
    label "osobowy"
    origin "text"
  ]
  node [
    id 10
    label "sprawa"
    origin "text"
  ]
  node [
    id 11
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 12
    label "wniosek"
    origin "text"
  ]
  node [
    id 13
    label "pomoc"
    origin "text"
  ]
  node [
    id 14
    label "prawny"
    origin "text"
  ]
  node [
    id 15
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "zrealizowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 19
    label "brak"
    origin "text"
  ]
  node [
    id 20
    label "taki"
    origin "text"
  ]
  node [
    id 21
    label "przypadek"
    origin "text"
  ]
  node [
    id 22
    label "zgodnie"
    origin "text"
  ]
  node [
    id 23
    label "niniejszy"
    origin "text"
  ]
  node [
    id 24
    label "ust&#281;p"
    origin "text"
  ]
  node [
    id 25
    label "za&#380;&#261;da&#263;"
    origin "text"
  ]
  node [
    id 26
    label "wzywa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "udzieli&#263;"
    origin "text"
  ]
  node [
    id 28
    label "informacja"
    origin "text"
  ]
  node [
    id 29
    label "temat"
    origin "text"
  ]
  node [
    id 30
    label "dow&#243;d"
    origin "text"
  ]
  node [
    id 31
    label "lub"
    origin "text"
  ]
  node [
    id 32
    label "Filipiny"
  ]
  node [
    id 33
    label "Rwanda"
  ]
  node [
    id 34
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 35
    label "Monako"
  ]
  node [
    id 36
    label "Korea"
  ]
  node [
    id 37
    label "Ghana"
  ]
  node [
    id 38
    label "Czarnog&#243;ra"
  ]
  node [
    id 39
    label "Malawi"
  ]
  node [
    id 40
    label "Indonezja"
  ]
  node [
    id 41
    label "Bu&#322;garia"
  ]
  node [
    id 42
    label "Nauru"
  ]
  node [
    id 43
    label "Kenia"
  ]
  node [
    id 44
    label "Kambod&#380;a"
  ]
  node [
    id 45
    label "Mali"
  ]
  node [
    id 46
    label "Austria"
  ]
  node [
    id 47
    label "interior"
  ]
  node [
    id 48
    label "Armenia"
  ]
  node [
    id 49
    label "Fid&#380;i"
  ]
  node [
    id 50
    label "Tuwalu"
  ]
  node [
    id 51
    label "Etiopia"
  ]
  node [
    id 52
    label "Malta"
  ]
  node [
    id 53
    label "Malezja"
  ]
  node [
    id 54
    label "Grenada"
  ]
  node [
    id 55
    label "Tad&#380;ykistan"
  ]
  node [
    id 56
    label "Wehrlen"
  ]
  node [
    id 57
    label "para"
  ]
  node [
    id 58
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 59
    label "Rumunia"
  ]
  node [
    id 60
    label "Maroko"
  ]
  node [
    id 61
    label "Bhutan"
  ]
  node [
    id 62
    label "S&#322;owacja"
  ]
  node [
    id 63
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 64
    label "Seszele"
  ]
  node [
    id 65
    label "Kuwejt"
  ]
  node [
    id 66
    label "Arabia_Saudyjska"
  ]
  node [
    id 67
    label "Ekwador"
  ]
  node [
    id 68
    label "Kanada"
  ]
  node [
    id 69
    label "Japonia"
  ]
  node [
    id 70
    label "ziemia"
  ]
  node [
    id 71
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 72
    label "Hiszpania"
  ]
  node [
    id 73
    label "Wyspy_Marshalla"
  ]
  node [
    id 74
    label "Botswana"
  ]
  node [
    id 75
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 76
    label "D&#380;ibuti"
  ]
  node [
    id 77
    label "grupa"
  ]
  node [
    id 78
    label "Wietnam"
  ]
  node [
    id 79
    label "Egipt"
  ]
  node [
    id 80
    label "Burkina_Faso"
  ]
  node [
    id 81
    label "Niemcy"
  ]
  node [
    id 82
    label "Khitai"
  ]
  node [
    id 83
    label "Macedonia"
  ]
  node [
    id 84
    label "Albania"
  ]
  node [
    id 85
    label "Madagaskar"
  ]
  node [
    id 86
    label "Bahrajn"
  ]
  node [
    id 87
    label "Jemen"
  ]
  node [
    id 88
    label "Lesoto"
  ]
  node [
    id 89
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 90
    label "Samoa"
  ]
  node [
    id 91
    label "Andora"
  ]
  node [
    id 92
    label "Chiny"
  ]
  node [
    id 93
    label "Cypr"
  ]
  node [
    id 94
    label "Wielka_Brytania"
  ]
  node [
    id 95
    label "Ukraina"
  ]
  node [
    id 96
    label "Paragwaj"
  ]
  node [
    id 97
    label "Trynidad_i_Tobago"
  ]
  node [
    id 98
    label "Libia"
  ]
  node [
    id 99
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 100
    label "Surinam"
  ]
  node [
    id 101
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 102
    label "Australia"
  ]
  node [
    id 103
    label "Nigeria"
  ]
  node [
    id 104
    label "Honduras"
  ]
  node [
    id 105
    label "Bangladesz"
  ]
  node [
    id 106
    label "Peru"
  ]
  node [
    id 107
    label "Kazachstan"
  ]
  node [
    id 108
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 109
    label "Irak"
  ]
  node [
    id 110
    label "holoarktyka"
  ]
  node [
    id 111
    label "USA"
  ]
  node [
    id 112
    label "Sudan"
  ]
  node [
    id 113
    label "Nepal"
  ]
  node [
    id 114
    label "San_Marino"
  ]
  node [
    id 115
    label "Burundi"
  ]
  node [
    id 116
    label "Dominikana"
  ]
  node [
    id 117
    label "Komory"
  ]
  node [
    id 118
    label "granica_pa&#324;stwa"
  ]
  node [
    id 119
    label "Gwatemala"
  ]
  node [
    id 120
    label "Antarktis"
  ]
  node [
    id 121
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 122
    label "Brunei"
  ]
  node [
    id 123
    label "Iran"
  ]
  node [
    id 124
    label "Zimbabwe"
  ]
  node [
    id 125
    label "Namibia"
  ]
  node [
    id 126
    label "Meksyk"
  ]
  node [
    id 127
    label "Kamerun"
  ]
  node [
    id 128
    label "zwrot"
  ]
  node [
    id 129
    label "Somalia"
  ]
  node [
    id 130
    label "Angola"
  ]
  node [
    id 131
    label "Gabon"
  ]
  node [
    id 132
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 133
    label "Mozambik"
  ]
  node [
    id 134
    label "Tajwan"
  ]
  node [
    id 135
    label "Tunezja"
  ]
  node [
    id 136
    label "Nowa_Zelandia"
  ]
  node [
    id 137
    label "Liban"
  ]
  node [
    id 138
    label "Jordania"
  ]
  node [
    id 139
    label "Tonga"
  ]
  node [
    id 140
    label "Czad"
  ]
  node [
    id 141
    label "Liberia"
  ]
  node [
    id 142
    label "Gwinea"
  ]
  node [
    id 143
    label "Belize"
  ]
  node [
    id 144
    label "&#321;otwa"
  ]
  node [
    id 145
    label "Syria"
  ]
  node [
    id 146
    label "Benin"
  ]
  node [
    id 147
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 148
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 149
    label "Dominika"
  ]
  node [
    id 150
    label "Antigua_i_Barbuda"
  ]
  node [
    id 151
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 152
    label "Hanower"
  ]
  node [
    id 153
    label "partia"
  ]
  node [
    id 154
    label "Afganistan"
  ]
  node [
    id 155
    label "Kiribati"
  ]
  node [
    id 156
    label "W&#322;ochy"
  ]
  node [
    id 157
    label "Szwajcaria"
  ]
  node [
    id 158
    label "Sahara_Zachodnia"
  ]
  node [
    id 159
    label "Chorwacja"
  ]
  node [
    id 160
    label "Tajlandia"
  ]
  node [
    id 161
    label "Salwador"
  ]
  node [
    id 162
    label "Bahamy"
  ]
  node [
    id 163
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 164
    label "S&#322;owenia"
  ]
  node [
    id 165
    label "Gambia"
  ]
  node [
    id 166
    label "Urugwaj"
  ]
  node [
    id 167
    label "Zair"
  ]
  node [
    id 168
    label "Erytrea"
  ]
  node [
    id 169
    label "Rosja"
  ]
  node [
    id 170
    label "Uganda"
  ]
  node [
    id 171
    label "Niger"
  ]
  node [
    id 172
    label "Mauritius"
  ]
  node [
    id 173
    label "Turkmenistan"
  ]
  node [
    id 174
    label "Turcja"
  ]
  node [
    id 175
    label "Irlandia"
  ]
  node [
    id 176
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 177
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 178
    label "Gwinea_Bissau"
  ]
  node [
    id 179
    label "Belgia"
  ]
  node [
    id 180
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 181
    label "Palau"
  ]
  node [
    id 182
    label "Barbados"
  ]
  node [
    id 183
    label "Chile"
  ]
  node [
    id 184
    label "Wenezuela"
  ]
  node [
    id 185
    label "W&#281;gry"
  ]
  node [
    id 186
    label "Argentyna"
  ]
  node [
    id 187
    label "Kolumbia"
  ]
  node [
    id 188
    label "Sierra_Leone"
  ]
  node [
    id 189
    label "Azerbejd&#380;an"
  ]
  node [
    id 190
    label "Kongo"
  ]
  node [
    id 191
    label "Pakistan"
  ]
  node [
    id 192
    label "Liechtenstein"
  ]
  node [
    id 193
    label "Nikaragua"
  ]
  node [
    id 194
    label "Senegal"
  ]
  node [
    id 195
    label "Indie"
  ]
  node [
    id 196
    label "Suazi"
  ]
  node [
    id 197
    label "Polska"
  ]
  node [
    id 198
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 199
    label "Algieria"
  ]
  node [
    id 200
    label "terytorium"
  ]
  node [
    id 201
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 202
    label "Jamajka"
  ]
  node [
    id 203
    label "Kostaryka"
  ]
  node [
    id 204
    label "Timor_Wschodni"
  ]
  node [
    id 205
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 206
    label "Kuba"
  ]
  node [
    id 207
    label "Mauretania"
  ]
  node [
    id 208
    label "Portoryko"
  ]
  node [
    id 209
    label "Brazylia"
  ]
  node [
    id 210
    label "Mo&#322;dawia"
  ]
  node [
    id 211
    label "organizacja"
  ]
  node [
    id 212
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 213
    label "Litwa"
  ]
  node [
    id 214
    label "Kirgistan"
  ]
  node [
    id 215
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 216
    label "Izrael"
  ]
  node [
    id 217
    label "Grecja"
  ]
  node [
    id 218
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 219
    label "Holandia"
  ]
  node [
    id 220
    label "Sri_Lanka"
  ]
  node [
    id 221
    label "Katar"
  ]
  node [
    id 222
    label "Mikronezja"
  ]
  node [
    id 223
    label "Mongolia"
  ]
  node [
    id 224
    label "Laos"
  ]
  node [
    id 225
    label "Malediwy"
  ]
  node [
    id 226
    label "Zambia"
  ]
  node [
    id 227
    label "Tanzania"
  ]
  node [
    id 228
    label "Gujana"
  ]
  node [
    id 229
    label "Czechy"
  ]
  node [
    id 230
    label "Panama"
  ]
  node [
    id 231
    label "Uzbekistan"
  ]
  node [
    id 232
    label "Gruzja"
  ]
  node [
    id 233
    label "Serbia"
  ]
  node [
    id 234
    label "Francja"
  ]
  node [
    id 235
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 236
    label "Togo"
  ]
  node [
    id 237
    label "Estonia"
  ]
  node [
    id 238
    label "Oman"
  ]
  node [
    id 239
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 240
    label "Portugalia"
  ]
  node [
    id 241
    label "Boliwia"
  ]
  node [
    id 242
    label "Luksemburg"
  ]
  node [
    id 243
    label "Haiti"
  ]
  node [
    id 244
    label "Wyspy_Salomona"
  ]
  node [
    id 245
    label "Birma"
  ]
  node [
    id 246
    label "Rodezja"
  ]
  node [
    id 247
    label "poprosi&#263;"
  ]
  node [
    id 248
    label "ask"
  ]
  node [
    id 249
    label "invite"
  ]
  node [
    id 250
    label "zach&#281;ci&#263;"
  ]
  node [
    id 251
    label "adduce"
  ]
  node [
    id 252
    label "przewo&#322;a&#263;"
  ]
  node [
    id 253
    label "poinformowa&#263;"
  ]
  node [
    id 254
    label "nakaza&#263;"
  ]
  node [
    id 255
    label "sta&#263;_si&#281;"
  ]
  node [
    id 256
    label "spowodowa&#263;"
  ]
  node [
    id 257
    label "wskaza&#263;"
  ]
  node [
    id 258
    label "install"
  ]
  node [
    id 259
    label "ustali&#263;"
  ]
  node [
    id 260
    label "situate"
  ]
  node [
    id 261
    label "dodatkowo"
  ]
  node [
    id 262
    label "uboczny"
  ]
  node [
    id 263
    label "poboczny"
  ]
  node [
    id 264
    label "za&#322;o&#380;enie"
  ]
  node [
    id 265
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 266
    label "umowa"
  ]
  node [
    id 267
    label "agent"
  ]
  node [
    id 268
    label "condition"
  ]
  node [
    id 269
    label "ekspozycja"
  ]
  node [
    id 270
    label "faktor"
  ]
  node [
    id 271
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 272
    label "zrobienie"
  ]
  node [
    id 273
    label "use"
  ]
  node [
    id 274
    label "u&#380;ycie"
  ]
  node [
    id 275
    label "stosowanie"
  ]
  node [
    id 276
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 277
    label "u&#380;yteczny"
  ]
  node [
    id 278
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 279
    label "exploitation"
  ]
  node [
    id 280
    label "dar"
  ]
  node [
    id 281
    label "cnota"
  ]
  node [
    id 282
    label "buddyzm"
  ]
  node [
    id 283
    label "osobowo"
  ]
  node [
    id 284
    label "kognicja"
  ]
  node [
    id 285
    label "idea"
  ]
  node [
    id 286
    label "szczeg&#243;&#322;"
  ]
  node [
    id 287
    label "rzecz"
  ]
  node [
    id 288
    label "wydarzenie"
  ]
  node [
    id 289
    label "przes&#322;anka"
  ]
  node [
    id 290
    label "rozprawa"
  ]
  node [
    id 291
    label "object"
  ]
  node [
    id 292
    label "proposition"
  ]
  node [
    id 293
    label "twierdzenie"
  ]
  node [
    id 294
    label "my&#347;l"
  ]
  node [
    id 295
    label "wnioskowanie"
  ]
  node [
    id 296
    label "propozycja"
  ]
  node [
    id 297
    label "motion"
  ]
  node [
    id 298
    label "pismo"
  ]
  node [
    id 299
    label "prayer"
  ]
  node [
    id 300
    label "zgodzi&#263;"
  ]
  node [
    id 301
    label "pomocnik"
  ]
  node [
    id 302
    label "doch&#243;d"
  ]
  node [
    id 303
    label "property"
  ]
  node [
    id 304
    label "przedmiot"
  ]
  node [
    id 305
    label "telefon_zaufania"
  ]
  node [
    id 306
    label "darowizna"
  ]
  node [
    id 307
    label "&#347;rodek"
  ]
  node [
    id 308
    label "liga"
  ]
  node [
    id 309
    label "prawniczo"
  ]
  node [
    id 310
    label "prawnie"
  ]
  node [
    id 311
    label "konstytucyjnoprawny"
  ]
  node [
    id 312
    label "legalny"
  ]
  node [
    id 313
    label "jurydyczny"
  ]
  node [
    id 314
    label "si&#281;ga&#263;"
  ]
  node [
    id 315
    label "trwa&#263;"
  ]
  node [
    id 316
    label "obecno&#347;&#263;"
  ]
  node [
    id 317
    label "stan"
  ]
  node [
    id 318
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 319
    label "stand"
  ]
  node [
    id 320
    label "mie&#263;_miejsce"
  ]
  node [
    id 321
    label "uczestniczy&#263;"
  ]
  node [
    id 322
    label "chodzi&#263;"
  ]
  node [
    id 323
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 324
    label "equal"
  ]
  node [
    id 325
    label "stworzy&#263;"
  ]
  node [
    id 326
    label "manufacture"
  ]
  node [
    id 327
    label "actualize"
  ]
  node [
    id 328
    label "realize"
  ]
  node [
    id 329
    label "wykorzysta&#263;"
  ]
  node [
    id 330
    label "spieni&#281;&#380;y&#263;"
  ]
  node [
    id 331
    label "strona"
  ]
  node [
    id 332
    label "przyczyna"
  ]
  node [
    id 333
    label "matuszka"
  ]
  node [
    id 334
    label "geneza"
  ]
  node [
    id 335
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 336
    label "czynnik"
  ]
  node [
    id 337
    label "poci&#261;ganie"
  ]
  node [
    id 338
    label "rezultat"
  ]
  node [
    id 339
    label "uprz&#261;&#380;"
  ]
  node [
    id 340
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 341
    label "subject"
  ]
  node [
    id 342
    label "prywatywny"
  ]
  node [
    id 343
    label "defect"
  ]
  node [
    id 344
    label "odej&#347;cie"
  ]
  node [
    id 345
    label "gap"
  ]
  node [
    id 346
    label "kr&#243;tki"
  ]
  node [
    id 347
    label "wyr&#243;b"
  ]
  node [
    id 348
    label "nieistnienie"
  ]
  node [
    id 349
    label "wada"
  ]
  node [
    id 350
    label "odej&#347;&#263;"
  ]
  node [
    id 351
    label "odchodzenie"
  ]
  node [
    id 352
    label "odchodzi&#263;"
  ]
  node [
    id 353
    label "okre&#347;lony"
  ]
  node [
    id 354
    label "jaki&#347;"
  ]
  node [
    id 355
    label "pacjent"
  ]
  node [
    id 356
    label "kategoria_gramatyczna"
  ]
  node [
    id 357
    label "schorzenie"
  ]
  node [
    id 358
    label "przeznaczenie"
  ]
  node [
    id 359
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 360
    label "happening"
  ]
  node [
    id 361
    label "przyk&#322;ad"
  ]
  node [
    id 362
    label "jednakowo"
  ]
  node [
    id 363
    label "spokojnie"
  ]
  node [
    id 364
    label "zgodny"
  ]
  node [
    id 365
    label "dobrze"
  ]
  node [
    id 366
    label "zbie&#380;nie"
  ]
  node [
    id 367
    label "ten"
  ]
  node [
    id 368
    label "toaleta"
  ]
  node [
    id 369
    label "artyku&#322;"
  ]
  node [
    id 370
    label "passage"
  ]
  node [
    id 371
    label "urywek"
  ]
  node [
    id 372
    label "tekst"
  ]
  node [
    id 373
    label "fragment"
  ]
  node [
    id 374
    label "charge"
  ]
  node [
    id 375
    label "dopomnie&#263;_si&#281;"
  ]
  node [
    id 376
    label "nakazywa&#263;"
  ]
  node [
    id 377
    label "cry"
  ]
  node [
    id 378
    label "pobudza&#263;"
  ]
  node [
    id 379
    label "donosi&#263;"
  ]
  node [
    id 380
    label "order"
  ]
  node [
    id 381
    label "prosi&#263;"
  ]
  node [
    id 382
    label "address"
  ]
  node [
    id 383
    label "da&#263;"
  ]
  node [
    id 384
    label "odst&#261;pi&#263;"
  ]
  node [
    id 385
    label "promocja"
  ]
  node [
    id 386
    label "udost&#281;pni&#263;"
  ]
  node [
    id 387
    label "przyzna&#263;"
  ]
  node [
    id 388
    label "give"
  ]
  node [
    id 389
    label "picture"
  ]
  node [
    id 390
    label "doj&#347;cie"
  ]
  node [
    id 391
    label "doj&#347;&#263;"
  ]
  node [
    id 392
    label "powzi&#261;&#263;"
  ]
  node [
    id 393
    label "wiedza"
  ]
  node [
    id 394
    label "sygna&#322;"
  ]
  node [
    id 395
    label "obiegni&#281;cie"
  ]
  node [
    id 396
    label "obieganie"
  ]
  node [
    id 397
    label "obiec"
  ]
  node [
    id 398
    label "dane"
  ]
  node [
    id 399
    label "obiega&#263;"
  ]
  node [
    id 400
    label "punkt"
  ]
  node [
    id 401
    label "publikacja"
  ]
  node [
    id 402
    label "powzi&#281;cie"
  ]
  node [
    id 403
    label "fraza"
  ]
  node [
    id 404
    label "forma"
  ]
  node [
    id 405
    label "melodia"
  ]
  node [
    id 406
    label "zbacza&#263;"
  ]
  node [
    id 407
    label "entity"
  ]
  node [
    id 408
    label "omawia&#263;"
  ]
  node [
    id 409
    label "topik"
  ]
  node [
    id 410
    label "wyraz_pochodny"
  ]
  node [
    id 411
    label "om&#243;wi&#263;"
  ]
  node [
    id 412
    label "omawianie"
  ]
  node [
    id 413
    label "w&#261;tek"
  ]
  node [
    id 414
    label "forum"
  ]
  node [
    id 415
    label "cecha"
  ]
  node [
    id 416
    label "zboczenie"
  ]
  node [
    id 417
    label "zbaczanie"
  ]
  node [
    id 418
    label "tre&#347;&#263;"
  ]
  node [
    id 419
    label "tematyka"
  ]
  node [
    id 420
    label "istota"
  ]
  node [
    id 421
    label "otoczka"
  ]
  node [
    id 422
    label "zboczy&#263;"
  ]
  node [
    id 423
    label "om&#243;wienie"
  ]
  node [
    id 424
    label "dokument"
  ]
  node [
    id 425
    label "forsing"
  ]
  node [
    id 426
    label "certificate"
  ]
  node [
    id 427
    label "rewizja"
  ]
  node [
    id 428
    label "argument"
  ]
  node [
    id 429
    label "act"
  ]
  node [
    id 430
    label "uzasadnienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 29
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 301
  ]
  edge [
    source 13
    target 302
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 304
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 305
  ]
  edge [
    source 13
    target 306
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 309
  ]
  edge [
    source 14
    target 310
  ]
  edge [
    source 14
    target 311
  ]
  edge [
    source 14
    target 312
  ]
  edge [
    source 14
    target 313
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 314
  ]
  edge [
    source 16
    target 315
  ]
  edge [
    source 16
    target 316
  ]
  edge [
    source 16
    target 317
  ]
  edge [
    source 16
    target 318
  ]
  edge [
    source 16
    target 319
  ]
  edge [
    source 16
    target 320
  ]
  edge [
    source 16
    target 321
  ]
  edge [
    source 16
    target 322
  ]
  edge [
    source 16
    target 323
  ]
  edge [
    source 16
    target 324
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 256
  ]
  edge [
    source 17
    target 325
  ]
  edge [
    source 17
    target 326
  ]
  edge [
    source 17
    target 327
  ]
  edge [
    source 17
    target 328
  ]
  edge [
    source 17
    target 329
  ]
  edge [
    source 17
    target 330
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 331
  ]
  edge [
    source 18
    target 332
  ]
  edge [
    source 18
    target 333
  ]
  edge [
    source 18
    target 334
  ]
  edge [
    source 18
    target 335
  ]
  edge [
    source 18
    target 336
  ]
  edge [
    source 18
    target 337
  ]
  edge [
    source 18
    target 338
  ]
  edge [
    source 18
    target 339
  ]
  edge [
    source 18
    target 340
  ]
  edge [
    source 18
    target 341
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 342
  ]
  edge [
    source 19
    target 343
  ]
  edge [
    source 19
    target 344
  ]
  edge [
    source 19
    target 345
  ]
  edge [
    source 19
    target 346
  ]
  edge [
    source 19
    target 347
  ]
  edge [
    source 19
    target 348
  ]
  edge [
    source 19
    target 349
  ]
  edge [
    source 19
    target 350
  ]
  edge [
    source 19
    target 351
  ]
  edge [
    source 19
    target 352
  ]
  edge [
    source 20
    target 353
  ]
  edge [
    source 20
    target 354
  ]
  edge [
    source 21
    target 355
  ]
  edge [
    source 21
    target 356
  ]
  edge [
    source 21
    target 357
  ]
  edge [
    source 21
    target 358
  ]
  edge [
    source 21
    target 359
  ]
  edge [
    source 21
    target 288
  ]
  edge [
    source 21
    target 360
  ]
  edge [
    source 21
    target 361
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 362
  ]
  edge [
    source 22
    target 363
  ]
  edge [
    source 22
    target 364
  ]
  edge [
    source 22
    target 365
  ]
  edge [
    source 22
    target 366
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 367
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 368
  ]
  edge [
    source 24
    target 369
  ]
  edge [
    source 24
    target 370
  ]
  edge [
    source 24
    target 371
  ]
  edge [
    source 24
    target 372
  ]
  edge [
    source 24
    target 373
  ]
  edge [
    source 25
    target 374
  ]
  edge [
    source 25
    target 375
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 376
  ]
  edge [
    source 26
    target 377
  ]
  edge [
    source 26
    target 378
  ]
  edge [
    source 26
    target 249
  ]
  edge [
    source 26
    target 379
  ]
  edge [
    source 26
    target 380
  ]
  edge [
    source 26
    target 381
  ]
  edge [
    source 26
    target 382
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 383
  ]
  edge [
    source 27
    target 384
  ]
  edge [
    source 27
    target 385
  ]
  edge [
    source 27
    target 386
  ]
  edge [
    source 27
    target 387
  ]
  edge [
    source 27
    target 388
  ]
  edge [
    source 27
    target 389
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 390
  ]
  edge [
    source 28
    target 391
  ]
  edge [
    source 28
    target 392
  ]
  edge [
    source 28
    target 393
  ]
  edge [
    source 28
    target 394
  ]
  edge [
    source 28
    target 395
  ]
  edge [
    source 28
    target 396
  ]
  edge [
    source 28
    target 397
  ]
  edge [
    source 28
    target 398
  ]
  edge [
    source 28
    target 399
  ]
  edge [
    source 28
    target 400
  ]
  edge [
    source 28
    target 401
  ]
  edge [
    source 28
    target 402
  ]
  edge [
    source 28
    target 28
  ]
  edge [
    source 29
    target 403
  ]
  edge [
    source 29
    target 404
  ]
  edge [
    source 29
    target 405
  ]
  edge [
    source 29
    target 287
  ]
  edge [
    source 29
    target 406
  ]
  edge [
    source 29
    target 407
  ]
  edge [
    source 29
    target 408
  ]
  edge [
    source 29
    target 409
  ]
  edge [
    source 29
    target 410
  ]
  edge [
    source 29
    target 411
  ]
  edge [
    source 29
    target 412
  ]
  edge [
    source 29
    target 413
  ]
  edge [
    source 29
    target 414
  ]
  edge [
    source 29
    target 415
  ]
  edge [
    source 29
    target 416
  ]
  edge [
    source 29
    target 417
  ]
  edge [
    source 29
    target 418
  ]
  edge [
    source 29
    target 419
  ]
  edge [
    source 29
    target 420
  ]
  edge [
    source 29
    target 421
  ]
  edge [
    source 29
    target 422
  ]
  edge [
    source 29
    target 423
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 424
  ]
  edge [
    source 30
    target 425
  ]
  edge [
    source 30
    target 426
  ]
  edge [
    source 30
    target 427
  ]
  edge [
    source 30
    target 428
  ]
  edge [
    source 30
    target 429
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 430
  ]
]
