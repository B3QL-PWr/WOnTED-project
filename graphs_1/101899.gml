graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.8372093023255813
  density 0.02161422708618331
  graphCliqueNumber 3
  node [
    id 0
    label "w&#281;giel"
    origin "text"
  ]
  node [
    id 1
    label "kamienny"
    origin "text"
  ]
  node [
    id 2
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przez"
    origin "text"
  ]
  node [
    id 5
    label "elektroenergetyka"
    origin "text"
  ]
  node [
    id 6
    label "ciep&#322;ownictwo"
    origin "text"
  ]
  node [
    id 7
    label "koksownictwo"
    origin "text"
  ]
  node [
    id 8
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 9
    label "sektor"
    origin "text"
  ]
  node [
    id 10
    label "komunalno"
    origin "text"
  ]
  node [
    id 11
    label "bytowy"
    origin "text"
  ]
  node [
    id 12
    label "surowiec_energetyczny"
  ]
  node [
    id 13
    label "w&#281;glowiec"
  ]
  node [
    id 14
    label "w&#281;glowodan"
  ]
  node [
    id 15
    label "kopalina_podstawowa"
  ]
  node [
    id 16
    label "coal"
  ]
  node [
    id 17
    label "przybory_do_pisania"
  ]
  node [
    id 18
    label "makroelement"
  ]
  node [
    id 19
    label "niemetal"
  ]
  node [
    id 20
    label "zsypnik"
  ]
  node [
    id 21
    label "w&#281;glarka"
  ]
  node [
    id 22
    label "zw&#281;glenie_si&#281;"
  ]
  node [
    id 23
    label "zw&#281;glanie_si&#281;"
  ]
  node [
    id 24
    label "coil"
  ]
  node [
    id 25
    label "bry&#322;a"
  ]
  node [
    id 26
    label "ska&#322;a"
  ]
  node [
    id 27
    label "carbon"
  ]
  node [
    id 28
    label "fulleren"
  ]
  node [
    id 29
    label "rysunek"
  ]
  node [
    id 30
    label "niewzruszony"
  ]
  node [
    id 31
    label "twardy"
  ]
  node [
    id 32
    label "kamiennie"
  ]
  node [
    id 33
    label "naturalny"
  ]
  node [
    id 34
    label "g&#322;&#281;boki"
  ]
  node [
    id 35
    label "mineralny"
  ]
  node [
    id 36
    label "ch&#322;odny"
  ]
  node [
    id 37
    label "ghaty"
  ]
  node [
    id 38
    label "use"
  ]
  node [
    id 39
    label "krzywdzi&#263;"
  ]
  node [
    id 40
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 41
    label "distribute"
  ]
  node [
    id 42
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 43
    label "give"
  ]
  node [
    id 44
    label "liga&#263;"
  ]
  node [
    id 45
    label "u&#380;ywa&#263;"
  ]
  node [
    id 46
    label "korzysta&#263;"
  ]
  node [
    id 47
    label "si&#281;ga&#263;"
  ]
  node [
    id 48
    label "trwa&#263;"
  ]
  node [
    id 49
    label "obecno&#347;&#263;"
  ]
  node [
    id 50
    label "stan"
  ]
  node [
    id 51
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 52
    label "stand"
  ]
  node [
    id 53
    label "mie&#263;_miejsce"
  ]
  node [
    id 54
    label "uczestniczy&#263;"
  ]
  node [
    id 55
    label "chodzi&#263;"
  ]
  node [
    id 56
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 57
    label "equal"
  ]
  node [
    id 58
    label "energetyka"
  ]
  node [
    id 59
    label "heat"
  ]
  node [
    id 60
    label "gospodarka"
  ]
  node [
    id 61
    label "przechowalnictwo"
  ]
  node [
    id 62
    label "uprzemys&#322;owienie"
  ]
  node [
    id 63
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 64
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 65
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 66
    label "uprzemys&#322;awianie"
  ]
  node [
    id 67
    label "klaster_dyskowy"
  ]
  node [
    id 68
    label "dziedzina"
  ]
  node [
    id 69
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 70
    label "widownia"
  ]
  node [
    id 71
    label "zag&#322;&#281;bie"
  ]
  node [
    id 72
    label "g&#243;rno&#347;l&#261;ski"
  ]
  node [
    id 73
    label "Zag&#322;ebie"
  ]
  node [
    id 74
    label "lubelski"
  ]
  node [
    id 75
    label "ruda"
  ]
  node [
    id 76
    label "&#347;l&#261;ski"
  ]
  node [
    id 77
    label "elektrownia"
  ]
  node [
    id 78
    label "Be&#322;chat&#243;w"
  ]
  node [
    id 79
    label "Gorz&#243;w"
  ]
  node [
    id 80
    label "wielkopolski"
  ]
  node [
    id 81
    label "kamie&#324;"
  ]
  node [
    id 82
    label "pomorski"
  ]
  node [
    id 83
    label "morze"
  ]
  node [
    id 84
    label "ba&#322;tycki"
  ]
  node [
    id 85
    label "ostrowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 80
    target 85
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 83
    target 84
  ]
]
