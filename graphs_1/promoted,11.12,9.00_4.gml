graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9111111111111112
  density 0.043434343434343436
  graphCliqueNumber 2
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "rozdzia&#322;"
    origin "text"
  ]
  node [
    id 2
    label "naturoterapia"
    origin "text"
  ]
  node [
    id 3
    label "otworzy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "znany"
    origin "text"
  ]
  node [
    id 5
    label "znachor"
    origin "text"
  ]
  node [
    id 6
    label "jerzy"
    origin "text"
  ]
  node [
    id 7
    label "zi&#281;ba"
    origin "text"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "nowotny"
  ]
  node [
    id 10
    label "drugi"
  ]
  node [
    id 11
    label "kolejny"
  ]
  node [
    id 12
    label "bie&#380;&#261;cy"
  ]
  node [
    id 13
    label "nowo"
  ]
  node [
    id 14
    label "narybek"
  ]
  node [
    id 15
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 16
    label "obcy"
  ]
  node [
    id 17
    label "faza"
  ]
  node [
    id 18
    label "interruption"
  ]
  node [
    id 19
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 20
    label "podzia&#322;"
  ]
  node [
    id 21
    label "podrozdzia&#322;"
  ]
  node [
    id 22
    label "wydarzenie"
  ]
  node [
    id 23
    label "fragment"
  ]
  node [
    id 24
    label "establish"
  ]
  node [
    id 25
    label "cia&#322;o"
  ]
  node [
    id 26
    label "zacz&#261;&#263;"
  ]
  node [
    id 27
    label "spowodowa&#263;"
  ]
  node [
    id 28
    label "begin"
  ]
  node [
    id 29
    label "udost&#281;pni&#263;"
  ]
  node [
    id 30
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 31
    label "uruchomi&#263;"
  ]
  node [
    id 32
    label "przeci&#261;&#263;"
  ]
  node [
    id 33
    label "wielki"
  ]
  node [
    id 34
    label "rozpowszechnianie"
  ]
  node [
    id 35
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 36
    label "medyk"
  ]
  node [
    id 37
    label "lekarz"
  ]
  node [
    id 38
    label "partacz"
  ]
  node [
    id 39
    label "niedouk"
  ]
  node [
    id 40
    label "paramedyk"
  ]
  node [
    id 41
    label "ptak_&#347;piewaj&#261;cy"
  ]
  node [
    id 42
    label "zi&#281;by"
  ]
  node [
    id 43
    label "Jerzy"
  ]
  node [
    id 44
    label "Zi&#281;ba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 43
    target 44
  ]
]
