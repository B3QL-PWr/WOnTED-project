graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "nawet"
    origin "text"
  ]
  node [
    id 1
    label "&#380;al"
    origin "text"
  ]
  node [
    id 2
    label "niezadowolenie"
  ]
  node [
    id 3
    label "wstyd"
  ]
  node [
    id 4
    label "gniewa&#263;_si&#281;"
  ]
  node [
    id 5
    label "gniewanie_si&#281;"
  ]
  node [
    id 6
    label "smutek"
  ]
  node [
    id 7
    label "czu&#263;"
  ]
  node [
    id 8
    label "emocja"
  ]
  node [
    id 9
    label "commiseration"
  ]
  node [
    id 10
    label "pang"
  ]
  node [
    id 11
    label "krytyka"
  ]
  node [
    id 12
    label "pogniewa&#263;_si&#281;"
  ]
  node [
    id 13
    label "uraza"
  ]
  node [
    id 14
    label "criticism"
  ]
  node [
    id 15
    label "sorrow"
  ]
  node [
    id 16
    label "pogniewanie_si&#281;"
  ]
  node [
    id 17
    label "sytuacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
]
