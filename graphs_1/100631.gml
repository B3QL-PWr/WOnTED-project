graph [
  maxDegree 155
  minDegree 1
  meanDegree 2.166101694915254
  density 0.003677592011740669
  graphCliqueNumber 3
  node [
    id 0
    label "policja"
    origin "text"
  ]
  node [
    id 1
    label "zatrzyma&#263;"
    origin "text"
  ]
  node [
    id 2
    label "bandyta"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "dwa"
    origin "text"
  ]
  node [
    id 5
    label "dni"
    origin "text"
  ]
  node [
    id 6
    label "wczesno"
    origin "text"
  ]
  node [
    id 7
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 8
    label "rozb&#243;j"
    origin "text"
  ]
  node [
    id 9
    label "park"
    origin "text"
  ]
  node [
    id 10
    label "rejon"
    origin "text"
  ]
  node [
    id 11
    label "ulica"
    origin "text"
  ]
  node [
    id 12
    label "starzy&#324;ski"
    origin "text"
  ]
  node [
    id 13
    label "listopadowy"
    origin "text"
  ]
  node [
    id 14
    label "przy"
    origin "text"
  ]
  node [
    id 15
    label "&#322;awka"
    origin "text"
  ]
  node [
    id 16
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 17
    label "raczy&#263;"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;"
    origin "text"
  ]
  node [
    id 19
    label "denaturat"
    origin "text"
  ]
  node [
    id 20
    label "przechodz&#261;cy"
    origin "text"
  ]
  node [
    id 21
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 22
    label "da&#263;"
    origin "text"
  ]
  node [
    id 23
    label "nauczka"
    origin "text"
  ]
  node [
    id 24
    label "jeden"
    origin "text"
  ]
  node [
    id 25
    label "pi&#263;"
    origin "text"
  ]
  node [
    id 26
    label "uciec"
    origin "text"
  ]
  node [
    id 27
    label "drugi"
    origin "text"
  ]
  node [
    id 28
    label "wiek"
    origin "text"
  ]
  node [
    id 29
    label "lata"
    origin "text"
  ]
  node [
    id 30
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 31
    label "dotkliwie"
    origin "text"
  ]
  node [
    id 32
    label "pobi&#263;"
    origin "text"
  ]
  node [
    id 33
    label "skra&#347;&#263;"
    origin "text"
  ]
  node [
    id 34
    label "kurtka"
    origin "text"
  ]
  node [
    id 35
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 36
    label "obc&#261;&#380;ki"
    origin "text"
  ]
  node [
    id 37
    label "paznokie&#263;"
    origin "text"
  ]
  node [
    id 38
    label "zatrzymany"
    origin "text"
  ]
  node [
    id 39
    label "bracia"
    origin "text"
  ]
  node [
    id 40
    label "krzysztof"
    origin "text"
  ]
  node [
    id 41
    label "robert"
    origin "text"
  ]
  node [
    id 42
    label "pawe&#322;"
    origin "text"
  ]
  node [
    id 43
    label "marcin"
    origin "text"
  ]
  node [
    id 44
    label "sprawca"
    origin "text"
  ]
  node [
    id 45
    label "maja"
    origin "text"
  ]
  node [
    id 46
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 47
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 48
    label "kilka"
    origin "text"
  ]
  node [
    id 49
    label "raz"
    origin "text"
  ]
  node [
    id 50
    label "karany"
    origin "text"
  ]
  node [
    id 51
    label "trzy"
    origin "text"
  ]
  node [
    id 52
    label "podczas"
    origin "text"
  ]
  node [
    id 53
    label "awantura"
    origin "text"
  ]
  node [
    id 54
    label "matka"
    origin "text"
  ]
  node [
    id 55
    label "uszkodzi&#263;"
    origin "text"
  ]
  node [
    id 56
    label "drzwi"
    origin "text"
  ]
  node [
    id 57
    label "gdy"
    origin "text"
  ]
  node [
    id 58
    label "dowiedzie&#263;"
    origin "text"
  ]
  node [
    id 59
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 60
    label "zawiadomi&#263;"
    origin "text"
  ]
  node [
    id 61
    label "wyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 62
    label "dom"
    origin "text"
  ]
  node [
    id 63
    label "zniszczy&#263;"
    origin "text"
  ]
  node [
    id 64
    label "kiosk"
    origin "text"
  ]
  node [
    id 65
    label "&#380;aluzja"
    origin "text"
  ]
  node [
    id 66
    label "wybi&#263;"
    origin "text"
  ]
  node [
    id 67
    label "szyb"
    origin "text"
  ]
  node [
    id 68
    label "komisariat"
  ]
  node [
    id 69
    label "psiarnia"
  ]
  node [
    id 70
    label "posterunek"
  ]
  node [
    id 71
    label "grupa"
  ]
  node [
    id 72
    label "organ"
  ]
  node [
    id 73
    label "s&#322;u&#380;ba"
  ]
  node [
    id 74
    label "continue"
  ]
  node [
    id 75
    label "zabra&#263;"
  ]
  node [
    id 76
    label "zaaresztowa&#263;"
  ]
  node [
    id 77
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 78
    label "spowodowa&#263;"
  ]
  node [
    id 79
    label "przerwa&#263;"
  ]
  node [
    id 80
    label "zamkn&#261;&#263;"
  ]
  node [
    id 81
    label "bury"
  ]
  node [
    id 82
    label "komornik"
  ]
  node [
    id 83
    label "unieruchomi&#263;"
  ]
  node [
    id 84
    label "suspend"
  ]
  node [
    id 85
    label "give"
  ]
  node [
    id 86
    label "bankrupt"
  ]
  node [
    id 87
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 88
    label "zaczepi&#263;"
  ]
  node [
    id 89
    label "przechowa&#263;"
  ]
  node [
    id 90
    label "anticipate"
  ]
  node [
    id 91
    label "apasz"
  ]
  node [
    id 92
    label "przest&#281;pca"
  ]
  node [
    id 93
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 94
    label "czas"
  ]
  node [
    id 95
    label "wcze&#347;nie"
  ]
  node [
    id 96
    label "przesta&#263;"
  ]
  node [
    id 97
    label "zrobi&#263;"
  ]
  node [
    id 98
    label "cause"
  ]
  node [
    id 99
    label "communicate"
  ]
  node [
    id 100
    label "atak"
  ]
  node [
    id 101
    label "wyzysk"
  ]
  node [
    id 102
    label "teren_przemys&#322;owy"
  ]
  node [
    id 103
    label "miejsce"
  ]
  node [
    id 104
    label "ballpark"
  ]
  node [
    id 105
    label "obszar"
  ]
  node [
    id 106
    label "sprz&#281;t"
  ]
  node [
    id 107
    label "tabor"
  ]
  node [
    id 108
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 109
    label "teren_zielony"
  ]
  node [
    id 110
    label "Skandynawia"
  ]
  node [
    id 111
    label "Yorkshire"
  ]
  node [
    id 112
    label "Kaukaz"
  ]
  node [
    id 113
    label "Kaszmir"
  ]
  node [
    id 114
    label "Toskania"
  ]
  node [
    id 115
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 116
    label "&#321;emkowszczyzna"
  ]
  node [
    id 117
    label "Amhara"
  ]
  node [
    id 118
    label "Lombardia"
  ]
  node [
    id 119
    label "Podbeskidzie"
  ]
  node [
    id 120
    label "Kalabria"
  ]
  node [
    id 121
    label "Tyrol"
  ]
  node [
    id 122
    label "Neogea"
  ]
  node [
    id 123
    label "Pamir"
  ]
  node [
    id 124
    label "Lubelszczyzna"
  ]
  node [
    id 125
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 126
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 127
    label "&#379;ywiecczyzna"
  ]
  node [
    id 128
    label "Europa_Wschodnia"
  ]
  node [
    id 129
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 130
    label "Zabajkale"
  ]
  node [
    id 131
    label "Kaszuby"
  ]
  node [
    id 132
    label "Bo&#347;nia"
  ]
  node [
    id 133
    label "Noworosja"
  ]
  node [
    id 134
    label "Ba&#322;kany"
  ]
  node [
    id 135
    label "Antarktyka"
  ]
  node [
    id 136
    label "Anglia"
  ]
  node [
    id 137
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 138
    label "Kielecczyzna"
  ]
  node [
    id 139
    label "Pomorze_Zachodnie"
  ]
  node [
    id 140
    label "Opolskie"
  ]
  node [
    id 141
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 142
    label "Ko&#322;yma"
  ]
  node [
    id 143
    label "Oksytania"
  ]
  node [
    id 144
    label "Arktyka"
  ]
  node [
    id 145
    label "Syjon"
  ]
  node [
    id 146
    label "Kresy_Zachodnie"
  ]
  node [
    id 147
    label "Kociewie"
  ]
  node [
    id 148
    label "Huculszczyzna"
  ]
  node [
    id 149
    label "wsch&#243;d"
  ]
  node [
    id 150
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 151
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 152
    label "Bawaria"
  ]
  node [
    id 153
    label "Rakowice"
  ]
  node [
    id 154
    label "Syberia_Wschodnia"
  ]
  node [
    id 155
    label "Maghreb"
  ]
  node [
    id 156
    label "Bory_Tucholskie"
  ]
  node [
    id 157
    label "Europa_Zachodnia"
  ]
  node [
    id 158
    label "antroposfera"
  ]
  node [
    id 159
    label "Kerala"
  ]
  node [
    id 160
    label "Podhale"
  ]
  node [
    id 161
    label "Kabylia"
  ]
  node [
    id 162
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 163
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 164
    label "Ma&#322;opolska"
  ]
  node [
    id 165
    label "Polesie"
  ]
  node [
    id 166
    label "Liguria"
  ]
  node [
    id 167
    label "&#321;&#243;dzkie"
  ]
  node [
    id 168
    label "Syberia_Zachodnia"
  ]
  node [
    id 169
    label "Notogea"
  ]
  node [
    id 170
    label "Palestyna"
  ]
  node [
    id 171
    label "&#321;&#281;g"
  ]
  node [
    id 172
    label "Bojkowszczyzna"
  ]
  node [
    id 173
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 174
    label "Karaiby"
  ]
  node [
    id 175
    label "S&#261;decczyzna"
  ]
  node [
    id 176
    label "Zab&#322;ocie"
  ]
  node [
    id 177
    label "Sand&#380;ak"
  ]
  node [
    id 178
    label "Nadrenia"
  ]
  node [
    id 179
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 180
    label "Zakarpacie"
  ]
  node [
    id 181
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 182
    label "Zag&#243;rze"
  ]
  node [
    id 183
    label "Andaluzja"
  ]
  node [
    id 184
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 185
    label "Turkiestan"
  ]
  node [
    id 186
    label "Zabu&#380;e"
  ]
  node [
    id 187
    label "Naddniestrze"
  ]
  node [
    id 188
    label "Hercegowina"
  ]
  node [
    id 189
    label "Opolszczyzna"
  ]
  node [
    id 190
    label "Lotaryngia"
  ]
  node [
    id 191
    label "pas_planetoid"
  ]
  node [
    id 192
    label "Afryka_Wschodnia"
  ]
  node [
    id 193
    label "Szlezwik"
  ]
  node [
    id 194
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 195
    label "holarktyka"
  ]
  node [
    id 196
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 197
    label "akrecja"
  ]
  node [
    id 198
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 199
    label "Mazowsze"
  ]
  node [
    id 200
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 201
    label "Afryka_Zachodnia"
  ]
  node [
    id 202
    label "Galicja"
  ]
  node [
    id 203
    label "Szkocja"
  ]
  node [
    id 204
    label "po&#322;udnie"
  ]
  node [
    id 205
    label "Walia"
  ]
  node [
    id 206
    label "Powi&#347;le"
  ]
  node [
    id 207
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 208
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 209
    label "Ruda_Pabianicka"
  ]
  node [
    id 210
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 211
    label "Zamojszczyzna"
  ]
  node [
    id 212
    label "Kujawy"
  ]
  node [
    id 213
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 214
    label "Podlasie"
  ]
  node [
    id 215
    label "Laponia"
  ]
  node [
    id 216
    label "Umbria"
  ]
  node [
    id 217
    label "Mezoameryka"
  ]
  node [
    id 218
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 219
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 220
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 221
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 222
    label "Kurdystan"
  ]
  node [
    id 223
    label "Kampania"
  ]
  node [
    id 224
    label "Armagnac"
  ]
  node [
    id 225
    label "Polinezja"
  ]
  node [
    id 226
    label "Warmia"
  ]
  node [
    id 227
    label "Wielkopolska"
  ]
  node [
    id 228
    label "Kosowo"
  ]
  node [
    id 229
    label "Bordeaux"
  ]
  node [
    id 230
    label "Lauda"
  ]
  node [
    id 231
    label "p&#243;&#322;noc"
  ]
  node [
    id 232
    label "Mazury"
  ]
  node [
    id 233
    label "Podkarpacie"
  ]
  node [
    id 234
    label "Oceania"
  ]
  node [
    id 235
    label "Lasko"
  ]
  node [
    id 236
    label "Amazonia"
  ]
  node [
    id 237
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 238
    label "zach&#243;d"
  ]
  node [
    id 239
    label "Olszanica"
  ]
  node [
    id 240
    label "przestrze&#324;"
  ]
  node [
    id 241
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 242
    label "Kurpie"
  ]
  node [
    id 243
    label "Tonkin"
  ]
  node [
    id 244
    label "Piotrowo"
  ]
  node [
    id 245
    label "Azja_Wschodnia"
  ]
  node [
    id 246
    label "Mikronezja"
  ]
  node [
    id 247
    label "Ukraina_Zachodnia"
  ]
  node [
    id 248
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 249
    label "Turyngia"
  ]
  node [
    id 250
    label "Baszkiria"
  ]
  node [
    id 251
    label "Apulia"
  ]
  node [
    id 252
    label "Pow&#261;zki"
  ]
  node [
    id 253
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 254
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 255
    label "Indochiny"
  ]
  node [
    id 256
    label "Biskupizna"
  ]
  node [
    id 257
    label "Lubuskie"
  ]
  node [
    id 258
    label "Ludwin&#243;w"
  ]
  node [
    id 259
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 260
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 261
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 262
    label "&#347;rodowisko"
  ]
  node [
    id 263
    label "miasteczko"
  ]
  node [
    id 264
    label "streetball"
  ]
  node [
    id 265
    label "pierzeja"
  ]
  node [
    id 266
    label "pas_ruchu"
  ]
  node [
    id 267
    label "pas_rozdzielczy"
  ]
  node [
    id 268
    label "jezdnia"
  ]
  node [
    id 269
    label "droga"
  ]
  node [
    id 270
    label "korona_drogi"
  ]
  node [
    id 271
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 272
    label "chodnik"
  ]
  node [
    id 273
    label "arteria"
  ]
  node [
    id 274
    label "Broadway"
  ]
  node [
    id 275
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 276
    label "wysepka"
  ]
  node [
    id 277
    label "autostrada"
  ]
  node [
    id 278
    label "krzes&#322;o"
  ]
  node [
    id 279
    label "siedzenie"
  ]
  node [
    id 280
    label "blat"
  ]
  node [
    id 281
    label "mebel"
  ]
  node [
    id 282
    label "klasa"
  ]
  node [
    id 283
    label "ch&#322;opina"
  ]
  node [
    id 284
    label "cz&#322;owiek"
  ]
  node [
    id 285
    label "bratek"
  ]
  node [
    id 286
    label "jegomo&#347;&#263;"
  ]
  node [
    id 287
    label "doros&#322;y"
  ]
  node [
    id 288
    label "samiec"
  ]
  node [
    id 289
    label "ojciec"
  ]
  node [
    id 290
    label "twardziel"
  ]
  node [
    id 291
    label "androlog"
  ]
  node [
    id 292
    label "pa&#324;stwo"
  ]
  node [
    id 293
    label "m&#261;&#380;"
  ]
  node [
    id 294
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 295
    label "andropauza"
  ]
  node [
    id 296
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 297
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 298
    label "condescend"
  ]
  node [
    id 299
    label "jagodzianka_na_ko&#347;ciach"
  ]
  node [
    id 300
    label "paliwo"
  ]
  node [
    id 301
    label "denat"
  ]
  node [
    id 302
    label "rozpuszczalnik"
  ]
  node [
    id 303
    label "sta&#263;_si&#281;"
  ]
  node [
    id 304
    label "podj&#261;&#263;"
  ]
  node [
    id 305
    label "determine"
  ]
  node [
    id 306
    label "dostarczy&#263;"
  ]
  node [
    id 307
    label "obieca&#263;"
  ]
  node [
    id 308
    label "pozwoli&#263;"
  ]
  node [
    id 309
    label "przeznaczy&#263;"
  ]
  node [
    id 310
    label "doda&#263;"
  ]
  node [
    id 311
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 312
    label "wyrzec_si&#281;"
  ]
  node [
    id 313
    label "supply"
  ]
  node [
    id 314
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 315
    label "zada&#263;"
  ]
  node [
    id 316
    label "odst&#261;pi&#263;"
  ]
  node [
    id 317
    label "feed"
  ]
  node [
    id 318
    label "testify"
  ]
  node [
    id 319
    label "powierzy&#263;"
  ]
  node [
    id 320
    label "convey"
  ]
  node [
    id 321
    label "przekaza&#263;"
  ]
  node [
    id 322
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 323
    label "zap&#322;aci&#263;"
  ]
  node [
    id 324
    label "dress"
  ]
  node [
    id 325
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 326
    label "udost&#281;pni&#263;"
  ]
  node [
    id 327
    label "sztachn&#261;&#263;"
  ]
  node [
    id 328
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 329
    label "przywali&#263;"
  ]
  node [
    id 330
    label "rap"
  ]
  node [
    id 331
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 332
    label "picture"
  ]
  node [
    id 333
    label "lesson"
  ]
  node [
    id 334
    label "kara"
  ]
  node [
    id 335
    label "lekcja"
  ]
  node [
    id 336
    label "kieliszek"
  ]
  node [
    id 337
    label "shot"
  ]
  node [
    id 338
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 339
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 340
    label "jaki&#347;"
  ]
  node [
    id 341
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 342
    label "jednolicie"
  ]
  node [
    id 343
    label "w&#243;dka"
  ]
  node [
    id 344
    label "ten"
  ]
  node [
    id 345
    label "ujednolicenie"
  ]
  node [
    id 346
    label "jednakowy"
  ]
  node [
    id 347
    label "goli&#263;"
  ]
  node [
    id 348
    label "ch&#322;on&#261;&#263;"
  ]
  node [
    id 349
    label "gulp"
  ]
  node [
    id 350
    label "nabiera&#263;"
  ]
  node [
    id 351
    label "naoliwia&#263;_si&#281;"
  ]
  node [
    id 352
    label "dostarcza&#263;"
  ]
  node [
    id 353
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 354
    label "obci&#261;ga&#263;"
  ]
  node [
    id 355
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 356
    label "nawi&#261;zywa&#263;"
  ]
  node [
    id 357
    label "wch&#322;ania&#263;"
  ]
  node [
    id 358
    label "uwiera&#263;"
  ]
  node [
    id 359
    label "wzi&#261;&#263;"
  ]
  node [
    id 360
    label "fly"
  ]
  node [
    id 361
    label "wypierdoli&#263;"
  ]
  node [
    id 362
    label "pass"
  ]
  node [
    id 363
    label "zwia&#263;"
  ]
  node [
    id 364
    label "spieprzy&#263;"
  ]
  node [
    id 365
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 366
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 367
    label "inny"
  ]
  node [
    id 368
    label "kolejny"
  ]
  node [
    id 369
    label "przeciwny"
  ]
  node [
    id 370
    label "sw&#243;j"
  ]
  node [
    id 371
    label "odwrotnie"
  ]
  node [
    id 372
    label "dzie&#324;"
  ]
  node [
    id 373
    label "podobny"
  ]
  node [
    id 374
    label "wt&#243;ry"
  ]
  node [
    id 375
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 376
    label "period"
  ]
  node [
    id 377
    label "cecha"
  ]
  node [
    id 378
    label "rok"
  ]
  node [
    id 379
    label "long_time"
  ]
  node [
    id 380
    label "choroba_wieku"
  ]
  node [
    id 381
    label "jednostka_geologiczna"
  ]
  node [
    id 382
    label "chron"
  ]
  node [
    id 383
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 384
    label "summer"
  ]
  node [
    id 385
    label "proceed"
  ]
  node [
    id 386
    label "catch"
  ]
  node [
    id 387
    label "pozosta&#263;"
  ]
  node [
    id 388
    label "osta&#263;_si&#281;"
  ]
  node [
    id 389
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 390
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 391
    label "change"
  ]
  node [
    id 392
    label "przykro"
  ]
  node [
    id 393
    label "dotkliwy"
  ]
  node [
    id 394
    label "pozabija&#263;"
  ]
  node [
    id 395
    label "pousuwa&#263;"
  ]
  node [
    id 396
    label "transgress"
  ]
  node [
    id 397
    label "beat"
  ]
  node [
    id 398
    label "pokona&#263;"
  ]
  node [
    id 399
    label "upset"
  ]
  node [
    id 400
    label "zbi&#263;"
  ]
  node [
    id 401
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 402
    label "nala&#263;"
  ]
  node [
    id 403
    label "wpierniczy&#263;"
  ]
  node [
    id 404
    label "pouderza&#263;"
  ]
  node [
    id 405
    label "obi&#263;"
  ]
  node [
    id 406
    label "pomacha&#263;"
  ]
  node [
    id 407
    label "poniszczy&#263;"
  ]
  node [
    id 408
    label "wygra&#263;"
  ]
  node [
    id 409
    label "zw&#281;dzi&#263;"
  ]
  node [
    id 410
    label "podpierdoli&#263;"
  ]
  node [
    id 411
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 412
    label "overcharge"
  ]
  node [
    id 413
    label "okrycie"
  ]
  node [
    id 414
    label "jacket"
  ]
  node [
    id 415
    label "podszewka"
  ]
  node [
    id 416
    label "szlachetny"
  ]
  node [
    id 417
    label "metaliczny"
  ]
  node [
    id 418
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 419
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 420
    label "grosz"
  ]
  node [
    id 421
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 422
    label "utytu&#322;owany"
  ]
  node [
    id 423
    label "poz&#322;ocenie"
  ]
  node [
    id 424
    label "Polska"
  ]
  node [
    id 425
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 426
    label "wspania&#322;y"
  ]
  node [
    id 427
    label "doskona&#322;y"
  ]
  node [
    id 428
    label "kochany"
  ]
  node [
    id 429
    label "jednostka_monetarna"
  ]
  node [
    id 430
    label "z&#322;ocenie"
  ]
  node [
    id 431
    label "przybory_toaletowe"
  ]
  node [
    id 432
    label "przyrz&#261;d"
  ]
  node [
    id 433
    label "&#380;a&#322;oba"
  ]
  node [
    id 434
    label "ob&#322;&#261;czek"
  ]
  node [
    id 435
    label "palec"
  ]
  node [
    id 436
    label "linie_Beau"
  ]
  node [
    id 437
    label "sprawiciel"
  ]
  node [
    id 438
    label "wedyzm"
  ]
  node [
    id 439
    label "energia"
  ]
  node [
    id 440
    label "buddyzm"
  ]
  node [
    id 441
    label "dawny"
  ]
  node [
    id 442
    label "rozw&#243;d"
  ]
  node [
    id 443
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 444
    label "eksprezydent"
  ]
  node [
    id 445
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 446
    label "partner"
  ]
  node [
    id 447
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 448
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 449
    label "wcze&#347;niejszy"
  ]
  node [
    id 450
    label "&#347;ledziowate"
  ]
  node [
    id 451
    label "ryba"
  ]
  node [
    id 452
    label "chwila"
  ]
  node [
    id 453
    label "uderzenie"
  ]
  node [
    id 454
    label "cios"
  ]
  node [
    id 455
    label "time"
  ]
  node [
    id 456
    label "zaj&#347;cie"
  ]
  node [
    id 457
    label "cyrk"
  ]
  node [
    id 458
    label "argument"
  ]
  node [
    id 459
    label "scene"
  ]
  node [
    id 460
    label "sensacja"
  ]
  node [
    id 461
    label "smr&#243;d"
  ]
  node [
    id 462
    label "konflikt"
  ]
  node [
    id 463
    label "przygoda"
  ]
  node [
    id 464
    label "Matka_Boska"
  ]
  node [
    id 465
    label "matka_zast&#281;pcza"
  ]
  node [
    id 466
    label "stara"
  ]
  node [
    id 467
    label "rodzic"
  ]
  node [
    id 468
    label "matczysko"
  ]
  node [
    id 469
    label "ro&#347;lina"
  ]
  node [
    id 470
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 471
    label "gracz"
  ]
  node [
    id 472
    label "zawodnik"
  ]
  node [
    id 473
    label "macierz"
  ]
  node [
    id 474
    label "owad"
  ]
  node [
    id 475
    label "przyczyna"
  ]
  node [
    id 476
    label "macocha"
  ]
  node [
    id 477
    label "dwa_ognie"
  ]
  node [
    id 478
    label "staruszka"
  ]
  node [
    id 479
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 480
    label "rozsadnik"
  ]
  node [
    id 481
    label "zakonnica"
  ]
  node [
    id 482
    label "obiekt"
  ]
  node [
    id 483
    label "samica"
  ]
  node [
    id 484
    label "przodkini"
  ]
  node [
    id 485
    label "rodzice"
  ]
  node [
    id 486
    label "shatter"
  ]
  node [
    id 487
    label "pamper"
  ]
  node [
    id 488
    label "naruszy&#263;"
  ]
  node [
    id 489
    label "wyj&#347;cie"
  ]
  node [
    id 490
    label "wytw&#243;r"
  ]
  node [
    id 491
    label "doj&#347;cie"
  ]
  node [
    id 492
    label "skrzyd&#322;o"
  ]
  node [
    id 493
    label "zamek"
  ]
  node [
    id 494
    label "futryna"
  ]
  node [
    id 495
    label "antaba"
  ]
  node [
    id 496
    label "szafa"
  ]
  node [
    id 497
    label "szafka"
  ]
  node [
    id 498
    label "wej&#347;cie"
  ]
  node [
    id 499
    label "zawiasy"
  ]
  node [
    id 500
    label "samozamykacz"
  ]
  node [
    id 501
    label "ko&#322;atka"
  ]
  node [
    id 502
    label "klamka"
  ]
  node [
    id 503
    label "wrzeci&#261;dz"
  ]
  node [
    id 504
    label "czu&#263;"
  ]
  node [
    id 505
    label "desire"
  ]
  node [
    id 506
    label "kcie&#263;"
  ]
  node [
    id 507
    label "poinformowa&#263;"
  ]
  node [
    id 508
    label "inform"
  ]
  node [
    id 509
    label "get"
  ]
  node [
    id 510
    label "opu&#347;ci&#263;"
  ]
  node [
    id 511
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 512
    label "zej&#347;&#263;"
  ]
  node [
    id 513
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 514
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 515
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 516
    label "sko&#324;czy&#263;"
  ]
  node [
    id 517
    label "ograniczenie"
  ]
  node [
    id 518
    label "ruszy&#263;"
  ]
  node [
    id 519
    label "wypa&#347;&#263;"
  ]
  node [
    id 520
    label "uko&#324;czy&#263;"
  ]
  node [
    id 521
    label "open"
  ]
  node [
    id 522
    label "moderate"
  ]
  node [
    id 523
    label "uzyska&#263;"
  ]
  node [
    id 524
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 525
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 526
    label "mount"
  ]
  node [
    id 527
    label "leave"
  ]
  node [
    id 528
    label "drive"
  ]
  node [
    id 529
    label "zagra&#263;"
  ]
  node [
    id 530
    label "zademonstrowa&#263;"
  ]
  node [
    id 531
    label "wystarczy&#263;"
  ]
  node [
    id 532
    label "perform"
  ]
  node [
    id 533
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 534
    label "drop"
  ]
  node [
    id 535
    label "garderoba"
  ]
  node [
    id 536
    label "wiecha"
  ]
  node [
    id 537
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 538
    label "budynek"
  ]
  node [
    id 539
    label "fratria"
  ]
  node [
    id 540
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 541
    label "poj&#281;cie"
  ]
  node [
    id 542
    label "rodzina"
  ]
  node [
    id 543
    label "substancja_mieszkaniowa"
  ]
  node [
    id 544
    label "instytucja"
  ]
  node [
    id 545
    label "dom_rodzinny"
  ]
  node [
    id 546
    label "stead"
  ]
  node [
    id 547
    label "siedziba"
  ]
  node [
    id 548
    label "zu&#380;y&#263;"
  ]
  node [
    id 549
    label "consume"
  ]
  node [
    id 550
    label "zaszkodzi&#263;"
  ]
  node [
    id 551
    label "os&#322;abi&#263;"
  ]
  node [
    id 552
    label "spoil"
  ]
  node [
    id 553
    label "zdrowie"
  ]
  node [
    id 554
    label "kondycja_fizyczna"
  ]
  node [
    id 555
    label "nadbud&#243;wka"
  ]
  node [
    id 556
    label "sklep"
  ]
  node [
    id 557
    label "prasa"
  ]
  node [
    id 558
    label "punkt"
  ]
  node [
    id 559
    label "budka"
  ]
  node [
    id 560
    label "okr&#281;t_podwodny"
  ]
  node [
    id 561
    label "zas&#322;ona"
  ]
  node [
    id 562
    label "dekoracja_okna"
  ]
  node [
    id 563
    label "pull"
  ]
  node [
    id 564
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 565
    label "nast&#261;pi&#263;"
  ]
  node [
    id 566
    label "strike"
  ]
  node [
    id 567
    label "pozbija&#263;"
  ]
  node [
    id 568
    label "zabi&#263;"
  ]
  node [
    id 569
    label "thrash"
  ]
  node [
    id 570
    label "sypn&#261;&#263;_si&#281;"
  ]
  node [
    id 571
    label "wywy&#380;szy&#263;"
  ]
  node [
    id 572
    label "precipitate"
  ]
  node [
    id 573
    label "wystuka&#263;"
  ]
  node [
    id 574
    label "usun&#261;&#263;"
  ]
  node [
    id 575
    label "wyperswadowa&#263;"
  ]
  node [
    id 576
    label "przegoni&#263;"
  ]
  node [
    id 577
    label "sprawi&#263;"
  ]
  node [
    id 578
    label "wyt&#322;oczy&#263;"
  ]
  node [
    id 579
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 580
    label "wskaza&#263;"
  ]
  node [
    id 581
    label "wytworzy&#263;"
  ]
  node [
    id 582
    label "po&#322;ama&#263;"
  ]
  node [
    id 583
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 584
    label "crush"
  ]
  node [
    id 585
    label "kana&#322;"
  ]
  node [
    id 586
    label "wyrobisko"
  ]
  node [
    id 587
    label "u"
  ]
  node [
    id 588
    label "Marcin"
  ]
  node [
    id 589
    label "wyspa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 278
  ]
  edge [
    source 15
    target 279
  ]
  edge [
    source 15
    target 71
  ]
  edge [
    source 15
    target 280
  ]
  edge [
    source 15
    target 281
  ]
  edge [
    source 15
    target 282
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 283
  ]
  edge [
    source 16
    target 284
  ]
  edge [
    source 16
    target 285
  ]
  edge [
    source 16
    target 286
  ]
  edge [
    source 16
    target 287
  ]
  edge [
    source 16
    target 288
  ]
  edge [
    source 16
    target 289
  ]
  edge [
    source 16
    target 290
  ]
  edge [
    source 16
    target 291
  ]
  edge [
    source 16
    target 292
  ]
  edge [
    source 16
    target 293
  ]
  edge [
    source 16
    target 294
  ]
  edge [
    source 16
    target 295
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 296
  ]
  edge [
    source 17
    target 297
  ]
  edge [
    source 17
    target 298
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 59
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 299
  ]
  edge [
    source 19
    target 300
  ]
  edge [
    source 19
    target 301
  ]
  edge [
    source 19
    target 302
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 303
  ]
  edge [
    source 21
    target 97
  ]
  edge [
    source 21
    target 304
  ]
  edge [
    source 21
    target 305
  ]
  edge [
    source 21
    target 39
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 306
  ]
  edge [
    source 22
    target 307
  ]
  edge [
    source 22
    target 308
  ]
  edge [
    source 22
    target 309
  ]
  edge [
    source 22
    target 310
  ]
  edge [
    source 22
    target 85
  ]
  edge [
    source 22
    target 311
  ]
  edge [
    source 22
    target 312
  ]
  edge [
    source 22
    target 313
  ]
  edge [
    source 22
    target 314
  ]
  edge [
    source 22
    target 315
  ]
  edge [
    source 22
    target 316
  ]
  edge [
    source 22
    target 317
  ]
  edge [
    source 22
    target 318
  ]
  edge [
    source 22
    target 319
  ]
  edge [
    source 22
    target 320
  ]
  edge [
    source 22
    target 321
  ]
  edge [
    source 22
    target 322
  ]
  edge [
    source 22
    target 323
  ]
  edge [
    source 22
    target 324
  ]
  edge [
    source 22
    target 325
  ]
  edge [
    source 22
    target 326
  ]
  edge [
    source 22
    target 327
  ]
  edge [
    source 22
    target 328
  ]
  edge [
    source 22
    target 97
  ]
  edge [
    source 22
    target 329
  ]
  edge [
    source 22
    target 330
  ]
  edge [
    source 22
    target 331
  ]
  edge [
    source 22
    target 332
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 333
  ]
  edge [
    source 23
    target 334
  ]
  edge [
    source 23
    target 335
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 39
  ]
  edge [
    source 24
    target 336
  ]
  edge [
    source 24
    target 337
  ]
  edge [
    source 24
    target 338
  ]
  edge [
    source 24
    target 339
  ]
  edge [
    source 24
    target 340
  ]
  edge [
    source 24
    target 341
  ]
  edge [
    source 24
    target 342
  ]
  edge [
    source 24
    target 343
  ]
  edge [
    source 24
    target 344
  ]
  edge [
    source 24
    target 345
  ]
  edge [
    source 24
    target 346
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 347
  ]
  edge [
    source 25
    target 348
  ]
  edge [
    source 25
    target 349
  ]
  edge [
    source 25
    target 350
  ]
  edge [
    source 25
    target 351
  ]
  edge [
    source 25
    target 352
  ]
  edge [
    source 25
    target 353
  ]
  edge [
    source 25
    target 354
  ]
  edge [
    source 25
    target 355
  ]
  edge [
    source 25
    target 356
  ]
  edge [
    source 25
    target 357
  ]
  edge [
    source 25
    target 358
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 359
  ]
  edge [
    source 26
    target 360
  ]
  edge [
    source 26
    target 361
  ]
  edge [
    source 26
    target 362
  ]
  edge [
    source 26
    target 363
  ]
  edge [
    source 26
    target 364
  ]
  edge [
    source 26
    target 365
  ]
  edge [
    source 26
    target 366
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 284
  ]
  edge [
    source 27
    target 367
  ]
  edge [
    source 27
    target 368
  ]
  edge [
    source 27
    target 369
  ]
  edge [
    source 27
    target 370
  ]
  edge [
    source 27
    target 371
  ]
  edge [
    source 27
    target 372
  ]
  edge [
    source 27
    target 373
  ]
  edge [
    source 27
    target 374
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 375
  ]
  edge [
    source 28
    target 94
  ]
  edge [
    source 28
    target 376
  ]
  edge [
    source 28
    target 377
  ]
  edge [
    source 28
    target 378
  ]
  edge [
    source 28
    target 379
  ]
  edge [
    source 28
    target 380
  ]
  edge [
    source 28
    target 381
  ]
  edge [
    source 28
    target 382
  ]
  edge [
    source 28
    target 383
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 45
  ]
  edge [
    source 29
    target 46
  ]
  edge [
    source 29
    target 384
  ]
  edge [
    source 29
    target 94
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 385
  ]
  edge [
    source 30
    target 386
  ]
  edge [
    source 30
    target 387
  ]
  edge [
    source 30
    target 388
  ]
  edge [
    source 30
    target 389
  ]
  edge [
    source 30
    target 390
  ]
  edge [
    source 30
    target 328
  ]
  edge [
    source 30
    target 391
  ]
  edge [
    source 30
    target 366
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 392
  ]
  edge [
    source 31
    target 393
  ]
  edge [
    source 32
    target 394
  ]
  edge [
    source 32
    target 395
  ]
  edge [
    source 32
    target 396
  ]
  edge [
    source 32
    target 397
  ]
  edge [
    source 32
    target 398
  ]
  edge [
    source 32
    target 399
  ]
  edge [
    source 32
    target 400
  ]
  edge [
    source 32
    target 401
  ]
  edge [
    source 32
    target 402
  ]
  edge [
    source 32
    target 403
  ]
  edge [
    source 32
    target 404
  ]
  edge [
    source 32
    target 405
  ]
  edge [
    source 32
    target 406
  ]
  edge [
    source 32
    target 407
  ]
  edge [
    source 32
    target 408
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 75
  ]
  edge [
    source 33
    target 409
  ]
  edge [
    source 33
    target 410
  ]
  edge [
    source 33
    target 411
  ]
  edge [
    source 33
    target 412
  ]
  edge [
    source 33
    target 66
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 413
  ]
  edge [
    source 34
    target 414
  ]
  edge [
    source 34
    target 415
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 416
  ]
  edge [
    source 35
    target 417
  ]
  edge [
    source 35
    target 418
  ]
  edge [
    source 35
    target 419
  ]
  edge [
    source 35
    target 420
  ]
  edge [
    source 35
    target 421
  ]
  edge [
    source 35
    target 422
  ]
  edge [
    source 35
    target 423
  ]
  edge [
    source 35
    target 424
  ]
  edge [
    source 35
    target 425
  ]
  edge [
    source 35
    target 426
  ]
  edge [
    source 35
    target 427
  ]
  edge [
    source 35
    target 428
  ]
  edge [
    source 35
    target 429
  ]
  edge [
    source 35
    target 430
  ]
  edge [
    source 35
    target 59
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 431
  ]
  edge [
    source 36
    target 432
  ]
  edge [
    source 37
    target 433
  ]
  edge [
    source 37
    target 434
  ]
  edge [
    source 37
    target 435
  ]
  edge [
    source 37
    target 436
  ]
  edge [
    source 38
    target 284
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 51
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 587
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 50
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 284
  ]
  edge [
    source 44
    target 437
  ]
  edge [
    source 45
    target 438
  ]
  edge [
    source 45
    target 439
  ]
  edge [
    source 45
    target 440
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 441
  ]
  edge [
    source 46
    target 442
  ]
  edge [
    source 46
    target 443
  ]
  edge [
    source 46
    target 444
  ]
  edge [
    source 46
    target 445
  ]
  edge [
    source 46
    target 446
  ]
  edge [
    source 46
    target 447
  ]
  edge [
    source 46
    target 448
  ]
  edge [
    source 46
    target 449
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 450
  ]
  edge [
    source 48
    target 451
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 452
  ]
  edge [
    source 49
    target 453
  ]
  edge [
    source 49
    target 454
  ]
  edge [
    source 49
    target 455
  ]
  edge [
    source 50
    target 284
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 456
  ]
  edge [
    source 53
    target 457
  ]
  edge [
    source 53
    target 458
  ]
  edge [
    source 53
    target 459
  ]
  edge [
    source 53
    target 460
  ]
  edge [
    source 53
    target 461
  ]
  edge [
    source 53
    target 462
  ]
  edge [
    source 53
    target 463
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 464
  ]
  edge [
    source 54
    target 465
  ]
  edge [
    source 54
    target 466
  ]
  edge [
    source 54
    target 467
  ]
  edge [
    source 54
    target 468
  ]
  edge [
    source 54
    target 469
  ]
  edge [
    source 54
    target 470
  ]
  edge [
    source 54
    target 471
  ]
  edge [
    source 54
    target 472
  ]
  edge [
    source 54
    target 473
  ]
  edge [
    source 54
    target 474
  ]
  edge [
    source 54
    target 475
  ]
  edge [
    source 54
    target 476
  ]
  edge [
    source 54
    target 477
  ]
  edge [
    source 54
    target 478
  ]
  edge [
    source 54
    target 479
  ]
  edge [
    source 54
    target 480
  ]
  edge [
    source 54
    target 481
  ]
  edge [
    source 54
    target 482
  ]
  edge [
    source 54
    target 483
  ]
  edge [
    source 54
    target 484
  ]
  edge [
    source 54
    target 485
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 486
  ]
  edge [
    source 55
    target 487
  ]
  edge [
    source 55
    target 78
  ]
  edge [
    source 55
    target 328
  ]
  edge [
    source 55
    target 488
  ]
  edge [
    source 55
    target 66
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 489
  ]
  edge [
    source 56
    target 490
  ]
  edge [
    source 56
    target 491
  ]
  edge [
    source 56
    target 492
  ]
  edge [
    source 56
    target 493
  ]
  edge [
    source 56
    target 494
  ]
  edge [
    source 56
    target 495
  ]
  edge [
    source 56
    target 496
  ]
  edge [
    source 56
    target 497
  ]
  edge [
    source 56
    target 498
  ]
  edge [
    source 56
    target 499
  ]
  edge [
    source 56
    target 500
  ]
  edge [
    source 56
    target 501
  ]
  edge [
    source 56
    target 502
  ]
  edge [
    source 56
    target 503
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 504
  ]
  edge [
    source 59
    target 505
  ]
  edge [
    source 59
    target 506
  ]
  edge [
    source 60
    target 507
  ]
  edge [
    source 60
    target 508
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 509
  ]
  edge [
    source 61
    target 510
  ]
  edge [
    source 61
    target 511
  ]
  edge [
    source 61
    target 512
  ]
  edge [
    source 61
    target 513
  ]
  edge [
    source 61
    target 514
  ]
  edge [
    source 61
    target 515
  ]
  edge [
    source 61
    target 516
  ]
  edge [
    source 61
    target 517
  ]
  edge [
    source 61
    target 518
  ]
  edge [
    source 61
    target 519
  ]
  edge [
    source 61
    target 520
  ]
  edge [
    source 61
    target 521
  ]
  edge [
    source 61
    target 522
  ]
  edge [
    source 61
    target 523
  ]
  edge [
    source 61
    target 524
  ]
  edge [
    source 61
    target 525
  ]
  edge [
    source 61
    target 526
  ]
  edge [
    source 61
    target 527
  ]
  edge [
    source 61
    target 528
  ]
  edge [
    source 61
    target 529
  ]
  edge [
    source 61
    target 530
  ]
  edge [
    source 61
    target 531
  ]
  edge [
    source 61
    target 532
  ]
  edge [
    source 61
    target 533
  ]
  edge [
    source 61
    target 534
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 535
  ]
  edge [
    source 62
    target 536
  ]
  edge [
    source 62
    target 537
  ]
  edge [
    source 62
    target 71
  ]
  edge [
    source 62
    target 538
  ]
  edge [
    source 62
    target 539
  ]
  edge [
    source 62
    target 540
  ]
  edge [
    source 62
    target 541
  ]
  edge [
    source 62
    target 542
  ]
  edge [
    source 62
    target 543
  ]
  edge [
    source 62
    target 544
  ]
  edge [
    source 62
    target 545
  ]
  edge [
    source 62
    target 546
  ]
  edge [
    source 62
    target 547
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 548
  ]
  edge [
    source 63
    target 549
  ]
  edge [
    source 63
    target 487
  ]
  edge [
    source 63
    target 78
  ]
  edge [
    source 63
    target 550
  ]
  edge [
    source 63
    target 551
  ]
  edge [
    source 63
    target 552
  ]
  edge [
    source 63
    target 553
  ]
  edge [
    source 63
    target 554
  ]
  edge [
    source 63
    target 408
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 555
  ]
  edge [
    source 64
    target 556
  ]
  edge [
    source 64
    target 557
  ]
  edge [
    source 64
    target 558
  ]
  edge [
    source 64
    target 559
  ]
  edge [
    source 64
    target 560
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 561
  ]
  edge [
    source 65
    target 562
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 563
  ]
  edge [
    source 66
    target 564
  ]
  edge [
    source 66
    target 565
  ]
  edge [
    source 66
    target 566
  ]
  edge [
    source 66
    target 567
  ]
  edge [
    source 66
    target 568
  ]
  edge [
    source 66
    target 569
  ]
  edge [
    source 66
    target 570
  ]
  edge [
    source 66
    target 571
  ]
  edge [
    source 66
    target 394
  ]
  edge [
    source 66
    target 396
  ]
  edge [
    source 66
    target 572
  ]
  edge [
    source 66
    target 397
  ]
  edge [
    source 66
    target 524
  ]
  edge [
    source 66
    target 573
  ]
  edge [
    source 66
    target 574
  ]
  edge [
    source 66
    target 575
  ]
  edge [
    source 66
    target 529
  ]
  edge [
    source 66
    target 328
  ]
  edge [
    source 66
    target 405
  ]
  edge [
    source 66
    target 576
  ]
  edge [
    source 66
    target 577
  ]
  edge [
    source 66
    target 578
  ]
  edge [
    source 66
    target 78
  ]
  edge [
    source 66
    target 579
  ]
  edge [
    source 66
    target 580
  ]
  edge [
    source 66
    target 581
  ]
  edge [
    source 66
    target 400
  ]
  edge [
    source 66
    target 582
  ]
  edge [
    source 66
    target 583
  ]
  edge [
    source 66
    target 584
  ]
  edge [
    source 67
    target 585
  ]
  edge [
    source 67
    target 586
  ]
  edge [
    source 588
    target 589
  ]
]
