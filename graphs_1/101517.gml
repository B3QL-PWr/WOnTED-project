graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.0579710144927534
  density 0.009990150555790066
  graphCliqueNumber 3
  node [
    id 0
    label "niemniej"
    origin "text"
  ]
  node [
    id 1
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "marianna"
    origin "text"
  ]
  node [
    id 3
    label "przez"
    origin "text"
  ]
  node [
    id 4
    label "reszta"
    origin "text"
  ]
  node [
    id 5
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 6
    label "zadumany"
    origin "text"
  ]
  node [
    id 7
    label "nazajutrz"
    origin "text"
  ]
  node [
    id 8
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 9
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 10
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 11
    label "niesw&#243;j"
    origin "text"
  ]
  node [
    id 12
    label "m&#322;ody"
    origin "text"
  ]
  node [
    id 13
    label "pani"
    origin "text"
  ]
  node [
    id 14
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 15
    label "&#380;al"
    origin "text"
  ]
  node [
    id 16
    label "mimowolny"
    origin "text"
  ]
  node [
    id 17
    label "porzuci&#263;"
    origin "text"
  ]
  node [
    id 18
    label "okazja"
    origin "text"
  ]
  node [
    id 19
    label "szcz&#281;&#347;cie"
    origin "text"
  ]
  node [
    id 20
    label "ale"
    origin "text"
  ]
  node [
    id 21
    label "by&#263;"
    origin "text"
  ]
  node [
    id 22
    label "preludium"
    origin "text"
  ]
  node [
    id 23
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 24
    label "febra"
    origin "text"
  ]
  node [
    id 25
    label "proceed"
  ]
  node [
    id 26
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 27
    label "bangla&#263;"
  ]
  node [
    id 28
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 29
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 30
    label "run"
  ]
  node [
    id 31
    label "tryb"
  ]
  node [
    id 32
    label "p&#322;ywa&#263;"
  ]
  node [
    id 33
    label "continue"
  ]
  node [
    id 34
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 35
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 36
    label "przebiega&#263;"
  ]
  node [
    id 37
    label "mie&#263;_miejsce"
  ]
  node [
    id 38
    label "wk&#322;ada&#263;"
  ]
  node [
    id 39
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 40
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 41
    label "para"
  ]
  node [
    id 42
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 43
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 44
    label "krok"
  ]
  node [
    id 45
    label "str&#243;j"
  ]
  node [
    id 46
    label "bywa&#263;"
  ]
  node [
    id 47
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 48
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 49
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 50
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 51
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 52
    label "dziama&#263;"
  ]
  node [
    id 53
    label "stara&#263;_si&#281;"
  ]
  node [
    id 54
    label "carry"
  ]
  node [
    id 55
    label "remainder"
  ]
  node [
    id 56
    label "wydanie"
  ]
  node [
    id 57
    label "wyda&#263;"
  ]
  node [
    id 58
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 59
    label "wydawa&#263;"
  ]
  node [
    id 60
    label "pozosta&#322;y"
  ]
  node [
    id 61
    label "kwota"
  ]
  node [
    id 62
    label "s&#322;o&#324;ce"
  ]
  node [
    id 63
    label "czynienie_si&#281;"
  ]
  node [
    id 64
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 65
    label "czas"
  ]
  node [
    id 66
    label "long_time"
  ]
  node [
    id 67
    label "przedpo&#322;udnie"
  ]
  node [
    id 68
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 69
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 70
    label "tydzie&#324;"
  ]
  node [
    id 71
    label "godzina"
  ]
  node [
    id 72
    label "t&#322;usty_czwartek"
  ]
  node [
    id 73
    label "wsta&#263;"
  ]
  node [
    id 74
    label "day"
  ]
  node [
    id 75
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 76
    label "przedwiecz&#243;r"
  ]
  node [
    id 77
    label "Sylwester"
  ]
  node [
    id 78
    label "po&#322;udnie"
  ]
  node [
    id 79
    label "wzej&#347;cie"
  ]
  node [
    id 80
    label "podwiecz&#243;r"
  ]
  node [
    id 81
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 82
    label "rano"
  ]
  node [
    id 83
    label "termin"
  ]
  node [
    id 84
    label "ranek"
  ]
  node [
    id 85
    label "doba"
  ]
  node [
    id 86
    label "wiecz&#243;r"
  ]
  node [
    id 87
    label "walentynki"
  ]
  node [
    id 88
    label "popo&#322;udnie"
  ]
  node [
    id 89
    label "noc"
  ]
  node [
    id 90
    label "wstanie"
  ]
  node [
    id 91
    label "nieobecnie"
  ]
  node [
    id 92
    label "nieobecny"
  ]
  node [
    id 93
    label "blisko"
  ]
  node [
    id 94
    label "jutrzejszy"
  ]
  node [
    id 95
    label "partnerka"
  ]
  node [
    id 96
    label "jako&#347;"
  ]
  node [
    id 97
    label "charakterystyczny"
  ]
  node [
    id 98
    label "ciekawy"
  ]
  node [
    id 99
    label "jako_tako"
  ]
  node [
    id 100
    label "dziwny"
  ]
  node [
    id 101
    label "niez&#322;y"
  ]
  node [
    id 102
    label "przyzwoity"
  ]
  node [
    id 103
    label "nieswojo"
  ]
  node [
    id 104
    label "s&#322;aby"
  ]
  node [
    id 105
    label "cz&#322;owiek"
  ]
  node [
    id 106
    label "nie&#380;onaty"
  ]
  node [
    id 107
    label "wczesny"
  ]
  node [
    id 108
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 109
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 110
    label "nowo&#380;eniec"
  ]
  node [
    id 111
    label "m&#261;&#380;"
  ]
  node [
    id 112
    label "m&#322;odo"
  ]
  node [
    id 113
    label "nowy"
  ]
  node [
    id 114
    label "przekwitanie"
  ]
  node [
    id 115
    label "zwrot"
  ]
  node [
    id 116
    label "uleganie"
  ]
  node [
    id 117
    label "ulega&#263;"
  ]
  node [
    id 118
    label "partner"
  ]
  node [
    id 119
    label "doros&#322;y"
  ]
  node [
    id 120
    label "przyw&#243;dczyni"
  ]
  node [
    id 121
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 122
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 123
    label "ulec"
  ]
  node [
    id 124
    label "kobita"
  ]
  node [
    id 125
    label "&#322;ono"
  ]
  node [
    id 126
    label "kobieta"
  ]
  node [
    id 127
    label "w&#322;a&#347;cicielka"
  ]
  node [
    id 128
    label "m&#281;&#380;yna"
  ]
  node [
    id 129
    label "babka"
  ]
  node [
    id 130
    label "samica"
  ]
  node [
    id 131
    label "pa&#324;stwo"
  ]
  node [
    id 132
    label "ulegni&#281;cie"
  ]
  node [
    id 133
    label "menopauza"
  ]
  node [
    id 134
    label "take_care"
  ]
  node [
    id 135
    label "troska&#263;_si&#281;"
  ]
  node [
    id 136
    label "zamierza&#263;"
  ]
  node [
    id 137
    label "os&#261;dza&#263;"
  ]
  node [
    id 138
    label "robi&#263;"
  ]
  node [
    id 139
    label "argue"
  ]
  node [
    id 140
    label "rozpatrywa&#263;"
  ]
  node [
    id 141
    label "deliver"
  ]
  node [
    id 142
    label "niezadowolenie"
  ]
  node [
    id 143
    label "wstyd"
  ]
  node [
    id 144
    label "gniewa&#263;_si&#281;"
  ]
  node [
    id 145
    label "gniewanie_si&#281;"
  ]
  node [
    id 146
    label "smutek"
  ]
  node [
    id 147
    label "czu&#263;"
  ]
  node [
    id 148
    label "emocja"
  ]
  node [
    id 149
    label "commiseration"
  ]
  node [
    id 150
    label "pang"
  ]
  node [
    id 151
    label "krytyka"
  ]
  node [
    id 152
    label "pogniewa&#263;_si&#281;"
  ]
  node [
    id 153
    label "uraza"
  ]
  node [
    id 154
    label "criticism"
  ]
  node [
    id 155
    label "sorrow"
  ]
  node [
    id 156
    label "pogniewanie_si&#281;"
  ]
  node [
    id 157
    label "sytuacja"
  ]
  node [
    id 158
    label "bezwiednie"
  ]
  node [
    id 159
    label "przypadkowy"
  ]
  node [
    id 160
    label "mimowolnie"
  ]
  node [
    id 161
    label "samoistny"
  ]
  node [
    id 162
    label "nie&#347;wiadomy"
  ]
  node [
    id 163
    label "poniewolny"
  ]
  node [
    id 164
    label "przesta&#263;"
  ]
  node [
    id 165
    label "unwrap"
  ]
  node [
    id 166
    label "zostawi&#263;"
  ]
  node [
    id 167
    label "drop"
  ]
  node [
    id 168
    label "atrakcyjny"
  ]
  node [
    id 169
    label "oferta"
  ]
  node [
    id 170
    label "adeptness"
  ]
  node [
    id 171
    label "okazka"
  ]
  node [
    id 172
    label "wydarzenie"
  ]
  node [
    id 173
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 174
    label "podw&#243;zka"
  ]
  node [
    id 175
    label "autostop"
  ]
  node [
    id 176
    label "wra&#380;enie"
  ]
  node [
    id 177
    label "przeznaczenie"
  ]
  node [
    id 178
    label "dobrodziejstwo"
  ]
  node [
    id 179
    label "dobro"
  ]
  node [
    id 180
    label "przypadek"
  ]
  node [
    id 181
    label "piwo"
  ]
  node [
    id 182
    label "si&#281;ga&#263;"
  ]
  node [
    id 183
    label "trwa&#263;"
  ]
  node [
    id 184
    label "obecno&#347;&#263;"
  ]
  node [
    id 185
    label "stan"
  ]
  node [
    id 186
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 187
    label "stand"
  ]
  node [
    id 188
    label "uczestniczy&#263;"
  ]
  node [
    id 189
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 190
    label "equal"
  ]
  node [
    id 191
    label "przedsmak"
  ]
  node [
    id 192
    label "wst&#281;p"
  ]
  node [
    id 193
    label "utw&#243;r"
  ]
  node [
    id 194
    label "jasny"
  ]
  node [
    id 195
    label "typ_mongoloidalny"
  ]
  node [
    id 196
    label "kolorowy"
  ]
  node [
    id 197
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 198
    label "ciep&#322;y"
  ]
  node [
    id 199
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 200
    label "gor&#261;czka"
  ]
  node [
    id 201
    label "opryszczkowe_zapalenie_opon_m&#243;zgowych_i_m&#243;zgu"
  ]
  node [
    id 202
    label "dreszcz"
  ]
  node [
    id 203
    label "wirus_opryszczki_pospolitej"
  ]
  node [
    id 204
    label "malaria"
  ]
  node [
    id 205
    label "p&#281;cherz"
  ]
  node [
    id 206
    label "choroba_wirusowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 37
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 24
    target 201
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
]
