graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.0970873786407767
  density 0.020559680182752713
  graphCliqueNumber 5
  node [
    id 0
    label "marek"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "osoba"
    origin "text"
  ]
  node [
    id 3
    label "niedowidzie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "porusza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "w&#243;zek"
    origin "text"
  ]
  node [
    id 7
    label "chorowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "rak"
    origin "text"
  ]
  node [
    id 9
    label "selerowate"
  ]
  node [
    id 10
    label "bylina"
  ]
  node [
    id 11
    label "hygrofit"
  ]
  node [
    id 12
    label "si&#281;ga&#263;"
  ]
  node [
    id 13
    label "trwa&#263;"
  ]
  node [
    id 14
    label "obecno&#347;&#263;"
  ]
  node [
    id 15
    label "stan"
  ]
  node [
    id 16
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "stand"
  ]
  node [
    id 18
    label "mie&#263;_miejsce"
  ]
  node [
    id 19
    label "uczestniczy&#263;"
  ]
  node [
    id 20
    label "chodzi&#263;"
  ]
  node [
    id 21
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 22
    label "equal"
  ]
  node [
    id 23
    label "Zgredek"
  ]
  node [
    id 24
    label "kategoria_gramatyczna"
  ]
  node [
    id 25
    label "Casanova"
  ]
  node [
    id 26
    label "Don_Juan"
  ]
  node [
    id 27
    label "Gargantua"
  ]
  node [
    id 28
    label "Faust"
  ]
  node [
    id 29
    label "profanum"
  ]
  node [
    id 30
    label "Chocho&#322;"
  ]
  node [
    id 31
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 32
    label "koniugacja"
  ]
  node [
    id 33
    label "Winnetou"
  ]
  node [
    id 34
    label "Dwukwiat"
  ]
  node [
    id 35
    label "homo_sapiens"
  ]
  node [
    id 36
    label "Edyp"
  ]
  node [
    id 37
    label "Herkules_Poirot"
  ]
  node [
    id 38
    label "ludzko&#347;&#263;"
  ]
  node [
    id 39
    label "mikrokosmos"
  ]
  node [
    id 40
    label "person"
  ]
  node [
    id 41
    label "Sherlock_Holmes"
  ]
  node [
    id 42
    label "portrecista"
  ]
  node [
    id 43
    label "Szwejk"
  ]
  node [
    id 44
    label "Hamlet"
  ]
  node [
    id 45
    label "duch"
  ]
  node [
    id 46
    label "g&#322;owa"
  ]
  node [
    id 47
    label "oddzia&#322;ywanie"
  ]
  node [
    id 48
    label "Quasimodo"
  ]
  node [
    id 49
    label "Dulcynea"
  ]
  node [
    id 50
    label "Don_Kiszot"
  ]
  node [
    id 51
    label "Wallenrod"
  ]
  node [
    id 52
    label "Plastu&#347;"
  ]
  node [
    id 53
    label "Harry_Potter"
  ]
  node [
    id 54
    label "figura"
  ]
  node [
    id 55
    label "parali&#380;owa&#263;"
  ]
  node [
    id 56
    label "istota"
  ]
  node [
    id 57
    label "Werter"
  ]
  node [
    id 58
    label "antropochoria"
  ]
  node [
    id 59
    label "posta&#263;"
  ]
  node [
    id 60
    label "cierpie&#263;"
  ]
  node [
    id 61
    label "podnosi&#263;"
  ]
  node [
    id 62
    label "meet"
  ]
  node [
    id 63
    label "move"
  ]
  node [
    id 64
    label "act"
  ]
  node [
    id 65
    label "wzbudza&#263;"
  ]
  node [
    id 66
    label "porobi&#263;"
  ]
  node [
    id 67
    label "drive"
  ]
  node [
    id 68
    label "robi&#263;"
  ]
  node [
    id 69
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 70
    label "go"
  ]
  node [
    id 71
    label "powodowa&#263;"
  ]
  node [
    id 72
    label "pojazd_niemechaniczny"
  ]
  node [
    id 73
    label "pain"
  ]
  node [
    id 74
    label "garlic"
  ]
  node [
    id 75
    label "pragn&#261;&#263;"
  ]
  node [
    id 76
    label "mozaika"
  ]
  node [
    id 77
    label "cz&#322;owiek"
  ]
  node [
    id 78
    label "przyrz&#261;d"
  ]
  node [
    id 79
    label "guz_z&#322;o&#347;liwy"
  ]
  node [
    id 80
    label "nacieka&#263;"
  ]
  node [
    id 81
    label "naciekanie"
  ]
  node [
    id 82
    label "erwinia"
  ]
  node [
    id 83
    label "rakowate"
  ]
  node [
    id 84
    label "mumia"
  ]
  node [
    id 85
    label "bakteria_brodawkowa"
  ]
  node [
    id 86
    label "dziesi&#281;cion&#243;g"
  ]
  node [
    id 87
    label "pieprzyk"
  ]
  node [
    id 88
    label "czarcia_miot&#322;a"
  ]
  node [
    id 89
    label "naciekn&#261;&#263;"
  ]
  node [
    id 90
    label "fitopatolog"
  ]
  node [
    id 91
    label "choroba_somatyczna"
  ]
  node [
    id 92
    label "schorzenie"
  ]
  node [
    id 93
    label "naciekni&#281;cie"
  ]
  node [
    id 94
    label "stres_oksydacyjny"
  ]
  node [
    id 95
    label "zbi&#243;r"
  ]
  node [
    id 96
    label "wada"
  ]
  node [
    id 97
    label "choroba"
  ]
  node [
    id 98
    label "HP"
  ]
  node [
    id 99
    label "250"
  ]
  node [
    id 100
    label "G1"
  ]
  node [
    id 101
    label "lub"
  ]
  node [
    id 102
    label "G2"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 100
  ]
  edge [
    source 98
    target 101
  ]
  edge [
    source 98
    target 102
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 101
  ]
  edge [
    source 99
    target 102
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 102
  ]
  edge [
    source 101
    target 102
  ]
]
