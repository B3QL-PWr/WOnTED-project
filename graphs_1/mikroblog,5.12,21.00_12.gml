graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.5384615384615385
  density 0.1282051282051282
  graphCliqueNumber 2
  node [
    id 0
    label "mirka"
    origin "text"
  ]
  node [
    id 1
    label "hit"
    origin "text"
  ]
  node [
    id 2
    label "sensacja"
  ]
  node [
    id 3
    label "popularny"
  ]
  node [
    id 4
    label "odkrycie"
  ]
  node [
    id 5
    label "nowina"
  ]
  node [
    id 6
    label "moda"
  ]
  node [
    id 7
    label "utw&#243;r"
  ]
  node [
    id 8
    label "XD"
  ]
  node [
    id 9
    label "go&#347;cia"
  ]
  node [
    id 10
    label "pan"
  ]
  node [
    id 11
    label "XXX"
  ]
  node [
    id 12
    label "telewizor"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 10
    target 11
  ]
]
