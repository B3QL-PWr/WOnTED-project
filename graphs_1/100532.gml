graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.15151515151515152
  graphCliqueNumber 3
  node [
    id 0
    label "marina"
    origin "text"
  ]
  node [
    id 1
    label "premeru"
    origin "text"
  ]
  node [
    id 2
    label "obraz"
  ]
  node [
    id 3
    label "przysta&#324;"
  ]
  node [
    id 4
    label "morze"
  ]
  node [
    id 5
    label "dzie&#322;o"
  ]
  node [
    id 6
    label "Premeru"
  ]
  node [
    id 7
    label "nowy"
  ]
  node [
    id 8
    label "sad"
  ]
  node [
    id 9
    label "europejski"
  ]
  node [
    id 10
    label "festiwal"
  ]
  node [
    id 11
    label "m&#322;odzie&#380;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
]
