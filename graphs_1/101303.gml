graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.4
  density 0.15555555555555556
  graphCliqueNumber 2
  node [
    id 0
    label "prowizorium"
    origin "text"
  ]
  node [
    id 1
    label "bud&#380;etowy"
    origin "text"
  ]
  node [
    id 2
    label "tymczasowo&#347;&#263;"
  ]
  node [
    id 3
    label "etatowy"
  ]
  node [
    id 4
    label "bud&#380;etowo"
  ]
  node [
    id 5
    label "budgetary"
  ]
  node [
    id 6
    label "rada"
  ]
  node [
    id 7
    label "minister"
  ]
  node [
    id 8
    label "konstytucja"
  ]
  node [
    id 9
    label "marcowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
]
