graph [
  maxDegree 47
  minDegree 1
  meanDegree 1.981651376146789
  density 0.01834862385321101
  graphCliqueNumber 2
  node [
    id 0
    label "electronic"
    origin "text"
  ]
  node [
    id 1
    label "frontier"
    origin "text"
  ]
  node [
    id 2
    label "foundation"
    origin "text"
  ]
  node [
    id 3
    label "kr&#243;tko"
    origin "text"
  ]
  node [
    id 4
    label "temat"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "dzieje"
    origin "text"
  ]
  node [
    id 7
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 8
    label "dyrektywa"
    origin "text"
  ]
  node [
    id 9
    label "prawo"
    origin "text"
  ]
  node [
    id 10
    label "autorski"
    origin "text"
  ]
  node [
    id 11
    label "polska"
    origin "text"
  ]
  node [
    id 12
    label "kr&#243;tki"
  ]
  node [
    id 13
    label "fraza"
  ]
  node [
    id 14
    label "forma"
  ]
  node [
    id 15
    label "melodia"
  ]
  node [
    id 16
    label "rzecz"
  ]
  node [
    id 17
    label "zbacza&#263;"
  ]
  node [
    id 18
    label "entity"
  ]
  node [
    id 19
    label "omawia&#263;"
  ]
  node [
    id 20
    label "topik"
  ]
  node [
    id 21
    label "wyraz_pochodny"
  ]
  node [
    id 22
    label "om&#243;wi&#263;"
  ]
  node [
    id 23
    label "omawianie"
  ]
  node [
    id 24
    label "w&#261;tek"
  ]
  node [
    id 25
    label "forum"
  ]
  node [
    id 26
    label "cecha"
  ]
  node [
    id 27
    label "zboczenie"
  ]
  node [
    id 28
    label "zbaczanie"
  ]
  node [
    id 29
    label "tre&#347;&#263;"
  ]
  node [
    id 30
    label "tematyka"
  ]
  node [
    id 31
    label "sprawa"
  ]
  node [
    id 32
    label "istota"
  ]
  node [
    id 33
    label "otoczka"
  ]
  node [
    id 34
    label "zboczy&#263;"
  ]
  node [
    id 35
    label "om&#243;wienie"
  ]
  node [
    id 36
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 37
    label "epoka"
  ]
  node [
    id 38
    label "odwodnienie"
  ]
  node [
    id 39
    label "konstytucja"
  ]
  node [
    id 40
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 41
    label "substancja_chemiczna"
  ]
  node [
    id 42
    label "bratnia_dusza"
  ]
  node [
    id 43
    label "zwi&#261;zanie"
  ]
  node [
    id 44
    label "lokant"
  ]
  node [
    id 45
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 46
    label "zwi&#261;za&#263;"
  ]
  node [
    id 47
    label "organizacja"
  ]
  node [
    id 48
    label "odwadnia&#263;"
  ]
  node [
    id 49
    label "marriage"
  ]
  node [
    id 50
    label "marketing_afiliacyjny"
  ]
  node [
    id 51
    label "bearing"
  ]
  node [
    id 52
    label "wi&#261;zanie"
  ]
  node [
    id 53
    label "odwadnianie"
  ]
  node [
    id 54
    label "koligacja"
  ]
  node [
    id 55
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 56
    label "odwodni&#263;"
  ]
  node [
    id 57
    label "azeotrop"
  ]
  node [
    id 58
    label "powi&#261;zanie"
  ]
  node [
    id 59
    label "polecenie"
  ]
  node [
    id 60
    label "guidance"
  ]
  node [
    id 61
    label "obserwacja"
  ]
  node [
    id 62
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 63
    label "nauka_prawa"
  ]
  node [
    id 64
    label "dominion"
  ]
  node [
    id 65
    label "normatywizm"
  ]
  node [
    id 66
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 67
    label "qualification"
  ]
  node [
    id 68
    label "opis"
  ]
  node [
    id 69
    label "regu&#322;a_Allena"
  ]
  node [
    id 70
    label "normalizacja"
  ]
  node [
    id 71
    label "kazuistyka"
  ]
  node [
    id 72
    label "regu&#322;a_Glogera"
  ]
  node [
    id 73
    label "kultura_duchowa"
  ]
  node [
    id 74
    label "prawo_karne"
  ]
  node [
    id 75
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 76
    label "standard"
  ]
  node [
    id 77
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 78
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 79
    label "struktura"
  ]
  node [
    id 80
    label "szko&#322;a"
  ]
  node [
    id 81
    label "prawo_karne_procesowe"
  ]
  node [
    id 82
    label "prawo_Mendla"
  ]
  node [
    id 83
    label "przepis"
  ]
  node [
    id 84
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 85
    label "criterion"
  ]
  node [
    id 86
    label "kanonistyka"
  ]
  node [
    id 87
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 88
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 89
    label "wykonawczy"
  ]
  node [
    id 90
    label "twierdzenie"
  ]
  node [
    id 91
    label "judykatura"
  ]
  node [
    id 92
    label "legislacyjnie"
  ]
  node [
    id 93
    label "umocowa&#263;"
  ]
  node [
    id 94
    label "podmiot"
  ]
  node [
    id 95
    label "procesualistyka"
  ]
  node [
    id 96
    label "kierunek"
  ]
  node [
    id 97
    label "kryminologia"
  ]
  node [
    id 98
    label "kryminalistyka"
  ]
  node [
    id 99
    label "cywilistyka"
  ]
  node [
    id 100
    label "law"
  ]
  node [
    id 101
    label "zasada_d'Alemberta"
  ]
  node [
    id 102
    label "jurisprudence"
  ]
  node [
    id 103
    label "zasada"
  ]
  node [
    id 104
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 105
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 106
    label "w&#322;asny"
  ]
  node [
    id 107
    label "oryginalny"
  ]
  node [
    id 108
    label "autorsko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
]
