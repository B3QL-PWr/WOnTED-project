graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9393939393939394
  density 0.06060606060606061
  graphCliqueNumber 2
  node [
    id 0
    label "miarka"
    origin "text"
  ]
  node [
    id 1
    label "kartka"
    origin "text"
  ]
  node [
    id 2
    label "o&#322;&#243;wek"
    origin "text"
  ]
  node [
    id 3
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 4
    label "aliexpress"
    origin "text"
  ]
  node [
    id 5
    label "zawarto&#347;&#263;"
  ]
  node [
    id 6
    label "naczynie"
  ]
  node [
    id 7
    label "dominion"
  ]
  node [
    id 8
    label "measure"
  ]
  node [
    id 9
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 10
    label "ticket"
  ]
  node [
    id 11
    label "kartonik"
  ]
  node [
    id 12
    label "arkusz"
  ]
  node [
    id 13
    label "bon"
  ]
  node [
    id 14
    label "wk&#322;ad"
  ]
  node [
    id 15
    label "faul"
  ]
  node [
    id 16
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 17
    label "s&#281;dzia"
  ]
  node [
    id 18
    label "kara"
  ]
  node [
    id 19
    label "strona"
  ]
  node [
    id 20
    label "przybory_do_pisania"
  ]
  node [
    id 21
    label "wypisa&#263;"
  ]
  node [
    id 22
    label "artyku&#322;"
  ]
  node [
    id 23
    label "sztyft"
  ]
  node [
    id 24
    label "wypisanie"
  ]
  node [
    id 25
    label "rysunek"
  ]
  node [
    id 26
    label "grafit"
  ]
  node [
    id 27
    label "czas"
  ]
  node [
    id 28
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 29
    label "rok"
  ]
  node [
    id 30
    label "miech"
  ]
  node [
    id 31
    label "kalendy"
  ]
  node [
    id 32
    label "tydzie&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
]
