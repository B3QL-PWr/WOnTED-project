graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9166666666666667
  density 0.08333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "taka"
    origin "text"
  ]
  node [
    id 1
    label "mama"
    origin "text"
  ]
  node [
    id 2
    label "refleksja"
    origin "text"
  ]
  node [
    id 3
    label "wpis"
    origin "text"
  ]
  node [
    id 4
    label "Bangladesz"
  ]
  node [
    id 5
    label "jednostka_monetarna"
  ]
  node [
    id 6
    label "matczysko"
  ]
  node [
    id 7
    label "macierz"
  ]
  node [
    id 8
    label "przodkini"
  ]
  node [
    id 9
    label "Matka_Boska"
  ]
  node [
    id 10
    label "macocha"
  ]
  node [
    id 11
    label "matka_zast&#281;pcza"
  ]
  node [
    id 12
    label "stara"
  ]
  node [
    id 13
    label "rodzice"
  ]
  node [
    id 14
    label "rodzic"
  ]
  node [
    id 15
    label "my&#347;l"
  ]
  node [
    id 16
    label "meditation"
  ]
  node [
    id 17
    label "namys&#322;"
  ]
  node [
    id 18
    label "czynno&#347;&#263;"
  ]
  node [
    id 19
    label "entrance"
  ]
  node [
    id 20
    label "inscription"
  ]
  node [
    id 21
    label "akt"
  ]
  node [
    id 22
    label "op&#322;ata"
  ]
  node [
    id 23
    label "tekst"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
]
