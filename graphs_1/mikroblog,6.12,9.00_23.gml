graph [
  maxDegree 32
  minDegree 1
  meanDegree 2
  density 0.03571428571428571
  graphCliqueNumber 3
  node [
    id 0
    label "jeszcze"
    origin "text"
  ]
  node [
    id 1
    label "jeden"
    origin "text"
  ]
  node [
    id 2
    label "filmik"
    origin "text"
  ]
  node [
    id 3
    label "i&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "spa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ci&#261;gle"
  ]
  node [
    id 6
    label "kieliszek"
  ]
  node [
    id 7
    label "shot"
  ]
  node [
    id 8
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 9
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 10
    label "jaki&#347;"
  ]
  node [
    id 11
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 12
    label "jednolicie"
  ]
  node [
    id 13
    label "w&#243;dka"
  ]
  node [
    id 14
    label "ten"
  ]
  node [
    id 15
    label "ujednolicenie"
  ]
  node [
    id 16
    label "jednakowy"
  ]
  node [
    id 17
    label "gif"
  ]
  node [
    id 18
    label "wideo"
  ]
  node [
    id 19
    label "impart"
  ]
  node [
    id 20
    label "proceed"
  ]
  node [
    id 21
    label "czas"
  ]
  node [
    id 22
    label "dzia&#263;_si&#281;"
  ]
  node [
    id 23
    label "lecie&#263;"
  ]
  node [
    id 24
    label "blend"
  ]
  node [
    id 25
    label "bangla&#263;"
  ]
  node [
    id 26
    label "trace"
  ]
  node [
    id 27
    label "describe"
  ]
  node [
    id 28
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 29
    label "by&#263;"
  ]
  node [
    id 30
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 31
    label "post&#281;powa&#263;"
  ]
  node [
    id 32
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 33
    label "tryb"
  ]
  node [
    id 34
    label "bie&#380;e&#263;"
  ]
  node [
    id 35
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 36
    label "atakowa&#263;"
  ]
  node [
    id 37
    label "continue"
  ]
  node [
    id 38
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 39
    label "try"
  ]
  node [
    id 40
    label "mie&#263;_miejsce"
  ]
  node [
    id 41
    label "boost"
  ]
  node [
    id 42
    label "draw"
  ]
  node [
    id 43
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 44
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 45
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 46
    label "wyrusza&#263;"
  ]
  node [
    id 47
    label "dziama&#263;"
  ]
  node [
    id 48
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 49
    label "bawi&#263;"
  ]
  node [
    id 50
    label "sp&#281;dza&#263;"
  ]
  node [
    id 51
    label "nyna&#263;"
  ]
  node [
    id 52
    label "nod"
  ]
  node [
    id 53
    label "uprawia&#263;_seks"
  ]
  node [
    id 54
    label "doze"
  ]
  node [
    id 55
    label "op&#322;ywa&#263;"
  ]
  node [
    id 56
    label "p&#322;ywa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
]
