graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9789473684210526
  density 0.021052631578947368
  graphCliqueNumber 2
  node [
    id 0
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ile"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "orientowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pewien"
    origin "text"
  ]
  node [
    id 6
    label "spo&#322;eczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "kobieta"
    origin "text"
  ]
  node [
    id 8
    label "internet"
    origin "text"
  ]
  node [
    id 9
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 10
    label "zarabia&#263;"
    origin "text"
  ]
  node [
    id 11
    label "skr&#243;t"
    origin "text"
  ]
  node [
    id 12
    label "sprzedawa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 14
    label "nagranie"
    origin "text"
  ]
  node [
    id 15
    label "cognizance"
  ]
  node [
    id 16
    label "marshal"
  ]
  node [
    id 17
    label "kierowa&#263;"
  ]
  node [
    id 18
    label "eastern_hemisphere"
  ]
  node [
    id 19
    label "kierunek"
  ]
  node [
    id 20
    label "wyznacza&#263;"
  ]
  node [
    id 21
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 22
    label "pomaga&#263;"
  ]
  node [
    id 23
    label "inform"
  ]
  node [
    id 24
    label "stand"
  ]
  node [
    id 25
    label "upewnienie_si&#281;"
  ]
  node [
    id 26
    label "wierzenie"
  ]
  node [
    id 27
    label "mo&#380;liwy"
  ]
  node [
    id 28
    label "ufanie"
  ]
  node [
    id 29
    label "jaki&#347;"
  ]
  node [
    id 30
    label "spokojny"
  ]
  node [
    id 31
    label "upewnianie_si&#281;"
  ]
  node [
    id 32
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 33
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 34
    label "Fremeni"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "przekwitanie"
  ]
  node [
    id 37
    label "m&#281;&#380;yna"
  ]
  node [
    id 38
    label "babka"
  ]
  node [
    id 39
    label "samica"
  ]
  node [
    id 40
    label "doros&#322;y"
  ]
  node [
    id 41
    label "ulec"
  ]
  node [
    id 42
    label "uleganie"
  ]
  node [
    id 43
    label "partnerka"
  ]
  node [
    id 44
    label "&#380;ona"
  ]
  node [
    id 45
    label "ulega&#263;"
  ]
  node [
    id 46
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 47
    label "pa&#324;stwo"
  ]
  node [
    id 48
    label "ulegni&#281;cie"
  ]
  node [
    id 49
    label "menopauza"
  ]
  node [
    id 50
    label "&#322;ono"
  ]
  node [
    id 51
    label "us&#322;uga_internetowa"
  ]
  node [
    id 52
    label "biznes_elektroniczny"
  ]
  node [
    id 53
    label "punkt_dost&#281;pu"
  ]
  node [
    id 54
    label "hipertekst"
  ]
  node [
    id 55
    label "gra_sieciowa"
  ]
  node [
    id 56
    label "mem"
  ]
  node [
    id 57
    label "e-hazard"
  ]
  node [
    id 58
    label "sie&#263;_komputerowa"
  ]
  node [
    id 59
    label "media"
  ]
  node [
    id 60
    label "podcast"
  ]
  node [
    id 61
    label "netbook"
  ]
  node [
    id 62
    label "provider"
  ]
  node [
    id 63
    label "cyberprzestrze&#324;"
  ]
  node [
    id 64
    label "grooming"
  ]
  node [
    id 65
    label "strona"
  ]
  node [
    id 66
    label "get"
  ]
  node [
    id 67
    label "niszczy&#263;"
  ]
  node [
    id 68
    label "dostawa&#263;"
  ]
  node [
    id 69
    label "pozyskiwa&#263;"
  ]
  node [
    id 70
    label "zas&#322;ugiwa&#263;"
  ]
  node [
    id 71
    label "ugniata&#263;"
  ]
  node [
    id 72
    label "zaczyna&#263;"
  ]
  node [
    id 73
    label "m&#281;czy&#263;"
  ]
  node [
    id 74
    label "wype&#322;nia&#263;"
  ]
  node [
    id 75
    label "miesza&#263;"
  ]
  node [
    id 76
    label "pracowa&#263;"
  ]
  node [
    id 77
    label "net_income"
  ]
  node [
    id 78
    label "retrenchment"
  ]
  node [
    id 79
    label "shortening"
  ]
  node [
    id 80
    label "redukcja"
  ]
  node [
    id 81
    label "contraction"
  ]
  node [
    id 82
    label "przej&#347;cie"
  ]
  node [
    id 83
    label "leksem"
  ]
  node [
    id 84
    label "tekst"
  ]
  node [
    id 85
    label "op&#281;dza&#263;"
  ]
  node [
    id 86
    label "oferowa&#263;"
  ]
  node [
    id 87
    label "oddawa&#263;"
  ]
  node [
    id 88
    label "handlowa&#263;"
  ]
  node [
    id 89
    label "sell"
  ]
  node [
    id 90
    label "wys&#322;uchanie"
  ]
  node [
    id 91
    label "wytw&#243;r"
  ]
  node [
    id 92
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 93
    label "recording"
  ]
  node [
    id 94
    label "utrwalenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
]
