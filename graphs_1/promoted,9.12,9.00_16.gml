graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9166666666666667
  density 0.08333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "akcja"
    origin "text"
  ]
  node [
    id 1
    label "dzieje"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "leeds"
    origin "text"
  ]
  node [
    id 4
    label "zagrywka"
  ]
  node [
    id 5
    label "czyn"
  ]
  node [
    id 6
    label "czynno&#347;&#263;"
  ]
  node [
    id 7
    label "wysoko&#347;&#263;"
  ]
  node [
    id 8
    label "stock"
  ]
  node [
    id 9
    label "gra"
  ]
  node [
    id 10
    label "w&#281;ze&#322;"
  ]
  node [
    id 11
    label "instrument_strunowy"
  ]
  node [
    id 12
    label "dywidenda"
  ]
  node [
    id 13
    label "przebieg"
  ]
  node [
    id 14
    label "udzia&#322;"
  ]
  node [
    id 15
    label "occupation"
  ]
  node [
    id 16
    label "jazda"
  ]
  node [
    id 17
    label "wydarzenie"
  ]
  node [
    id 18
    label "commotion"
  ]
  node [
    id 19
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 20
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 21
    label "operacja"
  ]
  node [
    id 22
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 23
    label "epoka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
]
