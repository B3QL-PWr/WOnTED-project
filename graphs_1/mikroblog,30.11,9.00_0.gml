graph [
  maxDegree 30
  minDegree 1
  meanDegree 2.0253164556962027
  density 0.02596559558584875
  graphCliqueNumber 2
  node [
    id 0
    label "da&#263;"
    origin "text"
  ]
  node [
    id 1
    label "plus"
    origin "text"
  ]
  node [
    id 2
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 3
    label "kocha&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 5
    label "babcia"
    origin "text"
  ]
  node [
    id 6
    label "scrolluj"
    origin "text"
  ]
  node [
    id 7
    label "daleko"
    origin "text"
  ]
  node [
    id 8
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 10
    label "umar&#322;a"
    origin "text"
  ]
  node [
    id 11
    label "dostarczy&#263;"
  ]
  node [
    id 12
    label "obieca&#263;"
  ]
  node [
    id 13
    label "pozwoli&#263;"
  ]
  node [
    id 14
    label "przeznaczy&#263;"
  ]
  node [
    id 15
    label "doda&#263;"
  ]
  node [
    id 16
    label "give"
  ]
  node [
    id 17
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 18
    label "wyrzec_si&#281;"
  ]
  node [
    id 19
    label "supply"
  ]
  node [
    id 20
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 21
    label "zada&#263;"
  ]
  node [
    id 22
    label "odst&#261;pi&#263;"
  ]
  node [
    id 23
    label "feed"
  ]
  node [
    id 24
    label "testify"
  ]
  node [
    id 25
    label "powierzy&#263;"
  ]
  node [
    id 26
    label "convey"
  ]
  node [
    id 27
    label "przekaza&#263;"
  ]
  node [
    id 28
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 29
    label "zap&#322;aci&#263;"
  ]
  node [
    id 30
    label "dress"
  ]
  node [
    id 31
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 32
    label "udost&#281;pni&#263;"
  ]
  node [
    id 33
    label "sztachn&#261;&#263;"
  ]
  node [
    id 34
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 35
    label "zrobi&#263;"
  ]
  node [
    id 36
    label "przywali&#263;"
  ]
  node [
    id 37
    label "rap"
  ]
  node [
    id 38
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 39
    label "picture"
  ]
  node [
    id 40
    label "warto&#347;&#263;"
  ]
  node [
    id 41
    label "rewaluowa&#263;"
  ]
  node [
    id 42
    label "wabik"
  ]
  node [
    id 43
    label "korzy&#347;&#263;"
  ]
  node [
    id 44
    label "dodawanie"
  ]
  node [
    id 45
    label "rewaluowanie"
  ]
  node [
    id 46
    label "stopie&#324;"
  ]
  node [
    id 47
    label "ocena"
  ]
  node [
    id 48
    label "zrewaluowa&#263;"
  ]
  node [
    id 49
    label "liczba"
  ]
  node [
    id 50
    label "znak_matematyczny"
  ]
  node [
    id 51
    label "strona"
  ]
  node [
    id 52
    label "zrewaluowanie"
  ]
  node [
    id 53
    label "like"
  ]
  node [
    id 54
    label "czu&#263;"
  ]
  node [
    id 55
    label "chowa&#263;"
  ]
  node [
    id 56
    label "mi&#322;owa&#263;"
  ]
  node [
    id 57
    label "cz&#322;owiek"
  ]
  node [
    id 58
    label "bli&#378;ni"
  ]
  node [
    id 59
    label "odpowiedni"
  ]
  node [
    id 60
    label "swojak"
  ]
  node [
    id 61
    label "samodzielny"
  ]
  node [
    id 62
    label "babulinka"
  ]
  node [
    id 63
    label "przodkini"
  ]
  node [
    id 64
    label "kobieta"
  ]
  node [
    id 65
    label "baba"
  ]
  node [
    id 66
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 67
    label "dziadkowie"
  ]
  node [
    id 68
    label "dawno"
  ]
  node [
    id 69
    label "nisko"
  ]
  node [
    id 70
    label "nieobecnie"
  ]
  node [
    id 71
    label "daleki"
  ]
  node [
    id 72
    label "het"
  ]
  node [
    id 73
    label "wysoko"
  ]
  node [
    id 74
    label "du&#380;o"
  ]
  node [
    id 75
    label "znacznie"
  ]
  node [
    id 76
    label "g&#322;&#281;boko"
  ]
  node [
    id 77
    label "desire"
  ]
  node [
    id 78
    label "kcie&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 9
    target 10
  ]
]
