graph [
  maxDegree 47
  minDegree 1
  meanDegree 2
  density 0.016666666666666666
  graphCliqueNumber 3
  node [
    id 0
    label "wojsko"
    origin "text"
  ]
  node [
    id 1
    label "polski"
    origin "text"
  ]
  node [
    id 2
    label "podj&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "decyzja"
    origin "text"
  ]
  node [
    id 4
    label "zako&#324;czenie"
    origin "text"
  ]
  node [
    id 5
    label "zakup"
    origin "text"
  ]
  node [
    id 6
    label "he&#322;m"
    origin "text"
  ]
  node [
    id 7
    label "wzorowy"
    origin "text"
  ]
  node [
    id 8
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 9
    label "dryl"
  ]
  node [
    id 10
    label "przedmiot"
  ]
  node [
    id 11
    label "zdemobilizowanie"
  ]
  node [
    id 12
    label "ods&#322;ugiwanie"
  ]
  node [
    id 13
    label "korpus"
  ]
  node [
    id 14
    label "s&#322;u&#380;ba"
  ]
  node [
    id 15
    label "wojo"
  ]
  node [
    id 16
    label "zrejterowanie"
  ]
  node [
    id 17
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 18
    label "zmobilizowa&#263;"
  ]
  node [
    id 19
    label "werbowanie_si&#281;"
  ]
  node [
    id 20
    label "struktura"
  ]
  node [
    id 21
    label "mobilizowa&#263;"
  ]
  node [
    id 22
    label "szko&#322;a"
  ]
  node [
    id 23
    label "oddzia&#322;"
  ]
  node [
    id 24
    label "zdemobilizowa&#263;"
  ]
  node [
    id 25
    label "Armia_Krajowa"
  ]
  node [
    id 26
    label "mobilizowanie"
  ]
  node [
    id 27
    label "pozycja"
  ]
  node [
    id 28
    label "si&#322;a"
  ]
  node [
    id 29
    label "fala"
  ]
  node [
    id 30
    label "obrona"
  ]
  node [
    id 31
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 32
    label "pospolite_ruszenie"
  ]
  node [
    id 33
    label "Armia_Czerwona"
  ]
  node [
    id 34
    label "cofni&#281;cie"
  ]
  node [
    id 35
    label "rejterowanie"
  ]
  node [
    id 36
    label "Eurokorpus"
  ]
  node [
    id 37
    label "tabor"
  ]
  node [
    id 38
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 39
    label "petarda"
  ]
  node [
    id 40
    label "zrejterowa&#263;"
  ]
  node [
    id 41
    label "zmobilizowanie"
  ]
  node [
    id 42
    label "rejterowa&#263;"
  ]
  node [
    id 43
    label "Czerwona_Gwardia"
  ]
  node [
    id 44
    label "Legia_Cudzoziemska"
  ]
  node [
    id 45
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 46
    label "wermacht"
  ]
  node [
    id 47
    label "soldateska"
  ]
  node [
    id 48
    label "oddzia&#322;_karny"
  ]
  node [
    id 49
    label "rezerwa"
  ]
  node [
    id 50
    label "or&#281;&#380;"
  ]
  node [
    id 51
    label "dezerter"
  ]
  node [
    id 52
    label "potencja"
  ]
  node [
    id 53
    label "sztabslekarz"
  ]
  node [
    id 54
    label "lacki"
  ]
  node [
    id 55
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 56
    label "sztajer"
  ]
  node [
    id 57
    label "drabant"
  ]
  node [
    id 58
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 59
    label "polak"
  ]
  node [
    id 60
    label "pierogi_ruskie"
  ]
  node [
    id 61
    label "krakowiak"
  ]
  node [
    id 62
    label "Polish"
  ]
  node [
    id 63
    label "j&#281;zyk"
  ]
  node [
    id 64
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 65
    label "oberek"
  ]
  node [
    id 66
    label "po_polsku"
  ]
  node [
    id 67
    label "mazur"
  ]
  node [
    id 68
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 69
    label "chodzony"
  ]
  node [
    id 70
    label "skoczny"
  ]
  node [
    id 71
    label "ryba_po_grecku"
  ]
  node [
    id 72
    label "goniony"
  ]
  node [
    id 73
    label "polsko"
  ]
  node [
    id 74
    label "zmieni&#263;"
  ]
  node [
    id 75
    label "zacz&#261;&#263;"
  ]
  node [
    id 76
    label "zareagowa&#263;"
  ]
  node [
    id 77
    label "allude"
  ]
  node [
    id 78
    label "raise"
  ]
  node [
    id 79
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 80
    label "draw"
  ]
  node [
    id 81
    label "zrobi&#263;"
  ]
  node [
    id 82
    label "dokument"
  ]
  node [
    id 83
    label "resolution"
  ]
  node [
    id 84
    label "zdecydowanie"
  ]
  node [
    id 85
    label "wytw&#243;r"
  ]
  node [
    id 86
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 87
    label "management"
  ]
  node [
    id 88
    label "dzia&#322;anie"
  ]
  node [
    id 89
    label "zrobienie"
  ]
  node [
    id 90
    label "koniec"
  ]
  node [
    id 91
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 92
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 93
    label "zrezygnowanie"
  ]
  node [
    id 94
    label "closing"
  ]
  node [
    id 95
    label "adjustment"
  ]
  node [
    id 96
    label "ukszta&#322;towanie"
  ]
  node [
    id 97
    label "conclusion"
  ]
  node [
    id 98
    label "termination"
  ]
  node [
    id 99
    label "closure"
  ]
  node [
    id 100
    label "sprzedaj&#261;cy"
  ]
  node [
    id 101
    label "dobro"
  ]
  node [
    id 102
    label "transakcja"
  ]
  node [
    id 103
    label "podbr&#243;dek"
  ]
  node [
    id 104
    label "dach"
  ]
  node [
    id 105
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 106
    label "daszek"
  ]
  node [
    id 107
    label "bro&#324;_ochronna"
  ]
  node [
    id 108
    label "bro&#324;"
  ]
  node [
    id 109
    label "grzebie&#324;"
  ]
  node [
    id 110
    label "wie&#380;a"
  ]
  node [
    id 111
    label "nausznik"
  ]
  node [
    id 112
    label "ubranie_ochronne"
  ]
  node [
    id 113
    label "grzebie&#324;_he&#322;mu"
  ]
  node [
    id 114
    label "zwie&#324;czenie"
  ]
  node [
    id 115
    label "labry"
  ]
  node [
    id 116
    label "&#322;adny"
  ]
  node [
    id 117
    label "przyk&#322;adny"
  ]
  node [
    id 118
    label "dobry"
  ]
  node [
    id 119
    label "wzorowo"
  ]
  node [
    id 120
    label "doskona&#322;y"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
]
