graph [
  maxDegree 28
  minDegree 1
  meanDegree 3.0980392156862746
  density 0.030673655600854204
  graphCliqueNumber 12
  node [
    id 0
    label "przechodzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "g&#322;osowanie"
    origin "text"
  ]
  node [
    id 2
    label "nad"
    origin "text"
  ]
  node [
    id 3
    label "ca&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "projekt"
    origin "text"
  ]
  node [
    id 5
    label "apel"
    origin "text"
  ]
  node [
    id 6
    label "podlega&#263;"
  ]
  node [
    id 7
    label "zmienia&#263;"
  ]
  node [
    id 8
    label "proceed"
  ]
  node [
    id 9
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 10
    label "saturate"
  ]
  node [
    id 11
    label "pass"
  ]
  node [
    id 12
    label "doznawa&#263;"
  ]
  node [
    id 13
    label "test"
  ]
  node [
    id 14
    label "zalicza&#263;"
  ]
  node [
    id 15
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 16
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "conflict"
  ]
  node [
    id 18
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 19
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 20
    label "go"
  ]
  node [
    id 21
    label "continue"
  ]
  node [
    id 22
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 23
    label "mie&#263;_miejsce"
  ]
  node [
    id 24
    label "przestawa&#263;"
  ]
  node [
    id 25
    label "przerabia&#263;"
  ]
  node [
    id 26
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 27
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 28
    label "move"
  ]
  node [
    id 29
    label "przebywa&#263;"
  ]
  node [
    id 30
    label "mija&#263;"
  ]
  node [
    id 31
    label "zaczyna&#263;"
  ]
  node [
    id 32
    label "i&#347;&#263;"
  ]
  node [
    id 33
    label "przeg&#322;osowywanie"
  ]
  node [
    id 34
    label "wybranie"
  ]
  node [
    id 35
    label "decydowanie"
  ]
  node [
    id 36
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 37
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 38
    label "reasumowa&#263;"
  ]
  node [
    id 39
    label "poll"
  ]
  node [
    id 40
    label "wi&#281;kszo&#347;&#263;_bezwzgl&#281;dna"
  ]
  node [
    id 41
    label "vote"
  ]
  node [
    id 42
    label "akcja"
  ]
  node [
    id 43
    label "przeg&#322;osowanie"
  ]
  node [
    id 44
    label "reasumowanie"
  ]
  node [
    id 45
    label "powodowanie"
  ]
  node [
    id 46
    label "wybieranie"
  ]
  node [
    id 47
    label "uk&#322;ad"
  ]
  node [
    id 48
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 49
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 50
    label "integer"
  ]
  node [
    id 51
    label "liczba"
  ]
  node [
    id 52
    label "pe&#322;ny"
  ]
  node [
    id 53
    label "ilo&#347;&#263;"
  ]
  node [
    id 54
    label "zlewanie_si&#281;"
  ]
  node [
    id 55
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 56
    label "dokument"
  ]
  node [
    id 57
    label "device"
  ]
  node [
    id 58
    label "program_u&#380;ytkowy"
  ]
  node [
    id 59
    label "intencja"
  ]
  node [
    id 60
    label "agreement"
  ]
  node [
    id 61
    label "pomys&#322;"
  ]
  node [
    id 62
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 63
    label "plan"
  ]
  node [
    id 64
    label "dokumentacja"
  ]
  node [
    id 65
    label "uderzenie"
  ]
  node [
    id 66
    label "zdyscyplinowanie"
  ]
  node [
    id 67
    label "sygna&#322;"
  ]
  node [
    id 68
    label "bid"
  ]
  node [
    id 69
    label "recoil"
  ]
  node [
    id 70
    label "spotkanie"
  ]
  node [
    id 71
    label "zbi&#243;rka"
  ]
  node [
    id 72
    label "pro&#347;ba"
  ]
  node [
    id 73
    label "pies_my&#347;liwski"
  ]
  node [
    id 74
    label "znak"
  ]
  node [
    id 75
    label "szermierka"
  ]
  node [
    id 76
    label "komisja"
  ]
  node [
    id 77
    label "ii"
  ]
  node [
    id 78
    label "iii"
  ]
  node [
    id 79
    label "sejm"
  ]
  node [
    id 80
    label "dziecko"
  ]
  node [
    id 81
    label "i"
  ]
  node [
    id 82
    label "m&#322;odzie&#380;"
  ]
  node [
    id 83
    label "Charles"
  ]
  node [
    id 84
    label "&#8217;"
  ]
  node [
    id 85
    label "albo"
  ]
  node [
    id 86
    label "Karolina"
  ]
  node [
    id 87
    label "Pankiewicz"
  ]
  node [
    id 88
    label "de"
  ]
  node [
    id 89
    label "Gaulle"
  ]
  node [
    id 90
    label "gimnazjum"
  ]
  node [
    id 91
    label "nr"
  ]
  node [
    id 92
    label "29"
  ]
  node [
    id 93
    label "zeszyt"
  ]
  node [
    id 94
    label "oddzia&#322;"
  ]
  node [
    id 95
    label "dwuj&#281;zyczny"
  ]
  node [
    id 96
    label "on"
  ]
  node [
    id 97
    label "daniel"
  ]
  node [
    id 98
    label "Milewski"
  ]
  node [
    id 99
    label "sesja"
  ]
  node [
    id 100
    label "rzeczpospolita"
  ]
  node [
    id 101
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 78
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 81
  ]
  edge [
    source 79
    target 82
  ]
  edge [
    source 79
    target 99
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 82
  ]
  edge [
    source 80
    target 99
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 99
  ]
  edge [
    source 82
    target 99
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 85
  ]
  edge [
    source 83
    target 88
  ]
  edge [
    source 83
    target 89
  ]
  edge [
    source 83
    target 90
  ]
  edge [
    source 83
    target 91
  ]
  edge [
    source 83
    target 92
  ]
  edge [
    source 83
    target 93
  ]
  edge [
    source 83
    target 94
  ]
  edge [
    source 83
    target 95
  ]
  edge [
    source 83
    target 96
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 88
  ]
  edge [
    source 84
    target 89
  ]
  edge [
    source 84
    target 84
  ]
  edge [
    source 84
    target 90
  ]
  edge [
    source 84
    target 91
  ]
  edge [
    source 84
    target 92
  ]
  edge [
    source 84
    target 93
  ]
  edge [
    source 84
    target 94
  ]
  edge [
    source 84
    target 95
  ]
  edge [
    source 84
    target 96
  ]
  edge [
    source 85
    target 88
  ]
  edge [
    source 85
    target 89
  ]
  edge [
    source 85
    target 85
  ]
  edge [
    source 85
    target 90
  ]
  edge [
    source 85
    target 91
  ]
  edge [
    source 85
    target 92
  ]
  edge [
    source 85
    target 93
  ]
  edge [
    source 85
    target 94
  ]
  edge [
    source 85
    target 95
  ]
  edge [
    source 85
    target 96
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 90
  ]
  edge [
    source 88
    target 91
  ]
  edge [
    source 88
    target 92
  ]
  edge [
    source 88
    target 93
  ]
  edge [
    source 88
    target 94
  ]
  edge [
    source 88
    target 95
  ]
  edge [
    source 88
    target 96
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 89
    target 92
  ]
  edge [
    source 89
    target 93
  ]
  edge [
    source 89
    target 94
  ]
  edge [
    source 89
    target 95
  ]
  edge [
    source 89
    target 96
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 92
  ]
  edge [
    source 90
    target 93
  ]
  edge [
    source 90
    target 94
  ]
  edge [
    source 90
    target 95
  ]
  edge [
    source 90
    target 96
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 93
  ]
  edge [
    source 91
    target 94
  ]
  edge [
    source 91
    target 95
  ]
  edge [
    source 91
    target 96
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 94
  ]
  edge [
    source 92
    target 95
  ]
  edge [
    source 92
    target 96
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 95
  ]
  edge [
    source 93
    target 96
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 96
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 100
    target 101
  ]
]
