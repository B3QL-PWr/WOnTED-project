graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.7894736842105263
  density 0.09941520467836257
  graphCliqueNumber 3
  node [
    id 0
    label "rok"
    origin "text"
  ]
  node [
    id 1
    label "stulecie"
  ]
  node [
    id 2
    label "kalendarz"
  ]
  node [
    id 3
    label "czas"
  ]
  node [
    id 4
    label "pora_roku"
  ]
  node [
    id 5
    label "cykl_astronomiczny"
  ]
  node [
    id 6
    label "p&#243;&#322;rocze"
  ]
  node [
    id 7
    label "grupa"
  ]
  node [
    id 8
    label "kwarta&#322;"
  ]
  node [
    id 9
    label "kurs"
  ]
  node [
    id 10
    label "jubileusz"
  ]
  node [
    id 11
    label "miesi&#261;c"
  ]
  node [
    id 12
    label "lata"
  ]
  node [
    id 13
    label "martwy_sezon"
  ]
  node [
    id 14
    label "Polska"
  ]
  node [
    id 15
    label "partia"
  ]
  node [
    id 16
    label "internet"
  ]
  node [
    id 17
    label "unia"
  ]
  node [
    id 18
    label "europejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
]
