graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.07407407407407407
  graphCliqueNumber 2
  node [
    id 0
    label "kuchnia"
    origin "text"
  ]
  node [
    id 1
    label "wraca&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ola"
    origin "text"
  ]
  node [
    id 3
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 4
    label "zlewozmywak"
  ]
  node [
    id 5
    label "tajniki"
  ]
  node [
    id 6
    label "jedzenie"
  ]
  node [
    id 7
    label "instytucja"
  ]
  node [
    id 8
    label "gotowa&#263;"
  ]
  node [
    id 9
    label "kultura"
  ]
  node [
    id 10
    label "zaplecze"
  ]
  node [
    id 11
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 12
    label "pomieszczenie"
  ]
  node [
    id 13
    label "zaj&#281;cie"
  ]
  node [
    id 14
    label "zostawa&#263;"
  ]
  node [
    id 15
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 16
    label "return"
  ]
  node [
    id 17
    label "godzi&#263;_si&#281;"
  ]
  node [
    id 18
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 19
    label "przybywa&#263;"
  ]
  node [
    id 20
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 21
    label "przychodzi&#263;"
  ]
  node [
    id 22
    label "zaczyna&#263;"
  ]
  node [
    id 23
    label "tax_return"
  ]
  node [
    id 24
    label "nawi&#261;zywa&#263;"
  ]
  node [
    id 25
    label "recur"
  ]
  node [
    id 26
    label "powodowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
]
