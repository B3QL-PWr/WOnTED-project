graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9047619047619047
  density 0.09523809523809523
  graphCliqueNumber 2
  node [
    id 0
    label "druk"
    origin "text"
  ]
  node [
    id 1
    label "termotransferowy"
    origin "text"
  ]
  node [
    id 2
    label "prohibita"
  ]
  node [
    id 3
    label "impression"
  ]
  node [
    id 4
    label "wytw&#243;r"
  ]
  node [
    id 5
    label "tkanina"
  ]
  node [
    id 6
    label "glif"
  ]
  node [
    id 7
    label "formatowanie"
  ]
  node [
    id 8
    label "printing"
  ]
  node [
    id 9
    label "technika"
  ]
  node [
    id 10
    label "formatowa&#263;"
  ]
  node [
    id 11
    label "pismo"
  ]
  node [
    id 12
    label "cymelium"
  ]
  node [
    id 13
    label "zdobnik"
  ]
  node [
    id 14
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 15
    label "tekst"
  ]
  node [
    id 16
    label "publikacja"
  ]
  node [
    id 17
    label "zaproszenie"
  ]
  node [
    id 18
    label "dese&#324;"
  ]
  node [
    id 19
    label "character"
  ]
  node [
    id 20
    label "specjalny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 20
  ]
]
