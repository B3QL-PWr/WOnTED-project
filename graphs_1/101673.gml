graph [
  maxDegree 45
  minDegree 1
  meanDegree 2.4389610389610388
  density 0.0031716008308986204
  graphCliqueNumber 7
  node [
    id 0
    label "szanowny"
    origin "text"
  ]
  node [
    id 1
    label "pan"
    origin "text"
  ]
  node [
    id 2
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 3
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 4
    label "ustawa"
    origin "text"
  ]
  node [
    id 5
    label "bud&#380;etowy"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 8
    label "projekt"
    origin "text"
  ]
  node [
    id 9
    label "dana"
    origin "text"
  ]
  node [
    id 10
    label "rok"
    origin "text"
  ]
  node [
    id 11
    label "tym"
    origin "text"
  ]
  node [
    id 12
    label "decydowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "wydatek"
    origin "text"
  ]
  node [
    id 14
    label "granica"
    origin "text"
  ]
  node [
    id 15
    label "miliard"
    origin "text"
  ]
  node [
    id 16
    label "musie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 18
    label "komisja"
    origin "text"
  ]
  node [
    id 19
    label "finanse"
    origin "text"
  ]
  node [
    id 20
    label "publiczny"
    origin "text"
  ]
  node [
    id 21
    label "praktycznie"
    origin "text"
  ]
  node [
    id 22
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 23
    label "dni"
    origin "text"
  ]
  node [
    id 24
    label "pe&#322;nia"
    origin "text"
  ]
  node [
    id 25
    label "rozumie&#263;"
    origin "text"
  ]
  node [
    id 26
    label "determinacja"
    origin "text"
  ]
  node [
    id 27
    label "koalicja"
    origin "text"
  ]
  node [
    id 28
    label "rz&#261;dz&#261;ca"
    origin "text"
  ]
  node [
    id 29
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 30
    label "zd&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 31
    label "bud&#380;et"
    origin "text"
  ]
  node [
    id 32
    label "uchwali&#263;"
    origin "text"
  ]
  node [
    id 33
    label "z&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 34
    label "podpis"
    origin "text"
  ]
  node [
    id 35
    label "prezydent"
    origin "text"
  ]
  node [
    id 36
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 37
    label "zupe&#322;nie"
    origin "text"
  ]
  node [
    id 38
    label "stanowisko"
    origin "text"
  ]
  node [
    id 39
    label "kancelaria"
    origin "text"
  ]
  node [
    id 40
    label "osobi&#347;cie"
    origin "text"
  ]
  node [
    id 41
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 42
    label "upiera&#263;"
    origin "text"
  ]
  node [
    id 43
    label "si&#281;"
    origin "text"
  ]
  node [
    id 44
    label "zasada"
    origin "text"
  ]
  node [
    id 45
    label "dyskontynuacji"
    origin "text"
  ]
  node [
    id 46
    label "praca"
    origin "text"
  ]
  node [
    id 47
    label "parlament"
    origin "text"
  ]
  node [
    id 48
    label "stosunek"
    origin "text"
  ]
  node [
    id 49
    label "funkcjonowa&#263;"
    origin "text"
  ]
  node [
    id 50
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 51
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 52
    label "fachowiec"
    origin "text"
  ]
  node [
    id 53
    label "jak"
    origin "text"
  ]
  node [
    id 54
    label "gebethner"
    origin "text"
  ]
  node [
    id 55
    label "wawrzyniak"
    origin "text"
  ]
  node [
    id 56
    label "zubik"
    origin "text"
  ]
  node [
    id 57
    label "zajmowa&#263;"
    origin "text"
  ]
  node [
    id 58
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 59
    label "wy&#322;&#261;czenie"
    origin "text"
  ]
  node [
    id 60
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 61
    label "odniesienie"
    origin "text"
  ]
  node [
    id 62
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 63
    label "osoba"
    origin "text"
  ]
  node [
    id 64
    label "problematyka"
    origin "text"
  ]
  node [
    id 65
    label "kosikowski"
    origin "text"
  ]
  node [
    id 66
    label "nassalski"
    origin "text"
  ]
  node [
    id 67
    label "twierdza"
    origin "text"
  ]
  node [
    id 68
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 69
    label "wystarczy&#263;by"
    origin "text"
  ]
  node [
    id 70
    label "dobra"
    origin "text"
  ]
  node [
    id 71
    label "wola"
    origin "text"
  ]
  node [
    id 72
    label "czy"
    origin "text"
  ]
  node [
    id 73
    label "mama"
    origin "text"
  ]
  node [
    id 74
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 75
    label "nad"
    origin "text"
  ]
  node [
    id 76
    label "z&#322;o&#380;one"
    origin "text"
  ]
  node [
    id 77
    label "przez"
    origin "text"
  ]
  node [
    id 78
    label "obecnie"
    origin "text"
  ]
  node [
    id 79
    label "listopad"
    origin "text"
  ]
  node [
    id 80
    label "up&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 81
    label "marco"
    origin "text"
  ]
  node [
    id 82
    label "spokojnie"
    origin "text"
  ]
  node [
    id 83
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 84
    label "ten"
    origin "text"
  ]
  node [
    id 85
    label "taki"
    origin "text"
  ]
  node [
    id 86
    label "pytanie"
    origin "text"
  ]
  node [
    id 87
    label "zada&#263;by&#263;"
    origin "text"
  ]
  node [
    id 88
    label "posiedzenie"
    origin "text"
  ]
  node [
    id 89
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 90
    label "nie"
    origin "text"
  ]
  node [
    id 91
    label "odpowiedzie&#263;"
    origin "text"
  ]
  node [
    id 92
    label "pismo"
    origin "text"
  ]
  node [
    id 93
    label "dzisiejszy"
    origin "text"
  ]
  node [
    id 94
    label "dyshonor"
    origin "text"
  ]
  node [
    id 95
    label "tak"
    origin "text"
  ]
  node [
    id 96
    label "bardzo"
    origin "text"
  ]
  node [
    id 97
    label "problem"
    origin "text"
  ]
  node [
    id 98
    label "rz&#261;dowy"
    origin "text"
  ]
  node [
    id 99
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 100
    label "dok&#322;adnie"
    origin "text"
  ]
  node [
    id 101
    label "przygotowa&#263;"
    origin "text"
  ]
  node [
    id 102
    label "analiza"
    origin "text"
  ]
  node [
    id 103
    label "poprawka"
    origin "text"
  ]
  node [
    id 104
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 105
    label "szybko"
    origin "text"
  ]
  node [
    id 106
    label "szacownie"
  ]
  node [
    id 107
    label "cz&#322;owiek"
  ]
  node [
    id 108
    label "profesor"
  ]
  node [
    id 109
    label "kszta&#322;ciciel"
  ]
  node [
    id 110
    label "jegomo&#347;&#263;"
  ]
  node [
    id 111
    label "zwrot"
  ]
  node [
    id 112
    label "pracodawca"
  ]
  node [
    id 113
    label "rz&#261;dzenie"
  ]
  node [
    id 114
    label "m&#261;&#380;"
  ]
  node [
    id 115
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 116
    label "ch&#322;opina"
  ]
  node [
    id 117
    label "bratek"
  ]
  node [
    id 118
    label "opiekun"
  ]
  node [
    id 119
    label "doros&#322;y"
  ]
  node [
    id 120
    label "preceptor"
  ]
  node [
    id 121
    label "Midas"
  ]
  node [
    id 122
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 123
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 124
    label "murza"
  ]
  node [
    id 125
    label "ojciec"
  ]
  node [
    id 126
    label "androlog"
  ]
  node [
    id 127
    label "pupil"
  ]
  node [
    id 128
    label "efendi"
  ]
  node [
    id 129
    label "nabab"
  ]
  node [
    id 130
    label "w&#322;odarz"
  ]
  node [
    id 131
    label "szkolnik"
  ]
  node [
    id 132
    label "pedagog"
  ]
  node [
    id 133
    label "popularyzator"
  ]
  node [
    id 134
    label "andropauza"
  ]
  node [
    id 135
    label "gra_w_karty"
  ]
  node [
    id 136
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 137
    label "Mieszko_I"
  ]
  node [
    id 138
    label "bogaty"
  ]
  node [
    id 139
    label "samiec"
  ]
  node [
    id 140
    label "przyw&#243;dca"
  ]
  node [
    id 141
    label "pa&#324;stwo"
  ]
  node [
    id 142
    label "belfer"
  ]
  node [
    id 143
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 144
    label "dostojnik"
  ]
  node [
    id 145
    label "oficer"
  ]
  node [
    id 146
    label "parlamentarzysta"
  ]
  node [
    id 147
    label "Pi&#322;sudski"
  ]
  node [
    id 148
    label "dyplomata"
  ]
  node [
    id 149
    label "wys&#322;annik"
  ]
  node [
    id 150
    label "kurier_dyplomatyczny"
  ]
  node [
    id 151
    label "ablegat"
  ]
  node [
    id 152
    label "klubista"
  ]
  node [
    id 153
    label "Miko&#322;ajczyk"
  ]
  node [
    id 154
    label "Korwin"
  ]
  node [
    id 155
    label "dyscyplina_partyjna"
  ]
  node [
    id 156
    label "izba_ni&#380;sza"
  ]
  node [
    id 157
    label "poselstwo"
  ]
  node [
    id 158
    label "Karta_Nauczyciela"
  ]
  node [
    id 159
    label "marc&#243;wka"
  ]
  node [
    id 160
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 161
    label "akt"
  ]
  node [
    id 162
    label "przej&#347;&#263;"
  ]
  node [
    id 163
    label "charter"
  ]
  node [
    id 164
    label "przej&#347;cie"
  ]
  node [
    id 165
    label "etatowy"
  ]
  node [
    id 166
    label "bud&#380;etowo"
  ]
  node [
    id 167
    label "budgetary"
  ]
  node [
    id 168
    label "si&#281;ga&#263;"
  ]
  node [
    id 169
    label "trwa&#263;"
  ]
  node [
    id 170
    label "obecno&#347;&#263;"
  ]
  node [
    id 171
    label "stan"
  ]
  node [
    id 172
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 173
    label "stand"
  ]
  node [
    id 174
    label "mie&#263;_miejsce"
  ]
  node [
    id 175
    label "uczestniczy&#263;"
  ]
  node [
    id 176
    label "chodzi&#263;"
  ]
  node [
    id 177
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 178
    label "equal"
  ]
  node [
    id 179
    label "silny"
  ]
  node [
    id 180
    label "wa&#380;nie"
  ]
  node [
    id 181
    label "eksponowany"
  ]
  node [
    id 182
    label "istotnie"
  ]
  node [
    id 183
    label "znaczny"
  ]
  node [
    id 184
    label "dobry"
  ]
  node [
    id 185
    label "wynios&#322;y"
  ]
  node [
    id 186
    label "dono&#347;ny"
  ]
  node [
    id 187
    label "dokument"
  ]
  node [
    id 188
    label "device"
  ]
  node [
    id 189
    label "program_u&#380;ytkowy"
  ]
  node [
    id 190
    label "intencja"
  ]
  node [
    id 191
    label "agreement"
  ]
  node [
    id 192
    label "pomys&#322;"
  ]
  node [
    id 193
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 194
    label "plan"
  ]
  node [
    id 195
    label "dokumentacja"
  ]
  node [
    id 196
    label "dar"
  ]
  node [
    id 197
    label "cnota"
  ]
  node [
    id 198
    label "buddyzm"
  ]
  node [
    id 199
    label "stulecie"
  ]
  node [
    id 200
    label "kalendarz"
  ]
  node [
    id 201
    label "czas"
  ]
  node [
    id 202
    label "pora_roku"
  ]
  node [
    id 203
    label "cykl_astronomiczny"
  ]
  node [
    id 204
    label "p&#243;&#322;rocze"
  ]
  node [
    id 205
    label "grupa"
  ]
  node [
    id 206
    label "kwarta&#322;"
  ]
  node [
    id 207
    label "kurs"
  ]
  node [
    id 208
    label "jubileusz"
  ]
  node [
    id 209
    label "lata"
  ]
  node [
    id 210
    label "martwy_sezon"
  ]
  node [
    id 211
    label "klasyfikator"
  ]
  node [
    id 212
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 213
    label "decide"
  ]
  node [
    id 214
    label "mean"
  ]
  node [
    id 215
    label "nak&#322;ad"
  ]
  node [
    id 216
    label "koszt"
  ]
  node [
    id 217
    label "wych&#243;d"
  ]
  node [
    id 218
    label "zakres"
  ]
  node [
    id 219
    label "Ural"
  ]
  node [
    id 220
    label "koniec"
  ]
  node [
    id 221
    label "kres"
  ]
  node [
    id 222
    label "granice"
  ]
  node [
    id 223
    label "granica_pa&#324;stwa"
  ]
  node [
    id 224
    label "pu&#322;ap"
  ]
  node [
    id 225
    label "frontier"
  ]
  node [
    id 226
    label "end"
  ]
  node [
    id 227
    label "miara"
  ]
  node [
    id 228
    label "poj&#281;cie"
  ]
  node [
    id 229
    label "liczba"
  ]
  node [
    id 230
    label "zorganizowa&#263;"
  ]
  node [
    id 231
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 232
    label "przerobi&#263;"
  ]
  node [
    id 233
    label "wystylizowa&#263;"
  ]
  node [
    id 234
    label "cause"
  ]
  node [
    id 235
    label "wydali&#263;"
  ]
  node [
    id 236
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 237
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 238
    label "post&#261;pi&#263;"
  ]
  node [
    id 239
    label "appoint"
  ]
  node [
    id 240
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 241
    label "nabra&#263;"
  ]
  node [
    id 242
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 243
    label "make"
  ]
  node [
    id 244
    label "obrady"
  ]
  node [
    id 245
    label "zesp&#243;&#322;"
  ]
  node [
    id 246
    label "organ"
  ]
  node [
    id 247
    label "Komisja_Europejska"
  ]
  node [
    id 248
    label "podkomisja"
  ]
  node [
    id 249
    label "uruchomienie"
  ]
  node [
    id 250
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 251
    label "nauka_ekonomiczna"
  ]
  node [
    id 252
    label "supernadz&#243;r"
  ]
  node [
    id 253
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 254
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 255
    label "absolutorium"
  ]
  node [
    id 256
    label "podupada&#263;"
  ]
  node [
    id 257
    label "nap&#322;ywanie"
  ]
  node [
    id 258
    label "podupadanie"
  ]
  node [
    id 259
    label "kwestor"
  ]
  node [
    id 260
    label "uruchamia&#263;"
  ]
  node [
    id 261
    label "mienie"
  ]
  node [
    id 262
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 263
    label "uruchamianie"
  ]
  node [
    id 264
    label "czynnik_produkcji"
  ]
  node [
    id 265
    label "jawny"
  ]
  node [
    id 266
    label "upublicznienie"
  ]
  node [
    id 267
    label "upublicznianie"
  ]
  node [
    id 268
    label "publicznie"
  ]
  node [
    id 269
    label "u&#380;ytecznie"
  ]
  node [
    id 270
    label "praktyczny"
  ]
  node [
    id 271
    label "racjonalnie"
  ]
  node [
    id 272
    label "si&#322;a"
  ]
  node [
    id 273
    label "lina"
  ]
  node [
    id 274
    label "way"
  ]
  node [
    id 275
    label "cable"
  ]
  node [
    id 276
    label "przebieg"
  ]
  node [
    id 277
    label "zbi&#243;r"
  ]
  node [
    id 278
    label "ch&#243;d"
  ]
  node [
    id 279
    label "trasa"
  ]
  node [
    id 280
    label "rz&#261;d"
  ]
  node [
    id 281
    label "k&#322;us"
  ]
  node [
    id 282
    label "progression"
  ]
  node [
    id 283
    label "current"
  ]
  node [
    id 284
    label "pr&#261;d"
  ]
  node [
    id 285
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 286
    label "wydarzenie"
  ]
  node [
    id 287
    label "lot"
  ]
  node [
    id 288
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 289
    label "szczyt"
  ]
  node [
    id 290
    label "wiedzie&#263;"
  ]
  node [
    id 291
    label "zna&#263;"
  ]
  node [
    id 292
    label "czu&#263;"
  ]
  node [
    id 293
    label "j&#281;zyk"
  ]
  node [
    id 294
    label "kuma&#263;"
  ]
  node [
    id 295
    label "give"
  ]
  node [
    id 296
    label "odbiera&#263;"
  ]
  node [
    id 297
    label "empatia"
  ]
  node [
    id 298
    label "see"
  ]
  node [
    id 299
    label "match"
  ]
  node [
    id 300
    label "dziama&#263;"
  ]
  node [
    id 301
    label "gotowo&#347;&#263;"
  ]
  node [
    id 302
    label "fajter"
  ]
  node [
    id 303
    label "zwi&#261;zek"
  ]
  node [
    id 304
    label "ONZ"
  ]
  node [
    id 305
    label "alianci"
  ]
  node [
    id 306
    label "blok"
  ]
  node [
    id 307
    label "Paneuropa"
  ]
  node [
    id 308
    label "NATO"
  ]
  node [
    id 309
    label "confederation"
  ]
  node [
    id 310
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 311
    label "po&#347;pie&#263;"
  ]
  node [
    id 312
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 313
    label "sko&#324;czy&#263;"
  ]
  node [
    id 314
    label "utrzyma&#263;"
  ]
  node [
    id 315
    label "wydatki_bud&#380;etowe"
  ]
  node [
    id 316
    label "etat"
  ]
  node [
    id 317
    label "portfel"
  ]
  node [
    id 318
    label "rezerwa_rewaluacyjna"
  ]
  node [
    id 319
    label "kwota"
  ]
  node [
    id 320
    label "ustanowi&#263;"
  ]
  node [
    id 321
    label "resoluteness"
  ]
  node [
    id 322
    label "zestaw"
  ]
  node [
    id 323
    label "scali&#263;"
  ]
  node [
    id 324
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 325
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 326
    label "note"
  ]
  node [
    id 327
    label "set"
  ]
  node [
    id 328
    label "da&#263;"
  ]
  node [
    id 329
    label "marshal"
  ]
  node [
    id 330
    label "opracowa&#263;"
  ]
  node [
    id 331
    label "przekaza&#263;"
  ]
  node [
    id 332
    label "zmieni&#263;"
  ]
  node [
    id 333
    label "pay"
  ]
  node [
    id 334
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 335
    label "odda&#263;"
  ]
  node [
    id 336
    label "przy&#322;o&#380;y&#263;"
  ]
  node [
    id 337
    label "jell"
  ]
  node [
    id 338
    label "spowodowa&#263;"
  ]
  node [
    id 339
    label "frame"
  ]
  node [
    id 340
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 341
    label "zebra&#263;"
  ]
  node [
    id 342
    label "fold"
  ]
  node [
    id 343
    label "w&#322;asnor&#281;czny"
  ]
  node [
    id 344
    label "napis"
  ]
  node [
    id 345
    label "obja&#347;nienie"
  ]
  node [
    id 346
    label "sign"
  ]
  node [
    id 347
    label "potwierdzenie"
  ]
  node [
    id 348
    label "signal"
  ]
  node [
    id 349
    label "znak"
  ]
  node [
    id 350
    label "Jelcyn"
  ]
  node [
    id 351
    label "Roosevelt"
  ]
  node [
    id 352
    label "Clinton"
  ]
  node [
    id 353
    label "Nixon"
  ]
  node [
    id 354
    label "Tito"
  ]
  node [
    id 355
    label "de_Gaulle"
  ]
  node [
    id 356
    label "gruba_ryba"
  ]
  node [
    id 357
    label "Gorbaczow"
  ]
  node [
    id 358
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 359
    label "Putin"
  ]
  node [
    id 360
    label "Naser"
  ]
  node [
    id 361
    label "samorz&#261;dowiec"
  ]
  node [
    id 362
    label "Kemal"
  ]
  node [
    id 363
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 364
    label "zwierzchnik"
  ]
  node [
    id 365
    label "Bierut"
  ]
  node [
    id 366
    label "Nowy_Rok"
  ]
  node [
    id 367
    label "kompletny"
  ]
  node [
    id 368
    label "wniwecz"
  ]
  node [
    id 369
    label "zupe&#322;ny"
  ]
  node [
    id 370
    label "postawi&#263;"
  ]
  node [
    id 371
    label "miejsce"
  ]
  node [
    id 372
    label "awansowanie"
  ]
  node [
    id 373
    label "po&#322;o&#380;enie"
  ]
  node [
    id 374
    label "awansowa&#263;"
  ]
  node [
    id 375
    label "uprawianie"
  ]
  node [
    id 376
    label "powierzanie"
  ]
  node [
    id 377
    label "punkt"
  ]
  node [
    id 378
    label "pogl&#261;d"
  ]
  node [
    id 379
    label "wojsko"
  ]
  node [
    id 380
    label "wakowa&#263;"
  ]
  node [
    id 381
    label "stawia&#263;"
  ]
  node [
    id 382
    label "s&#261;d"
  ]
  node [
    id 383
    label "biurko"
  ]
  node [
    id 384
    label "palestra"
  ]
  node [
    id 385
    label "pomieszczenie"
  ]
  node [
    id 386
    label "biuro"
  ]
  node [
    id 387
    label "regent"
  ]
  node [
    id 388
    label "boks"
  ]
  node [
    id 389
    label "chancellery"
  ]
  node [
    id 390
    label "bezpo&#347;rednio"
  ]
  node [
    id 391
    label "osobisty"
  ]
  node [
    id 392
    label "emocjonalnie"
  ]
  node [
    id 393
    label "intymnie"
  ]
  node [
    id 394
    label "szczerze"
  ]
  node [
    id 395
    label "obserwacja"
  ]
  node [
    id 396
    label "moralno&#347;&#263;"
  ]
  node [
    id 397
    label "podstawa"
  ]
  node [
    id 398
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 399
    label "umowa"
  ]
  node [
    id 400
    label "dominion"
  ]
  node [
    id 401
    label "qualification"
  ]
  node [
    id 402
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 403
    label "opis"
  ]
  node [
    id 404
    label "regu&#322;a_Allena"
  ]
  node [
    id 405
    label "normalizacja"
  ]
  node [
    id 406
    label "regu&#322;a_Glogera"
  ]
  node [
    id 407
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 408
    label "standard"
  ]
  node [
    id 409
    label "base"
  ]
  node [
    id 410
    label "substancja"
  ]
  node [
    id 411
    label "spos&#243;b"
  ]
  node [
    id 412
    label "prawid&#322;o"
  ]
  node [
    id 413
    label "prawo_Mendla"
  ]
  node [
    id 414
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 415
    label "criterion"
  ]
  node [
    id 416
    label "twierdzenie"
  ]
  node [
    id 417
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 418
    label "prawo"
  ]
  node [
    id 419
    label "occupation"
  ]
  node [
    id 420
    label "zasada_d'Alemberta"
  ]
  node [
    id 421
    label "stosunek_pracy"
  ]
  node [
    id 422
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 423
    label "benedykty&#324;ski"
  ]
  node [
    id 424
    label "pracowanie"
  ]
  node [
    id 425
    label "zaw&#243;d"
  ]
  node [
    id 426
    label "kierownictwo"
  ]
  node [
    id 427
    label "zmiana"
  ]
  node [
    id 428
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 429
    label "wytw&#243;r"
  ]
  node [
    id 430
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 431
    label "tynkarski"
  ]
  node [
    id 432
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 433
    label "zobowi&#261;zanie"
  ]
  node [
    id 434
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 435
    label "czynno&#347;&#263;"
  ]
  node [
    id 436
    label "tyrka"
  ]
  node [
    id 437
    label "siedziba"
  ]
  node [
    id 438
    label "poda&#380;_pracy"
  ]
  node [
    id 439
    label "zak&#322;ad"
  ]
  node [
    id 440
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 441
    label "najem"
  ]
  node [
    id 442
    label "plankton_polityczny"
  ]
  node [
    id 443
    label "ustawodawca"
  ]
  node [
    id 444
    label "urz&#261;d"
  ]
  node [
    id 445
    label "europarlament"
  ]
  node [
    id 446
    label "grupa_bilateralna"
  ]
  node [
    id 447
    label "erotyka"
  ]
  node [
    id 448
    label "podniecanie"
  ]
  node [
    id 449
    label "wzw&#243;d"
  ]
  node [
    id 450
    label "rozmna&#380;anie"
  ]
  node [
    id 451
    label "po&#380;&#261;danie"
  ]
  node [
    id 452
    label "imisja"
  ]
  node [
    id 453
    label "po&#380;ycie"
  ]
  node [
    id 454
    label "pozycja_misjonarska"
  ]
  node [
    id 455
    label "podnieci&#263;"
  ]
  node [
    id 456
    label "podnieca&#263;"
  ]
  node [
    id 457
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 458
    label "iloraz"
  ]
  node [
    id 459
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 460
    label "gra_wst&#281;pna"
  ]
  node [
    id 461
    label "podej&#347;cie"
  ]
  node [
    id 462
    label "cecha"
  ]
  node [
    id 463
    label "wyraz_skrajny"
  ]
  node [
    id 464
    label "numer"
  ]
  node [
    id 465
    label "ruch_frykcyjny"
  ]
  node [
    id 466
    label "baraszki"
  ]
  node [
    id 467
    label "powaga"
  ]
  node [
    id 468
    label "na_pieska"
  ]
  node [
    id 469
    label "z&#322;&#261;czenie"
  ]
  node [
    id 470
    label "relacja"
  ]
  node [
    id 471
    label "seks"
  ]
  node [
    id 472
    label "podniecenie"
  ]
  node [
    id 473
    label "bangla&#263;"
  ]
  node [
    id 474
    label "tryb"
  ]
  node [
    id 475
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 476
    label "express"
  ]
  node [
    id 477
    label "rzekn&#261;&#263;"
  ]
  node [
    id 478
    label "okre&#347;li&#263;"
  ]
  node [
    id 479
    label "wyrazi&#263;"
  ]
  node [
    id 480
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 481
    label "unwrap"
  ]
  node [
    id 482
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 483
    label "convey"
  ]
  node [
    id 484
    label "discover"
  ]
  node [
    id 485
    label "wydoby&#263;"
  ]
  node [
    id 486
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 487
    label "poda&#263;"
  ]
  node [
    id 488
    label "robotnik"
  ]
  node [
    id 489
    label "specjalista"
  ]
  node [
    id 490
    label "macher"
  ]
  node [
    id 491
    label "us&#322;ugowiec"
  ]
  node [
    id 492
    label "byd&#322;o"
  ]
  node [
    id 493
    label "zobo"
  ]
  node [
    id 494
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 495
    label "yakalo"
  ]
  node [
    id 496
    label "dzo"
  ]
  node [
    id 497
    label "return"
  ]
  node [
    id 498
    label "dostarcza&#263;"
  ]
  node [
    id 499
    label "anektowa&#263;"
  ]
  node [
    id 500
    label "pali&#263;_si&#281;"
  ]
  node [
    id 501
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 502
    label "korzysta&#263;"
  ]
  node [
    id 503
    label "fill"
  ]
  node [
    id 504
    label "aim"
  ]
  node [
    id 505
    label "rozciekawia&#263;"
  ]
  node [
    id 506
    label "zadawa&#263;"
  ]
  node [
    id 507
    label "robi&#263;"
  ]
  node [
    id 508
    label "do"
  ]
  node [
    id 509
    label "klasyfikacja"
  ]
  node [
    id 510
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 511
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 512
    label "bra&#263;"
  ]
  node [
    id 513
    label "obejmowa&#263;"
  ]
  node [
    id 514
    label "sake"
  ]
  node [
    id 515
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 516
    label "schorzenie"
  ]
  node [
    id 517
    label "zabiera&#263;"
  ]
  node [
    id 518
    label "komornik"
  ]
  node [
    id 519
    label "prosecute"
  ]
  node [
    id 520
    label "topographic_point"
  ]
  node [
    id 521
    label "powodowa&#263;"
  ]
  node [
    id 522
    label "czyj&#347;"
  ]
  node [
    id 523
    label "od&#322;&#261;czenie"
  ]
  node [
    id 524
    label "cutoff"
  ]
  node [
    id 525
    label "przestanie"
  ]
  node [
    id 526
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 527
    label "w&#322;&#261;czenie"
  ]
  node [
    id 528
    label "nieprawdopodobie&#324;stwo"
  ]
  node [
    id 529
    label "wydzielenie"
  ]
  node [
    id 530
    label "przygaszenie"
  ]
  node [
    id 531
    label "wykluczenie"
  ]
  node [
    id 532
    label "debarment"
  ]
  node [
    id 533
    label "gotowanie"
  ]
  node [
    id 534
    label "przerwanie"
  ]
  node [
    id 535
    label "zatrzymanie"
  ]
  node [
    id 536
    label "release"
  ]
  node [
    id 537
    label "closure"
  ]
  node [
    id 538
    label "odrzucenie"
  ]
  node [
    id 539
    label "odci&#281;cie"
  ]
  node [
    id 540
    label "u&#380;ywa&#263;"
  ]
  node [
    id 541
    label "wypowied&#378;"
  ]
  node [
    id 542
    label "od&#322;o&#380;enie"
  ]
  node [
    id 543
    label "skill"
  ]
  node [
    id 544
    label "doznanie"
  ]
  node [
    id 545
    label "gaze"
  ]
  node [
    id 546
    label "deference"
  ]
  node [
    id 547
    label "dochrapanie_si&#281;"
  ]
  node [
    id 548
    label "dostarczenie"
  ]
  node [
    id 549
    label "mention"
  ]
  node [
    id 550
    label "bearing"
  ]
  node [
    id 551
    label "po&#380;yczenie"
  ]
  node [
    id 552
    label "cel"
  ]
  node [
    id 553
    label "uzyskanie"
  ]
  node [
    id 554
    label "oddanie"
  ]
  node [
    id 555
    label "powi&#261;zanie"
  ]
  node [
    id 556
    label "Zgredek"
  ]
  node [
    id 557
    label "kategoria_gramatyczna"
  ]
  node [
    id 558
    label "Casanova"
  ]
  node [
    id 559
    label "Don_Juan"
  ]
  node [
    id 560
    label "Gargantua"
  ]
  node [
    id 561
    label "Faust"
  ]
  node [
    id 562
    label "profanum"
  ]
  node [
    id 563
    label "Chocho&#322;"
  ]
  node [
    id 564
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 565
    label "koniugacja"
  ]
  node [
    id 566
    label "Winnetou"
  ]
  node [
    id 567
    label "Dwukwiat"
  ]
  node [
    id 568
    label "homo_sapiens"
  ]
  node [
    id 569
    label "Edyp"
  ]
  node [
    id 570
    label "Herkules_Poirot"
  ]
  node [
    id 571
    label "ludzko&#347;&#263;"
  ]
  node [
    id 572
    label "mikrokosmos"
  ]
  node [
    id 573
    label "person"
  ]
  node [
    id 574
    label "Sherlock_Holmes"
  ]
  node [
    id 575
    label "portrecista"
  ]
  node [
    id 576
    label "Szwejk"
  ]
  node [
    id 577
    label "Hamlet"
  ]
  node [
    id 578
    label "duch"
  ]
  node [
    id 579
    label "g&#322;owa"
  ]
  node [
    id 580
    label "oddzia&#322;ywanie"
  ]
  node [
    id 581
    label "Quasimodo"
  ]
  node [
    id 582
    label "Dulcynea"
  ]
  node [
    id 583
    label "Don_Kiszot"
  ]
  node [
    id 584
    label "Wallenrod"
  ]
  node [
    id 585
    label "Plastu&#347;"
  ]
  node [
    id 586
    label "Harry_Potter"
  ]
  node [
    id 587
    label "figura"
  ]
  node [
    id 588
    label "parali&#380;owa&#263;"
  ]
  node [
    id 589
    label "istota"
  ]
  node [
    id 590
    label "Werter"
  ]
  node [
    id 591
    label "antropochoria"
  ]
  node [
    id 592
    label "posta&#263;"
  ]
  node [
    id 593
    label "Brenna"
  ]
  node [
    id 594
    label "Szlisselburg"
  ]
  node [
    id 595
    label "Dyjament"
  ]
  node [
    id 596
    label "flanka"
  ]
  node [
    id 597
    label "schronienie"
  ]
  node [
    id 598
    label "budowla"
  ]
  node [
    id 599
    label "bastion"
  ]
  node [
    id 600
    label "bargain"
  ]
  node [
    id 601
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 602
    label "tycze&#263;"
  ]
  node [
    id 603
    label "frymark"
  ]
  node [
    id 604
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 605
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 606
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 607
    label "commodity"
  ]
  node [
    id 608
    label "Wilko"
  ]
  node [
    id 609
    label "jednostka_monetarna"
  ]
  node [
    id 610
    label "centym"
  ]
  node [
    id 611
    label "oskoma"
  ]
  node [
    id 612
    label "wish"
  ]
  node [
    id 613
    label "emocja"
  ]
  node [
    id 614
    label "mniemanie"
  ]
  node [
    id 615
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 616
    label "inclination"
  ]
  node [
    id 617
    label "zajawka"
  ]
  node [
    id 618
    label "matczysko"
  ]
  node [
    id 619
    label "macierz"
  ]
  node [
    id 620
    label "przodkini"
  ]
  node [
    id 621
    label "Matka_Boska"
  ]
  node [
    id 622
    label "macocha"
  ]
  node [
    id 623
    label "matka_zast&#281;pcza"
  ]
  node [
    id 624
    label "stara"
  ]
  node [
    id 625
    label "rodzice"
  ]
  node [
    id 626
    label "rodzic"
  ]
  node [
    id 627
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 628
    label "miech"
  ]
  node [
    id 629
    label "kalendy"
  ]
  node [
    id 630
    label "tydzie&#324;"
  ]
  node [
    id 631
    label "plewinka"
  ]
  node [
    id 632
    label "astrowce"
  ]
  node [
    id 633
    label "ninie"
  ]
  node [
    id 634
    label "aktualny"
  ]
  node [
    id 635
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 636
    label "base_on_balls"
  ]
  node [
    id 637
    label "wolno"
  ]
  node [
    id 638
    label "cichy"
  ]
  node [
    id 639
    label "przyjemnie"
  ]
  node [
    id 640
    label "bezproblemowo"
  ]
  node [
    id 641
    label "spokojny"
  ]
  node [
    id 642
    label "endeavor"
  ]
  node [
    id 643
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 644
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 645
    label "dzia&#322;a&#263;"
  ]
  node [
    id 646
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 647
    label "work"
  ]
  node [
    id 648
    label "maszyna"
  ]
  node [
    id 649
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 650
    label "podejmowa&#263;"
  ]
  node [
    id 651
    label "okre&#347;lony"
  ]
  node [
    id 652
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 653
    label "jaki&#347;"
  ]
  node [
    id 654
    label "sprawa"
  ]
  node [
    id 655
    label "zadanie"
  ]
  node [
    id 656
    label "problemat"
  ]
  node [
    id 657
    label "rozpytywanie"
  ]
  node [
    id 658
    label "sprawdzian"
  ]
  node [
    id 659
    label "przes&#322;uchiwanie"
  ]
  node [
    id 660
    label "wypytanie"
  ]
  node [
    id 661
    label "zwracanie_si&#281;"
  ]
  node [
    id 662
    label "wypowiedzenie"
  ]
  node [
    id 663
    label "wywo&#322;ywanie"
  ]
  node [
    id 664
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 665
    label "question"
  ]
  node [
    id 666
    label "sprawdzanie"
  ]
  node [
    id 667
    label "odpowiadanie"
  ]
  node [
    id 668
    label "survey"
  ]
  node [
    id 669
    label "odpowiada&#263;"
  ]
  node [
    id 670
    label "egzaminowanie"
  ]
  node [
    id 671
    label "porobienie"
  ]
  node [
    id 672
    label "odsiedzenie"
  ]
  node [
    id 673
    label "dyskusja"
  ]
  node [
    id 674
    label "conference"
  ]
  node [
    id 675
    label "convention"
  ]
  node [
    id 676
    label "adjustment"
  ]
  node [
    id 677
    label "zgromadzenie"
  ]
  node [
    id 678
    label "pobycie"
  ]
  node [
    id 679
    label "konsylium"
  ]
  node [
    id 680
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 681
    label "cz&#322;onek"
  ]
  node [
    id 682
    label "substytuowanie"
  ]
  node [
    id 683
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 684
    label "przyk&#322;ad"
  ]
  node [
    id 685
    label "zast&#281;pca"
  ]
  node [
    id 686
    label "substytuowa&#263;"
  ]
  node [
    id 687
    label "sprzeciw"
  ]
  node [
    id 688
    label "copy"
  ]
  node [
    id 689
    label "ponie&#347;&#263;"
  ]
  node [
    id 690
    label "zareagowa&#263;"
  ]
  node [
    id 691
    label "react"
  ]
  node [
    id 692
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 693
    label "tax_return"
  ]
  node [
    id 694
    label "bekn&#261;&#263;"
  ]
  node [
    id 695
    label "picture"
  ]
  node [
    id 696
    label "paleograf"
  ]
  node [
    id 697
    label "list"
  ]
  node [
    id 698
    label "egzemplarz"
  ]
  node [
    id 699
    label "komunikacja"
  ]
  node [
    id 700
    label "psychotest"
  ]
  node [
    id 701
    label "ortografia"
  ]
  node [
    id 702
    label "handwriting"
  ]
  node [
    id 703
    label "grafia"
  ]
  node [
    id 704
    label "prasa"
  ]
  node [
    id 705
    label "adres"
  ]
  node [
    id 706
    label "script"
  ]
  node [
    id 707
    label "dzia&#322;"
  ]
  node [
    id 708
    label "paleografia"
  ]
  node [
    id 709
    label "Zwrotnica"
  ]
  node [
    id 710
    label "wk&#322;ad"
  ]
  node [
    id 711
    label "przekaz"
  ]
  node [
    id 712
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 713
    label "interpunkcja"
  ]
  node [
    id 714
    label "communication"
  ]
  node [
    id 715
    label "ok&#322;adka"
  ]
  node [
    id 716
    label "dzie&#322;o"
  ]
  node [
    id 717
    label "czasopismo"
  ]
  node [
    id 718
    label "letter"
  ]
  node [
    id 719
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 720
    label "dzisiejszo"
  ]
  node [
    id 721
    label "krzywda"
  ]
  node [
    id 722
    label "wyzwisko"
  ]
  node [
    id 723
    label "wrzuta"
  ]
  node [
    id 724
    label "ubliga"
  ]
  node [
    id 725
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 726
    label "indignation"
  ]
  node [
    id 727
    label "niedorobek"
  ]
  node [
    id 728
    label "tekst"
  ]
  node [
    id 729
    label "w_chuj"
  ]
  node [
    id 730
    label "trudno&#347;&#263;"
  ]
  node [
    id 731
    label "ambaras"
  ]
  node [
    id 732
    label "pierepa&#322;ka"
  ]
  node [
    id 733
    label "obstruction"
  ]
  node [
    id 734
    label "jajko_Kolumba"
  ]
  node [
    id 735
    label "subiekcja"
  ]
  node [
    id 736
    label "wsp&#243;lny"
  ]
  node [
    id 737
    label "punctiliously"
  ]
  node [
    id 738
    label "dok&#322;adny"
  ]
  node [
    id 739
    label "meticulously"
  ]
  node [
    id 740
    label "precyzyjnie"
  ]
  node [
    id 741
    label "rzetelnie"
  ]
  node [
    id 742
    label "arrange"
  ]
  node [
    id 743
    label "dress"
  ]
  node [
    id 744
    label "wyszkoli&#263;"
  ]
  node [
    id 745
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 746
    label "wytworzy&#263;"
  ]
  node [
    id 747
    label "ukierunkowa&#263;"
  ]
  node [
    id 748
    label "train"
  ]
  node [
    id 749
    label "wykona&#263;"
  ]
  node [
    id 750
    label "cook"
  ]
  node [
    id 751
    label "analysis"
  ]
  node [
    id 752
    label "reakcja_chemiczna"
  ]
  node [
    id 753
    label "dissection"
  ]
  node [
    id 754
    label "badanie"
  ]
  node [
    id 755
    label "metoda"
  ]
  node [
    id 756
    label "poprawa"
  ]
  node [
    id 757
    label "warto&#347;&#263;"
  ]
  node [
    id 758
    label "alternation"
  ]
  node [
    id 759
    label "modyfikacja"
  ]
  node [
    id 760
    label "egzamin"
  ]
  node [
    id 761
    label "quicker"
  ]
  node [
    id 762
    label "promptly"
  ]
  node [
    id 763
    label "quickest"
  ]
  node [
    id 764
    label "sprawnie"
  ]
  node [
    id 765
    label "dynamicznie"
  ]
  node [
    id 766
    label "szybciej"
  ]
  node [
    id 767
    label "prosto"
  ]
  node [
    id 768
    label "szybciochem"
  ]
  node [
    id 769
    label "szybki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 212
  ]
  edge [
    source 12
    target 213
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 215
  ]
  edge [
    source 13
    target 216
  ]
  edge [
    source 13
    target 217
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 218
  ]
  edge [
    source 14
    target 219
  ]
  edge [
    source 14
    target 220
  ]
  edge [
    source 14
    target 221
  ]
  edge [
    source 14
    target 222
  ]
  edge [
    source 14
    target 223
  ]
  edge [
    source 14
    target 224
  ]
  edge [
    source 14
    target 225
  ]
  edge [
    source 14
    target 226
  ]
  edge [
    source 14
    target 227
  ]
  edge [
    source 14
    target 228
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 35
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 15
    target 70
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 232
  ]
  edge [
    source 17
    target 233
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 237
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 239
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 241
  ]
  edge [
    source 17
    target 242
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 18
    target 244
  ]
  edge [
    source 18
    target 245
  ]
  edge [
    source 18
    target 246
  ]
  edge [
    source 18
    target 247
  ]
  edge [
    source 18
    target 248
  ]
  edge [
    source 18
    target 100
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 64
  ]
  edge [
    source 19
    target 53
  ]
  edge [
    source 19
    target 249
  ]
  edge [
    source 19
    target 250
  ]
  edge [
    source 19
    target 251
  ]
  edge [
    source 19
    target 252
  ]
  edge [
    source 19
    target 253
  ]
  edge [
    source 19
    target 254
  ]
  edge [
    source 19
    target 255
  ]
  edge [
    source 19
    target 256
  ]
  edge [
    source 19
    target 257
  ]
  edge [
    source 19
    target 258
  ]
  edge [
    source 19
    target 259
  ]
  edge [
    source 19
    target 260
  ]
  edge [
    source 19
    target 261
  ]
  edge [
    source 19
    target 262
  ]
  edge [
    source 19
    target 263
  ]
  edge [
    source 19
    target 264
  ]
  edge [
    source 19
    target 40
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 88
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 89
  ]
  edge [
    source 20
    target 95
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 266
  ]
  edge [
    source 20
    target 267
  ]
  edge [
    source 20
    target 268
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 269
  ]
  edge [
    source 21
    target 270
  ]
  edge [
    source 21
    target 271
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 272
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 273
  ]
  edge [
    source 22
    target 274
  ]
  edge [
    source 22
    target 275
  ]
  edge [
    source 22
    target 276
  ]
  edge [
    source 22
    target 277
  ]
  edge [
    source 22
    target 278
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 22
    target 280
  ]
  edge [
    source 22
    target 281
  ]
  edge [
    source 22
    target 282
  ]
  edge [
    source 22
    target 283
  ]
  edge [
    source 22
    target 284
  ]
  edge [
    source 22
    target 285
  ]
  edge [
    source 22
    target 286
  ]
  edge [
    source 22
    target 287
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 288
  ]
  edge [
    source 24
    target 289
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 37
  ]
  edge [
    source 25
    target 38
  ]
  edge [
    source 25
    target 290
  ]
  edge [
    source 25
    target 291
  ]
  edge [
    source 25
    target 292
  ]
  edge [
    source 25
    target 293
  ]
  edge [
    source 25
    target 294
  ]
  edge [
    source 25
    target 295
  ]
  edge [
    source 25
    target 296
  ]
  edge [
    source 25
    target 297
  ]
  edge [
    source 25
    target 298
  ]
  edge [
    source 25
    target 299
  ]
  edge [
    source 25
    target 300
  ]
  edge [
    source 25
    target 97
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 301
  ]
  edge [
    source 26
    target 302
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 74
  ]
  edge [
    source 27
    target 98
  ]
  edge [
    source 27
    target 303
  ]
  edge [
    source 27
    target 304
  ]
  edge [
    source 27
    target 305
  ]
  edge [
    source 27
    target 306
  ]
  edge [
    source 27
    target 307
  ]
  edge [
    source 27
    target 308
  ]
  edge [
    source 27
    target 309
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 78
  ]
  edge [
    source 28
    target 79
  ]
  edge [
    source 28
    target 104
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 105
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 91
  ]
  edge [
    source 29
    target 92
  ]
  edge [
    source 29
    target 94
  ]
  edge [
    source 30
    target 310
  ]
  edge [
    source 30
    target 311
  ]
  edge [
    source 30
    target 312
  ]
  edge [
    source 30
    target 313
  ]
  edge [
    source 30
    target 314
  ]
  edge [
    source 30
    target 105
  ]
  edge [
    source 31
    target 75
  ]
  edge [
    source 31
    target 76
  ]
  edge [
    source 31
    target 102
  ]
  edge [
    source 31
    target 103
  ]
  edge [
    source 31
    target 315
  ]
  edge [
    source 31
    target 316
  ]
  edge [
    source 31
    target 317
  ]
  edge [
    source 31
    target 318
  ]
  edge [
    source 31
    target 319
  ]
  edge [
    source 31
    target 55
  ]
  edge [
    source 31
    target 82
  ]
  edge [
    source 31
    target 101
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 322
  ]
  edge [
    source 33
    target 323
  ]
  edge [
    source 33
    target 324
  ]
  edge [
    source 33
    target 295
  ]
  edge [
    source 33
    target 325
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 33
    target 328
  ]
  edge [
    source 33
    target 329
  ]
  edge [
    source 33
    target 330
  ]
  edge [
    source 33
    target 331
  ]
  edge [
    source 33
    target 332
  ]
  edge [
    source 33
    target 333
  ]
  edge [
    source 33
    target 334
  ]
  edge [
    source 33
    target 335
  ]
  edge [
    source 33
    target 336
  ]
  edge [
    source 33
    target 337
  ]
  edge [
    source 33
    target 338
  ]
  edge [
    source 33
    target 339
  ]
  edge [
    source 33
    target 340
  ]
  edge [
    source 33
    target 341
  ]
  edge [
    source 33
    target 342
  ]
  edge [
    source 34
    target 343
  ]
  edge [
    source 34
    target 344
  ]
  edge [
    source 34
    target 345
  ]
  edge [
    source 34
    target 346
  ]
  edge [
    source 34
    target 347
  ]
  edge [
    source 34
    target 348
  ]
  edge [
    source 34
    target 349
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 39
  ]
  edge [
    source 35
    target 40
  ]
  edge [
    source 35
    target 41
  ]
  edge [
    source 35
    target 72
  ]
  edge [
    source 35
    target 90
  ]
  edge [
    source 35
    target 350
  ]
  edge [
    source 35
    target 351
  ]
  edge [
    source 35
    target 352
  ]
  edge [
    source 35
    target 144
  ]
  edge [
    source 35
    target 353
  ]
  edge [
    source 35
    target 354
  ]
  edge [
    source 35
    target 355
  ]
  edge [
    source 35
    target 356
  ]
  edge [
    source 35
    target 357
  ]
  edge [
    source 35
    target 358
  ]
  edge [
    source 35
    target 359
  ]
  edge [
    source 35
    target 360
  ]
  edge [
    source 35
    target 361
  ]
  edge [
    source 35
    target 362
  ]
  edge [
    source 35
    target 363
  ]
  edge [
    source 35
    target 364
  ]
  edge [
    source 35
    target 365
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 88
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 366
  ]
  edge [
    source 36
    target 74
  ]
  edge [
    source 37
    target 367
  ]
  edge [
    source 37
    target 368
  ]
  edge [
    source 37
    target 369
  ]
  edge [
    source 37
    target 97
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 370
  ]
  edge [
    source 38
    target 371
  ]
  edge [
    source 38
    target 372
  ]
  edge [
    source 38
    target 373
  ]
  edge [
    source 38
    target 374
  ]
  edge [
    source 38
    target 375
  ]
  edge [
    source 38
    target 376
  ]
  edge [
    source 38
    target 377
  ]
  edge [
    source 38
    target 378
  ]
  edge [
    source 38
    target 379
  ]
  edge [
    source 38
    target 46
  ]
  edge [
    source 38
    target 380
  ]
  edge [
    source 38
    target 381
  ]
  edge [
    source 39
    target 72
  ]
  edge [
    source 39
    target 73
  ]
  edge [
    source 39
    target 89
  ]
  edge [
    source 39
    target 382
  ]
  edge [
    source 39
    target 383
  ]
  edge [
    source 39
    target 384
  ]
  edge [
    source 39
    target 385
  ]
  edge [
    source 39
    target 386
  ]
  edge [
    source 39
    target 387
  ]
  edge [
    source 39
    target 388
  ]
  edge [
    source 39
    target 389
  ]
  edge [
    source 39
    target 46
  ]
  edge [
    source 39
    target 50
  ]
  edge [
    source 39
    target 54
  ]
  edge [
    source 40
    target 390
  ]
  edge [
    source 40
    target 391
  ]
  edge [
    source 40
    target 392
  ]
  edge [
    source 40
    target 393
  ]
  edge [
    source 40
    target 394
  ]
  edge [
    source 40
    target 88
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 63
  ]
  edge [
    source 41
    target 57
  ]
  edge [
    source 41
    target 66
  ]
  edge [
    source 41
    target 67
  ]
  edge [
    source 41
    target 76
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 57
  ]
  edge [
    source 43
    target 64
  ]
  edge [
    source 43
    target 99
  ]
  edge [
    source 43
    target 96
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 60
  ]
  edge [
    source 44
    target 67
  ]
  edge [
    source 44
    target 68
  ]
  edge [
    source 44
    target 395
  ]
  edge [
    source 44
    target 396
  ]
  edge [
    source 44
    target 397
  ]
  edge [
    source 44
    target 398
  ]
  edge [
    source 44
    target 399
  ]
  edge [
    source 44
    target 400
  ]
  edge [
    source 44
    target 401
  ]
  edge [
    source 44
    target 402
  ]
  edge [
    source 44
    target 403
  ]
  edge [
    source 44
    target 404
  ]
  edge [
    source 44
    target 405
  ]
  edge [
    source 44
    target 406
  ]
  edge [
    source 44
    target 407
  ]
  edge [
    source 44
    target 408
  ]
  edge [
    source 44
    target 409
  ]
  edge [
    source 44
    target 410
  ]
  edge [
    source 44
    target 411
  ]
  edge [
    source 44
    target 412
  ]
  edge [
    source 44
    target 413
  ]
  edge [
    source 44
    target 414
  ]
  edge [
    source 44
    target 415
  ]
  edge [
    source 44
    target 416
  ]
  edge [
    source 44
    target 417
  ]
  edge [
    source 44
    target 418
  ]
  edge [
    source 44
    target 419
  ]
  edge [
    source 44
    target 420
  ]
  edge [
    source 44
    target 94
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 61
  ]
  edge [
    source 45
    target 52
  ]
  edge [
    source 45
    target 89
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 74
  ]
  edge [
    source 46
    target 75
  ]
  edge [
    source 46
    target 421
  ]
  edge [
    source 46
    target 422
  ]
  edge [
    source 46
    target 423
  ]
  edge [
    source 46
    target 424
  ]
  edge [
    source 46
    target 425
  ]
  edge [
    source 46
    target 426
  ]
  edge [
    source 46
    target 427
  ]
  edge [
    source 46
    target 428
  ]
  edge [
    source 46
    target 429
  ]
  edge [
    source 46
    target 430
  ]
  edge [
    source 46
    target 431
  ]
  edge [
    source 46
    target 264
  ]
  edge [
    source 46
    target 432
  ]
  edge [
    source 46
    target 433
  ]
  edge [
    source 46
    target 434
  ]
  edge [
    source 46
    target 435
  ]
  edge [
    source 46
    target 436
  ]
  edge [
    source 46
    target 83
  ]
  edge [
    source 46
    target 437
  ]
  edge [
    source 46
    target 438
  ]
  edge [
    source 46
    target 371
  ]
  edge [
    source 46
    target 439
  ]
  edge [
    source 46
    target 440
  ]
  edge [
    source 46
    target 441
  ]
  edge [
    source 46
    target 50
  ]
  edge [
    source 46
    target 54
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 205
  ]
  edge [
    source 47
    target 442
  ]
  edge [
    source 47
    target 443
  ]
  edge [
    source 47
    target 444
  ]
  edge [
    source 47
    target 445
  ]
  edge [
    source 47
    target 446
  ]
  edge [
    source 47
    target 74
  ]
  edge [
    source 48
    target 447
  ]
  edge [
    source 48
    target 448
  ]
  edge [
    source 48
    target 449
  ]
  edge [
    source 48
    target 450
  ]
  edge [
    source 48
    target 451
  ]
  edge [
    source 48
    target 452
  ]
  edge [
    source 48
    target 453
  ]
  edge [
    source 48
    target 454
  ]
  edge [
    source 48
    target 455
  ]
  edge [
    source 48
    target 456
  ]
  edge [
    source 48
    target 457
  ]
  edge [
    source 48
    target 458
  ]
  edge [
    source 48
    target 435
  ]
  edge [
    source 48
    target 459
  ]
  edge [
    source 48
    target 460
  ]
  edge [
    source 48
    target 461
  ]
  edge [
    source 48
    target 462
  ]
  edge [
    source 48
    target 463
  ]
  edge [
    source 48
    target 464
  ]
  edge [
    source 48
    target 465
  ]
  edge [
    source 48
    target 466
  ]
  edge [
    source 48
    target 467
  ]
  edge [
    source 48
    target 468
  ]
  edge [
    source 48
    target 469
  ]
  edge [
    source 48
    target 470
  ]
  edge [
    source 48
    target 471
  ]
  edge [
    source 48
    target 472
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 174
  ]
  edge [
    source 49
    target 473
  ]
  edge [
    source 49
    target 300
  ]
  edge [
    source 49
    target 474
  ]
  edge [
    source 49
    target 83
  ]
  edge [
    source 49
    target 91
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 475
  ]
  edge [
    source 51
    target 476
  ]
  edge [
    source 51
    target 477
  ]
  edge [
    source 51
    target 478
  ]
  edge [
    source 51
    target 479
  ]
  edge [
    source 51
    target 480
  ]
  edge [
    source 51
    target 481
  ]
  edge [
    source 51
    target 482
  ]
  edge [
    source 51
    target 483
  ]
  edge [
    source 51
    target 484
  ]
  edge [
    source 51
    target 485
  ]
  edge [
    source 51
    target 486
  ]
  edge [
    source 51
    target 487
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 488
  ]
  edge [
    source 52
    target 489
  ]
  edge [
    source 52
    target 490
  ]
  edge [
    source 52
    target 491
  ]
  edge [
    source 52
    target 89
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 65
  ]
  edge [
    source 53
    target 492
  ]
  edge [
    source 53
    target 493
  ]
  edge [
    source 53
    target 494
  ]
  edge [
    source 53
    target 495
  ]
  edge [
    source 53
    target 496
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 82
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 497
  ]
  edge [
    source 57
    target 498
  ]
  edge [
    source 57
    target 499
  ]
  edge [
    source 57
    target 169
  ]
  edge [
    source 57
    target 500
  ]
  edge [
    source 57
    target 501
  ]
  edge [
    source 57
    target 502
  ]
  edge [
    source 57
    target 503
  ]
  edge [
    source 57
    target 504
  ]
  edge [
    source 57
    target 505
  ]
  edge [
    source 57
    target 506
  ]
  edge [
    source 57
    target 507
  ]
  edge [
    source 57
    target 508
  ]
  edge [
    source 57
    target 509
  ]
  edge [
    source 57
    target 510
  ]
  edge [
    source 57
    target 511
  ]
  edge [
    source 57
    target 512
  ]
  edge [
    source 57
    target 513
  ]
  edge [
    source 57
    target 514
  ]
  edge [
    source 57
    target 515
  ]
  edge [
    source 57
    target 516
  ]
  edge [
    source 57
    target 517
  ]
  edge [
    source 57
    target 518
  ]
  edge [
    source 57
    target 519
  ]
  edge [
    source 57
    target 520
  ]
  edge [
    source 57
    target 521
  ]
  edge [
    source 57
    target 77
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 522
  ]
  edge [
    source 58
    target 114
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 523
  ]
  edge [
    source 59
    target 524
  ]
  edge [
    source 59
    target 525
  ]
  edge [
    source 59
    target 526
  ]
  edge [
    source 59
    target 527
  ]
  edge [
    source 59
    target 528
  ]
  edge [
    source 59
    target 529
  ]
  edge [
    source 59
    target 530
  ]
  edge [
    source 59
    target 531
  ]
  edge [
    source 59
    target 532
  ]
  edge [
    source 59
    target 533
  ]
  edge [
    source 59
    target 534
  ]
  edge [
    source 59
    target 535
  ]
  edge [
    source 59
    target 536
  ]
  edge [
    source 59
    target 537
  ]
  edge [
    source 59
    target 538
  ]
  edge [
    source 59
    target 539
  ]
  edge [
    source 60
    target 540
  ]
  edge [
    source 61
    target 541
  ]
  edge [
    source 61
    target 542
  ]
  edge [
    source 61
    target 543
  ]
  edge [
    source 61
    target 544
  ]
  edge [
    source 61
    target 545
  ]
  edge [
    source 61
    target 546
  ]
  edge [
    source 61
    target 547
  ]
  edge [
    source 61
    target 548
  ]
  edge [
    source 61
    target 549
  ]
  edge [
    source 61
    target 550
  ]
  edge [
    source 61
    target 551
  ]
  edge [
    source 61
    target 552
  ]
  edge [
    source 61
    target 553
  ]
  edge [
    source 61
    target 554
  ]
  edge [
    source 61
    target 555
  ]
  edge [
    source 63
    target 556
  ]
  edge [
    source 63
    target 557
  ]
  edge [
    source 63
    target 558
  ]
  edge [
    source 63
    target 559
  ]
  edge [
    source 63
    target 560
  ]
  edge [
    source 63
    target 561
  ]
  edge [
    source 63
    target 562
  ]
  edge [
    source 63
    target 563
  ]
  edge [
    source 63
    target 564
  ]
  edge [
    source 63
    target 565
  ]
  edge [
    source 63
    target 566
  ]
  edge [
    source 63
    target 567
  ]
  edge [
    source 63
    target 568
  ]
  edge [
    source 63
    target 569
  ]
  edge [
    source 63
    target 570
  ]
  edge [
    source 63
    target 571
  ]
  edge [
    source 63
    target 572
  ]
  edge [
    source 63
    target 573
  ]
  edge [
    source 63
    target 574
  ]
  edge [
    source 63
    target 575
  ]
  edge [
    source 63
    target 576
  ]
  edge [
    source 63
    target 577
  ]
  edge [
    source 63
    target 578
  ]
  edge [
    source 63
    target 579
  ]
  edge [
    source 63
    target 580
  ]
  edge [
    source 63
    target 581
  ]
  edge [
    source 63
    target 582
  ]
  edge [
    source 63
    target 583
  ]
  edge [
    source 63
    target 584
  ]
  edge [
    source 63
    target 585
  ]
  edge [
    source 63
    target 586
  ]
  edge [
    source 63
    target 587
  ]
  edge [
    source 63
    target 588
  ]
  edge [
    source 63
    target 589
  ]
  edge [
    source 63
    target 590
  ]
  edge [
    source 63
    target 591
  ]
  edge [
    source 63
    target 592
  ]
  edge [
    source 64
    target 277
  ]
  edge [
    source 64
    target 97
  ]
  edge [
    source 64
    target 86
  ]
  edge [
    source 64
    target 79
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 67
    target 593
  ]
  edge [
    source 67
    target 594
  ]
  edge [
    source 67
    target 595
  ]
  edge [
    source 67
    target 596
  ]
  edge [
    source 67
    target 597
  ]
  edge [
    source 67
    target 598
  ]
  edge [
    source 67
    target 599
  ]
  edge [
    source 68
    target 600
  ]
  edge [
    source 68
    target 601
  ]
  edge [
    source 68
    target 602
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 603
  ]
  edge [
    source 70
    target 604
  ]
  edge [
    source 70
    target 605
  ]
  edge [
    source 70
    target 606
  ]
  edge [
    source 70
    target 607
  ]
  edge [
    source 70
    target 261
  ]
  edge [
    source 70
    target 608
  ]
  edge [
    source 70
    target 609
  ]
  edge [
    source 70
    target 610
  ]
  edge [
    source 70
    target 87
  ]
  edge [
    source 70
    target 102
  ]
  edge [
    source 71
    target 611
  ]
  edge [
    source 71
    target 612
  ]
  edge [
    source 71
    target 613
  ]
  edge [
    source 71
    target 614
  ]
  edge [
    source 71
    target 615
  ]
  edge [
    source 71
    target 616
  ]
  edge [
    source 71
    target 617
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 618
  ]
  edge [
    source 73
    target 619
  ]
  edge [
    source 73
    target 620
  ]
  edge [
    source 73
    target 621
  ]
  edge [
    source 73
    target 622
  ]
  edge [
    source 73
    target 623
  ]
  edge [
    source 73
    target 624
  ]
  edge [
    source 73
    target 625
  ]
  edge [
    source 73
    target 626
  ]
  edge [
    source 74
    target 97
  ]
  edge [
    source 74
    target 201
  ]
  edge [
    source 74
    target 627
  ]
  edge [
    source 74
    target 628
  ]
  edge [
    source 74
    target 629
  ]
  edge [
    source 74
    target 630
  ]
  edge [
    source 74
    target 79
  ]
  edge [
    source 75
    target 83
  ]
  edge [
    source 75
    target 84
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 631
  ]
  edge [
    source 76
    target 632
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 78
    target 633
  ]
  edge [
    source 78
    target 634
  ]
  edge [
    source 78
    target 635
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 636
  ]
  edge [
    source 80
    target 169
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 637
  ]
  edge [
    source 82
    target 638
  ]
  edge [
    source 82
    target 639
  ]
  edge [
    source 82
    target 640
  ]
  edge [
    source 82
    target 641
  ]
  edge [
    source 83
    target 103
  ]
  edge [
    source 83
    target 104
  ]
  edge [
    source 83
    target 642
  ]
  edge [
    source 83
    target 643
  ]
  edge [
    source 83
    target 174
  ]
  edge [
    source 83
    target 644
  ]
  edge [
    source 83
    target 645
  ]
  edge [
    source 83
    target 646
  ]
  edge [
    source 83
    target 647
  ]
  edge [
    source 83
    target 473
  ]
  edge [
    source 83
    target 508
  ]
  edge [
    source 83
    target 648
  ]
  edge [
    source 83
    target 474
  ]
  edge [
    source 83
    target 300
  ]
  edge [
    source 83
    target 649
  ]
  edge [
    source 83
    target 650
  ]
  edge [
    source 84
    target 651
  ]
  edge [
    source 84
    target 652
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 651
  ]
  edge [
    source 85
    target 653
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 654
  ]
  edge [
    source 86
    target 655
  ]
  edge [
    source 86
    target 541
  ]
  edge [
    source 86
    target 656
  ]
  edge [
    source 86
    target 657
  ]
  edge [
    source 86
    target 658
  ]
  edge [
    source 86
    target 659
  ]
  edge [
    source 86
    target 660
  ]
  edge [
    source 86
    target 661
  ]
  edge [
    source 86
    target 662
  ]
  edge [
    source 86
    target 663
  ]
  edge [
    source 86
    target 664
  ]
  edge [
    source 86
    target 665
  ]
  edge [
    source 86
    target 666
  ]
  edge [
    source 86
    target 667
  ]
  edge [
    source 86
    target 668
  ]
  edge [
    source 86
    target 669
  ]
  edge [
    source 86
    target 670
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 102
  ]
  edge [
    source 88
    target 671
  ]
  edge [
    source 88
    target 672
  ]
  edge [
    source 88
    target 673
  ]
  edge [
    source 88
    target 674
  ]
  edge [
    source 88
    target 675
  ]
  edge [
    source 88
    target 676
  ]
  edge [
    source 88
    target 677
  ]
  edge [
    source 88
    target 678
  ]
  edge [
    source 88
    target 679
  ]
  edge [
    source 88
    target 680
  ]
  edge [
    source 89
    target 107
  ]
  edge [
    source 89
    target 681
  ]
  edge [
    source 89
    target 682
  ]
  edge [
    source 89
    target 683
  ]
  edge [
    source 89
    target 684
  ]
  edge [
    source 89
    target 685
  ]
  edge [
    source 89
    target 686
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 687
  ]
  edge [
    source 91
    target 688
  ]
  edge [
    source 91
    target 328
  ]
  edge [
    source 91
    target 689
  ]
  edge [
    source 91
    target 338
  ]
  edge [
    source 91
    target 690
  ]
  edge [
    source 91
    target 691
  ]
  edge [
    source 91
    target 191
  ]
  edge [
    source 91
    target 692
  ]
  edge [
    source 91
    target 693
  ]
  edge [
    source 91
    target 694
  ]
  edge [
    source 91
    target 695
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 696
  ]
  edge [
    source 92
    target 697
  ]
  edge [
    source 92
    target 698
  ]
  edge [
    source 92
    target 699
  ]
  edge [
    source 92
    target 700
  ]
  edge [
    source 92
    target 701
  ]
  edge [
    source 92
    target 702
  ]
  edge [
    source 92
    target 703
  ]
  edge [
    source 92
    target 704
  ]
  edge [
    source 92
    target 293
  ]
  edge [
    source 92
    target 705
  ]
  edge [
    source 92
    target 706
  ]
  edge [
    source 92
    target 707
  ]
  edge [
    source 92
    target 708
  ]
  edge [
    source 92
    target 709
  ]
  edge [
    source 92
    target 710
  ]
  edge [
    source 92
    target 462
  ]
  edge [
    source 92
    target 711
  ]
  edge [
    source 92
    target 712
  ]
  edge [
    source 92
    target 713
  ]
  edge [
    source 92
    target 714
  ]
  edge [
    source 92
    target 187
  ]
  edge [
    source 92
    target 715
  ]
  edge [
    source 92
    target 716
  ]
  edge [
    source 92
    target 717
  ]
  edge [
    source 92
    target 718
  ]
  edge [
    source 92
    target 617
  ]
  edge [
    source 93
    target 719
  ]
  edge [
    source 93
    target 720
  ]
  edge [
    source 94
    target 721
  ]
  edge [
    source 94
    target 722
  ]
  edge [
    source 94
    target 723
  ]
  edge [
    source 94
    target 724
  ]
  edge [
    source 94
    target 725
  ]
  edge [
    source 94
    target 726
  ]
  edge [
    source 94
    target 727
  ]
  edge [
    source 94
    target 728
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 103
  ]
  edge [
    source 96
    target 100
  ]
  edge [
    source 96
    target 729
  ]
  edge [
    source 97
    target 730
  ]
  edge [
    source 97
    target 654
  ]
  edge [
    source 97
    target 731
  ]
  edge [
    source 97
    target 656
  ]
  edge [
    source 97
    target 732
  ]
  edge [
    source 97
    target 733
  ]
  edge [
    source 97
    target 734
  ]
  edge [
    source 97
    target 735
  ]
  edge [
    source 97
    target 664
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 736
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 737
  ]
  edge [
    source 100
    target 738
  ]
  edge [
    source 100
    target 739
  ]
  edge [
    source 100
    target 740
  ]
  edge [
    source 100
    target 741
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 742
  ]
  edge [
    source 101
    target 338
  ]
  edge [
    source 101
    target 743
  ]
  edge [
    source 101
    target 744
  ]
  edge [
    source 101
    target 745
  ]
  edge [
    source 101
    target 746
  ]
  edge [
    source 101
    target 747
  ]
  edge [
    source 101
    target 748
  ]
  edge [
    source 101
    target 749
  ]
  edge [
    source 101
    target 750
  ]
  edge [
    source 101
    target 327
  ]
  edge [
    source 102
    target 403
  ]
  edge [
    source 102
    target 751
  ]
  edge [
    source 102
    target 752
  ]
  edge [
    source 102
    target 753
  ]
  edge [
    source 102
    target 754
  ]
  edge [
    source 102
    target 755
  ]
  edge [
    source 103
    target 756
  ]
  edge [
    source 103
    target 757
  ]
  edge [
    source 103
    target 758
  ]
  edge [
    source 103
    target 161
  ]
  edge [
    source 103
    target 759
  ]
  edge [
    source 103
    target 760
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 105
    target 761
  ]
  edge [
    source 105
    target 762
  ]
  edge [
    source 105
    target 390
  ]
  edge [
    source 105
    target 763
  ]
  edge [
    source 105
    target 764
  ]
  edge [
    source 105
    target 765
  ]
  edge [
    source 105
    target 766
  ]
  edge [
    source 105
    target 767
  ]
  edge [
    source 105
    target 768
  ]
  edge [
    source 105
    target 769
  ]
]
