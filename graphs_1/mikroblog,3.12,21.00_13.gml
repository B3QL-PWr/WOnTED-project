graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.894736842105263
  density 0.051209103840682786
  graphCliqueNumber 2
  node [
    id 0
    label "mirasy"
    origin "text"
  ]
  node [
    id 1
    label "jak"
    origin "text"
  ]
  node [
    id 2
    label "kurwa"
    origin "text"
  ]
  node [
    id 3
    label "akcja"
    origin "text"
  ]
  node [
    id 4
    label "byd&#322;o"
  ]
  node [
    id 5
    label "zobo"
  ]
  node [
    id 6
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 7
    label "yakalo"
  ]
  node [
    id 8
    label "dzo"
  ]
  node [
    id 9
    label "szmaciarz"
  ]
  node [
    id 10
    label "zo&#322;za"
  ]
  node [
    id 11
    label "skurwienie_si&#281;"
  ]
  node [
    id 12
    label "kurwienie_si&#281;"
  ]
  node [
    id 13
    label "karze&#322;"
  ]
  node [
    id 14
    label "kurcz&#281;"
  ]
  node [
    id 15
    label "wyzwisko"
  ]
  node [
    id 16
    label "przekle&#324;stwo"
  ]
  node [
    id 17
    label "prostytutka"
  ]
  node [
    id 18
    label "zagrywka"
  ]
  node [
    id 19
    label "czyn"
  ]
  node [
    id 20
    label "czynno&#347;&#263;"
  ]
  node [
    id 21
    label "wysoko&#347;&#263;"
  ]
  node [
    id 22
    label "stock"
  ]
  node [
    id 23
    label "gra"
  ]
  node [
    id 24
    label "w&#281;ze&#322;"
  ]
  node [
    id 25
    label "instrument_strunowy"
  ]
  node [
    id 26
    label "dywidenda"
  ]
  node [
    id 27
    label "przebieg"
  ]
  node [
    id 28
    label "udzia&#322;"
  ]
  node [
    id 29
    label "occupation"
  ]
  node [
    id 30
    label "jazda"
  ]
  node [
    id 31
    label "wydarzenie"
  ]
  node [
    id 32
    label "commotion"
  ]
  node [
    id 33
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 34
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 35
    label "operacja"
  ]
  node [
    id 36
    label "Bogumi&#322;"
  ]
  node [
    id 37
    label "ma&#347;lanka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 36
    target 37
  ]
]
