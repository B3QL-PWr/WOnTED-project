graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.7647058823529411
  density 0.11029411764705882
  graphCliqueNumber 2
  node [
    id 0
    label "kania"
    origin "text"
  ]
  node [
    id 1
    label "stacja"
    origin "text"
  ]
  node [
    id 2
    label "ptak_drapie&#380;ny"
  ]
  node [
    id 3
    label "saprotrof"
  ]
  node [
    id 4
    label "stroszka_strzelista"
  ]
  node [
    id 5
    label "myszo&#322;owy"
  ]
  node [
    id 6
    label "kite"
  ]
  node [
    id 7
    label "czubajka"
  ]
  node [
    id 8
    label "miejsce"
  ]
  node [
    id 9
    label "instytucja"
  ]
  node [
    id 10
    label "droga_krzy&#380;owa"
  ]
  node [
    id 11
    label "punkt"
  ]
  node [
    id 12
    label "urz&#261;dzenie"
  ]
  node [
    id 13
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 14
    label "siedziba"
  ]
  node [
    id 15
    label "rejowiec"
  ]
  node [
    id 16
    label "fabryczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 15
    target 16
  ]
]
