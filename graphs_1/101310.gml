graph [
  maxDegree 73
  minDegree 1
  meanDegree 2.044943820224719
  density 0.02323799795709908
  graphCliqueNumber 4
  node [
    id 0
    label "malediwy"
    origin "text"
  ]
  node [
    id 1
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 2
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 3
    label "lekkoatletyka"
    origin "text"
  ]
  node [
    id 4
    label "championship"
  ]
  node [
    id 5
    label "Formu&#322;a_1"
  ]
  node [
    id 6
    label "zawody"
  ]
  node [
    id 7
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 8
    label "obszar"
  ]
  node [
    id 9
    label "obiekt_naturalny"
  ]
  node [
    id 10
    label "przedmiot"
  ]
  node [
    id 11
    label "Stary_&#346;wiat"
  ]
  node [
    id 12
    label "grupa"
  ]
  node [
    id 13
    label "stw&#243;r"
  ]
  node [
    id 14
    label "biosfera"
  ]
  node [
    id 15
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 16
    label "rzecz"
  ]
  node [
    id 17
    label "magnetosfera"
  ]
  node [
    id 18
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 19
    label "environment"
  ]
  node [
    id 20
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 21
    label "geosfera"
  ]
  node [
    id 22
    label "Nowy_&#346;wiat"
  ]
  node [
    id 23
    label "planeta"
  ]
  node [
    id 24
    label "przejmowa&#263;"
  ]
  node [
    id 25
    label "litosfera"
  ]
  node [
    id 26
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 27
    label "makrokosmos"
  ]
  node [
    id 28
    label "barysfera"
  ]
  node [
    id 29
    label "biota"
  ]
  node [
    id 30
    label "p&#243;&#322;noc"
  ]
  node [
    id 31
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 32
    label "fauna"
  ]
  node [
    id 33
    label "wszechstworzenie"
  ]
  node [
    id 34
    label "geotermia"
  ]
  node [
    id 35
    label "biegun"
  ]
  node [
    id 36
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 37
    label "ekosystem"
  ]
  node [
    id 38
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 39
    label "teren"
  ]
  node [
    id 40
    label "zjawisko"
  ]
  node [
    id 41
    label "p&#243;&#322;kula"
  ]
  node [
    id 42
    label "atmosfera"
  ]
  node [
    id 43
    label "mikrokosmos"
  ]
  node [
    id 44
    label "class"
  ]
  node [
    id 45
    label "po&#322;udnie"
  ]
  node [
    id 46
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 47
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 48
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 49
    label "przejmowanie"
  ]
  node [
    id 50
    label "przestrze&#324;"
  ]
  node [
    id 51
    label "asymilowanie_si&#281;"
  ]
  node [
    id 52
    label "przej&#261;&#263;"
  ]
  node [
    id 53
    label "ekosfera"
  ]
  node [
    id 54
    label "przyroda"
  ]
  node [
    id 55
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 56
    label "ciemna_materia"
  ]
  node [
    id 57
    label "geoida"
  ]
  node [
    id 58
    label "Wsch&#243;d"
  ]
  node [
    id 59
    label "populace"
  ]
  node [
    id 60
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 61
    label "huczek"
  ]
  node [
    id 62
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 63
    label "Ziemia"
  ]
  node [
    id 64
    label "universe"
  ]
  node [
    id 65
    label "ozonosfera"
  ]
  node [
    id 66
    label "rze&#378;ba"
  ]
  node [
    id 67
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 68
    label "zagranica"
  ]
  node [
    id 69
    label "hydrosfera"
  ]
  node [
    id 70
    label "woda"
  ]
  node [
    id 71
    label "kuchnia"
  ]
  node [
    id 72
    label "przej&#281;cie"
  ]
  node [
    id 73
    label "czarna_dziura"
  ]
  node [
    id 74
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 75
    label "morze"
  ]
  node [
    id 76
    label "sport"
  ]
  node [
    id 77
    label "tr&#243;jskok"
  ]
  node [
    id 78
    label "dziesi&#281;ciob&#243;j"
  ]
  node [
    id 79
    label "skok_o_tyczce"
  ]
  node [
    id 80
    label "skok_wzwy&#380;"
  ]
  node [
    id 81
    label "rzut_oszczepem"
  ]
  node [
    id 82
    label "bieg"
  ]
  node [
    id 83
    label "skok_w_dal"
  ]
  node [
    id 84
    label "ch&#243;d"
  ]
  node [
    id 85
    label "pchni&#281;cie_kul&#261;"
  ]
  node [
    id 86
    label "rzut_m&#322;otem"
  ]
  node [
    id 87
    label "mistrzostwo"
  ]
  node [
    id 88
    label "wyspa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 87
    target 88
  ]
]
