graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.01868131868131868
  graphCliqueNumber 2
  node [
    id 0
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "godzina"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "polski"
    origin "text"
  ]
  node [
    id 4
    label "spacex"
    origin "text"
  ]
  node [
    id 5
    label "zaplanowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "misja"
    origin "text"
  ]
  node [
    id 8
    label "sso"
    origin "text"
  ]
  node [
    id 9
    label "slc"
    origin "text"
  ]
  node [
    id 10
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 11
    label "miesi&#261;c"
  ]
  node [
    id 12
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 13
    label "Barb&#243;rka"
  ]
  node [
    id 14
    label "Sylwester"
  ]
  node [
    id 15
    label "minuta"
  ]
  node [
    id 16
    label "doba"
  ]
  node [
    id 17
    label "p&#243;&#322;godzina"
  ]
  node [
    id 18
    label "kwadrans"
  ]
  node [
    id 19
    label "time"
  ]
  node [
    id 20
    label "jednostka_czasu"
  ]
  node [
    id 21
    label "czasokres"
  ]
  node [
    id 22
    label "trawienie"
  ]
  node [
    id 23
    label "kategoria_gramatyczna"
  ]
  node [
    id 24
    label "period"
  ]
  node [
    id 25
    label "odczyt"
  ]
  node [
    id 26
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 27
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 28
    label "chwila"
  ]
  node [
    id 29
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 30
    label "poprzedzenie"
  ]
  node [
    id 31
    label "koniugacja"
  ]
  node [
    id 32
    label "dzieje"
  ]
  node [
    id 33
    label "poprzedzi&#263;"
  ]
  node [
    id 34
    label "przep&#322;ywanie"
  ]
  node [
    id 35
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 36
    label "odwlekanie_si&#281;"
  ]
  node [
    id 37
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 38
    label "Zeitgeist"
  ]
  node [
    id 39
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 40
    label "okres_czasu"
  ]
  node [
    id 41
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 42
    label "pochodzi&#263;"
  ]
  node [
    id 43
    label "schy&#322;ek"
  ]
  node [
    id 44
    label "czwarty_wymiar"
  ]
  node [
    id 45
    label "chronometria"
  ]
  node [
    id 46
    label "poprzedzanie"
  ]
  node [
    id 47
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 48
    label "pogoda"
  ]
  node [
    id 49
    label "zegar"
  ]
  node [
    id 50
    label "trawi&#263;"
  ]
  node [
    id 51
    label "pochodzenie"
  ]
  node [
    id 52
    label "poprzedza&#263;"
  ]
  node [
    id 53
    label "time_period"
  ]
  node [
    id 54
    label "rachuba_czasu"
  ]
  node [
    id 55
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 56
    label "czasoprzestrze&#324;"
  ]
  node [
    id 57
    label "laba"
  ]
  node [
    id 58
    label "lacki"
  ]
  node [
    id 59
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 60
    label "przedmiot"
  ]
  node [
    id 61
    label "sztajer"
  ]
  node [
    id 62
    label "drabant"
  ]
  node [
    id 63
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 64
    label "polak"
  ]
  node [
    id 65
    label "pierogi_ruskie"
  ]
  node [
    id 66
    label "krakowiak"
  ]
  node [
    id 67
    label "Polish"
  ]
  node [
    id 68
    label "j&#281;zyk"
  ]
  node [
    id 69
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 70
    label "oberek"
  ]
  node [
    id 71
    label "po_polsku"
  ]
  node [
    id 72
    label "mazur"
  ]
  node [
    id 73
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 74
    label "chodzony"
  ]
  node [
    id 75
    label "skoczny"
  ]
  node [
    id 76
    label "ryba_po_grecku"
  ]
  node [
    id 77
    label "goniony"
  ]
  node [
    id 78
    label "polsko"
  ]
  node [
    id 79
    label "pomy&#347;le&#263;"
  ]
  node [
    id 80
    label "opracowa&#263;"
  ]
  node [
    id 81
    label "line_up"
  ]
  node [
    id 82
    label "zrobi&#263;"
  ]
  node [
    id 83
    label "przemy&#347;le&#263;"
  ]
  node [
    id 84
    label "map"
  ]
  node [
    id 85
    label "pom&#243;c"
  ]
  node [
    id 86
    label "zbudowa&#263;"
  ]
  node [
    id 87
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 88
    label "leave"
  ]
  node [
    id 89
    label "przewie&#347;&#263;"
  ]
  node [
    id 90
    label "wykona&#263;"
  ]
  node [
    id 91
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 92
    label "draw"
  ]
  node [
    id 93
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 94
    label "carry"
  ]
  node [
    id 95
    label "plac&#243;wka"
  ]
  node [
    id 96
    label "misje"
  ]
  node [
    id 97
    label "zadanie"
  ]
  node [
    id 98
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 99
    label "obowi&#261;zek"
  ]
  node [
    id 100
    label "reprezentacja"
  ]
  node [
    id 101
    label "PW"
  ]
  node [
    id 102
    label "Sat2"
  ]
  node [
    id 103
    label "Mister"
  ]
  node [
    id 104
    label "Steven"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 103
    target 104
  ]
]
