graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.894736842105263
  density 0.051209103840682786
  graphCliqueNumber 2
  node [
    id 0
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 1
    label "archiwalny"
    origin "text"
  ]
  node [
    id 2
    label "publikowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "podczas"
    origin "text"
  ]
  node [
    id 4
    label "pierwsza"
    origin "text"
  ]
  node [
    id 5
    label "proba"
    origin "text"
  ]
  node [
    id 6
    label "tworzenie"
    origin "text"
  ]
  node [
    id 7
    label "obiektywu"
    origin "text"
  ]
  node [
    id 8
    label "dokument"
  ]
  node [
    id 9
    label "towar"
  ]
  node [
    id 10
    label "nag&#322;&#243;wek"
  ]
  node [
    id 11
    label "znak_j&#281;zykowy"
  ]
  node [
    id 12
    label "wyr&#243;b"
  ]
  node [
    id 13
    label "blok"
  ]
  node [
    id 14
    label "line"
  ]
  node [
    id 15
    label "paragraf"
  ]
  node [
    id 16
    label "rodzajnik"
  ]
  node [
    id 17
    label "prawda"
  ]
  node [
    id 18
    label "szkic"
  ]
  node [
    id 19
    label "tekst"
  ]
  node [
    id 20
    label "fragment"
  ]
  node [
    id 21
    label "cenny"
  ]
  node [
    id 22
    label "archival"
  ]
  node [
    id 23
    label "wydawnictwo"
  ]
  node [
    id 24
    label "wprowadza&#263;"
  ]
  node [
    id 25
    label "upublicznia&#263;"
  ]
  node [
    id 26
    label "give"
  ]
  node [
    id 27
    label "godzina"
  ]
  node [
    id 28
    label "robienie"
  ]
  node [
    id 29
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 30
    label "pope&#322;nianie"
  ]
  node [
    id 31
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 32
    label "development"
  ]
  node [
    id 33
    label "stanowienie"
  ]
  node [
    id 34
    label "exploitation"
  ]
  node [
    id 35
    label "structure"
  ]
  node [
    id 36
    label "wyspa"
  ]
  node [
    id 37
    label "brytania"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 36
    target 37
  ]
]
