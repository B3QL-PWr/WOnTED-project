graph [
  maxDegree 24
  minDegree 1
  meanDegree 2.05
  density 0.025949367088607594
  graphCliqueNumber 4
  node [
    id 0
    label "matematyk"
    origin "text"
  ]
  node [
    id 1
    label "magia"
    origin "text"
  ]
  node [
    id 2
    label "nauka"
    origin "text"
  ]
  node [
    id 3
    label "kontynuowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wietny"
    origin "text"
  ]
  node [
    id 5
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 6
    label "klopsztanga"
    origin "text"
  ]
  node [
    id 7
    label "Gauss"
  ]
  node [
    id 8
    label "Euklides"
  ]
  node [
    id 9
    label "Berkeley"
  ]
  node [
    id 10
    label "Ptolemeusz"
  ]
  node [
    id 11
    label "Doppler"
  ]
  node [
    id 12
    label "Archimedes"
  ]
  node [
    id 13
    label "Maxwell"
  ]
  node [
    id 14
    label "Bayes"
  ]
  node [
    id 15
    label "Pascal"
  ]
  node [
    id 16
    label "Borel"
  ]
  node [
    id 17
    label "Newton"
  ]
  node [
    id 18
    label "Kartezjusz"
  ]
  node [
    id 19
    label "Kepler"
  ]
  node [
    id 20
    label "naukowiec"
  ]
  node [
    id 21
    label "nauczyciel"
  ]
  node [
    id 22
    label "Fourier"
  ]
  node [
    id 23
    label "Pitagoras"
  ]
  node [
    id 24
    label "Galileusz"
  ]
  node [
    id 25
    label "Biot"
  ]
  node [
    id 26
    label "Laplace"
  ]
  node [
    id 27
    label "praktyki"
  ]
  node [
    id 28
    label "czar"
  ]
  node [
    id 29
    label "wikkanin"
  ]
  node [
    id 30
    label "czarodziejka"
  ]
  node [
    id 31
    label "cecha"
  ]
  node [
    id 32
    label "niecodzienno&#347;&#263;"
  ]
  node [
    id 33
    label "czarownica"
  ]
  node [
    id 34
    label "czarodziej"
  ]
  node [
    id 35
    label "zjawisko"
  ]
  node [
    id 36
    label "agreeableness"
  ]
  node [
    id 37
    label "nauki_o_Ziemi"
  ]
  node [
    id 38
    label "teoria_naukowa"
  ]
  node [
    id 39
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 40
    label "nauki_o_poznaniu"
  ]
  node [
    id 41
    label "nomotetyczny"
  ]
  node [
    id 42
    label "metodologia"
  ]
  node [
    id 43
    label "przem&#243;wienie"
  ]
  node [
    id 44
    label "wiedza"
  ]
  node [
    id 45
    label "kultura_duchowa"
  ]
  node [
    id 46
    label "nauki_penalne"
  ]
  node [
    id 47
    label "systematyka"
  ]
  node [
    id 48
    label "inwentyka"
  ]
  node [
    id 49
    label "dziedzina"
  ]
  node [
    id 50
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 51
    label "miasteczko_rowerowe"
  ]
  node [
    id 52
    label "fotowoltaika"
  ]
  node [
    id 53
    label "porada"
  ]
  node [
    id 54
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 55
    label "proces"
  ]
  node [
    id 56
    label "imagineskopia"
  ]
  node [
    id 57
    label "typologia"
  ]
  node [
    id 58
    label "&#322;awa_szkolna"
  ]
  node [
    id 59
    label "robi&#263;"
  ]
  node [
    id 60
    label "prosecute"
  ]
  node [
    id 61
    label "pomy&#347;lny"
  ]
  node [
    id 62
    label "pozytywny"
  ]
  node [
    id 63
    label "wspaniale"
  ]
  node [
    id 64
    label "dobry"
  ]
  node [
    id 65
    label "superancki"
  ]
  node [
    id 66
    label "arcydzielny"
  ]
  node [
    id 67
    label "zajebisty"
  ]
  node [
    id 68
    label "wa&#380;ny"
  ]
  node [
    id 69
    label "&#347;wietnie"
  ]
  node [
    id 70
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 71
    label "skuteczny"
  ]
  node [
    id 72
    label "spania&#322;y"
  ]
  node [
    id 73
    label "system"
  ]
  node [
    id 74
    label "wytw&#243;r"
  ]
  node [
    id 75
    label "idea"
  ]
  node [
    id 76
    label "ukra&#347;&#263;"
  ]
  node [
    id 77
    label "ukradzenie"
  ]
  node [
    id 78
    label "pocz&#261;tki"
  ]
  node [
    id 79
    label "Klopsztanga"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
]
