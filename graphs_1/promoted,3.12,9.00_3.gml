graph [
  maxDegree 24
  minDegree 1
  meanDegree 2
  density 0.01834862385321101
  graphCliqueNumber 3
  node [
    id 0
    label "nieszczepienie"
    origin "text"
  ]
  node [
    id 1
    label "dziecko"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "sprawa"
    origin "text"
  ]
  node [
    id 4
    label "prywatny"
    origin "text"
  ]
  node [
    id 5
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 6
    label "fi&#324;ski"
    origin "text"
  ]
  node [
    id 7
    label "minister"
    origin "text"
  ]
  node [
    id 8
    label "nauka"
    origin "text"
  ]
  node [
    id 9
    label "szkolnictwo"
    origin "text"
  ]
  node [
    id 10
    label "sanny"
    origin "text"
  ]
  node [
    id 11
    label "grahn"
    origin "text"
  ]
  node [
    id 12
    label "laasonen"
    origin "text"
  ]
  node [
    id 13
    label "cz&#322;owiek"
  ]
  node [
    id 14
    label "potomstwo"
  ]
  node [
    id 15
    label "organizm"
  ]
  node [
    id 16
    label "sraluch"
  ]
  node [
    id 17
    label "utulanie"
  ]
  node [
    id 18
    label "pediatra"
  ]
  node [
    id 19
    label "dzieciarnia"
  ]
  node [
    id 20
    label "m&#322;odziak"
  ]
  node [
    id 21
    label "dzieciak"
  ]
  node [
    id 22
    label "utula&#263;"
  ]
  node [
    id 23
    label "potomek"
  ]
  node [
    id 24
    label "pedofil"
  ]
  node [
    id 25
    label "entliczek-pentliczek"
  ]
  node [
    id 26
    label "m&#322;odzik"
  ]
  node [
    id 27
    label "cz&#322;owieczek"
  ]
  node [
    id 28
    label "zwierz&#281;"
  ]
  node [
    id 29
    label "niepe&#322;noletni"
  ]
  node [
    id 30
    label "fledgling"
  ]
  node [
    id 31
    label "utuli&#263;"
  ]
  node [
    id 32
    label "utulenie"
  ]
  node [
    id 33
    label "si&#281;ga&#263;"
  ]
  node [
    id 34
    label "trwa&#263;"
  ]
  node [
    id 35
    label "obecno&#347;&#263;"
  ]
  node [
    id 36
    label "stan"
  ]
  node [
    id 37
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 38
    label "stand"
  ]
  node [
    id 39
    label "mie&#263;_miejsce"
  ]
  node [
    id 40
    label "uczestniczy&#263;"
  ]
  node [
    id 41
    label "chodzi&#263;"
  ]
  node [
    id 42
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 43
    label "equal"
  ]
  node [
    id 44
    label "temat"
  ]
  node [
    id 45
    label "kognicja"
  ]
  node [
    id 46
    label "idea"
  ]
  node [
    id 47
    label "szczeg&#243;&#322;"
  ]
  node [
    id 48
    label "rzecz"
  ]
  node [
    id 49
    label "wydarzenie"
  ]
  node [
    id 50
    label "przes&#322;anka"
  ]
  node [
    id 51
    label "rozprawa"
  ]
  node [
    id 52
    label "object"
  ]
  node [
    id 53
    label "proposition"
  ]
  node [
    id 54
    label "czyj&#347;"
  ]
  node [
    id 55
    label "nieformalny"
  ]
  node [
    id 56
    label "personalny"
  ]
  node [
    id 57
    label "w&#322;asny"
  ]
  node [
    id 58
    label "prywatnie"
  ]
  node [
    id 59
    label "niepubliczny"
  ]
  node [
    id 60
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 61
    label "express"
  ]
  node [
    id 62
    label "rzekn&#261;&#263;"
  ]
  node [
    id 63
    label "okre&#347;li&#263;"
  ]
  node [
    id 64
    label "wyrazi&#263;"
  ]
  node [
    id 65
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 66
    label "unwrap"
  ]
  node [
    id 67
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 68
    label "convey"
  ]
  node [
    id 69
    label "discover"
  ]
  node [
    id 70
    label "wydoby&#263;"
  ]
  node [
    id 71
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 72
    label "poda&#263;"
  ]
  node [
    id 73
    label "j&#281;zyki_ba&#322;tyckofi&#324;skie"
  ]
  node [
    id 74
    label "pier&#243;g_karelski"
  ]
  node [
    id 75
    label "j&#281;zyk_ugrofi&#324;ski"
  ]
  node [
    id 76
    label "po_fi&#324;sku"
  ]
  node [
    id 77
    label "j&#281;zyk"
  ]
  node [
    id 78
    label "skandynawski"
  ]
  node [
    id 79
    label "Goebbels"
  ]
  node [
    id 80
    label "Sto&#322;ypin"
  ]
  node [
    id 81
    label "rz&#261;d"
  ]
  node [
    id 82
    label "dostojnik"
  ]
  node [
    id 83
    label "nauki_o_Ziemi"
  ]
  node [
    id 84
    label "teoria_naukowa"
  ]
  node [
    id 85
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 86
    label "nauki_o_poznaniu"
  ]
  node [
    id 87
    label "nomotetyczny"
  ]
  node [
    id 88
    label "metodologia"
  ]
  node [
    id 89
    label "przem&#243;wienie"
  ]
  node [
    id 90
    label "wiedza"
  ]
  node [
    id 91
    label "kultura_duchowa"
  ]
  node [
    id 92
    label "nauki_penalne"
  ]
  node [
    id 93
    label "systematyka"
  ]
  node [
    id 94
    label "inwentyka"
  ]
  node [
    id 95
    label "dziedzina"
  ]
  node [
    id 96
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 97
    label "miasteczko_rowerowe"
  ]
  node [
    id 98
    label "fotowoltaika"
  ]
  node [
    id 99
    label "porada"
  ]
  node [
    id 100
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 101
    label "proces"
  ]
  node [
    id 102
    label "imagineskopia"
  ]
  node [
    id 103
    label "typologia"
  ]
  node [
    id 104
    label "&#322;awa_szkolna"
  ]
  node [
    id 105
    label "gospodarka"
  ]
  node [
    id 106
    label "program_nauczania"
  ]
  node [
    id 107
    label "Karta_Nauczyciela"
  ]
  node [
    id 108
    label "Grahn"
  ]
  node [
    id 109
    label "Laasonen"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 108
    target 109
  ]
]
