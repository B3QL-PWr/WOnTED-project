graph [
  maxDegree 50
  minDegree 1
  meanDegree 6.846715328467154
  density 0.05034349506225848
  graphCliqueNumber 27
  node [
    id 0
    label "konsultacja"
    origin "text"
  ]
  node [
    id 1
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 2
    label "opracowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "projekt"
    origin "text"
  ]
  node [
    id 4
    label "ochrona"
    origin "text"
  ]
  node [
    id 5
    label "powietrze"
    origin "text"
  ]
  node [
    id 6
    label "dla"
    origin "text"
  ]
  node [
    id 7
    label "cztery"
    origin "text"
  ]
  node [
    id 8
    label "strefa"
    origin "text"
  ]
  node [
    id 9
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 10
    label "zachodniopomorskiego"
    origin "text"
  ]
  node [
    id 11
    label "szczeci&#324;ski"
    origin "text"
  ]
  node [
    id 12
    label "gryfi&#324;ski"
    origin "text"
  ]
  node [
    id 13
    label "oraz"
    origin "text"
  ]
  node [
    id 14
    label "szczecinecki"
    origin "text"
  ]
  node [
    id 15
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 16
    label "stwierdzenie"
    origin "text"
  ]
  node [
    id 17
    label "przekroczenie"
    origin "text"
  ]
  node [
    id 18
    label "poziom"
    origin "text"
  ]
  node [
    id 19
    label "docelowy"
    origin "text"
  ]
  node [
    id 20
    label "okre&#347;lony"
    origin "text"
  ]
  node [
    id 21
    label "benzo"
    origin "text"
  ]
  node [
    id 22
    label "ocena"
  ]
  node [
    id 23
    label "narada"
  ]
  node [
    id 24
    label "porada"
  ]
  node [
    id 25
    label "niepubliczny"
  ]
  node [
    id 26
    label "spo&#322;ecznie"
  ]
  node [
    id 27
    label "publiczny"
  ]
  node [
    id 28
    label "invent"
  ]
  node [
    id 29
    label "przygotowa&#263;"
  ]
  node [
    id 30
    label "dokument"
  ]
  node [
    id 31
    label "device"
  ]
  node [
    id 32
    label "program_u&#380;ytkowy"
  ]
  node [
    id 33
    label "intencja"
  ]
  node [
    id 34
    label "agreement"
  ]
  node [
    id 35
    label "pomys&#322;"
  ]
  node [
    id 36
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 37
    label "plan"
  ]
  node [
    id 38
    label "dokumentacja"
  ]
  node [
    id 39
    label "chemical_bond"
  ]
  node [
    id 40
    label "tarcza"
  ]
  node [
    id 41
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 42
    label "obiekt"
  ]
  node [
    id 43
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 44
    label "borowiec"
  ]
  node [
    id 45
    label "obstawienie"
  ]
  node [
    id 46
    label "formacja"
  ]
  node [
    id 47
    label "ubezpieczenie"
  ]
  node [
    id 48
    label "obstawia&#263;"
  ]
  node [
    id 49
    label "obstawianie"
  ]
  node [
    id 50
    label "transportacja"
  ]
  node [
    id 51
    label "mieszanina"
  ]
  node [
    id 52
    label "przewietrzy&#263;"
  ]
  node [
    id 53
    label "przewietrza&#263;"
  ]
  node [
    id 54
    label "tlen"
  ]
  node [
    id 55
    label "eter"
  ]
  node [
    id 56
    label "dmucha&#263;"
  ]
  node [
    id 57
    label "dmuchanie"
  ]
  node [
    id 58
    label "breeze"
  ]
  node [
    id 59
    label "pojazd"
  ]
  node [
    id 60
    label "pneumatyczny"
  ]
  node [
    id 61
    label "wydychanie"
  ]
  node [
    id 62
    label "podgrzew"
  ]
  node [
    id 63
    label "wdychanie"
  ]
  node [
    id 64
    label "luft"
  ]
  node [
    id 65
    label "geosystem"
  ]
  node [
    id 66
    label "dmuchn&#261;&#263;"
  ]
  node [
    id 67
    label "dmuchni&#281;cie"
  ]
  node [
    id 68
    label "&#380;ywio&#322;"
  ]
  node [
    id 69
    label "wdycha&#263;"
  ]
  node [
    id 70
    label "wydycha&#263;"
  ]
  node [
    id 71
    label "napowietrzy&#263;"
  ]
  node [
    id 72
    label "front"
  ]
  node [
    id 73
    label "przewietrzenie"
  ]
  node [
    id 74
    label "przewietrzanie"
  ]
  node [
    id 75
    label "obszar"
  ]
  node [
    id 76
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 77
    label "obrona_strefowa"
  ]
  node [
    id 78
    label "jednostka_administracyjna"
  ]
  node [
    id 79
    label "makroregion"
  ]
  node [
    id 80
    label "powiat"
  ]
  node [
    id 81
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 82
    label "mikroregion"
  ]
  node [
    id 83
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 84
    label "pa&#324;stwo"
  ]
  node [
    id 85
    label "po_szczeci&#324;sku"
  ]
  node [
    id 86
    label "pomorski"
  ]
  node [
    id 87
    label "przyczyna"
  ]
  node [
    id 88
    label "uwaga"
  ]
  node [
    id 89
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 90
    label "punkt_widzenia"
  ]
  node [
    id 91
    label "wypowied&#378;"
  ]
  node [
    id 92
    label "ustalenie"
  ]
  node [
    id 93
    label "claim"
  ]
  node [
    id 94
    label "statement"
  ]
  node [
    id 95
    label "oznajmienie"
  ]
  node [
    id 96
    label "transgression"
  ]
  node [
    id 97
    label "zrobienie"
  ]
  node [
    id 98
    label "transgresja"
  ]
  node [
    id 99
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 100
    label "offense"
  ]
  node [
    id 101
    label "przebycie"
  ]
  node [
    id 102
    label "mini&#281;cie"
  ]
  node [
    id 103
    label "przepuszczenie"
  ]
  node [
    id 104
    label "discourtesy"
  ]
  node [
    id 105
    label "ograniczenie"
  ]
  node [
    id 106
    label "emergence"
  ]
  node [
    id 107
    label "wysoko&#347;&#263;"
  ]
  node [
    id 108
    label "faza"
  ]
  node [
    id 109
    label "szczebel"
  ]
  node [
    id 110
    label "po&#322;o&#380;enie"
  ]
  node [
    id 111
    label "kierunek"
  ]
  node [
    id 112
    label "wyk&#322;adnik"
  ]
  node [
    id 113
    label "budynek"
  ]
  node [
    id 114
    label "jako&#347;&#263;"
  ]
  node [
    id 115
    label "ranga"
  ]
  node [
    id 116
    label "p&#322;aszczyzna"
  ]
  node [
    id 117
    label "ostatni"
  ]
  node [
    id 118
    label "bezpo&#347;redni"
  ]
  node [
    id 119
    label "ostateczny"
  ]
  node [
    id 120
    label "docelowo"
  ]
  node [
    id 121
    label "wiadomy"
  ]
  node [
    id 122
    label "program"
  ]
  node [
    id 123
    label "strefi&#263;"
  ]
  node [
    id 124
    label "aglomeracja"
  ]
  node [
    id 125
    label "m&#281;ski"
  ]
  node [
    id 126
    label "Koszalin"
  ]
  node [
    id 127
    label "z"
  ]
  node [
    id 128
    label "na"
  ]
  node [
    id 129
    label "stwierdzi&#263;"
  ]
  node [
    id 130
    label "przekroczy&#263;"
  ]
  node [
    id 131
    label "albo"
  ]
  node [
    id 132
    label "pirenu"
  ]
  node [
    id 133
    label "prawo"
  ]
  node [
    id 134
    label "&#347;rodowisko"
  ]
  node [
    id 135
    label "dziennik"
  ]
  node [
    id 136
    label "u"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 6
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 21
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 91
  ]
  edge [
    source 16
    target 92
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 97
  ]
  edge [
    source 17
    target 98
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 17
    target 106
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 107
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 90
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 76
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 80
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 80
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 80
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 21
    target 122
  ]
  edge [
    source 21
    target 123
  ]
  edge [
    source 21
    target 124
  ]
  edge [
    source 21
    target 125
  ]
  edge [
    source 21
    target 126
  ]
  edge [
    source 21
    target 80
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 21
    target 129
  ]
  edge [
    source 21
    target 130
  ]
  edge [
    source 21
    target 131
  ]
  edge [
    source 21
    target 132
  ]
  edge [
    source 80
    target 122
  ]
  edge [
    source 80
    target 123
  ]
  edge [
    source 80
    target 124
  ]
  edge [
    source 80
    target 125
  ]
  edge [
    source 80
    target 126
  ]
  edge [
    source 80
    target 80
  ]
  edge [
    source 80
    target 127
  ]
  edge [
    source 80
    target 128
  ]
  edge [
    source 80
    target 129
  ]
  edge [
    source 80
    target 130
  ]
  edge [
    source 80
    target 131
  ]
  edge [
    source 80
    target 132
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 124
  ]
  edge [
    source 122
    target 125
  ]
  edge [
    source 122
    target 126
  ]
  edge [
    source 122
    target 127
  ]
  edge [
    source 122
    target 128
  ]
  edge [
    source 122
    target 129
  ]
  edge [
    source 122
    target 130
  ]
  edge [
    source 122
    target 131
  ]
  edge [
    source 122
    target 132
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 125
  ]
  edge [
    source 123
    target 126
  ]
  edge [
    source 123
    target 127
  ]
  edge [
    source 123
    target 128
  ]
  edge [
    source 123
    target 129
  ]
  edge [
    source 123
    target 130
  ]
  edge [
    source 123
    target 131
  ]
  edge [
    source 123
    target 132
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 126
  ]
  edge [
    source 124
    target 127
  ]
  edge [
    source 124
    target 128
  ]
  edge [
    source 124
    target 129
  ]
  edge [
    source 124
    target 130
  ]
  edge [
    source 124
    target 131
  ]
  edge [
    source 124
    target 132
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 127
  ]
  edge [
    source 125
    target 128
  ]
  edge [
    source 125
    target 129
  ]
  edge [
    source 125
    target 130
  ]
  edge [
    source 125
    target 131
  ]
  edge [
    source 125
    target 132
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 128
  ]
  edge [
    source 126
    target 129
  ]
  edge [
    source 126
    target 130
  ]
  edge [
    source 126
    target 131
  ]
  edge [
    source 126
    target 132
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 129
  ]
  edge [
    source 127
    target 130
  ]
  edge [
    source 127
    target 131
  ]
  edge [
    source 127
    target 132
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 130
  ]
  edge [
    source 128
    target 131
  ]
  edge [
    source 128
    target 132
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 131
  ]
  edge [
    source 129
    target 132
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 130
    target 132
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 135
    target 136
  ]
]
