graph [
  maxDegree 70
  minDegree 1
  meanDegree 2.2452107279693485
  density 0.008635425876805188
  graphCliqueNumber 5
  node [
    id 0
    label "przy"
    origin "text"
  ]
  node [
    id 1
    label "ula"
    origin "text"
  ]
  node [
    id 2
    label "tuwim"
    origin "text"
  ]
  node [
    id 3
    label "wznie&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "gmach"
    origin "text"
  ]
  node [
    id 5
    label "poczet"
    origin "text"
  ]
  node [
    id 6
    label "g&#322;&#243;wny"
    origin "text"
  ]
  node [
    id 7
    label "rok"
    origin "text"
  ]
  node [
    id 8
    label "narutowicz"
    origin "text"
  ]
  node [
    id 9
    label "ekskluzywny"
    origin "text"
  ]
  node [
    id 10
    label "w&#243;wczas"
    origin "text"
  ]
  node [
    id 11
    label "hotel"
    origin "text"
  ]
  node [
    id 12
    label "polonia"
    origin "text"
  ]
  node [
    id 13
    label "naprzeciw"
    origin "text"
  ]
  node [
    id 14
    label "dworzec"
    origin "text"
  ]
  node [
    id 15
    label "kolejowy"
    origin "text"
  ]
  node [
    id 16
    label "powsta&#263;"
    origin "text"
  ]
  node [
    id 17
    label "okres"
    origin "text"
  ]
  node [
    id 18
    label "mi&#281;dzywojenny"
    origin "text"
  ]
  node [
    id 19
    label "dom"
    origin "text"
  ]
  node [
    id 20
    label "pomnik"
    origin "text"
  ]
  node [
    id 21
    label "j&#243;zef"
    origin "text"
  ]
  node [
    id 22
    label "pi&#322;sudski"
    origin "text"
  ]
  node [
    id 23
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 24
    label "&#322;&#243;dzki"
    origin "text"
  ]
  node [
    id 25
    label "kultura"
    origin "text"
  ]
  node [
    id 26
    label "establish"
  ]
  node [
    id 27
    label "pos&#322;a&#263;"
  ]
  node [
    id 28
    label "podnie&#347;&#263;"
  ]
  node [
    id 29
    label "wytworzy&#263;"
  ]
  node [
    id 30
    label "raise"
  ]
  node [
    id 31
    label "budowla"
  ]
  node [
    id 32
    label "building"
  ]
  node [
    id 33
    label "budynek"
  ]
  node [
    id 34
    label "formacja"
  ]
  node [
    id 35
    label "zesp&#243;&#322;"
  ]
  node [
    id 36
    label "number"
  ]
  node [
    id 37
    label "zbi&#243;r"
  ]
  node [
    id 38
    label "rank_and_file"
  ]
  node [
    id 39
    label "najwa&#380;niejszy"
  ]
  node [
    id 40
    label "g&#322;&#243;wnie"
  ]
  node [
    id 41
    label "stulecie"
  ]
  node [
    id 42
    label "kalendarz"
  ]
  node [
    id 43
    label "czas"
  ]
  node [
    id 44
    label "pora_roku"
  ]
  node [
    id 45
    label "cykl_astronomiczny"
  ]
  node [
    id 46
    label "p&#243;&#322;rocze"
  ]
  node [
    id 47
    label "grupa"
  ]
  node [
    id 48
    label "kwarta&#322;"
  ]
  node [
    id 49
    label "kurs"
  ]
  node [
    id 50
    label "jubileusz"
  ]
  node [
    id 51
    label "miesi&#261;c"
  ]
  node [
    id 52
    label "lata"
  ]
  node [
    id 53
    label "martwy_sezon"
  ]
  node [
    id 54
    label "wyszukany"
  ]
  node [
    id 55
    label "elitarny"
  ]
  node [
    id 56
    label "wymagaj&#261;cy"
  ]
  node [
    id 57
    label "galantyna"
  ]
  node [
    id 58
    label "luksusowo"
  ]
  node [
    id 59
    label "wyj&#261;tkowy"
  ]
  node [
    id 60
    label "zamkni&#281;ty"
  ]
  node [
    id 61
    label "wspania&#322;y"
  ]
  node [
    id 62
    label "ekskluzywnie"
  ]
  node [
    id 63
    label "drogi"
  ]
  node [
    id 64
    label "w&#261;ski"
  ]
  node [
    id 65
    label "na&#243;wczas"
  ]
  node [
    id 66
    label "wtedy"
  ]
  node [
    id 67
    label "recepcja"
  ]
  node [
    id 68
    label "go&#347;&#263;"
  ]
  node [
    id 69
    label "budynek_zamieszkania_zbiorowego"
  ]
  node [
    id 70
    label "restauracja"
  ]
  node [
    id 71
    label "nocleg"
  ]
  node [
    id 72
    label "numer"
  ]
  node [
    id 73
    label "z_naprzeciwka"
  ]
  node [
    id 74
    label "wprost"
  ]
  node [
    id 75
    label "po_przeciwnej_stronie"
  ]
  node [
    id 76
    label "przechowalnia"
  ]
  node [
    id 77
    label "hala"
  ]
  node [
    id 78
    label "peron"
  ]
  node [
    id 79
    label "poczekalnia"
  ]
  node [
    id 80
    label "stacja"
  ]
  node [
    id 81
    label "majdaniarz"
  ]
  node [
    id 82
    label "komunikacyjny"
  ]
  node [
    id 83
    label "zaistnie&#263;"
  ]
  node [
    id 84
    label "sprzeciwi&#263;_si&#281;"
  ]
  node [
    id 85
    label "originate"
  ]
  node [
    id 86
    label "rise"
  ]
  node [
    id 87
    label "zbuntowa&#263;_si&#281;"
  ]
  node [
    id 88
    label "mount"
  ]
  node [
    id 89
    label "stan&#261;&#263;"
  ]
  node [
    id 90
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 91
    label "kuca&#263;"
  ]
  node [
    id 92
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 93
    label "paleogen"
  ]
  node [
    id 94
    label "spell"
  ]
  node [
    id 95
    label "period"
  ]
  node [
    id 96
    label "prekambr"
  ]
  node [
    id 97
    label "jura"
  ]
  node [
    id 98
    label "interstadia&#322;"
  ]
  node [
    id 99
    label "jednostka_geologiczna"
  ]
  node [
    id 100
    label "izochronizm"
  ]
  node [
    id 101
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 102
    label "okres_noachijski"
  ]
  node [
    id 103
    label "orosir"
  ]
  node [
    id 104
    label "sten"
  ]
  node [
    id 105
    label "kreda"
  ]
  node [
    id 106
    label "drugorz&#281;d"
  ]
  node [
    id 107
    label "semester"
  ]
  node [
    id 108
    label "trzeciorz&#281;d"
  ]
  node [
    id 109
    label "ton"
  ]
  node [
    id 110
    label "dzieje"
  ]
  node [
    id 111
    label "poprzednik"
  ]
  node [
    id 112
    label "kalim"
  ]
  node [
    id 113
    label "ordowik"
  ]
  node [
    id 114
    label "karbon"
  ]
  node [
    id 115
    label "trias"
  ]
  node [
    id 116
    label "stater"
  ]
  node [
    id 117
    label "era"
  ]
  node [
    id 118
    label "cykl"
  ]
  node [
    id 119
    label "p&#243;&#322;okres"
  ]
  node [
    id 120
    label "czwartorz&#281;d"
  ]
  node [
    id 121
    label "pulsacja"
  ]
  node [
    id 122
    label "okres_amazo&#324;ski"
  ]
  node [
    id 123
    label "kambr"
  ]
  node [
    id 124
    label "Zeitgeist"
  ]
  node [
    id 125
    label "nast&#281;pnik"
  ]
  node [
    id 126
    label "kriogen"
  ]
  node [
    id 127
    label "glacja&#322;"
  ]
  node [
    id 128
    label "fala"
  ]
  node [
    id 129
    label "okres_czasu"
  ]
  node [
    id 130
    label "riak"
  ]
  node [
    id 131
    label "schy&#322;ek"
  ]
  node [
    id 132
    label "okres_hesperyjski"
  ]
  node [
    id 133
    label "sylur"
  ]
  node [
    id 134
    label "dewon"
  ]
  node [
    id 135
    label "ciota"
  ]
  node [
    id 136
    label "epoka"
  ]
  node [
    id 137
    label "pierwszorz&#281;d"
  ]
  node [
    id 138
    label "okres_halsztacki"
  ]
  node [
    id 139
    label "ektas"
  ]
  node [
    id 140
    label "zdanie"
  ]
  node [
    id 141
    label "condition"
  ]
  node [
    id 142
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 143
    label "rok_akademicki"
  ]
  node [
    id 144
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 145
    label "postglacja&#322;"
  ]
  node [
    id 146
    label "faza"
  ]
  node [
    id 147
    label "proces_fizjologiczny"
  ]
  node [
    id 148
    label "ediakar"
  ]
  node [
    id 149
    label "time_period"
  ]
  node [
    id 150
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 151
    label "perm"
  ]
  node [
    id 152
    label "rok_szkolny"
  ]
  node [
    id 153
    label "neogen"
  ]
  node [
    id 154
    label "sider"
  ]
  node [
    id 155
    label "flow"
  ]
  node [
    id 156
    label "podokres"
  ]
  node [
    id 157
    label "preglacja&#322;"
  ]
  node [
    id 158
    label "retoryka"
  ]
  node [
    id 159
    label "choroba_przyrodzona"
  ]
  node [
    id 160
    label "garderoba"
  ]
  node [
    id 161
    label "wiecha"
  ]
  node [
    id 162
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 163
    label "fratria"
  ]
  node [
    id 164
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 165
    label "poj&#281;cie"
  ]
  node [
    id 166
    label "rodzina"
  ]
  node [
    id 167
    label "substancja_mieszkaniowa"
  ]
  node [
    id 168
    label "instytucja"
  ]
  node [
    id 169
    label "dom_rodzinny"
  ]
  node [
    id 170
    label "stead"
  ]
  node [
    id 171
    label "siedziba"
  ]
  node [
    id 172
    label "cok&#243;&#322;"
  ]
  node [
    id 173
    label "&#347;wiadectwo"
  ]
  node [
    id 174
    label "p&#322;yta"
  ]
  node [
    id 175
    label "dzie&#322;o"
  ]
  node [
    id 176
    label "rzecz"
  ]
  node [
    id 177
    label "dow&#243;d"
  ]
  node [
    id 178
    label "gr&#243;b"
  ]
  node [
    id 179
    label "doba"
  ]
  node [
    id 180
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 181
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 182
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 183
    label "towar"
  ]
  node [
    id 184
    label "famu&#322;a"
  ]
  node [
    id 185
    label "kluski_&#380;elazne"
  ]
  node [
    id 186
    label "angielka"
  ]
  node [
    id 187
    label "polski"
  ]
  node [
    id 188
    label "&#380;ulik"
  ]
  node [
    id 189
    label "remiza"
  ]
  node [
    id 190
    label "po_&#322;&#243;dzku"
  ]
  node [
    id 191
    label "dziad"
  ]
  node [
    id 192
    label "czarne"
  ]
  node [
    id 193
    label "migawka"
  ]
  node [
    id 194
    label "siaja"
  ]
  node [
    id 195
    label "kra&#324;c&#243;wka"
  ]
  node [
    id 196
    label "brzuszek"
  ]
  node [
    id 197
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 198
    label "przedmiot"
  ]
  node [
    id 199
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 200
    label "Wsch&#243;d"
  ]
  node [
    id 201
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 202
    label "sztuka"
  ]
  node [
    id 203
    label "religia"
  ]
  node [
    id 204
    label "przejmowa&#263;"
  ]
  node [
    id 205
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 206
    label "makrokosmos"
  ]
  node [
    id 207
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 208
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 209
    label "zjawisko"
  ]
  node [
    id 210
    label "praca_rolnicza"
  ]
  node [
    id 211
    label "tradycja"
  ]
  node [
    id 212
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 213
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 214
    label "przejmowanie"
  ]
  node [
    id 215
    label "cecha"
  ]
  node [
    id 216
    label "asymilowanie_si&#281;"
  ]
  node [
    id 217
    label "przej&#261;&#263;"
  ]
  node [
    id 218
    label "hodowla"
  ]
  node [
    id 219
    label "brzoskwiniarnia"
  ]
  node [
    id 220
    label "populace"
  ]
  node [
    id 221
    label "konwencja"
  ]
  node [
    id 222
    label "propriety"
  ]
  node [
    id 223
    label "jako&#347;&#263;"
  ]
  node [
    id 224
    label "kuchnia"
  ]
  node [
    id 225
    label "zwyczaj"
  ]
  node [
    id 226
    label "przej&#281;cie"
  ]
  node [
    id 227
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 228
    label "poczta"
  ]
  node [
    id 229
    label "domowy"
  ]
  node [
    id 230
    label "J&#243;zefa"
  ]
  node [
    id 231
    label "Pi&#322;sudski"
  ]
  node [
    id 232
    label "ii"
  ]
  node [
    id 233
    label "wojna"
  ]
  node [
    id 234
    label "&#347;wiatowy"
  ]
  node [
    id 235
    label "aleja"
  ]
  node [
    id 236
    label "Jan"
  ]
  node [
    id 237
    label "Kili&#324;ski"
  ]
  node [
    id 238
    label "tramwaj"
  ]
  node [
    id 239
    label "regionalny"
  ]
  node [
    id 240
    label "zb&#243;r"
  ]
  node [
    id 241
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 242
    label "zielono&#347;wi&#261;tkowy"
  ]
  node [
    id 243
    label "Immanuel"
  ]
  node [
    id 244
    label "PW"
  ]
  node [
    id 245
    label "matka"
  ]
  node [
    id 246
    label "boski"
  ]
  node [
    id 247
    label "fatimski"
  ]
  node [
    id 248
    label "park"
  ]
  node [
    id 249
    label "on"
  ]
  node [
    id 250
    label "Stanis&#322;awa"
  ]
  node [
    id 251
    label "Moniuszko"
  ]
  node [
    id 252
    label "skwer"
  ]
  node [
    id 253
    label "wyspa"
  ]
  node [
    id 254
    label "Strzemi&#324;skiego"
  ]
  node [
    id 255
    label "koleje"
  ]
  node [
    id 256
    label "Fabryczno"
  ]
  node [
    id 257
    label "plac"
  ]
  node [
    id 258
    label "ko&#347;cielny"
  ]
  node [
    id 259
    label "zajezdnia"
  ]
  node [
    id 260
    label "d&#261;browski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 14
    target 78
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 255
  ]
  edge [
    source 14
    target 256
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 86
  ]
  edge [
    source 16
    target 87
  ]
  edge [
    source 16
    target 88
  ]
  edge [
    source 16
    target 89
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 91
  ]
  edge [
    source 16
    target 92
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 93
  ]
  edge [
    source 17
    target 94
  ]
  edge [
    source 17
    target 43
  ]
  edge [
    source 17
    target 95
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 97
  ]
  edge [
    source 17
    target 98
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 17
    target 106
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 17
    target 108
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 47
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 229
  ]
  edge [
    source 20
    target 230
  ]
  edge [
    source 20
    target 231
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 179
  ]
  edge [
    source 23
    target 180
  ]
  edge [
    source 23
    target 181
  ]
  edge [
    source 23
    target 182
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 24
    target 189
  ]
  edge [
    source 24
    target 190
  ]
  edge [
    source 24
    target 191
  ]
  edge [
    source 24
    target 192
  ]
  edge [
    source 24
    target 193
  ]
  edge [
    source 24
    target 194
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 196
  ]
  edge [
    source 24
    target 229
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 238
  ]
  edge [
    source 24
    target 239
  ]
  edge [
    source 24
    target 255
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 25
    target 197
  ]
  edge [
    source 25
    target 198
  ]
  edge [
    source 25
    target 199
  ]
  edge [
    source 25
    target 200
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 25
    target 202
  ]
  edge [
    source 25
    target 203
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 229
  ]
  edge [
    source 229
    target 230
  ]
  edge [
    source 229
    target 231
  ]
  edge [
    source 230
    target 231
  ]
  edge [
    source 231
    target 235
  ]
  edge [
    source 232
    target 233
  ]
  edge [
    source 232
    target 234
  ]
  edge [
    source 233
    target 234
  ]
  edge [
    source 236
    target 237
  ]
  edge [
    source 236
    target 248
  ]
  edge [
    source 236
    target 249
  ]
  edge [
    source 237
    target 248
  ]
  edge [
    source 237
    target 249
  ]
  edge [
    source 238
    target 239
  ]
  edge [
    source 240
    target 241
  ]
  edge [
    source 240
    target 242
  ]
  edge [
    source 240
    target 243
  ]
  edge [
    source 241
    target 242
  ]
  edge [
    source 241
    target 243
  ]
  edge [
    source 241
    target 244
  ]
  edge [
    source 241
    target 245
  ]
  edge [
    source 241
    target 246
  ]
  edge [
    source 241
    target 247
  ]
  edge [
    source 242
    target 243
  ]
  edge [
    source 244
    target 245
  ]
  edge [
    source 244
    target 246
  ]
  edge [
    source 244
    target 247
  ]
  edge [
    source 245
    target 246
  ]
  edge [
    source 245
    target 247
  ]
  edge [
    source 246
    target 247
  ]
  edge [
    source 248
    target 249
  ]
  edge [
    source 248
    target 250
  ]
  edge [
    source 248
    target 251
  ]
  edge [
    source 249
    target 250
  ]
  edge [
    source 249
    target 251
  ]
  edge [
    source 249
    target 252
  ]
  edge [
    source 249
    target 253
  ]
  edge [
    source 249
    target 254
  ]
  edge [
    source 250
    target 251
  ]
  edge [
    source 252
    target 253
  ]
  edge [
    source 252
    target 254
  ]
  edge [
    source 253
    target 254
  ]
  edge [
    source 255
    target 256
  ]
  edge [
    source 257
    target 258
  ]
  edge [
    source 259
    target 260
  ]
]
