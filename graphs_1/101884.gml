graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.050909090909091
  density 0.007485069674850697
  graphCliqueNumber 3
  node [
    id 0
    label "spotkanie"
    origin "text"
  ]
  node [
    id 1
    label "zadebiutowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nowy"
    origin "text"
  ]
  node [
    id 3
    label "model"
    origin "text"
  ]
  node [
    id 4
    label "hubert"
    origin "text"
  ]
  node [
    id 5
    label "team"
    origin "text"
  ]
  node [
    id 6
    label "associated"
    origin "text"
  ]
  node [
    id 7
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 8
    label "nap&#281;dza&#263;"
    origin "text"
  ]
  node [
    id 9
    label "bezszczotkowym"
    origin "text"
  ]
  node [
    id 10
    label "zestaw"
    origin "text"
  ]
  node [
    id 11
    label "novaka"
    origin "text"
  ]
  node [
    id 12
    label "silnik"
    origin "text"
  ]
  node [
    id 13
    label "okaza&#263;"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;"
    origin "text"
  ]
  node [
    id 15
    label "niesamowicie"
    origin "text"
  ]
  node [
    id 16
    label "trudny"
    origin "text"
  ]
  node [
    id 17
    label "opanowanie"
    origin "text"
  ]
  node [
    id 18
    label "opr&#243;cz"
    origin "text"
  ]
  node [
    id 19
    label "w&#322;asciciela"
    origin "text"
  ]
  node [
    id 20
    label "tylko"
    origin "text"
  ]
  node [
    id 21
    label "bazyl"
    origin "text"
  ]
  node [
    id 22
    label "by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "stan"
    origin "text"
  ]
  node [
    id 24
    label "wykr&#281;ca&#263;"
    origin "text"
  ]
  node [
    id 25
    label "nim"
    origin "text"
  ]
  node [
    id 26
    label "okr&#261;&#380;enie"
    origin "text"
  ]
  node [
    id 27
    label "r&#243;wne"
    origin "text"
  ]
  node [
    id 28
    label "temp"
    origin "text"
  ]
  node [
    id 29
    label "reszta"
    origin "text"
  ]
  node [
    id 30
    label "nasa"
    origin "text"
  ]
  node [
    id 31
    label "g&#322;&#243;wnie"
    origin "text"
  ]
  node [
    id 32
    label "b&#261;k"
    origin "text"
  ]
  node [
    id 33
    label "zakr&#281;t"
    origin "text"
  ]
  node [
    id 34
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 35
    label "po&#380;egnanie"
  ]
  node [
    id 36
    label "spowodowanie"
  ]
  node [
    id 37
    label "znalezienie"
  ]
  node [
    id 38
    label "znajomy"
  ]
  node [
    id 39
    label "doznanie"
  ]
  node [
    id 40
    label "employment"
  ]
  node [
    id 41
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 42
    label "gather"
  ]
  node [
    id 43
    label "powitanie"
  ]
  node [
    id 44
    label "spotykanie"
  ]
  node [
    id 45
    label "wydarzenie"
  ]
  node [
    id 46
    label "gathering"
  ]
  node [
    id 47
    label "spotkanie_si&#281;"
  ]
  node [
    id 48
    label "zdarzenie_si&#281;"
  ]
  node [
    id 49
    label "match"
  ]
  node [
    id 50
    label "zawarcie"
  ]
  node [
    id 51
    label "zastartowa&#263;"
  ]
  node [
    id 52
    label "cz&#322;owiek"
  ]
  node [
    id 53
    label "nowotny"
  ]
  node [
    id 54
    label "drugi"
  ]
  node [
    id 55
    label "kolejny"
  ]
  node [
    id 56
    label "bie&#380;&#261;cy"
  ]
  node [
    id 57
    label "nowo"
  ]
  node [
    id 58
    label "narybek"
  ]
  node [
    id 59
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 60
    label "obcy"
  ]
  node [
    id 61
    label "typ"
  ]
  node [
    id 62
    label "pozowa&#263;"
  ]
  node [
    id 63
    label "ideal"
  ]
  node [
    id 64
    label "matryca"
  ]
  node [
    id 65
    label "imitacja"
  ]
  node [
    id 66
    label "ruch"
  ]
  node [
    id 67
    label "motif"
  ]
  node [
    id 68
    label "pozowanie"
  ]
  node [
    id 69
    label "wz&#243;r"
  ]
  node [
    id 70
    label "miniatura"
  ]
  node [
    id 71
    label "prezenter"
  ]
  node [
    id 72
    label "facet"
  ]
  node [
    id 73
    label "orygina&#322;"
  ]
  node [
    id 74
    label "mildew"
  ]
  node [
    id 75
    label "spos&#243;b"
  ]
  node [
    id 76
    label "zi&#243;&#322;ko"
  ]
  node [
    id 77
    label "adaptation"
  ]
  node [
    id 78
    label "dublet"
  ]
  node [
    id 79
    label "zesp&#243;&#322;"
  ]
  node [
    id 80
    label "force"
  ]
  node [
    id 81
    label "baga&#380;nik"
  ]
  node [
    id 82
    label "immobilizer"
  ]
  node [
    id 83
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 84
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 85
    label "poduszka_powietrzna"
  ]
  node [
    id 86
    label "dachowanie"
  ]
  node [
    id 87
    label "dwu&#347;lad"
  ]
  node [
    id 88
    label "deska_rozdzielcza"
  ]
  node [
    id 89
    label "poci&#261;g_drogowy"
  ]
  node [
    id 90
    label "kierownica"
  ]
  node [
    id 91
    label "pojazd_drogowy"
  ]
  node [
    id 92
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 93
    label "pompa_wodna"
  ]
  node [
    id 94
    label "wycieraczka"
  ]
  node [
    id 95
    label "bak"
  ]
  node [
    id 96
    label "ABS"
  ]
  node [
    id 97
    label "most"
  ]
  node [
    id 98
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 99
    label "spryskiwacz"
  ]
  node [
    id 100
    label "t&#322;umik"
  ]
  node [
    id 101
    label "tempomat"
  ]
  node [
    id 102
    label "pobudza&#263;"
  ]
  node [
    id 103
    label "przesuwa&#263;"
  ]
  node [
    id 104
    label "gromadzi&#263;"
  ]
  node [
    id 105
    label "powodowa&#263;"
  ]
  node [
    id 106
    label "go"
  ]
  node [
    id 107
    label "rusza&#263;"
  ]
  node [
    id 108
    label "stage_set"
  ]
  node [
    id 109
    label "sygna&#322;"
  ]
  node [
    id 110
    label "sk&#322;ada&#263;"
  ]
  node [
    id 111
    label "zbi&#243;r"
  ]
  node [
    id 112
    label "struktura"
  ]
  node [
    id 113
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 114
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 115
    label "dotarcie"
  ]
  node [
    id 116
    label "wyci&#261;garka"
  ]
  node [
    id 117
    label "biblioteka"
  ]
  node [
    id 118
    label "aerosanie"
  ]
  node [
    id 119
    label "podgrzewacz"
  ]
  node [
    id 120
    label "bombowiec"
  ]
  node [
    id 121
    label "dociera&#263;"
  ]
  node [
    id 122
    label "gniazdo_zaworowe"
  ]
  node [
    id 123
    label "motor&#243;wka"
  ]
  node [
    id 124
    label "nap&#281;d"
  ]
  node [
    id 125
    label "perpetuum_mobile"
  ]
  node [
    id 126
    label "rz&#281;&#380;enie"
  ]
  node [
    id 127
    label "mechanizm"
  ]
  node [
    id 128
    label "gondola_silnikowa"
  ]
  node [
    id 129
    label "docieranie"
  ]
  node [
    id 130
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 131
    label "rz&#281;zi&#263;"
  ]
  node [
    id 132
    label "motoszybowiec"
  ]
  node [
    id 133
    label "motogodzina"
  ]
  node [
    id 134
    label "dotrze&#263;"
  ]
  node [
    id 135
    label "radiator"
  ]
  node [
    id 136
    label "program"
  ]
  node [
    id 137
    label "pokaza&#263;"
  ]
  node [
    id 138
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 139
    label "testify"
  ]
  node [
    id 140
    label "give"
  ]
  node [
    id 141
    label "bardzo"
  ]
  node [
    id 142
    label "niesamowito"
  ]
  node [
    id 143
    label "niezwykle"
  ]
  node [
    id 144
    label "niesamowity"
  ]
  node [
    id 145
    label "wymagaj&#261;cy"
  ]
  node [
    id 146
    label "skomplikowany"
  ]
  node [
    id 147
    label "k&#322;opotliwy"
  ]
  node [
    id 148
    label "ci&#281;&#380;ko"
  ]
  node [
    id 149
    label "zr&#243;wnowa&#380;enie"
  ]
  node [
    id 150
    label "control"
  ]
  node [
    id 151
    label "nasilenie_si&#281;"
  ]
  node [
    id 152
    label "powstrzymanie"
  ]
  node [
    id 153
    label "convention"
  ]
  node [
    id 154
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 155
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 156
    label "nauczenie_si&#281;"
  ]
  node [
    id 157
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 158
    label "poczucie"
  ]
  node [
    id 159
    label "podporz&#261;dkowanie"
  ]
  node [
    id 160
    label "dostanie"
  ]
  node [
    id 161
    label "wyniesienie"
  ]
  node [
    id 162
    label "si&#281;ga&#263;"
  ]
  node [
    id 163
    label "trwa&#263;"
  ]
  node [
    id 164
    label "obecno&#347;&#263;"
  ]
  node [
    id 165
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 166
    label "stand"
  ]
  node [
    id 167
    label "mie&#263;_miejsce"
  ]
  node [
    id 168
    label "uczestniczy&#263;"
  ]
  node [
    id 169
    label "chodzi&#263;"
  ]
  node [
    id 170
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 171
    label "equal"
  ]
  node [
    id 172
    label "Arizona"
  ]
  node [
    id 173
    label "Georgia"
  ]
  node [
    id 174
    label "warstwa"
  ]
  node [
    id 175
    label "jednostka_administracyjna"
  ]
  node [
    id 176
    label "Goa"
  ]
  node [
    id 177
    label "Hawaje"
  ]
  node [
    id 178
    label "Floryda"
  ]
  node [
    id 179
    label "Oklahoma"
  ]
  node [
    id 180
    label "punkt"
  ]
  node [
    id 181
    label "Alaska"
  ]
  node [
    id 182
    label "Alabama"
  ]
  node [
    id 183
    label "wci&#281;cie"
  ]
  node [
    id 184
    label "Oregon"
  ]
  node [
    id 185
    label "poziom"
  ]
  node [
    id 186
    label "Teksas"
  ]
  node [
    id 187
    label "Illinois"
  ]
  node [
    id 188
    label "Jukatan"
  ]
  node [
    id 189
    label "Waszyngton"
  ]
  node [
    id 190
    label "shape"
  ]
  node [
    id 191
    label "Nowy_Meksyk"
  ]
  node [
    id 192
    label "ilo&#347;&#263;"
  ]
  node [
    id 193
    label "state"
  ]
  node [
    id 194
    label "Nowy_York"
  ]
  node [
    id 195
    label "Arakan"
  ]
  node [
    id 196
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 197
    label "Kalifornia"
  ]
  node [
    id 198
    label "wektor"
  ]
  node [
    id 199
    label "Massachusetts"
  ]
  node [
    id 200
    label "miejsce"
  ]
  node [
    id 201
    label "Pensylwania"
  ]
  node [
    id 202
    label "Maryland"
  ]
  node [
    id 203
    label "Michigan"
  ]
  node [
    id 204
    label "Ohio"
  ]
  node [
    id 205
    label "Kansas"
  ]
  node [
    id 206
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 207
    label "Luizjana"
  ]
  node [
    id 208
    label "samopoczucie"
  ]
  node [
    id 209
    label "Wirginia"
  ]
  node [
    id 210
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 211
    label "odwraca&#263;"
  ]
  node [
    id 212
    label "wyjmowa&#263;"
  ]
  node [
    id 213
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 214
    label "corrupt"
  ]
  node [
    id 215
    label "deformowa&#263;"
  ]
  node [
    id 216
    label "kierunek"
  ]
  node [
    id 217
    label "wrench"
  ]
  node [
    id 218
    label "wy&#380;yma&#263;"
  ]
  node [
    id 219
    label "gra_planszowa"
  ]
  node [
    id 220
    label "czynno&#347;&#263;"
  ]
  node [
    id 221
    label "tour"
  ]
  node [
    id 222
    label "crack"
  ]
  node [
    id 223
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 224
    label "oboranie"
  ]
  node [
    id 225
    label "circulation"
  ]
  node [
    id 226
    label "odwiedzenie"
  ]
  node [
    id 227
    label "beltway"
  ]
  node [
    id 228
    label "remainder"
  ]
  node [
    id 229
    label "wydanie"
  ]
  node [
    id 230
    label "wyda&#263;"
  ]
  node [
    id 231
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 232
    label "wydawa&#263;"
  ]
  node [
    id 233
    label "pozosta&#322;y"
  ]
  node [
    id 234
    label "kwota"
  ]
  node [
    id 235
    label "g&#322;&#243;wny"
  ]
  node [
    id 236
    label "trzmiele_i_trzmielce"
  ]
  node [
    id 237
    label "ptak_wodno-b&#322;otny"
  ]
  node [
    id 238
    label "zabawka"
  ]
  node [
    id 239
    label "pierd"
  ]
  node [
    id 240
    label "bittern"
  ]
  node [
    id 241
    label "dziecko"
  ]
  node [
    id 242
    label "b&#261;kanie"
  ]
  node [
    id 243
    label "wydalina"
  ]
  node [
    id 244
    label "&#380;yroskop"
  ]
  node [
    id 245
    label "bry&#322;a_sztywna"
  ]
  node [
    id 246
    label "&#322;&#243;d&#378;_wios&#322;owa"
  ]
  node [
    id 247
    label "ptak_w&#281;drowny"
  ]
  node [
    id 248
    label "much&#243;wka"
  ]
  node [
    id 249
    label "czaplowate"
  ]
  node [
    id 250
    label "knot"
  ]
  node [
    id 251
    label "b&#261;kowate"
  ]
  node [
    id 252
    label "&#380;&#261;d&#322;&#243;wka"
  ]
  node [
    id 253
    label "b&#261;kni&#281;cie"
  ]
  node [
    id 254
    label "ekscentryk"
  ]
  node [
    id 255
    label "podsterowno&#347;&#263;"
  ]
  node [
    id 256
    label "czas"
  ]
  node [
    id 257
    label "zapaleniec"
  ]
  node [
    id 258
    label "zwrot"
  ]
  node [
    id 259
    label "odcinek"
  ]
  node [
    id 260
    label "nadsterowno&#347;&#263;"
  ]
  node [
    id 261
    label "szalona_g&#322;owa"
  ]
  node [
    id 262
    label "serpentyna"
  ]
  node [
    id 263
    label "trasa"
  ]
  node [
    id 264
    label "zajob"
  ]
  node [
    id 265
    label "Associated"
  ]
  node [
    id 266
    label "SC10"
  ]
  node [
    id 267
    label "Kyosho"
  ]
  node [
    id 268
    label "Lazerem"
  ]
  node [
    id 269
    label "Rustlera"
  ]
  node [
    id 270
    label "VXL"
  ]
  node [
    id 271
    label "Losi"
  ]
  node [
    id 272
    label "XXX"
  ]
  node [
    id 273
    label "CR"
  ]
  node [
    id 274
    label "XL"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 36
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 48
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 23
    target 173
  ]
  edge [
    source 23
    target 174
  ]
  edge [
    source 23
    target 175
  ]
  edge [
    source 23
    target 176
  ]
  edge [
    source 23
    target 177
  ]
  edge [
    source 23
    target 178
  ]
  edge [
    source 23
    target 179
  ]
  edge [
    source 23
    target 180
  ]
  edge [
    source 23
    target 181
  ]
  edge [
    source 23
    target 182
  ]
  edge [
    source 23
    target 183
  ]
  edge [
    source 23
    target 184
  ]
  edge [
    source 23
    target 185
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 23
    target 187
  ]
  edge [
    source 23
    target 188
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 220
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 26
    target 221
  ]
  edge [
    source 26
    target 66
  ]
  edge [
    source 26
    target 222
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 26
    target 224
  ]
  edge [
    source 26
    target 225
  ]
  edge [
    source 26
    target 226
  ]
  edge [
    source 26
    target 227
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 228
  ]
  edge [
    source 29
    target 229
  ]
  edge [
    source 29
    target 230
  ]
  edge [
    source 29
    target 231
  ]
  edge [
    source 29
    target 232
  ]
  edge [
    source 29
    target 233
  ]
  edge [
    source 29
    target 234
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 235
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 236
  ]
  edge [
    source 32
    target 237
  ]
  edge [
    source 32
    target 238
  ]
  edge [
    source 32
    target 239
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 32
    target 241
  ]
  edge [
    source 32
    target 242
  ]
  edge [
    source 32
    target 243
  ]
  edge [
    source 32
    target 244
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 246
  ]
  edge [
    source 32
    target 247
  ]
  edge [
    source 32
    target 248
  ]
  edge [
    source 32
    target 249
  ]
  edge [
    source 32
    target 250
  ]
  edge [
    source 32
    target 251
  ]
  edge [
    source 32
    target 252
  ]
  edge [
    source 32
    target 253
  ]
  edge [
    source 33
    target 200
  ]
  edge [
    source 33
    target 254
  ]
  edge [
    source 33
    target 255
  ]
  edge [
    source 33
    target 256
  ]
  edge [
    source 33
    target 257
  ]
  edge [
    source 33
    target 258
  ]
  edge [
    source 33
    target 259
  ]
  edge [
    source 33
    target 260
  ]
  edge [
    source 33
    target 231
  ]
  edge [
    source 33
    target 261
  ]
  edge [
    source 33
    target 262
  ]
  edge [
    source 33
    target 263
  ]
  edge [
    source 33
    target 264
  ]
  edge [
    source 265
    target 266
  ]
  edge [
    source 267
    target 268
  ]
  edge [
    source 269
    target 270
  ]
  edge [
    source 269
    target 274
  ]
  edge [
    source 271
    target 272
  ]
  edge [
    source 271
    target 273
  ]
  edge [
    source 272
    target 273
  ]
]
