graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 3
  node [
    id 0
    label "eksperymentowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 2
    label "kubizm"
    origin "text"
  ]
  node [
    id 3
    label "brzydota"
    origin "text"
  ]
  node [
    id 4
    label "cubism"
  ]
  node [
    id 5
    label "szko&#322;a"
  ]
  node [
    id 6
    label "wygl&#261;d"
  ]
  node [
    id 7
    label "szkarada"
  ]
  node [
    id 8
    label "dysproporcja"
  ]
  node [
    id 9
    label "cecha"
  ]
  node [
    id 10
    label "koszmarek"
  ]
  node [
    id 11
    label "ugliness"
  ]
  node [
    id 12
    label "dysharmonia"
  ]
  node [
    id 13
    label "par"
  ]
  node [
    id 14
    label "pi&#281;kny"
  ]
  node [
    id 15
    label "kochankowie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
]
