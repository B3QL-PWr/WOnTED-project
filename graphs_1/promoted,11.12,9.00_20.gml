graph [
  maxDegree 23
  minDegree 1
  meanDegree 2
  density 0.028985507246376812
  graphCliqueNumber 2
  node [
    id 0
    label "rekordowy"
    origin "text"
  ]
  node [
    id 1
    label "godzina"
    origin "text"
  ]
  node [
    id 2
    label "tyle"
    origin "text"
  ]
  node [
    id 3
    label "bez"
    origin "text"
  ]
  node [
    id 4
    label "przerwa"
    origin "text"
  ]
  node [
    id 5
    label "przepracowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "polski"
    origin "text"
  ]
  node [
    id 7
    label "lekarz"
    origin "text"
  ]
  node [
    id 8
    label "maksymalny"
  ]
  node [
    id 9
    label "rekordowo"
  ]
  node [
    id 10
    label "najlepszy"
  ]
  node [
    id 11
    label "minuta"
  ]
  node [
    id 12
    label "doba"
  ]
  node [
    id 13
    label "czas"
  ]
  node [
    id 14
    label "p&#243;&#322;godzina"
  ]
  node [
    id 15
    label "kwadrans"
  ]
  node [
    id 16
    label "time"
  ]
  node [
    id 17
    label "jednostka_czasu"
  ]
  node [
    id 18
    label "wiele"
  ]
  node [
    id 19
    label "konkretnie"
  ]
  node [
    id 20
    label "nieznacznie"
  ]
  node [
    id 21
    label "ki&#347;&#263;"
  ]
  node [
    id 22
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 23
    label "krzew"
  ]
  node [
    id 24
    label "pi&#380;maczkowate"
  ]
  node [
    id 25
    label "pestkowiec"
  ]
  node [
    id 26
    label "kwiat"
  ]
  node [
    id 27
    label "owoc"
  ]
  node [
    id 28
    label "oliwkowate"
  ]
  node [
    id 29
    label "ro&#347;lina"
  ]
  node [
    id 30
    label "hy&#263;ka"
  ]
  node [
    id 31
    label "lilac"
  ]
  node [
    id 32
    label "delfinidyna"
  ]
  node [
    id 33
    label "pauza"
  ]
  node [
    id 34
    label "miejsce"
  ]
  node [
    id 35
    label "przedzia&#322;"
  ]
  node [
    id 36
    label "upora&#263;_si&#281;"
  ]
  node [
    id 37
    label "prze&#380;y&#263;"
  ]
  node [
    id 38
    label "overwork"
  ]
  node [
    id 39
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 40
    label "lacki"
  ]
  node [
    id 41
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 42
    label "przedmiot"
  ]
  node [
    id 43
    label "sztajer"
  ]
  node [
    id 44
    label "drabant"
  ]
  node [
    id 45
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 46
    label "polak"
  ]
  node [
    id 47
    label "pierogi_ruskie"
  ]
  node [
    id 48
    label "krakowiak"
  ]
  node [
    id 49
    label "Polish"
  ]
  node [
    id 50
    label "j&#281;zyk"
  ]
  node [
    id 51
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 52
    label "oberek"
  ]
  node [
    id 53
    label "po_polsku"
  ]
  node [
    id 54
    label "mazur"
  ]
  node [
    id 55
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 56
    label "chodzony"
  ]
  node [
    id 57
    label "skoczny"
  ]
  node [
    id 58
    label "ryba_po_grecku"
  ]
  node [
    id 59
    label "goniony"
  ]
  node [
    id 60
    label "polsko"
  ]
  node [
    id 61
    label "zbada&#263;"
  ]
  node [
    id 62
    label "medyk"
  ]
  node [
    id 63
    label "lekarze"
  ]
  node [
    id 64
    label "pracownik"
  ]
  node [
    id 65
    label "eskulap"
  ]
  node [
    id 66
    label "Hipokrates"
  ]
  node [
    id 67
    label "Mesmer"
  ]
  node [
    id 68
    label "Galen"
  ]
  node [
    id 69
    label "dokt&#243;r"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
]
