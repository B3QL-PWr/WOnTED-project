graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9130434782608696
  density 0.08695652173913043
  graphCliqueNumber 2
  node [
    id 0
    label "m&#322;oda"
    origin "text"
  ]
  node [
    id 1
    label "wierna"
    origin "text"
  ]
  node [
    id 2
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 3
    label "cz&#322;owiek"
  ]
  node [
    id 4
    label "kobieta"
  ]
  node [
    id 5
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 6
    label "&#380;ona"
  ]
  node [
    id 7
    label "suknia_&#347;lubna"
  ]
  node [
    id 8
    label "niezam&#281;&#380;na"
  ]
  node [
    id 9
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 10
    label "zakrystia"
  ]
  node [
    id 11
    label "organizacja_religijna"
  ]
  node [
    id 12
    label "nawa"
  ]
  node [
    id 13
    label "nerwica_eklezjogenna"
  ]
  node [
    id 14
    label "prezbiterium"
  ]
  node [
    id 15
    label "kropielnica"
  ]
  node [
    id 16
    label "wsp&#243;lnota"
  ]
  node [
    id 17
    label "church"
  ]
  node [
    id 18
    label "kruchta"
  ]
  node [
    id 19
    label "Ska&#322;ka"
  ]
  node [
    id 20
    label "kult"
  ]
  node [
    id 21
    label "ub&#322;agalnia"
  ]
  node [
    id 22
    label "dom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
]
