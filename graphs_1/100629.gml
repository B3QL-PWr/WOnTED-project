graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.129032258064516
  density 0.01150828247602441
  graphCliqueNumber 4
  node [
    id 0
    label "miejski"
    origin "text"
  ]
  node [
    id 1
    label "kryty"
    origin "text"
  ]
  node [
    id 2
    label "p&#322;ywalnia"
    origin "text"
  ]
  node [
    id 3
    label "wodnik"
    origin "text"
  ]
  node [
    id 4
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 5
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "sobota"
    origin "text"
  ]
  node [
    id 7
    label "rocznik"
    origin "text"
  ]
  node [
    id 8
    label "koniec"
    origin "text"
  ]
  node [
    id 9
    label "wakacje"
    origin "text"
  ]
  node [
    id 10
    label "poniedzia&#322;ek"
    origin "text"
  ]
  node [
    id 11
    label "godzina"
    origin "text"
  ]
  node [
    id 12
    label "ostatnie"
    origin "text"
  ]
  node [
    id 13
    label "wej&#347;cie"
    origin "text"
  ]
  node [
    id 14
    label "ulgowy"
    origin "text"
  ]
  node [
    id 15
    label "dla"
    origin "text"
  ]
  node [
    id 16
    label "m&#322;odzie&#380;"
    origin "text"
  ]
  node [
    id 17
    label "uczy&#263;"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;"
    origin "text"
  ]
  node [
    id 19
    label "uko&#324;czenie"
    origin "text"
  ]
  node [
    id 20
    label "rok"
    origin "text"
  ]
  node [
    id 21
    label "okaza&#263;"
    origin "text"
  ]
  node [
    id 22
    label "legitymacja"
    origin "text"
  ]
  node [
    id 23
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "kwota"
    origin "text"
  ]
  node [
    id 25
    label "podlega&#263;"
    origin "text"
  ]
  node [
    id 26
    label "rozliczenie"
    origin "text"
  ]
  node [
    id 27
    label "abonament"
    origin "text"
  ]
  node [
    id 28
    label "miejsko"
  ]
  node [
    id 29
    label "miastowy"
  ]
  node [
    id 30
    label "typowy"
  ]
  node [
    id 31
    label "publiczny"
  ]
  node [
    id 32
    label "obiekt"
  ]
  node [
    id 33
    label "k&#261;pielisko"
  ]
  node [
    id 34
    label "basen"
  ]
  node [
    id 35
    label "budowla"
  ]
  node [
    id 36
    label "falownica"
  ]
  node [
    id 37
    label "duch"
  ]
  node [
    id 38
    label "cz&#322;owiek"
  ]
  node [
    id 39
    label "chru&#347;ciele"
  ]
  node [
    id 40
    label "woda"
  ]
  node [
    id 41
    label "ptak_wodny"
  ]
  node [
    id 42
    label "komunikowa&#263;"
  ]
  node [
    id 43
    label "powiada&#263;"
  ]
  node [
    id 44
    label "inform"
  ]
  node [
    id 45
    label "weekend"
  ]
  node [
    id 46
    label "dzie&#324;_powszedni"
  ]
  node [
    id 47
    label "Wielka_Sobota"
  ]
  node [
    id 48
    label "formacja"
  ]
  node [
    id 49
    label "kronika"
  ]
  node [
    id 50
    label "czasopismo"
  ]
  node [
    id 51
    label "yearbook"
  ]
  node [
    id 52
    label "defenestracja"
  ]
  node [
    id 53
    label "szereg"
  ]
  node [
    id 54
    label "dzia&#322;anie"
  ]
  node [
    id 55
    label "miejsce"
  ]
  node [
    id 56
    label "ostatnie_podrygi"
  ]
  node [
    id 57
    label "kres"
  ]
  node [
    id 58
    label "agonia"
  ]
  node [
    id 59
    label "visitation"
  ]
  node [
    id 60
    label "szeol"
  ]
  node [
    id 61
    label "mogi&#322;a"
  ]
  node [
    id 62
    label "chwila"
  ]
  node [
    id 63
    label "wydarzenie"
  ]
  node [
    id 64
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 65
    label "pogrzebanie"
  ]
  node [
    id 66
    label "punkt"
  ]
  node [
    id 67
    label "&#380;a&#322;oba"
  ]
  node [
    id 68
    label "zabicie"
  ]
  node [
    id 69
    label "kres_&#380;ycia"
  ]
  node [
    id 70
    label "urlop"
  ]
  node [
    id 71
    label "czas_wolny"
  ]
  node [
    id 72
    label "rok_akademicki"
  ]
  node [
    id 73
    label "rok_szkolny"
  ]
  node [
    id 74
    label "Poniedzia&#322;ek_Wielkonocny"
  ]
  node [
    id 75
    label "tydzie&#324;"
  ]
  node [
    id 76
    label "minuta"
  ]
  node [
    id 77
    label "doba"
  ]
  node [
    id 78
    label "czas"
  ]
  node [
    id 79
    label "p&#243;&#322;godzina"
  ]
  node [
    id 80
    label "kwadrans"
  ]
  node [
    id 81
    label "time"
  ]
  node [
    id 82
    label "jednostka_czasu"
  ]
  node [
    id 83
    label "wzi&#281;cie"
  ]
  node [
    id 84
    label "doj&#347;cie"
  ]
  node [
    id 85
    label "wnikni&#281;cie"
  ]
  node [
    id 86
    label "spotkanie"
  ]
  node [
    id 87
    label "przekroczenie"
  ]
  node [
    id 88
    label "bramka"
  ]
  node [
    id 89
    label "stanie_si&#281;"
  ]
  node [
    id 90
    label "podw&#243;rze"
  ]
  node [
    id 91
    label "dostanie_si&#281;"
  ]
  node [
    id 92
    label "zaatakowanie"
  ]
  node [
    id 93
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 94
    label "wzniesienie_si&#281;"
  ]
  node [
    id 95
    label "otw&#243;r"
  ]
  node [
    id 96
    label "pojawienie_si&#281;"
  ]
  node [
    id 97
    label "zacz&#281;cie"
  ]
  node [
    id 98
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 99
    label "trespass"
  ]
  node [
    id 100
    label "poznanie"
  ]
  node [
    id 101
    label "wnij&#347;cie"
  ]
  node [
    id 102
    label "zdarzenie_si&#281;"
  ]
  node [
    id 103
    label "approach"
  ]
  node [
    id 104
    label "nast&#261;pienie"
  ]
  node [
    id 105
    label "pocz&#261;tek"
  ]
  node [
    id 106
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 107
    label "wpuszczenie"
  ]
  node [
    id 108
    label "stimulation"
  ]
  node [
    id 109
    label "wch&#243;d"
  ]
  node [
    id 110
    label "dost&#281;p"
  ]
  node [
    id 111
    label "cz&#322;onek"
  ]
  node [
    id 112
    label "vent"
  ]
  node [
    id 113
    label "przenikni&#281;cie"
  ]
  node [
    id 114
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 115
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 116
    label "urz&#261;dzenie"
  ]
  node [
    id 117
    label "release"
  ]
  node [
    id 118
    label "dom"
  ]
  node [
    id 119
    label "ulgowo"
  ]
  node [
    id 120
    label "ta&#324;szy"
  ]
  node [
    id 121
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 122
    label "potomstwo"
  ]
  node [
    id 123
    label "smarkateria"
  ]
  node [
    id 124
    label "zapoznawa&#263;"
  ]
  node [
    id 125
    label "teach"
  ]
  node [
    id 126
    label "train"
  ]
  node [
    id 127
    label "rozwija&#263;"
  ]
  node [
    id 128
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 129
    label "pracowa&#263;"
  ]
  node [
    id 130
    label "szkoli&#263;"
  ]
  node [
    id 131
    label "zrobienie"
  ]
  node [
    id 132
    label "zako&#324;czenie"
  ]
  node [
    id 133
    label "uczenie_si&#281;"
  ]
  node [
    id 134
    label "termination"
  ]
  node [
    id 135
    label "completion"
  ]
  node [
    id 136
    label "stulecie"
  ]
  node [
    id 137
    label "kalendarz"
  ]
  node [
    id 138
    label "pora_roku"
  ]
  node [
    id 139
    label "cykl_astronomiczny"
  ]
  node [
    id 140
    label "p&#243;&#322;rocze"
  ]
  node [
    id 141
    label "grupa"
  ]
  node [
    id 142
    label "kwarta&#322;"
  ]
  node [
    id 143
    label "kurs"
  ]
  node [
    id 144
    label "jubileusz"
  ]
  node [
    id 145
    label "miesi&#261;c"
  ]
  node [
    id 146
    label "lata"
  ]
  node [
    id 147
    label "martwy_sezon"
  ]
  node [
    id 148
    label "pokaza&#263;"
  ]
  node [
    id 149
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 150
    label "testify"
  ]
  node [
    id 151
    label "give"
  ]
  node [
    id 152
    label "fotografia"
  ]
  node [
    id 153
    label "konfirmacja"
  ]
  node [
    id 154
    label "matryku&#322;a"
  ]
  node [
    id 155
    label "dow&#243;d_to&#380;samo&#347;ci"
  ]
  node [
    id 156
    label "law"
  ]
  node [
    id 157
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 158
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 159
    label "odsuwa&#263;"
  ]
  node [
    id 160
    label "zanosi&#263;"
  ]
  node [
    id 161
    label "otrzymywa&#263;"
  ]
  node [
    id 162
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 163
    label "rozpowszechnia&#263;"
  ]
  node [
    id 164
    label "ujawnia&#263;"
  ]
  node [
    id 165
    label "kra&#347;&#263;"
  ]
  node [
    id 166
    label "liczy&#263;"
  ]
  node [
    id 167
    label "raise"
  ]
  node [
    id 168
    label "podnosi&#263;"
  ]
  node [
    id 169
    label "limit"
  ]
  node [
    id 170
    label "wynie&#347;&#263;"
  ]
  node [
    id 171
    label "pieni&#261;dze"
  ]
  node [
    id 172
    label "ilo&#347;&#263;"
  ]
  node [
    id 173
    label "czu&#263;"
  ]
  node [
    id 174
    label "zale&#380;e&#263;"
  ]
  node [
    id 175
    label "doznawa&#263;"
  ]
  node [
    id 176
    label "zbilansowanie"
  ]
  node [
    id 177
    label "ustalenie"
  ]
  node [
    id 178
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 179
    label "obliczenie"
  ]
  node [
    id 180
    label "count"
  ]
  node [
    id 181
    label "naliczenie"
  ]
  node [
    id 182
    label "season"
  ]
  node [
    id 183
    label "przedp&#322;ata"
  ]
  node [
    id 184
    label "bloczek"
  ]
  node [
    id 185
    label "kry&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 54
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 19
    target 135
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 78
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 148
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 21
    target 150
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 22
    target 155
  ]
  edge [
    source 22
    target 156
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 158
  ]
  edge [
    source 23
    target 159
  ]
  edge [
    source 23
    target 160
  ]
  edge [
    source 23
    target 161
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 23
    target 164
  ]
  edge [
    source 23
    target 165
  ]
  edge [
    source 23
    target 166
  ]
  edge [
    source 23
    target 167
  ]
  edge [
    source 23
    target 168
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 170
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 177
  ]
  edge [
    source 26
    target 178
  ]
  edge [
    source 26
    target 179
  ]
  edge [
    source 26
    target 180
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 27
    target 182
  ]
  edge [
    source 27
    target 183
  ]
  edge [
    source 27
    target 184
  ]
]
