graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9090909090909092
  density 0.04439746300211417
  graphCliqueNumber 2
  node [
    id 0
    label "policjant"
    origin "text"
  ]
  node [
    id 1
    label "poszukiwa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "sprawca"
    origin "text"
  ]
  node [
    id 3
    label "kradzie&#380;"
    origin "text"
  ]
  node [
    id 4
    label "laptop"
    origin "text"
  ]
  node [
    id 5
    label "galeria"
    origin "text"
  ]
  node [
    id 6
    label "handlowy"
    origin "text"
  ]
  node [
    id 7
    label "przy"
    origin "text"
  ]
  node [
    id 8
    label "witos"
    origin "text"
  ]
  node [
    id 9
    label "lublin"
    origin "text"
  ]
  node [
    id 10
    label "policja"
  ]
  node [
    id 11
    label "blacharz"
  ]
  node [
    id 12
    label "pa&#322;a"
  ]
  node [
    id 13
    label "mundurowy"
  ]
  node [
    id 14
    label "str&#243;&#380;"
  ]
  node [
    id 15
    label "glina"
  ]
  node [
    id 16
    label "ask"
  ]
  node [
    id 17
    label "stara&#263;_si&#281;"
  ]
  node [
    id 18
    label "szuka&#263;"
  ]
  node [
    id 19
    label "look"
  ]
  node [
    id 20
    label "cz&#322;owiek"
  ]
  node [
    id 21
    label "sprawiciel"
  ]
  node [
    id 22
    label "plundering"
  ]
  node [
    id 23
    label "wydarzenie"
  ]
  node [
    id 24
    label "przest&#281;pstwo"
  ]
  node [
    id 25
    label "touchpad"
  ]
  node [
    id 26
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 27
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 28
    label "Ultrabook"
  ]
  node [
    id 29
    label "sklep"
  ]
  node [
    id 30
    label "eskalator"
  ]
  node [
    id 31
    label "wystawa"
  ]
  node [
    id 32
    label "balkon"
  ]
  node [
    id 33
    label "centrum_handlowe"
  ]
  node [
    id 34
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 35
    label "Galeria_Arsena&#322;"
  ]
  node [
    id 36
    label "publiczno&#347;&#263;"
  ]
  node [
    id 37
    label "zbi&#243;r"
  ]
  node [
    id 38
    label "sala"
  ]
  node [
    id 39
    label "&#322;&#261;cznik"
  ]
  node [
    id 40
    label "muzeum"
  ]
  node [
    id 41
    label "handlowo"
  ]
  node [
    id 42
    label "AL"
  ]
  node [
    id 43
    label "Witos"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 42
    target 43
  ]
]
