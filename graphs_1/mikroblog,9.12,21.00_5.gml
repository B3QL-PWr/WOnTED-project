graph [
  maxDegree 51
  minDegree 1
  meanDegree 1.962962962962963
  density 0.037037037037037035
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 2
    label "madki"
    origin "text"
  ]
  node [
    id 3
    label "patologia"
    origin "text"
  ]
  node [
    id 4
    label "histopatologia"
  ]
  node [
    id 5
    label "logopatologia"
  ]
  node [
    id 6
    label "ognisko"
  ]
  node [
    id 7
    label "osteopatologia"
  ]
  node [
    id 8
    label "odezwanie_si&#281;"
  ]
  node [
    id 9
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 10
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 11
    label "psychopatologia"
  ]
  node [
    id 12
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 13
    label "przypadek"
  ]
  node [
    id 14
    label "zajmowa&#263;"
  ]
  node [
    id 15
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 16
    label "&#347;rodowisko"
  ]
  node [
    id 17
    label "zajmowanie"
  ]
  node [
    id 18
    label "badanie_histopatologiczne"
  ]
  node [
    id 19
    label "immunopatologia"
  ]
  node [
    id 20
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 21
    label "atakowanie"
  ]
  node [
    id 22
    label "gangsterski"
  ]
  node [
    id 23
    label "paleopatologia"
  ]
  node [
    id 24
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 25
    label "remisja"
  ]
  node [
    id 26
    label "grupa_ryzyka"
  ]
  node [
    id 27
    label "atakowa&#263;"
  ]
  node [
    id 28
    label "kryzys"
  ]
  node [
    id 29
    label "nabawienie_si&#281;"
  ]
  node [
    id 30
    label "abnormality"
  ]
  node [
    id 31
    label "przemoc"
  ]
  node [
    id 32
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 33
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 34
    label "inkubacja"
  ]
  node [
    id 35
    label "meteoropatologia"
  ]
  node [
    id 36
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 37
    label "patolnia"
  ]
  node [
    id 38
    label "powalenie"
  ]
  node [
    id 39
    label "fizjologia_patologiczna"
  ]
  node [
    id 40
    label "nabawianie_si&#281;"
  ]
  node [
    id 41
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 42
    label "medycyna"
  ]
  node [
    id 43
    label "szambo"
  ]
  node [
    id 44
    label "odzywanie_si&#281;"
  ]
  node [
    id 45
    label "diagnoza"
  ]
  node [
    id 46
    label "patomorfologia"
  ]
  node [
    id 47
    label "patogeneza"
  ]
  node [
    id 48
    label "aspo&#322;eczny"
  ]
  node [
    id 49
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 50
    label "underworld"
  ]
  node [
    id 51
    label "neuropatologia"
  ]
  node [
    id 52
    label "powali&#263;"
  ]
  node [
    id 53
    label "zaburzenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
]
