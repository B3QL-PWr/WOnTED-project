graph [
  maxDegree 3
  minDegree 0
  meanDegree 1.6153846153846154
  density 0.06461538461538462
  graphCliqueNumber 4
  node [
    id 0
    label "ar&#322;am&#243;w"
    origin "text"
  ]
  node [
    id 1
    label "Ustrzyki"
  ]
  node [
    id 2
    label "dolny"
  ]
  node [
    id 3
    label "pog&#243;rze"
  ]
  node [
    id 4
    label "przemyski"
  ]
  node [
    id 5
    label "Jan"
  ]
  node [
    id 6
    label "Herburt"
  ]
  node [
    id 7
    label "Ar&#322;amowski"
  ]
  node [
    id 8
    label "akcja"
  ]
  node [
    id 9
    label "Wis&#322;a"
  ]
  node [
    id 10
    label "o&#347;rodek"
  ]
  node [
    id 11
    label "wypoczynkowy"
  ]
  node [
    id 12
    label "urz&#261;d"
  ]
  node [
    id 13
    label "rada"
  ]
  node [
    id 14
    label "minister"
  ]
  node [
    id 15
    label "wyspa"
  ]
  node [
    id 16
    label "2"
  ]
  node [
    id 17
    label "1"
  ]
  node [
    id 18
    label "Jamno"
  ]
  node [
    id 19
    label "g&#243;rny"
  ]
  node [
    id 20
    label "lecha"
  ]
  node [
    id 21
    label "wa&#322;&#281;sa&#263;"
  ]
  node [
    id 22
    label "fabryka"
  ]
  node [
    id 23
    label "urz&#261;dzenie"
  ]
  node [
    id 24
    label "mechaniczny"
  ]
  node [
    id 25
    label "KAMAX"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
]
