graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "depresja"
    origin "text"
  ]
  node [
    id 1
    label "heheszki"
    origin "text"
  ]
  node [
    id 2
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 3
    label "deprecha"
  ]
  node [
    id 4
    label "obni&#380;enie"
  ]
  node [
    id 5
    label "zesp&#243;&#322;_napi&#281;cia_przedmiesi&#261;czkowego"
  ]
  node [
    id 6
    label "zjawisko"
  ]
  node [
    id 7
    label "choroba_ducha"
  ]
  node [
    id 8
    label "choroba_psychiczna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
]
