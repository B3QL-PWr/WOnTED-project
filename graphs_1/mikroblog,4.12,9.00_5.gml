graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.13333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 2
    label "bogactwo"
    origin "text"
  ]
  node [
    id 3
    label "pieniadze"
    origin "text"
  ]
  node [
    id 4
    label "memy"
    origin "text"
  ]
  node [
    id 5
    label "podostatek"
  ]
  node [
    id 6
    label "fortune"
  ]
  node [
    id 7
    label "cecha"
  ]
  node [
    id 8
    label "wysyp"
  ]
  node [
    id 9
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 10
    label "mienie"
  ]
  node [
    id 11
    label "fullness"
  ]
  node [
    id 12
    label "ilo&#347;&#263;"
  ]
  node [
    id 13
    label "sytuacja"
  ]
  node [
    id 14
    label "z&#322;ote_czasy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
]
