graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "sukces"
    origin "text"
  ]
  node [
    id 1
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 2
    label "kamizelka"
    origin "text"
  ]
  node [
    id 3
    label "success"
  ]
  node [
    id 4
    label "passa"
  ]
  node [
    id 5
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 6
    label "kobieta_sukcesu"
  ]
  node [
    id 7
    label "rezultat"
  ]
  node [
    id 8
    label "jasny"
  ]
  node [
    id 9
    label "typ_mongoloidalny"
  ]
  node [
    id 10
    label "kolorowy"
  ]
  node [
    id 11
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 12
    label "ciep&#322;y"
  ]
  node [
    id 13
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 14
    label "westa"
  ]
  node [
    id 15
    label "g&#243;ra"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
]
