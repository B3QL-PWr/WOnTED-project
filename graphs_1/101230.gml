graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.7894736842105263
  density 0.09941520467836257
  graphCliqueNumber 2
  node [
    id 0
    label "antonio"
    origin "text"
  ]
  node [
    id 1
    label "imi&#281;"
    origin "text"
  ]
  node [
    id 2
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 3
    label "reputacja"
  ]
  node [
    id 4
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 5
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 6
    label "patron"
  ]
  node [
    id 7
    label "nazwa_w&#322;asna"
  ]
  node [
    id 8
    label "deklinacja"
  ]
  node [
    id 9
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 10
    label "imiennictwo"
  ]
  node [
    id 11
    label "wezwanie"
  ]
  node [
    id 12
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "personalia"
  ]
  node [
    id 14
    label "term"
  ]
  node [
    id 15
    label "leksem"
  ]
  node [
    id 16
    label "wielko&#347;&#263;"
  ]
  node [
    id 17
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 18
    label "katolicki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 17
    target 18
  ]
]
