graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9642857142857142
  density 0.03571428571428571
  graphCliqueNumber 2
  node [
    id 0
    label "pis"
    origin "text"
  ]
  node [
    id 1
    label "swoje"
    origin "text"
  ]
  node [
    id 2
    label "wobec"
    origin "text"
  ]
  node [
    id 3
    label "farma"
    origin "text"
  ]
  node [
    id 4
    label "s&#322;oneczny"
    origin "text"
  ]
  node [
    id 5
    label "wiatrowy"
    origin "text"
  ]
  node [
    id 6
    label "inny"
    origin "text"
  ]
  node [
    id 7
    label "oze"
    origin "text"
  ]
  node [
    id 8
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wygini&#281;cie"
    origin "text"
  ]
  node [
    id 10
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 11
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 12
    label "gospodarstwo_rolne"
  ]
  node [
    id 13
    label "bezchmurny"
  ]
  node [
    id 14
    label "bezdeszczowy"
  ]
  node [
    id 15
    label "s&#322;onecznie"
  ]
  node [
    id 16
    label "jasny"
  ]
  node [
    id 17
    label "fotowoltaiczny"
  ]
  node [
    id 18
    label "pogodny"
  ]
  node [
    id 19
    label "weso&#322;y"
  ]
  node [
    id 20
    label "ciep&#322;y"
  ]
  node [
    id 21
    label "letni"
  ]
  node [
    id 22
    label "kolejny"
  ]
  node [
    id 23
    label "inaczej"
  ]
  node [
    id 24
    label "r&#243;&#380;ny"
  ]
  node [
    id 25
    label "inszy"
  ]
  node [
    id 26
    label "osobno"
  ]
  node [
    id 27
    label "poprowadzi&#263;"
  ]
  node [
    id 28
    label "spowodowa&#263;"
  ]
  node [
    id 29
    label "pos&#322;a&#263;"
  ]
  node [
    id 30
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 31
    label "wykona&#263;"
  ]
  node [
    id 32
    label "wzbudzi&#263;"
  ]
  node [
    id 33
    label "wprowadzi&#263;"
  ]
  node [
    id 34
    label "set"
  ]
  node [
    id 35
    label "take"
  ]
  node [
    id 36
    label "carry"
  ]
  node [
    id 37
    label "pomarcie"
  ]
  node [
    id 38
    label "extinction"
  ]
  node [
    id 39
    label "Samojedzi"
  ]
  node [
    id 40
    label "nacja"
  ]
  node [
    id 41
    label "Aztekowie"
  ]
  node [
    id 42
    label "Irokezi"
  ]
  node [
    id 43
    label "Buriaci"
  ]
  node [
    id 44
    label "Komancze"
  ]
  node [
    id 45
    label "t&#322;um"
  ]
  node [
    id 46
    label "ludno&#347;&#263;"
  ]
  node [
    id 47
    label "lud"
  ]
  node [
    id 48
    label "Siuksowie"
  ]
  node [
    id 49
    label "Czejenowie"
  ]
  node [
    id 50
    label "Wotiacy"
  ]
  node [
    id 51
    label "Baszkirzy"
  ]
  node [
    id 52
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 53
    label "Mohikanie"
  ]
  node [
    id 54
    label "Apacze"
  ]
  node [
    id 55
    label "Syngalezi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
]
