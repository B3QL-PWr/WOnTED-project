graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8571428571428572
  density 0.14285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "podatek"
    origin "text"
  ]
  node [
    id 1
    label "progresywny"
    origin "text"
  ]
  node [
    id 2
    label "bilans_handlowy"
  ]
  node [
    id 3
    label "op&#322;ata"
  ]
  node [
    id 4
    label "danina"
  ]
  node [
    id 5
    label "trybut"
  ]
  node [
    id 6
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 7
    label "nowoczesny"
  ]
  node [
    id 8
    label "progresywnie"
  ]
  node [
    id 9
    label "skomplikowany"
  ]
  node [
    id 10
    label "ambitny"
  ]
  node [
    id 11
    label "rozwojowy"
  ]
  node [
    id 12
    label "innowacyjny"
  ]
  node [
    id 13
    label "stopniowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
]
