graph [
  maxDegree 35
  minDegree 1
  meanDegree 1.9772727272727273
  density 0.022727272727272728
  graphCliqueNumber 2
  node [
    id 0
    label "kurs"
    origin "text"
  ]
  node [
    id 1
    label "kryptowaluta"
    origin "text"
  ]
  node [
    id 2
    label "spada&#263;"
    origin "text"
  ]
  node [
    id 3
    label "&#322;eb"
    origin "text"
  ]
  node [
    id 4
    label "szyja"
    origin "text"
  ]
  node [
    id 5
    label "seria"
  ]
  node [
    id 6
    label "stawka"
  ]
  node [
    id 7
    label "course"
  ]
  node [
    id 8
    label "way"
  ]
  node [
    id 9
    label "zaj&#281;cia"
  ]
  node [
    id 10
    label "grupa"
  ]
  node [
    id 11
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 12
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 13
    label "rok"
  ]
  node [
    id 14
    label "Lira"
  ]
  node [
    id 15
    label "cedu&#322;a"
  ]
  node [
    id 16
    label "zwy&#380;kowanie"
  ]
  node [
    id 17
    label "spos&#243;b"
  ]
  node [
    id 18
    label "passage"
  ]
  node [
    id 19
    label "deprecjacja"
  ]
  node [
    id 20
    label "przejazd"
  ]
  node [
    id 21
    label "drive"
  ]
  node [
    id 22
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 23
    label "bearing"
  ]
  node [
    id 24
    label "przeorientowywa&#263;"
  ]
  node [
    id 25
    label "trasa"
  ]
  node [
    id 26
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 27
    label "manner"
  ]
  node [
    id 28
    label "nauka"
  ]
  node [
    id 29
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 30
    label "przeorientowa&#263;"
  ]
  node [
    id 31
    label "kierunek"
  ]
  node [
    id 32
    label "przeorientowanie"
  ]
  node [
    id 33
    label "zni&#380;kowanie"
  ]
  node [
    id 34
    label "przeorientowywanie"
  ]
  node [
    id 35
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 36
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 37
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 38
    label "klasa"
  ]
  node [
    id 39
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 40
    label "kryptograficzny"
  ]
  node [
    id 41
    label "jednostka_monetarna"
  ]
  node [
    id 42
    label "pieni&#261;dz_elektroniczny"
  ]
  node [
    id 43
    label "wisie&#263;"
  ]
  node [
    id 44
    label "spieprza&#263;_si&#281;"
  ]
  node [
    id 45
    label "opuszcza&#263;"
  ]
  node [
    id 46
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 47
    label "condescend"
  ]
  node [
    id 48
    label "sag"
  ]
  node [
    id 49
    label "opuszcza&#263;_si&#281;"
  ]
  node [
    id 50
    label "obci&#261;&#380;a&#263;"
  ]
  node [
    id 51
    label "refuse"
  ]
  node [
    id 52
    label "lecie&#263;"
  ]
  node [
    id 53
    label "chudn&#261;&#263;"
  ]
  node [
    id 54
    label "ucieka&#263;"
  ]
  node [
    id 55
    label "spotyka&#263;"
  ]
  node [
    id 56
    label "tumble"
  ]
  node [
    id 57
    label "obni&#380;a&#263;_si&#281;"
  ]
  node [
    id 58
    label "fall"
  ]
  node [
    id 59
    label "zmniejsza&#263;_si&#281;"
  ]
  node [
    id 60
    label "noosfera"
  ]
  node [
    id 61
    label "mak&#243;wka"
  ]
  node [
    id 62
    label "alkohol"
  ]
  node [
    id 63
    label "morda"
  ]
  node [
    id 64
    label "wiedza"
  ]
  node [
    id 65
    label "cecha"
  ]
  node [
    id 66
    label "g&#322;owa"
  ]
  node [
    id 67
    label "zdolno&#347;&#263;"
  ]
  node [
    id 68
    label "czaszka"
  ]
  node [
    id 69
    label "zwierz&#281;"
  ]
  node [
    id 70
    label "dynia"
  ]
  node [
    id 71
    label "umys&#322;"
  ]
  node [
    id 72
    label "cz&#322;onek"
  ]
  node [
    id 73
    label "gruczo&#322;_tarczycowy"
  ]
  node [
    id 74
    label "grdyka"
  ]
  node [
    id 75
    label "kark"
  ]
  node [
    id 76
    label "&#380;y&#322;a_szyjna_przednia"
  ]
  node [
    id 77
    label "d&#243;&#322;_nadobojczykowy"
  ]
  node [
    id 78
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 79
    label "&#380;y&#322;a_szyjna_zewn&#281;trzna"
  ]
  node [
    id 80
    label "&#380;y&#322;a_szyjna_wewn&#281;trzna"
  ]
  node [
    id 81
    label "gardziel"
  ]
  node [
    id 82
    label "neck"
  ]
  node [
    id 83
    label "mi&#281;sie&#324;_pochy&#322;y"
  ]
  node [
    id 84
    label "podgardle"
  ]
  node [
    id 85
    label "przedbramie"
  ]
  node [
    id 86
    label "mi&#281;sie&#324;_mostkowo-obojczykowo-sutkowy"
  ]
  node [
    id 87
    label "nerw_przeponowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
]
