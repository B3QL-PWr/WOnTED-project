graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "materia&#322;"
    origin "text"
  ]
  node [
    id 1
    label "wearechange"
    origin "text"
  ]
  node [
    id 2
    label "krajka"
  ]
  node [
    id 3
    label "cz&#322;owiek"
  ]
  node [
    id 4
    label "tworzywo"
  ]
  node [
    id 5
    label "krajalno&#347;&#263;"
  ]
  node [
    id 6
    label "archiwum"
  ]
  node [
    id 7
    label "kandydat"
  ]
  node [
    id 8
    label "bielarnia"
  ]
  node [
    id 9
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 10
    label "dane"
  ]
  node [
    id 11
    label "materia"
  ]
  node [
    id 12
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 13
    label "substancja"
  ]
  node [
    id 14
    label "nawil&#380;arka"
  ]
  node [
    id 15
    label "dyspozycja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
]
