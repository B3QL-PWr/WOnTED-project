graph [
  maxDegree 32
  minDegree 1
  meanDegree 1.9444444444444444
  density 0.027386541471048513
  graphCliqueNumber 2
  node [
    id 0
    label "wymiana"
    origin "text"
  ]
  node [
    id 1
    label "ogie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "wzajemny"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "zast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 6
    label "palec"
    origin "text"
  ]
  node [
    id 7
    label "zjawisko_fonetyczne"
  ]
  node [
    id 8
    label "zamiana"
  ]
  node [
    id 9
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 10
    label "implicite"
  ]
  node [
    id 11
    label "ruch"
  ]
  node [
    id 12
    label "handel"
  ]
  node [
    id 13
    label "deal"
  ]
  node [
    id 14
    label "exchange"
  ]
  node [
    id 15
    label "wydarzenie"
  ]
  node [
    id 16
    label "explicite"
  ]
  node [
    id 17
    label "szachy"
  ]
  node [
    id 18
    label "zarzewie"
  ]
  node [
    id 19
    label "rozpalenie"
  ]
  node [
    id 20
    label "rozpalanie"
  ]
  node [
    id 21
    label "nowotw&#243;r_niez&#322;o&#347;liwy"
  ]
  node [
    id 22
    label "ardor"
  ]
  node [
    id 23
    label "atak"
  ]
  node [
    id 24
    label "palenie"
  ]
  node [
    id 25
    label "deszcz"
  ]
  node [
    id 26
    label "zapalenie"
  ]
  node [
    id 27
    label "light"
  ]
  node [
    id 28
    label "pali&#263;_si&#281;"
  ]
  node [
    id 29
    label "hell"
  ]
  node [
    id 30
    label "znami&#281;"
  ]
  node [
    id 31
    label "palenie_si&#281;"
  ]
  node [
    id 32
    label "sk&#243;ra"
  ]
  node [
    id 33
    label "iskra"
  ]
  node [
    id 34
    label "incandescence"
  ]
  node [
    id 35
    label "akcesorium"
  ]
  node [
    id 36
    label "&#347;wiat&#322;o"
  ]
  node [
    id 37
    label "co&#347;"
  ]
  node [
    id 38
    label "rumieniec"
  ]
  node [
    id 39
    label "fire"
  ]
  node [
    id 40
    label "przyp&#322;yw"
  ]
  node [
    id 41
    label "kolor"
  ]
  node [
    id 42
    label "&#380;ywio&#322;"
  ]
  node [
    id 43
    label "energia"
  ]
  node [
    id 44
    label "p&#322;omie&#324;"
  ]
  node [
    id 45
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 46
    label "ciep&#322;o"
  ]
  node [
    id 47
    label "war"
  ]
  node [
    id 48
    label "zajemny"
  ]
  node [
    id 49
    label "wzajemnie"
  ]
  node [
    id 50
    label "zobop&#243;lny"
  ]
  node [
    id 51
    label "wsp&#243;lny"
  ]
  node [
    id 52
    label "zmienia&#263;"
  ]
  node [
    id 53
    label "decydowa&#263;"
  ]
  node [
    id 54
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 55
    label "dziewczynka"
  ]
  node [
    id 56
    label "dziewczyna"
  ]
  node [
    id 57
    label "dzia&#322;anie"
  ]
  node [
    id 58
    label "poduszka"
  ]
  node [
    id 59
    label "koniuszek_palca"
  ]
  node [
    id 60
    label "zap&#322;on"
  ]
  node [
    id 61
    label "paznokie&#263;"
  ]
  node [
    id 62
    label "polidaktylia"
  ]
  node [
    id 63
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 64
    label "knykie&#263;"
  ]
  node [
    id 65
    label "pazur"
  ]
  node [
    id 66
    label "d&#322;o&#324;"
  ]
  node [
    id 67
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 68
    label "palpacja"
  ]
  node [
    id 69
    label "element_anatomiczny"
  ]
  node [
    id 70
    label "Przemys&#322;aw"
  ]
  node [
    id 71
    label "W&#243;jtowicz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 70
    target 71
  ]
]
