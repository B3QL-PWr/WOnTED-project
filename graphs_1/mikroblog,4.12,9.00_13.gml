graph [
  maxDegree 30
  minDegree 1
  meanDegree 1.9696969696969697
  density 0.030303030303030304
  graphCliqueNumber 2
  node [
    id 0
    label "da&#263;"
    origin "text"
  ]
  node [
    id 1
    label "plus"
    origin "text"
  ]
  node [
    id 2
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 3
    label "kocha&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 5
    label "mama"
    origin "text"
  ]
  node [
    id 6
    label "dostarczy&#263;"
  ]
  node [
    id 7
    label "obieca&#263;"
  ]
  node [
    id 8
    label "pozwoli&#263;"
  ]
  node [
    id 9
    label "przeznaczy&#263;"
  ]
  node [
    id 10
    label "doda&#263;"
  ]
  node [
    id 11
    label "give"
  ]
  node [
    id 12
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 13
    label "wyrzec_si&#281;"
  ]
  node [
    id 14
    label "supply"
  ]
  node [
    id 15
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 16
    label "zada&#263;"
  ]
  node [
    id 17
    label "odst&#261;pi&#263;"
  ]
  node [
    id 18
    label "feed"
  ]
  node [
    id 19
    label "testify"
  ]
  node [
    id 20
    label "powierzy&#263;"
  ]
  node [
    id 21
    label "convey"
  ]
  node [
    id 22
    label "przekaza&#263;"
  ]
  node [
    id 23
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 24
    label "zap&#322;aci&#263;"
  ]
  node [
    id 25
    label "dress"
  ]
  node [
    id 26
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 27
    label "udost&#281;pni&#263;"
  ]
  node [
    id 28
    label "sztachn&#261;&#263;"
  ]
  node [
    id 29
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 30
    label "zrobi&#263;"
  ]
  node [
    id 31
    label "przywali&#263;"
  ]
  node [
    id 32
    label "rap"
  ]
  node [
    id 33
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 34
    label "picture"
  ]
  node [
    id 35
    label "warto&#347;&#263;"
  ]
  node [
    id 36
    label "wabik"
  ]
  node [
    id 37
    label "rewaluowa&#263;"
  ]
  node [
    id 38
    label "korzy&#347;&#263;"
  ]
  node [
    id 39
    label "dodawanie"
  ]
  node [
    id 40
    label "rewaluowanie"
  ]
  node [
    id 41
    label "stopie&#324;"
  ]
  node [
    id 42
    label "ocena"
  ]
  node [
    id 43
    label "zrewaluowa&#263;"
  ]
  node [
    id 44
    label "liczba"
  ]
  node [
    id 45
    label "znak_matematyczny"
  ]
  node [
    id 46
    label "strona"
  ]
  node [
    id 47
    label "zrewaluowanie"
  ]
  node [
    id 48
    label "like"
  ]
  node [
    id 49
    label "czu&#263;"
  ]
  node [
    id 50
    label "chowa&#263;"
  ]
  node [
    id 51
    label "mi&#322;owa&#263;"
  ]
  node [
    id 52
    label "cz&#322;owiek"
  ]
  node [
    id 53
    label "bli&#378;ni"
  ]
  node [
    id 54
    label "odpowiedni"
  ]
  node [
    id 55
    label "swojak"
  ]
  node [
    id 56
    label "samodzielny"
  ]
  node [
    id 57
    label "matczysko"
  ]
  node [
    id 58
    label "macierz"
  ]
  node [
    id 59
    label "przodkini"
  ]
  node [
    id 60
    label "Matka_Boska"
  ]
  node [
    id 61
    label "macocha"
  ]
  node [
    id 62
    label "matka_zast&#281;pcza"
  ]
  node [
    id 63
    label "stara"
  ]
  node [
    id 64
    label "rodzice"
  ]
  node [
    id 65
    label "rodzic"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
]
