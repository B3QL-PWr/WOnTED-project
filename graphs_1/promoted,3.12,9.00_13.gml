graph [
  maxDegree 42
  minDegree 1
  meanDegree 1.9591836734693877
  density 0.04081632653061224
  graphCliqueNumber 2
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "opis"
    origin "text"
  ]
  node [
    id 2
    label "stan"
    origin "text"
  ]
  node [
    id 3
    label "waszyngton"
    origin "text"
  ]
  node [
    id 4
    label "usa"
    origin "text"
  ]
  node [
    id 5
    label "exposition"
  ]
  node [
    id 6
    label "czynno&#347;&#263;"
  ]
  node [
    id 7
    label "wypowied&#378;"
  ]
  node [
    id 8
    label "obja&#347;nienie"
  ]
  node [
    id 9
    label "Arizona"
  ]
  node [
    id 10
    label "Georgia"
  ]
  node [
    id 11
    label "warstwa"
  ]
  node [
    id 12
    label "jednostka_administracyjna"
  ]
  node [
    id 13
    label "Goa"
  ]
  node [
    id 14
    label "Hawaje"
  ]
  node [
    id 15
    label "Floryda"
  ]
  node [
    id 16
    label "Oklahoma"
  ]
  node [
    id 17
    label "punkt"
  ]
  node [
    id 18
    label "Alaska"
  ]
  node [
    id 19
    label "Alabama"
  ]
  node [
    id 20
    label "wci&#281;cie"
  ]
  node [
    id 21
    label "Oregon"
  ]
  node [
    id 22
    label "poziom"
  ]
  node [
    id 23
    label "by&#263;"
  ]
  node [
    id 24
    label "Teksas"
  ]
  node [
    id 25
    label "Illinois"
  ]
  node [
    id 26
    label "Jukatan"
  ]
  node [
    id 27
    label "Waszyngton"
  ]
  node [
    id 28
    label "shape"
  ]
  node [
    id 29
    label "Nowy_Meksyk"
  ]
  node [
    id 30
    label "ilo&#347;&#263;"
  ]
  node [
    id 31
    label "state"
  ]
  node [
    id 32
    label "Nowy_York"
  ]
  node [
    id 33
    label "Arakan"
  ]
  node [
    id 34
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 35
    label "Kalifornia"
  ]
  node [
    id 36
    label "wektor"
  ]
  node [
    id 37
    label "Massachusetts"
  ]
  node [
    id 38
    label "miejsce"
  ]
  node [
    id 39
    label "Pensylwania"
  ]
  node [
    id 40
    label "Maryland"
  ]
  node [
    id 41
    label "Michigan"
  ]
  node [
    id 42
    label "Ohio"
  ]
  node [
    id 43
    label "Kansas"
  ]
  node [
    id 44
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 45
    label "Luizjana"
  ]
  node [
    id 46
    label "samopoczucie"
  ]
  node [
    id 47
    label "Wirginia"
  ]
  node [
    id 48
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 3
    target 4
  ]
]
