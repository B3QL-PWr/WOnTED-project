graph [
  maxDegree 37
  minDegree 1
  meanDegree 2.2853025936599423
  density 0.003297694940346237
  graphCliqueNumber 5
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "wysoki"
    origin "text"
  ]
  node [
    id 3
    label "izba"
    origin "text"
  ]
  node [
    id 4
    label "trybuna&#322;"
    origin "text"
  ]
  node [
    id 5
    label "konstytucyjny"
    origin "text"
  ]
  node [
    id 6
    label "wyrok"
    origin "text"
  ]
  node [
    id 7
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 8
    label "rocznik"
    origin "text"
  ]
  node [
    id 9
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "metr"
    origin "text"
  ]
  node [
    id 11
    label "jeden"
    origin "text"
  ]
  node [
    id 12
    label "podstawowy"
    origin "text"
  ]
  node [
    id 13
    label "przes&#322;anka"
    origin "text"
  ]
  node [
    id 14
    label "wype&#322;nienie"
    origin "text"
  ]
  node [
    id 15
    label "ustawowo"
    origin "text"
  ]
  node [
    id 16
    label "okre&#347;lony"
    origin "text"
  ]
  node [
    id 17
    label "misja"
    origin "text"
  ]
  node [
    id 18
    label "przez"
    origin "text"
  ]
  node [
    id 19
    label "media"
    origin "text"
  ]
  node [
    id 20
    label "publiczny"
    origin "text"
  ]
  node [
    id 21
    label "by&#263;"
    origin "text"
  ]
  node [
    id 22
    label "w&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 23
    label "zasada"
    origin "text"
  ]
  node [
    id 24
    label "funkcjonowanie"
    origin "text"
  ]
  node [
    id 25
    label "tymczasem"
    origin "text"
  ]
  node [
    id 26
    label "sp&#243;&#322;ka"
    origin "text"
  ]
  node [
    id 27
    label "radiofonia"
    origin "text"
  ]
  node [
    id 28
    label "telewizja"
    origin "text"
  ]
  node [
    id 29
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "coraz"
    origin "text"
  ]
  node [
    id 31
    label "mniejszy"
    origin "text"
  ]
  node [
    id 32
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 33
    label "finansowy"
    origin "text"
  ]
  node [
    id 34
    label "zdecydowanie"
    origin "text"
  ]
  node [
    id 35
    label "pogarsza&#263;"
    origin "text"
  ]
  node [
    id 36
    label "sytuacja"
    origin "text"
  ]
  node [
    id 37
    label "finansowo"
    origin "text"
  ]
  node [
    id 38
    label "ekonomiczny"
    origin "text"
  ]
  node [
    id 39
    label "nadawca"
    origin "text"
  ]
  node [
    id 40
    label "szczeg&#243;lnie"
    origin "text"
  ]
  node [
    id 41
    label "tychy"
    origin "text"
  ]
  node [
    id 42
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 43
    label "obecnie"
    origin "text"
  ]
  node [
    id 44
    label "drastyczny"
    origin "text"
  ]
  node [
    id 45
    label "ci&#281;cie"
    origin "text"
  ]
  node [
    id 46
    label "bud&#380;etowy"
    origin "text"
  ]
  node [
    id 47
    label "zwolnienie"
    origin "text"
  ]
  node [
    id 48
    label "pracownik"
    origin "text"
  ]
  node [
    id 49
    label "w&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 50
    label "za&#322;amanie"
    origin "text"
  ]
  node [
    id 51
    label "poziom"
    origin "text"
  ]
  node [
    id 52
    label "inkasowa&#263;"
    origin "text"
  ]
  node [
    id 53
    label "wp&#322;yw"
    origin "text"
  ]
  node [
    id 54
    label "abonamentowy"
    origin "text"
  ]
  node [
    id 55
    label "nast&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 56
    label "zapowied&#378;"
    origin "text"
  ]
  node [
    id 57
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 58
    label "ustawowy"
    origin "text"
  ]
  node [
    id 59
    label "op&#322;ata"
    origin "text"
  ]
  node [
    id 60
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 61
    label "grupa"
    origin "text"
  ]
  node [
    id 62
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 63
    label "ca&#322;kowity"
    origin "text"
  ]
  node [
    id 64
    label "zniesienie"
    origin "text"
  ]
  node [
    id 65
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 66
    label "tym"
    origin "text"
  ]
  node [
    id 67
    label "koalicyjny"
    origin "text"
  ]
  node [
    id 68
    label "czuj"
    origin "text"
  ]
  node [
    id 69
    label "si&#281;"
    origin "text"
  ]
  node [
    id 70
    label "odpowiedzialny"
    origin "text"
  ]
  node [
    id 71
    label "pogr&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 72
    label "obecna"
    origin "text"
  ]
  node [
    id 73
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 74
    label "mo&#380;liwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 75
    label "naprawienie"
    origin "text"
  ]
  node [
    id 76
    label "swoje"
    origin "text"
  ]
  node [
    id 77
    label "nieodpowiedzialny"
    origin "text"
  ]
  node [
    id 78
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 79
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 80
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 81
    label "zapa&#347;&#263;"
    origin "text"
  ]
  node [
    id 82
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 83
    label "bardzo"
    origin "text"
  ]
  node [
    id 84
    label "oklask"
    origin "text"
  ]
  node [
    id 85
    label "cz&#322;owiek"
  ]
  node [
    id 86
    label "profesor"
  ]
  node [
    id 87
    label "kszta&#322;ciciel"
  ]
  node [
    id 88
    label "jegomo&#347;&#263;"
  ]
  node [
    id 89
    label "zwrot"
  ]
  node [
    id 90
    label "pracodawca"
  ]
  node [
    id 91
    label "rz&#261;dzenie"
  ]
  node [
    id 92
    label "m&#261;&#380;"
  ]
  node [
    id 93
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 94
    label "ch&#322;opina"
  ]
  node [
    id 95
    label "bratek"
  ]
  node [
    id 96
    label "opiekun"
  ]
  node [
    id 97
    label "doros&#322;y"
  ]
  node [
    id 98
    label "preceptor"
  ]
  node [
    id 99
    label "Midas"
  ]
  node [
    id 100
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 101
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 102
    label "murza"
  ]
  node [
    id 103
    label "ojciec"
  ]
  node [
    id 104
    label "androlog"
  ]
  node [
    id 105
    label "pupil"
  ]
  node [
    id 106
    label "efendi"
  ]
  node [
    id 107
    label "nabab"
  ]
  node [
    id 108
    label "w&#322;odarz"
  ]
  node [
    id 109
    label "szkolnik"
  ]
  node [
    id 110
    label "pedagog"
  ]
  node [
    id 111
    label "popularyzator"
  ]
  node [
    id 112
    label "andropauza"
  ]
  node [
    id 113
    label "gra_w_karty"
  ]
  node [
    id 114
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 115
    label "Mieszko_I"
  ]
  node [
    id 116
    label "bogaty"
  ]
  node [
    id 117
    label "samiec"
  ]
  node [
    id 118
    label "przyw&#243;dca"
  ]
  node [
    id 119
    label "pa&#324;stwo"
  ]
  node [
    id 120
    label "belfer"
  ]
  node [
    id 121
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 122
    label "dostojnik"
  ]
  node [
    id 123
    label "oficer"
  ]
  node [
    id 124
    label "parlamentarzysta"
  ]
  node [
    id 125
    label "Pi&#322;sudski"
  ]
  node [
    id 126
    label "warto&#347;ciowy"
  ]
  node [
    id 127
    label "wysoce"
  ]
  node [
    id 128
    label "daleki"
  ]
  node [
    id 129
    label "znaczny"
  ]
  node [
    id 130
    label "wysoko"
  ]
  node [
    id 131
    label "szczytnie"
  ]
  node [
    id 132
    label "wznios&#322;y"
  ]
  node [
    id 133
    label "wyrafinowany"
  ]
  node [
    id 134
    label "z_wysoka"
  ]
  node [
    id 135
    label "chwalebny"
  ]
  node [
    id 136
    label "uprzywilejowany"
  ]
  node [
    id 137
    label "niepo&#347;ledni"
  ]
  node [
    id 138
    label "pok&#243;j"
  ]
  node [
    id 139
    label "parlament"
  ]
  node [
    id 140
    label "NIK"
  ]
  node [
    id 141
    label "urz&#261;d"
  ]
  node [
    id 142
    label "organ"
  ]
  node [
    id 143
    label "pomieszczenie"
  ]
  node [
    id 144
    label "Europejski_Trybuna&#322;_Praw_Cz&#322;owieka"
  ]
  node [
    id 145
    label "s&#261;d"
  ]
  node [
    id 146
    label "konstytucyjnie"
  ]
  node [
    id 147
    label "orzeczenie"
  ]
  node [
    id 148
    label "order"
  ]
  node [
    id 149
    label "wydarzenie"
  ]
  node [
    id 150
    label "kara"
  ]
  node [
    id 151
    label "judgment"
  ]
  node [
    id 152
    label "sentencja"
  ]
  node [
    id 153
    label "miesi&#261;c"
  ]
  node [
    id 154
    label "formacja"
  ]
  node [
    id 155
    label "kronika"
  ]
  node [
    id 156
    label "czasopismo"
  ]
  node [
    id 157
    label "yearbook"
  ]
  node [
    id 158
    label "powiedzie&#263;"
  ]
  node [
    id 159
    label "testify"
  ]
  node [
    id 160
    label "uzna&#263;"
  ]
  node [
    id 161
    label "oznajmi&#263;"
  ]
  node [
    id 162
    label "declare"
  ]
  node [
    id 163
    label "meter"
  ]
  node [
    id 164
    label "decymetr"
  ]
  node [
    id 165
    label "megabyte"
  ]
  node [
    id 166
    label "plon"
  ]
  node [
    id 167
    label "metrum"
  ]
  node [
    id 168
    label "dekametr"
  ]
  node [
    id 169
    label "jednostka_powierzchni"
  ]
  node [
    id 170
    label "uk&#322;ad_SI"
  ]
  node [
    id 171
    label "literaturoznawstwo"
  ]
  node [
    id 172
    label "wiersz"
  ]
  node [
    id 173
    label "gigametr"
  ]
  node [
    id 174
    label "miara"
  ]
  node [
    id 175
    label "nauczyciel"
  ]
  node [
    id 176
    label "kilometr_kwadratowy"
  ]
  node [
    id 177
    label "jednostka_metryczna"
  ]
  node [
    id 178
    label "jednostka_masy"
  ]
  node [
    id 179
    label "centymetr_kwadratowy"
  ]
  node [
    id 180
    label "kieliszek"
  ]
  node [
    id 181
    label "shot"
  ]
  node [
    id 182
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 183
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 184
    label "jaki&#347;"
  ]
  node [
    id 185
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 186
    label "jednolicie"
  ]
  node [
    id 187
    label "w&#243;dka"
  ]
  node [
    id 188
    label "ten"
  ]
  node [
    id 189
    label "ujednolicenie"
  ]
  node [
    id 190
    label "jednakowy"
  ]
  node [
    id 191
    label "pocz&#261;tkowy"
  ]
  node [
    id 192
    label "podstawowo"
  ]
  node [
    id 193
    label "najwa&#380;niejszy"
  ]
  node [
    id 194
    label "niezaawansowany"
  ]
  node [
    id 195
    label "wnioskowanie"
  ]
  node [
    id 196
    label "przyczyna"
  ]
  node [
    id 197
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 198
    label "proces"
  ]
  node [
    id 199
    label "fakt"
  ]
  node [
    id 200
    label "zrobienie"
  ]
  node [
    id 201
    label "control"
  ]
  node [
    id 202
    label "spowodowanie"
  ]
  node [
    id 203
    label "completion"
  ]
  node [
    id 204
    label "bash"
  ]
  node [
    id 205
    label "znalezienie_si&#281;"
  ]
  node [
    id 206
    label "activity"
  ]
  node [
    id 207
    label "ziszczenie_si&#281;"
  ]
  node [
    id 208
    label "nasilenie_si&#281;"
  ]
  node [
    id 209
    label "rubryka"
  ]
  node [
    id 210
    label "woof"
  ]
  node [
    id 211
    label "poczucie"
  ]
  node [
    id 212
    label "element"
  ]
  node [
    id 213
    label "pe&#322;ny"
  ]
  node [
    id 214
    label "zdarzenie_si&#281;"
  ]
  node [
    id 215
    label "uzupe&#322;nienie"
  ]
  node [
    id 216
    label "performance"
  ]
  node [
    id 217
    label "umieszczenie"
  ]
  node [
    id 218
    label "regulaminowo"
  ]
  node [
    id 219
    label "wiadomy"
  ]
  node [
    id 220
    label "plac&#243;wka"
  ]
  node [
    id 221
    label "misje"
  ]
  node [
    id 222
    label "zadanie"
  ]
  node [
    id 223
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 224
    label "obowi&#261;zek"
  ]
  node [
    id 225
    label "reprezentacja"
  ]
  node [
    id 226
    label "przekazior"
  ]
  node [
    id 227
    label "mass-media"
  ]
  node [
    id 228
    label "uzbrajanie"
  ]
  node [
    id 229
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 230
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 231
    label "medium"
  ]
  node [
    id 232
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 233
    label "jawny"
  ]
  node [
    id 234
    label "upublicznienie"
  ]
  node [
    id 235
    label "upublicznianie"
  ]
  node [
    id 236
    label "publicznie"
  ]
  node [
    id 237
    label "si&#281;ga&#263;"
  ]
  node [
    id 238
    label "trwa&#263;"
  ]
  node [
    id 239
    label "obecno&#347;&#263;"
  ]
  node [
    id 240
    label "stan"
  ]
  node [
    id 241
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 242
    label "stand"
  ]
  node [
    id 243
    label "mie&#263;_miejsce"
  ]
  node [
    id 244
    label "uczestniczy&#263;"
  ]
  node [
    id 245
    label "chodzi&#263;"
  ]
  node [
    id 246
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 247
    label "equal"
  ]
  node [
    id 248
    label "taki"
  ]
  node [
    id 249
    label "nale&#380;yty"
  ]
  node [
    id 250
    label "charakterystyczny"
  ]
  node [
    id 251
    label "stosownie"
  ]
  node [
    id 252
    label "dobry"
  ]
  node [
    id 253
    label "prawdziwy"
  ]
  node [
    id 254
    label "uprawniony"
  ]
  node [
    id 255
    label "zasadniczy"
  ]
  node [
    id 256
    label "typowy"
  ]
  node [
    id 257
    label "nale&#380;ny"
  ]
  node [
    id 258
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 259
    label "obserwacja"
  ]
  node [
    id 260
    label "moralno&#347;&#263;"
  ]
  node [
    id 261
    label "podstawa"
  ]
  node [
    id 262
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 263
    label "umowa"
  ]
  node [
    id 264
    label "dominion"
  ]
  node [
    id 265
    label "qualification"
  ]
  node [
    id 266
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 267
    label "opis"
  ]
  node [
    id 268
    label "regu&#322;a_Allena"
  ]
  node [
    id 269
    label "normalizacja"
  ]
  node [
    id 270
    label "regu&#322;a_Glogera"
  ]
  node [
    id 271
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 272
    label "standard"
  ]
  node [
    id 273
    label "base"
  ]
  node [
    id 274
    label "substancja"
  ]
  node [
    id 275
    label "spos&#243;b"
  ]
  node [
    id 276
    label "prawid&#322;o"
  ]
  node [
    id 277
    label "prawo_Mendla"
  ]
  node [
    id 278
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 279
    label "criterion"
  ]
  node [
    id 280
    label "twierdzenie"
  ]
  node [
    id 281
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 282
    label "prawo"
  ]
  node [
    id 283
    label "occupation"
  ]
  node [
    id 284
    label "zasada_d'Alemberta"
  ]
  node [
    id 285
    label "nakr&#281;cenie"
  ]
  node [
    id 286
    label "uruchomienie"
  ]
  node [
    id 287
    label "impact"
  ]
  node [
    id 288
    label "tr&#243;jstronny"
  ]
  node [
    id 289
    label "w&#322;&#261;czenie"
  ]
  node [
    id 290
    label "uruchamianie"
  ]
  node [
    id 291
    label "dzianie_si&#281;"
  ]
  node [
    id 292
    label "funkcja"
  ]
  node [
    id 293
    label "zatrzymanie"
  ]
  node [
    id 294
    label "podtrzymywanie"
  ]
  node [
    id 295
    label "w&#322;&#261;czanie"
  ]
  node [
    id 296
    label "nakr&#281;canie"
  ]
  node [
    id 297
    label "czasowo"
  ]
  node [
    id 298
    label "wtedy"
  ]
  node [
    id 299
    label "podmiot_gospodarczy"
  ]
  node [
    id 300
    label "organizacja"
  ]
  node [
    id 301
    label "zesp&#243;&#322;"
  ]
  node [
    id 302
    label "wsp&#243;lnictwo"
  ]
  node [
    id 303
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 304
    label "radio"
  ]
  node [
    id 305
    label "radiofonizacja"
  ]
  node [
    id 306
    label "infrastruktura"
  ]
  node [
    id 307
    label "radiokomunikacja"
  ]
  node [
    id 308
    label "Polsat"
  ]
  node [
    id 309
    label "paj&#281;czarz"
  ]
  node [
    id 310
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 311
    label "programowiec"
  ]
  node [
    id 312
    label "technologia"
  ]
  node [
    id 313
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 314
    label "Interwizja"
  ]
  node [
    id 315
    label "BBC"
  ]
  node [
    id 316
    label "ekran"
  ]
  node [
    id 317
    label "redakcja"
  ]
  node [
    id 318
    label "odbieranie"
  ]
  node [
    id 319
    label "odbiera&#263;"
  ]
  node [
    id 320
    label "odbiornik"
  ]
  node [
    id 321
    label "instytucja"
  ]
  node [
    id 322
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 323
    label "studio"
  ]
  node [
    id 324
    label "telekomunikacja"
  ]
  node [
    id 325
    label "muza"
  ]
  node [
    id 326
    label "wytwarza&#263;"
  ]
  node [
    id 327
    label "take"
  ]
  node [
    id 328
    label "dostawa&#263;"
  ]
  node [
    id 329
    label "return"
  ]
  node [
    id 330
    label "inny"
  ]
  node [
    id 331
    label "zmniejszenie_si&#281;"
  ]
  node [
    id 332
    label "zmniejszanie_si&#281;"
  ]
  node [
    id 333
    label "chudni&#281;cie"
  ]
  node [
    id 334
    label "zmniejszanie"
  ]
  node [
    id 335
    label "miejsce"
  ]
  node [
    id 336
    label "czas"
  ]
  node [
    id 337
    label "abstrakcja"
  ]
  node [
    id 338
    label "punkt"
  ]
  node [
    id 339
    label "chemikalia"
  ]
  node [
    id 340
    label "mi&#281;dzybankowy"
  ]
  node [
    id 341
    label "fizyczny"
  ]
  node [
    id 342
    label "pozamaterialny"
  ]
  node [
    id 343
    label "materjalny"
  ]
  node [
    id 344
    label "zdecydowany"
  ]
  node [
    id 345
    label "oddzia&#322;anie"
  ]
  node [
    id 346
    label "cecha"
  ]
  node [
    id 347
    label "resoluteness"
  ]
  node [
    id 348
    label "decyzja"
  ]
  node [
    id 349
    label "pewnie"
  ]
  node [
    id 350
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 351
    label "podj&#281;cie"
  ]
  node [
    id 352
    label "zauwa&#380;alnie"
  ]
  node [
    id 353
    label "worsen"
  ]
  node [
    id 354
    label "zmienia&#263;"
  ]
  node [
    id 355
    label "szczeg&#243;&#322;"
  ]
  node [
    id 356
    label "motyw"
  ]
  node [
    id 357
    label "state"
  ]
  node [
    id 358
    label "realia"
  ]
  node [
    id 359
    label "warunki"
  ]
  node [
    id 360
    label "fiscally"
  ]
  node [
    id 361
    label "financially"
  ]
  node [
    id 362
    label "bytowo"
  ]
  node [
    id 363
    label "ekonomicznie"
  ]
  node [
    id 364
    label "korzystny"
  ]
  node [
    id 365
    label "oszcz&#281;dny"
  ]
  node [
    id 366
    label "podmiot"
  ]
  node [
    id 367
    label "klient"
  ]
  node [
    id 368
    label "przesy&#322;ka"
  ]
  node [
    id 369
    label "autor"
  ]
  node [
    id 370
    label "szczeg&#243;lny"
  ]
  node [
    id 371
    label "osobnie"
  ]
  node [
    id 372
    label "wyj&#261;tkowo"
  ]
  node [
    id 373
    label "specially"
  ]
  node [
    id 374
    label "chance"
  ]
  node [
    id 375
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 376
    label "alternate"
  ]
  node [
    id 377
    label "naciska&#263;"
  ]
  node [
    id 378
    label "atakowa&#263;"
  ]
  node [
    id 379
    label "ninie"
  ]
  node [
    id 380
    label "aktualny"
  ]
  node [
    id 381
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 382
    label "drastycznie"
  ]
  node [
    id 383
    label "radykalny"
  ]
  node [
    id 384
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 385
    label "przemoc"
  ]
  node [
    id 386
    label "nieprzyzwoity"
  ]
  node [
    id 387
    label "dosadny"
  ]
  node [
    id 388
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 389
    label "mocny"
  ]
  node [
    id 390
    label "robienie"
  ]
  node [
    id 391
    label "poci&#281;cie"
  ]
  node [
    id 392
    label "m&#243;wienie"
  ]
  node [
    id 393
    label "dzielenie"
  ]
  node [
    id 394
    label "wygarnianie"
  ]
  node [
    id 395
    label "okrojenie"
  ]
  node [
    id 396
    label "wci&#281;cie"
  ]
  node [
    id 397
    label "dowalanie"
  ]
  node [
    id 398
    label "retrenchment"
  ]
  node [
    id 399
    label "okrawanie"
  ]
  node [
    id 400
    label "uderzanie"
  ]
  node [
    id 401
    label "time"
  ]
  node [
    id 402
    label "przeci&#281;cie"
  ]
  node [
    id 403
    label "naci&#281;cie_si&#281;"
  ]
  node [
    id 404
    label "czynno&#347;&#263;"
  ]
  node [
    id 405
    label "wcinanie"
  ]
  node [
    id 406
    label "cut"
  ]
  node [
    id 407
    label "redukcja"
  ]
  node [
    id 408
    label "uci&#281;cie"
  ]
  node [
    id 409
    label "film_editing"
  ]
  node [
    id 410
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 411
    label "uderzenie"
  ]
  node [
    id 412
    label "k&#261;sanie"
  ]
  node [
    id 413
    label "snub"
  ]
  node [
    id 414
    label "rozci&#281;cie_si&#281;"
  ]
  node [
    id 415
    label "przecinanie"
  ]
  node [
    id 416
    label "mikrotom"
  ]
  node [
    id 417
    label "etatowy"
  ]
  node [
    id 418
    label "bud&#380;etowo"
  ]
  node [
    id 419
    label "budgetary"
  ]
  node [
    id 420
    label "oddalenie"
  ]
  node [
    id 421
    label "zmniejszenie"
  ]
  node [
    id 422
    label "wolniejszy"
  ]
  node [
    id 423
    label "nieobecno&#347;&#263;"
  ]
  node [
    id 424
    label "uwolnienie"
  ]
  node [
    id 425
    label "wypowiedzenie"
  ]
  node [
    id 426
    label "liberation"
  ]
  node [
    id 427
    label "spowolnienie"
  ]
  node [
    id 428
    label "relief"
  ]
  node [
    id 429
    label "dowolny"
  ]
  node [
    id 430
    label "ulga"
  ]
  node [
    id 431
    label "release"
  ]
  node [
    id 432
    label "za&#347;wiadczenie"
  ]
  node [
    id 433
    label "wylanie"
  ]
  node [
    id 434
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 435
    label "delegowa&#263;"
  ]
  node [
    id 436
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 437
    label "pracu&#347;"
  ]
  node [
    id 438
    label "delegowanie"
  ]
  node [
    id 439
    label "r&#281;ka"
  ]
  node [
    id 440
    label "salariat"
  ]
  node [
    id 441
    label "deprecha"
  ]
  node [
    id 442
    label "zniszczenie"
  ]
  node [
    id 443
    label "przygn&#281;bienie"
  ]
  node [
    id 444
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 445
    label "zagi&#281;cie"
  ]
  node [
    id 446
    label "p&#281;kni&#281;cie"
  ]
  node [
    id 447
    label "dislocation"
  ]
  node [
    id 448
    label "zesp&#243;&#322;_napi&#281;cia_przedmiesi&#261;czkowego"
  ]
  node [
    id 449
    label "za&#322;amanie_si&#281;"
  ]
  node [
    id 450
    label "choroba_ducha"
  ]
  node [
    id 451
    label "choroba_psychiczna"
  ]
  node [
    id 452
    label "wysoko&#347;&#263;"
  ]
  node [
    id 453
    label "faza"
  ]
  node [
    id 454
    label "szczebel"
  ]
  node [
    id 455
    label "po&#322;o&#380;enie"
  ]
  node [
    id 456
    label "kierunek"
  ]
  node [
    id 457
    label "wyk&#322;adnik"
  ]
  node [
    id 458
    label "budynek"
  ]
  node [
    id 459
    label "punkt_widzenia"
  ]
  node [
    id 460
    label "jako&#347;&#263;"
  ]
  node [
    id 461
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 462
    label "ranga"
  ]
  node [
    id 463
    label "p&#322;aszczyzna"
  ]
  node [
    id 464
    label "arise"
  ]
  node [
    id 465
    label "bra&#263;"
  ]
  node [
    id 466
    label "&#347;lad"
  ]
  node [
    id 467
    label "doch&#243;d_narodowy"
  ]
  node [
    id 468
    label "zjawisko"
  ]
  node [
    id 469
    label "rezultat"
  ]
  node [
    id 470
    label "kwota"
  ]
  node [
    id 471
    label "lobbysta"
  ]
  node [
    id 472
    label "nacisn&#261;&#263;"
  ]
  node [
    id 473
    label "zaatakowa&#263;"
  ]
  node [
    id 474
    label "gamble"
  ]
  node [
    id 475
    label "supervene"
  ]
  node [
    id 476
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 477
    label "oznaka"
  ]
  node [
    id 478
    label "przewidywanie"
  ]
  node [
    id 479
    label "declaration"
  ]
  node [
    id 480
    label "zawiadomienie"
  ]
  node [
    id 481
    label "signal"
  ]
  node [
    id 482
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 483
    label "kategoria"
  ]
  node [
    id 484
    label "egzekutywa"
  ]
  node [
    id 485
    label "gabinet_cieni"
  ]
  node [
    id 486
    label "gromada"
  ]
  node [
    id 487
    label "premier"
  ]
  node [
    id 488
    label "Londyn"
  ]
  node [
    id 489
    label "Konsulat"
  ]
  node [
    id 490
    label "uporz&#261;dkowanie"
  ]
  node [
    id 491
    label "jednostka_systematyczna"
  ]
  node [
    id 492
    label "szpaler"
  ]
  node [
    id 493
    label "przybli&#380;enie"
  ]
  node [
    id 494
    label "tract"
  ]
  node [
    id 495
    label "number"
  ]
  node [
    id 496
    label "lon&#380;a"
  ]
  node [
    id 497
    label "w&#322;adza"
  ]
  node [
    id 498
    label "klasa"
  ]
  node [
    id 499
    label "regulaminowy"
  ]
  node [
    id 500
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 501
    label "wiele"
  ]
  node [
    id 502
    label "dorodny"
  ]
  node [
    id 503
    label "du&#380;o"
  ]
  node [
    id 504
    label "niema&#322;o"
  ]
  node [
    id 505
    label "wa&#380;ny"
  ]
  node [
    id 506
    label "rozwini&#281;ty"
  ]
  node [
    id 507
    label "odm&#322;adza&#263;"
  ]
  node [
    id 508
    label "asymilowa&#263;"
  ]
  node [
    id 509
    label "cz&#261;steczka"
  ]
  node [
    id 510
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 511
    label "egzemplarz"
  ]
  node [
    id 512
    label "formacja_geologiczna"
  ]
  node [
    id 513
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 514
    label "harcerze_starsi"
  ]
  node [
    id 515
    label "liga"
  ]
  node [
    id 516
    label "Terranie"
  ]
  node [
    id 517
    label "&#346;wietliki"
  ]
  node [
    id 518
    label "pakiet_klimatyczny"
  ]
  node [
    id 519
    label "oddzia&#322;"
  ]
  node [
    id 520
    label "stage_set"
  ]
  node [
    id 521
    label "Entuzjastki"
  ]
  node [
    id 522
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 523
    label "odm&#322;odzenie"
  ]
  node [
    id 524
    label "type"
  ]
  node [
    id 525
    label "category"
  ]
  node [
    id 526
    label "asymilowanie"
  ]
  node [
    id 527
    label "specgrupa"
  ]
  node [
    id 528
    label "odm&#322;adzanie"
  ]
  node [
    id 529
    label "Eurogrupa"
  ]
  node [
    id 530
    label "kompozycja"
  ]
  node [
    id 531
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 532
    label "zbi&#243;r"
  ]
  node [
    id 533
    label "niepubliczny"
  ]
  node [
    id 534
    label "spo&#322;ecznie"
  ]
  node [
    id 535
    label "w_pizdu"
  ]
  node [
    id 536
    label "&#322;&#261;czny"
  ]
  node [
    id 537
    label "og&#243;lnie"
  ]
  node [
    id 538
    label "ca&#322;y"
  ]
  node [
    id 539
    label "zupe&#322;nie"
  ]
  node [
    id 540
    label "kompletnie"
  ]
  node [
    id 541
    label "extinction"
  ]
  node [
    id 542
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 543
    label "withdrawal"
  ]
  node [
    id 544
    label "usuni&#281;cie"
  ]
  node [
    id 545
    label "jajko"
  ]
  node [
    id 546
    label "uniewa&#380;nienie"
  ]
  node [
    id 547
    label "ranny"
  ]
  node [
    id 548
    label "abolicjonista"
  ]
  node [
    id 549
    label "suspension"
  ]
  node [
    id 550
    label "wygranie"
  ]
  node [
    id 551
    label "removal"
  ]
  node [
    id 552
    label "urodzenie"
  ]
  node [
    id 553
    label "zgromadzenie"
  ]
  node [
    id 554
    label "revocation"
  ]
  node [
    id 555
    label "zebranie"
  ]
  node [
    id 556
    label "coitus_interruptus"
  ]
  node [
    id 557
    label "&#347;cierpienie"
  ]
  node [
    id 558
    label "porwanie"
  ]
  node [
    id 559
    label "przeniesienie"
  ]
  node [
    id 560
    label "posk&#322;adanie"
  ]
  node [
    id 561
    label "poddanie_si&#281;"
  ]
  node [
    id 562
    label "przetrwanie"
  ]
  node [
    id 563
    label "odwodnienie"
  ]
  node [
    id 564
    label "konstytucja"
  ]
  node [
    id 565
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 566
    label "substancja_chemiczna"
  ]
  node [
    id 567
    label "bratnia_dusza"
  ]
  node [
    id 568
    label "zwi&#261;zanie"
  ]
  node [
    id 569
    label "lokant"
  ]
  node [
    id 570
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 571
    label "zwi&#261;za&#263;"
  ]
  node [
    id 572
    label "odwadnia&#263;"
  ]
  node [
    id 573
    label "marriage"
  ]
  node [
    id 574
    label "marketing_afiliacyjny"
  ]
  node [
    id 575
    label "bearing"
  ]
  node [
    id 576
    label "wi&#261;zanie"
  ]
  node [
    id 577
    label "odwadnianie"
  ]
  node [
    id 578
    label "koligacja"
  ]
  node [
    id 579
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 580
    label "odwodni&#263;"
  ]
  node [
    id 581
    label "azeotrop"
  ]
  node [
    id 582
    label "powi&#261;zanie"
  ]
  node [
    id 583
    label "koalicyjnie"
  ]
  node [
    id 584
    label "wsp&#243;lny"
  ]
  node [
    id 585
    label "zdolno&#347;&#263;"
  ]
  node [
    id 586
    label "flare"
  ]
  node [
    id 587
    label "powa&#380;ny"
  ]
  node [
    id 588
    label "odpowiedzialnie"
  ]
  node [
    id 589
    label "przewinienie"
  ]
  node [
    id 590
    label "sprawca"
  ]
  node [
    id 591
    label "odpowiadanie"
  ]
  node [
    id 592
    label "&#347;wiadomy"
  ]
  node [
    id 593
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 594
    label "dip"
  ]
  node [
    id 595
    label "zanurzy&#263;"
  ]
  node [
    id 596
    label "zag&#322;ada"
  ]
  node [
    id 597
    label "wprowadzi&#263;"
  ]
  node [
    id 598
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 599
    label "perceive"
  ]
  node [
    id 600
    label "reagowa&#263;"
  ]
  node [
    id 601
    label "spowodowa&#263;"
  ]
  node [
    id 602
    label "male&#263;"
  ]
  node [
    id 603
    label "zmale&#263;"
  ]
  node [
    id 604
    label "spotka&#263;"
  ]
  node [
    id 605
    label "go_steady"
  ]
  node [
    id 606
    label "dostrzega&#263;"
  ]
  node [
    id 607
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 608
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 609
    label "ogl&#261;da&#263;"
  ]
  node [
    id 610
    label "os&#261;dza&#263;"
  ]
  node [
    id 611
    label "aprobowa&#263;"
  ]
  node [
    id 612
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 613
    label "wzrok"
  ]
  node [
    id 614
    label "postrzega&#263;"
  ]
  node [
    id 615
    label "notice"
  ]
  node [
    id 616
    label "ability"
  ]
  node [
    id 617
    label "wyb&#243;r"
  ]
  node [
    id 618
    label "prospect"
  ]
  node [
    id 619
    label "alternatywa"
  ]
  node [
    id 620
    label "potencja&#322;"
  ]
  node [
    id 621
    label "obliczeniowo"
  ]
  node [
    id 622
    label "operator_modalny"
  ]
  node [
    id 623
    label "posiada&#263;"
  ]
  node [
    id 624
    label "przywr&#243;cenie"
  ]
  node [
    id 625
    label "reform"
  ]
  node [
    id 626
    label "ponaprawianie"
  ]
  node [
    id 627
    label "beztroski"
  ]
  node [
    id 628
    label "nieodpowiedzialnie"
  ]
  node [
    id 629
    label "niem&#261;dry"
  ]
  node [
    id 630
    label "niebezpieczny"
  ]
  node [
    id 631
    label "niedojrza&#322;y"
  ]
  node [
    id 632
    label "zawodny"
  ]
  node [
    id 633
    label "strategia"
  ]
  node [
    id 634
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 635
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 636
    label "poprowadzi&#263;"
  ]
  node [
    id 637
    label "pos&#322;a&#263;"
  ]
  node [
    id 638
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 639
    label "wykona&#263;"
  ]
  node [
    id 640
    label "wzbudzi&#263;"
  ]
  node [
    id 641
    label "set"
  ]
  node [
    id 642
    label "carry"
  ]
  node [
    id 643
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 644
    label "fall"
  ]
  node [
    id 645
    label "ukry&#263;_si&#281;"
  ]
  node [
    id 646
    label "pogr&#261;&#380;y&#263;_si&#281;"
  ]
  node [
    id 647
    label "sink"
  ]
  node [
    id 648
    label "subside"
  ]
  node [
    id 649
    label "Marzec_'68"
  ]
  node [
    id 650
    label "opa&#347;&#263;"
  ]
  node [
    id 651
    label "utrwali&#263;_si&#281;"
  ]
  node [
    id 652
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 653
    label "popa&#347;&#263;"
  ]
  node [
    id 654
    label "drop"
  ]
  node [
    id 655
    label "July"
  ]
  node [
    id 656
    label "pogorszenie"
  ]
  node [
    id 657
    label "odmawia&#263;"
  ]
  node [
    id 658
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 659
    label "sk&#322;ada&#263;"
  ]
  node [
    id 660
    label "thank"
  ]
  node [
    id 661
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 662
    label "wyra&#380;a&#263;"
  ]
  node [
    id 663
    label "etykieta"
  ]
  node [
    id 664
    label "w_chuj"
  ]
  node [
    id 665
    label "oklaski"
  ]
  node [
    id 666
    label "kla&#347;ni&#281;cie"
  ]
  node [
    id 667
    label "Stefan"
  ]
  node [
    id 668
    label "Niesio&#322;owski"
  ]
  node [
    id 669
    label "gabriela"
  ]
  node [
    id 670
    label "mas&#322;owski"
  ]
  node [
    id 671
    label "i"
  ]
  node [
    id 672
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 673
    label "Rafa&#322;"
  ]
  node [
    id 674
    label "Kmity"
  ]
  node [
    id 675
    label "teleturniej"
  ]
  node [
    id 676
    label "zawsze"
  ]
  node [
    id 677
    label "dziewica"
  ]
  node [
    id 678
    label "Kabareton"
  ]
  node [
    id 679
    label "Sopot"
  ]
  node [
    id 680
    label "topi&#263;"
  ]
  node [
    id 681
    label "trend"
  ]
  node [
    id 682
    label "krajowy"
  ]
  node [
    id 683
    label "rada"
  ]
  node [
    id 684
    label "Maryja"
  ]
  node [
    id 685
    label "Tadeusz"
  ]
  node [
    id 686
    label "rydzyk"
  ]
  node [
    id 687
    label "Leszek"
  ]
  node [
    id 688
    label "Deptu&#322;a"
  ]
  node [
    id 689
    label "Jaros&#322;awa"
  ]
  node [
    id 690
    label "rusiecki"
  ]
  node [
    id 691
    label "Jerzy"
  ]
  node [
    id 692
    label "Feliksa"
  ]
  node [
    id 693
    label "Fedorowicz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 14
    target 213
  ]
  edge [
    source 14
    target 214
  ]
  edge [
    source 14
    target 215
  ]
  edge [
    source 14
    target 216
  ]
  edge [
    source 14
    target 217
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 58
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 232
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 20
    target 82
  ]
  edge [
    source 20
    target 233
  ]
  edge [
    source 20
    target 234
  ]
  edge [
    source 20
    target 235
  ]
  edge [
    source 20
    target 236
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 20
    target 79
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 21
    target 239
  ]
  edge [
    source 21
    target 240
  ]
  edge [
    source 21
    target 241
  ]
  edge [
    source 21
    target 242
  ]
  edge [
    source 21
    target 243
  ]
  edge [
    source 21
    target 244
  ]
  edge [
    source 21
    target 245
  ]
  edge [
    source 21
    target 246
  ]
  edge [
    source 21
    target 247
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 248
  ]
  edge [
    source 22
    target 249
  ]
  edge [
    source 22
    target 250
  ]
  edge [
    source 22
    target 251
  ]
  edge [
    source 22
    target 252
  ]
  edge [
    source 22
    target 253
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 254
  ]
  edge [
    source 22
    target 255
  ]
  edge [
    source 22
    target 256
  ]
  edge [
    source 22
    target 257
  ]
  edge [
    source 22
    target 258
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 259
  ]
  edge [
    source 23
    target 260
  ]
  edge [
    source 23
    target 261
  ]
  edge [
    source 23
    target 262
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 23
    target 281
  ]
  edge [
    source 23
    target 282
  ]
  edge [
    source 23
    target 283
  ]
  edge [
    source 23
    target 284
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 285
  ]
  edge [
    source 24
    target 286
  ]
  edge [
    source 24
    target 287
  ]
  edge [
    source 24
    target 288
  ]
  edge [
    source 24
    target 289
  ]
  edge [
    source 24
    target 290
  ]
  edge [
    source 24
    target 291
  ]
  edge [
    source 24
    target 292
  ]
  edge [
    source 24
    target 293
  ]
  edge [
    source 24
    target 294
  ]
  edge [
    source 24
    target 295
  ]
  edge [
    source 24
    target 296
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 43
  ]
  edge [
    source 24
    target 79
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 297
  ]
  edge [
    source 25
    target 298
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 41
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 299
  ]
  edge [
    source 26
    target 300
  ]
  edge [
    source 26
    target 301
  ]
  edge [
    source 26
    target 302
  ]
  edge [
    source 26
    target 58
  ]
  edge [
    source 26
    target 59
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 57
  ]
  edge [
    source 27
    target 303
  ]
  edge [
    source 27
    target 304
  ]
  edge [
    source 27
    target 305
  ]
  edge [
    source 27
    target 306
  ]
  edge [
    source 27
    target 307
  ]
  edge [
    source 27
    target 682
  ]
  edge [
    source 27
    target 683
  ]
  edge [
    source 27
    target 671
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 308
  ]
  edge [
    source 28
    target 309
  ]
  edge [
    source 28
    target 310
  ]
  edge [
    source 28
    target 311
  ]
  edge [
    source 28
    target 312
  ]
  edge [
    source 28
    target 313
  ]
  edge [
    source 28
    target 314
  ]
  edge [
    source 28
    target 315
  ]
  edge [
    source 28
    target 316
  ]
  edge [
    source 28
    target 317
  ]
  edge [
    source 28
    target 318
  ]
  edge [
    source 28
    target 319
  ]
  edge [
    source 28
    target 320
  ]
  edge [
    source 28
    target 321
  ]
  edge [
    source 28
    target 322
  ]
  edge [
    source 28
    target 323
  ]
  edge [
    source 28
    target 324
  ]
  edge [
    source 28
    target 325
  ]
  edge [
    source 28
    target 682
  ]
  edge [
    source 28
    target 683
  ]
  edge [
    source 28
    target 671
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 326
  ]
  edge [
    source 29
    target 327
  ]
  edge [
    source 29
    target 328
  ]
  edge [
    source 29
    target 329
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 29
    target 44
  ]
  edge [
    source 29
    target 78
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 43
  ]
  edge [
    source 30
    target 79
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 330
  ]
  edge [
    source 31
    target 331
  ]
  edge [
    source 31
    target 332
  ]
  edge [
    source 31
    target 333
  ]
  edge [
    source 31
    target 334
  ]
  edge [
    source 31
    target 42
  ]
  edge [
    source 31
    target 80
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 335
  ]
  edge [
    source 32
    target 336
  ]
  edge [
    source 32
    target 337
  ]
  edge [
    source 32
    target 338
  ]
  edge [
    source 32
    target 274
  ]
  edge [
    source 32
    target 275
  ]
  edge [
    source 32
    target 339
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 32
    target 80
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 71
  ]
  edge [
    source 33
    target 81
  ]
  edge [
    source 33
    target 340
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 341
  ]
  edge [
    source 33
    target 342
  ]
  edge [
    source 33
    target 343
  ]
  edge [
    source 33
    target 64
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 200
  ]
  edge [
    source 34
    target 344
  ]
  edge [
    source 34
    target 345
  ]
  edge [
    source 34
    target 346
  ]
  edge [
    source 34
    target 347
  ]
  edge [
    source 34
    target 348
  ]
  edge [
    source 34
    target 349
  ]
  edge [
    source 34
    target 350
  ]
  edge [
    source 34
    target 351
  ]
  edge [
    source 34
    target 352
  ]
  edge [
    source 34
    target 151
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 353
  ]
  edge [
    source 35
    target 354
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 72
  ]
  edge [
    source 36
    target 73
  ]
  edge [
    source 36
    target 355
  ]
  edge [
    source 36
    target 356
  ]
  edge [
    source 36
    target 197
  ]
  edge [
    source 36
    target 357
  ]
  edge [
    source 36
    target 358
  ]
  edge [
    source 36
    target 359
  ]
  edge [
    source 36
    target 81
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 360
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 362
  ]
  edge [
    source 37
    target 363
  ]
  edge [
    source 37
    target 77
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 363
  ]
  edge [
    source 38
    target 364
  ]
  edge [
    source 38
    target 365
  ]
  edge [
    source 39
    target 300
  ]
  edge [
    source 39
    target 366
  ]
  edge [
    source 39
    target 367
  ]
  edge [
    source 39
    target 368
  ]
  edge [
    source 39
    target 369
  ]
  edge [
    source 40
    target 370
  ]
  edge [
    source 40
    target 371
  ]
  edge [
    source 40
    target 372
  ]
  edge [
    source 40
    target 373
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 78
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 243
  ]
  edge [
    source 42
    target 374
  ]
  edge [
    source 42
    target 375
  ]
  edge [
    source 42
    target 376
  ]
  edge [
    source 42
    target 377
  ]
  edge [
    source 42
    target 378
  ]
  edge [
    source 42
    target 80
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 379
  ]
  edge [
    source 43
    target 380
  ]
  edge [
    source 43
    target 381
  ]
  edge [
    source 43
    target 79
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 382
  ]
  edge [
    source 44
    target 383
  ]
  edge [
    source 44
    target 384
  ]
  edge [
    source 44
    target 385
  ]
  edge [
    source 44
    target 386
  ]
  edge [
    source 44
    target 387
  ]
  edge [
    source 44
    target 388
  ]
  edge [
    source 44
    target 389
  ]
  edge [
    source 44
    target 78
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 390
  ]
  edge [
    source 45
    target 391
  ]
  edge [
    source 45
    target 392
  ]
  edge [
    source 45
    target 393
  ]
  edge [
    source 45
    target 394
  ]
  edge [
    source 45
    target 395
  ]
  edge [
    source 45
    target 396
  ]
  edge [
    source 45
    target 397
  ]
  edge [
    source 45
    target 398
  ]
  edge [
    source 45
    target 399
  ]
  edge [
    source 45
    target 400
  ]
  edge [
    source 45
    target 401
  ]
  edge [
    source 45
    target 402
  ]
  edge [
    source 45
    target 403
  ]
  edge [
    source 45
    target 404
  ]
  edge [
    source 45
    target 405
  ]
  edge [
    source 45
    target 334
  ]
  edge [
    source 45
    target 406
  ]
  edge [
    source 45
    target 407
  ]
  edge [
    source 45
    target 408
  ]
  edge [
    source 45
    target 409
  ]
  edge [
    source 45
    target 410
  ]
  edge [
    source 45
    target 411
  ]
  edge [
    source 45
    target 412
  ]
  edge [
    source 45
    target 413
  ]
  edge [
    source 45
    target 414
  ]
  edge [
    source 45
    target 415
  ]
  edge [
    source 45
    target 416
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 417
  ]
  edge [
    source 46
    target 418
  ]
  edge [
    source 46
    target 419
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 58
  ]
  edge [
    source 47
    target 59
  ]
  edge [
    source 47
    target 147
  ]
  edge [
    source 47
    target 404
  ]
  edge [
    source 47
    target 202
  ]
  edge [
    source 47
    target 420
  ]
  edge [
    source 47
    target 421
  ]
  edge [
    source 47
    target 216
  ]
  edge [
    source 47
    target 345
  ]
  edge [
    source 47
    target 422
  ]
  edge [
    source 47
    target 423
  ]
  edge [
    source 47
    target 424
  ]
  edge [
    source 47
    target 425
  ]
  edge [
    source 47
    target 426
  ]
  edge [
    source 47
    target 427
  ]
  edge [
    source 47
    target 428
  ]
  edge [
    source 47
    target 429
  ]
  edge [
    source 47
    target 430
  ]
  edge [
    source 47
    target 431
  ]
  edge [
    source 47
    target 432
  ]
  edge [
    source 47
    target 433
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 434
  ]
  edge [
    source 48
    target 85
  ]
  edge [
    source 48
    target 435
  ]
  edge [
    source 48
    target 436
  ]
  edge [
    source 48
    target 437
  ]
  edge [
    source 48
    target 438
  ]
  edge [
    source 48
    target 439
  ]
  edge [
    source 48
    target 440
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 441
  ]
  edge [
    source 50
    target 442
  ]
  edge [
    source 50
    target 443
  ]
  edge [
    source 50
    target 444
  ]
  edge [
    source 50
    target 445
  ]
  edge [
    source 50
    target 446
  ]
  edge [
    source 50
    target 447
  ]
  edge [
    source 50
    target 448
  ]
  edge [
    source 50
    target 449
  ]
  edge [
    source 50
    target 450
  ]
  edge [
    source 50
    target 451
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 452
  ]
  edge [
    source 51
    target 453
  ]
  edge [
    source 51
    target 454
  ]
  edge [
    source 51
    target 455
  ]
  edge [
    source 51
    target 456
  ]
  edge [
    source 51
    target 457
  ]
  edge [
    source 51
    target 458
  ]
  edge [
    source 51
    target 459
  ]
  edge [
    source 51
    target 460
  ]
  edge [
    source 51
    target 461
  ]
  edge [
    source 51
    target 462
  ]
  edge [
    source 51
    target 463
  ]
  edge [
    source 51
    target 63
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 464
  ]
  edge [
    source 52
    target 465
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 466
  ]
  edge [
    source 53
    target 467
  ]
  edge [
    source 53
    target 468
  ]
  edge [
    source 53
    target 469
  ]
  edge [
    source 53
    target 470
  ]
  edge [
    source 53
    target 471
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 59
  ]
  edge [
    source 54
    target 60
  ]
  edge [
    source 54
    target 65
  ]
  edge [
    source 55
    target 472
  ]
  edge [
    source 55
    target 473
  ]
  edge [
    source 55
    target 474
  ]
  edge [
    source 55
    target 475
  ]
  edge [
    source 55
    target 476
  ]
  edge [
    source 55
    target 81
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 477
  ]
  edge [
    source 56
    target 478
  ]
  edge [
    source 56
    target 479
  ]
  edge [
    source 56
    target 480
  ]
  edge [
    source 56
    target 481
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 66
  ]
  edge [
    source 57
    target 67
  ]
  edge [
    source 57
    target 72
  ]
  edge [
    source 57
    target 482
  ]
  edge [
    source 57
    target 483
  ]
  edge [
    source 57
    target 484
  ]
  edge [
    source 57
    target 485
  ]
  edge [
    source 57
    target 486
  ]
  edge [
    source 57
    target 487
  ]
  edge [
    source 57
    target 488
  ]
  edge [
    source 57
    target 489
  ]
  edge [
    source 57
    target 490
  ]
  edge [
    source 57
    target 491
  ]
  edge [
    source 57
    target 492
  ]
  edge [
    source 57
    target 493
  ]
  edge [
    source 57
    target 494
  ]
  edge [
    source 57
    target 495
  ]
  edge [
    source 57
    target 496
  ]
  edge [
    source 57
    target 497
  ]
  edge [
    source 57
    target 321
  ]
  edge [
    source 57
    target 498
  ]
  edge [
    source 58
    target 499
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 64
  ]
  edge [
    source 59
    target 500
  ]
  edge [
    source 59
    target 470
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 97
  ]
  edge [
    source 60
    target 501
  ]
  edge [
    source 60
    target 502
  ]
  edge [
    source 60
    target 129
  ]
  edge [
    source 60
    target 503
  ]
  edge [
    source 60
    target 253
  ]
  edge [
    source 60
    target 504
  ]
  edge [
    source 60
    target 505
  ]
  edge [
    source 60
    target 506
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 507
  ]
  edge [
    source 61
    target 508
  ]
  edge [
    source 61
    target 509
  ]
  edge [
    source 61
    target 510
  ]
  edge [
    source 61
    target 511
  ]
  edge [
    source 61
    target 512
  ]
  edge [
    source 61
    target 513
  ]
  edge [
    source 61
    target 514
  ]
  edge [
    source 61
    target 515
  ]
  edge [
    source 61
    target 516
  ]
  edge [
    source 61
    target 517
  ]
  edge [
    source 61
    target 518
  ]
  edge [
    source 61
    target 519
  ]
  edge [
    source 61
    target 520
  ]
  edge [
    source 61
    target 521
  ]
  edge [
    source 61
    target 522
  ]
  edge [
    source 61
    target 523
  ]
  edge [
    source 61
    target 524
  ]
  edge [
    source 61
    target 525
  ]
  edge [
    source 61
    target 526
  ]
  edge [
    source 61
    target 527
  ]
  edge [
    source 61
    target 528
  ]
  edge [
    source 61
    target 486
  ]
  edge [
    source 61
    target 529
  ]
  edge [
    source 61
    target 491
  ]
  edge [
    source 61
    target 530
  ]
  edge [
    source 61
    target 531
  ]
  edge [
    source 61
    target 532
  ]
  edge [
    source 61
    target 72
  ]
  edge [
    source 61
    target 81
  ]
  edge [
    source 61
    target 673
  ]
  edge [
    source 61
    target 674
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 533
  ]
  edge [
    source 62
    target 534
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 535
  ]
  edge [
    source 63
    target 536
  ]
  edge [
    source 63
    target 537
  ]
  edge [
    source 63
    target 538
  ]
  edge [
    source 63
    target 213
  ]
  edge [
    source 63
    target 539
  ]
  edge [
    source 63
    target 540
  ]
  edge [
    source 64
    target 541
  ]
  edge [
    source 64
    target 542
  ]
  edge [
    source 64
    target 543
  ]
  edge [
    source 64
    target 544
  ]
  edge [
    source 64
    target 545
  ]
  edge [
    source 64
    target 546
  ]
  edge [
    source 64
    target 547
  ]
  edge [
    source 64
    target 548
  ]
  edge [
    source 64
    target 549
  ]
  edge [
    source 64
    target 442
  ]
  edge [
    source 64
    target 550
  ]
  edge [
    source 64
    target 551
  ]
  edge [
    source 64
    target 552
  ]
  edge [
    source 64
    target 553
  ]
  edge [
    source 64
    target 554
  ]
  edge [
    source 64
    target 555
  ]
  edge [
    source 64
    target 556
  ]
  edge [
    source 64
    target 557
  ]
  edge [
    source 64
    target 558
  ]
  edge [
    source 64
    target 559
  ]
  edge [
    source 64
    target 560
  ]
  edge [
    source 64
    target 561
  ]
  edge [
    source 64
    target 562
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 563
  ]
  edge [
    source 65
    target 564
  ]
  edge [
    source 65
    target 565
  ]
  edge [
    source 65
    target 566
  ]
  edge [
    source 65
    target 567
  ]
  edge [
    source 65
    target 568
  ]
  edge [
    source 65
    target 569
  ]
  edge [
    source 65
    target 570
  ]
  edge [
    source 65
    target 571
  ]
  edge [
    source 65
    target 300
  ]
  edge [
    source 65
    target 572
  ]
  edge [
    source 65
    target 573
  ]
  edge [
    source 65
    target 574
  ]
  edge [
    source 65
    target 575
  ]
  edge [
    source 65
    target 576
  ]
  edge [
    source 65
    target 577
  ]
  edge [
    source 65
    target 578
  ]
  edge [
    source 65
    target 579
  ]
  edge [
    source 65
    target 580
  ]
  edge [
    source 65
    target 581
  ]
  edge [
    source 65
    target 582
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 583
  ]
  edge [
    source 67
    target 584
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 585
  ]
  edge [
    source 68
    target 586
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 85
  ]
  edge [
    source 70
    target 587
  ]
  edge [
    source 70
    target 588
  ]
  edge [
    source 70
    target 589
  ]
  edge [
    source 70
    target 590
  ]
  edge [
    source 70
    target 591
  ]
  edge [
    source 70
    target 592
  ]
  edge [
    source 71
    target 593
  ]
  edge [
    source 71
    target 594
  ]
  edge [
    source 71
    target 595
  ]
  edge [
    source 71
    target 596
  ]
  edge [
    source 71
    target 597
  ]
  edge [
    source 71
    target 80
  ]
  edge [
    source 72
    target 81
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 598
  ]
  edge [
    source 73
    target 599
  ]
  edge [
    source 73
    target 600
  ]
  edge [
    source 73
    target 601
  ]
  edge [
    source 73
    target 602
  ]
  edge [
    source 73
    target 603
  ]
  edge [
    source 73
    target 604
  ]
  edge [
    source 73
    target 605
  ]
  edge [
    source 73
    target 606
  ]
  edge [
    source 73
    target 607
  ]
  edge [
    source 73
    target 608
  ]
  edge [
    source 73
    target 609
  ]
  edge [
    source 73
    target 610
  ]
  edge [
    source 73
    target 611
  ]
  edge [
    source 73
    target 459
  ]
  edge [
    source 73
    target 612
  ]
  edge [
    source 73
    target 613
  ]
  edge [
    source 73
    target 614
  ]
  edge [
    source 73
    target 615
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 616
  ]
  edge [
    source 74
    target 617
  ]
  edge [
    source 74
    target 618
  ]
  edge [
    source 74
    target 484
  ]
  edge [
    source 74
    target 619
  ]
  edge [
    source 74
    target 620
  ]
  edge [
    source 74
    target 346
  ]
  edge [
    source 74
    target 197
  ]
  edge [
    source 74
    target 621
  ]
  edge [
    source 74
    target 149
  ]
  edge [
    source 74
    target 622
  ]
  edge [
    source 74
    target 623
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 624
  ]
  edge [
    source 75
    target 625
  ]
  edge [
    source 75
    target 626
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 627
  ]
  edge [
    source 77
    target 628
  ]
  edge [
    source 77
    target 629
  ]
  edge [
    source 77
    target 630
  ]
  edge [
    source 77
    target 631
  ]
  edge [
    source 77
    target 632
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 633
  ]
  edge [
    source 78
    target 223
  ]
  edge [
    source 78
    target 634
  ]
  edge [
    source 78
    target 635
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 636
  ]
  edge [
    source 80
    target 601
  ]
  edge [
    source 80
    target 637
  ]
  edge [
    source 80
    target 638
  ]
  edge [
    source 80
    target 639
  ]
  edge [
    source 80
    target 640
  ]
  edge [
    source 80
    target 597
  ]
  edge [
    source 80
    target 641
  ]
  edge [
    source 80
    target 327
  ]
  edge [
    source 80
    target 642
  ]
  edge [
    source 81
    target 643
  ]
  edge [
    source 81
    target 644
  ]
  edge [
    source 81
    target 645
  ]
  edge [
    source 81
    target 477
  ]
  edge [
    source 81
    target 646
  ]
  edge [
    source 81
    target 647
  ]
  edge [
    source 81
    target 648
  ]
  edge [
    source 81
    target 649
  ]
  edge [
    source 81
    target 650
  ]
  edge [
    source 81
    target 651
  ]
  edge [
    source 81
    target 652
  ]
  edge [
    source 81
    target 653
  ]
  edge [
    source 81
    target 654
  ]
  edge [
    source 81
    target 655
  ]
  edge [
    source 81
    target 656
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 657
  ]
  edge [
    source 82
    target 658
  ]
  edge [
    source 82
    target 659
  ]
  edge [
    source 82
    target 660
  ]
  edge [
    source 82
    target 661
  ]
  edge [
    source 82
    target 662
  ]
  edge [
    source 82
    target 663
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 664
  ]
  edge [
    source 84
    target 665
  ]
  edge [
    source 84
    target 666
  ]
  edge [
    source 282
    target 671
  ]
  edge [
    source 282
    target 672
  ]
  edge [
    source 304
    target 684
  ]
  edge [
    source 667
    target 668
  ]
  edge [
    source 669
    target 670
  ]
  edge [
    source 671
    target 672
  ]
  edge [
    source 671
    target 682
  ]
  edge [
    source 671
    target 683
  ]
  edge [
    source 673
    target 674
  ]
  edge [
    source 675
    target 676
  ]
  edge [
    source 675
    target 677
  ]
  edge [
    source 676
    target 677
  ]
  edge [
    source 678
    target 679
  ]
  edge [
    source 678
    target 680
  ]
  edge [
    source 678
    target 681
  ]
  edge [
    source 679
    target 680
  ]
  edge [
    source 679
    target 681
  ]
  edge [
    source 680
    target 681
  ]
  edge [
    source 682
    target 683
  ]
  edge [
    source 685
    target 686
  ]
  edge [
    source 687
    target 688
  ]
  edge [
    source 689
    target 690
  ]
  edge [
    source 691
    target 692
  ]
  edge [
    source 691
    target 693
  ]
  edge [
    source 692
    target 693
  ]
]
