graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.1464968152866244
  density 0.006857817301235221
  graphCliqueNumber 3
  node [
    id 0
    label "magazyn"
    origin "text"
  ]
  node [
    id 1
    label "make"
    origin "text"
  ]
  node [
    id 2
    label "fascynuj&#261;cy"
    origin "text"
  ]
  node [
    id 3
    label "pismo"
    origin "text"
  ]
  node [
    id 4
    label "dla"
    origin "text"
  ]
  node [
    id 5
    label "majsterkowicz"
    origin "text"
  ]
  node [
    id 6
    label "era"
    origin "text"
  ]
  node [
    id 7
    label "cyfrowy"
    origin "text"
  ]
  node [
    id 8
    label "&#380;ebyby&#263;"
    origin "text"
  ]
  node [
    id 9
    label "potrafi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "cokolwiek"
    origin "text"
  ]
  node [
    id 12
    label "proponowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "czytelnik"
    origin "text"
  ]
  node [
    id 15
    label "&#322;am"
    origin "text"
  ]
  node [
    id 16
    label "sentyment"
    origin "text"
  ]
  node [
    id 17
    label "jak"
    origin "text"
  ]
  node [
    id 18
    label "pozosta&#263;"
    origin "text"
  ]
  node [
    id 19
    label "czas"
    origin "text"
  ]
  node [
    id 20
    label "m&#322;odo&#347;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "przynajmniej"
    origin "text"
  ]
  node [
    id 22
    label "pr&#243;bowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "wykona&#263;"
    origin "text"
  ]
  node [
    id 24
    label "poniekt&#243;ry"
    origin "text"
  ]
  node [
    id 25
    label "konstrukcja"
    origin "text"
  ]
  node [
    id 26
    label "adam"
    origin "text"
  ]
  node [
    id 27
    label "s&#322;odowy"
    origin "text"
  ]
  node [
    id 28
    label "zr&#243;b"
    origin "text"
  ]
  node [
    id 29
    label "sam"
    origin "text"
  ]
  node [
    id 30
    label "powodowa&#263;"
    origin "text"
  ]
  node [
    id 31
    label "regularnie"
    origin "text"
  ]
  node [
    id 32
    label "blog"
    origin "text"
  ]
  node [
    id 33
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 34
    label "si&#281;"
    origin "text"
  ]
  node [
    id 35
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 36
    label "lista"
    origin "text"
  ]
  node [
    id 37
    label "open"
    origin "text"
  ]
  node [
    id 38
    label "sourcowych"
    origin "text"
  ]
  node [
    id 39
    label "diy"
    origin "text"
  ]
  node [
    id 40
    label "lutownica"
    origin "text"
  ]
  node [
    id 41
    label "kompilator"
    origin "text"
  ]
  node [
    id 42
    label "prezent"
    origin "text"
  ]
  node [
    id 43
    label "gwiazdkowy"
    origin "text"
  ]
  node [
    id 44
    label "ebaya"
    origin "text"
  ]
  node [
    id 45
    label "trudno"
    origin "text"
  ]
  node [
    id 46
    label "zaopatrzy&#263;"
    origin "text"
  ]
  node [
    id 47
    label "polska"
    origin "text"
  ]
  node [
    id 48
    label "poleca&#263;"
    origin "text"
  ]
  node [
    id 49
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 50
    label "kosztowa&#263;"
    origin "text"
  ]
  node [
    id 51
    label "nic"
    origin "text"
  ]
  node [
    id 52
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 53
    label "ubuntu"
    origin "text"
  ]
  node [
    id 54
    label "mark"
    origin "text"
  ]
  node [
    id 55
    label "shuttleworth"
    origin "text"
  ]
  node [
    id 56
    label "dostarczy&#263;"
    origin "text"
  ]
  node [
    id 57
    label "gratis"
    origin "text"
  ]
  node [
    id 58
    label "wycieraczka"
    origin "text"
  ]
  node [
    id 59
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 60
    label "robot"
    origin "text"
  ]
  node [
    id 61
    label "prawda"
    origin "text"
  ]
  node [
    id 62
    label "s&#322;odki"
    origin "text"
  ]
  node [
    id 63
    label "hurtownia"
  ]
  node [
    id 64
    label "fabryka"
  ]
  node [
    id 65
    label "zesp&#243;&#322;"
  ]
  node [
    id 66
    label "tytu&#322;"
  ]
  node [
    id 67
    label "czasopismo"
  ]
  node [
    id 68
    label "pomieszczenie"
  ]
  node [
    id 69
    label "pasjonuj&#261;co"
  ]
  node [
    id 70
    label "interesuj&#261;cy"
  ]
  node [
    id 71
    label "paleograf"
  ]
  node [
    id 72
    label "list"
  ]
  node [
    id 73
    label "egzemplarz"
  ]
  node [
    id 74
    label "komunikacja"
  ]
  node [
    id 75
    label "psychotest"
  ]
  node [
    id 76
    label "ortografia"
  ]
  node [
    id 77
    label "handwriting"
  ]
  node [
    id 78
    label "grafia"
  ]
  node [
    id 79
    label "prasa"
  ]
  node [
    id 80
    label "j&#281;zyk"
  ]
  node [
    id 81
    label "adres"
  ]
  node [
    id 82
    label "script"
  ]
  node [
    id 83
    label "dzia&#322;"
  ]
  node [
    id 84
    label "paleografia"
  ]
  node [
    id 85
    label "Zwrotnica"
  ]
  node [
    id 86
    label "wk&#322;ad"
  ]
  node [
    id 87
    label "cecha"
  ]
  node [
    id 88
    label "przekaz"
  ]
  node [
    id 89
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 90
    label "interpunkcja"
  ]
  node [
    id 91
    label "communication"
  ]
  node [
    id 92
    label "dokument"
  ]
  node [
    id 93
    label "ok&#322;adka"
  ]
  node [
    id 94
    label "dzie&#322;o"
  ]
  node [
    id 95
    label "letter"
  ]
  node [
    id 96
    label "zajawka"
  ]
  node [
    id 97
    label "majster-klepka"
  ]
  node [
    id 98
    label "hobbysta"
  ]
  node [
    id 99
    label "Adam_S&#322;odowy"
  ]
  node [
    id 100
    label "eon"
  ]
  node [
    id 101
    label "schy&#322;ek"
  ]
  node [
    id 102
    label "okres"
  ]
  node [
    id 103
    label "jednostka_geologiczna"
  ]
  node [
    id 104
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 105
    label "Zeitgeist"
  ]
  node [
    id 106
    label "dzieje"
  ]
  node [
    id 107
    label "cyfrowo"
  ]
  node [
    id 108
    label "sygna&#322;_cyfrowy"
  ]
  node [
    id 109
    label "elektroniczny"
  ]
  node [
    id 110
    label "cyfryzacja"
  ]
  node [
    id 111
    label "umie&#263;"
  ]
  node [
    id 112
    label "cope"
  ]
  node [
    id 113
    label "potrafia&#263;"
  ]
  node [
    id 114
    label "posuwa&#263;_si&#281;"
  ]
  node [
    id 115
    label "posun&#261;&#263;_si&#281;"
  ]
  node [
    id 116
    label "can"
  ]
  node [
    id 117
    label "zorganizowa&#263;"
  ]
  node [
    id 118
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 119
    label "przerobi&#263;"
  ]
  node [
    id 120
    label "wystylizowa&#263;"
  ]
  node [
    id 121
    label "cause"
  ]
  node [
    id 122
    label "wydali&#263;"
  ]
  node [
    id 123
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 124
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 125
    label "post&#261;pi&#263;"
  ]
  node [
    id 126
    label "appoint"
  ]
  node [
    id 127
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 128
    label "nabra&#263;"
  ]
  node [
    id 129
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 130
    label "ilo&#347;&#263;"
  ]
  node [
    id 131
    label "co&#347;"
  ]
  node [
    id 132
    label "report"
  ]
  node [
    id 133
    label "volunteer"
  ]
  node [
    id 134
    label "informowa&#263;"
  ]
  node [
    id 135
    label "zach&#281;ca&#263;"
  ]
  node [
    id 136
    label "wnioskowa&#263;"
  ]
  node [
    id 137
    label "suggest"
  ]
  node [
    id 138
    label "kandydatura"
  ]
  node [
    id 139
    label "si&#281;ga&#263;"
  ]
  node [
    id 140
    label "trwa&#263;"
  ]
  node [
    id 141
    label "obecno&#347;&#263;"
  ]
  node [
    id 142
    label "stan"
  ]
  node [
    id 143
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 144
    label "stand"
  ]
  node [
    id 145
    label "mie&#263;_miejsce"
  ]
  node [
    id 146
    label "uczestniczy&#263;"
  ]
  node [
    id 147
    label "chodzi&#263;"
  ]
  node [
    id 148
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 149
    label "equal"
  ]
  node [
    id 150
    label "biblioteka"
  ]
  node [
    id 151
    label "odbiorca"
  ]
  node [
    id 152
    label "klient"
  ]
  node [
    id 153
    label "kolumna"
  ]
  node [
    id 154
    label "emocja"
  ]
  node [
    id 155
    label "feel"
  ]
  node [
    id 156
    label "uczuciowo&#347;&#263;"
  ]
  node [
    id 157
    label "byd&#322;o"
  ]
  node [
    id 158
    label "zobo"
  ]
  node [
    id 159
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 160
    label "yakalo"
  ]
  node [
    id 161
    label "dzo"
  ]
  node [
    id 162
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 163
    label "proceed"
  ]
  node [
    id 164
    label "catch"
  ]
  node [
    id 165
    label "osta&#263;_si&#281;"
  ]
  node [
    id 166
    label "support"
  ]
  node [
    id 167
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 168
    label "prze&#380;y&#263;"
  ]
  node [
    id 169
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 170
    label "czasokres"
  ]
  node [
    id 171
    label "trawienie"
  ]
  node [
    id 172
    label "kategoria_gramatyczna"
  ]
  node [
    id 173
    label "period"
  ]
  node [
    id 174
    label "odczyt"
  ]
  node [
    id 175
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 176
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 177
    label "chwila"
  ]
  node [
    id 178
    label "poprzedzenie"
  ]
  node [
    id 179
    label "koniugacja"
  ]
  node [
    id 180
    label "poprzedzi&#263;"
  ]
  node [
    id 181
    label "przep&#322;ywanie"
  ]
  node [
    id 182
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 183
    label "odwlekanie_si&#281;"
  ]
  node [
    id 184
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 185
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 186
    label "okres_czasu"
  ]
  node [
    id 187
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 188
    label "pochodzi&#263;"
  ]
  node [
    id 189
    label "czwarty_wymiar"
  ]
  node [
    id 190
    label "chronometria"
  ]
  node [
    id 191
    label "poprzedzanie"
  ]
  node [
    id 192
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 193
    label "pogoda"
  ]
  node [
    id 194
    label "zegar"
  ]
  node [
    id 195
    label "trawi&#263;"
  ]
  node [
    id 196
    label "pochodzenie"
  ]
  node [
    id 197
    label "poprzedza&#263;"
  ]
  node [
    id 198
    label "time_period"
  ]
  node [
    id 199
    label "rachuba_czasu"
  ]
  node [
    id 200
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 201
    label "czasoprzestrze&#324;"
  ]
  node [
    id 202
    label "laba"
  ]
  node [
    id 203
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 204
    label "adolescence"
  ]
  node [
    id 205
    label "wiek"
  ]
  node [
    id 206
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 207
    label "&#380;ycie"
  ]
  node [
    id 208
    label "zielone_lata"
  ]
  node [
    id 209
    label "wytworzy&#263;"
  ]
  node [
    id 210
    label "manufacture"
  ]
  node [
    id 211
    label "picture"
  ]
  node [
    id 212
    label "jaki&#347;"
  ]
  node [
    id 213
    label "wytw&#243;r"
  ]
  node [
    id 214
    label "struktura"
  ]
  node [
    id 215
    label "practice"
  ]
  node [
    id 216
    label "budowa"
  ]
  node [
    id 217
    label "rzecz"
  ]
  node [
    id 218
    label "wykre&#347;lanie"
  ]
  node [
    id 219
    label "element_konstrukcyjny"
  ]
  node [
    id 220
    label "s&#322;odkawy"
  ]
  node [
    id 221
    label "zbo&#380;owy"
  ]
  node [
    id 222
    label "sklep"
  ]
  node [
    id 223
    label "motywowa&#263;"
  ]
  node [
    id 224
    label "act"
  ]
  node [
    id 225
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 226
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 227
    label "harmonijnie"
  ]
  node [
    id 228
    label "poprostu"
  ]
  node [
    id 229
    label "cz&#281;sto"
  ]
  node [
    id 230
    label "zwyczajny"
  ]
  node [
    id 231
    label "stale"
  ]
  node [
    id 232
    label "regularny"
  ]
  node [
    id 233
    label "komcio"
  ]
  node [
    id 234
    label "strona"
  ]
  node [
    id 235
    label "blogosfera"
  ]
  node [
    id 236
    label "pami&#281;tnik"
  ]
  node [
    id 237
    label "dok&#322;adnie"
  ]
  node [
    id 238
    label "wyliczanka"
  ]
  node [
    id 239
    label "catalog"
  ]
  node [
    id 240
    label "stock"
  ]
  node [
    id 241
    label "figurowa&#263;"
  ]
  node [
    id 242
    label "zbi&#243;r"
  ]
  node [
    id 243
    label "book"
  ]
  node [
    id 244
    label "pozycja"
  ]
  node [
    id 245
    label "tekst"
  ]
  node [
    id 246
    label "sumariusz"
  ]
  node [
    id 247
    label "lut&#243;wka"
  ]
  node [
    id 248
    label "narz&#281;dzie"
  ]
  node [
    id 249
    label "redaktor"
  ]
  node [
    id 250
    label "compiler"
  ]
  node [
    id 251
    label "autor"
  ]
  node [
    id 252
    label "translator"
  ]
  node [
    id 253
    label "dar"
  ]
  node [
    id 254
    label "presentation"
  ]
  node [
    id 255
    label "&#347;wi&#261;teczny"
  ]
  node [
    id 256
    label "trudny"
  ]
  node [
    id 257
    label "hard"
  ]
  node [
    id 258
    label "da&#263;"
  ]
  node [
    id 259
    label "spowodowa&#263;"
  ]
  node [
    id 260
    label "dress"
  ]
  node [
    id 261
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 262
    label "accommodate"
  ]
  node [
    id 263
    label "dopowiedzie&#263;"
  ]
  node [
    id 264
    label "control"
  ]
  node [
    id 265
    label "placard"
  ]
  node [
    id 266
    label "m&#243;wi&#263;"
  ]
  node [
    id 267
    label "charge"
  ]
  node [
    id 268
    label "zadawa&#263;"
  ]
  node [
    id 269
    label "ordynowa&#263;"
  ]
  node [
    id 270
    label "powierza&#263;"
  ]
  node [
    id 271
    label "wydawa&#263;"
  ]
  node [
    id 272
    label "doradza&#263;"
  ]
  node [
    id 273
    label "cena"
  ]
  node [
    id 274
    label "try"
  ]
  node [
    id 275
    label "essay"
  ]
  node [
    id 276
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 277
    label "doznawa&#263;"
  ]
  node [
    id 278
    label "savor"
  ]
  node [
    id 279
    label "miernota"
  ]
  node [
    id 280
    label "g&#243;wno"
  ]
  node [
    id 281
    label "love"
  ]
  node [
    id 282
    label "brak"
  ]
  node [
    id 283
    label "ciura"
  ]
  node [
    id 284
    label "cz&#322;owiek"
  ]
  node [
    id 285
    label "czyn"
  ]
  node [
    id 286
    label "przedstawiciel"
  ]
  node [
    id 287
    label "ilustracja"
  ]
  node [
    id 288
    label "fakt"
  ]
  node [
    id 289
    label "give"
  ]
  node [
    id 290
    label "darmowo"
  ]
  node [
    id 291
    label "promocja"
  ]
  node [
    id 292
    label "gratisowy"
  ]
  node [
    id 293
    label "mata"
  ]
  node [
    id 294
    label "samoch&#243;d"
  ]
  node [
    id 295
    label "urz&#261;dzenie"
  ]
  node [
    id 296
    label "czu&#263;"
  ]
  node [
    id 297
    label "desire"
  ]
  node [
    id 298
    label "kcie&#263;"
  ]
  node [
    id 299
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 300
    label "maszyna"
  ]
  node [
    id 301
    label "sprz&#281;t_AGD"
  ]
  node [
    id 302
    label "automat"
  ]
  node [
    id 303
    label "s&#261;d"
  ]
  node [
    id 304
    label "truth"
  ]
  node [
    id 305
    label "nieprawdziwy"
  ]
  node [
    id 306
    label "za&#322;o&#380;enie"
  ]
  node [
    id 307
    label "prawdziwy"
  ]
  node [
    id 308
    label "realia"
  ]
  node [
    id 309
    label "przyjemny"
  ]
  node [
    id 310
    label "uroczy"
  ]
  node [
    id 311
    label "s&#322;odko"
  ]
  node [
    id 312
    label "wspania&#322;y"
  ]
  node [
    id 313
    label "mi&#322;y"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 51
  ]
  edge [
    source 17
    target 52
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 43
  ]
  edge [
    source 19
    target 44
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 106
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 101
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 23
    target 211
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 60
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 222
  ]
  edge [
    source 30
    target 223
  ]
  edge [
    source 30
    target 145
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 227
  ]
  edge [
    source 31
    target 228
  ]
  edge [
    source 31
    target 229
  ]
  edge [
    source 31
    target 230
  ]
  edge [
    source 31
    target 231
  ]
  edge [
    source 31
    target 232
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 233
  ]
  edge [
    source 32
    target 234
  ]
  edge [
    source 32
    target 235
  ]
  edge [
    source 32
    target 236
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 45
  ]
  edge [
    source 34
    target 46
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 237
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 238
  ]
  edge [
    source 36
    target 239
  ]
  edge [
    source 36
    target 240
  ]
  edge [
    source 36
    target 241
  ]
  edge [
    source 36
    target 242
  ]
  edge [
    source 36
    target 243
  ]
  edge [
    source 36
    target 244
  ]
  edge [
    source 36
    target 245
  ]
  edge [
    source 36
    target 246
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 247
  ]
  edge [
    source 40
    target 248
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 249
  ]
  edge [
    source 41
    target 250
  ]
  edge [
    source 41
    target 251
  ]
  edge [
    source 41
    target 252
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 253
  ]
  edge [
    source 42
    target 254
  ]
  edge [
    source 42
    target 57
  ]
  edge [
    source 43
    target 255
  ]
  edge [
    source 45
    target 256
  ]
  edge [
    source 45
    target 257
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 258
  ]
  edge [
    source 46
    target 259
  ]
  edge [
    source 46
    target 260
  ]
  edge [
    source 46
    target 261
  ]
  edge [
    source 46
    target 262
  ]
  edge [
    source 46
    target 263
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 264
  ]
  edge [
    source 48
    target 265
  ]
  edge [
    source 48
    target 266
  ]
  edge [
    source 48
    target 267
  ]
  edge [
    source 48
    target 268
  ]
  edge [
    source 48
    target 269
  ]
  edge [
    source 48
    target 270
  ]
  edge [
    source 48
    target 271
  ]
  edge [
    source 48
    target 272
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 53
  ]
  edge [
    source 49
    target 54
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 273
  ]
  edge [
    source 50
    target 274
  ]
  edge [
    source 50
    target 275
  ]
  edge [
    source 50
    target 276
  ]
  edge [
    source 50
    target 277
  ]
  edge [
    source 50
    target 278
  ]
  edge [
    source 51
    target 279
  ]
  edge [
    source 51
    target 280
  ]
  edge [
    source 51
    target 281
  ]
  edge [
    source 51
    target 130
  ]
  edge [
    source 51
    target 282
  ]
  edge [
    source 51
    target 283
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 284
  ]
  edge [
    source 52
    target 285
  ]
  edge [
    source 52
    target 286
  ]
  edge [
    source 52
    target 287
  ]
  edge [
    source 52
    target 288
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 209
  ]
  edge [
    source 56
    target 289
  ]
  edge [
    source 56
    target 211
  ]
  edge [
    source 56
    target 259
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 290
  ]
  edge [
    source 57
    target 291
  ]
  edge [
    source 57
    target 292
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 293
  ]
  edge [
    source 58
    target 294
  ]
  edge [
    source 58
    target 295
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 296
  ]
  edge [
    source 59
    target 297
  ]
  edge [
    source 59
    target 298
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 299
  ]
  edge [
    source 60
    target 300
  ]
  edge [
    source 60
    target 301
  ]
  edge [
    source 60
    target 302
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 303
  ]
  edge [
    source 61
    target 304
  ]
  edge [
    source 61
    target 305
  ]
  edge [
    source 61
    target 306
  ]
  edge [
    source 61
    target 307
  ]
  edge [
    source 61
    target 308
  ]
  edge [
    source 62
    target 309
  ]
  edge [
    source 62
    target 310
  ]
  edge [
    source 62
    target 311
  ]
  edge [
    source 62
    target 312
  ]
  edge [
    source 62
    target 313
  ]
]
