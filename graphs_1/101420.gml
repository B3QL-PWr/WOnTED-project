graph [
  maxDegree 41
  minDegree 1
  meanDegree 1.9545454545454546
  density 0.045454545454545456
  graphCliqueNumber 2
  node [
    id 0
    label "fotoliza"
    origin "text"
  ]
  node [
    id 1
    label "woda"
    origin "text"
  ]
  node [
    id 2
    label "rozpad"
  ]
  node [
    id 3
    label "reakcja_chemiczna"
  ]
  node [
    id 4
    label "wypowied&#378;"
  ]
  node [
    id 5
    label "obiekt_naturalny"
  ]
  node [
    id 6
    label "bicie"
  ]
  node [
    id 7
    label "wysi&#281;k"
  ]
  node [
    id 8
    label "pustka"
  ]
  node [
    id 9
    label "woda_s&#322;odka"
  ]
  node [
    id 10
    label "p&#322;ycizna"
  ]
  node [
    id 11
    label "ciecz"
  ]
  node [
    id 12
    label "spi&#281;trza&#263;"
  ]
  node [
    id 13
    label "uj&#281;cie_wody"
  ]
  node [
    id 14
    label "chlasta&#263;"
  ]
  node [
    id 15
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 16
    label "nap&#243;j"
  ]
  node [
    id 17
    label "bombast"
  ]
  node [
    id 18
    label "water"
  ]
  node [
    id 19
    label "kryptodepresja"
  ]
  node [
    id 20
    label "wodnik"
  ]
  node [
    id 21
    label "pojazd"
  ]
  node [
    id 22
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 23
    label "fala"
  ]
  node [
    id 24
    label "Waruna"
  ]
  node [
    id 25
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 26
    label "zrzut"
  ]
  node [
    id 27
    label "dotleni&#263;"
  ]
  node [
    id 28
    label "utylizator"
  ]
  node [
    id 29
    label "przyroda"
  ]
  node [
    id 30
    label "uci&#261;g"
  ]
  node [
    id 31
    label "wybrze&#380;e"
  ]
  node [
    id 32
    label "nabranie"
  ]
  node [
    id 33
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 34
    label "chlastanie"
  ]
  node [
    id 35
    label "klarownik"
  ]
  node [
    id 36
    label "przybrze&#380;e"
  ]
  node [
    id 37
    label "deklamacja"
  ]
  node [
    id 38
    label "spi&#281;trzenie"
  ]
  node [
    id 39
    label "przybieranie"
  ]
  node [
    id 40
    label "nabra&#263;"
  ]
  node [
    id 41
    label "tlenek"
  ]
  node [
    id 42
    label "spi&#281;trzanie"
  ]
  node [
    id 43
    label "l&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
]
