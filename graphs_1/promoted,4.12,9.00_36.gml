graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.894736842105263
  density 0.10526315789473684
  graphCliqueNumber 2
  node [
    id 0
    label "samotno&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "samo"
    origin "text"
  ]
  node [
    id 2
    label "introwersja"
    origin "text"
  ]
  node [
    id 3
    label "l&#281;k"
    origin "text"
  ]
  node [
    id 4
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 5
    label "isolation"
  ]
  node [
    id 6
    label "izolacja"
  ]
  node [
    id 7
    label "samota"
  ]
  node [
    id 8
    label "wra&#380;enie"
  ]
  node [
    id 9
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 10
    label "akatyzja"
  ]
  node [
    id 11
    label "phobia"
  ]
  node [
    id 12
    label "ba&#263;_si&#281;"
  ]
  node [
    id 13
    label "emocja"
  ]
  node [
    id 14
    label "zastraszenie"
  ]
  node [
    id 15
    label "zastraszanie"
  ]
  node [
    id 16
    label "niepubliczny"
  ]
  node [
    id 17
    label "spo&#322;ecznie"
  ]
  node [
    id 18
    label "publiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
]
