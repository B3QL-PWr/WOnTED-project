graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.07692307692307693
  graphCliqueNumber 2
  node [
    id 0
    label "kierowca"
    origin "text"
  ]
  node [
    id 1
    label "suvem"
    origin "text"
  ]
  node [
    id 2
    label "jecha&#263;"
    origin "text"
  ]
  node [
    id 3
    label "cz&#322;owiek"
  ]
  node [
    id 4
    label "transportowiec"
  ]
  node [
    id 5
    label "proceed"
  ]
  node [
    id 6
    label "napada&#263;"
  ]
  node [
    id 7
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 8
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 9
    label "wykonywa&#263;"
  ]
  node [
    id 10
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 11
    label "czu&#263;"
  ]
  node [
    id 12
    label "overdrive"
  ]
  node [
    id 13
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 14
    label "ride"
  ]
  node [
    id 15
    label "korzysta&#263;"
  ]
  node [
    id 16
    label "go"
  ]
  node [
    id 17
    label "prowadzi&#263;"
  ]
  node [
    id 18
    label "continue"
  ]
  node [
    id 19
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 20
    label "drive"
  ]
  node [
    id 21
    label "kontynuowa&#263;"
  ]
  node [
    id 22
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 23
    label "odbywa&#263;"
  ]
  node [
    id 24
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 25
    label "carry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
]
