graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.3333333333333333
  graphCliqueNumber 3
  node [
    id 0
    label "maciej"
    origin "text"
  ]
  node [
    id 1
    label "erwin"
    origin "text"
  ]
  node [
    id 2
    label "halba&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "Maciej"
  ]
  node [
    id 4
    label "Erwina"
  ]
  node [
    id 5
    label "Halba&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
]
