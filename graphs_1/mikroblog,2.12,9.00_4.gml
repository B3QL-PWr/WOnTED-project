graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "ponownie"
    origin "text"
  ]
  node [
    id 1
    label "weekend"
    origin "text"
  ]
  node [
    id 2
    label "wszystko"
    origin "text"
  ]
  node [
    id 3
    label "dobre"
    origin "text"
  ]
  node [
    id 4
    label "znowu"
  ]
  node [
    id 5
    label "ponowny"
  ]
  node [
    id 6
    label "niedziela"
  ]
  node [
    id 7
    label "sobota"
  ]
  node [
    id 8
    label "tydzie&#324;"
  ]
  node [
    id 9
    label "lock"
  ]
  node [
    id 10
    label "absolut"
  ]
  node [
    id 11
    label "ca&#322;o&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
]
