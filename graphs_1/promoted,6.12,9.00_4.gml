graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.018018018018018
  density 0.018345618345618344
  graphCliqueNumber 2
  node [
    id 0
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 2
    label "&#347;wi&#281;towa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nadej&#347;cie"
    origin "text"
  ]
  node [
    id 4
    label "nowy"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;y"
  ]
  node [
    id 7
    label "jedyny"
  ]
  node [
    id 8
    label "kompletny"
  ]
  node [
    id 9
    label "zdr&#243;w"
  ]
  node [
    id 10
    label "&#380;ywy"
  ]
  node [
    id 11
    label "ca&#322;o"
  ]
  node [
    id 12
    label "pe&#322;ny"
  ]
  node [
    id 13
    label "calu&#347;ko"
  ]
  node [
    id 14
    label "podobny"
  ]
  node [
    id 15
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 16
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 17
    label "obszar"
  ]
  node [
    id 18
    label "obiekt_naturalny"
  ]
  node [
    id 19
    label "przedmiot"
  ]
  node [
    id 20
    label "biosfera"
  ]
  node [
    id 21
    label "grupa"
  ]
  node [
    id 22
    label "stw&#243;r"
  ]
  node [
    id 23
    label "Stary_&#346;wiat"
  ]
  node [
    id 24
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 25
    label "rzecz"
  ]
  node [
    id 26
    label "magnetosfera"
  ]
  node [
    id 27
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 28
    label "environment"
  ]
  node [
    id 29
    label "Nowy_&#346;wiat"
  ]
  node [
    id 30
    label "geosfera"
  ]
  node [
    id 31
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 32
    label "planeta"
  ]
  node [
    id 33
    label "przejmowa&#263;"
  ]
  node [
    id 34
    label "litosfera"
  ]
  node [
    id 35
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 36
    label "makrokosmos"
  ]
  node [
    id 37
    label "barysfera"
  ]
  node [
    id 38
    label "biota"
  ]
  node [
    id 39
    label "p&#243;&#322;noc"
  ]
  node [
    id 40
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 41
    label "fauna"
  ]
  node [
    id 42
    label "wszechstworzenie"
  ]
  node [
    id 43
    label "geotermia"
  ]
  node [
    id 44
    label "biegun"
  ]
  node [
    id 45
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 46
    label "ekosystem"
  ]
  node [
    id 47
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 48
    label "teren"
  ]
  node [
    id 49
    label "zjawisko"
  ]
  node [
    id 50
    label "p&#243;&#322;kula"
  ]
  node [
    id 51
    label "atmosfera"
  ]
  node [
    id 52
    label "mikrokosmos"
  ]
  node [
    id 53
    label "class"
  ]
  node [
    id 54
    label "po&#322;udnie"
  ]
  node [
    id 55
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 56
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 57
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 58
    label "przejmowanie"
  ]
  node [
    id 59
    label "przestrze&#324;"
  ]
  node [
    id 60
    label "asymilowanie_si&#281;"
  ]
  node [
    id 61
    label "przej&#261;&#263;"
  ]
  node [
    id 62
    label "ekosfera"
  ]
  node [
    id 63
    label "przyroda"
  ]
  node [
    id 64
    label "ciemna_materia"
  ]
  node [
    id 65
    label "geoida"
  ]
  node [
    id 66
    label "Wsch&#243;d"
  ]
  node [
    id 67
    label "populace"
  ]
  node [
    id 68
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 69
    label "huczek"
  ]
  node [
    id 70
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 71
    label "Ziemia"
  ]
  node [
    id 72
    label "universe"
  ]
  node [
    id 73
    label "ozonosfera"
  ]
  node [
    id 74
    label "rze&#378;ba"
  ]
  node [
    id 75
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 76
    label "zagranica"
  ]
  node [
    id 77
    label "hydrosfera"
  ]
  node [
    id 78
    label "woda"
  ]
  node [
    id 79
    label "kuchnia"
  ]
  node [
    id 80
    label "przej&#281;cie"
  ]
  node [
    id 81
    label "czarna_dziura"
  ]
  node [
    id 82
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 83
    label "morze"
  ]
  node [
    id 84
    label "bless"
  ]
  node [
    id 85
    label "obchodzi&#263;"
  ]
  node [
    id 86
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 87
    label "czas"
  ]
  node [
    id 88
    label "arrival"
  ]
  node [
    id 89
    label "przybycie"
  ]
  node [
    id 90
    label "stanie_si&#281;"
  ]
  node [
    id 91
    label "cz&#322;owiek"
  ]
  node [
    id 92
    label "nowotny"
  ]
  node [
    id 93
    label "drugi"
  ]
  node [
    id 94
    label "kolejny"
  ]
  node [
    id 95
    label "bie&#380;&#261;cy"
  ]
  node [
    id 96
    label "nowo"
  ]
  node [
    id 97
    label "narybek"
  ]
  node [
    id 98
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 99
    label "obcy"
  ]
  node [
    id 100
    label "stulecie"
  ]
  node [
    id 101
    label "kalendarz"
  ]
  node [
    id 102
    label "pora_roku"
  ]
  node [
    id 103
    label "cykl_astronomiczny"
  ]
  node [
    id 104
    label "p&#243;&#322;rocze"
  ]
  node [
    id 105
    label "kwarta&#322;"
  ]
  node [
    id 106
    label "kurs"
  ]
  node [
    id 107
    label "jubileusz"
  ]
  node [
    id 108
    label "miesi&#261;c"
  ]
  node [
    id 109
    label "lata"
  ]
  node [
    id 110
    label "martwy_sezon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
]
