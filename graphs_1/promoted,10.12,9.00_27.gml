graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "metody"
    origin "text"
  ]
  node [
    id 1
    label "budowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "chiny"
    origin "text"
  ]
  node [
    id 3
    label "planowa&#263;"
  ]
  node [
    id 4
    label "dodawa&#263;_otuchy"
  ]
  node [
    id 5
    label "consist"
  ]
  node [
    id 6
    label "train"
  ]
  node [
    id 7
    label "tworzy&#263;"
  ]
  node [
    id 8
    label "wytwarza&#263;"
  ]
  node [
    id 9
    label "raise"
  ]
  node [
    id 10
    label "stanowi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
]
