graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.1144578313253013
  density 0.006388090124849853
  graphCliqueNumber 3
  node [
    id 0
    label "charles"
    origin "text"
  ]
  node [
    id 1
    label "darwin"
    origin "text"
  ]
  node [
    id 2
    label "pewnie"
    origin "text"
  ]
  node [
    id 3
    label "nawet"
    origin "text"
  ]
  node [
    id 4
    label "podejrzewa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "polska"
    origin "text"
  ]
  node [
    id 6
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 7
    label "pod"
    origin "text"
  ]
  node [
    id 8
    label "kielce"
    origin "text"
  ]
  node [
    id 9
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 10
    label "odnale&#378;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "kolejny"
    origin "text"
  ]
  node [
    id 12
    label "niezbity"
    origin "text"
  ]
  node [
    id 13
    label "dow&#243;d"
    origin "text"
  ]
  node [
    id 14
    label "potwierdzaj&#261;cy"
    origin "text"
  ]
  node [
    id 15
    label "prze&#322;omowy"
    origin "text"
  ]
  node [
    id 16
    label "teoria"
    origin "text"
  ]
  node [
    id 17
    label "ewolucja"
    origin "text"
  ]
  node [
    id 18
    label "potrzebny"
    origin "text"
  ]
  node [
    id 19
    label "by&#263;"
    origin "text"
  ]
  node [
    id 20
    label "ponad"
    origin "text"
  ]
  node [
    id 21
    label "lata"
    origin "text"
  ]
  node [
    id 22
    label "aby"
    origin "text"
  ]
  node [
    id 23
    label "jednoznacznie"
    origin "text"
  ]
  node [
    id 24
    label "wyja&#347;ni&#263;"
    origin "text"
  ]
  node [
    id 25
    label "jak"
    origin "text"
  ]
  node [
    id 26
    label "ryba"
    origin "text"
  ]
  node [
    id 27
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 28
    label "si&#281;"
    origin "text"
  ]
  node [
    id 29
    label "l&#261;d"
    origin "text"
  ]
  node [
    id 30
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 31
    label "m&#322;ody"
    origin "text"
  ]
  node [
    id 32
    label "polski"
    origin "text"
  ]
  node [
    id 33
    label "naukowiec"
    origin "text"
  ]
  node [
    id 34
    label "uniwersytet"
    origin "text"
  ]
  node [
    id 35
    label "warszawskie"
    origin "text"
  ]
  node [
    id 36
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 37
    label "jeden"
    origin "text"
  ]
  node [
    id 38
    label "kamienio&#322;om"
    origin "text"
  ]
  node [
    id 39
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 40
    label "&#347;wi&#281;tokrzyski"
    origin "text"
  ]
  node [
    id 41
    label "najstarszy"
    origin "text"
  ]
  node [
    id 42
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 43
    label "&#347;lad"
    origin "text"
  ]
  node [
    id 44
    label "pierwsza"
    origin "text"
  ]
  node [
    id 45
    label "czworon&#243;g"
    origin "text"
  ]
  node [
    id 46
    label "zwinnie"
  ]
  node [
    id 47
    label "pewniej"
  ]
  node [
    id 48
    label "bezpiecznie"
  ]
  node [
    id 49
    label "wiarygodnie"
  ]
  node [
    id 50
    label "pewny"
  ]
  node [
    id 51
    label "mocno"
  ]
  node [
    id 52
    label "najpewniej"
  ]
  node [
    id 53
    label "przewidywa&#263;"
  ]
  node [
    id 54
    label "intuition"
  ]
  node [
    id 55
    label "os&#261;dza&#263;"
  ]
  node [
    id 56
    label "inflict"
  ]
  node [
    id 57
    label "przypuszcza&#263;"
  ]
  node [
    id 58
    label "proceed"
  ]
  node [
    id 59
    label "catch"
  ]
  node [
    id 60
    label "pozosta&#263;"
  ]
  node [
    id 61
    label "osta&#263;_si&#281;"
  ]
  node [
    id 62
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 63
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 64
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 65
    label "change"
  ]
  node [
    id 66
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 67
    label "odzyska&#263;"
  ]
  node [
    id 68
    label "zauwa&#380;y&#263;"
  ]
  node [
    id 69
    label "wybra&#263;"
  ]
  node [
    id 70
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 71
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 72
    label "discover"
  ]
  node [
    id 73
    label "znaj&#347;&#263;"
  ]
  node [
    id 74
    label "devise"
  ]
  node [
    id 75
    label "inny"
  ]
  node [
    id 76
    label "nast&#281;pnie"
  ]
  node [
    id 77
    label "kt&#243;ry&#347;"
  ]
  node [
    id 78
    label "kolejno"
  ]
  node [
    id 79
    label "nastopny"
  ]
  node [
    id 80
    label "niepodwa&#380;alnie"
  ]
  node [
    id 81
    label "dokument"
  ]
  node [
    id 82
    label "forsing"
  ]
  node [
    id 83
    label "certificate"
  ]
  node [
    id 84
    label "rewizja"
  ]
  node [
    id 85
    label "argument"
  ]
  node [
    id 86
    label "act"
  ]
  node [
    id 87
    label "rzecz"
  ]
  node [
    id 88
    label "&#347;rodek"
  ]
  node [
    id 89
    label "uzasadnienie"
  ]
  node [
    id 90
    label "potakuj&#261;cy"
  ]
  node [
    id 91
    label "potwierdzaj&#261;co"
  ]
  node [
    id 92
    label "twierdz&#261;cy"
  ]
  node [
    id 93
    label "donios&#322;y"
  ]
  node [
    id 94
    label "innowacyjny"
  ]
  node [
    id 95
    label "prze&#322;omowo"
  ]
  node [
    id 96
    label "wa&#380;ny"
  ]
  node [
    id 97
    label "system"
  ]
  node [
    id 98
    label "zderzenie_si&#281;"
  ]
  node [
    id 99
    label "s&#261;d"
  ]
  node [
    id 100
    label "twierdzenie"
  ]
  node [
    id 101
    label "teoria_Dowa"
  ]
  node [
    id 102
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 103
    label "teologicznie"
  ]
  node [
    id 104
    label "przypuszczenie"
  ]
  node [
    id 105
    label "belief"
  ]
  node [
    id 106
    label "wiedza"
  ]
  node [
    id 107
    label "teoria_Fishera"
  ]
  node [
    id 108
    label "teoria_Arrheniusa"
  ]
  node [
    id 109
    label "figura"
  ]
  node [
    id 110
    label "dob&#243;r_krewniaczy"
  ]
  node [
    id 111
    label "Darwin"
  ]
  node [
    id 112
    label "regresja"
  ]
  node [
    id 113
    label "development"
  ]
  node [
    id 114
    label "rozw&#243;j"
  ]
  node [
    id 115
    label "evolution"
  ]
  node [
    id 116
    label "pionizacja"
  ]
  node [
    id 117
    label "proces"
  ]
  node [
    id 118
    label "zjawisko"
  ]
  node [
    id 119
    label "acrobatics"
  ]
  node [
    id 120
    label "z&#322;ote_czasy"
  ]
  node [
    id 121
    label "proces_biologiczny"
  ]
  node [
    id 122
    label "potrzebnie"
  ]
  node [
    id 123
    label "przydatny"
  ]
  node [
    id 124
    label "si&#281;ga&#263;"
  ]
  node [
    id 125
    label "trwa&#263;"
  ]
  node [
    id 126
    label "obecno&#347;&#263;"
  ]
  node [
    id 127
    label "stan"
  ]
  node [
    id 128
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 129
    label "stand"
  ]
  node [
    id 130
    label "mie&#263;_miejsce"
  ]
  node [
    id 131
    label "uczestniczy&#263;"
  ]
  node [
    id 132
    label "chodzi&#263;"
  ]
  node [
    id 133
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 134
    label "equal"
  ]
  node [
    id 135
    label "summer"
  ]
  node [
    id 136
    label "czas"
  ]
  node [
    id 137
    label "troch&#281;"
  ]
  node [
    id 138
    label "jednoznaczny"
  ]
  node [
    id 139
    label "explain"
  ]
  node [
    id 140
    label "przedstawi&#263;"
  ]
  node [
    id 141
    label "poja&#347;ni&#263;"
  ]
  node [
    id 142
    label "clear"
  ]
  node [
    id 143
    label "byd&#322;o"
  ]
  node [
    id 144
    label "zobo"
  ]
  node [
    id 145
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 146
    label "yakalo"
  ]
  node [
    id 147
    label "dzo"
  ]
  node [
    id 148
    label "cz&#322;owiek"
  ]
  node [
    id 149
    label "wyrostek_filtracyjny"
  ]
  node [
    id 150
    label "doniczkowiec"
  ]
  node [
    id 151
    label "tar&#322;o"
  ]
  node [
    id 152
    label "patroszy&#263;"
  ]
  node [
    id 153
    label "rakowato&#347;&#263;"
  ]
  node [
    id 154
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 155
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 156
    label "pokrywa_skrzelowa"
  ]
  node [
    id 157
    label "ikra"
  ]
  node [
    id 158
    label "fish"
  ]
  node [
    id 159
    label "w&#281;dkarstwo"
  ]
  node [
    id 160
    label "ryby"
  ]
  node [
    id 161
    label "szczelina_skrzelowa"
  ]
  node [
    id 162
    label "m&#281;tnooki"
  ]
  node [
    id 163
    label "systemik"
  ]
  node [
    id 164
    label "linia_boczna"
  ]
  node [
    id 165
    label "kr&#281;gowiec"
  ]
  node [
    id 166
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 167
    label "mi&#281;so"
  ]
  node [
    id 168
    label "oceni&#263;"
  ]
  node [
    id 169
    label "wymy&#347;li&#263;"
  ]
  node [
    id 170
    label "invent"
  ]
  node [
    id 171
    label "pozyska&#263;"
  ]
  node [
    id 172
    label "wykry&#263;"
  ]
  node [
    id 173
    label "dozna&#263;"
  ]
  node [
    id 174
    label "obszar"
  ]
  node [
    id 175
    label "skorupa_ziemska"
  ]
  node [
    id 176
    label "pojazd"
  ]
  node [
    id 177
    label "przesta&#263;"
  ]
  node [
    id 178
    label "zrobi&#263;"
  ]
  node [
    id 179
    label "cause"
  ]
  node [
    id 180
    label "communicate"
  ]
  node [
    id 181
    label "nie&#380;onaty"
  ]
  node [
    id 182
    label "wczesny"
  ]
  node [
    id 183
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 184
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 185
    label "charakterystyczny"
  ]
  node [
    id 186
    label "nowo&#380;eniec"
  ]
  node [
    id 187
    label "m&#261;&#380;"
  ]
  node [
    id 188
    label "m&#322;odo"
  ]
  node [
    id 189
    label "nowy"
  ]
  node [
    id 190
    label "lacki"
  ]
  node [
    id 191
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 192
    label "przedmiot"
  ]
  node [
    id 193
    label "sztajer"
  ]
  node [
    id 194
    label "drabant"
  ]
  node [
    id 195
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 196
    label "polak"
  ]
  node [
    id 197
    label "pierogi_ruskie"
  ]
  node [
    id 198
    label "krakowiak"
  ]
  node [
    id 199
    label "Polish"
  ]
  node [
    id 200
    label "j&#281;zyk"
  ]
  node [
    id 201
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 202
    label "oberek"
  ]
  node [
    id 203
    label "po_polsku"
  ]
  node [
    id 204
    label "mazur"
  ]
  node [
    id 205
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 206
    label "chodzony"
  ]
  node [
    id 207
    label "skoczny"
  ]
  node [
    id 208
    label "ryba_po_grecku"
  ]
  node [
    id 209
    label "goniony"
  ]
  node [
    id 210
    label "polsko"
  ]
  node [
    id 211
    label "Miczurin"
  ]
  node [
    id 212
    label "&#347;ledziciel"
  ]
  node [
    id 213
    label "uczony"
  ]
  node [
    id 214
    label "ku&#378;nia"
  ]
  node [
    id 215
    label "Harvard"
  ]
  node [
    id 216
    label "uczelnia"
  ]
  node [
    id 217
    label "Sorbona"
  ]
  node [
    id 218
    label "Uniwersytet_Oksfordzki"
  ]
  node [
    id 219
    label "Stanford"
  ]
  node [
    id 220
    label "Princeton"
  ]
  node [
    id 221
    label "academy"
  ]
  node [
    id 222
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 223
    label "wydzia&#322;"
  ]
  node [
    id 224
    label "kieliszek"
  ]
  node [
    id 225
    label "shot"
  ]
  node [
    id 226
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 227
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 228
    label "jaki&#347;"
  ]
  node [
    id 229
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 230
    label "jednolicie"
  ]
  node [
    id 231
    label "w&#243;dka"
  ]
  node [
    id 232
    label "ten"
  ]
  node [
    id 233
    label "ujednolicenie"
  ]
  node [
    id 234
    label "jednakowy"
  ]
  node [
    id 235
    label "kopalnia"
  ]
  node [
    id 236
    label "grupa"
  ]
  node [
    id 237
    label "element"
  ]
  node [
    id 238
    label "przele&#378;&#263;"
  ]
  node [
    id 239
    label "pi&#281;tro"
  ]
  node [
    id 240
    label "karczek"
  ]
  node [
    id 241
    label "wysoki"
  ]
  node [
    id 242
    label "rami&#261;czko"
  ]
  node [
    id 243
    label "Ropa"
  ]
  node [
    id 244
    label "Jaworze"
  ]
  node [
    id 245
    label "Synaj"
  ]
  node [
    id 246
    label "wzniesienie"
  ]
  node [
    id 247
    label "przelezienie"
  ]
  node [
    id 248
    label "&#347;piew"
  ]
  node [
    id 249
    label "kupa"
  ]
  node [
    id 250
    label "kierunek"
  ]
  node [
    id 251
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 252
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 253
    label "d&#378;wi&#281;k"
  ]
  node [
    id 254
    label "Kreml"
  ]
  node [
    id 255
    label "famu&#322;a"
  ]
  node [
    id 256
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 257
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 258
    label "obiekt_naturalny"
  ]
  node [
    id 259
    label "biosfera"
  ]
  node [
    id 260
    label "stw&#243;r"
  ]
  node [
    id 261
    label "Stary_&#346;wiat"
  ]
  node [
    id 262
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 263
    label "magnetosfera"
  ]
  node [
    id 264
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 265
    label "environment"
  ]
  node [
    id 266
    label "Nowy_&#346;wiat"
  ]
  node [
    id 267
    label "geosfera"
  ]
  node [
    id 268
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 269
    label "planeta"
  ]
  node [
    id 270
    label "przejmowa&#263;"
  ]
  node [
    id 271
    label "litosfera"
  ]
  node [
    id 272
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 273
    label "makrokosmos"
  ]
  node [
    id 274
    label "barysfera"
  ]
  node [
    id 275
    label "biota"
  ]
  node [
    id 276
    label "p&#243;&#322;noc"
  ]
  node [
    id 277
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 278
    label "fauna"
  ]
  node [
    id 279
    label "wszechstworzenie"
  ]
  node [
    id 280
    label "geotermia"
  ]
  node [
    id 281
    label "biegun"
  ]
  node [
    id 282
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 283
    label "ekosystem"
  ]
  node [
    id 284
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 285
    label "teren"
  ]
  node [
    id 286
    label "p&#243;&#322;kula"
  ]
  node [
    id 287
    label "atmosfera"
  ]
  node [
    id 288
    label "mikrokosmos"
  ]
  node [
    id 289
    label "class"
  ]
  node [
    id 290
    label "po&#322;udnie"
  ]
  node [
    id 291
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 292
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 293
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 294
    label "przejmowanie"
  ]
  node [
    id 295
    label "przestrze&#324;"
  ]
  node [
    id 296
    label "asymilowanie_si&#281;"
  ]
  node [
    id 297
    label "przej&#261;&#263;"
  ]
  node [
    id 298
    label "ekosfera"
  ]
  node [
    id 299
    label "przyroda"
  ]
  node [
    id 300
    label "ciemna_materia"
  ]
  node [
    id 301
    label "geoida"
  ]
  node [
    id 302
    label "Wsch&#243;d"
  ]
  node [
    id 303
    label "populace"
  ]
  node [
    id 304
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 305
    label "huczek"
  ]
  node [
    id 306
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 307
    label "Ziemia"
  ]
  node [
    id 308
    label "universe"
  ]
  node [
    id 309
    label "ozonosfera"
  ]
  node [
    id 310
    label "rze&#378;ba"
  ]
  node [
    id 311
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 312
    label "zagranica"
  ]
  node [
    id 313
    label "hydrosfera"
  ]
  node [
    id 314
    label "woda"
  ]
  node [
    id 315
    label "kuchnia"
  ]
  node [
    id 316
    label "przej&#281;cie"
  ]
  node [
    id 317
    label "czarna_dziura"
  ]
  node [
    id 318
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 319
    label "morze"
  ]
  node [
    id 320
    label "sznurowanie"
  ]
  node [
    id 321
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 322
    label "odrobina"
  ]
  node [
    id 323
    label "sznurowa&#263;"
  ]
  node [
    id 324
    label "attribute"
  ]
  node [
    id 325
    label "wp&#322;yw"
  ]
  node [
    id 326
    label "odcisk"
  ]
  node [
    id 327
    label "skutek"
  ]
  node [
    id 328
    label "godzina"
  ]
  node [
    id 329
    label "zwierz&#281;_domowe"
  ]
  node [
    id 330
    label "tetrapody"
  ]
  node [
    id 331
    label "critter"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 27
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 50
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 135
  ]
  edge [
    source 21
    target 136
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 138
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 139
  ]
  edge [
    source 24
    target 140
  ]
  edge [
    source 24
    target 141
  ]
  edge [
    source 24
    target 142
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 143
  ]
  edge [
    source 25
    target 144
  ]
  edge [
    source 25
    target 145
  ]
  edge [
    source 25
    target 146
  ]
  edge [
    source 25
    target 147
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 148
  ]
  edge [
    source 26
    target 149
  ]
  edge [
    source 26
    target 150
  ]
  edge [
    source 26
    target 151
  ]
  edge [
    source 26
    target 152
  ]
  edge [
    source 26
    target 153
  ]
  edge [
    source 26
    target 97
  ]
  edge [
    source 26
    target 154
  ]
  edge [
    source 26
    target 155
  ]
  edge [
    source 26
    target 156
  ]
  edge [
    source 26
    target 157
  ]
  edge [
    source 26
    target 158
  ]
  edge [
    source 26
    target 159
  ]
  edge [
    source 26
    target 160
  ]
  edge [
    source 26
    target 161
  ]
  edge [
    source 26
    target 162
  ]
  edge [
    source 26
    target 163
  ]
  edge [
    source 26
    target 164
  ]
  edge [
    source 26
    target 165
  ]
  edge [
    source 26
    target 166
  ]
  edge [
    source 26
    target 167
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 67
  ]
  edge [
    source 27
    target 74
  ]
  edge [
    source 27
    target 168
  ]
  edge [
    source 27
    target 73
  ]
  edge [
    source 27
    target 169
  ]
  edge [
    source 27
    target 170
  ]
  edge [
    source 27
    target 171
  ]
  edge [
    source 27
    target 71
  ]
  edge [
    source 27
    target 172
  ]
  edge [
    source 27
    target 64
  ]
  edge [
    source 27
    target 173
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 174
  ]
  edge [
    source 29
    target 175
  ]
  edge [
    source 29
    target 176
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 177
  ]
  edge [
    source 30
    target 178
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 148
  ]
  edge [
    source 31
    target 181
  ]
  edge [
    source 31
    target 182
  ]
  edge [
    source 31
    target 183
  ]
  edge [
    source 31
    target 184
  ]
  edge [
    source 31
    target 185
  ]
  edge [
    source 31
    target 186
  ]
  edge [
    source 31
    target 187
  ]
  edge [
    source 31
    target 188
  ]
  edge [
    source 31
    target 189
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 190
  ]
  edge [
    source 32
    target 191
  ]
  edge [
    source 32
    target 192
  ]
  edge [
    source 32
    target 193
  ]
  edge [
    source 32
    target 194
  ]
  edge [
    source 32
    target 195
  ]
  edge [
    source 32
    target 196
  ]
  edge [
    source 32
    target 197
  ]
  edge [
    source 32
    target 198
  ]
  edge [
    source 32
    target 199
  ]
  edge [
    source 32
    target 200
  ]
  edge [
    source 32
    target 201
  ]
  edge [
    source 32
    target 202
  ]
  edge [
    source 32
    target 203
  ]
  edge [
    source 32
    target 204
  ]
  edge [
    source 32
    target 205
  ]
  edge [
    source 32
    target 206
  ]
  edge [
    source 32
    target 207
  ]
  edge [
    source 32
    target 208
  ]
  edge [
    source 32
    target 209
  ]
  edge [
    source 32
    target 210
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 211
  ]
  edge [
    source 33
    target 212
  ]
  edge [
    source 33
    target 213
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 214
  ]
  edge [
    source 34
    target 215
  ]
  edge [
    source 34
    target 216
  ]
  edge [
    source 34
    target 217
  ]
  edge [
    source 34
    target 218
  ]
  edge [
    source 34
    target 219
  ]
  edge [
    source 34
    target 220
  ]
  edge [
    source 34
    target 221
  ]
  edge [
    source 34
    target 222
  ]
  edge [
    source 34
    target 223
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 224
  ]
  edge [
    source 37
    target 225
  ]
  edge [
    source 37
    target 226
  ]
  edge [
    source 37
    target 227
  ]
  edge [
    source 37
    target 228
  ]
  edge [
    source 37
    target 229
  ]
  edge [
    source 37
    target 230
  ]
  edge [
    source 37
    target 231
  ]
  edge [
    source 37
    target 232
  ]
  edge [
    source 37
    target 233
  ]
  edge [
    source 37
    target 234
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 235
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 192
  ]
  edge [
    source 39
    target 236
  ]
  edge [
    source 39
    target 237
  ]
  edge [
    source 39
    target 238
  ]
  edge [
    source 39
    target 239
  ]
  edge [
    source 39
    target 240
  ]
  edge [
    source 39
    target 241
  ]
  edge [
    source 39
    target 242
  ]
  edge [
    source 39
    target 243
  ]
  edge [
    source 39
    target 244
  ]
  edge [
    source 39
    target 245
  ]
  edge [
    source 39
    target 246
  ]
  edge [
    source 39
    target 247
  ]
  edge [
    source 39
    target 248
  ]
  edge [
    source 39
    target 249
  ]
  edge [
    source 39
    target 250
  ]
  edge [
    source 39
    target 251
  ]
  edge [
    source 39
    target 252
  ]
  edge [
    source 39
    target 253
  ]
  edge [
    source 39
    target 254
  ]
  edge [
    source 40
    target 255
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 256
  ]
  edge [
    source 42
    target 257
  ]
  edge [
    source 42
    target 174
  ]
  edge [
    source 42
    target 258
  ]
  edge [
    source 42
    target 192
  ]
  edge [
    source 42
    target 259
  ]
  edge [
    source 42
    target 236
  ]
  edge [
    source 42
    target 260
  ]
  edge [
    source 42
    target 261
  ]
  edge [
    source 42
    target 262
  ]
  edge [
    source 42
    target 87
  ]
  edge [
    source 42
    target 263
  ]
  edge [
    source 42
    target 264
  ]
  edge [
    source 42
    target 265
  ]
  edge [
    source 42
    target 266
  ]
  edge [
    source 42
    target 267
  ]
  edge [
    source 42
    target 268
  ]
  edge [
    source 42
    target 269
  ]
  edge [
    source 42
    target 270
  ]
  edge [
    source 42
    target 271
  ]
  edge [
    source 42
    target 272
  ]
  edge [
    source 42
    target 273
  ]
  edge [
    source 42
    target 274
  ]
  edge [
    source 42
    target 275
  ]
  edge [
    source 42
    target 276
  ]
  edge [
    source 42
    target 277
  ]
  edge [
    source 42
    target 278
  ]
  edge [
    source 42
    target 279
  ]
  edge [
    source 42
    target 280
  ]
  edge [
    source 42
    target 281
  ]
  edge [
    source 42
    target 282
  ]
  edge [
    source 42
    target 283
  ]
  edge [
    source 42
    target 284
  ]
  edge [
    source 42
    target 285
  ]
  edge [
    source 42
    target 118
  ]
  edge [
    source 42
    target 286
  ]
  edge [
    source 42
    target 287
  ]
  edge [
    source 42
    target 288
  ]
  edge [
    source 42
    target 289
  ]
  edge [
    source 42
    target 290
  ]
  edge [
    source 42
    target 291
  ]
  edge [
    source 42
    target 292
  ]
  edge [
    source 42
    target 293
  ]
  edge [
    source 42
    target 294
  ]
  edge [
    source 42
    target 295
  ]
  edge [
    source 42
    target 296
  ]
  edge [
    source 42
    target 297
  ]
  edge [
    source 42
    target 298
  ]
  edge [
    source 42
    target 299
  ]
  edge [
    source 42
    target 300
  ]
  edge [
    source 42
    target 301
  ]
  edge [
    source 42
    target 302
  ]
  edge [
    source 42
    target 303
  ]
  edge [
    source 42
    target 304
  ]
  edge [
    source 42
    target 305
  ]
  edge [
    source 42
    target 306
  ]
  edge [
    source 42
    target 307
  ]
  edge [
    source 42
    target 308
  ]
  edge [
    source 42
    target 309
  ]
  edge [
    source 42
    target 310
  ]
  edge [
    source 42
    target 311
  ]
  edge [
    source 42
    target 312
  ]
  edge [
    source 42
    target 313
  ]
  edge [
    source 42
    target 314
  ]
  edge [
    source 42
    target 315
  ]
  edge [
    source 42
    target 316
  ]
  edge [
    source 42
    target 317
  ]
  edge [
    source 42
    target 318
  ]
  edge [
    source 42
    target 319
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 320
  ]
  edge [
    source 43
    target 321
  ]
  edge [
    source 43
    target 322
  ]
  edge [
    source 43
    target 323
  ]
  edge [
    source 43
    target 324
  ]
  edge [
    source 43
    target 325
  ]
  edge [
    source 43
    target 326
  ]
  edge [
    source 43
    target 327
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 328
  ]
  edge [
    source 45
    target 165
  ]
  edge [
    source 45
    target 329
  ]
  edge [
    source 45
    target 330
  ]
  edge [
    source 45
    target 331
  ]
]
