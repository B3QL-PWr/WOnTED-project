graph [
  maxDegree 2
  minDegree 0
  meanDegree 1.3333333333333333
  density 0.26666666666666666
  graphCliqueNumber 3
  node [
    id 0
    label "hej"
    origin "text"
  ]
  node [
    id 1
    label "bez"
  ]
  node [
    id 2
    label "Photoshopa"
  ]
  node [
    id 3
    label "ISO"
  ]
  node [
    id 4
    label "200"
  ]
  node [
    id 5
    label "httpswwwinstagramcomgratyfixation"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
]
