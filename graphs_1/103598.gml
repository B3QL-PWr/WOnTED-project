graph [
  maxDegree 43
  minDegree 1
  meanDegree 2.128440366972477
  density 0.004892966360856269
  graphCliqueNumber 3
  node [
    id 0
    label "kasztan"
    origin "text"
  ]
  node [
    id 1
    label "kwitn&#261;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "chyba"
    origin "text"
  ]
  node [
    id 3
    label "obrodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 5
    label "tekst"
    origin "text"
  ]
  node [
    id 6
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "matura"
    origin "text"
  ]
  node [
    id 8
    label "zasada"
    origin "text"
  ]
  node [
    id 9
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "media"
    origin "text"
  ]
  node [
    id 11
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 12
    label "przedrukowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zesz&#322;y"
    origin "text"
  ]
  node [
    id 14
    label "rok"
    origin "text"
  ]
  node [
    id 15
    label "zamiast"
    origin "text"
  ]
  node [
    id 16
    label "nowa"
    origin "text"
  ]
  node [
    id 17
    label "czas"
    origin "text"
  ]
  node [
    id 18
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "wysili&#263;"
    origin "text"
  ]
  node [
    id 21
    label "bardzo"
    origin "text"
  ]
  node [
    id 22
    label "pofantazjowa&#263;"
    origin "text"
  ]
  node [
    id 23
    label "temat"
    origin "text"
  ]
  node [
    id 24
    label "idealny"
    origin "text"
  ]
  node [
    id 25
    label "system"
    origin "text"
  ]
  node [
    id 26
    label "szkolnictwo"
    origin "text"
  ]
  node [
    id 27
    label "gdzie"
    origin "text"
  ]
  node [
    id 28
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 29
    label "rywalizacja"
    origin "text"
  ]
  node [
    id 30
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 31
    label "uczy&#263;"
    origin "text"
  ]
  node [
    id 32
    label "pasja"
    origin "text"
  ]
  node [
    id 33
    label "ucze&#324;"
    origin "text"
  ]
  node [
    id 34
    label "rozwija&#263;"
    origin "text"
  ]
  node [
    id 35
    label "swoje"
    origin "text"
  ]
  node [
    id 36
    label "zainteresowania"
    origin "text"
  ]
  node [
    id 37
    label "og&#243;&#322;"
    origin "text"
  ]
  node [
    id 38
    label "sielanka"
    origin "text"
  ]
  node [
    id 39
    label "niczym"
    origin "text"
  ]
  node [
    id 40
    label "pod"
    origin "text"
  ]
  node [
    id 41
    label "um&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "jawor"
    origin "text"
  ]
  node [
    id 43
    label "wystarczy&#263;by"
    origin "text"
  ]
  node [
    id 44
    label "zlikwidowa&#263;"
    origin "text"
  ]
  node [
    id 45
    label "egzamin"
    origin "text"
  ]
  node [
    id 46
    label "og&#243;lnopolski"
    origin "text"
  ]
  node [
    id 47
    label "ten"
    origin "text"
  ]
  node [
    id 48
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 49
    label "skomentowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 50
    label "tablica"
    origin "text"
  ]
  node [
    id 51
    label "pewne"
    origin "text"
  ]
  node [
    id 52
    label "d&#380;entelmen"
    origin "text"
  ]
  node [
    id 53
    label "zna&#263;"
    origin "text"
  ]
  node [
    id 54
    label "niemal"
    origin "text"
  ]
  node [
    id 55
    label "wszyscy"
    origin "text"
  ]
  node [
    id 56
    label "czytelnik"
    origin "text"
  ]
  node [
    id 57
    label "moje"
    origin "text"
  ]
  node [
    id 58
    label "blog"
    origin "text"
  ]
  node [
    id 59
    label "powtarza&#263;"
    origin "text"
  ]
  node [
    id 60
    label "pom&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 61
    label "matematyka"
    origin "text"
  ]
  node [
    id 62
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 63
    label "bukowate"
  ]
  node [
    id 64
    label "chestnut"
  ]
  node [
    id 65
    label "orzech"
  ]
  node [
    id 66
    label "torebka"
  ]
  node [
    id 67
    label "br&#261;z"
  ]
  node [
    id 68
    label "ko&#324;"
  ]
  node [
    id 69
    label "nasiono"
  ]
  node [
    id 70
    label "drewno"
  ]
  node [
    id 71
    label "ro&#347;lina_truj&#261;ca"
  ]
  node [
    id 72
    label "szrot&#243;wek_kasztanowcowiaczek"
  ]
  node [
    id 73
    label "kasztanowcowate"
  ]
  node [
    id 74
    label "mydle&#324;cowate"
  ]
  node [
    id 75
    label "drzewo"
  ]
  node [
    id 76
    label "flower"
  ]
  node [
    id 77
    label "b&#322;yszcze&#263;"
  ]
  node [
    id 78
    label "wygl&#261;da&#263;"
  ]
  node [
    id 79
    label "prosperowa&#263;"
  ]
  node [
    id 80
    label "zarasta&#263;"
  ]
  node [
    id 81
    label "tkwi&#263;"
  ]
  node [
    id 82
    label "blow"
  ]
  node [
    id 83
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 84
    label "wyda&#263;"
  ]
  node [
    id 85
    label "r&#243;&#380;nie"
  ]
  node [
    id 86
    label "inny"
  ]
  node [
    id 87
    label "jaki&#347;"
  ]
  node [
    id 88
    label "pisa&#263;"
  ]
  node [
    id 89
    label "odmianka"
  ]
  node [
    id 90
    label "opu&#347;ci&#263;"
  ]
  node [
    id 91
    label "wypowied&#378;"
  ]
  node [
    id 92
    label "wytw&#243;r"
  ]
  node [
    id 93
    label "koniektura"
  ]
  node [
    id 94
    label "preparacja"
  ]
  node [
    id 95
    label "ekscerpcja"
  ]
  node [
    id 96
    label "redakcja"
  ]
  node [
    id 97
    label "obelga"
  ]
  node [
    id 98
    label "dzie&#322;o"
  ]
  node [
    id 99
    label "j&#281;zykowo"
  ]
  node [
    id 100
    label "pomini&#281;cie"
  ]
  node [
    id 101
    label "bargain"
  ]
  node [
    id 102
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 103
    label "tycze&#263;"
  ]
  node [
    id 104
    label "&#347;wiadectwo"
  ]
  node [
    id 105
    label "studni&#243;wka"
  ]
  node [
    id 106
    label "matriculation"
  ]
  node [
    id 107
    label "obserwacja"
  ]
  node [
    id 108
    label "moralno&#347;&#263;"
  ]
  node [
    id 109
    label "podstawa"
  ]
  node [
    id 110
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 111
    label "umowa"
  ]
  node [
    id 112
    label "dominion"
  ]
  node [
    id 113
    label "qualification"
  ]
  node [
    id 114
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 115
    label "opis"
  ]
  node [
    id 116
    label "regu&#322;a_Allena"
  ]
  node [
    id 117
    label "normalizacja"
  ]
  node [
    id 118
    label "regu&#322;a_Glogera"
  ]
  node [
    id 119
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 120
    label "standard"
  ]
  node [
    id 121
    label "base"
  ]
  node [
    id 122
    label "substancja"
  ]
  node [
    id 123
    label "spos&#243;b"
  ]
  node [
    id 124
    label "prawid&#322;o"
  ]
  node [
    id 125
    label "prawo_Mendla"
  ]
  node [
    id 126
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 127
    label "criterion"
  ]
  node [
    id 128
    label "twierdzenie"
  ]
  node [
    id 129
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 130
    label "prawo"
  ]
  node [
    id 131
    label "occupation"
  ]
  node [
    id 132
    label "zasada_d'Alemberta"
  ]
  node [
    id 133
    label "majority"
  ]
  node [
    id 134
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 135
    label "przekazior"
  ]
  node [
    id 136
    label "mass-media"
  ]
  node [
    id 137
    label "uzbrajanie"
  ]
  node [
    id 138
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 139
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 140
    label "medium"
  ]
  node [
    id 141
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 142
    label "og&#322;osi&#263;_drukiem"
  ]
  node [
    id 143
    label "reissue"
  ]
  node [
    id 144
    label "skopiowa&#263;"
  ]
  node [
    id 145
    label "ostatni"
  ]
  node [
    id 146
    label "stulecie"
  ]
  node [
    id 147
    label "kalendarz"
  ]
  node [
    id 148
    label "pora_roku"
  ]
  node [
    id 149
    label "cykl_astronomiczny"
  ]
  node [
    id 150
    label "p&#243;&#322;rocze"
  ]
  node [
    id 151
    label "grupa"
  ]
  node [
    id 152
    label "kwarta&#322;"
  ]
  node [
    id 153
    label "kurs"
  ]
  node [
    id 154
    label "jubileusz"
  ]
  node [
    id 155
    label "miesi&#261;c"
  ]
  node [
    id 156
    label "lata"
  ]
  node [
    id 157
    label "martwy_sezon"
  ]
  node [
    id 158
    label "gwiazda"
  ]
  node [
    id 159
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 160
    label "czasokres"
  ]
  node [
    id 161
    label "trawienie"
  ]
  node [
    id 162
    label "kategoria_gramatyczna"
  ]
  node [
    id 163
    label "period"
  ]
  node [
    id 164
    label "odczyt"
  ]
  node [
    id 165
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 166
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 167
    label "chwila"
  ]
  node [
    id 168
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 169
    label "poprzedzenie"
  ]
  node [
    id 170
    label "koniugacja"
  ]
  node [
    id 171
    label "dzieje"
  ]
  node [
    id 172
    label "poprzedzi&#263;"
  ]
  node [
    id 173
    label "przep&#322;ywanie"
  ]
  node [
    id 174
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 175
    label "odwlekanie_si&#281;"
  ]
  node [
    id 176
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 177
    label "Zeitgeist"
  ]
  node [
    id 178
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 179
    label "okres_czasu"
  ]
  node [
    id 180
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 181
    label "pochodzi&#263;"
  ]
  node [
    id 182
    label "schy&#322;ek"
  ]
  node [
    id 183
    label "czwarty_wymiar"
  ]
  node [
    id 184
    label "chronometria"
  ]
  node [
    id 185
    label "poprzedzanie"
  ]
  node [
    id 186
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 187
    label "pogoda"
  ]
  node [
    id 188
    label "zegar"
  ]
  node [
    id 189
    label "trawi&#263;"
  ]
  node [
    id 190
    label "pochodzenie"
  ]
  node [
    id 191
    label "poprzedza&#263;"
  ]
  node [
    id 192
    label "time_period"
  ]
  node [
    id 193
    label "rachuba_czasu"
  ]
  node [
    id 194
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 195
    label "czasoprzestrze&#324;"
  ]
  node [
    id 196
    label "laba"
  ]
  node [
    id 197
    label "cz&#322;owiek"
  ]
  node [
    id 198
    label "znaczenie"
  ]
  node [
    id 199
    label "go&#347;&#263;"
  ]
  node [
    id 200
    label "osoba"
  ]
  node [
    id 201
    label "posta&#263;"
  ]
  node [
    id 202
    label "wzmocni&#263;"
  ]
  node [
    id 203
    label "strive"
  ]
  node [
    id 204
    label "w_chuj"
  ]
  node [
    id 205
    label "fraza"
  ]
  node [
    id 206
    label "forma"
  ]
  node [
    id 207
    label "melodia"
  ]
  node [
    id 208
    label "rzecz"
  ]
  node [
    id 209
    label "zbacza&#263;"
  ]
  node [
    id 210
    label "entity"
  ]
  node [
    id 211
    label "omawia&#263;"
  ]
  node [
    id 212
    label "topik"
  ]
  node [
    id 213
    label "wyraz_pochodny"
  ]
  node [
    id 214
    label "om&#243;wi&#263;"
  ]
  node [
    id 215
    label "omawianie"
  ]
  node [
    id 216
    label "w&#261;tek"
  ]
  node [
    id 217
    label "forum"
  ]
  node [
    id 218
    label "cecha"
  ]
  node [
    id 219
    label "zboczenie"
  ]
  node [
    id 220
    label "zbaczanie"
  ]
  node [
    id 221
    label "tre&#347;&#263;"
  ]
  node [
    id 222
    label "tematyka"
  ]
  node [
    id 223
    label "sprawa"
  ]
  node [
    id 224
    label "istota"
  ]
  node [
    id 225
    label "otoczka"
  ]
  node [
    id 226
    label "zboczy&#263;"
  ]
  node [
    id 227
    label "om&#243;wienie"
  ]
  node [
    id 228
    label "wznios&#322;y"
  ]
  node [
    id 229
    label "czysty"
  ]
  node [
    id 230
    label "idealistyczny"
  ]
  node [
    id 231
    label "wspania&#322;y"
  ]
  node [
    id 232
    label "pe&#322;ny"
  ]
  node [
    id 233
    label "abstrakcyjny"
  ]
  node [
    id 234
    label "doskonale"
  ]
  node [
    id 235
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 236
    label "skuteczny"
  ]
  node [
    id 237
    label "idealnie"
  ]
  node [
    id 238
    label "model"
  ]
  node [
    id 239
    label "sk&#322;ad"
  ]
  node [
    id 240
    label "zachowanie"
  ]
  node [
    id 241
    label "porz&#261;dek"
  ]
  node [
    id 242
    label "Android"
  ]
  node [
    id 243
    label "przyn&#281;ta"
  ]
  node [
    id 244
    label "jednostka_geologiczna"
  ]
  node [
    id 245
    label "metoda"
  ]
  node [
    id 246
    label "podsystem"
  ]
  node [
    id 247
    label "p&#322;&#243;d"
  ]
  node [
    id 248
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 249
    label "s&#261;d"
  ]
  node [
    id 250
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 251
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 252
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 253
    label "j&#261;dro"
  ]
  node [
    id 254
    label "eratem"
  ]
  node [
    id 255
    label "ryba"
  ]
  node [
    id 256
    label "pulpit"
  ]
  node [
    id 257
    label "struktura"
  ]
  node [
    id 258
    label "oddzia&#322;"
  ]
  node [
    id 259
    label "usenet"
  ]
  node [
    id 260
    label "o&#347;"
  ]
  node [
    id 261
    label "oprogramowanie"
  ]
  node [
    id 262
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 263
    label "poj&#281;cie"
  ]
  node [
    id 264
    label "w&#281;dkarstwo"
  ]
  node [
    id 265
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 266
    label "Leopard"
  ]
  node [
    id 267
    label "systemik"
  ]
  node [
    id 268
    label "rozprz&#261;c"
  ]
  node [
    id 269
    label "cybernetyk"
  ]
  node [
    id 270
    label "konstelacja"
  ]
  node [
    id 271
    label "doktryna"
  ]
  node [
    id 272
    label "net"
  ]
  node [
    id 273
    label "zbi&#243;r"
  ]
  node [
    id 274
    label "method"
  ]
  node [
    id 275
    label "systemat"
  ]
  node [
    id 276
    label "gospodarka"
  ]
  node [
    id 277
    label "program_nauczania"
  ]
  node [
    id 278
    label "Karta_Nauczyciela"
  ]
  node [
    id 279
    label "czyj&#347;"
  ]
  node [
    id 280
    label "m&#261;&#380;"
  ]
  node [
    id 281
    label "contest"
  ]
  node [
    id 282
    label "wydarzenie"
  ]
  node [
    id 283
    label "profesor"
  ]
  node [
    id 284
    label "kszta&#322;ciciel"
  ]
  node [
    id 285
    label "szkolnik"
  ]
  node [
    id 286
    label "preceptor"
  ]
  node [
    id 287
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 288
    label "pedagog"
  ]
  node [
    id 289
    label "popularyzator"
  ]
  node [
    id 290
    label "belfer"
  ]
  node [
    id 291
    label "zapoznawa&#263;"
  ]
  node [
    id 292
    label "teach"
  ]
  node [
    id 293
    label "train"
  ]
  node [
    id 294
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 295
    label "pracowa&#263;"
  ]
  node [
    id 296
    label "szkoli&#263;"
  ]
  node [
    id 297
    label "tendency"
  ]
  node [
    id 298
    label "nabo&#380;e&#324;stwo"
  ]
  node [
    id 299
    label "nami&#281;tny"
  ]
  node [
    id 300
    label "Jezus_Chrystus"
  ]
  node [
    id 301
    label "z&#322;o&#347;&#263;"
  ]
  node [
    id 302
    label "feblik"
  ]
  node [
    id 303
    label "lunacy"
  ]
  node [
    id 304
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 305
    label "dedication"
  ]
  node [
    id 306
    label "fondness"
  ]
  node [
    id 307
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 308
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 309
    label "utw&#243;r"
  ]
  node [
    id 310
    label "pie&#347;&#324;_pasyjna"
  ]
  node [
    id 311
    label "kurwica"
  ]
  node [
    id 312
    label "zaj&#281;cie"
  ]
  node [
    id 313
    label "zajawka"
  ]
  node [
    id 314
    label "zwolennik"
  ]
  node [
    id 315
    label "tarcza"
  ]
  node [
    id 316
    label "czeladnik"
  ]
  node [
    id 317
    label "elew"
  ]
  node [
    id 318
    label "rzemie&#347;lnik"
  ]
  node [
    id 319
    label "kontynuator"
  ]
  node [
    id 320
    label "klasa"
  ]
  node [
    id 321
    label "wyprawka"
  ]
  node [
    id 322
    label "mundurek"
  ]
  node [
    id 323
    label "absolwent"
  ]
  node [
    id 324
    label "szko&#322;a"
  ]
  node [
    id 325
    label "praktykant"
  ]
  node [
    id 326
    label "rozstawia&#263;"
  ]
  node [
    id 327
    label "puszcza&#263;"
  ]
  node [
    id 328
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 329
    label "dopowiada&#263;"
  ]
  node [
    id 330
    label "rozpakowywa&#263;"
  ]
  node [
    id 331
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 332
    label "inflate"
  ]
  node [
    id 333
    label "stawia&#263;"
  ]
  node [
    id 334
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 335
    label "pole"
  ]
  node [
    id 336
    label "kastowo&#347;&#263;"
  ]
  node [
    id 337
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 338
    label "ludzie_pracy"
  ]
  node [
    id 339
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 340
    label "community"
  ]
  node [
    id 341
    label "Fremeni"
  ]
  node [
    id 342
    label "status"
  ]
  node [
    id 343
    label "pozaklasowy"
  ]
  node [
    id 344
    label "aspo&#322;eczny"
  ]
  node [
    id 345
    label "ilo&#347;&#263;"
  ]
  node [
    id 346
    label "uwarstwienie"
  ]
  node [
    id 347
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 348
    label "zlewanie_si&#281;"
  ]
  node [
    id 349
    label "elita"
  ]
  node [
    id 350
    label "cywilizacja"
  ]
  node [
    id 351
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 352
    label "wiersz"
  ]
  node [
    id 353
    label "ber&#380;eretka"
  ]
  node [
    id 354
    label "szcz&#281;&#347;cie"
  ]
  node [
    id 355
    label "liryka"
  ]
  node [
    id 356
    label "porozumie&#263;_si&#281;"
  ]
  node [
    id 357
    label "appoint"
  ]
  node [
    id 358
    label "stage"
  ]
  node [
    id 359
    label "skontaktowa&#263;"
  ]
  node [
    id 360
    label "jaworzyna"
  ]
  node [
    id 361
    label "maple"
  ]
  node [
    id 362
    label "klon"
  ]
  node [
    id 363
    label "czeczotka"
  ]
  node [
    id 364
    label "za&#322;atwi&#263;"
  ]
  node [
    id 365
    label "usun&#261;&#263;"
  ]
  node [
    id 366
    label "magiel"
  ]
  node [
    id 367
    label "faza"
  ]
  node [
    id 368
    label "arkusz"
  ]
  node [
    id 369
    label "sprawdzian"
  ]
  node [
    id 370
    label "praca_pisemna"
  ]
  node [
    id 371
    label "oblewanie"
  ]
  node [
    id 372
    label "examination"
  ]
  node [
    id 373
    label "pr&#243;ba"
  ]
  node [
    id 374
    label "oblewa&#263;"
  ]
  node [
    id 375
    label "sesja_egzaminacyjna"
  ]
  node [
    id 376
    label "og&#243;lnopolsko"
  ]
  node [
    id 377
    label "og&#243;lnokrajowy"
  ]
  node [
    id 378
    label "okre&#347;lony"
  ]
  node [
    id 379
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 380
    label "spis"
  ]
  node [
    id 381
    label "uk&#322;ad"
  ]
  node [
    id 382
    label "kosz"
  ]
  node [
    id 383
    label "konstrukcja"
  ]
  node [
    id 384
    label "przedmiot"
  ]
  node [
    id 385
    label "plate"
  ]
  node [
    id 386
    label "transparent"
  ]
  node [
    id 387
    label "kontener"
  ]
  node [
    id 388
    label "chart"
  ]
  node [
    id 389
    label "rubryka"
  ]
  node [
    id 390
    label "szachownica_Punnetta"
  ]
  node [
    id 391
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 392
    label "rozmiar&#243;wka"
  ]
  node [
    id 393
    label "p&#322;aszczyzna"
  ]
  node [
    id 394
    label "gentelman"
  ]
  node [
    id 395
    label "elegant"
  ]
  node [
    id 396
    label "wiedzie&#263;"
  ]
  node [
    id 397
    label "cognizance"
  ]
  node [
    id 398
    label "biblioteka"
  ]
  node [
    id 399
    label "odbiorca"
  ]
  node [
    id 400
    label "klient"
  ]
  node [
    id 401
    label "komcio"
  ]
  node [
    id 402
    label "strona"
  ]
  node [
    id 403
    label "blogosfera"
  ]
  node [
    id 404
    label "pami&#281;tnik"
  ]
  node [
    id 405
    label "impart"
  ]
  node [
    id 406
    label "repeat"
  ]
  node [
    id 407
    label "robi&#263;"
  ]
  node [
    id 408
    label "podszkala&#263;_si&#281;"
  ]
  node [
    id 409
    label "podawa&#263;"
  ]
  node [
    id 410
    label "prate"
  ]
  node [
    id 411
    label "oskar&#380;y&#263;"
  ]
  node [
    id 412
    label "impute"
  ]
  node [
    id 413
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 414
    label "infimum"
  ]
  node [
    id 415
    label "matematyka_czysta"
  ]
  node [
    id 416
    label "fizyka_matematyczna"
  ]
  node [
    id 417
    label "matma"
  ]
  node [
    id 418
    label "rachunki"
  ]
  node [
    id 419
    label "kryptologia"
  ]
  node [
    id 420
    label "supremum"
  ]
  node [
    id 421
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 422
    label "rachunek_operatorowy"
  ]
  node [
    id 423
    label "funkcja"
  ]
  node [
    id 424
    label "rzut"
  ]
  node [
    id 425
    label "forsing"
  ]
  node [
    id 426
    label "teoria_graf&#243;w"
  ]
  node [
    id 427
    label "logicyzm"
  ]
  node [
    id 428
    label "logika"
  ]
  node [
    id 429
    label "topologia_algebraiczna"
  ]
  node [
    id 430
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 431
    label "matematyka_stosowana"
  ]
  node [
    id 432
    label "kierunek"
  ]
  node [
    id 433
    label "modelowanie_matematyczne"
  ]
  node [
    id 434
    label "teoria_katastrof"
  ]
  node [
    id 435
    label "jednostka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 58
  ]
  edge [
    source 19
    target 59
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 23
    target 211
  ]
  edge [
    source 23
    target 212
  ]
  edge [
    source 23
    target 213
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 23
    target 218
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 227
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 228
  ]
  edge [
    source 24
    target 229
  ]
  edge [
    source 24
    target 230
  ]
  edge [
    source 24
    target 231
  ]
  edge [
    source 24
    target 232
  ]
  edge [
    source 24
    target 233
  ]
  edge [
    source 24
    target 234
  ]
  edge [
    source 24
    target 235
  ]
  edge [
    source 24
    target 236
  ]
  edge [
    source 24
    target 237
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 238
  ]
  edge [
    source 25
    target 239
  ]
  edge [
    source 25
    target 240
  ]
  edge [
    source 25
    target 109
  ]
  edge [
    source 25
    target 241
  ]
  edge [
    source 25
    target 242
  ]
  edge [
    source 25
    target 243
  ]
  edge [
    source 25
    target 244
  ]
  edge [
    source 25
    target 245
  ]
  edge [
    source 25
    target 246
  ]
  edge [
    source 25
    target 247
  ]
  edge [
    source 25
    target 248
  ]
  edge [
    source 25
    target 249
  ]
  edge [
    source 25
    target 250
  ]
  edge [
    source 25
    target 251
  ]
  edge [
    source 25
    target 252
  ]
  edge [
    source 25
    target 253
  ]
  edge [
    source 25
    target 254
  ]
  edge [
    source 25
    target 255
  ]
  edge [
    source 25
    target 256
  ]
  edge [
    source 25
    target 257
  ]
  edge [
    source 25
    target 123
  ]
  edge [
    source 25
    target 258
  ]
  edge [
    source 25
    target 259
  ]
  edge [
    source 25
    target 260
  ]
  edge [
    source 25
    target 261
  ]
  edge [
    source 25
    target 262
  ]
  edge [
    source 25
    target 263
  ]
  edge [
    source 25
    target 264
  ]
  edge [
    source 25
    target 265
  ]
  edge [
    source 25
    target 266
  ]
  edge [
    source 25
    target 129
  ]
  edge [
    source 25
    target 267
  ]
  edge [
    source 25
    target 268
  ]
  edge [
    source 25
    target 269
  ]
  edge [
    source 25
    target 270
  ]
  edge [
    source 25
    target 271
  ]
  edge [
    source 25
    target 272
  ]
  edge [
    source 25
    target 273
  ]
  edge [
    source 25
    target 274
  ]
  edge [
    source 25
    target 275
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 276
  ]
  edge [
    source 26
    target 277
  ]
  edge [
    source 26
    target 278
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 281
  ]
  edge [
    source 29
    target 282
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 283
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 30
    target 288
  ]
  edge [
    source 30
    target 289
  ]
  edge [
    source 30
    target 290
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 291
  ]
  edge [
    source 31
    target 292
  ]
  edge [
    source 31
    target 293
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 294
  ]
  edge [
    source 31
    target 295
  ]
  edge [
    source 31
    target 296
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 297
  ]
  edge [
    source 32
    target 298
  ]
  edge [
    source 32
    target 299
  ]
  edge [
    source 32
    target 300
  ]
  edge [
    source 32
    target 301
  ]
  edge [
    source 32
    target 302
  ]
  edge [
    source 32
    target 303
  ]
  edge [
    source 32
    target 98
  ]
  edge [
    source 32
    target 304
  ]
  edge [
    source 32
    target 305
  ]
  edge [
    source 32
    target 306
  ]
  edge [
    source 32
    target 307
  ]
  edge [
    source 32
    target 308
  ]
  edge [
    source 32
    target 309
  ]
  edge [
    source 32
    target 310
  ]
  edge [
    source 32
    target 311
  ]
  edge [
    source 32
    target 312
  ]
  edge [
    source 32
    target 313
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 197
  ]
  edge [
    source 33
    target 314
  ]
  edge [
    source 33
    target 315
  ]
  edge [
    source 33
    target 316
  ]
  edge [
    source 33
    target 317
  ]
  edge [
    source 33
    target 318
  ]
  edge [
    source 33
    target 319
  ]
  edge [
    source 33
    target 320
  ]
  edge [
    source 33
    target 321
  ]
  edge [
    source 33
    target 322
  ]
  edge [
    source 33
    target 323
  ]
  edge [
    source 33
    target 324
  ]
  edge [
    source 33
    target 325
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 326
  ]
  edge [
    source 34
    target 327
  ]
  edge [
    source 34
    target 293
  ]
  edge [
    source 34
    target 328
  ]
  edge [
    source 34
    target 329
  ]
  edge [
    source 34
    target 294
  ]
  edge [
    source 34
    target 211
  ]
  edge [
    source 34
    target 330
  ]
  edge [
    source 34
    target 331
  ]
  edge [
    source 34
    target 332
  ]
  edge [
    source 34
    target 333
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 297
  ]
  edge [
    source 36
    target 302
  ]
  edge [
    source 36
    target 304
  ]
  edge [
    source 36
    target 307
  ]
  edge [
    source 36
    target 313
  ]
  edge [
    source 36
    target 41
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 334
  ]
  edge [
    source 37
    target 335
  ]
  edge [
    source 37
    target 336
  ]
  edge [
    source 37
    target 337
  ]
  edge [
    source 37
    target 338
  ]
  edge [
    source 37
    target 339
  ]
  edge [
    source 37
    target 340
  ]
  edge [
    source 37
    target 341
  ]
  edge [
    source 37
    target 342
  ]
  edge [
    source 37
    target 343
  ]
  edge [
    source 37
    target 344
  ]
  edge [
    source 37
    target 232
  ]
  edge [
    source 37
    target 345
  ]
  edge [
    source 37
    target 346
  ]
  edge [
    source 37
    target 347
  ]
  edge [
    source 37
    target 348
  ]
  edge [
    source 37
    target 349
  ]
  edge [
    source 37
    target 350
  ]
  edge [
    source 37
    target 351
  ]
  edge [
    source 37
    target 320
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 352
  ]
  edge [
    source 38
    target 353
  ]
  edge [
    source 38
    target 354
  ]
  edge [
    source 38
    target 355
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 356
  ]
  edge [
    source 41
    target 357
  ]
  edge [
    source 41
    target 358
  ]
  edge [
    source 41
    target 359
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 360
  ]
  edge [
    source 42
    target 361
  ]
  edge [
    source 42
    target 70
  ]
  edge [
    source 42
    target 362
  ]
  edge [
    source 42
    target 363
  ]
  edge [
    source 42
    target 75
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 364
  ]
  edge [
    source 44
    target 365
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 366
  ]
  edge [
    source 45
    target 367
  ]
  edge [
    source 45
    target 368
  ]
  edge [
    source 45
    target 369
  ]
  edge [
    source 45
    target 370
  ]
  edge [
    source 45
    target 371
  ]
  edge [
    source 45
    target 372
  ]
  edge [
    source 45
    target 373
  ]
  edge [
    source 45
    target 374
  ]
  edge [
    source 45
    target 375
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 376
  ]
  edge [
    source 46
    target 377
  ]
  edge [
    source 47
    target 378
  ]
  edge [
    source 47
    target 379
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 380
  ]
  edge [
    source 50
    target 381
  ]
  edge [
    source 50
    target 382
  ]
  edge [
    source 50
    target 315
  ]
  edge [
    source 50
    target 383
  ]
  edge [
    source 50
    target 384
  ]
  edge [
    source 50
    target 385
  ]
  edge [
    source 50
    target 386
  ]
  edge [
    source 50
    target 387
  ]
  edge [
    source 50
    target 388
  ]
  edge [
    source 50
    target 389
  ]
  edge [
    source 50
    target 390
  ]
  edge [
    source 50
    target 391
  ]
  edge [
    source 50
    target 320
  ]
  edge [
    source 50
    target 392
  ]
  edge [
    source 50
    target 324
  ]
  edge [
    source 50
    target 393
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 197
  ]
  edge [
    source 52
    target 394
  ]
  edge [
    source 52
    target 395
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 396
  ]
  edge [
    source 53
    target 397
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 398
  ]
  edge [
    source 56
    target 399
  ]
  edge [
    source 56
    target 400
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 58
    target 401
  ]
  edge [
    source 58
    target 402
  ]
  edge [
    source 58
    target 403
  ]
  edge [
    source 58
    target 404
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 405
  ]
  edge [
    source 59
    target 406
  ]
  edge [
    source 59
    target 407
  ]
  edge [
    source 59
    target 408
  ]
  edge [
    source 59
    target 409
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 410
  ]
  edge [
    source 60
    target 411
  ]
  edge [
    source 60
    target 412
  ]
  edge [
    source 60
    target 413
  ]
  edge [
    source 61
    target 414
  ]
  edge [
    source 61
    target 415
  ]
  edge [
    source 61
    target 384
  ]
  edge [
    source 61
    target 416
  ]
  edge [
    source 61
    target 417
  ]
  edge [
    source 61
    target 418
  ]
  edge [
    source 61
    target 419
  ]
  edge [
    source 61
    target 420
  ]
  edge [
    source 61
    target 421
  ]
  edge [
    source 61
    target 422
  ]
  edge [
    source 61
    target 423
  ]
  edge [
    source 61
    target 424
  ]
  edge [
    source 61
    target 425
  ]
  edge [
    source 61
    target 426
  ]
  edge [
    source 61
    target 427
  ]
  edge [
    source 61
    target 428
  ]
  edge [
    source 61
    target 429
  ]
  edge [
    source 61
    target 430
  ]
  edge [
    source 61
    target 431
  ]
  edge [
    source 61
    target 432
  ]
  edge [
    source 61
    target 433
  ]
  edge [
    source 61
    target 434
  ]
  edge [
    source 61
    target 435
  ]
]
