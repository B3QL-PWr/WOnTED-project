graph [
  maxDegree 13
  minDegree 1
  meanDegree 2.0285714285714285
  density 0.029399585921325053
  graphCliqueNumber 2
  node [
    id 0
    label "prawilno"
    origin "text"
  ]
  node [
    id 1
    label "przypomina&#263;"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "jeden"
    origin "text"
  ]
  node [
    id 4
    label "argument"
    origin "text"
  ]
  node [
    id 5
    label "zakaz"
    origin "text"
  ]
  node [
    id 6
    label "handel"
    origin "text"
  ]
  node [
    id 7
    label "niedziela"
    origin "text"
  ]
  node [
    id 8
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 9
    label "kto"
    origin "text"
  ]
  node [
    id 10
    label "popiera&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "sprzeciwia&#263;"
    origin "text"
  ]
  node [
    id 13
    label "ryj"
    origin "text"
  ]
  node [
    id 14
    label "za&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 15
    label "obrazek"
    origin "text"
  ]
  node [
    id 16
    label "recall"
  ]
  node [
    id 17
    label "u&#347;wiadamia&#263;"
  ]
  node [
    id 18
    label "informowa&#263;"
  ]
  node [
    id 19
    label "prompt"
  ]
  node [
    id 20
    label "czyj&#347;"
  ]
  node [
    id 21
    label "m&#261;&#380;"
  ]
  node [
    id 22
    label "kieliszek"
  ]
  node [
    id 23
    label "shot"
  ]
  node [
    id 24
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 25
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 26
    label "jaki&#347;"
  ]
  node [
    id 27
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 28
    label "jednolicie"
  ]
  node [
    id 29
    label "w&#243;dka"
  ]
  node [
    id 30
    label "ten"
  ]
  node [
    id 31
    label "ujednolicenie"
  ]
  node [
    id 32
    label "jednakowy"
  ]
  node [
    id 33
    label "s&#261;d"
  ]
  node [
    id 34
    label "argumentacja"
  ]
  node [
    id 35
    label "parametr"
  ]
  node [
    id 36
    label "rzecz"
  ]
  node [
    id 37
    label "dow&#243;d"
  ]
  node [
    id 38
    label "operand"
  ]
  node [
    id 39
    label "zmienna"
  ]
  node [
    id 40
    label "rozporz&#261;dzenie"
  ]
  node [
    id 41
    label "polecenie"
  ]
  node [
    id 42
    label "business"
  ]
  node [
    id 43
    label "popyt"
  ]
  node [
    id 44
    label "komercja"
  ]
  node [
    id 45
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 46
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 47
    label "Wielkanoc"
  ]
  node [
    id 48
    label "dzie&#324;_&#347;wi&#281;ty"
  ]
  node [
    id 49
    label "weekend"
  ]
  node [
    id 50
    label "Niedziela_Palmowa"
  ]
  node [
    id 51
    label "Zes&#322;anie_Ducha_&#346;wi&#281;tego"
  ]
  node [
    id 52
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 53
    label "bia&#322;a_niedziela"
  ]
  node [
    id 54
    label "niedziela_przewodnia"
  ]
  node [
    id 55
    label "tydzie&#324;"
  ]
  node [
    id 56
    label "uzasadnia&#263;"
  ]
  node [
    id 57
    label "pomaga&#263;"
  ]
  node [
    id 58
    label "unbosom"
  ]
  node [
    id 59
    label "twarz"
  ]
  node [
    id 60
    label "dzi&#243;b"
  ]
  node [
    id 61
    label "morda"
  ]
  node [
    id 62
    label "&#347;winia"
  ]
  node [
    id 63
    label "usta"
  ]
  node [
    id 64
    label "annex"
  ]
  node [
    id 65
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 66
    label "druk_ulotny"
  ]
  node [
    id 67
    label "opowiadanie"
  ]
  node [
    id 68
    label "rysunek"
  ]
  node [
    id 69
    label "picture"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 59
  ]
  edge [
    source 13
    target 60
  ]
  edge [
    source 13
    target 61
  ]
  edge [
    source 13
    target 62
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 65
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 67
  ]
  edge [
    source 15
    target 68
  ]
  edge [
    source 15
    target 69
  ]
]
