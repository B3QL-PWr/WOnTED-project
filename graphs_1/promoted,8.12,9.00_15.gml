graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9682539682539681
  density 0.031746031746031744
  graphCliqueNumber 2
  node [
    id 0
    label "skandaliczny"
    origin "text"
  ]
  node [
    id 1
    label "zachowanie"
    origin "text"
  ]
  node [
    id 2
    label "jeden"
    origin "text"
  ]
  node [
    id 3
    label "policjant"
    origin "text"
  ]
  node [
    id 4
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 5
    label "wielkopolski"
    origin "text"
  ]
  node [
    id 6
    label "skandalicznie"
  ]
  node [
    id 7
    label "straszny"
  ]
  node [
    id 8
    label "gorsz&#261;cy"
  ]
  node [
    id 9
    label "sensacyjny"
  ]
  node [
    id 10
    label "zwierz&#281;"
  ]
  node [
    id 11
    label "zrobienie"
  ]
  node [
    id 12
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 13
    label "podtrzymanie"
  ]
  node [
    id 14
    label "reakcja"
  ]
  node [
    id 15
    label "tajemnica"
  ]
  node [
    id 16
    label "zdyscyplinowanie"
  ]
  node [
    id 17
    label "observation"
  ]
  node [
    id 18
    label "behawior"
  ]
  node [
    id 19
    label "dieta"
  ]
  node [
    id 20
    label "bearing"
  ]
  node [
    id 21
    label "pochowanie"
  ]
  node [
    id 22
    label "wydarzenie"
  ]
  node [
    id 23
    label "przechowanie"
  ]
  node [
    id 24
    label "post&#261;pienie"
  ]
  node [
    id 25
    label "post"
  ]
  node [
    id 26
    label "struktura"
  ]
  node [
    id 27
    label "spos&#243;b"
  ]
  node [
    id 28
    label "etolog"
  ]
  node [
    id 29
    label "kieliszek"
  ]
  node [
    id 30
    label "shot"
  ]
  node [
    id 31
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 32
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 33
    label "jaki&#347;"
  ]
  node [
    id 34
    label "jednolicie"
  ]
  node [
    id 35
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 36
    label "w&#243;dka"
  ]
  node [
    id 37
    label "ten"
  ]
  node [
    id 38
    label "ujednolicenie"
  ]
  node [
    id 39
    label "jednakowy"
  ]
  node [
    id 40
    label "policja"
  ]
  node [
    id 41
    label "blacharz"
  ]
  node [
    id 42
    label "pa&#322;a"
  ]
  node [
    id 43
    label "mundurowy"
  ]
  node [
    id 44
    label "str&#243;&#380;"
  ]
  node [
    id 45
    label "glina"
  ]
  node [
    id 46
    label "jednostka_administracyjna"
  ]
  node [
    id 47
    label "makroregion"
  ]
  node [
    id 48
    label "powiat"
  ]
  node [
    id 49
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 50
    label "mikroregion"
  ]
  node [
    id 51
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 52
    label "pa&#324;stwo"
  ]
  node [
    id 53
    label "po_wielkopolsku"
  ]
  node [
    id 54
    label "knypek"
  ]
  node [
    id 55
    label "kwyrla"
  ]
  node [
    id 56
    label "bimba"
  ]
  node [
    id 57
    label "hy&#263;ka"
  ]
  node [
    id 58
    label "plyndz"
  ]
  node [
    id 59
    label "regionalny"
  ]
  node [
    id 60
    label "gzik"
  ]
  node [
    id 61
    label "polski"
  ]
  node [
    id 62
    label "myrdyrda"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
]
