graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.1043956043956045
  density 0.005797233069960343
  graphCliqueNumber 3
  node [
    id 0
    label "umie&#347;ci&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wika"
    origin "text"
  ]
  node [
    id 2
    label "plac"
    origin "text"
  ]
  node [
    id 3
    label "surowy"
    origin "text"
  ]
  node [
    id 4
    label "t&#322;umaczenie"
    origin "text"
  ]
  node [
    id 5
    label "film"
    origin "text"
  ]
  node [
    id 6
    label "good"
    origin "text"
  ]
  node [
    id 7
    label "bad"
    origin "text"
  ]
  node [
    id 8
    label "copy"
    origin "text"
  ]
  node [
    id 9
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 10
    label "wtorkowy"
    origin "text"
  ]
  node [
    id 11
    label "spotkanie"
    origin "text"
  ]
  node [
    id 12
    label "cykl"
    origin "text"
  ]
  node [
    id 13
    label "kultura"
    origin "text"
  ]
  node [
    id 14
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 15
    label "wszystek"
    origin "text"
  ]
  node [
    id 16
    label "osoba"
    origin "text"
  ]
  node [
    id 17
    label "by&#263;"
    origin "text"
  ]
  node [
    id 18
    label "zainteresowana"
    origin "text"
  ]
  node [
    id 19
    label "wykorzystanie"
    origin "text"
  ]
  node [
    id 20
    label "pomoc"
    origin "text"
  ]
  node [
    id 21
    label "listopad"
    origin "text"
  ]
  node [
    id 22
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 23
    label "oczy&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 24
    label "b&#322;&#261;d"
    origin "text"
  ]
  node [
    id 25
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 26
    label "serdecznie"
    origin "text"
  ]
  node [
    id 27
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 28
    label "korekta"
    origin "text"
  ]
  node [
    id 29
    label "jednocze&#347;nie"
    origin "text"
  ]
  node [
    id 30
    label "kopiowa&#263;"
    origin "text"
  ]
  node [
    id 31
    label "tekst"
    origin "text"
  ]
  node [
    id 32
    label "inny"
    origin "text"
  ]
  node [
    id 33
    label "miejsce"
    origin "text"
  ]
  node [
    id 34
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 35
    label "gdy&#380;"
    origin "text"
  ]
  node [
    id 36
    label "wersja"
    origin "text"
  ]
  node [
    id 37
    label "dopracowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "stoisko"
  ]
  node [
    id 39
    label "Majdan"
  ]
  node [
    id 40
    label "obszar"
  ]
  node [
    id 41
    label "kram"
  ]
  node [
    id 42
    label "pierzeja"
  ]
  node [
    id 43
    label "przestrze&#324;"
  ]
  node [
    id 44
    label "obiekt_handlowy"
  ]
  node [
    id 45
    label "targowica"
  ]
  node [
    id 46
    label "zgromadzenie"
  ]
  node [
    id 47
    label "miasto"
  ]
  node [
    id 48
    label "pole_bitwy"
  ]
  node [
    id 49
    label "&#321;ubianka"
  ]
  node [
    id 50
    label "area"
  ]
  node [
    id 51
    label "trudny"
  ]
  node [
    id 52
    label "srogi"
  ]
  node [
    id 53
    label "dokuczliwy"
  ]
  node [
    id 54
    label "powa&#380;ny"
  ]
  node [
    id 55
    label "twardy"
  ]
  node [
    id 56
    label "oszcz&#281;dny"
  ]
  node [
    id 57
    label "surowo"
  ]
  node [
    id 58
    label "gro&#378;nie"
  ]
  node [
    id 59
    label "&#347;wie&#380;y"
  ]
  node [
    id 60
    label "remark"
  ]
  node [
    id 61
    label "robienie"
  ]
  node [
    id 62
    label "kr&#281;ty"
  ]
  node [
    id 63
    label "rozwianie"
  ]
  node [
    id 64
    label "j&#281;zyk"
  ]
  node [
    id 65
    label "przekonywanie"
  ]
  node [
    id 66
    label "uzasadnianie"
  ]
  node [
    id 67
    label "przedstawianie"
  ]
  node [
    id 68
    label "przek&#322;adanie"
  ]
  node [
    id 69
    label "zrozumia&#322;y"
  ]
  node [
    id 70
    label "explanation"
  ]
  node [
    id 71
    label "rozwiewanie"
  ]
  node [
    id 72
    label "gossip"
  ]
  node [
    id 73
    label "rendition"
  ]
  node [
    id 74
    label "bronienie"
  ]
  node [
    id 75
    label "rozbieg&#243;wka"
  ]
  node [
    id 76
    label "block"
  ]
  node [
    id 77
    label "odczula&#263;"
  ]
  node [
    id 78
    label "blik"
  ]
  node [
    id 79
    label "rola"
  ]
  node [
    id 80
    label "trawiarnia"
  ]
  node [
    id 81
    label "b&#322;ona"
  ]
  node [
    id 82
    label "filmoteka"
  ]
  node [
    id 83
    label "sztuka"
  ]
  node [
    id 84
    label "muza"
  ]
  node [
    id 85
    label "odczuli&#263;"
  ]
  node [
    id 86
    label "klatka"
  ]
  node [
    id 87
    label "odczulenie"
  ]
  node [
    id 88
    label "emulsja_fotograficzna"
  ]
  node [
    id 89
    label "animatronika"
  ]
  node [
    id 90
    label "dorobek"
  ]
  node [
    id 91
    label "odczulanie"
  ]
  node [
    id 92
    label "scena"
  ]
  node [
    id 93
    label "czo&#322;&#243;wka"
  ]
  node [
    id 94
    label "ty&#322;&#243;wka"
  ]
  node [
    id 95
    label "napisy"
  ]
  node [
    id 96
    label "photograph"
  ]
  node [
    id 97
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 98
    label "postprodukcja"
  ]
  node [
    id 99
    label "sklejarka"
  ]
  node [
    id 100
    label "anamorfoza"
  ]
  node [
    id 101
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 102
    label "ta&#347;ma"
  ]
  node [
    id 103
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 104
    label "uj&#281;cie"
  ]
  node [
    id 105
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 106
    label "po&#380;egnanie"
  ]
  node [
    id 107
    label "spowodowanie"
  ]
  node [
    id 108
    label "znalezienie"
  ]
  node [
    id 109
    label "znajomy"
  ]
  node [
    id 110
    label "doznanie"
  ]
  node [
    id 111
    label "employment"
  ]
  node [
    id 112
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 113
    label "gather"
  ]
  node [
    id 114
    label "powitanie"
  ]
  node [
    id 115
    label "spotykanie"
  ]
  node [
    id 116
    label "wydarzenie"
  ]
  node [
    id 117
    label "gathering"
  ]
  node [
    id 118
    label "spotkanie_si&#281;"
  ]
  node [
    id 119
    label "zdarzenie_si&#281;"
  ]
  node [
    id 120
    label "match"
  ]
  node [
    id 121
    label "zawarcie"
  ]
  node [
    id 122
    label "sekwencja"
  ]
  node [
    id 123
    label "czas"
  ]
  node [
    id 124
    label "edycja"
  ]
  node [
    id 125
    label "przebieg"
  ]
  node [
    id 126
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 127
    label "okres"
  ]
  node [
    id 128
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 129
    label "cycle"
  ]
  node [
    id 130
    label "owulacja"
  ]
  node [
    id 131
    label "miesi&#261;czka"
  ]
  node [
    id 132
    label "set"
  ]
  node [
    id 133
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 134
    label "przedmiot"
  ]
  node [
    id 135
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 136
    label "Wsch&#243;d"
  ]
  node [
    id 137
    label "rzecz"
  ]
  node [
    id 138
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 139
    label "religia"
  ]
  node [
    id 140
    label "przejmowa&#263;"
  ]
  node [
    id 141
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 142
    label "makrokosmos"
  ]
  node [
    id 143
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 144
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 145
    label "zjawisko"
  ]
  node [
    id 146
    label "praca_rolnicza"
  ]
  node [
    id 147
    label "tradycja"
  ]
  node [
    id 148
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 149
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 150
    label "przejmowanie"
  ]
  node [
    id 151
    label "cecha"
  ]
  node [
    id 152
    label "asymilowanie_si&#281;"
  ]
  node [
    id 153
    label "przej&#261;&#263;"
  ]
  node [
    id 154
    label "hodowla"
  ]
  node [
    id 155
    label "brzoskwiniarnia"
  ]
  node [
    id 156
    label "populace"
  ]
  node [
    id 157
    label "konwencja"
  ]
  node [
    id 158
    label "propriety"
  ]
  node [
    id 159
    label "jako&#347;&#263;"
  ]
  node [
    id 160
    label "kuchnia"
  ]
  node [
    id 161
    label "zwyczaj"
  ]
  node [
    id 162
    label "przej&#281;cie"
  ]
  node [
    id 163
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 164
    label "trwa&#263;"
  ]
  node [
    id 165
    label "zezwala&#263;"
  ]
  node [
    id 166
    label "ask"
  ]
  node [
    id 167
    label "invite"
  ]
  node [
    id 168
    label "zach&#281;ca&#263;"
  ]
  node [
    id 169
    label "preach"
  ]
  node [
    id 170
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 171
    label "pies"
  ]
  node [
    id 172
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 173
    label "poleca&#263;"
  ]
  node [
    id 174
    label "suffice"
  ]
  node [
    id 175
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 176
    label "ca&#322;y"
  ]
  node [
    id 177
    label "Zgredek"
  ]
  node [
    id 178
    label "kategoria_gramatyczna"
  ]
  node [
    id 179
    label "Casanova"
  ]
  node [
    id 180
    label "Don_Juan"
  ]
  node [
    id 181
    label "Gargantua"
  ]
  node [
    id 182
    label "Faust"
  ]
  node [
    id 183
    label "profanum"
  ]
  node [
    id 184
    label "Chocho&#322;"
  ]
  node [
    id 185
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 186
    label "koniugacja"
  ]
  node [
    id 187
    label "Winnetou"
  ]
  node [
    id 188
    label "Dwukwiat"
  ]
  node [
    id 189
    label "homo_sapiens"
  ]
  node [
    id 190
    label "Edyp"
  ]
  node [
    id 191
    label "Herkules_Poirot"
  ]
  node [
    id 192
    label "ludzko&#347;&#263;"
  ]
  node [
    id 193
    label "mikrokosmos"
  ]
  node [
    id 194
    label "person"
  ]
  node [
    id 195
    label "Sherlock_Holmes"
  ]
  node [
    id 196
    label "portrecista"
  ]
  node [
    id 197
    label "Szwejk"
  ]
  node [
    id 198
    label "Hamlet"
  ]
  node [
    id 199
    label "duch"
  ]
  node [
    id 200
    label "g&#322;owa"
  ]
  node [
    id 201
    label "oddzia&#322;ywanie"
  ]
  node [
    id 202
    label "Quasimodo"
  ]
  node [
    id 203
    label "Dulcynea"
  ]
  node [
    id 204
    label "Don_Kiszot"
  ]
  node [
    id 205
    label "Wallenrod"
  ]
  node [
    id 206
    label "Plastu&#347;"
  ]
  node [
    id 207
    label "Harry_Potter"
  ]
  node [
    id 208
    label "figura"
  ]
  node [
    id 209
    label "parali&#380;owa&#263;"
  ]
  node [
    id 210
    label "istota"
  ]
  node [
    id 211
    label "Werter"
  ]
  node [
    id 212
    label "antropochoria"
  ]
  node [
    id 213
    label "posta&#263;"
  ]
  node [
    id 214
    label "si&#281;ga&#263;"
  ]
  node [
    id 215
    label "obecno&#347;&#263;"
  ]
  node [
    id 216
    label "stan"
  ]
  node [
    id 217
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 218
    label "stand"
  ]
  node [
    id 219
    label "mie&#263;_miejsce"
  ]
  node [
    id 220
    label "uczestniczy&#263;"
  ]
  node [
    id 221
    label "chodzi&#263;"
  ]
  node [
    id 222
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 223
    label "equal"
  ]
  node [
    id 224
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 225
    label "zrobienie"
  ]
  node [
    id 226
    label "use"
  ]
  node [
    id 227
    label "u&#380;ycie"
  ]
  node [
    id 228
    label "stosowanie"
  ]
  node [
    id 229
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 230
    label "u&#380;yteczny"
  ]
  node [
    id 231
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 232
    label "exploitation"
  ]
  node [
    id 233
    label "zgodzi&#263;"
  ]
  node [
    id 234
    label "pomocnik"
  ]
  node [
    id 235
    label "doch&#243;d"
  ]
  node [
    id 236
    label "property"
  ]
  node [
    id 237
    label "grupa"
  ]
  node [
    id 238
    label "telefon_zaufania"
  ]
  node [
    id 239
    label "darowizna"
  ]
  node [
    id 240
    label "&#347;rodek"
  ]
  node [
    id 241
    label "liga"
  ]
  node [
    id 242
    label "miesi&#261;c"
  ]
  node [
    id 243
    label "czu&#263;"
  ]
  node [
    id 244
    label "desire"
  ]
  node [
    id 245
    label "kcie&#263;"
  ]
  node [
    id 246
    label "uwolni&#263;"
  ]
  node [
    id 247
    label "pozbawi&#263;"
  ]
  node [
    id 248
    label "usun&#261;&#263;"
  ]
  node [
    id 249
    label "authorize"
  ]
  node [
    id 250
    label "baseball"
  ]
  node [
    id 251
    label "czyn"
  ]
  node [
    id 252
    label "error"
  ]
  node [
    id 253
    label "pomyli&#263;_si&#281;"
  ]
  node [
    id 254
    label "pomylenie_si&#281;"
  ]
  node [
    id 255
    label "mniemanie"
  ]
  node [
    id 256
    label "byk"
  ]
  node [
    id 257
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 258
    label "rezultat"
  ]
  node [
    id 259
    label "oferowa&#263;"
  ]
  node [
    id 260
    label "siarczy&#347;cie"
  ]
  node [
    id 261
    label "serdeczny"
  ]
  node [
    id 262
    label "szczerze"
  ]
  node [
    id 263
    label "mi&#322;o"
  ]
  node [
    id 264
    label "rynek"
  ]
  node [
    id 265
    label "issue"
  ]
  node [
    id 266
    label "evocation"
  ]
  node [
    id 267
    label "wst&#281;p"
  ]
  node [
    id 268
    label "nuklearyzacja"
  ]
  node [
    id 269
    label "umo&#380;liwienie"
  ]
  node [
    id 270
    label "zacz&#281;cie"
  ]
  node [
    id 271
    label "wpisanie"
  ]
  node [
    id 272
    label "zapoznanie"
  ]
  node [
    id 273
    label "czynno&#347;&#263;"
  ]
  node [
    id 274
    label "entrance"
  ]
  node [
    id 275
    label "wej&#347;cie"
  ]
  node [
    id 276
    label "podstawy"
  ]
  node [
    id 277
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 278
    label "w&#322;&#261;czenie"
  ]
  node [
    id 279
    label "doprowadzenie"
  ]
  node [
    id 280
    label "przewietrzenie"
  ]
  node [
    id 281
    label "deduction"
  ]
  node [
    id 282
    label "umieszczenie"
  ]
  node [
    id 283
    label "poprawa"
  ]
  node [
    id 284
    label "odbitka"
  ]
  node [
    id 285
    label "strojenie"
  ]
  node [
    id 286
    label "dzia&#322;"
  ]
  node [
    id 287
    label "opustka"
  ]
  node [
    id 288
    label "simultaneously"
  ]
  node [
    id 289
    label "coincidentally"
  ]
  node [
    id 290
    label "synchronously"
  ]
  node [
    id 291
    label "concurrently"
  ]
  node [
    id 292
    label "jednoczesny"
  ]
  node [
    id 293
    label "pirat"
  ]
  node [
    id 294
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 295
    label "mock"
  ]
  node [
    id 296
    label "robi&#263;"
  ]
  node [
    id 297
    label "transcribe"
  ]
  node [
    id 298
    label "wytwarza&#263;"
  ]
  node [
    id 299
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 300
    label "pisa&#263;"
  ]
  node [
    id 301
    label "odmianka"
  ]
  node [
    id 302
    label "opu&#347;ci&#263;"
  ]
  node [
    id 303
    label "wypowied&#378;"
  ]
  node [
    id 304
    label "wytw&#243;r"
  ]
  node [
    id 305
    label "koniektura"
  ]
  node [
    id 306
    label "preparacja"
  ]
  node [
    id 307
    label "ekscerpcja"
  ]
  node [
    id 308
    label "redakcja"
  ]
  node [
    id 309
    label "obelga"
  ]
  node [
    id 310
    label "dzie&#322;o"
  ]
  node [
    id 311
    label "j&#281;zykowo"
  ]
  node [
    id 312
    label "pomini&#281;cie"
  ]
  node [
    id 313
    label "kolejny"
  ]
  node [
    id 314
    label "inaczej"
  ]
  node [
    id 315
    label "r&#243;&#380;ny"
  ]
  node [
    id 316
    label "inszy"
  ]
  node [
    id 317
    label "osobno"
  ]
  node [
    id 318
    label "cia&#322;o"
  ]
  node [
    id 319
    label "uwaga"
  ]
  node [
    id 320
    label "status"
  ]
  node [
    id 321
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 322
    label "chwila"
  ]
  node [
    id 323
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 324
    label "rz&#261;d"
  ]
  node [
    id 325
    label "praca"
  ]
  node [
    id 326
    label "location"
  ]
  node [
    id 327
    label "warunek_lokalowy"
  ]
  node [
    id 328
    label "hipertekst"
  ]
  node [
    id 329
    label "gauze"
  ]
  node [
    id 330
    label "nitka"
  ]
  node [
    id 331
    label "mesh"
  ]
  node [
    id 332
    label "e-hazard"
  ]
  node [
    id 333
    label "netbook"
  ]
  node [
    id 334
    label "cyberprzestrze&#324;"
  ]
  node [
    id 335
    label "biznes_elektroniczny"
  ]
  node [
    id 336
    label "snu&#263;"
  ]
  node [
    id 337
    label "organization"
  ]
  node [
    id 338
    label "zasadzka"
  ]
  node [
    id 339
    label "web"
  ]
  node [
    id 340
    label "provider"
  ]
  node [
    id 341
    label "struktura"
  ]
  node [
    id 342
    label "us&#322;uga_internetowa"
  ]
  node [
    id 343
    label "punkt_dost&#281;pu"
  ]
  node [
    id 344
    label "organizacja"
  ]
  node [
    id 345
    label "mem"
  ]
  node [
    id 346
    label "vane"
  ]
  node [
    id 347
    label "podcast"
  ]
  node [
    id 348
    label "grooming"
  ]
  node [
    id 349
    label "kszta&#322;t"
  ]
  node [
    id 350
    label "strona"
  ]
  node [
    id 351
    label "obiekt"
  ]
  node [
    id 352
    label "wysnu&#263;"
  ]
  node [
    id 353
    label "gra_sieciowa"
  ]
  node [
    id 354
    label "instalacja"
  ]
  node [
    id 355
    label "sie&#263;_komputerowa"
  ]
  node [
    id 356
    label "net"
  ]
  node [
    id 357
    label "plecionka"
  ]
  node [
    id 358
    label "media"
  ]
  node [
    id 359
    label "rozmieszczenie"
  ]
  node [
    id 360
    label "typ"
  ]
  node [
    id 361
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 362
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 363
    label "dorobi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 29
  ]
  edge [
    source 14
    target 30
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 25
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 197
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 16
    target 203
  ]
  edge [
    source 16
    target 204
  ]
  edge [
    source 16
    target 205
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 207
  ]
  edge [
    source 16
    target 208
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 16
    target 213
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 35
  ]
  edge [
    source 17
    target 36
  ]
  edge [
    source 17
    target 214
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 215
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 217
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 219
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 232
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 233
  ]
  edge [
    source 20
    target 234
  ]
  edge [
    source 20
    target 235
  ]
  edge [
    source 20
    target 236
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 237
  ]
  edge [
    source 20
    target 238
  ]
  edge [
    source 20
    target 239
  ]
  edge [
    source 20
    target 240
  ]
  edge [
    source 20
    target 241
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 242
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 243
  ]
  edge [
    source 22
    target 244
  ]
  edge [
    source 22
    target 245
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 250
  ]
  edge [
    source 24
    target 251
  ]
  edge [
    source 24
    target 252
  ]
  edge [
    source 24
    target 253
  ]
  edge [
    source 24
    target 254
  ]
  edge [
    source 24
    target 255
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 24
    target 257
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 167
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 259
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 260
  ]
  edge [
    source 26
    target 261
  ]
  edge [
    source 26
    target 262
  ]
  edge [
    source 26
    target 263
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 264
  ]
  edge [
    source 27
    target 265
  ]
  edge [
    source 27
    target 266
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 27
    target 268
  ]
  edge [
    source 27
    target 269
  ]
  edge [
    source 27
    target 270
  ]
  edge [
    source 27
    target 271
  ]
  edge [
    source 27
    target 272
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 273
  ]
  edge [
    source 27
    target 274
  ]
  edge [
    source 27
    target 275
  ]
  edge [
    source 27
    target 276
  ]
  edge [
    source 27
    target 107
  ]
  edge [
    source 27
    target 277
  ]
  edge [
    source 27
    target 278
  ]
  edge [
    source 27
    target 279
  ]
  edge [
    source 27
    target 280
  ]
  edge [
    source 27
    target 281
  ]
  edge [
    source 27
    target 282
  ]
  edge [
    source 28
    target 283
  ]
  edge [
    source 28
    target 284
  ]
  edge [
    source 28
    target 285
  ]
  edge [
    source 28
    target 286
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 29
    target 288
  ]
  edge [
    source 29
    target 289
  ]
  edge [
    source 29
    target 290
  ]
  edge [
    source 29
    target 291
  ]
  edge [
    source 29
    target 292
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 293
  ]
  edge [
    source 30
    target 294
  ]
  edge [
    source 30
    target 295
  ]
  edge [
    source 30
    target 296
  ]
  edge [
    source 30
    target 297
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 300
  ]
  edge [
    source 31
    target 301
  ]
  edge [
    source 31
    target 302
  ]
  edge [
    source 31
    target 303
  ]
  edge [
    source 31
    target 304
  ]
  edge [
    source 31
    target 305
  ]
  edge [
    source 31
    target 306
  ]
  edge [
    source 31
    target 307
  ]
  edge [
    source 31
    target 308
  ]
  edge [
    source 31
    target 309
  ]
  edge [
    source 31
    target 310
  ]
  edge [
    source 31
    target 311
  ]
  edge [
    source 31
    target 312
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 313
  ]
  edge [
    source 32
    target 314
  ]
  edge [
    source 32
    target 315
  ]
  edge [
    source 32
    target 316
  ]
  edge [
    source 32
    target 317
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 318
  ]
  edge [
    source 33
    target 151
  ]
  edge [
    source 33
    target 319
  ]
  edge [
    source 33
    target 43
  ]
  edge [
    source 33
    target 320
  ]
  edge [
    source 33
    target 321
  ]
  edge [
    source 33
    target 322
  ]
  edge [
    source 33
    target 323
  ]
  edge [
    source 33
    target 324
  ]
  edge [
    source 33
    target 325
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 328
  ]
  edge [
    source 34
    target 329
  ]
  edge [
    source 34
    target 330
  ]
  edge [
    source 34
    target 331
  ]
  edge [
    source 34
    target 332
  ]
  edge [
    source 34
    target 333
  ]
  edge [
    source 34
    target 334
  ]
  edge [
    source 34
    target 335
  ]
  edge [
    source 34
    target 336
  ]
  edge [
    source 34
    target 337
  ]
  edge [
    source 34
    target 338
  ]
  edge [
    source 34
    target 126
  ]
  edge [
    source 34
    target 339
  ]
  edge [
    source 34
    target 340
  ]
  edge [
    source 34
    target 341
  ]
  edge [
    source 34
    target 342
  ]
  edge [
    source 34
    target 343
  ]
  edge [
    source 34
    target 344
  ]
  edge [
    source 34
    target 345
  ]
  edge [
    source 34
    target 346
  ]
  edge [
    source 34
    target 347
  ]
  edge [
    source 34
    target 348
  ]
  edge [
    source 34
    target 349
  ]
  edge [
    source 34
    target 350
  ]
  edge [
    source 34
    target 351
  ]
  edge [
    source 34
    target 352
  ]
  edge [
    source 34
    target 353
  ]
  edge [
    source 34
    target 354
  ]
  edge [
    source 34
    target 355
  ]
  edge [
    source 34
    target 356
  ]
  edge [
    source 34
    target 357
  ]
  edge [
    source 34
    target 358
  ]
  edge [
    source 34
    target 359
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 360
  ]
  edge [
    source 36
    target 213
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 362
  ]
  edge [
    source 37
    target 363
  ]
]
