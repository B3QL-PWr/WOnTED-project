graph [
  maxDegree 14
  minDegree 1
  meanDegree 2.176470588235294
  density 0.0324846356453029
  graphCliqueNumber 5
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "zatytu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "lekarz"
    origin "text"
  ]
  node [
    id 4
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "europa"
    origin "text"
  ]
  node [
    id 7
    label "ostatni"
    origin "text"
  ]
  node [
    id 8
    label "miejsce"
    origin "text"
  ]
  node [
    id 9
    label "cz&#322;owiek"
  ]
  node [
    id 10
    label "nowotny"
  ]
  node [
    id 11
    label "drugi"
  ]
  node [
    id 12
    label "kolejny"
  ]
  node [
    id 13
    label "bie&#380;&#261;cy"
  ]
  node [
    id 14
    label "nowo"
  ]
  node [
    id 15
    label "narybek"
  ]
  node [
    id 16
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 17
    label "obcy"
  ]
  node [
    id 18
    label "nazwa&#263;"
  ]
  node [
    id 19
    label "title"
  ]
  node [
    id 20
    label "zbada&#263;"
  ]
  node [
    id 21
    label "medyk"
  ]
  node [
    id 22
    label "lekarze"
  ]
  node [
    id 23
    label "pracownik"
  ]
  node [
    id 24
    label "eskulap"
  ]
  node [
    id 25
    label "Hipokrates"
  ]
  node [
    id 26
    label "Mesmer"
  ]
  node [
    id 27
    label "Galen"
  ]
  node [
    id 28
    label "dokt&#243;r"
  ]
  node [
    id 29
    label "odzyska&#263;"
  ]
  node [
    id 30
    label "devise"
  ]
  node [
    id 31
    label "oceni&#263;"
  ]
  node [
    id 32
    label "znaj&#347;&#263;"
  ]
  node [
    id 33
    label "wymy&#347;li&#263;"
  ]
  node [
    id 34
    label "invent"
  ]
  node [
    id 35
    label "pozyska&#263;"
  ]
  node [
    id 36
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 37
    label "wykry&#263;"
  ]
  node [
    id 38
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 39
    label "dozna&#263;"
  ]
  node [
    id 40
    label "istota_&#380;ywa"
  ]
  node [
    id 41
    label "najgorszy"
  ]
  node [
    id 42
    label "aktualny"
  ]
  node [
    id 43
    label "ostatnio"
  ]
  node [
    id 44
    label "niedawno"
  ]
  node [
    id 45
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 46
    label "sko&#324;czony"
  ]
  node [
    id 47
    label "poprzedni"
  ]
  node [
    id 48
    label "pozosta&#322;y"
  ]
  node [
    id 49
    label "w&#261;tpliwy"
  ]
  node [
    id 50
    label "cia&#322;o"
  ]
  node [
    id 51
    label "plac"
  ]
  node [
    id 52
    label "cecha"
  ]
  node [
    id 53
    label "uwaga"
  ]
  node [
    id 54
    label "przestrze&#324;"
  ]
  node [
    id 55
    label "status"
  ]
  node [
    id 56
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 57
    label "chwila"
  ]
  node [
    id 58
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 59
    label "rz&#261;d"
  ]
  node [
    id 60
    label "praca"
  ]
  node [
    id 61
    label "location"
  ]
  node [
    id 62
    label "warunek_lokalowy"
  ]
  node [
    id 63
    label "Health"
  ]
  node [
    id 64
    label "at"
  ]
  node [
    id 65
    label "a"
  ]
  node [
    id 66
    label "glanc"
  ]
  node [
    id 67
    label "2018"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 63
    target 66
  ]
  edge [
    source 63
    target 67
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 64
    target 67
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 67
  ]
  edge [
    source 66
    target 67
  ]
]
