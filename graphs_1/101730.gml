graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.0851063829787235
  density 0.045328399629972246
  graphCliqueNumber 3
  node [
    id 0
    label "udost&#281;pnia&#263;"
    origin "text"
  ]
  node [
    id 1
    label "siebie"
    origin "text"
  ]
  node [
    id 2
    label "wzajemnie"
    origin "text"
  ]
  node [
    id 3
    label "informacja"
    origin "text"
  ]
  node [
    id 4
    label "nowa"
    origin "text"
  ]
  node [
    id 5
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 6
    label "odurzaj&#261;cy"
    origin "text"
  ]
  node [
    id 7
    label "substancja"
    origin "text"
  ]
  node [
    id 8
    label "psychotropowy"
    origin "text"
  ]
  node [
    id 9
    label "prekursor"
    origin "text"
  ]
  node [
    id 10
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 11
    label "wsp&#243;lnie"
  ]
  node [
    id 12
    label "wzajemny"
  ]
  node [
    id 13
    label "doj&#347;cie"
  ]
  node [
    id 14
    label "doj&#347;&#263;"
  ]
  node [
    id 15
    label "powzi&#261;&#263;"
  ]
  node [
    id 16
    label "wiedza"
  ]
  node [
    id 17
    label "sygna&#322;"
  ]
  node [
    id 18
    label "obiegni&#281;cie"
  ]
  node [
    id 19
    label "obieganie"
  ]
  node [
    id 20
    label "obiec"
  ]
  node [
    id 21
    label "dane"
  ]
  node [
    id 22
    label "obiega&#263;"
  ]
  node [
    id 23
    label "punkt"
  ]
  node [
    id 24
    label "publikacja"
  ]
  node [
    id 25
    label "powzi&#281;cie"
  ]
  node [
    id 26
    label "gwiazda"
  ]
  node [
    id 27
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 28
    label "miejsce"
  ]
  node [
    id 29
    label "czas"
  ]
  node [
    id 30
    label "abstrakcja"
  ]
  node [
    id 31
    label "spos&#243;b"
  ]
  node [
    id 32
    label "chemikalia"
  ]
  node [
    id 33
    label "odurzaj&#261;co"
  ]
  node [
    id 34
    label "zniewalaj&#261;cy"
  ]
  node [
    id 35
    label "intensywny"
  ]
  node [
    id 36
    label "byt"
  ]
  node [
    id 37
    label "smolisty"
  ]
  node [
    id 38
    label "przenika&#263;"
  ]
  node [
    id 39
    label "cz&#261;steczka"
  ]
  node [
    id 40
    label "materia"
  ]
  node [
    id 41
    label "przenikanie"
  ]
  node [
    id 42
    label "temperatura_krytyczna"
  ]
  node [
    id 43
    label "psychoaktywny"
  ]
  node [
    id 44
    label "leczniczy"
  ]
  node [
    id 45
    label "innowator"
  ]
  node [
    id 46
    label "inicjator"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
]
