graph [
  maxDegree 73
  minDegree 1
  meanDegree 2.078624078624079
  density 0.005119763740453396
  graphCliqueNumber 3
  node [
    id 0
    label "kilka"
    origin "text"
  ]
  node [
    id 1
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "opublikowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "notatka"
    origin "text"
  ]
  node [
    id 5
    label "wyk&#322;ad"
    origin "text"
  ]
  node [
    id 6
    label "profesor"
    origin "text"
  ]
  node [
    id 7
    label "taylor"
    origin "text"
  ]
  node [
    id 8
    label "opowiada&#263;"
    origin "text"
  ]
  node [
    id 9
    label "rozkwit"
    origin "text"
  ]
  node [
    id 10
    label "design"
    origin "text"
  ]
  node [
    id 11
    label "brytyjski"
    origin "text"
  ]
  node [
    id 12
    label "schy&#322;ek"
    origin "text"
  ]
  node [
    id 13
    label "produkcja"
    origin "text"
  ]
  node [
    id 14
    label "wyspa"
    origin "text"
  ]
  node [
    id 15
    label "hala"
    origin "text"
  ]
  node [
    id 16
    label "varian"
    origin "text"
  ]
  node [
    id 17
    label "nyt"
    origin "text"
  ]
  node [
    id 18
    label "streszcza&#263;"
    origin "text"
  ]
  node [
    id 19
    label "wynik"
    origin "text"
  ]
  node [
    id 20
    label "badanie"
    origin "text"
  ]
  node [
    id 21
    label "poprzedni"
    origin "text"
  ]
  node [
    id 22
    label "link"
    origin "text"
  ]
  node [
    id 23
    label "by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 25
    label "dost&#281;pny"
    origin "text"
  ]
  node [
    id 26
    label "subskrypcja"
    origin "text"
  ]
  node [
    id 27
    label "poleca&#263;"
    origin "text"
  ]
  node [
    id 28
    label "raczej"
    origin "text"
  ]
  node [
    id 29
    label "ten"
    origin "text"
  ]
  node [
    id 30
    label "dochodzenie"
    origin "text"
  ]
  node [
    id 31
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 32
    label "przez"
    origin "text"
  ]
  node [
    id 33
    label "badacz"
    origin "text"
  ]
  node [
    id 34
    label "university"
    origin "text"
  ]
  node [
    id 35
    label "california"
    origin "text"
  ]
  node [
    id 36
    label "irvine"
    origin "text"
  ]
  node [
    id 37
    label "stara&#263;"
    origin "text"
  ]
  node [
    id 38
    label "si&#281;"
    origin "text"
  ]
  node [
    id 39
    label "dociec"
    origin "text"
  ]
  node [
    id 40
    label "kto"
    origin "text"
  ]
  node [
    id 41
    label "produkowa&#263;"
    origin "text"
  ]
  node [
    id 42
    label "ipody"
    origin "text"
  ]
  node [
    id 43
    label "pytanie"
    origin "text"
  ]
  node [
    id 44
    label "niebagatelny"
    origin "text"
  ]
  node [
    id 45
    label "bowiem"
    origin "text"
  ]
  node [
    id 46
    label "ipod"
    origin "text"
  ]
  node [
    id 47
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 48
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 49
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 50
    label "apple"
    origin "text"
  ]
  node [
    id 51
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 52
    label "tekst"
    origin "text"
  ]
  node [
    id 53
    label "zdradzi&#263;"
    origin "text"
  ]
  node [
    id 54
    label "puenta"
    origin "text"
  ]
  node [
    id 55
    label "&#347;ledziowate"
  ]
  node [
    id 56
    label "ryba"
  ]
  node [
    id 57
    label "czas"
  ]
  node [
    id 58
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 59
    label "rok"
  ]
  node [
    id 60
    label "miech"
  ]
  node [
    id 61
    label "kalendy"
  ]
  node [
    id 62
    label "tydzie&#324;"
  ]
  node [
    id 63
    label "konotatka"
  ]
  node [
    id 64
    label "tre&#347;ciwy"
  ]
  node [
    id 65
    label "zapis"
  ]
  node [
    id 66
    label "note"
  ]
  node [
    id 67
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 68
    label "przem&#243;wienie"
  ]
  node [
    id 69
    label "lecture"
  ]
  node [
    id 70
    label "kurs"
  ]
  node [
    id 71
    label "t&#322;umaczenie"
  ]
  node [
    id 72
    label "wirtuoz"
  ]
  node [
    id 73
    label "nauczyciel_akademicki"
  ]
  node [
    id 74
    label "tytu&#322;"
  ]
  node [
    id 75
    label "konsulent"
  ]
  node [
    id 76
    label "nauczyciel"
  ]
  node [
    id 77
    label "stopie&#324;_naukowy"
  ]
  node [
    id 78
    label "profesura"
  ]
  node [
    id 79
    label "relate"
  ]
  node [
    id 80
    label "przedstawia&#263;"
  ]
  node [
    id 81
    label "prawi&#263;"
  ]
  node [
    id 82
    label "rozw&#243;j"
  ]
  node [
    id 83
    label "wegetacja"
  ]
  node [
    id 84
    label "blooming"
  ]
  node [
    id 85
    label "wygl&#261;d"
  ]
  node [
    id 86
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 87
    label "angielsko"
  ]
  node [
    id 88
    label "po_brytyjsku"
  ]
  node [
    id 89
    label "europejski"
  ]
  node [
    id 90
    label "j&#281;zyk_celtycki"
  ]
  node [
    id 91
    label "j&#281;zyk_martwy"
  ]
  node [
    id 92
    label "j&#281;zyk_angielski"
  ]
  node [
    id 93
    label "zachodnioeuropejski"
  ]
  node [
    id 94
    label "morris"
  ]
  node [
    id 95
    label "angielski"
  ]
  node [
    id 96
    label "anglosaski"
  ]
  node [
    id 97
    label "brytyjsko"
  ]
  node [
    id 98
    label "kres"
  ]
  node [
    id 99
    label "tingel-tangel"
  ]
  node [
    id 100
    label "odtworzenie"
  ]
  node [
    id 101
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 102
    label "wydawa&#263;"
  ]
  node [
    id 103
    label "realizacja"
  ]
  node [
    id 104
    label "monta&#380;"
  ]
  node [
    id 105
    label "fabrication"
  ]
  node [
    id 106
    label "kreacja"
  ]
  node [
    id 107
    label "uzysk"
  ]
  node [
    id 108
    label "dorobek"
  ]
  node [
    id 109
    label "wyda&#263;"
  ]
  node [
    id 110
    label "impreza"
  ]
  node [
    id 111
    label "postprodukcja"
  ]
  node [
    id 112
    label "numer"
  ]
  node [
    id 113
    label "kooperowa&#263;"
  ]
  node [
    id 114
    label "creation"
  ]
  node [
    id 115
    label "trema"
  ]
  node [
    id 116
    label "zbi&#243;r"
  ]
  node [
    id 117
    label "product"
  ]
  node [
    id 118
    label "performance"
  ]
  node [
    id 119
    label "Lesbos"
  ]
  node [
    id 120
    label "Palau"
  ]
  node [
    id 121
    label "Barbados"
  ]
  node [
    id 122
    label "Wyspa_&#346;wi&#281;tej_Heleny"
  ]
  node [
    id 123
    label "obszar"
  ]
  node [
    id 124
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 125
    label "Tasmania"
  ]
  node [
    id 126
    label "Uznam"
  ]
  node [
    id 127
    label "Helgoland"
  ]
  node [
    id 128
    label "Eolia"
  ]
  node [
    id 129
    label "Sardynia"
  ]
  node [
    id 130
    label "Gotlandia"
  ]
  node [
    id 131
    label "Anguilla"
  ]
  node [
    id 132
    label "Sint_Maarten"
  ]
  node [
    id 133
    label "Rodos"
  ]
  node [
    id 134
    label "Cebu"
  ]
  node [
    id 135
    label "Nowa_Zelandia"
  ]
  node [
    id 136
    label "Tajwan"
  ]
  node [
    id 137
    label "mebel"
  ]
  node [
    id 138
    label "Rugia"
  ]
  node [
    id 139
    label "Tahiti"
  ]
  node [
    id 140
    label "kresom&#243;zgowie"
  ]
  node [
    id 141
    label "Nowa_Gwinea"
  ]
  node [
    id 142
    label "Wolin"
  ]
  node [
    id 143
    label "Madagaskar"
  ]
  node [
    id 144
    label "Sumatra"
  ]
  node [
    id 145
    label "Cejlon"
  ]
  node [
    id 146
    label "Sycylia"
  ]
  node [
    id 147
    label "Cura&#231;ao"
  ]
  node [
    id 148
    label "Man"
  ]
  node [
    id 149
    label "Kreta"
  ]
  node [
    id 150
    label "Jamajka"
  ]
  node [
    id 151
    label "Cypr"
  ]
  node [
    id 152
    label "Bornholm"
  ]
  node [
    id 153
    label "Trynidad"
  ]
  node [
    id 154
    label "Wielka_Brytania"
  ]
  node [
    id 155
    label "Kuba"
  ]
  node [
    id 156
    label "Jawa"
  ]
  node [
    id 157
    label "Ibiza"
  ]
  node [
    id 158
    label "Okinawa"
  ]
  node [
    id 159
    label "Sachalin"
  ]
  node [
    id 160
    label "Bali"
  ]
  node [
    id 161
    label "Portoryko"
  ]
  node [
    id 162
    label "Malta"
  ]
  node [
    id 163
    label "Cuszima"
  ]
  node [
    id 164
    label "Wielka_Bahama"
  ]
  node [
    id 165
    label "Tobago"
  ]
  node [
    id 166
    label "Sint_Eustatius"
  ]
  node [
    id 167
    label "Portland"
  ]
  node [
    id 168
    label "Montserrat"
  ]
  node [
    id 169
    label "Nowa_Fundlandia"
  ]
  node [
    id 170
    label "Saba"
  ]
  node [
    id 171
    label "Bonaire"
  ]
  node [
    id 172
    label "Gozo"
  ]
  node [
    id 173
    label "Eubea"
  ]
  node [
    id 174
    label "Grenlandia"
  ]
  node [
    id 175
    label "Samotraka"
  ]
  node [
    id 176
    label "Paros"
  ]
  node [
    id 177
    label "Borneo"
  ]
  node [
    id 178
    label "l&#261;d"
  ]
  node [
    id 179
    label "Majorka"
  ]
  node [
    id 180
    label "Itaka"
  ]
  node [
    id 181
    label "Irlandia"
  ]
  node [
    id 182
    label "Salamina"
  ]
  node [
    id 183
    label "Samos"
  ]
  node [
    id 184
    label "Timor"
  ]
  node [
    id 185
    label "Haiti"
  ]
  node [
    id 186
    label "Korsyka"
  ]
  node [
    id 187
    label "Zelandia"
  ]
  node [
    id 188
    label "Ostr&#243;w_Lednicki"
  ]
  node [
    id 189
    label "Ajon"
  ]
  node [
    id 190
    label "kopalnia"
  ]
  node [
    id 191
    label "halizna"
  ]
  node [
    id 192
    label "fabryka"
  ]
  node [
    id 193
    label "huta"
  ]
  node [
    id 194
    label "pastwisko"
  ]
  node [
    id 195
    label "budynek"
  ]
  node [
    id 196
    label "pi&#281;tro"
  ]
  node [
    id 197
    label "oczyszczalnia"
  ]
  node [
    id 198
    label "dworzec"
  ]
  node [
    id 199
    label "lotnisko"
  ]
  node [
    id 200
    label "pomieszczenie"
  ]
  node [
    id 201
    label "sum_up"
  ]
  node [
    id 202
    label "typ"
  ]
  node [
    id 203
    label "dzia&#322;anie"
  ]
  node [
    id 204
    label "przyczyna"
  ]
  node [
    id 205
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 206
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 207
    label "zaokr&#261;glenie"
  ]
  node [
    id 208
    label "event"
  ]
  node [
    id 209
    label "rezultat"
  ]
  node [
    id 210
    label "usi&#322;owanie"
  ]
  node [
    id 211
    label "examination"
  ]
  node [
    id 212
    label "investigation"
  ]
  node [
    id 213
    label "ustalenie"
  ]
  node [
    id 214
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 215
    label "ustalanie"
  ]
  node [
    id 216
    label "bia&#322;a_niedziela"
  ]
  node [
    id 217
    label "analysis"
  ]
  node [
    id 218
    label "rozpatrywanie"
  ]
  node [
    id 219
    label "wziernikowanie"
  ]
  node [
    id 220
    label "obserwowanie"
  ]
  node [
    id 221
    label "omawianie"
  ]
  node [
    id 222
    label "sprawdzanie"
  ]
  node [
    id 223
    label "udowadnianie"
  ]
  node [
    id 224
    label "diagnostyka"
  ]
  node [
    id 225
    label "czynno&#347;&#263;"
  ]
  node [
    id 226
    label "macanie"
  ]
  node [
    id 227
    label "rektalny"
  ]
  node [
    id 228
    label "penetrowanie"
  ]
  node [
    id 229
    label "krytykowanie"
  ]
  node [
    id 230
    label "kontrola"
  ]
  node [
    id 231
    label "dociekanie"
  ]
  node [
    id 232
    label "zrecenzowanie"
  ]
  node [
    id 233
    label "praca"
  ]
  node [
    id 234
    label "poprzednio"
  ]
  node [
    id 235
    label "przesz&#322;y"
  ]
  node [
    id 236
    label "wcze&#347;niejszy"
  ]
  node [
    id 237
    label "buton"
  ]
  node [
    id 238
    label "odsy&#322;acz"
  ]
  node [
    id 239
    label "si&#281;ga&#263;"
  ]
  node [
    id 240
    label "trwa&#263;"
  ]
  node [
    id 241
    label "obecno&#347;&#263;"
  ]
  node [
    id 242
    label "stan"
  ]
  node [
    id 243
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 244
    label "stand"
  ]
  node [
    id 245
    label "mie&#263;_miejsce"
  ]
  node [
    id 246
    label "uczestniczy&#263;"
  ]
  node [
    id 247
    label "chodzi&#263;"
  ]
  node [
    id 248
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 249
    label "equal"
  ]
  node [
    id 250
    label "&#322;atwy"
  ]
  node [
    id 251
    label "mo&#380;liwy"
  ]
  node [
    id 252
    label "dost&#281;pnie"
  ]
  node [
    id 253
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 254
    label "przyst&#281;pnie"
  ]
  node [
    id 255
    label "zrozumia&#322;y"
  ]
  node [
    id 256
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 257
    label "odblokowanie_si&#281;"
  ]
  node [
    id 258
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 259
    label "struktura"
  ]
  node [
    id 260
    label "przedp&#322;ata"
  ]
  node [
    id 261
    label "element"
  ]
  node [
    id 262
    label "poj&#281;cie"
  ]
  node [
    id 263
    label "control"
  ]
  node [
    id 264
    label "placard"
  ]
  node [
    id 265
    label "m&#243;wi&#263;"
  ]
  node [
    id 266
    label "charge"
  ]
  node [
    id 267
    label "zadawa&#263;"
  ]
  node [
    id 268
    label "ordynowa&#263;"
  ]
  node [
    id 269
    label "powierza&#263;"
  ]
  node [
    id 270
    label "doradza&#263;"
  ]
  node [
    id 271
    label "okre&#347;lony"
  ]
  node [
    id 272
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 273
    label "dosi&#281;ganie"
  ]
  node [
    id 274
    label "robienie"
  ]
  node [
    id 275
    label "dor&#281;czanie"
  ]
  node [
    id 276
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 277
    label "stawanie_si&#281;"
  ]
  node [
    id 278
    label "trial"
  ]
  node [
    id 279
    label "dop&#322;ata"
  ]
  node [
    id 280
    label "dojrza&#322;y"
  ]
  node [
    id 281
    label "assay"
  ]
  node [
    id 282
    label "dochrapywanie_si&#281;"
  ]
  node [
    id 283
    label "inquest"
  ]
  node [
    id 284
    label "dodatek"
  ]
  node [
    id 285
    label "orgazm"
  ]
  node [
    id 286
    label "przy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 287
    label "osi&#261;ganie"
  ]
  node [
    id 288
    label "rozpowszechnianie"
  ]
  node [
    id 289
    label "rozwijanie_si&#281;"
  ]
  node [
    id 290
    label "strzelenie"
  ]
  node [
    id 291
    label "dolatywanie"
  ]
  node [
    id 292
    label "roszczenie"
  ]
  node [
    id 293
    label "ro&#347;ni&#281;cie"
  ]
  node [
    id 294
    label "inquisition"
  ]
  node [
    id 295
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 296
    label "doczekanie"
  ]
  node [
    id 297
    label "wydarzenie"
  ]
  node [
    id 298
    label "docieranie"
  ]
  node [
    id 299
    label "dop&#322;ywanie"
  ]
  node [
    id 300
    label "zaznawanie"
  ]
  node [
    id 301
    label "maturation"
  ]
  node [
    id 302
    label "uzyskiwanie"
  ]
  node [
    id 303
    label "doje&#380;d&#380;anie"
  ]
  node [
    id 304
    label "przesy&#322;ka"
  ]
  node [
    id 305
    label "Inquisition"
  ]
  node [
    id 306
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 307
    label "postrzeganie"
  ]
  node [
    id 308
    label "detektyw"
  ]
  node [
    id 309
    label "powodowanie"
  ]
  node [
    id 310
    label "pom&#243;c"
  ]
  node [
    id 311
    label "zbudowa&#263;"
  ]
  node [
    id 312
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 313
    label "leave"
  ]
  node [
    id 314
    label "przewie&#347;&#263;"
  ]
  node [
    id 315
    label "wykona&#263;"
  ]
  node [
    id 316
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 317
    label "draw"
  ]
  node [
    id 318
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 319
    label "carry"
  ]
  node [
    id 320
    label "Miczurin"
  ]
  node [
    id 321
    label "&#347;ledziciel"
  ]
  node [
    id 322
    label "uczony"
  ]
  node [
    id 323
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 324
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 325
    label "search"
  ]
  node [
    id 326
    label "ask"
  ]
  node [
    id 327
    label "wytwarza&#263;"
  ]
  node [
    id 328
    label "create"
  ]
  node [
    id 329
    label "dostarcza&#263;"
  ]
  node [
    id 330
    label "tworzy&#263;"
  ]
  node [
    id 331
    label "sprawa"
  ]
  node [
    id 332
    label "zadanie"
  ]
  node [
    id 333
    label "wypowied&#378;"
  ]
  node [
    id 334
    label "problemat"
  ]
  node [
    id 335
    label "rozpytywanie"
  ]
  node [
    id 336
    label "sprawdzian"
  ]
  node [
    id 337
    label "przes&#322;uchiwanie"
  ]
  node [
    id 338
    label "wypytanie"
  ]
  node [
    id 339
    label "zwracanie_si&#281;"
  ]
  node [
    id 340
    label "wypowiedzenie"
  ]
  node [
    id 341
    label "wywo&#322;ywanie"
  ]
  node [
    id 342
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 343
    label "problematyka"
  ]
  node [
    id 344
    label "question"
  ]
  node [
    id 345
    label "odpowiadanie"
  ]
  node [
    id 346
    label "survey"
  ]
  node [
    id 347
    label "odpowiada&#263;"
  ]
  node [
    id 348
    label "egzaminowanie"
  ]
  node [
    id 349
    label "wa&#380;ny"
  ]
  node [
    id 350
    label "render"
  ]
  node [
    id 351
    label "zmienia&#263;"
  ]
  node [
    id 352
    label "zestaw"
  ]
  node [
    id 353
    label "train"
  ]
  node [
    id 354
    label "uk&#322;ada&#263;"
  ]
  node [
    id 355
    label "dzieli&#263;"
  ]
  node [
    id 356
    label "set"
  ]
  node [
    id 357
    label "przywraca&#263;"
  ]
  node [
    id 358
    label "dawa&#263;"
  ]
  node [
    id 359
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 360
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 361
    label "zbiera&#263;"
  ]
  node [
    id 362
    label "convey"
  ]
  node [
    id 363
    label "opracowywa&#263;"
  ]
  node [
    id 364
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 365
    label "publicize"
  ]
  node [
    id 366
    label "przekazywa&#263;"
  ]
  node [
    id 367
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 368
    label "scala&#263;"
  ]
  node [
    id 369
    label "oddawa&#263;"
  ]
  node [
    id 370
    label "whole"
  ]
  node [
    id 371
    label "Rzym_Zachodni"
  ]
  node [
    id 372
    label "ilo&#347;&#263;"
  ]
  node [
    id 373
    label "urz&#261;dzenie"
  ]
  node [
    id 374
    label "Rzym_Wschodni"
  ]
  node [
    id 375
    label "nijaki"
  ]
  node [
    id 376
    label "du&#380;y"
  ]
  node [
    id 377
    label "jedyny"
  ]
  node [
    id 378
    label "kompletny"
  ]
  node [
    id 379
    label "zdr&#243;w"
  ]
  node [
    id 380
    label "&#380;ywy"
  ]
  node [
    id 381
    label "ca&#322;o"
  ]
  node [
    id 382
    label "pe&#322;ny"
  ]
  node [
    id 383
    label "calu&#347;ko"
  ]
  node [
    id 384
    label "podobny"
  ]
  node [
    id 385
    label "pisa&#263;"
  ]
  node [
    id 386
    label "odmianka"
  ]
  node [
    id 387
    label "opu&#347;ci&#263;"
  ]
  node [
    id 388
    label "wytw&#243;r"
  ]
  node [
    id 389
    label "koniektura"
  ]
  node [
    id 390
    label "preparacja"
  ]
  node [
    id 391
    label "ekscerpcja"
  ]
  node [
    id 392
    label "redakcja"
  ]
  node [
    id 393
    label "obelga"
  ]
  node [
    id 394
    label "dzie&#322;o"
  ]
  node [
    id 395
    label "j&#281;zykowo"
  ]
  node [
    id 396
    label "pomini&#281;cie"
  ]
  node [
    id 397
    label "odst&#261;pi&#263;"
  ]
  node [
    id 398
    label "denounce"
  ]
  node [
    id 399
    label "rogi"
  ]
  node [
    id 400
    label "nabra&#263;"
  ]
  node [
    id 401
    label "objawi&#263;"
  ]
  node [
    id 402
    label "poinformowa&#263;"
  ]
  node [
    id 403
    label "naruszy&#263;"
  ]
  node [
    id 404
    label "baletka"
  ]
  node [
    id 405
    label "gra_w_bule"
  ]
  node [
    id 406
    label "fina&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 197
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 80
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 228
  ]
  edge [
    source 20
    target 229
  ]
  edge [
    source 20
    target 230
  ]
  edge [
    source 20
    target 231
  ]
  edge [
    source 20
    target 232
  ]
  edge [
    source 20
    target 233
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 234
  ]
  edge [
    source 21
    target 235
  ]
  edge [
    source 21
    target 236
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 22
    target 238
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 43
  ]
  edge [
    source 23
    target 44
  ]
  edge [
    source 23
    target 239
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 243
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 250
  ]
  edge [
    source 25
    target 251
  ]
  edge [
    source 25
    target 252
  ]
  edge [
    source 25
    target 253
  ]
  edge [
    source 25
    target 254
  ]
  edge [
    source 25
    target 255
  ]
  edge [
    source 25
    target 256
  ]
  edge [
    source 25
    target 257
  ]
  edge [
    source 25
    target 258
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 259
  ]
  edge [
    source 26
    target 260
  ]
  edge [
    source 26
    target 261
  ]
  edge [
    source 26
    target 262
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 50
  ]
  edge [
    source 27
    target 51
  ]
  edge [
    source 27
    target 263
  ]
  edge [
    source 27
    target 264
  ]
  edge [
    source 27
    target 265
  ]
  edge [
    source 27
    target 266
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 27
    target 268
  ]
  edge [
    source 27
    target 269
  ]
  edge [
    source 27
    target 102
  ]
  edge [
    source 27
    target 270
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 53
  ]
  edge [
    source 29
    target 271
  ]
  edge [
    source 29
    target 272
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 273
  ]
  edge [
    source 30
    target 274
  ]
  edge [
    source 30
    target 275
  ]
  edge [
    source 30
    target 276
  ]
  edge [
    source 30
    target 211
  ]
  edge [
    source 30
    target 277
  ]
  edge [
    source 30
    target 214
  ]
  edge [
    source 30
    target 278
  ]
  edge [
    source 30
    target 279
  ]
  edge [
    source 30
    target 280
  ]
  edge [
    source 30
    target 281
  ]
  edge [
    source 30
    target 282
  ]
  edge [
    source 30
    target 283
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 30
    target 288
  ]
  edge [
    source 30
    target 289
  ]
  edge [
    source 30
    target 290
  ]
  edge [
    source 30
    target 291
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 292
  ]
  edge [
    source 30
    target 293
  ]
  edge [
    source 30
    target 294
  ]
  edge [
    source 30
    target 295
  ]
  edge [
    source 30
    target 296
  ]
  edge [
    source 30
    target 297
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 308
  ]
  edge [
    source 30
    target 309
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 310
  ]
  edge [
    source 31
    target 311
  ]
  edge [
    source 31
    target 312
  ]
  edge [
    source 31
    target 313
  ]
  edge [
    source 31
    target 314
  ]
  edge [
    source 31
    target 315
  ]
  edge [
    source 31
    target 316
  ]
  edge [
    source 31
    target 317
  ]
  edge [
    source 31
    target 318
  ]
  edge [
    source 31
    target 319
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 320
  ]
  edge [
    source 33
    target 321
  ]
  edge [
    source 33
    target 322
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 47
  ]
  edge [
    source 38
    target 48
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 323
  ]
  edge [
    source 39
    target 324
  ]
  edge [
    source 39
    target 325
  ]
  edge [
    source 39
    target 326
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 49
  ]
  edge [
    source 41
    target 50
  ]
  edge [
    source 41
    target 327
  ]
  edge [
    source 41
    target 328
  ]
  edge [
    source 41
    target 329
  ]
  edge [
    source 41
    target 330
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 331
  ]
  edge [
    source 43
    target 332
  ]
  edge [
    source 43
    target 333
  ]
  edge [
    source 43
    target 334
  ]
  edge [
    source 43
    target 335
  ]
  edge [
    source 43
    target 336
  ]
  edge [
    source 43
    target 337
  ]
  edge [
    source 43
    target 338
  ]
  edge [
    source 43
    target 339
  ]
  edge [
    source 43
    target 340
  ]
  edge [
    source 43
    target 341
  ]
  edge [
    source 43
    target 342
  ]
  edge [
    source 43
    target 343
  ]
  edge [
    source 43
    target 344
  ]
  edge [
    source 43
    target 222
  ]
  edge [
    source 43
    target 345
  ]
  edge [
    source 43
    target 346
  ]
  edge [
    source 43
    target 347
  ]
  edge [
    source 43
    target 348
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 349
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 350
  ]
  edge [
    source 47
    target 351
  ]
  edge [
    source 47
    target 352
  ]
  edge [
    source 47
    target 353
  ]
  edge [
    source 47
    target 354
  ]
  edge [
    source 47
    target 355
  ]
  edge [
    source 47
    target 356
  ]
  edge [
    source 47
    target 357
  ]
  edge [
    source 47
    target 358
  ]
  edge [
    source 47
    target 359
  ]
  edge [
    source 47
    target 360
  ]
  edge [
    source 47
    target 361
  ]
  edge [
    source 47
    target 362
  ]
  edge [
    source 47
    target 363
  ]
  edge [
    source 47
    target 364
  ]
  edge [
    source 47
    target 365
  ]
  edge [
    source 47
    target 366
  ]
  edge [
    source 47
    target 367
  ]
  edge [
    source 47
    target 368
  ]
  edge [
    source 47
    target 369
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 370
  ]
  edge [
    source 48
    target 371
  ]
  edge [
    source 48
    target 261
  ]
  edge [
    source 48
    target 372
  ]
  edge [
    source 48
    target 373
  ]
  edge [
    source 48
    target 374
  ]
  edge [
    source 49
    target 375
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 376
  ]
  edge [
    source 51
    target 377
  ]
  edge [
    source 51
    target 378
  ]
  edge [
    source 51
    target 379
  ]
  edge [
    source 51
    target 380
  ]
  edge [
    source 51
    target 381
  ]
  edge [
    source 51
    target 382
  ]
  edge [
    source 51
    target 383
  ]
  edge [
    source 51
    target 384
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 385
  ]
  edge [
    source 52
    target 386
  ]
  edge [
    source 52
    target 387
  ]
  edge [
    source 52
    target 333
  ]
  edge [
    source 52
    target 388
  ]
  edge [
    source 52
    target 389
  ]
  edge [
    source 52
    target 390
  ]
  edge [
    source 52
    target 391
  ]
  edge [
    source 52
    target 392
  ]
  edge [
    source 52
    target 393
  ]
  edge [
    source 52
    target 394
  ]
  edge [
    source 52
    target 395
  ]
  edge [
    source 52
    target 396
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 397
  ]
  edge [
    source 53
    target 398
  ]
  edge [
    source 53
    target 399
  ]
  edge [
    source 53
    target 400
  ]
  edge [
    source 53
    target 401
  ]
  edge [
    source 53
    target 402
  ]
  edge [
    source 53
    target 403
  ]
  edge [
    source 54
    target 404
  ]
  edge [
    source 54
    target 405
  ]
  edge [
    source 54
    target 406
  ]
]
