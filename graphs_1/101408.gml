graph [
  maxDegree 1
  minDegree 0
  meanDegree 0.8571428571428571
  density 0.14285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "fzab"
    origin "text"
  ]
  node [
    id 1
    label "FZAB"
  ]
  node [
    id 2
    label "500M"
  ]
  node [
    id 3
    label "&#1060;&#1047;&#1040;&#1041;"
  ]
  node [
    id 4
    label "500&#1052;"
  ]
  node [
    id 5
    label "FAB"
  ]
  node [
    id 6
    label "500"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
]
