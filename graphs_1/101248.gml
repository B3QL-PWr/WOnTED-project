graph [
  maxDegree 14
  minDegree 1
  meanDegree 3.090909090909091
  density 0.04755244755244755
  graphCliqueNumber 9
  node [
    id 0
    label "lidia"
    origin "text"
  ]
  node [
    id 1
    label "cio&#322;kosz"
    origin "text"
  ]
  node [
    id 2
    label "Lidia"
  ]
  node [
    id 3
    label "Cio&#322;kosz"
  ]
  node [
    id 4
    label "Tomasz&#243;w"
  ]
  node [
    id 5
    label "mazowiecki"
  ]
  node [
    id 6
    label "Cio&#322;koszowa"
  ]
  node [
    id 7
    label "zeszyt"
  ]
  node [
    id 8
    label "dom"
  ]
  node [
    id 9
    label "Kahan"
  ]
  node [
    id 10
    label "Adam"
  ]
  node [
    id 11
    label "studium"
  ]
  node [
    id 12
    label "pedagogiczny"
  ]
  node [
    id 13
    label "uniwersytet"
  ]
  node [
    id 14
    label "jagiello&#324;ski"
  ]
  node [
    id 15
    label "wojna"
  ]
  node [
    id 16
    label "polsko"
  ]
  node [
    id 17
    label "bolszewicki"
  ]
  node [
    id 18
    label "polski"
  ]
  node [
    id 19
    label "partia"
  ]
  node [
    id 20
    label "socjalistyczny"
  ]
  node [
    id 21
    label "rada"
  ]
  node [
    id 22
    label "naczelny"
  ]
  node [
    id 23
    label "ii"
  ]
  node [
    id 24
    label "&#347;wiatowy"
  ]
  node [
    id 25
    label "towarzystwo"
  ]
  node [
    id 26
    label "robotniczy"
  ]
  node [
    id 27
    label "komitet"
  ]
  node [
    id 28
    label "zagraniczny"
  ]
  node [
    id 29
    label "centralny"
  ]
  node [
    id 30
    label "scaleniowy"
  ]
  node [
    id 31
    label "zjazd"
  ]
  node [
    id 32
    label "XXV"
  ]
  node [
    id 33
    label "partyjny"
  ]
  node [
    id 34
    label "rozg&#322;o&#347;nia"
  ]
  node [
    id 35
    label "radio"
  ]
  node [
    id 36
    label "wolny"
  ]
  node [
    id 37
    label "Europa"
  ]
  node [
    id 38
    label "Walery"
  ]
  node [
    id 39
    label "wr&#243;blewski"
  ]
  node [
    id 40
    label "genera&#322;"
  ]
  node [
    id 41
    label "zarys"
  ]
  node [
    id 42
    label "dzieje"
  ]
  node [
    id 43
    label "socjalizm"
  ]
  node [
    id 44
    label "polskie"
  ]
  node [
    id 45
    label "publicystyka"
  ]
  node [
    id 46
    label "na"
  ]
  node [
    id 47
    label "emigracja"
  ]
  node [
    id 48
    label "wyspa"
  ]
  node [
    id 49
    label "rok"
  ]
  node [
    id 50
    label "1940"
  ]
  node [
    id 51
    label "&#8211;"
  ]
  node [
    id 52
    label "1960"
  ]
  node [
    id 53
    label "Aleksandra"
  ]
  node [
    id 54
    label "wata"
  ]
  node [
    id 55
    label "m&#243;j"
  ]
  node [
    id 56
    label "wieko"
  ]
  node [
    id 57
    label "Polonia"
  ]
  node [
    id 58
    label "Restituta"
  ]
  node [
    id 59
    label "nowy"
  ]
  node [
    id 60
    label "Jork"
  ]
  node [
    id 61
    label "stowarzyszy&#263;"
  ]
  node [
    id 62
    label "kombatant"
  ]
  node [
    id 63
    label "fundacja"
  ]
  node [
    id 64
    label "on"
  ]
  node [
    id 65
    label "Jurzykowskiego"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 13
    target 26
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 24
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 34
  ]
  edge [
    source 18
    target 35
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 37
  ]
  edge [
    source 18
    target 45
  ]
  edge [
    source 18
    target 46
  ]
  edge [
    source 18
    target 47
  ]
  edge [
    source 18
    target 48
  ]
  edge [
    source 18
    target 49
  ]
  edge [
    source 18
    target 50
  ]
  edge [
    source 18
    target 51
  ]
  edge [
    source 18
    target 52
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 61
  ]
  edge [
    source 44
    target 62
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 45
    target 49
  ]
  edge [
    source 45
    target 50
  ]
  edge [
    source 45
    target 51
  ]
  edge [
    source 45
    target 52
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 46
    target 50
  ]
  edge [
    source 46
    target 51
  ]
  edge [
    source 46
    target 52
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 47
    target 52
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 49
    target 52
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 64
    target 65
  ]
]
