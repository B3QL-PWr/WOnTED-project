graph [
  maxDegree 16
  minDegree 1
  meanDegree 4.64
  density 0.19333333333333333
  graphCliqueNumber 5
  node [
    id 0
    label "luigi"
    origin "text"
  ]
  node [
    id 1
    label "scala&#263;"
    origin "text"
  ]
  node [
    id 2
    label "consort"
  ]
  node [
    id 3
    label "jednoczy&#263;"
  ]
  node [
    id 4
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 5
    label "Luigi"
  ]
  node [
    id 6
    label "Vico"
  ]
  node [
    id 7
    label "Equense"
  ]
  node [
    id 8
    label "mistrzostwo"
  ]
  node [
    id 9
    label "&#347;wiat"
  ]
  node [
    id 10
    label "&#8211;"
  ]
  node [
    id 11
    label "lucerna"
  ]
  node [
    id 12
    label "2001"
  ]
  node [
    id 13
    label "Sewilla"
  ]
  node [
    id 14
    label "2002"
  ]
  node [
    id 15
    label "Mediolan"
  ]
  node [
    id 16
    label "2003"
  ]
  node [
    id 17
    label "Gifu"
  ]
  node [
    id 18
    label "2005"
  ]
  node [
    id 19
    label "Eton"
  ]
  node [
    id 20
    label "2006"
  ]
  node [
    id 21
    label "Monachium"
  ]
  node [
    id 22
    label "2007"
  ]
  node [
    id 23
    label "poznanie"
  ]
  node [
    id 24
    label "2009"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
]
