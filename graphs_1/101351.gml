graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.4736842105263157
  density 0.08187134502923976
  graphCliqueNumber 3
  node [
    id 0
    label "micha&#322;"
    origin "text"
  ]
  node [
    id 1
    label "adamczyk"
    origin "text"
  ]
  node [
    id 2
    label "przedmiot"
  ]
  node [
    id 3
    label "Micha&#322;"
  ]
  node [
    id 4
    label "Adamczyk"
  ]
  node [
    id 5
    label "Polonia"
  ]
  node [
    id 6
    label "1"
  ]
  node [
    id 7
    label "fakt"
  ]
  node [
    id 8
    label "p&#243;&#322;noc"
  ]
  node [
    id 9
    label "tv"
  ]
  node [
    id 10
    label "puls"
  ]
  node [
    id 11
    label "TVP"
  ]
  node [
    id 12
    label "na"
  ]
  node [
    id 13
    label "celownik"
  ]
  node [
    id 14
    label "info"
  ]
  node [
    id 15
    label "poranek"
  ]
  node [
    id 16
    label "wyspa"
  ]
  node [
    id 17
    label "strona"
  ]
  node [
    id 18
    label "&#347;wiat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
]
