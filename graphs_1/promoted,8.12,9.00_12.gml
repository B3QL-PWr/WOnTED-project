graph [
  maxDegree 41
  minDegree 1
  meanDegree 1.9692307692307693
  density 0.03076923076923077
  graphCliqueNumber 2
  node [
    id 0
    label "policjant"
    origin "text"
  ]
  node [
    id 1
    label "toru&#324;"
    origin "text"
  ]
  node [
    id 2
    label "niemal"
    origin "text"
  ]
  node [
    id 3
    label "rozje&#380;d&#380;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pieszy"
    origin "text"
  ]
  node [
    id 5
    label "przej&#347;cie"
    origin "text"
  ]
  node [
    id 6
    label "policja"
  ]
  node [
    id 7
    label "blacharz"
  ]
  node [
    id 8
    label "pa&#322;a"
  ]
  node [
    id 9
    label "mundurowy"
  ]
  node [
    id 10
    label "str&#243;&#380;"
  ]
  node [
    id 11
    label "glina"
  ]
  node [
    id 12
    label "mia&#380;d&#380;y&#263;"
  ]
  node [
    id 13
    label "przygotowywa&#263;"
  ]
  node [
    id 14
    label "naciska&#263;"
  ]
  node [
    id 15
    label "naje&#380;d&#380;a&#263;"
  ]
  node [
    id 16
    label "poddawa&#263;"
  ]
  node [
    id 17
    label "uszkadza&#263;"
  ]
  node [
    id 18
    label "narusza&#263;"
  ]
  node [
    id 19
    label "specjalny"
  ]
  node [
    id 20
    label "cz&#322;owiek"
  ]
  node [
    id 21
    label "w&#281;drowiec"
  ]
  node [
    id 22
    label "pieszo"
  ]
  node [
    id 23
    label "piechotny"
  ]
  node [
    id 24
    label "chodnik"
  ]
  node [
    id 25
    label "nale&#380;enie"
  ]
  node [
    id 26
    label "odmienienie"
  ]
  node [
    id 27
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 28
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 29
    label "mini&#281;cie"
  ]
  node [
    id 30
    label "prze&#380;ycie"
  ]
  node [
    id 31
    label "strain"
  ]
  node [
    id 32
    label "przerobienie"
  ]
  node [
    id 33
    label "stanie_si&#281;"
  ]
  node [
    id 34
    label "dostanie_si&#281;"
  ]
  node [
    id 35
    label "wydeptanie"
  ]
  node [
    id 36
    label "wydeptywanie"
  ]
  node [
    id 37
    label "offense"
  ]
  node [
    id 38
    label "wymienienie"
  ]
  node [
    id 39
    label "zacz&#281;cie"
  ]
  node [
    id 40
    label "trwanie"
  ]
  node [
    id 41
    label "przepojenie"
  ]
  node [
    id 42
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 43
    label "zaliczenie"
  ]
  node [
    id 44
    label "zdarzenie_si&#281;"
  ]
  node [
    id 45
    label "uznanie"
  ]
  node [
    id 46
    label "nasycenie_si&#281;"
  ]
  node [
    id 47
    label "przemokni&#281;cie"
  ]
  node [
    id 48
    label "nas&#261;czenie"
  ]
  node [
    id 49
    label "mienie"
  ]
  node [
    id 50
    label "ustawa"
  ]
  node [
    id 51
    label "experience"
  ]
  node [
    id 52
    label "przewy&#380;szenie"
  ]
  node [
    id 53
    label "miejsce"
  ]
  node [
    id 54
    label "faza"
  ]
  node [
    id 55
    label "doznanie"
  ]
  node [
    id 56
    label "przestanie"
  ]
  node [
    id 57
    label "traversal"
  ]
  node [
    id 58
    label "przebycie"
  ]
  node [
    id 59
    label "przedostanie_si&#281;"
  ]
  node [
    id 60
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 61
    label "wstawka"
  ]
  node [
    id 62
    label "przepuszczenie"
  ]
  node [
    id 63
    label "wytyczenie"
  ]
  node [
    id 64
    label "crack"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
]
