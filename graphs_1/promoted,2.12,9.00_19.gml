graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.0357142857142856
  density 0.03701298701298701
  graphCliqueNumber 3
  node [
    id 0
    label "jeden"
    origin "text"
  ]
  node [
    id 1
    label "pewnik"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "niezmienny"
    origin "text"
  ]
  node [
    id 5
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 6
    label "bardziej"
    origin "text"
  ]
  node [
    id 7
    label "niekompetentny"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 9
    label "szansa"
    origin "text"
  ]
  node [
    id 10
    label "wspi&#261;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "wysoce"
    origin "text"
  ]
  node [
    id 13
    label "kieliszek"
  ]
  node [
    id 14
    label "shot"
  ]
  node [
    id 15
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 16
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 17
    label "jaki&#347;"
  ]
  node [
    id 18
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 19
    label "jednolicie"
  ]
  node [
    id 20
    label "w&#243;dka"
  ]
  node [
    id 21
    label "ten"
  ]
  node [
    id 22
    label "ujednolicenie"
  ]
  node [
    id 23
    label "jednakowy"
  ]
  node [
    id 24
    label "aksjomat_Pascha"
  ]
  node [
    id 25
    label "aksjomat_Archimedesa"
  ]
  node [
    id 26
    label "certainty"
  ]
  node [
    id 27
    label "za&#322;o&#380;enie"
  ]
  node [
    id 28
    label "axiom"
  ]
  node [
    id 29
    label "prawda"
  ]
  node [
    id 30
    label "si&#281;ga&#263;"
  ]
  node [
    id 31
    label "trwa&#263;"
  ]
  node [
    id 32
    label "obecno&#347;&#263;"
  ]
  node [
    id 33
    label "stan"
  ]
  node [
    id 34
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 35
    label "stand"
  ]
  node [
    id 36
    label "mie&#263;_miejsce"
  ]
  node [
    id 37
    label "uczestniczy&#263;"
  ]
  node [
    id 38
    label "chodzi&#263;"
  ]
  node [
    id 39
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 40
    label "equal"
  ]
  node [
    id 41
    label "niezmiennie"
  ]
  node [
    id 42
    label "sta&#322;y"
  ]
  node [
    id 43
    label "cz&#322;owiek"
  ]
  node [
    id 44
    label "znaczenie"
  ]
  node [
    id 45
    label "go&#347;&#263;"
  ]
  node [
    id 46
    label "osoba"
  ]
  node [
    id 47
    label "posta&#263;"
  ]
  node [
    id 48
    label "niem&#261;dry"
  ]
  node [
    id 49
    label "kiepski"
  ]
  node [
    id 50
    label "czyj&#347;"
  ]
  node [
    id 51
    label "m&#261;&#380;"
  ]
  node [
    id 52
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 53
    label "wysoki"
  ]
  node [
    id 54
    label "intensywnie"
  ]
  node [
    id 55
    label "wielki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 54
  ]
  edge [
    source 12
    target 55
  ]
]
