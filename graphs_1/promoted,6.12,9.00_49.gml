graph [
  maxDegree 153
  minDegree 1
  meanDegree 2.037037037037037
  density 0.007572628390472257
  graphCliqueNumber 3
  node [
    id 0
    label "silny"
    origin "text"
  ]
  node [
    id 1
    label "podmorski"
    origin "text"
  ]
  node [
    id 2
    label "trz&#281;sienie"
    origin "text"
  ]
  node [
    id 3
    label "ziemia"
    origin "text"
  ]
  node [
    id 4
    label "magnituda"
    origin "text"
  ]
  node [
    id 5
    label "nawiedzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "obszar"
    origin "text"
  ]
  node [
    id 7
    label "wschodni"
    origin "text"
  ]
  node [
    id 8
    label "wybrze&#380;e"
    origin "text"
  ]
  node [
    id 9
    label "nowa"
    origin "text"
  ]
  node [
    id 10
    label "kaledonia"
    origin "text"
  ]
  node [
    id 11
    label "po&#322;udniowy"
    origin "text"
  ]
  node [
    id 12
    label "pacyfik"
    origin "text"
  ]
  node [
    id 13
    label "poinformowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "centrum"
    origin "text"
  ]
  node [
    id 15
    label "ostrzega&#263;"
    origin "text"
  ]
  node [
    id 16
    label "przed"
    origin "text"
  ]
  node [
    id 17
    label "tsunami"
    origin "text"
  ]
  node [
    id 18
    label "ptwc"
    origin "text"
  ]
  node [
    id 19
    label "przekonuj&#261;cy"
  ]
  node [
    id 20
    label "du&#380;y"
  ]
  node [
    id 21
    label "zdecydowany"
  ]
  node [
    id 22
    label "niepodwa&#380;alny"
  ]
  node [
    id 23
    label "wytrzyma&#322;y"
  ]
  node [
    id 24
    label "zdrowy"
  ]
  node [
    id 25
    label "silnie"
  ]
  node [
    id 26
    label "&#380;ywotny"
  ]
  node [
    id 27
    label "konkretny"
  ]
  node [
    id 28
    label "intensywny"
  ]
  node [
    id 29
    label "krzepienie"
  ]
  node [
    id 30
    label "meflochina"
  ]
  node [
    id 31
    label "zajebisty"
  ]
  node [
    id 32
    label "mocno"
  ]
  node [
    id 33
    label "pokrzepienie"
  ]
  node [
    id 34
    label "mocny"
  ]
  node [
    id 35
    label "podwodny"
  ]
  node [
    id 36
    label "ruszanie"
  ]
  node [
    id 37
    label "roztrzepywanie"
  ]
  node [
    id 38
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 39
    label "jolt"
  ]
  node [
    id 40
    label "poruszanie"
  ]
  node [
    id 41
    label "agitation"
  ]
  node [
    id 42
    label "rz&#261;dzenie"
  ]
  node [
    id 43
    label "spin"
  ]
  node [
    id 44
    label "wytrz&#261;&#347;ni&#281;cie"
  ]
  node [
    id 45
    label "roztrz&#261;sanie"
  ]
  node [
    id 46
    label "roztrz&#261;&#347;ni&#281;cie"
  ]
  node [
    id 47
    label "wytrz&#261;sanie"
  ]
  node [
    id 48
    label "sk&#322;&#243;cenie"
  ]
  node [
    id 49
    label "Skandynawia"
  ]
  node [
    id 50
    label "Yorkshire"
  ]
  node [
    id 51
    label "Kaukaz"
  ]
  node [
    id 52
    label "Kaszmir"
  ]
  node [
    id 53
    label "Toskania"
  ]
  node [
    id 54
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 55
    label "&#321;emkowszczyzna"
  ]
  node [
    id 56
    label "Amhara"
  ]
  node [
    id 57
    label "Lombardia"
  ]
  node [
    id 58
    label "Podbeskidzie"
  ]
  node [
    id 59
    label "Kalabria"
  ]
  node [
    id 60
    label "kort"
  ]
  node [
    id 61
    label "Tyrol"
  ]
  node [
    id 62
    label "Pamir"
  ]
  node [
    id 63
    label "Lubelszczyzna"
  ]
  node [
    id 64
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 65
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 66
    label "&#379;ywiecczyzna"
  ]
  node [
    id 67
    label "ryzosfera"
  ]
  node [
    id 68
    label "Europa_Wschodnia"
  ]
  node [
    id 69
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 70
    label "Zabajkale"
  ]
  node [
    id 71
    label "Kaszuby"
  ]
  node [
    id 72
    label "Bo&#347;nia"
  ]
  node [
    id 73
    label "Noworosja"
  ]
  node [
    id 74
    label "Ba&#322;kany"
  ]
  node [
    id 75
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 76
    label "Anglia"
  ]
  node [
    id 77
    label "Kielecczyzna"
  ]
  node [
    id 78
    label "Pomorze_Zachodnie"
  ]
  node [
    id 79
    label "Opolskie"
  ]
  node [
    id 80
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 81
    label "skorupa_ziemska"
  ]
  node [
    id 82
    label "Ko&#322;yma"
  ]
  node [
    id 83
    label "Oksytania"
  ]
  node [
    id 84
    label "Syjon"
  ]
  node [
    id 85
    label "posadzka"
  ]
  node [
    id 86
    label "pa&#324;stwo"
  ]
  node [
    id 87
    label "Kociewie"
  ]
  node [
    id 88
    label "Huculszczyzna"
  ]
  node [
    id 89
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 90
    label "budynek"
  ]
  node [
    id 91
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 92
    label "Bawaria"
  ]
  node [
    id 93
    label "pomieszczenie"
  ]
  node [
    id 94
    label "pr&#243;chnica"
  ]
  node [
    id 95
    label "glinowanie"
  ]
  node [
    id 96
    label "Maghreb"
  ]
  node [
    id 97
    label "Bory_Tucholskie"
  ]
  node [
    id 98
    label "Europa_Zachodnia"
  ]
  node [
    id 99
    label "Kerala"
  ]
  node [
    id 100
    label "Podhale"
  ]
  node [
    id 101
    label "Kabylia"
  ]
  node [
    id 102
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 103
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 104
    label "Ma&#322;opolska"
  ]
  node [
    id 105
    label "Polesie"
  ]
  node [
    id 106
    label "Liguria"
  ]
  node [
    id 107
    label "&#321;&#243;dzkie"
  ]
  node [
    id 108
    label "geosystem"
  ]
  node [
    id 109
    label "Palestyna"
  ]
  node [
    id 110
    label "Bojkowszczyzna"
  ]
  node [
    id 111
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 112
    label "Karaiby"
  ]
  node [
    id 113
    label "S&#261;decczyzna"
  ]
  node [
    id 114
    label "Sand&#380;ak"
  ]
  node [
    id 115
    label "Nadrenia"
  ]
  node [
    id 116
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 117
    label "Zakarpacie"
  ]
  node [
    id 118
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 119
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 120
    label "Zag&#243;rze"
  ]
  node [
    id 121
    label "Andaluzja"
  ]
  node [
    id 122
    label "Turkiestan"
  ]
  node [
    id 123
    label "Naddniestrze"
  ]
  node [
    id 124
    label "Hercegowina"
  ]
  node [
    id 125
    label "p&#322;aszczyzna"
  ]
  node [
    id 126
    label "Opolszczyzna"
  ]
  node [
    id 127
    label "jednostka_administracyjna"
  ]
  node [
    id 128
    label "Lotaryngia"
  ]
  node [
    id 129
    label "Afryka_Wschodnia"
  ]
  node [
    id 130
    label "Szlezwik"
  ]
  node [
    id 131
    label "powierzchnia"
  ]
  node [
    id 132
    label "glinowa&#263;"
  ]
  node [
    id 133
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 134
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 135
    label "podglebie"
  ]
  node [
    id 136
    label "Mazowsze"
  ]
  node [
    id 137
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 138
    label "teren"
  ]
  node [
    id 139
    label "Afryka_Zachodnia"
  ]
  node [
    id 140
    label "czynnik_produkcji"
  ]
  node [
    id 141
    label "Galicja"
  ]
  node [
    id 142
    label "Szkocja"
  ]
  node [
    id 143
    label "Walia"
  ]
  node [
    id 144
    label "Powi&#347;le"
  ]
  node [
    id 145
    label "penetrator"
  ]
  node [
    id 146
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 147
    label "kompleks_sorpcyjny"
  ]
  node [
    id 148
    label "Zamojszczyzna"
  ]
  node [
    id 149
    label "Kujawy"
  ]
  node [
    id 150
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 151
    label "Podlasie"
  ]
  node [
    id 152
    label "Laponia"
  ]
  node [
    id 153
    label "Umbria"
  ]
  node [
    id 154
    label "plantowa&#263;"
  ]
  node [
    id 155
    label "Mezoameryka"
  ]
  node [
    id 156
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 157
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 158
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 159
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 160
    label "Kurdystan"
  ]
  node [
    id 161
    label "Kampania"
  ]
  node [
    id 162
    label "Armagnac"
  ]
  node [
    id 163
    label "Polinezja"
  ]
  node [
    id 164
    label "Warmia"
  ]
  node [
    id 165
    label "Wielkopolska"
  ]
  node [
    id 166
    label "litosfera"
  ]
  node [
    id 167
    label "Bordeaux"
  ]
  node [
    id 168
    label "Lauda"
  ]
  node [
    id 169
    label "Mazury"
  ]
  node [
    id 170
    label "Podkarpacie"
  ]
  node [
    id 171
    label "Oceania"
  ]
  node [
    id 172
    label "Lasko"
  ]
  node [
    id 173
    label "Amazonia"
  ]
  node [
    id 174
    label "pojazd"
  ]
  node [
    id 175
    label "glej"
  ]
  node [
    id 176
    label "martwica"
  ]
  node [
    id 177
    label "zapadnia"
  ]
  node [
    id 178
    label "przestrze&#324;"
  ]
  node [
    id 179
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 180
    label "dotleni&#263;"
  ]
  node [
    id 181
    label "Kurpie"
  ]
  node [
    id 182
    label "Tonkin"
  ]
  node [
    id 183
    label "Azja_Wschodnia"
  ]
  node [
    id 184
    label "Mikronezja"
  ]
  node [
    id 185
    label "Ukraina_Zachodnia"
  ]
  node [
    id 186
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 187
    label "Turyngia"
  ]
  node [
    id 188
    label "Baszkiria"
  ]
  node [
    id 189
    label "Apulia"
  ]
  node [
    id 190
    label "miejsce"
  ]
  node [
    id 191
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 192
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 193
    label "Indochiny"
  ]
  node [
    id 194
    label "Biskupizna"
  ]
  node [
    id 195
    label "Lubuskie"
  ]
  node [
    id 196
    label "domain"
  ]
  node [
    id 197
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 198
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 199
    label "moment_magnitude_scale"
  ]
  node [
    id 200
    label "trz&#281;sienie_ziemi"
  ]
  node [
    id 201
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 202
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 203
    label "visit"
  ]
  node [
    id 204
    label "spotka&#263;"
  ]
  node [
    id 205
    label "odwiedzi&#263;"
  ]
  node [
    id 206
    label "ogarn&#261;&#263;"
  ]
  node [
    id 207
    label "afflict"
  ]
  node [
    id 208
    label "inflict"
  ]
  node [
    id 209
    label "pas_planetoid"
  ]
  node [
    id 210
    label "wsch&#243;d"
  ]
  node [
    id 211
    label "Neogea"
  ]
  node [
    id 212
    label "holarktyka"
  ]
  node [
    id 213
    label "Rakowice"
  ]
  node [
    id 214
    label "Kosowo"
  ]
  node [
    id 215
    label "Syberia_Wschodnia"
  ]
  node [
    id 216
    label "wymiar"
  ]
  node [
    id 217
    label "p&#243;&#322;noc"
  ]
  node [
    id 218
    label "akrecja"
  ]
  node [
    id 219
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 220
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 221
    label "terytorium"
  ]
  node [
    id 222
    label "antroposfera"
  ]
  node [
    id 223
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 224
    label "po&#322;udnie"
  ]
  node [
    id 225
    label "zach&#243;d"
  ]
  node [
    id 226
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 227
    label "Olszanica"
  ]
  node [
    id 228
    label "Syberia_Zachodnia"
  ]
  node [
    id 229
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 230
    label "Ruda_Pabianicka"
  ]
  node [
    id 231
    label "Notogea"
  ]
  node [
    id 232
    label "&#321;&#281;g"
  ]
  node [
    id 233
    label "Antarktyka"
  ]
  node [
    id 234
    label "Piotrowo"
  ]
  node [
    id 235
    label "Zab&#322;ocie"
  ]
  node [
    id 236
    label "zakres"
  ]
  node [
    id 237
    label "Pow&#261;zki"
  ]
  node [
    id 238
    label "Arktyka"
  ]
  node [
    id 239
    label "zbi&#243;r"
  ]
  node [
    id 240
    label "Ludwin&#243;w"
  ]
  node [
    id 241
    label "Zabu&#380;e"
  ]
  node [
    id 242
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 243
    label "Kresy_Zachodnie"
  ]
  node [
    id 244
    label "syrniki"
  ]
  node [
    id 245
    label "linia"
  ]
  node [
    id 246
    label "ekoton"
  ]
  node [
    id 247
    label "str&#261;d"
  ]
  node [
    id 248
    label "woda"
  ]
  node [
    id 249
    label "gwiazda"
  ]
  node [
    id 250
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 251
    label "gor&#261;cy"
  ]
  node [
    id 252
    label "s&#322;oneczny"
  ]
  node [
    id 253
    label "po&#322;udniowo"
  ]
  node [
    id 254
    label "zakomunikowa&#263;"
  ]
  node [
    id 255
    label "inform"
  ]
  node [
    id 256
    label "centroprawica"
  ]
  node [
    id 257
    label "core"
  ]
  node [
    id 258
    label "Hollywood"
  ]
  node [
    id 259
    label "centrolew"
  ]
  node [
    id 260
    label "blok"
  ]
  node [
    id 261
    label "sejm"
  ]
  node [
    id 262
    label "punkt"
  ]
  node [
    id 263
    label "o&#347;rodek"
  ]
  node [
    id 264
    label "caution"
  ]
  node [
    id 265
    label "uprzedza&#263;"
  ]
  node [
    id 266
    label "fala_morska"
  ]
  node [
    id 267
    label "kl&#281;ska_&#380;ywio&#322;owa"
  ]
  node [
    id 268
    label "Kaledonia"
  ]
  node [
    id 269
    label "Pacyfik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 255
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 256
  ]
  edge [
    source 14
    target 257
  ]
  edge [
    source 14
    target 258
  ]
  edge [
    source 14
    target 259
  ]
  edge [
    source 14
    target 260
  ]
  edge [
    source 14
    target 261
  ]
  edge [
    source 14
    target 262
  ]
  edge [
    source 14
    target 263
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 264
  ]
  edge [
    source 15
    target 265
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 266
  ]
  edge [
    source 17
    target 267
  ]
]
