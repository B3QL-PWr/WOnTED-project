graph [
  maxDegree 46
  minDegree 1
  meanDegree 1.9858156028368794
  density 0.014184397163120567
  graphCliqueNumber 2
  node [
    id 0
    label "wiele"
    origin "text"
  ]
  node [
    id 1
    label "osoba"
    origin "text"
  ]
  node [
    id 2
    label "zna&#263;"
    origin "text"
  ]
  node [
    id 3
    label "php"
    origin "text"
  ]
  node [
    id 4
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 6
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zdawa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "siebie"
    origin "text"
  ]
  node [
    id 9
    label "sprawa"
    origin "text"
  ]
  node [
    id 10
    label "jeszcze"
    origin "text"
  ]
  node [
    id 11
    label "potrafi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "ten"
    origin "text"
  ]
  node [
    id 13
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 14
    label "wiela"
  ]
  node [
    id 15
    label "du&#380;y"
  ]
  node [
    id 16
    label "Zgredek"
  ]
  node [
    id 17
    label "kategoria_gramatyczna"
  ]
  node [
    id 18
    label "Casanova"
  ]
  node [
    id 19
    label "Don_Juan"
  ]
  node [
    id 20
    label "Gargantua"
  ]
  node [
    id 21
    label "Faust"
  ]
  node [
    id 22
    label "profanum"
  ]
  node [
    id 23
    label "Chocho&#322;"
  ]
  node [
    id 24
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 25
    label "koniugacja"
  ]
  node [
    id 26
    label "Winnetou"
  ]
  node [
    id 27
    label "Dwukwiat"
  ]
  node [
    id 28
    label "homo_sapiens"
  ]
  node [
    id 29
    label "Edyp"
  ]
  node [
    id 30
    label "Herkules_Poirot"
  ]
  node [
    id 31
    label "ludzko&#347;&#263;"
  ]
  node [
    id 32
    label "mikrokosmos"
  ]
  node [
    id 33
    label "person"
  ]
  node [
    id 34
    label "Sherlock_Holmes"
  ]
  node [
    id 35
    label "portrecista"
  ]
  node [
    id 36
    label "Szwejk"
  ]
  node [
    id 37
    label "Hamlet"
  ]
  node [
    id 38
    label "duch"
  ]
  node [
    id 39
    label "g&#322;owa"
  ]
  node [
    id 40
    label "oddzia&#322;ywanie"
  ]
  node [
    id 41
    label "Quasimodo"
  ]
  node [
    id 42
    label "Dulcynea"
  ]
  node [
    id 43
    label "Don_Kiszot"
  ]
  node [
    id 44
    label "Wallenrod"
  ]
  node [
    id 45
    label "Plastu&#347;"
  ]
  node [
    id 46
    label "Harry_Potter"
  ]
  node [
    id 47
    label "figura"
  ]
  node [
    id 48
    label "parali&#380;owa&#263;"
  ]
  node [
    id 49
    label "istota"
  ]
  node [
    id 50
    label "Werter"
  ]
  node [
    id 51
    label "antropochoria"
  ]
  node [
    id 52
    label "posta&#263;"
  ]
  node [
    id 53
    label "wiedzie&#263;"
  ]
  node [
    id 54
    label "cognizance"
  ]
  node [
    id 55
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 56
    label "cz&#281;sty"
  ]
  node [
    id 57
    label "use"
  ]
  node [
    id 58
    label "krzywdzi&#263;"
  ]
  node [
    id 59
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 60
    label "distribute"
  ]
  node [
    id 61
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 62
    label "give"
  ]
  node [
    id 63
    label "liga&#263;"
  ]
  node [
    id 64
    label "u&#380;ywa&#263;"
  ]
  node [
    id 65
    label "korzysta&#263;"
  ]
  node [
    id 66
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 67
    label "zalicza&#263;"
  ]
  node [
    id 68
    label "opowiada&#263;"
  ]
  node [
    id 69
    label "render"
  ]
  node [
    id 70
    label "impart"
  ]
  node [
    id 71
    label "oddawa&#263;"
  ]
  node [
    id 72
    label "zostawia&#263;"
  ]
  node [
    id 73
    label "sk&#322;ada&#263;"
  ]
  node [
    id 74
    label "convey"
  ]
  node [
    id 75
    label "powierza&#263;"
  ]
  node [
    id 76
    label "bequeath"
  ]
  node [
    id 77
    label "temat"
  ]
  node [
    id 78
    label "kognicja"
  ]
  node [
    id 79
    label "idea"
  ]
  node [
    id 80
    label "szczeg&#243;&#322;"
  ]
  node [
    id 81
    label "rzecz"
  ]
  node [
    id 82
    label "wydarzenie"
  ]
  node [
    id 83
    label "przes&#322;anka"
  ]
  node [
    id 84
    label "rozprawa"
  ]
  node [
    id 85
    label "object"
  ]
  node [
    id 86
    label "proposition"
  ]
  node [
    id 87
    label "ci&#261;gle"
  ]
  node [
    id 88
    label "umie&#263;"
  ]
  node [
    id 89
    label "cope"
  ]
  node [
    id 90
    label "potrafia&#263;"
  ]
  node [
    id 91
    label "posuwa&#263;_si&#281;"
  ]
  node [
    id 92
    label "posun&#261;&#263;_si&#281;"
  ]
  node [
    id 93
    label "can"
  ]
  node [
    id 94
    label "okre&#347;lony"
  ]
  node [
    id 95
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 96
    label "pisa&#263;"
  ]
  node [
    id 97
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 98
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 99
    label "ssanie"
  ]
  node [
    id 100
    label "po_koroniarsku"
  ]
  node [
    id 101
    label "przedmiot"
  ]
  node [
    id 102
    label "but"
  ]
  node [
    id 103
    label "m&#243;wienie"
  ]
  node [
    id 104
    label "rozumie&#263;"
  ]
  node [
    id 105
    label "formacja_geologiczna"
  ]
  node [
    id 106
    label "rozumienie"
  ]
  node [
    id 107
    label "m&#243;wi&#263;"
  ]
  node [
    id 108
    label "gramatyka"
  ]
  node [
    id 109
    label "pype&#263;"
  ]
  node [
    id 110
    label "makroglosja"
  ]
  node [
    id 111
    label "kawa&#322;ek"
  ]
  node [
    id 112
    label "artykulator"
  ]
  node [
    id 113
    label "kultura_duchowa"
  ]
  node [
    id 114
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 115
    label "jama_ustna"
  ]
  node [
    id 116
    label "spos&#243;b"
  ]
  node [
    id 117
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 118
    label "przet&#322;umaczenie"
  ]
  node [
    id 119
    label "t&#322;umaczenie"
  ]
  node [
    id 120
    label "language"
  ]
  node [
    id 121
    label "jeniec"
  ]
  node [
    id 122
    label "organ"
  ]
  node [
    id 123
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 124
    label "pismo"
  ]
  node [
    id 125
    label "formalizowanie"
  ]
  node [
    id 126
    label "fonetyka"
  ]
  node [
    id 127
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 128
    label "wokalizm"
  ]
  node [
    id 129
    label "liza&#263;"
  ]
  node [
    id 130
    label "s&#322;ownictwo"
  ]
  node [
    id 131
    label "napisa&#263;"
  ]
  node [
    id 132
    label "formalizowa&#263;"
  ]
  node [
    id 133
    label "natural_language"
  ]
  node [
    id 134
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 135
    label "stylik"
  ]
  node [
    id 136
    label "konsonantyzm"
  ]
  node [
    id 137
    label "urz&#261;dzenie"
  ]
  node [
    id 138
    label "ssa&#263;"
  ]
  node [
    id 139
    label "kod"
  ]
  node [
    id 140
    label "lizanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
]
