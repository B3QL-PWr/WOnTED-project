graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 1
    label "dziadek"
    origin "text"
  ]
  node [
    id 2
    label "mai&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 4
    label "lato"
    origin "text"
  ]
  node [
    id 5
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "smartfon"
    origin "text"
  ]
  node [
    id 8
    label "czyj&#347;"
  ]
  node [
    id 9
    label "m&#261;&#380;"
  ]
  node [
    id 10
    label "partner"
  ]
  node [
    id 11
    label "bryd&#380;ysta"
  ]
  node [
    id 12
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 13
    label "dziadyga"
  ]
  node [
    id 14
    label "starszyzna"
  ]
  node [
    id 15
    label "dziadowina"
  ]
  node [
    id 16
    label "rada_starc&#243;w"
  ]
  node [
    id 17
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 18
    label "dziad"
  ]
  node [
    id 19
    label "przodek"
  ]
  node [
    id 20
    label "dziadkowie"
  ]
  node [
    id 21
    label "posta&#263;"
  ]
  node [
    id 22
    label "czwarty"
  ]
  node [
    id 23
    label "ozdabia&#263;"
  ]
  node [
    id 24
    label "pora_roku"
  ]
  node [
    id 25
    label "sta&#263;_si&#281;"
  ]
  node [
    id 26
    label "zrobi&#263;"
  ]
  node [
    id 27
    label "podj&#261;&#263;"
  ]
  node [
    id 28
    label "determine"
  ]
  node [
    id 29
    label "czu&#263;"
  ]
  node [
    id 30
    label "desire"
  ]
  node [
    id 31
    label "kcie&#263;"
  ]
  node [
    id 32
    label "kom&#243;rka"
  ]
  node [
    id 33
    label "ekran_dotykowy"
  ]
  node [
    id 34
    label "komputer_przeno&#347;ny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
]
