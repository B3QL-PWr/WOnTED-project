graph [
  maxDegree 31
  minDegree 1
  meanDegree 1.9753086419753085
  density 0.024691358024691357
  graphCliqueNumber 2
  node [
    id 0
    label "napastnik"
    origin "text"
  ]
  node [
    id 1
    label "kilka"
    origin "text"
  ]
  node [
    id 2
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "temu"
    origin "text"
  ]
  node [
    id 4
    label "wyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wi&#281;zienie"
    origin "text"
  ]
  node [
    id 6
    label "Messi"
  ]
  node [
    id 7
    label "atak"
  ]
  node [
    id 8
    label "napadzior"
  ]
  node [
    id 9
    label "gracz"
  ]
  node [
    id 10
    label "z&#322;oczy&#324;ca"
  ]
  node [
    id 11
    label "zawodnik"
  ]
  node [
    id 12
    label "&#347;ledziowate"
  ]
  node [
    id 13
    label "ryba"
  ]
  node [
    id 14
    label "s&#322;o&#324;ce"
  ]
  node [
    id 15
    label "czynienie_si&#281;"
  ]
  node [
    id 16
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 17
    label "czas"
  ]
  node [
    id 18
    label "long_time"
  ]
  node [
    id 19
    label "przedpo&#322;udnie"
  ]
  node [
    id 20
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 21
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 22
    label "tydzie&#324;"
  ]
  node [
    id 23
    label "godzina"
  ]
  node [
    id 24
    label "t&#322;usty_czwartek"
  ]
  node [
    id 25
    label "wsta&#263;"
  ]
  node [
    id 26
    label "day"
  ]
  node [
    id 27
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 28
    label "przedwiecz&#243;r"
  ]
  node [
    id 29
    label "Sylwester"
  ]
  node [
    id 30
    label "po&#322;udnie"
  ]
  node [
    id 31
    label "wzej&#347;cie"
  ]
  node [
    id 32
    label "podwiecz&#243;r"
  ]
  node [
    id 33
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 34
    label "rano"
  ]
  node [
    id 35
    label "termin"
  ]
  node [
    id 36
    label "ranek"
  ]
  node [
    id 37
    label "doba"
  ]
  node [
    id 38
    label "wiecz&#243;r"
  ]
  node [
    id 39
    label "walentynki"
  ]
  node [
    id 40
    label "popo&#322;udnie"
  ]
  node [
    id 41
    label "noc"
  ]
  node [
    id 42
    label "wstanie"
  ]
  node [
    id 43
    label "get"
  ]
  node [
    id 44
    label "opu&#347;ci&#263;"
  ]
  node [
    id 45
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 46
    label "zej&#347;&#263;"
  ]
  node [
    id 47
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 48
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 49
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 50
    label "sko&#324;czy&#263;"
  ]
  node [
    id 51
    label "ograniczenie"
  ]
  node [
    id 52
    label "ruszy&#263;"
  ]
  node [
    id 53
    label "wypa&#347;&#263;"
  ]
  node [
    id 54
    label "uko&#324;czy&#263;"
  ]
  node [
    id 55
    label "open"
  ]
  node [
    id 56
    label "moderate"
  ]
  node [
    id 57
    label "uzyska&#263;"
  ]
  node [
    id 58
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 59
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 60
    label "mount"
  ]
  node [
    id 61
    label "leave"
  ]
  node [
    id 62
    label "drive"
  ]
  node [
    id 63
    label "zagra&#263;"
  ]
  node [
    id 64
    label "zademonstrowa&#263;"
  ]
  node [
    id 65
    label "wystarczy&#263;"
  ]
  node [
    id 66
    label "perform"
  ]
  node [
    id 67
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 68
    label "drop"
  ]
  node [
    id 69
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 70
    label "ogranicza&#263;"
  ]
  node [
    id 71
    label "uniemo&#380;liwianie"
  ]
  node [
    id 72
    label "Butyrki"
  ]
  node [
    id 73
    label "ciupa"
  ]
  node [
    id 74
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 75
    label "reedukator"
  ]
  node [
    id 76
    label "miejsce_odosobnienia"
  ]
  node [
    id 77
    label "&#321;ubianka"
  ]
  node [
    id 78
    label "pierdel"
  ]
  node [
    id 79
    label "imprisonment"
  ]
  node [
    id 80
    label "sytuacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
]
