graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.126126126126126
  density 0.009620480208715502
  graphCliqueNumber 3
  node [
    id 0
    label "pewne"
    origin "text"
  ]
  node [
    id 1
    label "moment"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "rodzinny"
    origin "text"
  ]
  node [
    id 4
    label "wakacje"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "sko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 8
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 9
    label "kiedy"
    origin "text"
  ]
  node [
    id 10
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "potrafi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "wskaza&#263;"
    origin "text"
  ]
  node [
    id 13
    label "konkretny"
    origin "text"
  ]
  node [
    id 14
    label "punkt"
    origin "text"
  ]
  node [
    id 15
    label "ale"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "przekonany"
    origin "text"
  ]
  node [
    id 18
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 19
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 20
    label "rodz&#261;ca"
    origin "text"
  ]
  node [
    id 21
    label "przez"
    origin "text"
  ]
  node [
    id 22
    label "lata"
    origin "text"
  ]
  node [
    id 23
    label "niezale&#380;no&#347;&#263;"
    origin "text"
  ]
  node [
    id 24
    label "ten"
    origin "text"
  ]
  node [
    id 25
    label "rodzic"
    origin "text"
  ]
  node [
    id 26
    label "okres_czasu"
  ]
  node [
    id 27
    label "minute"
  ]
  node [
    id 28
    label "jednostka_geologiczna"
  ]
  node [
    id 29
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 30
    label "time"
  ]
  node [
    id 31
    label "chron"
  ]
  node [
    id 32
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 33
    label "fragment"
  ]
  node [
    id 34
    label "czasokres"
  ]
  node [
    id 35
    label "trawienie"
  ]
  node [
    id 36
    label "kategoria_gramatyczna"
  ]
  node [
    id 37
    label "period"
  ]
  node [
    id 38
    label "odczyt"
  ]
  node [
    id 39
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 40
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 41
    label "chwila"
  ]
  node [
    id 42
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 43
    label "poprzedzenie"
  ]
  node [
    id 44
    label "koniugacja"
  ]
  node [
    id 45
    label "dzieje"
  ]
  node [
    id 46
    label "poprzedzi&#263;"
  ]
  node [
    id 47
    label "przep&#322;ywanie"
  ]
  node [
    id 48
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 49
    label "odwlekanie_si&#281;"
  ]
  node [
    id 50
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 51
    label "Zeitgeist"
  ]
  node [
    id 52
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 53
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 54
    label "pochodzi&#263;"
  ]
  node [
    id 55
    label "schy&#322;ek"
  ]
  node [
    id 56
    label "czwarty_wymiar"
  ]
  node [
    id 57
    label "chronometria"
  ]
  node [
    id 58
    label "poprzedzanie"
  ]
  node [
    id 59
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 60
    label "pogoda"
  ]
  node [
    id 61
    label "zegar"
  ]
  node [
    id 62
    label "trawi&#263;"
  ]
  node [
    id 63
    label "pochodzenie"
  ]
  node [
    id 64
    label "poprzedza&#263;"
  ]
  node [
    id 65
    label "time_period"
  ]
  node [
    id 66
    label "rachuba_czasu"
  ]
  node [
    id 67
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 68
    label "czasoprzestrze&#324;"
  ]
  node [
    id 69
    label "laba"
  ]
  node [
    id 70
    label "familijnie"
  ]
  node [
    id 71
    label "rodzinnie"
  ]
  node [
    id 72
    label "przyjemny"
  ]
  node [
    id 73
    label "wsp&#243;lny"
  ]
  node [
    id 74
    label "charakterystyczny"
  ]
  node [
    id 75
    label "swobodny"
  ]
  node [
    id 76
    label "zwi&#261;zany"
  ]
  node [
    id 77
    label "towarzyski"
  ]
  node [
    id 78
    label "ciep&#322;y"
  ]
  node [
    id 79
    label "urlop"
  ]
  node [
    id 80
    label "czas_wolny"
  ]
  node [
    id 81
    label "rok_akademicki"
  ]
  node [
    id 82
    label "rok_szkolny"
  ]
  node [
    id 83
    label "communicate"
  ]
  node [
    id 84
    label "zako&#324;czy&#263;"
  ]
  node [
    id 85
    label "przesta&#263;"
  ]
  node [
    id 86
    label "end"
  ]
  node [
    id 87
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 88
    label "zrobi&#263;"
  ]
  node [
    id 89
    label "doba"
  ]
  node [
    id 90
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 91
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 92
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 93
    label "si&#281;ga&#263;"
  ]
  node [
    id 94
    label "zna&#263;"
  ]
  node [
    id 95
    label "troska&#263;_si&#281;"
  ]
  node [
    id 96
    label "zachowywa&#263;"
  ]
  node [
    id 97
    label "chowa&#263;"
  ]
  node [
    id 98
    label "think"
  ]
  node [
    id 99
    label "pilnowa&#263;"
  ]
  node [
    id 100
    label "robi&#263;"
  ]
  node [
    id 101
    label "recall"
  ]
  node [
    id 102
    label "echo"
  ]
  node [
    id 103
    label "take_care"
  ]
  node [
    id 104
    label "pozostawa&#263;"
  ]
  node [
    id 105
    label "trwa&#263;"
  ]
  node [
    id 106
    label "wystarcza&#263;"
  ]
  node [
    id 107
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 108
    label "czeka&#263;"
  ]
  node [
    id 109
    label "stand"
  ]
  node [
    id 110
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 111
    label "mieszka&#263;"
  ]
  node [
    id 112
    label "wystarczy&#263;"
  ]
  node [
    id 113
    label "sprawowa&#263;"
  ]
  node [
    id 114
    label "przebywa&#263;"
  ]
  node [
    id 115
    label "kosztowa&#263;"
  ]
  node [
    id 116
    label "undertaking"
  ]
  node [
    id 117
    label "wystawa&#263;"
  ]
  node [
    id 118
    label "base"
  ]
  node [
    id 119
    label "digest"
  ]
  node [
    id 120
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 121
    label "umie&#263;"
  ]
  node [
    id 122
    label "cope"
  ]
  node [
    id 123
    label "potrafia&#263;"
  ]
  node [
    id 124
    label "posuwa&#263;_si&#281;"
  ]
  node [
    id 125
    label "posun&#261;&#263;_si&#281;"
  ]
  node [
    id 126
    label "can"
  ]
  node [
    id 127
    label "aim"
  ]
  node [
    id 128
    label "pokaza&#263;"
  ]
  node [
    id 129
    label "podkre&#347;li&#263;"
  ]
  node [
    id 130
    label "wybra&#263;"
  ]
  node [
    id 131
    label "indicate"
  ]
  node [
    id 132
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 133
    label "poda&#263;"
  ]
  node [
    id 134
    label "picture"
  ]
  node [
    id 135
    label "point"
  ]
  node [
    id 136
    label "tre&#347;ciwy"
  ]
  node [
    id 137
    label "&#322;adny"
  ]
  node [
    id 138
    label "jasny"
  ]
  node [
    id 139
    label "okre&#347;lony"
  ]
  node [
    id 140
    label "ogarni&#281;ty"
  ]
  node [
    id 141
    label "jaki&#347;"
  ]
  node [
    id 142
    label "po&#380;ywny"
  ]
  node [
    id 143
    label "skupiony"
  ]
  node [
    id 144
    label "konkretnie"
  ]
  node [
    id 145
    label "posilny"
  ]
  node [
    id 146
    label "abstrakcyjny"
  ]
  node [
    id 147
    label "solidnie"
  ]
  node [
    id 148
    label "niez&#322;y"
  ]
  node [
    id 149
    label "prosta"
  ]
  node [
    id 150
    label "po&#322;o&#380;enie"
  ]
  node [
    id 151
    label "ust&#281;p"
  ]
  node [
    id 152
    label "problemat"
  ]
  node [
    id 153
    label "kres"
  ]
  node [
    id 154
    label "mark"
  ]
  node [
    id 155
    label "pozycja"
  ]
  node [
    id 156
    label "stopie&#324;_pisma"
  ]
  node [
    id 157
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 158
    label "przestrze&#324;"
  ]
  node [
    id 159
    label "wojsko"
  ]
  node [
    id 160
    label "problematyka"
  ]
  node [
    id 161
    label "zapunktowa&#263;"
  ]
  node [
    id 162
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 163
    label "obiekt_matematyczny"
  ]
  node [
    id 164
    label "sprawa"
  ]
  node [
    id 165
    label "plamka"
  ]
  node [
    id 166
    label "miejsce"
  ]
  node [
    id 167
    label "obiekt"
  ]
  node [
    id 168
    label "plan"
  ]
  node [
    id 169
    label "podpunkt"
  ]
  node [
    id 170
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 171
    label "jednostka"
  ]
  node [
    id 172
    label "piwo"
  ]
  node [
    id 173
    label "obecno&#347;&#263;"
  ]
  node [
    id 174
    label "stan"
  ]
  node [
    id 175
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 176
    label "mie&#263;_miejsce"
  ]
  node [
    id 177
    label "uczestniczy&#263;"
  ]
  node [
    id 178
    label "chodzi&#263;"
  ]
  node [
    id 179
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 180
    label "equal"
  ]
  node [
    id 181
    label "upewnienie_si&#281;"
  ]
  node [
    id 182
    label "ufanie"
  ]
  node [
    id 183
    label "wierzenie"
  ]
  node [
    id 184
    label "upewnianie_si&#281;"
  ]
  node [
    id 185
    label "czu&#263;"
  ]
  node [
    id 186
    label "need"
  ]
  node [
    id 187
    label "hide"
  ]
  node [
    id 188
    label "support"
  ]
  node [
    id 189
    label "odwodnienie"
  ]
  node [
    id 190
    label "konstytucja"
  ]
  node [
    id 191
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 192
    label "substancja_chemiczna"
  ]
  node [
    id 193
    label "bratnia_dusza"
  ]
  node [
    id 194
    label "zwi&#261;zanie"
  ]
  node [
    id 195
    label "lokant"
  ]
  node [
    id 196
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 197
    label "zwi&#261;za&#263;"
  ]
  node [
    id 198
    label "organizacja"
  ]
  node [
    id 199
    label "odwadnia&#263;"
  ]
  node [
    id 200
    label "marriage"
  ]
  node [
    id 201
    label "marketing_afiliacyjny"
  ]
  node [
    id 202
    label "bearing"
  ]
  node [
    id 203
    label "wi&#261;zanie"
  ]
  node [
    id 204
    label "odwadnianie"
  ]
  node [
    id 205
    label "koligacja"
  ]
  node [
    id 206
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 207
    label "odwodni&#263;"
  ]
  node [
    id 208
    label "azeotrop"
  ]
  node [
    id 209
    label "powi&#261;zanie"
  ]
  node [
    id 210
    label "summer"
  ]
  node [
    id 211
    label "status"
  ]
  node [
    id 212
    label "relacja"
  ]
  node [
    id 213
    label "independence"
  ]
  node [
    id 214
    label "charakter"
  ]
  node [
    id 215
    label "brak"
  ]
  node [
    id 216
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 217
    label "opiekun"
  ]
  node [
    id 218
    label "wapniak"
  ]
  node [
    id 219
    label "rodzic_chrzestny"
  ]
  node [
    id 220
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 221
    label "rodzice"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 41
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 211
  ]
  edge [
    source 23
    target 212
  ]
  edge [
    source 23
    target 213
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 139
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 221
  ]
]
