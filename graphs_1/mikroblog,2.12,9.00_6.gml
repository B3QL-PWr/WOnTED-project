graph [
  maxDegree 28
  minDegree 1
  meanDegree 2
  density 0.047619047619047616
  graphCliqueNumber 2
  node [
    id 0
    label "zostawi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "like"
    origin "text"
  ]
  node [
    id 2
    label "wys&#322;a&#263;"
    origin "text"
  ]
  node [
    id 3
    label "linek"
    origin "text"
  ]
  node [
    id 4
    label "ksw"
    origin "text"
  ]
  node [
    id 5
    label "skrzywdzi&#263;"
  ]
  node [
    id 6
    label "impart"
  ]
  node [
    id 7
    label "liszy&#263;"
  ]
  node [
    id 8
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 9
    label "doprowadzi&#263;"
  ]
  node [
    id 10
    label "da&#263;"
  ]
  node [
    id 11
    label "zachowa&#263;"
  ]
  node [
    id 12
    label "stworzy&#263;"
  ]
  node [
    id 13
    label "overhaul"
  ]
  node [
    id 14
    label "permit"
  ]
  node [
    id 15
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 16
    label "przekaza&#263;"
  ]
  node [
    id 17
    label "wyda&#263;"
  ]
  node [
    id 18
    label "wyznaczy&#263;"
  ]
  node [
    id 19
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 20
    label "zrobi&#263;"
  ]
  node [
    id 21
    label "zerwa&#263;"
  ]
  node [
    id 22
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 23
    label "zaplanowa&#263;"
  ]
  node [
    id 24
    label "zabra&#263;"
  ]
  node [
    id 25
    label "shove"
  ]
  node [
    id 26
    label "spowodowa&#263;"
  ]
  node [
    id 27
    label "zrezygnowa&#263;"
  ]
  node [
    id 28
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 29
    label "drop"
  ]
  node [
    id 30
    label "release"
  ]
  node [
    id 31
    label "shelve"
  ]
  node [
    id 32
    label "Facebook"
  ]
  node [
    id 33
    label "odpowied&#378;"
  ]
  node [
    id 34
    label "interakcja"
  ]
  node [
    id 35
    label "informacja"
  ]
  node [
    id 36
    label "wytworzy&#263;"
  ]
  node [
    id 37
    label "line"
  ]
  node [
    id 38
    label "ship"
  ]
  node [
    id 39
    label "convey"
  ]
  node [
    id 40
    label "post"
  ]
  node [
    id 41
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 42
    label "nakaza&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 3
    target 4
  ]
]
