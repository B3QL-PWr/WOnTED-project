graph [
  maxDegree 14
  minDegree 1
  meanDegree 2.123456790123457
  density 0.02654320987654321
  graphCliqueNumber 3
  node [
    id 0
    label "bzura"
    origin "text"
  ]
  node [
    id 1
    label "ozorek"
    origin "text"
  ]
  node [
    id 2
    label "zremisowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mecz"
    origin "text"
  ]
  node [
    id 4
    label "puchar"
    origin "text"
  ]
  node [
    id 5
    label "polska"
    origin "text"
  ]
  node [
    id 6
    label "ekolog"
    origin "text"
  ]
  node [
    id 7
    label "zdu&#324;ski"
    origin "text"
  ]
  node [
    id 8
    label "wola"
    origin "text"
  ]
  node [
    id 9
    label "pierwsza"
    origin "text"
  ]
  node [
    id 10
    label "by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "dla"
    origin "text"
  ]
  node [
    id 12
    label "fortecki"
    origin "text"
  ]
  node [
    id 13
    label "ciszewski"
    origin "text"
  ]
  node [
    id 14
    label "suli&#324;ski"
    origin "text"
  ]
  node [
    id 15
    label "olejniczak"
    origin "text"
  ]
  node [
    id 16
    label "borkowski"
    origin "text"
  ]
  node [
    id 17
    label "karolak"
    origin "text"
  ]
  node [
    id 18
    label "szpiegowski"
    origin "text"
  ]
  node [
    id 19
    label "chmielecki"
    origin "text"
  ]
  node [
    id 20
    label "ziemniak"
    origin "text"
  ]
  node [
    id 21
    label "banasiak"
    origin "text"
  ]
  node [
    id 22
    label "koziak"
    origin "text"
  ]
  node [
    id 23
    label "micha&#322;"
    origin "text"
  ]
  node [
    id 24
    label "pieczarkowiec"
  ]
  node [
    id 25
    label "ozorkowate"
  ]
  node [
    id 26
    label "saprotrof"
  ]
  node [
    id 27
    label "grzyb"
  ]
  node [
    id 28
    label "paso&#380;yt"
  ]
  node [
    id 29
    label "podroby"
  ]
  node [
    id 30
    label "tie"
  ]
  node [
    id 31
    label "sko&#324;czy&#263;"
  ]
  node [
    id 32
    label "obrona"
  ]
  node [
    id 33
    label "gra"
  ]
  node [
    id 34
    label "dwumecz"
  ]
  node [
    id 35
    label "game"
  ]
  node [
    id 36
    label "serw"
  ]
  node [
    id 37
    label "zawarto&#347;&#263;"
  ]
  node [
    id 38
    label "zawody"
  ]
  node [
    id 39
    label "zwyci&#281;stwo"
  ]
  node [
    id 40
    label "naczynie"
  ]
  node [
    id 41
    label "nagroda"
  ]
  node [
    id 42
    label "zwolennik"
  ]
  node [
    id 43
    label "zieloni"
  ]
  node [
    id 44
    label "biolog"
  ]
  node [
    id 45
    label "dzia&#322;acz"
  ]
  node [
    id 46
    label "biomedyk"
  ]
  node [
    id 47
    label "oskoma"
  ]
  node [
    id 48
    label "wish"
  ]
  node [
    id 49
    label "emocja"
  ]
  node [
    id 50
    label "mniemanie"
  ]
  node [
    id 51
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 52
    label "inclination"
  ]
  node [
    id 53
    label "zajawka"
  ]
  node [
    id 54
    label "godzina"
  ]
  node [
    id 55
    label "si&#281;ga&#263;"
  ]
  node [
    id 56
    label "trwa&#263;"
  ]
  node [
    id 57
    label "obecno&#347;&#263;"
  ]
  node [
    id 58
    label "stan"
  ]
  node [
    id 59
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 60
    label "stand"
  ]
  node [
    id 61
    label "mie&#263;_miejsce"
  ]
  node [
    id 62
    label "uczestniczy&#263;"
  ]
  node [
    id 63
    label "chodzi&#263;"
  ]
  node [
    id 64
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 65
    label "equal"
  ]
  node [
    id 66
    label "szpiegowsko"
  ]
  node [
    id 67
    label "dyskretny"
  ]
  node [
    id 68
    label "bylina"
  ]
  node [
    id 69
    label "psianka"
  ]
  node [
    id 70
    label "warzywo"
  ]
  node [
    id 71
    label "m&#261;ka_ziemniaczana"
  ]
  node [
    id 72
    label "potato"
  ]
  node [
    id 73
    label "ro&#347;lina"
  ]
  node [
    id 74
    label "ba&#322;aban"
  ]
  node [
    id 75
    label "ro&#347;lina_bulwiasta"
  ]
  node [
    id 76
    label "p&#281;t&#243;wka"
  ]
  node [
    id 77
    label "ro&#347;lina_truj&#261;ca"
  ]
  node [
    id 78
    label "grula"
  ]
  node [
    id 79
    label "bulwa_p&#281;dowa"
  ]
  node [
    id 80
    label "przedmiot"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 66
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 68
  ]
  edge [
    source 20
    target 69
  ]
  edge [
    source 20
    target 70
  ]
  edge [
    source 20
    target 71
  ]
  edge [
    source 20
    target 72
  ]
  edge [
    source 20
    target 73
  ]
  edge [
    source 20
    target 74
  ]
  edge [
    source 20
    target 75
  ]
  edge [
    source 20
    target 76
  ]
  edge [
    source 20
    target 77
  ]
  edge [
    source 20
    target 78
  ]
  edge [
    source 20
    target 79
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 80
  ]
]
