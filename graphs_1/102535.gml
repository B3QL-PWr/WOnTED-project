graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.1379310344827585
  density 0.010583817002389894
  graphCliqueNumber 3
  node [
    id 0
    label "serwis"
    origin "text"
  ]
  node [
    id 1
    label "wikibooks"
    origin "text"
  ]
  node [
    id 2
    label "dost&#281;pny"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "podr&#281;cznik"
    origin "text"
  ]
  node [
    id 5
    label "pomaga&#263;"
    origin "text"
  ]
  node [
    id 6
    label "zapozna&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "standardowy"
    origin "text"
  ]
  node [
    id 9
    label "wersja"
    origin "text"
  ]
  node [
    id 10
    label "oprogramowanie"
    origin "text"
  ]
  node [
    id 11
    label "ponadto"
    origin "text"
  ]
  node [
    id 12
    label "rama"
    origin "text"
  ]
  node [
    id 13
    label "wika"
    origin "text"
  ]
  node [
    id 14
    label "projekt"
    origin "text"
  ]
  node [
    id 15
    label "wolne"
    origin "text"
  ]
  node [
    id 16
    label "strona"
    origin "text"
  ]
  node [
    id 17
    label "porada"
    origin "text"
  ]
  node [
    id 18
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "zasada"
    origin "text"
  ]
  node [
    id 20
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 21
    label "redagowa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "specyficzny"
    origin "text"
  ]
  node [
    id 23
    label "dla"
    origin "text"
  ]
  node [
    id 24
    label "lista"
    origin "text"
  ]
  node [
    id 25
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "mecz"
  ]
  node [
    id 27
    label "service"
  ]
  node [
    id 28
    label "wytw&#243;r"
  ]
  node [
    id 29
    label "zak&#322;ad"
  ]
  node [
    id 30
    label "us&#322;uga"
  ]
  node [
    id 31
    label "uderzenie"
  ]
  node [
    id 32
    label "doniesienie"
  ]
  node [
    id 33
    label "zastawa"
  ]
  node [
    id 34
    label "YouTube"
  ]
  node [
    id 35
    label "punkt"
  ]
  node [
    id 36
    label "porcja"
  ]
  node [
    id 37
    label "&#322;atwy"
  ]
  node [
    id 38
    label "mo&#380;liwy"
  ]
  node [
    id 39
    label "dost&#281;pnie"
  ]
  node [
    id 40
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 41
    label "przyst&#281;pnie"
  ]
  node [
    id 42
    label "zrozumia&#322;y"
  ]
  node [
    id 43
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 44
    label "odblokowanie_si&#281;"
  ]
  node [
    id 45
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 46
    label "si&#281;ga&#263;"
  ]
  node [
    id 47
    label "trwa&#263;"
  ]
  node [
    id 48
    label "obecno&#347;&#263;"
  ]
  node [
    id 49
    label "stan"
  ]
  node [
    id 50
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 51
    label "stand"
  ]
  node [
    id 52
    label "mie&#263;_miejsce"
  ]
  node [
    id 53
    label "uczestniczy&#263;"
  ]
  node [
    id 54
    label "chodzi&#263;"
  ]
  node [
    id 55
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 56
    label "equal"
  ]
  node [
    id 57
    label "wyprawka"
  ]
  node [
    id 58
    label "pomoc_naukowa"
  ]
  node [
    id 59
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 60
    label "sprzyja&#263;"
  ]
  node [
    id 61
    label "back"
  ]
  node [
    id 62
    label "skutkowa&#263;"
  ]
  node [
    id 63
    label "concur"
  ]
  node [
    id 64
    label "Warszawa"
  ]
  node [
    id 65
    label "robi&#263;"
  ]
  node [
    id 66
    label "aid"
  ]
  node [
    id 67
    label "u&#322;atwia&#263;"
  ]
  node [
    id 68
    label "powodowa&#263;"
  ]
  node [
    id 69
    label "digest"
  ]
  node [
    id 70
    label "pozna&#263;"
  ]
  node [
    id 71
    label "zawrze&#263;"
  ]
  node [
    id 72
    label "teach"
  ]
  node [
    id 73
    label "insert"
  ]
  node [
    id 74
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 75
    label "poinformowa&#263;"
  ]
  node [
    id 76
    label "obznajomi&#263;"
  ]
  node [
    id 77
    label "schematycznie"
  ]
  node [
    id 78
    label "zwyczajny"
  ]
  node [
    id 79
    label "standartowy"
  ]
  node [
    id 80
    label "typowy"
  ]
  node [
    id 81
    label "standardowo"
  ]
  node [
    id 82
    label "typ"
  ]
  node [
    id 83
    label "posta&#263;"
  ]
  node [
    id 84
    label "program"
  ]
  node [
    id 85
    label "zbi&#243;r"
  ]
  node [
    id 86
    label "reengineering"
  ]
  node [
    id 87
    label "zakres"
  ]
  node [
    id 88
    label "dodatek"
  ]
  node [
    id 89
    label "struktura"
  ]
  node [
    id 90
    label "stela&#380;"
  ]
  node [
    id 91
    label "za&#322;o&#380;enie"
  ]
  node [
    id 92
    label "human_body"
  ]
  node [
    id 93
    label "szablon"
  ]
  node [
    id 94
    label "oprawa"
  ]
  node [
    id 95
    label "paczka"
  ]
  node [
    id 96
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 97
    label "obramowanie"
  ]
  node [
    id 98
    label "pojazd"
  ]
  node [
    id 99
    label "postawa"
  ]
  node [
    id 100
    label "element_konstrukcyjny"
  ]
  node [
    id 101
    label "dokument"
  ]
  node [
    id 102
    label "device"
  ]
  node [
    id 103
    label "program_u&#380;ytkowy"
  ]
  node [
    id 104
    label "intencja"
  ]
  node [
    id 105
    label "agreement"
  ]
  node [
    id 106
    label "pomys&#322;"
  ]
  node [
    id 107
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 108
    label "plan"
  ]
  node [
    id 109
    label "dokumentacja"
  ]
  node [
    id 110
    label "czas_wolny"
  ]
  node [
    id 111
    label "skr&#281;canie"
  ]
  node [
    id 112
    label "voice"
  ]
  node [
    id 113
    label "forma"
  ]
  node [
    id 114
    label "internet"
  ]
  node [
    id 115
    label "skr&#281;ci&#263;"
  ]
  node [
    id 116
    label "kartka"
  ]
  node [
    id 117
    label "orientowa&#263;"
  ]
  node [
    id 118
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 119
    label "powierzchnia"
  ]
  node [
    id 120
    label "plik"
  ]
  node [
    id 121
    label "bok"
  ]
  node [
    id 122
    label "pagina"
  ]
  node [
    id 123
    label "orientowanie"
  ]
  node [
    id 124
    label "fragment"
  ]
  node [
    id 125
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 126
    label "s&#261;d"
  ]
  node [
    id 127
    label "skr&#281;ca&#263;"
  ]
  node [
    id 128
    label "g&#243;ra"
  ]
  node [
    id 129
    label "serwis_internetowy"
  ]
  node [
    id 130
    label "orientacja"
  ]
  node [
    id 131
    label "linia"
  ]
  node [
    id 132
    label "skr&#281;cenie"
  ]
  node [
    id 133
    label "layout"
  ]
  node [
    id 134
    label "zorientowa&#263;"
  ]
  node [
    id 135
    label "zorientowanie"
  ]
  node [
    id 136
    label "obiekt"
  ]
  node [
    id 137
    label "podmiot"
  ]
  node [
    id 138
    label "ty&#322;"
  ]
  node [
    id 139
    label "logowanie"
  ]
  node [
    id 140
    label "adres_internetowy"
  ]
  node [
    id 141
    label "uj&#281;cie"
  ]
  node [
    id 142
    label "prz&#243;d"
  ]
  node [
    id 143
    label "wskaz&#243;wka"
  ]
  node [
    id 144
    label "zapoznawa&#263;"
  ]
  node [
    id 145
    label "represent"
  ]
  node [
    id 146
    label "obserwacja"
  ]
  node [
    id 147
    label "moralno&#347;&#263;"
  ]
  node [
    id 148
    label "podstawa"
  ]
  node [
    id 149
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 150
    label "umowa"
  ]
  node [
    id 151
    label "dominion"
  ]
  node [
    id 152
    label "qualification"
  ]
  node [
    id 153
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 154
    label "opis"
  ]
  node [
    id 155
    label "regu&#322;a_Allena"
  ]
  node [
    id 156
    label "normalizacja"
  ]
  node [
    id 157
    label "regu&#322;a_Glogera"
  ]
  node [
    id 158
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 159
    label "standard"
  ]
  node [
    id 160
    label "base"
  ]
  node [
    id 161
    label "substancja"
  ]
  node [
    id 162
    label "prawid&#322;o"
  ]
  node [
    id 163
    label "prawo_Mendla"
  ]
  node [
    id 164
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 165
    label "criterion"
  ]
  node [
    id 166
    label "twierdzenie"
  ]
  node [
    id 167
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 168
    label "prawo"
  ]
  node [
    id 169
    label "occupation"
  ]
  node [
    id 170
    label "zasada_d'Alemberta"
  ]
  node [
    id 171
    label "model"
  ]
  node [
    id 172
    label "tryb"
  ]
  node [
    id 173
    label "narz&#281;dzie"
  ]
  node [
    id 174
    label "nature"
  ]
  node [
    id 175
    label "poprawia&#263;"
  ]
  node [
    id 176
    label "edit"
  ]
  node [
    id 177
    label "give_voice"
  ]
  node [
    id 178
    label "kierowa&#263;"
  ]
  node [
    id 179
    label "opracowywa&#263;"
  ]
  node [
    id 180
    label "specjalny"
  ]
  node [
    id 181
    label "wyj&#261;tkowy"
  ]
  node [
    id 182
    label "charakterystycznie"
  ]
  node [
    id 183
    label "specyficznie"
  ]
  node [
    id 184
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 185
    label "wyliczanka"
  ]
  node [
    id 186
    label "catalog"
  ]
  node [
    id 187
    label "stock"
  ]
  node [
    id 188
    label "figurowa&#263;"
  ]
  node [
    id 189
    label "book"
  ]
  node [
    id 190
    label "pozycja"
  ]
  node [
    id 191
    label "tekst"
  ]
  node [
    id 192
    label "sumariusz"
  ]
  node [
    id 193
    label "doznawa&#263;"
  ]
  node [
    id 194
    label "znachodzi&#263;"
  ]
  node [
    id 195
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 196
    label "pozyskiwa&#263;"
  ]
  node [
    id 197
    label "odzyskiwa&#263;"
  ]
  node [
    id 198
    label "os&#261;dza&#263;"
  ]
  node [
    id 199
    label "wykrywa&#263;"
  ]
  node [
    id 200
    label "unwrap"
  ]
  node [
    id 201
    label "detect"
  ]
  node [
    id 202
    label "wymy&#347;la&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 85
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 180
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 24
    target 85
  ]
  edge [
    source 24
    target 189
  ]
  edge [
    source 24
    target 190
  ]
  edge [
    source 24
    target 191
  ]
  edge [
    source 24
    target 192
  ]
  edge [
    source 25
    target 193
  ]
  edge [
    source 25
    target 194
  ]
  edge [
    source 25
    target 195
  ]
  edge [
    source 25
    target 196
  ]
  edge [
    source 25
    target 197
  ]
  edge [
    source 25
    target 198
  ]
  edge [
    source 25
    target 199
  ]
  edge [
    source 25
    target 200
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 25
    target 202
  ]
  edge [
    source 25
    target 68
  ]
]
