graph [
  maxDegree 34
  minDegree 1
  meanDegree 13.192982456140351
  density 0.23558897243107768
  graphCliqueNumber 24
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "ustawa"
    origin "text"
  ]
  node [
    id 2
    label "luty"
    origin "text"
  ]
  node [
    id 3
    label "poz"
    origin "text"
  ]
  node [
    id 4
    label "spis"
  ]
  node [
    id 5
    label "sheet"
  ]
  node [
    id 6
    label "gazeta"
  ]
  node [
    id 7
    label "diariusz"
  ]
  node [
    id 8
    label "pami&#281;tnik"
  ]
  node [
    id 9
    label "journal"
  ]
  node [
    id 10
    label "ksi&#281;ga"
  ]
  node [
    id 11
    label "program_informacyjny"
  ]
  node [
    id 12
    label "Karta_Nauczyciela"
  ]
  node [
    id 13
    label "marc&#243;wka"
  ]
  node [
    id 14
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 15
    label "akt"
  ]
  node [
    id 16
    label "przej&#347;&#263;"
  ]
  node [
    id 17
    label "charter"
  ]
  node [
    id 18
    label "przej&#347;cie"
  ]
  node [
    id 19
    label "miesi&#261;c"
  ]
  node [
    id 20
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 21
    label "walentynki"
  ]
  node [
    id 22
    label "ustawi&#263;"
  ]
  node [
    id 23
    label "ojciec"
  ]
  node [
    id 24
    label "zmiana"
  ]
  node [
    id 25
    label "ujawni&#263;"
  ]
  node [
    id 26
    label "praca"
  ]
  node [
    id 27
    label "lubi&#263;"
  ]
  node [
    id 28
    label "s&#322;u&#380;ba"
  ]
  node [
    id 29
    label "wyspa"
  ]
  node [
    id 30
    label "organy"
  ]
  node [
    id 31
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 32
    label "pa&#324;stwo"
  ]
  node [
    id 33
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 34
    label "zeszyt"
  ]
  node [
    id 35
    label "on"
  ]
  node [
    id 36
    label "rok"
  ]
  node [
    id 37
    label "1944"
  ]
  node [
    id 38
    label "&#8211;"
  ]
  node [
    id 39
    label "1990"
  ]
  node [
    id 40
    label "osoba"
  ]
  node [
    id 41
    label "pe&#322;ni&#263;"
  ]
  node [
    id 42
    label "funkcja"
  ]
  node [
    id 43
    label "publiczny"
  ]
  node [
    id 44
    label "ordynacja"
  ]
  node [
    id 45
    label "wyborczy"
  ]
  node [
    id 46
    label "do"
  ]
  node [
    id 47
    label "sejm"
  ]
  node [
    id 48
    label "rzeczpospolita"
  ]
  node [
    id 49
    label "polski"
  ]
  node [
    id 50
    label "i"
  ]
  node [
    id 51
    label "senat"
  ]
  node [
    id 52
    label "dzie&#324;"
  ]
  node [
    id 53
    label "11"
  ]
  node [
    id 54
    label "kwietni"
  ]
  node [
    id 55
    label "1997"
  ]
  node [
    id 56
    label "u"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 23
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 32
  ]
  edge [
    source 23
    target 33
  ]
  edge [
    source 23
    target 34
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 23
    target 38
  ]
  edge [
    source 23
    target 39
  ]
  edge [
    source 23
    target 40
  ]
  edge [
    source 23
    target 41
  ]
  edge [
    source 23
    target 42
  ]
  edge [
    source 23
    target 43
  ]
  edge [
    source 23
    target 52
  ]
  edge [
    source 23
    target 53
  ]
  edge [
    source 23
    target 54
  ]
  edge [
    source 23
    target 55
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 24
    target 37
  ]
  edge [
    source 24
    target 38
  ]
  edge [
    source 24
    target 39
  ]
  edge [
    source 24
    target 40
  ]
  edge [
    source 24
    target 41
  ]
  edge [
    source 24
    target 42
  ]
  edge [
    source 24
    target 43
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 25
    target 32
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 34
  ]
  edge [
    source 25
    target 35
  ]
  edge [
    source 25
    target 36
  ]
  edge [
    source 25
    target 37
  ]
  edge [
    source 25
    target 38
  ]
  edge [
    source 25
    target 39
  ]
  edge [
    source 25
    target 40
  ]
  edge [
    source 25
    target 41
  ]
  edge [
    source 25
    target 42
  ]
  edge [
    source 25
    target 43
  ]
  edge [
    source 25
    target 52
  ]
  edge [
    source 25
    target 53
  ]
  edge [
    source 25
    target 54
  ]
  edge [
    source 25
    target 55
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 26
    target 32
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 26
    target 37
  ]
  edge [
    source 26
    target 38
  ]
  edge [
    source 26
    target 39
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 41
  ]
  edge [
    source 26
    target 42
  ]
  edge [
    source 26
    target 43
  ]
  edge [
    source 26
    target 52
  ]
  edge [
    source 26
    target 53
  ]
  edge [
    source 26
    target 54
  ]
  edge [
    source 26
    target 55
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 27
    target 27
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 37
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 27
    target 39
  ]
  edge [
    source 27
    target 40
  ]
  edge [
    source 27
    target 41
  ]
  edge [
    source 27
    target 42
  ]
  edge [
    source 27
    target 43
  ]
  edge [
    source 27
    target 52
  ]
  edge [
    source 27
    target 53
  ]
  edge [
    source 27
    target 54
  ]
  edge [
    source 27
    target 55
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 28
    target 36
  ]
  edge [
    source 28
    target 37
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 28
    target 39
  ]
  edge [
    source 28
    target 40
  ]
  edge [
    source 28
    target 41
  ]
  edge [
    source 28
    target 42
  ]
  edge [
    source 28
    target 43
  ]
  edge [
    source 28
    target 52
  ]
  edge [
    source 28
    target 53
  ]
  edge [
    source 28
    target 54
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 29
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 29
    target 37
  ]
  edge [
    source 29
    target 38
  ]
  edge [
    source 29
    target 39
  ]
  edge [
    source 29
    target 40
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 29
    target 42
  ]
  edge [
    source 29
    target 43
  ]
  edge [
    source 29
    target 52
  ]
  edge [
    source 29
    target 53
  ]
  edge [
    source 29
    target 54
  ]
  edge [
    source 29
    target 55
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 30
    target 40
  ]
  edge [
    source 30
    target 41
  ]
  edge [
    source 30
    target 42
  ]
  edge [
    source 30
    target 43
  ]
  edge [
    source 30
    target 52
  ]
  edge [
    source 30
    target 53
  ]
  edge [
    source 30
    target 54
  ]
  edge [
    source 30
    target 55
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 38
  ]
  edge [
    source 31
    target 39
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 31
    target 41
  ]
  edge [
    source 31
    target 42
  ]
  edge [
    source 31
    target 43
  ]
  edge [
    source 31
    target 52
  ]
  edge [
    source 31
    target 53
  ]
  edge [
    source 31
    target 54
  ]
  edge [
    source 31
    target 55
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 32
    target 38
  ]
  edge [
    source 32
    target 39
  ]
  edge [
    source 32
    target 40
  ]
  edge [
    source 32
    target 41
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 32
    target 43
  ]
  edge [
    source 32
    target 52
  ]
  edge [
    source 32
    target 53
  ]
  edge [
    source 32
    target 54
  ]
  edge [
    source 32
    target 55
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 33
    target 41
  ]
  edge [
    source 33
    target 42
  ]
  edge [
    source 33
    target 43
  ]
  edge [
    source 33
    target 52
  ]
  edge [
    source 33
    target 53
  ]
  edge [
    source 33
    target 54
  ]
  edge [
    source 33
    target 55
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 34
    target 42
  ]
  edge [
    source 34
    target 43
  ]
  edge [
    source 34
    target 52
  ]
  edge [
    source 34
    target 53
  ]
  edge [
    source 34
    target 54
  ]
  edge [
    source 34
    target 55
  ]
  edge [
    source 34
    target 34
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 39
  ]
  edge [
    source 35
    target 40
  ]
  edge [
    source 35
    target 41
  ]
  edge [
    source 35
    target 42
  ]
  edge [
    source 35
    target 43
  ]
  edge [
    source 35
    target 52
  ]
  edge [
    source 35
    target 53
  ]
  edge [
    source 35
    target 54
  ]
  edge [
    source 35
    target 55
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 36
    target 41
  ]
  edge [
    source 36
    target 42
  ]
  edge [
    source 36
    target 43
  ]
  edge [
    source 36
    target 52
  ]
  edge [
    source 36
    target 53
  ]
  edge [
    source 36
    target 54
  ]
  edge [
    source 36
    target 55
  ]
  edge [
    source 36
    target 36
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 37
    target 41
  ]
  edge [
    source 37
    target 42
  ]
  edge [
    source 37
    target 43
  ]
  edge [
    source 37
    target 52
  ]
  edge [
    source 37
    target 53
  ]
  edge [
    source 37
    target 54
  ]
  edge [
    source 37
    target 55
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 38
    target 42
  ]
  edge [
    source 38
    target 43
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 39
    target 43
  ]
  edge [
    source 39
    target 52
  ]
  edge [
    source 39
    target 53
  ]
  edge [
    source 39
    target 54
  ]
  edge [
    source 39
    target 55
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 40
    target 52
  ]
  edge [
    source 40
    target 53
  ]
  edge [
    source 40
    target 54
  ]
  edge [
    source 40
    target 55
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 52
  ]
  edge [
    source 41
    target 53
  ]
  edge [
    source 41
    target 54
  ]
  edge [
    source 41
    target 55
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 52
  ]
  edge [
    source 42
    target 53
  ]
  edge [
    source 42
    target 54
  ]
  edge [
    source 42
    target 55
  ]
  edge [
    source 43
    target 52
  ]
  edge [
    source 43
    target 53
  ]
  edge [
    source 43
    target 54
  ]
  edge [
    source 43
    target 55
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 44
    target 49
  ]
  edge [
    source 44
    target 50
  ]
  edge [
    source 44
    target 51
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 45
    target 49
  ]
  edge [
    source 45
    target 50
  ]
  edge [
    source 45
    target 51
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 46
    target 50
  ]
  edge [
    source 46
    target 46
  ]
  edge [
    source 46
    target 51
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 48
    target 48
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 49
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 54
    target 55
  ]
]
