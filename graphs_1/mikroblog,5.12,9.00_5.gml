graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 2
  node [
    id 0
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "niebieski"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nim"
    origin "text"
  ]
  node [
    id 4
    label "zainteresowana"
    origin "text"
  ]
  node [
    id 5
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 6
    label "express"
  ]
  node [
    id 7
    label "rzekn&#261;&#263;"
  ]
  node [
    id 8
    label "okre&#347;li&#263;"
  ]
  node [
    id 9
    label "wyrazi&#263;"
  ]
  node [
    id 10
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 11
    label "unwrap"
  ]
  node [
    id 12
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 13
    label "convey"
  ]
  node [
    id 14
    label "discover"
  ]
  node [
    id 15
    label "wydoby&#263;"
  ]
  node [
    id 16
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 17
    label "poda&#263;"
  ]
  node [
    id 18
    label "ch&#322;odny"
  ]
  node [
    id 19
    label "siny"
  ]
  node [
    id 20
    label "niebiesko"
  ]
  node [
    id 21
    label "niebieszczenie"
  ]
  node [
    id 22
    label "si&#281;ga&#263;"
  ]
  node [
    id 23
    label "trwa&#263;"
  ]
  node [
    id 24
    label "obecno&#347;&#263;"
  ]
  node [
    id 25
    label "stan"
  ]
  node [
    id 26
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 27
    label "stand"
  ]
  node [
    id 28
    label "mie&#263;_miejsce"
  ]
  node [
    id 29
    label "uczestniczy&#263;"
  ]
  node [
    id 30
    label "chodzi&#263;"
  ]
  node [
    id 31
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 32
    label "equal"
  ]
  node [
    id 33
    label "gra_planszowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
]
