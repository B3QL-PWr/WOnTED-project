graph [
  maxDegree 10
  minDegree 1
  meanDegree 2.260869565217391
  density 0.10276679841897234
  graphCliqueNumber 5
  node [
    id 0
    label "zakon"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiadek"
    origin "text"
  ]
  node [
    id 2
    label "jerzy"
    origin "text"
  ]
  node [
    id 3
    label "karyntii"
    origin "text"
  ]
  node [
    id 4
    label "kongregacja"
  ]
  node [
    id 5
    label "klasztor"
  ]
  node [
    id 6
    label "kapitu&#322;a"
  ]
  node [
    id 7
    label "wsp&#243;lnota"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "s&#261;d"
  ]
  node [
    id 10
    label "osoba_fizyczna"
  ]
  node [
    id 11
    label "uczestnik"
  ]
  node [
    id 12
    label "obserwator"
  ]
  node [
    id 13
    label "dru&#380;ba"
  ]
  node [
    id 14
    label "&#347;wi&#281;ty"
  ]
  node [
    id 15
    label "Jerzy"
  ]
  node [
    id 16
    label "zeszyt"
  ]
  node [
    id 17
    label "Karyntii"
  ]
  node [
    id 18
    label "pawe&#322;"
  ]
  node [
    id 19
    label "ii"
  ]
  node [
    id 20
    label "Fryderyka"
  ]
  node [
    id 21
    label "iii"
  ]
  node [
    id 22
    label "krzy&#380;acki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
]
