graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.1254355400696863
  density 0.003709311588254252
  graphCliqueNumber 3
  node [
    id 0
    label "ciekawo&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "napisa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nawet"
    origin "text"
  ]
  node [
    id 3
    label "kino"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 6
    label "zaj&#281;ty"
    origin "text"
  ]
  node [
    id 7
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 8
    label "chodzenie"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "odpowied&#378;"
    origin "text"
  ]
  node [
    id 11
    label "sprawa"
    origin "text"
  ]
  node [
    id 12
    label "fotel"
    origin "text"
  ]
  node [
    id 13
    label "wszystko"
    origin "text"
  ]
  node [
    id 14
    label "stracona"
    origin "text"
  ]
  node [
    id 15
    label "koniec"
    origin "text"
  ]
  node [
    id 16
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 17
    label "sprawdza&#263;"
    origin "text"
  ]
  node [
    id 18
    label "spam"
    origin "text"
  ]
  node [
    id 19
    label "codziennie"
    origin "text"
  ]
  node [
    id 20
    label "zrozumie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 21
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 22
    label "wyst&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 23
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 24
    label "strz&#281;p"
    origin "text"
  ]
  node [
    id 25
    label "wypowied&#378;"
    origin "text"
  ]
  node [
    id 26
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 27
    label "ochrona"
    origin "text"
  ]
  node [
    id 28
    label "zabytek"
    origin "text"
  ]
  node [
    id 29
    label "mo&#380;liwy"
    origin "text"
  ]
  node [
    id 30
    label "jak"
    origin "text"
  ]
  node [
    id 31
    label "bardzo"
    origin "text"
  ]
  node [
    id 32
    label "nie"
    origin "text"
  ]
  node [
    id 33
    label "trudny"
    origin "text"
  ]
  node [
    id 34
    label "administracyjnie"
    origin "text"
  ]
  node [
    id 35
    label "jeszcze"
    origin "text"
  ]
  node [
    id 36
    label "droga"
    origin "text"
  ]
  node [
    id 37
    label "konserwator"
    origin "text"
  ]
  node [
    id 38
    label "zgodzi&#263;"
    origin "text"
  ]
  node [
    id 39
    label "tylko"
    origin "text"
  ]
  node [
    id 40
    label "nowoczesny"
    origin "text"
  ]
  node [
    id 41
    label "antyk"
    origin "text"
  ]
  node [
    id 42
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 43
    label "tychy"
    origin "text"
  ]
  node [
    id 44
    label "kawa&#322;ek"
    origin "text"
  ]
  node [
    id 45
    label "informacja"
    origin "text"
  ]
  node [
    id 46
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 47
    label "sala"
    origin "text"
  ]
  node [
    id 48
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 49
    label "obchodzi&#263;"
    origin "text"
  ]
  node [
    id 50
    label "okr&#261;g&#322;y"
    origin "text"
  ]
  node [
    id 51
    label "rocznica"
    origin "text"
  ]
  node [
    id 52
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 53
    label "stanowi&#263;"
    origin "text"
  ]
  node [
    id 54
    label "dobry"
    origin "text"
  ]
  node [
    id 55
    label "pretekst"
    origin "text"
  ]
  node [
    id 56
    label "nam&#243;wienie"
    origin "text"
  ]
  node [
    id 57
    label "instytucja"
    origin "text"
  ]
  node [
    id 58
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 59
    label "dotacja"
    origin "text"
  ]
  node [
    id 60
    label "teraz"
    origin "text"
  ]
  node [
    id 61
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 62
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 63
    label "bud&#380;et"
    origin "text"
  ]
  node [
    id 64
    label "ten"
    origin "text"
  ]
  node [
    id 65
    label "rok"
    origin "text"
  ]
  node [
    id 66
    label "dawno"
    origin "text"
  ]
  node [
    id 67
    label "zamkni&#281;ty"
    origin "text"
  ]
  node [
    id 68
    label "pi&#322;ka"
    origin "text"
  ]
  node [
    id 69
    label "atom"
    origin "text"
  ]
  node [
    id 70
    label "zagra&#263;"
    origin "text"
  ]
  node [
    id 71
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 72
    label "elektrownia"
    origin "text"
  ]
  node [
    id 73
    label "j&#261;drowy"
    origin "text"
  ]
  node [
    id 74
    label "te&#380;"
    origin "text"
  ]
  node [
    id 75
    label "polska"
    origin "text"
  ]
  node [
    id 76
    label "mama"
    origin "text"
  ]
  node [
    id 77
    label "emocja"
  ]
  node [
    id 78
    label "foreignness"
  ]
  node [
    id 79
    label "nastawienie"
  ]
  node [
    id 80
    label "animatronika"
  ]
  node [
    id 81
    label "cyrk"
  ]
  node [
    id 82
    label "seans"
  ]
  node [
    id 83
    label "dorobek"
  ]
  node [
    id 84
    label "ekran"
  ]
  node [
    id 85
    label "budynek"
  ]
  node [
    id 86
    label "kinoteatr"
  ]
  node [
    id 87
    label "picture"
  ]
  node [
    id 88
    label "bioskop"
  ]
  node [
    id 89
    label "sztuka"
  ]
  node [
    id 90
    label "muza"
  ]
  node [
    id 91
    label "si&#281;ga&#263;"
  ]
  node [
    id 92
    label "trwa&#263;"
  ]
  node [
    id 93
    label "obecno&#347;&#263;"
  ]
  node [
    id 94
    label "stan"
  ]
  node [
    id 95
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 96
    label "stand"
  ]
  node [
    id 97
    label "mie&#263;_miejsce"
  ]
  node [
    id 98
    label "uczestniczy&#263;"
  ]
  node [
    id 99
    label "chodzi&#263;"
  ]
  node [
    id 100
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 101
    label "equal"
  ]
  node [
    id 102
    label "zajmowanie"
  ]
  node [
    id 103
    label "pe&#322;ny"
  ]
  node [
    id 104
    label "dawny"
  ]
  node [
    id 105
    label "rozw&#243;d"
  ]
  node [
    id 106
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 107
    label "eksprezydent"
  ]
  node [
    id 108
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 109
    label "partner"
  ]
  node [
    id 110
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 111
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 112
    label "wcze&#347;niejszy"
  ]
  node [
    id 113
    label "uchodzenie_si&#281;"
  ]
  node [
    id 114
    label "uruchamianie"
  ]
  node [
    id 115
    label "przechodzenie"
  ]
  node [
    id 116
    label "ubieranie"
  ]
  node [
    id 117
    label "w&#322;&#261;czanie"
  ]
  node [
    id 118
    label "ucz&#281;szczanie"
  ]
  node [
    id 119
    label "rozdeptywanie"
  ]
  node [
    id 120
    label "noszenie"
  ]
  node [
    id 121
    label "wydeptanie"
  ]
  node [
    id 122
    label "wydeptywanie"
  ]
  node [
    id 123
    label "ko&#322;atanie_si&#281;"
  ]
  node [
    id 124
    label "zdeptanie"
  ]
  node [
    id 125
    label "uganianie_si&#281;"
  ]
  node [
    id 126
    label "p&#322;ywanie"
  ]
  node [
    id 127
    label "jitter"
  ]
  node [
    id 128
    label "funkcja"
  ]
  node [
    id 129
    label "dzianie_si&#281;"
  ]
  node [
    id 130
    label "zmierzanie"
  ]
  node [
    id 131
    label "wear"
  ]
  node [
    id 132
    label "deptanie"
  ]
  node [
    id 133
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 134
    label "rozdeptanie"
  ]
  node [
    id 135
    label "ubieranie_si&#281;"
  ]
  node [
    id 136
    label "znoszenie"
  ]
  node [
    id 137
    label "staranie_si&#281;"
  ]
  node [
    id 138
    label "podtrzymywanie"
  ]
  node [
    id 139
    label "przenoszenie"
  ]
  node [
    id 140
    label "poruszanie_si&#281;"
  ]
  node [
    id 141
    label "nakr&#281;canie"
  ]
  node [
    id 142
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 143
    label "pochodzenie"
  ]
  node [
    id 144
    label "uruchomienie"
  ]
  node [
    id 145
    label "bywanie"
  ]
  node [
    id 146
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 147
    label "tr&#243;jstronny"
  ]
  node [
    id 148
    label "impact"
  ]
  node [
    id 149
    label "dochodzenie"
  ]
  node [
    id 150
    label "w&#322;&#261;czenie"
  ]
  node [
    id 151
    label "zatrzymanie"
  ]
  node [
    id 152
    label "obchodzenie"
  ]
  node [
    id 153
    label "donoszenie"
  ]
  node [
    id 154
    label "nakr&#281;cenie"
  ]
  node [
    id 155
    label "donaszanie"
  ]
  node [
    id 156
    label "dokument"
  ]
  node [
    id 157
    label "rozmowa"
  ]
  node [
    id 158
    label "reakcja"
  ]
  node [
    id 159
    label "wyj&#347;cie"
  ]
  node [
    id 160
    label "react"
  ]
  node [
    id 161
    label "respondent"
  ]
  node [
    id 162
    label "replica"
  ]
  node [
    id 163
    label "temat"
  ]
  node [
    id 164
    label "kognicja"
  ]
  node [
    id 165
    label "idea"
  ]
  node [
    id 166
    label "szczeg&#243;&#322;"
  ]
  node [
    id 167
    label "rzecz"
  ]
  node [
    id 168
    label "wydarzenie"
  ]
  node [
    id 169
    label "przes&#322;anka"
  ]
  node [
    id 170
    label "rozprawa"
  ]
  node [
    id 171
    label "object"
  ]
  node [
    id 172
    label "proposition"
  ]
  node [
    id 173
    label "oparcie"
  ]
  node [
    id 174
    label "zaplecek"
  ]
  node [
    id 175
    label "siedzenie"
  ]
  node [
    id 176
    label "urz&#261;d"
  ]
  node [
    id 177
    label "pod&#322;okietnik"
  ]
  node [
    id 178
    label "komplet_wypoczynkowy"
  ]
  node [
    id 179
    label "por&#281;cz"
  ]
  node [
    id 180
    label "wezg&#322;owie"
  ]
  node [
    id 181
    label "mebel"
  ]
  node [
    id 182
    label "lock"
  ]
  node [
    id 183
    label "absolut"
  ]
  node [
    id 184
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 185
    label "defenestracja"
  ]
  node [
    id 186
    label "szereg"
  ]
  node [
    id 187
    label "dzia&#322;anie"
  ]
  node [
    id 188
    label "miejsce"
  ]
  node [
    id 189
    label "ostatnie_podrygi"
  ]
  node [
    id 190
    label "kres"
  ]
  node [
    id 191
    label "agonia"
  ]
  node [
    id 192
    label "visitation"
  ]
  node [
    id 193
    label "szeol"
  ]
  node [
    id 194
    label "mogi&#322;a"
  ]
  node [
    id 195
    label "chwila"
  ]
  node [
    id 196
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 197
    label "pogrzebanie"
  ]
  node [
    id 198
    label "punkt"
  ]
  node [
    id 199
    label "&#380;a&#322;oba"
  ]
  node [
    id 200
    label "zabicie"
  ]
  node [
    id 201
    label "kres_&#380;ycia"
  ]
  node [
    id 202
    label "robi&#263;"
  ]
  node [
    id 203
    label "examine"
  ]
  node [
    id 204
    label "szpiegowa&#263;"
  ]
  node [
    id 205
    label "filtr_antyspamowy"
  ]
  node [
    id 206
    label "reklama"
  ]
  node [
    id 207
    label "poczta_elektroniczna"
  ]
  node [
    id 208
    label "daily"
  ]
  node [
    id 209
    label "cz&#281;sto"
  ]
  node [
    id 210
    label "codzienny"
  ]
  node [
    id 211
    label "prozaicznie"
  ]
  node [
    id 212
    label "stale"
  ]
  node [
    id 213
    label "regularnie"
  ]
  node [
    id 214
    label "pospolicie"
  ]
  node [
    id 215
    label "jako&#347;"
  ]
  node [
    id 216
    label "charakterystyczny"
  ]
  node [
    id 217
    label "ciekawy"
  ]
  node [
    id 218
    label "jako_tako"
  ]
  node [
    id 219
    label "dziwny"
  ]
  node [
    id 220
    label "niez&#322;y"
  ]
  node [
    id 221
    label "przyzwoity"
  ]
  node [
    id 222
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 223
    label "wychodzi&#263;"
  ]
  node [
    id 224
    label "dzia&#322;a&#263;"
  ]
  node [
    id 225
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 226
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 227
    label "act"
  ]
  node [
    id 228
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 229
    label "unwrap"
  ]
  node [
    id 230
    label "seclude"
  ]
  node [
    id 231
    label "perform"
  ]
  node [
    id 232
    label "odst&#281;powa&#263;"
  ]
  node [
    id 233
    label "rezygnowa&#263;"
  ]
  node [
    id 234
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 235
    label "overture"
  ]
  node [
    id 236
    label "nak&#322;ania&#263;"
  ]
  node [
    id 237
    label "appear"
  ]
  node [
    id 238
    label "hipertekst"
  ]
  node [
    id 239
    label "gauze"
  ]
  node [
    id 240
    label "nitka"
  ]
  node [
    id 241
    label "mesh"
  ]
  node [
    id 242
    label "e-hazard"
  ]
  node [
    id 243
    label "netbook"
  ]
  node [
    id 244
    label "cyberprzestrze&#324;"
  ]
  node [
    id 245
    label "biznes_elektroniczny"
  ]
  node [
    id 246
    label "snu&#263;"
  ]
  node [
    id 247
    label "organization"
  ]
  node [
    id 248
    label "zasadzka"
  ]
  node [
    id 249
    label "web"
  ]
  node [
    id 250
    label "provider"
  ]
  node [
    id 251
    label "struktura"
  ]
  node [
    id 252
    label "us&#322;uga_internetowa"
  ]
  node [
    id 253
    label "punkt_dost&#281;pu"
  ]
  node [
    id 254
    label "organizacja"
  ]
  node [
    id 255
    label "mem"
  ]
  node [
    id 256
    label "vane"
  ]
  node [
    id 257
    label "podcast"
  ]
  node [
    id 258
    label "grooming"
  ]
  node [
    id 259
    label "kszta&#322;t"
  ]
  node [
    id 260
    label "strona"
  ]
  node [
    id 261
    label "obiekt"
  ]
  node [
    id 262
    label "wysnu&#263;"
  ]
  node [
    id 263
    label "gra_sieciowa"
  ]
  node [
    id 264
    label "instalacja"
  ]
  node [
    id 265
    label "sie&#263;_komputerowa"
  ]
  node [
    id 266
    label "net"
  ]
  node [
    id 267
    label "plecionka"
  ]
  node [
    id 268
    label "media"
  ]
  node [
    id 269
    label "rozmieszczenie"
  ]
  node [
    id 270
    label "ribbon"
  ]
  node [
    id 271
    label "targn&#261;&#263;"
  ]
  node [
    id 272
    label "parafrazowa&#263;"
  ]
  node [
    id 273
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 274
    label "sparafrazowa&#263;"
  ]
  node [
    id 275
    label "s&#261;d"
  ]
  node [
    id 276
    label "trawestowanie"
  ]
  node [
    id 277
    label "sparafrazowanie"
  ]
  node [
    id 278
    label "strawestowa&#263;"
  ]
  node [
    id 279
    label "sformu&#322;owanie"
  ]
  node [
    id 280
    label "strawestowanie"
  ]
  node [
    id 281
    label "komunikat"
  ]
  node [
    id 282
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 283
    label "delimitacja"
  ]
  node [
    id 284
    label "trawestowa&#263;"
  ]
  node [
    id 285
    label "parafrazowanie"
  ]
  node [
    id 286
    label "stylizacja"
  ]
  node [
    id 287
    label "ozdobnik"
  ]
  node [
    id 288
    label "pos&#322;uchanie"
  ]
  node [
    id 289
    label "rezultat"
  ]
  node [
    id 290
    label "wykupienie"
  ]
  node [
    id 291
    label "bycie_w_posiadaniu"
  ]
  node [
    id 292
    label "wykupywanie"
  ]
  node [
    id 293
    label "podmiot"
  ]
  node [
    id 294
    label "chemical_bond"
  ]
  node [
    id 295
    label "tarcza"
  ]
  node [
    id 296
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 297
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 298
    label "borowiec"
  ]
  node [
    id 299
    label "obstawienie"
  ]
  node [
    id 300
    label "formacja"
  ]
  node [
    id 301
    label "ubezpieczenie"
  ]
  node [
    id 302
    label "obstawia&#263;"
  ]
  node [
    id 303
    label "obstawianie"
  ]
  node [
    id 304
    label "transportacja"
  ]
  node [
    id 305
    label "&#347;wiadectwo"
  ]
  node [
    id 306
    label "przedmiot"
  ]
  node [
    id 307
    label "starzyzna"
  ]
  node [
    id 308
    label "keepsake"
  ]
  node [
    id 309
    label "zno&#347;ny"
  ]
  node [
    id 310
    label "mo&#380;liwie"
  ]
  node [
    id 311
    label "urealnianie"
  ]
  node [
    id 312
    label "umo&#380;liwienie"
  ]
  node [
    id 313
    label "mo&#380;ebny"
  ]
  node [
    id 314
    label "umo&#380;liwianie"
  ]
  node [
    id 315
    label "dost&#281;pny"
  ]
  node [
    id 316
    label "urealnienie"
  ]
  node [
    id 317
    label "byd&#322;o"
  ]
  node [
    id 318
    label "zobo"
  ]
  node [
    id 319
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 320
    label "yakalo"
  ]
  node [
    id 321
    label "dzo"
  ]
  node [
    id 322
    label "w_chuj"
  ]
  node [
    id 323
    label "sprzeciw"
  ]
  node [
    id 324
    label "wymagaj&#261;cy"
  ]
  node [
    id 325
    label "skomplikowany"
  ]
  node [
    id 326
    label "k&#322;opotliwy"
  ]
  node [
    id 327
    label "ci&#281;&#380;ko"
  ]
  node [
    id 328
    label "urz&#281;dowo"
  ]
  node [
    id 329
    label "administracyjny"
  ]
  node [
    id 330
    label "ci&#261;gle"
  ]
  node [
    id 331
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 332
    label "journey"
  ]
  node [
    id 333
    label "podbieg"
  ]
  node [
    id 334
    label "bezsilnikowy"
  ]
  node [
    id 335
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 336
    label "wylot"
  ]
  node [
    id 337
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 338
    label "drogowskaz"
  ]
  node [
    id 339
    label "nawierzchnia"
  ]
  node [
    id 340
    label "turystyka"
  ]
  node [
    id 341
    label "budowla"
  ]
  node [
    id 342
    label "spos&#243;b"
  ]
  node [
    id 343
    label "passage"
  ]
  node [
    id 344
    label "marszrutyzacja"
  ]
  node [
    id 345
    label "zbior&#243;wka"
  ]
  node [
    id 346
    label "rajza"
  ]
  node [
    id 347
    label "ekskursja"
  ]
  node [
    id 348
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 349
    label "ruch"
  ]
  node [
    id 350
    label "trasa"
  ]
  node [
    id 351
    label "wyb&#243;j"
  ]
  node [
    id 352
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 353
    label "ekwipunek"
  ]
  node [
    id 354
    label "korona_drogi"
  ]
  node [
    id 355
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 356
    label "pobocze"
  ]
  node [
    id 357
    label "remontowiec"
  ]
  node [
    id 358
    label "serwisant"
  ]
  node [
    id 359
    label "reperator"
  ]
  node [
    id 360
    label "mechanik"
  ]
  node [
    id 361
    label "zatrudni&#263;"
  ]
  node [
    id 362
    label "zgodzenie"
  ]
  node [
    id 363
    label "pomoc"
  ]
  node [
    id 364
    label "zgadza&#263;"
  ]
  node [
    id 365
    label "nowy"
  ]
  node [
    id 366
    label "nowocze&#347;nie"
  ]
  node [
    id 367
    label "otwarty"
  ]
  node [
    id 368
    label "nowo&#380;ytny"
  ]
  node [
    id 369
    label "staro&#263;"
  ]
  node [
    id 370
    label "staro&#380;ytno&#347;&#263;"
  ]
  node [
    id 371
    label "aretalogia"
  ]
  node [
    id 372
    label "styl_dorycki"
  ]
  node [
    id 373
    label "okres_klasyczny"
  ]
  node [
    id 374
    label "klasycyzm"
  ]
  node [
    id 375
    label "epoka"
  ]
  node [
    id 376
    label "styl_koryncki"
  ]
  node [
    id 377
    label "styl_kompozytowy"
  ]
  node [
    id 378
    label "styl_jo&#324;ski"
  ]
  node [
    id 379
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 380
    label "podp&#322;ywanie"
  ]
  node [
    id 381
    label "plot"
  ]
  node [
    id 382
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 383
    label "piece"
  ]
  node [
    id 384
    label "kawa&#322;"
  ]
  node [
    id 385
    label "utw&#243;r"
  ]
  node [
    id 386
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 387
    label "doj&#347;cie"
  ]
  node [
    id 388
    label "doj&#347;&#263;"
  ]
  node [
    id 389
    label "powzi&#261;&#263;"
  ]
  node [
    id 390
    label "wiedza"
  ]
  node [
    id 391
    label "sygna&#322;"
  ]
  node [
    id 392
    label "obiegni&#281;cie"
  ]
  node [
    id 393
    label "obieganie"
  ]
  node [
    id 394
    label "obiec"
  ]
  node [
    id 395
    label "dane"
  ]
  node [
    id 396
    label "obiega&#263;"
  ]
  node [
    id 397
    label "publikacja"
  ]
  node [
    id 398
    label "powzi&#281;cie"
  ]
  node [
    id 399
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 400
    label "rise"
  ]
  node [
    id 401
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 402
    label "zgromadzenie"
  ]
  node [
    id 403
    label "publiczno&#347;&#263;"
  ]
  node [
    id 404
    label "audience"
  ]
  node [
    id 405
    label "pomieszczenie"
  ]
  node [
    id 406
    label "wykorzystywa&#263;"
  ]
  node [
    id 407
    label "omija&#263;"
  ]
  node [
    id 408
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 409
    label "sp&#281;dza&#263;"
  ]
  node [
    id 410
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 411
    label "interesowa&#263;"
  ]
  node [
    id 412
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 413
    label "post&#281;powa&#263;"
  ]
  node [
    id 414
    label "feast"
  ]
  node [
    id 415
    label "wymija&#263;"
  ]
  node [
    id 416
    label "odwiedza&#263;"
  ]
  node [
    id 417
    label "prowadzi&#263;"
  ]
  node [
    id 418
    label "p&#322;ynny"
  ]
  node [
    id 419
    label "obr&#281;czowy"
  ]
  node [
    id 420
    label "zaokr&#261;glanie"
  ]
  node [
    id 421
    label "zaokr&#261;glanie_si&#281;"
  ]
  node [
    id 422
    label "r&#243;wny"
  ]
  node [
    id 423
    label "ca&#322;y"
  ]
  node [
    id 424
    label "okr&#261;g&#322;o"
  ]
  node [
    id 425
    label "zgrabny"
  ]
  node [
    id 426
    label "zaokr&#261;glenie_si&#281;"
  ]
  node [
    id 427
    label "zaokr&#261;glenie"
  ]
  node [
    id 428
    label "termin"
  ]
  node [
    id 429
    label "obchody"
  ]
  node [
    id 430
    label "typify"
  ]
  node [
    id 431
    label "represent"
  ]
  node [
    id 432
    label "decydowa&#263;"
  ]
  node [
    id 433
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 434
    label "decide"
  ]
  node [
    id 435
    label "zatrzymywa&#263;"
  ]
  node [
    id 436
    label "pies_my&#347;liwski"
  ]
  node [
    id 437
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 438
    label "pomy&#347;lny"
  ]
  node [
    id 439
    label "skuteczny"
  ]
  node [
    id 440
    label "moralny"
  ]
  node [
    id 441
    label "korzystny"
  ]
  node [
    id 442
    label "odpowiedni"
  ]
  node [
    id 443
    label "zwrot"
  ]
  node [
    id 444
    label "dobrze"
  ]
  node [
    id 445
    label "pozytywny"
  ]
  node [
    id 446
    label "grzeczny"
  ]
  node [
    id 447
    label "powitanie"
  ]
  node [
    id 448
    label "mi&#322;y"
  ]
  node [
    id 449
    label "dobroczynny"
  ]
  node [
    id 450
    label "pos&#322;uszny"
  ]
  node [
    id 451
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 452
    label "czw&#243;rka"
  ]
  node [
    id 453
    label "spokojny"
  ]
  node [
    id 454
    label "&#347;mieszny"
  ]
  node [
    id 455
    label "drogi"
  ]
  node [
    id 456
    label "pretense"
  ]
  node [
    id 457
    label "reason"
  ]
  node [
    id 458
    label "okazja"
  ]
  node [
    id 459
    label "wym&#243;wka"
  ]
  node [
    id 460
    label "nak&#322;onienie"
  ]
  node [
    id 461
    label "naci&#261;gni&#281;cie"
  ]
  node [
    id 462
    label "afiliowa&#263;"
  ]
  node [
    id 463
    label "osoba_prawna"
  ]
  node [
    id 464
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 465
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 466
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 467
    label "establishment"
  ]
  node [
    id 468
    label "standard"
  ]
  node [
    id 469
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 470
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 471
    label "zamykanie"
  ]
  node [
    id 472
    label "zamyka&#263;"
  ]
  node [
    id 473
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 474
    label "poj&#281;cie"
  ]
  node [
    id 475
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 476
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 477
    label "Fundusze_Unijne"
  ]
  node [
    id 478
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 479
    label "biuro"
  ]
  node [
    id 480
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 481
    label "dziewczynka"
  ]
  node [
    id 482
    label "dziewczyna"
  ]
  node [
    id 483
    label "dop&#322;ata"
  ]
  node [
    id 484
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 485
    label "p&#243;&#378;ny"
  ]
  node [
    id 486
    label "wydatki_bud&#380;etowe"
  ]
  node [
    id 487
    label "etat"
  ]
  node [
    id 488
    label "portfel"
  ]
  node [
    id 489
    label "rezerwa_rewaluacyjna"
  ]
  node [
    id 490
    label "kwota"
  ]
  node [
    id 491
    label "okre&#347;lony"
  ]
  node [
    id 492
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 493
    label "stulecie"
  ]
  node [
    id 494
    label "kalendarz"
  ]
  node [
    id 495
    label "czas"
  ]
  node [
    id 496
    label "pora_roku"
  ]
  node [
    id 497
    label "cykl_astronomiczny"
  ]
  node [
    id 498
    label "p&#243;&#322;rocze"
  ]
  node [
    id 499
    label "grupa"
  ]
  node [
    id 500
    label "kwarta&#322;"
  ]
  node [
    id 501
    label "kurs"
  ]
  node [
    id 502
    label "jubileusz"
  ]
  node [
    id 503
    label "miesi&#261;c"
  ]
  node [
    id 504
    label "lata"
  ]
  node [
    id 505
    label "martwy_sezon"
  ]
  node [
    id 506
    label "ongi&#347;"
  ]
  node [
    id 507
    label "dawnie"
  ]
  node [
    id 508
    label "wcze&#347;niej"
  ]
  node [
    id 509
    label "d&#322;ugotrwale"
  ]
  node [
    id 510
    label "introwertyczny"
  ]
  node [
    id 511
    label "hermetycznie"
  ]
  node [
    id 512
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 513
    label "kryjomy"
  ]
  node [
    id 514
    label "ograniczony"
  ]
  node [
    id 515
    label "zagrywka"
  ]
  node [
    id 516
    label "serwowanie"
  ]
  node [
    id 517
    label "sport"
  ]
  node [
    id 518
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 519
    label "sport_zespo&#322;owy"
  ]
  node [
    id 520
    label "odbicie"
  ]
  node [
    id 521
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 522
    label "rzucanka"
  ]
  node [
    id 523
    label "gra"
  ]
  node [
    id 524
    label "aut"
  ]
  node [
    id 525
    label "orb"
  ]
  node [
    id 526
    label "zaserwowanie"
  ]
  node [
    id 527
    label "do&#347;rodkowywanie"
  ]
  node [
    id 528
    label "zaserwowa&#263;"
  ]
  node [
    id 529
    label "kula"
  ]
  node [
    id 530
    label "&#347;wieca"
  ]
  node [
    id 531
    label "serwowa&#263;"
  ]
  node [
    id 532
    label "musket_ball"
  ]
  node [
    id 533
    label "elektron"
  ]
  node [
    id 534
    label "rdze&#324;_atomowy"
  ]
  node [
    id 535
    label "diadochia"
  ]
  node [
    id 536
    label "cz&#261;steczka"
  ]
  node [
    id 537
    label "masa_atomowa"
  ]
  node [
    id 538
    label "pierwiastek"
  ]
  node [
    id 539
    label "cz&#261;stka_elementarna"
  ]
  node [
    id 540
    label "j&#261;dro_atomowe"
  ]
  node [
    id 541
    label "mikrokosmos"
  ]
  node [
    id 542
    label "liczba_atomowa"
  ]
  node [
    id 543
    label "pow&#322;oka_elektronowa"
  ]
  node [
    id 544
    label "flare"
  ]
  node [
    id 545
    label "zaszczeka&#263;"
  ]
  node [
    id 546
    label "rola"
  ]
  node [
    id 547
    label "wykona&#263;"
  ]
  node [
    id 548
    label "wykorzysta&#263;"
  ]
  node [
    id 549
    label "zacz&#261;&#263;"
  ]
  node [
    id 550
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 551
    label "zatokowa&#263;"
  ]
  node [
    id 552
    label "zabrzmie&#263;"
  ]
  node [
    id 553
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 554
    label "leave"
  ]
  node [
    id 555
    label "uda&#263;_si&#281;"
  ]
  node [
    id 556
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 557
    label "instrument_muzyczny"
  ]
  node [
    id 558
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 559
    label "zrobi&#263;"
  ]
  node [
    id 560
    label "play"
  ]
  node [
    id 561
    label "sound"
  ]
  node [
    id 562
    label "rozegra&#263;"
  ]
  node [
    id 563
    label "free"
  ]
  node [
    id 564
    label "zak&#322;ad_komunalny"
  ]
  node [
    id 565
    label "matczysko"
  ]
  node [
    id 566
    label "macierz"
  ]
  node [
    id 567
    label "przodkini"
  ]
  node [
    id 568
    label "Matka_Boska"
  ]
  node [
    id 569
    label "macocha"
  ]
  node [
    id 570
    label "matka_zast&#281;pcza"
  ]
  node [
    id 571
    label "stara"
  ]
  node [
    id 572
    label "rodzice"
  ]
  node [
    id 573
    label "rodzic"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 36
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 197
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 207
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 56
  ]
  edge [
    source 21
    target 57
  ]
  edge [
    source 21
    target 215
  ]
  edge [
    source 21
    target 216
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 219
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 222
  ]
  edge [
    source 22
    target 223
  ]
  edge [
    source 22
    target 97
  ]
  edge [
    source 22
    target 224
  ]
  edge [
    source 22
    target 225
  ]
  edge [
    source 22
    target 98
  ]
  edge [
    source 22
    target 226
  ]
  edge [
    source 22
    target 227
  ]
  edge [
    source 22
    target 228
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 22
    target 231
  ]
  edge [
    source 22
    target 232
  ]
  edge [
    source 22
    target 233
  ]
  edge [
    source 22
    target 234
  ]
  edge [
    source 22
    target 235
  ]
  edge [
    source 22
    target 236
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 238
  ]
  edge [
    source 23
    target 239
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 243
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 184
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 23
    target 250
  ]
  edge [
    source 23
    target 251
  ]
  edge [
    source 23
    target 252
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 254
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 256
  ]
  edge [
    source 23
    target 257
  ]
  edge [
    source 23
    target 258
  ]
  edge [
    source 23
    target 259
  ]
  edge [
    source 23
    target 260
  ]
  edge [
    source 23
    target 261
  ]
  edge [
    source 23
    target 262
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 24
    target 270
  ]
  edge [
    source 24
    target 271
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 272
  ]
  edge [
    source 25
    target 273
  ]
  edge [
    source 25
    target 274
  ]
  edge [
    source 25
    target 275
  ]
  edge [
    source 25
    target 276
  ]
  edge [
    source 25
    target 277
  ]
  edge [
    source 25
    target 278
  ]
  edge [
    source 25
    target 279
  ]
  edge [
    source 25
    target 280
  ]
  edge [
    source 25
    target 281
  ]
  edge [
    source 25
    target 282
  ]
  edge [
    source 25
    target 283
  ]
  edge [
    source 25
    target 284
  ]
  edge [
    source 25
    target 285
  ]
  edge [
    source 25
    target 286
  ]
  edge [
    source 25
    target 287
  ]
  edge [
    source 25
    target 288
  ]
  edge [
    source 25
    target 289
  ]
  edge [
    source 26
    target 290
  ]
  edge [
    source 26
    target 291
  ]
  edge [
    source 26
    target 292
  ]
  edge [
    source 26
    target 293
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 294
  ]
  edge [
    source 27
    target 295
  ]
  edge [
    source 27
    target 296
  ]
  edge [
    source 27
    target 261
  ]
  edge [
    source 27
    target 297
  ]
  edge [
    source 27
    target 298
  ]
  edge [
    source 27
    target 299
  ]
  edge [
    source 27
    target 300
  ]
  edge [
    source 27
    target 301
  ]
  edge [
    source 27
    target 302
  ]
  edge [
    source 27
    target 303
  ]
  edge [
    source 27
    target 304
  ]
  edge [
    source 28
    target 305
  ]
  edge [
    source 28
    target 306
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 28
    target 308
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 309
  ]
  edge [
    source 29
    target 310
  ]
  edge [
    source 29
    target 311
  ]
  edge [
    source 29
    target 312
  ]
  edge [
    source 29
    target 313
  ]
  edge [
    source 29
    target 314
  ]
  edge [
    source 29
    target 315
  ]
  edge [
    source 29
    target 316
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 317
  ]
  edge [
    source 30
    target 318
  ]
  edge [
    source 30
    target 319
  ]
  edge [
    source 30
    target 320
  ]
  edge [
    source 30
    target 321
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 322
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 323
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 324
  ]
  edge [
    source 33
    target 325
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 328
  ]
  edge [
    source 34
    target 329
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 330
  ]
  edge [
    source 36
    target 331
  ]
  edge [
    source 36
    target 332
  ]
  edge [
    source 36
    target 333
  ]
  edge [
    source 36
    target 334
  ]
  edge [
    source 36
    target 335
  ]
  edge [
    source 36
    target 336
  ]
  edge [
    source 36
    target 337
  ]
  edge [
    source 36
    target 338
  ]
  edge [
    source 36
    target 339
  ]
  edge [
    source 36
    target 340
  ]
  edge [
    source 36
    target 341
  ]
  edge [
    source 36
    target 342
  ]
  edge [
    source 36
    target 343
  ]
  edge [
    source 36
    target 344
  ]
  edge [
    source 36
    target 345
  ]
  edge [
    source 36
    target 346
  ]
  edge [
    source 36
    target 347
  ]
  edge [
    source 36
    target 348
  ]
  edge [
    source 36
    target 349
  ]
  edge [
    source 36
    target 350
  ]
  edge [
    source 36
    target 351
  ]
  edge [
    source 36
    target 352
  ]
  edge [
    source 36
    target 353
  ]
  edge [
    source 36
    target 354
  ]
  edge [
    source 36
    target 355
  ]
  edge [
    source 36
    target 356
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 357
  ]
  edge [
    source 37
    target 358
  ]
  edge [
    source 37
    target 359
  ]
  edge [
    source 37
    target 360
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 361
  ]
  edge [
    source 38
    target 362
  ]
  edge [
    source 38
    target 363
  ]
  edge [
    source 38
    target 364
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 365
  ]
  edge [
    source 40
    target 366
  ]
  edge [
    source 40
    target 367
  ]
  edge [
    source 40
    target 368
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 369
  ]
  edge [
    source 41
    target 370
  ]
  edge [
    source 41
    target 371
  ]
  edge [
    source 41
    target 372
  ]
  edge [
    source 41
    target 373
  ]
  edge [
    source 41
    target 374
  ]
  edge [
    source 41
    target 375
  ]
  edge [
    source 41
    target 376
  ]
  edge [
    source 41
    target 377
  ]
  edge [
    source 41
    target 378
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 50
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 379
  ]
  edge [
    source 44
    target 380
  ]
  edge [
    source 44
    target 381
  ]
  edge [
    source 44
    target 382
  ]
  edge [
    source 44
    target 383
  ]
  edge [
    source 44
    target 384
  ]
  edge [
    source 44
    target 196
  ]
  edge [
    source 44
    target 385
  ]
  edge [
    source 44
    target 386
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 387
  ]
  edge [
    source 45
    target 388
  ]
  edge [
    source 45
    target 389
  ]
  edge [
    source 45
    target 390
  ]
  edge [
    source 45
    target 391
  ]
  edge [
    source 45
    target 392
  ]
  edge [
    source 45
    target 393
  ]
  edge [
    source 45
    target 394
  ]
  edge [
    source 45
    target 395
  ]
  edge [
    source 45
    target 396
  ]
  edge [
    source 45
    target 198
  ]
  edge [
    source 45
    target 397
  ]
  edge [
    source 45
    target 398
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 399
  ]
  edge [
    source 46
    target 400
  ]
  edge [
    source 46
    target 237
  ]
  edge [
    source 46
    target 401
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 402
  ]
  edge [
    source 47
    target 403
  ]
  edge [
    source 47
    target 404
  ]
  edge [
    source 47
    target 405
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 96
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 406
  ]
  edge [
    source 49
    target 407
  ]
  edge [
    source 49
    target 408
  ]
  edge [
    source 49
    target 409
  ]
  edge [
    source 49
    target 410
  ]
  edge [
    source 49
    target 411
  ]
  edge [
    source 49
    target 412
  ]
  edge [
    source 49
    target 413
  ]
  edge [
    source 49
    target 414
  ]
  edge [
    source 49
    target 99
  ]
  edge [
    source 49
    target 415
  ]
  edge [
    source 49
    target 416
  ]
  edge [
    source 49
    target 417
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 418
  ]
  edge [
    source 50
    target 419
  ]
  edge [
    source 50
    target 420
  ]
  edge [
    source 50
    target 421
  ]
  edge [
    source 50
    target 422
  ]
  edge [
    source 50
    target 423
  ]
  edge [
    source 50
    target 103
  ]
  edge [
    source 50
    target 424
  ]
  edge [
    source 50
    target 425
  ]
  edge [
    source 50
    target 426
  ]
  edge [
    source 50
    target 427
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 428
  ]
  edge [
    source 51
    target 429
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 430
  ]
  edge [
    source 53
    target 431
  ]
  edge [
    source 53
    target 432
  ]
  edge [
    source 53
    target 433
  ]
  edge [
    source 53
    target 434
  ]
  edge [
    source 53
    target 435
  ]
  edge [
    source 53
    target 436
  ]
  edge [
    source 53
    target 437
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 438
  ]
  edge [
    source 54
    target 439
  ]
  edge [
    source 54
    target 440
  ]
  edge [
    source 54
    target 441
  ]
  edge [
    source 54
    target 442
  ]
  edge [
    source 54
    target 443
  ]
  edge [
    source 54
    target 444
  ]
  edge [
    source 54
    target 445
  ]
  edge [
    source 54
    target 446
  ]
  edge [
    source 54
    target 447
  ]
  edge [
    source 54
    target 448
  ]
  edge [
    source 54
    target 449
  ]
  edge [
    source 54
    target 450
  ]
  edge [
    source 54
    target 423
  ]
  edge [
    source 54
    target 451
  ]
  edge [
    source 54
    target 452
  ]
  edge [
    source 54
    target 453
  ]
  edge [
    source 54
    target 454
  ]
  edge [
    source 54
    target 455
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 456
  ]
  edge [
    source 55
    target 457
  ]
  edge [
    source 55
    target 458
  ]
  edge [
    source 55
    target 459
  ]
  edge [
    source 56
    target 460
  ]
  edge [
    source 56
    target 461
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 462
  ]
  edge [
    source 57
    target 463
  ]
  edge [
    source 57
    target 464
  ]
  edge [
    source 57
    target 176
  ]
  edge [
    source 57
    target 465
  ]
  edge [
    source 57
    target 466
  ]
  edge [
    source 57
    target 467
  ]
  edge [
    source 57
    target 468
  ]
  edge [
    source 57
    target 254
  ]
  edge [
    source 57
    target 469
  ]
  edge [
    source 57
    target 470
  ]
  edge [
    source 57
    target 471
  ]
  edge [
    source 57
    target 472
  ]
  edge [
    source 57
    target 473
  ]
  edge [
    source 57
    target 474
  ]
  edge [
    source 57
    target 475
  ]
  edge [
    source 57
    target 476
  ]
  edge [
    source 57
    target 477
  ]
  edge [
    source 57
    target 478
  ]
  edge [
    source 57
    target 479
  ]
  edge [
    source 57
    target 480
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 481
  ]
  edge [
    source 58
    target 482
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 483
  ]
  edge [
    source 60
    target 195
  ]
  edge [
    source 60
    target 484
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 485
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 486
  ]
  edge [
    source 63
    target 487
  ]
  edge [
    source 63
    target 488
  ]
  edge [
    source 63
    target 489
  ]
  edge [
    source 63
    target 490
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 491
  ]
  edge [
    source 64
    target 492
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 493
  ]
  edge [
    source 65
    target 494
  ]
  edge [
    source 65
    target 495
  ]
  edge [
    source 65
    target 496
  ]
  edge [
    source 65
    target 497
  ]
  edge [
    source 65
    target 498
  ]
  edge [
    source 65
    target 499
  ]
  edge [
    source 65
    target 500
  ]
  edge [
    source 65
    target 501
  ]
  edge [
    source 65
    target 502
  ]
  edge [
    source 65
    target 503
  ]
  edge [
    source 65
    target 504
  ]
  edge [
    source 65
    target 505
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 104
  ]
  edge [
    source 66
    target 506
  ]
  edge [
    source 66
    target 507
  ]
  edge [
    source 66
    target 508
  ]
  edge [
    source 66
    target 509
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 510
  ]
  edge [
    source 67
    target 511
  ]
  edge [
    source 67
    target 512
  ]
  edge [
    source 67
    target 513
  ]
  edge [
    source 67
    target 514
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 515
  ]
  edge [
    source 68
    target 516
  ]
  edge [
    source 68
    target 517
  ]
  edge [
    source 68
    target 518
  ]
  edge [
    source 68
    target 519
  ]
  edge [
    source 68
    target 520
  ]
  edge [
    source 68
    target 521
  ]
  edge [
    source 68
    target 522
  ]
  edge [
    source 68
    target 523
  ]
  edge [
    source 68
    target 524
  ]
  edge [
    source 68
    target 525
  ]
  edge [
    source 68
    target 526
  ]
  edge [
    source 68
    target 527
  ]
  edge [
    source 68
    target 528
  ]
  edge [
    source 68
    target 529
  ]
  edge [
    source 68
    target 530
  ]
  edge [
    source 68
    target 531
  ]
  edge [
    source 68
    target 532
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 533
  ]
  edge [
    source 69
    target 534
  ]
  edge [
    source 69
    target 535
  ]
  edge [
    source 69
    target 536
  ]
  edge [
    source 69
    target 537
  ]
  edge [
    source 69
    target 538
  ]
  edge [
    source 69
    target 539
  ]
  edge [
    source 69
    target 196
  ]
  edge [
    source 69
    target 540
  ]
  edge [
    source 69
    target 541
  ]
  edge [
    source 69
    target 542
  ]
  edge [
    source 69
    target 543
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 430
  ]
  edge [
    source 70
    target 544
  ]
  edge [
    source 70
    target 545
  ]
  edge [
    source 70
    target 546
  ]
  edge [
    source 70
    target 547
  ]
  edge [
    source 70
    target 548
  ]
  edge [
    source 70
    target 549
  ]
  edge [
    source 70
    target 550
  ]
  edge [
    source 70
    target 551
  ]
  edge [
    source 70
    target 552
  ]
  edge [
    source 70
    target 553
  ]
  edge [
    source 70
    target 554
  ]
  edge [
    source 70
    target 555
  ]
  edge [
    source 70
    target 556
  ]
  edge [
    source 70
    target 557
  ]
  edge [
    source 70
    target 558
  ]
  edge [
    source 70
    target 559
  ]
  edge [
    source 70
    target 560
  ]
  edge [
    source 70
    target 561
  ]
  edge [
    source 70
    target 431
  ]
  edge [
    source 70
    target 562
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 563
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 564
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 76
    target 565
  ]
  edge [
    source 76
    target 566
  ]
  edge [
    source 76
    target 567
  ]
  edge [
    source 76
    target 568
  ]
  edge [
    source 76
    target 569
  ]
  edge [
    source 76
    target 570
  ]
  edge [
    source 76
    target 571
  ]
  edge [
    source 76
    target 572
  ]
  edge [
    source 76
    target 573
  ]
]
