graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "zgadza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "rzadko"
    origin "text"
  ]
  node [
    id 3
    label "zgodzi&#263;"
  ]
  node [
    id 4
    label "assent"
  ]
  node [
    id 5
    label "zgadzanie"
  ]
  node [
    id 6
    label "zatrudnia&#263;"
  ]
  node [
    id 7
    label "czasami"
  ]
  node [
    id 8
    label "rzadki"
  ]
  node [
    id 9
    label "niezwykle"
  ]
  node [
    id 10
    label "lu&#378;ny"
  ]
  node [
    id 11
    label "thinly"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
]
