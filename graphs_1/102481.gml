graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.084507042253521
  density 0.014783737888322844
  graphCliqueNumber 3
  node [
    id 0
    label "towarzystwo"
    origin "text"
  ]
  node [
    id 1
    label "przyjaciel"
    origin "text"
  ]
  node [
    id 2
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 3
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "historyczny"
    origin "text"
  ]
  node [
    id 5
    label "spacer"
    origin "text"
  ]
  node [
    id 6
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 7
    label "cel"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "zwiedza&#263;"
    origin "text"
  ]
  node [
    id 10
    label "teren"
    origin "text"
  ]
  node [
    id 11
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 12
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 13
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 14
    label "bawe&#322;niany"
    origin "text"
  ]
  node [
    id 15
    label "zbi&#243;rka"
    origin "text"
  ]
  node [
    id 16
    label "przed"
    origin "text"
  ]
  node [
    id 17
    label "miejski"
    origin "text"
  ]
  node [
    id 18
    label "biblioteka"
    origin "text"
  ]
  node [
    id 19
    label "publiczny"
    origin "text"
  ]
  node [
    id 20
    label "przy"
    origin "text"
  ]
  node [
    id 21
    label "ula"
    origin "text"
  ]
  node [
    id 22
    label "listopadowy"
    origin "text"
  ]
  node [
    id 23
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 24
    label "obecno&#347;&#263;"
  ]
  node [
    id 25
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 26
    label "organizacja"
  ]
  node [
    id 27
    label "Eleusis"
  ]
  node [
    id 28
    label "asystencja"
  ]
  node [
    id 29
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 30
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 31
    label "grupa"
  ]
  node [
    id 32
    label "fabianie"
  ]
  node [
    id 33
    label "Chewra_Kadisza"
  ]
  node [
    id 34
    label "grono"
  ]
  node [
    id 35
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 36
    label "wi&#281;&#378;"
  ]
  node [
    id 37
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 38
    label "partnership"
  ]
  node [
    id 39
    label "Monar"
  ]
  node [
    id 40
    label "Rotary_International"
  ]
  node [
    id 41
    label "kochanek"
  ]
  node [
    id 42
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 43
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 44
    label "kum"
  ]
  node [
    id 45
    label "sympatyk"
  ]
  node [
    id 46
    label "bratnia_dusza"
  ]
  node [
    id 47
    label "amikus"
  ]
  node [
    id 48
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 49
    label "pobratymiec"
  ]
  node [
    id 50
    label "drogi"
  ]
  node [
    id 51
    label "mi&#281;sny"
  ]
  node [
    id 52
    label "invite"
  ]
  node [
    id 53
    label "ask"
  ]
  node [
    id 54
    label "oferowa&#263;"
  ]
  node [
    id 55
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 56
    label "dawny"
  ]
  node [
    id 57
    label "zgodny"
  ]
  node [
    id 58
    label "historycznie"
  ]
  node [
    id 59
    label "wiekopomny"
  ]
  node [
    id 60
    label "prawdziwy"
  ]
  node [
    id 61
    label "dziejowo"
  ]
  node [
    id 62
    label "prezentacja"
  ]
  node [
    id 63
    label "natural_process"
  ]
  node [
    id 64
    label "czynno&#347;&#263;"
  ]
  node [
    id 65
    label "ruch"
  ]
  node [
    id 66
    label "miejsce"
  ]
  node [
    id 67
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 68
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 69
    label "rzecz"
  ]
  node [
    id 70
    label "punkt"
  ]
  node [
    id 71
    label "thing"
  ]
  node [
    id 72
    label "rezultat"
  ]
  node [
    id 73
    label "si&#281;ga&#263;"
  ]
  node [
    id 74
    label "trwa&#263;"
  ]
  node [
    id 75
    label "stan"
  ]
  node [
    id 76
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 77
    label "stand"
  ]
  node [
    id 78
    label "mie&#263;_miejsce"
  ]
  node [
    id 79
    label "uczestniczy&#263;"
  ]
  node [
    id 80
    label "chodzi&#263;"
  ]
  node [
    id 81
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 82
    label "equal"
  ]
  node [
    id 83
    label "odwiedza&#263;"
  ]
  node [
    id 84
    label "go_steady"
  ]
  node [
    id 85
    label "zakres"
  ]
  node [
    id 86
    label "kontekst"
  ]
  node [
    id 87
    label "wymiar"
  ]
  node [
    id 88
    label "obszar"
  ]
  node [
    id 89
    label "krajobraz"
  ]
  node [
    id 90
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 91
    label "w&#322;adza"
  ]
  node [
    id 92
    label "nation"
  ]
  node [
    id 93
    label "przyroda"
  ]
  node [
    id 94
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 95
    label "miejsce_pracy"
  ]
  node [
    id 96
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 97
    label "partnerka"
  ]
  node [
    id 98
    label "czyn"
  ]
  node [
    id 99
    label "company"
  ]
  node [
    id 100
    label "zak&#322;adka"
  ]
  node [
    id 101
    label "firma"
  ]
  node [
    id 102
    label "instytut"
  ]
  node [
    id 103
    label "wyko&#324;czenie"
  ]
  node [
    id 104
    label "jednostka_organizacyjna"
  ]
  node [
    id 105
    label "umowa"
  ]
  node [
    id 106
    label "instytucja"
  ]
  node [
    id 107
    label "gospodarka"
  ]
  node [
    id 108
    label "przechowalnictwo"
  ]
  node [
    id 109
    label "uprzemys&#322;owienie"
  ]
  node [
    id 110
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 111
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 112
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 113
    label "uprzemys&#322;awianie"
  ]
  node [
    id 114
    label "naturalny"
  ]
  node [
    id 115
    label "materia&#322;owy"
  ]
  node [
    id 116
    label "kwestowanie"
  ]
  node [
    id 117
    label "apel"
  ]
  node [
    id 118
    label "koszyk&#243;wka"
  ]
  node [
    id 119
    label "recoil"
  ]
  node [
    id 120
    label "spotkanie"
  ]
  node [
    id 121
    label "kwestarz"
  ]
  node [
    id 122
    label "collection"
  ]
  node [
    id 123
    label "chwyt"
  ]
  node [
    id 124
    label "miejsko"
  ]
  node [
    id 125
    label "miastowy"
  ]
  node [
    id 126
    label "typowy"
  ]
  node [
    id 127
    label "pok&#243;j"
  ]
  node [
    id 128
    label "rewers"
  ]
  node [
    id 129
    label "informatorium"
  ]
  node [
    id 130
    label "kolekcja"
  ]
  node [
    id 131
    label "czytelnik"
  ]
  node [
    id 132
    label "budynek"
  ]
  node [
    id 133
    label "zbi&#243;r"
  ]
  node [
    id 134
    label "programowanie"
  ]
  node [
    id 135
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 136
    label "library"
  ]
  node [
    id 137
    label "czytelnia"
  ]
  node [
    id 138
    label "jawny"
  ]
  node [
    id 139
    label "upublicznienie"
  ]
  node [
    id 140
    label "upublicznianie"
  ]
  node [
    id 141
    label "publicznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 64
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
]
