graph [
  maxDegree 33
  minDegree 1
  meanDegree 9.96923076923077
  density 0.15576923076923077
  graphCliqueNumber 21
  node [
    id 0
    label "bardzo"
    origin "text"
  ]
  node [
    id 1
    label "sekunda"
    origin "text"
  ]
  node [
    id 2
    label "cent"
    origin "text"
  ]
  node [
    id 3
    label "nad"
    origin "text"
  ]
  node [
    id 4
    label "w_chuj"
  ]
  node [
    id 5
    label "minuta"
  ]
  node [
    id 6
    label "tercja"
  ]
  node [
    id 7
    label "milisekunda"
  ]
  node [
    id 8
    label "nanosekunda"
  ]
  node [
    id 9
    label "uk&#322;ad_SI"
  ]
  node [
    id 10
    label "mikrosekunda"
  ]
  node [
    id 11
    label "time"
  ]
  node [
    id 12
    label "jednostka_czasu"
  ]
  node [
    id 13
    label "jednostka"
  ]
  node [
    id 14
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 15
    label "moneta"
  ]
  node [
    id 16
    label "ustawa"
  ]
  node [
    id 17
    label "zeszyt"
  ]
  node [
    id 18
    label "dzie&#324;"
  ]
  node [
    id 19
    label "3"
  ]
  node [
    id 20
    label "pa&#378;dziernik"
  ]
  node [
    id 21
    label "2008"
  ]
  node [
    id 22
    label "rok"
  ]
  node [
    id 23
    label "ojciec"
  ]
  node [
    id 24
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 25
    label "informacja"
  ]
  node [
    id 26
    label "&#347;rodowisko"
  ]
  node [
    id 27
    label "i"
  ]
  node [
    id 28
    label "on"
  ]
  node [
    id 29
    label "ochrona"
  ]
  node [
    id 30
    label "udzia&#322;"
  ]
  node [
    id 31
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 32
    label "wyspa"
  ]
  node [
    id 33
    label "oraz"
  ]
  node [
    id 34
    label "ocena"
  ]
  node [
    id 35
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 36
    label "na"
  ]
  node [
    id 37
    label "dziennik"
  ]
  node [
    id 38
    label "u"
  ]
  node [
    id 39
    label "27"
  ]
  node [
    id 40
    label "marzec"
  ]
  node [
    id 41
    label "2003"
  ]
  node [
    id 42
    label "planowa&#263;"
  ]
  node [
    id 43
    label "zagospodarowa&#263;"
  ]
  node [
    id 44
    label "przestrzenny"
  ]
  node [
    id 45
    label "kodeks"
  ]
  node [
    id 46
    label "post&#281;powa&#263;"
  ]
  node [
    id 47
    label "administracyjny"
  ]
  node [
    id 48
    label "prezydent"
  ]
  node [
    id 49
    label "miasto"
  ]
  node [
    id 50
    label "Bia&#322;ystok"
  ]
  node [
    id 51
    label "BK"
  ]
  node [
    id 52
    label "202"
  ]
  node [
    id 53
    label "203"
  ]
  node [
    id 54
    label "miejski"
  ]
  node [
    id 55
    label "przedsi&#281;biorstwo"
  ]
  node [
    id 56
    label "energetyka"
  ]
  node [
    id 57
    label "cieplny"
  ]
  node [
    id 58
    label "sp&#243;&#322;ka"
  ]
  node [
    id 59
    label "urz&#261;d"
  ]
  node [
    id 60
    label "departament"
  ]
  node [
    id 61
    label "architektura"
  ]
  node [
    id 62
    label "samorz&#261;dowy"
  ]
  node [
    id 63
    label "kolegium"
  ]
  node [
    id 64
    label "odwo&#322;awczy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 16
    target 27
  ]
  edge [
    source 16
    target 28
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 30
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 16
    target 35
  ]
  edge [
    source 16
    target 36
  ]
  edge [
    source 16
    target 39
  ]
  edge [
    source 16
    target 40
  ]
  edge [
    source 16
    target 41
  ]
  edge [
    source 16
    target 42
  ]
  edge [
    source 16
    target 43
  ]
  edge [
    source 16
    target 44
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 17
    target 27
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 17
    target 29
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 17
    target 32
  ]
  edge [
    source 17
    target 33
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 35
  ]
  edge [
    source 17
    target 36
  ]
  edge [
    source 17
    target 39
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 17
    target 41
  ]
  edge [
    source 17
    target 42
  ]
  edge [
    source 17
    target 43
  ]
  edge [
    source 17
    target 44
  ]
  edge [
    source 17
    target 54
  ]
  edge [
    source 17
    target 55
  ]
  edge [
    source 17
    target 56
  ]
  edge [
    source 17
    target 57
  ]
  edge [
    source 17
    target 58
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 25
  ]
  edge [
    source 18
    target 26
  ]
  edge [
    source 18
    target 27
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 18
    target 30
  ]
  edge [
    source 18
    target 31
  ]
  edge [
    source 18
    target 32
  ]
  edge [
    source 18
    target 33
  ]
  edge [
    source 18
    target 34
  ]
  edge [
    source 18
    target 35
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 39
  ]
  edge [
    source 18
    target 40
  ]
  edge [
    source 18
    target 41
  ]
  edge [
    source 18
    target 42
  ]
  edge [
    source 18
    target 43
  ]
  edge [
    source 18
    target 44
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 19
    target 29
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 19
    target 31
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 19
    target 34
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 36
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 20
    target 29
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 20
    target 31
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 20
    target 33
  ]
  edge [
    source 20
    target 34
  ]
  edge [
    source 20
    target 35
  ]
  edge [
    source 20
    target 36
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 22
    target 31
  ]
  edge [
    source 22
    target 32
  ]
  edge [
    source 22
    target 33
  ]
  edge [
    source 22
    target 34
  ]
  edge [
    source 22
    target 35
  ]
  edge [
    source 22
    target 36
  ]
  edge [
    source 22
    target 39
  ]
  edge [
    source 22
    target 40
  ]
  edge [
    source 22
    target 41
  ]
  edge [
    source 22
    target 42
  ]
  edge [
    source 22
    target 43
  ]
  edge [
    source 22
    target 44
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 23
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 32
  ]
  edge [
    source 23
    target 33
  ]
  edge [
    source 23
    target 34
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 23
    target 39
  ]
  edge [
    source 23
    target 40
  ]
  edge [
    source 23
    target 41
  ]
  edge [
    source 23
    target 42
  ]
  edge [
    source 23
    target 43
  ]
  edge [
    source 23
    target 44
  ]
  edge [
    source 23
    target 54
  ]
  edge [
    source 23
    target 55
  ]
  edge [
    source 23
    target 56
  ]
  edge [
    source 23
    target 57
  ]
  edge [
    source 23
    target 58
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 25
    target 32
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 34
  ]
  edge [
    source 25
    target 35
  ]
  edge [
    source 25
    target 36
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 26
    target 32
  ]
  edge [
    source 26
    target 26
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 39
  ]
  edge [
    source 27
    target 40
  ]
  edge [
    source 27
    target 41
  ]
  edge [
    source 27
    target 42
  ]
  edge [
    source 27
    target 43
  ]
  edge [
    source 27
    target 44
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 28
    target 36
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 29
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 59
  ]
  edge [
    source 32
    target 54
  ]
  edge [
    source 32
    target 50
  ]
  edge [
    source 32
    target 62
  ]
  edge [
    source 32
    target 63
  ]
  edge [
    source 32
    target 64
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 39
    target 43
  ]
  edge [
    source 39
    target 44
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 40
    target 44
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 59
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 50
    target 62
  ]
  edge [
    source 50
    target 63
  ]
  edge [
    source 50
    target 64
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 54
    target 59
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 63
    target 64
  ]
]
