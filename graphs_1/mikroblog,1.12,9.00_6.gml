graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.13333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "dawno"
    origin "text"
  ]
  node [
    id 1
    label "taki"
    origin "text"
  ]
  node [
    id 2
    label "smutny"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "dawny"
  ]
  node [
    id 5
    label "ongi&#347;"
  ]
  node [
    id 6
    label "dawnie"
  ]
  node [
    id 7
    label "wcze&#347;niej"
  ]
  node [
    id 8
    label "d&#322;ugotrwale"
  ]
  node [
    id 9
    label "okre&#347;lony"
  ]
  node [
    id 10
    label "jaki&#347;"
  ]
  node [
    id 11
    label "z&#322;y"
  ]
  node [
    id 12
    label "smutno"
  ]
  node [
    id 13
    label "negatywny"
  ]
  node [
    id 14
    label "przykry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
]
