graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.1979166666666665
  density 0.01150741710296684
  graphCliqueNumber 4
  node [
    id 0
    label "kr&#243;l"
    origin "text"
  ]
  node [
    id 1
    label "zygmunt"
    origin "text"
  ]
  node [
    id 2
    label "pozwoli&#263;"
    origin "text"
  ]
  node [
    id 3
    label "adam"
    origin "text"
  ]
  node [
    id 4
    label "wzdowskiemu"
    origin "text"
  ]
  node [
    id 5
    label "zastawi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wiesia"
    origin "text"
  ]
  node [
    id 7
    label "kr&#243;lewski"
    origin "text"
  ]
  node [
    id 8
    label "strachocina"
    origin "text"
  ]
  node [
    id 9
    label "jan"
    origin "text"
  ]
  node [
    id 10
    label "bobola"
    origin "text"
  ]
  node [
    id 11
    label "pyasky"
    origin "text"
  ]
  node [
    id 12
    label "lipiec"
    origin "text"
  ]
  node [
    id 13
    label "zezwala&#263;"
    origin "text"
  ]
  node [
    id 14
    label "podkomorzy"
    origin "text"
  ]
  node [
    id 15
    label "wojskowy"
    origin "text"
  ]
  node [
    id 16
    label "sprzeda&#263;"
    origin "text"
  ]
  node [
    id 17
    label "za&#322;ugi"
    origin "text"
  ]
  node [
    id 18
    label "dla"
    origin "text"
  ]
  node [
    id 19
    label "olbracht"
    origin "text"
  ]
  node [
    id 20
    label "tywoni"
    origin "text"
  ]
  node [
    id 21
    label "jaros&#322;aw"
    origin "text"
  ]
  node [
    id 22
    label "rok"
    origin "text"
  ]
  node [
    id 23
    label "wzdowski"
    origin "text"
  ]
  node [
    id 24
    label "powierzy&#263;"
    origin "text"
  ]
  node [
    id 25
    label "pawe&#322;"
    origin "text"
  ]
  node [
    id 26
    label "dynowiczowi"
    origin "text"
  ]
  node [
    id 27
    label "osadzony"
    origin "text"
  ]
  node [
    id 28
    label "nowa"
    origin "text"
  ]
  node [
    id 29
    label "kmie&#263;"
    origin "text"
  ]
  node [
    id 30
    label "nowota&#324;cu"
    origin "text"
  ]
  node [
    id 31
    label "jaworniku"
    origin "text"
  ]
  node [
    id 32
    label "prawie"
    origin "text"
  ]
  node [
    id 33
    label "kmiecy"
    origin "text"
  ]
  node [
    id 34
    label "eta"
    origin "text"
  ]
  node [
    id 35
    label "habent"
    origin "text"
  ]
  node [
    id 36
    label "esse"
    origin "text"
  ]
  node [
    id 37
    label "obedientes"
    origin "text"
  ]
  node [
    id 38
    label "jure"
    origin "text"
  ]
  node [
    id 39
    label "kmethonico"
    origin "text"
  ]
  node [
    id 40
    label "nowothaniecz"
    origin "text"
  ]
  node [
    id 41
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 42
    label "Otton_III"
  ]
  node [
    id 43
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 44
    label "monarchista"
  ]
  node [
    id 45
    label "Syzyf"
  ]
  node [
    id 46
    label "turzyca"
  ]
  node [
    id 47
    label "Fryderyk_II_Wielki"
  ]
  node [
    id 48
    label "Zygmunt_III_Waza"
  ]
  node [
    id 49
    label "Aleksander_Wielki"
  ]
  node [
    id 50
    label "monarcha"
  ]
  node [
    id 51
    label "gruba_ryba"
  ]
  node [
    id 52
    label "August_III_Sas"
  ]
  node [
    id 53
    label "koronowa&#263;"
  ]
  node [
    id 54
    label "tytu&#322;"
  ]
  node [
    id 55
    label "Boles&#322;aw_Chrobry"
  ]
  node [
    id 56
    label "zwierz&#281;_futerkowe"
  ]
  node [
    id 57
    label "basileus"
  ]
  node [
    id 58
    label "Zygmunt_II_August"
  ]
  node [
    id 59
    label "Jan_Kazimierz"
  ]
  node [
    id 60
    label "Manuel_I_Szcz&#281;&#347;liwy"
  ]
  node [
    id 61
    label "Ludwik_XVI"
  ]
  node [
    id 62
    label "trusia"
  ]
  node [
    id 63
    label "Kazimierz_Wielki"
  ]
  node [
    id 64
    label "Tantal"
  ]
  node [
    id 65
    label "HP"
  ]
  node [
    id 66
    label "Edward_VII"
  ]
  node [
    id 67
    label "Jugurta"
  ]
  node [
    id 68
    label "Herod"
  ]
  node [
    id 69
    label "omyk"
  ]
  node [
    id 70
    label "Augiasz"
  ]
  node [
    id 71
    label "Stanis&#322;aw_August_Poniatowski"
  ]
  node [
    id 72
    label "zaj&#261;cowate"
  ]
  node [
    id 73
    label "Salomon"
  ]
  node [
    id 74
    label "figura_karciana"
  ]
  node [
    id 75
    label "baron"
  ]
  node [
    id 76
    label "Karol_Albert"
  ]
  node [
    id 77
    label "figura"
  ]
  node [
    id 78
    label "Artur"
  ]
  node [
    id 79
    label "kicaj"
  ]
  node [
    id 80
    label "Dawid"
  ]
  node [
    id 81
    label "kr&#243;lowa_matka"
  ]
  node [
    id 82
    label "koronowanie"
  ]
  node [
    id 83
    label "Henryk_IV"
  ]
  node [
    id 84
    label "Zygmunt_I_Stary"
  ]
  node [
    id 85
    label "pofolgowa&#263;"
  ]
  node [
    id 86
    label "assent"
  ]
  node [
    id 87
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 88
    label "leave"
  ]
  node [
    id 89
    label "uzna&#263;"
  ]
  node [
    id 90
    label "zas&#322;oni&#263;"
  ]
  node [
    id 91
    label "zasadzka"
  ]
  node [
    id 92
    label "wype&#322;ni&#263;"
  ]
  node [
    id 93
    label "ustawi&#263;"
  ]
  node [
    id 94
    label "rig"
  ]
  node [
    id 95
    label "umie&#347;ci&#263;"
  ]
  node [
    id 96
    label "przekaza&#263;"
  ]
  node [
    id 97
    label "lie"
  ]
  node [
    id 98
    label "omyli&#263;"
  ]
  node [
    id 99
    label "set"
  ]
  node [
    id 100
    label "popisowy"
  ]
  node [
    id 101
    label "po_kr&#243;lewsku"
  ]
  node [
    id 102
    label "kr&#243;lewsko"
  ]
  node [
    id 103
    label "majestatyczny"
  ]
  node [
    id 104
    label "mistrzowski"
  ]
  node [
    id 105
    label "wspania&#322;y"
  ]
  node [
    id 106
    label "wynios&#322;y"
  ]
  node [
    id 107
    label "miesi&#261;c"
  ]
  node [
    id 108
    label "authorize"
  ]
  node [
    id 109
    label "uznawa&#263;"
  ]
  node [
    id 110
    label "dostojnik"
  ]
  node [
    id 111
    label "szambelan"
  ]
  node [
    id 112
    label "komornik"
  ]
  node [
    id 113
    label "s&#261;d_podkomorski"
  ]
  node [
    id 114
    label "urz&#281;dnik_ziemski"
  ]
  node [
    id 115
    label "urz&#281;dnik_nadworny"
  ]
  node [
    id 116
    label "cz&#322;owiek"
  ]
  node [
    id 117
    label "rota"
  ]
  node [
    id 118
    label "zdemobilizowanie"
  ]
  node [
    id 119
    label "militarnie"
  ]
  node [
    id 120
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 121
    label "Gurkha"
  ]
  node [
    id 122
    label "demobilizowanie"
  ]
  node [
    id 123
    label "walcz&#261;cy"
  ]
  node [
    id 124
    label "harcap"
  ]
  node [
    id 125
    label "&#380;o&#322;dowy"
  ]
  node [
    id 126
    label "mundurowy"
  ]
  node [
    id 127
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 128
    label "zdemobilizowa&#263;"
  ]
  node [
    id 129
    label "typowy"
  ]
  node [
    id 130
    label "antybalistyczny"
  ]
  node [
    id 131
    label "specjalny"
  ]
  node [
    id 132
    label "podleg&#322;y"
  ]
  node [
    id 133
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 134
    label "elew"
  ]
  node [
    id 135
    label "so&#322;dat"
  ]
  node [
    id 136
    label "wojsko"
  ]
  node [
    id 137
    label "wojskowo"
  ]
  node [
    id 138
    label "demobilizowa&#263;"
  ]
  node [
    id 139
    label "zhandlowa&#263;"
  ]
  node [
    id 140
    label "odda&#263;"
  ]
  node [
    id 141
    label "zach&#281;ci&#263;"
  ]
  node [
    id 142
    label "give_birth"
  ]
  node [
    id 143
    label "zdradzi&#263;"
  ]
  node [
    id 144
    label "op&#281;dzi&#263;"
  ]
  node [
    id 145
    label "sell"
  ]
  node [
    id 146
    label "stulecie"
  ]
  node [
    id 147
    label "kalendarz"
  ]
  node [
    id 148
    label "czas"
  ]
  node [
    id 149
    label "pora_roku"
  ]
  node [
    id 150
    label "cykl_astronomiczny"
  ]
  node [
    id 151
    label "p&#243;&#322;rocze"
  ]
  node [
    id 152
    label "grupa"
  ]
  node [
    id 153
    label "kwarta&#322;"
  ]
  node [
    id 154
    label "kurs"
  ]
  node [
    id 155
    label "jubileusz"
  ]
  node [
    id 156
    label "lata"
  ]
  node [
    id 157
    label "martwy_sezon"
  ]
  node [
    id 158
    label "charge"
  ]
  node [
    id 159
    label "ufa&#263;"
  ]
  node [
    id 160
    label "confide"
  ]
  node [
    id 161
    label "entrust"
  ]
  node [
    id 162
    label "wyzna&#263;"
  ]
  node [
    id 163
    label "consign"
  ]
  node [
    id 164
    label "zleci&#263;"
  ]
  node [
    id 165
    label "wi&#281;zie&#324;"
  ]
  node [
    id 166
    label "gwiazda"
  ]
  node [
    id 167
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 168
    label "ch&#322;op"
  ]
  node [
    id 169
    label "pro&#347;ciuch"
  ]
  node [
    id 170
    label "wie&#347;niak"
  ]
  node [
    id 171
    label "kmieci"
  ]
  node [
    id 172
    label "Janowy"
  ]
  node [
    id 173
    label "Bobola"
  ]
  node [
    id 174
    label "de"
  ]
  node [
    id 175
    label "Pyasky"
  ]
  node [
    id 176
    label "adamowy"
  ]
  node [
    id 177
    label "Wzdowskiemu"
  ]
  node [
    id 178
    label "albo"
  ]
  node [
    id 179
    label "Wzdowski"
  ]
  node [
    id 180
    label "Dynowiczowi"
  ]
  node [
    id 181
    label "Adam"
  ]
  node [
    id 182
    label "miko&#322;aj"
  ]
  node [
    id 183
    label "bal"
  ]
  node [
    id 184
    label "Jakub"
  ]
  node [
    id 185
    label "Freiberger"
  ]
  node [
    id 186
    label "Cadan"
  ]
  node [
    id 187
    label "Jan"
  ]
  node [
    id 188
    label "Wzdowskiego"
  ]
  node [
    id 189
    label "Stanis&#322;aw"
  ]
  node [
    id 190
    label "Hieronim"
  ]
  node [
    id 191
    label "sta&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 27
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 29
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 39
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 147
  ]
  edge [
    source 22
    target 148
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 22
    target 150
  ]
  edge [
    source 22
    target 151
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 22
    target 155
  ]
  edge [
    source 22
    target 107
  ]
  edge [
    source 22
    target 156
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 140
  ]
  edge [
    source 24
    target 158
  ]
  edge [
    source 24
    target 159
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 24
    target 161
  ]
  edge [
    source 24
    target 162
  ]
  edge [
    source 24
    target 163
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 165
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 166
  ]
  edge [
    source 28
    target 167
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 116
  ]
  edge [
    source 29
    target 168
  ]
  edge [
    source 29
    target 169
  ]
  edge [
    source 29
    target 170
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 171
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 172
    target 174
  ]
  edge [
    source 172
    target 175
  ]
  edge [
    source 173
    target 174
  ]
  edge [
    source 173
    target 175
  ]
  edge [
    source 174
    target 175
  ]
  edge [
    source 174
    target 184
  ]
  edge [
    source 174
    target 185
  ]
  edge [
    source 174
    target 186
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 179
    target 181
  ]
  edge [
    source 179
    target 187
  ]
  edge [
    source 181
    target 188
  ]
  edge [
    source 182
    target 183
  ]
  edge [
    source 183
    target 189
  ]
  edge [
    source 184
    target 185
  ]
  edge [
    source 184
    target 186
  ]
  edge [
    source 185
    target 186
  ]
  edge [
    source 190
    target 191
  ]
]
