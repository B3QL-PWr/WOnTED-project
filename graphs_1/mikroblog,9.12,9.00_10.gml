graph [
  maxDegree 33
  minDegree 1
  meanDegree 2
  density 0.015037593984962405
  graphCliqueNumber 2
  node [
    id 0
    label "fajnie"
    origin "text"
  ]
  node [
    id 1
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "rozowepaski"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "luby"
    origin "text"
  ]
  node [
    id 5
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "klub"
    origin "text"
  ]
  node [
    id 7
    label "weekendzik"
    origin "text"
  ]
  node [
    id 8
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 9
    label "napi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "spokojnie"
    origin "text"
  ]
  node [
    id 12
    label "piwko"
    origin "text"
  ]
  node [
    id 13
    label "w&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 14
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 15
    label "film"
    origin "text"
  ]
  node [
    id 16
    label "trzeba"
    origin "text"
  ]
  node [
    id 17
    label "spina&#263;"
    origin "text"
  ]
  node [
    id 18
    label "pr&#281;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 19
    label "fajny"
  ]
  node [
    id 20
    label "klawo"
  ]
  node [
    id 21
    label "dobrze"
  ]
  node [
    id 22
    label "byczo"
  ]
  node [
    id 23
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 24
    label "czu&#263;"
  ]
  node [
    id 25
    label "need"
  ]
  node [
    id 26
    label "hide"
  ]
  node [
    id 27
    label "support"
  ]
  node [
    id 28
    label "kochanek"
  ]
  node [
    id 29
    label "wybranek"
  ]
  node [
    id 30
    label "kochanie"
  ]
  node [
    id 31
    label "umi&#322;owany"
  ]
  node [
    id 32
    label "drogi"
  ]
  node [
    id 33
    label "proceed"
  ]
  node [
    id 34
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 35
    label "bangla&#263;"
  ]
  node [
    id 36
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 37
    label "by&#263;"
  ]
  node [
    id 38
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 39
    label "run"
  ]
  node [
    id 40
    label "tryb"
  ]
  node [
    id 41
    label "p&#322;ywa&#263;"
  ]
  node [
    id 42
    label "continue"
  ]
  node [
    id 43
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 44
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 45
    label "przebiega&#263;"
  ]
  node [
    id 46
    label "mie&#263;_miejsce"
  ]
  node [
    id 47
    label "wk&#322;ada&#263;"
  ]
  node [
    id 48
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 49
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 50
    label "para"
  ]
  node [
    id 51
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 52
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 53
    label "krok"
  ]
  node [
    id 54
    label "str&#243;j"
  ]
  node [
    id 55
    label "bywa&#263;"
  ]
  node [
    id 56
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 57
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 58
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 59
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 60
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 61
    label "dziama&#263;"
  ]
  node [
    id 62
    label "stara&#263;_si&#281;"
  ]
  node [
    id 63
    label "carry"
  ]
  node [
    id 64
    label "society"
  ]
  node [
    id 65
    label "jakobini"
  ]
  node [
    id 66
    label "klubista"
  ]
  node [
    id 67
    label "stowarzyszenie"
  ]
  node [
    id 68
    label "lokal"
  ]
  node [
    id 69
    label "od&#322;am"
  ]
  node [
    id 70
    label "siedziba"
  ]
  node [
    id 71
    label "bar"
  ]
  node [
    id 72
    label "free"
  ]
  node [
    id 73
    label "wolno"
  ]
  node [
    id 74
    label "cichy"
  ]
  node [
    id 75
    label "przyjemnie"
  ]
  node [
    id 76
    label "bezproblemowo"
  ]
  node [
    id 77
    label "spokojny"
  ]
  node [
    id 78
    label "bronek"
  ]
  node [
    id 79
    label "zacz&#261;&#263;"
  ]
  node [
    id 80
    label "nastawi&#263;"
  ]
  node [
    id 81
    label "prosecute"
  ]
  node [
    id 82
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 83
    label "impersonate"
  ]
  node [
    id 84
    label "umie&#347;ci&#263;"
  ]
  node [
    id 85
    label "obejrze&#263;"
  ]
  node [
    id 86
    label "incorporate"
  ]
  node [
    id 87
    label "draw"
  ]
  node [
    id 88
    label "uruchomi&#263;"
  ]
  node [
    id 89
    label "dokoptowa&#263;"
  ]
  node [
    id 90
    label "jako&#347;"
  ]
  node [
    id 91
    label "charakterystyczny"
  ]
  node [
    id 92
    label "ciekawy"
  ]
  node [
    id 93
    label "jako_tako"
  ]
  node [
    id 94
    label "dziwny"
  ]
  node [
    id 95
    label "niez&#322;y"
  ]
  node [
    id 96
    label "przyzwoity"
  ]
  node [
    id 97
    label "rozbieg&#243;wka"
  ]
  node [
    id 98
    label "block"
  ]
  node [
    id 99
    label "odczula&#263;"
  ]
  node [
    id 100
    label "blik"
  ]
  node [
    id 101
    label "rola"
  ]
  node [
    id 102
    label "trawiarnia"
  ]
  node [
    id 103
    label "b&#322;ona"
  ]
  node [
    id 104
    label "filmoteka"
  ]
  node [
    id 105
    label "sztuka"
  ]
  node [
    id 106
    label "muza"
  ]
  node [
    id 107
    label "odczuli&#263;"
  ]
  node [
    id 108
    label "klatka"
  ]
  node [
    id 109
    label "odczulenie"
  ]
  node [
    id 110
    label "emulsja_fotograficzna"
  ]
  node [
    id 111
    label "animatronika"
  ]
  node [
    id 112
    label "dorobek"
  ]
  node [
    id 113
    label "odczulanie"
  ]
  node [
    id 114
    label "scena"
  ]
  node [
    id 115
    label "czo&#322;&#243;wka"
  ]
  node [
    id 116
    label "ty&#322;&#243;wka"
  ]
  node [
    id 117
    label "napisy"
  ]
  node [
    id 118
    label "photograph"
  ]
  node [
    id 119
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 120
    label "postprodukcja"
  ]
  node [
    id 121
    label "sklejarka"
  ]
  node [
    id 122
    label "anamorfoza"
  ]
  node [
    id 123
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 124
    label "ta&#347;ma"
  ]
  node [
    id 125
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 126
    label "uj&#281;cie"
  ]
  node [
    id 127
    label "trza"
  ]
  node [
    id 128
    label "necessity"
  ]
  node [
    id 129
    label "zaciska&#263;"
  ]
  node [
    id 130
    label "pin"
  ]
  node [
    id 131
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 132
    label "tighten"
  ]
  node [
    id 133
    label "scala&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
]
