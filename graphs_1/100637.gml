graph [
  maxDegree 6
  minDegree 1
  meanDegree 2.1666666666666665
  density 0.19696969696969696
  graphCliqueNumber 4
  node [
    id 0
    label "koran"
    origin "text"
  ]
  node [
    id 1
    label "dym"
    origin "text"
  ]
  node [
    id 2
    label "mieszanina"
  ]
  node [
    id 3
    label "zamieszki"
  ]
  node [
    id 4
    label "gry&#378;&#263;"
  ]
  node [
    id 5
    label "gaz"
  ]
  node [
    id 6
    label "aggro"
  ]
  node [
    id 7
    label "Koran"
  ]
  node [
    id 8
    label "dymi&#263;"
  ]
  node [
    id 9
    label "Surah"
  ]
  node [
    id 10
    label "Ada"
  ]
  node [
    id 11
    label "Dukh&#226;n"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
]
