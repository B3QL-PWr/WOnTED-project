graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.07692307692307693
  graphCliqueNumber 2
  node [
    id 0
    label "madka"
    origin "text"
  ]
  node [
    id 1
    label "natarcie"
    origin "text"
  ]
  node [
    id 2
    label "gardzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "bekazpodludzi"
    origin "text"
  ]
  node [
    id 4
    label "madki"
    origin "text"
  ]
  node [
    id 5
    label "facebookcontent"
    origin "text"
  ]
  node [
    id 6
    label "zwierzaczek"
    origin "text"
  ]
  node [
    id 7
    label "zagrywka"
  ]
  node [
    id 8
    label "zrobienie"
  ]
  node [
    id 9
    label "manewr"
  ]
  node [
    id 10
    label "posmarowanie"
  ]
  node [
    id 11
    label "foray"
  ]
  node [
    id 12
    label "nast&#261;pienie"
  ]
  node [
    id 13
    label "walka"
  ]
  node [
    id 14
    label "wyskoczenie_z_g&#281;b&#261;"
  ]
  node [
    id 15
    label "wdarcie_si&#281;"
  ]
  node [
    id 16
    label "progress"
  ]
  node [
    id 17
    label "powiedzenie"
  ]
  node [
    id 18
    label "time"
  ]
  node [
    id 19
    label "skrytykowanie"
  ]
  node [
    id 20
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 21
    label "ignore"
  ]
  node [
    id 22
    label "traktowa&#263;"
  ]
  node [
    id 23
    label "lekcewa&#380;y&#263;"
  ]
  node [
    id 24
    label "mie&#263;_za_nic"
  ]
  node [
    id 25
    label "reject"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
]
