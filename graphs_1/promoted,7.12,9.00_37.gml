graph [
  maxDegree 43
  minDegree 1
  meanDegree 1.9801980198019802
  density 0.019801980198019802
  graphCliqueNumber 2
  node [
    id 0
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 1
    label "zbrojeniowy"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 4
    label "wszystko"
    origin "text"
  ]
  node [
    id 5
    label "stworzy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "dobry"
    origin "text"
  ]
  node [
    id 7
    label "odpowiednik"
    origin "text"
  ]
  node [
    id 8
    label "rosyjski"
    origin "text"
  ]
  node [
    id 9
    label "przeciwlotniczy"
    origin "text"
  ]
  node [
    id 10
    label "system"
    origin "text"
  ]
  node [
    id 11
    label "rakietowo"
    origin "text"
  ]
  node [
    id 12
    label "artyleryjski"
    origin "text"
  ]
  node [
    id 13
    label "gospodarka"
  ]
  node [
    id 14
    label "przechowalnictwo"
  ]
  node [
    id 15
    label "uprzemys&#322;owienie"
  ]
  node [
    id 16
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 17
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 18
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 19
    label "uprzemys&#322;awianie"
  ]
  node [
    id 20
    label "czyj&#347;"
  ]
  node [
    id 21
    label "m&#261;&#380;"
  ]
  node [
    id 22
    label "lock"
  ]
  node [
    id 23
    label "absolut"
  ]
  node [
    id 24
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "przygotowa&#263;"
  ]
  node [
    id 26
    label "specjalista_od_public_relations"
  ]
  node [
    id 27
    label "create"
  ]
  node [
    id 28
    label "zrobi&#263;"
  ]
  node [
    id 29
    label "wizerunek"
  ]
  node [
    id 30
    label "pomy&#347;lny"
  ]
  node [
    id 31
    label "skuteczny"
  ]
  node [
    id 32
    label "moralny"
  ]
  node [
    id 33
    label "korzystny"
  ]
  node [
    id 34
    label "odpowiedni"
  ]
  node [
    id 35
    label "zwrot"
  ]
  node [
    id 36
    label "dobrze"
  ]
  node [
    id 37
    label "pozytywny"
  ]
  node [
    id 38
    label "grzeczny"
  ]
  node [
    id 39
    label "powitanie"
  ]
  node [
    id 40
    label "mi&#322;y"
  ]
  node [
    id 41
    label "dobroczynny"
  ]
  node [
    id 42
    label "pos&#322;uszny"
  ]
  node [
    id 43
    label "ca&#322;y"
  ]
  node [
    id 44
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 45
    label "czw&#243;rka"
  ]
  node [
    id 46
    label "spokojny"
  ]
  node [
    id 47
    label "&#347;mieszny"
  ]
  node [
    id 48
    label "drogi"
  ]
  node [
    id 49
    label "odmiana"
  ]
  node [
    id 50
    label "po_rosyjsku"
  ]
  node [
    id 51
    label "j&#281;zyk"
  ]
  node [
    id 52
    label "wielkoruski"
  ]
  node [
    id 53
    label "kacapski"
  ]
  node [
    id 54
    label "Russian"
  ]
  node [
    id 55
    label "rusek"
  ]
  node [
    id 56
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 57
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 58
    label "bojowy"
  ]
  node [
    id 59
    label "model"
  ]
  node [
    id 60
    label "sk&#322;ad"
  ]
  node [
    id 61
    label "zachowanie"
  ]
  node [
    id 62
    label "podstawa"
  ]
  node [
    id 63
    label "porz&#261;dek"
  ]
  node [
    id 64
    label "Android"
  ]
  node [
    id 65
    label "przyn&#281;ta"
  ]
  node [
    id 66
    label "jednostka_geologiczna"
  ]
  node [
    id 67
    label "metoda"
  ]
  node [
    id 68
    label "podsystem"
  ]
  node [
    id 69
    label "p&#322;&#243;d"
  ]
  node [
    id 70
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 71
    label "s&#261;d"
  ]
  node [
    id 72
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 73
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 74
    label "j&#261;dro"
  ]
  node [
    id 75
    label "eratem"
  ]
  node [
    id 76
    label "ryba"
  ]
  node [
    id 77
    label "pulpit"
  ]
  node [
    id 78
    label "struktura"
  ]
  node [
    id 79
    label "spos&#243;b"
  ]
  node [
    id 80
    label "oddzia&#322;"
  ]
  node [
    id 81
    label "usenet"
  ]
  node [
    id 82
    label "o&#347;"
  ]
  node [
    id 83
    label "oprogramowanie"
  ]
  node [
    id 84
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 85
    label "poj&#281;cie"
  ]
  node [
    id 86
    label "w&#281;dkarstwo"
  ]
  node [
    id 87
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 88
    label "Leopard"
  ]
  node [
    id 89
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 90
    label "systemik"
  ]
  node [
    id 91
    label "rozprz&#261;c"
  ]
  node [
    id 92
    label "cybernetyk"
  ]
  node [
    id 93
    label "konstelacja"
  ]
  node [
    id 94
    label "doktryna"
  ]
  node [
    id 95
    label "net"
  ]
  node [
    id 96
    label "zbi&#243;r"
  ]
  node [
    id 97
    label "method"
  ]
  node [
    id 98
    label "systemat"
  ]
  node [
    id 99
    label "Pancyr"
  ]
  node [
    id 100
    label "S1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 99
    target 100
  ]
]
