graph [
  maxDegree 47
  minDegree 1
  meanDegree 2.034934497816594
  density 0.00892515130621313
  graphCliqueNumber 2
  node [
    id 0
    label "mie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "nadzieja"
    origin "text"
  ]
  node [
    id 2
    label "nasze"
    origin "text"
  ]
  node [
    id 3
    label "dziecko"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "chrzci&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pierwsza"
    origin "text"
  ]
  node [
    id 7
    label "komunia"
    origin "text"
  ]
  node [
    id 8
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 11
    label "gdybyby&#263;"
    origin "text"
  ]
  node [
    id 12
    label "mieszka&#263;"
    origin "text"
  ]
  node [
    id 13
    label "polska"
    origin "text"
  ]
  node [
    id 14
    label "wiedzie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 15
    label "jaki"
    origin "text"
  ]
  node [
    id 16
    label "lekarz"
    origin "text"
  ]
  node [
    id 17
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "wszystko"
    origin "text"
  ]
  node [
    id 19
    label "trudny"
    origin "text"
  ]
  node [
    id 20
    label "ogarn&#261;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "tak"
    origin "text"
  ]
  node [
    id 22
    label "blisko"
    origin "text"
  ]
  node [
    id 23
    label "podobny"
    origin "text"
  ]
  node [
    id 24
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 25
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 26
    label "zupe&#322;nie"
    origin "text"
  ]
  node [
    id 27
    label "inny"
    origin "text"
  ]
  node [
    id 28
    label "&#347;wiate&#322;ko_w_tunelu"
  ]
  node [
    id 29
    label "wierzy&#263;"
  ]
  node [
    id 30
    label "szansa"
  ]
  node [
    id 31
    label "oczekiwanie"
  ]
  node [
    id 32
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 33
    label "spoczywa&#263;"
  ]
  node [
    id 34
    label "potomstwo"
  ]
  node [
    id 35
    label "organizm"
  ]
  node [
    id 36
    label "sraluch"
  ]
  node [
    id 37
    label "utulanie"
  ]
  node [
    id 38
    label "pediatra"
  ]
  node [
    id 39
    label "dzieciarnia"
  ]
  node [
    id 40
    label "m&#322;odziak"
  ]
  node [
    id 41
    label "dzieciak"
  ]
  node [
    id 42
    label "utula&#263;"
  ]
  node [
    id 43
    label "potomek"
  ]
  node [
    id 44
    label "entliczek-pentliczek"
  ]
  node [
    id 45
    label "pedofil"
  ]
  node [
    id 46
    label "m&#322;odzik"
  ]
  node [
    id 47
    label "cz&#322;owieczek"
  ]
  node [
    id 48
    label "zwierz&#281;"
  ]
  node [
    id 49
    label "niepe&#322;noletni"
  ]
  node [
    id 50
    label "fledgling"
  ]
  node [
    id 51
    label "utuli&#263;"
  ]
  node [
    id 52
    label "utulenie"
  ]
  node [
    id 53
    label "si&#281;ga&#263;"
  ]
  node [
    id 54
    label "trwa&#263;"
  ]
  node [
    id 55
    label "obecno&#347;&#263;"
  ]
  node [
    id 56
    label "stan"
  ]
  node [
    id 57
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 58
    label "stand"
  ]
  node [
    id 59
    label "mie&#263;_miejsce"
  ]
  node [
    id 60
    label "uczestniczy&#263;"
  ]
  node [
    id 61
    label "chodzi&#263;"
  ]
  node [
    id 62
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 63
    label "equal"
  ]
  node [
    id 64
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 65
    label "usprawiedliwia&#263;"
  ]
  node [
    id 66
    label "do&#347;wiadcza&#263;"
  ]
  node [
    id 67
    label "rozcie&#324;cza&#263;"
  ]
  node [
    id 68
    label "robi&#263;"
  ]
  node [
    id 69
    label "nazywa&#263;"
  ]
  node [
    id 70
    label "opija&#263;"
  ]
  node [
    id 71
    label "dub"
  ]
  node [
    id 72
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 73
    label "przedstawia&#263;"
  ]
  node [
    id 74
    label "godzina"
  ]
  node [
    id 75
    label "zwi&#261;zek"
  ]
  node [
    id 76
    label "zgoda"
  ]
  node [
    id 77
    label "przedmiot"
  ]
  node [
    id 78
    label "bia&#322;y_tydzie&#324;"
  ]
  node [
    id 79
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 80
    label "sakrament"
  ]
  node [
    id 81
    label "praktyka"
  ]
  node [
    id 82
    label "msza"
  ]
  node [
    id 83
    label "association"
  ]
  node [
    id 84
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 85
    label "represent"
  ]
  node [
    id 86
    label "zajmowa&#263;"
  ]
  node [
    id 87
    label "sta&#263;"
  ]
  node [
    id 88
    label "przebywa&#263;"
  ]
  node [
    id 89
    label "room"
  ]
  node [
    id 90
    label "panowa&#263;"
  ]
  node [
    id 91
    label "fall"
  ]
  node [
    id 92
    label "zbada&#263;"
  ]
  node [
    id 93
    label "medyk"
  ]
  node [
    id 94
    label "lekarze"
  ]
  node [
    id 95
    label "pracownik"
  ]
  node [
    id 96
    label "eskulap"
  ]
  node [
    id 97
    label "Hipokrates"
  ]
  node [
    id 98
    label "Mesmer"
  ]
  node [
    id 99
    label "Galen"
  ]
  node [
    id 100
    label "dokt&#243;r"
  ]
  node [
    id 101
    label "opu&#347;ci&#263;"
  ]
  node [
    id 102
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 103
    label "proceed"
  ]
  node [
    id 104
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 105
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 106
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 107
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 108
    label "zacz&#261;&#263;"
  ]
  node [
    id 109
    label "zmieni&#263;"
  ]
  node [
    id 110
    label "zosta&#263;"
  ]
  node [
    id 111
    label "sail"
  ]
  node [
    id 112
    label "leave"
  ]
  node [
    id 113
    label "uda&#263;_si&#281;"
  ]
  node [
    id 114
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 115
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 116
    label "zrobi&#263;"
  ]
  node [
    id 117
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 118
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 119
    label "przyj&#261;&#263;"
  ]
  node [
    id 120
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 121
    label "become"
  ]
  node [
    id 122
    label "play_along"
  ]
  node [
    id 123
    label "travel"
  ]
  node [
    id 124
    label "lock"
  ]
  node [
    id 125
    label "absolut"
  ]
  node [
    id 126
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 127
    label "wymagaj&#261;cy"
  ]
  node [
    id 128
    label "skomplikowany"
  ]
  node [
    id 129
    label "k&#322;opotliwy"
  ]
  node [
    id 130
    label "ci&#281;&#380;ko"
  ]
  node [
    id 131
    label "visit"
  ]
  node [
    id 132
    label "spowodowa&#263;"
  ]
  node [
    id 133
    label "spotka&#263;"
  ]
  node [
    id 134
    label "manipulate"
  ]
  node [
    id 135
    label "environment"
  ]
  node [
    id 136
    label "otoczy&#263;"
  ]
  node [
    id 137
    label "dotkn&#261;&#263;"
  ]
  node [
    id 138
    label "involve"
  ]
  node [
    id 139
    label "dok&#322;adnie"
  ]
  node [
    id 140
    label "bliski"
  ]
  node [
    id 141
    label "silnie"
  ]
  node [
    id 142
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 143
    label "podobnie"
  ]
  node [
    id 144
    label "upodabnianie_si&#281;"
  ]
  node [
    id 145
    label "zasymilowanie"
  ]
  node [
    id 146
    label "drugi"
  ]
  node [
    id 147
    label "taki"
  ]
  node [
    id 148
    label "upodobnienie"
  ]
  node [
    id 149
    label "charakterystyczny"
  ]
  node [
    id 150
    label "przypominanie"
  ]
  node [
    id 151
    label "asymilowanie"
  ]
  node [
    id 152
    label "upodobnienie_si&#281;"
  ]
  node [
    id 153
    label "pisa&#263;"
  ]
  node [
    id 154
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 155
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 156
    label "ssanie"
  ]
  node [
    id 157
    label "po_koroniarsku"
  ]
  node [
    id 158
    label "but"
  ]
  node [
    id 159
    label "m&#243;wienie"
  ]
  node [
    id 160
    label "rozumie&#263;"
  ]
  node [
    id 161
    label "formacja_geologiczna"
  ]
  node [
    id 162
    label "rozumienie"
  ]
  node [
    id 163
    label "m&#243;wi&#263;"
  ]
  node [
    id 164
    label "gramatyka"
  ]
  node [
    id 165
    label "pype&#263;"
  ]
  node [
    id 166
    label "makroglosja"
  ]
  node [
    id 167
    label "kawa&#322;ek"
  ]
  node [
    id 168
    label "artykulator"
  ]
  node [
    id 169
    label "kultura_duchowa"
  ]
  node [
    id 170
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 171
    label "jama_ustna"
  ]
  node [
    id 172
    label "spos&#243;b"
  ]
  node [
    id 173
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 174
    label "przet&#322;umaczenie"
  ]
  node [
    id 175
    label "t&#322;umaczenie"
  ]
  node [
    id 176
    label "language"
  ]
  node [
    id 177
    label "jeniec"
  ]
  node [
    id 178
    label "organ"
  ]
  node [
    id 179
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 180
    label "pismo"
  ]
  node [
    id 181
    label "formalizowanie"
  ]
  node [
    id 182
    label "fonetyka"
  ]
  node [
    id 183
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 184
    label "wokalizm"
  ]
  node [
    id 185
    label "liza&#263;"
  ]
  node [
    id 186
    label "s&#322;ownictwo"
  ]
  node [
    id 187
    label "napisa&#263;"
  ]
  node [
    id 188
    label "formalizowa&#263;"
  ]
  node [
    id 189
    label "natural_language"
  ]
  node [
    id 190
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 191
    label "stylik"
  ]
  node [
    id 192
    label "konsonantyzm"
  ]
  node [
    id 193
    label "urz&#261;dzenie"
  ]
  node [
    id 194
    label "ssa&#263;"
  ]
  node [
    id 195
    label "kod"
  ]
  node [
    id 196
    label "lizanie"
  ]
  node [
    id 197
    label "asymilowa&#263;"
  ]
  node [
    id 198
    label "wapniak"
  ]
  node [
    id 199
    label "dwun&#243;g"
  ]
  node [
    id 200
    label "polifag"
  ]
  node [
    id 201
    label "wz&#243;r"
  ]
  node [
    id 202
    label "profanum"
  ]
  node [
    id 203
    label "hominid"
  ]
  node [
    id 204
    label "homo_sapiens"
  ]
  node [
    id 205
    label "nasada"
  ]
  node [
    id 206
    label "podw&#322;adny"
  ]
  node [
    id 207
    label "ludzko&#347;&#263;"
  ]
  node [
    id 208
    label "os&#322;abianie"
  ]
  node [
    id 209
    label "mikrokosmos"
  ]
  node [
    id 210
    label "portrecista"
  ]
  node [
    id 211
    label "duch"
  ]
  node [
    id 212
    label "oddzia&#322;ywanie"
  ]
  node [
    id 213
    label "g&#322;owa"
  ]
  node [
    id 214
    label "osoba"
  ]
  node [
    id 215
    label "os&#322;abia&#263;"
  ]
  node [
    id 216
    label "figura"
  ]
  node [
    id 217
    label "Adam"
  ]
  node [
    id 218
    label "senior"
  ]
  node [
    id 219
    label "antropochoria"
  ]
  node [
    id 220
    label "posta&#263;"
  ]
  node [
    id 221
    label "kompletny"
  ]
  node [
    id 222
    label "wniwecz"
  ]
  node [
    id 223
    label "zupe&#322;ny"
  ]
  node [
    id 224
    label "kolejny"
  ]
  node [
    id 225
    label "inaczej"
  ]
  node [
    id 226
    label "r&#243;&#380;ny"
  ]
  node [
    id 227
    label "inszy"
  ]
  node [
    id 228
    label "osobno"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 92
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 17
    target 106
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 17
    target 108
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 22
    target 140
  ]
  edge [
    source 22
    target 141
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 23
    target 151
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 153
  ]
  edge [
    source 24
    target 154
  ]
  edge [
    source 24
    target 155
  ]
  edge [
    source 24
    target 156
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 77
  ]
  edge [
    source 24
    target 158
  ]
  edge [
    source 24
    target 159
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 24
    target 161
  ]
  edge [
    source 24
    target 162
  ]
  edge [
    source 24
    target 163
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 24
    target 167
  ]
  edge [
    source 24
    target 168
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 170
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 24
    target 178
  ]
  edge [
    source 24
    target 179
  ]
  edge [
    source 24
    target 180
  ]
  edge [
    source 24
    target 181
  ]
  edge [
    source 24
    target 182
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 24
    target 189
  ]
  edge [
    source 24
    target 190
  ]
  edge [
    source 24
    target 191
  ]
  edge [
    source 24
    target 192
  ]
  edge [
    source 24
    target 193
  ]
  edge [
    source 24
    target 194
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 196
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 197
  ]
  edge [
    source 25
    target 198
  ]
  edge [
    source 25
    target 199
  ]
  edge [
    source 25
    target 200
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 25
    target 202
  ]
  edge [
    source 25
    target 203
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 151
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 221
  ]
  edge [
    source 26
    target 222
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 27
    target 228
  ]
]
