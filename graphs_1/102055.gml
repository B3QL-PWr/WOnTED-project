graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 1
    label "mama"
    origin "text"
  ]
  node [
    id 2
    label "forum"
    origin "text"
  ]
  node [
    id 3
    label "pismak"
    origin "text"
  ]
  node [
    id 4
    label "koniec"
  ]
  node [
    id 5
    label "conclusion"
  ]
  node [
    id 6
    label "coating"
  ]
  node [
    id 7
    label "runda"
  ]
  node [
    id 8
    label "matczysko"
  ]
  node [
    id 9
    label "macierz"
  ]
  node [
    id 10
    label "przodkini"
  ]
  node [
    id 11
    label "Matka_Boska"
  ]
  node [
    id 12
    label "macocha"
  ]
  node [
    id 13
    label "matka_zast&#281;pcza"
  ]
  node [
    id 14
    label "stara"
  ]
  node [
    id 15
    label "rodzice"
  ]
  node [
    id 16
    label "rodzic"
  ]
  node [
    id 17
    label "s&#261;d"
  ]
  node [
    id 18
    label "plac"
  ]
  node [
    id 19
    label "miejsce"
  ]
  node [
    id 20
    label "grupa_dyskusyjna"
  ]
  node [
    id 21
    label "grupa"
  ]
  node [
    id 22
    label "przestrze&#324;"
  ]
  node [
    id 23
    label "agora"
  ]
  node [
    id 24
    label "bazylika"
  ]
  node [
    id 25
    label "konferencja"
  ]
  node [
    id 26
    label "strona"
  ]
  node [
    id 27
    label "portal"
  ]
  node [
    id 28
    label "gryzipi&#243;rek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 28
  ]
]
