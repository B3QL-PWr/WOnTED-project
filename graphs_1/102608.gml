graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "wyrok"
    origin "text"
  ]
  node [
    id 1
    label "trybuna&#322;"
    origin "text"
  ]
  node [
    id 2
    label "konstytucyjny"
    origin "text"
  ]
  node [
    id 3
    label "orzeczenie"
  ]
  node [
    id 4
    label "order"
  ]
  node [
    id 5
    label "wydarzenie"
  ]
  node [
    id 6
    label "kara"
  ]
  node [
    id 7
    label "judgment"
  ]
  node [
    id 8
    label "sentencja"
  ]
  node [
    id 9
    label "Europejski_Trybuna&#322;_Praw_Cz&#322;owieka"
  ]
  node [
    id 10
    label "s&#261;d"
  ]
  node [
    id 11
    label "ustawowy"
  ]
  node [
    id 12
    label "konstytucyjnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
]
