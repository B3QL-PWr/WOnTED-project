graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.019607843137255
  density 0.019996117258784703
  graphCliqueNumber 3
  node [
    id 0
    label "przyznanie"
    origin "text"
  ]
  node [
    id 1
    label "dotacja"
    origin "text"
  ]
  node [
    id 2
    label "podmiot"
    origin "text"
  ]
  node [
    id 3
    label "prowadz&#261;cy"
    origin "text"
  ]
  node [
    id 4
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "po&#380;ytek"
    origin "text"
  ]
  node [
    id 6
    label "publiczny"
    origin "text"
  ]
  node [
    id 7
    label "wsparcie"
    origin "text"
  ]
  node [
    id 8
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 9
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 10
    label "hipoterapeutycznego"
    origin "text"
  ]
  node [
    id 11
    label "gdynia"
    origin "text"
  ]
  node [
    id 12
    label "recognition"
  ]
  node [
    id 13
    label "danie"
  ]
  node [
    id 14
    label "stwierdzenie"
  ]
  node [
    id 15
    label "confession"
  ]
  node [
    id 16
    label "oznajmienie"
  ]
  node [
    id 17
    label "dop&#322;ata"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "byt"
  ]
  node [
    id 20
    label "organizacja"
  ]
  node [
    id 21
    label "prawo"
  ]
  node [
    id 22
    label "nauka_prawa"
  ]
  node [
    id 23
    label "osobowo&#347;&#263;"
  ]
  node [
    id 24
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 25
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 26
    label "dzia&#322;anie"
  ]
  node [
    id 27
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 28
    label "absolutorium"
  ]
  node [
    id 29
    label "activity"
  ]
  node [
    id 30
    label "py&#322;ek"
  ]
  node [
    id 31
    label "korzy&#347;&#263;"
  ]
  node [
    id 32
    label "surowiec"
  ]
  node [
    id 33
    label "spad&#378;"
  ]
  node [
    id 34
    label "dobro"
  ]
  node [
    id 35
    label "zaleta"
  ]
  node [
    id 36
    label "nektar"
  ]
  node [
    id 37
    label "jawny"
  ]
  node [
    id 38
    label "upublicznienie"
  ]
  node [
    id 39
    label "upublicznianie"
  ]
  node [
    id 40
    label "publicznie"
  ]
  node [
    id 41
    label "comfort"
  ]
  node [
    id 42
    label "u&#322;atwienie"
  ]
  node [
    id 43
    label "doch&#243;d"
  ]
  node [
    id 44
    label "oparcie"
  ]
  node [
    id 45
    label "telefon_zaufania"
  ]
  node [
    id 46
    label "dar"
  ]
  node [
    id 47
    label "zapomoga"
  ]
  node [
    id 48
    label "pocieszenie"
  ]
  node [
    id 49
    label "darowizna"
  ]
  node [
    id 50
    label "pomoc"
  ]
  node [
    id 51
    label "&#347;rodek"
  ]
  node [
    id 52
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 53
    label "support"
  ]
  node [
    id 54
    label "robienie"
  ]
  node [
    id 55
    label "przywodzenie"
  ]
  node [
    id 56
    label "prowadzanie"
  ]
  node [
    id 57
    label "ukierunkowywanie"
  ]
  node [
    id 58
    label "kszta&#322;towanie"
  ]
  node [
    id 59
    label "poprowadzenie"
  ]
  node [
    id 60
    label "wprowadzanie"
  ]
  node [
    id 61
    label "dysponowanie"
  ]
  node [
    id 62
    label "przeci&#261;ganie"
  ]
  node [
    id 63
    label "doprowadzanie"
  ]
  node [
    id 64
    label "wprowadzenie"
  ]
  node [
    id 65
    label "eksponowanie"
  ]
  node [
    id 66
    label "oprowadzenie"
  ]
  node [
    id 67
    label "trzymanie"
  ]
  node [
    id 68
    label "ta&#324;czenie"
  ]
  node [
    id 69
    label "przeci&#281;cie"
  ]
  node [
    id 70
    label "przewy&#380;szanie"
  ]
  node [
    id 71
    label "prowadzi&#263;"
  ]
  node [
    id 72
    label "aim"
  ]
  node [
    id 73
    label "czynno&#347;&#263;"
  ]
  node [
    id 74
    label "zwracanie"
  ]
  node [
    id 75
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 76
    label "przecinanie"
  ]
  node [
    id 77
    label "sterowanie"
  ]
  node [
    id 78
    label "drive"
  ]
  node [
    id 79
    label "kre&#347;lenie"
  ]
  node [
    id 80
    label "management"
  ]
  node [
    id 81
    label "dawanie"
  ]
  node [
    id 82
    label "oprowadzanie"
  ]
  node [
    id 83
    label "pozarz&#261;dzanie"
  ]
  node [
    id 84
    label "g&#243;rowanie"
  ]
  node [
    id 85
    label "linia_melodyczna"
  ]
  node [
    id 86
    label "granie"
  ]
  node [
    id 87
    label "doprowadzenie"
  ]
  node [
    id 88
    label "kierowanie"
  ]
  node [
    id 89
    label "zaprowadzanie"
  ]
  node [
    id 90
    label "lead"
  ]
  node [
    id 91
    label "powodowanie"
  ]
  node [
    id 92
    label "krzywa"
  ]
  node [
    id 93
    label "miejsce"
  ]
  node [
    id 94
    label "Hollywood"
  ]
  node [
    id 95
    label "zal&#261;&#380;ek"
  ]
  node [
    id 96
    label "otoczenie"
  ]
  node [
    id 97
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 98
    label "center"
  ]
  node [
    id 99
    label "instytucja"
  ]
  node [
    id 100
    label "skupisko"
  ]
  node [
    id 101
    label "warunki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 10
    target 11
  ]
]
