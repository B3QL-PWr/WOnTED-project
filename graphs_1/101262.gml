graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.2727272727272727
  density 0.12727272727272726
  graphCliqueNumber 3
  node [
    id 0
    label "dinner"
    origin "text"
  ]
  node [
    id 1
    label "plain"
    origin "text"
  ]
  node [
    id 2
    label "Dinner"
  ]
  node [
    id 3
    label "Plain"
  ]
  node [
    id 4
    label "Alpy"
  ]
  node [
    id 5
    label "australijski"
  ]
  node [
    id 6
    label "Mount"
  ]
  node [
    id 7
    label "Hotham"
  ]
  node [
    id 8
    label "Great"
  ]
  node [
    id 9
    label "Alpine"
  ]
  node [
    id 10
    label "Road"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
]
