graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9322033898305084
  density 0.03331385154880187
  graphCliqueNumber 2
  node [
    id 0
    label "dlaczego"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wi&#281;to"
    origin "text"
  ]
  node [
    id 2
    label "barb&#243;rka"
    origin "text"
  ]
  node [
    id 3
    label "niema"
    origin "text"
  ]
  node [
    id 4
    label "jeszcze"
    origin "text"
  ]
  node [
    id 5
    label "gor&#261;cy"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 7
    label "hajera"
    origin "text"
  ]
  node [
    id 8
    label "przodowy"
    origin "text"
  ]
  node [
    id 9
    label "g&#243;rny"
    origin "text"
  ]
  node [
    id 10
    label "&#347;l&#261;sk"
    origin "text"
  ]
  node [
    id 11
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 14
    label "Nowy_Rok"
  ]
  node [
    id 15
    label "Godowe_&#346;wi&#281;to"
  ]
  node [
    id 16
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 17
    label "Barb&#243;rka"
  ]
  node [
    id 18
    label "ramadan"
  ]
  node [
    id 19
    label "impreza"
  ]
  node [
    id 20
    label "ci&#261;gle"
  ]
  node [
    id 21
    label "na_gor&#261;co"
  ]
  node [
    id 22
    label "szczery"
  ]
  node [
    id 23
    label "&#380;arki"
  ]
  node [
    id 24
    label "zdecydowany"
  ]
  node [
    id 25
    label "rozpalenie_si&#281;"
  ]
  node [
    id 26
    label "gor&#261;co"
  ]
  node [
    id 27
    label "g&#322;&#281;boki"
  ]
  node [
    id 28
    label "sensacyjny"
  ]
  node [
    id 29
    label "seksowny"
  ]
  node [
    id 30
    label "rozpalanie_si&#281;"
  ]
  node [
    id 31
    label "&#347;wie&#380;y"
  ]
  node [
    id 32
    label "ciep&#322;y"
  ]
  node [
    id 33
    label "stresogenny"
  ]
  node [
    id 34
    label "serdeczny"
  ]
  node [
    id 35
    label "doros&#322;y"
  ]
  node [
    id 36
    label "wiele"
  ]
  node [
    id 37
    label "dorodny"
  ]
  node [
    id 38
    label "znaczny"
  ]
  node [
    id 39
    label "du&#380;o"
  ]
  node [
    id 40
    label "prawdziwy"
  ]
  node [
    id 41
    label "niema&#322;o"
  ]
  node [
    id 42
    label "wa&#380;ny"
  ]
  node [
    id 43
    label "rozwini&#281;ty"
  ]
  node [
    id 44
    label "g&#243;rnik_do&#322;owy"
  ]
  node [
    id 45
    label "szlachetny"
  ]
  node [
    id 46
    label "maksymalnie"
  ]
  node [
    id 47
    label "wy&#380;ni"
  ]
  node [
    id 48
    label "powa&#380;ny"
  ]
  node [
    id 49
    label "maxymalny"
  ]
  node [
    id 50
    label "g&#243;rnie"
  ]
  node [
    id 51
    label "maksymalizowanie"
  ]
  node [
    id 52
    label "oderwany"
  ]
  node [
    id 53
    label "wznio&#347;le"
  ]
  node [
    id 54
    label "graniczny"
  ]
  node [
    id 55
    label "zmaksymalizowanie"
  ]
  node [
    id 56
    label "wysoki"
  ]
  node [
    id 57
    label "G&#243;rny"
  ]
  node [
    id 58
    label "&#346;l&#261;sk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 57
    target 58
  ]
]
