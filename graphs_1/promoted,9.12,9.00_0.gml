graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9607843137254901
  density 0.0392156862745098
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "oburzony"
    origin "text"
  ]
  node [
    id 2
    label "google"
    origin "text"
  ]
  node [
    id 3
    label "podkre&#347;la&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#380;e&#324;ski"
    origin "text"
  ]
  node [
    id 5
    label "tw&#243;r"
    origin "text"
  ]
  node [
    id 6
    label "wyrazowy"
    origin "text"
  ]
  node [
    id 7
    label "psycholo&#380;ka"
    origin "text"
  ]
  node [
    id 8
    label "minister"
    origin "text"
  ]
  node [
    id 9
    label "chirur&#380;ka"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;ga&#263;"
  ]
  node [
    id 11
    label "trwa&#263;"
  ]
  node [
    id 12
    label "obecno&#347;&#263;"
  ]
  node [
    id 13
    label "stan"
  ]
  node [
    id 14
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 15
    label "stand"
  ]
  node [
    id 16
    label "mie&#263;_miejsce"
  ]
  node [
    id 17
    label "uczestniczy&#263;"
  ]
  node [
    id 18
    label "chodzi&#263;"
  ]
  node [
    id 19
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 20
    label "equal"
  ]
  node [
    id 21
    label "poruszony"
  ]
  node [
    id 22
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 23
    label "underscore"
  ]
  node [
    id 24
    label "rysowa&#263;"
  ]
  node [
    id 25
    label "signalize"
  ]
  node [
    id 26
    label "oznacza&#263;"
  ]
  node [
    id 27
    label "kreska"
  ]
  node [
    id 28
    label "damsko"
  ]
  node [
    id 29
    label "&#380;e&#324;sko"
  ]
  node [
    id 30
    label "typowy"
  ]
  node [
    id 31
    label "po_damsku"
  ]
  node [
    id 32
    label "odr&#281;bny"
  ]
  node [
    id 33
    label "stosowny"
  ]
  node [
    id 34
    label "bia&#322;og&#322;owski"
  ]
  node [
    id 35
    label "organizm"
  ]
  node [
    id 36
    label "cia&#322;o"
  ]
  node [
    id 37
    label "istota"
  ]
  node [
    id 38
    label "part"
  ]
  node [
    id 39
    label "work"
  ]
  node [
    id 40
    label "organ"
  ]
  node [
    id 41
    label "rzecz"
  ]
  node [
    id 42
    label "przyroda"
  ]
  node [
    id 43
    label "substance"
  ]
  node [
    id 44
    label "rezultat"
  ]
  node [
    id 45
    label "p&#322;&#243;d"
  ]
  node [
    id 46
    label "element_anatomiczny"
  ]
  node [
    id 47
    label "Goebbels"
  ]
  node [
    id 48
    label "Sto&#322;ypin"
  ]
  node [
    id 49
    label "rz&#261;d"
  ]
  node [
    id 50
    label "dostojnik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
]
