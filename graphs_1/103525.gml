graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "student"
    origin "text"
  ]
  node [
    id 1
    label "tutor"
  ]
  node [
    id 2
    label "akademik"
  ]
  node [
    id 3
    label "immatrykulowanie"
  ]
  node [
    id 4
    label "s&#322;uchacz"
  ]
  node [
    id 5
    label "immatrykulowa&#263;"
  ]
  node [
    id 6
    label "absolwent"
  ]
  node [
    id 7
    label "indeks"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
]
