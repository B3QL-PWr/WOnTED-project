graph [
  maxDegree 45
  minDegree 1
  meanDegree 2.1890109890109892
  density 0.00482161010795372
  graphCliqueNumber 3
  node [
    id 0
    label "astrologia"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ani"
    origin "text"
  ]
  node [
    id 3
    label "&#322;atwy"
    origin "text"
  ]
  node [
    id 4
    label "rozrywka"
    origin "text"
  ]
  node [
    id 5
    label "te&#380;"
    origin "text"
  ]
  node [
    id 6
    label "zabawa"
    origin "text"
  ]
  node [
    id 7
    label "grunt"
    origin "text"
  ]
  node [
    id 8
    label "rzecz"
    origin "text"
  ]
  node [
    id 9
    label "wcale"
    origin "text"
  ]
  node [
    id 10
    label "rozleg&#322;y"
    origin "text"
  ]
  node [
    id 11
    label "skomplikowany"
    origin "text"
  ]
  node [
    id 12
    label "system"
    origin "text"
  ]
  node [
    id 13
    label "wiedza"
    origin "text"
  ]
  node [
    id 14
    label "czego"
    origin "text"
  ]
  node [
    id 15
    label "u&#347;wiadamia&#263;"
    origin "text"
  ]
  node [
    id 16
    label "siebie"
    origin "text"
  ]
  node [
    id 17
    label "og&#243;&#322;"
    origin "text"
  ]
  node [
    id 18
    label "liczny"
    origin "text"
  ]
  node [
    id 19
    label "rzesza"
    origin "text"
  ]
  node [
    id 20
    label "amator"
    origin "text"
  ]
  node [
    id 21
    label "bezsensowny"
    origin "text"
  ]
  node [
    id 22
    label "horoskop"
    origin "text"
  ]
  node [
    id 23
    label "gazetowe"
    origin "text"
  ]
  node [
    id 24
    label "maja"
    origin "text"
  ]
  node [
    id 25
    label "bowiem"
    origin "text"
  ]
  node [
    id 26
    label "nic"
    origin "text"
  ]
  node [
    id 27
    label "wsp&#243;lny"
    origin "text"
  ]
  node [
    id 28
    label "ur&#261;ga&#263;"
    origin "text"
  ]
  node [
    id 29
    label "najstarszy"
    origin "text"
  ]
  node [
    id 30
    label "znany"
    origin "text"
  ]
  node [
    id 31
    label "obszar"
    origin "text"
  ]
  node [
    id 32
    label "usystematyzowa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "zg&#322;&#281;bia&#263;"
    origin "text"
  ]
  node [
    id 34
    label "przez"
    origin "text"
  ]
  node [
    id 35
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 36
    label "umys&#322;"
    origin "text"
  ]
  node [
    id 37
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 38
    label "bez"
    origin "text"
  ]
  node [
    id 39
    label "wyj&#261;tek"
    origin "text"
  ]
  node [
    id 40
    label "epoka"
    origin "text"
  ]
  node [
    id 41
    label "historyczny"
    origin "text"
  ]
  node [
    id 42
    label "uprawianie"
    origin "text"
  ]
  node [
    id 43
    label "polega&#263;"
    origin "text"
  ]
  node [
    id 44
    label "oblicza&#263;"
    origin "text"
  ]
  node [
    id 45
    label "nad"
    origin "text"
  ]
  node [
    id 46
    label "wszystko"
    origin "text"
  ]
  node [
    id 47
    label "interpretacja"
    origin "text"
  ]
  node [
    id 48
    label "wymaga&#263;"
    origin "text"
  ]
  node [
    id 49
    label "d&#322;ugotrwa&#322;y"
    origin "text"
  ]
  node [
    id 50
    label "studia"
    origin "text"
  ]
  node [
    id 51
    label "umiej&#281;tny"
    origin "text"
  ]
  node [
    id 52
    label "&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 53
    label "element"
    origin "text"
  ]
  node [
    id 54
    label "psychologiczny"
    origin "text"
  ]
  node [
    id 55
    label "astronomiczny"
    origin "text"
  ]
  node [
    id 56
    label "biologiczny"
    origin "text"
  ]
  node [
    id 57
    label "medyczny"
    origin "text"
  ]
  node [
    id 58
    label "socjologiczny"
    origin "text"
  ]
  node [
    id 59
    label "przed"
    origin "text"
  ]
  node [
    id 60
    label "wszyscy"
    origin "text"
  ]
  node [
    id 61
    label "wieloletni"
    origin "text"
  ]
  node [
    id 62
    label "praktyka"
    origin "text"
  ]
  node [
    id 63
    label "astrology"
  ]
  node [
    id 64
    label "astrologia_naturalna"
  ]
  node [
    id 65
    label "wr&#243;&#380;biarstwo"
  ]
  node [
    id 66
    label "si&#281;ga&#263;"
  ]
  node [
    id 67
    label "trwa&#263;"
  ]
  node [
    id 68
    label "obecno&#347;&#263;"
  ]
  node [
    id 69
    label "stan"
  ]
  node [
    id 70
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 71
    label "stand"
  ]
  node [
    id 72
    label "mie&#263;_miejsce"
  ]
  node [
    id 73
    label "uczestniczy&#263;"
  ]
  node [
    id 74
    label "chodzi&#263;"
  ]
  node [
    id 75
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 76
    label "equal"
  ]
  node [
    id 77
    label "letki"
  ]
  node [
    id 78
    label "przyjemny"
  ]
  node [
    id 79
    label "snadny"
  ]
  node [
    id 80
    label "prosty"
  ]
  node [
    id 81
    label "&#322;atwo"
  ]
  node [
    id 82
    label "&#322;acny"
  ]
  node [
    id 83
    label "czasoumilacz"
  ]
  node [
    id 84
    label "game"
  ]
  node [
    id 85
    label "odpoczynek"
  ]
  node [
    id 86
    label "wodzirej"
  ]
  node [
    id 87
    label "nabawienie_si&#281;"
  ]
  node [
    id 88
    label "cecha"
  ]
  node [
    id 89
    label "gambling"
  ]
  node [
    id 90
    label "taniec"
  ]
  node [
    id 91
    label "impreza"
  ]
  node [
    id 92
    label "igraszka"
  ]
  node [
    id 93
    label "igra"
  ]
  node [
    id 94
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 95
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 96
    label "ubaw"
  ]
  node [
    id 97
    label "chwyt"
  ]
  node [
    id 98
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 99
    label "za&#322;o&#380;enie"
  ]
  node [
    id 100
    label "powierzchnia"
  ]
  node [
    id 101
    label "glinowa&#263;"
  ]
  node [
    id 102
    label "pr&#243;chnica"
  ]
  node [
    id 103
    label "podglebie"
  ]
  node [
    id 104
    label "glinowanie"
  ]
  node [
    id 105
    label "litosfera"
  ]
  node [
    id 106
    label "zasadzenie"
  ]
  node [
    id 107
    label "documentation"
  ]
  node [
    id 108
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 109
    label "teren"
  ]
  node [
    id 110
    label "ryzosfera"
  ]
  node [
    id 111
    label "czynnik_produkcji"
  ]
  node [
    id 112
    label "zasadzi&#263;"
  ]
  node [
    id 113
    label "glej"
  ]
  node [
    id 114
    label "martwica"
  ]
  node [
    id 115
    label "geosystem"
  ]
  node [
    id 116
    label "przestrze&#324;"
  ]
  node [
    id 117
    label "dotleni&#263;"
  ]
  node [
    id 118
    label "penetrator"
  ]
  node [
    id 119
    label "punkt_odniesienia"
  ]
  node [
    id 120
    label "kompleks_sorpcyjny"
  ]
  node [
    id 121
    label "podstawowy"
  ]
  node [
    id 122
    label "plantowa&#263;"
  ]
  node [
    id 123
    label "podk&#322;ad"
  ]
  node [
    id 124
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 125
    label "dno"
  ]
  node [
    id 126
    label "obiekt"
  ]
  node [
    id 127
    label "temat"
  ]
  node [
    id 128
    label "istota"
  ]
  node [
    id 129
    label "wpa&#347;&#263;"
  ]
  node [
    id 130
    label "wpadanie"
  ]
  node [
    id 131
    label "przedmiot"
  ]
  node [
    id 132
    label "wpada&#263;"
  ]
  node [
    id 133
    label "kultura"
  ]
  node [
    id 134
    label "przyroda"
  ]
  node [
    id 135
    label "mienie"
  ]
  node [
    id 136
    label "object"
  ]
  node [
    id 137
    label "wpadni&#281;cie"
  ]
  node [
    id 138
    label "ni_chuja"
  ]
  node [
    id 139
    label "ca&#322;kiem"
  ]
  node [
    id 140
    label "zupe&#322;nie"
  ]
  node [
    id 141
    label "szeroki"
  ]
  node [
    id 142
    label "rozlegle"
  ]
  node [
    id 143
    label "skomplikowanie"
  ]
  node [
    id 144
    label "trudny"
  ]
  node [
    id 145
    label "model"
  ]
  node [
    id 146
    label "sk&#322;ad"
  ]
  node [
    id 147
    label "zachowanie"
  ]
  node [
    id 148
    label "podstawa"
  ]
  node [
    id 149
    label "porz&#261;dek"
  ]
  node [
    id 150
    label "Android"
  ]
  node [
    id 151
    label "przyn&#281;ta"
  ]
  node [
    id 152
    label "jednostka_geologiczna"
  ]
  node [
    id 153
    label "metoda"
  ]
  node [
    id 154
    label "podsystem"
  ]
  node [
    id 155
    label "p&#322;&#243;d"
  ]
  node [
    id 156
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 157
    label "s&#261;d"
  ]
  node [
    id 158
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 159
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 160
    label "j&#261;dro"
  ]
  node [
    id 161
    label "eratem"
  ]
  node [
    id 162
    label "ryba"
  ]
  node [
    id 163
    label "pulpit"
  ]
  node [
    id 164
    label "struktura"
  ]
  node [
    id 165
    label "spos&#243;b"
  ]
  node [
    id 166
    label "oddzia&#322;"
  ]
  node [
    id 167
    label "usenet"
  ]
  node [
    id 168
    label "o&#347;"
  ]
  node [
    id 169
    label "oprogramowanie"
  ]
  node [
    id 170
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 171
    label "poj&#281;cie"
  ]
  node [
    id 172
    label "w&#281;dkarstwo"
  ]
  node [
    id 173
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 174
    label "Leopard"
  ]
  node [
    id 175
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 176
    label "systemik"
  ]
  node [
    id 177
    label "rozprz&#261;c"
  ]
  node [
    id 178
    label "cybernetyk"
  ]
  node [
    id 179
    label "konstelacja"
  ]
  node [
    id 180
    label "doktryna"
  ]
  node [
    id 181
    label "net"
  ]
  node [
    id 182
    label "zbi&#243;r"
  ]
  node [
    id 183
    label "method"
  ]
  node [
    id 184
    label "systemat"
  ]
  node [
    id 185
    label "pozwolenie"
  ]
  node [
    id 186
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 187
    label "wykszta&#322;cenie"
  ]
  node [
    id 188
    label "zaawansowanie"
  ]
  node [
    id 189
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 190
    label "intelekt"
  ]
  node [
    id 191
    label "cognition"
  ]
  node [
    id 192
    label "explain"
  ]
  node [
    id 193
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 194
    label "informowa&#263;"
  ]
  node [
    id 195
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 196
    label "pole"
  ]
  node [
    id 197
    label "kastowo&#347;&#263;"
  ]
  node [
    id 198
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 199
    label "ludzie_pracy"
  ]
  node [
    id 200
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 201
    label "community"
  ]
  node [
    id 202
    label "Fremeni"
  ]
  node [
    id 203
    label "status"
  ]
  node [
    id 204
    label "pozaklasowy"
  ]
  node [
    id 205
    label "aspo&#322;eczny"
  ]
  node [
    id 206
    label "pe&#322;ny"
  ]
  node [
    id 207
    label "ilo&#347;&#263;"
  ]
  node [
    id 208
    label "uwarstwienie"
  ]
  node [
    id 209
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 210
    label "zlewanie_si&#281;"
  ]
  node [
    id 211
    label "elita"
  ]
  node [
    id 212
    label "cywilizacja"
  ]
  node [
    id 213
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 214
    label "klasa"
  ]
  node [
    id 215
    label "rojenie_si&#281;"
  ]
  node [
    id 216
    label "cz&#281;sty"
  ]
  node [
    id 217
    label "licznie"
  ]
  node [
    id 218
    label "demofobia"
  ]
  node [
    id 219
    label "najazd"
  ]
  node [
    id 220
    label "grupa"
  ]
  node [
    id 221
    label "entuzjasta"
  ]
  node [
    id 222
    label "cz&#322;owiek"
  ]
  node [
    id 223
    label "sympatyk"
  ]
  node [
    id 224
    label "klient"
  ]
  node [
    id 225
    label "rekreacja"
  ]
  node [
    id 226
    label "ch&#281;tny"
  ]
  node [
    id 227
    label "sportowiec"
  ]
  node [
    id 228
    label "nieprofesjonalista"
  ]
  node [
    id 229
    label "bezsensownie"
  ]
  node [
    id 230
    label "ja&#322;owy"
  ]
  node [
    id 231
    label "nielogiczny"
  ]
  node [
    id 232
    label "bezsensowy"
  ]
  node [
    id 233
    label "niezrozumia&#322;y"
  ]
  node [
    id 234
    label "nieuzasadniony"
  ]
  node [
    id 235
    label "nieskuteczny"
  ]
  node [
    id 236
    label "horoscope"
  ]
  node [
    id 237
    label "sygnifikator"
  ]
  node [
    id 238
    label "przepowiednia"
  ]
  node [
    id 239
    label "znak_zodiaku"
  ]
  node [
    id 240
    label "wedyzm"
  ]
  node [
    id 241
    label "energia"
  ]
  node [
    id 242
    label "buddyzm"
  ]
  node [
    id 243
    label "miernota"
  ]
  node [
    id 244
    label "g&#243;wno"
  ]
  node [
    id 245
    label "love"
  ]
  node [
    id 246
    label "brak"
  ]
  node [
    id 247
    label "ciura"
  ]
  node [
    id 248
    label "spolny"
  ]
  node [
    id 249
    label "jeden"
  ]
  node [
    id 250
    label "sp&#243;lny"
  ]
  node [
    id 251
    label "wsp&#243;lnie"
  ]
  node [
    id 252
    label "uwsp&#243;lnianie"
  ]
  node [
    id 253
    label "uwsp&#243;lnienie"
  ]
  node [
    id 254
    label "przynosi&#263;"
  ]
  node [
    id 255
    label "tease"
  ]
  node [
    id 256
    label "wymy&#347;la&#263;"
  ]
  node [
    id 257
    label "mistreat"
  ]
  node [
    id 258
    label "kpi&#263;"
  ]
  node [
    id 259
    label "obra&#380;a&#263;"
  ]
  node [
    id 260
    label "wielki"
  ]
  node [
    id 261
    label "rozpowszechnianie"
  ]
  node [
    id 262
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 263
    label "pas_planetoid"
  ]
  node [
    id 264
    label "wsch&#243;d"
  ]
  node [
    id 265
    label "Neogea"
  ]
  node [
    id 266
    label "holarktyka"
  ]
  node [
    id 267
    label "Rakowice"
  ]
  node [
    id 268
    label "Kosowo"
  ]
  node [
    id 269
    label "Syberia_Wschodnia"
  ]
  node [
    id 270
    label "wymiar"
  ]
  node [
    id 271
    label "p&#243;&#322;noc"
  ]
  node [
    id 272
    label "akrecja"
  ]
  node [
    id 273
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 274
    label "terytorium"
  ]
  node [
    id 275
    label "antroposfera"
  ]
  node [
    id 276
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 277
    label "po&#322;udnie"
  ]
  node [
    id 278
    label "zach&#243;d"
  ]
  node [
    id 279
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 280
    label "Olszanica"
  ]
  node [
    id 281
    label "Syberia_Zachodnia"
  ]
  node [
    id 282
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 283
    label "Ruda_Pabianicka"
  ]
  node [
    id 284
    label "Notogea"
  ]
  node [
    id 285
    label "&#321;&#281;g"
  ]
  node [
    id 286
    label "Antarktyka"
  ]
  node [
    id 287
    label "Piotrowo"
  ]
  node [
    id 288
    label "Zab&#322;ocie"
  ]
  node [
    id 289
    label "zakres"
  ]
  node [
    id 290
    label "miejsce"
  ]
  node [
    id 291
    label "Pow&#261;zki"
  ]
  node [
    id 292
    label "Arktyka"
  ]
  node [
    id 293
    label "Ludwin&#243;w"
  ]
  node [
    id 294
    label "Zabu&#380;e"
  ]
  node [
    id 295
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 296
    label "Kresy_Zachodnie"
  ]
  node [
    id 297
    label "systematize"
  ]
  node [
    id 298
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 299
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 300
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 301
    label "escalate"
  ]
  node [
    id 302
    label "read"
  ]
  node [
    id 303
    label "doros&#322;y"
  ]
  node [
    id 304
    label "wiele"
  ]
  node [
    id 305
    label "dorodny"
  ]
  node [
    id 306
    label "znaczny"
  ]
  node [
    id 307
    label "du&#380;o"
  ]
  node [
    id 308
    label "prawdziwy"
  ]
  node [
    id 309
    label "niema&#322;o"
  ]
  node [
    id 310
    label "wa&#380;ny"
  ]
  node [
    id 311
    label "rozwini&#281;ty"
  ]
  node [
    id 312
    label "pami&#281;&#263;"
  ]
  node [
    id 313
    label "wn&#281;trze"
  ]
  node [
    id 314
    label "pomieszanie_si&#281;"
  ]
  node [
    id 315
    label "wyobra&#378;nia"
  ]
  node [
    id 316
    label "jaki&#347;"
  ]
  node [
    id 317
    label "ki&#347;&#263;"
  ]
  node [
    id 318
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 319
    label "krzew"
  ]
  node [
    id 320
    label "pi&#380;maczkowate"
  ]
  node [
    id 321
    label "pestkowiec"
  ]
  node [
    id 322
    label "kwiat"
  ]
  node [
    id 323
    label "owoc"
  ]
  node [
    id 324
    label "oliwkowate"
  ]
  node [
    id 325
    label "ro&#347;lina"
  ]
  node [
    id 326
    label "hy&#263;ka"
  ]
  node [
    id 327
    label "lilac"
  ]
  node [
    id 328
    label "delfinidyna"
  ]
  node [
    id 329
    label "cytat"
  ]
  node [
    id 330
    label "sytuacja"
  ]
  node [
    id 331
    label "wyci&#261;g"
  ]
  node [
    id 332
    label "ekscerptor"
  ]
  node [
    id 333
    label "passage"
  ]
  node [
    id 334
    label "urywek"
  ]
  node [
    id 335
    label "leksem"
  ]
  node [
    id 336
    label "ortografia"
  ]
  node [
    id 337
    label "pliocen"
  ]
  node [
    id 338
    label "czas"
  ]
  node [
    id 339
    label "eocen"
  ]
  node [
    id 340
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 341
    label "&#347;rodkowy_trias"
  ]
  node [
    id 342
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 343
    label "paleocen"
  ]
  node [
    id 344
    label "dzieje"
  ]
  node [
    id 345
    label "plejstocen"
  ]
  node [
    id 346
    label "bajos"
  ]
  node [
    id 347
    label "holocen"
  ]
  node [
    id 348
    label "oligocen"
  ]
  node [
    id 349
    label "term"
  ]
  node [
    id 350
    label "Zeitgeist"
  ]
  node [
    id 351
    label "kelowej"
  ]
  node [
    id 352
    label "schy&#322;ek"
  ]
  node [
    id 353
    label "miocen"
  ]
  node [
    id 354
    label "aalen"
  ]
  node [
    id 355
    label "wczesny_trias"
  ]
  node [
    id 356
    label "jura_wczesna"
  ]
  node [
    id 357
    label "jura_&#347;rodkowa"
  ]
  node [
    id 358
    label "okres"
  ]
  node [
    id 359
    label "dawny"
  ]
  node [
    id 360
    label "zgodny"
  ]
  node [
    id 361
    label "historycznie"
  ]
  node [
    id 362
    label "wiekopomny"
  ]
  node [
    id 363
    label "dziejowo"
  ]
  node [
    id 364
    label "hodowanie"
  ]
  node [
    id 365
    label "use"
  ]
  node [
    id 366
    label "culture"
  ]
  node [
    id 367
    label "czynno&#347;&#263;"
  ]
  node [
    id 368
    label "oprysk"
  ]
  node [
    id 369
    label "exercise"
  ]
  node [
    id 370
    label "koszenie"
  ]
  node [
    id 371
    label "biotechnika"
  ]
  node [
    id 372
    label "sadzenie"
  ]
  node [
    id 373
    label "rolnictwo"
  ]
  node [
    id 374
    label "obrabianie"
  ]
  node [
    id 375
    label "zajmowanie_si&#281;"
  ]
  node [
    id 376
    label "pielenie"
  ]
  node [
    id 377
    label "siew"
  ]
  node [
    id 378
    label "stanowisko"
  ]
  node [
    id 379
    label "szczepienie"
  ]
  node [
    id 380
    label "sianie"
  ]
  node [
    id 381
    label "orka"
  ]
  node [
    id 382
    label "ufa&#263;"
  ]
  node [
    id 383
    label "consist"
  ]
  node [
    id 384
    label "trust"
  ]
  node [
    id 385
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 386
    label "report"
  ]
  node [
    id 387
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 388
    label "rachowa&#263;"
  ]
  node [
    id 389
    label "dyskalkulia"
  ]
  node [
    id 390
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 391
    label "okre&#347;la&#263;"
  ]
  node [
    id 392
    label "lock"
  ]
  node [
    id 393
    label "absolut"
  ]
  node [
    id 394
    label "wypowied&#378;"
  ]
  node [
    id 395
    label "wytw&#243;r"
  ]
  node [
    id 396
    label "kontekst"
  ]
  node [
    id 397
    label "obja&#347;nienie"
  ]
  node [
    id 398
    label "wypracowanie"
  ]
  node [
    id 399
    label "hermeneutyka"
  ]
  node [
    id 400
    label "interpretation"
  ]
  node [
    id 401
    label "explanation"
  ]
  node [
    id 402
    label "realizacja"
  ]
  node [
    id 403
    label "zmusza&#263;"
  ]
  node [
    id 404
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 405
    label "claim"
  ]
  node [
    id 406
    label "force"
  ]
  node [
    id 407
    label "take"
  ]
  node [
    id 408
    label "d&#322;ugotrwale"
  ]
  node [
    id 409
    label "d&#322;ugi"
  ]
  node [
    id 410
    label "badanie"
  ]
  node [
    id 411
    label "nauka"
  ]
  node [
    id 412
    label "umiej&#281;tnie"
  ]
  node [
    id 413
    label "dobry"
  ]
  node [
    id 414
    label "udany"
  ]
  node [
    id 415
    label "umny"
  ]
  node [
    id 416
    label "relate"
  ]
  node [
    id 417
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 418
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 419
    label "robi&#263;"
  ]
  node [
    id 420
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 421
    label "powodowa&#263;"
  ]
  node [
    id 422
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 423
    label "szkodnik"
  ]
  node [
    id 424
    label "&#347;rodowisko"
  ]
  node [
    id 425
    label "component"
  ]
  node [
    id 426
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 427
    label "r&#243;&#380;niczka"
  ]
  node [
    id 428
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 429
    label "gangsterski"
  ]
  node [
    id 430
    label "szambo"
  ]
  node [
    id 431
    label "materia"
  ]
  node [
    id 432
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 433
    label "underworld"
  ]
  node [
    id 434
    label "ogromny"
  ]
  node [
    id 435
    label "olbrzymi"
  ]
  node [
    id 436
    label "astronomicznie"
  ]
  node [
    id 437
    label "&#380;ywy"
  ]
  node [
    id 438
    label "biologicznie"
  ]
  node [
    id 439
    label "specjalny"
  ]
  node [
    id 440
    label "praktyczny"
  ]
  node [
    id 441
    label "paramedyczny"
  ]
  node [
    id 442
    label "profilowy"
  ]
  node [
    id 443
    label "leczniczy"
  ]
  node [
    id 444
    label "lekarsko"
  ]
  node [
    id 445
    label "bia&#322;y"
  ]
  node [
    id 446
    label "medycznie"
  ]
  node [
    id 447
    label "specjalistyczny"
  ]
  node [
    id 448
    label "czyn"
  ]
  node [
    id 449
    label "practice"
  ]
  node [
    id 450
    label "skill"
  ]
  node [
    id 451
    label "znawstwo"
  ]
  node [
    id 452
    label "zwyczaj"
  ]
  node [
    id 453
    label "eksperiencja"
  ]
  node [
    id 454
    label "praca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 43
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 53
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 62
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 201
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 207
  ]
  edge [
    source 17
    target 208
  ]
  edge [
    source 17
    target 209
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 212
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 17
    target 214
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 216
  ]
  edge [
    source 18
    target 217
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 42
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 228
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 229
  ]
  edge [
    source 21
    target 230
  ]
  edge [
    source 21
    target 231
  ]
  edge [
    source 21
    target 232
  ]
  edge [
    source 21
    target 233
  ]
  edge [
    source 21
    target 234
  ]
  edge [
    source 21
    target 235
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 44
  ]
  edge [
    source 22
    target 45
  ]
  edge [
    source 22
    target 236
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 22
    target 238
  ]
  edge [
    source 22
    target 239
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 240
  ]
  edge [
    source 24
    target 241
  ]
  edge [
    source 24
    target 242
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 243
  ]
  edge [
    source 26
    target 244
  ]
  edge [
    source 26
    target 245
  ]
  edge [
    source 26
    target 207
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 248
  ]
  edge [
    source 27
    target 249
  ]
  edge [
    source 27
    target 250
  ]
  edge [
    source 27
    target 251
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 253
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 260
  ]
  edge [
    source 30
    target 261
  ]
  edge [
    source 30
    target 262
  ]
  edge [
    source 30
    target 55
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 263
  ]
  edge [
    source 31
    target 264
  ]
  edge [
    source 31
    target 265
  ]
  edge [
    source 31
    target 266
  ]
  edge [
    source 31
    target 267
  ]
  edge [
    source 31
    target 268
  ]
  edge [
    source 31
    target 269
  ]
  edge [
    source 31
    target 270
  ]
  edge [
    source 31
    target 271
  ]
  edge [
    source 31
    target 272
  ]
  edge [
    source 31
    target 273
  ]
  edge [
    source 31
    target 108
  ]
  edge [
    source 31
    target 274
  ]
  edge [
    source 31
    target 275
  ]
  edge [
    source 31
    target 276
  ]
  edge [
    source 31
    target 277
  ]
  edge [
    source 31
    target 278
  ]
  edge [
    source 31
    target 279
  ]
  edge [
    source 31
    target 280
  ]
  edge [
    source 31
    target 281
  ]
  edge [
    source 31
    target 116
  ]
  edge [
    source 31
    target 282
  ]
  edge [
    source 31
    target 283
  ]
  edge [
    source 31
    target 284
  ]
  edge [
    source 31
    target 285
  ]
  edge [
    source 31
    target 286
  ]
  edge [
    source 31
    target 287
  ]
  edge [
    source 31
    target 288
  ]
  edge [
    source 31
    target 289
  ]
  edge [
    source 31
    target 290
  ]
  edge [
    source 31
    target 291
  ]
  edge [
    source 31
    target 292
  ]
  edge [
    source 31
    target 182
  ]
  edge [
    source 31
    target 293
  ]
  edge [
    source 31
    target 294
  ]
  edge [
    source 31
    target 295
  ]
  edge [
    source 31
    target 296
  ]
  edge [
    source 32
    target 297
  ]
  edge [
    source 32
    target 298
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 299
  ]
  edge [
    source 33
    target 300
  ]
  edge [
    source 33
    target 301
  ]
  edge [
    source 33
    target 302
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 303
  ]
  edge [
    source 35
    target 304
  ]
  edge [
    source 35
    target 305
  ]
  edge [
    source 35
    target 306
  ]
  edge [
    source 35
    target 307
  ]
  edge [
    source 35
    target 308
  ]
  edge [
    source 35
    target 309
  ]
  edge [
    source 35
    target 310
  ]
  edge [
    source 35
    target 311
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 222
  ]
  edge [
    source 36
    target 312
  ]
  edge [
    source 36
    target 313
  ]
  edge [
    source 36
    target 314
  ]
  edge [
    source 36
    target 190
  ]
  edge [
    source 36
    target 315
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 317
  ]
  edge [
    source 38
    target 318
  ]
  edge [
    source 38
    target 319
  ]
  edge [
    source 38
    target 320
  ]
  edge [
    source 38
    target 321
  ]
  edge [
    source 38
    target 322
  ]
  edge [
    source 38
    target 323
  ]
  edge [
    source 38
    target 324
  ]
  edge [
    source 38
    target 325
  ]
  edge [
    source 38
    target 326
  ]
  edge [
    source 38
    target 327
  ]
  edge [
    source 38
    target 328
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 329
  ]
  edge [
    source 39
    target 330
  ]
  edge [
    source 39
    target 331
  ]
  edge [
    source 39
    target 332
  ]
  edge [
    source 39
    target 333
  ]
  edge [
    source 39
    target 334
  ]
  edge [
    source 39
    target 335
  ]
  edge [
    source 39
    target 336
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 337
  ]
  edge [
    source 40
    target 338
  ]
  edge [
    source 40
    target 339
  ]
  edge [
    source 40
    target 340
  ]
  edge [
    source 40
    target 152
  ]
  edge [
    source 40
    target 341
  ]
  edge [
    source 40
    target 342
  ]
  edge [
    source 40
    target 343
  ]
  edge [
    source 40
    target 344
  ]
  edge [
    source 40
    target 345
  ]
  edge [
    source 40
    target 346
  ]
  edge [
    source 40
    target 347
  ]
  edge [
    source 40
    target 348
  ]
  edge [
    source 40
    target 349
  ]
  edge [
    source 40
    target 350
  ]
  edge [
    source 40
    target 351
  ]
  edge [
    source 40
    target 352
  ]
  edge [
    source 40
    target 353
  ]
  edge [
    source 40
    target 354
  ]
  edge [
    source 40
    target 355
  ]
  edge [
    source 40
    target 356
  ]
  edge [
    source 40
    target 357
  ]
  edge [
    source 40
    target 358
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 359
  ]
  edge [
    source 41
    target 360
  ]
  edge [
    source 41
    target 361
  ]
  edge [
    source 41
    target 362
  ]
  edge [
    source 41
    target 308
  ]
  edge [
    source 41
    target 363
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 364
  ]
  edge [
    source 42
    target 365
  ]
  edge [
    source 42
    target 366
  ]
  edge [
    source 42
    target 367
  ]
  edge [
    source 42
    target 368
  ]
  edge [
    source 42
    target 369
  ]
  edge [
    source 42
    target 370
  ]
  edge [
    source 42
    target 371
  ]
  edge [
    source 42
    target 372
  ]
  edge [
    source 42
    target 373
  ]
  edge [
    source 42
    target 374
  ]
  edge [
    source 42
    target 375
  ]
  edge [
    source 42
    target 376
  ]
  edge [
    source 42
    target 182
  ]
  edge [
    source 42
    target 377
  ]
  edge [
    source 42
    target 378
  ]
  edge [
    source 42
    target 379
  ]
  edge [
    source 42
    target 380
  ]
  edge [
    source 42
    target 381
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 382
  ]
  edge [
    source 43
    target 383
  ]
  edge [
    source 43
    target 384
  ]
  edge [
    source 43
    target 385
  ]
  edge [
    source 44
    target 386
  ]
  edge [
    source 44
    target 387
  ]
  edge [
    source 44
    target 388
  ]
  edge [
    source 44
    target 389
  ]
  edge [
    source 44
    target 390
  ]
  edge [
    source 44
    target 391
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 392
  ]
  edge [
    source 46
    target 393
  ]
  edge [
    source 46
    target 108
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 394
  ]
  edge [
    source 47
    target 395
  ]
  edge [
    source 47
    target 396
  ]
  edge [
    source 47
    target 397
  ]
  edge [
    source 47
    target 398
  ]
  edge [
    source 47
    target 399
  ]
  edge [
    source 47
    target 400
  ]
  edge [
    source 47
    target 165
  ]
  edge [
    source 47
    target 401
  ]
  edge [
    source 47
    target 402
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 403
  ]
  edge [
    source 48
    target 404
  ]
  edge [
    source 48
    target 405
  ]
  edge [
    source 48
    target 406
  ]
  edge [
    source 48
    target 407
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 408
  ]
  edge [
    source 49
    target 409
  ]
  edge [
    source 49
    target 61
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 410
  ]
  edge [
    source 50
    target 411
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 412
  ]
  edge [
    source 51
    target 413
  ]
  edge [
    source 51
    target 414
  ]
  edge [
    source 51
    target 415
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 416
  ]
  edge [
    source 52
    target 417
  ]
  edge [
    source 52
    target 418
  ]
  edge [
    source 52
    target 419
  ]
  edge [
    source 52
    target 420
  ]
  edge [
    source 52
    target 421
  ]
  edge [
    source 52
    target 422
  ]
  edge [
    source 53
    target 423
  ]
  edge [
    source 53
    target 424
  ]
  edge [
    source 53
    target 425
  ]
  edge [
    source 53
    target 426
  ]
  edge [
    source 53
    target 427
  ]
  edge [
    source 53
    target 131
  ]
  edge [
    source 53
    target 428
  ]
  edge [
    source 53
    target 429
  ]
  edge [
    source 53
    target 430
  ]
  edge [
    source 53
    target 108
  ]
  edge [
    source 53
    target 431
  ]
  edge [
    source 53
    target 205
  ]
  edge [
    source 53
    target 432
  ]
  edge [
    source 53
    target 171
  ]
  edge [
    source 53
    target 433
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 434
  ]
  edge [
    source 55
    target 435
  ]
  edge [
    source 55
    target 436
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 437
  ]
  edge [
    source 56
    target 438
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 439
  ]
  edge [
    source 57
    target 440
  ]
  edge [
    source 57
    target 441
  ]
  edge [
    source 57
    target 360
  ]
  edge [
    source 57
    target 442
  ]
  edge [
    source 57
    target 443
  ]
  edge [
    source 57
    target 444
  ]
  edge [
    source 57
    target 445
  ]
  edge [
    source 57
    target 446
  ]
  edge [
    source 57
    target 447
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 409
  ]
  edge [
    source 62
    target 448
  ]
  edge [
    source 62
    target 449
  ]
  edge [
    source 62
    target 450
  ]
  edge [
    source 62
    target 451
  ]
  edge [
    source 62
    target 452
  ]
  edge [
    source 62
    target 453
  ]
  edge [
    source 62
    target 454
  ]
  edge [
    source 62
    target 411
  ]
]
