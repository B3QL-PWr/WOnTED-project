graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9583333333333333
  density 0.041666666666666664
  graphCliqueNumber 2
  node [
    id 0
    label "dzieeeeeeee&#324;"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "min&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "bawi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pomy&#347;lny"
  ]
  node [
    id 7
    label "skuteczny"
  ]
  node [
    id 8
    label "moralny"
  ]
  node [
    id 9
    label "korzystny"
  ]
  node [
    id 10
    label "odpowiedni"
  ]
  node [
    id 11
    label "zwrot"
  ]
  node [
    id 12
    label "dobrze"
  ]
  node [
    id 13
    label "pozytywny"
  ]
  node [
    id 14
    label "grzeczny"
  ]
  node [
    id 15
    label "powitanie"
  ]
  node [
    id 16
    label "mi&#322;y"
  ]
  node [
    id 17
    label "dobroczynny"
  ]
  node [
    id 18
    label "pos&#322;uszny"
  ]
  node [
    id 19
    label "ca&#322;y"
  ]
  node [
    id 20
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 21
    label "czw&#243;rka"
  ]
  node [
    id 22
    label "spokojny"
  ]
  node [
    id 23
    label "&#347;mieszny"
  ]
  node [
    id 24
    label "drogi"
  ]
  node [
    id 25
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 26
    label "omin&#261;&#263;"
  ]
  node [
    id 27
    label "spowodowa&#263;"
  ]
  node [
    id 28
    label "run"
  ]
  node [
    id 29
    label "przesta&#263;"
  ]
  node [
    id 30
    label "przej&#347;&#263;"
  ]
  node [
    id 31
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 32
    label "die"
  ]
  node [
    id 33
    label "overwhelm"
  ]
  node [
    id 34
    label "czas"
  ]
  node [
    id 35
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 36
    label "rok"
  ]
  node [
    id 37
    label "miech"
  ]
  node [
    id 38
    label "kalendy"
  ]
  node [
    id 39
    label "tydzie&#324;"
  ]
  node [
    id 40
    label "ubawia&#263;"
  ]
  node [
    id 41
    label "amuse"
  ]
  node [
    id 42
    label "zajmowa&#263;"
  ]
  node [
    id 43
    label "wzbudza&#263;"
  ]
  node [
    id 44
    label "przebywa&#263;"
  ]
  node [
    id 45
    label "sprawia&#263;"
  ]
  node [
    id 46
    label "zabawia&#263;"
  ]
  node [
    id 47
    label "play"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
]
