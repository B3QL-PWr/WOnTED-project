graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.7391304347826086
  density 0.03864734299516908
  graphCliqueNumber 2
  node [
    id 0
    label "zamek"
    origin "text"
  ]
  node [
    id 1
    label "powie&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "Windsor"
  ]
  node [
    id 3
    label "budynek"
  ]
  node [
    id 4
    label "bramka"
  ]
  node [
    id 5
    label "fortyfikacja"
  ]
  node [
    id 6
    label "blokada"
  ]
  node [
    id 7
    label "iglica"
  ]
  node [
    id 8
    label "fastener"
  ]
  node [
    id 9
    label "baszta"
  ]
  node [
    id 10
    label "budowla"
  ]
  node [
    id 11
    label "zagrywka"
  ]
  node [
    id 12
    label "hokej"
  ]
  node [
    id 13
    label "wjazd"
  ]
  node [
    id 14
    label "mechanizm"
  ]
  node [
    id 15
    label "bro&#324;_palna"
  ]
  node [
    id 16
    label "stra&#380;nica"
  ]
  node [
    id 17
    label "blockage"
  ]
  node [
    id 18
    label "informatyka"
  ]
  node [
    id 19
    label "Wawel"
  ]
  node [
    id 20
    label "brama"
  ]
  node [
    id 21
    label "ekspres"
  ]
  node [
    id 22
    label "rezydencja"
  ]
  node [
    id 23
    label "Zamek_Ogrodzieniec"
  ]
  node [
    id 24
    label "drzwi"
  ]
  node [
    id 25
    label "komora_zamkowa"
  ]
  node [
    id 26
    label "zamkni&#281;cie"
  ]
  node [
    id 27
    label "tercja"
  ]
  node [
    id 28
    label "zapi&#281;cie"
  ]
  node [
    id 29
    label "moderate"
  ]
  node [
    id 30
    label "powie&#347;&#263;_cykliczna"
  ]
  node [
    id 31
    label "proza"
  ]
  node [
    id 32
    label "gatunek_literacki"
  ]
  node [
    id 33
    label "utw&#243;r_epicki"
  ]
  node [
    id 34
    label "doprowadzi&#263;"
  ]
  node [
    id 35
    label "marynistyczny"
  ]
  node [
    id 36
    label "Das"
  ]
  node [
    id 37
    label "Schlo&#223;"
  ]
  node [
    id 38
    label "Franz"
  ]
  node [
    id 39
    label "Kafka"
  ]
  node [
    id 40
    label "trylogia"
  ]
  node [
    id 41
    label "samotno&#347;&#263;"
  ]
  node [
    id 42
    label "Maxowi"
  ]
  node [
    id 43
    label "br&#243;d"
  ]
  node [
    id 44
    label "Max"
  ]
  node [
    id 45
    label "Brod"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
]
