graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.9710144927536233
  density 0.028985507246376812
  graphCliqueNumber 2
  node [
    id 0
    label "nadajnik"
    origin "text"
  ]
  node [
    id 1
    label "gps"
    origin "text"
  ]
  node [
    id 2
    label "pokazowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "poszczeg&#243;lny"
    origin "text"
  ]
  node [
    id 4
    label "stado"
    origin "text"
  ]
  node [
    id 5
    label "wchodzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "siebie"
    origin "text"
  ]
  node [
    id 7
    label "droga"
    origin "text"
  ]
  node [
    id 8
    label "antena"
  ]
  node [
    id 9
    label "urz&#261;dzenie"
  ]
  node [
    id 10
    label "poszczeg&#243;lnie"
  ]
  node [
    id 11
    label "pojedynczy"
  ]
  node [
    id 12
    label "hurma"
  ]
  node [
    id 13
    label "school"
  ]
  node [
    id 14
    label "zbi&#243;r"
  ]
  node [
    id 15
    label "grupa"
  ]
  node [
    id 16
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 17
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 18
    label "przenika&#263;"
  ]
  node [
    id 19
    label "przekracza&#263;"
  ]
  node [
    id 20
    label "nast&#281;powa&#263;"
  ]
  node [
    id 21
    label "dochodzi&#263;"
  ]
  node [
    id 22
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 23
    label "intervene"
  ]
  node [
    id 24
    label "scale"
  ]
  node [
    id 25
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 26
    label "&#322;oi&#263;"
  ]
  node [
    id 27
    label "osi&#261;ga&#263;"
  ]
  node [
    id 28
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 29
    label "poznawa&#263;"
  ]
  node [
    id 30
    label "go"
  ]
  node [
    id 31
    label "atakowa&#263;"
  ]
  node [
    id 32
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 33
    label "mount"
  ]
  node [
    id 34
    label "invade"
  ]
  node [
    id 35
    label "bra&#263;"
  ]
  node [
    id 36
    label "wnika&#263;"
  ]
  node [
    id 37
    label "move"
  ]
  node [
    id 38
    label "zaziera&#263;"
  ]
  node [
    id 39
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 40
    label "spotyka&#263;"
  ]
  node [
    id 41
    label "zaczyna&#263;"
  ]
  node [
    id 42
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 43
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 44
    label "journey"
  ]
  node [
    id 45
    label "podbieg"
  ]
  node [
    id 46
    label "bezsilnikowy"
  ]
  node [
    id 47
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 48
    label "wylot"
  ]
  node [
    id 49
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 50
    label "drogowskaz"
  ]
  node [
    id 51
    label "nawierzchnia"
  ]
  node [
    id 52
    label "turystyka"
  ]
  node [
    id 53
    label "budowla"
  ]
  node [
    id 54
    label "spos&#243;b"
  ]
  node [
    id 55
    label "passage"
  ]
  node [
    id 56
    label "marszrutyzacja"
  ]
  node [
    id 57
    label "zbior&#243;wka"
  ]
  node [
    id 58
    label "rajza"
  ]
  node [
    id 59
    label "ekskursja"
  ]
  node [
    id 60
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 61
    label "ruch"
  ]
  node [
    id 62
    label "trasa"
  ]
  node [
    id 63
    label "wyb&#243;j"
  ]
  node [
    id 64
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 65
    label "ekwipunek"
  ]
  node [
    id 66
    label "korona_drogi"
  ]
  node [
    id 67
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 68
    label "pobocze"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
]
