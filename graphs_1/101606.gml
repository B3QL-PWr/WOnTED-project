graph [
  maxDegree 51
  minDegree 1
  meanDegree 2.0939597315436242
  density 0.004694976976555211
  graphCliqueNumber 3
  node [
    id 0
    label "nagle"
    origin "text"
  ]
  node [
    id 1
    label "szczyt"
    origin "text"
  ]
  node [
    id 2
    label "wzg&#243;rze"
    origin "text"
  ]
  node [
    id 3
    label "odleg&#322;y"
    origin "text"
  ]
  node [
    id 4
    label "ostatnie"
    origin "text"
  ]
  node [
    id 5
    label "znik&#322;y"
    origin "text"
  ]
  node [
    id 6
    label "s&#322;o&#324;ce"
    origin "text"
  ]
  node [
    id 7
    label "promie&#324;"
    origin "text"
  ]
  node [
    id 8
    label "zarazem"
    origin "text"
  ]
  node [
    id 9
    label "&#347;wietny"
    origin "text"
  ]
  node [
    id 10
    label "barwa"
    origin "text"
  ]
  node [
    id 11
    label "pi&#281;kne"
    origin "text"
  ]
  node [
    id 12
    label "ob&#322;ok"
    origin "text"
  ]
  node [
    id 13
    label "przygasa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "pocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 15
    label "naprz&#243;d"
    origin "text"
  ]
  node [
    id 16
    label "z&#322;ocisty"
    origin "text"
  ]
  node [
    id 17
    label "he&#322;m"
    origin "text"
  ]
  node [
    id 18
    label "pociemnie&#263;"
    origin "text"
  ]
  node [
    id 19
    label "jakby"
    origin "text"
  ]
  node [
    id 20
    label "rdza"
    origin "text"
  ]
  node [
    id 21
    label "pokry&#263;"
    origin "text"
  ]
  node [
    id 22
    label "potem"
    origin "text"
  ]
  node [
    id 23
    label "purpura"
    origin "text"
  ]
  node [
    id 24
    label "szata"
    origin "text"
  ]
  node [
    id 25
    label "rozwiewny"
    origin "text"
  ]
  node [
    id 26
    label "zblad&#322;y"
    origin "text"
  ]
  node [
    id 27
    label "m&#281;tny"
    origin "text"
  ]
  node [
    id 28
    label "obla&#263;"
    origin "text"
  ]
  node [
    id 29
    label "si&#281;"
    origin "text"
  ]
  node [
    id 30
    label "&#380;&#243;&#322;to&#347;&#263;"
    origin "text"
  ]
  node [
    id 31
    label "koniec"
    origin "text"
  ]
  node [
    id 32
    label "skrzyd&#322;o"
    origin "text"
  ]
  node [
    id 33
    label "srebrny"
    origin "text"
  ]
  node [
    id 34
    label "d&#243;&#322;"
    origin "text"
  ]
  node [
    id 35
    label "opu&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 36
    label "brudnopopielaty"
    origin "text"
  ]
  node [
    id 37
    label "nabra&#263;"
    origin "text"
  ]
  node [
    id 38
    label "mimo"
    origin "text"
  ]
  node [
    id 39
    label "jak"
    origin "text"
  ]
  node [
    id 40
    label "rycerz"
    origin "text"
  ]
  node [
    id 41
    label "odarty"
    origin "text"
  ]
  node [
    id 42
    label "zbroja"
    origin "text"
  ]
  node [
    id 43
    label "kroczy&#263;"
    origin "text"
  ]
  node [
    id 44
    label "jeszcze"
    origin "text"
  ]
  node [
    id 45
    label "chwila"
    origin "text"
  ]
  node [
    id 46
    label "kilka"
    origin "text"
  ]
  node [
    id 47
    label "sklepienie"
    origin "text"
  ]
  node [
    id 48
    label "wolny"
    origin "text"
  ]
  node [
    id 49
    label "wspaniale"
    origin "text"
  ]
  node [
    id 50
    label "szybko"
  ]
  node [
    id 51
    label "raptowny"
  ]
  node [
    id 52
    label "nieprzewidzianie"
  ]
  node [
    id 53
    label "Lubogoszcz"
  ]
  node [
    id 54
    label "wierch"
  ]
  node [
    id 55
    label "czas"
  ]
  node [
    id 56
    label "&#321;omnica"
  ]
  node [
    id 57
    label "Magura"
  ]
  node [
    id 58
    label "Wo&#322;ek"
  ]
  node [
    id 59
    label "Wielki_Chocz"
  ]
  node [
    id 60
    label "Turbacz"
  ]
  node [
    id 61
    label "Walig&#243;ra"
  ]
  node [
    id 62
    label "Orlica"
  ]
  node [
    id 63
    label "korona"
  ]
  node [
    id 64
    label "bok"
  ]
  node [
    id 65
    label "Jaworzyna"
  ]
  node [
    id 66
    label "Groniczki"
  ]
  node [
    id 67
    label "Radunia"
  ]
  node [
    id 68
    label "&#346;winica"
  ]
  node [
    id 69
    label "Okr&#261;glica"
  ]
  node [
    id 70
    label "Beskid"
  ]
  node [
    id 71
    label "poziom"
  ]
  node [
    id 72
    label "wzmo&#380;enie"
  ]
  node [
    id 73
    label "Czupel"
  ]
  node [
    id 74
    label "fasada"
  ]
  node [
    id 75
    label "Rysianka"
  ]
  node [
    id 76
    label "g&#243;ra"
  ]
  node [
    id 77
    label "Jaworz"
  ]
  node [
    id 78
    label "Rudawiec"
  ]
  node [
    id 79
    label "Che&#322;miec"
  ]
  node [
    id 80
    label "zwie&#324;czenie"
  ]
  node [
    id 81
    label "Wielki_Bukowiec"
  ]
  node [
    id 82
    label "wzniesienie"
  ]
  node [
    id 83
    label "godzina_szczytu"
  ]
  node [
    id 84
    label "summit"
  ]
  node [
    id 85
    label "Wielka_Racza"
  ]
  node [
    id 86
    label "wierzcho&#322;"
  ]
  node [
    id 87
    label "&#346;nie&#380;nik"
  ]
  node [
    id 88
    label "&#347;ciana"
  ]
  node [
    id 89
    label "Barania_G&#243;ra"
  ]
  node [
    id 90
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 91
    label "Ja&#322;owiec"
  ]
  node [
    id 92
    label "Wielka_Sowa"
  ]
  node [
    id 93
    label "wierzcho&#322;ek"
  ]
  node [
    id 94
    label "Obidowa"
  ]
  node [
    id 95
    label "konferencja"
  ]
  node [
    id 96
    label "Cubryna"
  ]
  node [
    id 97
    label "Szrenica"
  ]
  node [
    id 98
    label "Czarna_G&#243;ra"
  ]
  node [
    id 99
    label "Mody&#324;"
  ]
  node [
    id 100
    label "&#346;l&#281;&#380;a"
  ]
  node [
    id 101
    label "Wawel"
  ]
  node [
    id 102
    label "Eskwilin"
  ]
  node [
    id 103
    label "Kapitol"
  ]
  node [
    id 104
    label "Syjon"
  ]
  node [
    id 105
    label "Awentyn"
  ]
  node [
    id 106
    label "Kwiryna&#322;"
  ]
  node [
    id 107
    label "m&#243;zg"
  ]
  node [
    id 108
    label "Palatyn"
  ]
  node [
    id 109
    label "oddalony"
  ]
  node [
    id 110
    label "s&#322;aby"
  ]
  node [
    id 111
    label "daleko"
  ]
  node [
    id 112
    label "odlegle"
  ]
  node [
    id 113
    label "daleki"
  ]
  node [
    id 114
    label "r&#243;&#380;ny"
  ]
  node [
    id 115
    label "nieobecny"
  ]
  node [
    id 116
    label "obcy"
  ]
  node [
    id 117
    label "delikatny"
  ]
  node [
    id 118
    label "S&#322;o&#324;ce"
  ]
  node [
    id 119
    label "zach&#243;d"
  ]
  node [
    id 120
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 121
    label "&#347;wiat&#322;o"
  ]
  node [
    id 122
    label "sunlight"
  ]
  node [
    id 123
    label "wsch&#243;d"
  ]
  node [
    id 124
    label "kochanie"
  ]
  node [
    id 125
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 126
    label "pogoda"
  ]
  node [
    id 127
    label "dzie&#324;"
  ]
  node [
    id 128
    label "odrobina"
  ]
  node [
    id 129
    label "zapowied&#378;"
  ]
  node [
    id 130
    label "wyrostek"
  ]
  node [
    id 131
    label "odcinek"
  ]
  node [
    id 132
    label "pi&#243;rko"
  ]
  node [
    id 133
    label "strumie&#324;"
  ]
  node [
    id 134
    label "rozeta"
  ]
  node [
    id 135
    label "pomy&#347;lny"
  ]
  node [
    id 136
    label "pozytywny"
  ]
  node [
    id 137
    label "dobry"
  ]
  node [
    id 138
    label "superancki"
  ]
  node [
    id 139
    label "arcydzielny"
  ]
  node [
    id 140
    label "zajebisty"
  ]
  node [
    id 141
    label "wa&#380;ny"
  ]
  node [
    id 142
    label "&#347;wietnie"
  ]
  node [
    id 143
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 144
    label "skuteczny"
  ]
  node [
    id 145
    label "spania&#322;y"
  ]
  node [
    id 146
    label "&#347;wieci&#263;"
  ]
  node [
    id 147
    label "zblakn&#261;&#263;"
  ]
  node [
    id 148
    label "prze&#322;ama&#263;_si&#281;"
  ]
  node [
    id 149
    label "&#347;wiecenie"
  ]
  node [
    id 150
    label "prze&#322;ama&#263;"
  ]
  node [
    id 151
    label "ubarwienie"
  ]
  node [
    id 152
    label "prze&#322;amywa&#263;"
  ]
  node [
    id 153
    label "prze&#322;amanie"
  ]
  node [
    id 154
    label "blakni&#281;cie"
  ]
  node [
    id 155
    label "cecha"
  ]
  node [
    id 156
    label "prze&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 157
    label "blakn&#261;&#263;"
  ]
  node [
    id 158
    label "prze&#322;amywanie_si&#281;"
  ]
  node [
    id 159
    label "zblakni&#281;cie"
  ]
  node [
    id 160
    label "tone"
  ]
  node [
    id 161
    label "rejestr"
  ]
  node [
    id 162
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 163
    label "prze&#322;amywanie"
  ]
  node [
    id 164
    label "kolorystyka"
  ]
  node [
    id 165
    label "oberwa&#263;_si&#281;"
  ]
  node [
    id 166
    label "chmura"
  ]
  node [
    id 167
    label "ob&#322;ok_Oorta"
  ]
  node [
    id 168
    label "burza"
  ]
  node [
    id 169
    label "kszta&#322;t"
  ]
  node [
    id 170
    label "zjawisko"
  ]
  node [
    id 171
    label "oberwanie_si&#281;"
  ]
  node [
    id 172
    label "cloud"
  ]
  node [
    id 173
    label "wy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 174
    label "gasn&#261;&#263;"
  ]
  node [
    id 175
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 176
    label "do"
  ]
  node [
    id 177
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 178
    label "zrobi&#263;"
  ]
  node [
    id 179
    label "najpierw"
  ]
  node [
    id 180
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 181
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 182
    label "poz&#322;ocenie"
  ]
  node [
    id 183
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 184
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 185
    label "z&#322;ocenie"
  ]
  node [
    id 186
    label "podbr&#243;dek"
  ]
  node [
    id 187
    label "dach"
  ]
  node [
    id 188
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 189
    label "daszek"
  ]
  node [
    id 190
    label "bro&#324;_ochronna"
  ]
  node [
    id 191
    label "bro&#324;"
  ]
  node [
    id 192
    label "grzebie&#324;"
  ]
  node [
    id 193
    label "wie&#380;a"
  ]
  node [
    id 194
    label "nausznik"
  ]
  node [
    id 195
    label "ubranie_ochronne"
  ]
  node [
    id 196
    label "grzebie&#324;_he&#322;mu"
  ]
  node [
    id 197
    label "labry"
  ]
  node [
    id 198
    label "sta&#263;_si&#281;"
  ]
  node [
    id 199
    label "blacken"
  ]
  node [
    id 200
    label "zabarwi&#263;_si&#281;"
  ]
  node [
    id 201
    label "darken"
  ]
  node [
    id 202
    label "warstwa"
  ]
  node [
    id 203
    label "schorzenie"
  ]
  node [
    id 204
    label "rdzowate"
  ]
  node [
    id 205
    label "podstawczak"
  ]
  node [
    id 206
    label "grzybica"
  ]
  node [
    id 207
    label "grzyb"
  ]
  node [
    id 208
    label "paso&#380;yt"
  ]
  node [
    id 209
    label "cover"
  ]
  node [
    id 210
    label "przykry&#263;"
  ]
  node [
    id 211
    label "zap&#322;aci&#263;"
  ]
  node [
    id 212
    label "zaj&#261;&#263;"
  ]
  node [
    id 213
    label "defray"
  ]
  node [
    id 214
    label "ustawi&#263;_si&#281;"
  ]
  node [
    id 215
    label "sheathing"
  ]
  node [
    id 216
    label "zap&#322;odni&#263;"
  ]
  node [
    id 217
    label "brood"
  ]
  node [
    id 218
    label "zaspokoi&#263;"
  ]
  node [
    id 219
    label "zamaskowa&#263;"
  ]
  node [
    id 220
    label "str&#243;j"
  ]
  node [
    id 221
    label "fiolet"
  ]
  node [
    id 222
    label "oznaka"
  ]
  node [
    id 223
    label "atrybut"
  ]
  node [
    id 224
    label "czerwie&#324;"
  ]
  node [
    id 225
    label "urz&#261;d"
  ]
  node [
    id 226
    label "barwnik_naturalny"
  ]
  node [
    id 227
    label "purple"
  ]
  node [
    id 228
    label "tkanina"
  ]
  node [
    id 229
    label "przedmiot"
  ]
  node [
    id 230
    label "element"
  ]
  node [
    id 231
    label "pow&#322;oka"
  ]
  node [
    id 232
    label "ulotny"
  ]
  node [
    id 233
    label "zwiewny"
  ]
  node [
    id 234
    label "wyblak&#322;y"
  ]
  node [
    id 235
    label "przyblad&#322;y"
  ]
  node [
    id 236
    label "poblad&#322;y"
  ]
  node [
    id 237
    label "nieprzejrzysty"
  ]
  node [
    id 238
    label "podejrzanie"
  ]
  node [
    id 239
    label "nieuwa&#380;ny"
  ]
  node [
    id 240
    label "niewyra&#378;ny"
  ]
  node [
    id 241
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 242
    label "niepewny"
  ]
  node [
    id 243
    label "m&#281;tnie"
  ]
  node [
    id 244
    label "zawile"
  ]
  node [
    id 245
    label "niezrozumia&#322;y"
  ]
  node [
    id 246
    label "z&#322;y"
  ]
  node [
    id 247
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 248
    label "powlec"
  ]
  node [
    id 249
    label "z&#322;o&#380;y&#263;_egzamin"
  ]
  node [
    id 250
    label "uczci&#263;"
  ]
  node [
    id 251
    label "body_of_water"
  ]
  node [
    id 252
    label "moisture"
  ]
  node [
    id 253
    label "zabarwi&#263;"
  ]
  node [
    id 254
    label "oceni&#263;"
  ]
  node [
    id 255
    label "spill"
  ]
  node [
    id 256
    label "zmoczy&#263;"
  ]
  node [
    id 257
    label "cut"
  ]
  node [
    id 258
    label "przeegzaminowa&#263;"
  ]
  node [
    id 259
    label "otoczy&#263;"
  ]
  node [
    id 260
    label "zala&#263;"
  ]
  node [
    id 261
    label "op&#322;yn&#261;&#263;"
  ]
  node [
    id 262
    label "przegra&#263;"
  ]
  node [
    id 263
    label "barwa_podstawowa"
  ]
  node [
    id 264
    label "kolor"
  ]
  node [
    id 265
    label "defenestracja"
  ]
  node [
    id 266
    label "szereg"
  ]
  node [
    id 267
    label "dzia&#322;anie"
  ]
  node [
    id 268
    label "miejsce"
  ]
  node [
    id 269
    label "ostatnie_podrygi"
  ]
  node [
    id 270
    label "kres"
  ]
  node [
    id 271
    label "agonia"
  ]
  node [
    id 272
    label "visitation"
  ]
  node [
    id 273
    label "szeol"
  ]
  node [
    id 274
    label "mogi&#322;a"
  ]
  node [
    id 275
    label "wydarzenie"
  ]
  node [
    id 276
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 277
    label "pogrzebanie"
  ]
  node [
    id 278
    label "punkt"
  ]
  node [
    id 279
    label "&#380;a&#322;oba"
  ]
  node [
    id 280
    label "zabicie"
  ]
  node [
    id 281
    label "kres_&#380;ycia"
  ]
  node [
    id 282
    label "wirolot"
  ]
  node [
    id 283
    label "skrzyd&#322;owiec"
  ]
  node [
    id 284
    label "grupa"
  ]
  node [
    id 285
    label "budynek"
  ]
  node [
    id 286
    label "wing"
  ]
  node [
    id 287
    label "p&#243;&#322;tusza"
  ]
  node [
    id 288
    label "tuszka"
  ]
  node [
    id 289
    label "keson"
  ]
  node [
    id 290
    label "husarz"
  ]
  node [
    id 291
    label "dr&#243;bka"
  ]
  node [
    id 292
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 293
    label "samolot"
  ]
  node [
    id 294
    label "sterolotka"
  ]
  node [
    id 295
    label "szybowiec"
  ]
  node [
    id 296
    label "klapa"
  ]
  node [
    id 297
    label "ugrupowanie"
  ]
  node [
    id 298
    label "oddzia&#322;"
  ]
  node [
    id 299
    label "winglet"
  ]
  node [
    id 300
    label "organizacja"
  ]
  node [
    id 301
    label "dr&#243;b"
  ]
  node [
    id 302
    label "husaria"
  ]
  node [
    id 303
    label "brama"
  ]
  node [
    id 304
    label "strz&#281;pina"
  ]
  node [
    id 305
    label "boisko"
  ]
  node [
    id 306
    label "dywizjon_lotniczy"
  ]
  node [
    id 307
    label "okno"
  ]
  node [
    id 308
    label "narz&#261;d_ruchu"
  ]
  node [
    id 309
    label "drzwi"
  ]
  node [
    id 310
    label "si&#322;y_powietrzne"
  ]
  node [
    id 311
    label "lotka"
  ]
  node [
    id 312
    label "&#380;y&#322;kowanie"
  ]
  node [
    id 313
    label "szyk"
  ]
  node [
    id 314
    label "mi&#281;so"
  ]
  node [
    id 315
    label "wo&#322;owina"
  ]
  node [
    id 316
    label "skrzele"
  ]
  node [
    id 317
    label "o&#322;tarz"
  ]
  node [
    id 318
    label "metaliczny"
  ]
  node [
    id 319
    label "szary"
  ]
  node [
    id 320
    label "jasny"
  ]
  node [
    id 321
    label "utytu&#322;owany"
  ]
  node [
    id 322
    label "srebrzenie_si&#281;"
  ]
  node [
    id 323
    label "srebrno"
  ]
  node [
    id 324
    label "posrebrzenie"
  ]
  node [
    id 325
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 326
    label "srebrzenie"
  ]
  node [
    id 327
    label "srebrzy&#347;cie"
  ]
  node [
    id 328
    label "hole"
  ]
  node [
    id 329
    label "wykopywa&#263;"
  ]
  node [
    id 330
    label "low"
  ]
  node [
    id 331
    label "niski"
  ]
  node [
    id 332
    label "wykopywanie"
  ]
  node [
    id 333
    label "wykopa&#263;"
  ]
  node [
    id 334
    label "depressive_disorder"
  ]
  node [
    id 335
    label "d&#378;wi&#281;k"
  ]
  node [
    id 336
    label "wykopanie"
  ]
  node [
    id 337
    label "&#347;piew"
  ]
  node [
    id 338
    label "za&#322;amanie"
  ]
  node [
    id 339
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 340
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 341
    label "omin&#261;&#263;"
  ]
  node [
    id 342
    label "humiliate"
  ]
  node [
    id 343
    label "pozostawi&#263;"
  ]
  node [
    id 344
    label "potani&#263;"
  ]
  node [
    id 345
    label "obni&#380;y&#263;"
  ]
  node [
    id 346
    label "evacuate"
  ]
  node [
    id 347
    label "authorize"
  ]
  node [
    id 348
    label "leave"
  ]
  node [
    id 349
    label "przesta&#263;"
  ]
  node [
    id 350
    label "straci&#263;"
  ]
  node [
    id 351
    label "zostawi&#263;"
  ]
  node [
    id 352
    label "drop"
  ]
  node [
    id 353
    label "tekst"
  ]
  node [
    id 354
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 355
    label "kupi&#263;"
  ]
  node [
    id 356
    label "deceive"
  ]
  node [
    id 357
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 358
    label "objecha&#263;"
  ]
  node [
    id 359
    label "wzi&#261;&#263;"
  ]
  node [
    id 360
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 361
    label "hoax"
  ]
  node [
    id 362
    label "oszwabi&#263;"
  ]
  node [
    id 363
    label "naby&#263;"
  ]
  node [
    id 364
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 365
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 366
    label "woda"
  ]
  node [
    id 367
    label "fraud"
  ]
  node [
    id 368
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 369
    label "gull"
  ]
  node [
    id 370
    label "byd&#322;o"
  ]
  node [
    id 371
    label "zobo"
  ]
  node [
    id 372
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 373
    label "yakalo"
  ]
  node [
    id 374
    label "dzo"
  ]
  node [
    id 375
    label "&#380;o&#322;nierz"
  ]
  node [
    id 376
    label "rycerstwo"
  ]
  node [
    id 377
    label "wojownik"
  ]
  node [
    id 378
    label "ochrona"
  ]
  node [
    id 379
    label "plastron"
  ]
  node [
    id 380
    label "nabiodrnik"
  ]
  node [
    id 381
    label "napier&#347;nik"
  ]
  node [
    id 382
    label "naplecznik"
  ]
  node [
    id 383
    label "naramiennik"
  ]
  node [
    id 384
    label "nar&#281;czak"
  ]
  node [
    id 385
    label "zbroica"
  ]
  node [
    id 386
    label "obojczyk"
  ]
  node [
    id 387
    label "kasak"
  ]
  node [
    id 388
    label "nabiodrek"
  ]
  node [
    id 389
    label "karwasz"
  ]
  node [
    id 390
    label "taszka"
  ]
  node [
    id 391
    label "nagolennik"
  ]
  node [
    id 392
    label "zar&#281;kawie"
  ]
  node [
    id 393
    label "p&#322;atnerz"
  ]
  node [
    id 394
    label "pace"
  ]
  node [
    id 395
    label "post&#281;powa&#263;"
  ]
  node [
    id 396
    label "i&#347;&#263;"
  ]
  node [
    id 397
    label "try"
  ]
  node [
    id 398
    label "ci&#261;gle"
  ]
  node [
    id 399
    label "time"
  ]
  node [
    id 400
    label "&#347;ledziowate"
  ]
  node [
    id 401
    label "ryba"
  ]
  node [
    id 402
    label "wysklepianie"
  ]
  node [
    id 403
    label "kresom&#243;zgowie_&#347;rodkowe"
  ]
  node [
    id 404
    label "brosza"
  ]
  node [
    id 405
    label "kaseton"
  ]
  node [
    id 406
    label "pomieszczenie"
  ]
  node [
    id 407
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 408
    label "wysklepienie"
  ]
  node [
    id 409
    label "budowla"
  ]
  node [
    id 410
    label "&#380;agielek"
  ]
  node [
    id 411
    label "arch"
  ]
  node [
    id 412
    label "konstrukcja"
  ]
  node [
    id 413
    label "trompa"
  ]
  node [
    id 414
    label "luneta"
  ]
  node [
    id 415
    label "kozub"
  ]
  node [
    id 416
    label "koleba"
  ]
  node [
    id 417
    label "z&#322;&#261;czenie"
  ]
  node [
    id 418
    label "na&#322;&#281;czka"
  ]
  node [
    id 419
    label "wysklepi&#263;"
  ]
  node [
    id 420
    label "struktura_anatomiczna"
  ]
  node [
    id 421
    label "wysklepia&#263;"
  ]
  node [
    id 422
    label "vault"
  ]
  node [
    id 423
    label "niezale&#380;ny"
  ]
  node [
    id 424
    label "swobodnie"
  ]
  node [
    id 425
    label "niespieszny"
  ]
  node [
    id 426
    label "rozrzedzanie"
  ]
  node [
    id 427
    label "zwolnienie_si&#281;"
  ]
  node [
    id 428
    label "wolno"
  ]
  node [
    id 429
    label "rozrzedzenie"
  ]
  node [
    id 430
    label "lu&#378;no"
  ]
  node [
    id 431
    label "zwalnianie_si&#281;"
  ]
  node [
    id 432
    label "wolnie"
  ]
  node [
    id 433
    label "strza&#322;"
  ]
  node [
    id 434
    label "rozwodnienie"
  ]
  node [
    id 435
    label "wakowa&#263;"
  ]
  node [
    id 436
    label "rozwadnianie"
  ]
  node [
    id 437
    label "rzedni&#281;cie"
  ]
  node [
    id 438
    label "zrzedni&#281;cie"
  ]
  node [
    id 439
    label "zajebi&#347;cie"
  ]
  node [
    id 440
    label "dobrze"
  ]
  node [
    id 441
    label "pomy&#347;lnie"
  ]
  node [
    id 442
    label "pozytywnie"
  ]
  node [
    id 443
    label "och&#281;do&#380;nie"
  ]
  node [
    id 444
    label "wspania&#322;y"
  ]
  node [
    id 445
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 446
    label "bogaty"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 38
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 80
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 209
  ]
  edge [
    source 21
    target 210
  ]
  edge [
    source 21
    target 211
  ]
  edge [
    source 21
    target 212
  ]
  edge [
    source 21
    target 213
  ]
  edge [
    source 21
    target 214
  ]
  edge [
    source 21
    target 215
  ]
  edge [
    source 21
    target 216
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 219
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 41
  ]
  edge [
    source 23
    target 42
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 227
  ]
  edge [
    source 23
    target 228
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 229
  ]
  edge [
    source 24
    target 230
  ]
  edge [
    source 24
    target 231
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 232
  ]
  edge [
    source 25
    target 233
  ]
  edge [
    source 25
    target 117
  ]
  edge [
    source 25
    target 48
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 237
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 239
  ]
  edge [
    source 27
    target 240
  ]
  edge [
    source 27
    target 241
  ]
  edge [
    source 27
    target 242
  ]
  edge [
    source 27
    target 243
  ]
  edge [
    source 27
    target 115
  ]
  edge [
    source 27
    target 244
  ]
  edge [
    source 27
    target 245
  ]
  edge [
    source 27
    target 246
  ]
  edge [
    source 27
    target 247
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 262
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 264
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 265
  ]
  edge [
    source 31
    target 266
  ]
  edge [
    source 31
    target 267
  ]
  edge [
    source 31
    target 268
  ]
  edge [
    source 31
    target 269
  ]
  edge [
    source 31
    target 270
  ]
  edge [
    source 31
    target 271
  ]
  edge [
    source 31
    target 272
  ]
  edge [
    source 31
    target 273
  ]
  edge [
    source 31
    target 274
  ]
  edge [
    source 31
    target 45
  ]
  edge [
    source 31
    target 275
  ]
  edge [
    source 31
    target 276
  ]
  edge [
    source 31
    target 277
  ]
  edge [
    source 31
    target 278
  ]
  edge [
    source 31
    target 279
  ]
  edge [
    source 31
    target 280
  ]
  edge [
    source 31
    target 281
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 282
  ]
  edge [
    source 32
    target 283
  ]
  edge [
    source 32
    target 284
  ]
  edge [
    source 32
    target 285
  ]
  edge [
    source 32
    target 286
  ]
  edge [
    source 32
    target 287
  ]
  edge [
    source 32
    target 230
  ]
  edge [
    source 32
    target 288
  ]
  edge [
    source 32
    target 289
  ]
  edge [
    source 32
    target 290
  ]
  edge [
    source 32
    target 291
  ]
  edge [
    source 32
    target 292
  ]
  edge [
    source 32
    target 293
  ]
  edge [
    source 32
    target 294
  ]
  edge [
    source 32
    target 295
  ]
  edge [
    source 32
    target 296
  ]
  edge [
    source 32
    target 297
  ]
  edge [
    source 32
    target 298
  ]
  edge [
    source 32
    target 299
  ]
  edge [
    source 32
    target 300
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 32
    target 301
  ]
  edge [
    source 32
    target 302
  ]
  edge [
    source 32
    target 303
  ]
  edge [
    source 32
    target 304
  ]
  edge [
    source 32
    target 305
  ]
  edge [
    source 32
    target 306
  ]
  edge [
    source 32
    target 307
  ]
  edge [
    source 32
    target 308
  ]
  edge [
    source 32
    target 309
  ]
  edge [
    source 32
    target 310
  ]
  edge [
    source 32
    target 311
  ]
  edge [
    source 32
    target 312
  ]
  edge [
    source 32
    target 313
  ]
  edge [
    source 32
    target 314
  ]
  edge [
    source 32
    target 315
  ]
  edge [
    source 32
    target 316
  ]
  edge [
    source 32
    target 317
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 318
  ]
  edge [
    source 33
    target 319
  ]
  edge [
    source 33
    target 320
  ]
  edge [
    source 33
    target 321
  ]
  edge [
    source 33
    target 322
  ]
  edge [
    source 33
    target 323
  ]
  edge [
    source 33
    target 324
  ]
  edge [
    source 33
    target 325
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 34
    target 328
  ]
  edge [
    source 34
    target 329
  ]
  edge [
    source 34
    target 330
  ]
  edge [
    source 34
    target 331
  ]
  edge [
    source 34
    target 332
  ]
  edge [
    source 34
    target 333
  ]
  edge [
    source 34
    target 334
  ]
  edge [
    source 34
    target 276
  ]
  edge [
    source 34
    target 335
  ]
  edge [
    source 34
    target 336
  ]
  edge [
    source 34
    target 337
  ]
  edge [
    source 34
    target 338
  ]
  edge [
    source 34
    target 339
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 340
  ]
  edge [
    source 35
    target 341
  ]
  edge [
    source 35
    target 342
  ]
  edge [
    source 35
    target 343
  ]
  edge [
    source 35
    target 344
  ]
  edge [
    source 35
    target 345
  ]
  edge [
    source 35
    target 346
  ]
  edge [
    source 35
    target 347
  ]
  edge [
    source 35
    target 348
  ]
  edge [
    source 35
    target 349
  ]
  edge [
    source 35
    target 350
  ]
  edge [
    source 35
    target 351
  ]
  edge [
    source 35
    target 352
  ]
  edge [
    source 35
    target 353
  ]
  edge [
    source 35
    target 354
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 355
  ]
  edge [
    source 37
    target 356
  ]
  edge [
    source 37
    target 357
  ]
  edge [
    source 37
    target 358
  ]
  edge [
    source 37
    target 359
  ]
  edge [
    source 37
    target 360
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 362
  ]
  edge [
    source 37
    target 363
  ]
  edge [
    source 37
    target 364
  ]
  edge [
    source 37
    target 365
  ]
  edge [
    source 37
    target 366
  ]
  edge [
    source 37
    target 367
  ]
  edge [
    source 37
    target 368
  ]
  edge [
    source 37
    target 369
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 370
  ]
  edge [
    source 39
    target 371
  ]
  edge [
    source 39
    target 372
  ]
  edge [
    source 39
    target 373
  ]
  edge [
    source 39
    target 374
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 375
  ]
  edge [
    source 40
    target 376
  ]
  edge [
    source 40
    target 377
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 378
  ]
  edge [
    source 42
    target 379
  ]
  edge [
    source 42
    target 380
  ]
  edge [
    source 42
    target 381
  ]
  edge [
    source 42
    target 382
  ]
  edge [
    source 42
    target 383
  ]
  edge [
    source 42
    target 384
  ]
  edge [
    source 42
    target 385
  ]
  edge [
    source 42
    target 386
  ]
  edge [
    source 42
    target 387
  ]
  edge [
    source 42
    target 190
  ]
  edge [
    source 42
    target 388
  ]
  edge [
    source 42
    target 389
  ]
  edge [
    source 42
    target 390
  ]
  edge [
    source 42
    target 195
  ]
  edge [
    source 42
    target 391
  ]
  edge [
    source 42
    target 392
  ]
  edge [
    source 42
    target 393
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 394
  ]
  edge [
    source 43
    target 395
  ]
  edge [
    source 43
    target 396
  ]
  edge [
    source 43
    target 397
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 398
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 55
  ]
  edge [
    source 45
    target 399
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 400
  ]
  edge [
    source 46
    target 401
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 402
  ]
  edge [
    source 47
    target 403
  ]
  edge [
    source 47
    target 404
  ]
  edge [
    source 47
    target 405
  ]
  edge [
    source 47
    target 406
  ]
  edge [
    source 47
    target 407
  ]
  edge [
    source 47
    target 408
  ]
  edge [
    source 47
    target 409
  ]
  edge [
    source 47
    target 410
  ]
  edge [
    source 47
    target 411
  ]
  edge [
    source 47
    target 412
  ]
  edge [
    source 47
    target 413
  ]
  edge [
    source 47
    target 414
  ]
  edge [
    source 47
    target 415
  ]
  edge [
    source 47
    target 416
  ]
  edge [
    source 47
    target 417
  ]
  edge [
    source 47
    target 418
  ]
  edge [
    source 47
    target 419
  ]
  edge [
    source 47
    target 420
  ]
  edge [
    source 47
    target 421
  ]
  edge [
    source 47
    target 422
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 423
  ]
  edge [
    source 48
    target 424
  ]
  edge [
    source 48
    target 425
  ]
  edge [
    source 48
    target 426
  ]
  edge [
    source 48
    target 427
  ]
  edge [
    source 48
    target 428
  ]
  edge [
    source 48
    target 429
  ]
  edge [
    source 48
    target 430
  ]
  edge [
    source 48
    target 431
  ]
  edge [
    source 48
    target 432
  ]
  edge [
    source 48
    target 433
  ]
  edge [
    source 48
    target 434
  ]
  edge [
    source 48
    target 435
  ]
  edge [
    source 48
    target 436
  ]
  edge [
    source 48
    target 437
  ]
  edge [
    source 48
    target 438
  ]
  edge [
    source 49
    target 439
  ]
  edge [
    source 49
    target 440
  ]
  edge [
    source 49
    target 441
  ]
  edge [
    source 49
    target 442
  ]
  edge [
    source 49
    target 443
  ]
  edge [
    source 49
    target 444
  ]
  edge [
    source 49
    target 445
  ]
  edge [
    source 49
    target 446
  ]
]
