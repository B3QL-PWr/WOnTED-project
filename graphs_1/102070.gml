graph [
  maxDegree 21
  minDegree 1
  meanDegree 2.0254777070063694
  density 0.012983831455169036
  graphCliqueNumber 2
  node [
    id 0
    label "jeden"
    origin "text"
  ]
  node [
    id 1
    label "moi"
    origin "text"
  ]
  node [
    id 2
    label "ulubiony"
    origin "text"
  ]
  node [
    id 3
    label "bloger"
    origin "text"
  ]
  node [
    id 4
    label "john"
    origin "text"
  ]
  node [
    id 5
    label "naughton"
    origin "text"
  ]
  node [
    id 6
    label "pisz"
    origin "text"
  ]
  node [
    id 7
    label "dobry"
    origin "text"
  ]
  node [
    id 8
    label "wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "mama"
    origin "text"
  ]
  node [
    id 10
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 11
    label "przez"
    origin "text"
  ]
  node [
    id 12
    label "jak"
    origin "text"
  ]
  node [
    id 13
    label "europa"
    origin "text"
  ]
  node [
    id 14
    label "by&#263;"
    origin "text"
  ]
  node [
    id 15
    label "pora&#380;ka"
    origin "text"
  ]
  node [
    id 16
    label "pis"
    origin "text"
  ]
  node [
    id 17
    label "ostatni"
    origin "text"
  ]
  node [
    id 18
    label "wybory"
    origin "text"
  ]
  node [
    id 19
    label "zlinkowanym"
    origin "text"
  ]
  node [
    id 20
    label "naughtona"
    origin "text"
  ]
  node [
    id 21
    label "tekst"
    origin "text"
  ]
  node [
    id 22
    label "guardiana"
    origin "text"
  ]
  node [
    id 23
    label "zatytu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 24
    label "straszny"
    origin "text"
  ]
  node [
    id 25
    label "bli&#378;niak"
    origin "text"
  ]
  node [
    id 26
    label "kieliszek"
  ]
  node [
    id 27
    label "shot"
  ]
  node [
    id 28
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 29
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 30
    label "jaki&#347;"
  ]
  node [
    id 31
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 32
    label "jednolicie"
  ]
  node [
    id 33
    label "w&#243;dka"
  ]
  node [
    id 34
    label "ten"
  ]
  node [
    id 35
    label "ujednolicenie"
  ]
  node [
    id 36
    label "jednakowy"
  ]
  node [
    id 37
    label "wyj&#261;tkowy"
  ]
  node [
    id 38
    label "faworytny"
  ]
  node [
    id 39
    label "autor"
  ]
  node [
    id 40
    label "internauta"
  ]
  node [
    id 41
    label "pomy&#347;lny"
  ]
  node [
    id 42
    label "skuteczny"
  ]
  node [
    id 43
    label "moralny"
  ]
  node [
    id 44
    label "korzystny"
  ]
  node [
    id 45
    label "odpowiedni"
  ]
  node [
    id 46
    label "zwrot"
  ]
  node [
    id 47
    label "dobrze"
  ]
  node [
    id 48
    label "pozytywny"
  ]
  node [
    id 49
    label "grzeczny"
  ]
  node [
    id 50
    label "powitanie"
  ]
  node [
    id 51
    label "mi&#322;y"
  ]
  node [
    id 52
    label "dobroczynny"
  ]
  node [
    id 53
    label "pos&#322;uszny"
  ]
  node [
    id 54
    label "ca&#322;y"
  ]
  node [
    id 55
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 56
    label "czw&#243;rka"
  ]
  node [
    id 57
    label "spokojny"
  ]
  node [
    id 58
    label "&#347;mieszny"
  ]
  node [
    id 59
    label "drogi"
  ]
  node [
    id 60
    label "zarys"
  ]
  node [
    id 61
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 62
    label "zniesienie"
  ]
  node [
    id 63
    label "nap&#322;ywanie"
  ]
  node [
    id 64
    label "znosi&#263;"
  ]
  node [
    id 65
    label "informacja"
  ]
  node [
    id 66
    label "znie&#347;&#263;"
  ]
  node [
    id 67
    label "komunikat"
  ]
  node [
    id 68
    label "depesza_emska"
  ]
  node [
    id 69
    label "communication"
  ]
  node [
    id 70
    label "znoszenie"
  ]
  node [
    id 71
    label "signal"
  ]
  node [
    id 72
    label "matczysko"
  ]
  node [
    id 73
    label "macierz"
  ]
  node [
    id 74
    label "przodkini"
  ]
  node [
    id 75
    label "Matka_Boska"
  ]
  node [
    id 76
    label "macocha"
  ]
  node [
    id 77
    label "matka_zast&#281;pcza"
  ]
  node [
    id 78
    label "stara"
  ]
  node [
    id 79
    label "rodzice"
  ]
  node [
    id 80
    label "rodzic"
  ]
  node [
    id 81
    label "czas"
  ]
  node [
    id 82
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 83
    label "rok"
  ]
  node [
    id 84
    label "miech"
  ]
  node [
    id 85
    label "kalendy"
  ]
  node [
    id 86
    label "tydzie&#324;"
  ]
  node [
    id 87
    label "byd&#322;o"
  ]
  node [
    id 88
    label "zobo"
  ]
  node [
    id 89
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 90
    label "yakalo"
  ]
  node [
    id 91
    label "dzo"
  ]
  node [
    id 92
    label "si&#281;ga&#263;"
  ]
  node [
    id 93
    label "trwa&#263;"
  ]
  node [
    id 94
    label "obecno&#347;&#263;"
  ]
  node [
    id 95
    label "stan"
  ]
  node [
    id 96
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 97
    label "stand"
  ]
  node [
    id 98
    label "mie&#263;_miejsce"
  ]
  node [
    id 99
    label "uczestniczy&#263;"
  ]
  node [
    id 100
    label "chodzi&#263;"
  ]
  node [
    id 101
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 102
    label "equal"
  ]
  node [
    id 103
    label "gorzka_pigu&#322;ka"
  ]
  node [
    id 104
    label "lipa"
  ]
  node [
    id 105
    label "przegra"
  ]
  node [
    id 106
    label "wysiadka"
  ]
  node [
    id 107
    label "passa"
  ]
  node [
    id 108
    label "niefortunno&#347;&#263;"
  ]
  node [
    id 109
    label "po&#322;o&#380;enie"
  ]
  node [
    id 110
    label "niepowodzenie"
  ]
  node [
    id 111
    label "reverse"
  ]
  node [
    id 112
    label "rezultat"
  ]
  node [
    id 113
    label "k&#322;adzenie"
  ]
  node [
    id 114
    label "cz&#322;owiek"
  ]
  node [
    id 115
    label "kolejny"
  ]
  node [
    id 116
    label "istota_&#380;ywa"
  ]
  node [
    id 117
    label "najgorszy"
  ]
  node [
    id 118
    label "aktualny"
  ]
  node [
    id 119
    label "ostatnio"
  ]
  node [
    id 120
    label "niedawno"
  ]
  node [
    id 121
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 122
    label "sko&#324;czony"
  ]
  node [
    id 123
    label "poprzedni"
  ]
  node [
    id 124
    label "pozosta&#322;y"
  ]
  node [
    id 125
    label "w&#261;tpliwy"
  ]
  node [
    id 126
    label "skrutator"
  ]
  node [
    id 127
    label "g&#322;osowanie"
  ]
  node [
    id 128
    label "pisa&#263;"
  ]
  node [
    id 129
    label "odmianka"
  ]
  node [
    id 130
    label "opu&#347;ci&#263;"
  ]
  node [
    id 131
    label "wypowied&#378;"
  ]
  node [
    id 132
    label "wytw&#243;r"
  ]
  node [
    id 133
    label "koniektura"
  ]
  node [
    id 134
    label "preparacja"
  ]
  node [
    id 135
    label "ekscerpcja"
  ]
  node [
    id 136
    label "j&#281;zykowo"
  ]
  node [
    id 137
    label "obelga"
  ]
  node [
    id 138
    label "dzie&#322;o"
  ]
  node [
    id 139
    label "redakcja"
  ]
  node [
    id 140
    label "pomini&#281;cie"
  ]
  node [
    id 141
    label "nazwa&#263;"
  ]
  node [
    id 142
    label "title"
  ]
  node [
    id 143
    label "niemoralny"
  ]
  node [
    id 144
    label "niegrzeczny"
  ]
  node [
    id 145
    label "strasznie"
  ]
  node [
    id 146
    label "kurewski"
  ]
  node [
    id 147
    label "olbrzymi"
  ]
  node [
    id 148
    label "brat"
  ]
  node [
    id 149
    label "semi-detached_house"
  ]
  node [
    id 150
    label "bli&#378;ni&#281;"
  ]
  node [
    id 151
    label "g&#243;ra"
  ]
  node [
    id 152
    label "bli&#378;ni&#281;ta"
  ]
  node [
    id 153
    label "bluzka"
  ]
  node [
    id 154
    label "komplet"
  ]
  node [
    id 155
    label "sweter"
  ]
  node [
    id 156
    label "dom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 21
    target 129
  ]
  edge [
    source 21
    target 130
  ]
  edge [
    source 21
    target 131
  ]
  edge [
    source 21
    target 132
  ]
  edge [
    source 21
    target 133
  ]
  edge [
    source 21
    target 134
  ]
  edge [
    source 21
    target 135
  ]
  edge [
    source 21
    target 136
  ]
  edge [
    source 21
    target 137
  ]
  edge [
    source 21
    target 138
  ]
  edge [
    source 21
    target 139
  ]
  edge [
    source 21
    target 140
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 141
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 143
  ]
  edge [
    source 24
    target 144
  ]
  edge [
    source 24
    target 145
  ]
  edge [
    source 24
    target 146
  ]
  edge [
    source 24
    target 147
  ]
  edge [
    source 25
    target 148
  ]
  edge [
    source 25
    target 149
  ]
  edge [
    source 25
    target 150
  ]
  edge [
    source 25
    target 151
  ]
  edge [
    source 25
    target 152
  ]
  edge [
    source 25
    target 153
  ]
  edge [
    source 25
    target 154
  ]
  edge [
    source 25
    target 155
  ]
  edge [
    source 25
    target 156
  ]
]
