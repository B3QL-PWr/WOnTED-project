graph [
  maxDegree 11
  minDegree 1
  meanDegree 2
  density 0.031746031746031744
  graphCliqueNumber 2
  node [
    id 0
    label "proporcjonalny"
    origin "text"
  ]
  node [
    id 1
    label "podzia&#322;"
    origin "text"
  ]
  node [
    id 2
    label "zysk"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "stosowa&#263;by"
    origin "text"
  ]
  node [
    id 5
    label "niezale&#380;ny"
    origin "text"
  ]
  node [
    id 6
    label "podmiot"
    origin "text"
  ]
  node [
    id 7
    label "uczestniczy&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dana"
    origin "text"
  ]
  node [
    id 9
    label "transakcja"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "pomoc"
    origin "text"
  ]
  node [
    id 12
    label "adekwatny"
  ]
  node [
    id 13
    label "proporcjonalnie"
  ]
  node [
    id 14
    label "prosty"
  ]
  node [
    id 15
    label "harmonijny"
  ]
  node [
    id 16
    label "analogiczny"
  ]
  node [
    id 17
    label "jednakowy"
  ]
  node [
    id 18
    label "foremny"
  ]
  node [
    id 19
    label "wytw&#243;r"
  ]
  node [
    id 20
    label "competence"
  ]
  node [
    id 21
    label "eksdywizja"
  ]
  node [
    id 22
    label "stopie&#324;"
  ]
  node [
    id 23
    label "blastogeneza"
  ]
  node [
    id 24
    label "wydarzenie"
  ]
  node [
    id 25
    label "fission"
  ]
  node [
    id 26
    label "distribution"
  ]
  node [
    id 27
    label "dobro"
  ]
  node [
    id 28
    label "zaleta"
  ]
  node [
    id 29
    label "doch&#243;d"
  ]
  node [
    id 30
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 31
    label "usamodzielnienie"
  ]
  node [
    id 32
    label "niezale&#380;nie"
  ]
  node [
    id 33
    label "usamodzielnianie"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "byt"
  ]
  node [
    id 36
    label "organizacja"
  ]
  node [
    id 37
    label "prawo"
  ]
  node [
    id 38
    label "nauka_prawa"
  ]
  node [
    id 39
    label "osobowo&#347;&#263;"
  ]
  node [
    id 40
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 41
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 42
    label "participate"
  ]
  node [
    id 43
    label "robi&#263;"
  ]
  node [
    id 44
    label "by&#263;"
  ]
  node [
    id 45
    label "dar"
  ]
  node [
    id 46
    label "cnota"
  ]
  node [
    id 47
    label "buddyzm"
  ]
  node [
    id 48
    label "czynno&#347;&#263;"
  ]
  node [
    id 49
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 50
    label "zam&#243;wienie"
  ]
  node [
    id 51
    label "kontrakt_terminowy"
  ]
  node [
    id 52
    label "facjenda"
  ]
  node [
    id 53
    label "cena_transferowa"
  ]
  node [
    id 54
    label "arbitra&#380;"
  ]
  node [
    id 55
    label "zgodzi&#263;"
  ]
  node [
    id 56
    label "pomocnik"
  ]
  node [
    id 57
    label "property"
  ]
  node [
    id 58
    label "przedmiot"
  ]
  node [
    id 59
    label "grupa"
  ]
  node [
    id 60
    label "telefon_zaufania"
  ]
  node [
    id 61
    label "darowizna"
  ]
  node [
    id 62
    label "&#347;rodek"
  ]
  node [
    id 63
    label "liga"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
]
