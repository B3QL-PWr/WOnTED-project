graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.0694444444444446
  density 0.014471639471639472
  graphCliqueNumber 2
  node [
    id 0
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 1
    label "nic"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "zmieni&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 5
    label "bliski"
    origin "text"
  ]
  node [
    id 6
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "wis&#322;a"
    origin "text"
  ]
  node [
    id 8
    label "krak"
    origin "text"
  ]
  node [
    id 9
    label "przyst&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "rozgrywka"
    origin "text"
  ]
  node [
    id 11
    label "wiosna"
    origin "text"
  ]
  node [
    id 12
    label "obecna"
    origin "text"
  ]
  node [
    id 13
    label "dru&#380;yna"
    origin "text"
  ]
  node [
    id 14
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 15
    label "rozwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 16
    label "kolejny"
    origin "text"
  ]
  node [
    id 17
    label "sezon"
    origin "text"
  ]
  node [
    id 18
    label "rozpocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "czwarta"
    origin "text"
  ]
  node [
    id 20
    label "liga"
    origin "text"
  ]
  node [
    id 21
    label "miernota"
  ]
  node [
    id 22
    label "g&#243;wno"
  ]
  node [
    id 23
    label "love"
  ]
  node [
    id 24
    label "ilo&#347;&#263;"
  ]
  node [
    id 25
    label "brak"
  ]
  node [
    id 26
    label "ciura"
  ]
  node [
    id 27
    label "come_up"
  ]
  node [
    id 28
    label "straci&#263;"
  ]
  node [
    id 29
    label "przej&#347;&#263;"
  ]
  node [
    id 30
    label "zast&#261;pi&#263;"
  ]
  node [
    id 31
    label "sprawi&#263;"
  ]
  node [
    id 32
    label "zyska&#263;"
  ]
  node [
    id 33
    label "zrobi&#263;"
  ]
  node [
    id 34
    label "change"
  ]
  node [
    id 35
    label "si&#322;a"
  ]
  node [
    id 36
    label "stan"
  ]
  node [
    id 37
    label "lina"
  ]
  node [
    id 38
    label "way"
  ]
  node [
    id 39
    label "cable"
  ]
  node [
    id 40
    label "przebieg"
  ]
  node [
    id 41
    label "zbi&#243;r"
  ]
  node [
    id 42
    label "ch&#243;d"
  ]
  node [
    id 43
    label "trasa"
  ]
  node [
    id 44
    label "rz&#261;d"
  ]
  node [
    id 45
    label "k&#322;us"
  ]
  node [
    id 46
    label "progression"
  ]
  node [
    id 47
    label "current"
  ]
  node [
    id 48
    label "pr&#261;d"
  ]
  node [
    id 49
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 50
    label "wydarzenie"
  ]
  node [
    id 51
    label "lot"
  ]
  node [
    id 52
    label "cz&#322;owiek"
  ]
  node [
    id 53
    label "blisko"
  ]
  node [
    id 54
    label "przesz&#322;y"
  ]
  node [
    id 55
    label "gotowy"
  ]
  node [
    id 56
    label "dok&#322;adny"
  ]
  node [
    id 57
    label "kr&#243;tki"
  ]
  node [
    id 58
    label "znajomy"
  ]
  node [
    id 59
    label "przysz&#322;y"
  ]
  node [
    id 60
    label "oddalony"
  ]
  node [
    id 61
    label "silny"
  ]
  node [
    id 62
    label "zbli&#380;enie"
  ]
  node [
    id 63
    label "zwi&#261;zany"
  ]
  node [
    id 64
    label "nieodleg&#322;y"
  ]
  node [
    id 65
    label "ma&#322;y"
  ]
  node [
    id 66
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 67
    label "doba"
  ]
  node [
    id 68
    label "czas"
  ]
  node [
    id 69
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 70
    label "weekend"
  ]
  node [
    id 71
    label "miesi&#261;c"
  ]
  node [
    id 72
    label "wej&#347;&#263;"
  ]
  node [
    id 73
    label "cause"
  ]
  node [
    id 74
    label "zacz&#261;&#263;"
  ]
  node [
    id 75
    label "mount"
  ]
  node [
    id 76
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 77
    label "euroliga"
  ]
  node [
    id 78
    label "zagrywka"
  ]
  node [
    id 79
    label "rewan&#380;owy"
  ]
  node [
    id 80
    label "faza"
  ]
  node [
    id 81
    label "interliga"
  ]
  node [
    id 82
    label "trafienie"
  ]
  node [
    id 83
    label "runda"
  ]
  node [
    id 84
    label "contest"
  ]
  node [
    id 85
    label "nowalijka"
  ]
  node [
    id 86
    label "pora_roku"
  ]
  node [
    id 87
    label "przedn&#243;wek"
  ]
  node [
    id 88
    label "rok"
  ]
  node [
    id 89
    label "pocz&#261;tek"
  ]
  node [
    id 90
    label "zwiesna"
  ]
  node [
    id 91
    label "whole"
  ]
  node [
    id 92
    label "szczep"
  ]
  node [
    id 93
    label "dublet"
  ]
  node [
    id 94
    label "pluton"
  ]
  node [
    id 95
    label "formacja"
  ]
  node [
    id 96
    label "zesp&#243;&#322;"
  ]
  node [
    id 97
    label "force"
  ]
  node [
    id 98
    label "zast&#281;p"
  ]
  node [
    id 99
    label "pododdzia&#322;"
  ]
  node [
    id 100
    label "proceed"
  ]
  node [
    id 101
    label "catch"
  ]
  node [
    id 102
    label "pozosta&#263;"
  ]
  node [
    id 103
    label "osta&#263;_si&#281;"
  ]
  node [
    id 104
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 105
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 106
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 107
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 108
    label "unloose"
  ]
  node [
    id 109
    label "urzeczywistni&#263;"
  ]
  node [
    id 110
    label "usun&#261;&#263;"
  ]
  node [
    id 111
    label "wymy&#347;li&#263;"
  ]
  node [
    id 112
    label "bring"
  ]
  node [
    id 113
    label "przesta&#263;"
  ]
  node [
    id 114
    label "undo"
  ]
  node [
    id 115
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 116
    label "inny"
  ]
  node [
    id 117
    label "nast&#281;pnie"
  ]
  node [
    id 118
    label "kt&#243;ry&#347;"
  ]
  node [
    id 119
    label "kolejno"
  ]
  node [
    id 120
    label "nastopny"
  ]
  node [
    id 121
    label "season"
  ]
  node [
    id 122
    label "serial"
  ]
  node [
    id 123
    label "seria"
  ]
  node [
    id 124
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 125
    label "do"
  ]
  node [
    id 126
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 127
    label "godzina"
  ]
  node [
    id 128
    label "poziom"
  ]
  node [
    id 129
    label "organizacja"
  ]
  node [
    id 130
    label "&#347;rodowisko"
  ]
  node [
    id 131
    label "atak"
  ]
  node [
    id 132
    label "obrona"
  ]
  node [
    id 133
    label "grupa"
  ]
  node [
    id 134
    label "mecz_mistrzowski"
  ]
  node [
    id 135
    label "pr&#243;ba"
  ]
  node [
    id 136
    label "arrangement"
  ]
  node [
    id 137
    label "pomoc"
  ]
  node [
    id 138
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 139
    label "union"
  ]
  node [
    id 140
    label "rezerwa"
  ]
  node [
    id 141
    label "moneta"
  ]
  node [
    id 142
    label "Wis&#322;a"
  ]
  node [
    id 143
    label "Krak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 34
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 68
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 73
  ]
  edge [
    source 18
    target 74
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 33
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 41
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 142
    target 143
  ]
]
