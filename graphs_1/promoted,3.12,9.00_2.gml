graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 1
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 2
    label "wyja&#347;ni&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "dzieje"
    origin "text"
  ]
  node [
    id 5
    label "programista"
    origin "text"
  ]
  node [
    id 6
    label "wykop"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "znaczenie"
  ]
  node [
    id 9
    label "go&#347;&#263;"
  ]
  node [
    id 10
    label "osoba"
  ]
  node [
    id 11
    label "posta&#263;"
  ]
  node [
    id 12
    label "explain"
  ]
  node [
    id 13
    label "przedstawi&#263;"
  ]
  node [
    id 14
    label "poja&#347;ni&#263;"
  ]
  node [
    id 15
    label "clear"
  ]
  node [
    id 16
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 17
    label "epoka"
  ]
  node [
    id 18
    label "informatyk"
  ]
  node [
    id 19
    label "odwa&#322;"
  ]
  node [
    id 20
    label "chody"
  ]
  node [
    id 21
    label "grodzisko"
  ]
  node [
    id 22
    label "budowa"
  ]
  node [
    id 23
    label "kopniak"
  ]
  node [
    id 24
    label "wyrobisko"
  ]
  node [
    id 25
    label "zrzutowy"
  ]
  node [
    id 26
    label "szaniec"
  ]
  node [
    id 27
    label "odk&#322;ad"
  ]
  node [
    id 28
    label "wg&#322;&#281;bienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
]
