graph [
  maxDegree 18
  minDegree 1
  meanDegree 2
  density 0.05714285714285714
  graphCliqueNumber 3
  node [
    id 0
    label "tajemnica"
    origin "text"
  ]
  node [
    id 1
    label "lord"
    origin "text"
  ]
  node [
    id 2
    label "singelworth"
    origin "text"
  ]
  node [
    id 3
    label "dyskrecja"
  ]
  node [
    id 4
    label "secret"
  ]
  node [
    id 5
    label "zachowa&#263;"
  ]
  node [
    id 6
    label "zachowanie"
  ]
  node [
    id 7
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 8
    label "zachowywa&#263;"
  ]
  node [
    id 9
    label "wiedza"
  ]
  node [
    id 10
    label "wyda&#263;"
  ]
  node [
    id 11
    label "informacja"
  ]
  node [
    id 12
    label "rzecz"
  ]
  node [
    id 13
    label "enigmat"
  ]
  node [
    id 14
    label "zachowywanie"
  ]
  node [
    id 15
    label "wydawa&#263;"
  ]
  node [
    id 16
    label "wypaplanie"
  ]
  node [
    id 17
    label "taj&#324;"
  ]
  node [
    id 18
    label "spos&#243;b"
  ]
  node [
    id 19
    label "obowi&#261;zek"
  ]
  node [
    id 20
    label "dostojnik"
  ]
  node [
    id 21
    label "arystokrata"
  ]
  node [
    id 22
    label "lordostwo"
  ]
  node [
    id 23
    label "tytu&#322;"
  ]
  node [
    id 24
    label "milord"
  ]
  node [
    id 25
    label "lew"
  ]
  node [
    id 26
    label "&#8212;"
  ]
  node [
    id 27
    label "bia&#322;y"
  ]
  node [
    id 28
    label "Antonio"
  ]
  node [
    id 29
    label "della"
  ]
  node [
    id 30
    label "Brenta"
  ]
  node [
    id 31
    label "di"
  ]
  node [
    id 32
    label "bono"
  ]
  node [
    id 33
    label "Grazia"
  ]
  node [
    id 34
    label "san"
  ]
  node [
    id 35
    label "Luca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
]
