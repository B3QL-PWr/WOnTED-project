graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.6
  density 0.4
  graphCliqueNumber 2
  node [
    id 0
    label "francja"
    origin "text"
  ]
  node [
    id 1
    label "wyprzedzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 3
    label "zrobi&#263;"
  ]
  node [
    id 4
    label "overwhelm"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
]
