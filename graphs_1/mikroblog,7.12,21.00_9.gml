graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9555555555555555
  density 0.044444444444444446
  graphCliqueNumber 2
  node [
    id 0
    label "mirka"
    origin "text"
  ]
  node [
    id 1
    label "uwazam"
    origin "text"
  ]
  node [
    id 2
    label "taki"
    origin "text"
  ]
  node [
    id 3
    label "zdjecia"
    origin "text"
  ]
  node [
    id 4
    label "nadaja"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "prosty"
    origin "text"
  ]
  node [
    id 7
    label "obrzydliwy"
    origin "text"
  ]
  node [
    id 8
    label "okre&#347;lony"
  ]
  node [
    id 9
    label "jaki&#347;"
  ]
  node [
    id 10
    label "si&#281;ga&#263;"
  ]
  node [
    id 11
    label "trwa&#263;"
  ]
  node [
    id 12
    label "obecno&#347;&#263;"
  ]
  node [
    id 13
    label "stan"
  ]
  node [
    id 14
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 15
    label "stand"
  ]
  node [
    id 16
    label "mie&#263;_miejsce"
  ]
  node [
    id 17
    label "uczestniczy&#263;"
  ]
  node [
    id 18
    label "chodzi&#263;"
  ]
  node [
    id 19
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 20
    label "equal"
  ]
  node [
    id 21
    label "&#322;atwy"
  ]
  node [
    id 22
    label "prostowanie_si&#281;"
  ]
  node [
    id 23
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 24
    label "rozprostowanie"
  ]
  node [
    id 25
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 26
    label "prostoduszny"
  ]
  node [
    id 27
    label "naturalny"
  ]
  node [
    id 28
    label "naiwny"
  ]
  node [
    id 29
    label "cios"
  ]
  node [
    id 30
    label "prostowanie"
  ]
  node [
    id 31
    label "niepozorny"
  ]
  node [
    id 32
    label "zwyk&#322;y"
  ]
  node [
    id 33
    label "prosto"
  ]
  node [
    id 34
    label "po_prostu"
  ]
  node [
    id 35
    label "skromny"
  ]
  node [
    id 36
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 37
    label "parchaty"
  ]
  node [
    id 38
    label "okropny"
  ]
  node [
    id 39
    label "paskudnie"
  ]
  node [
    id 40
    label "niezno&#347;ny"
  ]
  node [
    id 41
    label "paskudny"
  ]
  node [
    id 42
    label "z&#322;y"
  ]
  node [
    id 43
    label "straszny"
  ]
  node [
    id 44
    label "obrzydliwie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
]
