graph [
  maxDegree 3
  minDegree 0
  meanDegree 1.1428571428571428
  density 0.19047619047619047
  graphCliqueNumber 2
  node [
    id 0
    label "simcity"
    origin "text"
  ]
  node [
    id 1
    label "SimCity"
  ]
  node [
    id 2
    label "3000"
  ]
  node [
    id 3
    label "willa"
  ]
  node [
    id 4
    label "Wright"
  ]
  node [
    id 5
    label "4"
  ]
  node [
    id 6
    label "2000"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 3
    target 4
  ]
]
