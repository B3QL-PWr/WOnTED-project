graph [
  maxDegree 14
  minDegree 1
  meanDegree 5.411764705882353
  density 0.16399286987522282
  graphCliqueNumber 12
  node [
    id 0
    label "wicemarsza&#322;ek"
    origin "text"
  ]
  node [
    id 1
    label "stefan"
    origin "text"
  ]
  node [
    id 2
    label "niesio&#322;owski"
    origin "text"
  ]
  node [
    id 3
    label "marsza&#322;ek"
  ]
  node [
    id 4
    label "zast&#281;pca"
  ]
  node [
    id 5
    label "Stefan"
  ]
  node [
    id 6
    label "Niesio&#322;owski"
  ]
  node [
    id 7
    label "Stanis&#322;awa"
  ]
  node [
    id 8
    label "Olas"
  ]
  node [
    id 9
    label "polskie"
  ]
  node [
    id 10
    label "stronnictwo"
  ]
  node [
    id 11
    label "ludowy"
  ]
  node [
    id 12
    label "ustawa"
  ]
  node [
    id 13
    label "kodeks"
  ]
  node [
    id 14
    label "post&#281;powa&#263;"
  ]
  node [
    id 15
    label "karny"
  ]
  node [
    id 16
    label "ojciec"
  ]
  node [
    id 17
    label "zmiana"
  ]
  node [
    id 18
    label "wykonawczy"
  ]
  node [
    id 19
    label "skarbowy"
  ]
  node [
    id 20
    label "oraz"
  ]
  node [
    id 21
    label "niekt&#243;ry"
  ]
  node [
    id 22
    label "inny"
  ]
  node [
    id 23
    label "ustawi&#263;"
  ]
  node [
    id 24
    label "policja"
  ]
  node [
    id 25
    label "komisja"
  ]
  node [
    id 26
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 27
    label "i"
  ]
  node [
    id 28
    label "prawy"
  ]
  node [
    id 29
    label "cz&#322;owiek"
  ]
  node [
    id 30
    label "Miros&#322;awa"
  ]
  node [
    id 31
    label "Pawlak"
  ]
  node [
    id 32
    label "super"
  ]
  node [
    id 33
    label "express"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 12
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 21
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 13
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 15
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
]
