graph [
  maxDegree 11
  minDegree 1
  meanDegree 2
  density 0.045454545454545456
  graphCliqueNumber 2
  node [
    id 0
    label "samsung"
    origin "text"
  ]
  node [
    id 1
    label "przy&#322;apa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "raz"
    origin "text"
  ]
  node [
    id 3
    label "kolejny"
    origin "text"
  ]
  node [
    id 4
    label "oszustwo"
    origin "text"
  ]
  node [
    id 5
    label "przy"
    origin "text"
  ]
  node [
    id 6
    label "reklamowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "swoje"
    origin "text"
  ]
  node [
    id 8
    label "nowy"
    origin "text"
  ]
  node [
    id 9
    label "smartphona"
    origin "text"
  ]
  node [
    id 10
    label "urz&#261;dzenie"
  ]
  node [
    id 11
    label "zasta&#263;"
  ]
  node [
    id 12
    label "na_gor&#261;cym_uczynku"
  ]
  node [
    id 13
    label "catch"
  ]
  node [
    id 14
    label "przydyba&#263;"
  ]
  node [
    id 15
    label "chwila"
  ]
  node [
    id 16
    label "uderzenie"
  ]
  node [
    id 17
    label "cios"
  ]
  node [
    id 18
    label "time"
  ]
  node [
    id 19
    label "inny"
  ]
  node [
    id 20
    label "nast&#281;pnie"
  ]
  node [
    id 21
    label "kt&#243;ry&#347;"
  ]
  node [
    id 22
    label "kolejno"
  ]
  node [
    id 23
    label "nastopny"
  ]
  node [
    id 24
    label "czyn"
  ]
  node [
    id 25
    label "trickery"
  ]
  node [
    id 26
    label "s&#322;up"
  ]
  node [
    id 27
    label "ba&#322;amutnia"
  ]
  node [
    id 28
    label "&#347;ciema"
  ]
  node [
    id 29
    label "ask"
  ]
  node [
    id 30
    label "wywy&#380;sza&#263;"
  ]
  node [
    id 31
    label "sk&#322;ada&#263;"
  ]
  node [
    id 32
    label "domaga&#263;_si&#281;"
  ]
  node [
    id 33
    label "zachwala&#263;"
  ]
  node [
    id 34
    label "nagradza&#263;"
  ]
  node [
    id 35
    label "publicize"
  ]
  node [
    id 36
    label "glorify"
  ]
  node [
    id 37
    label "cz&#322;owiek"
  ]
  node [
    id 38
    label "nowotny"
  ]
  node [
    id 39
    label "drugi"
  ]
  node [
    id 40
    label "bie&#380;&#261;cy"
  ]
  node [
    id 41
    label "nowo"
  ]
  node [
    id 42
    label "narybek"
  ]
  node [
    id 43
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 44
    label "obcy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
]
