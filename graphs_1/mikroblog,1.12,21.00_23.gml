graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.6923076923076923
  density 0.14102564102564102
  graphCliqueNumber 2
  node [
    id 0
    label "kupi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ostatnio"
    origin "text"
  ]
  node [
    id 2
    label "skarpetka"
    origin "text"
  ]
  node [
    id 3
    label "zdj"
    origin "text"
  ]
  node [
    id 4
    label "heheszki"
    origin "text"
  ]
  node [
    id 5
    label "ostatni"
  ]
  node [
    id 6
    label "poprzednio"
  ]
  node [
    id 7
    label "aktualnie"
  ]
  node [
    id 8
    label "dodatek"
  ]
  node [
    id 9
    label "dzianina"
  ]
  node [
    id 10
    label "&#322;atka"
  ]
  node [
    id 11
    label "H"
  ]
  node [
    id 12
    label "ampM"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 11
    target 12
  ]
]
