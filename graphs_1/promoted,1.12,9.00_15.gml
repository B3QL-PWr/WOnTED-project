graph [
  maxDegree 36
  minDegree 1
  meanDegree 2.062015503875969
  density 0.01610949612403101
  graphCliqueNumber 4
  node [
    id 0
    label "latko"
    origin "text"
  ]
  node [
    id 1
    label "usa"
    origin "text"
  ]
  node [
    id 2
    label "postrzeli&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "twarz"
    origin "text"
  ]
  node [
    id 5
    label "ca&#322;kowicie"
    origin "text"
  ]
  node [
    id 6
    label "deformowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "nos"
    origin "text"
  ]
  node [
    id 8
    label "szcz&#281;ka"
    origin "text"
  ]
  node [
    id 9
    label "podniebienie"
    origin "text"
  ]
  node [
    id 10
    label "z&#261;b"
    origin "text"
  ]
  node [
    id 11
    label "zrani&#263;"
  ]
  node [
    id 12
    label "pip"
  ]
  node [
    id 13
    label "cz&#322;owiek"
  ]
  node [
    id 14
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 15
    label "profil"
  ]
  node [
    id 16
    label "ucho"
  ]
  node [
    id 17
    label "policzek"
  ]
  node [
    id 18
    label "czo&#322;o"
  ]
  node [
    id 19
    label "usta"
  ]
  node [
    id 20
    label "micha"
  ]
  node [
    id 21
    label "powieka"
  ]
  node [
    id 22
    label "podbr&#243;dek"
  ]
  node [
    id 23
    label "p&#243;&#322;profil"
  ]
  node [
    id 24
    label "wyraz_twarzy"
  ]
  node [
    id 25
    label "liczko"
  ]
  node [
    id 26
    label "dzi&#243;b"
  ]
  node [
    id 27
    label "rys"
  ]
  node [
    id 28
    label "zas&#322;ona"
  ]
  node [
    id 29
    label "twarzyczka"
  ]
  node [
    id 30
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 31
    label "reputacja"
  ]
  node [
    id 32
    label "pysk"
  ]
  node [
    id 33
    label "cera"
  ]
  node [
    id 34
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 35
    label "p&#322;e&#263;"
  ]
  node [
    id 36
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 37
    label "maskowato&#347;&#263;"
  ]
  node [
    id 38
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 39
    label "przedstawiciel"
  ]
  node [
    id 40
    label "brew"
  ]
  node [
    id 41
    label "uj&#281;cie"
  ]
  node [
    id 42
    label "prz&#243;d"
  ]
  node [
    id 43
    label "posta&#263;"
  ]
  node [
    id 44
    label "wielko&#347;&#263;"
  ]
  node [
    id 45
    label "oko"
  ]
  node [
    id 46
    label "kompletny"
  ]
  node [
    id 47
    label "wniwecz"
  ]
  node [
    id 48
    label "zupe&#322;ny"
  ]
  node [
    id 49
    label "zmienia&#263;"
  ]
  node [
    id 50
    label "corrupt"
  ]
  node [
    id 51
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 52
    label "psychika"
  ]
  node [
    id 53
    label "si&#261;kanie"
  ]
  node [
    id 54
    label "si&#261;ka&#263;"
  ]
  node [
    id 55
    label "cz&#322;onek"
  ]
  node [
    id 56
    label "nozdrze"
  ]
  node [
    id 57
    label "rezonator"
  ]
  node [
    id 58
    label "katar"
  ]
  node [
    id 59
    label "eskimoski"
  ]
  node [
    id 60
    label "otw&#243;r_nosowy"
  ]
  node [
    id 61
    label "ozena"
  ]
  node [
    id 62
    label "jama_nosowa"
  ]
  node [
    id 63
    label "si&#261;kn&#261;&#263;"
  ]
  node [
    id 64
    label "eskimosek"
  ]
  node [
    id 65
    label "arhinia"
  ]
  node [
    id 66
    label "zako&#324;czenie"
  ]
  node [
    id 67
    label "ma&#322;&#380;owina_nosowa"
  ]
  node [
    id 68
    label "przegroda_nosowa"
  ]
  node [
    id 69
    label "si&#261;kni&#281;cie"
  ]
  node [
    id 70
    label "artykulacja"
  ]
  node [
    id 71
    label "szczena"
  ]
  node [
    id 72
    label "ko&#347;&#263;"
  ]
  node [
    id 73
    label "kram"
  ]
  node [
    id 74
    label "z&#281;bod&#243;&#322;"
  ]
  node [
    id 75
    label "imad&#322;o"
  ]
  node [
    id 76
    label "guzowato&#347;&#263;_br&#243;dkowa"
  ]
  node [
    id 77
    label "artykulator"
  ]
  node [
    id 78
    label "wi&#261;zanie"
  ]
  node [
    id 79
    label "trzewioczaszka"
  ]
  node [
    id 80
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 81
    label "hamulec"
  ]
  node [
    id 82
    label "jama_ustna"
  ]
  node [
    id 83
    label "&#322;&#281;kotka"
  ]
  node [
    id 84
    label "czaszka"
  ]
  node [
    id 85
    label "dzi&#261;s&#322;o"
  ]
  node [
    id 86
    label "ko&#347;&#263;_z&#281;bowa"
  ]
  node [
    id 87
    label "smakowa&#263;"
  ]
  node [
    id 88
    label "k&#322;ucie_w_z&#281;by"
  ]
  node [
    id 89
    label "smakowanie"
  ]
  node [
    id 90
    label "podniebienie_wt&#243;rne"
  ]
  node [
    id 91
    label "nip"
  ]
  node [
    id 92
    label "palate"
  ]
  node [
    id 93
    label "zmys&#322;"
  ]
  node [
    id 94
    label "podniebienie_mi&#281;kkie"
  ]
  node [
    id 95
    label "k&#322;u&#263;_w_z&#281;by"
  ]
  node [
    id 96
    label "podniebienie_twarde"
  ]
  node [
    id 97
    label "strona"
  ]
  node [
    id 98
    label "rozszczep_podniebienia"
  ]
  node [
    id 99
    label "kubek_smakowy"
  ]
  node [
    id 100
    label "uz&#281;bienie"
  ]
  node [
    id 101
    label "plombowa&#263;"
  ]
  node [
    id 102
    label "leczenie_kana&#322;owe"
  ]
  node [
    id 103
    label "polakowa&#263;"
  ]
  node [
    id 104
    label "korona"
  ]
  node [
    id 105
    label "zaplombowa&#263;"
  ]
  node [
    id 106
    label "ubytek"
  ]
  node [
    id 107
    label "emaliowa&#263;"
  ]
  node [
    id 108
    label "zaplombowanie"
  ]
  node [
    id 109
    label "kamfenol"
  ]
  node [
    id 110
    label "obr&#281;cz"
  ]
  node [
    id 111
    label "plombowanie"
  ]
  node [
    id 112
    label "z&#281;batka"
  ]
  node [
    id 113
    label "szczoteczka"
  ]
  node [
    id 114
    label "mostek"
  ]
  node [
    id 115
    label "&#322;uk_z&#281;bowy"
  ]
  node [
    id 116
    label "&#380;uchwa"
  ]
  node [
    id 117
    label "emaliowanie"
  ]
  node [
    id 118
    label "z&#281;bina"
  ]
  node [
    id 119
    label "ostrze"
  ]
  node [
    id 120
    label "polakowanie"
  ]
  node [
    id 121
    label "tooth"
  ]
  node [
    id 122
    label "borowa&#263;"
  ]
  node [
    id 123
    label "borowanie"
  ]
  node [
    id 124
    label "cement"
  ]
  node [
    id 125
    label "szkliwo"
  ]
  node [
    id 126
    label "wierzcho&#322;ek_korzenia"
  ]
  node [
    id 127
    label "miazga_z&#281;ba"
  ]
  node [
    id 128
    label "element_anatomiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
]
