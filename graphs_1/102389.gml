graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.0423280423280423
  density 0.010863447033659799
  graphCliqueNumber 3
  node [
    id 0
    label "zasilacz"
    origin "text"
  ]
  node [
    id 1
    label "atx"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "interesuj&#261;cy"
    origin "text"
  ]
  node [
    id 4
    label "cecha"
    origin "text"
  ]
  node [
    id 5
    label "przy"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 7
    label "obci&#261;&#380;enie"
    origin "text"
  ]
  node [
    id 8
    label "wyj&#347;cie"
    origin "text"
  ]
  node [
    id 9
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "spadek"
    origin "text"
  ]
  node [
    id 11
    label "napi&#281;cie"
    origin "text"
  ]
  node [
    id 12
    label "tym"
    origin "text"
  ]
  node [
    id 13
    label "poni&#380;ej"
    origin "text"
  ]
  node [
    id 14
    label "przerabia&#263;"
    origin "text"
  ]
  node [
    id 15
    label "przez"
    origin "text"
  ]
  node [
    id 16
    label "nasa"
    origin "text"
  ]
  node [
    id 17
    label "egzemplarz"
    origin "text"
  ]
  node [
    id 18
    label "opornik"
    origin "text"
  ]
  node [
    id 19
    label "pr&#261;d"
    origin "text"
  ]
  node [
    id 20
    label "spa&#347;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "p&#281;tla_ociekowa"
  ]
  node [
    id 22
    label "urz&#261;dzenie"
  ]
  node [
    id 23
    label "czyj&#347;"
  ]
  node [
    id 24
    label "m&#261;&#380;"
  ]
  node [
    id 25
    label "swoisty"
  ]
  node [
    id 26
    label "atrakcyjny"
  ]
  node [
    id 27
    label "ciekawie"
  ]
  node [
    id 28
    label "interesuj&#261;co"
  ]
  node [
    id 29
    label "dziwny"
  ]
  node [
    id 30
    label "charakterystyka"
  ]
  node [
    id 31
    label "m&#322;ot"
  ]
  node [
    id 32
    label "pr&#243;ba"
  ]
  node [
    id 33
    label "marka"
  ]
  node [
    id 34
    label "attribute"
  ]
  node [
    id 35
    label "znak"
  ]
  node [
    id 36
    label "drzewo"
  ]
  node [
    id 37
    label "doros&#322;y"
  ]
  node [
    id 38
    label "wiele"
  ]
  node [
    id 39
    label "dorodny"
  ]
  node [
    id 40
    label "znaczny"
  ]
  node [
    id 41
    label "du&#380;o"
  ]
  node [
    id 42
    label "prawdziwy"
  ]
  node [
    id 43
    label "niema&#322;o"
  ]
  node [
    id 44
    label "wa&#380;ny"
  ]
  node [
    id 45
    label "rozwini&#281;ty"
  ]
  node [
    id 46
    label "zawada"
  ]
  node [
    id 47
    label "hindrance"
  ]
  node [
    id 48
    label "baga&#380;"
  ]
  node [
    id 49
    label "charge"
  ]
  node [
    id 50
    label "encumbrance"
  ]
  node [
    id 51
    label "psucie_si&#281;"
  ]
  node [
    id 52
    label "oskar&#380;enie"
  ]
  node [
    id 53
    label "zaszkodzenie"
  ]
  node [
    id 54
    label "loading"
  ]
  node [
    id 55
    label "zobowi&#261;zanie"
  ]
  node [
    id 56
    label "na&#322;o&#380;enie"
  ]
  node [
    id 57
    label "transgression"
  ]
  node [
    id 58
    label "postrze&#380;enie"
  ]
  node [
    id 59
    label "withdrawal"
  ]
  node [
    id 60
    label "zagranie"
  ]
  node [
    id 61
    label "policzenie"
  ]
  node [
    id 62
    label "spotkanie"
  ]
  node [
    id 63
    label "odch&#243;d"
  ]
  node [
    id 64
    label "podziewanie_si&#281;"
  ]
  node [
    id 65
    label "uwolnienie_si&#281;"
  ]
  node [
    id 66
    label "ograniczenie"
  ]
  node [
    id 67
    label "exit"
  ]
  node [
    id 68
    label "powiedzenie_si&#281;"
  ]
  node [
    id 69
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 70
    label "kres"
  ]
  node [
    id 71
    label "wywiedzenie_si&#281;"
  ]
  node [
    id 72
    label "wypadni&#281;cie"
  ]
  node [
    id 73
    label "zako&#324;czenie"
  ]
  node [
    id 74
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 75
    label "ruszenie"
  ]
  node [
    id 76
    label "emergence"
  ]
  node [
    id 77
    label "opuszczenie"
  ]
  node [
    id 78
    label "przebywanie"
  ]
  node [
    id 79
    label "deviation"
  ]
  node [
    id 80
    label "podzianie_si&#281;"
  ]
  node [
    id 81
    label "wychodzenie"
  ]
  node [
    id 82
    label "wyczerpanie_si&#281;"
  ]
  node [
    id 83
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 84
    label "uzyskanie"
  ]
  node [
    id 85
    label "przedstawienie"
  ]
  node [
    id 86
    label "miejsce"
  ]
  node [
    id 87
    label "vent"
  ]
  node [
    id 88
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 89
    label "powychodzenie"
  ]
  node [
    id 90
    label "wych&#243;d"
  ]
  node [
    id 91
    label "uko&#324;czenie"
  ]
  node [
    id 92
    label "okazanie_si&#281;"
  ]
  node [
    id 93
    label "release"
  ]
  node [
    id 94
    label "mie&#263;_miejsce"
  ]
  node [
    id 95
    label "chance"
  ]
  node [
    id 96
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 97
    label "alternate"
  ]
  node [
    id 98
    label "naciska&#263;"
  ]
  node [
    id 99
    label "atakowa&#263;"
  ]
  node [
    id 100
    label "ton"
  ]
  node [
    id 101
    label "wydziedziczy&#263;"
  ]
  node [
    id 102
    label "zachowek"
  ]
  node [
    id 103
    label "wydziedziczenie"
  ]
  node [
    id 104
    label "ruch"
  ]
  node [
    id 105
    label "mienie"
  ]
  node [
    id 106
    label "scheda_spadkowa"
  ]
  node [
    id 107
    label "zmiana"
  ]
  node [
    id 108
    label "camber"
  ]
  node [
    id 109
    label "sukcesja"
  ]
  node [
    id 110
    label "p&#322;aszczyzna"
  ]
  node [
    id 111
    label "usztywnienie"
  ]
  node [
    id 112
    label "napr&#281;&#380;enie"
  ]
  node [
    id 113
    label "striving"
  ]
  node [
    id 114
    label "nastr&#243;j"
  ]
  node [
    id 115
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 116
    label "tension"
  ]
  node [
    id 117
    label "nast&#281;pnie"
  ]
  node [
    id 118
    label "poni&#380;szy"
  ]
  node [
    id 119
    label "zalicza&#263;"
  ]
  node [
    id 120
    label "overwork"
  ]
  node [
    id 121
    label "zmienia&#263;"
  ]
  node [
    id 122
    label "sp&#281;dza&#263;"
  ]
  node [
    id 123
    label "zamienia&#263;"
  ]
  node [
    id 124
    label "amend"
  ]
  node [
    id 125
    label "przechodzi&#263;"
  ]
  node [
    id 126
    label "przetwarza&#263;"
  ]
  node [
    id 127
    label "robi&#263;"
  ]
  node [
    id 128
    label "modyfikowa&#263;"
  ]
  node [
    id 129
    label "pracowa&#263;"
  ]
  node [
    id 130
    label "wytwarza&#263;"
  ]
  node [
    id 131
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 132
    label "convert"
  ]
  node [
    id 133
    label "radzi&#263;_sobie"
  ]
  node [
    id 134
    label "przyswoi&#263;"
  ]
  node [
    id 135
    label "ewoluowanie"
  ]
  node [
    id 136
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 137
    label "wytw&#243;r"
  ]
  node [
    id 138
    label "obiekt"
  ]
  node [
    id 139
    label "przyswajanie"
  ]
  node [
    id 140
    label "reakcja"
  ]
  node [
    id 141
    label "wyewoluowanie"
  ]
  node [
    id 142
    label "ewoluowa&#263;"
  ]
  node [
    id 143
    label "nicpo&#324;"
  ]
  node [
    id 144
    label "przyswojenie"
  ]
  node [
    id 145
    label "wyewoluowa&#263;"
  ]
  node [
    id 146
    label "okaz"
  ]
  node [
    id 147
    label "part"
  ]
  node [
    id 148
    label "individual"
  ]
  node [
    id 149
    label "agent"
  ]
  node [
    id 150
    label "czynnik_biotyczny"
  ]
  node [
    id 151
    label "starzenie_si&#281;"
  ]
  node [
    id 152
    label "przyswaja&#263;"
  ]
  node [
    id 153
    label "sztuka"
  ]
  node [
    id 154
    label "obw&#243;d"
  ]
  node [
    id 155
    label "gasik"
  ]
  node [
    id 156
    label "bobowate_w&#322;a&#347;ciwe"
  ]
  node [
    id 157
    label "ro&#347;lina"
  ]
  node [
    id 158
    label "system"
  ]
  node [
    id 159
    label "przep&#322;yw"
  ]
  node [
    id 160
    label "energia"
  ]
  node [
    id 161
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 162
    label "ideologia"
  ]
  node [
    id 163
    label "dreszcz"
  ]
  node [
    id 164
    label "praktyka"
  ]
  node [
    id 165
    label "metoda"
  ]
  node [
    id 166
    label "apparent_motion"
  ]
  node [
    id 167
    label "electricity"
  ]
  node [
    id 168
    label "zjawisko"
  ]
  node [
    id 169
    label "przyp&#322;yw"
  ]
  node [
    id 170
    label "zu&#380;y&#263;"
  ]
  node [
    id 171
    label "worsen"
  ]
  node [
    id 172
    label "precipitate"
  ]
  node [
    id 173
    label "schudn&#261;&#263;"
  ]
  node [
    id 174
    label "remit"
  ]
  node [
    id 175
    label "obci&#261;&#380;y&#263;"
  ]
  node [
    id 176
    label "spotka&#263;"
  ]
  node [
    id 177
    label "napa&#347;&#263;"
  ]
  node [
    id 178
    label "nakarmi&#263;"
  ]
  node [
    id 179
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 180
    label "ogarn&#261;&#263;"
  ]
  node [
    id 181
    label "decline"
  ]
  node [
    id 182
    label "zmniejszy&#263;_si&#281;"
  ]
  node [
    id 183
    label "spierdoli&#263;_si&#281;"
  ]
  node [
    id 184
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 185
    label "opu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 186
    label "fall"
  ]
  node [
    id 187
    label "schrzani&#263;_si&#281;"
  ]
  node [
    id 188
    label "condescend"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
]
