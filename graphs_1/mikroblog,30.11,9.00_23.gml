graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.1428571428571428
  density 0.19047619047619047
  graphCliqueNumber 2
  node [
    id 0
    label "pornopani"
    origin "text"
  ]
  node [
    id 1
    label "ladnapani"
    origin "text"
  ]
  node [
    id 2
    label "wszystkoboners"
    origin "text"
  ]
  node [
    id 3
    label "Kristina"
  ]
  node [
    id 4
    label "Shcherbinina"
  ]
  node [
    id 5
    label "Liya"
  ]
  node [
    id 6
    label "Silver"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
]
