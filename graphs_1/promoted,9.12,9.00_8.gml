graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.0327868852459017
  density 0.033879781420765025
  graphCliqueNumber 3
  node [
    id 0
    label "startowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wprost"
    origin "text"
  ]
  node [
    id 2
    label "pas"
    origin "text"
  ]
  node [
    id 3
    label "skr&#281;t"
    origin "text"
  ]
  node [
    id 4
    label "lewo"
    origin "text"
  ]
  node [
    id 5
    label "spycha&#263;"
    origin "text"
  ]
  node [
    id 6
    label "passat"
    origin "text"
  ]
  node [
    id 7
    label "katapultowa&#263;"
  ]
  node [
    id 8
    label "uczestniczy&#263;"
  ]
  node [
    id 9
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 10
    label "begin"
  ]
  node [
    id 11
    label "samolot"
  ]
  node [
    id 12
    label "zaczyna&#263;"
  ]
  node [
    id 13
    label "odchodzi&#263;"
  ]
  node [
    id 14
    label "otwarcie"
  ]
  node [
    id 15
    label "prosto"
  ]
  node [
    id 16
    label "naprzeciwko"
  ]
  node [
    id 17
    label "miejsce"
  ]
  node [
    id 18
    label "dodatek"
  ]
  node [
    id 19
    label "linia"
  ]
  node [
    id 20
    label "obiekt"
  ]
  node [
    id 21
    label "sk&#322;ad"
  ]
  node [
    id 22
    label "obszar"
  ]
  node [
    id 23
    label "tarcza_herbowa"
  ]
  node [
    id 24
    label "licytacja"
  ]
  node [
    id 25
    label "zagranie"
  ]
  node [
    id 26
    label "heraldyka"
  ]
  node [
    id 27
    label "kawa&#322;ek"
  ]
  node [
    id 28
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 29
    label "figura_heraldyczna"
  ]
  node [
    id 30
    label "odznaka"
  ]
  node [
    id 31
    label "nap&#281;d"
  ]
  node [
    id 32
    label "wci&#281;cie"
  ]
  node [
    id 33
    label "bielizna"
  ]
  node [
    id 34
    label "podsterowno&#347;&#263;"
  ]
  node [
    id 35
    label "papieros"
  ]
  node [
    id 36
    label "ruch"
  ]
  node [
    id 37
    label "odcinek"
  ]
  node [
    id 38
    label "whirl"
  ]
  node [
    id 39
    label "nadsterowno&#347;&#263;"
  ]
  node [
    id 40
    label "serpentyna"
  ]
  node [
    id 41
    label "trasa"
  ]
  node [
    id 42
    label "kszta&#322;t"
  ]
  node [
    id 43
    label "podejrzanie"
  ]
  node [
    id 44
    label "kierunek"
  ]
  node [
    id 45
    label "kiepsko"
  ]
  node [
    id 46
    label "szko&#322;a"
  ]
  node [
    id 47
    label "nielegalnie"
  ]
  node [
    id 48
    label "lewy"
  ]
  node [
    id 49
    label "zmusza&#263;"
  ]
  node [
    id 50
    label "spychologia"
  ]
  node [
    id 51
    label "obarcza&#263;"
  ]
  node [
    id 52
    label "przemieszcza&#263;"
  ]
  node [
    id 53
    label "zabiera&#263;"
  ]
  node [
    id 54
    label "force"
  ]
  node [
    id 55
    label "give"
  ]
  node [
    id 56
    label "oskar&#380;a&#263;"
  ]
  node [
    id 57
    label "volkswagen"
  ]
  node [
    id 58
    label "Passat"
  ]
  node [
    id 59
    label "samoch&#243;d"
  ]
  node [
    id 60
    label "fastback"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
]
