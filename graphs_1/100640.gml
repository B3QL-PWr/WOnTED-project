graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9636363636363636
  density 0.03636363636363636
  graphCliqueNumber 2
  node [
    id 0
    label "taki"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "los"
    origin "text"
  ]
  node [
    id 3
    label "dzienia"
    origin "text"
  ]
  node [
    id 4
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "okre&#347;lony"
  ]
  node [
    id 6
    label "jaki&#347;"
  ]
  node [
    id 7
    label "si&#281;ga&#263;"
  ]
  node [
    id 8
    label "trwa&#263;"
  ]
  node [
    id 9
    label "obecno&#347;&#263;"
  ]
  node [
    id 10
    label "stan"
  ]
  node [
    id 11
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 12
    label "stand"
  ]
  node [
    id 13
    label "mie&#263;_miejsce"
  ]
  node [
    id 14
    label "uczestniczy&#263;"
  ]
  node [
    id 15
    label "chodzi&#263;"
  ]
  node [
    id 16
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 17
    label "equal"
  ]
  node [
    id 18
    label "si&#322;a"
  ]
  node [
    id 19
    label "przymus"
  ]
  node [
    id 20
    label "rzuci&#263;"
  ]
  node [
    id 21
    label "hazard"
  ]
  node [
    id 22
    label "destiny"
  ]
  node [
    id 23
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 24
    label "bilet"
  ]
  node [
    id 25
    label "przebieg_&#380;ycia"
  ]
  node [
    id 26
    label "&#380;ycie"
  ]
  node [
    id 27
    label "rzucenie"
  ]
  node [
    id 28
    label "procesowicz"
  ]
  node [
    id 29
    label "wypowied&#378;"
  ]
  node [
    id 30
    label "pods&#261;dny"
  ]
  node [
    id 31
    label "podejrzany"
  ]
  node [
    id 32
    label "broni&#263;"
  ]
  node [
    id 33
    label "bronienie"
  ]
  node [
    id 34
    label "system"
  ]
  node [
    id 35
    label "my&#347;l"
  ]
  node [
    id 36
    label "wytw&#243;r"
  ]
  node [
    id 37
    label "urz&#261;d"
  ]
  node [
    id 38
    label "konektyw"
  ]
  node [
    id 39
    label "court"
  ]
  node [
    id 40
    label "obrona"
  ]
  node [
    id 41
    label "s&#261;downictwo"
  ]
  node [
    id 42
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 43
    label "forum"
  ]
  node [
    id 44
    label "zesp&#243;&#322;"
  ]
  node [
    id 45
    label "post&#281;powanie"
  ]
  node [
    id 46
    label "skazany"
  ]
  node [
    id 47
    label "wydarzenie"
  ]
  node [
    id 48
    label "&#347;wiadek"
  ]
  node [
    id 49
    label "antylogizm"
  ]
  node [
    id 50
    label "strona"
  ]
  node [
    id 51
    label "oskar&#380;yciel"
  ]
  node [
    id 52
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 53
    label "biuro"
  ]
  node [
    id 54
    label "instytucja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
]
