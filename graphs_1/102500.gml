graph [
  maxDegree 36
  minDegree 1
  meanDegree 2
  density 0.015384615384615385
  graphCliqueNumber 3
  node [
    id 0
    label "radny"
    origin "text"
  ]
  node [
    id 1
    label "ozorkowski"
    origin "text"
  ]
  node [
    id 2
    label "przyj&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "uchwa&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "scali&#263;"
    origin "text"
  ]
  node [
    id 5
    label "podzia&#322;"
    origin "text"
  ]
  node [
    id 6
    label "grunt"
    origin "text"
  ]
  node [
    id 7
    label "strona"
    origin "text"
  ]
  node [
    id 8
    label "unia"
    origin "text"
  ]
  node [
    id 9
    label "europejski"
    origin "text"
  ]
  node [
    id 10
    label "ula"
    origin "text"
  ]
  node [
    id 11
    label "lipowy"
    origin "text"
  ]
  node [
    id 12
    label "rada_gminy"
  ]
  node [
    id 13
    label "przedstawiciel"
  ]
  node [
    id 14
    label "rajca"
  ]
  node [
    id 15
    label "gmina"
  ]
  node [
    id 16
    label "rada"
  ]
  node [
    id 17
    label "samorz&#261;dowiec"
  ]
  node [
    id 18
    label "dostarczy&#263;"
  ]
  node [
    id 19
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 20
    label "strike"
  ]
  node [
    id 21
    label "przybra&#263;"
  ]
  node [
    id 22
    label "swallow"
  ]
  node [
    id 23
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 24
    label "odebra&#263;"
  ]
  node [
    id 25
    label "umie&#347;ci&#263;"
  ]
  node [
    id 26
    label "obra&#263;"
  ]
  node [
    id 27
    label "fall"
  ]
  node [
    id 28
    label "wzi&#261;&#263;"
  ]
  node [
    id 29
    label "undertake"
  ]
  node [
    id 30
    label "absorb"
  ]
  node [
    id 31
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 32
    label "receive"
  ]
  node [
    id 33
    label "draw"
  ]
  node [
    id 34
    label "zrobi&#263;"
  ]
  node [
    id 35
    label "przyj&#281;cie"
  ]
  node [
    id 36
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 37
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 38
    label "uzna&#263;"
  ]
  node [
    id 39
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 40
    label "resolution"
  ]
  node [
    id 41
    label "akt"
  ]
  node [
    id 42
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 43
    label "ally"
  ]
  node [
    id 44
    label "connect"
  ]
  node [
    id 45
    label "zjednoczy&#263;"
  ]
  node [
    id 46
    label "powi&#261;za&#263;"
  ]
  node [
    id 47
    label "wytw&#243;r"
  ]
  node [
    id 48
    label "competence"
  ]
  node [
    id 49
    label "eksdywizja"
  ]
  node [
    id 50
    label "stopie&#324;"
  ]
  node [
    id 51
    label "blastogeneza"
  ]
  node [
    id 52
    label "wydarzenie"
  ]
  node [
    id 53
    label "fission"
  ]
  node [
    id 54
    label "distribution"
  ]
  node [
    id 55
    label "za&#322;o&#380;enie"
  ]
  node [
    id 56
    label "powierzchnia"
  ]
  node [
    id 57
    label "glinowa&#263;"
  ]
  node [
    id 58
    label "pr&#243;chnica"
  ]
  node [
    id 59
    label "podglebie"
  ]
  node [
    id 60
    label "glinowanie"
  ]
  node [
    id 61
    label "litosfera"
  ]
  node [
    id 62
    label "zasadzenie"
  ]
  node [
    id 63
    label "documentation"
  ]
  node [
    id 64
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 65
    label "teren"
  ]
  node [
    id 66
    label "ryzosfera"
  ]
  node [
    id 67
    label "czynnik_produkcji"
  ]
  node [
    id 68
    label "zasadzi&#263;"
  ]
  node [
    id 69
    label "glej"
  ]
  node [
    id 70
    label "martwica"
  ]
  node [
    id 71
    label "geosystem"
  ]
  node [
    id 72
    label "przestrze&#324;"
  ]
  node [
    id 73
    label "dotleni&#263;"
  ]
  node [
    id 74
    label "penetrator"
  ]
  node [
    id 75
    label "punkt_odniesienia"
  ]
  node [
    id 76
    label "kompleks_sorpcyjny"
  ]
  node [
    id 77
    label "podstawowy"
  ]
  node [
    id 78
    label "plantowa&#263;"
  ]
  node [
    id 79
    label "podk&#322;ad"
  ]
  node [
    id 80
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 81
    label "dno"
  ]
  node [
    id 82
    label "skr&#281;canie"
  ]
  node [
    id 83
    label "voice"
  ]
  node [
    id 84
    label "forma"
  ]
  node [
    id 85
    label "internet"
  ]
  node [
    id 86
    label "skr&#281;ci&#263;"
  ]
  node [
    id 87
    label "kartka"
  ]
  node [
    id 88
    label "orientowa&#263;"
  ]
  node [
    id 89
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 90
    label "plik"
  ]
  node [
    id 91
    label "bok"
  ]
  node [
    id 92
    label "pagina"
  ]
  node [
    id 93
    label "orientowanie"
  ]
  node [
    id 94
    label "fragment"
  ]
  node [
    id 95
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 96
    label "s&#261;d"
  ]
  node [
    id 97
    label "skr&#281;ca&#263;"
  ]
  node [
    id 98
    label "g&#243;ra"
  ]
  node [
    id 99
    label "serwis_internetowy"
  ]
  node [
    id 100
    label "orientacja"
  ]
  node [
    id 101
    label "linia"
  ]
  node [
    id 102
    label "skr&#281;cenie"
  ]
  node [
    id 103
    label "layout"
  ]
  node [
    id 104
    label "zorientowa&#263;"
  ]
  node [
    id 105
    label "zorientowanie"
  ]
  node [
    id 106
    label "obiekt"
  ]
  node [
    id 107
    label "podmiot"
  ]
  node [
    id 108
    label "ty&#322;"
  ]
  node [
    id 109
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 110
    label "logowanie"
  ]
  node [
    id 111
    label "adres_internetowy"
  ]
  node [
    id 112
    label "uj&#281;cie"
  ]
  node [
    id 113
    label "prz&#243;d"
  ]
  node [
    id 114
    label "posta&#263;"
  ]
  node [
    id 115
    label "uk&#322;ad"
  ]
  node [
    id 116
    label "partia"
  ]
  node [
    id 117
    label "organizacja"
  ]
  node [
    id 118
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 119
    label "Unia_Europejska"
  ]
  node [
    id 120
    label "combination"
  ]
  node [
    id 121
    label "union"
  ]
  node [
    id 122
    label "Unia"
  ]
  node [
    id 123
    label "European"
  ]
  node [
    id 124
    label "po_europejsku"
  ]
  node [
    id 125
    label "charakterystyczny"
  ]
  node [
    id 126
    label "europejsko"
  ]
  node [
    id 127
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 128
    label "typowy"
  ]
  node [
    id 129
    label "ro&#347;linny"
  ]
  node [
    id 130
    label "drewniany"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
]
