graph [
  maxDegree 72
  minDegree 1
  meanDegree 2
  density 0.013071895424836602
  graphCliqueNumber 3
  node [
    id 0
    label "skazany"
    origin "text"
  ]
  node [
    id 1
    label "dania"
    origin "text"
  ]
  node [
    id 2
    label "przest&#281;pstwo"
    origin "text"
  ]
  node [
    id 3
    label "imigrant"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "osadza&#263;"
    origin "text"
  ]
  node [
    id 6
    label "specjalny"
    origin "text"
  ]
  node [
    id 7
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 8
    label "deportacyjny"
    origin "text"
  ]
  node [
    id 9
    label "jeden"
    origin "text"
  ]
  node [
    id 10
    label "niezamieszka&#322;y"
    origin "text"
  ]
  node [
    id 11
    label "du&#324;ski"
    origin "text"
  ]
  node [
    id 12
    label "wyspa"
    origin "text"
  ]
  node [
    id 13
    label "cz&#322;owiek"
  ]
  node [
    id 14
    label "s&#261;d"
  ]
  node [
    id 15
    label "dysponowa&#263;"
  ]
  node [
    id 16
    label "dysponowanie"
  ]
  node [
    id 17
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 18
    label "brudny"
  ]
  node [
    id 19
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 20
    label "sprawstwo"
  ]
  node [
    id 21
    label "crime"
  ]
  node [
    id 22
    label "cudzoziemiec"
  ]
  node [
    id 23
    label "przybysz"
  ]
  node [
    id 24
    label "migrant"
  ]
  node [
    id 25
    label "imigracja"
  ]
  node [
    id 26
    label "ochrona_uzupe&#322;niaj&#261;ca"
  ]
  node [
    id 27
    label "si&#281;ga&#263;"
  ]
  node [
    id 28
    label "trwa&#263;"
  ]
  node [
    id 29
    label "obecno&#347;&#263;"
  ]
  node [
    id 30
    label "stan"
  ]
  node [
    id 31
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "stand"
  ]
  node [
    id 33
    label "mie&#263;_miejsce"
  ]
  node [
    id 34
    label "uczestniczy&#263;"
  ]
  node [
    id 35
    label "chodzi&#263;"
  ]
  node [
    id 36
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 37
    label "equal"
  ]
  node [
    id 38
    label "przymocowywa&#263;"
  ]
  node [
    id 39
    label "umieszcza&#263;"
  ]
  node [
    id 40
    label "zatrzymywa&#263;"
  ]
  node [
    id 41
    label "settle"
  ]
  node [
    id 42
    label "wytwarza&#263;"
  ]
  node [
    id 43
    label "specjalnie"
  ]
  node [
    id 44
    label "nieetatowy"
  ]
  node [
    id 45
    label "intencjonalny"
  ]
  node [
    id 46
    label "szczeg&#243;lny"
  ]
  node [
    id 47
    label "odpowiedni"
  ]
  node [
    id 48
    label "niedorozw&#243;j"
  ]
  node [
    id 49
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 50
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 51
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 52
    label "nienormalny"
  ]
  node [
    id 53
    label "umy&#347;lnie"
  ]
  node [
    id 54
    label "miejsce"
  ]
  node [
    id 55
    label "Hollywood"
  ]
  node [
    id 56
    label "zal&#261;&#380;ek"
  ]
  node [
    id 57
    label "otoczenie"
  ]
  node [
    id 58
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 59
    label "&#347;rodek"
  ]
  node [
    id 60
    label "center"
  ]
  node [
    id 61
    label "instytucja"
  ]
  node [
    id 62
    label "skupisko"
  ]
  node [
    id 63
    label "warunki"
  ]
  node [
    id 64
    label "kieliszek"
  ]
  node [
    id 65
    label "shot"
  ]
  node [
    id 66
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 67
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 68
    label "jaki&#347;"
  ]
  node [
    id 69
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 70
    label "jednolicie"
  ]
  node [
    id 71
    label "w&#243;dka"
  ]
  node [
    id 72
    label "ten"
  ]
  node [
    id 73
    label "ujednolicenie"
  ]
  node [
    id 74
    label "jednakowy"
  ]
  node [
    id 75
    label "odludny"
  ]
  node [
    id 76
    label "Danish"
  ]
  node [
    id 77
    label "j&#281;zyk"
  ]
  node [
    id 78
    label "po_du&#324;sku"
  ]
  node [
    id 79
    label "skandynawski"
  ]
  node [
    id 80
    label "j&#281;zyk_germa&#324;ski"
  ]
  node [
    id 81
    label "Palau"
  ]
  node [
    id 82
    label "Lesbos"
  ]
  node [
    id 83
    label "Barbados"
  ]
  node [
    id 84
    label "Wyspa_&#346;wi&#281;tej_Heleny"
  ]
  node [
    id 85
    label "obszar"
  ]
  node [
    id 86
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 87
    label "Tasmania"
  ]
  node [
    id 88
    label "Uznam"
  ]
  node [
    id 89
    label "Helgoland"
  ]
  node [
    id 90
    label "Gotlandia"
  ]
  node [
    id 91
    label "Sardynia"
  ]
  node [
    id 92
    label "Eolia"
  ]
  node [
    id 93
    label "Anguilla"
  ]
  node [
    id 94
    label "Sint_Maarten"
  ]
  node [
    id 95
    label "Tahiti"
  ]
  node [
    id 96
    label "Cebu"
  ]
  node [
    id 97
    label "Rugia"
  ]
  node [
    id 98
    label "Tajwan"
  ]
  node [
    id 99
    label "mebel"
  ]
  node [
    id 100
    label "Nowa_Zelandia"
  ]
  node [
    id 101
    label "Rodos"
  ]
  node [
    id 102
    label "kresom&#243;zgowie"
  ]
  node [
    id 103
    label "Nowa_Gwinea"
  ]
  node [
    id 104
    label "Wolin"
  ]
  node [
    id 105
    label "Sumatra"
  ]
  node [
    id 106
    label "Sycylia"
  ]
  node [
    id 107
    label "Cejlon"
  ]
  node [
    id 108
    label "Madagaskar"
  ]
  node [
    id 109
    label "Cura&#231;ao"
  ]
  node [
    id 110
    label "Man"
  ]
  node [
    id 111
    label "Kreta"
  ]
  node [
    id 112
    label "Jamajka"
  ]
  node [
    id 113
    label "Cypr"
  ]
  node [
    id 114
    label "Bornholm"
  ]
  node [
    id 115
    label "Trynidad"
  ]
  node [
    id 116
    label "Wielka_Brytania"
  ]
  node [
    id 117
    label "Kuba"
  ]
  node [
    id 118
    label "Jawa"
  ]
  node [
    id 119
    label "Sachalin"
  ]
  node [
    id 120
    label "Ibiza"
  ]
  node [
    id 121
    label "Portoryko"
  ]
  node [
    id 122
    label "Okinawa"
  ]
  node [
    id 123
    label "Bali"
  ]
  node [
    id 124
    label "Malta"
  ]
  node [
    id 125
    label "Cuszima"
  ]
  node [
    id 126
    label "Wielka_Bahama"
  ]
  node [
    id 127
    label "Tobago"
  ]
  node [
    id 128
    label "Sint_Eustatius"
  ]
  node [
    id 129
    label "Portland"
  ]
  node [
    id 130
    label "Montserrat"
  ]
  node [
    id 131
    label "Nowa_Fundlandia"
  ]
  node [
    id 132
    label "Saba"
  ]
  node [
    id 133
    label "Bonaire"
  ]
  node [
    id 134
    label "Gozo"
  ]
  node [
    id 135
    label "Eubea"
  ]
  node [
    id 136
    label "Grenlandia"
  ]
  node [
    id 137
    label "Paros"
  ]
  node [
    id 138
    label "Samotraka"
  ]
  node [
    id 139
    label "Borneo"
  ]
  node [
    id 140
    label "l&#261;d"
  ]
  node [
    id 141
    label "Majorka"
  ]
  node [
    id 142
    label "Itaka"
  ]
  node [
    id 143
    label "Irlandia"
  ]
  node [
    id 144
    label "Samos"
  ]
  node [
    id 145
    label "Salamina"
  ]
  node [
    id 146
    label "Timor"
  ]
  node [
    id 147
    label "Haiti"
  ]
  node [
    id 148
    label "Korsyka"
  ]
  node [
    id 149
    label "Zelandia"
  ]
  node [
    id 150
    label "Ostr&#243;w_Lednicki"
  ]
  node [
    id 151
    label "Ajon"
  ]
  node [
    id 152
    label "uniwersytet"
  ]
  node [
    id 153
    label "techniczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 152
    target 153
  ]
]
