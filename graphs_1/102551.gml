graph [
  maxDegree 50
  minDegree 1
  meanDegree 2.35625
  density 0.007386363636363636
  graphCliqueNumber 5
  node [
    id 0
    label "metoda"
    origin "text"
  ]
  node [
    id 1
    label "pomiarowy"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 4
    label "por&#243;wnywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wielko&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "mierzy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wzorcowy"
    origin "text"
  ]
  node [
    id 8
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 9
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 10
    label "niezale&#380;ny"
    origin "text"
  ]
  node [
    id 11
    label "rodzaj"
    origin "text"
  ]
  node [
    id 12
    label "zjawisko"
    origin "text"
  ]
  node [
    id 13
    label "fizyczny"
    origin "text"
  ]
  node [
    id 14
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "przyrz&#261;d"
    origin "text"
  ]
  node [
    id 16
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "zasada"
    origin "text"
  ]
  node [
    id 18
    label "nazwa"
    origin "text"
  ]
  node [
    id 19
    label "przedstawienie"
    origin "text"
  ]
  node [
    id 20
    label "spotyka&#263;"
    origin "text"
  ]
  node [
    id 21
    label "literatura"
    origin "text"
  ]
  node [
    id 22
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 23
    label "r&#243;&#380;ni&#263;"
    origin "text"
  ]
  node [
    id 24
    label "si&#281;"
    origin "text"
  ]
  node [
    id 25
    label "w&#322;a&#347;ciwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 26
    label "u&#322;atwia&#263;"
    origin "text"
  ]
  node [
    id 27
    label "pozna&#324;"
    origin "text"
  ]
  node [
    id 28
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 29
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 30
    label "dana"
    origin "text"
  ]
  node [
    id 31
    label "zastosowa&#263;"
    origin "text"
  ]
  node [
    id 32
    label "okre&#347;lony"
    origin "text"
  ]
  node [
    id 33
    label "&#322;atwo"
    origin "text"
  ]
  node [
    id 34
    label "zidentyfikowa&#263;"
    origin "text"
  ]
  node [
    id 35
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "opisa&#263;"
    origin "text"
  ]
  node [
    id 37
    label "dzia&#322;anie"
    origin "text"
  ]
  node [
    id 38
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 39
    label "schemat"
    origin "text"
  ]
  node [
    id 40
    label "w&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 41
    label "dla"
    origin "text"
  ]
  node [
    id 42
    label "ten"
    origin "text"
  ]
  node [
    id 43
    label "metody"
    origin "text"
  ]
  node [
    id 44
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 45
    label "method"
  ]
  node [
    id 46
    label "doktryna"
  ]
  node [
    id 47
    label "pomierny"
  ]
  node [
    id 48
    label "si&#281;ga&#263;"
  ]
  node [
    id 49
    label "trwa&#263;"
  ]
  node [
    id 50
    label "obecno&#347;&#263;"
  ]
  node [
    id 51
    label "stan"
  ]
  node [
    id 52
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 53
    label "stand"
  ]
  node [
    id 54
    label "mie&#263;_miejsce"
  ]
  node [
    id 55
    label "uczestniczy&#263;"
  ]
  node [
    id 56
    label "chodzi&#263;"
  ]
  node [
    id 57
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 58
    label "equal"
  ]
  node [
    id 59
    label "model"
  ]
  node [
    id 60
    label "zbi&#243;r"
  ]
  node [
    id 61
    label "tryb"
  ]
  node [
    id 62
    label "narz&#281;dzie"
  ]
  node [
    id 63
    label "nature"
  ]
  node [
    id 64
    label "analizowa&#263;"
  ]
  node [
    id 65
    label "szacowa&#263;"
  ]
  node [
    id 66
    label "dymensja"
  ]
  node [
    id 67
    label "znaczenie"
  ]
  node [
    id 68
    label "opinia"
  ]
  node [
    id 69
    label "warunek_lokalowy"
  ]
  node [
    id 70
    label "property"
  ]
  node [
    id 71
    label "cecha"
  ]
  node [
    id 72
    label "rzadko&#347;&#263;"
  ]
  node [
    id 73
    label "rozmiar"
  ]
  node [
    id 74
    label "zdolno&#347;&#263;"
  ]
  node [
    id 75
    label "measure"
  ]
  node [
    id 76
    label "liczba"
  ]
  node [
    id 77
    label "poj&#281;cie"
  ]
  node [
    id 78
    label "ilo&#347;&#263;"
  ]
  node [
    id 79
    label "potencja"
  ]
  node [
    id 80
    label "zaleta"
  ]
  node [
    id 81
    label "take"
  ]
  node [
    id 82
    label "okre&#347;la&#263;"
  ]
  node [
    id 83
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 84
    label "modelowy"
  ]
  node [
    id 85
    label "wzorcowo"
  ]
  node [
    id 86
    label "zdecydowanie"
  ]
  node [
    id 87
    label "follow-up"
  ]
  node [
    id 88
    label "appointment"
  ]
  node [
    id 89
    label "ustalenie"
  ]
  node [
    id 90
    label "localization"
  ]
  node [
    id 91
    label "denomination"
  ]
  node [
    id 92
    label "wyra&#380;enie"
  ]
  node [
    id 93
    label "ozdobnik"
  ]
  node [
    id 94
    label "przewidzenie"
  ]
  node [
    id 95
    label "term"
  ]
  node [
    id 96
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 97
    label "rise"
  ]
  node [
    id 98
    label "appear"
  ]
  node [
    id 99
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 100
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 101
    label "usamodzielnienie"
  ]
  node [
    id 102
    label "niezale&#380;nie"
  ]
  node [
    id 103
    label "usamodzielnianie"
  ]
  node [
    id 104
    label "kategoria_gramatyczna"
  ]
  node [
    id 105
    label "autorament"
  ]
  node [
    id 106
    label "jednostka_systematyczna"
  ]
  node [
    id 107
    label "fashion"
  ]
  node [
    id 108
    label "rodzina"
  ]
  node [
    id 109
    label "variety"
  ]
  node [
    id 110
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 111
    label "krajobraz"
  ]
  node [
    id 112
    label "przywidzenie"
  ]
  node [
    id 113
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 114
    label "boski"
  ]
  node [
    id 115
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 116
    label "proces"
  ]
  node [
    id 117
    label "presence"
  ]
  node [
    id 118
    label "charakter"
  ]
  node [
    id 119
    label "fizykalnie"
  ]
  node [
    id 120
    label "fizycznie"
  ]
  node [
    id 121
    label "materializowanie"
  ]
  node [
    id 122
    label "pracownik"
  ]
  node [
    id 123
    label "gimnastyczny"
  ]
  node [
    id 124
    label "widoczny"
  ]
  node [
    id 125
    label "namacalny"
  ]
  node [
    id 126
    label "zmaterializowanie"
  ]
  node [
    id 127
    label "organiczny"
  ]
  node [
    id 128
    label "materjalny"
  ]
  node [
    id 129
    label "use"
  ]
  node [
    id 130
    label "krzywdzi&#263;"
  ]
  node [
    id 131
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 132
    label "distribute"
  ]
  node [
    id 133
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 134
    label "give"
  ]
  node [
    id 135
    label "liga&#263;"
  ]
  node [
    id 136
    label "korzysta&#263;"
  ]
  node [
    id 137
    label "u&#380;ywa&#263;"
  ]
  node [
    id 138
    label "utensylia"
  ]
  node [
    id 139
    label "zapoznawa&#263;"
  ]
  node [
    id 140
    label "represent"
  ]
  node [
    id 141
    label "obserwacja"
  ]
  node [
    id 142
    label "moralno&#347;&#263;"
  ]
  node [
    id 143
    label "podstawa"
  ]
  node [
    id 144
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 145
    label "umowa"
  ]
  node [
    id 146
    label "dominion"
  ]
  node [
    id 147
    label "qualification"
  ]
  node [
    id 148
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 149
    label "opis"
  ]
  node [
    id 150
    label "regu&#322;a_Allena"
  ]
  node [
    id 151
    label "normalizacja"
  ]
  node [
    id 152
    label "regu&#322;a_Glogera"
  ]
  node [
    id 153
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 154
    label "standard"
  ]
  node [
    id 155
    label "base"
  ]
  node [
    id 156
    label "substancja"
  ]
  node [
    id 157
    label "prawid&#322;o"
  ]
  node [
    id 158
    label "prawo_Mendla"
  ]
  node [
    id 159
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 160
    label "criterion"
  ]
  node [
    id 161
    label "twierdzenie"
  ]
  node [
    id 162
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 163
    label "prawo"
  ]
  node [
    id 164
    label "occupation"
  ]
  node [
    id 165
    label "zasada_d'Alemberta"
  ]
  node [
    id 166
    label "wezwanie"
  ]
  node [
    id 167
    label "leksem"
  ]
  node [
    id 168
    label "patron"
  ]
  node [
    id 169
    label "report"
  ]
  node [
    id 170
    label "theatrical_performance"
  ]
  node [
    id 171
    label "podanie"
  ]
  node [
    id 172
    label "ods&#322;ona"
  ]
  node [
    id 173
    label "malarstwo"
  ]
  node [
    id 174
    label "narration"
  ]
  node [
    id 175
    label "rola"
  ]
  node [
    id 176
    label "exhibit"
  ]
  node [
    id 177
    label "pokazanie"
  ]
  node [
    id 178
    label "realizacja"
  ]
  node [
    id 179
    label "wytw&#243;r"
  ]
  node [
    id 180
    label "scenografia"
  ]
  node [
    id 181
    label "zapoznanie"
  ]
  node [
    id 182
    label "obgadanie"
  ]
  node [
    id 183
    label "pokaz"
  ]
  node [
    id 184
    label "przedstawia&#263;"
  ]
  node [
    id 185
    label "pr&#243;bowanie"
  ]
  node [
    id 186
    label "teatr"
  ]
  node [
    id 187
    label "scena"
  ]
  node [
    id 188
    label "przedstawianie"
  ]
  node [
    id 189
    label "wyst&#261;pienie"
  ]
  node [
    id 190
    label "zademonstrowanie"
  ]
  node [
    id 191
    label "cyrk"
  ]
  node [
    id 192
    label "opisanie"
  ]
  node [
    id 193
    label "ukazanie"
  ]
  node [
    id 194
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 195
    label "przedstawi&#263;"
  ]
  node [
    id 196
    label "posta&#263;"
  ]
  node [
    id 197
    label "styka&#263;_si&#281;"
  ]
  node [
    id 198
    label "happen"
  ]
  node [
    id 199
    label "strike"
  ]
  node [
    id 200
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 201
    label "poznawa&#263;"
  ]
  node [
    id 202
    label "fall"
  ]
  node [
    id 203
    label "znajdowa&#263;"
  ]
  node [
    id 204
    label "dokument"
  ]
  node [
    id 205
    label "amorfizm"
  ]
  node [
    id 206
    label "liryka"
  ]
  node [
    id 207
    label "bibliografia"
  ]
  node [
    id 208
    label "translator"
  ]
  node [
    id 209
    label "dramat"
  ]
  node [
    id 210
    label "pi&#347;miennictwo"
  ]
  node [
    id 211
    label "zoologia_fantastyczna"
  ]
  node [
    id 212
    label "pisarstwo"
  ]
  node [
    id 213
    label "epika"
  ]
  node [
    id 214
    label "sztuka"
  ]
  node [
    id 215
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 216
    label "cz&#281;sty"
  ]
  node [
    id 217
    label "sprawowa&#263;"
  ]
  node [
    id 218
    label "wyregulowanie"
  ]
  node [
    id 219
    label "warto&#347;&#263;"
  ]
  node [
    id 220
    label "charakterystyka"
  ]
  node [
    id 221
    label "kompetencja"
  ]
  node [
    id 222
    label "regulowanie"
  ]
  node [
    id 223
    label "feature"
  ]
  node [
    id 224
    label "wyregulowa&#263;"
  ]
  node [
    id 225
    label "regulowa&#263;"
  ]
  node [
    id 226
    label "attribute"
  ]
  node [
    id 227
    label "ease"
  ]
  node [
    id 228
    label "robi&#263;"
  ]
  node [
    id 229
    label "powodowa&#263;"
  ]
  node [
    id 230
    label "&#322;atwi&#263;"
  ]
  node [
    id 231
    label "cognizance"
  ]
  node [
    id 232
    label "dar"
  ]
  node [
    id 233
    label "cnota"
  ]
  node [
    id 234
    label "buddyzm"
  ]
  node [
    id 235
    label "u&#380;y&#263;"
  ]
  node [
    id 236
    label "wiadomy"
  ]
  node [
    id 237
    label "&#322;atwy"
  ]
  node [
    id 238
    label "szybko"
  ]
  node [
    id 239
    label "&#322;atwie"
  ]
  node [
    id 240
    label "prosto"
  ]
  node [
    id 241
    label "snadnie"
  ]
  node [
    id 242
    label "przyjemnie"
  ]
  node [
    id 243
    label "&#322;acno"
  ]
  node [
    id 244
    label "rozpozna&#263;"
  ]
  node [
    id 245
    label "tag"
  ]
  node [
    id 246
    label "ujednolici&#263;"
  ]
  node [
    id 247
    label "whole"
  ]
  node [
    id 248
    label "Rzym_Zachodni"
  ]
  node [
    id 249
    label "element"
  ]
  node [
    id 250
    label "urz&#261;dzenie"
  ]
  node [
    id 251
    label "Rzym_Wschodni"
  ]
  node [
    id 252
    label "zinterpretowa&#263;"
  ]
  node [
    id 253
    label "relate"
  ]
  node [
    id 254
    label "zapozna&#263;"
  ]
  node [
    id 255
    label "delineate"
  ]
  node [
    id 256
    label "nakr&#281;cenie"
  ]
  node [
    id 257
    label "cz&#322;owiek"
  ]
  node [
    id 258
    label "robienie"
  ]
  node [
    id 259
    label "infimum"
  ]
  node [
    id 260
    label "hipnotyzowanie"
  ]
  node [
    id 261
    label "bycie"
  ]
  node [
    id 262
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 263
    label "jednostka"
  ]
  node [
    id 264
    label "uruchamianie"
  ]
  node [
    id 265
    label "kampania"
  ]
  node [
    id 266
    label "w&#322;&#261;czanie"
  ]
  node [
    id 267
    label "operacja"
  ]
  node [
    id 268
    label "operation"
  ]
  node [
    id 269
    label "supremum"
  ]
  node [
    id 270
    label "kres"
  ]
  node [
    id 271
    label "zako&#324;czenie"
  ]
  node [
    id 272
    label "funkcja"
  ]
  node [
    id 273
    label "skutek"
  ]
  node [
    id 274
    label "dzianie_si&#281;"
  ]
  node [
    id 275
    label "liczy&#263;"
  ]
  node [
    id 276
    label "zadzia&#322;anie"
  ]
  node [
    id 277
    label "podzia&#322;anie"
  ]
  node [
    id 278
    label "rzut"
  ]
  node [
    id 279
    label "czynny"
  ]
  node [
    id 280
    label "liczenie"
  ]
  node [
    id 281
    label "czynno&#347;&#263;"
  ]
  node [
    id 282
    label "wdzieranie_si&#281;"
  ]
  node [
    id 283
    label "rozpocz&#281;cie"
  ]
  node [
    id 284
    label "docieranie"
  ]
  node [
    id 285
    label "wp&#322;yw"
  ]
  node [
    id 286
    label "podtrzymywanie"
  ]
  node [
    id 287
    label "nakr&#281;canie"
  ]
  node [
    id 288
    label "uruchomienie"
  ]
  node [
    id 289
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 290
    label "impact"
  ]
  node [
    id 291
    label "tr&#243;jstronny"
  ]
  node [
    id 292
    label "matematyka"
  ]
  node [
    id 293
    label "reakcja_chemiczna"
  ]
  node [
    id 294
    label "act"
  ]
  node [
    id 295
    label "priorytet"
  ]
  node [
    id 296
    label "w&#322;&#261;czenie"
  ]
  node [
    id 297
    label "natural_process"
  ]
  node [
    id 298
    label "zatrzymanie"
  ]
  node [
    id 299
    label "rezultat"
  ]
  node [
    id 300
    label "powodowanie"
  ]
  node [
    id 301
    label "oferta"
  ]
  node [
    id 302
    label "drabina_analgetyczna"
  ]
  node [
    id 303
    label "radiation_pattern"
  ]
  node [
    id 304
    label "exemplar"
  ]
  node [
    id 305
    label "pomys&#322;"
  ]
  node [
    id 306
    label "mildew"
  ]
  node [
    id 307
    label "rysunek"
  ]
  node [
    id 308
    label "taki"
  ]
  node [
    id 309
    label "nale&#380;yty"
  ]
  node [
    id 310
    label "charakterystyczny"
  ]
  node [
    id 311
    label "stosownie"
  ]
  node [
    id 312
    label "dobry"
  ]
  node [
    id 313
    label "prawdziwy"
  ]
  node [
    id 314
    label "uprawniony"
  ]
  node [
    id 315
    label "zasadniczy"
  ]
  node [
    id 316
    label "typowy"
  ]
  node [
    id 317
    label "nale&#380;ny"
  ]
  node [
    id 318
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 319
    label "nast&#281;puj&#261;co"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 27
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 30
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 35
  ]
  edge [
    source 15
    target 36
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 62
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 40
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 18
    target 95
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 83
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 54
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 21
    target 206
  ]
  edge [
    source 21
    target 207
  ]
  edge [
    source 21
    target 208
  ]
  edge [
    source 21
    target 209
  ]
  edge [
    source 21
    target 210
  ]
  edge [
    source 21
    target 211
  ]
  edge [
    source 21
    target 212
  ]
  edge [
    source 21
    target 213
  ]
  edge [
    source 21
    target 214
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 71
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 154
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 80
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 227
  ]
  edge [
    source 26
    target 228
  ]
  edge [
    source 26
    target 229
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 231
  ]
  edge [
    source 30
    target 232
  ]
  edge [
    source 30
    target 233
  ]
  edge [
    source 30
    target 234
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 235
  ]
  edge [
    source 32
    target 236
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 33
    target 237
  ]
  edge [
    source 33
    target 238
  ]
  edge [
    source 33
    target 239
  ]
  edge [
    source 33
    target 240
  ]
  edge [
    source 33
    target 241
  ]
  edge [
    source 33
    target 242
  ]
  edge [
    source 33
    target 243
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 244
  ]
  edge [
    source 34
    target 245
  ]
  edge [
    source 34
    target 246
  ]
  edge [
    source 35
    target 247
  ]
  edge [
    source 35
    target 248
  ]
  edge [
    source 35
    target 249
  ]
  edge [
    source 35
    target 78
  ]
  edge [
    source 35
    target 250
  ]
  edge [
    source 35
    target 251
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 252
  ]
  edge [
    source 36
    target 253
  ]
  edge [
    source 36
    target 254
  ]
  edge [
    source 36
    target 255
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 256
  ]
  edge [
    source 37
    target 257
  ]
  edge [
    source 37
    target 258
  ]
  edge [
    source 37
    target 259
  ]
  edge [
    source 37
    target 260
  ]
  edge [
    source 37
    target 261
  ]
  edge [
    source 37
    target 262
  ]
  edge [
    source 37
    target 263
  ]
  edge [
    source 37
    target 264
  ]
  edge [
    source 37
    target 265
  ]
  edge [
    source 37
    target 266
  ]
  edge [
    source 37
    target 267
  ]
  edge [
    source 37
    target 268
  ]
  edge [
    source 37
    target 269
  ]
  edge [
    source 37
    target 270
  ]
  edge [
    source 37
    target 271
  ]
  edge [
    source 37
    target 272
  ]
  edge [
    source 37
    target 273
  ]
  edge [
    source 37
    target 274
  ]
  edge [
    source 37
    target 275
  ]
  edge [
    source 37
    target 276
  ]
  edge [
    source 37
    target 277
  ]
  edge [
    source 37
    target 278
  ]
  edge [
    source 37
    target 279
  ]
  edge [
    source 37
    target 280
  ]
  edge [
    source 37
    target 281
  ]
  edge [
    source 37
    target 282
  ]
  edge [
    source 37
    target 283
  ]
  edge [
    source 37
    target 284
  ]
  edge [
    source 37
    target 285
  ]
  edge [
    source 37
    target 286
  ]
  edge [
    source 37
    target 287
  ]
  edge [
    source 37
    target 288
  ]
  edge [
    source 37
    target 289
  ]
  edge [
    source 37
    target 290
  ]
  edge [
    source 37
    target 291
  ]
  edge [
    source 37
    target 292
  ]
  edge [
    source 37
    target 293
  ]
  edge [
    source 37
    target 294
  ]
  edge [
    source 37
    target 295
  ]
  edge [
    source 37
    target 296
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 37
    target 298
  ]
  edge [
    source 37
    target 299
  ]
  edge [
    source 37
    target 300
  ]
  edge [
    source 37
    target 301
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 302
  ]
  edge [
    source 39
    target 59
  ]
  edge [
    source 39
    target 303
  ]
  edge [
    source 39
    target 304
  ]
  edge [
    source 39
    target 305
  ]
  edge [
    source 39
    target 306
  ]
  edge [
    source 39
    target 307
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 308
  ]
  edge [
    source 40
    target 309
  ]
  edge [
    source 40
    target 310
  ]
  edge [
    source 40
    target 311
  ]
  edge [
    source 40
    target 312
  ]
  edge [
    source 40
    target 313
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 314
  ]
  edge [
    source 40
    target 315
  ]
  edge [
    source 40
    target 316
  ]
  edge [
    source 40
    target 317
  ]
  edge [
    source 40
    target 318
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 319
  ]
]
