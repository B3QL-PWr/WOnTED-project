graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ponownie"
    origin "text"
  ]
  node [
    id 2
    label "pozdrawia&#263;"
  ]
  node [
    id 3
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 4
    label "greet"
  ]
  node [
    id 5
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 6
    label "welcome"
  ]
  node [
    id 7
    label "znowu"
  ]
  node [
    id 8
    label "ponowny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
]
