graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.0714285714285716
  density 0.07671957671957672
  graphCliqueNumber 4
  node [
    id 0
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "rozdajo"
    origin "text"
  ]
  node [
    id 2
    label "rozdajosteam"
    origin "text"
  ]
  node [
    id 3
    label "tentegowa&#263;"
  ]
  node [
    id 4
    label "urz&#261;dza&#263;"
  ]
  node [
    id 5
    label "give"
  ]
  node [
    id 6
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 7
    label "czyni&#263;"
  ]
  node [
    id 8
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 9
    label "post&#281;powa&#263;"
  ]
  node [
    id 10
    label "wydala&#263;"
  ]
  node [
    id 11
    label "oszukiwa&#263;"
  ]
  node [
    id 12
    label "organizowa&#263;"
  ]
  node [
    id 13
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 14
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 15
    label "work"
  ]
  node [
    id 16
    label "przerabia&#263;"
  ]
  node [
    id 17
    label "stylizowa&#263;"
  ]
  node [
    id 18
    label "falowa&#263;"
  ]
  node [
    id 19
    label "act"
  ]
  node [
    id 20
    label "peddle"
  ]
  node [
    id 21
    label "ukazywa&#263;"
  ]
  node [
    id 22
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 23
    label "praca"
  ]
  node [
    id 24
    label "CITIES"
  ]
  node [
    id 25
    label "SKYLINES"
  ]
  node [
    id 26
    label "AFTER"
  ]
  node [
    id 27
    label "DARK"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
]
