graph [
  maxDegree 48
  minDegree 1
  meanDegree 2.3412844036697247
  density 0.004303831624392876
  graphCliqueNumber 4
  node [
    id 0
    label "krajowy"
    origin "text"
  ]
  node [
    id 1
    label "rada"
    origin "text"
  ]
  node [
    id 2
    label "radiofonia"
    origin "text"
  ]
  node [
    id 3
    label "telewizja"
    origin "text"
  ]
  node [
    id 4
    label "wybra&#263;"
    origin "text"
  ]
  node [
    id 5
    label "&#347;roda"
    origin "text"
  ]
  node [
    id 6
    label "nadzorczy"
    origin "text"
  ]
  node [
    id 7
    label "polski"
    origin "text"
  ]
  node [
    id 8
    label "radio"
    origin "text"
  ]
  node [
    id 9
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 11
    label "pis"
    origin "text"
  ]
  node [
    id 12
    label "ale"
    origin "text"
  ]
  node [
    id 13
    label "musie&#263;"
    origin "text"
  ]
  node [
    id 14
    label "miejsce"
    origin "text"
  ]
  node [
    id 15
    label "podzieli&#263;"
    origin "text"
  ]
  node [
    id 16
    label "si&#281;"
    origin "text"
  ]
  node [
    id 17
    label "lpr"
    origin "text"
  ]
  node [
    id 18
    label "samoobrona"
    origin "text"
  ]
  node [
    id 19
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "gazeta"
    origin "text"
  ]
  node [
    id 21
    label "wyborczy"
    origin "text"
  ]
  node [
    id 22
    label "jak"
    origin "text"
  ]
  node [
    id 23
    label "czyta&#263;"
    origin "text"
  ]
  node [
    id 24
    label "dziewi&#281;cioosobowej"
    origin "text"
  ]
  node [
    id 25
    label "rad"
    origin "text"
  ]
  node [
    id 26
    label "by&#263;"
    origin "text"
  ]
  node [
    id 27
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 28
    label "pi&#261;&#263;"
    origin "text"
  ]
  node [
    id 29
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 30
    label "dwa"
    origin "text"
  ]
  node [
    id 31
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 32
    label "lech"
    origin "text"
  ]
  node [
    id 33
    label "jaworski"
    origin "text"
  ]
  node [
    id 34
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 35
    label "cz&#322;onek"
    origin "text"
  ]
  node [
    id 36
    label "krrit"
    origin "text"
  ]
  node [
    id 37
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 38
    label "prezes"
    origin "text"
  ]
  node [
    id 39
    label "fundacja"
    origin "text"
  ]
  node [
    id 40
    label "media"
    origin "text"
  ]
  node [
    id 41
    label "pro"
    origin "text"
  ]
  node [
    id 42
    label "bona"
    origin "text"
  ]
  node [
    id 43
    label "ten"
    origin "text"
  ]
  node [
    id 44
    label "por"
    origin "text"
  ]
  node [
    id 45
    label "zawsze"
    origin "text"
  ]
  node [
    id 46
    label "niezale&#380;nie"
    origin "text"
  ]
  node [
    id 47
    label "kto"
    origin "text"
  ]
  node [
    id 48
    label "rz&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 49
    label "wybiera&#263;"
    origin "text"
  ]
  node [
    id 50
    label "opozycja"
    origin "text"
  ]
  node [
    id 51
    label "publiczny"
    origin "text"
  ]
  node [
    id 52
    label "przyznawa&#263;"
    origin "text"
  ]
  node [
    id 53
    label "mniejszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 54
    label "mogel"
    origin "text"
  ]
  node [
    id 55
    label "patrze&#263;"
    origin "text"
  ]
  node [
    id 56
    label "rz&#261;dz&#261;ca"
    origin "text"
  ]
  node [
    id 57
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 58
    label "tym"
    origin "text"
  ]
  node [
    id 59
    label "razem"
    origin "text"
  ]
  node [
    id 60
    label "nikt"
    origin "text"
  ]
  node [
    id 61
    label "jaros&#322;aw"
    origin "text"
  ]
  node [
    id 62
    label "sellin"
    origin "text"
  ]
  node [
    id 63
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 64
    label "kkrit"
    origin "text"
  ]
  node [
    id 65
    label "obecnie"
    origin "text"
  ]
  node [
    id 66
    label "wiceminister"
    origin "text"
  ]
  node [
    id 67
    label "kultura"
    origin "text"
  ]
  node [
    id 68
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 69
    label "wczesno"
    origin "text"
  ]
  node [
    id 70
    label "zapewnia&#263;"
    origin "text"
  ]
  node [
    id 71
    label "prawo"
    origin "text"
  ]
  node [
    id 72
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 73
    label "zmieni&#263;"
    origin "text"
  ]
  node [
    id 74
    label "s&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 75
    label "dzielenie"
    origin "text"
  ]
  node [
    id 76
    label "parytet"
    origin "text"
  ]
  node [
    id 77
    label "partyjny"
    origin "text"
  ]
  node [
    id 78
    label "sfera"
    origin "text"
  ]
  node [
    id 79
    label "wp&#322;yw"
    origin "text"
  ]
  node [
    id 80
    label "nowa"
    origin "text"
  ]
  node [
    id 81
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 82
    label "rozmawia&#263;"
    origin "text"
  ]
  node [
    id 83
    label "rodzimy"
  ]
  node [
    id 84
    label "dyskusja"
  ]
  node [
    id 85
    label "grupa"
  ]
  node [
    id 86
    label "Rada_Unii_Europejskiej"
  ]
  node [
    id 87
    label "conference"
  ]
  node [
    id 88
    label "organ"
  ]
  node [
    id 89
    label "zgromadzenie"
  ]
  node [
    id 90
    label "wskaz&#243;wka"
  ]
  node [
    id 91
    label "konsylium"
  ]
  node [
    id 92
    label "Rada_Europy"
  ]
  node [
    id 93
    label "Rada_Europejska"
  ]
  node [
    id 94
    label "posiedzenie"
  ]
  node [
    id 95
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 96
    label "radiofonizacja"
  ]
  node [
    id 97
    label "infrastruktura"
  ]
  node [
    id 98
    label "radiokomunikacja"
  ]
  node [
    id 99
    label "Polsat"
  ]
  node [
    id 100
    label "paj&#281;czarz"
  ]
  node [
    id 101
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 102
    label "programowiec"
  ]
  node [
    id 103
    label "technologia"
  ]
  node [
    id 104
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 105
    label "Interwizja"
  ]
  node [
    id 106
    label "BBC"
  ]
  node [
    id 107
    label "ekran"
  ]
  node [
    id 108
    label "redakcja"
  ]
  node [
    id 109
    label "odbieranie"
  ]
  node [
    id 110
    label "odbiera&#263;"
  ]
  node [
    id 111
    label "odbiornik"
  ]
  node [
    id 112
    label "instytucja"
  ]
  node [
    id 113
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 114
    label "studio"
  ]
  node [
    id 115
    label "telekomunikacja"
  ]
  node [
    id 116
    label "muza"
  ]
  node [
    id 117
    label "zu&#380;y&#263;"
  ]
  node [
    id 118
    label "distill"
  ]
  node [
    id 119
    label "wyj&#261;&#263;"
  ]
  node [
    id 120
    label "sie&#263;_rybacka"
  ]
  node [
    id 121
    label "powo&#322;a&#263;"
  ]
  node [
    id 122
    label "kotwica"
  ]
  node [
    id 123
    label "ustali&#263;"
  ]
  node [
    id 124
    label "pick"
  ]
  node [
    id 125
    label "Popielec"
  ]
  node [
    id 126
    label "dzie&#324;_powszedni"
  ]
  node [
    id 127
    label "tydzie&#324;"
  ]
  node [
    id 128
    label "kontrolny"
  ]
  node [
    id 129
    label "lacki"
  ]
  node [
    id 130
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 131
    label "przedmiot"
  ]
  node [
    id 132
    label "sztajer"
  ]
  node [
    id 133
    label "drabant"
  ]
  node [
    id 134
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 135
    label "polak"
  ]
  node [
    id 136
    label "pierogi_ruskie"
  ]
  node [
    id 137
    label "krakowiak"
  ]
  node [
    id 138
    label "Polish"
  ]
  node [
    id 139
    label "j&#281;zyk"
  ]
  node [
    id 140
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 141
    label "oberek"
  ]
  node [
    id 142
    label "po_polsku"
  ]
  node [
    id 143
    label "mazur"
  ]
  node [
    id 144
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 145
    label "chodzony"
  ]
  node [
    id 146
    label "skoczny"
  ]
  node [
    id 147
    label "ryba_po_grecku"
  ]
  node [
    id 148
    label "goniony"
  ]
  node [
    id 149
    label "polsko"
  ]
  node [
    id 150
    label "uk&#322;ad"
  ]
  node [
    id 151
    label "fala_radiowa"
  ]
  node [
    id 152
    label "spot"
  ]
  node [
    id 153
    label "eliminator"
  ]
  node [
    id 154
    label "radiola"
  ]
  node [
    id 155
    label "dyskryminator"
  ]
  node [
    id 156
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 157
    label "stacja"
  ]
  node [
    id 158
    label "radiolinia"
  ]
  node [
    id 159
    label "majority"
  ]
  node [
    id 160
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 161
    label "czyj&#347;"
  ]
  node [
    id 162
    label "m&#261;&#380;"
  ]
  node [
    id 163
    label "piwo"
  ]
  node [
    id 164
    label "need"
  ]
  node [
    id 165
    label "pragn&#261;&#263;"
  ]
  node [
    id 166
    label "cia&#322;o"
  ]
  node [
    id 167
    label "plac"
  ]
  node [
    id 168
    label "cecha"
  ]
  node [
    id 169
    label "uwaga"
  ]
  node [
    id 170
    label "przestrze&#324;"
  ]
  node [
    id 171
    label "status"
  ]
  node [
    id 172
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 173
    label "chwila"
  ]
  node [
    id 174
    label "rz&#261;d"
  ]
  node [
    id 175
    label "praca"
  ]
  node [
    id 176
    label "location"
  ]
  node [
    id 177
    label "warunek_lokalowy"
  ]
  node [
    id 178
    label "transgress"
  ]
  node [
    id 179
    label "impart"
  ]
  node [
    id 180
    label "spowodowa&#263;"
  ]
  node [
    id 181
    label "distribute"
  ]
  node [
    id 182
    label "wydzieli&#263;"
  ]
  node [
    id 183
    label "divide"
  ]
  node [
    id 184
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 185
    label "przyzna&#263;"
  ]
  node [
    id 186
    label "policzy&#263;"
  ]
  node [
    id 187
    label "pigeonhole"
  ]
  node [
    id 188
    label "exchange"
  ]
  node [
    id 189
    label "rozda&#263;"
  ]
  node [
    id 190
    label "sk&#322;&#243;ci&#263;"
  ]
  node [
    id 191
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 192
    label "zrobi&#263;"
  ]
  node [
    id 193
    label "change"
  ]
  node [
    id 194
    label "self-defense"
  ]
  node [
    id 195
    label "czynno&#347;&#263;"
  ]
  node [
    id 196
    label "obrona"
  ]
  node [
    id 197
    label "komunikowa&#263;"
  ]
  node [
    id 198
    label "powiada&#263;"
  ]
  node [
    id 199
    label "inform"
  ]
  node [
    id 200
    label "prasa"
  ]
  node [
    id 201
    label "tytu&#322;"
  ]
  node [
    id 202
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 203
    label "czasopismo"
  ]
  node [
    id 204
    label "byd&#322;o"
  ]
  node [
    id 205
    label "zobo"
  ]
  node [
    id 206
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 207
    label "yakalo"
  ]
  node [
    id 208
    label "dzo"
  ]
  node [
    id 209
    label "dysleksja"
  ]
  node [
    id 210
    label "umie&#263;"
  ]
  node [
    id 211
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 212
    label "przetwarza&#263;"
  ]
  node [
    id 213
    label "read"
  ]
  node [
    id 214
    label "poznawa&#263;"
  ]
  node [
    id 215
    label "obserwowa&#263;"
  ]
  node [
    id 216
    label "odczytywa&#263;"
  ]
  node [
    id 217
    label "berylowiec"
  ]
  node [
    id 218
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 219
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 220
    label "mikroradian"
  ]
  node [
    id 221
    label "zadowolenie_si&#281;"
  ]
  node [
    id 222
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 223
    label "content"
  ]
  node [
    id 224
    label "jednostka_promieniowania"
  ]
  node [
    id 225
    label "miliradian"
  ]
  node [
    id 226
    label "jednostka"
  ]
  node [
    id 227
    label "si&#281;ga&#263;"
  ]
  node [
    id 228
    label "trwa&#263;"
  ]
  node [
    id 229
    label "obecno&#347;&#263;"
  ]
  node [
    id 230
    label "stan"
  ]
  node [
    id 231
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 232
    label "stand"
  ]
  node [
    id 233
    label "mie&#263;_miejsce"
  ]
  node [
    id 234
    label "uczestniczy&#263;"
  ]
  node [
    id 235
    label "chodzi&#263;"
  ]
  node [
    id 236
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 237
    label "equal"
  ]
  node [
    id 238
    label "czu&#263;"
  ]
  node [
    id 239
    label "hide"
  ]
  node [
    id 240
    label "support"
  ]
  node [
    id 241
    label "cz&#322;owiek"
  ]
  node [
    id 242
    label "substytuowanie"
  ]
  node [
    id 243
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 244
    label "przyk&#322;ad"
  ]
  node [
    id 245
    label "zast&#281;pca"
  ]
  node [
    id 246
    label "substytuowa&#263;"
  ]
  node [
    id 247
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 248
    label "express"
  ]
  node [
    id 249
    label "rzekn&#261;&#263;"
  ]
  node [
    id 250
    label "okre&#347;li&#263;"
  ]
  node [
    id 251
    label "wyrazi&#263;"
  ]
  node [
    id 252
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 253
    label "unwrap"
  ]
  node [
    id 254
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 255
    label "convey"
  ]
  node [
    id 256
    label "discover"
  ]
  node [
    id 257
    label "wydoby&#263;"
  ]
  node [
    id 258
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 259
    label "poda&#263;"
  ]
  node [
    id 260
    label "dawny"
  ]
  node [
    id 261
    label "rozw&#243;d"
  ]
  node [
    id 262
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 263
    label "eksprezydent"
  ]
  node [
    id 264
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 265
    label "partner"
  ]
  node [
    id 266
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 267
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 268
    label "wcze&#347;niejszy"
  ]
  node [
    id 269
    label "organizacja"
  ]
  node [
    id 270
    label "shaft"
  ]
  node [
    id 271
    label "podmiot"
  ]
  node [
    id 272
    label "fiut"
  ]
  node [
    id 273
    label "przyrodzenie"
  ]
  node [
    id 274
    label "wchodzenie"
  ]
  node [
    id 275
    label "ptaszek"
  ]
  node [
    id 276
    label "wej&#347;cie"
  ]
  node [
    id 277
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 278
    label "element_anatomiczny"
  ]
  node [
    id 279
    label "doba"
  ]
  node [
    id 280
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 281
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 282
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 283
    label "gruba_ryba"
  ]
  node [
    id 284
    label "zwierzchnik"
  ]
  node [
    id 285
    label "foundation"
  ]
  node [
    id 286
    label "dar"
  ]
  node [
    id 287
    label "darowizna"
  ]
  node [
    id 288
    label "pocz&#261;tek"
  ]
  node [
    id 289
    label "przekazior"
  ]
  node [
    id 290
    label "mass-media"
  ]
  node [
    id 291
    label "uzbrajanie"
  ]
  node [
    id 292
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 293
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 294
    label "medium"
  ]
  node [
    id 295
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 296
    label "niania"
  ]
  node [
    id 297
    label "okre&#347;lony"
  ]
  node [
    id 298
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 299
    label "otw&#243;r"
  ]
  node [
    id 300
    label "w&#322;oszczyzna"
  ]
  node [
    id 301
    label "warzywo"
  ]
  node [
    id 302
    label "czosnek"
  ]
  node [
    id 303
    label "kapelusz"
  ]
  node [
    id 304
    label "uj&#347;cie"
  ]
  node [
    id 305
    label "zaw&#380;dy"
  ]
  node [
    id 306
    label "ci&#261;gle"
  ]
  node [
    id 307
    label "na_zawsze"
  ]
  node [
    id 308
    label "cz&#281;sto"
  ]
  node [
    id 309
    label "niezale&#380;ny"
  ]
  node [
    id 310
    label "dokazywa&#263;"
  ]
  node [
    id 311
    label "control"
  ]
  node [
    id 312
    label "dzier&#380;e&#263;"
  ]
  node [
    id 313
    label "sprawowa&#263;"
  ]
  node [
    id 314
    label "g&#243;rowa&#263;"
  ]
  node [
    id 315
    label "w&#322;adza"
  ]
  node [
    id 316
    label "manipulate"
  ]
  node [
    id 317
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 318
    label "warunkowa&#263;"
  ]
  node [
    id 319
    label "wyjmowa&#263;"
  ]
  node [
    id 320
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 321
    label "powo&#322;ywa&#263;"
  ]
  node [
    id 322
    label "take"
  ]
  node [
    id 323
    label "ustala&#263;"
  ]
  node [
    id 324
    label "reakcja"
  ]
  node [
    id 325
    label "protestacja"
  ]
  node [
    id 326
    label "partia"
  ]
  node [
    id 327
    label "opposition"
  ]
  node [
    id 328
    label "relacja"
  ]
  node [
    id 329
    label "odwrotno&#347;&#263;"
  ]
  node [
    id 330
    label "zjawisko"
  ]
  node [
    id 331
    label "ustawienie"
  ]
  node [
    id 332
    label "czerwona_kartka"
  ]
  node [
    id 333
    label "jawny"
  ]
  node [
    id 334
    label "upublicznienie"
  ]
  node [
    id 335
    label "upublicznianie"
  ]
  node [
    id 336
    label "publicznie"
  ]
  node [
    id 337
    label "dawa&#263;"
  ]
  node [
    id 338
    label "confer"
  ]
  node [
    id 339
    label "s&#261;dzi&#263;"
  ]
  node [
    id 340
    label "stwierdza&#263;"
  ]
  node [
    id 341
    label "nadawa&#263;"
  ]
  node [
    id 342
    label "minority"
  ]
  node [
    id 343
    label "grupa_etniczna"
  ]
  node [
    id 344
    label "multikulturalizm"
  ]
  node [
    id 345
    label "koso"
  ]
  node [
    id 346
    label "szuka&#263;"
  ]
  node [
    id 347
    label "go_steady"
  ]
  node [
    id 348
    label "dba&#263;"
  ]
  node [
    id 349
    label "traktowa&#263;"
  ]
  node [
    id 350
    label "os&#261;dza&#263;"
  ]
  node [
    id 351
    label "punkt_widzenia"
  ]
  node [
    id 352
    label "robi&#263;"
  ]
  node [
    id 353
    label "uwa&#380;a&#263;"
  ]
  node [
    id 354
    label "look"
  ]
  node [
    id 355
    label "pogl&#261;da&#263;"
  ]
  node [
    id 356
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 357
    label "krzy&#380;"
  ]
  node [
    id 358
    label "paw"
  ]
  node [
    id 359
    label "rami&#281;"
  ]
  node [
    id 360
    label "gestykulowanie"
  ]
  node [
    id 361
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 362
    label "pracownik"
  ]
  node [
    id 363
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 364
    label "bramkarz"
  ]
  node [
    id 365
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 366
    label "handwriting"
  ]
  node [
    id 367
    label "hasta"
  ]
  node [
    id 368
    label "pi&#322;ka"
  ]
  node [
    id 369
    label "&#322;okie&#263;"
  ]
  node [
    id 370
    label "spos&#243;b"
  ]
  node [
    id 371
    label "zagrywka"
  ]
  node [
    id 372
    label "obietnica"
  ]
  node [
    id 373
    label "przedrami&#281;"
  ]
  node [
    id 374
    label "chwyta&#263;"
  ]
  node [
    id 375
    label "r&#261;czyna"
  ]
  node [
    id 376
    label "wykroczenie"
  ]
  node [
    id 377
    label "kroki"
  ]
  node [
    id 378
    label "palec"
  ]
  node [
    id 379
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 380
    label "graba"
  ]
  node [
    id 381
    label "hand"
  ]
  node [
    id 382
    label "nadgarstek"
  ]
  node [
    id 383
    label "pomocnik"
  ]
  node [
    id 384
    label "k&#322;&#261;b"
  ]
  node [
    id 385
    label "hazena"
  ]
  node [
    id 386
    label "gestykulowa&#263;"
  ]
  node [
    id 387
    label "cmoknonsens"
  ]
  node [
    id 388
    label "d&#322;o&#324;"
  ]
  node [
    id 389
    label "chwytanie"
  ]
  node [
    id 390
    label "&#322;&#261;cznie"
  ]
  node [
    id 391
    label "miernota"
  ]
  node [
    id 392
    label "ciura"
  ]
  node [
    id 393
    label "ninie"
  ]
  node [
    id 394
    label "aktualny"
  ]
  node [
    id 395
    label "urz&#281;dnik"
  ]
  node [
    id 396
    label "dostojnik"
  ]
  node [
    id 397
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 398
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 399
    label "Wsch&#243;d"
  ]
  node [
    id 400
    label "rzecz"
  ]
  node [
    id 401
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 402
    label "sztuka"
  ]
  node [
    id 403
    label "religia"
  ]
  node [
    id 404
    label "przejmowa&#263;"
  ]
  node [
    id 405
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 406
    label "makrokosmos"
  ]
  node [
    id 407
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 408
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 409
    label "praca_rolnicza"
  ]
  node [
    id 410
    label "tradycja"
  ]
  node [
    id 411
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 412
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 413
    label "przejmowanie"
  ]
  node [
    id 414
    label "asymilowanie_si&#281;"
  ]
  node [
    id 415
    label "przej&#261;&#263;"
  ]
  node [
    id 416
    label "hodowla"
  ]
  node [
    id 417
    label "brzoskwiniarnia"
  ]
  node [
    id 418
    label "populace"
  ]
  node [
    id 419
    label "konwencja"
  ]
  node [
    id 420
    label "propriety"
  ]
  node [
    id 421
    label "jako&#347;&#263;"
  ]
  node [
    id 422
    label "kuchnia"
  ]
  node [
    id 423
    label "zwyczaj"
  ]
  node [
    id 424
    label "przej&#281;cie"
  ]
  node [
    id 425
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 426
    label "wcze&#347;nie"
  ]
  node [
    id 427
    label "utrzymywa&#263;"
  ]
  node [
    id 428
    label "dostarcza&#263;"
  ]
  node [
    id 429
    label "deliver"
  ]
  node [
    id 430
    label "obserwacja"
  ]
  node [
    id 431
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 432
    label "nauka_prawa"
  ]
  node [
    id 433
    label "dominion"
  ]
  node [
    id 434
    label "normatywizm"
  ]
  node [
    id 435
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 436
    label "qualification"
  ]
  node [
    id 437
    label "opis"
  ]
  node [
    id 438
    label "regu&#322;a_Allena"
  ]
  node [
    id 439
    label "normalizacja"
  ]
  node [
    id 440
    label "kazuistyka"
  ]
  node [
    id 441
    label "regu&#322;a_Glogera"
  ]
  node [
    id 442
    label "kultura_duchowa"
  ]
  node [
    id 443
    label "prawo_karne"
  ]
  node [
    id 444
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 445
    label "standard"
  ]
  node [
    id 446
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 447
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 448
    label "struktura"
  ]
  node [
    id 449
    label "szko&#322;a"
  ]
  node [
    id 450
    label "prawo_karne_procesowe"
  ]
  node [
    id 451
    label "prawo_Mendla"
  ]
  node [
    id 452
    label "przepis"
  ]
  node [
    id 453
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 454
    label "criterion"
  ]
  node [
    id 455
    label "kanonistyka"
  ]
  node [
    id 456
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 457
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 458
    label "wykonawczy"
  ]
  node [
    id 459
    label "twierdzenie"
  ]
  node [
    id 460
    label "judykatura"
  ]
  node [
    id 461
    label "legislacyjnie"
  ]
  node [
    id 462
    label "umocowa&#263;"
  ]
  node [
    id 463
    label "procesualistyka"
  ]
  node [
    id 464
    label "kierunek"
  ]
  node [
    id 465
    label "kryminologia"
  ]
  node [
    id 466
    label "kryminalistyka"
  ]
  node [
    id 467
    label "cywilistyka"
  ]
  node [
    id 468
    label "law"
  ]
  node [
    id 469
    label "zasada_d'Alemberta"
  ]
  node [
    id 470
    label "jurisprudence"
  ]
  node [
    id 471
    label "zasada"
  ]
  node [
    id 472
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 473
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 474
    label "konsekwencja"
  ]
  node [
    id 475
    label "punishment"
  ]
  node [
    id 476
    label "roboty_przymusowe"
  ]
  node [
    id 477
    label "nemezis"
  ]
  node [
    id 478
    label "righteousness"
  ]
  node [
    id 479
    label "come_up"
  ]
  node [
    id 480
    label "straci&#263;"
  ]
  node [
    id 481
    label "przej&#347;&#263;"
  ]
  node [
    id 482
    label "zast&#261;pi&#263;"
  ]
  node [
    id 483
    label "sprawi&#263;"
  ]
  node [
    id 484
    label "zyska&#263;"
  ]
  node [
    id 485
    label "use"
  ]
  node [
    id 486
    label "&#380;o&#322;nierz"
  ]
  node [
    id 487
    label "pies"
  ]
  node [
    id 488
    label "wait"
  ]
  node [
    id 489
    label "pomaga&#263;"
  ]
  node [
    id 490
    label "cel"
  ]
  node [
    id 491
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 492
    label "pracowa&#263;"
  ]
  node [
    id 493
    label "suffice"
  ]
  node [
    id 494
    label "match"
  ]
  node [
    id 495
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 496
    label "dzielna"
  ]
  node [
    id 497
    label "liczenie"
  ]
  node [
    id 498
    label "robienie"
  ]
  node [
    id 499
    label "rozdawanie"
  ]
  node [
    id 500
    label "stosowanie"
  ]
  node [
    id 501
    label "rozprowadzanie"
  ]
  node [
    id 502
    label "separation"
  ]
  node [
    id 503
    label "dzielnik"
  ]
  node [
    id 504
    label "sk&#322;&#243;canie"
  ]
  node [
    id 505
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 506
    label "division"
  ]
  node [
    id 507
    label "wyodr&#281;bnianie"
  ]
  node [
    id 508
    label "powodowanie"
  ]
  node [
    id 509
    label "contribution"
  ]
  node [
    id 510
    label "iloraz"
  ]
  node [
    id 511
    label "przeszkoda"
  ]
  node [
    id 512
    label "dzielenie_si&#281;"
  ]
  node [
    id 513
    label "proporcja"
  ]
  node [
    id 514
    label "parity"
  ]
  node [
    id 515
    label "Partia"
  ]
  node [
    id 516
    label "utrwalacz_w&#322;adzy_ludowej"
  ]
  node [
    id 517
    label "partyjnie"
  ]
  node [
    id 518
    label "polityczny"
  ]
  node [
    id 519
    label "polityk"
  ]
  node [
    id 520
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 521
    label "zakres"
  ]
  node [
    id 522
    label "huczek"
  ]
  node [
    id 523
    label "wymiar"
  ]
  node [
    id 524
    label "strefa"
  ]
  node [
    id 525
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 526
    label "powierzchnia"
  ]
  node [
    id 527
    label "kolur"
  ]
  node [
    id 528
    label "kula"
  ]
  node [
    id 529
    label "sector"
  ]
  node [
    id 530
    label "p&#243;&#322;sfera"
  ]
  node [
    id 531
    label "p&#243;&#322;kula"
  ]
  node [
    id 532
    label "class"
  ]
  node [
    id 533
    label "&#347;lad"
  ]
  node [
    id 534
    label "doch&#243;d_narodowy"
  ]
  node [
    id 535
    label "rezultat"
  ]
  node [
    id 536
    label "kwota"
  ]
  node [
    id 537
    label "lobbysta"
  ]
  node [
    id 538
    label "gwiazda"
  ]
  node [
    id 539
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 540
    label "desire"
  ]
  node [
    id 541
    label "kcie&#263;"
  ]
  node [
    id 542
    label "talk"
  ]
  node [
    id 543
    label "gaworzy&#263;"
  ]
  node [
    id 544
    label "kontaktowa&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 54
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 29
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 30
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 18
    target 195
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 49
  ]
  edge [
    source 18
    target 35
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 70
  ]
  edge [
    source 19
    target 79
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 82
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 108
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 22
    target 31
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 205
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 22
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 23
    target 211
  ]
  edge [
    source 23
    target 212
  ]
  edge [
    source 23
    target 213
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 24
    target 49
  ]
  edge [
    source 25
    target 50
  ]
  edge [
    source 25
    target 44
  ]
  edge [
    source 25
    target 80
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 73
  ]
  edge [
    source 26
    target 43
  ]
  edge [
    source 26
    target 227
  ]
  edge [
    source 26
    target 228
  ]
  edge [
    source 26
    target 229
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 74
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 164
  ]
  edge [
    source 27
    target 239
  ]
  edge [
    source 27
    target 240
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 49
  ]
  edge [
    source 29
    target 50
  ]
  edge [
    source 29
    target 241
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 242
  ]
  edge [
    source 29
    target 243
  ]
  edge [
    source 29
    target 244
  ]
  edge [
    source 29
    target 245
  ]
  edge [
    source 29
    target 246
  ]
  edge [
    source 31
    target 247
  ]
  edge [
    source 31
    target 248
  ]
  edge [
    source 31
    target 249
  ]
  edge [
    source 31
    target 250
  ]
  edge [
    source 31
    target 251
  ]
  edge [
    source 31
    target 252
  ]
  edge [
    source 31
    target 253
  ]
  edge [
    source 31
    target 254
  ]
  edge [
    source 31
    target 255
  ]
  edge [
    source 31
    target 256
  ]
  edge [
    source 31
    target 257
  ]
  edge [
    source 31
    target 258
  ]
  edge [
    source 31
    target 259
  ]
  edge [
    source 31
    target 51
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 51
  ]
  edge [
    source 33
    target 52
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 52
  ]
  edge [
    source 34
    target 53
  ]
  edge [
    source 34
    target 63
  ]
  edge [
    source 34
    target 260
  ]
  edge [
    source 34
    target 261
  ]
  edge [
    source 34
    target 262
  ]
  edge [
    source 34
    target 263
  ]
  edge [
    source 34
    target 264
  ]
  edge [
    source 34
    target 265
  ]
  edge [
    source 34
    target 266
  ]
  edge [
    source 34
    target 267
  ]
  edge [
    source 34
    target 268
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 64
  ]
  edge [
    source 35
    target 241
  ]
  edge [
    source 35
    target 166
  ]
  edge [
    source 35
    target 269
  ]
  edge [
    source 35
    target 270
  ]
  edge [
    source 35
    target 271
  ]
  edge [
    source 35
    target 272
  ]
  edge [
    source 35
    target 273
  ]
  edge [
    source 35
    target 274
  ]
  edge [
    source 35
    target 85
  ]
  edge [
    source 35
    target 275
  ]
  edge [
    source 35
    target 88
  ]
  edge [
    source 35
    target 276
  ]
  edge [
    source 35
    target 277
  ]
  edge [
    source 35
    target 278
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 48
  ]
  edge [
    source 36
    target 49
  ]
  edge [
    source 36
    target 54
  ]
  edge [
    source 36
    target 72
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 279
  ]
  edge [
    source 37
    target 280
  ]
  edge [
    source 37
    target 281
  ]
  edge [
    source 37
    target 282
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 283
  ]
  edge [
    source 38
    target 284
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 285
  ]
  edge [
    source 39
    target 112
  ]
  edge [
    source 39
    target 286
  ]
  edge [
    source 39
    target 287
  ]
  edge [
    source 39
    target 288
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 51
  ]
  edge [
    source 40
    target 289
  ]
  edge [
    source 40
    target 290
  ]
  edge [
    source 40
    target 291
  ]
  edge [
    source 40
    target 292
  ]
  edge [
    source 40
    target 293
  ]
  edge [
    source 40
    target 294
  ]
  edge [
    source 40
    target 295
  ]
  edge [
    source 40
    target 68
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 296
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 297
  ]
  edge [
    source 43
    target 298
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 299
  ]
  edge [
    source 44
    target 300
  ]
  edge [
    source 44
    target 301
  ]
  edge [
    source 44
    target 302
  ]
  edge [
    source 44
    target 303
  ]
  edge [
    source 44
    target 304
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 68
  ]
  edge [
    source 45
    target 74
  ]
  edge [
    source 45
    target 305
  ]
  edge [
    source 45
    target 306
  ]
  edge [
    source 45
    target 307
  ]
  edge [
    source 45
    target 308
  ]
  edge [
    source 45
    target 62
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 309
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 310
  ]
  edge [
    source 48
    target 311
  ]
  edge [
    source 48
    target 312
  ]
  edge [
    source 48
    target 313
  ]
  edge [
    source 48
    target 314
  ]
  edge [
    source 48
    target 315
  ]
  edge [
    source 48
    target 316
  ]
  edge [
    source 48
    target 317
  ]
  edge [
    source 48
    target 318
  ]
  edge [
    source 49
    target 120
  ]
  edge [
    source 49
    target 319
  ]
  edge [
    source 49
    target 320
  ]
  edge [
    source 49
    target 321
  ]
  edge [
    source 49
    target 122
  ]
  edge [
    source 49
    target 322
  ]
  edge [
    source 49
    target 323
  ]
  edge [
    source 50
    target 324
  ]
  edge [
    source 50
    target 325
  ]
  edge [
    source 50
    target 326
  ]
  edge [
    source 50
    target 327
  ]
  edge [
    source 50
    target 85
  ]
  edge [
    source 50
    target 328
  ]
  edge [
    source 50
    target 329
  ]
  edge [
    source 50
    target 330
  ]
  edge [
    source 50
    target 331
  ]
  edge [
    source 50
    target 332
  ]
  edge [
    source 51
    target 333
  ]
  edge [
    source 51
    target 334
  ]
  edge [
    source 51
    target 335
  ]
  edge [
    source 51
    target 336
  ]
  edge [
    source 52
    target 337
  ]
  edge [
    source 52
    target 338
  ]
  edge [
    source 52
    target 181
  ]
  edge [
    source 52
    target 339
  ]
  edge [
    source 52
    target 340
  ]
  edge [
    source 52
    target 341
  ]
  edge [
    source 52
    target 75
  ]
  edge [
    source 53
    target 342
  ]
  edge [
    source 53
    target 343
  ]
  edge [
    source 53
    target 344
  ]
  edge [
    source 53
    target 160
  ]
  edge [
    source 53
    target 77
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 72
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 345
  ]
  edge [
    source 55
    target 346
  ]
  edge [
    source 55
    target 347
  ]
  edge [
    source 55
    target 348
  ]
  edge [
    source 55
    target 349
  ]
  edge [
    source 55
    target 350
  ]
  edge [
    source 55
    target 351
  ]
  edge [
    source 55
    target 352
  ]
  edge [
    source 55
    target 353
  ]
  edge [
    source 55
    target 354
  ]
  edge [
    source 55
    target 355
  ]
  edge [
    source 55
    target 356
  ]
  edge [
    source 55
    target 70
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 357
  ]
  edge [
    source 57
    target 358
  ]
  edge [
    source 57
    target 359
  ]
  edge [
    source 57
    target 360
  ]
  edge [
    source 57
    target 361
  ]
  edge [
    source 57
    target 362
  ]
  edge [
    source 57
    target 363
  ]
  edge [
    source 57
    target 364
  ]
  edge [
    source 57
    target 365
  ]
  edge [
    source 57
    target 366
  ]
  edge [
    source 57
    target 367
  ]
  edge [
    source 57
    target 368
  ]
  edge [
    source 57
    target 369
  ]
  edge [
    source 57
    target 370
  ]
  edge [
    source 57
    target 371
  ]
  edge [
    source 57
    target 372
  ]
  edge [
    source 57
    target 373
  ]
  edge [
    source 57
    target 374
  ]
  edge [
    source 57
    target 375
  ]
  edge [
    source 57
    target 168
  ]
  edge [
    source 57
    target 376
  ]
  edge [
    source 57
    target 377
  ]
  edge [
    source 57
    target 378
  ]
  edge [
    source 57
    target 379
  ]
  edge [
    source 57
    target 380
  ]
  edge [
    source 57
    target 381
  ]
  edge [
    source 57
    target 382
  ]
  edge [
    source 57
    target 383
  ]
  edge [
    source 57
    target 384
  ]
  edge [
    source 57
    target 385
  ]
  edge [
    source 57
    target 386
  ]
  edge [
    source 57
    target 387
  ]
  edge [
    source 57
    target 388
  ]
  edge [
    source 57
    target 389
  ]
  edge [
    source 57
    target 332
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 390
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 391
  ]
  edge [
    source 60
    target 392
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 393
  ]
  edge [
    source 65
    target 394
  ]
  edge [
    source 65
    target 282
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 245
  ]
  edge [
    source 66
    target 395
  ]
  edge [
    source 66
    target 396
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 397
  ]
  edge [
    source 67
    target 131
  ]
  edge [
    source 67
    target 398
  ]
  edge [
    source 67
    target 399
  ]
  edge [
    source 67
    target 400
  ]
  edge [
    source 67
    target 401
  ]
  edge [
    source 67
    target 402
  ]
  edge [
    source 67
    target 403
  ]
  edge [
    source 67
    target 404
  ]
  edge [
    source 67
    target 405
  ]
  edge [
    source 67
    target 406
  ]
  edge [
    source 67
    target 407
  ]
  edge [
    source 67
    target 408
  ]
  edge [
    source 67
    target 330
  ]
  edge [
    source 67
    target 409
  ]
  edge [
    source 67
    target 410
  ]
  edge [
    source 67
    target 411
  ]
  edge [
    source 67
    target 412
  ]
  edge [
    source 67
    target 413
  ]
  edge [
    source 67
    target 168
  ]
  edge [
    source 67
    target 414
  ]
  edge [
    source 67
    target 415
  ]
  edge [
    source 67
    target 416
  ]
  edge [
    source 67
    target 417
  ]
  edge [
    source 67
    target 418
  ]
  edge [
    source 67
    target 419
  ]
  edge [
    source 67
    target 420
  ]
  edge [
    source 67
    target 421
  ]
  edge [
    source 67
    target 422
  ]
  edge [
    source 67
    target 423
  ]
  edge [
    source 67
    target 424
  ]
  edge [
    source 67
    target 425
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 426
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 427
  ]
  edge [
    source 70
    target 428
  ]
  edge [
    source 70
    target 429
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 430
  ]
  edge [
    source 71
    target 431
  ]
  edge [
    source 71
    target 432
  ]
  edge [
    source 71
    target 433
  ]
  edge [
    source 71
    target 434
  ]
  edge [
    source 71
    target 435
  ]
  edge [
    source 71
    target 436
  ]
  edge [
    source 71
    target 437
  ]
  edge [
    source 71
    target 438
  ]
  edge [
    source 71
    target 439
  ]
  edge [
    source 71
    target 440
  ]
  edge [
    source 71
    target 441
  ]
  edge [
    source 71
    target 442
  ]
  edge [
    source 71
    target 443
  ]
  edge [
    source 71
    target 444
  ]
  edge [
    source 71
    target 445
  ]
  edge [
    source 71
    target 446
  ]
  edge [
    source 71
    target 447
  ]
  edge [
    source 71
    target 448
  ]
  edge [
    source 71
    target 449
  ]
  edge [
    source 71
    target 450
  ]
  edge [
    source 71
    target 451
  ]
  edge [
    source 71
    target 452
  ]
  edge [
    source 71
    target 453
  ]
  edge [
    source 71
    target 454
  ]
  edge [
    source 71
    target 455
  ]
  edge [
    source 71
    target 456
  ]
  edge [
    source 71
    target 457
  ]
  edge [
    source 71
    target 458
  ]
  edge [
    source 71
    target 459
  ]
  edge [
    source 71
    target 460
  ]
  edge [
    source 71
    target 461
  ]
  edge [
    source 71
    target 462
  ]
  edge [
    source 71
    target 271
  ]
  edge [
    source 71
    target 463
  ]
  edge [
    source 71
    target 464
  ]
  edge [
    source 71
    target 465
  ]
  edge [
    source 71
    target 466
  ]
  edge [
    source 71
    target 467
  ]
  edge [
    source 71
    target 468
  ]
  edge [
    source 71
    target 469
  ]
  edge [
    source 71
    target 470
  ]
  edge [
    source 71
    target 471
  ]
  edge [
    source 71
    target 472
  ]
  edge [
    source 71
    target 473
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 474
  ]
  edge [
    source 72
    target 475
  ]
  edge [
    source 72
    target 168
  ]
  edge [
    source 72
    target 476
  ]
  edge [
    source 72
    target 477
  ]
  edge [
    source 72
    target 478
  ]
  edge [
    source 73
    target 479
  ]
  edge [
    source 73
    target 480
  ]
  edge [
    source 73
    target 481
  ]
  edge [
    source 73
    target 482
  ]
  edge [
    source 73
    target 483
  ]
  edge [
    source 73
    target 484
  ]
  edge [
    source 73
    target 192
  ]
  edge [
    source 73
    target 193
  ]
  edge [
    source 73
    target 81
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 485
  ]
  edge [
    source 74
    target 228
  ]
  edge [
    source 74
    target 486
  ]
  edge [
    source 74
    target 487
  ]
  edge [
    source 74
    target 352
  ]
  edge [
    source 74
    target 488
  ]
  edge [
    source 74
    target 489
  ]
  edge [
    source 74
    target 490
  ]
  edge [
    source 74
    target 317
  ]
  edge [
    source 74
    target 491
  ]
  edge [
    source 74
    target 492
  ]
  edge [
    source 74
    target 493
  ]
  edge [
    source 74
    target 494
  ]
  edge [
    source 74
    target 495
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 496
  ]
  edge [
    source 75
    target 497
  ]
  edge [
    source 75
    target 498
  ]
  edge [
    source 75
    target 499
  ]
  edge [
    source 75
    target 195
  ]
  edge [
    source 75
    target 500
  ]
  edge [
    source 75
    target 501
  ]
  edge [
    source 75
    target 502
  ]
  edge [
    source 75
    target 503
  ]
  edge [
    source 75
    target 504
  ]
  edge [
    source 75
    target 505
  ]
  edge [
    source 75
    target 506
  ]
  edge [
    source 75
    target 507
  ]
  edge [
    source 75
    target 508
  ]
  edge [
    source 75
    target 509
  ]
  edge [
    source 75
    target 510
  ]
  edge [
    source 75
    target 511
  ]
  edge [
    source 75
    target 512
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 513
  ]
  edge [
    source 76
    target 514
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 515
  ]
  edge [
    source 77
    target 516
  ]
  edge [
    source 77
    target 517
  ]
  edge [
    source 77
    target 518
  ]
  edge [
    source 77
    target 519
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 520
  ]
  edge [
    source 78
    target 521
  ]
  edge [
    source 78
    target 522
  ]
  edge [
    source 78
    target 523
  ]
  edge [
    source 78
    target 524
  ]
  edge [
    source 78
    target 85
  ]
  edge [
    source 78
    target 170
  ]
  edge [
    source 78
    target 525
  ]
  edge [
    source 78
    target 526
  ]
  edge [
    source 78
    target 527
  ]
  edge [
    source 78
    target 528
  ]
  edge [
    source 78
    target 529
  ]
  edge [
    source 78
    target 530
  ]
  edge [
    source 78
    target 531
  ]
  edge [
    source 78
    target 532
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 533
  ]
  edge [
    source 79
    target 534
  ]
  edge [
    source 79
    target 330
  ]
  edge [
    source 79
    target 535
  ]
  edge [
    source 79
    target 536
  ]
  edge [
    source 79
    target 537
  ]
  edge [
    source 80
    target 538
  ]
  edge [
    source 80
    target 539
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 238
  ]
  edge [
    source 81
    target 540
  ]
  edge [
    source 81
    target 541
  ]
  edge [
    source 82
    target 542
  ]
  edge [
    source 82
    target 543
  ]
  edge [
    source 82
    target 544
  ]
]
