graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.1153846153846154
  density 0.005827505827505828
  graphCliqueNumber 3
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 2
    label "jeszcze"
    origin "text"
  ]
  node [
    id 3
    label "wcze&#347;nie"
    origin "text"
  ]
  node [
    id 4
    label "obstawia&#263;"
    origin "text"
  ]
  node [
    id 5
    label "kiedy"
    origin "text"
  ]
  node [
    id 6
    label "bill"
    origin "text"
  ]
  node [
    id 7
    label "gates"
    origin "text"
  ]
  node [
    id 8
    label "wr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 9
    label "microsoft"
    origin "text"
  ]
  node [
    id 10
    label "skoro"
    origin "text"
  ]
  node [
    id 11
    label "oficjalnie"
    origin "text"
  ]
  node [
    id 12
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 13
    label "odej&#347;&#263;"
    origin "text"
  ]
  node [
    id 14
    label "dopiero"
    origin "text"
  ]
  node [
    id 15
    label "lipiec"
    origin "text"
  ]
  node [
    id 16
    label "william"
    origin "text"
  ]
  node [
    id 17
    label "hill"
    origin "text"
  ]
  node [
    id 18
    label "londyn"
    origin "text"
  ]
  node [
    id 19
    label "taki"
    origin "text"
  ]
  node [
    id 20
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 21
    label "przyjmowa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "raz"
    origin "text"
  ]
  node [
    id 23
    label "zaliczy&#263;"
    origin "text"
  ]
  node [
    id 24
    label "ostatnie"
    origin "text"
  ]
  node [
    id 25
    label "wyst&#261;pienie"
    origin "text"
  ]
  node [
    id 26
    label "ces"
    origin "text"
  ]
  node [
    id 27
    label "ostatni"
    origin "text"
  ]
  node [
    id 28
    label "lato"
    origin "text"
  ]
  node [
    id 29
    label "pozostawa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "cie&#324;"
    origin "text"
  ]
  node [
    id 31
    label "przem&#243;wienie"
    origin "text"
  ]
  node [
    id 32
    label "steve'a"
    origin "text"
  ]
  node [
    id 33
    label "jobsa"
    origin "text"
  ]
  node [
    id 34
    label "steve"
    origin "text"
  ]
  node [
    id 35
    label "wyst&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 36
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 37
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 38
    label "ale"
    origin "text"
  ]
  node [
    id 39
    label "nawet"
    origin "text"
  ]
  node [
    id 40
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 41
    label "og&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "odchodzi&#263;"
    origin "text"
  ]
  node [
    id 43
    label "wielki"
    origin "text"
  ]
  node [
    id 44
    label "sensacja"
    origin "text"
  ]
  node [
    id 45
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 46
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 47
    label "michael"
    origin "text"
  ]
  node [
    id 48
    label "jordan"
    origin "text"
  ]
  node [
    id 49
    label "te&#380;"
    origin "text"
  ]
  node [
    id 50
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 51
    label "kolejny"
    origin "text"
  ]
  node [
    id 52
    label "upokorzy&#263;"
    origin "text"
  ]
  node [
    id 53
    label "konkurencja"
    origin "text"
  ]
  node [
    id 54
    label "si&#281;ga&#263;"
  ]
  node [
    id 55
    label "trwa&#263;"
  ]
  node [
    id 56
    label "obecno&#347;&#263;"
  ]
  node [
    id 57
    label "stan"
  ]
  node [
    id 58
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 59
    label "stand"
  ]
  node [
    id 60
    label "mie&#263;_miejsce"
  ]
  node [
    id 61
    label "uczestniczy&#263;"
  ]
  node [
    id 62
    label "chodzi&#263;"
  ]
  node [
    id 63
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 64
    label "equal"
  ]
  node [
    id 65
    label "ci&#261;gle"
  ]
  node [
    id 66
    label "wczesno"
  ]
  node [
    id 67
    label "wczesny"
  ]
  node [
    id 68
    label "typ"
  ]
  node [
    id 69
    label "zapewnia&#263;"
  ]
  node [
    id 70
    label "zastawia&#263;"
  ]
  node [
    id 71
    label "bramka"
  ]
  node [
    id 72
    label "broni&#263;"
  ]
  node [
    id 73
    label "budowa&#263;"
  ]
  node [
    id 74
    label "ochrona"
  ]
  node [
    id 75
    label "zajmowa&#263;"
  ]
  node [
    id 76
    label "os&#322;ania&#263;"
  ]
  node [
    id 77
    label "przewidywa&#263;"
  ]
  node [
    id 78
    label "otacza&#263;"
  ]
  node [
    id 79
    label "typowa&#263;"
  ]
  node [
    id 80
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 81
    label "powierza&#263;"
  ]
  node [
    id 82
    label "venture"
  ]
  node [
    id 83
    label "ubezpiecza&#263;"
  ]
  node [
    id 84
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 85
    label "obejmowa&#263;"
  ]
  node [
    id 86
    label "frame"
  ]
  node [
    id 87
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 88
    label "wysy&#322;a&#263;"
  ]
  node [
    id 89
    label "render"
  ]
  node [
    id 90
    label "return"
  ]
  node [
    id 91
    label "zosta&#263;"
  ]
  node [
    id 92
    label "spowodowa&#263;"
  ]
  node [
    id 93
    label "przyj&#347;&#263;"
  ]
  node [
    id 94
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 95
    label "revive"
  ]
  node [
    id 96
    label "pojedna&#263;_si&#281;"
  ]
  node [
    id 97
    label "podj&#261;&#263;"
  ]
  node [
    id 98
    label "nawi&#261;za&#263;"
  ]
  node [
    id 99
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 100
    label "przyby&#263;"
  ]
  node [
    id 101
    label "recur"
  ]
  node [
    id 102
    label "regularly"
  ]
  node [
    id 103
    label "legalnie"
  ]
  node [
    id 104
    label "oficjalny"
  ]
  node [
    id 105
    label "pow&#347;ci&#261;gliwie"
  ]
  node [
    id 106
    label "formalny"
  ]
  node [
    id 107
    label "czyj&#347;"
  ]
  node [
    id 108
    label "m&#261;&#380;"
  ]
  node [
    id 109
    label "opu&#347;ci&#263;"
  ]
  node [
    id 110
    label "zrezygnowa&#263;"
  ]
  node [
    id 111
    label "proceed"
  ]
  node [
    id 112
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 113
    label "leave_office"
  ]
  node [
    id 114
    label "retract"
  ]
  node [
    id 115
    label "min&#261;&#263;"
  ]
  node [
    id 116
    label "przesta&#263;"
  ]
  node [
    id 117
    label "odrzut"
  ]
  node [
    id 118
    label "ruszy&#263;"
  ]
  node [
    id 119
    label "drop"
  ]
  node [
    id 120
    label "die"
  ]
  node [
    id 121
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 122
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 123
    label "miesi&#261;c"
  ]
  node [
    id 124
    label "okre&#347;lony"
  ]
  node [
    id 125
    label "jaki&#347;"
  ]
  node [
    id 126
    label "czyn"
  ]
  node [
    id 127
    label "company"
  ]
  node [
    id 128
    label "zak&#322;adka"
  ]
  node [
    id 129
    label "firma"
  ]
  node [
    id 130
    label "instytut"
  ]
  node [
    id 131
    label "wyko&#324;czenie"
  ]
  node [
    id 132
    label "jednostka_organizacyjna"
  ]
  node [
    id 133
    label "umowa"
  ]
  node [
    id 134
    label "instytucja"
  ]
  node [
    id 135
    label "miejsce_pracy"
  ]
  node [
    id 136
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 137
    label "poch&#322;ania&#263;"
  ]
  node [
    id 138
    label "dostarcza&#263;"
  ]
  node [
    id 139
    label "umieszcza&#263;"
  ]
  node [
    id 140
    label "uznawa&#263;"
  ]
  node [
    id 141
    label "swallow"
  ]
  node [
    id 142
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 143
    label "admit"
  ]
  node [
    id 144
    label "fall"
  ]
  node [
    id 145
    label "undertake"
  ]
  node [
    id 146
    label "dopuszcza&#263;"
  ]
  node [
    id 147
    label "wyprawia&#263;"
  ]
  node [
    id 148
    label "robi&#263;"
  ]
  node [
    id 149
    label "wpuszcza&#263;"
  ]
  node [
    id 150
    label "close"
  ]
  node [
    id 151
    label "przyjmowanie"
  ]
  node [
    id 152
    label "obiera&#263;"
  ]
  node [
    id 153
    label "pracowa&#263;"
  ]
  node [
    id 154
    label "bra&#263;"
  ]
  node [
    id 155
    label "zgadza&#263;_si&#281;"
  ]
  node [
    id 156
    label "odbiera&#263;"
  ]
  node [
    id 157
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 158
    label "chwila"
  ]
  node [
    id 159
    label "uderzenie"
  ]
  node [
    id 160
    label "cios"
  ]
  node [
    id 161
    label "time"
  ]
  node [
    id 162
    label "score"
  ]
  node [
    id 163
    label "przyporz&#261;dkowa&#263;"
  ]
  node [
    id 164
    label "wliczy&#263;"
  ]
  node [
    id 165
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 166
    label "think"
  ]
  node [
    id 167
    label "przelecie&#263;"
  ]
  node [
    id 168
    label "wywi&#261;za&#263;_si&#281;"
  ]
  node [
    id 169
    label "policzy&#263;"
  ]
  node [
    id 170
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 171
    label "stwierdzi&#263;"
  ]
  node [
    id 172
    label "dmuchn&#261;&#263;"
  ]
  node [
    id 173
    label "odb&#281;bni&#263;"
  ]
  node [
    id 174
    label "wyj&#347;cie"
  ]
  node [
    id 175
    label "wyst&#281;p"
  ]
  node [
    id 176
    label "exit"
  ]
  node [
    id 177
    label "appearance"
  ]
  node [
    id 178
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 179
    label "przepisanie_si&#281;"
  ]
  node [
    id 180
    label "porobienie_si&#281;"
  ]
  node [
    id 181
    label "case"
  ]
  node [
    id 182
    label "naznaczenie"
  ]
  node [
    id 183
    label "pojawienie_si&#281;"
  ]
  node [
    id 184
    label "zacz&#281;cie"
  ]
  node [
    id 185
    label "zrezygnowanie"
  ]
  node [
    id 186
    label "egress"
  ]
  node [
    id 187
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 188
    label "happening"
  ]
  node [
    id 189
    label "zdarzenie_si&#281;"
  ]
  node [
    id 190
    label "event"
  ]
  node [
    id 191
    label "nak&#322;onienie"
  ]
  node [
    id 192
    label "performance"
  ]
  node [
    id 193
    label "odst&#261;pienie"
  ]
  node [
    id 194
    label "do"
  ]
  node [
    id 195
    label "si"
  ]
  node [
    id 196
    label "cz&#322;owiek"
  ]
  node [
    id 197
    label "istota_&#380;ywa"
  ]
  node [
    id 198
    label "najgorszy"
  ]
  node [
    id 199
    label "aktualny"
  ]
  node [
    id 200
    label "ostatnio"
  ]
  node [
    id 201
    label "niedawno"
  ]
  node [
    id 202
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 203
    label "sko&#324;czony"
  ]
  node [
    id 204
    label "poprzedni"
  ]
  node [
    id 205
    label "pozosta&#322;y"
  ]
  node [
    id 206
    label "w&#261;tpliwy"
  ]
  node [
    id 207
    label "pora_roku"
  ]
  node [
    id 208
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 209
    label "stop"
  ]
  node [
    id 210
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 211
    label "przebywa&#263;"
  ]
  node [
    id 212
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 213
    label "support"
  ]
  node [
    id 214
    label "blend"
  ]
  node [
    id 215
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 216
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 217
    label "odrobina"
  ]
  node [
    id 218
    label "&#263;ma"
  ]
  node [
    id 219
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 220
    label "eyeshadow"
  ]
  node [
    id 221
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 222
    label "obw&#243;dka"
  ]
  node [
    id 223
    label "rzuca&#263;"
  ]
  node [
    id 224
    label "archetyp"
  ]
  node [
    id 225
    label "sowie_oczy"
  ]
  node [
    id 226
    label "wycieniowa&#263;"
  ]
  node [
    id 227
    label "rzucenie"
  ]
  node [
    id 228
    label "nekromancja"
  ]
  node [
    id 229
    label "ciemnota"
  ]
  node [
    id 230
    label "noktowizja"
  ]
  node [
    id 231
    label "Ereb"
  ]
  node [
    id 232
    label "cieniowa&#263;"
  ]
  node [
    id 233
    label "pomrok"
  ]
  node [
    id 234
    label "zjawisko"
  ]
  node [
    id 235
    label "duch"
  ]
  node [
    id 236
    label "zacienie"
  ]
  node [
    id 237
    label "zm&#281;czenie"
  ]
  node [
    id 238
    label "rzuci&#263;"
  ]
  node [
    id 239
    label "plama"
  ]
  node [
    id 240
    label "bearing"
  ]
  node [
    id 241
    label "zmar&#322;y"
  ]
  node [
    id 242
    label "shade"
  ]
  node [
    id 243
    label "kszta&#322;t"
  ]
  node [
    id 244
    label "rzucanie"
  ]
  node [
    id 245
    label "sylwetka"
  ]
  node [
    id 246
    label "miejsce"
  ]
  node [
    id 247
    label "kosmetyk_kolorowy"
  ]
  node [
    id 248
    label "oznaka"
  ]
  node [
    id 249
    label "zjawa"
  ]
  node [
    id 250
    label "przebarwienie"
  ]
  node [
    id 251
    label "cloud"
  ]
  node [
    id 252
    label "oko"
  ]
  node [
    id 253
    label "sermon"
  ]
  node [
    id 254
    label "wypowied&#378;"
  ]
  node [
    id 255
    label "obronienie"
  ]
  node [
    id 256
    label "wydanie"
  ]
  node [
    id 257
    label "oddzia&#322;anie"
  ]
  node [
    id 258
    label "odzyskanie"
  ]
  node [
    id 259
    label "wyg&#322;oszenie"
  ]
  node [
    id 260
    label "zrozumienie"
  ]
  node [
    id 261
    label "talk"
  ]
  node [
    id 262
    label "wydobycie"
  ]
  node [
    id 263
    label "address"
  ]
  node [
    id 264
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 265
    label "odst&#261;pi&#263;"
  ]
  node [
    id 266
    label "zacz&#261;&#263;"
  ]
  node [
    id 267
    label "wyj&#347;&#263;"
  ]
  node [
    id 268
    label "happen"
  ]
  node [
    id 269
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 270
    label "perform"
  ]
  node [
    id 271
    label "nak&#322;oni&#263;"
  ]
  node [
    id 272
    label "appear"
  ]
  node [
    id 273
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 274
    label "doba"
  ]
  node [
    id 275
    label "czas"
  ]
  node [
    id 276
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 277
    label "weekend"
  ]
  node [
    id 278
    label "Nowy_Rok"
  ]
  node [
    id 279
    label "piwo"
  ]
  node [
    id 280
    label "communicate"
  ]
  node [
    id 281
    label "opublikowa&#263;"
  ]
  node [
    id 282
    label "obwo&#322;a&#263;"
  ]
  node [
    id 283
    label "poda&#263;"
  ]
  node [
    id 284
    label "publish"
  ]
  node [
    id 285
    label "declare"
  ]
  node [
    id 286
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 287
    label "odstawa&#263;"
  ]
  node [
    id 288
    label "opuszcza&#263;"
  ]
  node [
    id 289
    label "impart"
  ]
  node [
    id 290
    label "gasn&#261;&#263;"
  ]
  node [
    id 291
    label "przestawa&#263;"
  ]
  node [
    id 292
    label "wyrusza&#263;"
  ]
  node [
    id 293
    label "seclude"
  ]
  node [
    id 294
    label "mija&#263;"
  ]
  node [
    id 295
    label "rezygnowa&#263;"
  ]
  node [
    id 296
    label "rozgrywa&#263;_si&#281;"
  ]
  node [
    id 297
    label "i&#347;&#263;"
  ]
  node [
    id 298
    label "go"
  ]
  node [
    id 299
    label "dupny"
  ]
  node [
    id 300
    label "wysoce"
  ]
  node [
    id 301
    label "wyj&#261;tkowy"
  ]
  node [
    id 302
    label "wybitny"
  ]
  node [
    id 303
    label "znaczny"
  ]
  node [
    id 304
    label "prawdziwy"
  ]
  node [
    id 305
    label "wa&#380;ny"
  ]
  node [
    id 306
    label "nieprzeci&#281;tny"
  ]
  node [
    id 307
    label "rozg&#322;os"
  ]
  node [
    id 308
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 309
    label "novum"
  ]
  node [
    id 310
    label "disclosure"
  ]
  node [
    id 311
    label "podekscytowanie"
  ]
  node [
    id 312
    label "zamieszanie"
  ]
  node [
    id 313
    label "niespodzianka"
  ]
  node [
    id 314
    label "zorganizowa&#263;"
  ]
  node [
    id 315
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 316
    label "przerobi&#263;"
  ]
  node [
    id 317
    label "wystylizowa&#263;"
  ]
  node [
    id 318
    label "cause"
  ]
  node [
    id 319
    label "wydali&#263;"
  ]
  node [
    id 320
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 321
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 322
    label "post&#261;pi&#263;"
  ]
  node [
    id 323
    label "appoint"
  ]
  node [
    id 324
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 325
    label "nabra&#263;"
  ]
  node [
    id 326
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 327
    label "make"
  ]
  node [
    id 328
    label "inny"
  ]
  node [
    id 329
    label "nast&#281;pnie"
  ]
  node [
    id 330
    label "kt&#243;ry&#347;"
  ]
  node [
    id 331
    label "kolejno"
  ]
  node [
    id 332
    label "nastopny"
  ]
  node [
    id 333
    label "poni&#380;y&#263;"
  ]
  node [
    id 334
    label "humiliate"
  ]
  node [
    id 335
    label "dob&#243;r_naturalny"
  ]
  node [
    id 336
    label "dyscyplina_sportowa"
  ]
  node [
    id 337
    label "interakcja"
  ]
  node [
    id 338
    label "wydarzenie"
  ]
  node [
    id 339
    label "rywalizacja"
  ]
  node [
    id 340
    label "uczestnik"
  ]
  node [
    id 341
    label "contest"
  ]
  node [
    id 342
    label "Gates"
  ]
  node [
    id 343
    label "William"
  ]
  node [
    id 344
    label "Hill"
  ]
  node [
    id 345
    label "Stevea"
  ]
  node [
    id 346
    label "Jobsa"
  ]
  node [
    id 347
    label "Michael"
  ]
  node [
    id 348
    label "Linus"
  ]
  node [
    id 349
    label "Torvalds"
  ]
  node [
    id 350
    label "Karl"
  ]
  node [
    id 351
    label "Malone"
  ]
  node [
    id 352
    label "The"
  ]
  node [
    id 353
    label "office"
  ]
  node [
    id 354
    label "Baracka"
  ]
  node [
    id 355
    label "Obamy"
  ]
  node [
    id 356
    label "Hillary"
  ]
  node [
    id 357
    label "Clinton"
  ]
  node [
    id 358
    label "Mitta"
  ]
  node [
    id 359
    label "Romneya"
  ]
  node [
    id 360
    label "Freda"
  ]
  node [
    id 361
    label "Thompsona"
  ]
  node [
    id 362
    label "Barack"
  ]
  node [
    id 363
    label "Obama"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 29
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 46
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 35
  ]
  edge [
    source 14
    target 36
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 136
  ]
  edge [
    source 21
    target 137
  ]
  edge [
    source 21
    target 138
  ]
  edge [
    source 21
    target 139
  ]
  edge [
    source 21
    target 140
  ]
  edge [
    source 21
    target 141
  ]
  edge [
    source 21
    target 142
  ]
  edge [
    source 21
    target 143
  ]
  edge [
    source 21
    target 144
  ]
  edge [
    source 21
    target 145
  ]
  edge [
    source 21
    target 146
  ]
  edge [
    source 21
    target 147
  ]
  edge [
    source 21
    target 148
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 21
    target 150
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 22
    target 44
  ]
  edge [
    source 22
    target 45
  ]
  edge [
    source 22
    target 50
  ]
  edge [
    source 22
    target 51
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 23
    target 164
  ]
  edge [
    source 23
    target 165
  ]
  edge [
    source 23
    target 166
  ]
  edge [
    source 23
    target 167
  ]
  edge [
    source 23
    target 168
  ]
  edge [
    source 23
    target 169
  ]
  edge [
    source 23
    target 170
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 23
    target 173
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 25
    target 181
  ]
  edge [
    source 25
    target 182
  ]
  edge [
    source 25
    target 183
  ]
  edge [
    source 25
    target 184
  ]
  edge [
    source 25
    target 185
  ]
  edge [
    source 25
    target 186
  ]
  edge [
    source 25
    target 187
  ]
  edge [
    source 25
    target 188
  ]
  edge [
    source 25
    target 189
  ]
  edge [
    source 25
    target 190
  ]
  edge [
    source 25
    target 191
  ]
  edge [
    source 25
    target 192
  ]
  edge [
    source 25
    target 193
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 194
  ]
  edge [
    source 26
    target 195
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 196
  ]
  edge [
    source 27
    target 51
  ]
  edge [
    source 27
    target 197
  ]
  edge [
    source 27
    target 198
  ]
  edge [
    source 27
    target 199
  ]
  edge [
    source 27
    target 200
  ]
  edge [
    source 27
    target 201
  ]
  edge [
    source 27
    target 202
  ]
  edge [
    source 27
    target 203
  ]
  edge [
    source 27
    target 204
  ]
  edge [
    source 27
    target 205
  ]
  edge [
    source 27
    target 206
  ]
  edge [
    source 28
    target 207
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 208
  ]
  edge [
    source 29
    target 209
  ]
  edge [
    source 29
    target 210
  ]
  edge [
    source 29
    target 211
  ]
  edge [
    source 29
    target 212
  ]
  edge [
    source 29
    target 213
  ]
  edge [
    source 29
    target 214
  ]
  edge [
    source 29
    target 215
  ]
  edge [
    source 29
    target 216
  ]
  edge [
    source 29
    target 37
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 217
  ]
  edge [
    source 30
    target 218
  ]
  edge [
    source 30
    target 219
  ]
  edge [
    source 30
    target 220
  ]
  edge [
    source 30
    target 221
  ]
  edge [
    source 30
    target 222
  ]
  edge [
    source 30
    target 223
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 30
    target 227
  ]
  edge [
    source 30
    target 228
  ]
  edge [
    source 30
    target 229
  ]
  edge [
    source 30
    target 230
  ]
  edge [
    source 30
    target 231
  ]
  edge [
    source 30
    target 232
  ]
  edge [
    source 30
    target 233
  ]
  edge [
    source 30
    target 234
  ]
  edge [
    source 30
    target 235
  ]
  edge [
    source 30
    target 236
  ]
  edge [
    source 30
    target 237
  ]
  edge [
    source 30
    target 238
  ]
  edge [
    source 30
    target 239
  ]
  edge [
    source 30
    target 240
  ]
  edge [
    source 30
    target 241
  ]
  edge [
    source 30
    target 242
  ]
  edge [
    source 30
    target 243
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 30
    target 245
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 247
  ]
  edge [
    source 30
    target 248
  ]
  edge [
    source 30
    target 249
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 253
  ]
  edge [
    source 31
    target 254
  ]
  edge [
    source 31
    target 255
  ]
  edge [
    source 31
    target 256
  ]
  edge [
    source 31
    target 257
  ]
  edge [
    source 31
    target 258
  ]
  edge [
    source 31
    target 259
  ]
  edge [
    source 31
    target 260
  ]
  edge [
    source 31
    target 261
  ]
  edge [
    source 31
    target 262
  ]
  edge [
    source 31
    target 263
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 264
  ]
  edge [
    source 35
    target 265
  ]
  edge [
    source 35
    target 266
  ]
  edge [
    source 35
    target 267
  ]
  edge [
    source 35
    target 110
  ]
  edge [
    source 35
    target 165
  ]
  edge [
    source 35
    target 268
  ]
  edge [
    source 35
    target 269
  ]
  edge [
    source 35
    target 270
  ]
  edge [
    source 35
    target 271
  ]
  edge [
    source 35
    target 272
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 273
  ]
  edge [
    source 36
    target 274
  ]
  edge [
    source 36
    target 275
  ]
  edge [
    source 36
    target 276
  ]
  edge [
    source 36
    target 277
  ]
  edge [
    source 36
    target 123
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 278
  ]
  edge [
    source 37
    target 123
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 279
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 280
  ]
  edge [
    source 41
    target 281
  ]
  edge [
    source 41
    target 282
  ]
  edge [
    source 41
    target 283
  ]
  edge [
    source 41
    target 284
  ]
  edge [
    source 41
    target 285
  ]
  edge [
    source 42
    target 286
  ]
  edge [
    source 42
    target 287
  ]
  edge [
    source 42
    target 288
  ]
  edge [
    source 42
    target 289
  ]
  edge [
    source 42
    target 111
  ]
  edge [
    source 42
    target 290
  ]
  edge [
    source 42
    target 291
  ]
  edge [
    source 42
    target 212
  ]
  edge [
    source 42
    target 214
  ]
  edge [
    source 42
    target 292
  ]
  edge [
    source 42
    target 293
  ]
  edge [
    source 42
    target 117
  ]
  edge [
    source 42
    target 294
  ]
  edge [
    source 42
    target 295
  ]
  edge [
    source 42
    target 296
  ]
  edge [
    source 42
    target 297
  ]
  edge [
    source 42
    target 298
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 299
  ]
  edge [
    source 43
    target 300
  ]
  edge [
    source 43
    target 301
  ]
  edge [
    source 43
    target 302
  ]
  edge [
    source 43
    target 303
  ]
  edge [
    source 43
    target 304
  ]
  edge [
    source 43
    target 305
  ]
  edge [
    source 43
    target 306
  ]
  edge [
    source 44
    target 307
  ]
  edge [
    source 44
    target 308
  ]
  edge [
    source 44
    target 309
  ]
  edge [
    source 44
    target 310
  ]
  edge [
    source 44
    target 311
  ]
  edge [
    source 44
    target 312
  ]
  edge [
    source 44
    target 313
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 314
  ]
  edge [
    source 46
    target 315
  ]
  edge [
    source 46
    target 316
  ]
  edge [
    source 46
    target 317
  ]
  edge [
    source 46
    target 318
  ]
  edge [
    source 46
    target 319
  ]
  edge [
    source 46
    target 320
  ]
  edge [
    source 46
    target 321
  ]
  edge [
    source 46
    target 322
  ]
  edge [
    source 46
    target 323
  ]
  edge [
    source 46
    target 324
  ]
  edge [
    source 46
    target 325
  ]
  edge [
    source 46
    target 326
  ]
  edge [
    source 46
    target 327
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 347
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 328
  ]
  edge [
    source 51
    target 329
  ]
  edge [
    source 51
    target 330
  ]
  edge [
    source 51
    target 331
  ]
  edge [
    source 51
    target 332
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 333
  ]
  edge [
    source 52
    target 334
  ]
  edge [
    source 53
    target 335
  ]
  edge [
    source 53
    target 129
  ]
  edge [
    source 53
    target 336
  ]
  edge [
    source 53
    target 337
  ]
  edge [
    source 53
    target 338
  ]
  edge [
    source 53
    target 339
  ]
  edge [
    source 53
    target 340
  ]
  edge [
    source 53
    target 341
  ]
  edge [
    source 343
    target 344
  ]
  edge [
    source 345
    target 346
  ]
  edge [
    source 348
    target 349
  ]
  edge [
    source 350
    target 351
  ]
  edge [
    source 352
    target 353
  ]
  edge [
    source 354
    target 355
  ]
  edge [
    source 356
    target 357
  ]
  edge [
    source 358
    target 359
  ]
  edge [
    source 360
    target 361
  ]
  edge [
    source 362
    target 363
  ]
]
