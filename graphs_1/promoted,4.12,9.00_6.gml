graph [
  maxDegree 52
  minDegree 1
  meanDegree 1.9906976744186047
  density 0.009302325581395349
  graphCliqueNumber 2
  node [
    id 0
    label "fizyk"
    origin "text"
  ]
  node [
    id 1
    label "raz"
    origin "text"
  ]
  node [
    id 2
    label "pierwszy"
    origin "text"
  ]
  node [
    id 3
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "postulat"
    origin "text"
  ]
  node [
    id 5
    label "fizyka"
    origin "text"
  ]
  node [
    id 6
    label "klasyczny"
    origin "text"
  ]
  node [
    id 7
    label "zastosowanie"
    origin "text"
  ]
  node [
    id 8
    label "cia&#322;o"
    origin "text"
  ]
  node [
    id 9
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "kilkana&#347;cie"
    origin "text"
  ]
  node [
    id 12
    label "miliard"
    origin "text"
  ]
  node [
    id 13
    label "atom"
    origin "text"
  ]
  node [
    id 14
    label "by&#263;"
    origin "text"
  ]
  node [
    id 15
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "Doppler"
  ]
  node [
    id 17
    label "Galvani"
  ]
  node [
    id 18
    label "Maxwell"
  ]
  node [
    id 19
    label "William_Nicol"
  ]
  node [
    id 20
    label "Gilbert"
  ]
  node [
    id 21
    label "Weber"
  ]
  node [
    id 22
    label "Pascal"
  ]
  node [
    id 23
    label "Faraday"
  ]
  node [
    id 24
    label "Newton"
  ]
  node [
    id 25
    label "Kartezjusz"
  ]
  node [
    id 26
    label "naukowiec"
  ]
  node [
    id 27
    label "nauczyciel"
  ]
  node [
    id 28
    label "Lorentz"
  ]
  node [
    id 29
    label "Culomb"
  ]
  node [
    id 30
    label "Einstein"
  ]
  node [
    id 31
    label "Biot"
  ]
  node [
    id 32
    label "chwila"
  ]
  node [
    id 33
    label "uderzenie"
  ]
  node [
    id 34
    label "cios"
  ]
  node [
    id 35
    label "time"
  ]
  node [
    id 36
    label "najwa&#380;niejszy"
  ]
  node [
    id 37
    label "pocz&#261;tkowy"
  ]
  node [
    id 38
    label "dobry"
  ]
  node [
    id 39
    label "ch&#281;tny"
  ]
  node [
    id 40
    label "dzie&#324;"
  ]
  node [
    id 41
    label "pr&#281;dki"
  ]
  node [
    id 42
    label "spowodowa&#263;"
  ]
  node [
    id 43
    label "wyrazi&#263;"
  ]
  node [
    id 44
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 45
    label "przedstawi&#263;"
  ]
  node [
    id 46
    label "testify"
  ]
  node [
    id 47
    label "indicate"
  ]
  node [
    id 48
    label "przeszkoli&#263;"
  ]
  node [
    id 49
    label "udowodni&#263;"
  ]
  node [
    id 50
    label "poinformowa&#263;"
  ]
  node [
    id 51
    label "poda&#263;"
  ]
  node [
    id 52
    label "point"
  ]
  node [
    id 53
    label "wniosek"
  ]
  node [
    id 54
    label "aksjomat_Pascha"
  ]
  node [
    id 55
    label "aksjomat_Archimedesa"
  ]
  node [
    id 56
    label "czas"
  ]
  node [
    id 57
    label "za&#322;o&#380;enie"
  ]
  node [
    id 58
    label "axiom"
  ]
  node [
    id 59
    label "claim"
  ]
  node [
    id 60
    label "akustyka"
  ]
  node [
    id 61
    label "optyka"
  ]
  node [
    id 62
    label "termodynamika"
  ]
  node [
    id 63
    label "fizyka_cia&#322;a_sta&#322;ego"
  ]
  node [
    id 64
    label "przedmiot"
  ]
  node [
    id 65
    label "fizyka_kwantowa"
  ]
  node [
    id 66
    label "dylatometria"
  ]
  node [
    id 67
    label "elektryka"
  ]
  node [
    id 68
    label "elektrokinetyka"
  ]
  node [
    id 69
    label "fizyka_teoretyczna"
  ]
  node [
    id 70
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 71
    label "dozymetria"
  ]
  node [
    id 72
    label "teoria_p&#243;l_kwantowych"
  ]
  node [
    id 73
    label "elektrodynamika"
  ]
  node [
    id 74
    label "fizyka_medyczna"
  ]
  node [
    id 75
    label "fizyka_atomowa"
  ]
  node [
    id 76
    label "fizyka_molekularna"
  ]
  node [
    id 77
    label "elektromagnetyzm"
  ]
  node [
    id 78
    label "pr&#243;&#380;nia"
  ]
  node [
    id 79
    label "fizyka_statystyczna"
  ]
  node [
    id 80
    label "kriofizyka"
  ]
  node [
    id 81
    label "mikrofizyka"
  ]
  node [
    id 82
    label "fiza"
  ]
  node [
    id 83
    label "elektrostatyka"
  ]
  node [
    id 84
    label "interferometria"
  ]
  node [
    id 85
    label "agrofizyka"
  ]
  node [
    id 86
    label "teoria_pola"
  ]
  node [
    id 87
    label "elektryczno&#347;&#263;"
  ]
  node [
    id 88
    label "kierunek"
  ]
  node [
    id 89
    label "fizyka_plazmy"
  ]
  node [
    id 90
    label "spektroskopia"
  ]
  node [
    id 91
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 92
    label "mechanika"
  ]
  node [
    id 93
    label "chemia_powierzchni"
  ]
  node [
    id 94
    label "geofizyka"
  ]
  node [
    id 95
    label "nauka_przyrodnicza"
  ]
  node [
    id 96
    label "rentgenologia"
  ]
  node [
    id 97
    label "klasycznie"
  ]
  node [
    id 98
    label "tradycyjny"
  ]
  node [
    id 99
    label "nieklasyczny"
  ]
  node [
    id 100
    label "klasyczno"
  ]
  node [
    id 101
    label "zwyczajny"
  ]
  node [
    id 102
    label "normatywny"
  ]
  node [
    id 103
    label "modelowy"
  ]
  node [
    id 104
    label "staro&#380;ytny"
  ]
  node [
    id 105
    label "typowy"
  ]
  node [
    id 106
    label "use"
  ]
  node [
    id 107
    label "zrobienie"
  ]
  node [
    id 108
    label "stosowanie"
  ]
  node [
    id 109
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 110
    label "funkcja"
  ]
  node [
    id 111
    label "cel"
  ]
  node [
    id 112
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 113
    label "balsamowa&#263;"
  ]
  node [
    id 114
    label "Komitet_Region&#243;w"
  ]
  node [
    id 115
    label "pochowa&#263;"
  ]
  node [
    id 116
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 117
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 118
    label "odwodnienie"
  ]
  node [
    id 119
    label "otworzenie"
  ]
  node [
    id 120
    label "zabalsamowanie"
  ]
  node [
    id 121
    label "tanatoplastyk"
  ]
  node [
    id 122
    label "biorytm"
  ]
  node [
    id 123
    label "istota_&#380;ywa"
  ]
  node [
    id 124
    label "zabalsamowa&#263;"
  ]
  node [
    id 125
    label "pogrzeb"
  ]
  node [
    id 126
    label "otwieranie"
  ]
  node [
    id 127
    label "tanatoplastyka"
  ]
  node [
    id 128
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 129
    label "l&#281;d&#378;wie"
  ]
  node [
    id 130
    label "sk&#243;ra"
  ]
  node [
    id 131
    label "nieumar&#322;y"
  ]
  node [
    id 132
    label "unerwienie"
  ]
  node [
    id 133
    label "sekcja"
  ]
  node [
    id 134
    label "ow&#322;osienie"
  ]
  node [
    id 135
    label "odwadnia&#263;"
  ]
  node [
    id 136
    label "zesp&#243;&#322;"
  ]
  node [
    id 137
    label "ekshumowa&#263;"
  ]
  node [
    id 138
    label "jednostka_organizacyjna"
  ]
  node [
    id 139
    label "ekshumowanie"
  ]
  node [
    id 140
    label "pochowanie"
  ]
  node [
    id 141
    label "kremacja"
  ]
  node [
    id 142
    label "otworzy&#263;"
  ]
  node [
    id 143
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 144
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 145
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 146
    label "balsamowanie"
  ]
  node [
    id 147
    label "Izba_Konsyliarska"
  ]
  node [
    id 148
    label "odwadnianie"
  ]
  node [
    id 149
    label "uk&#322;ad"
  ]
  node [
    id 150
    label "cz&#322;onek"
  ]
  node [
    id 151
    label "miejsce"
  ]
  node [
    id 152
    label "szkielet"
  ]
  node [
    id 153
    label "odwodni&#263;"
  ]
  node [
    id 154
    label "ty&#322;"
  ]
  node [
    id 155
    label "materia"
  ]
  node [
    id 156
    label "zbi&#243;r"
  ]
  node [
    id 157
    label "temperatura"
  ]
  node [
    id 158
    label "staw"
  ]
  node [
    id 159
    label "mi&#281;so"
  ]
  node [
    id 160
    label "prz&#243;d"
  ]
  node [
    id 161
    label "otwiera&#263;"
  ]
  node [
    id 162
    label "p&#322;aszczyzna"
  ]
  node [
    id 163
    label "render"
  ]
  node [
    id 164
    label "zmienia&#263;"
  ]
  node [
    id 165
    label "zestaw"
  ]
  node [
    id 166
    label "train"
  ]
  node [
    id 167
    label "uk&#322;ada&#263;"
  ]
  node [
    id 168
    label "dzieli&#263;"
  ]
  node [
    id 169
    label "set"
  ]
  node [
    id 170
    label "przywraca&#263;"
  ]
  node [
    id 171
    label "dawa&#263;"
  ]
  node [
    id 172
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 173
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 174
    label "zbiera&#263;"
  ]
  node [
    id 175
    label "convey"
  ]
  node [
    id 176
    label "opracowywa&#263;"
  ]
  node [
    id 177
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 178
    label "publicize"
  ]
  node [
    id 179
    label "przekazywa&#263;"
  ]
  node [
    id 180
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 181
    label "scala&#263;"
  ]
  node [
    id 182
    label "oddawa&#263;"
  ]
  node [
    id 183
    label "liczba"
  ]
  node [
    id 184
    label "elektron"
  ]
  node [
    id 185
    label "rdze&#324;_atomowy"
  ]
  node [
    id 186
    label "diadochia"
  ]
  node [
    id 187
    label "cz&#261;steczka"
  ]
  node [
    id 188
    label "masa_atomowa"
  ]
  node [
    id 189
    label "pierwiastek"
  ]
  node [
    id 190
    label "cz&#261;stka_elementarna"
  ]
  node [
    id 191
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 192
    label "j&#261;dro_atomowe"
  ]
  node [
    id 193
    label "mikrokosmos"
  ]
  node [
    id 194
    label "liczba_atomowa"
  ]
  node [
    id 195
    label "pow&#322;oka_elektronowa"
  ]
  node [
    id 196
    label "si&#281;ga&#263;"
  ]
  node [
    id 197
    label "trwa&#263;"
  ]
  node [
    id 198
    label "obecno&#347;&#263;"
  ]
  node [
    id 199
    label "stan"
  ]
  node [
    id 200
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 201
    label "stand"
  ]
  node [
    id 202
    label "mie&#263;_miejsce"
  ]
  node [
    id 203
    label "uczestniczy&#263;"
  ]
  node [
    id 204
    label "chodzi&#263;"
  ]
  node [
    id 205
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 206
    label "equal"
  ]
  node [
    id 207
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 208
    label "work"
  ]
  node [
    id 209
    label "robi&#263;"
  ]
  node [
    id 210
    label "muzyka"
  ]
  node [
    id 211
    label "rola"
  ]
  node [
    id 212
    label "create"
  ]
  node [
    id 213
    label "wytwarza&#263;"
  ]
  node [
    id 214
    label "praca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
]
