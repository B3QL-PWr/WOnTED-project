graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.0253164556962027
  density 0.02596559558584875
  graphCliqueNumber 3
  node [
    id 0
    label "koniec"
    origin "text"
  ]
  node [
    id 1
    label "g&#322;od&#243;wka"
    origin "text"
  ]
  node [
    id 2
    label "protest"
    origin "text"
  ]
  node [
    id 3
    label "gda&#324;skie"
    origin "text"
  ]
  node [
    id 4
    label "listonosz"
    origin "text"
  ]
  node [
    id 5
    label "ozz"
    origin "text"
  ]
  node [
    id 6
    label "inicjatywa"
    origin "text"
  ]
  node [
    id 7
    label "pracowniczy"
    origin "text"
  ]
  node [
    id 8
    label "&#347;roda"
    origin "text"
  ]
  node [
    id 9
    label "g&#322;odowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "jeszcze"
    origin "text"
  ]
  node [
    id 11
    label "dwa"
    origin "text"
  ]
  node [
    id 12
    label "dor&#281;czyciel"
    origin "text"
  ]
  node [
    id 13
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 14
    label "zako&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 15
    label "si&#281;"
    origin "text"
  ]
  node [
    id 16
    label "ca&#322;kowity"
    origin "text"
  ]
  node [
    id 17
    label "sukces"
    origin "text"
  ]
  node [
    id 18
    label "defenestracja"
  ]
  node [
    id 19
    label "szereg"
  ]
  node [
    id 20
    label "dzia&#322;anie"
  ]
  node [
    id 21
    label "miejsce"
  ]
  node [
    id 22
    label "ostatnie_podrygi"
  ]
  node [
    id 23
    label "kres"
  ]
  node [
    id 24
    label "agonia"
  ]
  node [
    id 25
    label "visitation"
  ]
  node [
    id 26
    label "szeol"
  ]
  node [
    id 27
    label "mogi&#322;a"
  ]
  node [
    id 28
    label "chwila"
  ]
  node [
    id 29
    label "wydarzenie"
  ]
  node [
    id 30
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 31
    label "pogrzebanie"
  ]
  node [
    id 32
    label "punkt"
  ]
  node [
    id 33
    label "&#380;a&#322;oba"
  ]
  node [
    id 34
    label "zabicie"
  ]
  node [
    id 35
    label "kres_&#380;ycia"
  ]
  node [
    id 36
    label "strajk"
  ]
  node [
    id 37
    label "dieta"
  ]
  node [
    id 38
    label "reakcja"
  ]
  node [
    id 39
    label "protestacja"
  ]
  node [
    id 40
    label "czerwona_kartka"
  ]
  node [
    id 41
    label "pocztylion"
  ]
  node [
    id 42
    label "pocztowiec"
  ]
  node [
    id 43
    label "plan"
  ]
  node [
    id 44
    label "propozycja"
  ]
  node [
    id 45
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 46
    label "Popielec"
  ]
  node [
    id 47
    label "dzie&#324;_powszedni"
  ]
  node [
    id 48
    label "tydzie&#324;"
  ]
  node [
    id 49
    label "cierpie&#263;"
  ]
  node [
    id 50
    label "hunger"
  ]
  node [
    id 51
    label "biedowa&#263;"
  ]
  node [
    id 52
    label "ci&#261;gle"
  ]
  node [
    id 53
    label "dostawca"
  ]
  node [
    id 54
    label "przekaziciel"
  ]
  node [
    id 55
    label "communicate"
  ]
  node [
    id 56
    label "cause"
  ]
  node [
    id 57
    label "zrezygnowa&#263;"
  ]
  node [
    id 58
    label "wytworzy&#263;"
  ]
  node [
    id 59
    label "przesta&#263;"
  ]
  node [
    id 60
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 61
    label "dispose"
  ]
  node [
    id 62
    label "zrobi&#263;"
  ]
  node [
    id 63
    label "w_pizdu"
  ]
  node [
    id 64
    label "&#322;&#261;czny"
  ]
  node [
    id 65
    label "og&#243;lnie"
  ]
  node [
    id 66
    label "ca&#322;y"
  ]
  node [
    id 67
    label "pe&#322;ny"
  ]
  node [
    id 68
    label "zupe&#322;nie"
  ]
  node [
    id 69
    label "kompletnie"
  ]
  node [
    id 70
    label "success"
  ]
  node [
    id 71
    label "passa"
  ]
  node [
    id 72
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 73
    label "kobieta_sukcesu"
  ]
  node [
    id 74
    label "rezultat"
  ]
  node [
    id 75
    label "OZZ"
  ]
  node [
    id 76
    label "poczta"
  ]
  node [
    id 77
    label "polski"
  ]
  node [
    id 78
    label "dyrekcja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 54
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 55
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 57
  ]
  edge [
    source 14
    target 58
  ]
  edge [
    source 14
    target 59
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 61
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 63
  ]
  edge [
    source 16
    target 64
  ]
  edge [
    source 16
    target 65
  ]
  edge [
    source 16
    target 66
  ]
  edge [
    source 16
    target 67
  ]
  edge [
    source 16
    target 68
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 17
    target 70
  ]
  edge [
    source 17
    target 71
  ]
  edge [
    source 17
    target 72
  ]
  edge [
    source 17
    target 73
  ]
  edge [
    source 17
    target 74
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 78
  ]
  edge [
    source 77
    target 78
  ]
]
