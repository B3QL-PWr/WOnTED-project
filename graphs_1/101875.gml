graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.8095238095238095
  density 0.09047619047619047
  graphCliqueNumber 2
  node [
    id 0
    label "zmiana"
    origin "text"
  ]
  node [
    id 1
    label "z&#281;batka"
    origin "text"
  ]
  node [
    id 2
    label "rustlerze"
    origin "text"
  ]
  node [
    id 3
    label "anatomopatolog"
  ]
  node [
    id 4
    label "rewizja"
  ]
  node [
    id 5
    label "oznaka"
  ]
  node [
    id 6
    label "czas"
  ]
  node [
    id 7
    label "ferment"
  ]
  node [
    id 8
    label "komplet"
  ]
  node [
    id 9
    label "tura"
  ]
  node [
    id 10
    label "amendment"
  ]
  node [
    id 11
    label "zmianka"
  ]
  node [
    id 12
    label "odmienianie"
  ]
  node [
    id 13
    label "passage"
  ]
  node [
    id 14
    label "zjawisko"
  ]
  node [
    id 15
    label "change"
  ]
  node [
    id 16
    label "praca"
  ]
  node [
    id 17
    label "podci&#261;gnik_z&#281;batkowy"
  ]
  node [
    id 18
    label "przek&#322;adnia"
  ]
  node [
    id 19
    label "robinson"
  ]
  node [
    id 20
    label "Racing"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 19
    target 20
  ]
]
