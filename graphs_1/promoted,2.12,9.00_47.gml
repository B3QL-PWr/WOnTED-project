graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.0253164556962027
  density 0.02596559558584875
  graphCliqueNumber 3
  node [
    id 0
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 1
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "urodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "polska"
    origin "text"
  ]
  node [
    id 6
    label "tys"
    origin "text"
  ]
  node [
    id 7
    label "dziecko"
    origin "text"
  ]
  node [
    id 8
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 9
    label "&#347;wie&#380;y"
    origin "text"
  ]
  node [
    id 10
    label "dana"
    origin "text"
  ]
  node [
    id 11
    label "gus"
    origin "text"
  ]
  node [
    id 12
    label "Nowy_Rok"
  ]
  node [
    id 13
    label "miesi&#261;c"
  ]
  node [
    id 14
    label "stulecie"
  ]
  node [
    id 15
    label "kalendarz"
  ]
  node [
    id 16
    label "czas"
  ]
  node [
    id 17
    label "pora_roku"
  ]
  node [
    id 18
    label "cykl_astronomiczny"
  ]
  node [
    id 19
    label "p&#243;&#322;rocze"
  ]
  node [
    id 20
    label "grupa"
  ]
  node [
    id 21
    label "kwarta&#322;"
  ]
  node [
    id 22
    label "kurs"
  ]
  node [
    id 23
    label "jubileusz"
  ]
  node [
    id 24
    label "lata"
  ]
  node [
    id 25
    label "martwy_sezon"
  ]
  node [
    id 26
    label "narodzi&#263;"
  ]
  node [
    id 27
    label "zlec"
  ]
  node [
    id 28
    label "engender"
  ]
  node [
    id 29
    label "powi&#263;"
  ]
  node [
    id 30
    label "zrobi&#263;"
  ]
  node [
    id 31
    label "porodzi&#263;"
  ]
  node [
    id 32
    label "cz&#322;owiek"
  ]
  node [
    id 33
    label "potomstwo"
  ]
  node [
    id 34
    label "organizm"
  ]
  node [
    id 35
    label "sraluch"
  ]
  node [
    id 36
    label "utulanie"
  ]
  node [
    id 37
    label "pediatra"
  ]
  node [
    id 38
    label "dzieciarnia"
  ]
  node [
    id 39
    label "m&#322;odziak"
  ]
  node [
    id 40
    label "dzieciak"
  ]
  node [
    id 41
    label "utula&#263;"
  ]
  node [
    id 42
    label "potomek"
  ]
  node [
    id 43
    label "entliczek-pentliczek"
  ]
  node [
    id 44
    label "pedofil"
  ]
  node [
    id 45
    label "m&#322;odzik"
  ]
  node [
    id 46
    label "cz&#322;owieczek"
  ]
  node [
    id 47
    label "zwierz&#281;"
  ]
  node [
    id 48
    label "niepe&#322;noletni"
  ]
  node [
    id 49
    label "fledgling"
  ]
  node [
    id 50
    label "utuli&#263;"
  ]
  node [
    id 51
    label "utulenie"
  ]
  node [
    id 52
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 53
    label "rise"
  ]
  node [
    id 54
    label "appear"
  ]
  node [
    id 55
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 56
    label "orze&#378;wiaj&#261;co"
  ]
  node [
    id 57
    label "jasny"
  ]
  node [
    id 58
    label "&#347;wie&#380;o"
  ]
  node [
    id 59
    label "orze&#378;wienie"
  ]
  node [
    id 60
    label "o&#380;ywczy"
  ]
  node [
    id 61
    label "soczysty"
  ]
  node [
    id 62
    label "inny"
  ]
  node [
    id 63
    label "nowotny"
  ]
  node [
    id 64
    label "przyjemny"
  ]
  node [
    id 65
    label "zdrowy"
  ]
  node [
    id 66
    label "&#380;ywy"
  ]
  node [
    id 67
    label "czysty"
  ]
  node [
    id 68
    label "orze&#378;wianie"
  ]
  node [
    id 69
    label "oryginalnie"
  ]
  node [
    id 70
    label "m&#322;ody"
  ]
  node [
    id 71
    label "energiczny"
  ]
  node [
    id 72
    label "nowy"
  ]
  node [
    id 73
    label "dobry"
  ]
  node [
    id 74
    label "rze&#347;ki"
  ]
  node [
    id 75
    label "surowy"
  ]
  node [
    id 76
    label "dar"
  ]
  node [
    id 77
    label "cnota"
  ]
  node [
    id 78
    label "buddyzm"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
]
