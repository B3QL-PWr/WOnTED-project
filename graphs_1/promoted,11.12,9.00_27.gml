graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9574468085106382
  density 0.0425531914893617
  graphCliqueNumber 2
  node [
    id 0
    label "rozstrzygni&#281;cie"
    origin "text"
  ]
  node [
    id 1
    label "plebiscyt"
    origin "text"
  ]
  node [
    id 2
    label "m&#322;odzie&#380;owy"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;owo"
    origin "text"
  ]
  node [
    id 4
    label "rok"
    origin "text"
  ]
  node [
    id 5
    label "xdd"
    origin "text"
  ]
  node [
    id 6
    label "zdecydowanie"
  ]
  node [
    id 7
    label "adjudication"
  ]
  node [
    id 8
    label "oddzia&#322;anie"
  ]
  node [
    id 9
    label "resoluteness"
  ]
  node [
    id 10
    label "decyzja"
  ]
  node [
    id 11
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 12
    label "rezultat"
  ]
  node [
    id 13
    label "konkurs"
  ]
  node [
    id 14
    label "g&#322;osowanie"
  ]
  node [
    id 15
    label "m&#322;odzie&#380;owo"
  ]
  node [
    id 16
    label "obietnica"
  ]
  node [
    id 17
    label "bit"
  ]
  node [
    id 18
    label "s&#322;ownictwo"
  ]
  node [
    id 19
    label "jednostka_leksykalna"
  ]
  node [
    id 20
    label "pisanie_si&#281;"
  ]
  node [
    id 21
    label "wykrzyknik"
  ]
  node [
    id 22
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 23
    label "pole_semantyczne"
  ]
  node [
    id 24
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 25
    label "komunikat"
  ]
  node [
    id 26
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 27
    label "wypowiedzenie"
  ]
  node [
    id 28
    label "nag&#322;os"
  ]
  node [
    id 29
    label "wordnet"
  ]
  node [
    id 30
    label "morfem"
  ]
  node [
    id 31
    label "czasownik"
  ]
  node [
    id 32
    label "wyg&#322;os"
  ]
  node [
    id 33
    label "jednostka_informacji"
  ]
  node [
    id 34
    label "stulecie"
  ]
  node [
    id 35
    label "kalendarz"
  ]
  node [
    id 36
    label "czas"
  ]
  node [
    id 37
    label "pora_roku"
  ]
  node [
    id 38
    label "cykl_astronomiczny"
  ]
  node [
    id 39
    label "p&#243;&#322;rocze"
  ]
  node [
    id 40
    label "grupa"
  ]
  node [
    id 41
    label "kwarta&#322;"
  ]
  node [
    id 42
    label "kurs"
  ]
  node [
    id 43
    label "jubileusz"
  ]
  node [
    id 44
    label "miesi&#261;c"
  ]
  node [
    id 45
    label "lata"
  ]
  node [
    id 46
    label "martwy_sezon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
]
