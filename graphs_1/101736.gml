graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.9
  density 0.1
  graphCliqueNumber 2
  node [
    id 0
    label "janusz"
    origin "text"
  ]
  node [
    id 1
    label "niemcewicz"
    origin "text"
  ]
  node [
    id 2
    label "sprawozdawca"
    origin "text"
  ]
  node [
    id 3
    label "protokolant"
    origin "text"
  ]
  node [
    id 4
    label "krzysztof"
    origin "text"
  ]
  node [
    id 5
    label "zalecki"
    origin "text"
  ]
  node [
    id 6
    label "Polak"
  ]
  node [
    id 7
    label "gra&#380;yna"
  ]
  node [
    id 8
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 9
    label "pata&#322;ach"
  ]
  node [
    id 10
    label "cwaniaczek"
  ]
  node [
    id 11
    label "kibic"
  ]
  node [
    id 12
    label "przeci&#281;tniak"
  ]
  node [
    id 13
    label "tatusiek"
  ]
  node [
    id 14
    label "facet"
  ]
  node [
    id 15
    label "przekaziciel"
  ]
  node [
    id 16
    label "&#347;wiadek"
  ]
  node [
    id 17
    label "dziennikarz"
  ]
  node [
    id 18
    label "sekretarz"
  ]
  node [
    id 19
    label "urz&#281;dnik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 5
  ]
]
