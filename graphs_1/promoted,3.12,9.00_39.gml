graph [
  maxDegree 46
  minDegree 1
  meanDegree 2
  density 0.023809523809523808
  graphCliqueNumber 2
  node [
    id 0
    label "uczy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "historia"
    origin "text"
  ]
  node [
    id 3
    label "wyci&#261;ga&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wniosek"
    origin "text"
  ]
  node [
    id 5
    label "zapoznawa&#263;"
  ]
  node [
    id 6
    label "teach"
  ]
  node [
    id 7
    label "train"
  ]
  node [
    id 8
    label "rozwija&#263;"
  ]
  node [
    id 9
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 10
    label "pracowa&#263;"
  ]
  node [
    id 11
    label "szkoli&#263;"
  ]
  node [
    id 12
    label "report"
  ]
  node [
    id 13
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 14
    label "wypowied&#378;"
  ]
  node [
    id 15
    label "neografia"
  ]
  node [
    id 16
    label "przedmiot"
  ]
  node [
    id 17
    label "papirologia"
  ]
  node [
    id 18
    label "historia_gospodarcza"
  ]
  node [
    id 19
    label "przebiec"
  ]
  node [
    id 20
    label "hista"
  ]
  node [
    id 21
    label "nauka_humanistyczna"
  ]
  node [
    id 22
    label "filigranistyka"
  ]
  node [
    id 23
    label "dyplomatyka"
  ]
  node [
    id 24
    label "annalistyka"
  ]
  node [
    id 25
    label "historyka"
  ]
  node [
    id 26
    label "heraldyka"
  ]
  node [
    id 27
    label "fabu&#322;a"
  ]
  node [
    id 28
    label "muzealnictwo"
  ]
  node [
    id 29
    label "varsavianistyka"
  ]
  node [
    id 30
    label "mediewistyka"
  ]
  node [
    id 31
    label "prezentyzm"
  ]
  node [
    id 32
    label "przebiegni&#281;cie"
  ]
  node [
    id 33
    label "charakter"
  ]
  node [
    id 34
    label "paleografia"
  ]
  node [
    id 35
    label "genealogia"
  ]
  node [
    id 36
    label "czynno&#347;&#263;"
  ]
  node [
    id 37
    label "prozopografia"
  ]
  node [
    id 38
    label "motyw"
  ]
  node [
    id 39
    label "nautologia"
  ]
  node [
    id 40
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 41
    label "epoka"
  ]
  node [
    id 42
    label "numizmatyka"
  ]
  node [
    id 43
    label "ruralistyka"
  ]
  node [
    id 44
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 45
    label "epigrafika"
  ]
  node [
    id 46
    label "historiografia"
  ]
  node [
    id 47
    label "bizantynistyka"
  ]
  node [
    id 48
    label "weksylologia"
  ]
  node [
    id 49
    label "kierunek"
  ]
  node [
    id 50
    label "ikonografia"
  ]
  node [
    id 51
    label "chronologia"
  ]
  node [
    id 52
    label "archiwistyka"
  ]
  node [
    id 53
    label "sfragistyka"
  ]
  node [
    id 54
    label "zabytkoznawstwo"
  ]
  node [
    id 55
    label "historia_sztuki"
  ]
  node [
    id 56
    label "perpetrate"
  ]
  node [
    id 57
    label "prostowa&#263;"
  ]
  node [
    id 58
    label "zdobywa&#263;_podst&#281;pem"
  ]
  node [
    id 59
    label "zarabia&#263;"
  ]
  node [
    id 60
    label "develop"
  ]
  node [
    id 61
    label "przypomina&#263;"
  ]
  node [
    id 62
    label "ocala&#263;"
  ]
  node [
    id 63
    label "obrysowywa&#263;"
  ]
  node [
    id 64
    label "osi&#261;ga&#263;"
  ]
  node [
    id 65
    label "pr&#281;dko&#347;&#263;"
  ]
  node [
    id 66
    label "wch&#322;ania&#263;"
  ]
  node [
    id 67
    label "rozci&#261;ga&#263;"
  ]
  node [
    id 68
    label "widen"
  ]
  node [
    id 69
    label "przemieszcza&#263;"
  ]
  node [
    id 70
    label "dane"
  ]
  node [
    id 71
    label "nak&#322;ania&#263;"
  ]
  node [
    id 72
    label "zmusza&#263;"
  ]
  node [
    id 73
    label "&#347;piewa&#263;"
  ]
  node [
    id 74
    label "zabiera&#263;"
  ]
  node [
    id 75
    label "wydostawa&#263;"
  ]
  node [
    id 76
    label "expand"
  ]
  node [
    id 77
    label "wy&#322;udza&#263;"
  ]
  node [
    id 78
    label "twierdzenie"
  ]
  node [
    id 79
    label "my&#347;l"
  ]
  node [
    id 80
    label "wnioskowanie"
  ]
  node [
    id 81
    label "propozycja"
  ]
  node [
    id 82
    label "motion"
  ]
  node [
    id 83
    label "pismo"
  ]
  node [
    id 84
    label "prayer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
]
