graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.2727272727272727
  density 0.12727272727272726
  graphCliqueNumber 3
  node [
    id 0
    label "jonas"
    origin "text"
  ]
  node [
    id 1
    label "gunnarsson"
    origin "text"
  ]
  node [
    id 2
    label "Jonas"
  ]
  node [
    id 3
    label "Gunnarsson"
  ]
  node [
    id 4
    label "san"
  ]
  node [
    id 5
    label "candida"
  ]
  node [
    id 6
    label "puchar"
  ]
  node [
    id 7
    label "&#347;wiat"
  ]
  node [
    id 8
    label "ma&#322;y"
  ]
  node [
    id 9
    label "kryszta&#322;owy"
  ]
  node [
    id 10
    label "kuli&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
]
