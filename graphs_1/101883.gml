graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.0745341614906834
  density 0.01296583850931677
  graphCliqueNumber 4
  node [
    id 0
    label "wreszcie"
    origin "text"
  ]
  node [
    id 1
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "po&#347;ciga&#263;"
    origin "text"
  ]
  node [
    id 4
    label "spotkanie"
    origin "text"
  ]
  node [
    id 5
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "anda"
    origin "text"
  ]
  node [
    id 7
    label "model"
    origin "text"
  ]
  node [
    id 8
    label "kyosho"
    origin "text"
  ]
  node [
    id 9
    label "laser"
    origin "text"
  ]
  node [
    id 10
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "starowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "eliminacja"
    origin "text"
  ]
  node [
    id 13
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 14
    label "polska"
    origin "text"
  ]
  node [
    id 15
    label "klasa"
    origin "text"
  ]
  node [
    id 16
    label "eta"
    origin "text"
  ]
  node [
    id 17
    label "szybko"
    origin "text"
  ]
  node [
    id 18
    label "wje&#378;dzi&#322;"
    origin "text"
  ]
  node [
    id 19
    label "tora"
    origin "text"
  ]
  node [
    id 20
    label "m&#243;cby&#263;"
    origin "text"
  ]
  node [
    id 21
    label "rozpocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 22
    label "pi&#281;ciominutowy"
    origin "text"
  ]
  node [
    id 23
    label "bieg"
    origin "text"
  ]
  node [
    id 24
    label "w&#380;dy"
  ]
  node [
    id 25
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 26
    label "represent"
  ]
  node [
    id 27
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 28
    label "po&#380;egnanie"
  ]
  node [
    id 29
    label "spowodowanie"
  ]
  node [
    id 30
    label "znalezienie"
  ]
  node [
    id 31
    label "znajomy"
  ]
  node [
    id 32
    label "doznanie"
  ]
  node [
    id 33
    label "employment"
  ]
  node [
    id 34
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 35
    label "gather"
  ]
  node [
    id 36
    label "powitanie"
  ]
  node [
    id 37
    label "spotykanie"
  ]
  node [
    id 38
    label "wydarzenie"
  ]
  node [
    id 39
    label "gathering"
  ]
  node [
    id 40
    label "spotkanie_si&#281;"
  ]
  node [
    id 41
    label "zdarzenie_si&#281;"
  ]
  node [
    id 42
    label "match"
  ]
  node [
    id 43
    label "zawarcie"
  ]
  node [
    id 44
    label "typ"
  ]
  node [
    id 45
    label "cz&#322;owiek"
  ]
  node [
    id 46
    label "pozowa&#263;"
  ]
  node [
    id 47
    label "ideal"
  ]
  node [
    id 48
    label "matryca"
  ]
  node [
    id 49
    label "imitacja"
  ]
  node [
    id 50
    label "ruch"
  ]
  node [
    id 51
    label "motif"
  ]
  node [
    id 52
    label "pozowanie"
  ]
  node [
    id 53
    label "wz&#243;r"
  ]
  node [
    id 54
    label "miniatura"
  ]
  node [
    id 55
    label "prezenter"
  ]
  node [
    id 56
    label "facet"
  ]
  node [
    id 57
    label "orygina&#322;"
  ]
  node [
    id 58
    label "mildew"
  ]
  node [
    id 59
    label "spos&#243;b"
  ]
  node [
    id 60
    label "zi&#243;&#322;ko"
  ]
  node [
    id 61
    label "adaptation"
  ]
  node [
    id 62
    label "kriostat"
  ]
  node [
    id 63
    label "lidar"
  ]
  node [
    id 64
    label "urz&#261;dzenie"
  ]
  node [
    id 65
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 66
    label "wyb&#243;r"
  ]
  node [
    id 67
    label "reakcja_chemiczna"
  ]
  node [
    id 68
    label "fire"
  ]
  node [
    id 69
    label "choice"
  ]
  node [
    id 70
    label "retirement"
  ]
  node [
    id 71
    label "championship"
  ]
  node [
    id 72
    label "Formu&#322;a_1"
  ]
  node [
    id 73
    label "zawody"
  ]
  node [
    id 74
    label "warstwa"
  ]
  node [
    id 75
    label "znak_jako&#347;ci"
  ]
  node [
    id 76
    label "przedmiot"
  ]
  node [
    id 77
    label "przepisa&#263;"
  ]
  node [
    id 78
    label "grupa"
  ]
  node [
    id 79
    label "pomoc"
  ]
  node [
    id 80
    label "arrangement"
  ]
  node [
    id 81
    label "wagon"
  ]
  node [
    id 82
    label "form"
  ]
  node [
    id 83
    label "zaleta"
  ]
  node [
    id 84
    label "poziom"
  ]
  node [
    id 85
    label "dziennik_lekcyjny"
  ]
  node [
    id 86
    label "&#347;rodowisko"
  ]
  node [
    id 87
    label "atak"
  ]
  node [
    id 88
    label "przepisanie"
  ]
  node [
    id 89
    label "szko&#322;a"
  ]
  node [
    id 90
    label "class"
  ]
  node [
    id 91
    label "organizacja"
  ]
  node [
    id 92
    label "obrona"
  ]
  node [
    id 93
    label "type"
  ]
  node [
    id 94
    label "promocja"
  ]
  node [
    id 95
    label "&#322;awka"
  ]
  node [
    id 96
    label "kurs"
  ]
  node [
    id 97
    label "botanika"
  ]
  node [
    id 98
    label "sala"
  ]
  node [
    id 99
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 100
    label "gromada"
  ]
  node [
    id 101
    label "obiekt"
  ]
  node [
    id 102
    label "Ekwici"
  ]
  node [
    id 103
    label "fakcja"
  ]
  node [
    id 104
    label "tablica"
  ]
  node [
    id 105
    label "programowanie_obiektowe"
  ]
  node [
    id 106
    label "wykrzyknik"
  ]
  node [
    id 107
    label "jednostka_systematyczna"
  ]
  node [
    id 108
    label "mecz_mistrzowski"
  ]
  node [
    id 109
    label "zbi&#243;r"
  ]
  node [
    id 110
    label "jako&#347;&#263;"
  ]
  node [
    id 111
    label "rezerwa"
  ]
  node [
    id 112
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 113
    label "quicker"
  ]
  node [
    id 114
    label "promptly"
  ]
  node [
    id 115
    label "bezpo&#347;rednio"
  ]
  node [
    id 116
    label "quickest"
  ]
  node [
    id 117
    label "sprawnie"
  ]
  node [
    id 118
    label "dynamicznie"
  ]
  node [
    id 119
    label "szybciej"
  ]
  node [
    id 120
    label "prosto"
  ]
  node [
    id 121
    label "szybciochem"
  ]
  node [
    id 122
    label "szybki"
  ]
  node [
    id 123
    label "zw&#243;j"
  ]
  node [
    id 124
    label "Tora"
  ]
  node [
    id 125
    label "cause"
  ]
  node [
    id 126
    label "zacz&#261;&#263;"
  ]
  node [
    id 127
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 128
    label "do"
  ]
  node [
    id 129
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 130
    label "zrobi&#263;"
  ]
  node [
    id 131
    label "kilkuminutowy"
  ]
  node [
    id 132
    label "wy&#347;cig"
  ]
  node [
    id 133
    label "parametr"
  ]
  node [
    id 134
    label "ciek_wodny"
  ]
  node [
    id 135
    label "roll"
  ]
  node [
    id 136
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 137
    label "przedbieg"
  ]
  node [
    id 138
    label "konkurencja"
  ]
  node [
    id 139
    label "tryb"
  ]
  node [
    id 140
    label "bystrzyca"
  ]
  node [
    id 141
    label "pr&#261;d"
  ]
  node [
    id 142
    label "pozycja"
  ]
  node [
    id 143
    label "linia"
  ]
  node [
    id 144
    label "procedura"
  ]
  node [
    id 145
    label "czo&#322;&#243;wka"
  ]
  node [
    id 146
    label "syfon"
  ]
  node [
    id 147
    label "kierunek"
  ]
  node [
    id 148
    label "cycle"
  ]
  node [
    id 149
    label "proces"
  ]
  node [
    id 150
    label "d&#261;&#380;enie"
  ]
  node [
    id 151
    label "Kyosho"
  ]
  node [
    id 152
    label "ZX"
  ]
  node [
    id 153
    label "5"
  ]
  node [
    id 154
    label "mistrzostwo"
  ]
  node [
    id 155
    label "polski"
  ]
  node [
    id 156
    label "Lightningiem"
  ]
  node [
    id 157
    label "stadium"
  ]
  node [
    id 158
    label "10"
  ]
  node [
    id 159
    label "Rustlerem"
  ]
  node [
    id 160
    label "VXL"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 44
  ]
  edge [
    source 15
    target 74
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 15
    target 81
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 15
    target 84
  ]
  edge [
    source 15
    target 85
  ]
  edge [
    source 15
    target 86
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 125
  ]
  edge [
    source 21
    target 126
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 21
    target 129
  ]
  edge [
    source 21
    target 130
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 23
    target 132
  ]
  edge [
    source 23
    target 133
  ]
  edge [
    source 23
    target 134
  ]
  edge [
    source 23
    target 135
  ]
  edge [
    source 23
    target 136
  ]
  edge [
    source 23
    target 137
  ]
  edge [
    source 23
    target 138
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 23
    target 140
  ]
  edge [
    source 23
    target 141
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 50
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 96
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 151
    target 153
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 156
    target 158
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 159
    target 160
  ]
]
