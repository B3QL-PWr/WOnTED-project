graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.0454545454545454
  density 0.04756871035940803
  graphCliqueNumber 3
  node [
    id 0
    label "subiektywny"
    origin "text"
  ]
  node [
    id 1
    label "przegl&#261;d"
    origin "text"
  ]
  node [
    id 2
    label "kawa"
    origin "text"
  ]
  node [
    id 3
    label "mleko"
    origin "text"
  ]
  node [
    id 4
    label "rogalik"
    origin "text"
  ]
  node [
    id 5
    label "nieobiektywny"
  ]
  node [
    id 6
    label "indywidualny"
  ]
  node [
    id 7
    label "zsubiektywizowanie"
  ]
  node [
    id 8
    label "subiektywizowanie"
  ]
  node [
    id 9
    label "subiektywnie"
  ]
  node [
    id 10
    label "impreza"
  ]
  node [
    id 11
    label "kontrola"
  ]
  node [
    id 12
    label "inspection"
  ]
  node [
    id 13
    label "u&#380;ywka"
  ]
  node [
    id 14
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 15
    label "dripper"
  ]
  node [
    id 16
    label "marzanowate"
  ]
  node [
    id 17
    label "produkt"
  ]
  node [
    id 18
    label "ziarno"
  ]
  node [
    id 19
    label "pestkowiec"
  ]
  node [
    id 20
    label "nap&#243;j"
  ]
  node [
    id 21
    label "jedzenie"
  ]
  node [
    id 22
    label "ro&#347;lina"
  ]
  node [
    id 23
    label "egzotyk"
  ]
  node [
    id 24
    label "porcja"
  ]
  node [
    id 25
    label "kofeina"
  ]
  node [
    id 26
    label "chemex"
  ]
  node [
    id 27
    label "laktoferyna"
  ]
  node [
    id 28
    label "bia&#322;ko"
  ]
  node [
    id 29
    label "warzenie_si&#281;"
  ]
  node [
    id 30
    label "szkopek"
  ]
  node [
    id 31
    label "zawiesina"
  ]
  node [
    id 32
    label "mg&#322;a"
  ]
  node [
    id 33
    label "laktacja"
  ]
  node [
    id 34
    label "laktoza"
  ]
  node [
    id 35
    label "milk"
  ]
  node [
    id 36
    label "nabia&#322;"
  ]
  node [
    id 37
    label "ssa&#263;"
  ]
  node [
    id 38
    label "wydzielina"
  ]
  node [
    id 39
    label "kazeina"
  ]
  node [
    id 40
    label "ryboflawina"
  ]
  node [
    id 41
    label "ciecz"
  ]
  node [
    id 42
    label "Alka"
  ]
  node [
    id 43
    label "Tarkowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 42
    target 43
  ]
]
