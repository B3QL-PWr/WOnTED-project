graph [
  maxDegree 9
  minDegree 1
  meanDegree 2.2
  density 0.11578947368421053
  graphCliqueNumber 4
  node [
    id 0
    label "dziadowy"
    origin "text"
  ]
  node [
    id 1
    label "k&#322;oda"
    origin "text"
  ]
  node [
    id 2
    label "stacja"
    origin "text"
  ]
  node [
    id 3
    label "kolejowy"
    origin "text"
  ]
  node [
    id 4
    label "dziadowsko"
  ]
  node [
    id 5
    label "kiepski"
  ]
  node [
    id 6
    label "pryzma"
  ]
  node [
    id 7
    label "drewno_okr&#261;g&#322;e"
  ]
  node [
    id 8
    label "pie&#324;"
  ]
  node [
    id 9
    label "miejsce"
  ]
  node [
    id 10
    label "instytucja"
  ]
  node [
    id 11
    label "droga_krzy&#380;owa"
  ]
  node [
    id 12
    label "punkt"
  ]
  node [
    id 13
    label "urz&#261;dzenie"
  ]
  node [
    id 14
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 15
    label "siedziba"
  ]
  node [
    id 16
    label "komunikacyjny"
  ]
  node [
    id 17
    label "linia"
  ]
  node [
    id 18
    label "nr"
  ]
  node [
    id 19
    label "317"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
]
