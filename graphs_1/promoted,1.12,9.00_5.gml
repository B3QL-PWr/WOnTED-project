graph [
  maxDegree 16
  minDegree 1
  meanDegree 2.024390243902439
  density 0.02499247214694369
  graphCliqueNumber 2
  node [
    id 0
    label "trwa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ponad"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "&#347;ledztwo"
    origin "text"
  ]
  node [
    id 4
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "przez"
    origin "text"
  ]
  node [
    id 6
    label "bbc"
    origin "text"
  ]
  node [
    id 7
    label "ujawni&#263;"
    origin "text"
  ]
  node [
    id 8
    label "koszmar"
    origin "text"
  ]
  node [
    id 9
    label "pracownik"
    origin "text"
  ]
  node [
    id 10
    label "sprowadzi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "tajlandia"
    origin "text"
  ]
  node [
    id 12
    label "izraelski"
    origin "text"
  ]
  node [
    id 13
    label "farma"
    origin "text"
  ]
  node [
    id 14
    label "zostawa&#263;"
  ]
  node [
    id 15
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 16
    label "pozostawa&#263;"
  ]
  node [
    id 17
    label "stand"
  ]
  node [
    id 18
    label "adhere"
  ]
  node [
    id 19
    label "istnie&#263;"
  ]
  node [
    id 20
    label "stulecie"
  ]
  node [
    id 21
    label "kalendarz"
  ]
  node [
    id 22
    label "czas"
  ]
  node [
    id 23
    label "pora_roku"
  ]
  node [
    id 24
    label "cykl_astronomiczny"
  ]
  node [
    id 25
    label "p&#243;&#322;rocze"
  ]
  node [
    id 26
    label "grupa"
  ]
  node [
    id 27
    label "kwarta&#322;"
  ]
  node [
    id 28
    label "kurs"
  ]
  node [
    id 29
    label "jubileusz"
  ]
  node [
    id 30
    label "miesi&#261;c"
  ]
  node [
    id 31
    label "lata"
  ]
  node [
    id 32
    label "martwy_sezon"
  ]
  node [
    id 33
    label "detektyw"
  ]
  node [
    id 34
    label "wydarzenie"
  ]
  node [
    id 35
    label "examination"
  ]
  node [
    id 36
    label "pom&#243;c"
  ]
  node [
    id 37
    label "zbudowa&#263;"
  ]
  node [
    id 38
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 39
    label "leave"
  ]
  node [
    id 40
    label "przewie&#347;&#263;"
  ]
  node [
    id 41
    label "wykona&#263;"
  ]
  node [
    id 42
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 43
    label "draw"
  ]
  node [
    id 44
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 45
    label "carry"
  ]
  node [
    id 46
    label "dostrzec"
  ]
  node [
    id 47
    label "denounce"
  ]
  node [
    id 48
    label "discover"
  ]
  node [
    id 49
    label "objawi&#263;"
  ]
  node [
    id 50
    label "poinformowa&#263;"
  ]
  node [
    id 51
    label "udr&#281;ka"
  ]
  node [
    id 52
    label "miscarriage"
  ]
  node [
    id 53
    label "makabra"
  ]
  node [
    id 54
    label "marzenie_senne"
  ]
  node [
    id 55
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 56
    label "cz&#322;owiek"
  ]
  node [
    id 57
    label "delegowa&#263;"
  ]
  node [
    id 58
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 59
    label "pracu&#347;"
  ]
  node [
    id 60
    label "delegowanie"
  ]
  node [
    id 61
    label "r&#281;ka"
  ]
  node [
    id 62
    label "salariat"
  ]
  node [
    id 63
    label "powie&#347;&#263;"
  ]
  node [
    id 64
    label "get"
  ]
  node [
    id 65
    label "ograniczy&#263;"
  ]
  node [
    id 66
    label "pos&#322;a&#263;"
  ]
  node [
    id 67
    label "spowodowa&#263;"
  ]
  node [
    id 68
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 69
    label "become"
  ]
  node [
    id 70
    label "bring"
  ]
  node [
    id 71
    label "u&#322;amek"
  ]
  node [
    id 72
    label "zmieni&#263;"
  ]
  node [
    id 73
    label "wprowadzi&#263;"
  ]
  node [
    id 74
    label "upro&#347;ci&#263;"
  ]
  node [
    id 75
    label "bliskowschodni"
  ]
  node [
    id 76
    label "zachodnioazjatycki"
  ]
  node [
    id 77
    label "moszaw"
  ]
  node [
    id 78
    label "azjatycki"
  ]
  node [
    id 79
    label "po_izraelsku"
  ]
  node [
    id 80
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 81
    label "gospodarstwo_rolne"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
]
