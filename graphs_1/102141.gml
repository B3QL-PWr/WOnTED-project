graph [
  maxDegree 159
  minDegree 1
  meanDegree 2.3573573573573574
  density 0.003544898281740387
  graphCliqueNumber 6
  node [
    id 0
    label "beskid"
    origin "text"
  ]
  node [
    id 1
    label "zbudowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "flisz"
    origin "text"
  ]
  node [
    id 4
    label "powsta&#263;"
    origin "text"
  ]
  node [
    id 5
    label "g&#322;&#281;bokie"
    origin "text"
  ]
  node [
    id 6
    label "zbiornik"
    origin "text"
  ]
  node [
    id 7
    label "morski"
    origin "text"
  ]
  node [
    id 8
    label "naprzemianlegle"
    origin "text"
  ]
  node [
    id 9
    label "u&#322;o&#380;ony"
    origin "text"
  ]
  node [
    id 10
    label "ska&#322;a"
    origin "text"
  ]
  node [
    id 11
    label "zlepieniec"
    origin "text"
  ]
  node [
    id 12
    label "&#322;upek"
    origin "text"
  ]
  node [
    id 13
    label "ilasty"
    origin "text"
  ]
  node [
    id 14
    label "piaskowiec"
    origin "text"
  ]
  node [
    id 15
    label "margiel"
    origin "text"
  ]
  node [
    id 16
    label "osadzi&#263;"
    origin "text"
  ]
  node [
    id 17
    label "si&#281;"
    origin "text"
  ]
  node [
    id 18
    label "fliszowy"
    origin "text"
  ]
  node [
    id 19
    label "nast&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 20
    label "fa&#322;dowanie"
    origin "text"
  ]
  node [
    id 21
    label "osad"
    origin "text"
  ]
  node [
    id 22
    label "najwy&#380;szy"
    origin "text"
  ]
  node [
    id 23
    label "pasmo"
    origin "text"
  ]
  node [
    id 24
    label "makowski"
    origin "text"
  ]
  node [
    id 25
    label "&#380;ywiecki"
    origin "text"
  ]
  node [
    id 26
    label "gorce"
    origin "text"
  ]
  node [
    id 27
    label "s&#261;decki"
    origin "text"
  ]
  node [
    id 28
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 29
    label "znaczny"
    origin "text"
  ]
  node [
    id 30
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 31
    label "poniewa&#380;"
    origin "text"
  ]
  node [
    id 32
    label "bardzo"
    origin "text"
  ]
  node [
    id 33
    label "odporny"
    origin "text"
  ]
  node [
    id 34
    label "miejsce"
    origin "text"
  ]
  node [
    id 35
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 36
    label "materia"
    origin "text"
  ]
  node [
    id 37
    label "organiczny"
    origin "text"
  ]
  node [
    id 38
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 39
    label "przysypa&#263;"
    origin "text"
  ]
  node [
    id 40
    label "powsta&#322;y"
    origin "text"
  ]
  node [
    id 41
    label "z&#322;o&#380;e"
    origin "text"
  ]
  node [
    id 42
    label "ropa"
    origin "text"
  ]
  node [
    id 43
    label "naftowy"
    origin "text"
  ]
  node [
    id 44
    label "gaz"
    origin "text"
  ]
  node [
    id 45
    label "ziemny"
    origin "text"
  ]
  node [
    id 46
    label "kotlina"
    origin "text"
  ]
  node [
    id 47
    label "jasielsko"
    origin "text"
  ]
  node [
    id 48
    label "kro&#347;nie&#324;ski"
    origin "text"
  ]
  node [
    id 49
    label "pog&#243;rze"
    origin "text"
  ]
  node [
    id 50
    label "jasielski"
    origin "text"
  ]
  node [
    id 51
    label "najstarszy"
    origin "text"
  ]
  node [
    id 52
    label "polska"
    origin "text"
  ]
  node [
    id 53
    label "rejon"
    origin "text"
  ]
  node [
    id 54
    label "wydobycie"
    origin "text"
  ]
  node [
    id 55
    label "tychy"
    origin "text"
  ]
  node [
    id 56
    label "surowiec"
    origin "text"
  ]
  node [
    id 57
    label "przez"
    origin "text"
  ]
  node [
    id 58
    label "ponad"
    origin "text"
  ]
  node [
    id 59
    label "lata"
    origin "text"
  ]
  node [
    id 60
    label "eksploatacja"
    origin "text"
  ]
  node [
    id 61
    label "uleg&#322;y"
    origin "text"
  ]
  node [
    id 62
    label "wyczerpanie"
    origin "text"
  ]
  node [
    id 63
    label "b&#243;brce"
    origin "text"
  ]
  node [
    id 64
    label "ko&#322;o"
    origin "text"
  ]
  node [
    id 65
    label "krosno"
    origin "text"
  ]
  node [
    id 66
    label "ignacy"
    origin "text"
  ]
  node [
    id 67
    label "&#322;ukasiewicz"
    origin "text"
  ]
  node [
    id 68
    label "pierwszy"
    origin "text"
  ]
  node [
    id 69
    label "szyb"
    origin "text"
  ]
  node [
    id 70
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 71
    label "destylacja"
    origin "text"
  ]
  node [
    id 72
    label "wydzieli&#263;"
    origin "text"
  ]
  node [
    id 73
    label "nafta"
    origin "text"
  ]
  node [
    id 74
    label "rocznik"
    origin "text"
  ]
  node [
    id 75
    label "skonstruowa&#263;"
    origin "text"
  ]
  node [
    id 76
    label "pierwsza"
    origin "text"
  ]
  node [
    id 77
    label "lampa"
    origin "text"
  ]
  node [
    id 78
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 79
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 80
    label "skansen"
    origin "text"
  ]
  node [
    id 81
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 82
    label "wydobywczy"
    origin "text"
  ]
  node [
    id 83
    label "charakteryzowa&#263;"
    origin "text"
  ]
  node [
    id 84
    label "bogactwo"
    origin "text"
  ]
  node [
    id 85
    label "&#378;r&#243;d&#322;o"
    origin "text"
  ]
  node [
    id 86
    label "mineralny"
    origin "text"
  ]
  node [
    id 87
    label "uzdrowisko"
    origin "text"
  ]
  node [
    id 88
    label "krynica"
    origin "text"
  ]
  node [
    id 89
    label "zdr&#243;j"
    origin "text"
  ]
  node [
    id 90
    label "muszyna"
    origin "text"
  ]
  node [
    id 91
    label "&#380;egiest&#243;w"
    origin "text"
  ]
  node [
    id 92
    label "piwniczna"
    origin "text"
  ]
  node [
    id 93
    label "zaplanowa&#263;"
  ]
  node [
    id 94
    label "establish"
  ]
  node [
    id 95
    label "stworzy&#263;"
  ]
  node [
    id 96
    label "wytworzy&#263;"
  ]
  node [
    id 97
    label "evolve"
  ]
  node [
    id 98
    label "budowla"
  ]
  node [
    id 99
    label "si&#281;ga&#263;"
  ]
  node [
    id 100
    label "trwa&#263;"
  ]
  node [
    id 101
    label "obecno&#347;&#263;"
  ]
  node [
    id 102
    label "stan"
  ]
  node [
    id 103
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 104
    label "stand"
  ]
  node [
    id 105
    label "mie&#263;_miejsce"
  ]
  node [
    id 106
    label "uczestniczy&#263;"
  ]
  node [
    id 107
    label "chodzi&#263;"
  ]
  node [
    id 108
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 109
    label "equal"
  ]
  node [
    id 110
    label "ska&#322;a_osadowa"
  ]
  node [
    id 111
    label "zaistnie&#263;"
  ]
  node [
    id 112
    label "sprzeciwi&#263;_si&#281;"
  ]
  node [
    id 113
    label "originate"
  ]
  node [
    id 114
    label "rise"
  ]
  node [
    id 115
    label "zbuntowa&#263;_si&#281;"
  ]
  node [
    id 116
    label "mount"
  ]
  node [
    id 117
    label "stan&#261;&#263;"
  ]
  node [
    id 118
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 119
    label "kuca&#263;"
  ]
  node [
    id 120
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 121
    label "zawarto&#347;&#263;"
  ]
  node [
    id 122
    label "kraw&#281;d&#378;"
  ]
  node [
    id 123
    label "pojemnik"
  ]
  node [
    id 124
    label "spichlerz"
  ]
  node [
    id 125
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 126
    label "specjalny"
  ]
  node [
    id 127
    label "niebieski"
  ]
  node [
    id 128
    label "nadmorski"
  ]
  node [
    id 129
    label "morsko"
  ]
  node [
    id 130
    label "wodny"
  ]
  node [
    id 131
    label "s&#322;ony"
  ]
  node [
    id 132
    label "zielony"
  ]
  node [
    id 133
    label "przypominaj&#261;cy"
  ]
  node [
    id 134
    label "typowy"
  ]
  node [
    id 135
    label "naprzemianleg&#322;y"
  ]
  node [
    id 136
    label "dobrze_wychowany"
  ]
  node [
    id 137
    label "mieszanina"
  ]
  node [
    id 138
    label "lamina"
  ]
  node [
    id 139
    label "uskakiwanie"
  ]
  node [
    id 140
    label "lepiszcze_skalne"
  ]
  node [
    id 141
    label "uskoczy&#263;"
  ]
  node [
    id 142
    label "obiekt"
  ]
  node [
    id 143
    label "sklerometr"
  ]
  node [
    id 144
    label "rygiel"
  ]
  node [
    id 145
    label "zmetamorfizowanie"
  ]
  node [
    id 146
    label "rock"
  ]
  node [
    id 147
    label "porwak"
  ]
  node [
    id 148
    label "&#322;upkowato&#347;&#263;"
  ]
  node [
    id 149
    label "opoka"
  ]
  node [
    id 150
    label "soczewa"
  ]
  node [
    id 151
    label "uskoczenie"
  ]
  node [
    id 152
    label "uskakiwa&#263;"
  ]
  node [
    id 153
    label "wyg&#322;ad_lodowcowy"
  ]
  node [
    id 154
    label "bloczno&#347;&#263;"
  ]
  node [
    id 155
    label "zmetamorfizowa&#263;"
  ]
  node [
    id 156
    label "conglomeration"
  ]
  node [
    id 157
    label "krzemianowy"
  ]
  node [
    id 158
    label "ro&#347;lina_zielna"
  ]
  node [
    id 159
    label "ma&#347;lak_pstry"
  ]
  node [
    id 160
    label "pieczarniak"
  ]
  node [
    id 161
    label "piaskowcowate"
  ]
  node [
    id 162
    label "s&#322;onki"
  ]
  node [
    id 163
    label "go&#378;dzikowate"
  ]
  node [
    id 164
    label "ptak_w&#281;drowny"
  ]
  node [
    id 165
    label "grzyb_jadalny"
  ]
  node [
    id 166
    label "ptak_wodny"
  ]
  node [
    id 167
    label "marl"
  ]
  node [
    id 168
    label "settle"
  ]
  node [
    id 169
    label "introduce"
  ]
  node [
    id 170
    label "zlokalizowa&#263;"
  ]
  node [
    id 171
    label "przymocowa&#263;"
  ]
  node [
    id 172
    label "zatrzyma&#263;"
  ]
  node [
    id 173
    label "umie&#347;ci&#263;"
  ]
  node [
    id 174
    label "set"
  ]
  node [
    id 175
    label "kamienny"
  ]
  node [
    id 176
    label "nacisn&#261;&#263;"
  ]
  node [
    id 177
    label "zaatakowa&#263;"
  ]
  node [
    id 178
    label "gamble"
  ]
  node [
    id 179
    label "supervene"
  ]
  node [
    id 180
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 181
    label "wypi&#281;trzenie"
  ]
  node [
    id 182
    label "plication"
  ]
  node [
    id 183
    label "proces_geologiczny"
  ]
  node [
    id 184
    label "ruchy"
  ]
  node [
    id 185
    label "faza_g&#243;rotw&#243;rcza"
  ]
  node [
    id 186
    label "wypi&#281;trzy&#263;_si&#281;"
  ]
  node [
    id 187
    label "uk&#322;adanie"
  ]
  node [
    id 188
    label "wypi&#281;trzanie_si&#281;"
  ]
  node [
    id 189
    label "wypi&#281;trza&#263;_si&#281;"
  ]
  node [
    id 190
    label "kolmatacja"
  ]
  node [
    id 191
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 192
    label "warstwa"
  ]
  node [
    id 193
    label "sedymentacja"
  ]
  node [
    id 194
    label "deposit"
  ]
  node [
    id 195
    label "terygeniczny"
  ]
  node [
    id 196
    label "kompakcja"
  ]
  node [
    id 197
    label "wspomnienie"
  ]
  node [
    id 198
    label "streak"
  ]
  node [
    id 199
    label "przebieg"
  ]
  node [
    id 200
    label "ulica"
  ]
  node [
    id 201
    label "strip"
  ]
  node [
    id 202
    label "swath"
  ]
  node [
    id 203
    label "kana&#322;"
  ]
  node [
    id 204
    label "kszta&#322;t"
  ]
  node [
    id 205
    label "wydarzenie"
  ]
  node [
    id 206
    label "pas"
  ]
  node [
    id 207
    label "famu&#322;a"
  ]
  node [
    id 208
    label "polski"
  ]
  node [
    id 209
    label "po_&#380;ywiecku"
  ]
  node [
    id 210
    label "znacznie"
  ]
  node [
    id 211
    label "zauwa&#380;alny"
  ]
  node [
    id 212
    label "wa&#380;ny"
  ]
  node [
    id 213
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 214
    label "kwota"
  ]
  node [
    id 215
    label "ilo&#347;&#263;"
  ]
  node [
    id 216
    label "w_chuj"
  ]
  node [
    id 217
    label "uodpornienie_si&#281;"
  ]
  node [
    id 218
    label "silny"
  ]
  node [
    id 219
    label "uodpornienie"
  ]
  node [
    id 220
    label "uodpornianie"
  ]
  node [
    id 221
    label "hartowny"
  ]
  node [
    id 222
    label "uodparnianie"
  ]
  node [
    id 223
    label "uodparnianie_si&#281;"
  ]
  node [
    id 224
    label "mocny"
  ]
  node [
    id 225
    label "cia&#322;o"
  ]
  node [
    id 226
    label "plac"
  ]
  node [
    id 227
    label "cecha"
  ]
  node [
    id 228
    label "uwaga"
  ]
  node [
    id 229
    label "przestrze&#324;"
  ]
  node [
    id 230
    label "status"
  ]
  node [
    id 231
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 232
    label "chwila"
  ]
  node [
    id 233
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 234
    label "rz&#261;d"
  ]
  node [
    id 235
    label "praca"
  ]
  node [
    id 236
    label "location"
  ]
  node [
    id 237
    label "warunek_lokalowy"
  ]
  node [
    id 238
    label "du&#380;y"
  ]
  node [
    id 239
    label "cz&#281;sto"
  ]
  node [
    id 240
    label "mocno"
  ]
  node [
    id 241
    label "wiela"
  ]
  node [
    id 242
    label "byt"
  ]
  node [
    id 243
    label "temat"
  ]
  node [
    id 244
    label "materia&#322;"
  ]
  node [
    id 245
    label "szczeg&#243;&#322;"
  ]
  node [
    id 246
    label "informacja"
  ]
  node [
    id 247
    label "rzecz"
  ]
  node [
    id 248
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 249
    label "organicznie"
  ]
  node [
    id 250
    label "naturalny"
  ]
  node [
    id 251
    label "nieodparty"
  ]
  node [
    id 252
    label "na&#347;ladowczy"
  ]
  node [
    id 253
    label "trwa&#322;y"
  ]
  node [
    id 254
    label "podobny"
  ]
  node [
    id 255
    label "proceed"
  ]
  node [
    id 256
    label "catch"
  ]
  node [
    id 257
    label "pozosta&#263;"
  ]
  node [
    id 258
    label "osta&#263;_si&#281;"
  ]
  node [
    id 259
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 260
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 261
    label "change"
  ]
  node [
    id 262
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 263
    label "posypa&#263;"
  ]
  node [
    id 264
    label "spowodowa&#263;"
  ]
  node [
    id 265
    label "zalega&#263;"
  ]
  node [
    id 266
    label "zasoby_kopalin"
  ]
  node [
    id 267
    label "skupienie"
  ]
  node [
    id 268
    label "bilansowo&#347;&#263;"
  ]
  node [
    id 269
    label "zaleganie"
  ]
  node [
    id 270
    label "wychodnia"
  ]
  node [
    id 271
    label "surowiec_energetyczny"
  ]
  node [
    id 272
    label "bitum"
  ]
  node [
    id 273
    label "kopalina_podstawowa"
  ]
  node [
    id 274
    label "matter"
  ]
  node [
    id 275
    label "mineraloid"
  ]
  node [
    id 276
    label "oktan"
  ]
  node [
    id 277
    label "petrodolar"
  ]
  node [
    id 278
    label "Orlen"
  ]
  node [
    id 279
    label "wydzielina"
  ]
  node [
    id 280
    label "ciecz"
  ]
  node [
    id 281
    label "ropopochodny"
  ]
  node [
    id 282
    label "rze&#347;ki"
  ]
  node [
    id 283
    label "chemiczny"
  ]
  node [
    id 284
    label "kerosene"
  ]
  node [
    id 285
    label "peda&#322;"
  ]
  node [
    id 286
    label "przy&#347;piesznik"
  ]
  node [
    id 287
    label "p&#322;omie&#324;"
  ]
  node [
    id 288
    label "paliwo"
  ]
  node [
    id 289
    label "instalacja"
  ]
  node [
    id 290
    label "gas"
  ]
  node [
    id 291
    label "bro&#324;"
  ]
  node [
    id 292
    label "accelerator"
  ]
  node [
    id 293
    label "rozpr&#281;&#380;liwo&#347;&#263;"
  ]
  node [
    id 294
    label "substancja"
  ]
  node [
    id 295
    label "stan_skupienia"
  ]
  node [
    id 296
    label "termojonizacja"
  ]
  node [
    id 297
    label "dale"
  ]
  node [
    id 298
    label "zbocze"
  ]
  node [
    id 299
    label "Kotlina_K&#322;odzka"
  ]
  node [
    id 300
    label "dolina"
  ]
  node [
    id 301
    label "g&#243;ry"
  ]
  node [
    id 302
    label "niecka"
  ]
  node [
    id 303
    label "teren"
  ]
  node [
    id 304
    label "Skandynawia"
  ]
  node [
    id 305
    label "Yorkshire"
  ]
  node [
    id 306
    label "Kaukaz"
  ]
  node [
    id 307
    label "Kaszmir"
  ]
  node [
    id 308
    label "Toskania"
  ]
  node [
    id 309
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 310
    label "&#321;emkowszczyzna"
  ]
  node [
    id 311
    label "obszar"
  ]
  node [
    id 312
    label "Amhara"
  ]
  node [
    id 313
    label "Lombardia"
  ]
  node [
    id 314
    label "Podbeskidzie"
  ]
  node [
    id 315
    label "Kalabria"
  ]
  node [
    id 316
    label "Tyrol"
  ]
  node [
    id 317
    label "Neogea"
  ]
  node [
    id 318
    label "Pamir"
  ]
  node [
    id 319
    label "Lubelszczyzna"
  ]
  node [
    id 320
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 321
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 322
    label "&#379;ywiecczyzna"
  ]
  node [
    id 323
    label "Europa_Wschodnia"
  ]
  node [
    id 324
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 325
    label "Zabajkale"
  ]
  node [
    id 326
    label "Kaszuby"
  ]
  node [
    id 327
    label "Bo&#347;nia"
  ]
  node [
    id 328
    label "Noworosja"
  ]
  node [
    id 329
    label "Ba&#322;kany"
  ]
  node [
    id 330
    label "Antarktyka"
  ]
  node [
    id 331
    label "Anglia"
  ]
  node [
    id 332
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 333
    label "Kielecczyzna"
  ]
  node [
    id 334
    label "Pomorze_Zachodnie"
  ]
  node [
    id 335
    label "Opolskie"
  ]
  node [
    id 336
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 337
    label "Ko&#322;yma"
  ]
  node [
    id 338
    label "Oksytania"
  ]
  node [
    id 339
    label "Arktyka"
  ]
  node [
    id 340
    label "Syjon"
  ]
  node [
    id 341
    label "Kresy_Zachodnie"
  ]
  node [
    id 342
    label "Kociewie"
  ]
  node [
    id 343
    label "Huculszczyzna"
  ]
  node [
    id 344
    label "wsch&#243;d"
  ]
  node [
    id 345
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 346
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 347
    label "Bawaria"
  ]
  node [
    id 348
    label "Rakowice"
  ]
  node [
    id 349
    label "Syberia_Wschodnia"
  ]
  node [
    id 350
    label "Maghreb"
  ]
  node [
    id 351
    label "Bory_Tucholskie"
  ]
  node [
    id 352
    label "Europa_Zachodnia"
  ]
  node [
    id 353
    label "antroposfera"
  ]
  node [
    id 354
    label "Kerala"
  ]
  node [
    id 355
    label "Podhale"
  ]
  node [
    id 356
    label "Kabylia"
  ]
  node [
    id 357
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 358
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 359
    label "Ma&#322;opolska"
  ]
  node [
    id 360
    label "Polesie"
  ]
  node [
    id 361
    label "Liguria"
  ]
  node [
    id 362
    label "&#321;&#243;dzkie"
  ]
  node [
    id 363
    label "Syberia_Zachodnia"
  ]
  node [
    id 364
    label "Notogea"
  ]
  node [
    id 365
    label "Palestyna"
  ]
  node [
    id 366
    label "&#321;&#281;g"
  ]
  node [
    id 367
    label "Bojkowszczyzna"
  ]
  node [
    id 368
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 369
    label "Karaiby"
  ]
  node [
    id 370
    label "S&#261;decczyzna"
  ]
  node [
    id 371
    label "Zab&#322;ocie"
  ]
  node [
    id 372
    label "Sand&#380;ak"
  ]
  node [
    id 373
    label "Nadrenia"
  ]
  node [
    id 374
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 375
    label "Zakarpacie"
  ]
  node [
    id 376
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 377
    label "Zag&#243;rze"
  ]
  node [
    id 378
    label "Andaluzja"
  ]
  node [
    id 379
    label "Turkiestan"
  ]
  node [
    id 380
    label "Zabu&#380;e"
  ]
  node [
    id 381
    label "Naddniestrze"
  ]
  node [
    id 382
    label "Hercegowina"
  ]
  node [
    id 383
    label "Opolszczyzna"
  ]
  node [
    id 384
    label "Lotaryngia"
  ]
  node [
    id 385
    label "pas_planetoid"
  ]
  node [
    id 386
    label "Afryka_Wschodnia"
  ]
  node [
    id 387
    label "Szlezwik"
  ]
  node [
    id 388
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 389
    label "holarktyka"
  ]
  node [
    id 390
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 391
    label "akrecja"
  ]
  node [
    id 392
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 393
    label "Mazowsze"
  ]
  node [
    id 394
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 395
    label "Afryka_Zachodnia"
  ]
  node [
    id 396
    label "Galicja"
  ]
  node [
    id 397
    label "Szkocja"
  ]
  node [
    id 398
    label "po&#322;udnie"
  ]
  node [
    id 399
    label "Walia"
  ]
  node [
    id 400
    label "Powi&#347;le"
  ]
  node [
    id 401
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 402
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 403
    label "Ruda_Pabianicka"
  ]
  node [
    id 404
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 405
    label "Zamojszczyzna"
  ]
  node [
    id 406
    label "Kujawy"
  ]
  node [
    id 407
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 408
    label "Podlasie"
  ]
  node [
    id 409
    label "Laponia"
  ]
  node [
    id 410
    label "Umbria"
  ]
  node [
    id 411
    label "Mezoameryka"
  ]
  node [
    id 412
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 413
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 414
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 415
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 416
    label "Kurdystan"
  ]
  node [
    id 417
    label "Kampania"
  ]
  node [
    id 418
    label "Armagnac"
  ]
  node [
    id 419
    label "Polinezja"
  ]
  node [
    id 420
    label "Warmia"
  ]
  node [
    id 421
    label "Wielkopolska"
  ]
  node [
    id 422
    label "Kosowo"
  ]
  node [
    id 423
    label "Bordeaux"
  ]
  node [
    id 424
    label "Lauda"
  ]
  node [
    id 425
    label "p&#243;&#322;noc"
  ]
  node [
    id 426
    label "Mazury"
  ]
  node [
    id 427
    label "Podkarpacie"
  ]
  node [
    id 428
    label "Oceania"
  ]
  node [
    id 429
    label "Lasko"
  ]
  node [
    id 430
    label "Amazonia"
  ]
  node [
    id 431
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 432
    label "zach&#243;d"
  ]
  node [
    id 433
    label "Olszanica"
  ]
  node [
    id 434
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 435
    label "Kurpie"
  ]
  node [
    id 436
    label "Tonkin"
  ]
  node [
    id 437
    label "Piotrowo"
  ]
  node [
    id 438
    label "Azja_Wschodnia"
  ]
  node [
    id 439
    label "Mikronezja"
  ]
  node [
    id 440
    label "Ukraina_Zachodnia"
  ]
  node [
    id 441
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 442
    label "Turyngia"
  ]
  node [
    id 443
    label "Baszkiria"
  ]
  node [
    id 444
    label "Apulia"
  ]
  node [
    id 445
    label "Pow&#261;zki"
  ]
  node [
    id 446
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 447
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 448
    label "Indochiny"
  ]
  node [
    id 449
    label "Biskupizna"
  ]
  node [
    id 450
    label "Lubuskie"
  ]
  node [
    id 451
    label "Ludwin&#243;w"
  ]
  node [
    id 452
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 453
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 454
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 455
    label "dobycie"
  ]
  node [
    id 456
    label "zrobienie"
  ]
  node [
    id 457
    label "spowodowanie"
  ]
  node [
    id 458
    label "powyci&#261;ganie"
  ]
  node [
    id 459
    label "fusillade"
  ]
  node [
    id 460
    label "wyeksploatowanie"
  ]
  node [
    id 461
    label "explosion"
  ]
  node [
    id 462
    label "draw"
  ]
  node [
    id 463
    label "wydostanie"
  ]
  node [
    id 464
    label "uzyskanie"
  ]
  node [
    id 465
    label "produkcja"
  ]
  node [
    id 466
    label "uwydatnienie"
  ]
  node [
    id 467
    label "wyj&#281;cie"
  ]
  node [
    id 468
    label "wyratowanie"
  ]
  node [
    id 469
    label "g&#243;rnictwo"
  ]
  node [
    id 470
    label "tworzywo"
  ]
  node [
    id 471
    label "sk&#322;adnik"
  ]
  node [
    id 472
    label "summer"
  ]
  node [
    id 473
    label "czas"
  ]
  node [
    id 474
    label "utilization"
  ]
  node [
    id 475
    label "czynno&#347;&#263;"
  ]
  node [
    id 476
    label "wyzysk"
  ]
  node [
    id 477
    label "proces"
  ]
  node [
    id 478
    label "zmi&#281;kni&#281;cie"
  ]
  node [
    id 479
    label "mi&#281;kni&#281;cie"
  ]
  node [
    id 480
    label "zale&#380;ny"
  ]
  node [
    id 481
    label "ulegle"
  ]
  node [
    id 482
    label "opracowanie"
  ]
  node [
    id 483
    label "inanition"
  ]
  node [
    id 484
    label "niewyspanie"
  ]
  node [
    id 485
    label "wybranie"
  ]
  node [
    id 486
    label "zm&#281;czenie"
  ]
  node [
    id 487
    label "znu&#380;enie"
  ]
  node [
    id 488
    label "os&#322;abienie"
  ]
  node [
    id 489
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 490
    label "siniec"
  ]
  node [
    id 491
    label "wyko&#324;czenie"
  ]
  node [
    id 492
    label "fatigue_duty"
  ]
  node [
    id 493
    label "adjustment"
  ]
  node [
    id 494
    label "zabrakni&#281;cie"
  ]
  node [
    id 495
    label "kondycja_fizyczna"
  ]
  node [
    id 496
    label "zu&#380;ycie"
  ]
  node [
    id 497
    label "skonany"
  ]
  node [
    id 498
    label "om&#243;wienie"
  ]
  node [
    id 499
    label "odcinek_ko&#322;a"
  ]
  node [
    id 500
    label "whip"
  ]
  node [
    id 501
    label "przedmiot"
  ]
  node [
    id 502
    label "grupa"
  ]
  node [
    id 503
    label "zabawa"
  ]
  node [
    id 504
    label "gang"
  ]
  node [
    id 505
    label "obr&#281;cz"
  ]
  node [
    id 506
    label "pi"
  ]
  node [
    id 507
    label "zwolnica"
  ]
  node [
    id 508
    label "piasta"
  ]
  node [
    id 509
    label "pojazd"
  ]
  node [
    id 510
    label "&#322;amanie"
  ]
  node [
    id 511
    label "o&#347;"
  ]
  node [
    id 512
    label "okr&#261;g"
  ]
  node [
    id 513
    label "figura_ograniczona"
  ]
  node [
    id 514
    label "stowarzyszenie"
  ]
  node [
    id 515
    label "figura_geometryczna"
  ]
  node [
    id 516
    label "kolokwium"
  ]
  node [
    id 517
    label "sphere"
  ]
  node [
    id 518
    label "p&#243;&#322;kole"
  ]
  node [
    id 519
    label "lap"
  ]
  node [
    id 520
    label "podwozie"
  ]
  node [
    id 521
    label "sejmik"
  ]
  node [
    id 522
    label "&#322;ama&#263;"
  ]
  node [
    id 523
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 524
    label "cz&#243;&#322;enko"
  ]
  node [
    id 525
    label "nicielnica"
  ]
  node [
    id 526
    label "futryna"
  ]
  node [
    id 527
    label "chwytak"
  ]
  node [
    id 528
    label "kro&#347;niak"
  ]
  node [
    id 529
    label "maszyna_w&#322;&#243;kiennicza"
  ]
  node [
    id 530
    label "narz&#281;dzie"
  ]
  node [
    id 531
    label "bid&#322;o"
  ]
  node [
    id 532
    label "przewa&#322;"
  ]
  node [
    id 533
    label "najwa&#380;niejszy"
  ]
  node [
    id 534
    label "pocz&#261;tkowy"
  ]
  node [
    id 535
    label "dobry"
  ]
  node [
    id 536
    label "ch&#281;tny"
  ]
  node [
    id 537
    label "dzie&#324;"
  ]
  node [
    id 538
    label "pr&#281;dki"
  ]
  node [
    id 539
    label "wyrobisko"
  ]
  node [
    id 540
    label "przesta&#263;"
  ]
  node [
    id 541
    label "zrobi&#263;"
  ]
  node [
    id 542
    label "cause"
  ]
  node [
    id 543
    label "communicate"
  ]
  node [
    id 544
    label "pogon"
  ]
  node [
    id 545
    label "technologia"
  ]
  node [
    id 546
    label "distillation"
  ]
  node [
    id 547
    label "przydzieli&#263;"
  ]
  node [
    id 548
    label "allocate"
  ]
  node [
    id 549
    label "signalize"
  ]
  node [
    id 550
    label "wykroi&#263;"
  ]
  node [
    id 551
    label "wyznaczy&#263;"
  ]
  node [
    id 552
    label "oddzieli&#263;"
  ]
  node [
    id 553
    label "rozdzieli&#263;"
  ]
  node [
    id 554
    label "formacja"
  ]
  node [
    id 555
    label "kronika"
  ]
  node [
    id 556
    label "czasopismo"
  ]
  node [
    id 557
    label "yearbook"
  ]
  node [
    id 558
    label "godzina"
  ]
  node [
    id 559
    label "iluminowa&#263;"
  ]
  node [
    id 560
    label "&#380;ar&#243;wka"
  ]
  node [
    id 561
    label "sztuczne_&#378;r&#243;d&#322;o_&#347;wiat&#322;a"
  ]
  node [
    id 562
    label "o&#347;wietla&#263;"
  ]
  node [
    id 563
    label "doba"
  ]
  node [
    id 564
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 565
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 566
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 567
    label "doznawa&#263;"
  ]
  node [
    id 568
    label "znachodzi&#263;"
  ]
  node [
    id 569
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 570
    label "pozyskiwa&#263;"
  ]
  node [
    id 571
    label "odzyskiwa&#263;"
  ]
  node [
    id 572
    label "os&#261;dza&#263;"
  ]
  node [
    id 573
    label "wykrywa&#263;"
  ]
  node [
    id 574
    label "unwrap"
  ]
  node [
    id 575
    label "detect"
  ]
  node [
    id 576
    label "wymy&#347;la&#263;"
  ]
  node [
    id 577
    label "powodowa&#263;"
  ]
  node [
    id 578
    label "zbi&#243;r"
  ]
  node [
    id 579
    label "muzeum"
  ]
  node [
    id 580
    label "gospodarka"
  ]
  node [
    id 581
    label "przechowalnictwo"
  ]
  node [
    id 582
    label "uprzemys&#322;owienie"
  ]
  node [
    id 583
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 584
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 585
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 586
    label "uprzemys&#322;awianie"
  ]
  node [
    id 587
    label "report"
  ]
  node [
    id 588
    label "opisywa&#263;"
  ]
  node [
    id 589
    label "mark"
  ]
  node [
    id 590
    label "przygotowywa&#263;"
  ]
  node [
    id 591
    label "cechowa&#263;"
  ]
  node [
    id 592
    label "podostatek"
  ]
  node [
    id 593
    label "fortune"
  ]
  node [
    id 594
    label "wysyp"
  ]
  node [
    id 595
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 596
    label "mienie"
  ]
  node [
    id 597
    label "fullness"
  ]
  node [
    id 598
    label "sytuacja"
  ]
  node [
    id 599
    label "z&#322;ote_czasy"
  ]
  node [
    id 600
    label "bra&#263;_si&#281;"
  ]
  node [
    id 601
    label "&#347;wiadectwo"
  ]
  node [
    id 602
    label "przyczyna"
  ]
  node [
    id 603
    label "matuszka"
  ]
  node [
    id 604
    label "geneza"
  ]
  node [
    id 605
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 606
    label "kamena"
  ]
  node [
    id 607
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 608
    label "czynnik"
  ]
  node [
    id 609
    label "pocz&#261;tek"
  ]
  node [
    id 610
    label "poci&#261;ganie"
  ]
  node [
    id 611
    label "rezultat"
  ]
  node [
    id 612
    label "ciek_wodny"
  ]
  node [
    id 613
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 614
    label "subject"
  ]
  node [
    id 615
    label "Busko-Zdr&#243;j"
  ]
  node [
    id 616
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 617
    label "Rabka-Zdr&#243;j"
  ]
  node [
    id 618
    label "Sopot"
  ]
  node [
    id 619
    label "&#379;egiest&#243;w-Zdr&#243;j"
  ]
  node [
    id 620
    label "D&#261;bki"
  ]
  node [
    id 621
    label "Inowroc&#322;aw"
  ]
  node [
    id 622
    label "Solec-Zdr&#243;j"
  ]
  node [
    id 623
    label "Go&#322;dap"
  ]
  node [
    id 624
    label "Ciechocinek"
  ]
  node [
    id 625
    label "Aleksandr&#243;w_Kujawski"
  ]
  node [
    id 626
    label "Ko&#322;obrzeg"
  ]
  node [
    id 627
    label "Kamie&#324;_Pomorski"
  ]
  node [
    id 628
    label "Szczawnica"
  ]
  node [
    id 629
    label "kuracjusz"
  ]
  node [
    id 630
    label "Gocza&#322;kowice-Zdr&#243;j"
  ]
  node [
    id 631
    label "Ustro&#324;"
  ]
  node [
    id 632
    label "G&#322;ucho&#322;azy"
  ]
  node [
    id 633
    label "Ustka"
  ]
  node [
    id 634
    label "Wieniec-Zdr&#243;j"
  ]
  node [
    id 635
    label "Swoszowice"
  ]
  node [
    id 636
    label "August&#243;w"
  ]
  node [
    id 637
    label "&#346;winouj&#347;cie"
  ]
  node [
    id 638
    label "Trzci&#324;sko-Zdr&#243;j"
  ]
  node [
    id 639
    label "Konstancin-Jeziorna"
  ]
  node [
    id 640
    label "sanatorium"
  ]
  node [
    id 641
    label "strumie&#324;"
  ]
  node [
    id 642
    label "zdrojowisko"
  ]
  node [
    id 643
    label "Beskid"
  ]
  node [
    id 644
    label "Ignacy"
  ]
  node [
    id 645
    label "&#321;ukasiewicz"
  ]
  node [
    id 646
    label "&#379;egiest&#243;w"
  ]
  node [
    id 647
    label "wysoki"
  ]
  node [
    id 648
    label "ska&#322;ka"
  ]
  node [
    id 649
    label "trzy"
  ]
  node [
    id 650
    label "korona"
  ]
  node [
    id 651
    label "Sromowce"
  ]
  node [
    id 652
    label "Ni&#380;nych"
  ]
  node [
    id 653
    label "wy&#380;ni"
  ]
  node [
    id 654
    label "prze&#322;&#281;cz"
  ]
  node [
    id 655
    label "liliowy"
  ]
  node [
    id 656
    label "Tatry"
  ]
  node [
    id 657
    label "wschodni"
  ]
  node [
    id 658
    label "zachodni"
  ]
  node [
    id 659
    label "oko"
  ]
  node [
    id 660
    label "wielki"
  ]
  node [
    id 661
    label "stawi&#263;"
  ]
  node [
    id 662
    label "czarny"
  ]
  node [
    id 663
    label "G&#261;siennicowy"
  ]
  node [
    id 664
    label "jaskinia"
  ]
  node [
    id 665
    label "mro&#378;ny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 34
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 30
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 34
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 17
    target 79
  ]
  edge [
    source 17
    target 80
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 19
    target 37
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 39
  ]
  edge [
    source 21
    target 40
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 24
    target 643
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 643
  ]
  edge [
    source 27
    target 83
  ]
  edge [
    source 27
    target 52
  ]
  edge [
    source 27
    target 71
  ]
  edge [
    source 27
    target 85
  ]
  edge [
    source 27
    target 643
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 36
  ]
  edge [
    source 28
    target 47
  ]
  edge [
    source 28
    target 45
  ]
  edge [
    source 29
    target 210
  ]
  edge [
    source 29
    target 211
  ]
  edge [
    source 29
    target 212
  ]
  edge [
    source 29
    target 63
  ]
  edge [
    source 30
    target 101
  ]
  edge [
    source 30
    target 213
  ]
  edge [
    source 30
    target 214
  ]
  edge [
    source 30
    target 215
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 31
    target 41
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 216
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 47
  ]
  edge [
    source 32
    target 45
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 217
  ]
  edge [
    source 33
    target 218
  ]
  edge [
    source 33
    target 219
  ]
  edge [
    source 33
    target 220
  ]
  edge [
    source 33
    target 221
  ]
  edge [
    source 33
    target 222
  ]
  edge [
    source 33
    target 223
  ]
  edge [
    source 33
    target 224
  ]
  edge [
    source 34
    target 225
  ]
  edge [
    source 34
    target 226
  ]
  edge [
    source 34
    target 227
  ]
  edge [
    source 34
    target 228
  ]
  edge [
    source 34
    target 229
  ]
  edge [
    source 34
    target 230
  ]
  edge [
    source 34
    target 231
  ]
  edge [
    source 34
    target 232
  ]
  edge [
    source 34
    target 233
  ]
  edge [
    source 34
    target 234
  ]
  edge [
    source 34
    target 235
  ]
  edge [
    source 34
    target 236
  ]
  edge [
    source 34
    target 237
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 238
  ]
  edge [
    source 35
    target 239
  ]
  edge [
    source 35
    target 240
  ]
  edge [
    source 35
    target 241
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 242
  ]
  edge [
    source 36
    target 243
  ]
  edge [
    source 36
    target 244
  ]
  edge [
    source 36
    target 245
  ]
  edge [
    source 36
    target 246
  ]
  edge [
    source 36
    target 247
  ]
  edge [
    source 36
    target 42
  ]
  edge [
    source 36
    target 248
  ]
  edge [
    source 36
    target 47
  ]
  edge [
    source 36
    target 45
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 249
  ]
  edge [
    source 37
    target 250
  ]
  edge [
    source 37
    target 251
  ]
  edge [
    source 37
    target 252
  ]
  edge [
    source 37
    target 253
  ]
  edge [
    source 37
    target 254
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 255
  ]
  edge [
    source 38
    target 256
  ]
  edge [
    source 38
    target 257
  ]
  edge [
    source 38
    target 258
  ]
  edge [
    source 38
    target 259
  ]
  edge [
    source 38
    target 260
  ]
  edge [
    source 38
    target 180
  ]
  edge [
    source 38
    target 261
  ]
  edge [
    source 38
    target 262
  ]
  edge [
    source 39
    target 180
  ]
  edge [
    source 39
    target 263
  ]
  edge [
    source 39
    target 264
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 86
  ]
  edge [
    source 40
    target 87
  ]
  edge [
    source 40
    target 77
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 265
  ]
  edge [
    source 41
    target 266
  ]
  edge [
    source 41
    target 267
  ]
  edge [
    source 41
    target 268
  ]
  edge [
    source 41
    target 269
  ]
  edge [
    source 41
    target 270
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 60
  ]
  edge [
    source 42
    target 71
  ]
  edge [
    source 42
    target 271
  ]
  edge [
    source 42
    target 272
  ]
  edge [
    source 42
    target 273
  ]
  edge [
    source 42
    target 274
  ]
  edge [
    source 42
    target 275
  ]
  edge [
    source 42
    target 276
  ]
  edge [
    source 42
    target 277
  ]
  edge [
    source 42
    target 278
  ]
  edge [
    source 42
    target 279
  ]
  edge [
    source 42
    target 280
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 69
  ]
  edge [
    source 43
    target 70
  ]
  edge [
    source 43
    target 72
  ]
  edge [
    source 43
    target 77
  ]
  edge [
    source 43
    target 78
  ]
  edge [
    source 43
    target 281
  ]
  edge [
    source 43
    target 282
  ]
  edge [
    source 43
    target 283
  ]
  edge [
    source 43
    target 284
  ]
  edge [
    source 43
    target 53
  ]
  edge [
    source 43
    target 57
  ]
  edge [
    source 43
    target 58
  ]
  edge [
    source 43
    target 81
  ]
  edge [
    source 43
    target 83
  ]
  edge [
    source 43
    target 91
  ]
  edge [
    source 43
    target 87
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 225
  ]
  edge [
    source 44
    target 285
  ]
  edge [
    source 44
    target 286
  ]
  edge [
    source 44
    target 287
  ]
  edge [
    source 44
    target 288
  ]
  edge [
    source 44
    target 289
  ]
  edge [
    source 44
    target 290
  ]
  edge [
    source 44
    target 291
  ]
  edge [
    source 44
    target 292
  ]
  edge [
    source 44
    target 293
  ]
  edge [
    source 44
    target 294
  ]
  edge [
    source 44
    target 295
  ]
  edge [
    source 44
    target 296
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 61
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 297
  ]
  edge [
    source 46
    target 298
  ]
  edge [
    source 46
    target 299
  ]
  edge [
    source 46
    target 300
  ]
  edge [
    source 46
    target 301
  ]
  edge [
    source 46
    target 302
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 303
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 71
  ]
  edge [
    source 52
    target 85
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 304
  ]
  edge [
    source 53
    target 305
  ]
  edge [
    source 53
    target 306
  ]
  edge [
    source 53
    target 307
  ]
  edge [
    source 53
    target 308
  ]
  edge [
    source 53
    target 309
  ]
  edge [
    source 53
    target 310
  ]
  edge [
    source 53
    target 311
  ]
  edge [
    source 53
    target 312
  ]
  edge [
    source 53
    target 313
  ]
  edge [
    source 53
    target 314
  ]
  edge [
    source 53
    target 315
  ]
  edge [
    source 53
    target 316
  ]
  edge [
    source 53
    target 317
  ]
  edge [
    source 53
    target 318
  ]
  edge [
    source 53
    target 319
  ]
  edge [
    source 53
    target 320
  ]
  edge [
    source 53
    target 321
  ]
  edge [
    source 53
    target 322
  ]
  edge [
    source 53
    target 323
  ]
  edge [
    source 53
    target 324
  ]
  edge [
    source 53
    target 325
  ]
  edge [
    source 53
    target 326
  ]
  edge [
    source 53
    target 327
  ]
  edge [
    source 53
    target 328
  ]
  edge [
    source 53
    target 329
  ]
  edge [
    source 53
    target 330
  ]
  edge [
    source 53
    target 331
  ]
  edge [
    source 53
    target 332
  ]
  edge [
    source 53
    target 333
  ]
  edge [
    source 53
    target 334
  ]
  edge [
    source 53
    target 335
  ]
  edge [
    source 53
    target 336
  ]
  edge [
    source 53
    target 337
  ]
  edge [
    source 53
    target 338
  ]
  edge [
    source 53
    target 339
  ]
  edge [
    source 53
    target 340
  ]
  edge [
    source 53
    target 341
  ]
  edge [
    source 53
    target 342
  ]
  edge [
    source 53
    target 343
  ]
  edge [
    source 53
    target 344
  ]
  edge [
    source 53
    target 345
  ]
  edge [
    source 53
    target 346
  ]
  edge [
    source 53
    target 347
  ]
  edge [
    source 53
    target 348
  ]
  edge [
    source 53
    target 349
  ]
  edge [
    source 53
    target 350
  ]
  edge [
    source 53
    target 351
  ]
  edge [
    source 53
    target 352
  ]
  edge [
    source 53
    target 353
  ]
  edge [
    source 53
    target 354
  ]
  edge [
    source 53
    target 355
  ]
  edge [
    source 53
    target 356
  ]
  edge [
    source 53
    target 357
  ]
  edge [
    source 53
    target 358
  ]
  edge [
    source 53
    target 359
  ]
  edge [
    source 53
    target 360
  ]
  edge [
    source 53
    target 361
  ]
  edge [
    source 53
    target 362
  ]
  edge [
    source 53
    target 363
  ]
  edge [
    source 53
    target 364
  ]
  edge [
    source 53
    target 365
  ]
  edge [
    source 53
    target 366
  ]
  edge [
    source 53
    target 367
  ]
  edge [
    source 53
    target 368
  ]
  edge [
    source 53
    target 369
  ]
  edge [
    source 53
    target 370
  ]
  edge [
    source 53
    target 371
  ]
  edge [
    source 53
    target 372
  ]
  edge [
    source 53
    target 373
  ]
  edge [
    source 53
    target 374
  ]
  edge [
    source 53
    target 375
  ]
  edge [
    source 53
    target 376
  ]
  edge [
    source 53
    target 377
  ]
  edge [
    source 53
    target 378
  ]
  edge [
    source 53
    target 233
  ]
  edge [
    source 53
    target 379
  ]
  edge [
    source 53
    target 380
  ]
  edge [
    source 53
    target 381
  ]
  edge [
    source 53
    target 382
  ]
  edge [
    source 53
    target 383
  ]
  edge [
    source 53
    target 384
  ]
  edge [
    source 53
    target 385
  ]
  edge [
    source 53
    target 386
  ]
  edge [
    source 53
    target 387
  ]
  edge [
    source 53
    target 388
  ]
  edge [
    source 53
    target 389
  ]
  edge [
    source 53
    target 390
  ]
  edge [
    source 53
    target 391
  ]
  edge [
    source 53
    target 392
  ]
  edge [
    source 53
    target 393
  ]
  edge [
    source 53
    target 394
  ]
  edge [
    source 53
    target 395
  ]
  edge [
    source 53
    target 396
  ]
  edge [
    source 53
    target 397
  ]
  edge [
    source 53
    target 398
  ]
  edge [
    source 53
    target 399
  ]
  edge [
    source 53
    target 400
  ]
  edge [
    source 53
    target 401
  ]
  edge [
    source 53
    target 402
  ]
  edge [
    source 53
    target 403
  ]
  edge [
    source 53
    target 404
  ]
  edge [
    source 53
    target 405
  ]
  edge [
    source 53
    target 406
  ]
  edge [
    source 53
    target 407
  ]
  edge [
    source 53
    target 408
  ]
  edge [
    source 53
    target 409
  ]
  edge [
    source 53
    target 410
  ]
  edge [
    source 53
    target 411
  ]
  edge [
    source 53
    target 412
  ]
  edge [
    source 53
    target 413
  ]
  edge [
    source 53
    target 414
  ]
  edge [
    source 53
    target 415
  ]
  edge [
    source 53
    target 416
  ]
  edge [
    source 53
    target 417
  ]
  edge [
    source 53
    target 418
  ]
  edge [
    source 53
    target 419
  ]
  edge [
    source 53
    target 420
  ]
  edge [
    source 53
    target 421
  ]
  edge [
    source 53
    target 422
  ]
  edge [
    source 53
    target 423
  ]
  edge [
    source 53
    target 424
  ]
  edge [
    source 53
    target 425
  ]
  edge [
    source 53
    target 426
  ]
  edge [
    source 53
    target 427
  ]
  edge [
    source 53
    target 428
  ]
  edge [
    source 53
    target 429
  ]
  edge [
    source 53
    target 430
  ]
  edge [
    source 53
    target 431
  ]
  edge [
    source 53
    target 432
  ]
  edge [
    source 53
    target 433
  ]
  edge [
    source 53
    target 229
  ]
  edge [
    source 53
    target 434
  ]
  edge [
    source 53
    target 435
  ]
  edge [
    source 53
    target 436
  ]
  edge [
    source 53
    target 437
  ]
  edge [
    source 53
    target 438
  ]
  edge [
    source 53
    target 439
  ]
  edge [
    source 53
    target 440
  ]
  edge [
    source 53
    target 441
  ]
  edge [
    source 53
    target 442
  ]
  edge [
    source 53
    target 443
  ]
  edge [
    source 53
    target 444
  ]
  edge [
    source 53
    target 445
  ]
  edge [
    source 53
    target 446
  ]
  edge [
    source 53
    target 447
  ]
  edge [
    source 53
    target 448
  ]
  edge [
    source 53
    target 449
  ]
  edge [
    source 53
    target 450
  ]
  edge [
    source 53
    target 451
  ]
  edge [
    source 53
    target 452
  ]
  edge [
    source 53
    target 453
  ]
  edge [
    source 53
    target 454
  ]
  edge [
    source 53
    target 81
  ]
  edge [
    source 53
    target 83
  ]
  edge [
    source 53
    target 91
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 455
  ]
  edge [
    source 54
    target 456
  ]
  edge [
    source 54
    target 457
  ]
  edge [
    source 54
    target 458
  ]
  edge [
    source 54
    target 459
  ]
  edge [
    source 54
    target 460
  ]
  edge [
    source 54
    target 461
  ]
  edge [
    source 54
    target 462
  ]
  edge [
    source 54
    target 463
  ]
  edge [
    source 54
    target 464
  ]
  edge [
    source 54
    target 465
  ]
  edge [
    source 54
    target 466
  ]
  edge [
    source 54
    target 467
  ]
  edge [
    source 54
    target 468
  ]
  edge [
    source 54
    target 469
  ]
  edge [
    source 54
    target 72
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 73
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 470
  ]
  edge [
    source 56
    target 471
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 472
  ]
  edge [
    source 59
    target 473
  ]
  edge [
    source 60
    target 474
  ]
  edge [
    source 60
    target 475
  ]
  edge [
    source 60
    target 476
  ]
  edge [
    source 60
    target 477
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 478
  ]
  edge [
    source 61
    target 479
  ]
  edge [
    source 61
    target 480
  ]
  edge [
    source 61
    target 481
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 482
  ]
  edge [
    source 62
    target 483
  ]
  edge [
    source 62
    target 484
  ]
  edge [
    source 62
    target 485
  ]
  edge [
    source 62
    target 486
  ]
  edge [
    source 62
    target 487
  ]
  edge [
    source 62
    target 488
  ]
  edge [
    source 62
    target 489
  ]
  edge [
    source 62
    target 490
  ]
  edge [
    source 62
    target 491
  ]
  edge [
    source 62
    target 492
  ]
  edge [
    source 62
    target 493
  ]
  edge [
    source 62
    target 494
  ]
  edge [
    source 62
    target 495
  ]
  edge [
    source 62
    target 496
  ]
  edge [
    source 62
    target 497
  ]
  edge [
    source 62
    target 498
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 78
  ]
  edge [
    source 63
    target 79
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 499
  ]
  edge [
    source 64
    target 500
  ]
  edge [
    source 64
    target 501
  ]
  edge [
    source 64
    target 502
  ]
  edge [
    source 64
    target 503
  ]
  edge [
    source 64
    target 504
  ]
  edge [
    source 64
    target 505
  ]
  edge [
    source 64
    target 506
  ]
  edge [
    source 64
    target 507
  ]
  edge [
    source 64
    target 508
  ]
  edge [
    source 64
    target 509
  ]
  edge [
    source 64
    target 510
  ]
  edge [
    source 64
    target 511
  ]
  edge [
    source 64
    target 512
  ]
  edge [
    source 64
    target 513
  ]
  edge [
    source 64
    target 514
  ]
  edge [
    source 64
    target 515
  ]
  edge [
    source 64
    target 516
  ]
  edge [
    source 64
    target 517
  ]
  edge [
    source 64
    target 518
  ]
  edge [
    source 64
    target 519
  ]
  edge [
    source 64
    target 520
  ]
  edge [
    source 64
    target 521
  ]
  edge [
    source 64
    target 233
  ]
  edge [
    source 64
    target 522
  ]
  edge [
    source 64
    target 523
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 524
  ]
  edge [
    source 65
    target 525
  ]
  edge [
    source 65
    target 526
  ]
  edge [
    source 65
    target 527
  ]
  edge [
    source 65
    target 528
  ]
  edge [
    source 65
    target 529
  ]
  edge [
    source 65
    target 530
  ]
  edge [
    source 65
    target 531
  ]
  edge [
    source 65
    target 532
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 74
  ]
  edge [
    source 67
    target 75
  ]
  edge [
    source 67
    target 92
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 533
  ]
  edge [
    source 68
    target 534
  ]
  edge [
    source 68
    target 535
  ]
  edge [
    source 68
    target 536
  ]
  edge [
    source 68
    target 537
  ]
  edge [
    source 68
    target 538
  ]
  edge [
    source 69
    target 203
  ]
  edge [
    source 69
    target 539
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 540
  ]
  edge [
    source 70
    target 541
  ]
  edge [
    source 70
    target 542
  ]
  edge [
    source 70
    target 543
  ]
  edge [
    source 71
    target 544
  ]
  edge [
    source 71
    target 545
  ]
  edge [
    source 71
    target 546
  ]
  edge [
    source 71
    target 85
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 547
  ]
  edge [
    source 72
    target 548
  ]
  edge [
    source 72
    target 549
  ]
  edge [
    source 72
    target 550
  ]
  edge [
    source 72
    target 96
  ]
  edge [
    source 72
    target 551
  ]
  edge [
    source 72
    target 97
  ]
  edge [
    source 72
    target 552
  ]
  edge [
    source 72
    target 553
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 271
  ]
  edge [
    source 73
    target 137
  ]
  edge [
    source 73
    target 272
  ]
  edge [
    source 73
    target 273
  ]
  edge [
    source 73
    target 275
  ]
  edge [
    source 73
    target 276
  ]
  edge [
    source 73
    target 277
  ]
  edge [
    source 73
    target 278
  ]
  edge [
    source 73
    target 280
  ]
  edge [
    source 74
    target 554
  ]
  edge [
    source 74
    target 555
  ]
  edge [
    source 74
    target 556
  ]
  edge [
    source 74
    target 557
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 97
  ]
  edge [
    source 75
    target 95
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 558
  ]
  edge [
    source 77
    target 559
  ]
  edge [
    source 77
    target 560
  ]
  edge [
    source 77
    target 561
  ]
  edge [
    source 77
    target 562
  ]
  edge [
    source 78
    target 563
  ]
  edge [
    source 78
    target 564
  ]
  edge [
    source 78
    target 565
  ]
  edge [
    source 78
    target 566
  ]
  edge [
    source 79
    target 567
  ]
  edge [
    source 79
    target 568
  ]
  edge [
    source 79
    target 569
  ]
  edge [
    source 79
    target 570
  ]
  edge [
    source 79
    target 571
  ]
  edge [
    source 79
    target 572
  ]
  edge [
    source 79
    target 573
  ]
  edge [
    source 79
    target 574
  ]
  edge [
    source 79
    target 575
  ]
  edge [
    source 79
    target 576
  ]
  edge [
    source 79
    target 577
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 578
  ]
  edge [
    source 80
    target 579
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 580
  ]
  edge [
    source 81
    target 581
  ]
  edge [
    source 81
    target 582
  ]
  edge [
    source 81
    target 583
  ]
  edge [
    source 81
    target 584
  ]
  edge [
    source 81
    target 585
  ]
  edge [
    source 81
    target 586
  ]
  edge [
    source 81
    target 83
  ]
  edge [
    source 81
    target 91
  ]
  edge [
    source 83
    target 587
  ]
  edge [
    source 83
    target 588
  ]
  edge [
    source 83
    target 589
  ]
  edge [
    source 83
    target 590
  ]
  edge [
    source 83
    target 591
  ]
  edge [
    source 83
    target 91
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 592
  ]
  edge [
    source 84
    target 593
  ]
  edge [
    source 84
    target 227
  ]
  edge [
    source 84
    target 594
  ]
  edge [
    source 84
    target 595
  ]
  edge [
    source 84
    target 596
  ]
  edge [
    source 84
    target 597
  ]
  edge [
    source 84
    target 215
  ]
  edge [
    source 84
    target 598
  ]
  edge [
    source 84
    target 599
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 600
  ]
  edge [
    source 85
    target 601
  ]
  edge [
    source 85
    target 602
  ]
  edge [
    source 85
    target 603
  ]
  edge [
    source 85
    target 604
  ]
  edge [
    source 85
    target 605
  ]
  edge [
    source 85
    target 606
  ]
  edge [
    source 85
    target 607
  ]
  edge [
    source 85
    target 608
  ]
  edge [
    source 85
    target 609
  ]
  edge [
    source 85
    target 610
  ]
  edge [
    source 85
    target 611
  ]
  edge [
    source 85
    target 612
  ]
  edge [
    source 85
    target 613
  ]
  edge [
    source 85
    target 614
  ]
  edge [
    source 85
    target 88
  ]
  edge [
    source 86
    target 282
  ]
  edge [
    source 86
    target 250
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 615
  ]
  edge [
    source 87
    target 616
  ]
  edge [
    source 87
    target 617
  ]
  edge [
    source 87
    target 618
  ]
  edge [
    source 87
    target 619
  ]
  edge [
    source 87
    target 620
  ]
  edge [
    source 87
    target 621
  ]
  edge [
    source 87
    target 622
  ]
  edge [
    source 87
    target 623
  ]
  edge [
    source 87
    target 624
  ]
  edge [
    source 87
    target 625
  ]
  edge [
    source 87
    target 626
  ]
  edge [
    source 87
    target 627
  ]
  edge [
    source 87
    target 628
  ]
  edge [
    source 87
    target 629
  ]
  edge [
    source 87
    target 630
  ]
  edge [
    source 87
    target 631
  ]
  edge [
    source 87
    target 632
  ]
  edge [
    source 87
    target 633
  ]
  edge [
    source 87
    target 634
  ]
  edge [
    source 87
    target 635
  ]
  edge [
    source 87
    target 636
  ]
  edge [
    source 87
    target 637
  ]
  edge [
    source 87
    target 638
  ]
  edge [
    source 87
    target 639
  ]
  edge [
    source 87
    target 640
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 641
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 89
    target 92
  ]
  edge [
    source 89
    target 642
  ]
  edge [
    source 89
    target 612
  ]
  edge [
    source 89
    target 606
  ]
  edge [
    source 89
    target 609
  ]
  edge [
    source 89
    target 646
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 644
    target 645
  ]
  edge [
    source 647
    target 648
  ]
  edge [
    source 647
    target 656
  ]
  edge [
    source 649
    target 650
  ]
  edge [
    source 651
    target 652
  ]
  edge [
    source 651
    target 653
  ]
  edge [
    source 654
    target 655
  ]
  edge [
    source 656
    target 657
  ]
  edge [
    source 656
    target 658
  ]
  edge [
    source 660
    target 661
  ]
  edge [
    source 661
    target 662
  ]
  edge [
    source 661
    target 663
  ]
  edge [
    source 662
    target 663
  ]
  edge [
    source 664
    target 665
  ]
]
