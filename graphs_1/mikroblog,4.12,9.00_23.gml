graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9333333333333333
  density 0.06666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 1
    label "fantazja"
    origin "text"
  ]
  node [
    id 2
    label "wellman"
    origin "text"
  ]
  node [
    id 3
    label "heheszki"
    origin "text"
  ]
  node [
    id 4
    label "boczek"
    origin "text"
  ]
  node [
    id 5
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 6
    label "tvn"
    origin "text"
  ]
  node [
    id 7
    label "czyj&#347;"
  ]
  node [
    id 8
    label "m&#261;&#380;"
  ]
  node [
    id 9
    label "energy"
  ]
  node [
    id 10
    label "vision"
  ]
  node [
    id 11
    label "ch&#281;tka"
  ]
  node [
    id 12
    label "zapa&#322;"
  ]
  node [
    id 13
    label "kraina"
  ]
  node [
    id 14
    label "odwaga"
  ]
  node [
    id 15
    label "wymys&#322;"
  ]
  node [
    id 16
    label "zdolno&#347;&#263;"
  ]
  node [
    id 17
    label "rapsodia"
  ]
  node [
    id 18
    label "imagineskopia"
  ]
  node [
    id 19
    label "utw&#243;r"
  ]
  node [
    id 20
    label "umys&#322;"
  ]
  node [
    id 21
    label "fondness"
  ]
  node [
    id 22
    label "caprice"
  ]
  node [
    id 23
    label "wieprzowy"
  ]
  node [
    id 24
    label "wieprzowina"
  ]
  node [
    id 25
    label "rubryka"
  ]
  node [
    id 26
    label "informacja"
  ]
  node [
    id 27
    label "s&#322;onina"
  ]
  node [
    id 28
    label "margines"
  ]
  node [
    id 29
    label "cholewa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 6
  ]
]
