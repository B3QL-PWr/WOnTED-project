graph [
  maxDegree 73
  minDegree 1
  meanDegree 2.442278860569715
  density 0.0018321671872240923
  graphCliqueNumber 4
  node [
    id 0
    label "poni&#380;ej"
    origin "text"
  ]
  node [
    id 1
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "wst&#281;pny"
    origin "text"
  ]
  node [
    id 4
    label "rozk&#322;ad"
    origin "text"
  ]
  node [
    id 5
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "podr&#281;cznik"
    origin "text"
  ]
  node [
    id 7
    label "fizyka"
    origin "text"
  ]
  node [
    id 8
    label "dla"
    origin "text"
  ]
  node [
    id 9
    label "gimnazjum"
    origin "text"
  ]
  node [
    id 10
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "traktowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "tylko"
    origin "text"
  ]
  node [
    id 13
    label "jako"
    origin "text"
  ]
  node [
    id 14
    label "punkt"
    origin "text"
  ]
  node [
    id 15
    label "odniesienie"
    origin "text"
  ]
  node [
    id 16
    label "dyskusja"
    origin "text"
  ]
  node [
    id 17
    label "wynik"
    origin "text"
  ]
  node [
    id 18
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 19
    label "wszystko"
    origin "text"
  ]
  node [
    id 20
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 21
    label "zmieni&#263;"
    origin "text"
  ]
  node [
    id 22
    label "doda&#263;"
    origin "text"
  ]
  node [
    id 23
    label "nowa"
    origin "text"
  ]
  node [
    id 24
    label "raz"
    origin "text"
  ]
  node [
    id 25
    label "by&#263;"
    origin "text"
  ]
  node [
    id 26
    label "prosty"
    origin "text"
  ]
  node [
    id 27
    label "pr&#243;ba"
    origin "text"
  ]
  node [
    id 28
    label "przet&#322;umaczy&#263;"
    origin "text"
  ]
  node [
    id 29
    label "zawarto&#347;&#263;"
    origin "text"
  ]
  node [
    id 30
    label "podstawa"
    origin "text"
  ]
  node [
    id 31
    label "programowy"
    origin "text"
  ]
  node [
    id 32
    label "spis"
    origin "text"
  ]
  node [
    id 33
    label "wst&#281;p"
    origin "text"
  ]
  node [
    id 34
    label "rola"
    origin "text"
  ]
  node [
    id 35
    label "wsp&#243;&#322;czesny"
    origin "text"
  ]
  node [
    id 36
    label "nauka"
    origin "text"
  ]
  node [
    id 37
    label "do&#347;wiadczalny"
    origin "text"
  ]
  node [
    id 38
    label "do&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 39
    label "wielko&#347;&#263;"
    origin "text"
  ]
  node [
    id 40
    label "fizyczny"
    origin "text"
  ]
  node [
    id 41
    label "mierzy&#263;"
    origin "text"
  ]
  node [
    id 42
    label "niepewno&#347;&#263;"
    origin "text"
  ]
  node [
    id 43
    label "pomiar"
    origin "text"
  ]
  node [
    id 44
    label "uk&#322;ad"
    origin "text"
  ]
  node [
    id 45
    label "osniesienia"
    origin "text"
  ]
  node [
    id 46
    label "po&#322;o&#380;enie"
    origin "text"
  ]
  node [
    id 47
    label "tora"
    origin "text"
  ]
  node [
    id 48
    label "droga"
    origin "text"
  ]
  node [
    id 49
    label "pr&#281;dko&#347;&#263;"
    origin "text"
  ]
  node [
    id 50
    label "&#347;rednia"
    origin "text"
  ]
  node [
    id 51
    label "chwilowy"
    origin "text"
  ]
  node [
    id 52
    label "ruch"
    origin "text"
  ]
  node [
    id 53
    label "jednostajny"
    origin "text"
  ]
  node [
    id 54
    label "niejednostajny"
    origin "text"
  ]
  node [
    id 55
    label "jednostajnie"
    origin "text"
  ]
  node [
    id 56
    label "zmienny"
    origin "text"
  ]
  node [
    id 57
    label "przyspieszenie"
    origin "text"
  ]
  node [
    id 58
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 59
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 60
    label "grawitacja"
    origin "text"
  ]
  node [
    id 61
    label "spr&#281;&#380;ysto&#347;&#263;"
    origin "text"
  ]
  node [
    id 62
    label "op&#243;r"
    origin "text"
  ]
  node [
    id 63
    label "skutek"
    origin "text"
  ]
  node [
    id 64
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 65
    label "zasada"
    origin "text"
  ]
  node [
    id 66
    label "dynamika"
    origin "text"
  ]
  node [
    id 67
    label "newton"
    origin "text"
  ]
  node [
    id 68
    label "iii"
    origin "text"
  ]
  node [
    id 69
    label "w&#322;a&#347;ciwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 70
    label "budowa"
    origin "text"
  ]
  node [
    id 71
    label "materia"
    origin "text"
  ]
  node [
    id 72
    label "trzy"
    origin "text"
  ]
  node [
    id 73
    label "stany"
    origin "text"
  ]
  node [
    id 74
    label "skupienie"
    origin "text"
  ]
  node [
    id 75
    label "przemian"
    origin "text"
  ]
  node [
    id 76
    label "fazowy"
    origin "text"
  ]
  node [
    id 77
    label "mikroskopowy"
    origin "text"
  ]
  node [
    id 78
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 79
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 80
    label "energia"
    origin "text"
  ]
  node [
    id 81
    label "kinetyczny"
    origin "text"
  ]
  node [
    id 82
    label "cz&#261;steczka"
    origin "text"
  ]
  node [
    id 83
    label "temperatura"
    origin "text"
  ]
  node [
    id 84
    label "elektryzowa&#263;"
    origin "text"
  ]
  node [
    id 85
    label "cia&#322;o"
    origin "text"
  ]
  node [
    id 86
    label "&#322;adunek"
    origin "text"
  ]
  node [
    id 87
    label "elektryczny"
    origin "text"
  ]
  node [
    id 88
    label "prawo"
    origin "text"
  ]
  node [
    id 89
    label "coulomb"
    origin "text"
  ]
  node [
    id 90
    label "przewodnik"
    origin "text"
  ]
  node [
    id 91
    label "izolator"
    origin "text"
  ]
  node [
    id 92
    label "pr&#261;d"
    origin "text"
  ]
  node [
    id 93
    label "nat&#281;&#380;enie"
    origin "text"
  ]
  node [
    id 94
    label "magnes"
    origin "text"
  ]
  node [
    id 95
    label "biegun"
    origin "text"
  ]
  node [
    id 96
    label "kompas"
    origin "text"
  ]
  node [
    id 97
    label "odzia&#322;ywanie"
    origin "text"
  ]
  node [
    id 98
    label "ig&#322;a"
    origin "text"
  ]
  node [
    id 99
    label "magnetyczny"
    origin "text"
  ]
  node [
    id 100
    label "elektromagnes"
    origin "text"
  ]
  node [
    id 101
    label "silnik"
    origin "text"
  ]
  node [
    id 102
    label "sta&#322;y"
    origin "text"
  ]
  node [
    id 103
    label "drgania"
    origin "text"
  ]
  node [
    id 104
    label "fala"
    origin "text"
  ]
  node [
    id 105
    label "opis"
    origin "text"
  ]
  node [
    id 106
    label "amplituda"
    origin "text"
  ]
  node [
    id 107
    label "okres"
    origin "text"
  ]
  node [
    id 108
    label "cz&#281;stotliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 109
    label "r&#243;wnowaga"
    origin "text"
  ]
  node [
    id 110
    label "drga&#263;"
    origin "text"
  ]
  node [
    id 111
    label "rozchodzi&#263;"
    origin "text"
  ]
  node [
    id 112
    label "d&#322;ugo&#347;&#263;"
    origin "text"
  ]
  node [
    id 113
    label "&#347;wiat&#322;o"
    origin "text"
  ]
  node [
    id 114
    label "elektromagntyczna"
    origin "text"
  ]
  node [
    id 115
    label "odbicie"
    origin "text"
  ]
  node [
    id 116
    label "za&#322;amanie"
    origin "text"
  ]
  node [
    id 117
    label "rozproszenie"
    origin "text"
  ]
  node [
    id 118
    label "kolor"
    origin "text"
  ]
  node [
    id 119
    label "pryzmat"
    origin "text"
  ]
  node [
    id 120
    label "optyk"
    origin "text"
  ]
  node [
    id 121
    label "geometryczny"
    origin "text"
  ]
  node [
    id 122
    label "cie&#324;"
    origin "text"
  ]
  node [
    id 123
    label "p&#243;&#322;cie&#324;"
    origin "text"
  ]
  node [
    id 124
    label "zwierciad&#322;o"
    origin "text"
  ]
  node [
    id 125
    label "p&#322;aski"
    origin "text"
  ]
  node [
    id 126
    label "wkl&#281;s&#322;y"
    origin "text"
  ]
  node [
    id 127
    label "ognisko"
    origin "text"
  ]
  node [
    id 128
    label "ogniskowa"
    origin "text"
  ]
  node [
    id 129
    label "soczewka"
    origin "text"
  ]
  node [
    id 130
    label "skupia&#263;"
    origin "text"
  ]
  node [
    id 131
    label "rozprasza&#263;"
    origin "text"
  ]
  node [
    id 132
    label "obraz"
    origin "text"
  ]
  node [
    id 133
    label "rzeczywisty"
    origin "text"
  ]
  node [
    id 134
    label "pozorny"
    origin "text"
  ]
  node [
    id 135
    label "kr&#243;tkowzroczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 136
    label "dalekowzroczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 137
    label "rozszczepienie"
    origin "text"
  ]
  node [
    id 138
    label "&#347;wiat&#322;a"
    origin "text"
  ]
  node [
    id 139
    label "nast&#281;pnie"
  ]
  node [
    id 140
    label "poni&#380;szy"
  ]
  node [
    id 141
    label "doznawa&#263;"
  ]
  node [
    id 142
    label "znachodzi&#263;"
  ]
  node [
    id 143
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 144
    label "pozyskiwa&#263;"
  ]
  node [
    id 145
    label "odzyskiwa&#263;"
  ]
  node [
    id 146
    label "os&#261;dza&#263;"
  ]
  node [
    id 147
    label "wykrywa&#263;"
  ]
  node [
    id 148
    label "unwrap"
  ]
  node [
    id 149
    label "detect"
  ]
  node [
    id 150
    label "wymy&#347;la&#263;"
  ]
  node [
    id 151
    label "powodowa&#263;"
  ]
  node [
    id 152
    label "szkicowy"
  ]
  node [
    id 153
    label "wst&#281;pnie"
  ]
  node [
    id 154
    label "pocz&#261;tkowy"
  ]
  node [
    id 155
    label "linea&#380;"
  ]
  node [
    id 156
    label "ojcowie"
  ]
  node [
    id 157
    label "antecesor"
  ]
  node [
    id 158
    label "krewny"
  ]
  node [
    id 159
    label "dziad"
  ]
  node [
    id 160
    label "u&#322;o&#380;enie"
  ]
  node [
    id 161
    label "miara_probabilistyczna"
  ]
  node [
    id 162
    label "reticule"
  ]
  node [
    id 163
    label "katabolizm"
  ]
  node [
    id 164
    label "wyst&#281;powanie"
  ]
  node [
    id 165
    label "zwierzyna"
  ]
  node [
    id 166
    label "dissociation"
  ]
  node [
    id 167
    label "cecha"
  ]
  node [
    id 168
    label "czas_p&#243;&#322;trwania"
  ]
  node [
    id 169
    label "inclination"
  ]
  node [
    id 170
    label "manner"
  ]
  node [
    id 171
    label "dissolution"
  ]
  node [
    id 172
    label "kondycja"
  ]
  node [
    id 173
    label "proces_chemiczny"
  ]
  node [
    id 174
    label "plan"
  ]
  node [
    id 175
    label "proces"
  ]
  node [
    id 176
    label "rozmieszczenie"
  ]
  node [
    id 177
    label "reducent"
  ]
  node [
    id 178
    label "proces_fizyczny"
  ]
  node [
    id 179
    label "antykataboliczny"
  ]
  node [
    id 180
    label "temat"
  ]
  node [
    id 181
    label "istota"
  ]
  node [
    id 182
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 183
    label "informacja"
  ]
  node [
    id 184
    label "wyprawka"
  ]
  node [
    id 185
    label "pomoc_naukowa"
  ]
  node [
    id 186
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 187
    label "optyka"
  ]
  node [
    id 188
    label "akustyka"
  ]
  node [
    id 189
    label "termodynamika"
  ]
  node [
    id 190
    label "fizyka_cia&#322;a_sta&#322;ego"
  ]
  node [
    id 191
    label "przedmiot"
  ]
  node [
    id 192
    label "fizyka_kwantowa"
  ]
  node [
    id 193
    label "dylatometria"
  ]
  node [
    id 194
    label "elektrokinetyka"
  ]
  node [
    id 195
    label "elektryka"
  ]
  node [
    id 196
    label "fizyka_teoretyczna"
  ]
  node [
    id 197
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 198
    label "dozymetria"
  ]
  node [
    id 199
    label "teoria_p&#243;l_kwantowych"
  ]
  node [
    id 200
    label "elektrodynamika"
  ]
  node [
    id 201
    label "fizyka_atomowa"
  ]
  node [
    id 202
    label "fizyka_medyczna"
  ]
  node [
    id 203
    label "fizyka_molekularna"
  ]
  node [
    id 204
    label "elektromagnetyzm"
  ]
  node [
    id 205
    label "pr&#243;&#380;nia"
  ]
  node [
    id 206
    label "fizyka_statystyczna"
  ]
  node [
    id 207
    label "kriofizyka"
  ]
  node [
    id 208
    label "mikrofizyka"
  ]
  node [
    id 209
    label "fiza"
  ]
  node [
    id 210
    label "elektrostatyka"
  ]
  node [
    id 211
    label "interferometria"
  ]
  node [
    id 212
    label "agrofizyka"
  ]
  node [
    id 213
    label "elektryczno&#347;&#263;"
  ]
  node [
    id 214
    label "teoria_pola"
  ]
  node [
    id 215
    label "kierunek"
  ]
  node [
    id 216
    label "fizyka_plazmy"
  ]
  node [
    id 217
    label "spektroskopia"
  ]
  node [
    id 218
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 219
    label "mechanika"
  ]
  node [
    id 220
    label "chemia_powierzchni"
  ]
  node [
    id 221
    label "geofizyka"
  ]
  node [
    id 222
    label "nauka_przyrodnicza"
  ]
  node [
    id 223
    label "rentgenologia"
  ]
  node [
    id 224
    label "miejsce"
  ]
  node [
    id 225
    label "szko&#322;a"
  ]
  node [
    id 226
    label "szko&#322;a_ponadpodstawowa"
  ]
  node [
    id 227
    label "trwa&#263;"
  ]
  node [
    id 228
    label "zezwala&#263;"
  ]
  node [
    id 229
    label "ask"
  ]
  node [
    id 230
    label "invite"
  ]
  node [
    id 231
    label "zach&#281;ca&#263;"
  ]
  node [
    id 232
    label "preach"
  ]
  node [
    id 233
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 234
    label "pies"
  ]
  node [
    id 235
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 236
    label "poleca&#263;"
  ]
  node [
    id 237
    label "zaprasza&#263;"
  ]
  node [
    id 238
    label "suffice"
  ]
  node [
    id 239
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 240
    label "use"
  ]
  node [
    id 241
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 242
    label "robi&#263;"
  ]
  node [
    id 243
    label "dotyczy&#263;"
  ]
  node [
    id 244
    label "poddawa&#263;"
  ]
  node [
    id 245
    label "prosta"
  ]
  node [
    id 246
    label "chwila"
  ]
  node [
    id 247
    label "ust&#281;p"
  ]
  node [
    id 248
    label "problemat"
  ]
  node [
    id 249
    label "kres"
  ]
  node [
    id 250
    label "mark"
  ]
  node [
    id 251
    label "pozycja"
  ]
  node [
    id 252
    label "point"
  ]
  node [
    id 253
    label "stopie&#324;_pisma"
  ]
  node [
    id 254
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 255
    label "przestrze&#324;"
  ]
  node [
    id 256
    label "wojsko"
  ]
  node [
    id 257
    label "problematyka"
  ]
  node [
    id 258
    label "zapunktowa&#263;"
  ]
  node [
    id 259
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 260
    label "obiekt_matematyczny"
  ]
  node [
    id 261
    label "sprawa"
  ]
  node [
    id 262
    label "plamka"
  ]
  node [
    id 263
    label "obiekt"
  ]
  node [
    id 264
    label "podpunkt"
  ]
  node [
    id 265
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 266
    label "jednostka"
  ]
  node [
    id 267
    label "wypowied&#378;"
  ]
  node [
    id 268
    label "od&#322;o&#380;enie"
  ]
  node [
    id 269
    label "skill"
  ]
  node [
    id 270
    label "doznanie"
  ]
  node [
    id 271
    label "gaze"
  ]
  node [
    id 272
    label "deference"
  ]
  node [
    id 273
    label "dochrapanie_si&#281;"
  ]
  node [
    id 274
    label "dostarczenie"
  ]
  node [
    id 275
    label "mention"
  ]
  node [
    id 276
    label "bearing"
  ]
  node [
    id 277
    label "po&#380;yczenie"
  ]
  node [
    id 278
    label "cel"
  ]
  node [
    id 279
    label "uzyskanie"
  ]
  node [
    id 280
    label "oddanie"
  ]
  node [
    id 281
    label "powi&#261;zanie"
  ]
  node [
    id 282
    label "rozmowa"
  ]
  node [
    id 283
    label "sympozjon"
  ]
  node [
    id 284
    label "conference"
  ]
  node [
    id 285
    label "typ"
  ]
  node [
    id 286
    label "dzia&#322;anie"
  ]
  node [
    id 287
    label "przyczyna"
  ]
  node [
    id 288
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 289
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 290
    label "zaokr&#261;glenie"
  ]
  node [
    id 291
    label "event"
  ]
  node [
    id 292
    label "rezultat"
  ]
  node [
    id 293
    label "lock"
  ]
  node [
    id 294
    label "absolut"
  ]
  node [
    id 295
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 296
    label "uprawi&#263;"
  ]
  node [
    id 297
    label "gotowy"
  ]
  node [
    id 298
    label "might"
  ]
  node [
    id 299
    label "come_up"
  ]
  node [
    id 300
    label "straci&#263;"
  ]
  node [
    id 301
    label "przej&#347;&#263;"
  ]
  node [
    id 302
    label "zast&#261;pi&#263;"
  ]
  node [
    id 303
    label "sprawi&#263;"
  ]
  node [
    id 304
    label "zyska&#263;"
  ]
  node [
    id 305
    label "zrobi&#263;"
  ]
  node [
    id 306
    label "change"
  ]
  node [
    id 307
    label "nada&#263;"
  ]
  node [
    id 308
    label "policzy&#263;"
  ]
  node [
    id 309
    label "complete"
  ]
  node [
    id 310
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 311
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 312
    label "set"
  ]
  node [
    id 313
    label "gwiazda"
  ]
  node [
    id 314
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 315
    label "uderzenie"
  ]
  node [
    id 316
    label "cios"
  ]
  node [
    id 317
    label "time"
  ]
  node [
    id 318
    label "si&#281;ga&#263;"
  ]
  node [
    id 319
    label "obecno&#347;&#263;"
  ]
  node [
    id 320
    label "stan"
  ]
  node [
    id 321
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 322
    label "stand"
  ]
  node [
    id 323
    label "mie&#263;_miejsce"
  ]
  node [
    id 324
    label "uczestniczy&#263;"
  ]
  node [
    id 325
    label "chodzi&#263;"
  ]
  node [
    id 326
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 327
    label "equal"
  ]
  node [
    id 328
    label "&#322;atwy"
  ]
  node [
    id 329
    label "prostowanie_si&#281;"
  ]
  node [
    id 330
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 331
    label "rozprostowanie"
  ]
  node [
    id 332
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 333
    label "prostoduszny"
  ]
  node [
    id 334
    label "naturalny"
  ]
  node [
    id 335
    label "naiwny"
  ]
  node [
    id 336
    label "prostowanie"
  ]
  node [
    id 337
    label "niepozorny"
  ]
  node [
    id 338
    label "zwyk&#322;y"
  ]
  node [
    id 339
    label "prosto"
  ]
  node [
    id 340
    label "po_prostu"
  ]
  node [
    id 341
    label "skromny"
  ]
  node [
    id 342
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 343
    label "usi&#322;owanie"
  ]
  node [
    id 344
    label "pobiera&#263;"
  ]
  node [
    id 345
    label "spotkanie"
  ]
  node [
    id 346
    label "analiza_chemiczna"
  ]
  node [
    id 347
    label "test"
  ]
  node [
    id 348
    label "znak"
  ]
  node [
    id 349
    label "item"
  ]
  node [
    id 350
    label "ilo&#347;&#263;"
  ]
  node [
    id 351
    label "effort"
  ]
  node [
    id 352
    label "czynno&#347;&#263;"
  ]
  node [
    id 353
    label "metal_szlachetny"
  ]
  node [
    id 354
    label "pobranie"
  ]
  node [
    id 355
    label "pobieranie"
  ]
  node [
    id 356
    label "sytuacja"
  ]
  node [
    id 357
    label "probiernictwo"
  ]
  node [
    id 358
    label "zbi&#243;r"
  ]
  node [
    id 359
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 360
    label "pobra&#263;"
  ]
  node [
    id 361
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 362
    label "frame"
  ]
  node [
    id 363
    label "zinterpretowa&#263;"
  ]
  node [
    id 364
    label "j&#281;zyk"
  ]
  node [
    id 365
    label "przekona&#263;"
  ]
  node [
    id 366
    label "put"
  ]
  node [
    id 367
    label "wn&#281;trze"
  ]
  node [
    id 368
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 369
    label "podstawowy"
  ]
  node [
    id 370
    label "strategia"
  ]
  node [
    id 371
    label "pot&#281;ga"
  ]
  node [
    id 372
    label "zasadzenie"
  ]
  node [
    id 373
    label "za&#322;o&#380;enie"
  ]
  node [
    id 374
    label "&#347;ciana"
  ]
  node [
    id 375
    label "documentation"
  ]
  node [
    id 376
    label "dzieci&#281;ctwo"
  ]
  node [
    id 377
    label "pomys&#322;"
  ]
  node [
    id 378
    label "bok"
  ]
  node [
    id 379
    label "d&#243;&#322;"
  ]
  node [
    id 380
    label "punkt_odniesienia"
  ]
  node [
    id 381
    label "column"
  ]
  node [
    id 382
    label "zasadzi&#263;"
  ]
  node [
    id 383
    label "background"
  ]
  node [
    id 384
    label "zaplanowany"
  ]
  node [
    id 385
    label "kierunkowy"
  ]
  node [
    id 386
    label "zdeklarowany"
  ]
  node [
    id 387
    label "przewidywalny"
  ]
  node [
    id 388
    label "wa&#380;ny"
  ]
  node [
    id 389
    label "reprezentatywny"
  ]
  node [
    id 390
    label "celowy"
  ]
  node [
    id 391
    label "programowo"
  ]
  node [
    id 392
    label "wyliczanka"
  ]
  node [
    id 393
    label "catalog"
  ]
  node [
    id 394
    label "stock"
  ]
  node [
    id 395
    label "akt"
  ]
  node [
    id 396
    label "figurowa&#263;"
  ]
  node [
    id 397
    label "book"
  ]
  node [
    id 398
    label "tekst"
  ]
  node [
    id 399
    label "sumariusz"
  ]
  node [
    id 400
    label "doj&#347;cie"
  ]
  node [
    id 401
    label "zapowied&#378;"
  ]
  node [
    id 402
    label "evocation"
  ]
  node [
    id 403
    label "g&#322;oska"
  ]
  node [
    id 404
    label "wymowa"
  ]
  node [
    id 405
    label "pocz&#261;tek"
  ]
  node [
    id 406
    label "utw&#243;r"
  ]
  node [
    id 407
    label "podstawy"
  ]
  node [
    id 408
    label "pole"
  ]
  node [
    id 409
    label "znaczenie"
  ]
  node [
    id 410
    label "ziemia"
  ]
  node [
    id 411
    label "sk&#322;ad"
  ]
  node [
    id 412
    label "zastosowanie"
  ]
  node [
    id 413
    label "zreinterpretowa&#263;"
  ]
  node [
    id 414
    label "zreinterpretowanie"
  ]
  node [
    id 415
    label "function"
  ]
  node [
    id 416
    label "zagranie"
  ]
  node [
    id 417
    label "p&#322;osa"
  ]
  node [
    id 418
    label "plik"
  ]
  node [
    id 419
    label "reinterpretowanie"
  ]
  node [
    id 420
    label "wykonywa&#263;"
  ]
  node [
    id 421
    label "uprawienie"
  ]
  node [
    id 422
    label "gra&#263;"
  ]
  node [
    id 423
    label "radlina"
  ]
  node [
    id 424
    label "ustawi&#263;"
  ]
  node [
    id 425
    label "irygowa&#263;"
  ]
  node [
    id 426
    label "wrench"
  ]
  node [
    id 427
    label "irygowanie"
  ]
  node [
    id 428
    label "dialog"
  ]
  node [
    id 429
    label "zagon"
  ]
  node [
    id 430
    label "scenariusz"
  ]
  node [
    id 431
    label "zagra&#263;"
  ]
  node [
    id 432
    label "kszta&#322;t"
  ]
  node [
    id 433
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 434
    label "ustawienie"
  ]
  node [
    id 435
    label "czyn"
  ]
  node [
    id 436
    label "gospodarstwo"
  ]
  node [
    id 437
    label "reinterpretowa&#263;"
  ]
  node [
    id 438
    label "granie"
  ]
  node [
    id 439
    label "wykonywanie"
  ]
  node [
    id 440
    label "aktorstwo"
  ]
  node [
    id 441
    label "kostium"
  ]
  node [
    id 442
    label "posta&#263;"
  ]
  node [
    id 443
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 444
    label "tera&#378;niejszy"
  ]
  node [
    id 445
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 446
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 447
    label "unowocze&#347;nianie"
  ]
  node [
    id 448
    label "jednoczesny"
  ]
  node [
    id 449
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 450
    label "nauki_o_Ziemi"
  ]
  node [
    id 451
    label "teoria_naukowa"
  ]
  node [
    id 452
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 453
    label "nauki_o_poznaniu"
  ]
  node [
    id 454
    label "nomotetyczny"
  ]
  node [
    id 455
    label "metodologia"
  ]
  node [
    id 456
    label "przem&#243;wienie"
  ]
  node [
    id 457
    label "wiedza"
  ]
  node [
    id 458
    label "kultura_duchowa"
  ]
  node [
    id 459
    label "nauki_penalne"
  ]
  node [
    id 460
    label "systematyka"
  ]
  node [
    id 461
    label "inwentyka"
  ]
  node [
    id 462
    label "dziedzina"
  ]
  node [
    id 463
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 464
    label "miasteczko_rowerowe"
  ]
  node [
    id 465
    label "fotowoltaika"
  ]
  node [
    id 466
    label "porada"
  ]
  node [
    id 467
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 468
    label "imagineskopia"
  ]
  node [
    id 469
    label "typologia"
  ]
  node [
    id 470
    label "&#322;awa_szkolna"
  ]
  node [
    id 471
    label "do&#347;wiadczalnie"
  ]
  node [
    id 472
    label "praktyczny"
  ]
  node [
    id 473
    label "badawczy"
  ]
  node [
    id 474
    label "zbadanie"
  ]
  node [
    id 475
    label "wy&#347;wiadczenie"
  ]
  node [
    id 476
    label "znawstwo"
  ]
  node [
    id 477
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 478
    label "poczucie"
  ]
  node [
    id 479
    label "do&#347;wiadczanie"
  ]
  node [
    id 480
    label "wydarzenie"
  ]
  node [
    id 481
    label "badanie"
  ]
  node [
    id 482
    label "assay"
  ]
  node [
    id 483
    label "obserwowanie"
  ]
  node [
    id 484
    label "checkup"
  ]
  node [
    id 485
    label "potraktowanie"
  ]
  node [
    id 486
    label "eksperiencja"
  ]
  node [
    id 487
    label "dymensja"
  ]
  node [
    id 488
    label "opinia"
  ]
  node [
    id 489
    label "warunek_lokalowy"
  ]
  node [
    id 490
    label "property"
  ]
  node [
    id 491
    label "rzadko&#347;&#263;"
  ]
  node [
    id 492
    label "rozmiar"
  ]
  node [
    id 493
    label "zdolno&#347;&#263;"
  ]
  node [
    id 494
    label "measure"
  ]
  node [
    id 495
    label "liczba"
  ]
  node [
    id 496
    label "poj&#281;cie"
  ]
  node [
    id 497
    label "potencja"
  ]
  node [
    id 498
    label "zaleta"
  ]
  node [
    id 499
    label "fizykalnie"
  ]
  node [
    id 500
    label "fizycznie"
  ]
  node [
    id 501
    label "materializowanie"
  ]
  node [
    id 502
    label "pracownik"
  ]
  node [
    id 503
    label "gimnastyczny"
  ]
  node [
    id 504
    label "widoczny"
  ]
  node [
    id 505
    label "namacalny"
  ]
  node [
    id 506
    label "zmaterializowanie"
  ]
  node [
    id 507
    label "organiczny"
  ]
  node [
    id 508
    label "materjalny"
  ]
  node [
    id 509
    label "take"
  ]
  node [
    id 510
    label "okre&#347;la&#263;"
  ]
  node [
    id 511
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 512
    label "wytw&#243;r"
  ]
  node [
    id 513
    label "niespokojno&#347;&#263;"
  ]
  node [
    id 514
    label "akatyzja"
  ]
  node [
    id 515
    label "emocja"
  ]
  node [
    id 516
    label "w&#261;tpliwo&#347;&#263;"
  ]
  node [
    id 517
    label "nie&#347;mia&#322;o&#347;&#263;"
  ]
  node [
    id 518
    label "question"
  ]
  node [
    id 519
    label "w&#261;tpienie"
  ]
  node [
    id 520
    label "dynamometryczny"
  ]
  node [
    id 521
    label "pomiara"
  ]
  node [
    id 522
    label "wskazanie"
  ]
  node [
    id 523
    label "survey"
  ]
  node [
    id 524
    label "zachowanie"
  ]
  node [
    id 525
    label "umowa"
  ]
  node [
    id 526
    label "podsystem"
  ]
  node [
    id 527
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 528
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 529
    label "system"
  ]
  node [
    id 530
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 531
    label "struktura"
  ]
  node [
    id 532
    label "wi&#281;&#378;"
  ]
  node [
    id 533
    label "zawarcie"
  ]
  node [
    id 534
    label "systemat"
  ]
  node [
    id 535
    label "usenet"
  ]
  node [
    id 536
    label "ONZ"
  ]
  node [
    id 537
    label "o&#347;"
  ]
  node [
    id 538
    label "organ"
  ]
  node [
    id 539
    label "przestawi&#263;"
  ]
  node [
    id 540
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 541
    label "traktat_wersalski"
  ]
  node [
    id 542
    label "rozprz&#261;c"
  ]
  node [
    id 543
    label "cybernetyk"
  ]
  node [
    id 544
    label "zawrze&#263;"
  ]
  node [
    id 545
    label "konstelacja"
  ]
  node [
    id 546
    label "alliance"
  ]
  node [
    id 547
    label "NATO"
  ]
  node [
    id 548
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 549
    label "treaty"
  ]
  node [
    id 550
    label "pokrycie"
  ]
  node [
    id 551
    label "zepsucie"
  ]
  node [
    id 552
    label "ugoszczenie"
  ]
  node [
    id 553
    label "nak&#322;adzenie"
  ]
  node [
    id 554
    label "wygranie"
  ]
  node [
    id 555
    label "adres"
  ]
  node [
    id 556
    label "le&#380;enie"
  ]
  node [
    id 557
    label "presentation"
  ]
  node [
    id 558
    label "pora&#380;ka"
  ]
  node [
    id 559
    label "le&#380;e&#263;"
  ]
  node [
    id 560
    label "trim"
  ]
  node [
    id 561
    label "zabicie"
  ]
  node [
    id 562
    label "spowodowanie"
  ]
  node [
    id 563
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 564
    label "pouk&#322;adanie"
  ]
  node [
    id 565
    label "przenocowanie"
  ]
  node [
    id 566
    label "reading"
  ]
  node [
    id 567
    label "zbudowanie"
  ]
  node [
    id 568
    label "umieszczenie"
  ]
  node [
    id 569
    label "zw&#243;j"
  ]
  node [
    id 570
    label "Tora"
  ]
  node [
    id 571
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 572
    label "journey"
  ]
  node [
    id 573
    label "podbieg"
  ]
  node [
    id 574
    label "bezsilnikowy"
  ]
  node [
    id 575
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 576
    label "wylot"
  ]
  node [
    id 577
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 578
    label "drogowskaz"
  ]
  node [
    id 579
    label "nawierzchnia"
  ]
  node [
    id 580
    label "turystyka"
  ]
  node [
    id 581
    label "budowla"
  ]
  node [
    id 582
    label "spos&#243;b"
  ]
  node [
    id 583
    label "passage"
  ]
  node [
    id 584
    label "marszrutyzacja"
  ]
  node [
    id 585
    label "zbior&#243;wka"
  ]
  node [
    id 586
    label "ekskursja"
  ]
  node [
    id 587
    label "rajza"
  ]
  node [
    id 588
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 589
    label "trasa"
  ]
  node [
    id 590
    label "wyb&#243;j"
  ]
  node [
    id 591
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 592
    label "ekwipunek"
  ]
  node [
    id 593
    label "korona_drogi"
  ]
  node [
    id 594
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 595
    label "pobocze"
  ]
  node [
    id 596
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 597
    label "celerity"
  ]
  node [
    id 598
    label "tempo"
  ]
  node [
    id 599
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 600
    label "miara_tendencji_centralnej"
  ]
  node [
    id 601
    label "chwilowo"
  ]
  node [
    id 602
    label "momentalny"
  ]
  node [
    id 603
    label "kr&#243;tkotrwa&#322;y"
  ]
  node [
    id 604
    label "przemijaj&#261;cy"
  ]
  node [
    id 605
    label "czasowy"
  ]
  node [
    id 606
    label "manewr"
  ]
  node [
    id 607
    label "model"
  ]
  node [
    id 608
    label "movement"
  ]
  node [
    id 609
    label "apraksja"
  ]
  node [
    id 610
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 611
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 612
    label "poruszenie"
  ]
  node [
    id 613
    label "commercial_enterprise"
  ]
  node [
    id 614
    label "dyssypacja_energii"
  ]
  node [
    id 615
    label "zmiana"
  ]
  node [
    id 616
    label "utrzymanie"
  ]
  node [
    id 617
    label "utrzyma&#263;"
  ]
  node [
    id 618
    label "komunikacja"
  ]
  node [
    id 619
    label "tumult"
  ]
  node [
    id 620
    label "kr&#243;tki"
  ]
  node [
    id 621
    label "drift"
  ]
  node [
    id 622
    label "utrzymywa&#263;"
  ]
  node [
    id 623
    label "stopek"
  ]
  node [
    id 624
    label "kanciasty"
  ]
  node [
    id 625
    label "d&#322;ugi"
  ]
  node [
    id 626
    label "zjawisko"
  ]
  node [
    id 627
    label "utrzymywanie"
  ]
  node [
    id 628
    label "myk"
  ]
  node [
    id 629
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 630
    label "taktyka"
  ]
  node [
    id 631
    label "move"
  ]
  node [
    id 632
    label "natural_process"
  ]
  node [
    id 633
    label "lokomocja"
  ]
  node [
    id 634
    label "strumie&#324;"
  ]
  node [
    id 635
    label "aktywno&#347;&#263;"
  ]
  node [
    id 636
    label "travel"
  ]
  node [
    id 637
    label "ci&#261;g&#322;y"
  ]
  node [
    id 638
    label "jednolity"
  ]
  node [
    id 639
    label "jednotonny"
  ]
  node [
    id 640
    label "jednakowy"
  ]
  node [
    id 641
    label "zgodny"
  ]
  node [
    id 642
    label "spokojny"
  ]
  node [
    id 643
    label "niejednakowy"
  ]
  node [
    id 644
    label "jednakowo"
  ]
  node [
    id 645
    label "spokojnie"
  ]
  node [
    id 646
    label "ci&#261;gle"
  ]
  node [
    id 647
    label "nudno"
  ]
  node [
    id 648
    label "bezbarwnie"
  ]
  node [
    id 649
    label "zgodnie"
  ]
  node [
    id 650
    label "zmiennie"
  ]
  node [
    id 651
    label "chor&#261;giewka_na_wietrze"
  ]
  node [
    id 652
    label "pickup"
  ]
  node [
    id 653
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 654
    label "powi&#281;kszenie"
  ]
  node [
    id 655
    label "boost"
  ]
  node [
    id 656
    label "cz&#322;owiek"
  ]
  node [
    id 657
    label "przedstawiciel"
  ]
  node [
    id 658
    label "ilustracja"
  ]
  node [
    id 659
    label "fakt"
  ]
  node [
    id 660
    label "magnitude"
  ]
  node [
    id 661
    label "capacity"
  ]
  node [
    id 662
    label "wuchta"
  ]
  node [
    id 663
    label "parametr"
  ]
  node [
    id 664
    label "moment_si&#322;y"
  ]
  node [
    id 665
    label "przemoc"
  ]
  node [
    id 666
    label "mn&#243;stwo"
  ]
  node [
    id 667
    label "rozwi&#261;zanie"
  ]
  node [
    id 668
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 669
    label "oddzia&#322;ywanie_podstawowe"
  ]
  node [
    id 670
    label "grawiton"
  ]
  node [
    id 671
    label "masa_grawitacyjna"
  ]
  node [
    id 672
    label "elasticity"
  ]
  node [
    id 673
    label "w&#322;a&#347;ciwo&#347;&#263;_fizyczna"
  ]
  node [
    id 674
    label "elastyczno&#347;&#263;"
  ]
  node [
    id 675
    label "reakcja"
  ]
  node [
    id 676
    label "resistance"
  ]
  node [
    id 677
    label "impedancja"
  ]
  node [
    id 678
    label "reluctance"
  ]
  node [
    id 679
    label "immunity"
  ]
  node [
    id 680
    label "opory_ruchu"
  ]
  node [
    id 681
    label "niech&#281;&#263;"
  ]
  node [
    id 682
    label "protestacja"
  ]
  node [
    id 683
    label "czerwona_kartka"
  ]
  node [
    id 684
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 685
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 686
    label "obserwacja"
  ]
  node [
    id 687
    label "moralno&#347;&#263;"
  ]
  node [
    id 688
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 689
    label "dominion"
  ]
  node [
    id 690
    label "qualification"
  ]
  node [
    id 691
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 692
    label "regu&#322;a_Allena"
  ]
  node [
    id 693
    label "normalizacja"
  ]
  node [
    id 694
    label "regu&#322;a_Glogera"
  ]
  node [
    id 695
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 696
    label "standard"
  ]
  node [
    id 697
    label "base"
  ]
  node [
    id 698
    label "substancja"
  ]
  node [
    id 699
    label "prawid&#322;o"
  ]
  node [
    id 700
    label "prawo_Mendla"
  ]
  node [
    id 701
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 702
    label "criterion"
  ]
  node [
    id 703
    label "twierdzenie"
  ]
  node [
    id 704
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 705
    label "occupation"
  ]
  node [
    id 706
    label "zasada_d'Alemberta"
  ]
  node [
    id 707
    label "muzykologia"
  ]
  node [
    id 708
    label "g&#322;o&#347;no&#347;&#263;"
  ]
  node [
    id 709
    label "kinetyka"
  ]
  node [
    id 710
    label "masa_bezw&#322;adna"
  ]
  node [
    id 711
    label "mechanika_klasyczna"
  ]
  node [
    id 712
    label "zmienno&#347;&#263;"
  ]
  node [
    id 713
    label "wyregulowanie"
  ]
  node [
    id 714
    label "warto&#347;&#263;"
  ]
  node [
    id 715
    label "charakterystyka"
  ]
  node [
    id 716
    label "kompetencja"
  ]
  node [
    id 717
    label "regulowanie"
  ]
  node [
    id 718
    label "feature"
  ]
  node [
    id 719
    label "wyregulowa&#263;"
  ]
  node [
    id 720
    label "regulowa&#263;"
  ]
  node [
    id 721
    label "attribute"
  ]
  node [
    id 722
    label "figura"
  ]
  node [
    id 723
    label "wjazd"
  ]
  node [
    id 724
    label "konstrukcja"
  ]
  node [
    id 725
    label "r&#243;w"
  ]
  node [
    id 726
    label "kreacja"
  ]
  node [
    id 727
    label "posesja"
  ]
  node [
    id 728
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 729
    label "zwierz&#281;"
  ]
  node [
    id 730
    label "miejsce_pracy"
  ]
  node [
    id 731
    label "praca"
  ]
  node [
    id 732
    label "constitution"
  ]
  node [
    id 733
    label "byt"
  ]
  node [
    id 734
    label "materia&#322;"
  ]
  node [
    id 735
    label "szczeg&#243;&#322;"
  ]
  node [
    id 736
    label "rzecz"
  ]
  node [
    id 737
    label "ropa"
  ]
  node [
    id 738
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 739
    label "przegrupowanie"
  ]
  node [
    id 740
    label "concentration"
  ]
  node [
    id 741
    label "po&#322;&#261;czenie"
  ]
  node [
    id 742
    label "z&#322;&#261;czenie"
  ]
  node [
    id 743
    label "congestion"
  ]
  node [
    id 744
    label "uwaga"
  ]
  node [
    id 745
    label "agglomeration"
  ]
  node [
    id 746
    label "zgromadzenie"
  ]
  node [
    id 747
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 748
    label "kupienie"
  ]
  node [
    id 749
    label "odjechany"
  ]
  node [
    id 750
    label "fazowo"
  ]
  node [
    id 751
    label "etapowy"
  ]
  node [
    id 752
    label "odwodnienie"
  ]
  node [
    id 753
    label "konstytucja"
  ]
  node [
    id 754
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 755
    label "substancja_chemiczna"
  ]
  node [
    id 756
    label "bratnia_dusza"
  ]
  node [
    id 757
    label "zwi&#261;zanie"
  ]
  node [
    id 758
    label "lokant"
  ]
  node [
    id 759
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 760
    label "zwi&#261;za&#263;"
  ]
  node [
    id 761
    label "organizacja"
  ]
  node [
    id 762
    label "odwadnia&#263;"
  ]
  node [
    id 763
    label "marriage"
  ]
  node [
    id 764
    label "marketing_afiliacyjny"
  ]
  node [
    id 765
    label "wi&#261;zanie"
  ]
  node [
    id 766
    label "odwadnianie"
  ]
  node [
    id 767
    label "koligacja"
  ]
  node [
    id 768
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 769
    label "odwodni&#263;"
  ]
  node [
    id 770
    label "azeotrop"
  ]
  node [
    id 771
    label "egzergia"
  ]
  node [
    id 772
    label "kwant_energii"
  ]
  node [
    id 773
    label "szwung"
  ]
  node [
    id 774
    label "energy"
  ]
  node [
    id 775
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 776
    label "emitowanie"
  ]
  node [
    id 777
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 778
    label "power"
  ]
  node [
    id 779
    label "emitowa&#263;"
  ]
  node [
    id 780
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 781
    label "grupa_funkcyjna"
  ]
  node [
    id 782
    label "diadochia"
  ]
  node [
    id 783
    label "cz&#261;stka"
  ]
  node [
    id 784
    label "konfiguracja"
  ]
  node [
    id 785
    label "masa_cz&#261;steczkowa"
  ]
  node [
    id 786
    label "termoelektryczno&#347;&#263;"
  ]
  node [
    id 787
    label "tautochrona"
  ]
  node [
    id 788
    label "rozpalony"
  ]
  node [
    id 789
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 790
    label "oznaka"
  ]
  node [
    id 791
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 792
    label "choroba_bosto&#324;ska"
  ]
  node [
    id 793
    label "denga"
  ]
  node [
    id 794
    label "termoczu&#322;y"
  ]
  node [
    id 795
    label "zapalenie_opon_m&#243;zgowo-rdzeniowych"
  ]
  node [
    id 796
    label "hotness"
  ]
  node [
    id 797
    label "zagrza&#263;"
  ]
  node [
    id 798
    label "atmosfera"
  ]
  node [
    id 799
    label "porusza&#263;"
  ]
  node [
    id 800
    label "balsamowa&#263;"
  ]
  node [
    id 801
    label "Komitet_Region&#243;w"
  ]
  node [
    id 802
    label "pochowa&#263;"
  ]
  node [
    id 803
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 804
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 805
    label "otworzenie"
  ]
  node [
    id 806
    label "zabalsamowanie"
  ]
  node [
    id 807
    label "tanatoplastyk"
  ]
  node [
    id 808
    label "biorytm"
  ]
  node [
    id 809
    label "istota_&#380;ywa"
  ]
  node [
    id 810
    label "zabalsamowa&#263;"
  ]
  node [
    id 811
    label "pogrzeb"
  ]
  node [
    id 812
    label "otwieranie"
  ]
  node [
    id 813
    label "tanatoplastyka"
  ]
  node [
    id 814
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 815
    label "l&#281;d&#378;wie"
  ]
  node [
    id 816
    label "sk&#243;ra"
  ]
  node [
    id 817
    label "nieumar&#322;y"
  ]
  node [
    id 818
    label "unerwienie"
  ]
  node [
    id 819
    label "sekcja"
  ]
  node [
    id 820
    label "ow&#322;osienie"
  ]
  node [
    id 821
    label "zesp&#243;&#322;"
  ]
  node [
    id 822
    label "ekshumowa&#263;"
  ]
  node [
    id 823
    label "jednostka_organizacyjna"
  ]
  node [
    id 824
    label "ekshumowanie"
  ]
  node [
    id 825
    label "pochowanie"
  ]
  node [
    id 826
    label "kremacja"
  ]
  node [
    id 827
    label "otworzy&#263;"
  ]
  node [
    id 828
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 829
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 830
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 831
    label "balsamowanie"
  ]
  node [
    id 832
    label "Izba_Konsyliarska"
  ]
  node [
    id 833
    label "cz&#322;onek"
  ]
  node [
    id 834
    label "szkielet"
  ]
  node [
    id 835
    label "ty&#322;"
  ]
  node [
    id 836
    label "staw"
  ]
  node [
    id 837
    label "mi&#281;so"
  ]
  node [
    id 838
    label "prz&#243;d"
  ]
  node [
    id 839
    label "otwiera&#263;"
  ]
  node [
    id 840
    label "p&#322;aszczyzna"
  ]
  node [
    id 841
    label "samoch&#243;d_pu&#322;apka"
  ]
  node [
    id 842
    label "unos"
  ]
  node [
    id 843
    label "weight"
  ]
  node [
    id 844
    label "patron"
  ]
  node [
    id 845
    label "charge"
  ]
  node [
    id 846
    label "proch_bezdymny"
  ]
  node [
    id 847
    label "prochownia"
  ]
  node [
    id 848
    label "towar"
  ]
  node [
    id 849
    label "elektrycznie"
  ]
  node [
    id 850
    label "nauka_prawa"
  ]
  node [
    id 851
    label "normatywizm"
  ]
  node [
    id 852
    label "kazuistyka"
  ]
  node [
    id 853
    label "prawo_karne"
  ]
  node [
    id 854
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 855
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 856
    label "prawo_karne_procesowe"
  ]
  node [
    id 857
    label "przepis"
  ]
  node [
    id 858
    label "kanonistyka"
  ]
  node [
    id 859
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 860
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 861
    label "wykonawczy"
  ]
  node [
    id 862
    label "judykatura"
  ]
  node [
    id 863
    label "legislacyjnie"
  ]
  node [
    id 864
    label "umocowa&#263;"
  ]
  node [
    id 865
    label "podmiot"
  ]
  node [
    id 866
    label "procesualistyka"
  ]
  node [
    id 867
    label "kryminologia"
  ]
  node [
    id 868
    label "kryminalistyka"
  ]
  node [
    id 869
    label "cywilistyka"
  ]
  node [
    id 870
    label "law"
  ]
  node [
    id 871
    label "jurisprudence"
  ]
  node [
    id 872
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 873
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 874
    label "Ko&#347;ciuszko"
  ]
  node [
    id 875
    label "lider"
  ]
  node [
    id 876
    label "opiekun"
  ]
  node [
    id 877
    label "przeno&#347;nik"
  ]
  node [
    id 878
    label "Tito"
  ]
  node [
    id 879
    label "Fidel_Castro"
  ]
  node [
    id 880
    label "kriotron"
  ]
  node [
    id 881
    label "Mao"
  ]
  node [
    id 882
    label "Miko&#322;ajczyk"
  ]
  node [
    id 883
    label "Saba&#322;a"
  ]
  node [
    id 884
    label "Sabataj_Cwi"
  ]
  node [
    id 885
    label "Anders"
  ]
  node [
    id 886
    label "path"
  ]
  node [
    id 887
    label "handbook"
  ]
  node [
    id 888
    label "topik"
  ]
  node [
    id 889
    label "insulator"
  ]
  node [
    id 890
    label "tworzywo"
  ]
  node [
    id 891
    label "przep&#322;yw"
  ]
  node [
    id 892
    label "ideologia"
  ]
  node [
    id 893
    label "dreszcz"
  ]
  node [
    id 894
    label "praktyka"
  ]
  node [
    id 895
    label "metoda"
  ]
  node [
    id 896
    label "apparent_motion"
  ]
  node [
    id 897
    label "electricity"
  ]
  node [
    id 898
    label "przyp&#322;yw"
  ]
  node [
    id 899
    label "strength"
  ]
  node [
    id 900
    label "wzmocnienie"
  ]
  node [
    id 901
    label "striving"
  ]
  node [
    id 902
    label "amperomierz"
  ]
  node [
    id 903
    label "gad&#380;et"
  ]
  node [
    id 904
    label "wabik"
  ]
  node [
    id 905
    label "przyrz&#261;d"
  ]
  node [
    id 906
    label "magnes_trwa&#322;y"
  ]
  node [
    id 907
    label "rdze&#324;"
  ]
  node [
    id 908
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 909
    label "brzeg"
  ]
  node [
    id 910
    label "Ziemia"
  ]
  node [
    id 911
    label "p&#322;oza"
  ]
  node [
    id 912
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 913
    label "zawiasy"
  ]
  node [
    id 914
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 915
    label "element_anatomiczny"
  ]
  node [
    id 916
    label "zawieszenie_Cardana"
  ]
  node [
    id 917
    label "urz&#261;dzenie"
  ]
  node [
    id 918
    label "naktuz"
  ]
  node [
    id 919
    label "przyrz&#261;d_nawigacyjny"
  ]
  node [
    id 920
    label "compass"
  ]
  node [
    id 921
    label "sie&#263;_rybacka"
  ]
  node [
    id 922
    label "stylus"
  ]
  node [
    id 923
    label "li&#347;&#263;"
  ]
  node [
    id 924
    label "ucho"
  ]
  node [
    id 925
    label "narz&#281;dzie_do_szycia"
  ]
  node [
    id 926
    label "narz&#281;dzie"
  ]
  node [
    id 927
    label "pr&#281;cik"
  ]
  node [
    id 928
    label "zwie&#324;czenie"
  ]
  node [
    id 929
    label "igliwie"
  ]
  node [
    id 930
    label "magnetycznie"
  ]
  node [
    id 931
    label "zniewalaj&#261;cy"
  ]
  node [
    id 932
    label "cewka"
  ]
  node [
    id 933
    label "dotarcie"
  ]
  node [
    id 934
    label "wyci&#261;garka"
  ]
  node [
    id 935
    label "biblioteka"
  ]
  node [
    id 936
    label "aerosanie"
  ]
  node [
    id 937
    label "podgrzewacz"
  ]
  node [
    id 938
    label "bombowiec"
  ]
  node [
    id 939
    label "dociera&#263;"
  ]
  node [
    id 940
    label "gniazdo_zaworowe"
  ]
  node [
    id 941
    label "motor&#243;wka"
  ]
  node [
    id 942
    label "nap&#281;d"
  ]
  node [
    id 943
    label "perpetuum_mobile"
  ]
  node [
    id 944
    label "rz&#281;&#380;enie"
  ]
  node [
    id 945
    label "mechanizm"
  ]
  node [
    id 946
    label "gondola_silnikowa"
  ]
  node [
    id 947
    label "docieranie"
  ]
  node [
    id 948
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 949
    label "rz&#281;zi&#263;"
  ]
  node [
    id 950
    label "motoszybowiec"
  ]
  node [
    id 951
    label "motogodzina"
  ]
  node [
    id 952
    label "samoch&#243;d"
  ]
  node [
    id 953
    label "dotrze&#263;"
  ]
  node [
    id 954
    label "radiator"
  ]
  node [
    id 955
    label "program"
  ]
  node [
    id 956
    label "stale"
  ]
  node [
    id 957
    label "regularny"
  ]
  node [
    id 958
    label "zafalowanie"
  ]
  node [
    id 959
    label "czo&#322;o_fali"
  ]
  node [
    id 960
    label "rozbijanie_si&#281;"
  ]
  node [
    id 961
    label "clutter"
  ]
  node [
    id 962
    label "pasemko"
  ]
  node [
    id 963
    label "grzywa_fali"
  ]
  node [
    id 964
    label "t&#322;um"
  ]
  node [
    id 965
    label "kot"
  ]
  node [
    id 966
    label "efekt_Dopplera"
  ]
  node [
    id 967
    label "obcinka"
  ]
  node [
    id 968
    label "stream"
  ]
  node [
    id 969
    label "fit"
  ]
  node [
    id 970
    label "zafalowa&#263;"
  ]
  node [
    id 971
    label "karb"
  ]
  node [
    id 972
    label "rozbicie_si&#281;"
  ]
  node [
    id 973
    label "znak_diakrytyczny"
  ]
  node [
    id 974
    label "woda"
  ]
  node [
    id 975
    label "exposition"
  ]
  node [
    id 976
    label "obja&#347;nienie"
  ]
  node [
    id 977
    label "zakres"
  ]
  node [
    id 978
    label "distribution"
  ]
  node [
    id 979
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 980
    label "izochronizm"
  ]
  node [
    id 981
    label "r&#243;&#380;nica"
  ]
  node [
    id 982
    label "zasi&#261;g"
  ]
  node [
    id 983
    label "bridge"
  ]
  node [
    id 984
    label "paleogen"
  ]
  node [
    id 985
    label "spell"
  ]
  node [
    id 986
    label "czas"
  ]
  node [
    id 987
    label "period"
  ]
  node [
    id 988
    label "prekambr"
  ]
  node [
    id 989
    label "jura"
  ]
  node [
    id 990
    label "interstadia&#322;"
  ]
  node [
    id 991
    label "jednostka_geologiczna"
  ]
  node [
    id 992
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 993
    label "okres_noachijski"
  ]
  node [
    id 994
    label "orosir"
  ]
  node [
    id 995
    label "kreda"
  ]
  node [
    id 996
    label "sten"
  ]
  node [
    id 997
    label "drugorz&#281;d"
  ]
  node [
    id 998
    label "semester"
  ]
  node [
    id 999
    label "trzeciorz&#281;d"
  ]
  node [
    id 1000
    label "ton"
  ]
  node [
    id 1001
    label "dzieje"
  ]
  node [
    id 1002
    label "poprzednik"
  ]
  node [
    id 1003
    label "ordowik"
  ]
  node [
    id 1004
    label "karbon"
  ]
  node [
    id 1005
    label "trias"
  ]
  node [
    id 1006
    label "kalim"
  ]
  node [
    id 1007
    label "stater"
  ]
  node [
    id 1008
    label "era"
  ]
  node [
    id 1009
    label "cykl"
  ]
  node [
    id 1010
    label "p&#243;&#322;okres"
  ]
  node [
    id 1011
    label "czwartorz&#281;d"
  ]
  node [
    id 1012
    label "pulsacja"
  ]
  node [
    id 1013
    label "okres_amazo&#324;ski"
  ]
  node [
    id 1014
    label "kambr"
  ]
  node [
    id 1015
    label "Zeitgeist"
  ]
  node [
    id 1016
    label "nast&#281;pnik"
  ]
  node [
    id 1017
    label "kriogen"
  ]
  node [
    id 1018
    label "glacja&#322;"
  ]
  node [
    id 1019
    label "okres_czasu"
  ]
  node [
    id 1020
    label "riak"
  ]
  node [
    id 1021
    label "schy&#322;ek"
  ]
  node [
    id 1022
    label "okres_hesperyjski"
  ]
  node [
    id 1023
    label "sylur"
  ]
  node [
    id 1024
    label "dewon"
  ]
  node [
    id 1025
    label "ciota"
  ]
  node [
    id 1026
    label "epoka"
  ]
  node [
    id 1027
    label "pierwszorz&#281;d"
  ]
  node [
    id 1028
    label "okres_halsztacki"
  ]
  node [
    id 1029
    label "ektas"
  ]
  node [
    id 1030
    label "zdanie"
  ]
  node [
    id 1031
    label "condition"
  ]
  node [
    id 1032
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1033
    label "rok_akademicki"
  ]
  node [
    id 1034
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 1035
    label "postglacja&#322;"
  ]
  node [
    id 1036
    label "faza"
  ]
  node [
    id 1037
    label "proces_fizjologiczny"
  ]
  node [
    id 1038
    label "ediakar"
  ]
  node [
    id 1039
    label "time_period"
  ]
  node [
    id 1040
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 1041
    label "perm"
  ]
  node [
    id 1042
    label "rok_szkolny"
  ]
  node [
    id 1043
    label "neogen"
  ]
  node [
    id 1044
    label "sider"
  ]
  node [
    id 1045
    label "flow"
  ]
  node [
    id 1046
    label "podokres"
  ]
  node [
    id 1047
    label "preglacja&#322;"
  ]
  node [
    id 1048
    label "retoryka"
  ]
  node [
    id 1049
    label "choroba_przyrodzona"
  ]
  node [
    id 1050
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1051
    label "cyclicity"
  ]
  node [
    id 1052
    label "radiokomunikacja"
  ]
  node [
    id 1053
    label "fala_elektromagnetyczna"
  ]
  node [
    id 1054
    label "bezruch"
  ]
  node [
    id 1055
    label "spok&#243;j"
  ]
  node [
    id 1056
    label "porz&#261;dek"
  ]
  node [
    id 1057
    label "narz&#261;d_otolitowy"
  ]
  node [
    id 1058
    label "b&#322;&#281;dnik"
  ]
  node [
    id 1059
    label "proceed"
  ]
  node [
    id 1060
    label "brzmie&#263;"
  ]
  node [
    id 1061
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1062
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1063
    label "ta&#324;czy&#263;"
  ]
  node [
    id 1064
    label "jitter"
  ]
  node [
    id 1065
    label "rusza&#263;_si&#281;"
  ]
  node [
    id 1066
    label "maraton"
  ]
  node [
    id 1067
    label "longevity"
  ]
  node [
    id 1068
    label "circumference"
  ]
  node [
    id 1069
    label "distance"
  ]
  node [
    id 1070
    label "leksem"
  ]
  node [
    id 1071
    label "strona"
  ]
  node [
    id 1072
    label "&#347;wieci&#263;"
  ]
  node [
    id 1073
    label "o&#347;wietlenie"
  ]
  node [
    id 1074
    label "wpa&#347;&#263;"
  ]
  node [
    id 1075
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 1076
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 1077
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 1078
    label "punkt_widzenia"
  ]
  node [
    id 1079
    label "obsadnik"
  ]
  node [
    id 1080
    label "rzuca&#263;"
  ]
  node [
    id 1081
    label "rzucenie"
  ]
  node [
    id 1082
    label "promieniowanie_optyczne"
  ]
  node [
    id 1083
    label "promieniowanie_elektromagnetyczne"
  ]
  node [
    id 1084
    label "light"
  ]
  node [
    id 1085
    label "przy&#263;mienie"
  ]
  node [
    id 1086
    label "wpada&#263;"
  ]
  node [
    id 1087
    label "odst&#281;p"
  ]
  node [
    id 1088
    label "interpretacja"
  ]
  node [
    id 1089
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 1090
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 1091
    label "lighting"
  ]
  node [
    id 1092
    label "rzuci&#263;"
  ]
  node [
    id 1093
    label "plama"
  ]
  node [
    id 1094
    label "radiance"
  ]
  node [
    id 1095
    label "przy&#263;miewanie"
  ]
  node [
    id 1096
    label "fotokataliza"
  ]
  node [
    id 1097
    label "ja&#347;nia"
  ]
  node [
    id 1098
    label "m&#261;drze"
  ]
  node [
    id 1099
    label "rzucanie"
  ]
  node [
    id 1100
    label "wpadni&#281;cie"
  ]
  node [
    id 1101
    label "&#347;wiecenie"
  ]
  node [
    id 1102
    label "&#347;rednica"
  ]
  node [
    id 1103
    label "b&#322;ysk"
  ]
  node [
    id 1104
    label "wpadanie"
  ]
  node [
    id 1105
    label "promie&#324;"
  ]
  node [
    id 1106
    label "instalacja"
  ]
  node [
    id 1107
    label "przy&#263;mi&#263;"
  ]
  node [
    id 1108
    label "lighter"
  ]
  node [
    id 1109
    label "&#347;wiat&#322;y"
  ]
  node [
    id 1110
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 1111
    label "lampa"
  ]
  node [
    id 1112
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1113
    label "wybicie"
  ]
  node [
    id 1114
    label "zostawienie"
  ]
  node [
    id 1115
    label "zata&#324;czenie"
  ]
  node [
    id 1116
    label "picture"
  ]
  node [
    id 1117
    label "reproduction"
  ]
  node [
    id 1118
    label "lustro"
  ]
  node [
    id 1119
    label "blask"
  ]
  node [
    id 1120
    label "stracenie_g&#322;owy"
  ]
  node [
    id 1121
    label "uszkodzenie"
  ]
  node [
    id 1122
    label "impression"
  ]
  node [
    id 1123
    label "wyswobodzenie"
  ]
  node [
    id 1124
    label "wynagrodzenie"
  ]
  node [
    id 1125
    label "odbicie_si&#281;"
  ]
  node [
    id 1126
    label "wymienienie"
  ]
  node [
    id 1127
    label "recoil"
  ]
  node [
    id 1128
    label "odegranie_si&#281;"
  ]
  node [
    id 1129
    label "ut&#322;uczenie"
  ]
  node [
    id 1130
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1131
    label "pi&#322;ka"
  ]
  node [
    id 1132
    label "prototype"
  ]
  node [
    id 1133
    label "zrobienie"
  ]
  node [
    id 1134
    label "reflection"
  ]
  node [
    id 1135
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1136
    label "skopiowanie"
  ]
  node [
    id 1137
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 1138
    label "oddalenie_si&#281;"
  ]
  node [
    id 1139
    label "przelobowanie"
  ]
  node [
    id 1140
    label "odskoczenie"
  ]
  node [
    id 1141
    label "zabranie"
  ]
  node [
    id 1142
    label "stamp"
  ]
  node [
    id 1143
    label "deprecha"
  ]
  node [
    id 1144
    label "zniszczenie"
  ]
  node [
    id 1145
    label "przygn&#281;bienie"
  ]
  node [
    id 1146
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 1147
    label "zagi&#281;cie"
  ]
  node [
    id 1148
    label "p&#281;kni&#281;cie"
  ]
  node [
    id 1149
    label "dislocation"
  ]
  node [
    id 1150
    label "zesp&#243;&#322;_napi&#281;cia_przedmiesi&#261;czkowego"
  ]
  node [
    id 1151
    label "za&#322;amanie_si&#281;"
  ]
  node [
    id 1152
    label "choroba_ducha"
  ]
  node [
    id 1153
    label "choroba_psychiczna"
  ]
  node [
    id 1154
    label "usuni&#281;cie"
  ]
  node [
    id 1155
    label "rozja&#347;nienie_si&#281;"
  ]
  node [
    id 1156
    label "przeszkodzenie"
  ]
  node [
    id 1157
    label "waste"
  ]
  node [
    id 1158
    label "roztrwonienie"
  ]
  node [
    id 1159
    label "wyp&#281;dzenie"
  ]
  node [
    id 1160
    label "rozsianie"
  ]
  node [
    id 1161
    label "rozproszenie_si&#281;"
  ]
  node [
    id 1162
    label "zdekoncentrowanie_si&#281;"
  ]
  node [
    id 1163
    label "diversification"
  ]
  node [
    id 1164
    label "roztoczenie"
  ]
  node [
    id 1165
    label "rozdrobnienie"
  ]
  node [
    id 1166
    label "rozpierzchni&#281;cie_si&#281;"
  ]
  node [
    id 1167
    label "degeneration"
  ]
  node [
    id 1168
    label "diffusion"
  ]
  node [
    id 1169
    label "prze&#322;ama&#263;_si&#281;"
  ]
  node [
    id 1170
    label "prze&#322;amanie"
  ]
  node [
    id 1171
    label "prze&#322;amywa&#263;"
  ]
  node [
    id 1172
    label "prze&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 1173
    label "prze&#322;amywanie_si&#281;"
  ]
  node [
    id 1174
    label "ubarwienie"
  ]
  node [
    id 1175
    label "symbol"
  ]
  node [
    id 1176
    label "prze&#322;amywanie"
  ]
  node [
    id 1177
    label "prze&#322;ama&#263;"
  ]
  node [
    id 1178
    label "zblakn&#261;&#263;"
  ]
  node [
    id 1179
    label "liczba_kwantowa"
  ]
  node [
    id 1180
    label "blakn&#261;&#263;"
  ]
  node [
    id 1181
    label "zblakni&#281;cie"
  ]
  node [
    id 1182
    label "poker"
  ]
  node [
    id 1183
    label "blakni&#281;cie"
  ]
  node [
    id 1184
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 1185
    label "prism"
  ]
  node [
    id 1186
    label "graniastos&#322;up_prosty"
  ]
  node [
    id 1187
    label "monochromator"
  ]
  node [
    id 1188
    label "sprzedawca"
  ]
  node [
    id 1189
    label "sklep"
  ]
  node [
    id 1190
    label "rzemie&#347;lnik"
  ]
  node [
    id 1191
    label "fizyk"
  ]
  node [
    id 1192
    label "geometrycznie"
  ]
  node [
    id 1193
    label "naukowy"
  ]
  node [
    id 1194
    label "odrobina"
  ]
  node [
    id 1195
    label "&#263;ma"
  ]
  node [
    id 1196
    label "eyeshadow"
  ]
  node [
    id 1197
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 1198
    label "obw&#243;dka"
  ]
  node [
    id 1199
    label "archetyp"
  ]
  node [
    id 1200
    label "sowie_oczy"
  ]
  node [
    id 1201
    label "wycieniowa&#263;"
  ]
  node [
    id 1202
    label "nekromancja"
  ]
  node [
    id 1203
    label "ciemnota"
  ]
  node [
    id 1204
    label "noktowizja"
  ]
  node [
    id 1205
    label "Ereb"
  ]
  node [
    id 1206
    label "cieniowa&#263;"
  ]
  node [
    id 1207
    label "pomrok"
  ]
  node [
    id 1208
    label "duch"
  ]
  node [
    id 1209
    label "zacienie"
  ]
  node [
    id 1210
    label "zm&#281;czenie"
  ]
  node [
    id 1211
    label "zmar&#322;y"
  ]
  node [
    id 1212
    label "shade"
  ]
  node [
    id 1213
    label "sylwetka"
  ]
  node [
    id 1214
    label "kosmetyk_kolorowy"
  ]
  node [
    id 1215
    label "zjawa"
  ]
  node [
    id 1216
    label "przebarwienie"
  ]
  node [
    id 1217
    label "cloud"
  ]
  node [
    id 1218
    label "oko"
  ]
  node [
    id 1219
    label "twilight"
  ]
  node [
    id 1220
    label "skrzyd&#322;o"
  ]
  node [
    id 1221
    label "g&#322;ad&#378;"
  ]
  node [
    id 1222
    label "zwierciad&#322;o_p&#322;askie"
  ]
  node [
    id 1223
    label "powierzchnia"
  ]
  node [
    id 1224
    label "odbija&#263;"
  ]
  node [
    id 1225
    label "wyraz"
  ]
  node [
    id 1226
    label "odbijanie"
  ]
  node [
    id 1227
    label "przegl&#261;da&#263;_si&#281;"
  ]
  node [
    id 1228
    label "odbi&#263;"
  ]
  node [
    id 1229
    label "s&#322;aby"
  ]
  node [
    id 1230
    label "sp&#322;aszczanie"
  ]
  node [
    id 1231
    label "poziomy"
  ]
  node [
    id 1232
    label "trywialny"
  ]
  node [
    id 1233
    label "niski"
  ]
  node [
    id 1234
    label "kiepski"
  ]
  node [
    id 1235
    label "p&#322;asko"
  ]
  node [
    id 1236
    label "r&#243;wny"
  ]
  node [
    id 1237
    label "pospolity"
  ]
  node [
    id 1238
    label "sp&#322;aszczenie"
  ]
  node [
    id 1239
    label "szeroki"
  ]
  node [
    id 1240
    label "jednowymiarowy"
  ]
  node [
    id 1241
    label "wkl&#281;s&#322;o"
  ]
  node [
    id 1242
    label "hotbed"
  ]
  node [
    id 1243
    label "skupi&#263;"
  ]
  node [
    id 1244
    label "Hollywood"
  ]
  node [
    id 1245
    label "schorzenie"
  ]
  node [
    id 1246
    label "palenisko"
  ]
  node [
    id 1247
    label "impreza"
  ]
  node [
    id 1248
    label "center"
  ]
  node [
    id 1249
    label "skupisko"
  ]
  node [
    id 1250
    label "o&#347;rodek"
  ]
  node [
    id 1251
    label "watra"
  ]
  node [
    id 1252
    label "dioptryka"
  ]
  node [
    id 1253
    label "decentracja"
  ]
  node [
    id 1254
    label "telekonwerter"
  ]
  node [
    id 1255
    label "astygmatyzm"
  ]
  node [
    id 1256
    label "kora_soczewki"
  ]
  node [
    id 1257
    label "ga&#322;ka_oczna"
  ]
  node [
    id 1258
    label "zbiera&#263;"
  ]
  node [
    id 1259
    label "masowa&#263;"
  ]
  node [
    id 1260
    label "po&#347;wi&#281;ca&#263;"
  ]
  node [
    id 1261
    label "huddle"
  ]
  node [
    id 1262
    label "rozdrabnia&#263;"
  ]
  node [
    id 1263
    label "postpone"
  ]
  node [
    id 1264
    label "przeszkadza&#263;"
  ]
  node [
    id 1265
    label "circulate"
  ]
  node [
    id 1266
    label "trwoni&#263;"
  ]
  node [
    id 1267
    label "usuwa&#263;"
  ]
  node [
    id 1268
    label "rozsiewa&#263;"
  ]
  node [
    id 1269
    label "rozmieszcza&#263;"
  ]
  node [
    id 1270
    label "przep&#281;dza&#263;"
  ]
  node [
    id 1271
    label "split"
  ]
  node [
    id 1272
    label "roztacza&#263;"
  ]
  node [
    id 1273
    label "zwalcza&#263;"
  ]
  node [
    id 1274
    label "opinion"
  ]
  node [
    id 1275
    label "effigy"
  ]
  node [
    id 1276
    label "sztafa&#380;"
  ]
  node [
    id 1277
    label "przeplot"
  ]
  node [
    id 1278
    label "pulment"
  ]
  node [
    id 1279
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1280
    label "representation"
  ]
  node [
    id 1281
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 1282
    label "projekcja"
  ]
  node [
    id 1283
    label "malarz"
  ]
  node [
    id 1284
    label "inning"
  ]
  node [
    id 1285
    label "oprawianie"
  ]
  node [
    id 1286
    label "oprawia&#263;"
  ]
  node [
    id 1287
    label "wypunktowa&#263;"
  ]
  node [
    id 1288
    label "zaj&#347;cie"
  ]
  node [
    id 1289
    label "t&#322;o"
  ]
  node [
    id 1290
    label "punktowa&#263;"
  ]
  node [
    id 1291
    label "widok"
  ]
  node [
    id 1292
    label "plama_barwna"
  ]
  node [
    id 1293
    label "scena"
  ]
  node [
    id 1294
    label "czo&#322;&#243;wka"
  ]
  node [
    id 1295
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1296
    label "human_body"
  ]
  node [
    id 1297
    label "ty&#322;&#243;wka"
  ]
  node [
    id 1298
    label "napisy"
  ]
  node [
    id 1299
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 1300
    label "postprodukcja"
  ]
  node [
    id 1301
    label "przedstawienie"
  ]
  node [
    id 1302
    label "podobrazie"
  ]
  node [
    id 1303
    label "anamorfoza"
  ]
  node [
    id 1304
    label "parkiet"
  ]
  node [
    id 1305
    label "ziarno"
  ]
  node [
    id 1306
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 1307
    label "ostro&#347;&#263;"
  ]
  node [
    id 1308
    label "persona"
  ]
  node [
    id 1309
    label "pogl&#261;d"
  ]
  node [
    id 1310
    label "filmoteka"
  ]
  node [
    id 1311
    label "uj&#281;cie"
  ]
  node [
    id 1312
    label "perspektywa"
  ]
  node [
    id 1313
    label "mo&#380;liwy"
  ]
  node [
    id 1314
    label "realnie"
  ]
  node [
    id 1315
    label "prawdziwy"
  ]
  node [
    id 1316
    label "podobny"
  ]
  node [
    id 1317
    label "nieprawdziwy"
  ]
  node [
    id 1318
    label "niemiarowo&#347;&#263;_sferyczna"
  ]
  node [
    id 1319
    label "m&#261;dro&#347;&#263;"
  ]
  node [
    id 1320
    label "d&#322;ugofalowo&#347;&#263;"
  ]
  node [
    id 1321
    label "przerwa"
  ]
  node [
    id 1322
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 1323
    label "resolution"
  ]
  node [
    id 1324
    label "rozdzielenie"
  ]
  node [
    id 1325
    label "podzia&#322;"
  ]
  node [
    id 1326
    label "schism"
  ]
  node [
    id 1327
    label "rozszczepienie_si&#281;"
  ]
  node [
    id 1328
    label "rozpad"
  ]
  node [
    id 1329
    label "mechanizm_obronny"
  ]
  node [
    id 1330
    label "przerwa_Enckego"
  ]
  node [
    id 1331
    label "sygnalizacja_&#347;wietlna"
  ]
  node [
    id 1332
    label "skrzy&#380;owanie"
  ]
  node [
    id 1333
    label "pasy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 245
  ]
  edge [
    source 14
    target 46
  ]
  edge [
    source 14
    target 246
  ]
  edge [
    source 14
    target 247
  ]
  edge [
    source 14
    target 248
  ]
  edge [
    source 14
    target 249
  ]
  edge [
    source 14
    target 250
  ]
  edge [
    source 14
    target 251
  ]
  edge [
    source 14
    target 252
  ]
  edge [
    source 14
    target 253
  ]
  edge [
    source 14
    target 254
  ]
  edge [
    source 14
    target 255
  ]
  edge [
    source 14
    target 256
  ]
  edge [
    source 14
    target 257
  ]
  edge [
    source 14
    target 258
  ]
  edge [
    source 14
    target 259
  ]
  edge [
    source 14
    target 260
  ]
  edge [
    source 14
    target 261
  ]
  edge [
    source 14
    target 262
  ]
  edge [
    source 14
    target 224
  ]
  edge [
    source 14
    target 263
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 264
  ]
  edge [
    source 14
    target 265
  ]
  edge [
    source 14
    target 266
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 267
  ]
  edge [
    source 15
    target 268
  ]
  edge [
    source 15
    target 269
  ]
  edge [
    source 15
    target 270
  ]
  edge [
    source 15
    target 271
  ]
  edge [
    source 15
    target 272
  ]
  edge [
    source 15
    target 273
  ]
  edge [
    source 15
    target 274
  ]
  edge [
    source 15
    target 275
  ]
  edge [
    source 15
    target 276
  ]
  edge [
    source 15
    target 277
  ]
  edge [
    source 15
    target 278
  ]
  edge [
    source 15
    target 279
  ]
  edge [
    source 15
    target 280
  ]
  edge [
    source 15
    target 281
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 282
  ]
  edge [
    source 16
    target 283
  ]
  edge [
    source 16
    target 284
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 285
  ]
  edge [
    source 17
    target 286
  ]
  edge [
    source 17
    target 287
  ]
  edge [
    source 17
    target 288
  ]
  edge [
    source 17
    target 289
  ]
  edge [
    source 17
    target 290
  ]
  edge [
    source 17
    target 291
  ]
  edge [
    source 17
    target 292
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 293
  ]
  edge [
    source 19
    target 294
  ]
  edge [
    source 19
    target 295
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 20
    target 296
  ]
  edge [
    source 20
    target 297
  ]
  edge [
    source 20
    target 298
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 299
  ]
  edge [
    source 21
    target 300
  ]
  edge [
    source 21
    target 301
  ]
  edge [
    source 21
    target 302
  ]
  edge [
    source 21
    target 303
  ]
  edge [
    source 21
    target 304
  ]
  edge [
    source 21
    target 305
  ]
  edge [
    source 21
    target 306
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 307
  ]
  edge [
    source 22
    target 308
  ]
  edge [
    source 22
    target 309
  ]
  edge [
    source 22
    target 310
  ]
  edge [
    source 22
    target 311
  ]
  edge [
    source 22
    target 312
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 23
    target 313
  ]
  edge [
    source 23
    target 314
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 246
  ]
  edge [
    source 24
    target 315
  ]
  edge [
    source 24
    target 316
  ]
  edge [
    source 24
    target 317
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 318
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 319
  ]
  edge [
    source 25
    target 320
  ]
  edge [
    source 25
    target 321
  ]
  edge [
    source 25
    target 322
  ]
  edge [
    source 25
    target 323
  ]
  edge [
    source 25
    target 324
  ]
  edge [
    source 25
    target 325
  ]
  edge [
    source 25
    target 326
  ]
  edge [
    source 25
    target 327
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 328
  ]
  edge [
    source 26
    target 329
  ]
  edge [
    source 26
    target 330
  ]
  edge [
    source 26
    target 331
  ]
  edge [
    source 26
    target 332
  ]
  edge [
    source 26
    target 333
  ]
  edge [
    source 26
    target 334
  ]
  edge [
    source 26
    target 335
  ]
  edge [
    source 26
    target 316
  ]
  edge [
    source 26
    target 336
  ]
  edge [
    source 26
    target 337
  ]
  edge [
    source 26
    target 338
  ]
  edge [
    source 26
    target 339
  ]
  edge [
    source 26
    target 340
  ]
  edge [
    source 26
    target 341
  ]
  edge [
    source 26
    target 342
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 343
  ]
  edge [
    source 27
    target 344
  ]
  edge [
    source 27
    target 345
  ]
  edge [
    source 27
    target 346
  ]
  edge [
    source 27
    target 347
  ]
  edge [
    source 27
    target 348
  ]
  edge [
    source 27
    target 349
  ]
  edge [
    source 27
    target 350
  ]
  edge [
    source 27
    target 351
  ]
  edge [
    source 27
    target 352
  ]
  edge [
    source 27
    target 353
  ]
  edge [
    source 27
    target 354
  ]
  edge [
    source 27
    target 355
  ]
  edge [
    source 27
    target 356
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 27
    target 357
  ]
  edge [
    source 27
    target 358
  ]
  edge [
    source 27
    target 359
  ]
  edge [
    source 27
    target 360
  ]
  edge [
    source 27
    target 292
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 361
  ]
  edge [
    source 28
    target 362
  ]
  edge [
    source 28
    target 363
  ]
  edge [
    source 28
    target 364
  ]
  edge [
    source 28
    target 365
  ]
  edge [
    source 28
    target 305
  ]
  edge [
    source 28
    target 366
  ]
  edge [
    source 29
    target 367
  ]
  edge [
    source 29
    target 180
  ]
  edge [
    source 29
    target 368
  ]
  edge [
    source 29
    target 183
  ]
  edge [
    source 29
    target 350
  ]
  edge [
    source 29
    target 86
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 369
  ]
  edge [
    source 30
    target 370
  ]
  edge [
    source 30
    target 371
  ]
  edge [
    source 30
    target 372
  ]
  edge [
    source 30
    target 373
  ]
  edge [
    source 30
    target 374
  ]
  edge [
    source 30
    target 289
  ]
  edge [
    source 30
    target 191
  ]
  edge [
    source 30
    target 375
  ]
  edge [
    source 30
    target 376
  ]
  edge [
    source 30
    target 377
  ]
  edge [
    source 30
    target 378
  ]
  edge [
    source 30
    target 379
  ]
  edge [
    source 30
    target 380
  ]
  edge [
    source 30
    target 381
  ]
  edge [
    source 30
    target 382
  ]
  edge [
    source 30
    target 383
  ]
  edge [
    source 30
    target 65
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 384
  ]
  edge [
    source 31
    target 385
  ]
  edge [
    source 31
    target 386
  ]
  edge [
    source 31
    target 387
  ]
  edge [
    source 31
    target 388
  ]
  edge [
    source 31
    target 389
  ]
  edge [
    source 31
    target 390
  ]
  edge [
    source 31
    target 391
  ]
  edge [
    source 32
    target 392
  ]
  edge [
    source 32
    target 352
  ]
  edge [
    source 32
    target 393
  ]
  edge [
    source 32
    target 394
  ]
  edge [
    source 32
    target 395
  ]
  edge [
    source 32
    target 396
  ]
  edge [
    source 32
    target 358
  ]
  edge [
    source 32
    target 397
  ]
  edge [
    source 32
    target 251
  ]
  edge [
    source 32
    target 398
  ]
  edge [
    source 32
    target 399
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 400
  ]
  edge [
    source 33
    target 401
  ]
  edge [
    source 33
    target 402
  ]
  edge [
    source 33
    target 403
  ]
  edge [
    source 33
    target 359
  ]
  edge [
    source 33
    target 404
  ]
  edge [
    source 33
    target 405
  ]
  edge [
    source 33
    target 406
  ]
  edge [
    source 33
    target 407
  ]
  edge [
    source 33
    target 398
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 34
    target 408
  ]
  edge [
    source 34
    target 409
  ]
  edge [
    source 34
    target 410
  ]
  edge [
    source 34
    target 411
  ]
  edge [
    source 34
    target 412
  ]
  edge [
    source 34
    target 413
  ]
  edge [
    source 34
    target 414
  ]
  edge [
    source 34
    target 415
  ]
  edge [
    source 34
    target 416
  ]
  edge [
    source 34
    target 417
  ]
  edge [
    source 34
    target 418
  ]
  edge [
    source 34
    target 278
  ]
  edge [
    source 34
    target 419
  ]
  edge [
    source 34
    target 398
  ]
  edge [
    source 34
    target 420
  ]
  edge [
    source 34
    target 296
  ]
  edge [
    source 34
    target 421
  ]
  edge [
    source 34
    target 422
  ]
  edge [
    source 34
    target 423
  ]
  edge [
    source 34
    target 424
  ]
  edge [
    source 34
    target 425
  ]
  edge [
    source 34
    target 426
  ]
  edge [
    source 34
    target 427
  ]
  edge [
    source 34
    target 428
  ]
  edge [
    source 34
    target 429
  ]
  edge [
    source 34
    target 430
  ]
  edge [
    source 34
    target 431
  ]
  edge [
    source 34
    target 432
  ]
  edge [
    source 34
    target 433
  ]
  edge [
    source 34
    target 434
  ]
  edge [
    source 34
    target 435
  ]
  edge [
    source 34
    target 436
  ]
  edge [
    source 34
    target 437
  ]
  edge [
    source 34
    target 438
  ]
  edge [
    source 34
    target 439
  ]
  edge [
    source 34
    target 440
  ]
  edge [
    source 34
    target 441
  ]
  edge [
    source 34
    target 442
  ]
  edge [
    source 34
    target 132
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 443
  ]
  edge [
    source 35
    target 444
  ]
  edge [
    source 35
    target 445
  ]
  edge [
    source 35
    target 446
  ]
  edge [
    source 35
    target 447
  ]
  edge [
    source 35
    target 448
  ]
  edge [
    source 35
    target 449
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 450
  ]
  edge [
    source 36
    target 451
  ]
  edge [
    source 36
    target 452
  ]
  edge [
    source 36
    target 453
  ]
  edge [
    source 36
    target 454
  ]
  edge [
    source 36
    target 455
  ]
  edge [
    source 36
    target 456
  ]
  edge [
    source 36
    target 457
  ]
  edge [
    source 36
    target 458
  ]
  edge [
    source 36
    target 459
  ]
  edge [
    source 36
    target 460
  ]
  edge [
    source 36
    target 461
  ]
  edge [
    source 36
    target 462
  ]
  edge [
    source 36
    target 463
  ]
  edge [
    source 36
    target 464
  ]
  edge [
    source 36
    target 465
  ]
  edge [
    source 36
    target 466
  ]
  edge [
    source 36
    target 467
  ]
  edge [
    source 36
    target 175
  ]
  edge [
    source 36
    target 468
  ]
  edge [
    source 36
    target 469
  ]
  edge [
    source 36
    target 470
  ]
  edge [
    source 37
    target 471
  ]
  edge [
    source 37
    target 472
  ]
  edge [
    source 37
    target 473
  ]
  edge [
    source 38
    target 474
  ]
  edge [
    source 38
    target 269
  ]
  edge [
    source 38
    target 475
  ]
  edge [
    source 38
    target 476
  ]
  edge [
    source 38
    target 457
  ]
  edge [
    source 38
    target 477
  ]
  edge [
    source 38
    target 478
  ]
  edge [
    source 38
    target 345
  ]
  edge [
    source 38
    target 479
  ]
  edge [
    source 38
    target 480
  ]
  edge [
    source 38
    target 481
  ]
  edge [
    source 38
    target 482
  ]
  edge [
    source 38
    target 483
  ]
  edge [
    source 38
    target 484
  ]
  edge [
    source 38
    target 485
  ]
  edge [
    source 38
    target 225
  ]
  edge [
    source 38
    target 486
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 487
  ]
  edge [
    source 39
    target 409
  ]
  edge [
    source 39
    target 488
  ]
  edge [
    source 39
    target 489
  ]
  edge [
    source 39
    target 490
  ]
  edge [
    source 39
    target 167
  ]
  edge [
    source 39
    target 491
  ]
  edge [
    source 39
    target 492
  ]
  edge [
    source 39
    target 493
  ]
  edge [
    source 39
    target 494
  ]
  edge [
    source 39
    target 495
  ]
  edge [
    source 39
    target 496
  ]
  edge [
    source 39
    target 350
  ]
  edge [
    source 39
    target 497
  ]
  edge [
    source 39
    target 498
  ]
  edge [
    source 39
    target 69
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 499
  ]
  edge [
    source 40
    target 500
  ]
  edge [
    source 40
    target 501
  ]
  edge [
    source 40
    target 502
  ]
  edge [
    source 40
    target 503
  ]
  edge [
    source 40
    target 504
  ]
  edge [
    source 40
    target 505
  ]
  edge [
    source 40
    target 506
  ]
  edge [
    source 40
    target 507
  ]
  edge [
    source 40
    target 508
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 509
  ]
  edge [
    source 41
    target 510
  ]
  edge [
    source 41
    target 511
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 512
  ]
  edge [
    source 42
    target 513
  ]
  edge [
    source 42
    target 514
  ]
  edge [
    source 42
    target 515
  ]
  edge [
    source 42
    target 516
  ]
  edge [
    source 42
    target 517
  ]
  edge [
    source 42
    target 518
  ]
  edge [
    source 42
    target 519
  ]
  edge [
    source 42
    target 48
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 352
  ]
  edge [
    source 43
    target 520
  ]
  edge [
    source 43
    target 521
  ]
  edge [
    source 43
    target 522
  ]
  edge [
    source 43
    target 523
  ]
  edge [
    source 43
    target 50
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 411
  ]
  edge [
    source 44
    target 524
  ]
  edge [
    source 44
    target 525
  ]
  edge [
    source 44
    target 526
  ]
  edge [
    source 44
    target 527
  ]
  edge [
    source 44
    target 528
  ]
  edge [
    source 44
    target 529
  ]
  edge [
    source 44
    target 530
  ]
  edge [
    source 44
    target 295
  ]
  edge [
    source 44
    target 531
  ]
  edge [
    source 44
    target 532
  ]
  edge [
    source 44
    target 533
  ]
  edge [
    source 44
    target 534
  ]
  edge [
    source 44
    target 535
  ]
  edge [
    source 44
    target 536
  ]
  edge [
    source 44
    target 537
  ]
  edge [
    source 44
    target 538
  ]
  edge [
    source 44
    target 539
  ]
  edge [
    source 44
    target 540
  ]
  edge [
    source 44
    target 541
  ]
  edge [
    source 44
    target 542
  ]
  edge [
    source 44
    target 543
  ]
  edge [
    source 44
    target 85
  ]
  edge [
    source 44
    target 544
  ]
  edge [
    source 44
    target 545
  ]
  edge [
    source 44
    target 546
  ]
  edge [
    source 44
    target 358
  ]
  edge [
    source 44
    target 547
  ]
  edge [
    source 44
    target 548
  ]
  edge [
    source 44
    target 549
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 108
  ]
  edge [
    source 46
    target 109
  ]
  edge [
    source 46
    target 550
  ]
  edge [
    source 46
    target 551
  ]
  edge [
    source 46
    target 552
  ]
  edge [
    source 46
    target 553
  ]
  edge [
    source 46
    target 554
  ]
  edge [
    source 46
    target 555
  ]
  edge [
    source 46
    target 556
  ]
  edge [
    source 46
    target 557
  ]
  edge [
    source 46
    target 558
  ]
  edge [
    source 46
    target 352
  ]
  edge [
    source 46
    target 559
  ]
  edge [
    source 46
    target 560
  ]
  edge [
    source 46
    target 561
  ]
  edge [
    source 46
    target 434
  ]
  edge [
    source 46
    target 356
  ]
  edge [
    source 46
    target 224
  ]
  edge [
    source 46
    target 562
  ]
  edge [
    source 46
    target 563
  ]
  edge [
    source 46
    target 564
  ]
  edge [
    source 46
    target 565
  ]
  edge [
    source 46
    target 566
  ]
  edge [
    source 46
    target 567
  ]
  edge [
    source 46
    target 568
  ]
  edge [
    source 46
    target 103
  ]
  edge [
    source 46
    target 111
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 569
  ]
  edge [
    source 47
    target 570
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 571
  ]
  edge [
    source 48
    target 572
  ]
  edge [
    source 48
    target 573
  ]
  edge [
    source 48
    target 574
  ]
  edge [
    source 48
    target 575
  ]
  edge [
    source 48
    target 576
  ]
  edge [
    source 48
    target 577
  ]
  edge [
    source 48
    target 578
  ]
  edge [
    source 48
    target 579
  ]
  edge [
    source 48
    target 580
  ]
  edge [
    source 48
    target 581
  ]
  edge [
    source 48
    target 582
  ]
  edge [
    source 48
    target 583
  ]
  edge [
    source 48
    target 584
  ]
  edge [
    source 48
    target 585
  ]
  edge [
    source 48
    target 586
  ]
  edge [
    source 48
    target 587
  ]
  edge [
    source 48
    target 588
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 48
    target 589
  ]
  edge [
    source 48
    target 590
  ]
  edge [
    source 48
    target 591
  ]
  edge [
    source 48
    target 592
  ]
  edge [
    source 48
    target 593
  ]
  edge [
    source 48
    target 594
  ]
  edge [
    source 48
    target 595
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 108
  ]
  edge [
    source 49
    target 112
  ]
  edge [
    source 49
    target 80
  ]
  edge [
    source 49
    target 596
  ]
  edge [
    source 49
    target 597
  ]
  edge [
    source 49
    target 167
  ]
  edge [
    source 49
    target 598
  ]
  edge [
    source 49
    target 599
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 600
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 601
  ]
  edge [
    source 51
    target 602
  ]
  edge [
    source 51
    target 603
  ]
  edge [
    source 51
    target 604
  ]
  edge [
    source 51
    target 605
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 80
  ]
  edge [
    source 52
    target 110
  ]
  edge [
    source 52
    target 606
  ]
  edge [
    source 52
    target 607
  ]
  edge [
    source 52
    target 608
  ]
  edge [
    source 52
    target 609
  ]
  edge [
    source 52
    target 610
  ]
  edge [
    source 52
    target 611
  ]
  edge [
    source 52
    target 612
  ]
  edge [
    source 52
    target 613
  ]
  edge [
    source 52
    target 614
  ]
  edge [
    source 52
    target 615
  ]
  edge [
    source 52
    target 616
  ]
  edge [
    source 52
    target 617
  ]
  edge [
    source 52
    target 618
  ]
  edge [
    source 52
    target 619
  ]
  edge [
    source 52
    target 620
  ]
  edge [
    source 52
    target 621
  ]
  edge [
    source 52
    target 622
  ]
  edge [
    source 52
    target 623
  ]
  edge [
    source 52
    target 624
  ]
  edge [
    source 52
    target 625
  ]
  edge [
    source 52
    target 626
  ]
  edge [
    source 52
    target 627
  ]
  edge [
    source 52
    target 352
  ]
  edge [
    source 52
    target 628
  ]
  edge [
    source 52
    target 629
  ]
  edge [
    source 52
    target 480
  ]
  edge [
    source 52
    target 630
  ]
  edge [
    source 52
    target 631
  ]
  edge [
    source 52
    target 632
  ]
  edge [
    source 52
    target 633
  ]
  edge [
    source 52
    target 219
  ]
  edge [
    source 52
    target 175
  ]
  edge [
    source 52
    target 634
  ]
  edge [
    source 52
    target 635
  ]
  edge [
    source 52
    target 636
  ]
  edge [
    source 52
    target 92
  ]
  edge [
    source 53
    target 637
  ]
  edge [
    source 53
    target 638
  ]
  edge [
    source 53
    target 639
  ]
  edge [
    source 53
    target 640
  ]
  edge [
    source 53
    target 641
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 642
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 643
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 644
  ]
  edge [
    source 55
    target 645
  ]
  edge [
    source 55
    target 646
  ]
  edge [
    source 55
    target 647
  ]
  edge [
    source 55
    target 648
  ]
  edge [
    source 55
    target 649
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 650
  ]
  edge [
    source 56
    target 651
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 652
  ]
  edge [
    source 57
    target 653
  ]
  edge [
    source 57
    target 654
  ]
  edge [
    source 57
    target 655
  ]
  edge [
    source 57
    target 615
  ]
  edge [
    source 57
    target 599
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 656
  ]
  edge [
    source 58
    target 435
  ]
  edge [
    source 58
    target 657
  ]
  edge [
    source 58
    target 658
  ]
  edge [
    source 58
    target 659
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 59
    target 62
  ]
  edge [
    source 59
    target 64
  ]
  edge [
    source 59
    target 65
  ]
  edge [
    source 59
    target 256
  ]
  edge [
    source 59
    target 660
  ]
  edge [
    source 59
    target 80
  ]
  edge [
    source 59
    target 661
  ]
  edge [
    source 59
    target 662
  ]
  edge [
    source 59
    target 167
  ]
  edge [
    source 59
    target 663
  ]
  edge [
    source 59
    target 664
  ]
  edge [
    source 59
    target 665
  ]
  edge [
    source 59
    target 493
  ]
  edge [
    source 59
    target 666
  ]
  edge [
    source 59
    target 599
  ]
  edge [
    source 59
    target 667
  ]
  edge [
    source 59
    target 668
  ]
  edge [
    source 59
    target 497
  ]
  edge [
    source 59
    target 626
  ]
  edge [
    source 59
    target 498
  ]
  edge [
    source 59
    target 93
  ]
  edge [
    source 59
    target 94
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 669
  ]
  edge [
    source 60
    target 670
  ]
  edge [
    source 60
    target 671
  ]
  edge [
    source 60
    target 131
  ]
  edge [
    source 61
    target 80
  ]
  edge [
    source 61
    target 672
  ]
  edge [
    source 61
    target 167
  ]
  edge [
    source 61
    target 673
  ]
  edge [
    source 61
    target 493
  ]
  edge [
    source 61
    target 219
  ]
  edge [
    source 61
    target 674
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 675
  ]
  edge [
    source 62
    target 676
  ]
  edge [
    source 62
    target 677
  ]
  edge [
    source 62
    target 678
  ]
  edge [
    source 62
    target 679
  ]
  edge [
    source 62
    target 680
  ]
  edge [
    source 62
    target 681
  ]
  edge [
    source 62
    target 673
  ]
  edge [
    source 62
    target 175
  ]
  edge [
    source 62
    target 626
  ]
  edge [
    source 62
    target 682
  ]
  edge [
    source 62
    target 683
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 292
  ]
  edge [
    source 64
    target 370
  ]
  edge [
    source 64
    target 610
  ]
  edge [
    source 64
    target 684
  ]
  edge [
    source 64
    target 685
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 67
  ]
  edge [
    source 65
    target 68
  ]
  edge [
    source 65
    target 686
  ]
  edge [
    source 65
    target 687
  ]
  edge [
    source 65
    target 688
  ]
  edge [
    source 65
    target 525
  ]
  edge [
    source 65
    target 689
  ]
  edge [
    source 65
    target 690
  ]
  edge [
    source 65
    target 691
  ]
  edge [
    source 65
    target 105
  ]
  edge [
    source 65
    target 692
  ]
  edge [
    source 65
    target 693
  ]
  edge [
    source 65
    target 694
  ]
  edge [
    source 65
    target 695
  ]
  edge [
    source 65
    target 696
  ]
  edge [
    source 65
    target 697
  ]
  edge [
    source 65
    target 698
  ]
  edge [
    source 65
    target 582
  ]
  edge [
    source 65
    target 699
  ]
  edge [
    source 65
    target 700
  ]
  edge [
    source 65
    target 701
  ]
  edge [
    source 65
    target 702
  ]
  edge [
    source 65
    target 703
  ]
  edge [
    source 65
    target 704
  ]
  edge [
    source 65
    target 88
  ]
  edge [
    source 65
    target 705
  ]
  edge [
    source 65
    target 706
  ]
  edge [
    source 65
    target 65
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 707
  ]
  edge [
    source 66
    target 708
  ]
  edge [
    source 66
    target 167
  ]
  edge [
    source 66
    target 709
  ]
  edge [
    source 66
    target 710
  ]
  edge [
    source 66
    target 598
  ]
  edge [
    source 66
    target 358
  ]
  edge [
    source 66
    target 711
  ]
  edge [
    source 66
    target 712
  ]
  edge [
    source 66
    target 133
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 69
  ]
  edge [
    source 67
    target 133
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 89
  ]
  edge [
    source 69
    target 87
  ]
  edge [
    source 69
    target 713
  ]
  edge [
    source 69
    target 714
  ]
  edge [
    source 69
    target 715
  ]
  edge [
    source 69
    target 716
  ]
  edge [
    source 69
    target 167
  ]
  edge [
    source 69
    target 717
  ]
  edge [
    source 69
    target 718
  ]
  edge [
    source 69
    target 719
  ]
  edge [
    source 69
    target 720
  ]
  edge [
    source 69
    target 696
  ]
  edge [
    source 69
    target 721
  ]
  edge [
    source 69
    target 498
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 77
  ]
  edge [
    source 70
    target 722
  ]
  edge [
    source 70
    target 723
  ]
  edge [
    source 70
    target 531
  ]
  edge [
    source 70
    target 724
  ]
  edge [
    source 70
    target 725
  ]
  edge [
    source 70
    target 726
  ]
  edge [
    source 70
    target 727
  ]
  edge [
    source 70
    target 167
  ]
  edge [
    source 70
    target 728
  ]
  edge [
    source 70
    target 538
  ]
  edge [
    source 70
    target 219
  ]
  edge [
    source 70
    target 729
  ]
  edge [
    source 70
    target 730
  ]
  edge [
    source 70
    target 731
  ]
  edge [
    source 70
    target 732
  ]
  edge [
    source 70
    target 74
  ]
  edge [
    source 70
    target 70
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 78
  ]
  edge [
    source 71
    target 733
  ]
  edge [
    source 71
    target 180
  ]
  edge [
    source 71
    target 734
  ]
  edge [
    source 71
    target 735
  ]
  edge [
    source 71
    target 183
  ]
  edge [
    source 71
    target 736
  ]
  edge [
    source 71
    target 737
  ]
  edge [
    source 71
    target 738
  ]
  edge [
    source 71
    target 85
  ]
  edge [
    source 71
    target 80
  ]
  edge [
    source 71
    target 84
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 739
  ]
  edge [
    source 74
    target 740
  ]
  edge [
    source 74
    target 562
  ]
  edge [
    source 74
    target 352
  ]
  edge [
    source 74
    target 741
  ]
  edge [
    source 74
    target 742
  ]
  edge [
    source 74
    target 743
  ]
  edge [
    source 74
    target 744
  ]
  edge [
    source 74
    target 745
  ]
  edge [
    source 74
    target 746
  ]
  edge [
    source 74
    target 358
  ]
  edge [
    source 74
    target 747
  ]
  edge [
    source 74
    target 748
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 109
  ]
  edge [
    source 75
    target 80
  ]
  edge [
    source 75
    target 115
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 749
  ]
  edge [
    source 76
    target 750
  ]
  edge [
    source 76
    target 751
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 752
  ]
  edge [
    source 78
    target 753
  ]
  edge [
    source 78
    target 754
  ]
  edge [
    source 78
    target 755
  ]
  edge [
    source 78
    target 756
  ]
  edge [
    source 78
    target 757
  ]
  edge [
    source 78
    target 758
  ]
  edge [
    source 78
    target 759
  ]
  edge [
    source 78
    target 760
  ]
  edge [
    source 78
    target 761
  ]
  edge [
    source 78
    target 762
  ]
  edge [
    source 78
    target 763
  ]
  edge [
    source 78
    target 764
  ]
  edge [
    source 78
    target 276
  ]
  edge [
    source 78
    target 765
  ]
  edge [
    source 78
    target 766
  ]
  edge [
    source 78
    target 767
  ]
  edge [
    source 78
    target 768
  ]
  edge [
    source 78
    target 769
  ]
  edge [
    source 78
    target 770
  ]
  edge [
    source 78
    target 281
  ]
  edge [
    source 78
    target 81
  ]
  edge [
    source 78
    target 85
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 82
  ]
  edge [
    source 79
    target 86
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 771
  ]
  edge [
    source 80
    target 772
  ]
  edge [
    source 80
    target 773
  ]
  edge [
    source 80
    target 774
  ]
  edge [
    source 80
    target 775
  ]
  edge [
    source 80
    target 167
  ]
  edge [
    source 80
    target 776
  ]
  edge [
    source 80
    target 777
  ]
  edge [
    source 80
    target 778
  ]
  edge [
    source 80
    target 626
  ]
  edge [
    source 80
    target 779
  ]
  edge [
    source 80
    target 780
  ]
  edge [
    source 80
    target 92
  ]
  edge [
    source 80
    target 113
  ]
  edge [
    source 80
    target 84
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 85
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 781
  ]
  edge [
    source 82
    target 782
  ]
  edge [
    source 82
    target 783
  ]
  edge [
    source 82
    target 784
  ]
  edge [
    source 82
    target 698
  ]
  edge [
    source 82
    target 785
  ]
  edge [
    source 82
    target 86
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 786
  ]
  edge [
    source 83
    target 787
  ]
  edge [
    source 83
    target 788
  ]
  edge [
    source 83
    target 789
  ]
  edge [
    source 83
    target 790
  ]
  edge [
    source 83
    target 791
  ]
  edge [
    source 83
    target 85
  ]
  edge [
    source 83
    target 515
  ]
  edge [
    source 83
    target 792
  ]
  edge [
    source 83
    target 793
  ]
  edge [
    source 83
    target 794
  ]
  edge [
    source 83
    target 795
  ]
  edge [
    source 83
    target 796
  ]
  edge [
    source 83
    target 797
  ]
  edge [
    source 83
    target 780
  ]
  edge [
    source 83
    target 798
  ]
  edge [
    source 84
    target 799
  ]
  edge [
    source 84
    target 244
  ]
  edge [
    source 84
    target 213
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 87
  ]
  edge [
    source 85
    target 90
  ]
  edge [
    source 85
    target 800
  ]
  edge [
    source 85
    target 801
  ]
  edge [
    source 85
    target 802
  ]
  edge [
    source 85
    target 803
  ]
  edge [
    source 85
    target 804
  ]
  edge [
    source 85
    target 752
  ]
  edge [
    source 85
    target 805
  ]
  edge [
    source 85
    target 806
  ]
  edge [
    source 85
    target 807
  ]
  edge [
    source 85
    target 808
  ]
  edge [
    source 85
    target 809
  ]
  edge [
    source 85
    target 810
  ]
  edge [
    source 85
    target 811
  ]
  edge [
    source 85
    target 812
  ]
  edge [
    source 85
    target 813
  ]
  edge [
    source 85
    target 814
  ]
  edge [
    source 85
    target 815
  ]
  edge [
    source 85
    target 816
  ]
  edge [
    source 85
    target 817
  ]
  edge [
    source 85
    target 818
  ]
  edge [
    source 85
    target 819
  ]
  edge [
    source 85
    target 820
  ]
  edge [
    source 85
    target 762
  ]
  edge [
    source 85
    target 821
  ]
  edge [
    source 85
    target 822
  ]
  edge [
    source 85
    target 823
  ]
  edge [
    source 85
    target 824
  ]
  edge [
    source 85
    target 825
  ]
  edge [
    source 85
    target 826
  ]
  edge [
    source 85
    target 827
  ]
  edge [
    source 85
    target 828
  ]
  edge [
    source 85
    target 829
  ]
  edge [
    source 85
    target 830
  ]
  edge [
    source 85
    target 831
  ]
  edge [
    source 85
    target 832
  ]
  edge [
    source 85
    target 766
  ]
  edge [
    source 85
    target 833
  ]
  edge [
    source 85
    target 224
  ]
  edge [
    source 85
    target 834
  ]
  edge [
    source 85
    target 769
  ]
  edge [
    source 85
    target 835
  ]
  edge [
    source 85
    target 358
  ]
  edge [
    source 85
    target 836
  ]
  edge [
    source 85
    target 837
  ]
  edge [
    source 85
    target 838
  ]
  edge [
    source 85
    target 839
  ]
  edge [
    source 85
    target 840
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 841
  ]
  edge [
    source 86
    target 842
  ]
  edge [
    source 86
    target 843
  ]
  edge [
    source 86
    target 844
  ]
  edge [
    source 86
    target 845
  ]
  edge [
    source 86
    target 846
  ]
  edge [
    source 86
    target 350
  ]
  edge [
    source 86
    target 847
  ]
  edge [
    source 86
    target 848
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 92
  ]
  edge [
    source 87
    target 93
  ]
  edge [
    source 87
    target 94
  ]
  edge [
    source 87
    target 849
  ]
  edge [
    source 87
    target 108
  ]
  edge [
    source 87
    target 89
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 686
  ]
  edge [
    source 88
    target 688
  ]
  edge [
    source 88
    target 850
  ]
  edge [
    source 88
    target 689
  ]
  edge [
    source 88
    target 851
  ]
  edge [
    source 88
    target 691
  ]
  edge [
    source 88
    target 690
  ]
  edge [
    source 88
    target 105
  ]
  edge [
    source 88
    target 692
  ]
  edge [
    source 88
    target 693
  ]
  edge [
    source 88
    target 852
  ]
  edge [
    source 88
    target 694
  ]
  edge [
    source 88
    target 458
  ]
  edge [
    source 88
    target 853
  ]
  edge [
    source 88
    target 854
  ]
  edge [
    source 88
    target 696
  ]
  edge [
    source 88
    target 855
  ]
  edge [
    source 88
    target 695
  ]
  edge [
    source 88
    target 531
  ]
  edge [
    source 88
    target 225
  ]
  edge [
    source 88
    target 856
  ]
  edge [
    source 88
    target 700
  ]
  edge [
    source 88
    target 857
  ]
  edge [
    source 88
    target 701
  ]
  edge [
    source 88
    target 702
  ]
  edge [
    source 88
    target 858
  ]
  edge [
    source 88
    target 859
  ]
  edge [
    source 88
    target 860
  ]
  edge [
    source 88
    target 861
  ]
  edge [
    source 88
    target 703
  ]
  edge [
    source 88
    target 862
  ]
  edge [
    source 88
    target 863
  ]
  edge [
    source 88
    target 864
  ]
  edge [
    source 88
    target 865
  ]
  edge [
    source 88
    target 866
  ]
  edge [
    source 88
    target 215
  ]
  edge [
    source 88
    target 867
  ]
  edge [
    source 88
    target 868
  ]
  edge [
    source 88
    target 869
  ]
  edge [
    source 88
    target 870
  ]
  edge [
    source 88
    target 706
  ]
  edge [
    source 88
    target 871
  ]
  edge [
    source 88
    target 872
  ]
  edge [
    source 88
    target 873
  ]
  edge [
    source 88
    target 93
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 97
  ]
  edge [
    source 90
    target 92
  ]
  edge [
    source 90
    target 874
  ]
  edge [
    source 90
    target 875
  ]
  edge [
    source 90
    target 876
  ]
  edge [
    source 90
    target 186
  ]
  edge [
    source 90
    target 877
  ]
  edge [
    source 90
    target 878
  ]
  edge [
    source 90
    target 879
  ]
  edge [
    source 90
    target 880
  ]
  edge [
    source 90
    target 881
  ]
  edge [
    source 90
    target 882
  ]
  edge [
    source 90
    target 883
  ]
  edge [
    source 90
    target 884
  ]
  edge [
    source 90
    target 885
  ]
  edge [
    source 90
    target 886
  ]
  edge [
    source 90
    target 887
  ]
  edge [
    source 90
    target 888
  ]
  edge [
    source 90
    target 128
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 889
  ]
  edge [
    source 91
    target 890
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 98
  ]
  edge [
    source 92
    target 101
  ]
  edge [
    source 92
    target 102
  ]
  edge [
    source 92
    target 529
  ]
  edge [
    source 92
    target 891
  ]
  edge [
    source 92
    target 775
  ]
  edge [
    source 92
    target 892
  ]
  edge [
    source 92
    target 893
  ]
  edge [
    source 92
    target 894
  ]
  edge [
    source 92
    target 895
  ]
  edge [
    source 92
    target 896
  ]
  edge [
    source 92
    target 897
  ]
  edge [
    source 92
    target 626
  ]
  edge [
    source 92
    target 898
  ]
  edge [
    source 93
    target 899
  ]
  edge [
    source 93
    target 900
  ]
  edge [
    source 93
    target 661
  ]
  edge [
    source 93
    target 901
  ]
  edge [
    source 93
    target 902
  ]
  edge [
    source 93
    target 780
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 903
  ]
  edge [
    source 94
    target 904
  ]
  edge [
    source 94
    target 905
  ]
  edge [
    source 94
    target 906
  ]
  edge [
    source 94
    target 907
  ]
  edge [
    source 94
    target 100
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 908
  ]
  edge [
    source 95
    target 909
  ]
  edge [
    source 95
    target 910
  ]
  edge [
    source 95
    target 538
  ]
  edge [
    source 95
    target 911
  ]
  edge [
    source 95
    target 912
  ]
  edge [
    source 95
    target 913
  ]
  edge [
    source 95
    target 914
  ]
  edge [
    source 95
    target 915
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 916
  ]
  edge [
    source 96
    target 906
  ]
  edge [
    source 96
    target 917
  ]
  edge [
    source 96
    target 918
  ]
  edge [
    source 96
    target 919
  ]
  edge [
    source 96
    target 920
  ]
  edge [
    source 96
    target 132
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 921
  ]
  edge [
    source 98
    target 922
  ]
  edge [
    source 98
    target 923
  ]
  edge [
    source 98
    target 924
  ]
  edge [
    source 98
    target 925
  ]
  edge [
    source 98
    target 926
  ]
  edge [
    source 98
    target 917
  ]
  edge [
    source 98
    target 927
  ]
  edge [
    source 98
    target 928
  ]
  edge [
    source 98
    target 929
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 930
  ]
  edge [
    source 99
    target 931
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 932
  ]
  edge [
    source 100
    target 917
  ]
  edge [
    source 101
    target 933
  ]
  edge [
    source 101
    target 934
  ]
  edge [
    source 101
    target 935
  ]
  edge [
    source 101
    target 936
  ]
  edge [
    source 101
    target 937
  ]
  edge [
    source 101
    target 938
  ]
  edge [
    source 101
    target 939
  ]
  edge [
    source 101
    target 940
  ]
  edge [
    source 101
    target 941
  ]
  edge [
    source 101
    target 942
  ]
  edge [
    source 101
    target 943
  ]
  edge [
    source 101
    target 944
  ]
  edge [
    source 101
    target 945
  ]
  edge [
    source 101
    target 946
  ]
  edge [
    source 101
    target 947
  ]
  edge [
    source 101
    target 948
  ]
  edge [
    source 101
    target 949
  ]
  edge [
    source 101
    target 950
  ]
  edge [
    source 101
    target 951
  ]
  edge [
    source 101
    target 952
  ]
  edge [
    source 101
    target 953
  ]
  edge [
    source 101
    target 954
  ]
  edge [
    source 101
    target 955
  ]
  edge [
    source 101
    target 137
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 338
  ]
  edge [
    source 102
    target 640
  ]
  edge [
    source 102
    target 956
  ]
  edge [
    source 102
    target 957
  ]
  edge [
    source 102
    target 109
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 105
  ]
  edge [
    source 103
    target 106
  ]
  edge [
    source 103
    target 111
  ]
  edge [
    source 103
    target 108
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 106
  ]
  edge [
    source 104
    target 112
  ]
  edge [
    source 104
    target 113
  ]
  edge [
    source 104
    target 114
  ]
  edge [
    source 104
    target 675
  ]
  edge [
    source 104
    target 958
  ]
  edge [
    source 104
    target 959
  ]
  edge [
    source 104
    target 960
  ]
  edge [
    source 104
    target 626
  ]
  edge [
    source 104
    target 961
  ]
  edge [
    source 104
    target 962
  ]
  edge [
    source 104
    target 963
  ]
  edge [
    source 104
    target 964
  ]
  edge [
    source 104
    target 665
  ]
  edge [
    source 104
    target 965
  ]
  edge [
    source 104
    target 666
  ]
  edge [
    source 104
    target 256
  ]
  edge [
    source 104
    target 966
  ]
  edge [
    source 104
    target 432
  ]
  edge [
    source 104
    target 967
  ]
  edge [
    source 104
    target 968
  ]
  edge [
    source 104
    target 969
  ]
  edge [
    source 104
    target 970
  ]
  edge [
    source 104
    target 971
  ]
  edge [
    source 104
    target 972
  ]
  edge [
    source 104
    target 973
  ]
  edge [
    source 104
    target 107
  ]
  edge [
    source 104
    target 974
  ]
  edge [
    source 104
    target 634
  ]
  edge [
    source 105
    target 110
  ]
  edge [
    source 105
    target 111
  ]
  edge [
    source 105
    target 975
  ]
  edge [
    source 105
    target 352
  ]
  edge [
    source 105
    target 267
  ]
  edge [
    source 105
    target 976
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 977
  ]
  edge [
    source 106
    target 978
  ]
  edge [
    source 106
    target 979
  ]
  edge [
    source 106
    target 980
  ]
  edge [
    source 106
    target 981
  ]
  edge [
    source 106
    target 982
  ]
  edge [
    source 106
    target 983
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 984
  ]
  edge [
    source 107
    target 985
  ]
  edge [
    source 107
    target 986
  ]
  edge [
    source 107
    target 987
  ]
  edge [
    source 107
    target 988
  ]
  edge [
    source 107
    target 989
  ]
  edge [
    source 107
    target 990
  ]
  edge [
    source 107
    target 991
  ]
  edge [
    source 107
    target 980
  ]
  edge [
    source 107
    target 992
  ]
  edge [
    source 107
    target 993
  ]
  edge [
    source 107
    target 994
  ]
  edge [
    source 107
    target 995
  ]
  edge [
    source 107
    target 996
  ]
  edge [
    source 107
    target 997
  ]
  edge [
    source 107
    target 998
  ]
  edge [
    source 107
    target 999
  ]
  edge [
    source 107
    target 1000
  ]
  edge [
    source 107
    target 1001
  ]
  edge [
    source 107
    target 1002
  ]
  edge [
    source 107
    target 1003
  ]
  edge [
    source 107
    target 1004
  ]
  edge [
    source 107
    target 1005
  ]
  edge [
    source 107
    target 1006
  ]
  edge [
    source 107
    target 1007
  ]
  edge [
    source 107
    target 1008
  ]
  edge [
    source 107
    target 1009
  ]
  edge [
    source 107
    target 1010
  ]
  edge [
    source 107
    target 1011
  ]
  edge [
    source 107
    target 1012
  ]
  edge [
    source 107
    target 1013
  ]
  edge [
    source 107
    target 1014
  ]
  edge [
    source 107
    target 1015
  ]
  edge [
    source 107
    target 1016
  ]
  edge [
    source 107
    target 1017
  ]
  edge [
    source 107
    target 1018
  ]
  edge [
    source 107
    target 1019
  ]
  edge [
    source 107
    target 1020
  ]
  edge [
    source 107
    target 1021
  ]
  edge [
    source 107
    target 1022
  ]
  edge [
    source 107
    target 1023
  ]
  edge [
    source 107
    target 1024
  ]
  edge [
    source 107
    target 1025
  ]
  edge [
    source 107
    target 1026
  ]
  edge [
    source 107
    target 1027
  ]
  edge [
    source 107
    target 1028
  ]
  edge [
    source 107
    target 1029
  ]
  edge [
    source 107
    target 1030
  ]
  edge [
    source 107
    target 1031
  ]
  edge [
    source 107
    target 1032
  ]
  edge [
    source 107
    target 1033
  ]
  edge [
    source 107
    target 1034
  ]
  edge [
    source 107
    target 1035
  ]
  edge [
    source 107
    target 1036
  ]
  edge [
    source 107
    target 1037
  ]
  edge [
    source 107
    target 1038
  ]
  edge [
    source 107
    target 1039
  ]
  edge [
    source 107
    target 1040
  ]
  edge [
    source 107
    target 1041
  ]
  edge [
    source 107
    target 1042
  ]
  edge [
    source 107
    target 1043
  ]
  edge [
    source 107
    target 1044
  ]
  edge [
    source 107
    target 1045
  ]
  edge [
    source 107
    target 1046
  ]
  edge [
    source 107
    target 1047
  ]
  edge [
    source 107
    target 1048
  ]
  edge [
    source 107
    target 1049
  ]
  edge [
    source 107
    target 110
  ]
  edge [
    source 107
    target 116
  ]
  edge [
    source 107
    target 121
  ]
  edge [
    source 108
    target 1050
  ]
  edge [
    source 108
    target 1051
  ]
  edge [
    source 108
    target 492
  ]
  edge [
    source 108
    target 1052
  ]
  edge [
    source 108
    target 1053
  ]
  edge [
    source 108
    target 112
  ]
  edge [
    source 108
    target 780
  ]
  edge [
    source 108
    target 111
  ]
  edge [
    source 109
    target 1054
  ]
  edge [
    source 109
    target 1055
  ]
  edge [
    source 109
    target 167
  ]
  edge [
    source 109
    target 1056
  ]
  edge [
    source 109
    target 1057
  ]
  edge [
    source 109
    target 1058
  ]
  edge [
    source 110
    target 1059
  ]
  edge [
    source 110
    target 1060
  ]
  edge [
    source 110
    target 1061
  ]
  edge [
    source 110
    target 1062
  ]
  edge [
    source 110
    target 1063
  ]
  edge [
    source 110
    target 1064
  ]
  edge [
    source 110
    target 1065
  ]
  edge [
    source 110
    target 116
  ]
  edge [
    source 110
    target 121
  ]
  edge [
    source 112
    target 224
  ]
  edge [
    source 112
    target 1066
  ]
  edge [
    source 112
    target 1067
  ]
  edge [
    source 112
    target 167
  ]
  edge [
    source 112
    target 1068
  ]
  edge [
    source 112
    target 492
  ]
  edge [
    source 112
    target 495
  ]
  edge [
    source 112
    target 1069
  ]
  edge [
    source 112
    target 1070
  ]
  edge [
    source 112
    target 1071
  ]
  edge [
    source 113
    target 1072
  ]
  edge [
    source 113
    target 1073
  ]
  edge [
    source 113
    target 1074
  ]
  edge [
    source 113
    target 1075
  ]
  edge [
    source 113
    target 1076
  ]
  edge [
    source 113
    target 1077
  ]
  edge [
    source 113
    target 1078
  ]
  edge [
    source 113
    target 1079
  ]
  edge [
    source 113
    target 1080
  ]
  edge [
    source 113
    target 1081
  ]
  edge [
    source 113
    target 1082
  ]
  edge [
    source 113
    target 1083
  ]
  edge [
    source 113
    target 1084
  ]
  edge [
    source 113
    target 1085
  ]
  edge [
    source 113
    target 1086
  ]
  edge [
    source 113
    target 1087
  ]
  edge [
    source 113
    target 626
  ]
  edge [
    source 113
    target 1088
  ]
  edge [
    source 113
    target 1089
  ]
  edge [
    source 113
    target 1090
  ]
  edge [
    source 113
    target 775
  ]
  edge [
    source 113
    target 1091
  ]
  edge [
    source 113
    target 1092
  ]
  edge [
    source 113
    target 167
  ]
  edge [
    source 113
    target 1093
  ]
  edge [
    source 113
    target 1094
  ]
  edge [
    source 113
    target 1095
  ]
  edge [
    source 113
    target 1096
  ]
  edge [
    source 113
    target 1097
  ]
  edge [
    source 113
    target 1098
  ]
  edge [
    source 113
    target 1099
  ]
  edge [
    source 113
    target 1100
  ]
  edge [
    source 113
    target 1101
  ]
  edge [
    source 113
    target 1102
  ]
  edge [
    source 113
    target 1103
  ]
  edge [
    source 113
    target 1104
  ]
  edge [
    source 113
    target 1105
  ]
  edge [
    source 113
    target 1106
  ]
  edge [
    source 113
    target 1107
  ]
  edge [
    source 113
    target 1108
  ]
  edge [
    source 113
    target 1109
  ]
  edge [
    source 113
    target 1110
  ]
  edge [
    source 113
    target 1111
  ]
  edge [
    source 113
    target 1112
  ]
  edge [
    source 113
    target 127
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 115
    target 1113
  ]
  edge [
    source 115
    target 524
  ]
  edge [
    source 115
    target 1114
  ]
  edge [
    source 115
    target 1115
  ]
  edge [
    source 115
    target 1116
  ]
  edge [
    source 115
    target 1117
  ]
  edge [
    source 115
    target 1118
  ]
  edge [
    source 115
    target 1119
  ]
  edge [
    source 115
    target 1120
  ]
  edge [
    source 115
    target 1121
  ]
  edge [
    source 115
    target 1122
  ]
  edge [
    source 115
    target 1123
  ]
  edge [
    source 115
    target 1124
  ]
  edge [
    source 115
    target 132
  ]
  edge [
    source 115
    target 1125
  ]
  edge [
    source 115
    target 1126
  ]
  edge [
    source 115
    target 1127
  ]
  edge [
    source 115
    target 1128
  ]
  edge [
    source 115
    target 1129
  ]
  edge [
    source 115
    target 1130
  ]
  edge [
    source 115
    target 1131
  ]
  edge [
    source 115
    target 1132
  ]
  edge [
    source 115
    target 1133
  ]
  edge [
    source 115
    target 124
  ]
  edge [
    source 115
    target 1134
  ]
  edge [
    source 115
    target 1135
  ]
  edge [
    source 115
    target 1136
  ]
  edge [
    source 115
    target 562
  ]
  edge [
    source 115
    target 1137
  ]
  edge [
    source 115
    target 1138
  ]
  edge [
    source 115
    target 1139
  ]
  edge [
    source 115
    target 1140
  ]
  edge [
    source 115
    target 1141
  ]
  edge [
    source 115
    target 1142
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 1143
  ]
  edge [
    source 116
    target 1144
  ]
  edge [
    source 116
    target 1145
  ]
  edge [
    source 116
    target 1146
  ]
  edge [
    source 116
    target 1147
  ]
  edge [
    source 116
    target 1148
  ]
  edge [
    source 116
    target 1149
  ]
  edge [
    source 116
    target 1150
  ]
  edge [
    source 116
    target 1151
  ]
  edge [
    source 116
    target 1152
  ]
  edge [
    source 116
    target 1153
  ]
  edge [
    source 116
    target 121
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 1073
  ]
  edge [
    source 117
    target 1154
  ]
  edge [
    source 117
    target 1155
  ]
  edge [
    source 117
    target 1156
  ]
  edge [
    source 117
    target 1157
  ]
  edge [
    source 117
    target 1158
  ]
  edge [
    source 117
    target 626
  ]
  edge [
    source 117
    target 1159
  ]
  edge [
    source 117
    target 1160
  ]
  edge [
    source 117
    target 167
  ]
  edge [
    source 117
    target 1161
  ]
  edge [
    source 117
    target 1162
  ]
  edge [
    source 117
    target 1163
  ]
  edge [
    source 117
    target 1164
  ]
  edge [
    source 117
    target 739
  ]
  edge [
    source 117
    target 1165
  ]
  edge [
    source 117
    target 1166
  ]
  edge [
    source 117
    target 175
  ]
  edge [
    source 117
    target 1167
  ]
  edge [
    source 117
    target 1168
  ]
  edge [
    source 117
    target 176
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 1072
  ]
  edge [
    source 118
    target 1169
  ]
  edge [
    source 118
    target 1170
  ]
  edge [
    source 118
    target 1171
  ]
  edge [
    source 118
    target 1172
  ]
  edge [
    source 118
    target 1173
  ]
  edge [
    source 118
    target 1174
  ]
  edge [
    source 118
    target 1175
  ]
  edge [
    source 118
    target 1176
  ]
  edge [
    source 118
    target 531
  ]
  edge [
    source 118
    target 1177
  ]
  edge [
    source 118
    target 1178
  ]
  edge [
    source 118
    target 167
  ]
  edge [
    source 118
    target 1179
  ]
  edge [
    source 118
    target 1180
  ]
  edge [
    source 118
    target 1181
  ]
  edge [
    source 118
    target 1182
  ]
  edge [
    source 118
    target 1101
  ]
  edge [
    source 118
    target 1183
  ]
  edge [
    source 118
    target 1184
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 1185
  ]
  edge [
    source 119
    target 1186
  ]
  edge [
    source 119
    target 1187
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 1188
  ]
  edge [
    source 120
    target 1189
  ]
  edge [
    source 120
    target 1190
  ]
  edge [
    source 120
    target 1191
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 1192
  ]
  edge [
    source 121
    target 1193
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 1194
  ]
  edge [
    source 122
    target 1195
  ]
  edge [
    source 122
    target 1075
  ]
  edge [
    source 122
    target 1196
  ]
  edge [
    source 122
    target 1197
  ]
  edge [
    source 122
    target 1198
  ]
  edge [
    source 122
    target 1080
  ]
  edge [
    source 122
    target 1199
  ]
  edge [
    source 122
    target 1200
  ]
  edge [
    source 122
    target 1201
  ]
  edge [
    source 122
    target 1081
  ]
  edge [
    source 122
    target 1202
  ]
  edge [
    source 122
    target 1203
  ]
  edge [
    source 122
    target 1204
  ]
  edge [
    source 122
    target 1205
  ]
  edge [
    source 122
    target 1206
  ]
  edge [
    source 122
    target 1207
  ]
  edge [
    source 122
    target 626
  ]
  edge [
    source 122
    target 1208
  ]
  edge [
    source 122
    target 1209
  ]
  edge [
    source 122
    target 1210
  ]
  edge [
    source 122
    target 1092
  ]
  edge [
    source 122
    target 1093
  ]
  edge [
    source 122
    target 276
  ]
  edge [
    source 122
    target 1211
  ]
  edge [
    source 122
    target 1212
  ]
  edge [
    source 122
    target 432
  ]
  edge [
    source 122
    target 1099
  ]
  edge [
    source 122
    target 1213
  ]
  edge [
    source 122
    target 224
  ]
  edge [
    source 122
    target 1214
  ]
  edge [
    source 122
    target 790
  ]
  edge [
    source 122
    target 1215
  ]
  edge [
    source 122
    target 1216
  ]
  edge [
    source 122
    target 1217
  ]
  edge [
    source 122
    target 1218
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 1219
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 512
  ]
  edge [
    source 124
    target 1220
  ]
  edge [
    source 124
    target 1221
  ]
  edge [
    source 124
    target 191
  ]
  edge [
    source 124
    target 1093
  ]
  edge [
    source 124
    target 1222
  ]
  edge [
    source 124
    target 1223
  ]
  edge [
    source 124
    target 1224
  ]
  edge [
    source 124
    target 1225
  ]
  edge [
    source 124
    target 1226
  ]
  edge [
    source 124
    target 1227
  ]
  edge [
    source 124
    target 1228
  ]
  edge [
    source 124
    target 127
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 1229
  ]
  edge [
    source 125
    target 1230
  ]
  edge [
    source 125
    target 1231
  ]
  edge [
    source 125
    target 1232
  ]
  edge [
    source 125
    target 1233
  ]
  edge [
    source 125
    target 1234
  ]
  edge [
    source 125
    target 1235
  ]
  edge [
    source 125
    target 1236
  ]
  edge [
    source 125
    target 1237
  ]
  edge [
    source 125
    target 1238
  ]
  edge [
    source 125
    target 1239
  ]
  edge [
    source 125
    target 1240
  ]
  edge [
    source 125
    target 138
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 1241
  ]
  edge [
    source 126
    target 135
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 131
  ]
  edge [
    source 127
    target 1242
  ]
  edge [
    source 127
    target 224
  ]
  edge [
    source 127
    target 1243
  ]
  edge [
    source 127
    target 1244
  ]
  edge [
    source 127
    target 1245
  ]
  edge [
    source 127
    target 130
  ]
  edge [
    source 127
    target 1246
  ]
  edge [
    source 127
    target 1247
  ]
  edge [
    source 127
    target 1248
  ]
  edge [
    source 127
    target 1249
  ]
  edge [
    source 127
    target 1112
  ]
  edge [
    source 127
    target 1250
  ]
  edge [
    source 127
    target 1251
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 132
  ]
  edge [
    source 128
    target 591
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 1252
  ]
  edge [
    source 129
    target 1253
  ]
  edge [
    source 129
    target 1254
  ]
  edge [
    source 129
    target 1255
  ]
  edge [
    source 129
    target 905
  ]
  edge [
    source 129
    target 1256
  ]
  edge [
    source 129
    target 1257
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 130
    target 1258
  ]
  edge [
    source 130
    target 1259
  ]
  edge [
    source 130
    target 1260
  ]
  edge [
    source 130
    target 1261
  ]
  edge [
    source 131
    target 1262
  ]
  edge [
    source 131
    target 1263
  ]
  edge [
    source 131
    target 1264
  ]
  edge [
    source 131
    target 1265
  ]
  edge [
    source 131
    target 1266
  ]
  edge [
    source 131
    target 1267
  ]
  edge [
    source 131
    target 1268
  ]
  edge [
    source 131
    target 1269
  ]
  edge [
    source 131
    target 1270
  ]
  edge [
    source 131
    target 1271
  ]
  edge [
    source 131
    target 1272
  ]
  edge [
    source 131
    target 1273
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 132
    target 1274
  ]
  edge [
    source 132
    target 1275
  ]
  edge [
    source 132
    target 1276
  ]
  edge [
    source 132
    target 1277
  ]
  edge [
    source 132
    target 1278
  ]
  edge [
    source 132
    target 1279
  ]
  edge [
    source 132
    target 1280
  ]
  edge [
    source 132
    target 1281
  ]
  edge [
    source 132
    target 1282
  ]
  edge [
    source 132
    target 512
  ]
  edge [
    source 132
    target 1283
  ]
  edge [
    source 132
    target 1284
  ]
  edge [
    source 132
    target 295
  ]
  edge [
    source 132
    target 1285
  ]
  edge [
    source 132
    target 1286
  ]
  edge [
    source 132
    target 626
  ]
  edge [
    source 132
    target 1287
  ]
  edge [
    source 132
    target 1288
  ]
  edge [
    source 132
    target 1289
  ]
  edge [
    source 132
    target 1290
  ]
  edge [
    source 132
    target 1291
  ]
  edge [
    source 132
    target 1292
  ]
  edge [
    source 132
    target 1293
  ]
  edge [
    source 132
    target 1294
  ]
  edge [
    source 132
    target 1295
  ]
  edge [
    source 132
    target 1296
  ]
  edge [
    source 132
    target 1297
  ]
  edge [
    source 132
    target 1298
  ]
  edge [
    source 132
    target 1299
  ]
  edge [
    source 132
    target 1300
  ]
  edge [
    source 132
    target 1301
  ]
  edge [
    source 132
    target 1302
  ]
  edge [
    source 132
    target 1303
  ]
  edge [
    source 132
    target 1304
  ]
  edge [
    source 132
    target 1305
  ]
  edge [
    source 132
    target 1306
  ]
  edge [
    source 132
    target 1307
  ]
  edge [
    source 132
    target 1308
  ]
  edge [
    source 132
    target 358
  ]
  edge [
    source 132
    target 1309
  ]
  edge [
    source 132
    target 1310
  ]
  edge [
    source 132
    target 1311
  ]
  edge [
    source 132
    target 1312
  ]
  edge [
    source 132
    target 1116
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 133
    target 1313
  ]
  edge [
    source 133
    target 1314
  ]
  edge [
    source 133
    target 1315
  ]
  edge [
    source 133
    target 1316
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 134
    target 1317
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 135
    target 1318
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 136
    target 1319
  ]
  edge [
    source 136
    target 1318
  ]
  edge [
    source 136
    target 1320
  ]
  edge [
    source 137
    target 138
  ]
  edge [
    source 137
    target 1321
  ]
  edge [
    source 137
    target 1322
  ]
  edge [
    source 137
    target 1323
  ]
  edge [
    source 137
    target 1324
  ]
  edge [
    source 137
    target 166
  ]
  edge [
    source 137
    target 1325
  ]
  edge [
    source 137
    target 1326
  ]
  edge [
    source 137
    target 1327
  ]
  edge [
    source 137
    target 1328
  ]
  edge [
    source 137
    target 1329
  ]
  edge [
    source 137
    target 1330
  ]
  edge [
    source 138
    target 1331
  ]
  edge [
    source 138
    target 224
  ]
  edge [
    source 138
    target 1332
  ]
  edge [
    source 138
    target 1333
  ]
]
