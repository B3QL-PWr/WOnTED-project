graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "pa&#324;stwowy"
    origin "text"
  ]
  node [
    id 1
    label "pe&#322;nia"
    origin "text"
  ]
  node [
    id 2
    label "legalny"
    origin "text"
  ]
  node [
    id 3
    label "narodowy"
    origin "text"
  ]
  node [
    id 4
    label "kasyno"
    origin "text"
  ]
  node [
    id 5
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 6
    label "dzia&#322;o"
    origin "text"
  ]
  node [
    id 7
    label "pa&#324;stwowo"
  ]
  node [
    id 8
    label "upa&#324;stwowienie"
  ]
  node [
    id 9
    label "upa&#324;stwawianie"
  ]
  node [
    id 10
    label "wsp&#243;lny"
  ]
  node [
    id 11
    label "zakres"
  ]
  node [
    id 12
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 13
    label "szczyt"
  ]
  node [
    id 14
    label "gajny"
  ]
  node [
    id 15
    label "legalnie"
  ]
  node [
    id 16
    label "nacjonalistyczny"
  ]
  node [
    id 17
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 18
    label "narodowo"
  ]
  node [
    id 19
    label "wa&#380;ny"
  ]
  node [
    id 20
    label "zak&#322;ad"
  ]
  node [
    id 21
    label "lokal"
  ]
  node [
    id 22
    label "gastronomia"
  ]
  node [
    id 23
    label "krupier"
  ]
  node [
    id 24
    label "dom"
  ]
  node [
    id 25
    label "bateria"
  ]
  node [
    id 26
    label "laweta"
  ]
  node [
    id 27
    label "bro&#324;_artyleryjska"
  ]
  node [
    id 28
    label "bro&#324;"
  ]
  node [
    id 29
    label "oporopowrotnik"
  ]
  node [
    id 30
    label "przedmuchiwacz"
  ]
  node [
    id 31
    label "artyleria"
  ]
  node [
    id 32
    label "waln&#261;&#263;"
  ]
  node [
    id 33
    label "bateria_artylerii"
  ]
  node [
    id 34
    label "cannon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
]
