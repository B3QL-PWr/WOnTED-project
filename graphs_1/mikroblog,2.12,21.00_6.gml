graph [
  maxDegree 38
  minDegree 1
  meanDegree 1.9714285714285715
  density 0.02857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kot"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;ga&#263;"
  ]
  node [
    id 4
    label "trwa&#263;"
  ]
  node [
    id 5
    label "obecno&#347;&#263;"
  ]
  node [
    id 6
    label "stan"
  ]
  node [
    id 7
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 8
    label "stand"
  ]
  node [
    id 9
    label "mie&#263;_miejsce"
  ]
  node [
    id 10
    label "uczestniczy&#263;"
  ]
  node [
    id 11
    label "chodzi&#263;"
  ]
  node [
    id 12
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 13
    label "equal"
  ]
  node [
    id 14
    label "zamiaucze&#263;"
  ]
  node [
    id 15
    label "pierwszoklasista"
  ]
  node [
    id 16
    label "fala"
  ]
  node [
    id 17
    label "miaucze&#263;"
  ]
  node [
    id 18
    label "kabanos"
  ]
  node [
    id 19
    label "kotowate"
  ]
  node [
    id 20
    label "miaukni&#281;cie"
  ]
  node [
    id 21
    label "samiec"
  ]
  node [
    id 22
    label "otrz&#281;siny"
  ]
  node [
    id 23
    label "miauczenie"
  ]
  node [
    id 24
    label "czworon&#243;g"
  ]
  node [
    id 25
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 26
    label "rekrut"
  ]
  node [
    id 27
    label "zaj&#261;c"
  ]
  node [
    id 28
    label "kotwica"
  ]
  node [
    id 29
    label "trackball"
  ]
  node [
    id 30
    label "felinoterapia"
  ]
  node [
    id 31
    label "odk&#322;aczacz"
  ]
  node [
    id 32
    label "zamiauczenie"
  ]
  node [
    id 33
    label "czasokres"
  ]
  node [
    id 34
    label "trawienie"
  ]
  node [
    id 35
    label "kategoria_gramatyczna"
  ]
  node [
    id 36
    label "period"
  ]
  node [
    id 37
    label "odczyt"
  ]
  node [
    id 38
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 39
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 40
    label "chwila"
  ]
  node [
    id 41
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 42
    label "poprzedzenie"
  ]
  node [
    id 43
    label "koniugacja"
  ]
  node [
    id 44
    label "dzieje"
  ]
  node [
    id 45
    label "poprzedzi&#263;"
  ]
  node [
    id 46
    label "przep&#322;ywanie"
  ]
  node [
    id 47
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 48
    label "odwlekanie_si&#281;"
  ]
  node [
    id 49
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 50
    label "Zeitgeist"
  ]
  node [
    id 51
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 52
    label "okres_czasu"
  ]
  node [
    id 53
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 54
    label "pochodzi&#263;"
  ]
  node [
    id 55
    label "schy&#322;ek"
  ]
  node [
    id 56
    label "czwarty_wymiar"
  ]
  node [
    id 57
    label "chronometria"
  ]
  node [
    id 58
    label "poprzedzanie"
  ]
  node [
    id 59
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 60
    label "pogoda"
  ]
  node [
    id 61
    label "zegar"
  ]
  node [
    id 62
    label "trawi&#263;"
  ]
  node [
    id 63
    label "pochodzenie"
  ]
  node [
    id 64
    label "poprzedza&#263;"
  ]
  node [
    id 65
    label "time_period"
  ]
  node [
    id 66
    label "rachuba_czasu"
  ]
  node [
    id 67
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 68
    label "czasoprzestrze&#324;"
  ]
  node [
    id 69
    label "laba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
]
