graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9789473684210526
  density 0.021052631578947368
  graphCliqueNumber 2
  node [
    id 0
    label "czynno&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kontrolny"
    origin "text"
  ]
  node [
    id 2
    label "dokonywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przy"
    origin "text"
  ]
  node [
    id 5
    label "wsp&#243;&#322;udzia&#322;"
    origin "text"
  ]
  node [
    id 6
    label "kierownik"
    origin "text"
  ]
  node [
    id 7
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 8
    label "opieka"
    origin "text"
  ]
  node [
    id 9
    label "zdrowotny"
    origin "text"
  ]
  node [
    id 10
    label "lub"
    origin "text"
  ]
  node [
    id 11
    label "osoba"
    origin "text"
  ]
  node [
    id 12
    label "przez"
    origin "text"
  ]
  node [
    id 13
    label "upowa&#380;ni&#263;"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "activity"
  ]
  node [
    id 16
    label "wydarzenie"
  ]
  node [
    id 17
    label "bezproblemowy"
  ]
  node [
    id 18
    label "supervision"
  ]
  node [
    id 19
    label "kontrolnie"
  ]
  node [
    id 20
    label "pr&#243;bny"
  ]
  node [
    id 21
    label "robi&#263;"
  ]
  node [
    id 22
    label "determine"
  ]
  node [
    id 23
    label "przestawa&#263;"
  ]
  node [
    id 24
    label "make"
  ]
  node [
    id 25
    label "si&#281;ga&#263;"
  ]
  node [
    id 26
    label "trwa&#263;"
  ]
  node [
    id 27
    label "obecno&#347;&#263;"
  ]
  node [
    id 28
    label "stan"
  ]
  node [
    id 29
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 30
    label "stand"
  ]
  node [
    id 31
    label "mie&#263;_miejsce"
  ]
  node [
    id 32
    label "uczestniczy&#263;"
  ]
  node [
    id 33
    label "chodzi&#263;"
  ]
  node [
    id 34
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 35
    label "equal"
  ]
  node [
    id 36
    label "uczestnictwo"
  ]
  node [
    id 37
    label "zwierzchnik"
  ]
  node [
    id 38
    label "czyn"
  ]
  node [
    id 39
    label "company"
  ]
  node [
    id 40
    label "zak&#322;adka"
  ]
  node [
    id 41
    label "firma"
  ]
  node [
    id 42
    label "instytut"
  ]
  node [
    id 43
    label "wyko&#324;czenie"
  ]
  node [
    id 44
    label "jednostka_organizacyjna"
  ]
  node [
    id 45
    label "umowa"
  ]
  node [
    id 46
    label "instytucja"
  ]
  node [
    id 47
    label "miejsce_pracy"
  ]
  node [
    id 48
    label "opieku&#324;cze_skrzyd&#322;a"
  ]
  node [
    id 49
    label "pomoc"
  ]
  node [
    id 50
    label "nadz&#243;r"
  ]
  node [
    id 51
    label "staranie"
  ]
  node [
    id 52
    label "zdrowy"
  ]
  node [
    id 53
    label "dobry"
  ]
  node [
    id 54
    label "prozdrowotny"
  ]
  node [
    id 55
    label "zdrowotnie"
  ]
  node [
    id 56
    label "Zgredek"
  ]
  node [
    id 57
    label "kategoria_gramatyczna"
  ]
  node [
    id 58
    label "Casanova"
  ]
  node [
    id 59
    label "Don_Juan"
  ]
  node [
    id 60
    label "Gargantua"
  ]
  node [
    id 61
    label "Faust"
  ]
  node [
    id 62
    label "profanum"
  ]
  node [
    id 63
    label "Chocho&#322;"
  ]
  node [
    id 64
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 65
    label "koniugacja"
  ]
  node [
    id 66
    label "Winnetou"
  ]
  node [
    id 67
    label "Dwukwiat"
  ]
  node [
    id 68
    label "homo_sapiens"
  ]
  node [
    id 69
    label "Edyp"
  ]
  node [
    id 70
    label "Herkules_Poirot"
  ]
  node [
    id 71
    label "ludzko&#347;&#263;"
  ]
  node [
    id 72
    label "mikrokosmos"
  ]
  node [
    id 73
    label "person"
  ]
  node [
    id 74
    label "Szwejk"
  ]
  node [
    id 75
    label "portrecista"
  ]
  node [
    id 76
    label "Sherlock_Holmes"
  ]
  node [
    id 77
    label "Hamlet"
  ]
  node [
    id 78
    label "duch"
  ]
  node [
    id 79
    label "oddzia&#322;ywanie"
  ]
  node [
    id 80
    label "g&#322;owa"
  ]
  node [
    id 81
    label "Quasimodo"
  ]
  node [
    id 82
    label "Dulcynea"
  ]
  node [
    id 83
    label "Wallenrod"
  ]
  node [
    id 84
    label "Don_Kiszot"
  ]
  node [
    id 85
    label "Plastu&#347;"
  ]
  node [
    id 86
    label "Harry_Potter"
  ]
  node [
    id 87
    label "figura"
  ]
  node [
    id 88
    label "parali&#380;owa&#263;"
  ]
  node [
    id 89
    label "istota"
  ]
  node [
    id 90
    label "Werter"
  ]
  node [
    id 91
    label "antropochoria"
  ]
  node [
    id 92
    label "posta&#263;"
  ]
  node [
    id 93
    label "authorize"
  ]
  node [
    id 94
    label "spowodowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
]
