graph [
  maxDegree 56
  minDegree 1
  meanDegree 1.9859154929577465
  density 0.014084507042253521
  graphCliqueNumber 2
  node [
    id 0
    label "zwierz&#281;"
    origin "text"
  ]
  node [
    id 1
    label "oddycha&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wydziela&#263;"
    origin "text"
  ]
  node [
    id 3
    label "dwutlenek"
    origin "text"
  ]
  node [
    id 4
    label "w&#281;giel"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "konsekwentny"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 8
    label "te&#380;"
    origin "text"
  ]
  node [
    id 9
    label "nale&#380;e&#263;by"
    origin "text"
  ]
  node [
    id 10
    label "zlikwidowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "grzbiet"
  ]
  node [
    id 12
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 13
    label "fukanie"
  ]
  node [
    id 14
    label "zachowanie"
  ]
  node [
    id 15
    label "popapraniec"
  ]
  node [
    id 16
    label "siedzie&#263;"
  ]
  node [
    id 17
    label "tresowa&#263;"
  ]
  node [
    id 18
    label "oswaja&#263;"
  ]
  node [
    id 19
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 20
    label "poskramia&#263;"
  ]
  node [
    id 21
    label "zwyrol"
  ]
  node [
    id 22
    label "animalista"
  ]
  node [
    id 23
    label "skubn&#261;&#263;"
  ]
  node [
    id 24
    label "fukni&#281;cie"
  ]
  node [
    id 25
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 26
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 27
    label "farba"
  ]
  node [
    id 28
    label "istota_&#380;ywa"
  ]
  node [
    id 29
    label "budowa"
  ]
  node [
    id 30
    label "monogamia"
  ]
  node [
    id 31
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 32
    label "sodomita"
  ]
  node [
    id 33
    label "budowa_cia&#322;a"
  ]
  node [
    id 34
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 35
    label "oz&#243;r"
  ]
  node [
    id 36
    label "gad"
  ]
  node [
    id 37
    label "&#322;eb"
  ]
  node [
    id 38
    label "treser"
  ]
  node [
    id 39
    label "fauna"
  ]
  node [
    id 40
    label "pasienie_si&#281;"
  ]
  node [
    id 41
    label "degenerat"
  ]
  node [
    id 42
    label "czerniak"
  ]
  node [
    id 43
    label "siedzenie"
  ]
  node [
    id 44
    label "le&#380;e&#263;"
  ]
  node [
    id 45
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 46
    label "weterynarz"
  ]
  node [
    id 47
    label "wiwarium"
  ]
  node [
    id 48
    label "wios&#322;owa&#263;"
  ]
  node [
    id 49
    label "skuba&#263;"
  ]
  node [
    id 50
    label "skubni&#281;cie"
  ]
  node [
    id 51
    label "poligamia"
  ]
  node [
    id 52
    label "hodowla"
  ]
  node [
    id 53
    label "przyssawka"
  ]
  node [
    id 54
    label "agresja"
  ]
  node [
    id 55
    label "niecz&#322;owiek"
  ]
  node [
    id 56
    label "skubanie"
  ]
  node [
    id 57
    label "wios&#322;owanie"
  ]
  node [
    id 58
    label "napasienie_si&#281;"
  ]
  node [
    id 59
    label "okrutnik"
  ]
  node [
    id 60
    label "wylinka"
  ]
  node [
    id 61
    label "paszcza"
  ]
  node [
    id 62
    label "bestia"
  ]
  node [
    id 63
    label "zwierz&#281;ta"
  ]
  node [
    id 64
    label "le&#380;enie"
  ]
  node [
    id 65
    label "hesitate"
  ]
  node [
    id 66
    label "breathe"
  ]
  node [
    id 67
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 68
    label "odpoczywa&#263;"
  ]
  node [
    id 69
    label "przepuszcza&#263;"
  ]
  node [
    id 70
    label "zrobi&#263;"
  ]
  node [
    id 71
    label "exhaust"
  ]
  node [
    id 72
    label "przydziela&#263;"
  ]
  node [
    id 73
    label "allocate"
  ]
  node [
    id 74
    label "stagger"
  ]
  node [
    id 75
    label "wyznacza&#263;"
  ]
  node [
    id 76
    label "rozdziela&#263;"
  ]
  node [
    id 77
    label "oddziela&#263;"
  ]
  node [
    id 78
    label "wytwarza&#263;"
  ]
  node [
    id 79
    label "wykrawa&#263;"
  ]
  node [
    id 80
    label "tlenek"
  ]
  node [
    id 81
    label "surowiec_energetyczny"
  ]
  node [
    id 82
    label "w&#281;glowiec"
  ]
  node [
    id 83
    label "w&#281;glowodan"
  ]
  node [
    id 84
    label "kopalina_podstawowa"
  ]
  node [
    id 85
    label "coal"
  ]
  node [
    id 86
    label "przybory_do_pisania"
  ]
  node [
    id 87
    label "makroelement"
  ]
  node [
    id 88
    label "niemetal"
  ]
  node [
    id 89
    label "zsypnik"
  ]
  node [
    id 90
    label "w&#281;glarka"
  ]
  node [
    id 91
    label "zw&#281;glenie_si&#281;"
  ]
  node [
    id 92
    label "zw&#281;glanie_si&#281;"
  ]
  node [
    id 93
    label "coil"
  ]
  node [
    id 94
    label "bry&#322;a"
  ]
  node [
    id 95
    label "ska&#322;a"
  ]
  node [
    id 96
    label "carbon"
  ]
  node [
    id 97
    label "fulleren"
  ]
  node [
    id 98
    label "rysunek"
  ]
  node [
    id 99
    label "si&#281;ga&#263;"
  ]
  node [
    id 100
    label "trwa&#263;"
  ]
  node [
    id 101
    label "obecno&#347;&#263;"
  ]
  node [
    id 102
    label "stan"
  ]
  node [
    id 103
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 104
    label "stand"
  ]
  node [
    id 105
    label "mie&#263;_miejsce"
  ]
  node [
    id 106
    label "uczestniczy&#263;"
  ]
  node [
    id 107
    label "chodzi&#263;"
  ]
  node [
    id 108
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 109
    label "equal"
  ]
  node [
    id 110
    label "konsekwentnie"
  ]
  node [
    id 111
    label "wytrwa&#322;y"
  ]
  node [
    id 112
    label "sta&#322;y"
  ]
  node [
    id 113
    label "asymilowa&#263;"
  ]
  node [
    id 114
    label "wapniak"
  ]
  node [
    id 115
    label "dwun&#243;g"
  ]
  node [
    id 116
    label "polifag"
  ]
  node [
    id 117
    label "wz&#243;r"
  ]
  node [
    id 118
    label "profanum"
  ]
  node [
    id 119
    label "hominid"
  ]
  node [
    id 120
    label "homo_sapiens"
  ]
  node [
    id 121
    label "nasada"
  ]
  node [
    id 122
    label "podw&#322;adny"
  ]
  node [
    id 123
    label "ludzko&#347;&#263;"
  ]
  node [
    id 124
    label "os&#322;abianie"
  ]
  node [
    id 125
    label "mikrokosmos"
  ]
  node [
    id 126
    label "portrecista"
  ]
  node [
    id 127
    label "duch"
  ]
  node [
    id 128
    label "g&#322;owa"
  ]
  node [
    id 129
    label "oddzia&#322;ywanie"
  ]
  node [
    id 130
    label "asymilowanie"
  ]
  node [
    id 131
    label "osoba"
  ]
  node [
    id 132
    label "os&#322;abia&#263;"
  ]
  node [
    id 133
    label "figura"
  ]
  node [
    id 134
    label "Adam"
  ]
  node [
    id 135
    label "senior"
  ]
  node [
    id 136
    label "antropochoria"
  ]
  node [
    id 137
    label "posta&#263;"
  ]
  node [
    id 138
    label "za&#322;atwi&#263;"
  ]
  node [
    id 139
    label "usun&#261;&#263;"
  ]
  node [
    id 140
    label "Henryk"
  ]
  node [
    id 141
    label "kowalczyk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 140
    target 141
  ]
]
