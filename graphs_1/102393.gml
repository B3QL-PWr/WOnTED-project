graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.6
  density 0.4
  graphCliqueNumber 2
  node [
    id 0
    label "rocznik"
    origin "text"
  ]
  node [
    id 1
    label "formacja"
  ]
  node [
    id 2
    label "kronika"
  ]
  node [
    id 3
    label "czasopismo"
  ]
  node [
    id 4
    label "yearbook"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
]
