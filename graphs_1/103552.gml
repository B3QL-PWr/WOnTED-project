graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 2
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "dziewczynka"
    origin "text"
  ]
  node [
    id 4
    label "dziewczyna"
  ]
  node [
    id 5
    label "potomkini"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "prostytutka"
  ]
  node [
    id 8
    label "dziecko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
]
