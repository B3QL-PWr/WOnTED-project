graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9375
  density 0.0625
  graphCliqueNumber 2
  node [
    id 0
    label "igor"
    origin "text"
  ]
  node [
    id 1
    label "stokfiszewski"
    origin "text"
  ]
  node [
    id 2
    label "wolny"
    origin "text"
  ]
  node [
    id 3
    label "lektura"
    origin "text"
  ]
  node [
    id 4
    label "niezale&#380;ny"
  ]
  node [
    id 5
    label "swobodnie"
  ]
  node [
    id 6
    label "niespieszny"
  ]
  node [
    id 7
    label "rozrzedzanie"
  ]
  node [
    id 8
    label "zwolnienie_si&#281;"
  ]
  node [
    id 9
    label "wolno"
  ]
  node [
    id 10
    label "rozrzedzenie"
  ]
  node [
    id 11
    label "lu&#378;no"
  ]
  node [
    id 12
    label "zwalnianie_si&#281;"
  ]
  node [
    id 13
    label "wolnie"
  ]
  node [
    id 14
    label "strza&#322;"
  ]
  node [
    id 15
    label "rozwodnienie"
  ]
  node [
    id 16
    label "wakowa&#263;"
  ]
  node [
    id 17
    label "rozwadnianie"
  ]
  node [
    id 18
    label "rzedni&#281;cie"
  ]
  node [
    id 19
    label "zrzedni&#281;cie"
  ]
  node [
    id 20
    label "wyczytywanie"
  ]
  node [
    id 21
    label "doczytywanie"
  ]
  node [
    id 22
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 23
    label "oczytywanie_si&#281;"
  ]
  node [
    id 24
    label "zaczytanie_si&#281;"
  ]
  node [
    id 25
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 26
    label "czytywanie"
  ]
  node [
    id 27
    label "recitation"
  ]
  node [
    id 28
    label "poznawanie"
  ]
  node [
    id 29
    label "poczytanie"
  ]
  node [
    id 30
    label "wczytywanie_si&#281;"
  ]
  node [
    id 31
    label "tekst"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
]
