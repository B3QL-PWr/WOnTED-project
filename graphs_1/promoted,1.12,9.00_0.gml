graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.2666666666666666
  density 0.0384180790960452
  graphCliqueNumber 6
  node [
    id 0
    label "zemrze&#263;"
    origin "text"
  ]
  node [
    id 1
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "prezydent"
    origin "text"
  ]
  node [
    id 3
    label "stany"
    origin "text"
  ]
  node [
    id 4
    label "zjednoczy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "george"
    origin "text"
  ]
  node [
    id 6
    label "bush"
    origin "text"
  ]
  node [
    id 7
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "usa"
    origin "text"
  ]
  node [
    id 9
    label "today"
    origin "text"
  ]
  node [
    id 10
    label "umrze&#263;"
  ]
  node [
    id 11
    label "dawny"
  ]
  node [
    id 12
    label "rozw&#243;d"
  ]
  node [
    id 13
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 14
    label "eksprezydent"
  ]
  node [
    id 15
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 16
    label "partner"
  ]
  node [
    id 17
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 18
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 19
    label "wcze&#347;niejszy"
  ]
  node [
    id 20
    label "Jelcyn"
  ]
  node [
    id 21
    label "Roosevelt"
  ]
  node [
    id 22
    label "Clinton"
  ]
  node [
    id 23
    label "dostojnik"
  ]
  node [
    id 24
    label "Tito"
  ]
  node [
    id 25
    label "de_Gaulle"
  ]
  node [
    id 26
    label "Nixon"
  ]
  node [
    id 27
    label "gruba_ryba"
  ]
  node [
    id 28
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 29
    label "Putin"
  ]
  node [
    id 30
    label "Gorbaczow"
  ]
  node [
    id 31
    label "Naser"
  ]
  node [
    id 32
    label "samorz&#261;dowiec"
  ]
  node [
    id 33
    label "Kemal"
  ]
  node [
    id 34
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 35
    label "zwierzchnik"
  ]
  node [
    id 36
    label "Bierut"
  ]
  node [
    id 37
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 38
    label "consort"
  ]
  node [
    id 39
    label "tenis"
  ]
  node [
    id 40
    label "cover"
  ]
  node [
    id 41
    label "siatk&#243;wka"
  ]
  node [
    id 42
    label "dawa&#263;"
  ]
  node [
    id 43
    label "faszerowa&#263;"
  ]
  node [
    id 44
    label "informowa&#263;"
  ]
  node [
    id 45
    label "introduce"
  ]
  node [
    id 46
    label "jedzenie"
  ]
  node [
    id 47
    label "tender"
  ]
  node [
    id 48
    label "deal"
  ]
  node [
    id 49
    label "kelner"
  ]
  node [
    id 50
    label "serwowa&#263;"
  ]
  node [
    id 51
    label "rozgrywa&#263;"
  ]
  node [
    id 52
    label "stawia&#263;"
  ]
  node [
    id 53
    label "Stany"
  ]
  node [
    id 54
    label "George"
  ]
  node [
    id 55
    label "H"
  ]
  node [
    id 56
    label "w"
  ]
  node [
    id 57
    label "Bush"
  ]
  node [
    id 58
    label "USA"
  ]
  node [
    id 59
    label "Today"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 58
    target 59
  ]
]
