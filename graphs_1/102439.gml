graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.1951219512195124
  density 0.02710027100271003
  graphCliqueNumber 3
  node [
    id 0
    label "bardzo"
    origin "text"
  ]
  node [
    id 1
    label "podoba"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "idea"
    origin "text"
  ]
  node [
    id 4
    label "blog"
    origin "text"
  ]
  node [
    id 5
    label "dwu&#347;cie&#380;kowego"
    origin "text"
  ]
  node [
    id 6
    label "taki"
    origin "text"
  ]
  node [
    id 7
    label "jak"
    origin "text"
  ]
  node [
    id 8
    label "jeden"
    origin "text"
  ]
  node [
    id 9
    label "&#347;cie&#380;ek"
    origin "text"
  ]
  node [
    id 10
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 11
    label "tekst"
    origin "text"
  ]
  node [
    id 12
    label "druga"
    origin "text"
  ]
  node [
    id 13
    label "kr&#243;tki"
    origin "text"
  ]
  node [
    id 14
    label "m&#243;cbyby&#263;"
    origin "text"
  ]
  node [
    id 15
    label "ten"
    origin "text"
  ]
  node [
    id 16
    label "wrzuca&#263;"
    origin "text"
  ]
  node [
    id 17
    label "notka"
    origin "text"
  ]
  node [
    id 18
    label "w_chuj"
  ]
  node [
    id 19
    label "byt"
  ]
  node [
    id 20
    label "istota"
  ]
  node [
    id 21
    label "ideologia"
  ]
  node [
    id 22
    label "intelekt"
  ]
  node [
    id 23
    label "Kant"
  ]
  node [
    id 24
    label "pomys&#322;"
  ]
  node [
    id 25
    label "poj&#281;cie"
  ]
  node [
    id 26
    label "cel"
  ]
  node [
    id 27
    label "p&#322;&#243;d"
  ]
  node [
    id 28
    label "ideacja"
  ]
  node [
    id 29
    label "komcio"
  ]
  node [
    id 30
    label "strona"
  ]
  node [
    id 31
    label "blogosfera"
  ]
  node [
    id 32
    label "pami&#281;tnik"
  ]
  node [
    id 33
    label "okre&#347;lony"
  ]
  node [
    id 34
    label "jaki&#347;"
  ]
  node [
    id 35
    label "byd&#322;o"
  ]
  node [
    id 36
    label "zobo"
  ]
  node [
    id 37
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 38
    label "yakalo"
  ]
  node [
    id 39
    label "dzo"
  ]
  node [
    id 40
    label "kieliszek"
  ]
  node [
    id 41
    label "shot"
  ]
  node [
    id 42
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 43
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 44
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 45
    label "jednolicie"
  ]
  node [
    id 46
    label "w&#243;dka"
  ]
  node [
    id 47
    label "ujednolicenie"
  ]
  node [
    id 48
    label "jednakowy"
  ]
  node [
    id 49
    label "daleki"
  ]
  node [
    id 50
    label "d&#322;ugo"
  ]
  node [
    id 51
    label "ruch"
  ]
  node [
    id 52
    label "pisa&#263;"
  ]
  node [
    id 53
    label "odmianka"
  ]
  node [
    id 54
    label "opu&#347;ci&#263;"
  ]
  node [
    id 55
    label "wypowied&#378;"
  ]
  node [
    id 56
    label "wytw&#243;r"
  ]
  node [
    id 57
    label "koniektura"
  ]
  node [
    id 58
    label "preparacja"
  ]
  node [
    id 59
    label "ekscerpcja"
  ]
  node [
    id 60
    label "redakcja"
  ]
  node [
    id 61
    label "obelga"
  ]
  node [
    id 62
    label "dzie&#322;o"
  ]
  node [
    id 63
    label "j&#281;zykowo"
  ]
  node [
    id 64
    label "pomini&#281;cie"
  ]
  node [
    id 65
    label "godzina"
  ]
  node [
    id 66
    label "jednowyrazowy"
  ]
  node [
    id 67
    label "s&#322;aby"
  ]
  node [
    id 68
    label "bliski"
  ]
  node [
    id 69
    label "drobny"
  ]
  node [
    id 70
    label "kr&#243;tko"
  ]
  node [
    id 71
    label "z&#322;y"
  ]
  node [
    id 72
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 73
    label "szybki"
  ]
  node [
    id 74
    label "brak"
  ]
  node [
    id 75
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 76
    label "give"
  ]
  node [
    id 77
    label "umieszcza&#263;"
  ]
  node [
    id 78
    label "note"
  ]
  node [
    id 79
    label "przypis"
  ]
  node [
    id 80
    label "informacja"
  ]
  node [
    id 81
    label "notatka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 51
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 33
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 16
    target 76
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 17
    target 78
  ]
  edge [
    source 17
    target 79
  ]
  edge [
    source 17
    target 80
  ]
  edge [
    source 17
    target 81
  ]
]
