graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.8604651162790697
  density 0.044296788482835
  graphCliqueNumber 2
  node [
    id 0
    label "podczas"
    origin "text"
  ]
  node [
    id 1
    label "transmisja"
    origin "text"
  ]
  node [
    id 2
    label "dwukrotnie"
    origin "text"
  ]
  node [
    id 3
    label "nazwa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "niemiecki"
    origin "text"
  ]
  node [
    id 5
    label "trener"
    origin "text"
  ]
  node [
    id 6
    label "reinharda"
    origin "text"
  ]
  node [
    id 7
    label "hessa"
    origin "text"
  ]
  node [
    id 8
    label "program"
  ]
  node [
    id 9
    label "przekaz"
  ]
  node [
    id 10
    label "zjawisko"
  ]
  node [
    id 11
    label "proces"
  ]
  node [
    id 12
    label "dwukrotny"
  ]
  node [
    id 13
    label "kilkukrotnie"
  ]
  node [
    id 14
    label "dwakro&#263;"
  ]
  node [
    id 15
    label "parokrotnie"
  ]
  node [
    id 16
    label "broadcast"
  ]
  node [
    id 17
    label "okre&#347;li&#263;"
  ]
  node [
    id 18
    label "nada&#263;"
  ]
  node [
    id 19
    label "szwabski"
  ]
  node [
    id 20
    label "po_niemiecku"
  ]
  node [
    id 21
    label "niemiec"
  ]
  node [
    id 22
    label "cenar"
  ]
  node [
    id 23
    label "j&#281;zyk"
  ]
  node [
    id 24
    label "europejski"
  ]
  node [
    id 25
    label "German"
  ]
  node [
    id 26
    label "pionier"
  ]
  node [
    id 27
    label "niemiecko"
  ]
  node [
    id 28
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 29
    label "zachodnioeuropejski"
  ]
  node [
    id 30
    label "strudel"
  ]
  node [
    id 31
    label "junkers"
  ]
  node [
    id 32
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 33
    label "cz&#322;owiek"
  ]
  node [
    id 34
    label "opiekun"
  ]
  node [
    id 35
    label "konsultant"
  ]
  node [
    id 36
    label "mentor"
  ]
  node [
    id 37
    label "Daniel_Dubicki"
  ]
  node [
    id 38
    label "nauczyciel"
  ]
  node [
    id 39
    label "Reinharda"
  ]
  node [
    id 40
    label "Hessa"
  ]
  node [
    id 41
    label "Rudolf"
  ]
  node [
    id 42
    label "Hesse"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 41
    target 42
  ]
]
