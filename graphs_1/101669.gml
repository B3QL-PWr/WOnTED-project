graph [
  maxDegree 44
  minDegree 1
  meanDegree 2.271356783919598
  density 0.00572130172271939
  graphCliqueNumber 6
  node [
    id 0
    label "przechodzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kolejny"
    origin "text"
  ]
  node [
    id 2
    label "pytanie"
    origin "text"
  ]
  node [
    id 3
    label "odpowiedzie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "ten"
    origin "text"
  ]
  node [
    id 7
    label "bezpo&#347;redni"
    origin "text"
  ]
  node [
    id 8
    label "wp&#322;yw"
    origin "text"
  ]
  node [
    id 9
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "dzia&#322;o"
    origin "text"
  ]
  node [
    id 12
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 13
    label "taki"
    origin "text"
  ]
  node [
    id 14
    label "sytuacja"
    origin "text"
  ]
  node [
    id 15
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 16
    label "kto"
    origin "text"
  ]
  node [
    id 17
    label "za&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 18
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 19
    label "skarga"
    origin "text"
  ]
  node [
    id 20
    label "kasacyjny"
    origin "text"
  ]
  node [
    id 21
    label "odpis"
    origin "text"
  ]
  node [
    id 22
    label "wezwa&#263;"
    origin "text"
  ]
  node [
    id 23
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 24
    label "tylko"
    origin "text"
  ]
  node [
    id 25
    label "uniemo&#380;liwia&#263;"
    origin "text"
  ]
  node [
    id 26
    label "nadanie"
    origin "text"
  ]
  node [
    id 27
    label "prawid&#322;owy"
    origin "text"
  ]
  node [
    id 28
    label "bieg"
    origin "text"
  ]
  node [
    id 29
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 30
    label "tak"
    origin "text"
  ]
  node [
    id 31
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 32
    label "zapomnie&#263;"
    origin "text"
  ]
  node [
    id 33
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 34
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 35
    label "sam"
    origin "text"
  ]
  node [
    id 36
    label "wniosek"
    origin "text"
  ]
  node [
    id 37
    label "rozpatrzenie"
    origin "text"
  ]
  node [
    id 38
    label "prosty"
    origin "text"
  ]
  node [
    id 39
    label "powinien"
    origin "text"
  ]
  node [
    id 40
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 41
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 42
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 43
    label "s&#322;ysze&#263;by&#263;"
    origin "text"
  ]
  node [
    id 44
    label "dzisiejszy"
    origin "text"
  ]
  node [
    id 45
    label "dyskusja"
    origin "text"
  ]
  node [
    id 46
    label "wszystek"
    origin "text"
  ]
  node [
    id 47
    label "klub"
    origin "text"
  ]
  node [
    id 48
    label "popiera&#263;"
    origin "text"
  ]
  node [
    id 49
    label "nowelizacja"
    origin "text"
  ]
  node [
    id 50
    label "te&#380;"
    origin "text"
  ]
  node [
    id 51
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 52
    label "wszyscy"
    origin "text"
  ]
  node [
    id 53
    label "tym"
    origin "text"
  ]
  node [
    id 54
    label "moment"
    origin "text"
  ]
  node [
    id 55
    label "podzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 56
    label "wystarczy&#263;"
    origin "text"
  ]
  node [
    id 57
    label "komentarz"
    origin "text"
  ]
  node [
    id 58
    label "podlega&#263;"
  ]
  node [
    id 59
    label "zmienia&#263;"
  ]
  node [
    id 60
    label "proceed"
  ]
  node [
    id 61
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 62
    label "saturate"
  ]
  node [
    id 63
    label "pass"
  ]
  node [
    id 64
    label "doznawa&#263;"
  ]
  node [
    id 65
    label "test"
  ]
  node [
    id 66
    label "zalicza&#263;"
  ]
  node [
    id 67
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 68
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 69
    label "conflict"
  ]
  node [
    id 70
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 71
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 72
    label "go"
  ]
  node [
    id 73
    label "continue"
  ]
  node [
    id 74
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 75
    label "mie&#263;_miejsce"
  ]
  node [
    id 76
    label "przestawa&#263;"
  ]
  node [
    id 77
    label "przerabia&#263;"
  ]
  node [
    id 78
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 79
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 80
    label "move"
  ]
  node [
    id 81
    label "przebywa&#263;"
  ]
  node [
    id 82
    label "mija&#263;"
  ]
  node [
    id 83
    label "zaczyna&#263;"
  ]
  node [
    id 84
    label "i&#347;&#263;"
  ]
  node [
    id 85
    label "inny"
  ]
  node [
    id 86
    label "nast&#281;pnie"
  ]
  node [
    id 87
    label "kt&#243;ry&#347;"
  ]
  node [
    id 88
    label "kolejno"
  ]
  node [
    id 89
    label "nastopny"
  ]
  node [
    id 90
    label "sprawa"
  ]
  node [
    id 91
    label "zadanie"
  ]
  node [
    id 92
    label "wypowied&#378;"
  ]
  node [
    id 93
    label "problemat"
  ]
  node [
    id 94
    label "rozpytywanie"
  ]
  node [
    id 95
    label "sprawdzian"
  ]
  node [
    id 96
    label "przes&#322;uchiwanie"
  ]
  node [
    id 97
    label "wypytanie"
  ]
  node [
    id 98
    label "zwracanie_si&#281;"
  ]
  node [
    id 99
    label "wypowiedzenie"
  ]
  node [
    id 100
    label "wywo&#322;ywanie"
  ]
  node [
    id 101
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 102
    label "problematyka"
  ]
  node [
    id 103
    label "question"
  ]
  node [
    id 104
    label "sprawdzanie"
  ]
  node [
    id 105
    label "odpowiadanie"
  ]
  node [
    id 106
    label "survey"
  ]
  node [
    id 107
    label "odpowiada&#263;"
  ]
  node [
    id 108
    label "egzaminowanie"
  ]
  node [
    id 109
    label "copy"
  ]
  node [
    id 110
    label "da&#263;"
  ]
  node [
    id 111
    label "ponie&#347;&#263;"
  ]
  node [
    id 112
    label "spowodowa&#263;"
  ]
  node [
    id 113
    label "zareagowa&#263;"
  ]
  node [
    id 114
    label "react"
  ]
  node [
    id 115
    label "agreement"
  ]
  node [
    id 116
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 117
    label "tax_return"
  ]
  node [
    id 118
    label "bekn&#261;&#263;"
  ]
  node [
    id 119
    label "picture"
  ]
  node [
    id 120
    label "dok&#322;adnie"
  ]
  node [
    id 121
    label "si&#281;ga&#263;"
  ]
  node [
    id 122
    label "trwa&#263;"
  ]
  node [
    id 123
    label "obecno&#347;&#263;"
  ]
  node [
    id 124
    label "stan"
  ]
  node [
    id 125
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 126
    label "stand"
  ]
  node [
    id 127
    label "uczestniczy&#263;"
  ]
  node [
    id 128
    label "chodzi&#263;"
  ]
  node [
    id 129
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 130
    label "equal"
  ]
  node [
    id 131
    label "okre&#347;lony"
  ]
  node [
    id 132
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 133
    label "szczery"
  ]
  node [
    id 134
    label "bliski"
  ]
  node [
    id 135
    label "bezpo&#347;rednio"
  ]
  node [
    id 136
    label "&#347;lad"
  ]
  node [
    id 137
    label "doch&#243;d_narodowy"
  ]
  node [
    id 138
    label "zjawisko"
  ]
  node [
    id 139
    label "rezultat"
  ]
  node [
    id 140
    label "kwota"
  ]
  node [
    id 141
    label "lobbysta"
  ]
  node [
    id 142
    label "energy"
  ]
  node [
    id 143
    label "czas"
  ]
  node [
    id 144
    label "bycie"
  ]
  node [
    id 145
    label "zegar_biologiczny"
  ]
  node [
    id 146
    label "okres_noworodkowy"
  ]
  node [
    id 147
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 148
    label "entity"
  ]
  node [
    id 149
    label "prze&#380;ywanie"
  ]
  node [
    id 150
    label "prze&#380;ycie"
  ]
  node [
    id 151
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 152
    label "wiek_matuzalemowy"
  ]
  node [
    id 153
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 154
    label "dzieci&#324;stwo"
  ]
  node [
    id 155
    label "power"
  ]
  node [
    id 156
    label "szwung"
  ]
  node [
    id 157
    label "menopauza"
  ]
  node [
    id 158
    label "umarcie"
  ]
  node [
    id 159
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 160
    label "life"
  ]
  node [
    id 161
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 162
    label "&#380;ywy"
  ]
  node [
    id 163
    label "rozw&#243;j"
  ]
  node [
    id 164
    label "po&#322;&#243;g"
  ]
  node [
    id 165
    label "byt"
  ]
  node [
    id 166
    label "przebywanie"
  ]
  node [
    id 167
    label "subsistence"
  ]
  node [
    id 168
    label "koleje_losu"
  ]
  node [
    id 169
    label "raj_utracony"
  ]
  node [
    id 170
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 171
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 172
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 173
    label "andropauza"
  ]
  node [
    id 174
    label "warunki"
  ]
  node [
    id 175
    label "do&#380;ywanie"
  ]
  node [
    id 176
    label "niemowl&#281;ctwo"
  ]
  node [
    id 177
    label "umieranie"
  ]
  node [
    id 178
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 179
    label "staro&#347;&#263;"
  ]
  node [
    id 180
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 181
    label "&#347;mier&#263;"
  ]
  node [
    id 182
    label "bateria"
  ]
  node [
    id 183
    label "laweta"
  ]
  node [
    id 184
    label "bro&#324;_artyleryjska"
  ]
  node [
    id 185
    label "bro&#324;"
  ]
  node [
    id 186
    label "oporopowrotnik"
  ]
  node [
    id 187
    label "przedmuchiwacz"
  ]
  node [
    id 188
    label "artyleria"
  ]
  node [
    id 189
    label "waln&#261;&#263;"
  ]
  node [
    id 190
    label "bateria_artylerii"
  ]
  node [
    id 191
    label "cannon"
  ]
  node [
    id 192
    label "jaki&#347;"
  ]
  node [
    id 193
    label "szczeg&#243;&#322;"
  ]
  node [
    id 194
    label "motyw"
  ]
  node [
    id 195
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 196
    label "state"
  ]
  node [
    id 197
    label "realia"
  ]
  node [
    id 198
    label "cz&#322;owiek"
  ]
  node [
    id 199
    label "znaczenie"
  ]
  node [
    id 200
    label "go&#347;&#263;"
  ]
  node [
    id 201
    label "osoba"
  ]
  node [
    id 202
    label "posta&#263;"
  ]
  node [
    id 203
    label "annex"
  ]
  node [
    id 204
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 205
    label "czyn"
  ]
  node [
    id 206
    label "przedstawiciel"
  ]
  node [
    id 207
    label "ilustracja"
  ]
  node [
    id 208
    label "fakt"
  ]
  node [
    id 209
    label "request"
  ]
  node [
    id 210
    label "skwierk"
  ]
  node [
    id 211
    label "odwo&#322;anie"
  ]
  node [
    id 212
    label "akt_oskar&#380;enia"
  ]
  node [
    id 213
    label "zawiadomienie"
  ]
  node [
    id 214
    label "apelacyjny"
  ]
  node [
    id 215
    label "duplikat"
  ]
  node [
    id 216
    label "poprosi&#263;"
  ]
  node [
    id 217
    label "ask"
  ]
  node [
    id 218
    label "invite"
  ]
  node [
    id 219
    label "zach&#281;ci&#263;"
  ]
  node [
    id 220
    label "adduce"
  ]
  node [
    id 221
    label "przewo&#322;a&#263;"
  ]
  node [
    id 222
    label "poinformowa&#263;"
  ]
  node [
    id 223
    label "nakaza&#263;"
  ]
  node [
    id 224
    label "przeszkadza&#263;"
  ]
  node [
    id 225
    label "anticipate"
  ]
  node [
    id 226
    label "czynno&#347;&#263;"
  ]
  node [
    id 227
    label "spowodowanie"
  ]
  node [
    id 228
    label "nazwanie"
  ]
  node [
    id 229
    label "akt"
  ]
  node [
    id 230
    label "broadcast"
  ]
  node [
    id 231
    label "denomination"
  ]
  node [
    id 232
    label "przyznanie"
  ]
  node [
    id 233
    label "zdarzenie_si&#281;"
  ]
  node [
    id 234
    label "przes&#322;anie"
  ]
  node [
    id 235
    label "symetryczny"
  ]
  node [
    id 236
    label "po&#380;&#261;dany"
  ]
  node [
    id 237
    label "dobry"
  ]
  node [
    id 238
    label "s&#322;uszny"
  ]
  node [
    id 239
    label "prawid&#322;owo"
  ]
  node [
    id 240
    label "wy&#347;cig"
  ]
  node [
    id 241
    label "parametr"
  ]
  node [
    id 242
    label "ciek_wodny"
  ]
  node [
    id 243
    label "roll"
  ]
  node [
    id 244
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 245
    label "przedbieg"
  ]
  node [
    id 246
    label "konkurencja"
  ]
  node [
    id 247
    label "tryb"
  ]
  node [
    id 248
    label "bystrzyca"
  ]
  node [
    id 249
    label "pr&#261;d"
  ]
  node [
    id 250
    label "pozycja"
  ]
  node [
    id 251
    label "linia"
  ]
  node [
    id 252
    label "procedura"
  ]
  node [
    id 253
    label "ruch"
  ]
  node [
    id 254
    label "czo&#322;&#243;wka"
  ]
  node [
    id 255
    label "kurs"
  ]
  node [
    id 256
    label "syfon"
  ]
  node [
    id 257
    label "kierunek"
  ]
  node [
    id 258
    label "cycle"
  ]
  node [
    id 259
    label "proces"
  ]
  node [
    id 260
    label "d&#261;&#380;enie"
  ]
  node [
    id 261
    label "porzuci&#263;"
  ]
  node [
    id 262
    label "opu&#347;ci&#263;"
  ]
  node [
    id 263
    label "pozostawi&#263;"
  ]
  node [
    id 264
    label "screw"
  ]
  node [
    id 265
    label "przesta&#263;"
  ]
  node [
    id 266
    label "fuck"
  ]
  node [
    id 267
    label "straci&#263;"
  ]
  node [
    id 268
    label "zdolno&#347;&#263;"
  ]
  node [
    id 269
    label "wybaczy&#263;"
  ]
  node [
    id 270
    label "zrobi&#263;"
  ]
  node [
    id 271
    label "zabaczy&#263;"
  ]
  node [
    id 272
    label "postawi&#263;"
  ]
  node [
    id 273
    label "prasa"
  ]
  node [
    id 274
    label "stworzy&#263;"
  ]
  node [
    id 275
    label "donie&#347;&#263;"
  ]
  node [
    id 276
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 277
    label "write"
  ]
  node [
    id 278
    label "styl"
  ]
  node [
    id 279
    label "read"
  ]
  node [
    id 280
    label "zawarto&#347;&#263;"
  ]
  node [
    id 281
    label "temat"
  ]
  node [
    id 282
    label "istota"
  ]
  node [
    id 283
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 284
    label "informacja"
  ]
  node [
    id 285
    label "sklep"
  ]
  node [
    id 286
    label "twierdzenie"
  ]
  node [
    id 287
    label "my&#347;l"
  ]
  node [
    id 288
    label "wnioskowanie"
  ]
  node [
    id 289
    label "propozycja"
  ]
  node [
    id 290
    label "motion"
  ]
  node [
    id 291
    label "pismo"
  ]
  node [
    id 292
    label "prayer"
  ]
  node [
    id 293
    label "probe"
  ]
  node [
    id 294
    label "przemy&#347;lenie"
  ]
  node [
    id 295
    label "&#322;atwy"
  ]
  node [
    id 296
    label "prostowanie_si&#281;"
  ]
  node [
    id 297
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 298
    label "rozprostowanie"
  ]
  node [
    id 299
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 300
    label "prostoduszny"
  ]
  node [
    id 301
    label "naturalny"
  ]
  node [
    id 302
    label "naiwny"
  ]
  node [
    id 303
    label "cios"
  ]
  node [
    id 304
    label "prostowanie"
  ]
  node [
    id 305
    label "niepozorny"
  ]
  node [
    id 306
    label "zwyk&#322;y"
  ]
  node [
    id 307
    label "prosto"
  ]
  node [
    id 308
    label "po_prostu"
  ]
  node [
    id 309
    label "skromny"
  ]
  node [
    id 310
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 311
    label "musie&#263;"
  ]
  node [
    id 312
    label "due"
  ]
  node [
    id 313
    label "take_care"
  ]
  node [
    id 314
    label "troska&#263;_si&#281;"
  ]
  node [
    id 315
    label "zamierza&#263;"
  ]
  node [
    id 316
    label "os&#261;dza&#263;"
  ]
  node [
    id 317
    label "robi&#263;"
  ]
  node [
    id 318
    label "argue"
  ]
  node [
    id 319
    label "rozpatrywa&#263;"
  ]
  node [
    id 320
    label "deliver"
  ]
  node [
    id 321
    label "opinion"
  ]
  node [
    id 322
    label "zmatowienie"
  ]
  node [
    id 323
    label "wpa&#347;&#263;"
  ]
  node [
    id 324
    label "grupa"
  ]
  node [
    id 325
    label "wokal"
  ]
  node [
    id 326
    label "note"
  ]
  node [
    id 327
    label "wydawa&#263;"
  ]
  node [
    id 328
    label "nakaz"
  ]
  node [
    id 329
    label "regestr"
  ]
  node [
    id 330
    label "&#347;piewak_operowy"
  ]
  node [
    id 331
    label "matowie&#263;"
  ]
  node [
    id 332
    label "wpada&#263;"
  ]
  node [
    id 333
    label "stanowisko"
  ]
  node [
    id 334
    label "mutacja"
  ]
  node [
    id 335
    label "partia"
  ]
  node [
    id 336
    label "&#347;piewak"
  ]
  node [
    id 337
    label "emisja"
  ]
  node [
    id 338
    label "brzmienie"
  ]
  node [
    id 339
    label "zmatowie&#263;"
  ]
  node [
    id 340
    label "wydanie"
  ]
  node [
    id 341
    label "zesp&#243;&#322;"
  ]
  node [
    id 342
    label "wyda&#263;"
  ]
  node [
    id 343
    label "decyzja"
  ]
  node [
    id 344
    label "wpadni&#281;cie"
  ]
  node [
    id 345
    label "linia_melodyczna"
  ]
  node [
    id 346
    label "wpadanie"
  ]
  node [
    id 347
    label "onomatopeja"
  ]
  node [
    id 348
    label "sound"
  ]
  node [
    id 349
    label "matowienie"
  ]
  node [
    id 350
    label "ch&#243;rzysta"
  ]
  node [
    id 351
    label "d&#378;wi&#281;k"
  ]
  node [
    id 352
    label "foniatra"
  ]
  node [
    id 353
    label "&#347;piewaczka"
  ]
  node [
    id 354
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 355
    label "dzisiejszo"
  ]
  node [
    id 356
    label "rozmowa"
  ]
  node [
    id 357
    label "sympozjon"
  ]
  node [
    id 358
    label "conference"
  ]
  node [
    id 359
    label "ca&#322;y"
  ]
  node [
    id 360
    label "society"
  ]
  node [
    id 361
    label "jakobini"
  ]
  node [
    id 362
    label "klubista"
  ]
  node [
    id 363
    label "stowarzyszenie"
  ]
  node [
    id 364
    label "lokal"
  ]
  node [
    id 365
    label "od&#322;am"
  ]
  node [
    id 366
    label "siedziba"
  ]
  node [
    id 367
    label "bar"
  ]
  node [
    id 368
    label "uzasadnia&#263;"
  ]
  node [
    id 369
    label "pomaga&#263;"
  ]
  node [
    id 370
    label "unbosom"
  ]
  node [
    id 371
    label "modyfikacja"
  ]
  node [
    id 372
    label "story"
  ]
  node [
    id 373
    label "ustawa"
  ]
  node [
    id 374
    label "amendment"
  ]
  node [
    id 375
    label "okres_czasu"
  ]
  node [
    id 376
    label "minute"
  ]
  node [
    id 377
    label "jednostka_geologiczna"
  ]
  node [
    id 378
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 379
    label "time"
  ]
  node [
    id 380
    label "chron"
  ]
  node [
    id 381
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 382
    label "fragment"
  ]
  node [
    id 383
    label "sprzeciwi&#263;_si&#281;"
  ]
  node [
    id 384
    label "thank"
  ]
  node [
    id 385
    label "decline"
  ]
  node [
    id 386
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 387
    label "stan&#261;&#263;"
  ]
  node [
    id 388
    label "dosta&#263;"
  ]
  node [
    id 389
    label "suffice"
  ]
  node [
    id 390
    label "zaspokoi&#263;"
  ]
  node [
    id 391
    label "comment"
  ]
  node [
    id 392
    label "artyku&#322;"
  ]
  node [
    id 393
    label "ocena"
  ]
  node [
    id 394
    label "gossip"
  ]
  node [
    id 395
    label "interpretacja"
  ]
  node [
    id 396
    label "audycja"
  ]
  node [
    id 397
    label "tekst"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 32
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 33
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 17
    target 57
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 207
  ]
  edge [
    source 18
    target 208
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 36
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 92
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 33
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 20
    target 57
  ]
  edge [
    source 21
    target 215
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 38
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 217
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 219
  ]
  edge [
    source 22
    target 220
  ]
  edge [
    source 22
    target 221
  ]
  edge [
    source 22
    target 222
  ]
  edge [
    source 22
    target 223
  ]
  edge [
    source 22
    target 39
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 45
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 45
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 226
  ]
  edge [
    source 26
    target 227
  ]
  edge [
    source 26
    target 228
  ]
  edge [
    source 26
    target 229
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 235
  ]
  edge [
    source 27
    target 236
  ]
  edge [
    source 27
    target 237
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 239
  ]
  edge [
    source 28
    target 240
  ]
  edge [
    source 28
    target 241
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 52
  ]
  edge [
    source 29
    target 56
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 261
  ]
  edge [
    source 32
    target 262
  ]
  edge [
    source 32
    target 263
  ]
  edge [
    source 32
    target 264
  ]
  edge [
    source 32
    target 265
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 32
    target 271
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 272
  ]
  edge [
    source 33
    target 273
  ]
  edge [
    source 33
    target 274
  ]
  edge [
    source 33
    target 275
  ]
  edge [
    source 33
    target 276
  ]
  edge [
    source 33
    target 277
  ]
  edge [
    source 33
    target 278
  ]
  edge [
    source 33
    target 279
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 33
    target 57
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 280
  ]
  edge [
    source 34
    target 281
  ]
  edge [
    source 34
    target 282
  ]
  edge [
    source 34
    target 283
  ]
  edge [
    source 34
    target 284
  ]
  edge [
    source 35
    target 285
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 286
  ]
  edge [
    source 36
    target 287
  ]
  edge [
    source 36
    target 288
  ]
  edge [
    source 36
    target 289
  ]
  edge [
    source 36
    target 290
  ]
  edge [
    source 36
    target 291
  ]
  edge [
    source 36
    target 292
  ]
  edge [
    source 37
    target 293
  ]
  edge [
    source 37
    target 294
  ]
  edge [
    source 38
    target 295
  ]
  edge [
    source 38
    target 296
  ]
  edge [
    source 38
    target 297
  ]
  edge [
    source 38
    target 298
  ]
  edge [
    source 38
    target 299
  ]
  edge [
    source 38
    target 300
  ]
  edge [
    source 38
    target 301
  ]
  edge [
    source 38
    target 302
  ]
  edge [
    source 38
    target 303
  ]
  edge [
    source 38
    target 304
  ]
  edge [
    source 38
    target 305
  ]
  edge [
    source 38
    target 306
  ]
  edge [
    source 38
    target 307
  ]
  edge [
    source 38
    target 308
  ]
  edge [
    source 38
    target 309
  ]
  edge [
    source 38
    target 310
  ]
  edge [
    source 39
    target 55
  ]
  edge [
    source 39
    target 56
  ]
  edge [
    source 39
    target 311
  ]
  edge [
    source 39
    target 312
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 313
  ]
  edge [
    source 40
    target 314
  ]
  edge [
    source 40
    target 315
  ]
  edge [
    source 40
    target 316
  ]
  edge [
    source 40
    target 317
  ]
  edge [
    source 40
    target 318
  ]
  edge [
    source 40
    target 319
  ]
  edge [
    source 40
    target 320
  ]
  edge [
    source 40
    target 57
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 321
  ]
  edge [
    source 41
    target 92
  ]
  edge [
    source 41
    target 322
  ]
  edge [
    source 41
    target 323
  ]
  edge [
    source 41
    target 324
  ]
  edge [
    source 41
    target 325
  ]
  edge [
    source 41
    target 326
  ]
  edge [
    source 41
    target 327
  ]
  edge [
    source 41
    target 328
  ]
  edge [
    source 41
    target 329
  ]
  edge [
    source 41
    target 330
  ]
  edge [
    source 41
    target 331
  ]
  edge [
    source 41
    target 332
  ]
  edge [
    source 41
    target 333
  ]
  edge [
    source 41
    target 138
  ]
  edge [
    source 41
    target 334
  ]
  edge [
    source 41
    target 335
  ]
  edge [
    source 41
    target 336
  ]
  edge [
    source 41
    target 337
  ]
  edge [
    source 41
    target 338
  ]
  edge [
    source 41
    target 339
  ]
  edge [
    source 41
    target 340
  ]
  edge [
    source 41
    target 341
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 41
    target 268
  ]
  edge [
    source 41
    target 343
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 345
  ]
  edge [
    source 41
    target 346
  ]
  edge [
    source 41
    target 347
  ]
  edge [
    source 41
    target 348
  ]
  edge [
    source 41
    target 349
  ]
  edge [
    source 41
    target 350
  ]
  edge [
    source 41
    target 351
  ]
  edge [
    source 41
    target 352
  ]
  edge [
    source 41
    target 353
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 354
  ]
  edge [
    source 44
    target 355
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 356
  ]
  edge [
    source 45
    target 357
  ]
  edge [
    source 45
    target 358
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 359
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 360
  ]
  edge [
    source 47
    target 361
  ]
  edge [
    source 47
    target 362
  ]
  edge [
    source 47
    target 363
  ]
  edge [
    source 47
    target 364
  ]
  edge [
    source 47
    target 365
  ]
  edge [
    source 47
    target 366
  ]
  edge [
    source 47
    target 367
  ]
  edge [
    source 48
    target 368
  ]
  edge [
    source 48
    target 369
  ]
  edge [
    source 48
    target 370
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 371
  ]
  edge [
    source 49
    target 372
  ]
  edge [
    source 49
    target 373
  ]
  edge [
    source 49
    target 374
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 375
  ]
  edge [
    source 54
    target 376
  ]
  edge [
    source 54
    target 377
  ]
  edge [
    source 54
    target 378
  ]
  edge [
    source 54
    target 379
  ]
  edge [
    source 54
    target 380
  ]
  edge [
    source 54
    target 381
  ]
  edge [
    source 54
    target 382
  ]
  edge [
    source 55
    target 383
  ]
  edge [
    source 55
    target 384
  ]
  edge [
    source 55
    target 385
  ]
  edge [
    source 55
    target 386
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 112
  ]
  edge [
    source 56
    target 387
  ]
  edge [
    source 56
    target 388
  ]
  edge [
    source 56
    target 389
  ]
  edge [
    source 56
    target 390
  ]
  edge [
    source 57
    target 391
  ]
  edge [
    source 57
    target 392
  ]
  edge [
    source 57
    target 393
  ]
  edge [
    source 57
    target 394
  ]
  edge [
    source 57
    target 395
  ]
  edge [
    source 57
    target 396
  ]
  edge [
    source 57
    target 397
  ]
]
