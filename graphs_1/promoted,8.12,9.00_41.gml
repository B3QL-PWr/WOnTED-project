graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9393939393939394
  density 0.06060606060606061
  graphCliqueNumber 2
  node [
    id 0
    label "gdyby"
    origin "text"
  ]
  node [
    id 1
    label "wsz&#281;dobylski"
    origin "text"
  ]
  node [
    id 2
    label "monitoring"
    origin "text"
  ]
  node [
    id 3
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 4
    label "zapewne"
    origin "text"
  ]
  node [
    id 5
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 6
    label "garowa&#263;by"
    origin "text"
  ]
  node [
    id 7
    label "pierdel"
    origin "text"
  ]
  node [
    id 8
    label "wsz&#281;dobylsko"
  ]
  node [
    id 9
    label "niezno&#347;ny"
  ]
  node [
    id 10
    label "niegrzeczny"
  ]
  node [
    id 11
    label "ciekawy"
  ]
  node [
    id 12
    label "rozleg&#322;y"
  ]
  node [
    id 13
    label "ochrona"
  ]
  node [
    id 14
    label "obserwacja"
  ]
  node [
    id 15
    label "kamera"
  ]
  node [
    id 16
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 17
    label "ch&#322;opina"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "jegomo&#347;&#263;"
  ]
  node [
    id 20
    label "bratek"
  ]
  node [
    id 21
    label "doros&#322;y"
  ]
  node [
    id 22
    label "samiec"
  ]
  node [
    id 23
    label "ojciec"
  ]
  node [
    id 24
    label "twardziel"
  ]
  node [
    id 25
    label "androlog"
  ]
  node [
    id 26
    label "pa&#324;stwo"
  ]
  node [
    id 27
    label "m&#261;&#380;"
  ]
  node [
    id 28
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 29
    label "andropauza"
  ]
  node [
    id 30
    label "wi&#281;zienie"
  ]
  node [
    id 31
    label "odsiadka"
  ]
  node [
    id 32
    label "alfabet_wi&#281;zienny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
]
