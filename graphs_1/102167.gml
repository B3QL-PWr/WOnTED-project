graph [
  maxDegree 11
  minDegree 1
  meanDegree 2.413793103448276
  density 0.04234724742891712
  graphCliqueNumber 6
  node [
    id 0
    label "uzasadnienie"
    origin "text"
  ]
  node [
    id 1
    label "projekt"
    origin "text"
  ]
  node [
    id 2
    label "uchwa&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "justyfikacja"
  ]
  node [
    id 4
    label "apologetyk"
  ]
  node [
    id 5
    label "informacja"
  ]
  node [
    id 6
    label "gossip"
  ]
  node [
    id 7
    label "wyja&#347;nienie"
  ]
  node [
    id 8
    label "dokument"
  ]
  node [
    id 9
    label "device"
  ]
  node [
    id 10
    label "program_u&#380;ytkowy"
  ]
  node [
    id 11
    label "intencja"
  ]
  node [
    id 12
    label "agreement"
  ]
  node [
    id 13
    label "pomys&#322;"
  ]
  node [
    id 14
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 15
    label "plan"
  ]
  node [
    id 16
    label "dokumentacja"
  ]
  node [
    id 17
    label "resolution"
  ]
  node [
    id 18
    label "akt"
  ]
  node [
    id 19
    label "ustawa"
  ]
  node [
    id 20
    label "ojciec"
  ]
  node [
    id 21
    label "samorz&#261;d"
  ]
  node [
    id 22
    label "gminny"
  ]
  node [
    id 23
    label "dom"
  ]
  node [
    id 24
    label "harcerz"
  ]
  node [
    id 25
    label "Stanis&#322;awa"
  ]
  node [
    id 26
    label "Wyspia&#324;ski"
  ]
  node [
    id 27
    label "plac"
  ]
  node [
    id 28
    label "Korczakowc&#243;w"
  ]
  node [
    id 29
    label "Rogulski"
  ]
  node [
    id 30
    label "Leszek"
  ]
  node [
    id 31
    label "Kornosza"
  ]
  node [
    id 32
    label "Jerzy"
  ]
  node [
    id 33
    label "Zgodzi&#324;skiego"
  ]
  node [
    id 34
    label "zesp&#243;&#322;"
  ]
  node [
    id 35
    label "szko&#322;a"
  ]
  node [
    id 36
    label "elektroniczny"
  ]
  node [
    id 37
    label "i"
  ]
  node [
    id 38
    label "samochodowy"
  ]
  node [
    id 39
    label "zwi&#261;zek"
  ]
  node [
    id 40
    label "harcerstwo"
  ]
  node [
    id 41
    label "polskie"
  ]
  node [
    id 42
    label "komitet"
  ]
  node [
    id 43
    label "Korczakowski"
  ]
  node [
    id 44
    label "chor&#261;giew"
  ]
  node [
    id 45
    label "ziemia"
  ]
  node [
    id 46
    label "lubuski"
  ]
  node [
    id 47
    label "zasadniczy"
  ]
  node [
    id 48
    label "zawodowy"
  ]
  node [
    id 49
    label "zielony"
  ]
  node [
    id 50
    label "g&#243;ra"
  ]
  node [
    id 51
    label "Janusz"
  ]
  node [
    id 52
    label "korczak"
  ]
  node [
    id 53
    label "20"
  ]
  node [
    id 54
    label "dru&#380;yna"
  ]
  node [
    id 55
    label "harcerski"
  ]
  node [
    id 56
    label "szczep"
  ]
  node [
    id 57
    label "on"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 47
  ]
  edge [
    source 35
    target 48
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 56
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 52
    target 57
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 56
    target 57
  ]
]
