graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.017467248908297
  density 0.008848540565387267
  graphCliqueNumber 2
  node [
    id 0
    label "policjant"
    origin "text"
  ]
  node [
    id 1
    label "zgierz"
    origin "text"
  ]
  node [
    id 2
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 3
    label "rozbi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "gang"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "&#380;&#261;da&#263;"
    origin "text"
  ]
  node [
    id 7
    label "okup"
    origin "text"
  ]
  node [
    id 8
    label "grozi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 10
    label "letni"
    origin "text"
  ]
  node [
    id 11
    label "przedsi&#281;biorca"
    origin "text"
  ]
  node [
    id 12
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 13
    label "przest&#281;pca"
    origin "text"
  ]
  node [
    id 14
    label "spali&#263;"
    origin "text"
  ]
  node [
    id 15
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 16
    label "cztery"
    origin "text"
  ]
  node [
    id 17
    label "auto"
    origin "text"
  ]
  node [
    id 18
    label "zatrzyma&#263;"
    origin "text"
  ]
  node [
    id 19
    label "sze&#347;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "osoba"
    origin "text"
  ]
  node [
    id 21
    label "tym"
    origin "text"
  ]
  node [
    id 22
    label "dwa"
    origin "text"
  ]
  node [
    id 23
    label "ma&#322;&#380;e&#324;stwo"
    origin "text"
  ]
  node [
    id 24
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 25
    label "kara"
    origin "text"
  ]
  node [
    id 26
    label "lata"
    origin "text"
  ]
  node [
    id 27
    label "wi&#281;zienie"
    origin "text"
  ]
  node [
    id 28
    label "policja"
  ]
  node [
    id 29
    label "blacharz"
  ]
  node [
    id 30
    label "pa&#322;a"
  ]
  node [
    id 31
    label "mundurowy"
  ]
  node [
    id 32
    label "str&#243;&#380;"
  ]
  node [
    id 33
    label "glina"
  ]
  node [
    id 34
    label "wyrobi&#263;"
  ]
  node [
    id 35
    label "unieszkodliwi&#263;"
  ]
  node [
    id 36
    label "pomiesza&#263;"
  ]
  node [
    id 37
    label "blast"
  ]
  node [
    id 38
    label "przybi&#263;"
  ]
  node [
    id 39
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 40
    label "rozpi&#261;&#263;"
  ]
  node [
    id 41
    label "rozdrobni&#263;"
  ]
  node [
    id 42
    label "zdezorganizowa&#263;"
  ]
  node [
    id 43
    label "poturbowa&#263;"
  ]
  node [
    id 44
    label "pokona&#263;"
  ]
  node [
    id 45
    label "podzieli&#263;"
  ]
  node [
    id 46
    label "zniszczy&#263;"
  ]
  node [
    id 47
    label "rozepcha&#263;"
  ]
  node [
    id 48
    label "rozszyfrowa&#263;"
  ]
  node [
    id 49
    label "zgarn&#261;&#263;"
  ]
  node [
    id 50
    label "ubi&#263;"
  ]
  node [
    id 51
    label "bankrupt"
  ]
  node [
    id 52
    label "obrabowa&#263;"
  ]
  node [
    id 53
    label "zbudowa&#263;"
  ]
  node [
    id 54
    label "st&#322;uc"
  ]
  node [
    id 55
    label "divide"
  ]
  node [
    id 56
    label "rozproszy&#263;"
  ]
  node [
    id 57
    label "peddle"
  ]
  node [
    id 58
    label "pitch"
  ]
  node [
    id 59
    label "odbi&#263;"
  ]
  node [
    id 60
    label "naruszy&#263;"
  ]
  node [
    id 61
    label "banda"
  ]
  node [
    id 62
    label "organizacja"
  ]
  node [
    id 63
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 64
    label "gangster"
  ]
  node [
    id 65
    label "chcie&#263;"
  ]
  node [
    id 66
    label "woo"
  ]
  node [
    id 67
    label "domaga&#263;_si&#281;"
  ]
  node [
    id 68
    label "op&#322;ata"
  ]
  node [
    id 69
    label "zapowiada&#263;"
  ]
  node [
    id 70
    label "boast"
  ]
  node [
    id 71
    label "hazard"
  ]
  node [
    id 72
    label "kres_&#380;ycia"
  ]
  node [
    id 73
    label "defenestracja"
  ]
  node [
    id 74
    label "kres"
  ]
  node [
    id 75
    label "agonia"
  ]
  node [
    id 76
    label "&#380;ycie"
  ]
  node [
    id 77
    label "szeol"
  ]
  node [
    id 78
    label "mogi&#322;a"
  ]
  node [
    id 79
    label "pogrzeb"
  ]
  node [
    id 80
    label "istota_nadprzyrodzona"
  ]
  node [
    id 81
    label "pogrzebanie"
  ]
  node [
    id 82
    label "&#380;a&#322;oba"
  ]
  node [
    id 83
    label "zabicie"
  ]
  node [
    id 84
    label "upadek"
  ]
  node [
    id 85
    label "nijaki"
  ]
  node [
    id 86
    label "sezonowy"
  ]
  node [
    id 87
    label "letnio"
  ]
  node [
    id 88
    label "s&#322;oneczny"
  ]
  node [
    id 89
    label "weso&#322;y"
  ]
  node [
    id 90
    label "oboj&#281;tny"
  ]
  node [
    id 91
    label "latowy"
  ]
  node [
    id 92
    label "ciep&#322;y"
  ]
  node [
    id 93
    label "typowy"
  ]
  node [
    id 94
    label "wydawca"
  ]
  node [
    id 95
    label "kapitalista"
  ]
  node [
    id 96
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 97
    label "osoba_fizyczna"
  ]
  node [
    id 98
    label "wsp&#243;lnik"
  ]
  node [
    id 99
    label "klasa_&#347;rednia"
  ]
  node [
    id 100
    label "mi&#281;sny"
  ]
  node [
    id 101
    label "z&#322;oczy&#324;ca"
  ]
  node [
    id 102
    label "pogwa&#322;ciciel"
  ]
  node [
    id 103
    label "zu&#380;y&#263;"
  ]
  node [
    id 104
    label "przypali&#263;"
  ]
  node [
    id 105
    label "opali&#263;"
  ]
  node [
    id 106
    label "urazi&#263;"
  ]
  node [
    id 107
    label "utleni&#263;"
  ]
  node [
    id 108
    label "podda&#263;"
  ]
  node [
    id 109
    label "os&#322;abi&#263;"
  ]
  node [
    id 110
    label "spowodowa&#263;"
  ]
  node [
    id 111
    label "paliwo"
  ]
  node [
    id 112
    label "sear"
  ]
  node [
    id 113
    label "uszkodzi&#263;"
  ]
  node [
    id 114
    label "burn"
  ]
  node [
    id 115
    label "zapali&#263;"
  ]
  node [
    id 116
    label "zmetabolizowa&#263;"
  ]
  node [
    id 117
    label "odstawi&#263;"
  ]
  node [
    id 118
    label "bake"
  ]
  node [
    id 119
    label "zepsu&#263;"
  ]
  node [
    id 120
    label "scorch"
  ]
  node [
    id 121
    label "baga&#380;nik"
  ]
  node [
    id 122
    label "immobilizer"
  ]
  node [
    id 123
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 124
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 125
    label "poduszka_powietrzna"
  ]
  node [
    id 126
    label "dachowanie"
  ]
  node [
    id 127
    label "dwu&#347;lad"
  ]
  node [
    id 128
    label "deska_rozdzielcza"
  ]
  node [
    id 129
    label "poci&#261;g_drogowy"
  ]
  node [
    id 130
    label "kierownica"
  ]
  node [
    id 131
    label "pojazd_drogowy"
  ]
  node [
    id 132
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 133
    label "pompa_wodna"
  ]
  node [
    id 134
    label "silnik"
  ]
  node [
    id 135
    label "wycieraczka"
  ]
  node [
    id 136
    label "bak"
  ]
  node [
    id 137
    label "ABS"
  ]
  node [
    id 138
    label "most"
  ]
  node [
    id 139
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 140
    label "spryskiwacz"
  ]
  node [
    id 141
    label "t&#322;umik"
  ]
  node [
    id 142
    label "tempomat"
  ]
  node [
    id 143
    label "continue"
  ]
  node [
    id 144
    label "zabra&#263;"
  ]
  node [
    id 145
    label "zaaresztowa&#263;"
  ]
  node [
    id 146
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 147
    label "przerwa&#263;"
  ]
  node [
    id 148
    label "zamkn&#261;&#263;"
  ]
  node [
    id 149
    label "bury"
  ]
  node [
    id 150
    label "komornik"
  ]
  node [
    id 151
    label "unieruchomi&#263;"
  ]
  node [
    id 152
    label "suspend"
  ]
  node [
    id 153
    label "give"
  ]
  node [
    id 154
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 155
    label "zaczepi&#263;"
  ]
  node [
    id 156
    label "przechowa&#263;"
  ]
  node [
    id 157
    label "anticipate"
  ]
  node [
    id 158
    label "Zgredek"
  ]
  node [
    id 159
    label "kategoria_gramatyczna"
  ]
  node [
    id 160
    label "Casanova"
  ]
  node [
    id 161
    label "Don_Juan"
  ]
  node [
    id 162
    label "Gargantua"
  ]
  node [
    id 163
    label "Faust"
  ]
  node [
    id 164
    label "profanum"
  ]
  node [
    id 165
    label "Chocho&#322;"
  ]
  node [
    id 166
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 167
    label "koniugacja"
  ]
  node [
    id 168
    label "Winnetou"
  ]
  node [
    id 169
    label "Dwukwiat"
  ]
  node [
    id 170
    label "homo_sapiens"
  ]
  node [
    id 171
    label "Edyp"
  ]
  node [
    id 172
    label "Herkules_Poirot"
  ]
  node [
    id 173
    label "ludzko&#347;&#263;"
  ]
  node [
    id 174
    label "mikrokosmos"
  ]
  node [
    id 175
    label "person"
  ]
  node [
    id 176
    label "Sherlock_Holmes"
  ]
  node [
    id 177
    label "portrecista"
  ]
  node [
    id 178
    label "Szwejk"
  ]
  node [
    id 179
    label "Hamlet"
  ]
  node [
    id 180
    label "duch"
  ]
  node [
    id 181
    label "g&#322;owa"
  ]
  node [
    id 182
    label "oddzia&#322;ywanie"
  ]
  node [
    id 183
    label "Quasimodo"
  ]
  node [
    id 184
    label "Dulcynea"
  ]
  node [
    id 185
    label "Don_Kiszot"
  ]
  node [
    id 186
    label "Wallenrod"
  ]
  node [
    id 187
    label "Plastu&#347;"
  ]
  node [
    id 188
    label "Harry_Potter"
  ]
  node [
    id 189
    label "figura"
  ]
  node [
    id 190
    label "parali&#380;owa&#263;"
  ]
  node [
    id 191
    label "istota"
  ]
  node [
    id 192
    label "Werter"
  ]
  node [
    id 193
    label "antropochoria"
  ]
  node [
    id 194
    label "posta&#263;"
  ]
  node [
    id 195
    label "partia"
  ]
  node [
    id 196
    label "zwi&#261;zek"
  ]
  node [
    id 197
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 198
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 199
    label "sakrament"
  ]
  node [
    id 200
    label "para"
  ]
  node [
    id 201
    label "stan_cywilny"
  ]
  node [
    id 202
    label "lewirat"
  ]
  node [
    id 203
    label "matrymonialny"
  ]
  node [
    id 204
    label "majority"
  ]
  node [
    id 205
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 206
    label "klacz"
  ]
  node [
    id 207
    label "konsekwencja"
  ]
  node [
    id 208
    label "punishment"
  ]
  node [
    id 209
    label "forfeit"
  ]
  node [
    id 210
    label "roboty_przymusowe"
  ]
  node [
    id 211
    label "nemezis"
  ]
  node [
    id 212
    label "kwota"
  ]
  node [
    id 213
    label "summer"
  ]
  node [
    id 214
    label "czas"
  ]
  node [
    id 215
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 216
    label "ogranicza&#263;"
  ]
  node [
    id 217
    label "uniemo&#380;liwianie"
  ]
  node [
    id 218
    label "Butyrki"
  ]
  node [
    id 219
    label "ciupa"
  ]
  node [
    id 220
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 221
    label "reedukator"
  ]
  node [
    id 222
    label "miejsce_odosobnienia"
  ]
  node [
    id 223
    label "&#321;ubianka"
  ]
  node [
    id 224
    label "pierdel"
  ]
  node [
    id 225
    label "imprisonment"
  ]
  node [
    id 226
    label "sytuacja"
  ]
  node [
    id 227
    label "Magdalena"
  ]
  node [
    id 228
    label "Zieli&#324;ska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 46
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 51
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 213
  ]
  edge [
    source 26
    target 214
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 216
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 227
    target 228
  ]
]
