graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.0689655172413794
  density 0.024057738572574178
  graphCliqueNumber 2
  node [
    id 0
    label "anonimowemirkowyznania"
    origin "text"
  ]
  node [
    id 1
    label "pad&#322;o"
    origin "text"
  ]
  node [
    id 2
    label "ofiara"
    origin "text"
  ]
  node [
    id 3
    label "w&#322;asny"
    origin "text"
  ]
  node [
    id 4
    label "zarzutka"
    origin "text"
  ]
  node [
    id 5
    label "pan"
    origin "text"
  ]
  node [
    id 6
    label "nigdy"
    origin "text"
  ]
  node [
    id 7
    label "ufa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "kobieta"
    origin "text"
  ]
  node [
    id 9
    label "cz&#322;owiek"
  ]
  node [
    id 10
    label "gamo&#324;"
  ]
  node [
    id 11
    label "istota_&#380;ywa"
  ]
  node [
    id 12
    label "dupa_wo&#322;owa"
  ]
  node [
    id 13
    label "przedmiot"
  ]
  node [
    id 14
    label "pastwa"
  ]
  node [
    id 15
    label "rzecz"
  ]
  node [
    id 16
    label "zbi&#243;rka"
  ]
  node [
    id 17
    label "dar"
  ]
  node [
    id 18
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 19
    label "nastawienie"
  ]
  node [
    id 20
    label "crack"
  ]
  node [
    id 21
    label "zwierz&#281;"
  ]
  node [
    id 22
    label "ko&#347;cielny"
  ]
  node [
    id 23
    label "po&#347;miewisko"
  ]
  node [
    id 24
    label "swoisty"
  ]
  node [
    id 25
    label "czyj&#347;"
  ]
  node [
    id 26
    label "osobny"
  ]
  node [
    id 27
    label "zwi&#261;zany"
  ]
  node [
    id 28
    label "samodzielny"
  ]
  node [
    id 29
    label "pelerynka"
  ]
  node [
    id 30
    label "jarzyn&#243;wka"
  ]
  node [
    id 31
    label "fitka"
  ]
  node [
    id 32
    label "profesor"
  ]
  node [
    id 33
    label "kszta&#322;ciciel"
  ]
  node [
    id 34
    label "jegomo&#347;&#263;"
  ]
  node [
    id 35
    label "zwrot"
  ]
  node [
    id 36
    label "pracodawca"
  ]
  node [
    id 37
    label "rz&#261;dzenie"
  ]
  node [
    id 38
    label "m&#261;&#380;"
  ]
  node [
    id 39
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 40
    label "ch&#322;opina"
  ]
  node [
    id 41
    label "bratek"
  ]
  node [
    id 42
    label "opiekun"
  ]
  node [
    id 43
    label "doros&#322;y"
  ]
  node [
    id 44
    label "preceptor"
  ]
  node [
    id 45
    label "Midas"
  ]
  node [
    id 46
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 47
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 48
    label "murza"
  ]
  node [
    id 49
    label "ojciec"
  ]
  node [
    id 50
    label "androlog"
  ]
  node [
    id 51
    label "pupil"
  ]
  node [
    id 52
    label "efendi"
  ]
  node [
    id 53
    label "nabab"
  ]
  node [
    id 54
    label "w&#322;odarz"
  ]
  node [
    id 55
    label "szkolnik"
  ]
  node [
    id 56
    label "pedagog"
  ]
  node [
    id 57
    label "popularyzator"
  ]
  node [
    id 58
    label "andropauza"
  ]
  node [
    id 59
    label "gra_w_karty"
  ]
  node [
    id 60
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 61
    label "Mieszko_I"
  ]
  node [
    id 62
    label "bogaty"
  ]
  node [
    id 63
    label "samiec"
  ]
  node [
    id 64
    label "przyw&#243;dca"
  ]
  node [
    id 65
    label "pa&#324;stwo"
  ]
  node [
    id 66
    label "belfer"
  ]
  node [
    id 67
    label "kompletnie"
  ]
  node [
    id 68
    label "czu&#263;"
  ]
  node [
    id 69
    label "chowa&#263;"
  ]
  node [
    id 70
    label "wierza&#263;"
  ]
  node [
    id 71
    label "powierzy&#263;"
  ]
  node [
    id 72
    label "powierza&#263;"
  ]
  node [
    id 73
    label "trust"
  ]
  node [
    id 74
    label "przekwitanie"
  ]
  node [
    id 75
    label "m&#281;&#380;yna"
  ]
  node [
    id 76
    label "babka"
  ]
  node [
    id 77
    label "samica"
  ]
  node [
    id 78
    label "ulec"
  ]
  node [
    id 79
    label "uleganie"
  ]
  node [
    id 80
    label "partnerka"
  ]
  node [
    id 81
    label "&#380;ona"
  ]
  node [
    id 82
    label "ulega&#263;"
  ]
  node [
    id 83
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 84
    label "ulegni&#281;cie"
  ]
  node [
    id 85
    label "menopauza"
  ]
  node [
    id 86
    label "&#322;ono"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
]
