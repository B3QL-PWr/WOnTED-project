graph [
  maxDegree 37
  minDegree 1
  meanDegree 1.958762886597938
  density 0.020403780068728523
  graphCliqueNumber 2
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "rzep"
    origin "text"
  ]
  node [
    id 2
    label "pope&#322;ni&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kolejny"
    origin "text"
  ]
  node [
    id 4
    label "satyryczny"
    origin "text"
  ]
  node [
    id 5
    label "wpis"
    origin "text"
  ]
  node [
    id 6
    label "atak"
    origin "text"
  ]
  node [
    id 7
    label "no&#380;ownik"
    origin "text"
  ]
  node [
    id 8
    label "dworzec"
    origin "text"
  ]
  node [
    id 9
    label "tarnowo"
    origin "text"
  ]
  node [
    id 10
    label "cz&#322;owiek"
  ]
  node [
    id 11
    label "profesor"
  ]
  node [
    id 12
    label "kszta&#322;ciciel"
  ]
  node [
    id 13
    label "jegomo&#347;&#263;"
  ]
  node [
    id 14
    label "zwrot"
  ]
  node [
    id 15
    label "pracodawca"
  ]
  node [
    id 16
    label "rz&#261;dzenie"
  ]
  node [
    id 17
    label "m&#261;&#380;"
  ]
  node [
    id 18
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 19
    label "ch&#322;opina"
  ]
  node [
    id 20
    label "bratek"
  ]
  node [
    id 21
    label "opiekun"
  ]
  node [
    id 22
    label "doros&#322;y"
  ]
  node [
    id 23
    label "preceptor"
  ]
  node [
    id 24
    label "Midas"
  ]
  node [
    id 25
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 26
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 27
    label "murza"
  ]
  node [
    id 28
    label "ojciec"
  ]
  node [
    id 29
    label "androlog"
  ]
  node [
    id 30
    label "pupil"
  ]
  node [
    id 31
    label "efendi"
  ]
  node [
    id 32
    label "nabab"
  ]
  node [
    id 33
    label "w&#322;odarz"
  ]
  node [
    id 34
    label "szkolnik"
  ]
  node [
    id 35
    label "pedagog"
  ]
  node [
    id 36
    label "popularyzator"
  ]
  node [
    id 37
    label "andropauza"
  ]
  node [
    id 38
    label "gra_w_karty"
  ]
  node [
    id 39
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 40
    label "Mieszko_I"
  ]
  node [
    id 41
    label "bogaty"
  ]
  node [
    id 42
    label "samiec"
  ]
  node [
    id 43
    label "przyw&#243;dca"
  ]
  node [
    id 44
    label "pa&#324;stwo"
  ]
  node [
    id 45
    label "belfer"
  ]
  node [
    id 46
    label "nie&#322;upka"
  ]
  node [
    id 47
    label "bur"
  ]
  node [
    id 48
    label "zapi&#281;cie"
  ]
  node [
    id 49
    label "zrobi&#263;"
  ]
  node [
    id 50
    label "stworzy&#263;"
  ]
  node [
    id 51
    label "inny"
  ]
  node [
    id 52
    label "nast&#281;pnie"
  ]
  node [
    id 53
    label "kt&#243;ry&#347;"
  ]
  node [
    id 54
    label "kolejno"
  ]
  node [
    id 55
    label "nastopny"
  ]
  node [
    id 56
    label "krytyczny"
  ]
  node [
    id 57
    label "prze&#347;miewczy"
  ]
  node [
    id 58
    label "satyrycznie"
  ]
  node [
    id 59
    label "czynno&#347;&#263;"
  ]
  node [
    id 60
    label "entrance"
  ]
  node [
    id 61
    label "inscription"
  ]
  node [
    id 62
    label "akt"
  ]
  node [
    id 63
    label "op&#322;ata"
  ]
  node [
    id 64
    label "tekst"
  ]
  node [
    id 65
    label "manewr"
  ]
  node [
    id 66
    label "spasm"
  ]
  node [
    id 67
    label "wypowied&#378;"
  ]
  node [
    id 68
    label "&#380;&#261;danie"
  ]
  node [
    id 69
    label "rzucenie"
  ]
  node [
    id 70
    label "pogorszenie"
  ]
  node [
    id 71
    label "krytyka"
  ]
  node [
    id 72
    label "stroke"
  ]
  node [
    id 73
    label "liga"
  ]
  node [
    id 74
    label "pozycja"
  ]
  node [
    id 75
    label "zagrywka"
  ]
  node [
    id 76
    label "rzuci&#263;"
  ]
  node [
    id 77
    label "walka"
  ]
  node [
    id 78
    label "przemoc"
  ]
  node [
    id 79
    label "pogoda"
  ]
  node [
    id 80
    label "przyp&#322;yw"
  ]
  node [
    id 81
    label "fit"
  ]
  node [
    id 82
    label "ofensywa"
  ]
  node [
    id 83
    label "knock"
  ]
  node [
    id 84
    label "oznaka"
  ]
  node [
    id 85
    label "bat"
  ]
  node [
    id 86
    label "kaszel"
  ]
  node [
    id 87
    label "rzemie&#347;lnik"
  ]
  node [
    id 88
    label "bandyta"
  ]
  node [
    id 89
    label "przechowalnia"
  ]
  node [
    id 90
    label "hala"
  ]
  node [
    id 91
    label "peron"
  ]
  node [
    id 92
    label "poczekalnia"
  ]
  node [
    id 93
    label "stacja"
  ]
  node [
    id 94
    label "majdaniarz"
  ]
  node [
    id 95
    label "Polska"
  ]
  node [
    id 96
    label "stabilnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 95
    target 96
  ]
]
