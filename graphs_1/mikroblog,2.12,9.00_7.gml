graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.894736842105263
  density 0.10526315789473684
  graphCliqueNumber 2
  node [
    id 0
    label "solgazu"
    origin "text"
  ]
  node [
    id 1
    label "bycie"
    origin "text"
  ]
  node [
    id 2
    label "statysta"
    origin "text"
  ]
  node [
    id 3
    label "produkowanie"
  ]
  node [
    id 4
    label "przeszkadzanie"
  ]
  node [
    id 5
    label "widzenie"
  ]
  node [
    id 6
    label "byt"
  ]
  node [
    id 7
    label "robienie"
  ]
  node [
    id 8
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 9
    label "znikni&#281;cie"
  ]
  node [
    id 10
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 11
    label "obejrzenie"
  ]
  node [
    id 12
    label "urzeczywistnianie"
  ]
  node [
    id 13
    label "wyprodukowanie"
  ]
  node [
    id 14
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 15
    label "przeszkodzenie"
  ]
  node [
    id 16
    label "being"
  ]
  node [
    id 17
    label "obserwator"
  ]
  node [
    id 18
    label "aktor"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
]
