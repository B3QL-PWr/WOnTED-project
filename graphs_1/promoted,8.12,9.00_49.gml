graph [
  maxDegree 16
  minDegree 1
  meanDegree 2.016260162601626
  density 0.016526722644275623
  graphCliqueNumber 2
  node [
    id 0
    label "brak"
    origin "text"
  ]
  node [
    id 1
    label "konsekwencja"
    origin "text"
  ]
  node [
    id 2
    label "przez"
    origin "text"
  ]
  node [
    id 3
    label "jeszcze"
    origin "text"
  ]
  node [
    id 4
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 5
    label "nier&#243;wno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "w&#347;r&#243;d"
    origin "text"
  ]
  node [
    id 7
    label "pracownik"
    origin "text"
  ]
  node [
    id 8
    label "podej&#347;cie"
    origin "text"
  ]
  node [
    id 9
    label "rz&#261;dz&#261;ca"
    origin "text"
  ]
  node [
    id 10
    label "kontrola"
    origin "text"
  ]
  node [
    id 11
    label "absencja"
    origin "text"
  ]
  node [
    id 12
    label "chorobowe"
    origin "text"
  ]
  node [
    id 13
    label "zmiana"
    origin "text"
  ]
  node [
    id 14
    label "przepis"
    origin "text"
  ]
  node [
    id 15
    label "tym"
    origin "text"
  ]
  node [
    id 16
    label "ocenia&#263;"
    origin "text"
  ]
  node [
    id 17
    label "specjalista"
    origin "text"
  ]
  node [
    id 18
    label "prywatywny"
  ]
  node [
    id 19
    label "defect"
  ]
  node [
    id 20
    label "odej&#347;cie"
  ]
  node [
    id 21
    label "gap"
  ]
  node [
    id 22
    label "kr&#243;tki"
  ]
  node [
    id 23
    label "wyr&#243;b"
  ]
  node [
    id 24
    label "nieistnienie"
  ]
  node [
    id 25
    label "wada"
  ]
  node [
    id 26
    label "odej&#347;&#263;"
  ]
  node [
    id 27
    label "odchodzenie"
  ]
  node [
    id 28
    label "odchodzi&#263;"
  ]
  node [
    id 29
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 30
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 31
    label "skrupianie_si&#281;"
  ]
  node [
    id 32
    label "odczuwanie"
  ]
  node [
    id 33
    label "skrupienie_si&#281;"
  ]
  node [
    id 34
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 35
    label "odczucie"
  ]
  node [
    id 36
    label "odczuwa&#263;"
  ]
  node [
    id 37
    label "odczu&#263;"
  ]
  node [
    id 38
    label "event"
  ]
  node [
    id 39
    label "rezultat"
  ]
  node [
    id 40
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 41
    label "koszula_Dejaniry"
  ]
  node [
    id 42
    label "ci&#261;gle"
  ]
  node [
    id 43
    label "doros&#322;y"
  ]
  node [
    id 44
    label "wiele"
  ]
  node [
    id 45
    label "dorodny"
  ]
  node [
    id 46
    label "znaczny"
  ]
  node [
    id 47
    label "du&#380;o"
  ]
  node [
    id 48
    label "prawdziwy"
  ]
  node [
    id 49
    label "niema&#322;o"
  ]
  node [
    id 50
    label "wa&#380;ny"
  ]
  node [
    id 51
    label "rozwini&#281;ty"
  ]
  node [
    id 52
    label "miejsce"
  ]
  node [
    id 53
    label "krzywda"
  ]
  node [
    id 54
    label "brzydota"
  ]
  node [
    id 55
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 56
    label "r&#243;&#380;nica"
  ]
  node [
    id 57
    label "poj&#281;cie"
  ]
  node [
    id 58
    label "imbalance"
  ]
  node [
    id 59
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 60
    label "cz&#322;owiek"
  ]
  node [
    id 61
    label "delegowa&#263;"
  ]
  node [
    id 62
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 63
    label "pracu&#347;"
  ]
  node [
    id 64
    label "delegowanie"
  ]
  node [
    id 65
    label "r&#281;ka"
  ]
  node [
    id 66
    label "salariat"
  ]
  node [
    id 67
    label "powaga"
  ]
  node [
    id 68
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 69
    label "cecha"
  ]
  node [
    id 70
    label "droga"
  ]
  node [
    id 71
    label "ploy"
  ]
  node [
    id 72
    label "nastawienie"
  ]
  node [
    id 73
    label "potraktowanie"
  ]
  node [
    id 74
    label "nasi&#261;kni&#281;cie"
  ]
  node [
    id 75
    label "nabranie"
  ]
  node [
    id 76
    label "czynno&#347;&#263;"
  ]
  node [
    id 77
    label "examination"
  ]
  node [
    id 78
    label "legalizacja_pierwotna"
  ]
  node [
    id 79
    label "w&#322;adza"
  ]
  node [
    id 80
    label "perlustracja"
  ]
  node [
    id 81
    label "instytucja"
  ]
  node [
    id 82
    label "legalizacja_ponowna"
  ]
  node [
    id 83
    label "failure"
  ]
  node [
    id 84
    label "zwolnienie"
  ]
  node [
    id 85
    label "zasi&#322;ek"
  ]
  node [
    id 86
    label "zasi&#322;ek_chorobowy"
  ]
  node [
    id 87
    label "anatomopatolog"
  ]
  node [
    id 88
    label "rewizja"
  ]
  node [
    id 89
    label "oznaka"
  ]
  node [
    id 90
    label "czas"
  ]
  node [
    id 91
    label "ferment"
  ]
  node [
    id 92
    label "komplet"
  ]
  node [
    id 93
    label "tura"
  ]
  node [
    id 94
    label "amendment"
  ]
  node [
    id 95
    label "zmianka"
  ]
  node [
    id 96
    label "odmienianie"
  ]
  node [
    id 97
    label "passage"
  ]
  node [
    id 98
    label "zjawisko"
  ]
  node [
    id 99
    label "change"
  ]
  node [
    id 100
    label "praca"
  ]
  node [
    id 101
    label "przedawnienie_si&#281;"
  ]
  node [
    id 102
    label "recepta"
  ]
  node [
    id 103
    label "norma_prawna"
  ]
  node [
    id 104
    label "kodeks"
  ]
  node [
    id 105
    label "prawo"
  ]
  node [
    id 106
    label "regulation"
  ]
  node [
    id 107
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 108
    label "porada"
  ]
  node [
    id 109
    label "przedawnianie_si&#281;"
  ]
  node [
    id 110
    label "spos&#243;b"
  ]
  node [
    id 111
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 112
    label "award"
  ]
  node [
    id 113
    label "gauge"
  ]
  node [
    id 114
    label "s&#261;dzi&#263;"
  ]
  node [
    id 115
    label "strike"
  ]
  node [
    id 116
    label "wystawia&#263;"
  ]
  node [
    id 117
    label "okre&#347;la&#263;"
  ]
  node [
    id 118
    label "znajdowa&#263;"
  ]
  node [
    id 119
    label "znawca"
  ]
  node [
    id 120
    label "wyspecjalizowanie_si&#281;"
  ]
  node [
    id 121
    label "lekarz"
  ]
  node [
    id 122
    label "spec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 17
    target 60
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
]
