graph [
  maxDegree 43
  minDegree 1
  meanDegree 2.229268292682927
  density 0.002721939307305161
  graphCliqueNumber 3
  node [
    id 0
    label "wtedy"
    origin "text"
  ]
  node [
    id 1
    label "suszy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "usi&#261;&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "znowu"
    origin "text"
  ]
  node [
    id 4
    label "przed"
    origin "text"
  ]
  node [
    id 5
    label "swem"
    origin "text"
  ]
  node [
    id 6
    label "biuro"
    origin "text"
  ]
  node [
    id 7
    label "d&#322;ugo"
    origin "text"
  ]
  node [
    id 8
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "nieruchomy"
    origin "text"
  ]
  node [
    id 10
    label "szklistemi"
    origin "text"
  ]
  node [
    id 11
    label "oko"
    origin "text"
  ]
  node [
    id 12
    label "patrze&#263;"
    origin "text"
  ]
  node [
    id 13
    label "przestrze&#324;"
    origin "text"
  ]
  node [
    id 14
    label "u&#347;miech"
    origin "text"
  ]
  node [
    id 15
    label "ukazywa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "si&#281;"
    origin "text"
  ]
  node [
    id 17
    label "znika&#263;"
    origin "text"
  ]
  node [
    id 18
    label "blady"
    origin "text"
  ]
  node [
    id 19
    label "wa&#380;ki"
    origin "text"
  ]
  node [
    id 20
    label "warga"
    origin "text"
  ]
  node [
    id 21
    label "stopniowo"
    origin "text"
  ]
  node [
    id 22
    label "czo&#322;o"
    origin "text"
  ]
  node [
    id 23
    label "chyli&#263;"
    origin "text"
  ]
  node [
    id 24
    label "coraz"
    origin "text"
  ]
  node [
    id 25
    label "nisko"
    origin "text"
  ]
  node [
    id 26
    label "opa&#347;&#263;"
    origin "text"
  ]
  node [
    id 27
    label "d&#322;o&#324;"
    origin "text"
  ]
  node [
    id 28
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 29
    label "ten"
    origin "text"
  ]
  node [
    id 30
    label "dziwnem"
    origin "text"
  ]
  node [
    id 31
    label "jakiem&#347;"
    origin "text"
  ]
  node [
    id 32
    label "ogarniony"
    origin "text"
  ]
  node [
    id 33
    label "duma&#263;"
    origin "text"
  ]
  node [
    id 34
    label "pozosta&#263;"
    origin "text"
  ]
  node [
    id 35
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 36
    label "tak"
    origin "text"
  ]
  node [
    id 37
    label "schy&#322;ek"
    origin "text"
  ]
  node [
    id 38
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 39
    label "bez"
    origin "text"
  ]
  node [
    id 40
    label "ruch"
    origin "text"
  ]
  node [
    id 41
    label "drgnienie"
    origin "text"
  ]
  node [
    id 42
    label "dom"
    origin "text"
  ]
  node [
    id 43
    label "gwar"
    origin "text"
  ]
  node [
    id 44
    label "przycicha&#263;"
    origin "text"
  ]
  node [
    id 45
    label "stopie&#324;"
    origin "text"
  ]
  node [
    id 46
    label "przy"
    origin "text"
  ]
  node [
    id 47
    label "zapada&#263;"
    origin "text"
  ]
  node [
    id 48
    label "zmrok"
    origin "text"
  ]
  node [
    id 49
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 50
    label "by&#263;"
    origin "text"
  ]
  node [
    id 51
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 52
    label "trzy"
    origin "text"
  ]
  node [
    id 53
    label "kobieta"
    origin "text"
  ]
  node [
    id 54
    label "towarzystwo"
    origin "text"
  ]
  node [
    id 55
    label "m&#322;ode"
    origin "text"
  ]
  node [
    id 56
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 57
    label "prowadz&#261;cy"
    origin "text"
  ]
  node [
    id 58
    label "siebie"
    origin "text"
  ]
  node [
    id 59
    label "pudel"
    origin "text"
  ]
  node [
    id 60
    label "opuszcza&#263;"
    origin "text"
  ]
  node [
    id 61
    label "wychodz&#261;ca"
    origin "text"
  ]
  node [
    id 62
    label "ulica"
    origin "text"
  ]
  node [
    id 63
    label "bardzo"
    origin "text"
  ]
  node [
    id 64
    label "mizerny"
    origin "text"
  ]
  node [
    id 65
    label "wytrze&#263;"
    origin "text"
  ]
  node [
    id 66
    label "futerko"
    origin "text"
  ]
  node [
    id 67
    label "zarzuci&#263;"
    origin "text"
  ]
  node [
    id 68
    label "szeroki"
    origin "text"
  ]
  node [
    id 69
    label "fruwa&#263;"
    origin "text"
  ]
  node [
    id 70
    label "wyfalbanowa&#263;"
    origin "text"
  ]
  node [
    id 71
    label "suknia"
    origin "text"
  ]
  node [
    id 72
    label "przybrudny"
    origin "text"
  ]
  node [
    id 73
    label "stara"
    origin "text"
  ]
  node [
    id 74
    label "szalik"
    origin "text"
  ]
  node [
    id 75
    label "okrywa&#263;"
    origin "text"
  ]
  node [
    id 76
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 77
    label "wystroi&#263;"
    origin "text"
  ]
  node [
    id 78
    label "niob"
    origin "text"
  ]
  node [
    id 79
    label "lok"
    origin "text"
  ]
  node [
    id 80
    label "kokarda"
    origin "text"
  ]
  node [
    id 81
    label "papierowy"
    origin "text"
  ]
  node [
    id 82
    label "kwiat"
    origin "text"
  ]
  node [
    id 83
    label "str&#243;j"
    origin "text"
  ]
  node [
    id 84
    label "odziewa&#263;"
    origin "text"
  ]
  node [
    id 85
    label "dwa"
    origin "text"
  ]
  node [
    id 86
    label "tylko"
    origin "text"
  ]
  node [
    id 87
    label "m&#322;odsza"
    origin "text"
  ]
  node [
    id 88
    label "starsza"
    origin "text"
  ]
  node [
    id 89
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 90
    label "ciep&#322;y"
    origin "text"
  ]
  node [
    id 91
    label "dostatnie"
    origin "text"
  ]
  node [
    id 92
    label "futro"
    origin "text"
  ]
  node [
    id 93
    label "pod"
    origin "text"
  ]
  node [
    id 94
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 95
    label "widnie&#263;"
    origin "text"
  ]
  node [
    id 96
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 97
    label "jak"
    origin "text"
  ]
  node [
    id 98
    label "pancerz"
    origin "text"
  ]
  node [
    id 99
    label "at&#322;asowy"
    origin "text"
  ]
  node [
    id 100
    label "stanik"
    origin "text"
  ]
  node [
    id 101
    label "stalowemi"
    origin "text"
  ]
  node [
    id 102
    label "guzik"
    origin "text"
  ]
  node [
    id 103
    label "rudawy"
    origin "text"
  ]
  node [
    id 104
    label "w&#322;os"
    origin "text"
  ]
  node [
    id 105
    label "zaczesa&#263;"
    origin "text"
  ]
  node [
    id 106
    label "p&#322;aski"
    origin "text"
  ]
  node [
    id 107
    label "pasmo"
    origin "text"
  ]
  node [
    id 108
    label "warkocz"
    origin "text"
  ]
  node [
    id 109
    label "przykry&#263;"
    origin "text"
  ]
  node [
    id 110
    label "wielki"
    origin "text"
  ]
  node [
    id 111
    label "bezbarwny"
    origin "text"
  ]
  node [
    id 112
    label "wygodny"
    origin "text"
  ]
  node [
    id 113
    label "chustka"
    origin "text"
  ]
  node [
    id 114
    label "kiedy&#347;"
  ]
  node [
    id 115
    label "zmienia&#263;"
  ]
  node [
    id 116
    label "pemikan"
  ]
  node [
    id 117
    label "&#347;cina&#263;"
  ]
  node [
    id 118
    label "robi&#263;"
  ]
  node [
    id 119
    label "drain"
  ]
  node [
    id 120
    label "powodowa&#263;"
  ]
  node [
    id 121
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 122
    label "zaj&#261;&#263;"
  ]
  node [
    id 123
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 124
    label "uplasowa&#263;_si&#281;"
  ]
  node [
    id 125
    label "przyj&#261;&#263;"
  ]
  node [
    id 126
    label "mount"
  ]
  node [
    id 127
    label "spocz&#261;&#263;"
  ]
  node [
    id 128
    label "wyl&#261;dowa&#263;"
  ]
  node [
    id 129
    label "zmieni&#263;"
  ]
  node [
    id 130
    label "przyst&#261;pi&#263;"
  ]
  node [
    id 131
    label "sie&#347;&#263;"
  ]
  node [
    id 132
    label "s&#261;d"
  ]
  node [
    id 133
    label "biurko"
  ]
  node [
    id 134
    label "palestra"
  ]
  node [
    id 135
    label "pomieszczenie"
  ]
  node [
    id 136
    label "agency"
  ]
  node [
    id 137
    label "Biuro_Lustracyjne"
  ]
  node [
    id 138
    label "instytucja"
  ]
  node [
    id 139
    label "board"
  ]
  node [
    id 140
    label "dzia&#322;"
  ]
  node [
    id 141
    label "boks"
  ]
  node [
    id 142
    label "d&#322;ugi"
  ]
  node [
    id 143
    label "trwa&#263;"
  ]
  node [
    id 144
    label "sit"
  ]
  node [
    id 145
    label "pause"
  ]
  node [
    id 146
    label "ptak"
  ]
  node [
    id 147
    label "garowa&#263;"
  ]
  node [
    id 148
    label "mieszka&#263;"
  ]
  node [
    id 149
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 150
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 151
    label "przebywa&#263;"
  ]
  node [
    id 152
    label "brood"
  ]
  node [
    id 153
    label "zwierz&#281;"
  ]
  node [
    id 154
    label "doprowadza&#263;"
  ]
  node [
    id 155
    label "spoczywa&#263;"
  ]
  node [
    id 156
    label "tkwi&#263;"
  ]
  node [
    id 157
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 158
    label "znieruchomienie"
  ]
  node [
    id 159
    label "stacjonarnie"
  ]
  node [
    id 160
    label "unieruchamianie"
  ]
  node [
    id 161
    label "nieruchomo"
  ]
  node [
    id 162
    label "nieruchomienie"
  ]
  node [
    id 163
    label "unieruchomienie"
  ]
  node [
    id 164
    label "wypowied&#378;"
  ]
  node [
    id 165
    label "siniec"
  ]
  node [
    id 166
    label "uwaga"
  ]
  node [
    id 167
    label "rzecz"
  ]
  node [
    id 168
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 169
    label "powieka"
  ]
  node [
    id 170
    label "oczy"
  ]
  node [
    id 171
    label "&#347;lepko"
  ]
  node [
    id 172
    label "ga&#322;ka_oczna"
  ]
  node [
    id 173
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 174
    label "&#347;lepie"
  ]
  node [
    id 175
    label "twarz"
  ]
  node [
    id 176
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 177
    label "organ"
  ]
  node [
    id 178
    label "nerw_wzrokowy"
  ]
  node [
    id 179
    label "wzrok"
  ]
  node [
    id 180
    label "spoj&#243;wka"
  ]
  node [
    id 181
    label "&#378;renica"
  ]
  node [
    id 182
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 183
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 184
    label "kaprawie&#263;"
  ]
  node [
    id 185
    label "kaprawienie"
  ]
  node [
    id 186
    label "spojrzenie"
  ]
  node [
    id 187
    label "net"
  ]
  node [
    id 188
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 189
    label "coloboma"
  ]
  node [
    id 190
    label "ros&#243;&#322;"
  ]
  node [
    id 191
    label "koso"
  ]
  node [
    id 192
    label "szuka&#263;"
  ]
  node [
    id 193
    label "go_steady"
  ]
  node [
    id 194
    label "dba&#263;"
  ]
  node [
    id 195
    label "traktowa&#263;"
  ]
  node [
    id 196
    label "os&#261;dza&#263;"
  ]
  node [
    id 197
    label "punkt_widzenia"
  ]
  node [
    id 198
    label "uwa&#380;a&#263;"
  ]
  node [
    id 199
    label "look"
  ]
  node [
    id 200
    label "pogl&#261;da&#263;"
  ]
  node [
    id 201
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 202
    label "oktant"
  ]
  node [
    id 203
    label "niezmierzony"
  ]
  node [
    id 204
    label "miejsce"
  ]
  node [
    id 205
    label "bezbrze&#380;e"
  ]
  node [
    id 206
    label "przedzieli&#263;"
  ]
  node [
    id 207
    label "rozdzielanie"
  ]
  node [
    id 208
    label "rozdziela&#263;"
  ]
  node [
    id 209
    label "zbi&#243;r"
  ]
  node [
    id 210
    label "punkt"
  ]
  node [
    id 211
    label "przestw&#243;r"
  ]
  node [
    id 212
    label "przedzielenie"
  ]
  node [
    id 213
    label "nielito&#347;ciwy"
  ]
  node [
    id 214
    label "czasoprzestrze&#324;"
  ]
  node [
    id 215
    label "reakcja"
  ]
  node [
    id 216
    label "mina"
  ]
  node [
    id 217
    label "smile"
  ]
  node [
    id 218
    label "unwrap"
  ]
  node [
    id 219
    label "pokazywa&#263;"
  ]
  node [
    id 220
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 221
    label "wychodzi&#263;"
  ]
  node [
    id 222
    label "decrease"
  ]
  node [
    id 223
    label "przepada&#263;"
  ]
  node [
    id 224
    label "kamforowy"
  ]
  node [
    id 225
    label "shrink"
  ]
  node [
    id 226
    label "gin&#261;&#263;"
  ]
  node [
    id 227
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 228
    label "die"
  ]
  node [
    id 229
    label "leak"
  ]
  node [
    id 230
    label "s&#322;aby"
  ]
  node [
    id 231
    label "nienasycony"
  ]
  node [
    id 232
    label "blado"
  ]
  node [
    id 233
    label "jasny"
  ]
  node [
    id 234
    label "zwyczajny"
  ]
  node [
    id 235
    label "szarzenie"
  ]
  node [
    id 236
    label "niezabawny"
  ]
  node [
    id 237
    label "niewa&#380;ny"
  ]
  node [
    id 238
    label "oboj&#281;tny"
  ]
  node [
    id 239
    label "nieciekawy"
  ]
  node [
    id 240
    label "bezbarwnie"
  ]
  node [
    id 241
    label "poszarzenie"
  ]
  node [
    id 242
    label "ch&#322;odny"
  ]
  node [
    id 243
    label "nieatrakcyjny"
  ]
  node [
    id 244
    label "owady_uskrzydlone"
  ]
  node [
    id 245
    label "wa&#380;ny"
  ]
  node [
    id 246
    label "rozszczep_wargi"
  ]
  node [
    id 247
    label "artykulator"
  ]
  node [
    id 248
    label "usta"
  ]
  node [
    id 249
    label "zgrubienie"
  ]
  node [
    id 250
    label "stopniowy"
  ]
  node [
    id 251
    label "linearnie"
  ]
  node [
    id 252
    label "kierunek"
  ]
  node [
    id 253
    label "strona"
  ]
  node [
    id 254
    label "skro&#324;"
  ]
  node [
    id 255
    label "obni&#380;a&#263;"
  ]
  node [
    id 256
    label "refuse"
  ]
  node [
    id 257
    label "blisko"
  ]
  node [
    id 258
    label "po&#347;lednio"
  ]
  node [
    id 259
    label "despicably"
  ]
  node [
    id 260
    label "ma&#322;o"
  ]
  node [
    id 261
    label "niski"
  ]
  node [
    id 262
    label "vilely"
  ]
  node [
    id 263
    label "uni&#380;enie"
  ]
  node [
    id 264
    label "wstydliwie"
  ]
  node [
    id 265
    label "pospolicie"
  ]
  node [
    id 266
    label "ma&#322;y"
  ]
  node [
    id 267
    label "odpa&#347;&#263;"
  ]
  node [
    id 268
    label "sta&#263;_si&#281;"
  ]
  node [
    id 269
    label "spend"
  ]
  node [
    id 270
    label "os&#322;abn&#261;&#263;"
  ]
  node [
    id 271
    label "subside"
  ]
  node [
    id 272
    label "utuczy&#263;"
  ]
  node [
    id 273
    label "ogarn&#261;&#263;"
  ]
  node [
    id 274
    label "obni&#380;y&#263;_si&#281;"
  ]
  node [
    id 275
    label "zmniejszy&#263;_si&#281;"
  ]
  node [
    id 276
    label "otoczy&#263;"
  ]
  node [
    id 277
    label "spa&#347;&#263;"
  ]
  node [
    id 278
    label "dotyka&#263;"
  ]
  node [
    id 279
    label "krzy&#380;"
  ]
  node [
    id 280
    label "r&#281;ka"
  ]
  node [
    id 281
    label "hasta"
  ]
  node [
    id 282
    label "szeroko&#347;&#263;_d&#322;oni"
  ]
  node [
    id 283
    label "linia_rozumu"
  ]
  node [
    id 284
    label "wyklepanie"
  ]
  node [
    id 285
    label "chwyta&#263;"
  ]
  node [
    id 286
    label "dotykanie"
  ]
  node [
    id 287
    label "r&#261;czyna"
  ]
  node [
    id 288
    label "palec"
  ]
  node [
    id 289
    label "graba"
  ]
  node [
    id 290
    label "nadgarstek"
  ]
  node [
    id 291
    label "linia_mi&#322;o&#347;ci"
  ]
  node [
    id 292
    label "linia_&#380;ycia"
  ]
  node [
    id 293
    label "klepanie"
  ]
  node [
    id 294
    label "cmoknonsens"
  ]
  node [
    id 295
    label "chiromancja"
  ]
  node [
    id 296
    label "wyklepa&#263;"
  ]
  node [
    id 297
    label "klepa&#263;"
  ]
  node [
    id 298
    label "chwytanie"
  ]
  node [
    id 299
    label "poduszka"
  ]
  node [
    id 300
    label "asymilowa&#263;"
  ]
  node [
    id 301
    label "wapniak"
  ]
  node [
    id 302
    label "dwun&#243;g"
  ]
  node [
    id 303
    label "polifag"
  ]
  node [
    id 304
    label "wz&#243;r"
  ]
  node [
    id 305
    label "profanum"
  ]
  node [
    id 306
    label "hominid"
  ]
  node [
    id 307
    label "homo_sapiens"
  ]
  node [
    id 308
    label "nasada"
  ]
  node [
    id 309
    label "podw&#322;adny"
  ]
  node [
    id 310
    label "ludzko&#347;&#263;"
  ]
  node [
    id 311
    label "os&#322;abianie"
  ]
  node [
    id 312
    label "mikrokosmos"
  ]
  node [
    id 313
    label "portrecista"
  ]
  node [
    id 314
    label "duch"
  ]
  node [
    id 315
    label "oddzia&#322;ywanie"
  ]
  node [
    id 316
    label "asymilowanie"
  ]
  node [
    id 317
    label "osoba"
  ]
  node [
    id 318
    label "os&#322;abia&#263;"
  ]
  node [
    id 319
    label "figura"
  ]
  node [
    id 320
    label "Adam"
  ]
  node [
    id 321
    label "senior"
  ]
  node [
    id 322
    label "antropochoria"
  ]
  node [
    id 323
    label "posta&#263;"
  ]
  node [
    id 324
    label "okre&#347;lony"
  ]
  node [
    id 325
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 326
    label "my&#347;le&#263;"
  ]
  node [
    id 327
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 328
    label "proceed"
  ]
  node [
    id 329
    label "catch"
  ]
  node [
    id 330
    label "osta&#263;_si&#281;"
  ]
  node [
    id 331
    label "support"
  ]
  node [
    id 332
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 333
    label "prze&#380;y&#263;"
  ]
  node [
    id 334
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 335
    label "kres"
  ]
  node [
    id 336
    label "s&#322;o&#324;ce"
  ]
  node [
    id 337
    label "czynienie_si&#281;"
  ]
  node [
    id 338
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 339
    label "czas"
  ]
  node [
    id 340
    label "long_time"
  ]
  node [
    id 341
    label "przedpo&#322;udnie"
  ]
  node [
    id 342
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 343
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 344
    label "tydzie&#324;"
  ]
  node [
    id 345
    label "godzina"
  ]
  node [
    id 346
    label "t&#322;usty_czwartek"
  ]
  node [
    id 347
    label "wsta&#263;"
  ]
  node [
    id 348
    label "day"
  ]
  node [
    id 349
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 350
    label "przedwiecz&#243;r"
  ]
  node [
    id 351
    label "Sylwester"
  ]
  node [
    id 352
    label "po&#322;udnie"
  ]
  node [
    id 353
    label "wzej&#347;cie"
  ]
  node [
    id 354
    label "podwiecz&#243;r"
  ]
  node [
    id 355
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 356
    label "rano"
  ]
  node [
    id 357
    label "termin"
  ]
  node [
    id 358
    label "ranek"
  ]
  node [
    id 359
    label "doba"
  ]
  node [
    id 360
    label "wiecz&#243;r"
  ]
  node [
    id 361
    label "walentynki"
  ]
  node [
    id 362
    label "popo&#322;udnie"
  ]
  node [
    id 363
    label "noc"
  ]
  node [
    id 364
    label "wstanie"
  ]
  node [
    id 365
    label "ki&#347;&#263;"
  ]
  node [
    id 366
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 367
    label "krzew"
  ]
  node [
    id 368
    label "pi&#380;maczkowate"
  ]
  node [
    id 369
    label "pestkowiec"
  ]
  node [
    id 370
    label "owoc"
  ]
  node [
    id 371
    label "oliwkowate"
  ]
  node [
    id 372
    label "ro&#347;lina"
  ]
  node [
    id 373
    label "hy&#263;ka"
  ]
  node [
    id 374
    label "lilac"
  ]
  node [
    id 375
    label "delfinidyna"
  ]
  node [
    id 376
    label "manewr"
  ]
  node [
    id 377
    label "model"
  ]
  node [
    id 378
    label "movement"
  ]
  node [
    id 379
    label "apraksja"
  ]
  node [
    id 380
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 381
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 382
    label "poruszenie"
  ]
  node [
    id 383
    label "commercial_enterprise"
  ]
  node [
    id 384
    label "dyssypacja_energii"
  ]
  node [
    id 385
    label "zmiana"
  ]
  node [
    id 386
    label "utrzymanie"
  ]
  node [
    id 387
    label "utrzyma&#263;"
  ]
  node [
    id 388
    label "komunikacja"
  ]
  node [
    id 389
    label "tumult"
  ]
  node [
    id 390
    label "kr&#243;tki"
  ]
  node [
    id 391
    label "drift"
  ]
  node [
    id 392
    label "utrzymywa&#263;"
  ]
  node [
    id 393
    label "stopek"
  ]
  node [
    id 394
    label "kanciasty"
  ]
  node [
    id 395
    label "zjawisko"
  ]
  node [
    id 396
    label "utrzymywanie"
  ]
  node [
    id 397
    label "czynno&#347;&#263;"
  ]
  node [
    id 398
    label "myk"
  ]
  node [
    id 399
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 400
    label "wydarzenie"
  ]
  node [
    id 401
    label "taktyka"
  ]
  node [
    id 402
    label "move"
  ]
  node [
    id 403
    label "natural_process"
  ]
  node [
    id 404
    label "lokomocja"
  ]
  node [
    id 405
    label "mechanika"
  ]
  node [
    id 406
    label "proces"
  ]
  node [
    id 407
    label "strumie&#324;"
  ]
  node [
    id 408
    label "aktywno&#347;&#263;"
  ]
  node [
    id 409
    label "travel"
  ]
  node [
    id 410
    label "vibration"
  ]
  node [
    id 411
    label "wiggle"
  ]
  node [
    id 412
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 413
    label "poruszenie_si&#281;"
  ]
  node [
    id 414
    label "garderoba"
  ]
  node [
    id 415
    label "wiecha"
  ]
  node [
    id 416
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 417
    label "grupa"
  ]
  node [
    id 418
    label "budynek"
  ]
  node [
    id 419
    label "fratria"
  ]
  node [
    id 420
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 421
    label "poj&#281;cie"
  ]
  node [
    id 422
    label "rodzina"
  ]
  node [
    id 423
    label "substancja_mieszkaniowa"
  ]
  node [
    id 424
    label "dom_rodzinny"
  ]
  node [
    id 425
    label "stead"
  ]
  node [
    id 426
    label "siedziba"
  ]
  node [
    id 427
    label "swar"
  ]
  node [
    id 428
    label "busyness"
  ]
  node [
    id 429
    label "ha&#322;as"
  ]
  node [
    id 430
    label "noise"
  ]
  node [
    id 431
    label "cichn&#261;&#263;"
  ]
  node [
    id 432
    label "minuta"
  ]
  node [
    id 433
    label "forma"
  ]
  node [
    id 434
    label "znaczenie"
  ]
  node [
    id 435
    label "kategoria_gramatyczna"
  ]
  node [
    id 436
    label "przys&#322;&#243;wek"
  ]
  node [
    id 437
    label "szczebel"
  ]
  node [
    id 438
    label "element"
  ]
  node [
    id 439
    label "poziom"
  ]
  node [
    id 440
    label "degree"
  ]
  node [
    id 441
    label "podn&#243;&#380;ek"
  ]
  node [
    id 442
    label "rank"
  ]
  node [
    id 443
    label "przymiotnik"
  ]
  node [
    id 444
    label "podzia&#322;"
  ]
  node [
    id 445
    label "ocena"
  ]
  node [
    id 446
    label "kszta&#322;t"
  ]
  node [
    id 447
    label "wschodek"
  ]
  node [
    id 448
    label "schody"
  ]
  node [
    id 449
    label "gama"
  ]
  node [
    id 450
    label "podstopie&#324;"
  ]
  node [
    id 451
    label "d&#378;wi&#281;k"
  ]
  node [
    id 452
    label "wielko&#347;&#263;"
  ]
  node [
    id 453
    label "jednostka"
  ]
  node [
    id 454
    label "popada&#263;"
  ]
  node [
    id 455
    label "ukrywa&#263;_si&#281;"
  ]
  node [
    id 456
    label "transgress"
  ]
  node [
    id 457
    label "fall"
  ]
  node [
    id 458
    label "opada&#263;"
  ]
  node [
    id 459
    label "pogr&#261;&#380;a&#263;_si&#281;"
  ]
  node [
    id 460
    label "flood"
  ]
  node [
    id 461
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 462
    label "zasypa&#263;"
  ]
  node [
    id 463
    label "shed"
  ]
  node [
    id 464
    label "break"
  ]
  node [
    id 465
    label "nabawia&#263;_si&#281;"
  ]
  node [
    id 466
    label "flatten"
  ]
  node [
    id 467
    label "nast&#281;powa&#263;"
  ]
  node [
    id 468
    label "utrwala&#263;_si&#281;"
  ]
  node [
    id 469
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 470
    label "zach&#243;d"
  ]
  node [
    id 471
    label "pora"
  ]
  node [
    id 472
    label "free"
  ]
  node [
    id 473
    label "si&#281;ga&#263;"
  ]
  node [
    id 474
    label "obecno&#347;&#263;"
  ]
  node [
    id 475
    label "stan"
  ]
  node [
    id 476
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 477
    label "stand"
  ]
  node [
    id 478
    label "mie&#263;_miejsce"
  ]
  node [
    id 479
    label "uczestniczy&#263;"
  ]
  node [
    id 480
    label "chodzi&#263;"
  ]
  node [
    id 481
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 482
    label "equal"
  ]
  node [
    id 483
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 484
    label "perceive"
  ]
  node [
    id 485
    label "reagowa&#263;"
  ]
  node [
    id 486
    label "spowodowa&#263;"
  ]
  node [
    id 487
    label "male&#263;"
  ]
  node [
    id 488
    label "zmale&#263;"
  ]
  node [
    id 489
    label "spotka&#263;"
  ]
  node [
    id 490
    label "dostrzega&#263;"
  ]
  node [
    id 491
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 492
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 493
    label "ogl&#261;da&#263;"
  ]
  node [
    id 494
    label "aprobowa&#263;"
  ]
  node [
    id 495
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 496
    label "postrzega&#263;"
  ]
  node [
    id 497
    label "notice"
  ]
  node [
    id 498
    label "przekwitanie"
  ]
  node [
    id 499
    label "m&#281;&#380;yna"
  ]
  node [
    id 500
    label "babka"
  ]
  node [
    id 501
    label "samica"
  ]
  node [
    id 502
    label "doros&#322;y"
  ]
  node [
    id 503
    label "ulec"
  ]
  node [
    id 504
    label "uleganie"
  ]
  node [
    id 505
    label "partnerka"
  ]
  node [
    id 506
    label "&#380;ona"
  ]
  node [
    id 507
    label "ulega&#263;"
  ]
  node [
    id 508
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 509
    label "pa&#324;stwo"
  ]
  node [
    id 510
    label "ulegni&#281;cie"
  ]
  node [
    id 511
    label "menopauza"
  ]
  node [
    id 512
    label "&#322;ono"
  ]
  node [
    id 513
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 514
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 515
    label "organizacja"
  ]
  node [
    id 516
    label "Eleusis"
  ]
  node [
    id 517
    label "asystencja"
  ]
  node [
    id 518
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 519
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 520
    label "fabianie"
  ]
  node [
    id 521
    label "Chewra_Kadisza"
  ]
  node [
    id 522
    label "grono"
  ]
  node [
    id 523
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 524
    label "wi&#281;&#378;"
  ]
  node [
    id 525
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 526
    label "partnership"
  ]
  node [
    id 527
    label "Monar"
  ]
  node [
    id 528
    label "Rotary_International"
  ]
  node [
    id 529
    label "potomstwo"
  ]
  node [
    id 530
    label "organizm"
  ]
  node [
    id 531
    label "m&#322;odziak"
  ]
  node [
    id 532
    label "fledgling"
  ]
  node [
    id 533
    label "ch&#322;opina"
  ]
  node [
    id 534
    label "jegomo&#347;&#263;"
  ]
  node [
    id 535
    label "bratek"
  ]
  node [
    id 536
    label "samiec"
  ]
  node [
    id 537
    label "ojciec"
  ]
  node [
    id 538
    label "twardziel"
  ]
  node [
    id 539
    label "androlog"
  ]
  node [
    id 540
    label "m&#261;&#380;"
  ]
  node [
    id 541
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 542
    label "andropauza"
  ]
  node [
    id 543
    label "pies_ozdobny"
  ]
  node [
    id 544
    label "pies_pokojowy"
  ]
  node [
    id 545
    label "pies_do_towarzystwa"
  ]
  node [
    id 546
    label "poodle"
  ]
  node [
    id 547
    label "omija&#263;"
  ]
  node [
    id 548
    label "pozostawia&#263;"
  ]
  node [
    id 549
    label "potania&#263;"
  ]
  node [
    id 550
    label "przestawa&#263;"
  ]
  node [
    id 551
    label "abort"
  ]
  node [
    id 552
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 553
    label "give"
  ]
  node [
    id 554
    label "traci&#263;"
  ]
  node [
    id 555
    label "forowa&#263;_si&#281;"
  ]
  node [
    id 556
    label "&#347;rodowisko"
  ]
  node [
    id 557
    label "miasteczko"
  ]
  node [
    id 558
    label "streetball"
  ]
  node [
    id 559
    label "pierzeja"
  ]
  node [
    id 560
    label "pas_ruchu"
  ]
  node [
    id 561
    label "pas_rozdzielczy"
  ]
  node [
    id 562
    label "jezdnia"
  ]
  node [
    id 563
    label "droga"
  ]
  node [
    id 564
    label "korona_drogi"
  ]
  node [
    id 565
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 566
    label "chodnik"
  ]
  node [
    id 567
    label "arteria"
  ]
  node [
    id 568
    label "Broadway"
  ]
  node [
    id 569
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 570
    label "wysepka"
  ]
  node [
    id 571
    label "autostrada"
  ]
  node [
    id 572
    label "w_chuj"
  ]
  node [
    id 573
    label "marny"
  ]
  node [
    id 574
    label "s&#322;abowity"
  ]
  node [
    id 575
    label "n&#281;dznie"
  ]
  node [
    id 576
    label "sm&#281;tny"
  ]
  node [
    id 577
    label "biedny"
  ]
  node [
    id 578
    label "mizernie"
  ]
  node [
    id 579
    label "niezdrowy"
  ]
  node [
    id 580
    label "wyczy&#347;ci&#263;"
  ]
  node [
    id 581
    label "mule"
  ]
  node [
    id 582
    label "zniszczy&#263;"
  ]
  node [
    id 583
    label "osuszy&#263;"
  ]
  node [
    id 584
    label "pout"
  ]
  node [
    id 585
    label "hide"
  ]
  node [
    id 586
    label "ow&#322;osienie_&#322;onowe"
  ]
  node [
    id 587
    label "zakomunikowa&#263;"
  ]
  node [
    id 588
    label "przeznaczy&#263;"
  ]
  node [
    id 589
    label "disapprove"
  ]
  node [
    id 590
    label "throw"
  ]
  node [
    id 591
    label "umie&#347;ci&#263;"
  ]
  node [
    id 592
    label "odej&#347;&#263;"
  ]
  node [
    id 593
    label "project"
  ]
  node [
    id 594
    label "du&#380;y"
  ]
  node [
    id 595
    label "lu&#378;no"
  ]
  node [
    id 596
    label "rozdeptanie"
  ]
  node [
    id 597
    label "rozlegle"
  ]
  node [
    id 598
    label "rozleg&#322;y"
  ]
  node [
    id 599
    label "szeroko"
  ]
  node [
    id 600
    label "rozdeptywanie"
  ]
  node [
    id 601
    label "lata&#263;"
  ]
  node [
    id 602
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 603
    label "fly"
  ]
  node [
    id 604
    label "powiewa&#263;"
  ]
  node [
    id 605
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 606
    label "lecie&#263;"
  ]
  node [
    id 607
    label "uwija&#263;_si&#281;"
  ]
  node [
    id 608
    label "szale&#263;"
  ]
  node [
    id 609
    label "biec"
  ]
  node [
    id 610
    label "frun&#261;&#263;"
  ]
  node [
    id 611
    label "plisa"
  ]
  node [
    id 612
    label "przedmiot"
  ]
  node [
    id 613
    label "tren"
  ]
  node [
    id 614
    label "matka"
  ]
  node [
    id 615
    label "starzy"
  ]
  node [
    id 616
    label "damper"
  ]
  node [
    id 617
    label "dodatek"
  ]
  node [
    id 618
    label "przysparza&#263;"
  ]
  node [
    id 619
    label "supernatural"
  ]
  node [
    id 620
    label "cover"
  ]
  node [
    id 621
    label "przykrywa&#263;"
  ]
  node [
    id 622
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 623
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 624
    label "ucho"
  ]
  node [
    id 625
    label "makrocefalia"
  ]
  node [
    id 626
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 627
    label "m&#243;zg"
  ]
  node [
    id 628
    label "kierownictwo"
  ]
  node [
    id 629
    label "czaszka"
  ]
  node [
    id 630
    label "dekiel"
  ]
  node [
    id 631
    label "umys&#322;"
  ]
  node [
    id 632
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 633
    label "&#347;ci&#281;cie"
  ]
  node [
    id 634
    label "sztuka"
  ]
  node [
    id 635
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 636
    label "g&#243;ra"
  ]
  node [
    id 637
    label "byd&#322;o"
  ]
  node [
    id 638
    label "alkohol"
  ]
  node [
    id 639
    label "wiedza"
  ]
  node [
    id 640
    label "&#347;ci&#281;gno"
  ]
  node [
    id 641
    label "&#380;ycie"
  ]
  node [
    id 642
    label "pryncypa&#322;"
  ]
  node [
    id 643
    label "fryzura"
  ]
  node [
    id 644
    label "noosfera"
  ]
  node [
    id 645
    label "kierowa&#263;"
  ]
  node [
    id 646
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 647
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 648
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 649
    label "cecha"
  ]
  node [
    id 650
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 651
    label "zdolno&#347;&#263;"
  ]
  node [
    id 652
    label "cz&#322;onek"
  ]
  node [
    id 653
    label "cia&#322;o"
  ]
  node [
    id 654
    label "obiekt"
  ]
  node [
    id 655
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 656
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 657
    label "tune"
  ]
  node [
    id 658
    label "wyregulowa&#263;"
  ]
  node [
    id 659
    label "wanadowiec"
  ]
  node [
    id 660
    label "coil"
  ]
  node [
    id 661
    label "pasemko"
  ]
  node [
    id 662
    label "ozdoba"
  ]
  node [
    id 663
    label "bow"
  ]
  node [
    id 664
    label "specjalny"
  ]
  node [
    id 665
    label "analogowy"
  ]
  node [
    id 666
    label "nierealistyczny"
  ]
  node [
    id 667
    label "papierowo"
  ]
  node [
    id 668
    label "w&#261;tpliwy"
  ]
  node [
    id 669
    label "podobny"
  ]
  node [
    id 670
    label "martwy"
  ]
  node [
    id 671
    label "flakon"
  ]
  node [
    id 672
    label "dno_kwiatowe"
  ]
  node [
    id 673
    label "organ_ro&#347;linny"
  ]
  node [
    id 674
    label "kielich"
  ]
  node [
    id 675
    label "korona"
  ]
  node [
    id 676
    label "ogon"
  ]
  node [
    id 677
    label "przykoronek"
  ]
  node [
    id 678
    label "rurka"
  ]
  node [
    id 679
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 680
    label "zrzucenie"
  ]
  node [
    id 681
    label "odziewek"
  ]
  node [
    id 682
    label "struktura"
  ]
  node [
    id 683
    label "kr&#243;j"
  ]
  node [
    id 684
    label "pasmanteria"
  ]
  node [
    id 685
    label "pochodzi&#263;"
  ]
  node [
    id 686
    label "ubranie_si&#281;"
  ]
  node [
    id 687
    label "wyko&#324;czenie"
  ]
  node [
    id 688
    label "znosi&#263;"
  ]
  node [
    id 689
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 690
    label "znoszenie"
  ]
  node [
    id 691
    label "w&#322;o&#380;enie"
  ]
  node [
    id 692
    label "pochodzenie"
  ]
  node [
    id 693
    label "nosi&#263;"
  ]
  node [
    id 694
    label "odzie&#380;"
  ]
  node [
    id 695
    label "gorset"
  ]
  node [
    id 696
    label "zrzuci&#263;"
  ]
  node [
    id 697
    label "zasada"
  ]
  node [
    id 698
    label "wk&#322;ada&#263;"
  ]
  node [
    id 699
    label "czu&#263;"
  ]
  node [
    id 700
    label "need"
  ]
  node [
    id 701
    label "zagrzanie"
  ]
  node [
    id 702
    label "przyjemny"
  ]
  node [
    id 703
    label "ocieplenie"
  ]
  node [
    id 704
    label "korzystny"
  ]
  node [
    id 705
    label "ocieplanie_si&#281;"
  ]
  node [
    id 706
    label "grzanie"
  ]
  node [
    id 707
    label "dobry"
  ]
  node [
    id 708
    label "ocieplenie_si&#281;"
  ]
  node [
    id 709
    label "ocieplanie"
  ]
  node [
    id 710
    label "mi&#322;y"
  ]
  node [
    id 711
    label "ciep&#322;o"
  ]
  node [
    id 712
    label "surowiec"
  ]
  node [
    id 713
    label "okrycie"
  ]
  node [
    id 714
    label "sier&#347;&#263;"
  ]
  node [
    id 715
    label "haircloth"
  ]
  node [
    id 716
    label "wida&#263;"
  ]
  node [
    id 717
    label "rozja&#347;nia&#263;_si&#281;"
  ]
  node [
    id 718
    label "dawn"
  ]
  node [
    id 719
    label "czeka&#263;"
  ]
  node [
    id 720
    label "lookout"
  ]
  node [
    id 721
    label "wyziera&#263;"
  ]
  node [
    id 722
    label "peep"
  ]
  node [
    id 723
    label "zobo"
  ]
  node [
    id 724
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 725
    label "yakalo"
  ]
  node [
    id 726
    label "dzo"
  ]
  node [
    id 727
    label "nar&#281;czak"
  ]
  node [
    id 728
    label "bro&#324;_ochronna"
  ]
  node [
    id 729
    label "os&#322;ona_cia&#322;a"
  ]
  node [
    id 730
    label "zar&#281;kawie"
  ]
  node [
    id 731
    label "taszka"
  ]
  node [
    id 732
    label "ochrona"
  ]
  node [
    id 733
    label "plastron"
  ]
  node [
    id 734
    label "osk&#243;rek"
  ]
  node [
    id 735
    label "kasak"
  ]
  node [
    id 736
    label "naplecznik"
  ]
  node [
    id 737
    label "karwasz"
  ]
  node [
    id 738
    label "wstydliwo&#347;&#263;"
  ]
  node [
    id 739
    label "nabiodrnik"
  ]
  node [
    id 740
    label "ubranie_ochronne"
  ]
  node [
    id 741
    label "napier&#347;nik"
  ]
  node [
    id 742
    label "zbroica"
  ]
  node [
    id 743
    label "naramiennik"
  ]
  node [
    id 744
    label "skrzyd&#322;o"
  ]
  node [
    id 745
    label "os&#322;ona"
  ]
  node [
    id 746
    label "shell"
  ]
  node [
    id 747
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 748
    label "nabiodrek"
  ]
  node [
    id 749
    label "obojczyk"
  ]
  node [
    id 750
    label "nagolennik"
  ]
  node [
    id 751
    label "hull"
  ]
  node [
    id 752
    label "p&#322;atnerz"
  ]
  node [
    id 753
    label "g&#322;adki"
  ]
  node [
    id 754
    label "subtelny"
  ]
  node [
    id 755
    label "at&#322;asowo"
  ]
  node [
    id 756
    label "tkaninowy"
  ]
  node [
    id 757
    label "delikatny"
  ]
  node [
    id 758
    label "brafitting"
  ]
  node [
    id 759
    label "miseczka"
  ]
  node [
    id 760
    label "brafitterka"
  ]
  node [
    id 761
    label "kamizelka"
  ]
  node [
    id 762
    label "bielizna"
  ]
  node [
    id 763
    label "r&#243;g"
  ]
  node [
    id 764
    label "knob"
  ]
  node [
    id 765
    label "guzikarz"
  ]
  node [
    id 766
    label "filobutonista"
  ]
  node [
    id 767
    label "przycisk"
  ]
  node [
    id 768
    label "filobutonistyka"
  ]
  node [
    id 769
    label "zapi&#281;cie"
  ]
  node [
    id 770
    label "zero"
  ]
  node [
    id 771
    label "rudawo"
  ]
  node [
    id 772
    label "pomara&#324;czowawy"
  ]
  node [
    id 773
    label "cebulka"
  ]
  node [
    id 774
    label "wytw&#243;r"
  ]
  node [
    id 775
    label "powierzchnia"
  ]
  node [
    id 776
    label "mieszek_w&#322;osowy"
  ]
  node [
    id 777
    label "tkanina"
  ]
  node [
    id 778
    label "uczesa&#263;"
  ]
  node [
    id 779
    label "sp&#322;aszczanie"
  ]
  node [
    id 780
    label "poziomy"
  ]
  node [
    id 781
    label "trywialny"
  ]
  node [
    id 782
    label "kiepski"
  ]
  node [
    id 783
    label "p&#322;asko"
  ]
  node [
    id 784
    label "r&#243;wny"
  ]
  node [
    id 785
    label "pospolity"
  ]
  node [
    id 786
    label "sp&#322;aszczenie"
  ]
  node [
    id 787
    label "jednowymiarowy"
  ]
  node [
    id 788
    label "streak"
  ]
  node [
    id 789
    label "przebieg"
  ]
  node [
    id 790
    label "strip"
  ]
  node [
    id 791
    label "swath"
  ]
  node [
    id 792
    label "kana&#322;"
  ]
  node [
    id 793
    label "pas"
  ]
  node [
    id 794
    label "splot"
  ]
  node [
    id 795
    label "uczesanie"
  ]
  node [
    id 796
    label "cha&#322;ka"
  ]
  node [
    id 797
    label "splata&#263;"
  ]
  node [
    id 798
    label "tail"
  ]
  node [
    id 799
    label "obrz&#281;d"
  ]
  node [
    id 800
    label "report"
  ]
  node [
    id 801
    label "cap"
  ]
  node [
    id 802
    label "zakry&#263;"
  ]
  node [
    id 803
    label "dupny"
  ]
  node [
    id 804
    label "wysoce"
  ]
  node [
    id 805
    label "wyj&#261;tkowy"
  ]
  node [
    id 806
    label "wybitny"
  ]
  node [
    id 807
    label "znaczny"
  ]
  node [
    id 808
    label "prawdziwy"
  ]
  node [
    id 809
    label "nieprzeci&#281;tny"
  ]
  node [
    id 810
    label "nudny"
  ]
  node [
    id 811
    label "zniszczony"
  ]
  node [
    id 812
    label "blakni&#281;cie"
  ]
  node [
    id 813
    label "wyburza&#322;y"
  ]
  node [
    id 814
    label "zblakni&#281;cie"
  ]
  node [
    id 815
    label "wyblak&#322;y"
  ]
  node [
    id 816
    label "leniwy"
  ]
  node [
    id 817
    label "odpowiedni"
  ]
  node [
    id 818
    label "wygodnie"
  ]
  node [
    id 819
    label "dogodnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 200
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 206
  ]
  edge [
    source 13
    target 207
  ]
  edge [
    source 13
    target 208
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 13
    target 210
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 212
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 214
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 215
  ]
  edge [
    source 14
    target 216
  ]
  edge [
    source 14
    target 217
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 15
    target 220
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 229
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 230
  ]
  edge [
    source 18
    target 231
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 18
    target 233
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 18
    target 235
  ]
  edge [
    source 18
    target 236
  ]
  edge [
    source 18
    target 237
  ]
  edge [
    source 18
    target 238
  ]
  edge [
    source 18
    target 239
  ]
  edge [
    source 18
    target 240
  ]
  edge [
    source 18
    target 241
  ]
  edge [
    source 18
    target 242
  ]
  edge [
    source 18
    target 243
  ]
  edge [
    source 18
    target 64
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 244
  ]
  edge [
    source 19
    target 245
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 82
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 20
    target 249
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 250
  ]
  edge [
    source 21
    target 251
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 252
  ]
  edge [
    source 22
    target 175
  ]
  edge [
    source 22
    target 253
  ]
  edge [
    source 22
    target 254
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 256
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 257
  ]
  edge [
    source 25
    target 258
  ]
  edge [
    source 25
    target 259
  ]
  edge [
    source 25
    target 260
  ]
  edge [
    source 25
    target 261
  ]
  edge [
    source 25
    target 262
  ]
  edge [
    source 25
    target 263
  ]
  edge [
    source 25
    target 264
  ]
  edge [
    source 25
    target 265
  ]
  edge [
    source 25
    target 266
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 267
  ]
  edge [
    source 26
    target 268
  ]
  edge [
    source 26
    target 122
  ]
  edge [
    source 26
    target 123
  ]
  edge [
    source 26
    target 269
  ]
  edge [
    source 26
    target 270
  ]
  edge [
    source 26
    target 271
  ]
  edge [
    source 26
    target 125
  ]
  edge [
    source 26
    target 272
  ]
  edge [
    source 26
    target 273
  ]
  edge [
    source 26
    target 274
  ]
  edge [
    source 26
    target 275
  ]
  edge [
    source 26
    target 276
  ]
  edge [
    source 26
    target 129
  ]
  edge [
    source 26
    target 277
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 278
  ]
  edge [
    source 27
    target 279
  ]
  edge [
    source 27
    target 280
  ]
  edge [
    source 27
    target 281
  ]
  edge [
    source 27
    target 282
  ]
  edge [
    source 27
    target 283
  ]
  edge [
    source 27
    target 284
  ]
  edge [
    source 27
    target 285
  ]
  edge [
    source 27
    target 286
  ]
  edge [
    source 27
    target 287
  ]
  edge [
    source 27
    target 288
  ]
  edge [
    source 27
    target 289
  ]
  edge [
    source 27
    target 290
  ]
  edge [
    source 27
    target 291
  ]
  edge [
    source 27
    target 292
  ]
  edge [
    source 27
    target 293
  ]
  edge [
    source 27
    target 294
  ]
  edge [
    source 27
    target 295
  ]
  edge [
    source 27
    target 296
  ]
  edge [
    source 27
    target 297
  ]
  edge [
    source 27
    target 298
  ]
  edge [
    source 27
    target 299
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 300
  ]
  edge [
    source 28
    target 301
  ]
  edge [
    source 28
    target 302
  ]
  edge [
    source 28
    target 303
  ]
  edge [
    source 28
    target 304
  ]
  edge [
    source 28
    target 305
  ]
  edge [
    source 28
    target 306
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 28
    target 308
  ]
  edge [
    source 28
    target 309
  ]
  edge [
    source 28
    target 310
  ]
  edge [
    source 28
    target 311
  ]
  edge [
    source 28
    target 312
  ]
  edge [
    source 28
    target 313
  ]
  edge [
    source 28
    target 314
  ]
  edge [
    source 28
    target 315
  ]
  edge [
    source 28
    target 76
  ]
  edge [
    source 28
    target 316
  ]
  edge [
    source 28
    target 317
  ]
  edge [
    source 28
    target 318
  ]
  edge [
    source 28
    target 319
  ]
  edge [
    source 28
    target 320
  ]
  edge [
    source 28
    target 321
  ]
  edge [
    source 28
    target 322
  ]
  edge [
    source 28
    target 323
  ]
  edge [
    source 28
    target 53
  ]
  edge [
    source 28
    target 56
  ]
  edge [
    source 28
    target 57
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 83
  ]
  edge [
    source 29
    target 84
  ]
  edge [
    source 29
    target 324
  ]
  edge [
    source 29
    target 325
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 327
  ]
  edge [
    source 34
    target 328
  ]
  edge [
    source 34
    target 329
  ]
  edge [
    source 34
    target 330
  ]
  edge [
    source 34
    target 331
  ]
  edge [
    source 34
    target 332
  ]
  edge [
    source 34
    target 333
  ]
  edge [
    source 34
    target 334
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 335
  ]
  edge [
    source 37
    target 76
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 336
  ]
  edge [
    source 38
    target 337
  ]
  edge [
    source 38
    target 338
  ]
  edge [
    source 38
    target 339
  ]
  edge [
    source 38
    target 340
  ]
  edge [
    source 38
    target 341
  ]
  edge [
    source 38
    target 342
  ]
  edge [
    source 38
    target 343
  ]
  edge [
    source 38
    target 344
  ]
  edge [
    source 38
    target 345
  ]
  edge [
    source 38
    target 346
  ]
  edge [
    source 38
    target 347
  ]
  edge [
    source 38
    target 348
  ]
  edge [
    source 38
    target 349
  ]
  edge [
    source 38
    target 350
  ]
  edge [
    source 38
    target 351
  ]
  edge [
    source 38
    target 352
  ]
  edge [
    source 38
    target 353
  ]
  edge [
    source 38
    target 354
  ]
  edge [
    source 38
    target 355
  ]
  edge [
    source 38
    target 356
  ]
  edge [
    source 38
    target 357
  ]
  edge [
    source 38
    target 358
  ]
  edge [
    source 38
    target 359
  ]
  edge [
    source 38
    target 360
  ]
  edge [
    source 38
    target 361
  ]
  edge [
    source 38
    target 362
  ]
  edge [
    source 38
    target 363
  ]
  edge [
    source 38
    target 364
  ]
  edge [
    source 38
    target 48
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 365
  ]
  edge [
    source 39
    target 366
  ]
  edge [
    source 39
    target 367
  ]
  edge [
    source 39
    target 368
  ]
  edge [
    source 39
    target 369
  ]
  edge [
    source 39
    target 82
  ]
  edge [
    source 39
    target 370
  ]
  edge [
    source 39
    target 371
  ]
  edge [
    source 39
    target 372
  ]
  edge [
    source 39
    target 373
  ]
  edge [
    source 39
    target 374
  ]
  edge [
    source 39
    target 375
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 376
  ]
  edge [
    source 40
    target 377
  ]
  edge [
    source 40
    target 378
  ]
  edge [
    source 40
    target 379
  ]
  edge [
    source 40
    target 380
  ]
  edge [
    source 40
    target 381
  ]
  edge [
    source 40
    target 382
  ]
  edge [
    source 40
    target 383
  ]
  edge [
    source 40
    target 384
  ]
  edge [
    source 40
    target 385
  ]
  edge [
    source 40
    target 386
  ]
  edge [
    source 40
    target 387
  ]
  edge [
    source 40
    target 388
  ]
  edge [
    source 40
    target 389
  ]
  edge [
    source 40
    target 390
  ]
  edge [
    source 40
    target 391
  ]
  edge [
    source 40
    target 392
  ]
  edge [
    source 40
    target 393
  ]
  edge [
    source 40
    target 394
  ]
  edge [
    source 40
    target 142
  ]
  edge [
    source 40
    target 395
  ]
  edge [
    source 40
    target 396
  ]
  edge [
    source 40
    target 397
  ]
  edge [
    source 40
    target 398
  ]
  edge [
    source 40
    target 399
  ]
  edge [
    source 40
    target 400
  ]
  edge [
    source 40
    target 401
  ]
  edge [
    source 40
    target 402
  ]
  edge [
    source 40
    target 403
  ]
  edge [
    source 40
    target 404
  ]
  edge [
    source 40
    target 405
  ]
  edge [
    source 40
    target 406
  ]
  edge [
    source 40
    target 407
  ]
  edge [
    source 40
    target 408
  ]
  edge [
    source 40
    target 409
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 215
  ]
  edge [
    source 41
    target 410
  ]
  edge [
    source 41
    target 411
  ]
  edge [
    source 41
    target 412
  ]
  edge [
    source 41
    target 413
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 60
  ]
  edge [
    source 42
    target 61
  ]
  edge [
    source 42
    target 414
  ]
  edge [
    source 42
    target 415
  ]
  edge [
    source 42
    target 416
  ]
  edge [
    source 42
    target 417
  ]
  edge [
    source 42
    target 418
  ]
  edge [
    source 42
    target 419
  ]
  edge [
    source 42
    target 420
  ]
  edge [
    source 42
    target 421
  ]
  edge [
    source 42
    target 422
  ]
  edge [
    source 42
    target 423
  ]
  edge [
    source 42
    target 138
  ]
  edge [
    source 42
    target 424
  ]
  edge [
    source 42
    target 425
  ]
  edge [
    source 42
    target 426
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 427
  ]
  edge [
    source 43
    target 428
  ]
  edge [
    source 43
    target 429
  ]
  edge [
    source 43
    target 430
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 431
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 432
  ]
  edge [
    source 45
    target 433
  ]
  edge [
    source 45
    target 434
  ]
  edge [
    source 45
    target 435
  ]
  edge [
    source 45
    target 436
  ]
  edge [
    source 45
    target 437
  ]
  edge [
    source 45
    target 438
  ]
  edge [
    source 45
    target 439
  ]
  edge [
    source 45
    target 440
  ]
  edge [
    source 45
    target 441
  ]
  edge [
    source 45
    target 442
  ]
  edge [
    source 45
    target 443
  ]
  edge [
    source 45
    target 444
  ]
  edge [
    source 45
    target 445
  ]
  edge [
    source 45
    target 446
  ]
  edge [
    source 45
    target 447
  ]
  edge [
    source 45
    target 204
  ]
  edge [
    source 45
    target 448
  ]
  edge [
    source 45
    target 449
  ]
  edge [
    source 45
    target 450
  ]
  edge [
    source 45
    target 451
  ]
  edge [
    source 45
    target 452
  ]
  edge [
    source 45
    target 453
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 454
  ]
  edge [
    source 47
    target 455
  ]
  edge [
    source 47
    target 456
  ]
  edge [
    source 47
    target 457
  ]
  edge [
    source 47
    target 458
  ]
  edge [
    source 47
    target 459
  ]
  edge [
    source 47
    target 460
  ]
  edge [
    source 47
    target 461
  ]
  edge [
    source 47
    target 462
  ]
  edge [
    source 47
    target 463
  ]
  edge [
    source 47
    target 464
  ]
  edge [
    source 47
    target 465
  ]
  edge [
    source 47
    target 466
  ]
  edge [
    source 47
    target 467
  ]
  edge [
    source 47
    target 468
  ]
  edge [
    source 47
    target 469
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 470
  ]
  edge [
    source 48
    target 471
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 472
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 473
  ]
  edge [
    source 50
    target 143
  ]
  edge [
    source 50
    target 474
  ]
  edge [
    source 50
    target 475
  ]
  edge [
    source 50
    target 476
  ]
  edge [
    source 50
    target 477
  ]
  edge [
    source 50
    target 478
  ]
  edge [
    source 50
    target 479
  ]
  edge [
    source 50
    target 480
  ]
  edge [
    source 50
    target 481
  ]
  edge [
    source 50
    target 482
  ]
  edge [
    source 50
    target 69
  ]
  edge [
    source 50
    target 96
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 483
  ]
  edge [
    source 51
    target 484
  ]
  edge [
    source 51
    target 485
  ]
  edge [
    source 51
    target 486
  ]
  edge [
    source 51
    target 487
  ]
  edge [
    source 51
    target 488
  ]
  edge [
    source 51
    target 489
  ]
  edge [
    source 51
    target 193
  ]
  edge [
    source 51
    target 490
  ]
  edge [
    source 51
    target 491
  ]
  edge [
    source 51
    target 492
  ]
  edge [
    source 51
    target 493
  ]
  edge [
    source 51
    target 196
  ]
  edge [
    source 51
    target 494
  ]
  edge [
    source 51
    target 197
  ]
  edge [
    source 51
    target 495
  ]
  edge [
    source 51
    target 179
  ]
  edge [
    source 51
    target 496
  ]
  edge [
    source 51
    target 497
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 87
  ]
  edge [
    source 53
    target 88
  ]
  edge [
    source 53
    target 498
  ]
  edge [
    source 53
    target 499
  ]
  edge [
    source 53
    target 500
  ]
  edge [
    source 53
    target 501
  ]
  edge [
    source 53
    target 502
  ]
  edge [
    source 53
    target 503
  ]
  edge [
    source 53
    target 504
  ]
  edge [
    source 53
    target 505
  ]
  edge [
    source 53
    target 506
  ]
  edge [
    source 53
    target 507
  ]
  edge [
    source 53
    target 508
  ]
  edge [
    source 53
    target 509
  ]
  edge [
    source 53
    target 510
  ]
  edge [
    source 53
    target 511
  ]
  edge [
    source 53
    target 512
  ]
  edge [
    source 53
    target 73
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 513
  ]
  edge [
    source 54
    target 474
  ]
  edge [
    source 54
    target 514
  ]
  edge [
    source 54
    target 515
  ]
  edge [
    source 54
    target 516
  ]
  edge [
    source 54
    target 517
  ]
  edge [
    source 54
    target 518
  ]
  edge [
    source 54
    target 519
  ]
  edge [
    source 54
    target 417
  ]
  edge [
    source 54
    target 520
  ]
  edge [
    source 54
    target 521
  ]
  edge [
    source 54
    target 522
  ]
  edge [
    source 54
    target 523
  ]
  edge [
    source 54
    target 524
  ]
  edge [
    source 54
    target 525
  ]
  edge [
    source 54
    target 526
  ]
  edge [
    source 54
    target 527
  ]
  edge [
    source 54
    target 528
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 529
  ]
  edge [
    source 55
    target 530
  ]
  edge [
    source 55
    target 531
  ]
  edge [
    source 55
    target 153
  ]
  edge [
    source 55
    target 532
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 533
  ]
  edge [
    source 56
    target 534
  ]
  edge [
    source 56
    target 535
  ]
  edge [
    source 56
    target 502
  ]
  edge [
    source 56
    target 536
  ]
  edge [
    source 56
    target 537
  ]
  edge [
    source 56
    target 538
  ]
  edge [
    source 56
    target 539
  ]
  edge [
    source 56
    target 509
  ]
  edge [
    source 56
    target 540
  ]
  edge [
    source 56
    target 541
  ]
  edge [
    source 56
    target 542
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 76
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 89
  ]
  edge [
    source 58
    target 63
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 543
  ]
  edge [
    source 59
    target 544
  ]
  edge [
    source 59
    target 545
  ]
  edge [
    source 59
    target 546
  ]
  edge [
    source 60
    target 547
  ]
  edge [
    source 60
    target 255
  ]
  edge [
    source 60
    target 548
  ]
  edge [
    source 60
    target 549
  ]
  edge [
    source 60
    target 550
  ]
  edge [
    source 60
    target 551
  ]
  edge [
    source 60
    target 552
  ]
  edge [
    source 60
    target 553
  ]
  edge [
    source 60
    target 554
  ]
  edge [
    source 60
    target 555
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 556
  ]
  edge [
    source 62
    target 557
  ]
  edge [
    source 62
    target 558
  ]
  edge [
    source 62
    target 559
  ]
  edge [
    source 62
    target 417
  ]
  edge [
    source 62
    target 560
  ]
  edge [
    source 62
    target 561
  ]
  edge [
    source 62
    target 562
  ]
  edge [
    source 62
    target 563
  ]
  edge [
    source 62
    target 564
  ]
  edge [
    source 62
    target 565
  ]
  edge [
    source 62
    target 566
  ]
  edge [
    source 62
    target 567
  ]
  edge [
    source 62
    target 568
  ]
  edge [
    source 62
    target 569
  ]
  edge [
    source 62
    target 570
  ]
  edge [
    source 62
    target 571
  ]
  edge [
    source 62
    target 107
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 90
  ]
  edge [
    source 63
    target 572
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 573
  ]
  edge [
    source 64
    target 230
  ]
  edge [
    source 64
    target 574
  ]
  edge [
    source 64
    target 575
  ]
  edge [
    source 64
    target 576
  ]
  edge [
    source 64
    target 237
  ]
  edge [
    source 64
    target 577
  ]
  edge [
    source 64
    target 578
  ]
  edge [
    source 64
    target 579
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 580
  ]
  edge [
    source 65
    target 581
  ]
  edge [
    source 65
    target 582
  ]
  edge [
    source 65
    target 583
  ]
  edge [
    source 65
    target 584
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 585
  ]
  edge [
    source 66
    target 586
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 587
  ]
  edge [
    source 67
    target 588
  ]
  edge [
    source 67
    target 589
  ]
  edge [
    source 67
    target 590
  ]
  edge [
    source 67
    target 591
  ]
  edge [
    source 67
    target 592
  ]
  edge [
    source 67
    target 593
  ]
  edge [
    source 67
    target 105
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 594
  ]
  edge [
    source 68
    target 595
  ]
  edge [
    source 68
    target 596
  ]
  edge [
    source 68
    target 597
  ]
  edge [
    source 68
    target 598
  ]
  edge [
    source 68
    target 599
  ]
  edge [
    source 68
    target 600
  ]
  edge [
    source 68
    target 106
  ]
  edge [
    source 68
    target 108
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 601
  ]
  edge [
    source 69
    target 602
  ]
  edge [
    source 69
    target 603
  ]
  edge [
    source 69
    target 604
  ]
  edge [
    source 69
    target 605
  ]
  edge [
    source 69
    target 606
  ]
  edge [
    source 69
    target 607
  ]
  edge [
    source 69
    target 608
  ]
  edge [
    source 69
    target 609
  ]
  edge [
    source 69
    target 610
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 611
  ]
  edge [
    source 71
    target 612
  ]
  edge [
    source 71
    target 438
  ]
  edge [
    source 71
    target 613
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 614
  ]
  edge [
    source 73
    target 505
  ]
  edge [
    source 73
    target 506
  ]
  edge [
    source 73
    target 615
  ]
  edge [
    source 73
    target 111
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 616
  ]
  edge [
    source 74
    target 617
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 618
  ]
  edge [
    source 75
    target 619
  ]
  edge [
    source 75
    target 620
  ]
  edge [
    source 75
    target 621
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 102
  ]
  edge [
    source 76
    target 103
  ]
  edge [
    source 76
    target 622
  ]
  edge [
    source 76
    target 623
  ]
  edge [
    source 76
    target 624
  ]
  edge [
    source 76
    target 625
  ]
  edge [
    source 76
    target 626
  ]
  edge [
    source 76
    target 627
  ]
  edge [
    source 76
    target 628
  ]
  edge [
    source 76
    target 629
  ]
  edge [
    source 76
    target 630
  ]
  edge [
    source 76
    target 631
  ]
  edge [
    source 76
    target 632
  ]
  edge [
    source 76
    target 633
  ]
  edge [
    source 76
    target 634
  ]
  edge [
    source 76
    target 635
  ]
  edge [
    source 76
    target 636
  ]
  edge [
    source 76
    target 637
  ]
  edge [
    source 76
    target 638
  ]
  edge [
    source 76
    target 639
  ]
  edge [
    source 76
    target 372
  ]
  edge [
    source 76
    target 640
  ]
  edge [
    source 76
    target 641
  ]
  edge [
    source 76
    target 642
  ]
  edge [
    source 76
    target 643
  ]
  edge [
    source 76
    target 644
  ]
  edge [
    source 76
    target 645
  ]
  edge [
    source 76
    target 646
  ]
  edge [
    source 76
    target 647
  ]
  edge [
    source 76
    target 648
  ]
  edge [
    source 76
    target 649
  ]
  edge [
    source 76
    target 650
  ]
  edge [
    source 76
    target 651
  ]
  edge [
    source 76
    target 446
  ]
  edge [
    source 76
    target 652
  ]
  edge [
    source 76
    target 653
  ]
  edge [
    source 76
    target 654
  ]
  edge [
    source 76
    target 655
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 656
  ]
  edge [
    source 77
    target 657
  ]
  edge [
    source 77
    target 658
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 659
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 660
  ]
  edge [
    source 79
    target 661
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 662
  ]
  edge [
    source 80
    target 663
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 664
  ]
  edge [
    source 81
    target 665
  ]
  edge [
    source 81
    target 666
  ]
  edge [
    source 81
    target 667
  ]
  edge [
    source 81
    target 668
  ]
  edge [
    source 81
    target 669
  ]
  edge [
    source 81
    target 670
  ]
  edge [
    source 81
    target 90
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 671
  ]
  edge [
    source 82
    target 672
  ]
  edge [
    source 82
    target 673
  ]
  edge [
    source 82
    target 662
  ]
  edge [
    source 82
    target 372
  ]
  edge [
    source 82
    target 674
  ]
  edge [
    source 82
    target 675
  ]
  edge [
    source 82
    target 676
  ]
  edge [
    source 82
    target 677
  ]
  edge [
    source 82
    target 678
  ]
  edge [
    source 83
    target 679
  ]
  edge [
    source 83
    target 680
  ]
  edge [
    source 83
    target 681
  ]
  edge [
    source 83
    target 414
  ]
  edge [
    source 83
    target 682
  ]
  edge [
    source 83
    target 683
  ]
  edge [
    source 83
    target 684
  ]
  edge [
    source 83
    target 685
  ]
  edge [
    source 83
    target 686
  ]
  edge [
    source 83
    target 687
  ]
  edge [
    source 83
    target 688
  ]
  edge [
    source 83
    target 689
  ]
  edge [
    source 83
    target 690
  ]
  edge [
    source 83
    target 691
  ]
  edge [
    source 83
    target 692
  ]
  edge [
    source 83
    target 693
  ]
  edge [
    source 83
    target 694
  ]
  edge [
    source 83
    target 695
  ]
  edge [
    source 83
    target 696
  ]
  edge [
    source 83
    target 697
  ]
  edge [
    source 83
    target 656
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 698
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 614
  ]
  edge [
    source 88
    target 615
  ]
  edge [
    source 89
    target 476
  ]
  edge [
    source 89
    target 699
  ]
  edge [
    source 89
    target 700
  ]
  edge [
    source 89
    target 585
  ]
  edge [
    source 89
    target 331
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 111
  ]
  edge [
    source 90
    target 112
  ]
  edge [
    source 90
    target 701
  ]
  edge [
    source 90
    target 702
  ]
  edge [
    source 90
    target 703
  ]
  edge [
    source 90
    target 704
  ]
  edge [
    source 90
    target 705
  ]
  edge [
    source 90
    target 706
  ]
  edge [
    source 90
    target 707
  ]
  edge [
    source 90
    target 708
  ]
  edge [
    source 90
    target 709
  ]
  edge [
    source 90
    target 710
  ]
  edge [
    source 90
    target 711
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 712
  ]
  edge [
    source 92
    target 713
  ]
  edge [
    source 92
    target 714
  ]
  edge [
    source 92
    target 715
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 716
  ]
  edge [
    source 95
    target 717
  ]
  edge [
    source 95
    target 718
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 719
  ]
  edge [
    source 96
    target 720
  ]
  edge [
    source 96
    target 721
  ]
  edge [
    source 96
    target 722
  ]
  edge [
    source 96
    target 199
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 637
  ]
  edge [
    source 97
    target 723
  ]
  edge [
    source 97
    target 724
  ]
  edge [
    source 97
    target 725
  ]
  edge [
    source 97
    target 726
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 727
  ]
  edge [
    source 98
    target 728
  ]
  edge [
    source 98
    target 729
  ]
  edge [
    source 98
    target 730
  ]
  edge [
    source 98
    target 731
  ]
  edge [
    source 98
    target 732
  ]
  edge [
    source 98
    target 733
  ]
  edge [
    source 98
    target 734
  ]
  edge [
    source 98
    target 735
  ]
  edge [
    source 98
    target 736
  ]
  edge [
    source 98
    target 737
  ]
  edge [
    source 98
    target 738
  ]
  edge [
    source 98
    target 739
  ]
  edge [
    source 98
    target 740
  ]
  edge [
    source 98
    target 741
  ]
  edge [
    source 98
    target 742
  ]
  edge [
    source 98
    target 743
  ]
  edge [
    source 98
    target 744
  ]
  edge [
    source 98
    target 745
  ]
  edge [
    source 98
    target 746
  ]
  edge [
    source 98
    target 747
  ]
  edge [
    source 98
    target 748
  ]
  edge [
    source 98
    target 749
  ]
  edge [
    source 98
    target 750
  ]
  edge [
    source 98
    target 751
  ]
  edge [
    source 98
    target 752
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 753
  ]
  edge [
    source 99
    target 754
  ]
  edge [
    source 99
    target 755
  ]
  edge [
    source 99
    target 756
  ]
  edge [
    source 99
    target 757
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 758
  ]
  edge [
    source 100
    target 759
  ]
  edge [
    source 100
    target 760
  ]
  edge [
    source 100
    target 761
  ]
  edge [
    source 100
    target 762
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 102
    target 684
  ]
  edge [
    source 102
    target 763
  ]
  edge [
    source 102
    target 764
  ]
  edge [
    source 102
    target 765
  ]
  edge [
    source 102
    target 766
  ]
  edge [
    source 102
    target 767
  ]
  edge [
    source 102
    target 768
  ]
  edge [
    source 102
    target 769
  ]
  edge [
    source 102
    target 770
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 771
  ]
  edge [
    source 103
    target 772
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 773
  ]
  edge [
    source 104
    target 774
  ]
  edge [
    source 104
    target 775
  ]
  edge [
    source 104
    target 776
  ]
  edge [
    source 104
    target 777
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 778
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 230
  ]
  edge [
    source 106
    target 779
  ]
  edge [
    source 106
    target 780
  ]
  edge [
    source 106
    target 781
  ]
  edge [
    source 106
    target 261
  ]
  edge [
    source 106
    target 782
  ]
  edge [
    source 106
    target 783
  ]
  edge [
    source 106
    target 784
  ]
  edge [
    source 106
    target 785
  ]
  edge [
    source 106
    target 786
  ]
  edge [
    source 106
    target 787
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 788
  ]
  edge [
    source 107
    target 654
  ]
  edge [
    source 107
    target 789
  ]
  edge [
    source 107
    target 790
  ]
  edge [
    source 107
    target 791
  ]
  edge [
    source 107
    target 792
  ]
  edge [
    source 107
    target 446
  ]
  edge [
    source 107
    target 400
  ]
  edge [
    source 107
    target 793
  ]
  edge [
    source 107
    target 109
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 794
  ]
  edge [
    source 108
    target 795
  ]
  edge [
    source 108
    target 796
  ]
  edge [
    source 108
    target 797
  ]
  edge [
    source 108
    target 798
  ]
  edge [
    source 108
    target 791
  ]
  edge [
    source 108
    target 400
  ]
  edge [
    source 108
    target 793
  ]
  edge [
    source 108
    target 799
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 121
  ]
  edge [
    source 109
    target 620
  ]
  edge [
    source 109
    target 800
  ]
  edge [
    source 109
    target 801
  ]
  edge [
    source 109
    target 802
  ]
  edge [
    source 109
    target 591
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 803
  ]
  edge [
    source 110
    target 804
  ]
  edge [
    source 110
    target 805
  ]
  edge [
    source 110
    target 806
  ]
  edge [
    source 110
    target 807
  ]
  edge [
    source 110
    target 808
  ]
  edge [
    source 110
    target 245
  ]
  edge [
    source 110
    target 809
  ]
  edge [
    source 111
    target 810
  ]
  edge [
    source 111
    target 811
  ]
  edge [
    source 111
    target 812
  ]
  edge [
    source 111
    target 813
  ]
  edge [
    source 111
    target 234
  ]
  edge [
    source 111
    target 235
  ]
  edge [
    source 111
    target 236
  ]
  edge [
    source 111
    target 814
  ]
  edge [
    source 111
    target 238
  ]
  edge [
    source 111
    target 239
  ]
  edge [
    source 111
    target 240
  ]
  edge [
    source 111
    target 241
  ]
  edge [
    source 111
    target 815
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 816
  ]
  edge [
    source 112
    target 702
  ]
  edge [
    source 112
    target 817
  ]
  edge [
    source 112
    target 818
  ]
  edge [
    source 112
    target 819
  ]
  edge [
    source 113
    target 617
  ]
]
