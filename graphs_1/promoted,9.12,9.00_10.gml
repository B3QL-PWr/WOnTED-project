graph [
  maxDegree 32
  minDegree 1
  meanDegree 1.9550561797752808
  density 0.022216547497446375
  graphCliqueNumber 3
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "szklany"
    origin "text"
  ]
  node [
    id 3
    label "sufit"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 6
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 8
    label "przebi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "get"
  ]
  node [
    id 10
    label "zaj&#347;&#263;"
  ]
  node [
    id 11
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 12
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 13
    label "dop&#322;ata"
  ]
  node [
    id 14
    label "supervene"
  ]
  node [
    id 15
    label "heed"
  ]
  node [
    id 16
    label "dodatek"
  ]
  node [
    id 17
    label "catch"
  ]
  node [
    id 18
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 19
    label "uzyska&#263;"
  ]
  node [
    id 20
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 21
    label "orgazm"
  ]
  node [
    id 22
    label "dozna&#263;"
  ]
  node [
    id 23
    label "sta&#263;_si&#281;"
  ]
  node [
    id 24
    label "bodziec"
  ]
  node [
    id 25
    label "drive"
  ]
  node [
    id 26
    label "informacja"
  ]
  node [
    id 27
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 28
    label "spowodowa&#263;"
  ]
  node [
    id 29
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 30
    label "dotrze&#263;"
  ]
  node [
    id 31
    label "postrzega&#263;"
  ]
  node [
    id 32
    label "become"
  ]
  node [
    id 33
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 34
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 35
    label "przesy&#322;ka"
  ]
  node [
    id 36
    label "dolecie&#263;"
  ]
  node [
    id 37
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 38
    label "dokoptowa&#263;"
  ]
  node [
    id 39
    label "d&#378;wi&#281;czny"
  ]
  node [
    id 40
    label "oboj&#281;tny"
  ]
  node [
    id 41
    label "pusty"
  ]
  node [
    id 42
    label "szkli&#347;cie"
  ]
  node [
    id 43
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 44
    label "przezroczysty"
  ]
  node [
    id 45
    label "chorobliwy"
  ]
  node [
    id 46
    label "kaseton"
  ]
  node [
    id 47
    label "pomieszczenie"
  ]
  node [
    id 48
    label "p&#322;aszczyzna"
  ]
  node [
    id 49
    label "nijaki"
  ]
  node [
    id 50
    label "model"
  ]
  node [
    id 51
    label "zbi&#243;r"
  ]
  node [
    id 52
    label "tryb"
  ]
  node [
    id 53
    label "narz&#281;dzie"
  ]
  node [
    id 54
    label "nature"
  ]
  node [
    id 55
    label "by&#263;"
  ]
  node [
    id 56
    label "uprawi&#263;"
  ]
  node [
    id 57
    label "gotowy"
  ]
  node [
    id 58
    label "might"
  ]
  node [
    id 59
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 60
    label "przenikn&#261;&#263;"
  ]
  node [
    id 61
    label "przeszy&#263;"
  ]
  node [
    id 62
    label "przekrzycze&#263;"
  ]
  node [
    id 63
    label "wybi&#263;"
  ]
  node [
    id 64
    label "tear"
  ]
  node [
    id 65
    label "dopowiedzie&#263;"
  ]
  node [
    id 66
    label "przerzuci&#263;"
  ]
  node [
    id 67
    label "zdeklasowa&#263;"
  ]
  node [
    id 68
    label "zmieni&#263;"
  ]
  node [
    id 69
    label "pokona&#263;"
  ]
  node [
    id 70
    label "beat"
  ]
  node [
    id 71
    label "zm&#261;ci&#263;"
  ]
  node [
    id 72
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 73
    label "doj&#261;&#263;"
  ]
  node [
    id 74
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 75
    label "rozgromi&#263;"
  ]
  node [
    id 76
    label "przewierci&#263;"
  ]
  node [
    id 77
    label "strickle"
  ]
  node [
    id 78
    label "stick"
  ]
  node [
    id 79
    label "przedziurawi&#263;"
  ]
  node [
    id 80
    label "zbi&#263;"
  ]
  node [
    id 81
    label "tug"
  ]
  node [
    id 82
    label "zaproponowa&#263;"
  ]
  node [
    id 83
    label "zarysowa&#263;_si&#281;"
  ]
  node [
    id 84
    label "Justyn"
  ]
  node [
    id 85
    label "Krowicka"
  ]
  node [
    id 86
    label "Gemini"
  ]
  node [
    id 87
    label "w"
  ]
  node [
    id 88
    label "Ostrawie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 88
  ]
  edge [
    source 87
    target 88
  ]
]
