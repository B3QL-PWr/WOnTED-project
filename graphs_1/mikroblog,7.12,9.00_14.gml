graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 2
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 3
    label "dok&#322;adnie"
  ]
  node [
    id 4
    label "by&#263;"
  ]
  node [
    id 5
    label "czeka&#263;"
  ]
  node [
    id 6
    label "lookout"
  ]
  node [
    id 7
    label "wyziera&#263;"
  ]
  node [
    id 8
    label "peep"
  ]
  node [
    id 9
    label "look"
  ]
  node [
    id 10
    label "patrze&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
]
