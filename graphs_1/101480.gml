graph [
  maxDegree 5
  minDegree 1
  meanDegree 2
  density 0.18181818181818182
  graphCliqueNumber 4
  node [
    id 0
    label "tom"
    origin "text"
  ]
  node [
    id 1
    label "newman"
    origin "text"
  ]
  node [
    id 2
    label "b&#281;ben"
  ]
  node [
    id 3
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 4
    label "pi&#281;cioksi&#261;g"
  ]
  node [
    id 5
    label "Newman"
  ]
  node [
    id 6
    label "mistrzostwo"
  ]
  node [
    id 7
    label "&#347;wiat"
  ]
  node [
    id 8
    label "wyspa"
  ]
  node [
    id 9
    label "snooker"
  ]
  node [
    id 10
    label "Joe"
  ]
  node [
    id 11
    label "Davis"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
]
