graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.13333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "telatycki"
    origin "text"
  ]
  node [
    id 1
    label "herb"
    origin "text"
  ]
  node [
    id 2
    label "szlachecki"
    origin "text"
  ]
  node [
    id 3
    label "trzymacz"
  ]
  node [
    id 4
    label "symbol"
  ]
  node [
    id 5
    label "tarcza_herbowa"
  ]
  node [
    id 6
    label "korona_rangowa"
  ]
  node [
    id 7
    label "heraldyka"
  ]
  node [
    id 8
    label "blazonowa&#263;"
  ]
  node [
    id 9
    label "blazonowanie"
  ]
  node [
    id 10
    label "klejnot_herbowy"
  ]
  node [
    id 11
    label "barwy"
  ]
  node [
    id 12
    label "znak"
  ]
  node [
    id 13
    label "szlachecko"
  ]
  node [
    id 14
    label "&#347;lachecki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
]
