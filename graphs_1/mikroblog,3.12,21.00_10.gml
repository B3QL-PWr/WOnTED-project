graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.96875
  density 0.03125
  graphCliqueNumber 3
  node [
    id 0
    label "wpad&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 2
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 3
    label "wydawnictwo"
    origin "text"
  ]
  node [
    id 4
    label "literanova"
    origin "text"
  ]
  node [
    id 5
    label "mama"
    origin "text"
  ]
  node [
    id 6
    label "okazja"
    origin "text"
  ]
  node [
    id 7
    label "zawsze"
    origin "text"
  ]
  node [
    id 8
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "odwdzi&#281;czy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "mirasom"
    origin "text"
  ]
  node [
    id 12
    label "dziewczynka"
  ]
  node [
    id 13
    label "dziewczyna"
  ]
  node [
    id 14
    label "wsp&#243;&#322;pracownictwo"
  ]
  node [
    id 15
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 16
    label "debit"
  ]
  node [
    id 17
    label "szata_graficzna"
  ]
  node [
    id 18
    label "firma"
  ]
  node [
    id 19
    label "wyda&#263;"
  ]
  node [
    id 20
    label "redakcja"
  ]
  node [
    id 21
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 22
    label "wydawa&#263;"
  ]
  node [
    id 23
    label "redaktor"
  ]
  node [
    id 24
    label "druk"
  ]
  node [
    id 25
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 26
    label "poster"
  ]
  node [
    id 27
    label "publikacja"
  ]
  node [
    id 28
    label "matczysko"
  ]
  node [
    id 29
    label "macierz"
  ]
  node [
    id 30
    label "przodkini"
  ]
  node [
    id 31
    label "Matka_Boska"
  ]
  node [
    id 32
    label "macocha"
  ]
  node [
    id 33
    label "matka_zast&#281;pcza"
  ]
  node [
    id 34
    label "stara"
  ]
  node [
    id 35
    label "rodzice"
  ]
  node [
    id 36
    label "rodzic"
  ]
  node [
    id 37
    label "atrakcyjny"
  ]
  node [
    id 38
    label "oferta"
  ]
  node [
    id 39
    label "adeptness"
  ]
  node [
    id 40
    label "okazka"
  ]
  node [
    id 41
    label "wydarzenie"
  ]
  node [
    id 42
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 43
    label "podw&#243;zka"
  ]
  node [
    id 44
    label "autostop"
  ]
  node [
    id 45
    label "sytuacja"
  ]
  node [
    id 46
    label "zaw&#380;dy"
  ]
  node [
    id 47
    label "ci&#261;gle"
  ]
  node [
    id 48
    label "na_zawsze"
  ]
  node [
    id 49
    label "cz&#281;sto"
  ]
  node [
    id 50
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 51
    label "feel"
  ]
  node [
    id 52
    label "przedstawienie"
  ]
  node [
    id 53
    label "try"
  ]
  node [
    id 54
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 55
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 56
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 57
    label "sprawdza&#263;"
  ]
  node [
    id 58
    label "stara&#263;_si&#281;"
  ]
  node [
    id 59
    label "kosztowa&#263;"
  ]
  node [
    id 60
    label "Literanova"
  ]
  node [
    id 61
    label "24h"
  ]
  node [
    id 62
    label "losowa&#263;"
  ]
  node [
    id 63
    label "i"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 62
    target 63
  ]
]
