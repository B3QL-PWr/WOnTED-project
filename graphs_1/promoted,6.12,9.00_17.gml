graph [
  maxDegree 18
  minDegree 1
  meanDegree 2.0869565217391304
  density 0.018306636155606407
  graphCliqueNumber 3
  node [
    id 0
    label "dlaczego"
    origin "text"
  ]
  node [
    id 1
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 2
    label "r&#243;&#380;nica"
    origin "text"
  ]
  node [
    id 3
    label "cech"
    origin "text"
  ]
  node [
    id 4
    label "osobowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zainteresowanie"
    origin "text"
  ]
  node [
    id 6
    label "pomi&#281;dzy"
    origin "text"
  ]
  node [
    id 7
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 8
    label "kobieta"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "skandynawia"
    origin "text"
  ]
  node [
    id 11
    label "aktywnie"
    origin "text"
  ]
  node [
    id 12
    label "realizowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "egalitarny"
    origin "text"
  ]
  node [
    id 14
    label "polityka"
    origin "text"
  ]
  node [
    id 15
    label "r&#243;wno&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "doros&#322;y"
  ]
  node [
    id 17
    label "wiele"
  ]
  node [
    id 18
    label "dorodny"
  ]
  node [
    id 19
    label "znaczny"
  ]
  node [
    id 20
    label "du&#380;o"
  ]
  node [
    id 21
    label "prawdziwy"
  ]
  node [
    id 22
    label "niema&#322;o"
  ]
  node [
    id 23
    label "wa&#380;ny"
  ]
  node [
    id 24
    label "rozwini&#281;ty"
  ]
  node [
    id 25
    label "wynik"
  ]
  node [
    id 26
    label "r&#243;&#380;nienie"
  ]
  node [
    id 27
    label "cecha"
  ]
  node [
    id 28
    label "kontrastowy"
  ]
  node [
    id 29
    label "discord"
  ]
  node [
    id 30
    label "cechmistrz"
  ]
  node [
    id 31
    label "czeladnik"
  ]
  node [
    id 32
    label "stowarzyszenie"
  ]
  node [
    id 33
    label "club"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "byt"
  ]
  node [
    id 36
    label "wn&#281;trze"
  ]
  node [
    id 37
    label "psychika"
  ]
  node [
    id 38
    label "podmiot"
  ]
  node [
    id 39
    label "wyj&#261;tkowy"
  ]
  node [
    id 40
    label "self"
  ]
  node [
    id 41
    label "superego"
  ]
  node [
    id 42
    label "charakter"
  ]
  node [
    id 43
    label "mentalno&#347;&#263;"
  ]
  node [
    id 44
    label "ciekawo&#347;&#263;"
  ]
  node [
    id 45
    label "care"
  ]
  node [
    id 46
    label "emocja"
  ]
  node [
    id 47
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 48
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 49
    label "love"
  ]
  node [
    id 50
    label "wzbudzenie"
  ]
  node [
    id 51
    label "ch&#322;opina"
  ]
  node [
    id 52
    label "bratek"
  ]
  node [
    id 53
    label "jegomo&#347;&#263;"
  ]
  node [
    id 54
    label "samiec"
  ]
  node [
    id 55
    label "ojciec"
  ]
  node [
    id 56
    label "twardziel"
  ]
  node [
    id 57
    label "androlog"
  ]
  node [
    id 58
    label "pa&#324;stwo"
  ]
  node [
    id 59
    label "m&#261;&#380;"
  ]
  node [
    id 60
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 61
    label "andropauza"
  ]
  node [
    id 62
    label "przekwitanie"
  ]
  node [
    id 63
    label "m&#281;&#380;yna"
  ]
  node [
    id 64
    label "babka"
  ]
  node [
    id 65
    label "samica"
  ]
  node [
    id 66
    label "ulec"
  ]
  node [
    id 67
    label "uleganie"
  ]
  node [
    id 68
    label "partnerka"
  ]
  node [
    id 69
    label "&#380;ona"
  ]
  node [
    id 70
    label "ulega&#263;"
  ]
  node [
    id 71
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 72
    label "ulegni&#281;cie"
  ]
  node [
    id 73
    label "menopauza"
  ]
  node [
    id 74
    label "&#322;ono"
  ]
  node [
    id 75
    label "si&#281;ga&#263;"
  ]
  node [
    id 76
    label "trwa&#263;"
  ]
  node [
    id 77
    label "obecno&#347;&#263;"
  ]
  node [
    id 78
    label "stan"
  ]
  node [
    id 79
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 80
    label "stand"
  ]
  node [
    id 81
    label "mie&#263;_miejsce"
  ]
  node [
    id 82
    label "uczestniczy&#263;"
  ]
  node [
    id 83
    label "chodzi&#263;"
  ]
  node [
    id 84
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 85
    label "equal"
  ]
  node [
    id 86
    label "czynny"
  ]
  node [
    id 87
    label "ciekawie"
  ]
  node [
    id 88
    label "realnie"
  ]
  node [
    id 89
    label "faktycznie"
  ]
  node [
    id 90
    label "intensywnie"
  ]
  node [
    id 91
    label "aktywny"
  ]
  node [
    id 92
    label "wykorzystywa&#263;"
  ]
  node [
    id 93
    label "przeprowadza&#263;"
  ]
  node [
    id 94
    label "spieni&#281;&#380;a&#263;"
  ]
  node [
    id 95
    label "prosecute"
  ]
  node [
    id 96
    label "create"
  ]
  node [
    id 97
    label "prawdzi&#263;"
  ]
  node [
    id 98
    label "tworzy&#263;"
  ]
  node [
    id 99
    label "egalitarnie"
  ]
  node [
    id 100
    label "dost&#281;pny"
  ]
  node [
    id 101
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 102
    label "policy"
  ]
  node [
    id 103
    label "dyplomacja"
  ]
  node [
    id 104
    label "Europejska_Polityka_S&#261;siedztwa"
  ]
  node [
    id 105
    label "metoda"
  ]
  node [
    id 106
    label "spok&#243;j"
  ]
  node [
    id 107
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 108
    label "g&#322;adko&#347;&#263;"
  ]
  node [
    id 109
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 110
    label "poj&#281;cie"
  ]
  node [
    id 111
    label "podobie&#324;stwo"
  ]
  node [
    id 112
    label "Jordan"
  ]
  node [
    id 113
    label "bit"
  ]
  node [
    id 114
    label "Peterson"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 114
  ]
  edge [
    source 113
    target 114
  ]
]
