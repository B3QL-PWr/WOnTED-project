graph [
  maxDegree 11
  minDegree 1
  meanDegree 2.1142857142857143
  density 0.06218487394957983
  graphCliqueNumber 4
  node [
    id 0
    label "parafia"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiadek"
    origin "text"
  ]
  node [
    id 2
    label "kr&#243;lowa"
    origin "text"
  ]
  node [
    id 3
    label "jadwiga"
    origin "text"
  ]
  node [
    id 4
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "rada_parafialna"
  ]
  node [
    id 6
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 7
    label "parochia"
  ]
  node [
    id 8
    label "zabudowania"
  ]
  node [
    id 9
    label "dekanat"
  ]
  node [
    id 10
    label "parafianin"
  ]
  node [
    id 11
    label "diecezja"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "s&#261;d"
  ]
  node [
    id 14
    label "osoba_fizyczna"
  ]
  node [
    id 15
    label "uczestnik"
  ]
  node [
    id 16
    label "obserwator"
  ]
  node [
    id 17
    label "dru&#380;ba"
  ]
  node [
    id 18
    label "Izabela_I_Kastylijska"
  ]
  node [
    id 19
    label "pszczo&#322;a"
  ]
  node [
    id 20
    label "Bona"
  ]
  node [
    id 21
    label "Wiktoria"
  ]
  node [
    id 22
    label "kand"
  ]
  node [
    id 23
    label "strzelec"
  ]
  node [
    id 24
    label "El&#380;bieta_I"
  ]
  node [
    id 25
    label "kr&#243;lowa_matka"
  ]
  node [
    id 26
    label "mr&#243;wka"
  ]
  node [
    id 27
    label "miesi&#261;c"
  ]
  node [
    id 28
    label "&#347;wi&#281;ty"
  ]
  node [
    id 29
    label "kr&#243;lowy"
  ]
  node [
    id 30
    label "Jadwiga"
  ]
  node [
    id 31
    label "wrzesi&#324;ski"
  ]
  node [
    id 32
    label "ii"
  ]
  node [
    id 33
    label "J&#243;zefa"
  ]
  node [
    id 34
    label "Glemp"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
]
