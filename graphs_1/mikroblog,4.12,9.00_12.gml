graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9444444444444444
  density 0.05555555555555555
  graphCliqueNumber 2
  node [
    id 0
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 1
    label "kot"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "prostokotem"
    origin "text"
  ]
  node [
    id 4
    label "czyj&#347;"
  ]
  node [
    id 5
    label "m&#261;&#380;"
  ]
  node [
    id 6
    label "zamiaucze&#263;"
  ]
  node [
    id 7
    label "pierwszoklasista"
  ]
  node [
    id 8
    label "fala"
  ]
  node [
    id 9
    label "miaucze&#263;"
  ]
  node [
    id 10
    label "kabanos"
  ]
  node [
    id 11
    label "kotowate"
  ]
  node [
    id 12
    label "miaukni&#281;cie"
  ]
  node [
    id 13
    label "samiec"
  ]
  node [
    id 14
    label "otrz&#281;siny"
  ]
  node [
    id 15
    label "miauczenie"
  ]
  node [
    id 16
    label "czworon&#243;g"
  ]
  node [
    id 17
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 18
    label "rekrut"
  ]
  node [
    id 19
    label "zaj&#261;c"
  ]
  node [
    id 20
    label "kotwica"
  ]
  node [
    id 21
    label "trackball"
  ]
  node [
    id 22
    label "felinoterapia"
  ]
  node [
    id 23
    label "odk&#322;aczacz"
  ]
  node [
    id 24
    label "zamiauczenie"
  ]
  node [
    id 25
    label "si&#281;ga&#263;"
  ]
  node [
    id 26
    label "trwa&#263;"
  ]
  node [
    id 27
    label "obecno&#347;&#263;"
  ]
  node [
    id 28
    label "stan"
  ]
  node [
    id 29
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 30
    label "stand"
  ]
  node [
    id 31
    label "mie&#263;_miejsce"
  ]
  node [
    id 32
    label "uczestniczy&#263;"
  ]
  node [
    id 33
    label "chodzi&#263;"
  ]
  node [
    id 34
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 35
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
]
