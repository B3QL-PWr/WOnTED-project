graph [
  maxDegree 3
  minDegree 1
  meanDegree 2.4
  density 0.6
  graphCliqueNumber 3
  node [
    id 0
    label "hpi"
    origin "text"
  ]
  node [
    id 1
    label "firestorm"
    origin "text"
  ]
  node [
    id 2
    label "versus"
    origin "text"
  ]
  node [
    id 3
    label "traxxas"
    origin "text"
  ]
  node [
    id 4
    label "rustler"
    origin "text"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
]
