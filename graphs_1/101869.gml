graph [
  maxDegree 57
  minDegree 1
  meanDegree 2.1858864027538725
  density 0.00376876965992047
  graphCliqueNumber 3
  node [
    id 0
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 1
    label "letni"
    origin "text"
  ]
  node [
    id 2
    label "marcin"
    origin "text"
  ]
  node [
    id 3
    label "sekunda"
    origin "text"
  ]
  node [
    id 4
    label "druga"
    origin "text"
  ]
  node [
    id 5
    label "pi&#281;tro"
    origin "text"
  ]
  node [
    id 6
    label "blok"
    origin "text"
  ]
  node [
    id 7
    label "przy"
    origin "text"
  ]
  node [
    id 8
    label "ula"
    origin "text"
  ]
  node [
    id 9
    label "pi&#322;sudski"
    origin "text"
  ]
  node [
    id 10
    label "pijany"
    origin "text"
  ]
  node [
    id 11
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 12
    label "wyrzuci&#263;"
    origin "text"
  ]
  node [
    id 13
    label "przez"
    origin "text"
  ]
  node [
    id 14
    label "okno"
    origin "text"
  ]
  node [
    id 15
    label "litrowy"
    origin "text"
  ]
  node [
    id 16
    label "pusta"
    origin "text"
  ]
  node [
    id 17
    label "butla"
    origin "text"
  ]
  node [
    id 18
    label "gazowy"
    origin "text"
  ]
  node [
    id 19
    label "sprz&#261;ta&#263;"
    origin "text"
  ]
  node [
    id 20
    label "teren"
    origin "text"
  ]
  node [
    id 21
    label "dozorca"
    origin "text"
  ]
  node [
    id 22
    label "szcz&#281;&#347;cie"
    origin "text"
  ]
  node [
    id 23
    label "chybi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "ryszard"
    origin "text"
  ]
  node [
    id 25
    label "dawny"
    origin "text"
  ]
  node [
    id 26
    label "wezwa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "policja"
    origin "text"
  ]
  node [
    id 28
    label "widok"
    origin "text"
  ]
  node [
    id 29
    label "radiow&#243;z"
    origin "text"
  ]
  node [
    id 30
    label "kolega"
    origin "text"
  ]
  node [
    id 31
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 32
    label "wyrzuca&#263;"
    origin "text"
  ]
  node [
    id 33
    label "balkonowy"
    origin "text"
  ]
  node [
    id 34
    label "taler"
    origin "text"
  ]
  node [
    id 35
    label "fotel"
    origin "text"
  ]
  node [
    id 36
    label "puszek"
    origin "text"
  ]
  node [
    id 37
    label "piwo"
    origin "text"
  ]
  node [
    id 38
    label "nawet"
    origin "text"
  ]
  node [
    id 39
    label "antena"
    origin "text"
  ]
  node [
    id 40
    label "satelitarny"
    origin "text"
  ]
  node [
    id 41
    label "potem"
    origin "text"
  ]
  node [
    id 42
    label "zabarykadowa&#263;"
    origin "text"
  ]
  node [
    id 43
    label "si&#281;"
    origin "text"
  ]
  node [
    id 44
    label "policjant"
    origin "text"
  ]
  node [
    id 45
    label "stra&#380;ak"
    origin "text"
  ]
  node [
    id 46
    label "wywa&#380;y&#263;"
    origin "text"
  ]
  node [
    id 47
    label "drzwi"
    origin "text"
  ]
  node [
    id 48
    label "zanim"
    origin "text"
  ]
  node [
    id 49
    label "awanturnik"
    origin "text"
  ]
  node [
    id 50
    label "sku&#263;"
    origin "text"
  ]
  node [
    id 51
    label "kajdanki"
    origin "text"
  ]
  node [
    id 52
    label "kopn&#261;&#263;"
    origin "text"
  ]
  node [
    id 53
    label "noga"
    origin "text"
  ]
  node [
    id 54
    label "obrzuci&#263;"
    origin "text"
  ]
  node [
    id 55
    label "str&#243;&#380;"
    origin "text"
  ]
  node [
    id 56
    label "porz&#261;dek"
    origin "text"
  ]
  node [
    id 57
    label "obelga"
    origin "text"
  ]
  node [
    id 58
    label "zatrzyma&#263;"
    origin "text"
  ]
  node [
    id 59
    label "u&#380;ycie"
    origin "text"
  ]
  node [
    id 60
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 61
    label "wobec"
    origin "text"
  ]
  node [
    id 62
    label "nara&#380;enie"
    origin "text"
  ]
  node [
    id 63
    label "przechodzie&#324;"
    origin "text"
  ]
  node [
    id 64
    label "utrata"
    origin "text"
  ]
  node [
    id 65
    label "zdrowie"
    origin "text"
  ]
  node [
    id 66
    label "lub"
    origin "text"
  ]
  node [
    id 67
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 68
    label "grozi&#263;"
    origin "text"
  ]
  node [
    id 69
    label "trzy"
    origin "text"
  ]
  node [
    id 70
    label "lata"
    origin "text"
  ]
  node [
    id 71
    label "wi&#281;zienie"
    origin "text"
  ]
  node [
    id 72
    label "stanie"
  ]
  node [
    id 73
    label "przebywanie"
  ]
  node [
    id 74
    label "panowanie"
  ]
  node [
    id 75
    label "zajmowanie"
  ]
  node [
    id 76
    label "pomieszkanie"
  ]
  node [
    id 77
    label "adjustment"
  ]
  node [
    id 78
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 79
    label "lokal"
  ]
  node [
    id 80
    label "kwadrat"
  ]
  node [
    id 81
    label "animation"
  ]
  node [
    id 82
    label "dom"
  ]
  node [
    id 83
    label "nijaki"
  ]
  node [
    id 84
    label "sezonowy"
  ]
  node [
    id 85
    label "letnio"
  ]
  node [
    id 86
    label "s&#322;oneczny"
  ]
  node [
    id 87
    label "weso&#322;y"
  ]
  node [
    id 88
    label "oboj&#281;tny"
  ]
  node [
    id 89
    label "latowy"
  ]
  node [
    id 90
    label "ciep&#322;y"
  ]
  node [
    id 91
    label "typowy"
  ]
  node [
    id 92
    label "minuta"
  ]
  node [
    id 93
    label "tercja"
  ]
  node [
    id 94
    label "milisekunda"
  ]
  node [
    id 95
    label "nanosekunda"
  ]
  node [
    id 96
    label "uk&#322;ad_SI"
  ]
  node [
    id 97
    label "mikrosekunda"
  ]
  node [
    id 98
    label "time"
  ]
  node [
    id 99
    label "jednostka_czasu"
  ]
  node [
    id 100
    label "jednostka"
  ]
  node [
    id 101
    label "godzina"
  ]
  node [
    id 102
    label "kondygnacja"
  ]
  node [
    id 103
    label "floor"
  ]
  node [
    id 104
    label "eta&#380;"
  ]
  node [
    id 105
    label "grupa"
  ]
  node [
    id 106
    label "budynek"
  ]
  node [
    id 107
    label "jednostka_geologiczna"
  ]
  node [
    id 108
    label "chronozona"
  ]
  node [
    id 109
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 110
    label "oddzia&#322;"
  ]
  node [
    id 111
    label "p&#322;aszczyzna"
  ]
  node [
    id 112
    label "square"
  ]
  node [
    id 113
    label "whole"
  ]
  node [
    id 114
    label "zamek"
  ]
  node [
    id 115
    label "block"
  ]
  node [
    id 116
    label "blokowisko"
  ]
  node [
    id 117
    label "barak"
  ]
  node [
    id 118
    label "stok_kontynentalny"
  ]
  node [
    id 119
    label "blokada"
  ]
  node [
    id 120
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 121
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 122
    label "siatk&#243;wka"
  ]
  node [
    id 123
    label "kr&#261;g"
  ]
  node [
    id 124
    label "bloking"
  ]
  node [
    id 125
    label "dom_wielorodzinny"
  ]
  node [
    id 126
    label "dzia&#322;"
  ]
  node [
    id 127
    label "przeszkoda"
  ]
  node [
    id 128
    label "bie&#380;nia"
  ]
  node [
    id 129
    label "organizacja"
  ]
  node [
    id 130
    label "start"
  ]
  node [
    id 131
    label "blockage"
  ]
  node [
    id 132
    label "obrona"
  ]
  node [
    id 133
    label "bajt"
  ]
  node [
    id 134
    label "zesp&#243;&#322;"
  ]
  node [
    id 135
    label "artyku&#322;"
  ]
  node [
    id 136
    label "bry&#322;a"
  ]
  node [
    id 137
    label "zeszyt"
  ]
  node [
    id 138
    label "ok&#322;adka"
  ]
  node [
    id 139
    label "kontynent"
  ]
  node [
    id 140
    label "referat"
  ]
  node [
    id 141
    label "nastawnia"
  ]
  node [
    id 142
    label "skorupa_ziemska"
  ]
  node [
    id 143
    label "program"
  ]
  node [
    id 144
    label "zbi&#243;r"
  ]
  node [
    id 145
    label "ram&#243;wka"
  ]
  node [
    id 146
    label "j&#261;kanie"
  ]
  node [
    id 147
    label "cz&#322;owiek"
  ]
  node [
    id 148
    label "upicie_si&#281;"
  ]
  node [
    id 149
    label "szalony"
  ]
  node [
    id 150
    label "d&#281;tka"
  ]
  node [
    id 151
    label "pij&#261;cy"
  ]
  node [
    id 152
    label "upijanie_si&#281;"
  ]
  node [
    id 153
    label "napi&#322;y"
  ]
  node [
    id 154
    label "nieprzytomny"
  ]
  node [
    id 155
    label "ch&#322;opina"
  ]
  node [
    id 156
    label "bratek"
  ]
  node [
    id 157
    label "jegomo&#347;&#263;"
  ]
  node [
    id 158
    label "doros&#322;y"
  ]
  node [
    id 159
    label "samiec"
  ]
  node [
    id 160
    label "ojciec"
  ]
  node [
    id 161
    label "twardziel"
  ]
  node [
    id 162
    label "androlog"
  ]
  node [
    id 163
    label "pa&#324;stwo"
  ]
  node [
    id 164
    label "m&#261;&#380;"
  ]
  node [
    id 165
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 166
    label "andropauza"
  ]
  node [
    id 167
    label "roll"
  ]
  node [
    id 168
    label "wypierdoli&#263;"
  ]
  node [
    id 169
    label "frame"
  ]
  node [
    id 170
    label "powiedzie&#263;"
  ]
  node [
    id 171
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 172
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 173
    label "usun&#261;&#263;"
  ]
  node [
    id 174
    label "arouse"
  ]
  node [
    id 175
    label "wychrzani&#263;"
  ]
  node [
    id 176
    label "wypierniczy&#263;"
  ]
  node [
    id 177
    label "wysadzi&#263;"
  ]
  node [
    id 178
    label "ruszy&#263;"
  ]
  node [
    id 179
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 180
    label "zrobi&#263;"
  ]
  node [
    id 181
    label "reproach"
  ]
  node [
    id 182
    label "lufcik"
  ]
  node [
    id 183
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 184
    label "parapet"
  ]
  node [
    id 185
    label "otw&#243;r"
  ]
  node [
    id 186
    label "skrzyd&#322;o"
  ]
  node [
    id 187
    label "kwatera_okienna"
  ]
  node [
    id 188
    label "szyba"
  ]
  node [
    id 189
    label "futryna"
  ]
  node [
    id 190
    label "nadokiennik"
  ]
  node [
    id 191
    label "interfejs"
  ]
  node [
    id 192
    label "inspekt"
  ]
  node [
    id 193
    label "casement"
  ]
  node [
    id 194
    label "prze&#347;wit"
  ]
  node [
    id 195
    label "menad&#380;er_okien"
  ]
  node [
    id 196
    label "pulpit"
  ]
  node [
    id 197
    label "transenna"
  ]
  node [
    id 198
    label "nora"
  ]
  node [
    id 199
    label "okiennica"
  ]
  node [
    id 200
    label "zawarto&#347;&#263;"
  ]
  node [
    id 201
    label "naczynie"
  ]
  node [
    id 202
    label "zbiornik"
  ]
  node [
    id 203
    label "akwalung"
  ]
  node [
    id 204
    label "gazowo"
  ]
  node [
    id 205
    label "odsuwa&#263;"
  ]
  node [
    id 206
    label "authorize"
  ]
  node [
    id 207
    label "czy&#347;ci&#263;"
  ]
  node [
    id 208
    label "ch&#281;do&#380;y&#263;"
  ]
  node [
    id 209
    label "zakres"
  ]
  node [
    id 210
    label "kontekst"
  ]
  node [
    id 211
    label "wymiar"
  ]
  node [
    id 212
    label "obszar"
  ]
  node [
    id 213
    label "krajobraz"
  ]
  node [
    id 214
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 215
    label "w&#322;adza"
  ]
  node [
    id 216
    label "nation"
  ]
  node [
    id 217
    label "przyroda"
  ]
  node [
    id 218
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 219
    label "miejsce_pracy"
  ]
  node [
    id 220
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 221
    label "gospodarz"
  ]
  node [
    id 222
    label "cie&#263;"
  ]
  node [
    id 223
    label "nadzorca"
  ]
  node [
    id 224
    label "wra&#380;enie"
  ]
  node [
    id 225
    label "przeznaczenie"
  ]
  node [
    id 226
    label "dobrodziejstwo"
  ]
  node [
    id 227
    label "dobro"
  ]
  node [
    id 228
    label "przypadek"
  ]
  node [
    id 229
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 230
    label "miss"
  ]
  node [
    id 231
    label "przesz&#322;y"
  ]
  node [
    id 232
    label "dawno"
  ]
  node [
    id 233
    label "dawniej"
  ]
  node [
    id 234
    label "kombatant"
  ]
  node [
    id 235
    label "stary"
  ]
  node [
    id 236
    label "odleg&#322;y"
  ]
  node [
    id 237
    label "anachroniczny"
  ]
  node [
    id 238
    label "przestarza&#322;y"
  ]
  node [
    id 239
    label "od_dawna"
  ]
  node [
    id 240
    label "poprzedni"
  ]
  node [
    id 241
    label "d&#322;ugoletni"
  ]
  node [
    id 242
    label "wcze&#347;niejszy"
  ]
  node [
    id 243
    label "niegdysiejszy"
  ]
  node [
    id 244
    label "poprosi&#263;"
  ]
  node [
    id 245
    label "ask"
  ]
  node [
    id 246
    label "invite"
  ]
  node [
    id 247
    label "zach&#281;ci&#263;"
  ]
  node [
    id 248
    label "adduce"
  ]
  node [
    id 249
    label "przewo&#322;a&#263;"
  ]
  node [
    id 250
    label "poinformowa&#263;"
  ]
  node [
    id 251
    label "nakaza&#263;"
  ]
  node [
    id 252
    label "komisariat"
  ]
  node [
    id 253
    label "psiarnia"
  ]
  node [
    id 254
    label "posterunek"
  ]
  node [
    id 255
    label "organ"
  ]
  node [
    id 256
    label "s&#322;u&#380;ba"
  ]
  node [
    id 257
    label "wygl&#261;d"
  ]
  node [
    id 258
    label "obraz"
  ]
  node [
    id 259
    label "cecha"
  ]
  node [
    id 260
    label "przestrze&#324;"
  ]
  node [
    id 261
    label "perspektywa"
  ]
  node [
    id 262
    label "samoch&#243;d"
  ]
  node [
    id 263
    label "misiow&#243;z"
  ]
  node [
    id 264
    label "kumplowanie_si&#281;"
  ]
  node [
    id 265
    label "znajomy"
  ]
  node [
    id 266
    label "konfrater"
  ]
  node [
    id 267
    label "towarzysz"
  ]
  node [
    id 268
    label "zakolegowanie_si&#281;"
  ]
  node [
    id 269
    label "partner"
  ]
  node [
    id 270
    label "ziom"
  ]
  node [
    id 271
    label "cause"
  ]
  node [
    id 272
    label "introduce"
  ]
  node [
    id 273
    label "begin"
  ]
  node [
    id 274
    label "odj&#261;&#263;"
  ]
  node [
    id 275
    label "post&#261;pi&#263;"
  ]
  node [
    id 276
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 277
    label "do"
  ]
  node [
    id 278
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 279
    label "pami&#281;ta&#263;"
  ]
  node [
    id 280
    label "m&#243;wi&#263;"
  ]
  node [
    id 281
    label "wysadza&#263;"
  ]
  node [
    id 282
    label "przemieszcza&#263;"
  ]
  node [
    id 283
    label "wychrzania&#263;"
  ]
  node [
    id 284
    label "usuwa&#263;"
  ]
  node [
    id 285
    label "wypierdala&#263;"
  ]
  node [
    id 286
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 287
    label "oskar&#380;a&#263;"
  ]
  node [
    id 288
    label "resist"
  ]
  node [
    id 289
    label "wypiernicza&#263;"
  ]
  node [
    id 290
    label "raise"
  ]
  node [
    id 291
    label "denounce"
  ]
  node [
    id 292
    label "rusza&#263;"
  ]
  node [
    id 293
    label "oparcie"
  ]
  node [
    id 294
    label "zaplecek"
  ]
  node [
    id 295
    label "siedzenie"
  ]
  node [
    id 296
    label "urz&#261;d"
  ]
  node [
    id 297
    label "pod&#322;okietnik"
  ]
  node [
    id 298
    label "komplet_wypoczynkowy"
  ]
  node [
    id 299
    label "por&#281;cz"
  ]
  node [
    id 300
    label "wezg&#322;owie"
  ]
  node [
    id 301
    label "mebel"
  ]
  node [
    id 302
    label "akcesorium"
  ]
  node [
    id 303
    label "browarnia"
  ]
  node [
    id 304
    label "anta&#322;"
  ]
  node [
    id 305
    label "wyj&#347;cie"
  ]
  node [
    id 306
    label "warzy&#263;"
  ]
  node [
    id 307
    label "warzenie"
  ]
  node [
    id 308
    label "uwarzenie"
  ]
  node [
    id 309
    label "alkohol"
  ]
  node [
    id 310
    label "nap&#243;j"
  ]
  node [
    id 311
    label "nawarzy&#263;"
  ]
  node [
    id 312
    label "bacik"
  ]
  node [
    id 313
    label "uwarzy&#263;"
  ]
  node [
    id 314
    label "nawarzenie"
  ]
  node [
    id 315
    label "birofilia"
  ]
  node [
    id 316
    label "szyk_antenowy"
  ]
  node [
    id 317
    label "stawon&#243;g"
  ]
  node [
    id 318
    label "odbieranie"
  ]
  node [
    id 319
    label "czu&#322;ek"
  ]
  node [
    id 320
    label "nadajnik"
  ]
  node [
    id 321
    label "odbiera&#263;"
  ]
  node [
    id 322
    label "odbiornik"
  ]
  node [
    id 323
    label "zale&#380;ny"
  ]
  node [
    id 324
    label "cyfrowy"
  ]
  node [
    id 325
    label "zbudowa&#263;"
  ]
  node [
    id 326
    label "zablokowa&#263;"
  ]
  node [
    id 327
    label "roadblock"
  ]
  node [
    id 328
    label "blacharz"
  ]
  node [
    id 329
    label "pa&#322;a"
  ]
  node [
    id 330
    label "mundurowy"
  ]
  node [
    id 331
    label "glina"
  ]
  node [
    id 332
    label "rota"
  ]
  node [
    id 333
    label "gasi&#263;"
  ]
  node [
    id 334
    label "po&#380;arnik"
  ]
  node [
    id 335
    label "sztafeta_po&#380;arnicza"
  ]
  node [
    id 336
    label "stra&#380;_po&#380;arna"
  ]
  node [
    id 337
    label "po&#380;ar"
  ]
  node [
    id 338
    label "sikawkowy"
  ]
  node [
    id 339
    label "wypcha&#263;"
  ]
  node [
    id 340
    label "dobra&#263;"
  ]
  node [
    id 341
    label "zastanowi&#263;_si&#281;"
  ]
  node [
    id 342
    label "zr&#243;wnowa&#380;y&#263;"
  ]
  node [
    id 343
    label "wytw&#243;r"
  ]
  node [
    id 344
    label "doj&#347;cie"
  ]
  node [
    id 345
    label "antaba"
  ]
  node [
    id 346
    label "szafa"
  ]
  node [
    id 347
    label "szafka"
  ]
  node [
    id 348
    label "wej&#347;cie"
  ]
  node [
    id 349
    label "zawiasy"
  ]
  node [
    id 350
    label "samozamykacz"
  ]
  node [
    id 351
    label "ko&#322;atka"
  ]
  node [
    id 352
    label "klamka"
  ]
  node [
    id 353
    label "wrzeci&#261;dz"
  ]
  node [
    id 354
    label "niespokojny_duch"
  ]
  node [
    id 355
    label "ha&#322;aburda"
  ]
  node [
    id 356
    label "k&#322;&#243;tnik"
  ]
  node [
    id 357
    label "ryzykant"
  ]
  node [
    id 358
    label "furfant"
  ]
  node [
    id 359
    label "podr&#243;&#380;nik"
  ]
  node [
    id 360
    label "troublemaker"
  ]
  node [
    id 361
    label "gor&#261;ca_g&#322;owa"
  ]
  node [
    id 362
    label "przygoda"
  ]
  node [
    id 363
    label "ha&#322;asownik"
  ]
  node [
    id 364
    label "spowodowa&#263;"
  ]
  node [
    id 365
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 366
    label "scali&#263;"
  ]
  node [
    id 367
    label "fetter"
  ]
  node [
    id 368
    label "narz&#281;dzie_wymierzania_kary"
  ]
  node [
    id 369
    label "handcuff"
  ]
  node [
    id 370
    label "uderzy&#263;"
  ]
  node [
    id 371
    label "dig"
  ]
  node [
    id 372
    label "porazi&#263;"
  ]
  node [
    id 373
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 374
    label "&#322;amaga"
  ]
  node [
    id 375
    label "kopa&#263;"
  ]
  node [
    id 376
    label "jedenastka"
  ]
  node [
    id 377
    label "mi&#281;sie&#324;_krawiecki"
  ]
  node [
    id 378
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 379
    label "sfaulowanie"
  ]
  node [
    id 380
    label "czpas"
  ]
  node [
    id 381
    label "Wis&#322;a"
  ]
  node [
    id 382
    label "depta&#263;"
  ]
  node [
    id 383
    label "ekstraklasa"
  ]
  node [
    id 384
    label "kopni&#281;cie"
  ]
  node [
    id 385
    label "bramkarz"
  ]
  node [
    id 386
    label "mato&#322;"
  ]
  node [
    id 387
    label "r&#281;ka"
  ]
  node [
    id 388
    label "zamurowywa&#263;"
  ]
  node [
    id 389
    label "dogranie"
  ]
  node [
    id 390
    label "catenaccio"
  ]
  node [
    id 391
    label "lobowanie"
  ]
  node [
    id 392
    label "mi&#281;sie&#324;_po&#347;ladkowy_ma&#322;y"
  ]
  node [
    id 393
    label "tackle"
  ]
  node [
    id 394
    label "ko&#324;czyna_dolna"
  ]
  node [
    id 395
    label "interliga"
  ]
  node [
    id 396
    label "lobowa&#263;"
  ]
  node [
    id 397
    label "mi&#281;czak"
  ]
  node [
    id 398
    label "podpora"
  ]
  node [
    id 399
    label "stopa"
  ]
  node [
    id 400
    label "pi&#322;ka"
  ]
  node [
    id 401
    label "nerw_udowo-goleniowy"
  ]
  node [
    id 402
    label "bezbramkowy"
  ]
  node [
    id 403
    label "zamurowywanie"
  ]
  node [
    id 404
    label "przelobowa&#263;"
  ]
  node [
    id 405
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 406
    label "dogrywanie"
  ]
  node [
    id 407
    label "zamurowanie"
  ]
  node [
    id 408
    label "ko&#324;czyna"
  ]
  node [
    id 409
    label "faulowa&#263;"
  ]
  node [
    id 410
    label "narz&#261;d_ruchu"
  ]
  node [
    id 411
    label "napinacz"
  ]
  node [
    id 412
    label "mi&#281;sie&#324;_po&#347;ladkowy_&#347;redni"
  ]
  node [
    id 413
    label "dublet"
  ]
  node [
    id 414
    label "gira"
  ]
  node [
    id 415
    label "przelobowanie"
  ]
  node [
    id 416
    label "dogra&#263;"
  ]
  node [
    id 417
    label "zamurowa&#263;"
  ]
  node [
    id 418
    label "kopanie"
  ]
  node [
    id 419
    label "mundial"
  ]
  node [
    id 420
    label "mi&#281;sie&#324;_po&#347;ladkowy_wielki"
  ]
  node [
    id 421
    label "&#322;&#261;czyna"
  ]
  node [
    id 422
    label "dogrywa&#263;"
  ]
  node [
    id 423
    label "s&#322;abeusz"
  ]
  node [
    id 424
    label "faulowanie"
  ]
  node [
    id 425
    label "sfaulowa&#263;"
  ]
  node [
    id 426
    label "nerw_udowy"
  ]
  node [
    id 427
    label "czerwona_kartka"
  ]
  node [
    id 428
    label "obsypa&#263;"
  ]
  node [
    id 429
    label "rzuci&#263;"
  ]
  node [
    id 430
    label "hem"
  ]
  node [
    id 431
    label "napa&#347;&#263;"
  ]
  node [
    id 432
    label "hide"
  ]
  node [
    id 433
    label "obr&#281;bi&#263;"
  ]
  node [
    id 434
    label "stra&#380;nik"
  ]
  node [
    id 435
    label "obro&#324;ca"
  ]
  node [
    id 436
    label "anio&#322;"
  ]
  node [
    id 437
    label "opiekun"
  ]
  node [
    id 438
    label "uk&#322;ad"
  ]
  node [
    id 439
    label "styl_architektoniczny"
  ]
  node [
    id 440
    label "stan"
  ]
  node [
    id 441
    label "normalizacja"
  ]
  node [
    id 442
    label "relacja"
  ]
  node [
    id 443
    label "zasada"
  ]
  node [
    id 444
    label "struktura"
  ]
  node [
    id 445
    label "cholera"
  ]
  node [
    id 446
    label "wypowied&#378;"
  ]
  node [
    id 447
    label "krzywda"
  ]
  node [
    id 448
    label "pies"
  ]
  node [
    id 449
    label "szmata"
  ]
  node [
    id 450
    label "bluzg"
  ]
  node [
    id 451
    label "wyzwisko"
  ]
  node [
    id 452
    label "chujowy"
  ]
  node [
    id 453
    label "chuj"
  ]
  node [
    id 454
    label "indignation"
  ]
  node [
    id 455
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 456
    label "ubliga"
  ]
  node [
    id 457
    label "niedorobek"
  ]
  node [
    id 458
    label "wrzuta"
  ]
  node [
    id 459
    label "tekst"
  ]
  node [
    id 460
    label "continue"
  ]
  node [
    id 461
    label "zabra&#263;"
  ]
  node [
    id 462
    label "zaaresztowa&#263;"
  ]
  node [
    id 463
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 464
    label "przerwa&#263;"
  ]
  node [
    id 465
    label "zamkn&#261;&#263;"
  ]
  node [
    id 466
    label "bury"
  ]
  node [
    id 467
    label "komornik"
  ]
  node [
    id 468
    label "unieruchomi&#263;"
  ]
  node [
    id 469
    label "suspend"
  ]
  node [
    id 470
    label "give"
  ]
  node [
    id 471
    label "bankrupt"
  ]
  node [
    id 472
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 473
    label "zaczepi&#263;"
  ]
  node [
    id 474
    label "przechowa&#263;"
  ]
  node [
    id 475
    label "anticipate"
  ]
  node [
    id 476
    label "use"
  ]
  node [
    id 477
    label "zrobienie"
  ]
  node [
    id 478
    label "doznanie"
  ]
  node [
    id 479
    label "stosowanie"
  ]
  node [
    id 480
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 481
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 482
    label "enjoyment"
  ]
  node [
    id 483
    label "u&#380;yteczny"
  ]
  node [
    id 484
    label "zabawa"
  ]
  node [
    id 485
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 486
    label "wojsko"
  ]
  node [
    id 487
    label "magnitude"
  ]
  node [
    id 488
    label "energia"
  ]
  node [
    id 489
    label "capacity"
  ]
  node [
    id 490
    label "wuchta"
  ]
  node [
    id 491
    label "parametr"
  ]
  node [
    id 492
    label "moment_si&#322;y"
  ]
  node [
    id 493
    label "przemoc"
  ]
  node [
    id 494
    label "zdolno&#347;&#263;"
  ]
  node [
    id 495
    label "mn&#243;stwo"
  ]
  node [
    id 496
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 497
    label "rozwi&#261;zanie"
  ]
  node [
    id 498
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 499
    label "potencja"
  ]
  node [
    id 500
    label "zjawisko"
  ]
  node [
    id 501
    label "zaleta"
  ]
  node [
    id 502
    label "wystawienie"
  ]
  node [
    id 503
    label "expose"
  ]
  node [
    id 504
    label "pieszy"
  ]
  node [
    id 505
    label "zniszczenie"
  ]
  node [
    id 506
    label "ubytek"
  ]
  node [
    id 507
    label "niepowodzenie"
  ]
  node [
    id 508
    label "szwank"
  ]
  node [
    id 509
    label "os&#322;abia&#263;"
  ]
  node [
    id 510
    label "niszczy&#263;"
  ]
  node [
    id 511
    label "zniszczy&#263;"
  ]
  node [
    id 512
    label "firmness"
  ]
  node [
    id 513
    label "kondycja"
  ]
  node [
    id 514
    label "os&#322;abi&#263;"
  ]
  node [
    id 515
    label "rozsypanie_si&#281;"
  ]
  node [
    id 516
    label "zdarcie"
  ]
  node [
    id 517
    label "zedrze&#263;"
  ]
  node [
    id 518
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 519
    label "niszczenie"
  ]
  node [
    id 520
    label "os&#322;abienie"
  ]
  node [
    id 521
    label "soundness"
  ]
  node [
    id 522
    label "os&#322;abianie"
  ]
  node [
    id 523
    label "energy"
  ]
  node [
    id 524
    label "czas"
  ]
  node [
    id 525
    label "bycie"
  ]
  node [
    id 526
    label "zegar_biologiczny"
  ]
  node [
    id 527
    label "okres_noworodkowy"
  ]
  node [
    id 528
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 529
    label "entity"
  ]
  node [
    id 530
    label "prze&#380;ywanie"
  ]
  node [
    id 531
    label "prze&#380;ycie"
  ]
  node [
    id 532
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 533
    label "wiek_matuzalemowy"
  ]
  node [
    id 534
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 535
    label "dzieci&#324;stwo"
  ]
  node [
    id 536
    label "power"
  ]
  node [
    id 537
    label "szwung"
  ]
  node [
    id 538
    label "menopauza"
  ]
  node [
    id 539
    label "umarcie"
  ]
  node [
    id 540
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 541
    label "life"
  ]
  node [
    id 542
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 543
    label "&#380;ywy"
  ]
  node [
    id 544
    label "rozw&#243;j"
  ]
  node [
    id 545
    label "po&#322;&#243;g"
  ]
  node [
    id 546
    label "byt"
  ]
  node [
    id 547
    label "subsistence"
  ]
  node [
    id 548
    label "koleje_losu"
  ]
  node [
    id 549
    label "raj_utracony"
  ]
  node [
    id 550
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 551
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 552
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 553
    label "warunki"
  ]
  node [
    id 554
    label "do&#380;ywanie"
  ]
  node [
    id 555
    label "niemowl&#281;ctwo"
  ]
  node [
    id 556
    label "umieranie"
  ]
  node [
    id 557
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 558
    label "staro&#347;&#263;"
  ]
  node [
    id 559
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 560
    label "&#347;mier&#263;"
  ]
  node [
    id 561
    label "zapowiada&#263;"
  ]
  node [
    id 562
    label "boast"
  ]
  node [
    id 563
    label "hazard"
  ]
  node [
    id 564
    label "summer"
  ]
  node [
    id 565
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 566
    label "ogranicza&#263;"
  ]
  node [
    id 567
    label "uniemo&#380;liwianie"
  ]
  node [
    id 568
    label "Butyrki"
  ]
  node [
    id 569
    label "ciupa"
  ]
  node [
    id 570
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 571
    label "reedukator"
  ]
  node [
    id 572
    label "miejsce_odosobnienia"
  ]
  node [
    id 573
    label "&#321;ubianka"
  ]
  node [
    id 574
    label "pierdel"
  ]
  node [
    id 575
    label "imprisonment"
  ]
  node [
    id 576
    label "sytuacja"
  ]
  node [
    id 577
    label "Marcin"
  ]
  node [
    id 578
    label "syn"
  ]
  node [
    id 579
    label "Ryszarda"
  ]
  node [
    id 580
    label "dzie&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 33
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 35
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 201
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 36
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 38
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 55
  ]
  edge [
    source 21
    target 223
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 63
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 224
  ]
  edge [
    source 22
    target 225
  ]
  edge [
    source 22
    target 226
  ]
  edge [
    source 22
    target 227
  ]
  edge [
    source 22
    target 228
  ]
  edge [
    source 23
    target 229
  ]
  edge [
    source 23
    target 230
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 231
  ]
  edge [
    source 25
    target 232
  ]
  edge [
    source 25
    target 233
  ]
  edge [
    source 25
    target 234
  ]
  edge [
    source 25
    target 235
  ]
  edge [
    source 25
    target 236
  ]
  edge [
    source 25
    target 237
  ]
  edge [
    source 25
    target 238
  ]
  edge [
    source 25
    target 239
  ]
  edge [
    source 25
    target 240
  ]
  edge [
    source 25
    target 241
  ]
  edge [
    source 25
    target 242
  ]
  edge [
    source 25
    target 243
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 44
  ]
  edge [
    source 26
    target 45
  ]
  edge [
    source 26
    target 244
  ]
  edge [
    source 26
    target 245
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 26
    target 248
  ]
  edge [
    source 26
    target 249
  ]
  edge [
    source 26
    target 250
  ]
  edge [
    source 26
    target 251
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 253
  ]
  edge [
    source 27
    target 254
  ]
  edge [
    source 27
    target 105
  ]
  edge [
    source 27
    target 255
  ]
  edge [
    source 27
    target 256
  ]
  edge [
    source 27
    target 44
  ]
  edge [
    source 27
    target 63
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 29
    target 262
  ]
  edge [
    source 29
    target 263
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 53
  ]
  edge [
    source 30
    target 54
  ]
  edge [
    source 30
    target 264
  ]
  edge [
    source 30
    target 265
  ]
  edge [
    source 30
    target 266
  ]
  edge [
    source 30
    target 267
  ]
  edge [
    source 30
    target 268
  ]
  edge [
    source 30
    target 269
  ]
  edge [
    source 30
    target 270
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 271
  ]
  edge [
    source 31
    target 272
  ]
  edge [
    source 31
    target 273
  ]
  edge [
    source 31
    target 274
  ]
  edge [
    source 31
    target 275
  ]
  edge [
    source 31
    target 276
  ]
  edge [
    source 31
    target 277
  ]
  edge [
    source 31
    target 278
  ]
  edge [
    source 31
    target 180
  ]
  edge [
    source 32
    target 279
  ]
  edge [
    source 32
    target 280
  ]
  edge [
    source 32
    target 169
  ]
  edge [
    source 32
    target 281
  ]
  edge [
    source 32
    target 282
  ]
  edge [
    source 32
    target 283
  ]
  edge [
    source 32
    target 284
  ]
  edge [
    source 32
    target 285
  ]
  edge [
    source 32
    target 286
  ]
  edge [
    source 32
    target 287
  ]
  edge [
    source 32
    target 288
  ]
  edge [
    source 32
    target 289
  ]
  edge [
    source 32
    target 290
  ]
  edge [
    source 32
    target 291
  ]
  edge [
    source 32
    target 292
  ]
  edge [
    source 32
    target 57
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 44
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 293
  ]
  edge [
    source 35
    target 294
  ]
  edge [
    source 35
    target 295
  ]
  edge [
    source 35
    target 296
  ]
  edge [
    source 35
    target 297
  ]
  edge [
    source 35
    target 298
  ]
  edge [
    source 35
    target 299
  ]
  edge [
    source 35
    target 300
  ]
  edge [
    source 35
    target 301
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 302
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 303
  ]
  edge [
    source 37
    target 304
  ]
  edge [
    source 37
    target 305
  ]
  edge [
    source 37
    target 306
  ]
  edge [
    source 37
    target 307
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 309
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 37
    target 311
  ]
  edge [
    source 37
    target 312
  ]
  edge [
    source 37
    target 313
  ]
  edge [
    source 37
    target 314
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 316
  ]
  edge [
    source 39
    target 317
  ]
  edge [
    source 39
    target 318
  ]
  edge [
    source 39
    target 319
  ]
  edge [
    source 39
    target 320
  ]
  edge [
    source 39
    target 321
  ]
  edge [
    source 39
    target 322
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 323
  ]
  edge [
    source 40
    target 324
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 325
  ]
  edge [
    source 42
    target 326
  ]
  edge [
    source 42
    target 327
  ]
  edge [
    source 44
    target 52
  ]
  edge [
    source 44
    target 53
  ]
  edge [
    source 44
    target 61
  ]
  edge [
    source 44
    target 62
  ]
  edge [
    source 44
    target 328
  ]
  edge [
    source 44
    target 329
  ]
  edge [
    source 44
    target 330
  ]
  edge [
    source 44
    target 55
  ]
  edge [
    source 44
    target 331
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 332
  ]
  edge [
    source 45
    target 333
  ]
  edge [
    source 45
    target 330
  ]
  edge [
    source 45
    target 334
  ]
  edge [
    source 45
    target 335
  ]
  edge [
    source 45
    target 336
  ]
  edge [
    source 45
    target 337
  ]
  edge [
    source 45
    target 338
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 339
  ]
  edge [
    source 46
    target 340
  ]
  edge [
    source 46
    target 341
  ]
  edge [
    source 46
    target 342
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 305
  ]
  edge [
    source 47
    target 343
  ]
  edge [
    source 47
    target 344
  ]
  edge [
    source 47
    target 186
  ]
  edge [
    source 47
    target 114
  ]
  edge [
    source 47
    target 189
  ]
  edge [
    source 47
    target 345
  ]
  edge [
    source 47
    target 346
  ]
  edge [
    source 47
    target 347
  ]
  edge [
    source 47
    target 348
  ]
  edge [
    source 47
    target 349
  ]
  edge [
    source 47
    target 350
  ]
  edge [
    source 47
    target 351
  ]
  edge [
    source 47
    target 352
  ]
  edge [
    source 47
    target 353
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 57
  ]
  edge [
    source 49
    target 58
  ]
  edge [
    source 49
    target 354
  ]
  edge [
    source 49
    target 355
  ]
  edge [
    source 49
    target 356
  ]
  edge [
    source 49
    target 357
  ]
  edge [
    source 49
    target 358
  ]
  edge [
    source 49
    target 359
  ]
  edge [
    source 49
    target 360
  ]
  edge [
    source 49
    target 361
  ]
  edge [
    source 49
    target 362
  ]
  edge [
    source 49
    target 363
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 364
  ]
  edge [
    source 50
    target 365
  ]
  edge [
    source 50
    target 366
  ]
  edge [
    source 50
    target 173
  ]
  edge [
    source 50
    target 367
  ]
  edge [
    source 50
    target 229
  ]
  edge [
    source 51
    target 368
  ]
  edge [
    source 51
    target 369
  ]
  edge [
    source 51
    target 59
  ]
  edge [
    source 52
    target 370
  ]
  edge [
    source 52
    target 371
  ]
  edge [
    source 52
    target 372
  ]
  edge [
    source 52
    target 373
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 374
  ]
  edge [
    source 53
    target 375
  ]
  edge [
    source 53
    target 376
  ]
  edge [
    source 53
    target 377
  ]
  edge [
    source 53
    target 378
  ]
  edge [
    source 53
    target 379
  ]
  edge [
    source 53
    target 380
  ]
  edge [
    source 53
    target 381
  ]
  edge [
    source 53
    target 382
  ]
  edge [
    source 53
    target 383
  ]
  edge [
    source 53
    target 384
  ]
  edge [
    source 53
    target 385
  ]
  edge [
    source 53
    target 386
  ]
  edge [
    source 53
    target 387
  ]
  edge [
    source 53
    target 388
  ]
  edge [
    source 53
    target 389
  ]
  edge [
    source 53
    target 390
  ]
  edge [
    source 53
    target 391
  ]
  edge [
    source 53
    target 392
  ]
  edge [
    source 53
    target 393
  ]
  edge [
    source 53
    target 394
  ]
  edge [
    source 53
    target 395
  ]
  edge [
    source 53
    target 396
  ]
  edge [
    source 53
    target 397
  ]
  edge [
    source 53
    target 398
  ]
  edge [
    source 53
    target 399
  ]
  edge [
    source 53
    target 400
  ]
  edge [
    source 53
    target 401
  ]
  edge [
    source 53
    target 402
  ]
  edge [
    source 53
    target 403
  ]
  edge [
    source 53
    target 404
  ]
  edge [
    source 53
    target 405
  ]
  edge [
    source 53
    target 406
  ]
  edge [
    source 53
    target 407
  ]
  edge [
    source 53
    target 408
  ]
  edge [
    source 53
    target 409
  ]
  edge [
    source 53
    target 410
  ]
  edge [
    source 53
    target 411
  ]
  edge [
    source 53
    target 412
  ]
  edge [
    source 53
    target 413
  ]
  edge [
    source 53
    target 414
  ]
  edge [
    source 53
    target 415
  ]
  edge [
    source 53
    target 416
  ]
  edge [
    source 53
    target 417
  ]
  edge [
    source 53
    target 418
  ]
  edge [
    source 53
    target 419
  ]
  edge [
    source 53
    target 420
  ]
  edge [
    source 53
    target 421
  ]
  edge [
    source 53
    target 422
  ]
  edge [
    source 53
    target 423
  ]
  edge [
    source 53
    target 424
  ]
  edge [
    source 53
    target 425
  ]
  edge [
    source 53
    target 426
  ]
  edge [
    source 53
    target 427
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 428
  ]
  edge [
    source 54
    target 429
  ]
  edge [
    source 54
    target 430
  ]
  edge [
    source 54
    target 431
  ]
  edge [
    source 54
    target 432
  ]
  edge [
    source 54
    target 433
  ]
  edge [
    source 54
    target 67
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 434
  ]
  edge [
    source 55
    target 435
  ]
  edge [
    source 55
    target 436
  ]
  edge [
    source 55
    target 437
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 438
  ]
  edge [
    source 56
    target 439
  ]
  edge [
    source 56
    target 440
  ]
  edge [
    source 56
    target 441
  ]
  edge [
    source 56
    target 259
  ]
  edge [
    source 56
    target 442
  ]
  edge [
    source 56
    target 443
  ]
  edge [
    source 56
    target 444
  ]
  edge [
    source 57
    target 445
  ]
  edge [
    source 57
    target 446
  ]
  edge [
    source 57
    target 447
  ]
  edge [
    source 57
    target 448
  ]
  edge [
    source 57
    target 449
  ]
  edge [
    source 57
    target 450
  ]
  edge [
    source 57
    target 451
  ]
  edge [
    source 57
    target 452
  ]
  edge [
    source 57
    target 453
  ]
  edge [
    source 57
    target 454
  ]
  edge [
    source 57
    target 455
  ]
  edge [
    source 57
    target 456
  ]
  edge [
    source 57
    target 457
  ]
  edge [
    source 57
    target 458
  ]
  edge [
    source 57
    target 459
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 460
  ]
  edge [
    source 58
    target 461
  ]
  edge [
    source 58
    target 462
  ]
  edge [
    source 58
    target 463
  ]
  edge [
    source 58
    target 364
  ]
  edge [
    source 58
    target 464
  ]
  edge [
    source 58
    target 465
  ]
  edge [
    source 58
    target 466
  ]
  edge [
    source 58
    target 467
  ]
  edge [
    source 58
    target 468
  ]
  edge [
    source 58
    target 469
  ]
  edge [
    source 58
    target 470
  ]
  edge [
    source 58
    target 471
  ]
  edge [
    source 58
    target 472
  ]
  edge [
    source 58
    target 473
  ]
  edge [
    source 58
    target 474
  ]
  edge [
    source 58
    target 475
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 476
  ]
  edge [
    source 59
    target 477
  ]
  edge [
    source 59
    target 478
  ]
  edge [
    source 59
    target 479
  ]
  edge [
    source 59
    target 480
  ]
  edge [
    source 59
    target 481
  ]
  edge [
    source 59
    target 482
  ]
  edge [
    source 59
    target 483
  ]
  edge [
    source 59
    target 484
  ]
  edge [
    source 59
    target 485
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 486
  ]
  edge [
    source 60
    target 487
  ]
  edge [
    source 60
    target 488
  ]
  edge [
    source 60
    target 489
  ]
  edge [
    source 60
    target 490
  ]
  edge [
    source 60
    target 259
  ]
  edge [
    source 60
    target 491
  ]
  edge [
    source 60
    target 492
  ]
  edge [
    source 60
    target 493
  ]
  edge [
    source 60
    target 494
  ]
  edge [
    source 60
    target 495
  ]
  edge [
    source 60
    target 496
  ]
  edge [
    source 60
    target 497
  ]
  edge [
    source 60
    target 498
  ]
  edge [
    source 60
    target 499
  ]
  edge [
    source 60
    target 500
  ]
  edge [
    source 60
    target 501
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 502
  ]
  edge [
    source 62
    target 503
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 504
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 505
  ]
  edge [
    source 64
    target 506
  ]
  edge [
    source 64
    target 507
  ]
  edge [
    source 64
    target 508
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 509
  ]
  edge [
    source 65
    target 510
  ]
  edge [
    source 65
    target 511
  ]
  edge [
    source 65
    target 440
  ]
  edge [
    source 65
    target 512
  ]
  edge [
    source 65
    target 513
  ]
  edge [
    source 65
    target 514
  ]
  edge [
    source 65
    target 515
  ]
  edge [
    source 65
    target 516
  ]
  edge [
    source 65
    target 259
  ]
  edge [
    source 65
    target 505
  ]
  edge [
    source 65
    target 517
  ]
  edge [
    source 65
    target 518
  ]
  edge [
    source 65
    target 519
  ]
  edge [
    source 65
    target 520
  ]
  edge [
    source 65
    target 521
  ]
  edge [
    source 65
    target 522
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 523
  ]
  edge [
    source 67
    target 524
  ]
  edge [
    source 67
    target 525
  ]
  edge [
    source 67
    target 526
  ]
  edge [
    source 67
    target 527
  ]
  edge [
    source 67
    target 528
  ]
  edge [
    source 67
    target 529
  ]
  edge [
    source 67
    target 530
  ]
  edge [
    source 67
    target 531
  ]
  edge [
    source 67
    target 532
  ]
  edge [
    source 67
    target 533
  ]
  edge [
    source 67
    target 534
  ]
  edge [
    source 67
    target 535
  ]
  edge [
    source 67
    target 536
  ]
  edge [
    source 67
    target 537
  ]
  edge [
    source 67
    target 538
  ]
  edge [
    source 67
    target 539
  ]
  edge [
    source 67
    target 540
  ]
  edge [
    source 67
    target 541
  ]
  edge [
    source 67
    target 542
  ]
  edge [
    source 67
    target 543
  ]
  edge [
    source 67
    target 544
  ]
  edge [
    source 67
    target 545
  ]
  edge [
    source 67
    target 546
  ]
  edge [
    source 67
    target 73
  ]
  edge [
    source 67
    target 547
  ]
  edge [
    source 67
    target 548
  ]
  edge [
    source 67
    target 549
  ]
  edge [
    source 67
    target 550
  ]
  edge [
    source 67
    target 551
  ]
  edge [
    source 67
    target 552
  ]
  edge [
    source 67
    target 166
  ]
  edge [
    source 67
    target 553
  ]
  edge [
    source 67
    target 554
  ]
  edge [
    source 67
    target 555
  ]
  edge [
    source 67
    target 556
  ]
  edge [
    source 67
    target 557
  ]
  edge [
    source 67
    target 558
  ]
  edge [
    source 67
    target 559
  ]
  edge [
    source 67
    target 560
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 561
  ]
  edge [
    source 68
    target 562
  ]
  edge [
    source 68
    target 563
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 564
  ]
  edge [
    source 70
    target 524
  ]
  edge [
    source 71
    target 565
  ]
  edge [
    source 71
    target 566
  ]
  edge [
    source 71
    target 567
  ]
  edge [
    source 71
    target 568
  ]
  edge [
    source 71
    target 569
  ]
  edge [
    source 71
    target 570
  ]
  edge [
    source 71
    target 571
  ]
  edge [
    source 71
    target 572
  ]
  edge [
    source 71
    target 573
  ]
  edge [
    source 71
    target 574
  ]
  edge [
    source 71
    target 575
  ]
  edge [
    source 71
    target 576
  ]
  edge [
    source 577
    target 578
  ]
  edge [
    source 579
    target 580
  ]
]
