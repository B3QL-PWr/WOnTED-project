graph [
  maxDegree 136
  minDegree 1
  meanDegree 2.028776978417266
  density 0.004876867736579967
  graphCliqueNumber 2
  node [
    id 0
    label "rada"
    origin "text"
  ]
  node [
    id 1
    label "miejski"
    origin "text"
  ]
  node [
    id 2
    label "radom"
    origin "text"
  ]
  node [
    id 3
    label "zwraca&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "apel"
    origin "text"
  ]
  node [
    id 6
    label "uwzgl&#281;dnienie"
    origin "text"
  ]
  node [
    id 7
    label "program"
    origin "text"
  ]
  node [
    id 8
    label "budowa"
    origin "text"
  ]
  node [
    id 9
    label "droga"
    origin "text"
  ]
  node [
    id 10
    label "krajowy"
    origin "text"
  ]
  node [
    id 11
    label "lata"
    origin "text"
  ]
  node [
    id 12
    label "inwestycja"
    origin "text"
  ]
  node [
    id 13
    label "drogowe"
    origin "text"
  ]
  node [
    id 14
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 15
    label "dla"
    origin "text"
  ]
  node [
    id 16
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 17
    label "region"
    origin "text"
  ]
  node [
    id 18
    label "radomskie"
    origin "text"
  ]
  node [
    id 19
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 20
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 21
    label "pomin&#261;&#263;"
    origin "text"
  ]
  node [
    id 22
    label "lub"
    origin "text"
  ]
  node [
    id 23
    label "przesun&#261;&#263;"
    origin "text"
  ]
  node [
    id 24
    label "daleki"
    origin "text"
  ]
  node [
    id 25
    label "okres"
    origin "text"
  ]
  node [
    id 26
    label "realizacja"
    origin "text"
  ]
  node [
    id 27
    label "dyskusja"
  ]
  node [
    id 28
    label "grupa"
  ]
  node [
    id 29
    label "Rada_Unii_Europejskiej"
  ]
  node [
    id 30
    label "conference"
  ]
  node [
    id 31
    label "organ"
  ]
  node [
    id 32
    label "zgromadzenie"
  ]
  node [
    id 33
    label "wskaz&#243;wka"
  ]
  node [
    id 34
    label "konsylium"
  ]
  node [
    id 35
    label "Rada_Europy"
  ]
  node [
    id 36
    label "Rada_Europejska"
  ]
  node [
    id 37
    label "posiedzenie"
  ]
  node [
    id 38
    label "miejsko"
  ]
  node [
    id 39
    label "miastowy"
  ]
  node [
    id 40
    label "typowy"
  ]
  node [
    id 41
    label "publiczny"
  ]
  node [
    id 42
    label "ustawia&#263;"
  ]
  node [
    id 43
    label "wydala&#263;"
  ]
  node [
    id 44
    label "give"
  ]
  node [
    id 45
    label "manipulate"
  ]
  node [
    id 46
    label "indicate"
  ]
  node [
    id 47
    label "przeznacza&#263;"
  ]
  node [
    id 48
    label "haftowa&#263;"
  ]
  node [
    id 49
    label "przekazywa&#263;"
  ]
  node [
    id 50
    label "uderzenie"
  ]
  node [
    id 51
    label "zdyscyplinowanie"
  ]
  node [
    id 52
    label "sygna&#322;"
  ]
  node [
    id 53
    label "bid"
  ]
  node [
    id 54
    label "recoil"
  ]
  node [
    id 55
    label "spotkanie"
  ]
  node [
    id 56
    label "zbi&#243;rka"
  ]
  node [
    id 57
    label "pro&#347;ba"
  ]
  node [
    id 58
    label "pies_my&#347;liwski"
  ]
  node [
    id 59
    label "znak"
  ]
  node [
    id 60
    label "szermierka"
  ]
  node [
    id 61
    label "wzi&#281;cie"
  ]
  node [
    id 62
    label "acknowledgment"
  ]
  node [
    id 63
    label "spis"
  ]
  node [
    id 64
    label "odinstalowanie"
  ]
  node [
    id 65
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 66
    label "za&#322;o&#380;enie"
  ]
  node [
    id 67
    label "podstawa"
  ]
  node [
    id 68
    label "emitowanie"
  ]
  node [
    id 69
    label "odinstalowywanie"
  ]
  node [
    id 70
    label "instrukcja"
  ]
  node [
    id 71
    label "punkt"
  ]
  node [
    id 72
    label "teleferie"
  ]
  node [
    id 73
    label "emitowa&#263;"
  ]
  node [
    id 74
    label "wytw&#243;r"
  ]
  node [
    id 75
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 76
    label "sekcja_krytyczna"
  ]
  node [
    id 77
    label "oferta"
  ]
  node [
    id 78
    label "prezentowa&#263;"
  ]
  node [
    id 79
    label "blok"
  ]
  node [
    id 80
    label "podprogram"
  ]
  node [
    id 81
    label "tryb"
  ]
  node [
    id 82
    label "dzia&#322;"
  ]
  node [
    id 83
    label "broszura"
  ]
  node [
    id 84
    label "deklaracja"
  ]
  node [
    id 85
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 86
    label "struktura_organizacyjna"
  ]
  node [
    id 87
    label "zaprezentowanie"
  ]
  node [
    id 88
    label "informatyka"
  ]
  node [
    id 89
    label "booklet"
  ]
  node [
    id 90
    label "menu"
  ]
  node [
    id 91
    label "oprogramowanie"
  ]
  node [
    id 92
    label "instalowanie"
  ]
  node [
    id 93
    label "furkacja"
  ]
  node [
    id 94
    label "odinstalowa&#263;"
  ]
  node [
    id 95
    label "instalowa&#263;"
  ]
  node [
    id 96
    label "pirat"
  ]
  node [
    id 97
    label "zainstalowanie"
  ]
  node [
    id 98
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 99
    label "ogranicznik_referencyjny"
  ]
  node [
    id 100
    label "zainstalowa&#263;"
  ]
  node [
    id 101
    label "kana&#322;"
  ]
  node [
    id 102
    label "zaprezentowa&#263;"
  ]
  node [
    id 103
    label "interfejs"
  ]
  node [
    id 104
    label "odinstalowywa&#263;"
  ]
  node [
    id 105
    label "folder"
  ]
  node [
    id 106
    label "course_of_study"
  ]
  node [
    id 107
    label "ram&#243;wka"
  ]
  node [
    id 108
    label "prezentowanie"
  ]
  node [
    id 109
    label "okno"
  ]
  node [
    id 110
    label "figura"
  ]
  node [
    id 111
    label "wjazd"
  ]
  node [
    id 112
    label "struktura"
  ]
  node [
    id 113
    label "konstrukcja"
  ]
  node [
    id 114
    label "r&#243;w"
  ]
  node [
    id 115
    label "kreacja"
  ]
  node [
    id 116
    label "posesja"
  ]
  node [
    id 117
    label "cecha"
  ]
  node [
    id 118
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 119
    label "mechanika"
  ]
  node [
    id 120
    label "zwierz&#281;"
  ]
  node [
    id 121
    label "miejsce_pracy"
  ]
  node [
    id 122
    label "praca"
  ]
  node [
    id 123
    label "constitution"
  ]
  node [
    id 124
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 125
    label "journey"
  ]
  node [
    id 126
    label "podbieg"
  ]
  node [
    id 127
    label "bezsilnikowy"
  ]
  node [
    id 128
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 129
    label "wylot"
  ]
  node [
    id 130
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 131
    label "drogowskaz"
  ]
  node [
    id 132
    label "nawierzchnia"
  ]
  node [
    id 133
    label "turystyka"
  ]
  node [
    id 134
    label "budowla"
  ]
  node [
    id 135
    label "spos&#243;b"
  ]
  node [
    id 136
    label "passage"
  ]
  node [
    id 137
    label "marszrutyzacja"
  ]
  node [
    id 138
    label "zbior&#243;wka"
  ]
  node [
    id 139
    label "rajza"
  ]
  node [
    id 140
    label "ekskursja"
  ]
  node [
    id 141
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 142
    label "ruch"
  ]
  node [
    id 143
    label "trasa"
  ]
  node [
    id 144
    label "wyb&#243;j"
  ]
  node [
    id 145
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 146
    label "ekwipunek"
  ]
  node [
    id 147
    label "korona_drogi"
  ]
  node [
    id 148
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 149
    label "pobocze"
  ]
  node [
    id 150
    label "rodzimy"
  ]
  node [
    id 151
    label "summer"
  ]
  node [
    id 152
    label "czas"
  ]
  node [
    id 153
    label "inwestycje"
  ]
  node [
    id 154
    label "wydatki_maj&#261;tkowe"
  ]
  node [
    id 155
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 156
    label "wk&#322;ad"
  ]
  node [
    id 157
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 158
    label "kapita&#322;"
  ]
  node [
    id 159
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 160
    label "inwestowanie"
  ]
  node [
    id 161
    label "bud&#380;et_domowy"
  ]
  node [
    id 162
    label "sentyment_inwestycyjny"
  ]
  node [
    id 163
    label "rezultat"
  ]
  node [
    id 164
    label "du&#380;y"
  ]
  node [
    id 165
    label "jedyny"
  ]
  node [
    id 166
    label "kompletny"
  ]
  node [
    id 167
    label "zdr&#243;w"
  ]
  node [
    id 168
    label "&#380;ywy"
  ]
  node [
    id 169
    label "ca&#322;o"
  ]
  node [
    id 170
    label "pe&#322;ny"
  ]
  node [
    id 171
    label "calu&#347;ko"
  ]
  node [
    id 172
    label "podobny"
  ]
  node [
    id 173
    label "Skandynawia"
  ]
  node [
    id 174
    label "Yorkshire"
  ]
  node [
    id 175
    label "Kaukaz"
  ]
  node [
    id 176
    label "Kaszmir"
  ]
  node [
    id 177
    label "Podbeskidzie"
  ]
  node [
    id 178
    label "Toskania"
  ]
  node [
    id 179
    label "&#321;emkowszczyzna"
  ]
  node [
    id 180
    label "obszar"
  ]
  node [
    id 181
    label "Amhara"
  ]
  node [
    id 182
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 183
    label "Lombardia"
  ]
  node [
    id 184
    label "okr&#281;g"
  ]
  node [
    id 185
    label "Chiny_Wschodnie"
  ]
  node [
    id 186
    label "Kalabria"
  ]
  node [
    id 187
    label "Tyrol"
  ]
  node [
    id 188
    label "Pamir"
  ]
  node [
    id 189
    label "Lubelszczyzna"
  ]
  node [
    id 190
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 191
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 192
    label "&#379;ywiecczyzna"
  ]
  node [
    id 193
    label "Europa_Wschodnia"
  ]
  node [
    id 194
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 195
    label "Chiny_Zachodnie"
  ]
  node [
    id 196
    label "Zabajkale"
  ]
  node [
    id 197
    label "Kaszuby"
  ]
  node [
    id 198
    label "Noworosja"
  ]
  node [
    id 199
    label "Bo&#347;nia"
  ]
  node [
    id 200
    label "Ba&#322;kany"
  ]
  node [
    id 201
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 202
    label "Anglia"
  ]
  node [
    id 203
    label "Kielecczyzna"
  ]
  node [
    id 204
    label "Krajina"
  ]
  node [
    id 205
    label "Pomorze_Zachodnie"
  ]
  node [
    id 206
    label "Opolskie"
  ]
  node [
    id 207
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 208
    label "Ko&#322;yma"
  ]
  node [
    id 209
    label "Oksytania"
  ]
  node [
    id 210
    label "country"
  ]
  node [
    id 211
    label "Syjon"
  ]
  node [
    id 212
    label "Kociewie"
  ]
  node [
    id 213
    label "Huculszczyzna"
  ]
  node [
    id 214
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 215
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 216
    label "Bawaria"
  ]
  node [
    id 217
    label "Maghreb"
  ]
  node [
    id 218
    label "Bory_Tucholskie"
  ]
  node [
    id 219
    label "Europa_Zachodnia"
  ]
  node [
    id 220
    label "Kerala"
  ]
  node [
    id 221
    label "Burgundia"
  ]
  node [
    id 222
    label "Podhale"
  ]
  node [
    id 223
    label "Kabylia"
  ]
  node [
    id 224
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 225
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 226
    label "Ma&#322;opolska"
  ]
  node [
    id 227
    label "Polesie"
  ]
  node [
    id 228
    label "Liguria"
  ]
  node [
    id 229
    label "&#321;&#243;dzkie"
  ]
  node [
    id 230
    label "Palestyna"
  ]
  node [
    id 231
    label "Bojkowszczyzna"
  ]
  node [
    id 232
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 233
    label "Karaiby"
  ]
  node [
    id 234
    label "S&#261;decczyzna"
  ]
  node [
    id 235
    label "Sand&#380;ak"
  ]
  node [
    id 236
    label "Nadrenia"
  ]
  node [
    id 237
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 238
    label "Zakarpacie"
  ]
  node [
    id 239
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 240
    label "Zag&#243;rze"
  ]
  node [
    id 241
    label "Andaluzja"
  ]
  node [
    id 242
    label "&#379;mud&#378;"
  ]
  node [
    id 243
    label "Turkiestan"
  ]
  node [
    id 244
    label "Naddniestrze"
  ]
  node [
    id 245
    label "Hercegowina"
  ]
  node [
    id 246
    label "Opolszczyzna"
  ]
  node [
    id 247
    label "jednostka_administracyjna"
  ]
  node [
    id 248
    label "Lotaryngia"
  ]
  node [
    id 249
    label "Afryka_Wschodnia"
  ]
  node [
    id 250
    label "Szlezwik"
  ]
  node [
    id 251
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 252
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 253
    label "Mazowsze"
  ]
  node [
    id 254
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 255
    label "Afryka_Zachodnia"
  ]
  node [
    id 256
    label "Galicja"
  ]
  node [
    id 257
    label "Szkocja"
  ]
  node [
    id 258
    label "Walia"
  ]
  node [
    id 259
    label "Powi&#347;le"
  ]
  node [
    id 260
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 261
    label "Zamojszczyzna"
  ]
  node [
    id 262
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 263
    label "Kujawy"
  ]
  node [
    id 264
    label "Podlasie"
  ]
  node [
    id 265
    label "Laponia"
  ]
  node [
    id 266
    label "Umbria"
  ]
  node [
    id 267
    label "Mezoameryka"
  ]
  node [
    id 268
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 269
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 270
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 271
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 272
    label "Kraina"
  ]
  node [
    id 273
    label "Kurdystan"
  ]
  node [
    id 274
    label "Flandria"
  ]
  node [
    id 275
    label "Kampania"
  ]
  node [
    id 276
    label "Armagnac"
  ]
  node [
    id 277
    label "Polinezja"
  ]
  node [
    id 278
    label "Warmia"
  ]
  node [
    id 279
    label "Wielkopolska"
  ]
  node [
    id 280
    label "Bordeaux"
  ]
  node [
    id 281
    label "Lauda"
  ]
  node [
    id 282
    label "Mazury"
  ]
  node [
    id 283
    label "Podkarpacie"
  ]
  node [
    id 284
    label "Oceania"
  ]
  node [
    id 285
    label "Lasko"
  ]
  node [
    id 286
    label "Amazonia"
  ]
  node [
    id 287
    label "podregion"
  ]
  node [
    id 288
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 289
    label "Kurpie"
  ]
  node [
    id 290
    label "Tonkin"
  ]
  node [
    id 291
    label "Azja_Wschodnia"
  ]
  node [
    id 292
    label "Mikronezja"
  ]
  node [
    id 293
    label "Ukraina_Zachodnia"
  ]
  node [
    id 294
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 295
    label "subregion"
  ]
  node [
    id 296
    label "Turyngia"
  ]
  node [
    id 297
    label "Baszkiria"
  ]
  node [
    id 298
    label "Apulia"
  ]
  node [
    id 299
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 300
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 301
    label "Indochiny"
  ]
  node [
    id 302
    label "Lubuskie"
  ]
  node [
    id 303
    label "Biskupizna"
  ]
  node [
    id 304
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 305
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 306
    label "proceed"
  ]
  node [
    id 307
    label "catch"
  ]
  node [
    id 308
    label "pozosta&#263;"
  ]
  node [
    id 309
    label "osta&#263;_si&#281;"
  ]
  node [
    id 310
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 311
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 312
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 313
    label "change"
  ]
  node [
    id 314
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 315
    label "spowodowa&#263;"
  ]
  node [
    id 316
    label "omin&#261;&#263;"
  ]
  node [
    id 317
    label "zby&#263;"
  ]
  node [
    id 318
    label "dostosowa&#263;"
  ]
  node [
    id 319
    label "motivate"
  ]
  node [
    id 320
    label "przenie&#347;&#263;"
  ]
  node [
    id 321
    label "shift"
  ]
  node [
    id 322
    label "deepen"
  ]
  node [
    id 323
    label "transfer"
  ]
  node [
    id 324
    label "ruszy&#263;"
  ]
  node [
    id 325
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 326
    label "zmieni&#263;"
  ]
  node [
    id 327
    label "dawny"
  ]
  node [
    id 328
    label "s&#322;aby"
  ]
  node [
    id 329
    label "oddalony"
  ]
  node [
    id 330
    label "daleko"
  ]
  node [
    id 331
    label "przysz&#322;y"
  ]
  node [
    id 332
    label "ogl&#281;dny"
  ]
  node [
    id 333
    label "r&#243;&#380;ny"
  ]
  node [
    id 334
    label "g&#322;&#281;boki"
  ]
  node [
    id 335
    label "odlegle"
  ]
  node [
    id 336
    label "nieobecny"
  ]
  node [
    id 337
    label "odleg&#322;y"
  ]
  node [
    id 338
    label "d&#322;ugi"
  ]
  node [
    id 339
    label "zwi&#261;zany"
  ]
  node [
    id 340
    label "obcy"
  ]
  node [
    id 341
    label "paleogen"
  ]
  node [
    id 342
    label "spell"
  ]
  node [
    id 343
    label "period"
  ]
  node [
    id 344
    label "prekambr"
  ]
  node [
    id 345
    label "jura"
  ]
  node [
    id 346
    label "interstadia&#322;"
  ]
  node [
    id 347
    label "jednostka_geologiczna"
  ]
  node [
    id 348
    label "izochronizm"
  ]
  node [
    id 349
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 350
    label "okres_noachijski"
  ]
  node [
    id 351
    label "orosir"
  ]
  node [
    id 352
    label "kreda"
  ]
  node [
    id 353
    label "sten"
  ]
  node [
    id 354
    label "drugorz&#281;d"
  ]
  node [
    id 355
    label "semester"
  ]
  node [
    id 356
    label "trzeciorz&#281;d"
  ]
  node [
    id 357
    label "ton"
  ]
  node [
    id 358
    label "dzieje"
  ]
  node [
    id 359
    label "poprzednik"
  ]
  node [
    id 360
    label "ordowik"
  ]
  node [
    id 361
    label "karbon"
  ]
  node [
    id 362
    label "trias"
  ]
  node [
    id 363
    label "kalim"
  ]
  node [
    id 364
    label "stater"
  ]
  node [
    id 365
    label "era"
  ]
  node [
    id 366
    label "cykl"
  ]
  node [
    id 367
    label "p&#243;&#322;okres"
  ]
  node [
    id 368
    label "czwartorz&#281;d"
  ]
  node [
    id 369
    label "pulsacja"
  ]
  node [
    id 370
    label "okres_amazo&#324;ski"
  ]
  node [
    id 371
    label "kambr"
  ]
  node [
    id 372
    label "Zeitgeist"
  ]
  node [
    id 373
    label "nast&#281;pnik"
  ]
  node [
    id 374
    label "kriogen"
  ]
  node [
    id 375
    label "glacja&#322;"
  ]
  node [
    id 376
    label "fala"
  ]
  node [
    id 377
    label "okres_czasu"
  ]
  node [
    id 378
    label "riak"
  ]
  node [
    id 379
    label "schy&#322;ek"
  ]
  node [
    id 380
    label "okres_hesperyjski"
  ]
  node [
    id 381
    label "sylur"
  ]
  node [
    id 382
    label "dewon"
  ]
  node [
    id 383
    label "ciota"
  ]
  node [
    id 384
    label "epoka"
  ]
  node [
    id 385
    label "pierwszorz&#281;d"
  ]
  node [
    id 386
    label "okres_halsztacki"
  ]
  node [
    id 387
    label "ektas"
  ]
  node [
    id 388
    label "zdanie"
  ]
  node [
    id 389
    label "condition"
  ]
  node [
    id 390
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 391
    label "rok_akademicki"
  ]
  node [
    id 392
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 393
    label "postglacja&#322;"
  ]
  node [
    id 394
    label "faza"
  ]
  node [
    id 395
    label "proces_fizjologiczny"
  ]
  node [
    id 396
    label "ediakar"
  ]
  node [
    id 397
    label "time_period"
  ]
  node [
    id 398
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 399
    label "perm"
  ]
  node [
    id 400
    label "rok_szkolny"
  ]
  node [
    id 401
    label "neogen"
  ]
  node [
    id 402
    label "sider"
  ]
  node [
    id 403
    label "flow"
  ]
  node [
    id 404
    label "podokres"
  ]
  node [
    id 405
    label "preglacja&#322;"
  ]
  node [
    id 406
    label "retoryka"
  ]
  node [
    id 407
    label "choroba_przyrodzona"
  ]
  node [
    id 408
    label "monta&#380;"
  ]
  node [
    id 409
    label "fabrication"
  ]
  node [
    id 410
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 411
    label "performance"
  ]
  node [
    id 412
    label "dzie&#322;o"
  ]
  node [
    id 413
    label "proces"
  ]
  node [
    id 414
    label "postprodukcja"
  ]
  node [
    id 415
    label "scheduling"
  ]
  node [
    id 416
    label "operacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 201
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 207
  ]
  edge [
    source 17
    target 208
  ]
  edge [
    source 17
    target 209
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 212
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 17
    target 214
  ]
  edge [
    source 17
    target 215
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 217
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 219
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 229
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 232
  ]
  edge [
    source 17
    target 233
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 237
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 239
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 241
  ]
  edge [
    source 17
    target 242
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 245
  ]
  edge [
    source 17
    target 246
  ]
  edge [
    source 17
    target 247
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 249
  ]
  edge [
    source 17
    target 250
  ]
  edge [
    source 17
    target 251
  ]
  edge [
    source 17
    target 252
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 17
    target 254
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 256
  ]
  edge [
    source 17
    target 257
  ]
  edge [
    source 17
    target 258
  ]
  edge [
    source 17
    target 259
  ]
  edge [
    source 17
    target 260
  ]
  edge [
    source 17
    target 261
  ]
  edge [
    source 17
    target 262
  ]
  edge [
    source 17
    target 263
  ]
  edge [
    source 17
    target 264
  ]
  edge [
    source 17
    target 265
  ]
  edge [
    source 17
    target 266
  ]
  edge [
    source 17
    target 267
  ]
  edge [
    source 17
    target 268
  ]
  edge [
    source 17
    target 269
  ]
  edge [
    source 17
    target 270
  ]
  edge [
    source 17
    target 271
  ]
  edge [
    source 17
    target 272
  ]
  edge [
    source 17
    target 273
  ]
  edge [
    source 17
    target 274
  ]
  edge [
    source 17
    target 275
  ]
  edge [
    source 17
    target 276
  ]
  edge [
    source 17
    target 277
  ]
  edge [
    source 17
    target 278
  ]
  edge [
    source 17
    target 279
  ]
  edge [
    source 17
    target 280
  ]
  edge [
    source 17
    target 281
  ]
  edge [
    source 17
    target 282
  ]
  edge [
    source 17
    target 283
  ]
  edge [
    source 17
    target 284
  ]
  edge [
    source 17
    target 285
  ]
  edge [
    source 17
    target 286
  ]
  edge [
    source 17
    target 287
  ]
  edge [
    source 17
    target 288
  ]
  edge [
    source 17
    target 289
  ]
  edge [
    source 17
    target 290
  ]
  edge [
    source 17
    target 291
  ]
  edge [
    source 17
    target 292
  ]
  edge [
    source 17
    target 293
  ]
  edge [
    source 17
    target 294
  ]
  edge [
    source 17
    target 295
  ]
  edge [
    source 17
    target 296
  ]
  edge [
    source 17
    target 297
  ]
  edge [
    source 17
    target 298
  ]
  edge [
    source 17
    target 299
  ]
  edge [
    source 17
    target 300
  ]
  edge [
    source 17
    target 301
  ]
  edge [
    source 17
    target 302
  ]
  edge [
    source 17
    target 303
  ]
  edge [
    source 17
    target 304
  ]
  edge [
    source 17
    target 305
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 306
  ]
  edge [
    source 20
    target 307
  ]
  edge [
    source 20
    target 308
  ]
  edge [
    source 20
    target 309
  ]
  edge [
    source 20
    target 310
  ]
  edge [
    source 20
    target 311
  ]
  edge [
    source 20
    target 312
  ]
  edge [
    source 20
    target 313
  ]
  edge [
    source 20
    target 314
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 315
  ]
  edge [
    source 21
    target 316
  ]
  edge [
    source 21
    target 317
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 318
  ]
  edge [
    source 23
    target 319
  ]
  edge [
    source 23
    target 320
  ]
  edge [
    source 23
    target 321
  ]
  edge [
    source 23
    target 322
  ]
  edge [
    source 23
    target 323
  ]
  edge [
    source 23
    target 324
  ]
  edge [
    source 23
    target 325
  ]
  edge [
    source 23
    target 326
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 327
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 24
    target 328
  ]
  edge [
    source 24
    target 329
  ]
  edge [
    source 24
    target 330
  ]
  edge [
    source 24
    target 331
  ]
  edge [
    source 24
    target 332
  ]
  edge [
    source 24
    target 333
  ]
  edge [
    source 24
    target 334
  ]
  edge [
    source 24
    target 335
  ]
  edge [
    source 24
    target 336
  ]
  edge [
    source 24
    target 337
  ]
  edge [
    source 24
    target 338
  ]
  edge [
    source 24
    target 339
  ]
  edge [
    source 24
    target 340
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 341
  ]
  edge [
    source 25
    target 342
  ]
  edge [
    source 25
    target 152
  ]
  edge [
    source 25
    target 343
  ]
  edge [
    source 25
    target 344
  ]
  edge [
    source 25
    target 345
  ]
  edge [
    source 25
    target 346
  ]
  edge [
    source 25
    target 347
  ]
  edge [
    source 25
    target 348
  ]
  edge [
    source 25
    target 349
  ]
  edge [
    source 25
    target 350
  ]
  edge [
    source 25
    target 351
  ]
  edge [
    source 25
    target 352
  ]
  edge [
    source 25
    target 353
  ]
  edge [
    source 25
    target 354
  ]
  edge [
    source 25
    target 355
  ]
  edge [
    source 25
    target 356
  ]
  edge [
    source 25
    target 357
  ]
  edge [
    source 25
    target 358
  ]
  edge [
    source 25
    target 359
  ]
  edge [
    source 25
    target 360
  ]
  edge [
    source 25
    target 361
  ]
  edge [
    source 25
    target 362
  ]
  edge [
    source 25
    target 363
  ]
  edge [
    source 25
    target 364
  ]
  edge [
    source 25
    target 365
  ]
  edge [
    source 25
    target 366
  ]
  edge [
    source 25
    target 367
  ]
  edge [
    source 25
    target 368
  ]
  edge [
    source 25
    target 369
  ]
  edge [
    source 25
    target 370
  ]
  edge [
    source 25
    target 371
  ]
  edge [
    source 25
    target 372
  ]
  edge [
    source 25
    target 373
  ]
  edge [
    source 25
    target 374
  ]
  edge [
    source 25
    target 375
  ]
  edge [
    source 25
    target 376
  ]
  edge [
    source 25
    target 377
  ]
  edge [
    source 25
    target 378
  ]
  edge [
    source 25
    target 379
  ]
  edge [
    source 25
    target 380
  ]
  edge [
    source 25
    target 381
  ]
  edge [
    source 25
    target 382
  ]
  edge [
    source 25
    target 383
  ]
  edge [
    source 25
    target 384
  ]
  edge [
    source 25
    target 385
  ]
  edge [
    source 25
    target 386
  ]
  edge [
    source 25
    target 387
  ]
  edge [
    source 25
    target 388
  ]
  edge [
    source 25
    target 389
  ]
  edge [
    source 25
    target 390
  ]
  edge [
    source 25
    target 391
  ]
  edge [
    source 25
    target 392
  ]
  edge [
    source 25
    target 393
  ]
  edge [
    source 25
    target 394
  ]
  edge [
    source 25
    target 395
  ]
  edge [
    source 25
    target 396
  ]
  edge [
    source 25
    target 397
  ]
  edge [
    source 25
    target 398
  ]
  edge [
    source 25
    target 399
  ]
  edge [
    source 25
    target 400
  ]
  edge [
    source 25
    target 401
  ]
  edge [
    source 25
    target 402
  ]
  edge [
    source 25
    target 403
  ]
  edge [
    source 25
    target 404
  ]
  edge [
    source 25
    target 405
  ]
  edge [
    source 25
    target 406
  ]
  edge [
    source 25
    target 407
  ]
  edge [
    source 26
    target 408
  ]
  edge [
    source 26
    target 409
  ]
  edge [
    source 26
    target 410
  ]
  edge [
    source 26
    target 115
  ]
  edge [
    source 26
    target 411
  ]
  edge [
    source 26
    target 412
  ]
  edge [
    source 26
    target 413
  ]
  edge [
    source 26
    target 414
  ]
  edge [
    source 26
    target 415
  ]
  edge [
    source 26
    target 416
  ]
]
