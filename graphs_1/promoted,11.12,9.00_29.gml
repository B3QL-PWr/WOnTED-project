graph [
  maxDegree 17
  minDegree 1
  meanDegree 2
  density 0.02857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "kobieta"
    origin "text"
  ]
  node [
    id 1
    label "wyznaczy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nagroda"
    origin "text"
  ]
  node [
    id 3
    label "wskazanie"
    origin "text"
  ]
  node [
    id 4
    label "sprawca"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "zaatakowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "amstaff"
    origin "text"
  ]
  node [
    id 8
    label "spowodowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "fejsbukowy"
    origin "text"
  ]
  node [
    id 10
    label "lincz"
    origin "text"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "przekwitanie"
  ]
  node [
    id 13
    label "m&#281;&#380;yna"
  ]
  node [
    id 14
    label "babka"
  ]
  node [
    id 15
    label "samica"
  ]
  node [
    id 16
    label "doros&#322;y"
  ]
  node [
    id 17
    label "ulec"
  ]
  node [
    id 18
    label "uleganie"
  ]
  node [
    id 19
    label "partnerka"
  ]
  node [
    id 20
    label "&#380;ona"
  ]
  node [
    id 21
    label "ulega&#263;"
  ]
  node [
    id 22
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 23
    label "pa&#324;stwo"
  ]
  node [
    id 24
    label "ulegni&#281;cie"
  ]
  node [
    id 25
    label "menopauza"
  ]
  node [
    id 26
    label "&#322;ono"
  ]
  node [
    id 27
    label "aim"
  ]
  node [
    id 28
    label "okre&#347;li&#263;"
  ]
  node [
    id 29
    label "wybra&#263;"
  ]
  node [
    id 30
    label "sign"
  ]
  node [
    id 31
    label "position"
  ]
  node [
    id 32
    label "zaznaczy&#263;"
  ]
  node [
    id 33
    label "ustali&#263;"
  ]
  node [
    id 34
    label "set"
  ]
  node [
    id 35
    label "return"
  ]
  node [
    id 36
    label "konsekwencja"
  ]
  node [
    id 37
    label "oskar"
  ]
  node [
    id 38
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 39
    label "wynik"
  ]
  node [
    id 40
    label "podkre&#347;lenie"
  ]
  node [
    id 41
    label "wybranie"
  ]
  node [
    id 42
    label "appointment"
  ]
  node [
    id 43
    label "podanie"
  ]
  node [
    id 44
    label "education"
  ]
  node [
    id 45
    label "przyczyna"
  ]
  node [
    id 46
    label "meaning"
  ]
  node [
    id 47
    label "wskaz&#243;wka"
  ]
  node [
    id 48
    label "pokazanie"
  ]
  node [
    id 49
    label "wyja&#347;nienie"
  ]
  node [
    id 50
    label "sprawiciel"
  ]
  node [
    id 51
    label "sport"
  ]
  node [
    id 52
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 53
    label "skrytykowa&#263;"
  ]
  node [
    id 54
    label "spell"
  ]
  node [
    id 55
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 56
    label "powiedzie&#263;"
  ]
  node [
    id 57
    label "attack"
  ]
  node [
    id 58
    label "nast&#261;pi&#263;"
  ]
  node [
    id 59
    label "postara&#263;_si&#281;"
  ]
  node [
    id 60
    label "zrobi&#263;"
  ]
  node [
    id 61
    label "przeby&#263;"
  ]
  node [
    id 62
    label "anoint"
  ]
  node [
    id 63
    label "rozegra&#263;"
  ]
  node [
    id 64
    label "pies_rodzinny"
  ]
  node [
    id 65
    label "terier_w_typie_bull"
  ]
  node [
    id 66
    label "Bachus"
  ]
  node [
    id 67
    label "ast"
  ]
  node [
    id 68
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 69
    label "act"
  ]
  node [
    id 70
    label "samos&#261;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 70
  ]
]
