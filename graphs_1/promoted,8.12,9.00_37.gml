graph [
  maxDegree 22
  minDegree 1
  meanDegree 2
  density 0.024390243902439025
  graphCliqueNumber 2
  node [
    id 0
    label "jarmark"
    origin "text"
  ]
  node [
    id 1
    label "bo&#380;onarodzeniowy"
    origin "text"
  ]
  node [
    id 2
    label "kolonia"
    origin "text"
  ]
  node [
    id 3
    label "bez"
    origin "text"
  ]
  node [
    id 4
    label "w&#261;tpienie"
    origin "text"
  ]
  node [
    id 5
    label "jeden"
    origin "text"
  ]
  node [
    id 6
    label "dobry"
    origin "text"
  ]
  node [
    id 7
    label "europa"
    origin "text"
  ]
  node [
    id 8
    label "stoisko"
  ]
  node [
    id 9
    label "plac"
  ]
  node [
    id 10
    label "kram"
  ]
  node [
    id 11
    label "market"
  ]
  node [
    id 12
    label "obiekt_handlowy"
  ]
  node [
    id 13
    label "targowica"
  ]
  node [
    id 14
    label "targ"
  ]
  node [
    id 15
    label "enklawa"
  ]
  node [
    id 16
    label "colony"
  ]
  node [
    id 17
    label "posiad&#322;o&#347;&#263;"
  ]
  node [
    id 18
    label "skupienie"
  ]
  node [
    id 19
    label "Adampol"
  ]
  node [
    id 20
    label "Sahara_Zachodnia"
  ]
  node [
    id 21
    label "osiedle"
  ]
  node [
    id 22
    label "Malaje"
  ]
  node [
    id 23
    label "Zapora"
  ]
  node [
    id 24
    label "odpoczynek"
  ]
  node [
    id 25
    label "Holenderskie_Indie_Wschodnie"
  ]
  node [
    id 26
    label "Hiszpa&#324;skie_Indie_Wschodnie"
  ]
  node [
    id 27
    label "rodzina"
  ]
  node [
    id 28
    label "zbi&#243;r"
  ]
  node [
    id 29
    label "terytorium_zale&#380;ne"
  ]
  node [
    id 30
    label "emigracja"
  ]
  node [
    id 31
    label "grupa_organizm&#243;w"
  ]
  node [
    id 32
    label "Gibraltar"
  ]
  node [
    id 33
    label "Zgorzel"
  ]
  node [
    id 34
    label "osada"
  ]
  node [
    id 35
    label "ki&#347;&#263;"
  ]
  node [
    id 36
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 37
    label "krzew"
  ]
  node [
    id 38
    label "pi&#380;maczkowate"
  ]
  node [
    id 39
    label "pestkowiec"
  ]
  node [
    id 40
    label "kwiat"
  ]
  node [
    id 41
    label "owoc"
  ]
  node [
    id 42
    label "oliwkowate"
  ]
  node [
    id 43
    label "ro&#347;lina"
  ]
  node [
    id 44
    label "hy&#263;ka"
  ]
  node [
    id 45
    label "lilac"
  ]
  node [
    id 46
    label "delfinidyna"
  ]
  node [
    id 47
    label "w&#261;tpliwo&#347;&#263;"
  ]
  node [
    id 48
    label "doubt"
  ]
  node [
    id 49
    label "bycie"
  ]
  node [
    id 50
    label "kieliszek"
  ]
  node [
    id 51
    label "shot"
  ]
  node [
    id 52
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 53
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 54
    label "jaki&#347;"
  ]
  node [
    id 55
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 56
    label "jednolicie"
  ]
  node [
    id 57
    label "w&#243;dka"
  ]
  node [
    id 58
    label "ten"
  ]
  node [
    id 59
    label "ujednolicenie"
  ]
  node [
    id 60
    label "jednakowy"
  ]
  node [
    id 61
    label "pomy&#347;lny"
  ]
  node [
    id 62
    label "skuteczny"
  ]
  node [
    id 63
    label "moralny"
  ]
  node [
    id 64
    label "korzystny"
  ]
  node [
    id 65
    label "odpowiedni"
  ]
  node [
    id 66
    label "zwrot"
  ]
  node [
    id 67
    label "dobrze"
  ]
  node [
    id 68
    label "pozytywny"
  ]
  node [
    id 69
    label "grzeczny"
  ]
  node [
    id 70
    label "powitanie"
  ]
  node [
    id 71
    label "mi&#322;y"
  ]
  node [
    id 72
    label "dobroczynny"
  ]
  node [
    id 73
    label "pos&#322;uszny"
  ]
  node [
    id 74
    label "ca&#322;y"
  ]
  node [
    id 75
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 76
    label "czw&#243;rka"
  ]
  node [
    id 77
    label "spokojny"
  ]
  node [
    id 78
    label "&#347;mieszny"
  ]
  node [
    id 79
    label "drogi"
  ]
  node [
    id 80
    label "skrzat"
  ]
  node [
    id 81
    label "portowy"
  ]
  node [
    id 82
    label "anielski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
]
