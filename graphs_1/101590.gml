graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.794871794871795
  density 0.04723346828609987
  graphCliqueNumber 4
  node [
    id 0
    label "wicemarsza&#322;ek"
    origin "text"
  ]
  node [
    id 1
    label "krzysztof"
    origin "text"
  ]
  node [
    id 2
    label "putra"
    origin "text"
  ]
  node [
    id 3
    label "marsza&#322;ek"
  ]
  node [
    id 4
    label "zast&#281;pca"
  ]
  node [
    id 5
    label "Krzysztofa"
  ]
  node [
    id 6
    label "Putra"
  ]
  node [
    id 7
    label "Waldemar"
  ]
  node [
    id 8
    label "Andzel"
  ]
  node [
    id 9
    label "prawo"
  ]
  node [
    id 10
    label "i"
  ]
  node [
    id 11
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 12
    label "Stefan"
  ]
  node [
    id 13
    label "&#379;eromski"
  ]
  node [
    id 14
    label "g&#243;ry"
  ]
  node [
    id 15
    label "&#347;wi&#281;tokrzyski"
  ]
  node [
    id 16
    label "kielecki"
  ]
  node [
    id 17
    label "gimnazjum"
  ]
  node [
    id 18
    label "miejski"
  ]
  node [
    id 19
    label "szko&#322;a"
  ]
  node [
    id 20
    label "weterynaryjny"
  ]
  node [
    id 21
    label "Oktawia"
  ]
  node [
    id 22
    label "Rodkiewiczow&#261;"
  ]
  node [
    id 23
    label "zeszyt"
  ]
  node [
    id 24
    label "Radziwi&#322;&#322;owicz&#243;w"
  ]
  node [
    id 25
    label "uniwersytet"
  ]
  node [
    id 26
    label "ludowy"
  ]
  node [
    id 27
    label "biblioteka"
  ]
  node [
    id 28
    label "ordynacja"
  ]
  node [
    id 29
    label "zamojski"
  ]
  node [
    id 30
    label "Anna"
  ]
  node [
    id 31
    label "Zawadzka"
  ]
  node [
    id 32
    label "wojna"
  ]
  node [
    id 33
    label "&#347;wiatowy"
  ]
  node [
    id 34
    label "naczelny"
  ]
  node [
    id 35
    label "komitet"
  ]
  node [
    id 36
    label "zakopia&#324;ski"
  ]
  node [
    id 37
    label "narodowy"
  ]
  node [
    id 38
    label "rzeczpospolita"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 38
  ]
]
