graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.07407407407407407
  graphCliqueNumber 2
  node [
    id 0
    label "&#347;wie&#380;y"
    origin "text"
  ]
  node [
    id 1
    label "nagranie"
    origin "text"
  ]
  node [
    id 2
    label "orze&#378;wiaj&#261;co"
  ]
  node [
    id 3
    label "jasny"
  ]
  node [
    id 4
    label "&#347;wie&#380;o"
  ]
  node [
    id 5
    label "orze&#378;wienie"
  ]
  node [
    id 6
    label "o&#380;ywczy"
  ]
  node [
    id 7
    label "soczysty"
  ]
  node [
    id 8
    label "inny"
  ]
  node [
    id 9
    label "nowotny"
  ]
  node [
    id 10
    label "przyjemny"
  ]
  node [
    id 11
    label "zdrowy"
  ]
  node [
    id 12
    label "&#380;ywy"
  ]
  node [
    id 13
    label "czysty"
  ]
  node [
    id 14
    label "orze&#378;wianie"
  ]
  node [
    id 15
    label "oryginalnie"
  ]
  node [
    id 16
    label "m&#322;ody"
  ]
  node [
    id 17
    label "energiczny"
  ]
  node [
    id 18
    label "nowy"
  ]
  node [
    id 19
    label "dobry"
  ]
  node [
    id 20
    label "rze&#347;ki"
  ]
  node [
    id 21
    label "surowy"
  ]
  node [
    id 22
    label "wys&#322;uchanie"
  ]
  node [
    id 23
    label "wytw&#243;r"
  ]
  node [
    id 24
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 25
    label "recording"
  ]
  node [
    id 26
    label "utrwalenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
]
