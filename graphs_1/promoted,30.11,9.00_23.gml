graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "wysoce"
    origin "text"
  ]
  node [
    id 1
    label "wysoki"
  ]
  node [
    id 2
    label "intensywnie"
  ]
  node [
    id 3
    label "wielki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
]
