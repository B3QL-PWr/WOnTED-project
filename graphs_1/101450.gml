graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.4285714285714286
  density 0.23809523809523808
  graphCliqueNumber 3
  node [
    id 0
    label "piotr"
    origin "text"
  ]
  node [
    id 1
    label "fr&#261;ckiewicz"
    origin "text"
  ]
  node [
    id 2
    label "Piotr"
  ]
  node [
    id 3
    label "Fr&#261;ckiewicz"
  ]
  node [
    id 4
    label "zielony"
  ]
  node [
    id 5
    label "g&#243;ra"
  ]
  node [
    id 6
    label "ZLKLu"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
]
