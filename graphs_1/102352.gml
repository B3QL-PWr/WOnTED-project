graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.051813471502591
  density 0.010686528497409326
  graphCliqueNumber 2
  node [
    id 0
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pieprzy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wyzwoli&#263;"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "jednak&#380;e"
    origin "text"
  ]
  node [
    id 7
    label "g&#322;&#281;boki"
    origin "text"
  ]
  node [
    id 8
    label "moi"
    origin "text"
  ]
  node [
    id 9
    label "przekonanie"
    origin "text"
  ]
  node [
    id 10
    label "slogan"
    origin "text"
  ]
  node [
    id 11
    label "cie&#324;"
    origin "text"
  ]
  node [
    id 12
    label "sens"
    origin "text"
  ]
  node [
    id 13
    label "przestrzega&#263;"
    origin "text"
  ]
  node [
    id 14
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 15
    label "przed"
    origin "text"
  ]
  node [
    id 16
    label "nadu&#380;ywanie"
    origin "text"
  ]
  node [
    id 17
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 18
    label "teoretycznie"
    origin "text"
  ]
  node [
    id 19
    label "wymierzy&#263;"
    origin "text"
  ]
  node [
    id 20
    label "wiedza"
    origin "text"
  ]
  node [
    id 21
    label "tyle"
    origin "text"
  ]
  node [
    id 22
    label "przez"
    origin "text"
  ]
  node [
    id 23
    label "ujawni&#263;"
    origin "text"
  ]
  node [
    id 24
    label "prawda"
    origin "text"
  ]
  node [
    id 25
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 26
    label "si&#281;"
    origin "text"
  ]
  node [
    id 27
    label "bardzo"
    origin "text"
  ]
  node [
    id 28
    label "spodoba&#263;"
    origin "text"
  ]
  node [
    id 29
    label "dzisiejsi"
    origin "text"
  ]
  node [
    id 30
    label "arrywistom"
    origin "text"
  ]
  node [
    id 31
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 32
    label "sumienie"
    origin "text"
  ]
  node [
    id 33
    label "sam"
    origin "text"
  ]
  node [
    id 34
    label "tak"
    origin "text"
  ]
  node [
    id 35
    label "zabrudzi&#263;"
    origin "text"
  ]
  node [
    id 36
    label "cognizance"
  ]
  node [
    id 37
    label "jaki&#347;"
  ]
  node [
    id 38
    label "cause"
  ]
  node [
    id 39
    label "introduce"
  ]
  node [
    id 40
    label "begin"
  ]
  node [
    id 41
    label "odj&#261;&#263;"
  ]
  node [
    id 42
    label "post&#261;pi&#263;"
  ]
  node [
    id 43
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 44
    label "do"
  ]
  node [
    id 45
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 46
    label "zrobi&#263;"
  ]
  node [
    id 47
    label "talk_through_one's_hat"
  ]
  node [
    id 48
    label "ple&#347;&#263;"
  ]
  node [
    id 49
    label "przyprawia&#263;"
  ]
  node [
    id 50
    label "screw"
  ]
  node [
    id 51
    label "mie&#263;_gdzie&#347;"
  ]
  node [
    id 52
    label "bra&#263;"
  ]
  node [
    id 53
    label "pom&#243;c"
  ]
  node [
    id 54
    label "spowodowa&#263;"
  ]
  node [
    id 55
    label "arouse"
  ]
  node [
    id 56
    label "wywo&#322;a&#263;"
  ]
  node [
    id 57
    label "release"
  ]
  node [
    id 58
    label "deliver"
  ]
  node [
    id 59
    label "si&#281;ga&#263;"
  ]
  node [
    id 60
    label "trwa&#263;"
  ]
  node [
    id 61
    label "obecno&#347;&#263;"
  ]
  node [
    id 62
    label "stan"
  ]
  node [
    id 63
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 64
    label "stand"
  ]
  node [
    id 65
    label "mie&#263;_miejsce"
  ]
  node [
    id 66
    label "uczestniczy&#263;"
  ]
  node [
    id 67
    label "chodzi&#263;"
  ]
  node [
    id 68
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 69
    label "equal"
  ]
  node [
    id 70
    label "szczery"
  ]
  node [
    id 71
    label "silny"
  ]
  node [
    id 72
    label "dog&#322;&#281;bny"
  ]
  node [
    id 73
    label "daleki"
  ]
  node [
    id 74
    label "gruntowny"
  ]
  node [
    id 75
    label "niski"
  ]
  node [
    id 76
    label "wyrazisty"
  ]
  node [
    id 77
    label "intensywny"
  ]
  node [
    id 78
    label "niezrozumia&#322;y"
  ]
  node [
    id 79
    label "ukryty"
  ]
  node [
    id 80
    label "m&#261;dry"
  ]
  node [
    id 81
    label "mocny"
  ]
  node [
    id 82
    label "g&#322;&#281;boko"
  ]
  node [
    id 83
    label "zderzenie_si&#281;"
  ]
  node [
    id 84
    label "przes&#261;dny"
  ]
  node [
    id 85
    label "s&#261;d"
  ]
  node [
    id 86
    label "teoria_Arrheniusa"
  ]
  node [
    id 87
    label "teologicznie"
  ]
  node [
    id 88
    label "oddzia&#322;anie"
  ]
  node [
    id 89
    label "belief"
  ]
  node [
    id 90
    label "przekonanie_si&#281;"
  ]
  node [
    id 91
    label "view"
  ]
  node [
    id 92
    label "nak&#322;onienie"
  ]
  node [
    id 93
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 94
    label "postawa"
  ]
  node [
    id 95
    label "trywializm"
  ]
  node [
    id 96
    label "wypowied&#378;"
  ]
  node [
    id 97
    label "has&#322;o"
  ]
  node [
    id 98
    label "odrobina"
  ]
  node [
    id 99
    label "&#263;ma"
  ]
  node [
    id 100
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 101
    label "eyeshadow"
  ]
  node [
    id 102
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 103
    label "obw&#243;dka"
  ]
  node [
    id 104
    label "rzuca&#263;"
  ]
  node [
    id 105
    label "archetyp"
  ]
  node [
    id 106
    label "sowie_oczy"
  ]
  node [
    id 107
    label "wycieniowa&#263;"
  ]
  node [
    id 108
    label "rzucenie"
  ]
  node [
    id 109
    label "nekromancja"
  ]
  node [
    id 110
    label "ciemnota"
  ]
  node [
    id 111
    label "noktowizja"
  ]
  node [
    id 112
    label "Ereb"
  ]
  node [
    id 113
    label "cieniowa&#263;"
  ]
  node [
    id 114
    label "pomrok"
  ]
  node [
    id 115
    label "zjawisko"
  ]
  node [
    id 116
    label "duch"
  ]
  node [
    id 117
    label "zacienie"
  ]
  node [
    id 118
    label "zm&#281;czenie"
  ]
  node [
    id 119
    label "rzuci&#263;"
  ]
  node [
    id 120
    label "plama"
  ]
  node [
    id 121
    label "bearing"
  ]
  node [
    id 122
    label "zmar&#322;y"
  ]
  node [
    id 123
    label "shade"
  ]
  node [
    id 124
    label "kszta&#322;t"
  ]
  node [
    id 125
    label "rzucanie"
  ]
  node [
    id 126
    label "sylwetka"
  ]
  node [
    id 127
    label "miejsce"
  ]
  node [
    id 128
    label "kosmetyk_kolorowy"
  ]
  node [
    id 129
    label "oznaka"
  ]
  node [
    id 130
    label "zjawa"
  ]
  node [
    id 131
    label "przebarwienie"
  ]
  node [
    id 132
    label "cloud"
  ]
  node [
    id 133
    label "oko"
  ]
  node [
    id 134
    label "istota"
  ]
  node [
    id 135
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 136
    label "informacja"
  ]
  node [
    id 137
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 138
    label "hold"
  ]
  node [
    id 139
    label "zdyscyplinowanie"
  ]
  node [
    id 140
    label "uprzedza&#263;"
  ]
  node [
    id 141
    label "post"
  ]
  node [
    id 142
    label "caution"
  ]
  node [
    id 143
    label "dieta"
  ]
  node [
    id 144
    label "stosowanie"
  ]
  node [
    id 145
    label "teoretyczny"
  ]
  node [
    id 146
    label "nierealnie"
  ]
  node [
    id 147
    label "zada&#263;"
  ]
  node [
    id 148
    label "bargain"
  ]
  node [
    id 149
    label "rap"
  ]
  node [
    id 150
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 151
    label "deal"
  ]
  node [
    id 152
    label "level"
  ]
  node [
    id 153
    label "sztachn&#261;&#263;"
  ]
  node [
    id 154
    label "skierowa&#263;"
  ]
  node [
    id 155
    label "zmierzy&#263;"
  ]
  node [
    id 156
    label "przywali&#263;"
  ]
  node [
    id 157
    label "pozwolenie"
  ]
  node [
    id 158
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 159
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 160
    label "wykszta&#322;cenie"
  ]
  node [
    id 161
    label "zaawansowanie"
  ]
  node [
    id 162
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 163
    label "intelekt"
  ]
  node [
    id 164
    label "cognition"
  ]
  node [
    id 165
    label "wiele"
  ]
  node [
    id 166
    label "konkretnie"
  ]
  node [
    id 167
    label "nieznacznie"
  ]
  node [
    id 168
    label "dostrzec"
  ]
  node [
    id 169
    label "denounce"
  ]
  node [
    id 170
    label "discover"
  ]
  node [
    id 171
    label "objawi&#263;"
  ]
  node [
    id 172
    label "poinformowa&#263;"
  ]
  node [
    id 173
    label "truth"
  ]
  node [
    id 174
    label "nieprawdziwy"
  ]
  node [
    id 175
    label "za&#322;o&#380;enie"
  ]
  node [
    id 176
    label "prawdziwy"
  ]
  node [
    id 177
    label "realia"
  ]
  node [
    id 178
    label "w_chuj"
  ]
  node [
    id 179
    label "majority"
  ]
  node [
    id 180
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 181
    label "psychika"
  ]
  node [
    id 182
    label "sklep"
  ]
  node [
    id 183
    label "skali&#263;"
  ]
  node [
    id 184
    label "uwala&#263;"
  ]
  node [
    id 185
    label "upierdoli&#263;"
  ]
  node [
    id 186
    label "zaszkodzi&#263;"
  ]
  node [
    id 187
    label "ujeba&#263;"
  ]
  node [
    id 188
    label "zanieczy&#347;ci&#263;"
  ]
  node [
    id 189
    label "zeszmaci&#263;"
  ]
  node [
    id 190
    label "take_down"
  ]
  node [
    id 191
    label "smear"
  ]
  node [
    id 192
    label "zbruka&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 54
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 168
  ]
  edge [
    source 23
    target 169
  ]
  edge [
    source 23
    target 170
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 85
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 178
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 179
  ]
  edge [
    source 31
    target 180
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 162
  ]
  edge [
    source 32
    target 181
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 182
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 183
  ]
  edge [
    source 35
    target 184
  ]
  edge [
    source 35
    target 185
  ]
  edge [
    source 35
    target 186
  ]
  edge [
    source 35
    target 187
  ]
  edge [
    source 35
    target 188
  ]
  edge [
    source 35
    target 189
  ]
  edge [
    source 35
    target 190
  ]
  edge [
    source 35
    target 191
  ]
  edge [
    source 35
    target 192
  ]
]
