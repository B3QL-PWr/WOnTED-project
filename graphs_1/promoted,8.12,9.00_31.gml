graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.0172413793103448
  density 0.017541229385307347
  graphCliqueNumber 2
  node [
    id 0
    label "nadmiar"
    origin "text"
  ]
  node [
    id 1
    label "sol"
    origin "text"
  ]
  node [
    id 2
    label "dieta"
    origin "text"
  ]
  node [
    id 3
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 4
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 5
    label "negatywny"
    origin "text"
  ]
  node [
    id 6
    label "konsekwencja"
    origin "text"
  ]
  node [
    id 7
    label "dla"
    origin "text"
  ]
  node [
    id 8
    label "nasze"
    origin "text"
  ]
  node [
    id 9
    label "zdrowie"
    origin "text"
  ]
  node [
    id 10
    label "inny"
    origin "text"
  ]
  node [
    id 11
    label "uk&#322;ad"
    origin "text"
  ]
  node [
    id 12
    label "sercowo"
    origin "text"
  ]
  node [
    id 13
    label "przekarmia&#263;"
  ]
  node [
    id 14
    label "zbywa&#263;"
  ]
  node [
    id 15
    label "przekarmi&#263;"
  ]
  node [
    id 16
    label "surfeit"
  ]
  node [
    id 17
    label "ilo&#347;&#263;"
  ]
  node [
    id 18
    label "przekarmienie"
  ]
  node [
    id 19
    label "przekarmianie"
  ]
  node [
    id 20
    label "doba"
  ]
  node [
    id 21
    label "d&#378;wi&#281;k"
  ]
  node [
    id 22
    label "Peru"
  ]
  node [
    id 23
    label "jednostka_monetarna"
  ]
  node [
    id 24
    label "centym"
  ]
  node [
    id 25
    label "Mars"
  ]
  node [
    id 26
    label "zachowanie"
  ]
  node [
    id 27
    label "zachowa&#263;"
  ]
  node [
    id 28
    label "wynagrodzenie"
  ]
  node [
    id 29
    label "zachowywa&#263;"
  ]
  node [
    id 30
    label "r&#243;wnowarto&#347;&#263;"
  ]
  node [
    id 31
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 32
    label "chart"
  ]
  node [
    id 33
    label "regimen"
  ]
  node [
    id 34
    label "zachowywanie"
  ]
  node [
    id 35
    label "terapia"
  ]
  node [
    id 36
    label "spos&#243;b"
  ]
  node [
    id 37
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 38
    label "czu&#263;"
  ]
  node [
    id 39
    label "need"
  ]
  node [
    id 40
    label "hide"
  ]
  node [
    id 41
    label "support"
  ]
  node [
    id 42
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 43
    label "negatywnie"
  ]
  node [
    id 44
    label "syf"
  ]
  node [
    id 45
    label "&#378;le"
  ]
  node [
    id 46
    label "ujemnie"
  ]
  node [
    id 47
    label "nieprzyjemny"
  ]
  node [
    id 48
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 49
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 50
    label "skrupianie_si&#281;"
  ]
  node [
    id 51
    label "odczuwanie"
  ]
  node [
    id 52
    label "skrupienie_si&#281;"
  ]
  node [
    id 53
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 54
    label "odczucie"
  ]
  node [
    id 55
    label "odczuwa&#263;"
  ]
  node [
    id 56
    label "odczu&#263;"
  ]
  node [
    id 57
    label "event"
  ]
  node [
    id 58
    label "rezultat"
  ]
  node [
    id 59
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 60
    label "koszula_Dejaniry"
  ]
  node [
    id 61
    label "os&#322;abia&#263;"
  ]
  node [
    id 62
    label "niszczy&#263;"
  ]
  node [
    id 63
    label "zniszczy&#263;"
  ]
  node [
    id 64
    label "stan"
  ]
  node [
    id 65
    label "firmness"
  ]
  node [
    id 66
    label "kondycja"
  ]
  node [
    id 67
    label "zniszczenie"
  ]
  node [
    id 68
    label "rozsypanie_si&#281;"
  ]
  node [
    id 69
    label "os&#322;abi&#263;"
  ]
  node [
    id 70
    label "cecha"
  ]
  node [
    id 71
    label "zdarcie"
  ]
  node [
    id 72
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 73
    label "zedrze&#263;"
  ]
  node [
    id 74
    label "niszczenie"
  ]
  node [
    id 75
    label "os&#322;abienie"
  ]
  node [
    id 76
    label "soundness"
  ]
  node [
    id 77
    label "os&#322;abianie"
  ]
  node [
    id 78
    label "kolejny"
  ]
  node [
    id 79
    label "inaczej"
  ]
  node [
    id 80
    label "r&#243;&#380;ny"
  ]
  node [
    id 81
    label "inszy"
  ]
  node [
    id 82
    label "osobno"
  ]
  node [
    id 83
    label "sk&#322;ad"
  ]
  node [
    id 84
    label "umowa"
  ]
  node [
    id 85
    label "podsystem"
  ]
  node [
    id 86
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 87
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 88
    label "system"
  ]
  node [
    id 89
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 90
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 91
    label "struktura"
  ]
  node [
    id 92
    label "wi&#281;&#378;"
  ]
  node [
    id 93
    label "zawarcie"
  ]
  node [
    id 94
    label "systemat"
  ]
  node [
    id 95
    label "usenet"
  ]
  node [
    id 96
    label "ONZ"
  ]
  node [
    id 97
    label "o&#347;"
  ]
  node [
    id 98
    label "organ"
  ]
  node [
    id 99
    label "przestawi&#263;"
  ]
  node [
    id 100
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 101
    label "traktat_wersalski"
  ]
  node [
    id 102
    label "rozprz&#261;c"
  ]
  node [
    id 103
    label "cybernetyk"
  ]
  node [
    id 104
    label "cia&#322;o"
  ]
  node [
    id 105
    label "zawrze&#263;"
  ]
  node [
    id 106
    label "konstelacja"
  ]
  node [
    id 107
    label "alliance"
  ]
  node [
    id 108
    label "zbi&#243;r"
  ]
  node [
    id 109
    label "NATO"
  ]
  node [
    id 110
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 111
    label "treaty"
  ]
  node [
    id 112
    label "uczuciowo"
  ]
  node [
    id 113
    label "sercowy"
  ]
  node [
    id 114
    label "&#380;yczliwie"
  ]
  node [
    id 115
    label "sercowaty"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
]
