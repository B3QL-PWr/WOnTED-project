graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.5
  density 0.21428571428571427
  graphCliqueNumber 2
  node [
    id 0
    label "flaga"
    origin "text"
  ]
  node [
    id 1
    label "odessa"
    origin "text"
  ]
  node [
    id 2
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 3
    label "flag"
  ]
  node [
    id 4
    label "transparent"
  ]
  node [
    id 5
    label "oznaka"
  ]
  node [
    id 6
    label "rada"
  ]
  node [
    id 7
    label "miasto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 6
    target 7
  ]
]
