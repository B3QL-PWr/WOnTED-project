graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9285714285714286
  density 0.07142857142857142
  graphCliqueNumber 2
  node [
    id 0
    label "dobry"
    origin "text"
  ]
  node [
    id 1
    label "wiecz&#243;r"
    origin "text"
  ]
  node [
    id 2
    label "pomy&#347;lny"
  ]
  node [
    id 3
    label "skuteczny"
  ]
  node [
    id 4
    label "moralny"
  ]
  node [
    id 5
    label "korzystny"
  ]
  node [
    id 6
    label "odpowiedni"
  ]
  node [
    id 7
    label "zwrot"
  ]
  node [
    id 8
    label "dobrze"
  ]
  node [
    id 9
    label "pozytywny"
  ]
  node [
    id 10
    label "grzeczny"
  ]
  node [
    id 11
    label "powitanie"
  ]
  node [
    id 12
    label "mi&#322;y"
  ]
  node [
    id 13
    label "dobroczynny"
  ]
  node [
    id 14
    label "pos&#322;uszny"
  ]
  node [
    id 15
    label "ca&#322;y"
  ]
  node [
    id 16
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 17
    label "czw&#243;rka"
  ]
  node [
    id 18
    label "spokojny"
  ]
  node [
    id 19
    label "&#347;mieszny"
  ]
  node [
    id 20
    label "drogi"
  ]
  node [
    id 21
    label "zach&#243;d"
  ]
  node [
    id 22
    label "vesper"
  ]
  node [
    id 23
    label "spotkanie"
  ]
  node [
    id 24
    label "przyj&#281;cie"
  ]
  node [
    id 25
    label "pora"
  ]
  node [
    id 26
    label "dzie&#324;"
  ]
  node [
    id 27
    label "night"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
]
