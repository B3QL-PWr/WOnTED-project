graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "prawda"
    origin "text"
  ]
  node [
    id 2
    label "s&#261;d"
  ]
  node [
    id 3
    label "truth"
  ]
  node [
    id 4
    label "nieprawdziwy"
  ]
  node [
    id 5
    label "za&#322;o&#380;enie"
  ]
  node [
    id 6
    label "prawdziwy"
  ]
  node [
    id 7
    label "realia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
]
