graph [
  maxDegree 65
  minDegree 1
  meanDegree 2.0350877192982457
  density 0.01197110423116615
  graphCliqueNumber 4
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "linia"
    origin "text"
  ]
  node [
    id 2
    label "lotniczy"
    origin "text"
  ]
  node [
    id 3
    label "lot"
    origin "text"
  ]
  node [
    id 4
    label "numer"
    origin "text"
  ]
  node [
    id 5
    label "jeden"
    origin "text"
  ]
  node [
    id 6
    label "ranking"
    origin "text"
  ]
  node [
    id 7
    label "cichy"
    origin "text"
  ]
  node [
    id 8
    label "przewo&#378;nik"
    origin "text"
  ]
  node [
    id 9
    label "londy&#324;ski"
    origin "text"
  ]
  node [
    id 10
    label "lotnisko"
    origin "text"
  ]
  node [
    id 11
    label "heathrow"
    origin "text"
  ]
  node [
    id 12
    label "lacki"
  ]
  node [
    id 13
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 14
    label "przedmiot"
  ]
  node [
    id 15
    label "sztajer"
  ]
  node [
    id 16
    label "drabant"
  ]
  node [
    id 17
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 18
    label "polak"
  ]
  node [
    id 19
    label "pierogi_ruskie"
  ]
  node [
    id 20
    label "krakowiak"
  ]
  node [
    id 21
    label "Polish"
  ]
  node [
    id 22
    label "j&#281;zyk"
  ]
  node [
    id 23
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 24
    label "oberek"
  ]
  node [
    id 25
    label "po_polsku"
  ]
  node [
    id 26
    label "mazur"
  ]
  node [
    id 27
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 28
    label "chodzony"
  ]
  node [
    id 29
    label "skoczny"
  ]
  node [
    id 30
    label "ryba_po_grecku"
  ]
  node [
    id 31
    label "goniony"
  ]
  node [
    id 32
    label "polsko"
  ]
  node [
    id 33
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "koniec"
  ]
  node [
    id 36
    label "uporz&#261;dkowanie"
  ]
  node [
    id 37
    label "coalescence"
  ]
  node [
    id 38
    label "rz&#261;d"
  ]
  node [
    id 39
    label "grupa_organizm&#243;w"
  ]
  node [
    id 40
    label "curve"
  ]
  node [
    id 41
    label "kompleksja"
  ]
  node [
    id 42
    label "access"
  ]
  node [
    id 43
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 44
    label "tekst"
  ]
  node [
    id 45
    label "fragment"
  ]
  node [
    id 46
    label "cord"
  ]
  node [
    id 47
    label "budowa"
  ]
  node [
    id 48
    label "granica"
  ]
  node [
    id 49
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 50
    label "szpaler"
  ]
  node [
    id 51
    label "phreaker"
  ]
  node [
    id 52
    label "tract"
  ]
  node [
    id 53
    label "sztrych"
  ]
  node [
    id 54
    label "kontakt"
  ]
  node [
    id 55
    label "spos&#243;b"
  ]
  node [
    id 56
    label "przystanek"
  ]
  node [
    id 57
    label "prowadzi&#263;"
  ]
  node [
    id 58
    label "point"
  ]
  node [
    id 59
    label "materia&#322;_zecerski"
  ]
  node [
    id 60
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 61
    label "linijka"
  ]
  node [
    id 62
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 63
    label "po&#322;&#261;czenie"
  ]
  node [
    id 64
    label "cecha"
  ]
  node [
    id 65
    label "transporter"
  ]
  node [
    id 66
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 67
    label "przeorientowywa&#263;"
  ]
  node [
    id 68
    label "bearing"
  ]
  node [
    id 69
    label "line"
  ]
  node [
    id 70
    label "trasa"
  ]
  node [
    id 71
    label "przew&#243;d"
  ]
  node [
    id 72
    label "figura_geometryczna"
  ]
  node [
    id 73
    label "kszta&#322;t"
  ]
  node [
    id 74
    label "drzewo_genealogiczne"
  ]
  node [
    id 75
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 76
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 77
    label "wygl&#261;d"
  ]
  node [
    id 78
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 79
    label "poprowadzi&#263;"
  ]
  node [
    id 80
    label "armia"
  ]
  node [
    id 81
    label "szczep"
  ]
  node [
    id 82
    label "Ural"
  ]
  node [
    id 83
    label "przeorientowa&#263;"
  ]
  node [
    id 84
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 85
    label "granice"
  ]
  node [
    id 86
    label "przeorientowanie"
  ]
  node [
    id 87
    label "billing"
  ]
  node [
    id 88
    label "prowadzenie"
  ]
  node [
    id 89
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 90
    label "zbi&#243;r"
  ]
  node [
    id 91
    label "przeorientowywanie"
  ]
  node [
    id 92
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 93
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 94
    label "jard"
  ]
  node [
    id 95
    label "chronometra&#380;ysta"
  ]
  node [
    id 96
    label "start"
  ]
  node [
    id 97
    label "ruch"
  ]
  node [
    id 98
    label "ci&#261;g"
  ]
  node [
    id 99
    label "l&#261;dowanie"
  ]
  node [
    id 100
    label "odlot"
  ]
  node [
    id 101
    label "podr&#243;&#380;"
  ]
  node [
    id 102
    label "flight"
  ]
  node [
    id 103
    label "manewr"
  ]
  node [
    id 104
    label "sztos"
  ]
  node [
    id 105
    label "pok&#243;j"
  ]
  node [
    id 106
    label "facet"
  ]
  node [
    id 107
    label "wyst&#281;p"
  ]
  node [
    id 108
    label "turn"
  ]
  node [
    id 109
    label "impression"
  ]
  node [
    id 110
    label "hotel"
  ]
  node [
    id 111
    label "liczba"
  ]
  node [
    id 112
    label "punkt"
  ]
  node [
    id 113
    label "czasopismo"
  ]
  node [
    id 114
    label "&#380;art"
  ]
  node [
    id 115
    label "orygina&#322;"
  ]
  node [
    id 116
    label "oznaczenie"
  ]
  node [
    id 117
    label "zi&#243;&#322;ko"
  ]
  node [
    id 118
    label "akt_p&#322;ciowy"
  ]
  node [
    id 119
    label "publikacja"
  ]
  node [
    id 120
    label "kieliszek"
  ]
  node [
    id 121
    label "shot"
  ]
  node [
    id 122
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 123
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 124
    label "jaki&#347;"
  ]
  node [
    id 125
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 126
    label "jednolicie"
  ]
  node [
    id 127
    label "w&#243;dka"
  ]
  node [
    id 128
    label "ten"
  ]
  node [
    id 129
    label "ujednolicenie"
  ]
  node [
    id 130
    label "jednakowy"
  ]
  node [
    id 131
    label "lista_rankingowa"
  ]
  node [
    id 132
    label "klasyfikacja"
  ]
  node [
    id 133
    label "skromny"
  ]
  node [
    id 134
    label "trusia"
  ]
  node [
    id 135
    label "s&#322;aby"
  ]
  node [
    id 136
    label "skryty"
  ]
  node [
    id 137
    label "zamazywanie"
  ]
  node [
    id 138
    label "niemy"
  ]
  node [
    id 139
    label "przycichni&#281;cie"
  ]
  node [
    id 140
    label "zamazanie"
  ]
  node [
    id 141
    label "cicho"
  ]
  node [
    id 142
    label "ucichni&#281;cie"
  ]
  node [
    id 143
    label "uciszanie"
  ]
  node [
    id 144
    label "uciszenie"
  ]
  node [
    id 145
    label "cichni&#281;cie"
  ]
  node [
    id 146
    label "przycichanie"
  ]
  node [
    id 147
    label "niezauwa&#380;alny"
  ]
  node [
    id 148
    label "tajemniczy"
  ]
  node [
    id 149
    label "spokojny"
  ]
  node [
    id 150
    label "podst&#281;pny"
  ]
  node [
    id 151
    label "t&#322;umienie"
  ]
  node [
    id 152
    label "firma"
  ]
  node [
    id 153
    label "transportowiec"
  ]
  node [
    id 154
    label "angielski"
  ]
  node [
    id 155
    label "po_londy&#324;sku"
  ]
  node [
    id 156
    label "brytyjski"
  ]
  node [
    id 157
    label "hala"
  ]
  node [
    id 158
    label "droga_ko&#322;owania"
  ]
  node [
    id 159
    label "baza"
  ]
  node [
    id 160
    label "p&#322;yta_postojowa"
  ]
  node [
    id 161
    label "wie&#380;a_kontroli_lot&#243;w"
  ]
  node [
    id 162
    label "betonka"
  ]
  node [
    id 163
    label "budowla"
  ]
  node [
    id 164
    label "aerodrom"
  ]
  node [
    id 165
    label "pas_startowy"
  ]
  node [
    id 166
    label "terminal"
  ]
  node [
    id 167
    label "Max"
  ]
  node [
    id 168
    label "8"
  ]
  node [
    id 169
    label "boeing"
  ]
  node [
    id 170
    label "737"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 167
    target 169
  ]
  edge [
    source 167
    target 170
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 168
    target 170
  ]
  edge [
    source 169
    target 170
  ]
]
