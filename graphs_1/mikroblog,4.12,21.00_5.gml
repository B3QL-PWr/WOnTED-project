graph [
  maxDegree 15
  minDegree 1
  meanDegree 2
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "s&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "g&#243;wno"
    origin "text"
  ]
  node [
    id 2
    label "odpowiada&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wsp&#243;&#322;rz&#261;dzi&#263;"
  ]
  node [
    id 4
    label "hold"
  ]
  node [
    id 5
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 6
    label "my&#347;le&#263;"
  ]
  node [
    id 7
    label "sprawowa&#263;"
  ]
  node [
    id 8
    label "os&#261;dza&#263;"
  ]
  node [
    id 9
    label "robi&#263;"
  ]
  node [
    id 10
    label "zas&#261;dza&#263;"
  ]
  node [
    id 11
    label "powodowa&#263;"
  ]
  node [
    id 12
    label "deliver"
  ]
  node [
    id 13
    label "ka&#322;"
  ]
  node [
    id 14
    label "zero"
  ]
  node [
    id 15
    label "drobiazg"
  ]
  node [
    id 16
    label "tandeta"
  ]
  node [
    id 17
    label "report"
  ]
  node [
    id 18
    label "by&#263;"
  ]
  node [
    id 19
    label "dawa&#263;"
  ]
  node [
    id 20
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 21
    label "reagowa&#263;"
  ]
  node [
    id 22
    label "contend"
  ]
  node [
    id 23
    label "ponosi&#263;"
  ]
  node [
    id 24
    label "impart"
  ]
  node [
    id 25
    label "react"
  ]
  node [
    id 26
    label "tone"
  ]
  node [
    id 27
    label "equate"
  ]
  node [
    id 28
    label "pytanie"
  ]
  node [
    id 29
    label "answer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 29
  ]
]
