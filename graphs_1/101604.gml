graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.0184757505773674
  density 0.0046723975707809425
  graphCliqueNumber 3
  node [
    id 0
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 1
    label "tema"
    origin "text"
  ]
  node [
    id 2
    label "wina"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "moja"
    origin "text"
  ]
  node [
    id 5
    label "m&#322;odo&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "tychy"
    origin "text"
  ]
  node [
    id 7
    label "gr&#243;b"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "kraj"
    origin "text"
  ]
  node [
    id 10
    label "mno&#380;y&#263;"
    origin "text"
  ]
  node [
    id 11
    label "ten"
    origin "text"
  ]
  node [
    id 12
    label "wieczny"
    origin "text"
  ]
  node [
    id 13
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 14
    label "samotno&#347;&#263;"
    origin "text"
  ]
  node [
    id 15
    label "Cesarstwo_Bizanty&#324;skie"
  ]
  node [
    id 16
    label "jednostka_administracyjna"
  ]
  node [
    id 17
    label "wstyd"
  ]
  node [
    id 18
    label "konsekwencja"
  ]
  node [
    id 19
    label "guilt"
  ]
  node [
    id 20
    label "lutnia"
  ]
  node [
    id 21
    label "si&#281;ga&#263;"
  ]
  node [
    id 22
    label "trwa&#263;"
  ]
  node [
    id 23
    label "obecno&#347;&#263;"
  ]
  node [
    id 24
    label "stan"
  ]
  node [
    id 25
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 26
    label "stand"
  ]
  node [
    id 27
    label "mie&#263;_miejsce"
  ]
  node [
    id 28
    label "uczestniczy&#263;"
  ]
  node [
    id 29
    label "chodzi&#263;"
  ]
  node [
    id 30
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 31
    label "equal"
  ]
  node [
    id 32
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 33
    label "adolescence"
  ]
  node [
    id 34
    label "wiek"
  ]
  node [
    id 35
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 36
    label "zielone_lata"
  ]
  node [
    id 37
    label "defenestracja"
  ]
  node [
    id 38
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 39
    label "nagrobek"
  ]
  node [
    id 40
    label "kres"
  ]
  node [
    id 41
    label "agonia"
  ]
  node [
    id 42
    label "spocz&#281;cie"
  ]
  node [
    id 43
    label "szeol"
  ]
  node [
    id 44
    label "spoczywanie"
  ]
  node [
    id 45
    label "mogi&#322;a"
  ]
  node [
    id 46
    label "spocz&#261;&#263;"
  ]
  node [
    id 47
    label "pochowanie"
  ]
  node [
    id 48
    label "pomnik"
  ]
  node [
    id 49
    label "prochowisko"
  ]
  node [
    id 50
    label "pogrzebanie"
  ]
  node [
    id 51
    label "chowanie"
  ]
  node [
    id 52
    label "park_sztywnych"
  ]
  node [
    id 53
    label "&#380;a&#322;oba"
  ]
  node [
    id 54
    label "zabicie"
  ]
  node [
    id 55
    label "spoczywa&#263;"
  ]
  node [
    id 56
    label "kres_&#380;ycia"
  ]
  node [
    id 57
    label "Skandynawia"
  ]
  node [
    id 58
    label "Rwanda"
  ]
  node [
    id 59
    label "Filipiny"
  ]
  node [
    id 60
    label "Yorkshire"
  ]
  node [
    id 61
    label "Kaukaz"
  ]
  node [
    id 62
    label "Podbeskidzie"
  ]
  node [
    id 63
    label "Toskania"
  ]
  node [
    id 64
    label "&#321;emkowszczyzna"
  ]
  node [
    id 65
    label "obszar"
  ]
  node [
    id 66
    label "Monako"
  ]
  node [
    id 67
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 68
    label "Amhara"
  ]
  node [
    id 69
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 70
    label "Lombardia"
  ]
  node [
    id 71
    label "Korea"
  ]
  node [
    id 72
    label "Kalabria"
  ]
  node [
    id 73
    label "Czarnog&#243;ra"
  ]
  node [
    id 74
    label "Ghana"
  ]
  node [
    id 75
    label "Tyrol"
  ]
  node [
    id 76
    label "Malawi"
  ]
  node [
    id 77
    label "Indonezja"
  ]
  node [
    id 78
    label "Bu&#322;garia"
  ]
  node [
    id 79
    label "Nauru"
  ]
  node [
    id 80
    label "Kenia"
  ]
  node [
    id 81
    label "Pamir"
  ]
  node [
    id 82
    label "Kambod&#380;a"
  ]
  node [
    id 83
    label "Lubelszczyzna"
  ]
  node [
    id 84
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 85
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 86
    label "Mali"
  ]
  node [
    id 87
    label "&#379;ywiecczyzna"
  ]
  node [
    id 88
    label "Austria"
  ]
  node [
    id 89
    label "interior"
  ]
  node [
    id 90
    label "Europa_Wschodnia"
  ]
  node [
    id 91
    label "Armenia"
  ]
  node [
    id 92
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 93
    label "Fid&#380;i"
  ]
  node [
    id 94
    label "Tuwalu"
  ]
  node [
    id 95
    label "Zabajkale"
  ]
  node [
    id 96
    label "Etiopia"
  ]
  node [
    id 97
    label "Malezja"
  ]
  node [
    id 98
    label "Malta"
  ]
  node [
    id 99
    label "Kaszuby"
  ]
  node [
    id 100
    label "Noworosja"
  ]
  node [
    id 101
    label "Bo&#347;nia"
  ]
  node [
    id 102
    label "Tad&#380;ykistan"
  ]
  node [
    id 103
    label "Grenada"
  ]
  node [
    id 104
    label "Ba&#322;kany"
  ]
  node [
    id 105
    label "Wehrlen"
  ]
  node [
    id 106
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 107
    label "Anglia"
  ]
  node [
    id 108
    label "Kielecczyzna"
  ]
  node [
    id 109
    label "Rumunia"
  ]
  node [
    id 110
    label "Pomorze_Zachodnie"
  ]
  node [
    id 111
    label "Maroko"
  ]
  node [
    id 112
    label "Bhutan"
  ]
  node [
    id 113
    label "Opolskie"
  ]
  node [
    id 114
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 115
    label "Ko&#322;yma"
  ]
  node [
    id 116
    label "Oksytania"
  ]
  node [
    id 117
    label "S&#322;owacja"
  ]
  node [
    id 118
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 119
    label "Seszele"
  ]
  node [
    id 120
    label "Syjon"
  ]
  node [
    id 121
    label "Kuwejt"
  ]
  node [
    id 122
    label "Arabia_Saudyjska"
  ]
  node [
    id 123
    label "Kociewie"
  ]
  node [
    id 124
    label "Kanada"
  ]
  node [
    id 125
    label "Ekwador"
  ]
  node [
    id 126
    label "ziemia"
  ]
  node [
    id 127
    label "Japonia"
  ]
  node [
    id 128
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 129
    label "Hiszpania"
  ]
  node [
    id 130
    label "Wyspy_Marshalla"
  ]
  node [
    id 131
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 132
    label "D&#380;ibuti"
  ]
  node [
    id 133
    label "Botswana"
  ]
  node [
    id 134
    label "Huculszczyzna"
  ]
  node [
    id 135
    label "Wietnam"
  ]
  node [
    id 136
    label "Egipt"
  ]
  node [
    id 137
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 138
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 139
    label "Burkina_Faso"
  ]
  node [
    id 140
    label "Bawaria"
  ]
  node [
    id 141
    label "Niemcy"
  ]
  node [
    id 142
    label "Khitai"
  ]
  node [
    id 143
    label "Macedonia"
  ]
  node [
    id 144
    label "Albania"
  ]
  node [
    id 145
    label "Madagaskar"
  ]
  node [
    id 146
    label "Bahrajn"
  ]
  node [
    id 147
    label "Jemen"
  ]
  node [
    id 148
    label "Lesoto"
  ]
  node [
    id 149
    label "Maghreb"
  ]
  node [
    id 150
    label "Samoa"
  ]
  node [
    id 151
    label "Andora"
  ]
  node [
    id 152
    label "Bory_Tucholskie"
  ]
  node [
    id 153
    label "Chiny"
  ]
  node [
    id 154
    label "Europa_Zachodnia"
  ]
  node [
    id 155
    label "Cypr"
  ]
  node [
    id 156
    label "Wielka_Brytania"
  ]
  node [
    id 157
    label "Kerala"
  ]
  node [
    id 158
    label "Podhale"
  ]
  node [
    id 159
    label "Kabylia"
  ]
  node [
    id 160
    label "Ukraina"
  ]
  node [
    id 161
    label "Paragwaj"
  ]
  node [
    id 162
    label "Trynidad_i_Tobago"
  ]
  node [
    id 163
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 164
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 165
    label "Ma&#322;opolska"
  ]
  node [
    id 166
    label "Polesie"
  ]
  node [
    id 167
    label "Liguria"
  ]
  node [
    id 168
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 169
    label "Libia"
  ]
  node [
    id 170
    label "&#321;&#243;dzkie"
  ]
  node [
    id 171
    label "Surinam"
  ]
  node [
    id 172
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 173
    label "Palestyna"
  ]
  node [
    id 174
    label "Nigeria"
  ]
  node [
    id 175
    label "Australia"
  ]
  node [
    id 176
    label "Honduras"
  ]
  node [
    id 177
    label "Bojkowszczyzna"
  ]
  node [
    id 178
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 179
    label "Karaiby"
  ]
  node [
    id 180
    label "Peru"
  ]
  node [
    id 181
    label "USA"
  ]
  node [
    id 182
    label "Bangladesz"
  ]
  node [
    id 183
    label "Kazachstan"
  ]
  node [
    id 184
    label "Nepal"
  ]
  node [
    id 185
    label "Irak"
  ]
  node [
    id 186
    label "Nadrenia"
  ]
  node [
    id 187
    label "Sudan"
  ]
  node [
    id 188
    label "S&#261;decczyzna"
  ]
  node [
    id 189
    label "Sand&#380;ak"
  ]
  node [
    id 190
    label "San_Marino"
  ]
  node [
    id 191
    label "Burundi"
  ]
  node [
    id 192
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 193
    label "Dominikana"
  ]
  node [
    id 194
    label "Komory"
  ]
  node [
    id 195
    label "Zakarpacie"
  ]
  node [
    id 196
    label "Gwatemala"
  ]
  node [
    id 197
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 198
    label "Zag&#243;rze"
  ]
  node [
    id 199
    label "Andaluzja"
  ]
  node [
    id 200
    label "granica_pa&#324;stwa"
  ]
  node [
    id 201
    label "Turkiestan"
  ]
  node [
    id 202
    label "Naddniestrze"
  ]
  node [
    id 203
    label "Hercegowina"
  ]
  node [
    id 204
    label "Brunei"
  ]
  node [
    id 205
    label "Iran"
  ]
  node [
    id 206
    label "Zimbabwe"
  ]
  node [
    id 207
    label "Namibia"
  ]
  node [
    id 208
    label "Meksyk"
  ]
  node [
    id 209
    label "Opolszczyzna"
  ]
  node [
    id 210
    label "Kamerun"
  ]
  node [
    id 211
    label "Afryka_Wschodnia"
  ]
  node [
    id 212
    label "Szlezwik"
  ]
  node [
    id 213
    label "Lotaryngia"
  ]
  node [
    id 214
    label "Somalia"
  ]
  node [
    id 215
    label "Angola"
  ]
  node [
    id 216
    label "Gabon"
  ]
  node [
    id 217
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 218
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 219
    label "Nowa_Zelandia"
  ]
  node [
    id 220
    label "Mozambik"
  ]
  node [
    id 221
    label "Tunezja"
  ]
  node [
    id 222
    label "Tajwan"
  ]
  node [
    id 223
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 224
    label "Liban"
  ]
  node [
    id 225
    label "Jordania"
  ]
  node [
    id 226
    label "Tonga"
  ]
  node [
    id 227
    label "Czad"
  ]
  node [
    id 228
    label "Gwinea"
  ]
  node [
    id 229
    label "Liberia"
  ]
  node [
    id 230
    label "Belize"
  ]
  node [
    id 231
    label "Mazowsze"
  ]
  node [
    id 232
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 233
    label "Benin"
  ]
  node [
    id 234
    label "&#321;otwa"
  ]
  node [
    id 235
    label "Syria"
  ]
  node [
    id 236
    label "Afryka_Zachodnia"
  ]
  node [
    id 237
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 238
    label "Dominika"
  ]
  node [
    id 239
    label "Antigua_i_Barbuda"
  ]
  node [
    id 240
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 241
    label "Hanower"
  ]
  node [
    id 242
    label "Galicja"
  ]
  node [
    id 243
    label "Szkocja"
  ]
  node [
    id 244
    label "Walia"
  ]
  node [
    id 245
    label "Afganistan"
  ]
  node [
    id 246
    label "W&#322;ochy"
  ]
  node [
    id 247
    label "Kiribati"
  ]
  node [
    id 248
    label "Szwajcaria"
  ]
  node [
    id 249
    label "Powi&#347;le"
  ]
  node [
    id 250
    label "Chorwacja"
  ]
  node [
    id 251
    label "Sahara_Zachodnia"
  ]
  node [
    id 252
    label "Tajlandia"
  ]
  node [
    id 253
    label "Salwador"
  ]
  node [
    id 254
    label "Bahamy"
  ]
  node [
    id 255
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 256
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 257
    label "Zamojszczyzna"
  ]
  node [
    id 258
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 259
    label "S&#322;owenia"
  ]
  node [
    id 260
    label "Gambia"
  ]
  node [
    id 261
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 262
    label "Urugwaj"
  ]
  node [
    id 263
    label "Podlasie"
  ]
  node [
    id 264
    label "Zair"
  ]
  node [
    id 265
    label "Erytrea"
  ]
  node [
    id 266
    label "Laponia"
  ]
  node [
    id 267
    label "Kujawy"
  ]
  node [
    id 268
    label "Umbria"
  ]
  node [
    id 269
    label "Rosja"
  ]
  node [
    id 270
    label "Mauritius"
  ]
  node [
    id 271
    label "Niger"
  ]
  node [
    id 272
    label "Uganda"
  ]
  node [
    id 273
    label "Turkmenistan"
  ]
  node [
    id 274
    label "Turcja"
  ]
  node [
    id 275
    label "Mezoameryka"
  ]
  node [
    id 276
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 277
    label "Irlandia"
  ]
  node [
    id 278
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 279
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 280
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 281
    label "Gwinea_Bissau"
  ]
  node [
    id 282
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 283
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 284
    label "Kurdystan"
  ]
  node [
    id 285
    label "Belgia"
  ]
  node [
    id 286
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 287
    label "Palau"
  ]
  node [
    id 288
    label "Barbados"
  ]
  node [
    id 289
    label "Wenezuela"
  ]
  node [
    id 290
    label "W&#281;gry"
  ]
  node [
    id 291
    label "Chile"
  ]
  node [
    id 292
    label "Argentyna"
  ]
  node [
    id 293
    label "Kolumbia"
  ]
  node [
    id 294
    label "Armagnac"
  ]
  node [
    id 295
    label "Kampania"
  ]
  node [
    id 296
    label "Sierra_Leone"
  ]
  node [
    id 297
    label "Azerbejd&#380;an"
  ]
  node [
    id 298
    label "Kongo"
  ]
  node [
    id 299
    label "Polinezja"
  ]
  node [
    id 300
    label "Warmia"
  ]
  node [
    id 301
    label "Pakistan"
  ]
  node [
    id 302
    label "Liechtenstein"
  ]
  node [
    id 303
    label "Wielkopolska"
  ]
  node [
    id 304
    label "Nikaragua"
  ]
  node [
    id 305
    label "Senegal"
  ]
  node [
    id 306
    label "brzeg"
  ]
  node [
    id 307
    label "Bordeaux"
  ]
  node [
    id 308
    label "Lauda"
  ]
  node [
    id 309
    label "Indie"
  ]
  node [
    id 310
    label "Mazury"
  ]
  node [
    id 311
    label "Suazi"
  ]
  node [
    id 312
    label "Polska"
  ]
  node [
    id 313
    label "Algieria"
  ]
  node [
    id 314
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 315
    label "Jamajka"
  ]
  node [
    id 316
    label "Timor_Wschodni"
  ]
  node [
    id 317
    label "Oceania"
  ]
  node [
    id 318
    label "Kostaryka"
  ]
  node [
    id 319
    label "Lasko"
  ]
  node [
    id 320
    label "Podkarpacie"
  ]
  node [
    id 321
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 322
    label "Kuba"
  ]
  node [
    id 323
    label "Mauretania"
  ]
  node [
    id 324
    label "Amazonia"
  ]
  node [
    id 325
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 326
    label "Portoryko"
  ]
  node [
    id 327
    label "Brazylia"
  ]
  node [
    id 328
    label "Mo&#322;dawia"
  ]
  node [
    id 329
    label "organizacja"
  ]
  node [
    id 330
    label "Litwa"
  ]
  node [
    id 331
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 332
    label "Kirgistan"
  ]
  node [
    id 333
    label "Izrael"
  ]
  node [
    id 334
    label "Grecja"
  ]
  node [
    id 335
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 336
    label "Kurpie"
  ]
  node [
    id 337
    label "Holandia"
  ]
  node [
    id 338
    label "Sri_Lanka"
  ]
  node [
    id 339
    label "Tonkin"
  ]
  node [
    id 340
    label "Katar"
  ]
  node [
    id 341
    label "Azja_Wschodnia"
  ]
  node [
    id 342
    label "Kaszmir"
  ]
  node [
    id 343
    label "Mikronezja"
  ]
  node [
    id 344
    label "Ukraina_Zachodnia"
  ]
  node [
    id 345
    label "Laos"
  ]
  node [
    id 346
    label "Mongolia"
  ]
  node [
    id 347
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 348
    label "Malediwy"
  ]
  node [
    id 349
    label "Zambia"
  ]
  node [
    id 350
    label "Turyngia"
  ]
  node [
    id 351
    label "Tanzania"
  ]
  node [
    id 352
    label "Gujana"
  ]
  node [
    id 353
    label "Apulia"
  ]
  node [
    id 354
    label "Uzbekistan"
  ]
  node [
    id 355
    label "Panama"
  ]
  node [
    id 356
    label "Czechy"
  ]
  node [
    id 357
    label "Gruzja"
  ]
  node [
    id 358
    label "Baszkiria"
  ]
  node [
    id 359
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 360
    label "Francja"
  ]
  node [
    id 361
    label "Serbia"
  ]
  node [
    id 362
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 363
    label "Togo"
  ]
  node [
    id 364
    label "Estonia"
  ]
  node [
    id 365
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 366
    label "Indochiny"
  ]
  node [
    id 367
    label "Boliwia"
  ]
  node [
    id 368
    label "Oman"
  ]
  node [
    id 369
    label "Portugalia"
  ]
  node [
    id 370
    label "Wyspy_Salomona"
  ]
  node [
    id 371
    label "Haiti"
  ]
  node [
    id 372
    label "Luksemburg"
  ]
  node [
    id 373
    label "Lubuskie"
  ]
  node [
    id 374
    label "Biskupizna"
  ]
  node [
    id 375
    label "Birma"
  ]
  node [
    id 376
    label "Rodezja"
  ]
  node [
    id 377
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 378
    label "circulate"
  ]
  node [
    id 379
    label "augment"
  ]
  node [
    id 380
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 381
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 382
    label "liczy&#263;"
  ]
  node [
    id 383
    label "okre&#347;lony"
  ]
  node [
    id 384
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 385
    label "ci&#261;g&#322;y"
  ]
  node [
    id 386
    label "pradawny"
  ]
  node [
    id 387
    label "wiecznie"
  ]
  node [
    id 388
    label "sta&#322;y"
  ]
  node [
    id 389
    label "trwa&#322;y"
  ]
  node [
    id 390
    label "energy"
  ]
  node [
    id 391
    label "czas"
  ]
  node [
    id 392
    label "bycie"
  ]
  node [
    id 393
    label "zegar_biologiczny"
  ]
  node [
    id 394
    label "okres_noworodkowy"
  ]
  node [
    id 395
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 396
    label "entity"
  ]
  node [
    id 397
    label "prze&#380;ywanie"
  ]
  node [
    id 398
    label "prze&#380;ycie"
  ]
  node [
    id 399
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 400
    label "wiek_matuzalemowy"
  ]
  node [
    id 401
    label "dzieci&#324;stwo"
  ]
  node [
    id 402
    label "power"
  ]
  node [
    id 403
    label "szwung"
  ]
  node [
    id 404
    label "menopauza"
  ]
  node [
    id 405
    label "umarcie"
  ]
  node [
    id 406
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 407
    label "life"
  ]
  node [
    id 408
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 409
    label "&#380;ywy"
  ]
  node [
    id 410
    label "rozw&#243;j"
  ]
  node [
    id 411
    label "po&#322;&#243;g"
  ]
  node [
    id 412
    label "byt"
  ]
  node [
    id 413
    label "przebywanie"
  ]
  node [
    id 414
    label "subsistence"
  ]
  node [
    id 415
    label "koleje_losu"
  ]
  node [
    id 416
    label "raj_utracony"
  ]
  node [
    id 417
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 418
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 419
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 420
    label "andropauza"
  ]
  node [
    id 421
    label "warunki"
  ]
  node [
    id 422
    label "do&#380;ywanie"
  ]
  node [
    id 423
    label "niemowl&#281;ctwo"
  ]
  node [
    id 424
    label "umieranie"
  ]
  node [
    id 425
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 426
    label "staro&#347;&#263;"
  ]
  node [
    id 427
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 428
    label "&#347;mier&#263;"
  ]
  node [
    id 429
    label "isolation"
  ]
  node [
    id 430
    label "izolacja"
  ]
  node [
    id 431
    label "samota"
  ]
  node [
    id 432
    label "wra&#380;enie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 14
    target 429
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 431
  ]
  edge [
    source 14
    target 432
  ]
]
