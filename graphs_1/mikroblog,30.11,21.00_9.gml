graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9813084112149533
  density 0.018691588785046728
  graphCliqueNumber 2
  node [
    id 0
    label "cie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "druga"
    origin "text"
  ]
  node [
    id 2
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 3
    label "osoba"
    origin "text"
  ]
  node [
    id 4
    label "firma"
    origin "text"
  ]
  node [
    id 5
    label "zaraz"
    origin "text"
  ]
  node [
    id 6
    label "prezes"
    origin "text"
  ]
  node [
    id 7
    label "heheszki"
    origin "text"
  ]
  node [
    id 8
    label "bekaztransa"
    origin "text"
  ]
  node [
    id 9
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 10
    label "transport"
    origin "text"
  ]
  node [
    id 11
    label "dozorca"
  ]
  node [
    id 12
    label "str&#243;&#380;"
  ]
  node [
    id 13
    label "godzina"
  ]
  node [
    id 14
    label "silny"
  ]
  node [
    id 15
    label "wa&#380;nie"
  ]
  node [
    id 16
    label "eksponowany"
  ]
  node [
    id 17
    label "istotnie"
  ]
  node [
    id 18
    label "znaczny"
  ]
  node [
    id 19
    label "dobry"
  ]
  node [
    id 20
    label "wynios&#322;y"
  ]
  node [
    id 21
    label "dono&#347;ny"
  ]
  node [
    id 22
    label "Zgredek"
  ]
  node [
    id 23
    label "kategoria_gramatyczna"
  ]
  node [
    id 24
    label "Casanova"
  ]
  node [
    id 25
    label "Don_Juan"
  ]
  node [
    id 26
    label "Gargantua"
  ]
  node [
    id 27
    label "Faust"
  ]
  node [
    id 28
    label "profanum"
  ]
  node [
    id 29
    label "Chocho&#322;"
  ]
  node [
    id 30
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 31
    label "koniugacja"
  ]
  node [
    id 32
    label "Winnetou"
  ]
  node [
    id 33
    label "Dwukwiat"
  ]
  node [
    id 34
    label "homo_sapiens"
  ]
  node [
    id 35
    label "Edyp"
  ]
  node [
    id 36
    label "Herkules_Poirot"
  ]
  node [
    id 37
    label "ludzko&#347;&#263;"
  ]
  node [
    id 38
    label "mikrokosmos"
  ]
  node [
    id 39
    label "person"
  ]
  node [
    id 40
    label "Sherlock_Holmes"
  ]
  node [
    id 41
    label "portrecista"
  ]
  node [
    id 42
    label "Szwejk"
  ]
  node [
    id 43
    label "Hamlet"
  ]
  node [
    id 44
    label "duch"
  ]
  node [
    id 45
    label "g&#322;owa"
  ]
  node [
    id 46
    label "oddzia&#322;ywanie"
  ]
  node [
    id 47
    label "Quasimodo"
  ]
  node [
    id 48
    label "Dulcynea"
  ]
  node [
    id 49
    label "Don_Kiszot"
  ]
  node [
    id 50
    label "Wallenrod"
  ]
  node [
    id 51
    label "Plastu&#347;"
  ]
  node [
    id 52
    label "Harry_Potter"
  ]
  node [
    id 53
    label "figura"
  ]
  node [
    id 54
    label "parali&#380;owa&#263;"
  ]
  node [
    id 55
    label "istota"
  ]
  node [
    id 56
    label "Werter"
  ]
  node [
    id 57
    label "antropochoria"
  ]
  node [
    id 58
    label "posta&#263;"
  ]
  node [
    id 59
    label "cz&#322;owiek"
  ]
  node [
    id 60
    label "Hortex"
  ]
  node [
    id 61
    label "MAC"
  ]
  node [
    id 62
    label "reengineering"
  ]
  node [
    id 63
    label "nazwa_w&#322;asna"
  ]
  node [
    id 64
    label "podmiot_gospodarczy"
  ]
  node [
    id 65
    label "Google"
  ]
  node [
    id 66
    label "zaufanie"
  ]
  node [
    id 67
    label "biurowiec"
  ]
  node [
    id 68
    label "networking"
  ]
  node [
    id 69
    label "zasoby_ludzkie"
  ]
  node [
    id 70
    label "interes"
  ]
  node [
    id 71
    label "paczkarnia"
  ]
  node [
    id 72
    label "Canon"
  ]
  node [
    id 73
    label "HP"
  ]
  node [
    id 74
    label "Baltona"
  ]
  node [
    id 75
    label "Pewex"
  ]
  node [
    id 76
    label "MAN_SE"
  ]
  node [
    id 77
    label "Apeks"
  ]
  node [
    id 78
    label "zasoby"
  ]
  node [
    id 79
    label "Orbis"
  ]
  node [
    id 80
    label "miejsce_pracy"
  ]
  node [
    id 81
    label "siedziba"
  ]
  node [
    id 82
    label "Spo&#322;em"
  ]
  node [
    id 83
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 84
    label "Orlen"
  ]
  node [
    id 85
    label "klasa"
  ]
  node [
    id 86
    label "blisko"
  ]
  node [
    id 87
    label "zara"
  ]
  node [
    id 88
    label "nied&#322;ugo"
  ]
  node [
    id 89
    label "gruba_ryba"
  ]
  node [
    id 90
    label "zwierzchnik"
  ]
  node [
    id 91
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 92
    label "zawarto&#347;&#263;"
  ]
  node [
    id 93
    label "unos"
  ]
  node [
    id 94
    label "traffic"
  ]
  node [
    id 95
    label "za&#322;adunek"
  ]
  node [
    id 96
    label "gospodarka"
  ]
  node [
    id 97
    label "roz&#322;adunek"
  ]
  node [
    id 98
    label "grupa"
  ]
  node [
    id 99
    label "sprz&#281;t"
  ]
  node [
    id 100
    label "jednoszynowy"
  ]
  node [
    id 101
    label "cedu&#322;a"
  ]
  node [
    id 102
    label "tyfon"
  ]
  node [
    id 103
    label "us&#322;uga"
  ]
  node [
    id 104
    label "prze&#322;adunek"
  ]
  node [
    id 105
    label "komunikacja"
  ]
  node [
    id 106
    label "towar"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
]
