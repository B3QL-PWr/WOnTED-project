graph [
  maxDegree 10
  minDegree 1
  meanDegree 2
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "okazja"
    origin "text"
  ]
  node [
    id 1
    label "imieniny"
    origin "text"
  ]
  node [
    id 2
    label "andrzej"
    origin "text"
  ]
  node [
    id 3
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "do&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "glupiewykopowezabawy"
    origin "text"
  ]
  node [
    id 6
    label "nasi"
    origin "text"
  ]
  node [
    id 7
    label "ulubiony"
    origin "text"
  ]
  node [
    id 8
    label "atrakcyjny"
  ]
  node [
    id 9
    label "oferta"
  ]
  node [
    id 10
    label "adeptness"
  ]
  node [
    id 11
    label "okazka"
  ]
  node [
    id 12
    label "wydarzenie"
  ]
  node [
    id 13
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 14
    label "podw&#243;zka"
  ]
  node [
    id 15
    label "autostop"
  ]
  node [
    id 16
    label "sytuacja"
  ]
  node [
    id 17
    label "impreza"
  ]
  node [
    id 18
    label "dzie&#324;"
  ]
  node [
    id 19
    label "czu&#263;"
  ]
  node [
    id 20
    label "desire"
  ]
  node [
    id 21
    label "kcie&#263;"
  ]
  node [
    id 22
    label "catch"
  ]
  node [
    id 23
    label "spowodowa&#263;"
  ]
  node [
    id 24
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 25
    label "zrobi&#263;"
  ]
  node [
    id 26
    label "articulation"
  ]
  node [
    id 27
    label "dokoptowa&#263;"
  ]
  node [
    id 28
    label "wyj&#261;tkowy"
  ]
  node [
    id 29
    label "faworytny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
]
