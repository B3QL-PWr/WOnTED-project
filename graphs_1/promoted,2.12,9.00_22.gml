graph [
  maxDegree 18
  minDegree 1
  meanDegree 2.091743119266055
  density 0.019367991845056064
  graphCliqueNumber 3
  node [
    id 0
    label "prokuratura"
    origin "text"
  ]
  node [
    id 1
    label "ropczyce"
    origin "text"
  ]
  node [
    id 2
    label "podkarpacie"
    origin "text"
  ]
  node [
    id 3
    label "wszcz&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "post&#281;powanie"
    origin "text"
  ]
  node [
    id 5
    label "sprawa"
    origin "text"
  ]
  node [
    id 6
    label "por&#243;d"
    origin "text"
  ]
  node [
    id 7
    label "jak"
    origin "text"
  ]
  node [
    id 8
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 9
    label "miejsce"
    origin "text"
  ]
  node [
    id 10
    label "teren"
    origin "text"
  ]
  node [
    id 11
    label "tamtejszy"
    origin "text"
  ]
  node [
    id 12
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 13
    label "zdrowie"
    origin "text"
  ]
  node [
    id 14
    label "siedziba"
  ]
  node [
    id 15
    label "urz&#261;d"
  ]
  node [
    id 16
    label "organ"
  ]
  node [
    id 17
    label "zacz&#261;&#263;"
  ]
  node [
    id 18
    label "originate"
  ]
  node [
    id 19
    label "spowodowa&#263;"
  ]
  node [
    id 20
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 21
    label "do"
  ]
  node [
    id 22
    label "zrobi&#263;"
  ]
  node [
    id 23
    label "robienie"
  ]
  node [
    id 24
    label "czynno&#347;&#263;"
  ]
  node [
    id 25
    label "zachowanie"
  ]
  node [
    id 26
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 27
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 28
    label "kognicja"
  ]
  node [
    id 29
    label "rozprawa"
  ]
  node [
    id 30
    label "s&#261;d"
  ]
  node [
    id 31
    label "kazanie"
  ]
  node [
    id 32
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 33
    label "campaign"
  ]
  node [
    id 34
    label "fashion"
  ]
  node [
    id 35
    label "wydarzenie"
  ]
  node [
    id 36
    label "przes&#322;anka"
  ]
  node [
    id 37
    label "zmierzanie"
  ]
  node [
    id 38
    label "temat"
  ]
  node [
    id 39
    label "idea"
  ]
  node [
    id 40
    label "szczeg&#243;&#322;"
  ]
  node [
    id 41
    label "rzecz"
  ]
  node [
    id 42
    label "object"
  ]
  node [
    id 43
    label "proposition"
  ]
  node [
    id 44
    label "po&#322;o&#380;na"
  ]
  node [
    id 45
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 46
    label "proces_fizjologiczny"
  ]
  node [
    id 47
    label "marc&#243;wka"
  ]
  node [
    id 48
    label "po&#322;&#243;g"
  ]
  node [
    id 49
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 50
    label "szok_poporodowy"
  ]
  node [
    id 51
    label "dula"
  ]
  node [
    id 52
    label "byd&#322;o"
  ]
  node [
    id 53
    label "zobo"
  ]
  node [
    id 54
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 55
    label "yakalo"
  ]
  node [
    id 56
    label "dzo"
  ]
  node [
    id 57
    label "proszek"
  ]
  node [
    id 58
    label "cia&#322;o"
  ]
  node [
    id 59
    label "plac"
  ]
  node [
    id 60
    label "cecha"
  ]
  node [
    id 61
    label "uwaga"
  ]
  node [
    id 62
    label "przestrze&#324;"
  ]
  node [
    id 63
    label "status"
  ]
  node [
    id 64
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 65
    label "chwila"
  ]
  node [
    id 66
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 67
    label "rz&#261;d"
  ]
  node [
    id 68
    label "praca"
  ]
  node [
    id 69
    label "location"
  ]
  node [
    id 70
    label "warunek_lokalowy"
  ]
  node [
    id 71
    label "zakres"
  ]
  node [
    id 72
    label "kontekst"
  ]
  node [
    id 73
    label "wymiar"
  ]
  node [
    id 74
    label "obszar"
  ]
  node [
    id 75
    label "krajobraz"
  ]
  node [
    id 76
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 77
    label "w&#322;adza"
  ]
  node [
    id 78
    label "nation"
  ]
  node [
    id 79
    label "przyroda"
  ]
  node [
    id 80
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 81
    label "miejsce_pracy"
  ]
  node [
    id 82
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 83
    label "nietutejszy"
  ]
  node [
    id 84
    label "Hollywood"
  ]
  node [
    id 85
    label "zal&#261;&#380;ek"
  ]
  node [
    id 86
    label "otoczenie"
  ]
  node [
    id 87
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 88
    label "&#347;rodek"
  ]
  node [
    id 89
    label "center"
  ]
  node [
    id 90
    label "instytucja"
  ]
  node [
    id 91
    label "skupisko"
  ]
  node [
    id 92
    label "warunki"
  ]
  node [
    id 93
    label "os&#322;abia&#263;"
  ]
  node [
    id 94
    label "niszczy&#263;"
  ]
  node [
    id 95
    label "zniszczy&#263;"
  ]
  node [
    id 96
    label "stan"
  ]
  node [
    id 97
    label "firmness"
  ]
  node [
    id 98
    label "kondycja"
  ]
  node [
    id 99
    label "os&#322;abi&#263;"
  ]
  node [
    id 100
    label "rozsypanie_si&#281;"
  ]
  node [
    id 101
    label "zdarcie"
  ]
  node [
    id 102
    label "zniszczenie"
  ]
  node [
    id 103
    label "zedrze&#263;"
  ]
  node [
    id 104
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 105
    label "niszczenie"
  ]
  node [
    id 106
    label "os&#322;abienie"
  ]
  node [
    id 107
    label "soundness"
  ]
  node [
    id 108
    label "os&#322;abianie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 60
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
]
