graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.0047732696897373
  density 0.004796108300693152
  graphCliqueNumber 3
  node [
    id 0
    label "p&#322;yn"
    origin "text"
  ]
  node [
    id 1
    label "mycie"
    origin "text"
  ]
  node [
    id 2
    label "naczynie"
    origin "text"
  ]
  node [
    id 3
    label "fairy"
    origin "text"
  ]
  node [
    id 4
    label "lemon"
    origin "text"
  ]
  node [
    id 5
    label "ksi&#281;ga_malachiasza"
    origin "text"
  ]
  node [
    id 6
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 7
    label "oba"
    origin "text"
  ]
  node [
    id 8
    label "kraj"
    origin "text"
  ]
  node [
    id 9
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 10
    label "zawarto&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 12
    label "powierzchniowo"
    origin "text"
  ]
  node [
    id 13
    label "czynny"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "u&#322;atwia&#263;"
    origin "text"
  ]
  node [
    id 16
    label "nieprzejrzysty"
  ]
  node [
    id 17
    label "&#347;ciekni&#281;cie"
  ]
  node [
    id 18
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 19
    label "baniak"
  ]
  node [
    id 20
    label "chlupa&#263;"
  ]
  node [
    id 21
    label "w&#322;oskowato&#347;&#263;"
  ]
  node [
    id 22
    label "odp&#322;yn&#261;&#263;"
  ]
  node [
    id 23
    label "p&#322;ywa&#263;"
  ]
  node [
    id 24
    label "substancja"
  ]
  node [
    id 25
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 26
    label "stan_skupienia"
  ]
  node [
    id 27
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 28
    label "ciek&#322;y"
  ]
  node [
    id 29
    label "zachlupa&#263;"
  ]
  node [
    id 30
    label "podbiec"
  ]
  node [
    id 31
    label "odp&#322;ywanie"
  ]
  node [
    id 32
    label "wpadni&#281;cie"
  ]
  node [
    id 33
    label "cia&#322;o"
  ]
  node [
    id 34
    label "wpadanie"
  ]
  node [
    id 35
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 36
    label "podbiega&#263;"
  ]
  node [
    id 37
    label "wytoczenie"
  ]
  node [
    id 38
    label "ablucja"
  ]
  node [
    id 39
    label "czyszczenie"
  ]
  node [
    id 40
    label "toaleta"
  ]
  node [
    id 41
    label "czysty"
  ]
  node [
    id 42
    label "wymywanie"
  ]
  node [
    id 43
    label "wash"
  ]
  node [
    id 44
    label "wymycie"
  ]
  node [
    id 45
    label "przemycie"
  ]
  node [
    id 46
    label "przemywanie"
  ]
  node [
    id 47
    label "rewaskularyzacja"
  ]
  node [
    id 48
    label "statki"
  ]
  node [
    id 49
    label "ceramika"
  ]
  node [
    id 50
    label "sprz&#281;t"
  ]
  node [
    id 51
    label "unaczyni&#263;"
  ]
  node [
    id 52
    label "receptacle"
  ]
  node [
    id 53
    label "vessel"
  ]
  node [
    id 54
    label "drewno"
  ]
  node [
    id 55
    label "przew&#243;d"
  ]
  node [
    id 56
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 57
    label "czyj&#347;"
  ]
  node [
    id 58
    label "m&#261;&#380;"
  ]
  node [
    id 59
    label "Skandynawia"
  ]
  node [
    id 60
    label "Rwanda"
  ]
  node [
    id 61
    label "Filipiny"
  ]
  node [
    id 62
    label "Yorkshire"
  ]
  node [
    id 63
    label "Kaukaz"
  ]
  node [
    id 64
    label "Podbeskidzie"
  ]
  node [
    id 65
    label "Toskania"
  ]
  node [
    id 66
    label "&#321;emkowszczyzna"
  ]
  node [
    id 67
    label "obszar"
  ]
  node [
    id 68
    label "Monako"
  ]
  node [
    id 69
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 70
    label "Amhara"
  ]
  node [
    id 71
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 72
    label "Lombardia"
  ]
  node [
    id 73
    label "Korea"
  ]
  node [
    id 74
    label "Kalabria"
  ]
  node [
    id 75
    label "Czarnog&#243;ra"
  ]
  node [
    id 76
    label "Ghana"
  ]
  node [
    id 77
    label "Tyrol"
  ]
  node [
    id 78
    label "Malawi"
  ]
  node [
    id 79
    label "Indonezja"
  ]
  node [
    id 80
    label "Bu&#322;garia"
  ]
  node [
    id 81
    label "Nauru"
  ]
  node [
    id 82
    label "Kenia"
  ]
  node [
    id 83
    label "Pamir"
  ]
  node [
    id 84
    label "Kambod&#380;a"
  ]
  node [
    id 85
    label "Lubelszczyzna"
  ]
  node [
    id 86
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 87
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 88
    label "Mali"
  ]
  node [
    id 89
    label "&#379;ywiecczyzna"
  ]
  node [
    id 90
    label "Austria"
  ]
  node [
    id 91
    label "interior"
  ]
  node [
    id 92
    label "Europa_Wschodnia"
  ]
  node [
    id 93
    label "Armenia"
  ]
  node [
    id 94
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 95
    label "Fid&#380;i"
  ]
  node [
    id 96
    label "Tuwalu"
  ]
  node [
    id 97
    label "Zabajkale"
  ]
  node [
    id 98
    label "Etiopia"
  ]
  node [
    id 99
    label "Malezja"
  ]
  node [
    id 100
    label "Malta"
  ]
  node [
    id 101
    label "Kaszuby"
  ]
  node [
    id 102
    label "Noworosja"
  ]
  node [
    id 103
    label "Bo&#347;nia"
  ]
  node [
    id 104
    label "Tad&#380;ykistan"
  ]
  node [
    id 105
    label "Grenada"
  ]
  node [
    id 106
    label "Ba&#322;kany"
  ]
  node [
    id 107
    label "Wehrlen"
  ]
  node [
    id 108
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 109
    label "Anglia"
  ]
  node [
    id 110
    label "Kielecczyzna"
  ]
  node [
    id 111
    label "Rumunia"
  ]
  node [
    id 112
    label "Pomorze_Zachodnie"
  ]
  node [
    id 113
    label "Maroko"
  ]
  node [
    id 114
    label "Bhutan"
  ]
  node [
    id 115
    label "Opolskie"
  ]
  node [
    id 116
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 117
    label "Ko&#322;yma"
  ]
  node [
    id 118
    label "Oksytania"
  ]
  node [
    id 119
    label "S&#322;owacja"
  ]
  node [
    id 120
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 121
    label "Seszele"
  ]
  node [
    id 122
    label "Syjon"
  ]
  node [
    id 123
    label "Kuwejt"
  ]
  node [
    id 124
    label "Arabia_Saudyjska"
  ]
  node [
    id 125
    label "Kociewie"
  ]
  node [
    id 126
    label "Kanada"
  ]
  node [
    id 127
    label "Ekwador"
  ]
  node [
    id 128
    label "ziemia"
  ]
  node [
    id 129
    label "Japonia"
  ]
  node [
    id 130
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 131
    label "Hiszpania"
  ]
  node [
    id 132
    label "Wyspy_Marshalla"
  ]
  node [
    id 133
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 134
    label "D&#380;ibuti"
  ]
  node [
    id 135
    label "Botswana"
  ]
  node [
    id 136
    label "Huculszczyzna"
  ]
  node [
    id 137
    label "Wietnam"
  ]
  node [
    id 138
    label "Egipt"
  ]
  node [
    id 139
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 140
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 141
    label "Burkina_Faso"
  ]
  node [
    id 142
    label "Bawaria"
  ]
  node [
    id 143
    label "Niemcy"
  ]
  node [
    id 144
    label "Khitai"
  ]
  node [
    id 145
    label "Macedonia"
  ]
  node [
    id 146
    label "Albania"
  ]
  node [
    id 147
    label "Madagaskar"
  ]
  node [
    id 148
    label "Bahrajn"
  ]
  node [
    id 149
    label "Jemen"
  ]
  node [
    id 150
    label "Lesoto"
  ]
  node [
    id 151
    label "Maghreb"
  ]
  node [
    id 152
    label "Samoa"
  ]
  node [
    id 153
    label "Andora"
  ]
  node [
    id 154
    label "Bory_Tucholskie"
  ]
  node [
    id 155
    label "Chiny"
  ]
  node [
    id 156
    label "Europa_Zachodnia"
  ]
  node [
    id 157
    label "Cypr"
  ]
  node [
    id 158
    label "Wielka_Brytania"
  ]
  node [
    id 159
    label "Kerala"
  ]
  node [
    id 160
    label "Podhale"
  ]
  node [
    id 161
    label "Kabylia"
  ]
  node [
    id 162
    label "Ukraina"
  ]
  node [
    id 163
    label "Paragwaj"
  ]
  node [
    id 164
    label "Trynidad_i_Tobago"
  ]
  node [
    id 165
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 166
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 167
    label "Ma&#322;opolska"
  ]
  node [
    id 168
    label "Polesie"
  ]
  node [
    id 169
    label "Liguria"
  ]
  node [
    id 170
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 171
    label "Libia"
  ]
  node [
    id 172
    label "&#321;&#243;dzkie"
  ]
  node [
    id 173
    label "Surinam"
  ]
  node [
    id 174
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 175
    label "Palestyna"
  ]
  node [
    id 176
    label "Nigeria"
  ]
  node [
    id 177
    label "Australia"
  ]
  node [
    id 178
    label "Honduras"
  ]
  node [
    id 179
    label "Bojkowszczyzna"
  ]
  node [
    id 180
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 181
    label "Karaiby"
  ]
  node [
    id 182
    label "Peru"
  ]
  node [
    id 183
    label "USA"
  ]
  node [
    id 184
    label "Bangladesz"
  ]
  node [
    id 185
    label "Kazachstan"
  ]
  node [
    id 186
    label "Nepal"
  ]
  node [
    id 187
    label "Irak"
  ]
  node [
    id 188
    label "Nadrenia"
  ]
  node [
    id 189
    label "Sudan"
  ]
  node [
    id 190
    label "S&#261;decczyzna"
  ]
  node [
    id 191
    label "Sand&#380;ak"
  ]
  node [
    id 192
    label "San_Marino"
  ]
  node [
    id 193
    label "Burundi"
  ]
  node [
    id 194
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 195
    label "Dominikana"
  ]
  node [
    id 196
    label "Komory"
  ]
  node [
    id 197
    label "Zakarpacie"
  ]
  node [
    id 198
    label "Gwatemala"
  ]
  node [
    id 199
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 200
    label "Zag&#243;rze"
  ]
  node [
    id 201
    label "Andaluzja"
  ]
  node [
    id 202
    label "granica_pa&#324;stwa"
  ]
  node [
    id 203
    label "Turkiestan"
  ]
  node [
    id 204
    label "Naddniestrze"
  ]
  node [
    id 205
    label "Hercegowina"
  ]
  node [
    id 206
    label "Brunei"
  ]
  node [
    id 207
    label "Iran"
  ]
  node [
    id 208
    label "jednostka_administracyjna"
  ]
  node [
    id 209
    label "Zimbabwe"
  ]
  node [
    id 210
    label "Namibia"
  ]
  node [
    id 211
    label "Meksyk"
  ]
  node [
    id 212
    label "Opolszczyzna"
  ]
  node [
    id 213
    label "Kamerun"
  ]
  node [
    id 214
    label "Afryka_Wschodnia"
  ]
  node [
    id 215
    label "Szlezwik"
  ]
  node [
    id 216
    label "Lotaryngia"
  ]
  node [
    id 217
    label "Somalia"
  ]
  node [
    id 218
    label "Angola"
  ]
  node [
    id 219
    label "Gabon"
  ]
  node [
    id 220
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 221
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 222
    label "Nowa_Zelandia"
  ]
  node [
    id 223
    label "Mozambik"
  ]
  node [
    id 224
    label "Tunezja"
  ]
  node [
    id 225
    label "Tajwan"
  ]
  node [
    id 226
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 227
    label "Liban"
  ]
  node [
    id 228
    label "Jordania"
  ]
  node [
    id 229
    label "Tonga"
  ]
  node [
    id 230
    label "Czad"
  ]
  node [
    id 231
    label "Gwinea"
  ]
  node [
    id 232
    label "Liberia"
  ]
  node [
    id 233
    label "Belize"
  ]
  node [
    id 234
    label "Mazowsze"
  ]
  node [
    id 235
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 236
    label "Benin"
  ]
  node [
    id 237
    label "&#321;otwa"
  ]
  node [
    id 238
    label "Syria"
  ]
  node [
    id 239
    label "Afryka_Zachodnia"
  ]
  node [
    id 240
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 241
    label "Dominika"
  ]
  node [
    id 242
    label "Antigua_i_Barbuda"
  ]
  node [
    id 243
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 244
    label "Hanower"
  ]
  node [
    id 245
    label "Galicja"
  ]
  node [
    id 246
    label "Szkocja"
  ]
  node [
    id 247
    label "Walia"
  ]
  node [
    id 248
    label "Afganistan"
  ]
  node [
    id 249
    label "W&#322;ochy"
  ]
  node [
    id 250
    label "Kiribati"
  ]
  node [
    id 251
    label "Szwajcaria"
  ]
  node [
    id 252
    label "Powi&#347;le"
  ]
  node [
    id 253
    label "Chorwacja"
  ]
  node [
    id 254
    label "Sahara_Zachodnia"
  ]
  node [
    id 255
    label "Tajlandia"
  ]
  node [
    id 256
    label "Salwador"
  ]
  node [
    id 257
    label "Bahamy"
  ]
  node [
    id 258
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 259
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 260
    label "Zamojszczyzna"
  ]
  node [
    id 261
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 262
    label "S&#322;owenia"
  ]
  node [
    id 263
    label "Gambia"
  ]
  node [
    id 264
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 265
    label "Urugwaj"
  ]
  node [
    id 266
    label "Podlasie"
  ]
  node [
    id 267
    label "Zair"
  ]
  node [
    id 268
    label "Erytrea"
  ]
  node [
    id 269
    label "Laponia"
  ]
  node [
    id 270
    label "Kujawy"
  ]
  node [
    id 271
    label "Umbria"
  ]
  node [
    id 272
    label "Rosja"
  ]
  node [
    id 273
    label "Mauritius"
  ]
  node [
    id 274
    label "Niger"
  ]
  node [
    id 275
    label "Uganda"
  ]
  node [
    id 276
    label "Turkmenistan"
  ]
  node [
    id 277
    label "Turcja"
  ]
  node [
    id 278
    label "Mezoameryka"
  ]
  node [
    id 279
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 280
    label "Irlandia"
  ]
  node [
    id 281
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 282
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 283
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 284
    label "Gwinea_Bissau"
  ]
  node [
    id 285
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 286
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 287
    label "Kurdystan"
  ]
  node [
    id 288
    label "Belgia"
  ]
  node [
    id 289
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 290
    label "Palau"
  ]
  node [
    id 291
    label "Barbados"
  ]
  node [
    id 292
    label "Wenezuela"
  ]
  node [
    id 293
    label "W&#281;gry"
  ]
  node [
    id 294
    label "Chile"
  ]
  node [
    id 295
    label "Argentyna"
  ]
  node [
    id 296
    label "Kolumbia"
  ]
  node [
    id 297
    label "Armagnac"
  ]
  node [
    id 298
    label "Kampania"
  ]
  node [
    id 299
    label "Sierra_Leone"
  ]
  node [
    id 300
    label "Azerbejd&#380;an"
  ]
  node [
    id 301
    label "Kongo"
  ]
  node [
    id 302
    label "Polinezja"
  ]
  node [
    id 303
    label "Warmia"
  ]
  node [
    id 304
    label "Pakistan"
  ]
  node [
    id 305
    label "Liechtenstein"
  ]
  node [
    id 306
    label "Wielkopolska"
  ]
  node [
    id 307
    label "Nikaragua"
  ]
  node [
    id 308
    label "Senegal"
  ]
  node [
    id 309
    label "brzeg"
  ]
  node [
    id 310
    label "Bordeaux"
  ]
  node [
    id 311
    label "Lauda"
  ]
  node [
    id 312
    label "Indie"
  ]
  node [
    id 313
    label "Mazury"
  ]
  node [
    id 314
    label "Suazi"
  ]
  node [
    id 315
    label "Polska"
  ]
  node [
    id 316
    label "Algieria"
  ]
  node [
    id 317
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 318
    label "Jamajka"
  ]
  node [
    id 319
    label "Timor_Wschodni"
  ]
  node [
    id 320
    label "Oceania"
  ]
  node [
    id 321
    label "Kostaryka"
  ]
  node [
    id 322
    label "Lasko"
  ]
  node [
    id 323
    label "Podkarpacie"
  ]
  node [
    id 324
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 325
    label "Kuba"
  ]
  node [
    id 326
    label "Mauretania"
  ]
  node [
    id 327
    label "Amazonia"
  ]
  node [
    id 328
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 329
    label "Portoryko"
  ]
  node [
    id 330
    label "Brazylia"
  ]
  node [
    id 331
    label "Mo&#322;dawia"
  ]
  node [
    id 332
    label "organizacja"
  ]
  node [
    id 333
    label "Litwa"
  ]
  node [
    id 334
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 335
    label "Kirgistan"
  ]
  node [
    id 336
    label "Izrael"
  ]
  node [
    id 337
    label "Grecja"
  ]
  node [
    id 338
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 339
    label "Kurpie"
  ]
  node [
    id 340
    label "Holandia"
  ]
  node [
    id 341
    label "Sri_Lanka"
  ]
  node [
    id 342
    label "Tonkin"
  ]
  node [
    id 343
    label "Katar"
  ]
  node [
    id 344
    label "Azja_Wschodnia"
  ]
  node [
    id 345
    label "Kaszmir"
  ]
  node [
    id 346
    label "Mikronezja"
  ]
  node [
    id 347
    label "Ukraina_Zachodnia"
  ]
  node [
    id 348
    label "Laos"
  ]
  node [
    id 349
    label "Mongolia"
  ]
  node [
    id 350
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 351
    label "Malediwy"
  ]
  node [
    id 352
    label "Zambia"
  ]
  node [
    id 353
    label "Turyngia"
  ]
  node [
    id 354
    label "Tanzania"
  ]
  node [
    id 355
    label "Gujana"
  ]
  node [
    id 356
    label "Apulia"
  ]
  node [
    id 357
    label "Uzbekistan"
  ]
  node [
    id 358
    label "Panama"
  ]
  node [
    id 359
    label "Czechy"
  ]
  node [
    id 360
    label "Gruzja"
  ]
  node [
    id 361
    label "Baszkiria"
  ]
  node [
    id 362
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 363
    label "Francja"
  ]
  node [
    id 364
    label "Serbia"
  ]
  node [
    id 365
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 366
    label "Togo"
  ]
  node [
    id 367
    label "Estonia"
  ]
  node [
    id 368
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 369
    label "Indochiny"
  ]
  node [
    id 370
    label "Boliwia"
  ]
  node [
    id 371
    label "Oman"
  ]
  node [
    id 372
    label "Portugalia"
  ]
  node [
    id 373
    label "Wyspy_Salomona"
  ]
  node [
    id 374
    label "Haiti"
  ]
  node [
    id 375
    label "Luksemburg"
  ]
  node [
    id 376
    label "Lubuskie"
  ]
  node [
    id 377
    label "Biskupizna"
  ]
  node [
    id 378
    label "Birma"
  ]
  node [
    id 379
    label "Rodezja"
  ]
  node [
    id 380
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 381
    label "r&#243;&#380;nie"
  ]
  node [
    id 382
    label "inny"
  ]
  node [
    id 383
    label "jaki&#347;"
  ]
  node [
    id 384
    label "wn&#281;trze"
  ]
  node [
    id 385
    label "temat"
  ]
  node [
    id 386
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 387
    label "informacja"
  ]
  node [
    id 388
    label "ilo&#347;&#263;"
  ]
  node [
    id 389
    label "miejsce"
  ]
  node [
    id 390
    label "czas"
  ]
  node [
    id 391
    label "abstrakcja"
  ]
  node [
    id 392
    label "punkt"
  ]
  node [
    id 393
    label "spos&#243;b"
  ]
  node [
    id 394
    label "chemikalia"
  ]
  node [
    id 395
    label "powierzchniowy"
  ]
  node [
    id 396
    label "dzia&#322;anie"
  ]
  node [
    id 397
    label "uczynnienie"
  ]
  node [
    id 398
    label "zaj&#281;ty"
  ]
  node [
    id 399
    label "czynnie"
  ]
  node [
    id 400
    label "realny"
  ]
  node [
    id 401
    label "aktywnie"
  ]
  node [
    id 402
    label "dzia&#322;alny"
  ]
  node [
    id 403
    label "wa&#380;ny"
  ]
  node [
    id 404
    label "intensywny"
  ]
  node [
    id 405
    label "ciekawy"
  ]
  node [
    id 406
    label "faktyczny"
  ]
  node [
    id 407
    label "dobry"
  ]
  node [
    id 408
    label "zaanga&#380;owany"
  ]
  node [
    id 409
    label "istotny"
  ]
  node [
    id 410
    label "zdolny"
  ]
  node [
    id 411
    label "uczynnianie"
  ]
  node [
    id 412
    label "ease"
  ]
  node [
    id 413
    label "robi&#263;"
  ]
  node [
    id 414
    label "powodowa&#263;"
  ]
  node [
    id 415
    label "&#322;atwi&#263;"
  ]
  node [
    id 416
    label "Fairy"
  ]
  node [
    id 417
    label "Lemon"
  ]
  node [
    id 418
    label "450"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 412
  ]
  edge [
    source 15
    target 413
  ]
  edge [
    source 15
    target 414
  ]
  edge [
    source 15
    target 415
  ]
  edge [
    source 416
    target 417
  ]
  edge [
    source 416
    target 418
  ]
  edge [
    source 417
    target 418
  ]
]
