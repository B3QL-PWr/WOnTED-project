graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.5
  density 0.21428571428571427
  graphCliqueNumber 2
  node [
    id 0
    label "roczny"
    origin "text"
  ]
  node [
    id 1
    label "kacperek"
    origin "text"
  ]
  node [
    id 2
    label "gor&#261;czkowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kilkunastomiesi&#281;czny"
  ]
  node [
    id 4
    label "chorowa&#263;"
  ]
  node [
    id 5
    label "Kacperek"
  ]
  node [
    id 6
    label "grodziski"
  ]
  node [
    id 7
    label "Mazowiecki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 6
    target 7
  ]
]
