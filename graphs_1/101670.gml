graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.206378986866792
  density 0.004147328922681939
  graphCliqueNumber 3
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "problem"
    origin "text"
  ]
  node [
    id 2
    label "stan"
    origin "text"
  ]
  node [
    id 3
    label "zmiana"
    origin "text"
  ]
  node [
    id 4
    label "system"
    origin "text"
  ]
  node [
    id 5
    label "kar"
    origin "text"
  ]
  node [
    id 6
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przyj&#281;ty"
    origin "text"
  ]
  node [
    id 8
    label "projekt"
    origin "text"
  ]
  node [
    id 9
    label "pozostawa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 11
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 12
    label "kondycja"
    origin "text"
  ]
  node [
    id 13
    label "finansowy"
    origin "text"
  ]
  node [
    id 14
    label "przychod"
    origin "text"
  ]
  node [
    id 15
    label "podmiot"
    origin "text"
  ]
  node [
    id 16
    label "przyj&#281;cie"
    origin "text"
  ]
  node [
    id 17
    label "jednolity"
    origin "text"
  ]
  node [
    id 18
    label "wysoko&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "tys"
    origin "text"
  ]
  node [
    id 20
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 21
    label "szkolenie"
    origin "text"
  ]
  node [
    id 22
    label "by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "skuteczny"
    origin "text"
  ]
  node [
    id 24
    label "taki"
    origin "text"
  ]
  node [
    id 25
    label "sam"
    origin "text"
  ]
  node [
    id 26
    label "stopie&#324;"
    origin "text"
  ]
  node [
    id 27
    label "dla"
    origin "text"
  ]
  node [
    id 28
    label "ma&#322;y"
    origin "text"
  ]
  node [
    id 29
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 30
    label "bud&#380;et"
    origin "text"
  ]
  node [
    id 31
    label "og&#243;&#322;"
    origin "text"
  ]
  node [
    id 32
    label "dysponowa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 34
    label "grozi&#263;"
    origin "text"
  ]
  node [
    id 35
    label "ruina"
    origin "text"
  ]
  node [
    id 36
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 37
    label "kara"
    origin "text"
  ]
  node [
    id 38
    label "stowarzyszenie"
    origin "text"
  ]
  node [
    id 39
    label "przypadek"
    origin "text"
  ]
  node [
    id 40
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 41
    label "instytucja"
    origin "text"
  ]
  node [
    id 42
    label "tyle"
    origin "text"
  ]
  node [
    id 43
    label "nieistotny"
    origin "text"
  ]
  node [
    id 44
    label "si&#281;"
    origin "text"
  ]
  node [
    id 45
    label "op&#322;aca&#263;"
    origin "text"
  ]
  node [
    id 46
    label "nie"
    origin "text"
  ]
  node [
    id 47
    label "przepis"
    origin "text"
  ]
  node [
    id 48
    label "ustawa"
    origin "text"
  ]
  node [
    id 49
    label "taka"
    origin "text"
  ]
  node [
    id 50
    label "zaproponowa&#263;"
    origin "text"
  ]
  node [
    id 51
    label "niewsp&#243;&#322;mierny"
    origin "text"
  ]
  node [
    id 52
    label "dlatego"
    origin "text"
  ]
  node [
    id 53
    label "proponowa&#263;"
    origin "text"
  ]
  node [
    id 54
    label "powr&#243;t"
    origin "text"
  ]
  node [
    id 55
    label "procentowy"
    origin "text"
  ]
  node [
    id 56
    label "odej&#347;cie"
    origin "text"
  ]
  node [
    id 57
    label "kwotowy"
    origin "text"
  ]
  node [
    id 58
    label "inny"
  ]
  node [
    id 59
    label "nast&#281;pnie"
  ]
  node [
    id 60
    label "kt&#243;ry&#347;"
  ]
  node [
    id 61
    label "kolejno"
  ]
  node [
    id 62
    label "nastopny"
  ]
  node [
    id 63
    label "trudno&#347;&#263;"
  ]
  node [
    id 64
    label "sprawa"
  ]
  node [
    id 65
    label "ambaras"
  ]
  node [
    id 66
    label "problemat"
  ]
  node [
    id 67
    label "pierepa&#322;ka"
  ]
  node [
    id 68
    label "obstruction"
  ]
  node [
    id 69
    label "problematyka"
  ]
  node [
    id 70
    label "jajko_Kolumba"
  ]
  node [
    id 71
    label "subiekcja"
  ]
  node [
    id 72
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 73
    label "Arizona"
  ]
  node [
    id 74
    label "Georgia"
  ]
  node [
    id 75
    label "warstwa"
  ]
  node [
    id 76
    label "jednostka_administracyjna"
  ]
  node [
    id 77
    label "Hawaje"
  ]
  node [
    id 78
    label "Goa"
  ]
  node [
    id 79
    label "Floryda"
  ]
  node [
    id 80
    label "Oklahoma"
  ]
  node [
    id 81
    label "punkt"
  ]
  node [
    id 82
    label "Alaska"
  ]
  node [
    id 83
    label "wci&#281;cie"
  ]
  node [
    id 84
    label "Alabama"
  ]
  node [
    id 85
    label "Oregon"
  ]
  node [
    id 86
    label "poziom"
  ]
  node [
    id 87
    label "Teksas"
  ]
  node [
    id 88
    label "Illinois"
  ]
  node [
    id 89
    label "Waszyngton"
  ]
  node [
    id 90
    label "Jukatan"
  ]
  node [
    id 91
    label "shape"
  ]
  node [
    id 92
    label "Nowy_Meksyk"
  ]
  node [
    id 93
    label "ilo&#347;&#263;"
  ]
  node [
    id 94
    label "state"
  ]
  node [
    id 95
    label "Nowy_York"
  ]
  node [
    id 96
    label "Arakan"
  ]
  node [
    id 97
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 98
    label "Kalifornia"
  ]
  node [
    id 99
    label "wektor"
  ]
  node [
    id 100
    label "Massachusetts"
  ]
  node [
    id 101
    label "miejsce"
  ]
  node [
    id 102
    label "Pensylwania"
  ]
  node [
    id 103
    label "Michigan"
  ]
  node [
    id 104
    label "Maryland"
  ]
  node [
    id 105
    label "Ohio"
  ]
  node [
    id 106
    label "Kansas"
  ]
  node [
    id 107
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 108
    label "Luizjana"
  ]
  node [
    id 109
    label "samopoczucie"
  ]
  node [
    id 110
    label "Wirginia"
  ]
  node [
    id 111
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 112
    label "anatomopatolog"
  ]
  node [
    id 113
    label "rewizja"
  ]
  node [
    id 114
    label "oznaka"
  ]
  node [
    id 115
    label "czas"
  ]
  node [
    id 116
    label "ferment"
  ]
  node [
    id 117
    label "komplet"
  ]
  node [
    id 118
    label "tura"
  ]
  node [
    id 119
    label "amendment"
  ]
  node [
    id 120
    label "zmianka"
  ]
  node [
    id 121
    label "odmienianie"
  ]
  node [
    id 122
    label "passage"
  ]
  node [
    id 123
    label "zjawisko"
  ]
  node [
    id 124
    label "change"
  ]
  node [
    id 125
    label "praca"
  ]
  node [
    id 126
    label "model"
  ]
  node [
    id 127
    label "sk&#322;ad"
  ]
  node [
    id 128
    label "zachowanie"
  ]
  node [
    id 129
    label "podstawa"
  ]
  node [
    id 130
    label "porz&#261;dek"
  ]
  node [
    id 131
    label "Android"
  ]
  node [
    id 132
    label "przyn&#281;ta"
  ]
  node [
    id 133
    label "jednostka_geologiczna"
  ]
  node [
    id 134
    label "metoda"
  ]
  node [
    id 135
    label "podsystem"
  ]
  node [
    id 136
    label "p&#322;&#243;d"
  ]
  node [
    id 137
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 138
    label "s&#261;d"
  ]
  node [
    id 139
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 140
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 141
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 142
    label "j&#261;dro"
  ]
  node [
    id 143
    label "eratem"
  ]
  node [
    id 144
    label "ryba"
  ]
  node [
    id 145
    label "pulpit"
  ]
  node [
    id 146
    label "struktura"
  ]
  node [
    id 147
    label "spos&#243;b"
  ]
  node [
    id 148
    label "oddzia&#322;"
  ]
  node [
    id 149
    label "usenet"
  ]
  node [
    id 150
    label "o&#347;"
  ]
  node [
    id 151
    label "oprogramowanie"
  ]
  node [
    id 152
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 153
    label "poj&#281;cie"
  ]
  node [
    id 154
    label "w&#281;dkarstwo"
  ]
  node [
    id 155
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 156
    label "Leopard"
  ]
  node [
    id 157
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 158
    label "systemik"
  ]
  node [
    id 159
    label "rozprz&#261;c"
  ]
  node [
    id 160
    label "cybernetyk"
  ]
  node [
    id 161
    label "konstelacja"
  ]
  node [
    id 162
    label "doktryna"
  ]
  node [
    id 163
    label "net"
  ]
  node [
    id 164
    label "zbi&#243;r"
  ]
  node [
    id 165
    label "method"
  ]
  node [
    id 166
    label "systemat"
  ]
  node [
    id 167
    label "wn&#281;ka"
  ]
  node [
    id 168
    label "continue"
  ]
  node [
    id 169
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 170
    label "consider"
  ]
  node [
    id 171
    label "my&#347;le&#263;"
  ]
  node [
    id 172
    label "pilnowa&#263;"
  ]
  node [
    id 173
    label "robi&#263;"
  ]
  node [
    id 174
    label "uznawa&#263;"
  ]
  node [
    id 175
    label "obserwowa&#263;"
  ]
  node [
    id 176
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 177
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 178
    label "deliver"
  ]
  node [
    id 179
    label "obowi&#261;zuj&#261;cy"
  ]
  node [
    id 180
    label "znajomy"
  ]
  node [
    id 181
    label "powszechny"
  ]
  node [
    id 182
    label "dokument"
  ]
  node [
    id 183
    label "device"
  ]
  node [
    id 184
    label "program_u&#380;ytkowy"
  ]
  node [
    id 185
    label "intencja"
  ]
  node [
    id 186
    label "agreement"
  ]
  node [
    id 187
    label "pomys&#322;"
  ]
  node [
    id 188
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 189
    label "plan"
  ]
  node [
    id 190
    label "dokumentacja"
  ]
  node [
    id 191
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 192
    label "stop"
  ]
  node [
    id 193
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 194
    label "przebywa&#263;"
  ]
  node [
    id 195
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 196
    label "support"
  ]
  node [
    id 197
    label "blend"
  ]
  node [
    id 198
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 199
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 200
    label "nijaki"
  ]
  node [
    id 201
    label "odwodnienie"
  ]
  node [
    id 202
    label "konstytucja"
  ]
  node [
    id 203
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 204
    label "substancja_chemiczna"
  ]
  node [
    id 205
    label "bratnia_dusza"
  ]
  node [
    id 206
    label "zwi&#261;zanie"
  ]
  node [
    id 207
    label "lokant"
  ]
  node [
    id 208
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 209
    label "zwi&#261;za&#263;"
  ]
  node [
    id 210
    label "organizacja"
  ]
  node [
    id 211
    label "odwadnia&#263;"
  ]
  node [
    id 212
    label "marriage"
  ]
  node [
    id 213
    label "marketing_afiliacyjny"
  ]
  node [
    id 214
    label "bearing"
  ]
  node [
    id 215
    label "wi&#261;zanie"
  ]
  node [
    id 216
    label "odwadnianie"
  ]
  node [
    id 217
    label "koligacja"
  ]
  node [
    id 218
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 219
    label "odwodni&#263;"
  ]
  node [
    id 220
    label "azeotrop"
  ]
  node [
    id 221
    label "powi&#261;zanie"
  ]
  node [
    id 222
    label "situation"
  ]
  node [
    id 223
    label "zdolno&#347;&#263;"
  ]
  node [
    id 224
    label "rank"
  ]
  node [
    id 225
    label "sytuacja"
  ]
  node [
    id 226
    label "dyspozycja"
  ]
  node [
    id 227
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 228
    label "mi&#281;dzybankowy"
  ]
  node [
    id 229
    label "finansowo"
  ]
  node [
    id 230
    label "fizyczny"
  ]
  node [
    id 231
    label "pozamaterialny"
  ]
  node [
    id 232
    label "materjalny"
  ]
  node [
    id 233
    label "cz&#322;owiek"
  ]
  node [
    id 234
    label "byt"
  ]
  node [
    id 235
    label "prawo"
  ]
  node [
    id 236
    label "nauka_prawa"
  ]
  node [
    id 237
    label "osobowo&#347;&#263;"
  ]
  node [
    id 238
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 239
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 240
    label "wzi&#281;cie"
  ]
  node [
    id 241
    label "reception"
  ]
  node [
    id 242
    label "zareagowanie"
  ]
  node [
    id 243
    label "poch&#322;oni&#281;cie"
  ]
  node [
    id 244
    label "entertainment"
  ]
  node [
    id 245
    label "spotkanie"
  ]
  node [
    id 246
    label "zobowi&#261;zanie_si&#281;"
  ]
  node [
    id 247
    label "stanie_si&#281;"
  ]
  node [
    id 248
    label "presumption"
  ]
  node [
    id 249
    label "dopuszczenie"
  ]
  node [
    id 250
    label "credence"
  ]
  node [
    id 251
    label "uznanie"
  ]
  node [
    id 252
    label "zgodzenie_si&#281;"
  ]
  node [
    id 253
    label "zrobienie"
  ]
  node [
    id 254
    label "party"
  ]
  node [
    id 255
    label "impreza"
  ]
  node [
    id 256
    label "wpuszczenie"
  ]
  node [
    id 257
    label "przyj&#261;&#263;"
  ]
  node [
    id 258
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 259
    label "w&#322;&#261;czenie"
  ]
  node [
    id 260
    label "umieszczenie"
  ]
  node [
    id 261
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 262
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 263
    label "jednostajny"
  ]
  node [
    id 264
    label "jednolicie"
  ]
  node [
    id 265
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 266
    label "ujednolicenie"
  ]
  node [
    id 267
    label "jednakowy"
  ]
  node [
    id 268
    label "tallness"
  ]
  node [
    id 269
    label "sum"
  ]
  node [
    id 270
    label "degree"
  ]
  node [
    id 271
    label "brzmienie"
  ]
  node [
    id 272
    label "altitude"
  ]
  node [
    id 273
    label "odcinek"
  ]
  node [
    id 274
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 275
    label "rozmiar"
  ]
  node [
    id 276
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 277
    label "k&#261;t"
  ]
  node [
    id 278
    label "wielko&#347;&#263;"
  ]
  node [
    id 279
    label "pom&#243;c"
  ]
  node [
    id 280
    label "zbudowa&#263;"
  ]
  node [
    id 281
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 282
    label "leave"
  ]
  node [
    id 283
    label "przewie&#347;&#263;"
  ]
  node [
    id 284
    label "wykona&#263;"
  ]
  node [
    id 285
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 286
    label "draw"
  ]
  node [
    id 287
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 288
    label "carry"
  ]
  node [
    id 289
    label "zapoznawanie"
  ]
  node [
    id 290
    label "seria"
  ]
  node [
    id 291
    label "course"
  ]
  node [
    id 292
    label "training"
  ]
  node [
    id 293
    label "zaj&#281;cia"
  ]
  node [
    id 294
    label "Lira"
  ]
  node [
    id 295
    label "pomaganie"
  ]
  node [
    id 296
    label "o&#347;wiecanie"
  ]
  node [
    id 297
    label "kliker"
  ]
  node [
    id 298
    label "pouczenie"
  ]
  node [
    id 299
    label "nauka"
  ]
  node [
    id 300
    label "si&#281;ga&#263;"
  ]
  node [
    id 301
    label "trwa&#263;"
  ]
  node [
    id 302
    label "obecno&#347;&#263;"
  ]
  node [
    id 303
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 304
    label "stand"
  ]
  node [
    id 305
    label "mie&#263;_miejsce"
  ]
  node [
    id 306
    label "uczestniczy&#263;"
  ]
  node [
    id 307
    label "chodzi&#263;"
  ]
  node [
    id 308
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 309
    label "equal"
  ]
  node [
    id 310
    label "skutkowanie"
  ]
  node [
    id 311
    label "poskutkowanie"
  ]
  node [
    id 312
    label "dobry"
  ]
  node [
    id 313
    label "sprawny"
  ]
  node [
    id 314
    label "skutecznie"
  ]
  node [
    id 315
    label "okre&#347;lony"
  ]
  node [
    id 316
    label "jaki&#347;"
  ]
  node [
    id 317
    label "sklep"
  ]
  node [
    id 318
    label "minuta"
  ]
  node [
    id 319
    label "forma"
  ]
  node [
    id 320
    label "znaczenie"
  ]
  node [
    id 321
    label "kategoria_gramatyczna"
  ]
  node [
    id 322
    label "przys&#322;&#243;wek"
  ]
  node [
    id 323
    label "szczebel"
  ]
  node [
    id 324
    label "element"
  ]
  node [
    id 325
    label "podn&#243;&#380;ek"
  ]
  node [
    id 326
    label "przymiotnik"
  ]
  node [
    id 327
    label "podzia&#322;"
  ]
  node [
    id 328
    label "ocena"
  ]
  node [
    id 329
    label "kszta&#322;t"
  ]
  node [
    id 330
    label "wschodek"
  ]
  node [
    id 331
    label "schody"
  ]
  node [
    id 332
    label "gama"
  ]
  node [
    id 333
    label "podstopie&#324;"
  ]
  node [
    id 334
    label "d&#378;wi&#281;k"
  ]
  node [
    id 335
    label "jednostka"
  ]
  node [
    id 336
    label "marny"
  ]
  node [
    id 337
    label "nieznaczny"
  ]
  node [
    id 338
    label "s&#322;aby"
  ]
  node [
    id 339
    label "ch&#322;opiec"
  ]
  node [
    id 340
    label "ma&#322;o"
  ]
  node [
    id 341
    label "n&#281;dznie"
  ]
  node [
    id 342
    label "niewa&#380;ny"
  ]
  node [
    id 343
    label "przeci&#281;tny"
  ]
  node [
    id 344
    label "nieliczny"
  ]
  node [
    id 345
    label "wstydliwy"
  ]
  node [
    id 346
    label "szybki"
  ]
  node [
    id 347
    label "m&#322;ody"
  ]
  node [
    id 348
    label "wydatki_bud&#380;etowe"
  ]
  node [
    id 349
    label "etat"
  ]
  node [
    id 350
    label "portfel"
  ]
  node [
    id 351
    label "rezerwa_rewaluacyjna"
  ]
  node [
    id 352
    label "kwota"
  ]
  node [
    id 353
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 354
    label "pole"
  ]
  node [
    id 355
    label "kastowo&#347;&#263;"
  ]
  node [
    id 356
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 357
    label "ludzie_pracy"
  ]
  node [
    id 358
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 359
    label "community"
  ]
  node [
    id 360
    label "Fremeni"
  ]
  node [
    id 361
    label "status"
  ]
  node [
    id 362
    label "pozaklasowy"
  ]
  node [
    id 363
    label "aspo&#322;eczny"
  ]
  node [
    id 364
    label "pe&#322;ny"
  ]
  node [
    id 365
    label "uwarstwienie"
  ]
  node [
    id 366
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 367
    label "zlewanie_si&#281;"
  ]
  node [
    id 368
    label "elita"
  ]
  node [
    id 369
    label "cywilizacja"
  ]
  node [
    id 370
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 371
    label "klasa"
  ]
  node [
    id 372
    label "namaszczenie_chorych"
  ]
  node [
    id 373
    label "control"
  ]
  node [
    id 374
    label "ekonomia"
  ]
  node [
    id 375
    label "command"
  ]
  node [
    id 376
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 377
    label "rozporz&#261;dza&#263;"
  ]
  node [
    id 378
    label "przygotowywa&#263;"
  ]
  node [
    id 379
    label "skazany"
  ]
  node [
    id 380
    label "dispose"
  ]
  node [
    id 381
    label "zapowiada&#263;"
  ]
  node [
    id 382
    label "boast"
  ]
  node [
    id 383
    label "hazard"
  ]
  node [
    id 384
    label "descent"
  ]
  node [
    id 385
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 386
    label "czyn"
  ]
  node [
    id 387
    label "przedstawiciel"
  ]
  node [
    id 388
    label "ilustracja"
  ]
  node [
    id 389
    label "fakt"
  ]
  node [
    id 390
    label "klacz"
  ]
  node [
    id 391
    label "konsekwencja"
  ]
  node [
    id 392
    label "punishment"
  ]
  node [
    id 393
    label "forfeit"
  ]
  node [
    id 394
    label "roboty_przymusowe"
  ]
  node [
    id 395
    label "nemezis"
  ]
  node [
    id 396
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 397
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 398
    label "Eleusis"
  ]
  node [
    id 399
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 400
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 401
    label "grupa"
  ]
  node [
    id 402
    label "fabianie"
  ]
  node [
    id 403
    label "Chewra_Kadisza"
  ]
  node [
    id 404
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 405
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 406
    label "Rotary_International"
  ]
  node [
    id 407
    label "Monar"
  ]
  node [
    id 408
    label "pacjent"
  ]
  node [
    id 409
    label "schorzenie"
  ]
  node [
    id 410
    label "przeznaczenie"
  ]
  node [
    id 411
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 412
    label "wydarzenie"
  ]
  node [
    id 413
    label "happening"
  ]
  node [
    id 414
    label "doros&#322;y"
  ]
  node [
    id 415
    label "wiele"
  ]
  node [
    id 416
    label "dorodny"
  ]
  node [
    id 417
    label "znaczny"
  ]
  node [
    id 418
    label "du&#380;o"
  ]
  node [
    id 419
    label "prawdziwy"
  ]
  node [
    id 420
    label "niema&#322;o"
  ]
  node [
    id 421
    label "wa&#380;ny"
  ]
  node [
    id 422
    label "rozwini&#281;ty"
  ]
  node [
    id 423
    label "afiliowa&#263;"
  ]
  node [
    id 424
    label "osoba_prawna"
  ]
  node [
    id 425
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 426
    label "urz&#261;d"
  ]
  node [
    id 427
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 428
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 429
    label "establishment"
  ]
  node [
    id 430
    label "standard"
  ]
  node [
    id 431
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 432
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 433
    label "zamykanie"
  ]
  node [
    id 434
    label "zamyka&#263;"
  ]
  node [
    id 435
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 436
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 437
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 438
    label "Fundusze_Unijne"
  ]
  node [
    id 439
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 440
    label "biuro"
  ]
  node [
    id 441
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 442
    label "konkretnie"
  ]
  node [
    id 443
    label "nieznacznie"
  ]
  node [
    id 444
    label "nieistotnie"
  ]
  node [
    id 445
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 446
    label "finance"
  ]
  node [
    id 447
    label "p&#322;aci&#263;"
  ]
  node [
    id 448
    label "give"
  ]
  node [
    id 449
    label "osi&#261;ga&#263;"
  ]
  node [
    id 450
    label "sprzeciw"
  ]
  node [
    id 451
    label "przedawnienie_si&#281;"
  ]
  node [
    id 452
    label "norma_prawna"
  ]
  node [
    id 453
    label "kodeks"
  ]
  node [
    id 454
    label "regulation"
  ]
  node [
    id 455
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 456
    label "porada"
  ]
  node [
    id 457
    label "przedawnianie_si&#281;"
  ]
  node [
    id 458
    label "recepta"
  ]
  node [
    id 459
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 460
    label "Karta_Nauczyciela"
  ]
  node [
    id 461
    label "marc&#243;wka"
  ]
  node [
    id 462
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 463
    label "akt"
  ]
  node [
    id 464
    label "przej&#347;&#263;"
  ]
  node [
    id 465
    label "charter"
  ]
  node [
    id 466
    label "przej&#347;cie"
  ]
  node [
    id 467
    label "Bangladesz"
  ]
  node [
    id 468
    label "jednostka_monetarna"
  ]
  node [
    id 469
    label "volunteer"
  ]
  node [
    id 470
    label "announce"
  ]
  node [
    id 471
    label "zach&#281;ci&#263;"
  ]
  node [
    id 472
    label "indicate"
  ]
  node [
    id 473
    label "kandydatura"
  ]
  node [
    id 474
    label "poinformowa&#263;"
  ]
  node [
    id 475
    label "niepor&#243;wnywalny"
  ]
  node [
    id 476
    label "nieproporcjonalnie"
  ]
  node [
    id 477
    label "report"
  ]
  node [
    id 478
    label "informowa&#263;"
  ]
  node [
    id 479
    label "zach&#281;ca&#263;"
  ]
  node [
    id 480
    label "wnioskowa&#263;"
  ]
  node [
    id 481
    label "suggest"
  ]
  node [
    id 482
    label "return"
  ]
  node [
    id 483
    label "odyseja"
  ]
  node [
    id 484
    label "para"
  ]
  node [
    id 485
    label "rektyfikacja"
  ]
  node [
    id 486
    label "liczbowy"
  ]
  node [
    id 487
    label "procentowo"
  ]
  node [
    id 488
    label "dysponowanie_si&#281;"
  ]
  node [
    id 489
    label "odumarcie"
  ]
  node [
    id 490
    label "withdrawal"
  ]
  node [
    id 491
    label "usuni&#281;cie"
  ]
  node [
    id 492
    label "stracenie"
  ]
  node [
    id 493
    label "mini&#281;cie"
  ]
  node [
    id 494
    label "mogi&#322;a"
  ]
  node [
    id 495
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 496
    label "po&#322;o&#380;enie_lachy"
  ]
  node [
    id 497
    label "defenestracja"
  ]
  node [
    id 498
    label "exit"
  ]
  node [
    id 499
    label "zwolnienie_si&#281;"
  ]
  node [
    id 500
    label "kres"
  ]
  node [
    id 501
    label "sko&#324;czenie"
  ]
  node [
    id 502
    label "szeol"
  ]
  node [
    id 503
    label "zako&#324;czenie"
  ]
  node [
    id 504
    label "&#380;ycie"
  ]
  node [
    id 505
    label "zb&#281;dny"
  ]
  node [
    id 506
    label "die"
  ]
  node [
    id 507
    label "ruszenie"
  ]
  node [
    id 508
    label "odrzut"
  ]
  node [
    id 509
    label "martwy"
  ]
  node [
    id 510
    label "opuszczenie"
  ]
  node [
    id 511
    label "deviation"
  ]
  node [
    id 512
    label "oddzielenie_si&#281;"
  ]
  node [
    id 513
    label "wr&#243;cenie"
  ]
  node [
    id 514
    label "porozchodzenie_si&#281;"
  ]
  node [
    id 515
    label "poniechanie"
  ]
  node [
    id 516
    label "pogrzebanie"
  ]
  node [
    id 517
    label "wypisanie_si&#281;"
  ]
  node [
    id 518
    label "zabicie"
  ]
  node [
    id 519
    label "zdechni&#281;cie"
  ]
  node [
    id 520
    label "kres_&#380;ycia"
  ]
  node [
    id 521
    label "oddalenie_si&#281;"
  ]
  node [
    id 522
    label "przestanie"
  ]
  node [
    id 523
    label "ust&#261;pienie"
  ]
  node [
    id 524
    label "wydalenie"
  ]
  node [
    id 525
    label "p&#243;j&#347;cie"
  ]
  node [
    id 526
    label "relinquishment"
  ]
  node [
    id 527
    label "agonia"
  ]
  node [
    id 528
    label "danie_sobie_spokoju"
  ]
  node [
    id 529
    label "pomarcie"
  ]
  node [
    id 530
    label "&#380;a&#322;oba"
  ]
  node [
    id 531
    label "spisanie_"
  ]
  node [
    id 532
    label "kwotowo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 206
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 208
  ]
  edge [
    source 11
    target 209
  ]
  edge [
    source 11
    target 210
  ]
  edge [
    source 11
    target 211
  ]
  edge [
    source 11
    target 212
  ]
  edge [
    source 11
    target 213
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 219
  ]
  edge [
    source 11
    target 220
  ]
  edge [
    source 11
    target 221
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 224
  ]
  edge [
    source 12
    target 225
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 228
  ]
  edge [
    source 13
    target 229
  ]
  edge [
    source 13
    target 230
  ]
  edge [
    source 13
    target 231
  ]
  edge [
    source 13
    target 232
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 27
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 233
  ]
  edge [
    source 15
    target 234
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 235
  ]
  edge [
    source 15
    target 236
  ]
  edge [
    source 15
    target 237
  ]
  edge [
    source 15
    target 238
  ]
  edge [
    source 15
    target 239
  ]
  edge [
    source 15
    target 29
  ]
  edge [
    source 15
    target 54
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 240
  ]
  edge [
    source 16
    target 241
  ]
  edge [
    source 16
    target 242
  ]
  edge [
    source 16
    target 243
  ]
  edge [
    source 16
    target 244
  ]
  edge [
    source 16
    target 245
  ]
  edge [
    source 16
    target 246
  ]
  edge [
    source 16
    target 247
  ]
  edge [
    source 16
    target 248
  ]
  edge [
    source 16
    target 249
  ]
  edge [
    source 16
    target 250
  ]
  edge [
    source 16
    target 251
  ]
  edge [
    source 16
    target 252
  ]
  edge [
    source 16
    target 253
  ]
  edge [
    source 16
    target 254
  ]
  edge [
    source 16
    target 255
  ]
  edge [
    source 16
    target 256
  ]
  edge [
    source 16
    target 257
  ]
  edge [
    source 16
    target 258
  ]
  edge [
    source 16
    target 259
  ]
  edge [
    source 16
    target 260
  ]
  edge [
    source 17
    target 261
  ]
  edge [
    source 17
    target 262
  ]
  edge [
    source 17
    target 263
  ]
  edge [
    source 17
    target 264
  ]
  edge [
    source 17
    target 265
  ]
  edge [
    source 17
    target 266
  ]
  edge [
    source 17
    target 267
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 268
  ]
  edge [
    source 18
    target 269
  ]
  edge [
    source 18
    target 270
  ]
  edge [
    source 18
    target 271
  ]
  edge [
    source 18
    target 272
  ]
  edge [
    source 18
    target 273
  ]
  edge [
    source 18
    target 274
  ]
  edge [
    source 18
    target 275
  ]
  edge [
    source 18
    target 276
  ]
  edge [
    source 18
    target 277
  ]
  edge [
    source 18
    target 278
  ]
  edge [
    source 18
    target 39
  ]
  edge [
    source 19
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 37
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 38
  ]
  edge [
    source 20
    target 279
  ]
  edge [
    source 20
    target 280
  ]
  edge [
    source 20
    target 281
  ]
  edge [
    source 20
    target 282
  ]
  edge [
    source 20
    target 283
  ]
  edge [
    source 20
    target 284
  ]
  edge [
    source 20
    target 285
  ]
  edge [
    source 20
    target 286
  ]
  edge [
    source 20
    target 287
  ]
  edge [
    source 20
    target 288
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 39
  ]
  edge [
    source 21
    target 289
  ]
  edge [
    source 21
    target 290
  ]
  edge [
    source 21
    target 291
  ]
  edge [
    source 21
    target 292
  ]
  edge [
    source 21
    target 293
  ]
  edge [
    source 21
    target 294
  ]
  edge [
    source 21
    target 295
  ]
  edge [
    source 21
    target 296
  ]
  edge [
    source 21
    target 297
  ]
  edge [
    source 21
    target 298
  ]
  edge [
    source 21
    target 299
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 33
  ]
  edge [
    source 22
    target 42
  ]
  edge [
    source 22
    target 43
  ]
  edge [
    source 22
    target 44
  ]
  edge [
    source 22
    target 51
  ]
  edge [
    source 22
    target 300
  ]
  edge [
    source 22
    target 301
  ]
  edge [
    source 22
    target 302
  ]
  edge [
    source 22
    target 303
  ]
  edge [
    source 22
    target 304
  ]
  edge [
    source 22
    target 305
  ]
  edge [
    source 22
    target 306
  ]
  edge [
    source 22
    target 307
  ]
  edge [
    source 22
    target 308
  ]
  edge [
    source 22
    target 309
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 310
  ]
  edge [
    source 23
    target 311
  ]
  edge [
    source 23
    target 312
  ]
  edge [
    source 23
    target 313
  ]
  edge [
    source 23
    target 314
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 315
  ]
  edge [
    source 24
    target 316
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 317
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 318
  ]
  edge [
    source 26
    target 319
  ]
  edge [
    source 26
    target 320
  ]
  edge [
    source 26
    target 321
  ]
  edge [
    source 26
    target 322
  ]
  edge [
    source 26
    target 323
  ]
  edge [
    source 26
    target 324
  ]
  edge [
    source 26
    target 86
  ]
  edge [
    source 26
    target 270
  ]
  edge [
    source 26
    target 325
  ]
  edge [
    source 26
    target 224
  ]
  edge [
    source 26
    target 326
  ]
  edge [
    source 26
    target 327
  ]
  edge [
    source 26
    target 328
  ]
  edge [
    source 26
    target 329
  ]
  edge [
    source 26
    target 330
  ]
  edge [
    source 26
    target 101
  ]
  edge [
    source 26
    target 331
  ]
  edge [
    source 26
    target 332
  ]
  edge [
    source 26
    target 333
  ]
  edge [
    source 26
    target 334
  ]
  edge [
    source 26
    target 278
  ]
  edge [
    source 26
    target 335
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 336
  ]
  edge [
    source 28
    target 337
  ]
  edge [
    source 28
    target 338
  ]
  edge [
    source 28
    target 339
  ]
  edge [
    source 28
    target 340
  ]
  edge [
    source 28
    target 341
  ]
  edge [
    source 28
    target 342
  ]
  edge [
    source 28
    target 343
  ]
  edge [
    source 28
    target 344
  ]
  edge [
    source 28
    target 345
  ]
  edge [
    source 28
    target 346
  ]
  edge [
    source 28
    target 347
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 54
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 348
  ]
  edge [
    source 30
    target 349
  ]
  edge [
    source 30
    target 350
  ]
  edge [
    source 30
    target 351
  ]
  edge [
    source 30
    target 352
  ]
  edge [
    source 30
    target 56
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 353
  ]
  edge [
    source 31
    target 354
  ]
  edge [
    source 31
    target 355
  ]
  edge [
    source 31
    target 356
  ]
  edge [
    source 31
    target 357
  ]
  edge [
    source 31
    target 358
  ]
  edge [
    source 31
    target 359
  ]
  edge [
    source 31
    target 360
  ]
  edge [
    source 31
    target 361
  ]
  edge [
    source 31
    target 362
  ]
  edge [
    source 31
    target 363
  ]
  edge [
    source 31
    target 364
  ]
  edge [
    source 31
    target 93
  ]
  edge [
    source 31
    target 365
  ]
  edge [
    source 31
    target 366
  ]
  edge [
    source 31
    target 367
  ]
  edge [
    source 31
    target 368
  ]
  edge [
    source 31
    target 369
  ]
  edge [
    source 31
    target 370
  ]
  edge [
    source 31
    target 371
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 372
  ]
  edge [
    source 32
    target 373
  ]
  edge [
    source 32
    target 374
  ]
  edge [
    source 32
    target 375
  ]
  edge [
    source 32
    target 376
  ]
  edge [
    source 32
    target 377
  ]
  edge [
    source 32
    target 378
  ]
  edge [
    source 32
    target 379
  ]
  edge [
    source 32
    target 380
  ]
  edge [
    source 32
    target 57
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 381
  ]
  edge [
    source 34
    target 382
  ]
  edge [
    source 34
    target 383
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 384
  ]
  edge [
    source 35
    target 385
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 233
  ]
  edge [
    source 36
    target 386
  ]
  edge [
    source 36
    target 387
  ]
  edge [
    source 36
    target 388
  ]
  edge [
    source 36
    target 389
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 43
  ]
  edge [
    source 37
    target 49
  ]
  edge [
    source 37
    target 390
  ]
  edge [
    source 37
    target 391
  ]
  edge [
    source 37
    target 392
  ]
  edge [
    source 37
    target 393
  ]
  edge [
    source 37
    target 394
  ]
  edge [
    source 37
    target 395
  ]
  edge [
    source 37
    target 352
  ]
  edge [
    source 38
    target 396
  ]
  edge [
    source 38
    target 397
  ]
  edge [
    source 38
    target 210
  ]
  edge [
    source 38
    target 398
  ]
  edge [
    source 38
    target 399
  ]
  edge [
    source 38
    target 400
  ]
  edge [
    source 38
    target 401
  ]
  edge [
    source 38
    target 402
  ]
  edge [
    source 38
    target 403
  ]
  edge [
    source 38
    target 404
  ]
  edge [
    source 38
    target 405
  ]
  edge [
    source 38
    target 406
  ]
  edge [
    source 38
    target 407
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 408
  ]
  edge [
    source 39
    target 321
  ]
  edge [
    source 39
    target 409
  ]
  edge [
    source 39
    target 410
  ]
  edge [
    source 39
    target 411
  ]
  edge [
    source 39
    target 412
  ]
  edge [
    source 39
    target 413
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 414
  ]
  edge [
    source 40
    target 415
  ]
  edge [
    source 40
    target 416
  ]
  edge [
    source 40
    target 417
  ]
  edge [
    source 40
    target 418
  ]
  edge [
    source 40
    target 419
  ]
  edge [
    source 40
    target 420
  ]
  edge [
    source 40
    target 421
  ]
  edge [
    source 40
    target 422
  ]
  edge [
    source 41
    target 423
  ]
  edge [
    source 41
    target 424
  ]
  edge [
    source 41
    target 425
  ]
  edge [
    source 41
    target 426
  ]
  edge [
    source 41
    target 427
  ]
  edge [
    source 41
    target 428
  ]
  edge [
    source 41
    target 429
  ]
  edge [
    source 41
    target 430
  ]
  edge [
    source 41
    target 210
  ]
  edge [
    source 41
    target 431
  ]
  edge [
    source 41
    target 432
  ]
  edge [
    source 41
    target 433
  ]
  edge [
    source 41
    target 434
  ]
  edge [
    source 41
    target 435
  ]
  edge [
    source 41
    target 153
  ]
  edge [
    source 41
    target 436
  ]
  edge [
    source 41
    target 437
  ]
  edge [
    source 41
    target 438
  ]
  edge [
    source 41
    target 439
  ]
  edge [
    source 41
    target 440
  ]
  edge [
    source 41
    target 441
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 415
  ]
  edge [
    source 42
    target 442
  ]
  edge [
    source 42
    target 443
  ]
  edge [
    source 43
    target 444
  ]
  edge [
    source 43
    target 337
  ]
  edge [
    source 43
    target 445
  ]
  edge [
    source 43
    target 338
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 446
  ]
  edge [
    source 45
    target 447
  ]
  edge [
    source 45
    target 448
  ]
  edge [
    source 45
    target 449
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 450
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 451
  ]
  edge [
    source 47
    target 147
  ]
  edge [
    source 47
    target 452
  ]
  edge [
    source 47
    target 453
  ]
  edge [
    source 47
    target 235
  ]
  edge [
    source 47
    target 454
  ]
  edge [
    source 47
    target 455
  ]
  edge [
    source 47
    target 456
  ]
  edge [
    source 47
    target 457
  ]
  edge [
    source 47
    target 458
  ]
  edge [
    source 47
    target 459
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 460
  ]
  edge [
    source 48
    target 461
  ]
  edge [
    source 48
    target 462
  ]
  edge [
    source 48
    target 463
  ]
  edge [
    source 48
    target 464
  ]
  edge [
    source 48
    target 465
  ]
  edge [
    source 48
    target 466
  ]
  edge [
    source 49
    target 467
  ]
  edge [
    source 49
    target 468
  ]
  edge [
    source 50
    target 469
  ]
  edge [
    source 50
    target 470
  ]
  edge [
    source 50
    target 471
  ]
  edge [
    source 50
    target 472
  ]
  edge [
    source 50
    target 473
  ]
  edge [
    source 50
    target 474
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 475
  ]
  edge [
    source 51
    target 476
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 477
  ]
  edge [
    source 53
    target 469
  ]
  edge [
    source 53
    target 478
  ]
  edge [
    source 53
    target 479
  ]
  edge [
    source 53
    target 480
  ]
  edge [
    source 53
    target 481
  ]
  edge [
    source 53
    target 473
  ]
  edge [
    source 54
    target 482
  ]
  edge [
    source 54
    target 483
  ]
  edge [
    source 54
    target 484
  ]
  edge [
    source 54
    target 412
  ]
  edge [
    source 54
    target 485
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 486
  ]
  edge [
    source 55
    target 487
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 488
  ]
  edge [
    source 56
    target 489
  ]
  edge [
    source 56
    target 490
  ]
  edge [
    source 56
    target 491
  ]
  edge [
    source 56
    target 492
  ]
  edge [
    source 56
    target 493
  ]
  edge [
    source 56
    target 494
  ]
  edge [
    source 56
    target 495
  ]
  edge [
    source 56
    target 496
  ]
  edge [
    source 56
    target 497
  ]
  edge [
    source 56
    target 498
  ]
  edge [
    source 56
    target 499
  ]
  edge [
    source 56
    target 500
  ]
  edge [
    source 56
    target 501
  ]
  edge [
    source 56
    target 502
  ]
  edge [
    source 56
    target 503
  ]
  edge [
    source 56
    target 504
  ]
  edge [
    source 56
    target 505
  ]
  edge [
    source 56
    target 506
  ]
  edge [
    source 56
    target 507
  ]
  edge [
    source 56
    target 508
  ]
  edge [
    source 56
    target 509
  ]
  edge [
    source 56
    target 510
  ]
  edge [
    source 56
    target 253
  ]
  edge [
    source 56
    target 511
  ]
  edge [
    source 56
    target 512
  ]
  edge [
    source 56
    target 513
  ]
  edge [
    source 56
    target 514
  ]
  edge [
    source 56
    target 515
  ]
  edge [
    source 56
    target 516
  ]
  edge [
    source 56
    target 517
  ]
  edge [
    source 56
    target 518
  ]
  edge [
    source 56
    target 519
  ]
  edge [
    source 56
    target 520
  ]
  edge [
    source 56
    target 521
  ]
  edge [
    source 56
    target 522
  ]
  edge [
    source 56
    target 523
  ]
  edge [
    source 56
    target 524
  ]
  edge [
    source 56
    target 525
  ]
  edge [
    source 56
    target 526
  ]
  edge [
    source 56
    target 527
  ]
  edge [
    source 56
    target 528
  ]
  edge [
    source 56
    target 529
  ]
  edge [
    source 56
    target 530
  ]
  edge [
    source 56
    target 531
  ]
  edge [
    source 57
    target 486
  ]
  edge [
    source 57
    target 532
  ]
]
