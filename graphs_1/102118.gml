graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.048076923076923
  density 0.009894091415830546
  graphCliqueNumber 2
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "jesienny"
    origin "text"
  ]
  node [
    id 2
    label "spotkanie"
    origin "text"
  ]
  node [
    id 3
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "zaprosi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "robert"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "przyjecha&#263;"
    origin "text"
  ]
  node [
    id 9
    label "swoje"
    origin "text"
  ]
  node [
    id 10
    label "jamminem"
    origin "text"
  ]
  node [
    id 11
    label "rtr"
    origin "text"
  ]
  node [
    id 12
    label "model"
    origin "text"
  ]
  node [
    id 13
    label "ten"
    origin "text"
  ]
  node [
    id 14
    label "zna&#263;"
    origin "text"
  ]
  node [
    id 15
    label "zesz&#322;oroczny"
    origin "text"
  ]
  node [
    id 16
    label "trening"
    origin "text"
  ]
  node [
    id 17
    label "tora"
    origin "text"
  ]
  node [
    id 18
    label "henpolu"
    origin "text"
  ]
  node [
    id 19
    label "nie&#378;le"
    origin "text"
  ]
  node [
    id 20
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 21
    label "siebie"
    origin "text"
  ]
  node [
    id 22
    label "rada"
    origin "text"
  ]
  node [
    id 23
    label "ciasny"
    origin "text"
  ]
  node [
    id 24
    label "stormdust"
    origin "text"
  ]
  node [
    id 25
    label "poniewa&#380;"
    origin "text"
  ]
  node [
    id 26
    label "adam"
    origin "text"
  ]
  node [
    id 27
    label "bazyl"
    origin "text"
  ]
  node [
    id 28
    label "maciolus"
    origin "text"
  ]
  node [
    id 29
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 30
    label "je&#378;dzi&#263;"
    origin "text"
  ]
  node [
    id 31
    label "bagusami"
    origin "text"
  ]
  node [
    id 32
    label "m&#243;cby&#263;"
    origin "text"
  ]
  node [
    id 33
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 34
    label "wspania&#322;y"
    origin "text"
  ]
  node [
    id 35
    label "pojedynek"
    origin "text"
  ]
  node [
    id 36
    label "cztery"
    origin "text"
  ]
  node [
    id 37
    label "buggy"
    origin "text"
  ]
  node [
    id 38
    label "godzina"
  ]
  node [
    id 39
    label "jesiennie"
  ]
  node [
    id 40
    label "sezonowy"
  ]
  node [
    id 41
    label "zajesienienie_si&#281;"
  ]
  node [
    id 42
    label "ciep&#322;y"
  ]
  node [
    id 43
    label "typowy"
  ]
  node [
    id 44
    label "jesienienie_si&#281;"
  ]
  node [
    id 45
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 46
    label "po&#380;egnanie"
  ]
  node [
    id 47
    label "spowodowanie"
  ]
  node [
    id 48
    label "znalezienie"
  ]
  node [
    id 49
    label "znajomy"
  ]
  node [
    id 50
    label "doznanie"
  ]
  node [
    id 51
    label "employment"
  ]
  node [
    id 52
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 53
    label "gather"
  ]
  node [
    id 54
    label "powitanie"
  ]
  node [
    id 55
    label "spotykanie"
  ]
  node [
    id 56
    label "wydarzenie"
  ]
  node [
    id 57
    label "gathering"
  ]
  node [
    id 58
    label "spotkanie_si&#281;"
  ]
  node [
    id 59
    label "zdarzenie_si&#281;"
  ]
  node [
    id 60
    label "match"
  ]
  node [
    id 61
    label "zawarcie"
  ]
  node [
    id 62
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 63
    label "represent"
  ]
  node [
    id 64
    label "zaproponowa&#263;"
  ]
  node [
    id 65
    label "invite"
  ]
  node [
    id 66
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 67
    label "ask"
  ]
  node [
    id 68
    label "get"
  ]
  node [
    id 69
    label "dotrze&#263;"
  ]
  node [
    id 70
    label "stawi&#263;_si&#281;"
  ]
  node [
    id 71
    label "typ"
  ]
  node [
    id 72
    label "cz&#322;owiek"
  ]
  node [
    id 73
    label "pozowa&#263;"
  ]
  node [
    id 74
    label "ideal"
  ]
  node [
    id 75
    label "matryca"
  ]
  node [
    id 76
    label "imitacja"
  ]
  node [
    id 77
    label "ruch"
  ]
  node [
    id 78
    label "motif"
  ]
  node [
    id 79
    label "pozowanie"
  ]
  node [
    id 80
    label "wz&#243;r"
  ]
  node [
    id 81
    label "miniatura"
  ]
  node [
    id 82
    label "prezenter"
  ]
  node [
    id 83
    label "facet"
  ]
  node [
    id 84
    label "orygina&#322;"
  ]
  node [
    id 85
    label "mildew"
  ]
  node [
    id 86
    label "spos&#243;b"
  ]
  node [
    id 87
    label "zi&#243;&#322;ko"
  ]
  node [
    id 88
    label "adaptation"
  ]
  node [
    id 89
    label "okre&#347;lony"
  ]
  node [
    id 90
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 91
    label "wiedzie&#263;"
  ]
  node [
    id 92
    label "cognizance"
  ]
  node [
    id 93
    label "stary"
  ]
  node [
    id 94
    label "doskonalenie"
  ]
  node [
    id 95
    label "training"
  ]
  node [
    id 96
    label "zgrupowanie"
  ]
  node [
    id 97
    label "&#263;wiczenie"
  ]
  node [
    id 98
    label "obw&#243;d"
  ]
  node [
    id 99
    label "warsztat"
  ]
  node [
    id 100
    label "zw&#243;j"
  ]
  node [
    id 101
    label "Tora"
  ]
  node [
    id 102
    label "sporo"
  ]
  node [
    id 103
    label "pozytywnie"
  ]
  node [
    id 104
    label "intensywnie"
  ]
  node [
    id 105
    label "nieszpetnie"
  ]
  node [
    id 106
    label "niez&#322;y"
  ]
  node [
    id 107
    label "skutecznie"
  ]
  node [
    id 108
    label "render"
  ]
  node [
    id 109
    label "hold"
  ]
  node [
    id 110
    label "surrender"
  ]
  node [
    id 111
    label "traktowa&#263;"
  ]
  node [
    id 112
    label "dostarcza&#263;"
  ]
  node [
    id 113
    label "tender"
  ]
  node [
    id 114
    label "train"
  ]
  node [
    id 115
    label "give"
  ]
  node [
    id 116
    label "umieszcza&#263;"
  ]
  node [
    id 117
    label "nalewa&#263;"
  ]
  node [
    id 118
    label "przeznacza&#263;"
  ]
  node [
    id 119
    label "p&#322;aci&#263;"
  ]
  node [
    id 120
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 121
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 122
    label "powierza&#263;"
  ]
  node [
    id 123
    label "hold_out"
  ]
  node [
    id 124
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 125
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 126
    label "mie&#263;_miejsce"
  ]
  node [
    id 127
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 128
    label "robi&#263;"
  ]
  node [
    id 129
    label "t&#322;uc"
  ]
  node [
    id 130
    label "wpiernicza&#263;"
  ]
  node [
    id 131
    label "przekazywa&#263;"
  ]
  node [
    id 132
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 133
    label "zezwala&#263;"
  ]
  node [
    id 134
    label "rap"
  ]
  node [
    id 135
    label "obiecywa&#263;"
  ]
  node [
    id 136
    label "&#322;adowa&#263;"
  ]
  node [
    id 137
    label "odst&#281;powa&#263;"
  ]
  node [
    id 138
    label "exsert"
  ]
  node [
    id 139
    label "dyskusja"
  ]
  node [
    id 140
    label "grupa"
  ]
  node [
    id 141
    label "Rada_Unii_Europejskiej"
  ]
  node [
    id 142
    label "conference"
  ]
  node [
    id 143
    label "organ"
  ]
  node [
    id 144
    label "zgromadzenie"
  ]
  node [
    id 145
    label "wskaz&#243;wka"
  ]
  node [
    id 146
    label "konsylium"
  ]
  node [
    id 147
    label "Rada_Europy"
  ]
  node [
    id 148
    label "Rada_Europejska"
  ]
  node [
    id 149
    label "posiedzenie"
  ]
  node [
    id 150
    label "niewygodny"
  ]
  node [
    id 151
    label "niedostateczny"
  ]
  node [
    id 152
    label "zwarty"
  ]
  node [
    id 153
    label "kr&#281;powanie"
  ]
  node [
    id 154
    label "nieelastyczny"
  ]
  node [
    id 155
    label "zw&#281;&#380;enie"
  ]
  node [
    id 156
    label "jajognioty"
  ]
  node [
    id 157
    label "duszny"
  ]
  node [
    id 158
    label "obcis&#322;y"
  ]
  node [
    id 159
    label "dobry"
  ]
  node [
    id 160
    label "niewystarczaj&#261;cy"
  ]
  node [
    id 161
    label "ciasno"
  ]
  node [
    id 162
    label "ma&#322;y"
  ]
  node [
    id 163
    label "w&#261;ski"
  ]
  node [
    id 164
    label "continue"
  ]
  node [
    id 165
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 166
    label "umie&#263;"
  ]
  node [
    id 167
    label "napada&#263;"
  ]
  node [
    id 168
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 169
    label "proceed"
  ]
  node [
    id 170
    label "przybywa&#263;"
  ]
  node [
    id 171
    label "uprawia&#263;"
  ]
  node [
    id 172
    label "drive"
  ]
  node [
    id 173
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 174
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 175
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 176
    label "ride"
  ]
  node [
    id 177
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 178
    label "carry"
  ]
  node [
    id 179
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 180
    label "prowadzi&#263;"
  ]
  node [
    id 181
    label "notice"
  ]
  node [
    id 182
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 183
    label "styka&#263;_si&#281;"
  ]
  node [
    id 184
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 185
    label "pomy&#347;lny"
  ]
  node [
    id 186
    label "warto&#347;ciowy"
  ]
  node [
    id 187
    label "och&#281;do&#380;ny"
  ]
  node [
    id 188
    label "pozytywny"
  ]
  node [
    id 189
    label "wspaniale"
  ]
  node [
    id 190
    label "zajebisty"
  ]
  node [
    id 191
    label "&#347;wietnie"
  ]
  node [
    id 192
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 193
    label "spania&#322;y"
  ]
  node [
    id 194
    label "bogato"
  ]
  node [
    id 195
    label "wyzwanie"
  ]
  node [
    id 196
    label "wyzywanie"
  ]
  node [
    id 197
    label "engagement"
  ]
  node [
    id 198
    label "walka"
  ]
  node [
    id 199
    label "wyzywa&#263;"
  ]
  node [
    id 200
    label "turniej"
  ]
  node [
    id 201
    label "wyzwa&#263;"
  ]
  node [
    id 202
    label "competitiveness"
  ]
  node [
    id 203
    label "bout"
  ]
  node [
    id 204
    label "odyniec"
  ]
  node [
    id 205
    label "sekundant"
  ]
  node [
    id 206
    label "sp&#243;r"
  ]
  node [
    id 207
    label "samoch&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 102
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 106
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 108
  ]
  edge [
    source 20
    target 109
  ]
  edge [
    source 20
    target 110
  ]
  edge [
    source 20
    target 111
  ]
  edge [
    source 20
    target 112
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 22
    target 140
  ]
  edge [
    source 22
    target 141
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 22
    target 143
  ]
  edge [
    source 22
    target 144
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 147
  ]
  edge [
    source 22
    target 148
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 23
    target 151
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 23
    target 153
  ]
  edge [
    source 23
    target 154
  ]
  edge [
    source 23
    target 155
  ]
  edge [
    source 23
    target 156
  ]
  edge [
    source 23
    target 157
  ]
  edge [
    source 23
    target 158
  ]
  edge [
    source 23
    target 159
  ]
  edge [
    source 23
    target 160
  ]
  edge [
    source 23
    target 161
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 164
  ]
  edge [
    source 30
    target 165
  ]
  edge [
    source 30
    target 166
  ]
  edge [
    source 30
    target 167
  ]
  edge [
    source 30
    target 168
  ]
  edge [
    source 30
    target 169
  ]
  edge [
    source 30
    target 170
  ]
  edge [
    source 30
    target 171
  ]
  edge [
    source 30
    target 172
  ]
  edge [
    source 30
    target 173
  ]
  edge [
    source 30
    target 174
  ]
  edge [
    source 30
    target 175
  ]
  edge [
    source 30
    target 176
  ]
  edge [
    source 30
    target 177
  ]
  edge [
    source 30
    target 178
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 181
  ]
  edge [
    source 33
    target 182
  ]
  edge [
    source 33
    target 183
  ]
  edge [
    source 33
    target 184
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 185
  ]
  edge [
    source 34
    target 186
  ]
  edge [
    source 34
    target 187
  ]
  edge [
    source 34
    target 188
  ]
  edge [
    source 34
    target 189
  ]
  edge [
    source 34
    target 190
  ]
  edge [
    source 34
    target 159
  ]
  edge [
    source 34
    target 191
  ]
  edge [
    source 34
    target 192
  ]
  edge [
    source 34
    target 193
  ]
  edge [
    source 34
    target 194
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 195
  ]
  edge [
    source 35
    target 196
  ]
  edge [
    source 35
    target 197
  ]
  edge [
    source 35
    target 198
  ]
  edge [
    source 35
    target 199
  ]
  edge [
    source 35
    target 200
  ]
  edge [
    source 35
    target 201
  ]
  edge [
    source 35
    target 202
  ]
  edge [
    source 35
    target 203
  ]
  edge [
    source 35
    target 204
  ]
  edge [
    source 35
    target 205
  ]
  edge [
    source 35
    target 206
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 207
  ]
]
