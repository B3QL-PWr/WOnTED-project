graph [
  maxDegree 16
  minDegree 1
  meanDegree 2.05607476635514
  density 0.01939693175806736
  graphCliqueNumber 3
  node [
    id 0
    label "ostatni"
    origin "text"
  ]
  node [
    id 1
    label "rozprawa"
    origin "text"
  ]
  node [
    id 2
    label "pow&#243;dztwo"
    origin "text"
  ]
  node [
    id 3
    label "cywilny"
    origin "text"
  ]
  node [
    id 4
    label "natalia"
    origin "text"
  ]
  node [
    id 5
    label "nitka"
    origin "text"
  ]
  node [
    id 6
    label "p&#322;a&#380;y&#324;skiej"
    origin "text"
  ]
  node [
    id 7
    label "przeciwko"
    origin "text"
  ]
  node [
    id 8
    label "niemiecki"
    origin "text"
  ]
  node [
    id 9
    label "biznesmen"
    origin "text"
  ]
  node [
    id 10
    label "hans"
    origin "text"
  ]
  node [
    id 11
    label "gram"
    origin "text"
  ]
  node [
    id 12
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 13
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 14
    label "wobec"
    origin "text"
  ]
  node [
    id 15
    label "swoje"
    origin "text"
  ]
  node [
    id 16
    label "pracownik"
    origin "text"
  ]
  node [
    id 17
    label "u&#380;ywa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "sformu&#322;owanie"
    origin "text"
  ]
  node [
    id 19
    label "za&#347;"
    origin "text"
  ]
  node [
    id 20
    label "pow&#243;dka"
    origin "text"
  ]
  node [
    id 21
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 23
    label "gro&#378;ba"
    origin "text"
  ]
  node [
    id 24
    label "cz&#322;owiek"
  ]
  node [
    id 25
    label "kolejny"
  ]
  node [
    id 26
    label "istota_&#380;ywa"
  ]
  node [
    id 27
    label "najgorszy"
  ]
  node [
    id 28
    label "aktualny"
  ]
  node [
    id 29
    label "ostatnio"
  ]
  node [
    id 30
    label "niedawno"
  ]
  node [
    id 31
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 32
    label "sko&#324;czony"
  ]
  node [
    id 33
    label "poprzedni"
  ]
  node [
    id 34
    label "pozosta&#322;y"
  ]
  node [
    id 35
    label "w&#261;tpliwy"
  ]
  node [
    id 36
    label "opracowanie"
  ]
  node [
    id 37
    label "s&#261;d"
  ]
  node [
    id 38
    label "obrady"
  ]
  node [
    id 39
    label "obja&#347;nienie"
  ]
  node [
    id 40
    label "rozumowanie"
  ]
  node [
    id 41
    label "proces"
  ]
  node [
    id 42
    label "s&#261;dzenie"
  ]
  node [
    id 43
    label "tekst"
  ]
  node [
    id 44
    label "cytat"
  ]
  node [
    id 45
    label "wniosek"
  ]
  node [
    id 46
    label "pozew"
  ]
  node [
    id 47
    label "cywilnie"
  ]
  node [
    id 48
    label "nieoficjalny"
  ]
  node [
    id 49
    label "motowid&#322;o"
  ]
  node [
    id 50
    label "nawijad&#322;o"
  ]
  node [
    id 51
    label "sznur"
  ]
  node [
    id 52
    label "makaron"
  ]
  node [
    id 53
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 54
    label "sie&#263;"
  ]
  node [
    id 55
    label "szwabski"
  ]
  node [
    id 56
    label "po_niemiecku"
  ]
  node [
    id 57
    label "niemiec"
  ]
  node [
    id 58
    label "cenar"
  ]
  node [
    id 59
    label "j&#281;zyk"
  ]
  node [
    id 60
    label "europejski"
  ]
  node [
    id 61
    label "German"
  ]
  node [
    id 62
    label "pionier"
  ]
  node [
    id 63
    label "niemiecko"
  ]
  node [
    id 64
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 65
    label "zachodnioeuropejski"
  ]
  node [
    id 66
    label "strudel"
  ]
  node [
    id 67
    label "junkers"
  ]
  node [
    id 68
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 69
    label "przedsi&#281;biorca"
  ]
  node [
    id 70
    label "dekagram"
  ]
  node [
    id 71
    label "megagram"
  ]
  node [
    id 72
    label "centygram"
  ]
  node [
    id 73
    label "miligram"
  ]
  node [
    id 74
    label "metryczna_jednostka_masy"
  ]
  node [
    id 75
    label "proszek"
  ]
  node [
    id 76
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 77
    label "delegowa&#263;"
  ]
  node [
    id 78
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 79
    label "pracu&#347;"
  ]
  node [
    id 80
    label "delegowanie"
  ]
  node [
    id 81
    label "r&#281;ka"
  ]
  node [
    id 82
    label "salariat"
  ]
  node [
    id 83
    label "bash"
  ]
  node [
    id 84
    label "distribute"
  ]
  node [
    id 85
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 86
    label "give"
  ]
  node [
    id 87
    label "korzysta&#263;"
  ]
  node [
    id 88
    label "doznawa&#263;"
  ]
  node [
    id 89
    label "wypowied&#378;"
  ]
  node [
    id 90
    label "poinformowanie"
  ]
  node [
    id 91
    label "zapisanie"
  ]
  node [
    id 92
    label "wording"
  ]
  node [
    id 93
    label "statement"
  ]
  node [
    id 94
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 95
    label "rzucenie"
  ]
  node [
    id 96
    label "postrach"
  ]
  node [
    id 97
    label "zawisa&#263;"
  ]
  node [
    id 98
    label "zawisanie"
  ]
  node [
    id 99
    label "threat"
  ]
  node [
    id 100
    label "cecha"
  ]
  node [
    id 101
    label "pogroza"
  ]
  node [
    id 102
    label "czarny_punkt"
  ]
  node [
    id 103
    label "zagrozi&#263;"
  ]
  node [
    id 104
    label "Natalia"
  ]
  node [
    id 105
    label "P&#322;a&#380;y&#324;skiej"
  ]
  node [
    id 106
    label "Hans"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 76
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 16
    target 79
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 17
    target 85
  ]
  edge [
    source 17
    target 86
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 89
  ]
  edge [
    source 18
    target 90
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 92
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 94
  ]
  edge [
    source 18
    target 95
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 96
  ]
  edge [
    source 23
    target 89
  ]
  edge [
    source 23
    target 97
  ]
  edge [
    source 23
    target 98
  ]
  edge [
    source 23
    target 99
  ]
  edge [
    source 23
    target 100
  ]
  edge [
    source 23
    target 101
  ]
  edge [
    source 23
    target 102
  ]
  edge [
    source 23
    target 103
  ]
  edge [
    source 104
    target 105
  ]
]
