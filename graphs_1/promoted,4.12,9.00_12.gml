graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.1142857142857143
  density 0.03064182194616977
  graphCliqueNumber 2
  node [
    id 0
    label "nawierzchnia"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nowa"
    origin "text"
  ]
  node [
    id 3
    label "skoro"
    origin "text"
  ]
  node [
    id 4
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 5
    label "tak"
    origin "text"
  ]
  node [
    id 6
    label "jakby"
    origin "text"
  ]
  node [
    id 7
    label "raz"
    origin "text"
  ]
  node [
    id 8
    label "trzeba"
    origin "text"
  ]
  node [
    id 9
    label "remontowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pyta"
    origin "text"
  ]
  node [
    id 11
    label "andrzej"
    origin "text"
  ]
  node [
    id 12
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 13
    label "odnawia&#263;"
    origin "text"
  ]
  node [
    id 14
    label "ulica"
    origin "text"
  ]
  node [
    id 15
    label "jaracz"
    origin "text"
  ]
  node [
    id 16
    label "droga"
  ]
  node [
    id 17
    label "warstwa"
  ]
  node [
    id 18
    label "pokrycie"
  ]
  node [
    id 19
    label "si&#281;ga&#263;"
  ]
  node [
    id 20
    label "trwa&#263;"
  ]
  node [
    id 21
    label "obecno&#347;&#263;"
  ]
  node [
    id 22
    label "stan"
  ]
  node [
    id 23
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 24
    label "stand"
  ]
  node [
    id 25
    label "mie&#263;_miejsce"
  ]
  node [
    id 26
    label "uczestniczy&#263;"
  ]
  node [
    id 27
    label "chodzi&#263;"
  ]
  node [
    id 28
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 29
    label "equal"
  ]
  node [
    id 30
    label "gwiazda"
  ]
  node [
    id 31
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 32
    label "czeka&#263;"
  ]
  node [
    id 33
    label "lookout"
  ]
  node [
    id 34
    label "wyziera&#263;"
  ]
  node [
    id 35
    label "peep"
  ]
  node [
    id 36
    label "look"
  ]
  node [
    id 37
    label "patrze&#263;"
  ]
  node [
    id 38
    label "chwila"
  ]
  node [
    id 39
    label "uderzenie"
  ]
  node [
    id 40
    label "cios"
  ]
  node [
    id 41
    label "time"
  ]
  node [
    id 42
    label "trza"
  ]
  node [
    id 43
    label "necessity"
  ]
  node [
    id 44
    label "narz&#281;dzie_wymierzania_kary"
  ]
  node [
    id 45
    label "penis"
  ]
  node [
    id 46
    label "cz&#322;owiek"
  ]
  node [
    id 47
    label "ludno&#347;&#263;"
  ]
  node [
    id 48
    label "zwierz&#281;"
  ]
  node [
    id 49
    label "powtarza&#263;"
  ]
  node [
    id 50
    label "restore"
  ]
  node [
    id 51
    label "przywraca&#263;"
  ]
  node [
    id 52
    label "sum_up"
  ]
  node [
    id 53
    label "ulepsza&#263;"
  ]
  node [
    id 54
    label "&#347;rodowisko"
  ]
  node [
    id 55
    label "miasteczko"
  ]
  node [
    id 56
    label "streetball"
  ]
  node [
    id 57
    label "pierzeja"
  ]
  node [
    id 58
    label "grupa"
  ]
  node [
    id 59
    label "pas_ruchu"
  ]
  node [
    id 60
    label "pas_rozdzielczy"
  ]
  node [
    id 61
    label "jezdnia"
  ]
  node [
    id 62
    label "korona_drogi"
  ]
  node [
    id 63
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 64
    label "chodnik"
  ]
  node [
    id 65
    label "arteria"
  ]
  node [
    id 66
    label "Broadway"
  ]
  node [
    id 67
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 68
    label "wysepka"
  ]
  node [
    id 69
    label "autostrada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 46
  ]
  edge [
    source 12
    target 47
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 13
    target 51
  ]
  edge [
    source 13
    target 52
  ]
  edge [
    source 13
    target 53
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 54
  ]
  edge [
    source 14
    target 55
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 57
  ]
  edge [
    source 14
    target 58
  ]
  edge [
    source 14
    target 59
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 61
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 65
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 14
    target 67
  ]
  edge [
    source 14
    target 68
  ]
  edge [
    source 14
    target 69
  ]
]
