graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9710144927536233
  density 0.028985507246376812
  graphCliqueNumber 2
  node [
    id 0
    label "ojciec"
    origin "text"
  ]
  node [
    id 1
    label "dziecko"
    origin "text"
  ]
  node [
    id 2
    label "kilkukrotnie"
    origin "text"
  ]
  node [
    id 3
    label "wzywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "personel"
    origin "text"
  ]
  node [
    id 5
    label "dzwonek"
    origin "text"
  ]
  node [
    id 6
    label "pomys&#322;odawca"
  ]
  node [
    id 7
    label "kszta&#322;ciciel"
  ]
  node [
    id 8
    label "tworzyciel"
  ]
  node [
    id 9
    label "ojczym"
  ]
  node [
    id 10
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 11
    label "stary"
  ]
  node [
    id 12
    label "samiec"
  ]
  node [
    id 13
    label "papa"
  ]
  node [
    id 14
    label "&#347;w"
  ]
  node [
    id 15
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 16
    label "zakonnik"
  ]
  node [
    id 17
    label "kuwada"
  ]
  node [
    id 18
    label "przodek"
  ]
  node [
    id 19
    label "wykonawca"
  ]
  node [
    id 20
    label "rodzice"
  ]
  node [
    id 21
    label "rodzic"
  ]
  node [
    id 22
    label "cz&#322;owiek"
  ]
  node [
    id 23
    label "potomstwo"
  ]
  node [
    id 24
    label "organizm"
  ]
  node [
    id 25
    label "sraluch"
  ]
  node [
    id 26
    label "utulanie"
  ]
  node [
    id 27
    label "pediatra"
  ]
  node [
    id 28
    label "dzieciarnia"
  ]
  node [
    id 29
    label "m&#322;odziak"
  ]
  node [
    id 30
    label "dzieciak"
  ]
  node [
    id 31
    label "utula&#263;"
  ]
  node [
    id 32
    label "potomek"
  ]
  node [
    id 33
    label "pedofil"
  ]
  node [
    id 34
    label "entliczek-pentliczek"
  ]
  node [
    id 35
    label "m&#322;odzik"
  ]
  node [
    id 36
    label "cz&#322;owieczek"
  ]
  node [
    id 37
    label "zwierz&#281;"
  ]
  node [
    id 38
    label "niepe&#322;noletni"
  ]
  node [
    id 39
    label "fledgling"
  ]
  node [
    id 40
    label "utuli&#263;"
  ]
  node [
    id 41
    label "utulenie"
  ]
  node [
    id 42
    label "kilkakro&#263;"
  ]
  node [
    id 43
    label "kilkukrotny"
  ]
  node [
    id 44
    label "parokrotny"
  ]
  node [
    id 45
    label "nakazywa&#263;"
  ]
  node [
    id 46
    label "cry"
  ]
  node [
    id 47
    label "pobudza&#263;"
  ]
  node [
    id 48
    label "invite"
  ]
  node [
    id 49
    label "donosi&#263;"
  ]
  node [
    id 50
    label "order"
  ]
  node [
    id 51
    label "prosi&#263;"
  ]
  node [
    id 52
    label "address"
  ]
  node [
    id 53
    label "persona&#322;"
  ]
  node [
    id 54
    label "zesp&#243;&#322;"
  ]
  node [
    id 55
    label "force"
  ]
  node [
    id 56
    label "ro&#347;lina_zielna"
  ]
  node [
    id 57
    label "karo"
  ]
  node [
    id 58
    label "dzwonkowate"
  ]
  node [
    id 59
    label "campanula"
  ]
  node [
    id 60
    label "przycisk"
  ]
  node [
    id 61
    label "&#322;&#261;cznik_instalacyjny"
  ]
  node [
    id 62
    label "dzwoni&#263;"
  ]
  node [
    id 63
    label "sygna&#322;_d&#378;wi&#281;kowy"
  ]
  node [
    id 64
    label "dzwonienie"
  ]
  node [
    id 65
    label "zadzwoni&#263;"
  ]
  node [
    id 66
    label "karta"
  ]
  node [
    id 67
    label "sygnalizator"
  ]
  node [
    id 68
    label "kolor"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
]
