graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "pompa"
    origin "text"
  ]
  node [
    id 1
    label "&#347;mig&#322;owy"
    origin "text"
  ]
  node [
    id 2
    label "ulewa"
  ]
  node [
    id 3
    label "maszyna_hydrauliczna"
  ]
  node [
    id 4
    label "podnios&#322;o&#347;&#263;"
  ]
  node [
    id 5
    label "przepompownia"
  ]
  node [
    id 6
    label "smok"
  ]
  node [
    id 7
    label "nurnik"
  ]
  node [
    id 8
    label "rozmach"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
]
