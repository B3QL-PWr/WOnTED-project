graph [
  maxDegree 23
  minDegree 1
  meanDegree 2.1586206896551725
  density 0.007469275742751462
  graphCliqueNumber 6
  node [
    id 0
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 1
    label "redakcja"
    origin "text"
  ]
  node [
    id 2
    label "qmama"
    origin "text"
  ]
  node [
    id 3
    label "autor"
    origin "text"
  ]
  node [
    id 4
    label "maja"
    origin "text"
  ]
  node [
    id 5
    label "szansa"
    origin "text"
  ]
  node [
    id 6
    label "wygrana"
    origin "text"
  ]
  node [
    id 7
    label "konkurs"
    origin "text"
  ]
  node [
    id 8
    label "fundacja"
    origin "text"
  ]
  node [
    id 9
    label "nowa"
    origin "text"
  ]
  node [
    id 10
    label "media"
    origin "text"
  ]
  node [
    id 11
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 12
    label "rozpoczyna&#263;"
    origin "text"
  ]
  node [
    id 13
    label "si&#281;"
    origin "text"
  ]
  node [
    id 14
    label "pierwsza"
    origin "text"
  ]
  node [
    id 15
    label "edycja"
    origin "text"
  ]
  node [
    id 16
    label "pocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "maj"
    origin "text"
  ]
  node [
    id 18
    label "raz"
    origin "text"
  ]
  node [
    id 19
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 20
    label "jura"
    origin "text"
  ]
  node [
    id 21
    label "wskaza&#263;"
    origin "text"
  ]
  node [
    id 22
    label "ciekawy"
    origin "text"
  ]
  node [
    id 23
    label "materia&#322;"
    origin "text"
  ]
  node [
    id 24
    label "nagrodzi&#263;"
    origin "text"
  ]
  node [
    id 25
    label "sprawny"
    origin "text"
  ]
  node [
    id 26
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 27
    label "projekt"
    origin "text"
  ]
  node [
    id 28
    label "czo&#322;owy"
    origin "text"
  ]
  node [
    id 29
    label "polski"
    origin "text"
  ]
  node [
    id 30
    label "dziennikarz"
    origin "text"
  ]
  node [
    id 31
    label "przyznawa&#263;"
    origin "text"
  ]
  node [
    id 32
    label "dobry"
    origin "text"
  ]
  node [
    id 33
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 34
    label "qmam&#243;w"
    origin "text"
  ]
  node [
    id 35
    label "rok"
    origin "text"
  ]
  node [
    id 36
    label "jaki&#347;"
  ]
  node [
    id 37
    label "telewizja"
  ]
  node [
    id 38
    label "redaction"
  ]
  node [
    id 39
    label "obr&#243;bka"
  ]
  node [
    id 40
    label "radio"
  ]
  node [
    id 41
    label "wydawnictwo"
  ]
  node [
    id 42
    label "zesp&#243;&#322;"
  ]
  node [
    id 43
    label "redaktor"
  ]
  node [
    id 44
    label "siedziba"
  ]
  node [
    id 45
    label "composition"
  ]
  node [
    id 46
    label "tekst"
  ]
  node [
    id 47
    label "pomys&#322;odawca"
  ]
  node [
    id 48
    label "kszta&#322;ciciel"
  ]
  node [
    id 49
    label "tworzyciel"
  ]
  node [
    id 50
    label "&#347;w"
  ]
  node [
    id 51
    label "wykonawca"
  ]
  node [
    id 52
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 53
    label "wedyzm"
  ]
  node [
    id 54
    label "energia"
  ]
  node [
    id 55
    label "buddyzm"
  ]
  node [
    id 56
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 57
    label "puchar"
  ]
  node [
    id 58
    label "korzy&#347;&#263;"
  ]
  node [
    id 59
    label "sukces"
  ]
  node [
    id 60
    label "przedmiot"
  ]
  node [
    id 61
    label "conquest"
  ]
  node [
    id 62
    label "eliminacje"
  ]
  node [
    id 63
    label "Interwizja"
  ]
  node [
    id 64
    label "emulation"
  ]
  node [
    id 65
    label "impreza"
  ]
  node [
    id 66
    label "casting"
  ]
  node [
    id 67
    label "Eurowizja"
  ]
  node [
    id 68
    label "nab&#243;r"
  ]
  node [
    id 69
    label "foundation"
  ]
  node [
    id 70
    label "instytucja"
  ]
  node [
    id 71
    label "dar"
  ]
  node [
    id 72
    label "darowizna"
  ]
  node [
    id 73
    label "pocz&#261;tek"
  ]
  node [
    id 74
    label "gwiazda"
  ]
  node [
    id 75
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 76
    label "przekazior"
  ]
  node [
    id 77
    label "mass-media"
  ]
  node [
    id 78
    label "uzbrajanie"
  ]
  node [
    id 79
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 80
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 81
    label "medium"
  ]
  node [
    id 82
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 83
    label "dok&#322;adnie"
  ]
  node [
    id 84
    label "start"
  ]
  node [
    id 85
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 86
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 87
    label "begin"
  ]
  node [
    id 88
    label "sprawowa&#263;"
  ]
  node [
    id 89
    label "godzina"
  ]
  node [
    id 90
    label "impression"
  ]
  node [
    id 91
    label "odmiana"
  ]
  node [
    id 92
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 93
    label "notification"
  ]
  node [
    id 94
    label "cykl"
  ]
  node [
    id 95
    label "zmiana"
  ]
  node [
    id 96
    label "produkcja"
  ]
  node [
    id 97
    label "egzemplarz"
  ]
  node [
    id 98
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 99
    label "do"
  ]
  node [
    id 100
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 101
    label "zrobi&#263;"
  ]
  node [
    id 102
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 103
    label "chwila"
  ]
  node [
    id 104
    label "uderzenie"
  ]
  node [
    id 105
    label "cios"
  ]
  node [
    id 106
    label "time"
  ]
  node [
    id 107
    label "czas"
  ]
  node [
    id 108
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 109
    label "miech"
  ]
  node [
    id 110
    label "kalendy"
  ]
  node [
    id 111
    label "tydzie&#324;"
  ]
  node [
    id 112
    label "formacja_geologiczna"
  ]
  node [
    id 113
    label "jura_&#347;rodkowa"
  ]
  node [
    id 114
    label "dogger"
  ]
  node [
    id 115
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 116
    label "era_mezozoiczna"
  ]
  node [
    id 117
    label "plezjozaur"
  ]
  node [
    id 118
    label "euoplocefal"
  ]
  node [
    id 119
    label "jura_wczesna"
  ]
  node [
    id 120
    label "aim"
  ]
  node [
    id 121
    label "pokaza&#263;"
  ]
  node [
    id 122
    label "podkre&#347;li&#263;"
  ]
  node [
    id 123
    label "wybra&#263;"
  ]
  node [
    id 124
    label "indicate"
  ]
  node [
    id 125
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 126
    label "poda&#263;"
  ]
  node [
    id 127
    label "picture"
  ]
  node [
    id 128
    label "point"
  ]
  node [
    id 129
    label "swoisty"
  ]
  node [
    id 130
    label "cz&#322;owiek"
  ]
  node [
    id 131
    label "interesowanie"
  ]
  node [
    id 132
    label "nietuzinkowy"
  ]
  node [
    id 133
    label "ciekawie"
  ]
  node [
    id 134
    label "indagator"
  ]
  node [
    id 135
    label "interesuj&#261;cy"
  ]
  node [
    id 136
    label "dziwny"
  ]
  node [
    id 137
    label "intryguj&#261;cy"
  ]
  node [
    id 138
    label "ch&#281;tny"
  ]
  node [
    id 139
    label "krajka"
  ]
  node [
    id 140
    label "tworzywo"
  ]
  node [
    id 141
    label "krajalno&#347;&#263;"
  ]
  node [
    id 142
    label "archiwum"
  ]
  node [
    id 143
    label "kandydat"
  ]
  node [
    id 144
    label "bielarnia"
  ]
  node [
    id 145
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 146
    label "dane"
  ]
  node [
    id 147
    label "materia"
  ]
  node [
    id 148
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 149
    label "substancja"
  ]
  node [
    id 150
    label "nawil&#380;arka"
  ]
  node [
    id 151
    label "dyspozycja"
  ]
  node [
    id 152
    label "da&#263;"
  ]
  node [
    id 153
    label "przyzna&#263;"
  ]
  node [
    id 154
    label "recompense"
  ]
  node [
    id 155
    label "nadgrodzi&#263;"
  ]
  node [
    id 156
    label "pay"
  ]
  node [
    id 157
    label "letki"
  ]
  node [
    id 158
    label "zdrowy"
  ]
  node [
    id 159
    label "dzia&#322;alny"
  ]
  node [
    id 160
    label "sprawnie"
  ]
  node [
    id 161
    label "umiej&#281;tny"
  ]
  node [
    id 162
    label "szybki"
  ]
  node [
    id 163
    label "koniec"
  ]
  node [
    id 164
    label "conclusion"
  ]
  node [
    id 165
    label "coating"
  ]
  node [
    id 166
    label "runda"
  ]
  node [
    id 167
    label "dokument"
  ]
  node [
    id 168
    label "device"
  ]
  node [
    id 169
    label "program_u&#380;ytkowy"
  ]
  node [
    id 170
    label "intencja"
  ]
  node [
    id 171
    label "agreement"
  ]
  node [
    id 172
    label "pomys&#322;"
  ]
  node [
    id 173
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 174
    label "plan"
  ]
  node [
    id 175
    label "dokumentacja"
  ]
  node [
    id 176
    label "czo&#322;owo"
  ]
  node [
    id 177
    label "pierwszy"
  ]
  node [
    id 178
    label "przedni"
  ]
  node [
    id 179
    label "wybitny"
  ]
  node [
    id 180
    label "lacki"
  ]
  node [
    id 181
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 182
    label "sztajer"
  ]
  node [
    id 183
    label "drabant"
  ]
  node [
    id 184
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 185
    label "polak"
  ]
  node [
    id 186
    label "pierogi_ruskie"
  ]
  node [
    id 187
    label "krakowiak"
  ]
  node [
    id 188
    label "Polish"
  ]
  node [
    id 189
    label "j&#281;zyk"
  ]
  node [
    id 190
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 191
    label "oberek"
  ]
  node [
    id 192
    label "po_polsku"
  ]
  node [
    id 193
    label "mazur"
  ]
  node [
    id 194
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 195
    label "chodzony"
  ]
  node [
    id 196
    label "skoczny"
  ]
  node [
    id 197
    label "ryba_po_grecku"
  ]
  node [
    id 198
    label "goniony"
  ]
  node [
    id 199
    label "polsko"
  ]
  node [
    id 200
    label "nowiniarz"
  ]
  node [
    id 201
    label "akredytowa&#263;"
  ]
  node [
    id 202
    label "akredytowanie"
  ]
  node [
    id 203
    label "bran&#380;owiec"
  ]
  node [
    id 204
    label "publicysta"
  ]
  node [
    id 205
    label "dawa&#263;"
  ]
  node [
    id 206
    label "confer"
  ]
  node [
    id 207
    label "distribute"
  ]
  node [
    id 208
    label "s&#261;dzi&#263;"
  ]
  node [
    id 209
    label "stwierdza&#263;"
  ]
  node [
    id 210
    label "nadawa&#263;"
  ]
  node [
    id 211
    label "pomy&#347;lny"
  ]
  node [
    id 212
    label "skuteczny"
  ]
  node [
    id 213
    label "moralny"
  ]
  node [
    id 214
    label "korzystny"
  ]
  node [
    id 215
    label "odpowiedni"
  ]
  node [
    id 216
    label "zwrot"
  ]
  node [
    id 217
    label "dobrze"
  ]
  node [
    id 218
    label "pozytywny"
  ]
  node [
    id 219
    label "grzeczny"
  ]
  node [
    id 220
    label "powitanie"
  ]
  node [
    id 221
    label "mi&#322;y"
  ]
  node [
    id 222
    label "dobroczynny"
  ]
  node [
    id 223
    label "pos&#322;uszny"
  ]
  node [
    id 224
    label "ca&#322;y"
  ]
  node [
    id 225
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 226
    label "czw&#243;rka"
  ]
  node [
    id 227
    label "spokojny"
  ]
  node [
    id 228
    label "&#347;mieszny"
  ]
  node [
    id 229
    label "drogi"
  ]
  node [
    id 230
    label "podtytu&#322;"
  ]
  node [
    id 231
    label "debit"
  ]
  node [
    id 232
    label "szata_graficzna"
  ]
  node [
    id 233
    label "elevation"
  ]
  node [
    id 234
    label "wyda&#263;"
  ]
  node [
    id 235
    label "nadtytu&#322;"
  ]
  node [
    id 236
    label "tytulatura"
  ]
  node [
    id 237
    label "nazwa"
  ]
  node [
    id 238
    label "wydawa&#263;"
  ]
  node [
    id 239
    label "druk"
  ]
  node [
    id 240
    label "mianowaniec"
  ]
  node [
    id 241
    label "poster"
  ]
  node [
    id 242
    label "publikacja"
  ]
  node [
    id 243
    label "stulecie"
  ]
  node [
    id 244
    label "kalendarz"
  ]
  node [
    id 245
    label "pora_roku"
  ]
  node [
    id 246
    label "cykl_astronomiczny"
  ]
  node [
    id 247
    label "p&#243;&#322;rocze"
  ]
  node [
    id 248
    label "grupa"
  ]
  node [
    id 249
    label "kwarta&#322;"
  ]
  node [
    id 250
    label "kurs"
  ]
  node [
    id 251
    label "jubileusz"
  ]
  node [
    id 252
    label "lata"
  ]
  node [
    id 253
    label "martwy_sezon"
  ]
  node [
    id 254
    label "nowy"
  ]
  node [
    id 255
    label "Qmam"
  ]
  node [
    id 256
    label "Amelia"
  ]
  node [
    id 257
    label "&#321;ukasiak"
  ]
  node [
    id 258
    label "Aleksandra"
  ]
  node [
    id 259
    label "Zieleniewska"
  ]
  node [
    id 260
    label "Renata"
  ]
  node [
    id 261
    label "kto"
  ]
  node [
    id 262
    label "Jan"
  ]
  node [
    id 263
    label "wr&#243;bel"
  ]
  node [
    id 264
    label "Mariusz"
  ]
  node [
    id 265
    label "szczygie&#322;"
  ]
  node [
    id 266
    label "du&#380;y"
  ]
  node [
    id 267
    label "format"
  ]
  node [
    id 268
    label "gazeta"
  ]
  node [
    id 269
    label "wyborczy"
  ]
  node [
    id 270
    label "na"
  ]
  node [
    id 271
    label "temat"
  ]
  node [
    id 272
    label "RMF"
  ]
  node [
    id 273
    label "FM"
  ]
  node [
    id 274
    label "plus"
  ]
  node [
    id 275
    label "tv"
  ]
  node [
    id 276
    label "puls"
  ]
  node [
    id 277
    label "europejski"
  ]
  node [
    id 278
    label "nagroda"
  ]
  node [
    id 279
    label "literacki"
  ]
  node [
    id 280
    label "i"
  ]
  node [
    id 281
    label "spo&#322;eczny"
  ]
  node [
    id 282
    label "liceum"
  ]
  node [
    id 283
    label "og&#243;lnokszta&#322;c&#261;cy"
  ]
  node [
    id 284
    label "wyspa"
  ]
  node [
    id 285
    label "warszawa"
  ]
  node [
    id 286
    label "centrum"
  ]
  node [
    id 287
    label "stosunki"
  ]
  node [
    id 288
    label "mi&#281;dzynarodowy"
  ]
  node [
    id 289
    label "rada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 10
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 103
  ]
  edge [
    source 18
    target 104
  ]
  edge [
    source 18
    target 105
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 108
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 19
    target 111
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 112
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 120
  ]
  edge [
    source 21
    target 121
  ]
  edge [
    source 21
    target 122
  ]
  edge [
    source 21
    target 123
  ]
  edge [
    source 21
    target 124
  ]
  edge [
    source 21
    target 125
  ]
  edge [
    source 21
    target 126
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 130
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 22
    target 133
  ]
  edge [
    source 22
    target 134
  ]
  edge [
    source 22
    target 135
  ]
  edge [
    source 22
    target 136
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 138
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 23
    target 130
  ]
  edge [
    source 23
    target 140
  ]
  edge [
    source 23
    target 141
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 23
    target 151
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 152
  ]
  edge [
    source 24
    target 153
  ]
  edge [
    source 24
    target 154
  ]
  edge [
    source 24
    target 155
  ]
  edge [
    source 24
    target 156
  ]
  edge [
    source 25
    target 157
  ]
  edge [
    source 25
    target 158
  ]
  edge [
    source 25
    target 159
  ]
  edge [
    source 25
    target 160
  ]
  edge [
    source 25
    target 32
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 162
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 163
  ]
  edge [
    source 26
    target 164
  ]
  edge [
    source 26
    target 165
  ]
  edge [
    source 26
    target 166
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 167
  ]
  edge [
    source 27
    target 168
  ]
  edge [
    source 27
    target 169
  ]
  edge [
    source 27
    target 170
  ]
  edge [
    source 27
    target 171
  ]
  edge [
    source 27
    target 172
  ]
  edge [
    source 27
    target 173
  ]
  edge [
    source 27
    target 174
  ]
  edge [
    source 27
    target 175
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 176
  ]
  edge [
    source 28
    target 177
  ]
  edge [
    source 28
    target 178
  ]
  edge [
    source 28
    target 179
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 180
  ]
  edge [
    source 29
    target 181
  ]
  edge [
    source 29
    target 60
  ]
  edge [
    source 29
    target 182
  ]
  edge [
    source 29
    target 183
  ]
  edge [
    source 29
    target 184
  ]
  edge [
    source 29
    target 185
  ]
  edge [
    source 29
    target 186
  ]
  edge [
    source 29
    target 187
  ]
  edge [
    source 29
    target 188
  ]
  edge [
    source 29
    target 189
  ]
  edge [
    source 29
    target 190
  ]
  edge [
    source 29
    target 191
  ]
  edge [
    source 29
    target 192
  ]
  edge [
    source 29
    target 193
  ]
  edge [
    source 29
    target 194
  ]
  edge [
    source 29
    target 195
  ]
  edge [
    source 29
    target 196
  ]
  edge [
    source 29
    target 197
  ]
  edge [
    source 29
    target 198
  ]
  edge [
    source 29
    target 199
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 200
  ]
  edge [
    source 30
    target 201
  ]
  edge [
    source 30
    target 202
  ]
  edge [
    source 30
    target 203
  ]
  edge [
    source 30
    target 204
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 205
  ]
  edge [
    source 31
    target 206
  ]
  edge [
    source 31
    target 207
  ]
  edge [
    source 31
    target 208
  ]
  edge [
    source 31
    target 209
  ]
  edge [
    source 31
    target 210
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 211
  ]
  edge [
    source 32
    target 212
  ]
  edge [
    source 32
    target 213
  ]
  edge [
    source 32
    target 214
  ]
  edge [
    source 32
    target 215
  ]
  edge [
    source 32
    target 216
  ]
  edge [
    source 32
    target 217
  ]
  edge [
    source 32
    target 218
  ]
  edge [
    source 32
    target 219
  ]
  edge [
    source 32
    target 220
  ]
  edge [
    source 32
    target 221
  ]
  edge [
    source 32
    target 222
  ]
  edge [
    source 32
    target 223
  ]
  edge [
    source 32
    target 224
  ]
  edge [
    source 32
    target 225
  ]
  edge [
    source 32
    target 226
  ]
  edge [
    source 32
    target 227
  ]
  edge [
    source 32
    target 228
  ]
  edge [
    source 32
    target 229
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 230
  ]
  edge [
    source 33
    target 231
  ]
  edge [
    source 33
    target 232
  ]
  edge [
    source 33
    target 233
  ]
  edge [
    source 33
    target 234
  ]
  edge [
    source 33
    target 235
  ]
  edge [
    source 33
    target 236
  ]
  edge [
    source 33
    target 237
  ]
  edge [
    source 33
    target 238
  ]
  edge [
    source 33
    target 43
  ]
  edge [
    source 33
    target 239
  ]
  edge [
    source 33
    target 240
  ]
  edge [
    source 33
    target 241
  ]
  edge [
    source 33
    target 242
  ]
  edge [
    source 35
    target 243
  ]
  edge [
    source 35
    target 244
  ]
  edge [
    source 35
    target 107
  ]
  edge [
    source 35
    target 245
  ]
  edge [
    source 35
    target 246
  ]
  edge [
    source 35
    target 247
  ]
  edge [
    source 35
    target 248
  ]
  edge [
    source 35
    target 249
  ]
  edge [
    source 35
    target 250
  ]
  edge [
    source 35
    target 251
  ]
  edge [
    source 35
    target 252
  ]
  edge [
    source 35
    target 253
  ]
  edge [
    source 40
    target 274
  ]
  edge [
    source 81
    target 254
  ]
  edge [
    source 81
    target 255
  ]
  edge [
    source 81
    target 289
  ]
  edge [
    source 254
    target 289
  ]
  edge [
    source 256
    target 257
  ]
  edge [
    source 258
    target 259
  ]
  edge [
    source 260
    target 261
  ]
  edge [
    source 262
    target 263
  ]
  edge [
    source 264
    target 265
  ]
  edge [
    source 266
    target 267
  ]
  edge [
    source 268
    target 269
  ]
  edge [
    source 270
    target 271
  ]
  edge [
    source 272
    target 273
  ]
  edge [
    source 275
    target 276
  ]
  edge [
    source 277
    target 278
  ]
  edge [
    source 277
    target 279
  ]
  edge [
    source 278
    target 279
  ]
  edge [
    source 280
    target 281
  ]
  edge [
    source 280
    target 282
  ]
  edge [
    source 280
    target 283
  ]
  edge [
    source 280
    target 284
  ]
  edge [
    source 280
    target 285
  ]
  edge [
    source 281
    target 282
  ]
  edge [
    source 281
    target 283
  ]
  edge [
    source 281
    target 284
  ]
  edge [
    source 281
    target 285
  ]
  edge [
    source 282
    target 283
  ]
  edge [
    source 282
    target 284
  ]
  edge [
    source 282
    target 285
  ]
  edge [
    source 283
    target 284
  ]
  edge [
    source 283
    target 285
  ]
  edge [
    source 284
    target 285
  ]
  edge [
    source 286
    target 287
  ]
  edge [
    source 286
    target 288
  ]
  edge [
    source 286
    target 289
  ]
  edge [
    source 287
    target 288
  ]
  edge [
    source 287
    target 289
  ]
  edge [
    source 288
    target 289
  ]
]
