graph [
  maxDegree 32
  minDegree 1
  meanDegree 1.945945945945946
  density 0.05405405405405406
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "filmpolski"
    origin "text"
  ]
  node [
    id 2
    label "film"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "miodowelata"
    origin "text"
  ]
  node [
    id 5
    label "diablo"
    origin "text"
  ]
  node [
    id 6
    label "polskiefilmy"
    origin "text"
  ]
  node [
    id 7
    label "rozbieg&#243;wka"
  ]
  node [
    id 8
    label "block"
  ]
  node [
    id 9
    label "odczula&#263;"
  ]
  node [
    id 10
    label "blik"
  ]
  node [
    id 11
    label "rola"
  ]
  node [
    id 12
    label "trawiarnia"
  ]
  node [
    id 13
    label "b&#322;ona"
  ]
  node [
    id 14
    label "filmoteka"
  ]
  node [
    id 15
    label "sztuka"
  ]
  node [
    id 16
    label "muza"
  ]
  node [
    id 17
    label "odczuli&#263;"
  ]
  node [
    id 18
    label "klatka"
  ]
  node [
    id 19
    label "odczulenie"
  ]
  node [
    id 20
    label "emulsja_fotograficzna"
  ]
  node [
    id 21
    label "animatronika"
  ]
  node [
    id 22
    label "dorobek"
  ]
  node [
    id 23
    label "odczulanie"
  ]
  node [
    id 24
    label "scena"
  ]
  node [
    id 25
    label "czo&#322;&#243;wka"
  ]
  node [
    id 26
    label "ty&#322;&#243;wka"
  ]
  node [
    id 27
    label "napisy"
  ]
  node [
    id 28
    label "photograph"
  ]
  node [
    id 29
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 30
    label "postprodukcja"
  ]
  node [
    id 31
    label "sklejarka"
  ]
  node [
    id 32
    label "anamorfoza"
  ]
  node [
    id 33
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 34
    label "ta&#347;ma"
  ]
  node [
    id 35
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 36
    label "uj&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
]
