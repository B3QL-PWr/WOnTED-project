graph [
  maxDegree 9
  minDegree 1
  meanDegree 2.2
  density 0.07586206896551724
  graphCliqueNumber 5
  node [
    id 0
    label "copernicus"
    origin "text"
  ]
  node [
    id 1
    label "nagroda"
    origin "text"
  ]
  node [
    id 2
    label "return"
  ]
  node [
    id 3
    label "konsekwencja"
  ]
  node [
    id 4
    label "oskar"
  ]
  node [
    id 5
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 6
    label "polsko"
  ]
  node [
    id 7
    label "niemiecki"
  ]
  node [
    id 8
    label "naukowy"
  ]
  node [
    id 9
    label "Copernicus"
  ]
  node [
    id 10
    label "Award"
  ]
  node [
    id 11
    label "fundacja"
  ]
  node [
    id 12
    label "na"
  ]
  node [
    id 13
    label "rzecz"
  ]
  node [
    id 14
    label "nauka"
  ]
  node [
    id 15
    label "polski"
  ]
  node [
    id 16
    label "deutsche"
  ]
  node [
    id 17
    label "Forschungsgemeinschaft"
  ]
  node [
    id 18
    label "Barbara"
  ]
  node [
    id 19
    label "Malinowska"
  ]
  node [
    id 20
    label "Eberhard"
  ]
  node [
    id 21
    label "Schlicker"
  ]
  node [
    id 22
    label "Andrzej"
  ]
  node [
    id 23
    label "sobolewski"
  ]
  node [
    id 24
    label "Wolfgang"
  ]
  node [
    id 25
    label "Domcke"
  ]
  node [
    id 26
    label "Alfreda"
  ]
  node [
    id 27
    label "Forchel"
  ]
  node [
    id 28
    label "Jan"
  ]
  node [
    id 29
    label "Misiewicz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
]
