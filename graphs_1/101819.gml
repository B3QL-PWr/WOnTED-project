graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.0579710144927534
  density 0.009990150555790066
  graphCliqueNumber 3
  node [
    id 0
    label "plastikowy"
    origin "text"
  ]
  node [
    id 1
    label "uk&#322;ad"
    origin "text"
  ]
  node [
    id 2
    label "kierowniczy"
    origin "text"
  ]
  node [
    id 3
    label "bellcrank"
    origin "text"
  ]
  node [
    id 4
    label "sprawowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "nasi"
    origin "text"
  ]
  node [
    id 6
    label "rustlerze"
    origin "text"
  ]
  node [
    id 7
    label "ca&#322;kiem"
    origin "text"
  ]
  node [
    id 8
    label "dobrze"
    origin "text"
  ]
  node [
    id 9
    label "czas"
    origin "text"
  ]
  node [
    id 10
    label "wymieni&#263;by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "obejm"
    origin "text"
  ]
  node [
    id 12
    label "zwrotnica"
    origin "text"
  ]
  node [
    id 13
    label "aluminiowy"
    origin "text"
  ]
  node [
    id 14
    label "ten"
    origin "text"
  ]
  node [
    id 15
    label "por"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 18
    label "uszkadza&#263;"
    origin "text"
  ]
  node [
    id 19
    label "element"
    origin "text"
  ]
  node [
    id 20
    label "skasowa&#263;"
    origin "text"
  ]
  node [
    id 21
    label "trzeci"
    origin "text"
  ]
  node [
    id 22
    label "zdecydowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "si&#281;"
    origin "text"
  ]
  node [
    id 24
    label "zakup"
    origin "text"
  ]
  node [
    id 25
    label "wersja"
    origin "text"
  ]
  node [
    id 26
    label "oferowa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "firma"
    origin "text"
  ]
  node [
    id 28
    label "integy"
    origin "text"
  ]
  node [
    id 29
    label "sztuczny"
  ]
  node [
    id 30
    label "chemiczny"
  ]
  node [
    id 31
    label "sk&#322;ad"
  ]
  node [
    id 32
    label "zachowanie"
  ]
  node [
    id 33
    label "umowa"
  ]
  node [
    id 34
    label "podsystem"
  ]
  node [
    id 35
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 36
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 37
    label "system"
  ]
  node [
    id 38
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 39
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 40
    label "struktura"
  ]
  node [
    id 41
    label "wi&#281;&#378;"
  ]
  node [
    id 42
    label "zawarcie"
  ]
  node [
    id 43
    label "systemat"
  ]
  node [
    id 44
    label "usenet"
  ]
  node [
    id 45
    label "ONZ"
  ]
  node [
    id 46
    label "o&#347;"
  ]
  node [
    id 47
    label "organ"
  ]
  node [
    id 48
    label "przestawi&#263;"
  ]
  node [
    id 49
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 50
    label "traktat_wersalski"
  ]
  node [
    id 51
    label "rozprz&#261;c"
  ]
  node [
    id 52
    label "cybernetyk"
  ]
  node [
    id 53
    label "cia&#322;o"
  ]
  node [
    id 54
    label "zawrze&#263;"
  ]
  node [
    id 55
    label "konstelacja"
  ]
  node [
    id 56
    label "alliance"
  ]
  node [
    id 57
    label "zbi&#243;r"
  ]
  node [
    id 58
    label "NATO"
  ]
  node [
    id 59
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 60
    label "treaty"
  ]
  node [
    id 61
    label "prosecute"
  ]
  node [
    id 62
    label "wniwecz"
  ]
  node [
    id 63
    label "zupe&#322;ny"
  ]
  node [
    id 64
    label "moralnie"
  ]
  node [
    id 65
    label "wiele"
  ]
  node [
    id 66
    label "lepiej"
  ]
  node [
    id 67
    label "korzystnie"
  ]
  node [
    id 68
    label "pomy&#347;lnie"
  ]
  node [
    id 69
    label "pozytywnie"
  ]
  node [
    id 70
    label "dobry"
  ]
  node [
    id 71
    label "dobroczynnie"
  ]
  node [
    id 72
    label "odpowiednio"
  ]
  node [
    id 73
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 74
    label "skutecznie"
  ]
  node [
    id 75
    label "czasokres"
  ]
  node [
    id 76
    label "trawienie"
  ]
  node [
    id 77
    label "kategoria_gramatyczna"
  ]
  node [
    id 78
    label "period"
  ]
  node [
    id 79
    label "odczyt"
  ]
  node [
    id 80
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 81
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 82
    label "chwila"
  ]
  node [
    id 83
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 84
    label "poprzedzenie"
  ]
  node [
    id 85
    label "koniugacja"
  ]
  node [
    id 86
    label "dzieje"
  ]
  node [
    id 87
    label "poprzedzi&#263;"
  ]
  node [
    id 88
    label "przep&#322;ywanie"
  ]
  node [
    id 89
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 90
    label "odwlekanie_si&#281;"
  ]
  node [
    id 91
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 92
    label "Zeitgeist"
  ]
  node [
    id 93
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 94
    label "okres_czasu"
  ]
  node [
    id 95
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 96
    label "pochodzi&#263;"
  ]
  node [
    id 97
    label "schy&#322;ek"
  ]
  node [
    id 98
    label "czwarty_wymiar"
  ]
  node [
    id 99
    label "chronometria"
  ]
  node [
    id 100
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 101
    label "poprzedzanie"
  ]
  node [
    id 102
    label "pogoda"
  ]
  node [
    id 103
    label "zegar"
  ]
  node [
    id 104
    label "pochodzenie"
  ]
  node [
    id 105
    label "poprzedza&#263;"
  ]
  node [
    id 106
    label "trawi&#263;"
  ]
  node [
    id 107
    label "time_period"
  ]
  node [
    id 108
    label "rachuba_czasu"
  ]
  node [
    id 109
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 110
    label "czasoprzestrze&#324;"
  ]
  node [
    id 111
    label "laba"
  ]
  node [
    id 112
    label "blokada"
  ]
  node [
    id 113
    label "opornica"
  ]
  node [
    id 114
    label "switch"
  ]
  node [
    id 115
    label "urz&#261;dzenie"
  ]
  node [
    id 116
    label "glinowy"
  ]
  node [
    id 117
    label "metaliczny"
  ]
  node [
    id 118
    label "okre&#347;lony"
  ]
  node [
    id 119
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 120
    label "otw&#243;r"
  ]
  node [
    id 121
    label "w&#322;oszczyzna"
  ]
  node [
    id 122
    label "warzywo"
  ]
  node [
    id 123
    label "czosnek"
  ]
  node [
    id 124
    label "kapelusz"
  ]
  node [
    id 125
    label "uj&#347;cie"
  ]
  node [
    id 126
    label "si&#281;ga&#263;"
  ]
  node [
    id 127
    label "trwa&#263;"
  ]
  node [
    id 128
    label "obecno&#347;&#263;"
  ]
  node [
    id 129
    label "stan"
  ]
  node [
    id 130
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 131
    label "stand"
  ]
  node [
    id 132
    label "mie&#263;_miejsce"
  ]
  node [
    id 133
    label "uczestniczy&#263;"
  ]
  node [
    id 134
    label "chodzi&#263;"
  ]
  node [
    id 135
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 136
    label "equal"
  ]
  node [
    id 137
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 138
    label "cz&#281;sty"
  ]
  node [
    id 139
    label "powodowa&#263;"
  ]
  node [
    id 140
    label "narusza&#263;"
  ]
  node [
    id 141
    label "mar"
  ]
  node [
    id 142
    label "pamper"
  ]
  node [
    id 143
    label "szkodnik"
  ]
  node [
    id 144
    label "&#347;rodowisko"
  ]
  node [
    id 145
    label "component"
  ]
  node [
    id 146
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 147
    label "r&#243;&#380;niczka"
  ]
  node [
    id 148
    label "przedmiot"
  ]
  node [
    id 149
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 150
    label "gangsterski"
  ]
  node [
    id 151
    label "szambo"
  ]
  node [
    id 152
    label "materia"
  ]
  node [
    id 153
    label "aspo&#322;eczny"
  ]
  node [
    id 154
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 155
    label "poj&#281;cie"
  ]
  node [
    id 156
    label "underworld"
  ]
  node [
    id 157
    label "zamordowa&#263;"
  ]
  node [
    id 158
    label "wzi&#261;&#263;"
  ]
  node [
    id 159
    label "kill"
  ]
  node [
    id 160
    label "usun&#261;&#263;"
  ]
  node [
    id 161
    label "oznaczy&#263;"
  ]
  node [
    id 162
    label "retract"
  ]
  node [
    id 163
    label "roztrzaska&#263;"
  ]
  node [
    id 164
    label "naliczy&#263;"
  ]
  node [
    id 165
    label "uchyli&#263;"
  ]
  node [
    id 166
    label "revoke"
  ]
  node [
    id 167
    label "cz&#322;owiek"
  ]
  node [
    id 168
    label "neutralny"
  ]
  node [
    id 169
    label "przypadkowy"
  ]
  node [
    id 170
    label "dzie&#324;"
  ]
  node [
    id 171
    label "postronnie"
  ]
  node [
    id 172
    label "sprzedaj&#261;cy"
  ]
  node [
    id 173
    label "dobro"
  ]
  node [
    id 174
    label "transakcja"
  ]
  node [
    id 175
    label "typ"
  ]
  node [
    id 176
    label "posta&#263;"
  ]
  node [
    id 177
    label "volunteer"
  ]
  node [
    id 178
    label "zach&#281;ca&#263;"
  ]
  node [
    id 179
    label "Hortex"
  ]
  node [
    id 180
    label "MAC"
  ]
  node [
    id 181
    label "reengineering"
  ]
  node [
    id 182
    label "nazwa_w&#322;asna"
  ]
  node [
    id 183
    label "podmiot_gospodarczy"
  ]
  node [
    id 184
    label "Google"
  ]
  node [
    id 185
    label "zaufanie"
  ]
  node [
    id 186
    label "biurowiec"
  ]
  node [
    id 187
    label "networking"
  ]
  node [
    id 188
    label "zasoby_ludzkie"
  ]
  node [
    id 189
    label "interes"
  ]
  node [
    id 190
    label "paczkarnia"
  ]
  node [
    id 191
    label "Canon"
  ]
  node [
    id 192
    label "HP"
  ]
  node [
    id 193
    label "Baltona"
  ]
  node [
    id 194
    label "Pewex"
  ]
  node [
    id 195
    label "MAN_SE"
  ]
  node [
    id 196
    label "Apeks"
  ]
  node [
    id 197
    label "zasoby"
  ]
  node [
    id 198
    label "Orbis"
  ]
  node [
    id 199
    label "miejsce_pracy"
  ]
  node [
    id 200
    label "siedziba"
  ]
  node [
    id 201
    label "Spo&#322;em"
  ]
  node [
    id 202
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 203
    label "Orlen"
  ]
  node [
    id 204
    label "klasa"
  ]
  node [
    id 205
    label "RC"
  ]
  node [
    id 206
    label "smok"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 13
    target 26
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 177
  ]
  edge [
    source 26
    target 178
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 167
  ]
  edge [
    source 27
    target 179
  ]
  edge [
    source 27
    target 180
  ]
  edge [
    source 27
    target 181
  ]
  edge [
    source 27
    target 182
  ]
  edge [
    source 27
    target 183
  ]
  edge [
    source 27
    target 184
  ]
  edge [
    source 27
    target 185
  ]
  edge [
    source 27
    target 186
  ]
  edge [
    source 27
    target 187
  ]
  edge [
    source 27
    target 188
  ]
  edge [
    source 27
    target 189
  ]
  edge [
    source 27
    target 190
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 27
    target 192
  ]
  edge [
    source 27
    target 193
  ]
  edge [
    source 27
    target 194
  ]
  edge [
    source 27
    target 195
  ]
  edge [
    source 27
    target 196
  ]
  edge [
    source 27
    target 197
  ]
  edge [
    source 27
    target 198
  ]
  edge [
    source 27
    target 199
  ]
  edge [
    source 27
    target 200
  ]
  edge [
    source 27
    target 201
  ]
  edge [
    source 27
    target 202
  ]
  edge [
    source 27
    target 203
  ]
  edge [
    source 27
    target 204
  ]
  edge [
    source 205
    target 206
  ]
]
