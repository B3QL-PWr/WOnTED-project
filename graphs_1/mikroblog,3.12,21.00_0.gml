graph [
  maxDegree 35
  minDegree 1
  meanDegree 1.9696969696969697
  density 0.030303030303030304
  graphCliqueNumber 2
  node [
    id 0
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "salon"
    origin "text"
  ]
  node [
    id 2
    label "sprzeda&#380;"
    origin "text"
  ]
  node [
    id 3
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "kom&#243;rkowy"
    origin "text"
  ]
  node [
    id 5
    label "endeavor"
  ]
  node [
    id 6
    label "funkcjonowa&#263;"
  ]
  node [
    id 7
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 8
    label "mie&#263;_miejsce"
  ]
  node [
    id 9
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 10
    label "dzia&#322;a&#263;"
  ]
  node [
    id 11
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 12
    label "work"
  ]
  node [
    id 13
    label "bangla&#263;"
  ]
  node [
    id 14
    label "do"
  ]
  node [
    id 15
    label "maszyna"
  ]
  node [
    id 16
    label "tryb"
  ]
  node [
    id 17
    label "dziama&#263;"
  ]
  node [
    id 18
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 19
    label "praca"
  ]
  node [
    id 20
    label "podejmowa&#263;"
  ]
  node [
    id 21
    label "pok&#243;j"
  ]
  node [
    id 22
    label "kanapa"
  ]
  node [
    id 23
    label "sklep"
  ]
  node [
    id 24
    label "zak&#322;ad"
  ]
  node [
    id 25
    label "przyj&#281;cie"
  ]
  node [
    id 26
    label "telewizor"
  ]
  node [
    id 27
    label "komplet_wypoczynkowy"
  ]
  node [
    id 28
    label "transakcja"
  ]
  node [
    id 29
    label "sprzedaj&#261;cy"
  ]
  node [
    id 30
    label "przeniesienie_praw"
  ]
  node [
    id 31
    label "rabat"
  ]
  node [
    id 32
    label "przeda&#380;"
  ]
  node [
    id 33
    label "hipertekst"
  ]
  node [
    id 34
    label "gauze"
  ]
  node [
    id 35
    label "nitka"
  ]
  node [
    id 36
    label "mesh"
  ]
  node [
    id 37
    label "e-hazard"
  ]
  node [
    id 38
    label "netbook"
  ]
  node [
    id 39
    label "cyberprzestrze&#324;"
  ]
  node [
    id 40
    label "biznes_elektroniczny"
  ]
  node [
    id 41
    label "snu&#263;"
  ]
  node [
    id 42
    label "organization"
  ]
  node [
    id 43
    label "zasadzka"
  ]
  node [
    id 44
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 45
    label "web"
  ]
  node [
    id 46
    label "provider"
  ]
  node [
    id 47
    label "struktura"
  ]
  node [
    id 48
    label "us&#322;uga_internetowa"
  ]
  node [
    id 49
    label "punkt_dost&#281;pu"
  ]
  node [
    id 50
    label "organizacja"
  ]
  node [
    id 51
    label "mem"
  ]
  node [
    id 52
    label "vane"
  ]
  node [
    id 53
    label "podcast"
  ]
  node [
    id 54
    label "grooming"
  ]
  node [
    id 55
    label "kszta&#322;t"
  ]
  node [
    id 56
    label "strona"
  ]
  node [
    id 57
    label "obiekt"
  ]
  node [
    id 58
    label "wysnu&#263;"
  ]
  node [
    id 59
    label "gra_sieciowa"
  ]
  node [
    id 60
    label "instalacja"
  ]
  node [
    id 61
    label "sie&#263;_komputerowa"
  ]
  node [
    id 62
    label "net"
  ]
  node [
    id 63
    label "plecionka"
  ]
  node [
    id 64
    label "media"
  ]
  node [
    id 65
    label "rozmieszczenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
]
