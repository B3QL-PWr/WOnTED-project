graph [
  maxDegree 17
  minDegree 1
  meanDegree 2.024390243902439
  density 0.02499247214694369
  graphCliqueNumber 3
  node [
    id 0
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 1
    label "ruszy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "proces"
    origin "text"
  ]
  node [
    id 3
    label "sprawa"
    origin "text"
  ]
  node [
    id 4
    label "brutalny"
    origin "text"
  ]
  node [
    id 5
    label "pobi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 7
    label "wiceszef"
    origin "text"
  ]
  node [
    id 8
    label "knf"
    origin "text"
  ]
  node [
    id 9
    label "wojciech"
    origin "text"
  ]
  node [
    id 10
    label "kwa&#347;niaka"
    origin "text"
  ]
  node [
    id 11
    label "dowiedzie&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "reporter"
    origin "text"
  ]
  node [
    id 14
    label "rmf"
    origin "text"
  ]
  node [
    id 15
    label "motivate"
  ]
  node [
    id 16
    label "zabra&#263;"
  ]
  node [
    id 17
    label "wzbudzi&#263;"
  ]
  node [
    id 18
    label "zacz&#261;&#263;"
  ]
  node [
    id 19
    label "spowodowa&#263;"
  ]
  node [
    id 20
    label "allude"
  ]
  node [
    id 21
    label "stimulate"
  ]
  node [
    id 22
    label "cut"
  ]
  node [
    id 23
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 24
    label "zrobi&#263;"
  ]
  node [
    id 25
    label "go"
  ]
  node [
    id 26
    label "legislacyjnie"
  ]
  node [
    id 27
    label "kognicja"
  ]
  node [
    id 28
    label "przebieg"
  ]
  node [
    id 29
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 30
    label "wydarzenie"
  ]
  node [
    id 31
    label "przes&#322;anka"
  ]
  node [
    id 32
    label "rozprawa"
  ]
  node [
    id 33
    label "zjawisko"
  ]
  node [
    id 34
    label "nast&#281;pstwo"
  ]
  node [
    id 35
    label "temat"
  ]
  node [
    id 36
    label "idea"
  ]
  node [
    id 37
    label "szczeg&#243;&#322;"
  ]
  node [
    id 38
    label "rzecz"
  ]
  node [
    id 39
    label "object"
  ]
  node [
    id 40
    label "proposition"
  ]
  node [
    id 41
    label "szczery"
  ]
  node [
    id 42
    label "drastycznie"
  ]
  node [
    id 43
    label "silny"
  ]
  node [
    id 44
    label "bezlitosny"
  ]
  node [
    id 45
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 46
    label "okrutny"
  ]
  node [
    id 47
    label "brutalnie"
  ]
  node [
    id 48
    label "przemoc"
  ]
  node [
    id 49
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 50
    label "mocny"
  ]
  node [
    id 51
    label "niedelikatny"
  ]
  node [
    id 52
    label "pozabija&#263;"
  ]
  node [
    id 53
    label "pousuwa&#263;"
  ]
  node [
    id 54
    label "transgress"
  ]
  node [
    id 55
    label "beat"
  ]
  node [
    id 56
    label "pokona&#263;"
  ]
  node [
    id 57
    label "upset"
  ]
  node [
    id 58
    label "zbi&#263;"
  ]
  node [
    id 59
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 60
    label "wpierniczy&#263;"
  ]
  node [
    id 61
    label "nala&#263;"
  ]
  node [
    id 62
    label "pouderza&#263;"
  ]
  node [
    id 63
    label "obi&#263;"
  ]
  node [
    id 64
    label "pomacha&#263;"
  ]
  node [
    id 65
    label "poniszczy&#263;"
  ]
  node [
    id 66
    label "wygra&#263;"
  ]
  node [
    id 67
    label "dawny"
  ]
  node [
    id 68
    label "rozw&#243;d"
  ]
  node [
    id 69
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 70
    label "eksprezydent"
  ]
  node [
    id 71
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 72
    label "partner"
  ]
  node [
    id 73
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 74
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 75
    label "wcze&#347;niejszy"
  ]
  node [
    id 76
    label "zast&#281;pca"
  ]
  node [
    id 77
    label "dziennikarz"
  ]
  node [
    id 78
    label "Wojciech"
  ]
  node [
    id 79
    label "Kwa&#347;niaka"
  ]
  node [
    id 80
    label "RMF"
  ]
  node [
    id 81
    label "FM"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 80
    target 81
  ]
]
