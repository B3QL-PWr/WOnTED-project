graph [
  maxDegree 26
  minDegree 1
  meanDegree 1.9523809523809523
  density 0.047619047619047616
  graphCliqueNumber 2
  node [
    id 0
    label "&#380;y&#263;"
    origin "text"
  ]
  node [
    id 1
    label "danuta"
    origin "text"
  ]
  node [
    id 2
    label "ga&#322;kowy"
    origin "text"
  ]
  node [
    id 3
    label "post_scriptum"
    origin "text"
  ]
  node [
    id 4
    label "&#322;&#261;czniczka"
    origin "text"
  ]
  node [
    id 5
    label "powstanie"
    origin "text"
  ]
  node [
    id 6
    label "warszawski"
    origin "text"
  ]
  node [
    id 7
    label "pause"
  ]
  node [
    id 8
    label "stay"
  ]
  node [
    id 9
    label "consist"
  ]
  node [
    id 10
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 11
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 12
    label "istnie&#263;"
  ]
  node [
    id 13
    label "korzenny"
  ]
  node [
    id 14
    label "uniesienie_si&#281;"
  ]
  node [
    id 15
    label "geneza"
  ]
  node [
    id 16
    label "chmielnicczyzna"
  ]
  node [
    id 17
    label "beginning"
  ]
  node [
    id 18
    label "orgy"
  ]
  node [
    id 19
    label "Ko&#347;ciuszko"
  ]
  node [
    id 20
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 21
    label "utworzenie"
  ]
  node [
    id 22
    label "powstanie_listopadowe"
  ]
  node [
    id 23
    label "potworzenie_si&#281;"
  ]
  node [
    id 24
    label "powstanie_warszawskie"
  ]
  node [
    id 25
    label "kl&#281;czenie"
  ]
  node [
    id 26
    label "zbuntowanie_si&#281;"
  ]
  node [
    id 27
    label "siedzenie"
  ]
  node [
    id 28
    label "stworzenie"
  ]
  node [
    id 29
    label "walka"
  ]
  node [
    id 30
    label "zaistnienie"
  ]
  node [
    id 31
    label "odbudowanie_si&#281;"
  ]
  node [
    id 32
    label "pierwocina"
  ]
  node [
    id 33
    label "origin"
  ]
  node [
    id 34
    label "koliszczyzna"
  ]
  node [
    id 35
    label "powstanie_tambowskie"
  ]
  node [
    id 36
    label "&#380;akieria"
  ]
  node [
    id 37
    label "le&#380;enie"
  ]
  node [
    id 38
    label "po_warszawsku"
  ]
  node [
    id 39
    label "marmuzela"
  ]
  node [
    id 40
    label "mazowiecki"
  ]
  node [
    id 41
    label "Danuta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
]
