graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.068027210884354
  density 0.014164569937564066
  graphCliqueNumber 3
  node [
    id 0
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "rozpocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "xxiv"
    origin "text"
  ]
  node [
    id 4
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 5
    label "turniej"
    origin "text"
  ]
  node [
    id 6
    label "puchar"
    origin "text"
  ]
  node [
    id 7
    label "syrenka"
    origin "text"
  ]
  node [
    id 8
    label "m&#322;odzie&#380;owy"
    origin "text"
  ]
  node [
    id 9
    label "impreza"
    origin "text"
  ]
  node [
    id 10
    label "pi&#322;karski"
    origin "text"
  ]
  node [
    id 11
    label "bez"
    origin "text"
  ]
  node [
    id 12
    label "precedens"
    origin "text"
  ]
  node [
    id 13
    label "tylko"
    origin "text"
  ]
  node [
    id 14
    label "nasi"
    origin "text"
  ]
  node [
    id 15
    label "ale"
    origin "text"
  ]
  node [
    id 16
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 17
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 18
    label "europejski"
    origin "text"
  ]
  node [
    id 19
    label "futbol"
    origin "text"
  ]
  node [
    id 20
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 21
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 22
    label "mecz"
    origin "text"
  ]
  node [
    id 23
    label "miejsce"
    origin "text"
  ]
  node [
    id 24
    label "miesi&#261;c"
  ]
  node [
    id 25
    label "cause"
  ]
  node [
    id 26
    label "zacz&#261;&#263;"
  ]
  node [
    id 27
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 28
    label "do"
  ]
  node [
    id 29
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 30
    label "zrobi&#263;"
  ]
  node [
    id 31
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 32
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 33
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 34
    label "eliminacje"
  ]
  node [
    id 35
    label "zawody"
  ]
  node [
    id 36
    label "Wielki_Szlem"
  ]
  node [
    id 37
    label "drive"
  ]
  node [
    id 38
    label "pojedynek"
  ]
  node [
    id 39
    label "runda"
  ]
  node [
    id 40
    label "tournament"
  ]
  node [
    id 41
    label "rywalizacja"
  ]
  node [
    id 42
    label "zawarto&#347;&#263;"
  ]
  node [
    id 43
    label "zwyci&#281;stwo"
  ]
  node [
    id 44
    label "naczynie"
  ]
  node [
    id 45
    label "nagroda"
  ]
  node [
    id 46
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 47
    label "Syrena"
  ]
  node [
    id 48
    label "samoch&#243;d"
  ]
  node [
    id 49
    label "m&#322;odzie&#380;owo"
  ]
  node [
    id 50
    label "party"
  ]
  node [
    id 51
    label "rozrywka"
  ]
  node [
    id 52
    label "przyj&#281;cie"
  ]
  node [
    id 53
    label "okazja"
  ]
  node [
    id 54
    label "impra"
  ]
  node [
    id 55
    label "specjalny"
  ]
  node [
    id 56
    label "sportowy"
  ]
  node [
    id 57
    label "po_pi&#322;karsku"
  ]
  node [
    id 58
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 59
    label "typowy"
  ]
  node [
    id 60
    label "pi&#322;karsko"
  ]
  node [
    id 61
    label "ki&#347;&#263;"
  ]
  node [
    id 62
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 63
    label "krzew"
  ]
  node [
    id 64
    label "pi&#380;maczkowate"
  ]
  node [
    id 65
    label "pestkowiec"
  ]
  node [
    id 66
    label "kwiat"
  ]
  node [
    id 67
    label "owoc"
  ]
  node [
    id 68
    label "oliwkowate"
  ]
  node [
    id 69
    label "ro&#347;lina"
  ]
  node [
    id 70
    label "hy&#263;ka"
  ]
  node [
    id 71
    label "lilac"
  ]
  node [
    id 72
    label "delfinidyna"
  ]
  node [
    id 73
    label "precedent"
  ]
  node [
    id 74
    label "orzeczenie"
  ]
  node [
    id 75
    label "wydarzenie"
  ]
  node [
    id 76
    label "przypadek"
  ]
  node [
    id 77
    label "piwo"
  ]
  node [
    id 78
    label "du&#380;y"
  ]
  node [
    id 79
    label "jedyny"
  ]
  node [
    id 80
    label "kompletny"
  ]
  node [
    id 81
    label "zdr&#243;w"
  ]
  node [
    id 82
    label "&#380;ywy"
  ]
  node [
    id 83
    label "ca&#322;o"
  ]
  node [
    id 84
    label "pe&#322;ny"
  ]
  node [
    id 85
    label "calu&#347;ko"
  ]
  node [
    id 86
    label "podobny"
  ]
  node [
    id 87
    label "European"
  ]
  node [
    id 88
    label "po_europejsku"
  ]
  node [
    id 89
    label "charakterystyczny"
  ]
  node [
    id 90
    label "europejsko"
  ]
  node [
    id 91
    label "kopa&#263;"
  ]
  node [
    id 92
    label "jedenastka"
  ]
  node [
    id 93
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 94
    label "sfaulowanie"
  ]
  node [
    id 95
    label "kopn&#261;&#263;"
  ]
  node [
    id 96
    label "Wis&#322;a"
  ]
  node [
    id 97
    label "ekstraklasa"
  ]
  node [
    id 98
    label "kopni&#281;cie"
  ]
  node [
    id 99
    label "r&#281;ka"
  ]
  node [
    id 100
    label "bramkarz"
  ]
  node [
    id 101
    label "zamurowywa&#263;"
  ]
  node [
    id 102
    label "dogranie"
  ]
  node [
    id 103
    label "catenaccio"
  ]
  node [
    id 104
    label "lobowanie"
  ]
  node [
    id 105
    label "tackle"
  ]
  node [
    id 106
    label "interliga"
  ]
  node [
    id 107
    label "lobowa&#263;"
  ]
  node [
    id 108
    label "pi&#322;ka"
  ]
  node [
    id 109
    label "bezbramkowy"
  ]
  node [
    id 110
    label "zamurowywanie"
  ]
  node [
    id 111
    label "przelobowa&#263;"
  ]
  node [
    id 112
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 113
    label "dogrywanie"
  ]
  node [
    id 114
    label "zamurowanie"
  ]
  node [
    id 115
    label "dublet"
  ]
  node [
    id 116
    label "zamurowa&#263;"
  ]
  node [
    id 117
    label "przelobowanie"
  ]
  node [
    id 118
    label "dogra&#263;"
  ]
  node [
    id 119
    label "kopanie"
  ]
  node [
    id 120
    label "mundial"
  ]
  node [
    id 121
    label "dogrywa&#263;"
  ]
  node [
    id 122
    label "faulowanie"
  ]
  node [
    id 123
    label "sfaulowa&#263;"
  ]
  node [
    id 124
    label "faulowa&#263;"
  ]
  node [
    id 125
    label "czerwona_kartka"
  ]
  node [
    id 126
    label "reserve"
  ]
  node [
    id 127
    label "przej&#347;&#263;"
  ]
  node [
    id 128
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 129
    label "obrona"
  ]
  node [
    id 130
    label "gra"
  ]
  node [
    id 131
    label "dwumecz"
  ]
  node [
    id 132
    label "game"
  ]
  node [
    id 133
    label "serw"
  ]
  node [
    id 134
    label "cia&#322;o"
  ]
  node [
    id 135
    label "plac"
  ]
  node [
    id 136
    label "cecha"
  ]
  node [
    id 137
    label "uwaga"
  ]
  node [
    id 138
    label "przestrze&#324;"
  ]
  node [
    id 139
    label "status"
  ]
  node [
    id 140
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 141
    label "chwila"
  ]
  node [
    id 142
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 143
    label "rz&#261;d"
  ]
  node [
    id 144
    label "praca"
  ]
  node [
    id 145
    label "location"
  ]
  node [
    id 146
    label "warunek_lokalowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 78
  ]
  edge [
    source 17
    target 79
  ]
  edge [
    source 17
    target 80
  ]
  edge [
    source 17
    target 81
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 17
    target 85
  ]
  edge [
    source 17
    target 86
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 87
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 89
  ]
  edge [
    source 18
    target 90
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 59
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 91
  ]
  edge [
    source 19
    target 92
  ]
  edge [
    source 19
    target 93
  ]
  edge [
    source 19
    target 94
  ]
  edge [
    source 19
    target 95
  ]
  edge [
    source 19
    target 96
  ]
  edge [
    source 19
    target 97
  ]
  edge [
    source 19
    target 98
  ]
  edge [
    source 19
    target 99
  ]
  edge [
    source 19
    target 100
  ]
  edge [
    source 19
    target 101
  ]
  edge [
    source 19
    target 102
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 106
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 108
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 19
    target 111
  ]
  edge [
    source 19
    target 112
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 114
  ]
  edge [
    source 19
    target 115
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 126
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 130
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 22
    target 133
  ]
  edge [
    source 23
    target 134
  ]
  edge [
    source 23
    target 135
  ]
  edge [
    source 23
    target 136
  ]
  edge [
    source 23
    target 137
  ]
  edge [
    source 23
    target 138
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 23
    target 140
  ]
  edge [
    source 23
    target 141
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 146
  ]
]
