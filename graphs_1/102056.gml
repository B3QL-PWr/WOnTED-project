graph [
  maxDegree 44
  minDegree 1
  meanDegree 2.3392568659127626
  density 0.003785205284648483
  graphCliqueNumber 3
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "forum"
    origin "text"
  ]
  node [
    id 2
    label "pismak"
    origin "text"
  ]
  node [
    id 3
    label "dobiec"
    origin "text"
  ]
  node [
    id 4
    label "koniec"
    origin "text"
  ]
  node [
    id 5
    label "emocja"
    origin "text"
  ]
  node [
    id 6
    label "opad&#322;y"
    origin "text"
  ]
  node [
    id 7
    label "redakcja"
    origin "text"
  ]
  node [
    id 8
    label "wraca&#263;"
    origin "text"
  ]
  node [
    id 9
    label "poci&#261;g"
    origin "text"
  ]
  node [
    id 10
    label "autobus"
    origin "text"
  ]
  node [
    id 11
    label "swoje"
    origin "text"
  ]
  node [
    id 12
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 13
    label "dla"
    origin "text"
  ]
  node [
    id 14
    label "wszyscy"
    origin "text"
  ]
  node [
    id 15
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 16
    label "mogel"
    origin "text"
  ]
  node [
    id 17
    label "by&#263;"
    origin "text"
  ]
  node [
    id 18
    label "obecni"
    origin "text"
  ]
  node [
    id 19
    label "wa&#322;brzych"
    origin "text"
  ]
  node [
    id 20
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 21
    label "pierwsza"
    origin "text"
  ]
  node [
    id 22
    label "relacja"
    origin "text"
  ]
  node [
    id 23
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 24
    label "og&#243;lnopolski"
    origin "text"
  ]
  node [
    id 25
    label "konkurs"
    origin "text"
  ]
  node [
    id 26
    label "gazetka"
    origin "text"
  ]
  node [
    id 27
    label "szkolny"
    origin "text"
  ]
  node [
    id 28
    label "wynik"
    origin "text"
  ]
  node [
    id 29
    label "wybrana"
    origin "text"
  ]
  node [
    id 30
    label "tekst"
    origin "text"
  ]
  node [
    id 31
    label "nagrodzi&#263;"
    origin "text"
  ]
  node [
    id 32
    label "kategoria"
    origin "text"
  ]
  node [
    id 33
    label "indywidualny"
    origin "text"
  ]
  node [
    id 34
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 35
    label "przeczyta&#263;"
    origin "text"
  ]
  node [
    id 36
    label "qmamie"
    origin "text"
  ]
  node [
    id 37
    label "mig"
    origin "text"
  ]
  node [
    id 38
    label "publiczny"
    origin "text"
  ]
  node [
    id 39
    label "gimnazjum"
    origin "text"
  ]
  node [
    id 40
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 41
    label "odbywa&#263;"
    origin "text"
  ]
  node [
    id 42
    label "si&#281;"
    origin "text"
  ]
  node [
    id 43
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 44
    label "przed"
    origin "text"
  ]
  node [
    id 45
    label "fina&#322;owy"
    origin "text"
  ]
  node [
    id 46
    label "gala"
    origin "text"
  ]
  node [
    id 47
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 48
    label "kilka"
    origin "text"
  ]
  node [
    id 49
    label "lata"
    origin "text"
  ]
  node [
    id 50
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 51
    label "warsztat"
    origin "text"
  ]
  node [
    id 52
    label "dziennikarski"
    origin "text"
  ]
  node [
    id 53
    label "rezultat"
    origin "text"
  ]
  node [
    id 54
    label "tym"
    origin "text"
  ]
  node [
    id 55
    label "rok"
    origin "text"
  ]
  node [
    id 56
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 57
    label "qmamy"
    origin "text"
  ]
  node [
    id 58
    label "raz"
    origin "text"
  ]
  node [
    id 59
    label "pierwszy"
    origin "text"
  ]
  node [
    id 60
    label "nauczycielski"
    origin "text"
  ]
  node [
    id 61
    label "dwa"
    origin "text"
  ]
  node [
    id 62
    label "pod"
    origin "text"
  ]
  node [
    id 63
    label "wodza"
    origin "text"
  ]
  node [
    id 64
    label "monika"
    origin "text"
  ]
  node [
    id 65
    label "toppich"
    origin "text"
  ]
  node [
    id 66
    label "kamil"
    origin "text"
  ]
  node [
    id 67
    label "wi&#347;niewski"
    origin "text"
  ]
  node [
    id 68
    label "m&#322;ode"
    origin "text"
  ]
  node [
    id 69
    label "redaktor"
    origin "text"
  ]
  node [
    id 70
    label "pismo"
    origin "text"
  ]
  node [
    id 71
    label "m&#322;odzie&#380;owy"
    origin "text"
  ]
  node [
    id 72
    label "outro"
    origin "text"
  ]
  node [
    id 73
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 74
    label "powstanie"
    origin "text"
  ]
  node [
    id 75
    label "godzina"
    origin "text"
  ]
  node [
    id 76
    label "the"
    origin "text"
  ]
  node [
    id 77
    label "qmax"
    origin "text"
  ]
  node [
    id 78
    label "nauczycielka"
    origin "text"
  ]
  node [
    id 79
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 80
    label "opiekun"
    origin "text"
  ]
  node [
    id 81
    label "przyby&#322;a"
    origin "text"
  ]
  node [
    id 82
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 83
    label "&#347;wirak&#243;w"
    origin "text"
  ]
  node [
    id 84
    label "ksi&#261;&#380;&#281;cy"
    origin "text"
  ]
  node [
    id 85
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 86
    label "lektura"
    origin "text"
  ]
  node [
    id 87
    label "gazeta"
    origin "text"
  ]
  node [
    id 88
    label "&#347;ledzi&#263;"
    origin "text"
  ]
  node [
    id 89
    label "strona"
    origin "text"
  ]
  node [
    id 90
    label "gdzie"
    origin "text"
  ]
  node [
    id 91
    label "ukazywa&#263;"
    origin "text"
  ]
  node [
    id 92
    label "kolejny"
    origin "text"
  ]
  node [
    id 93
    label "poleca&#263;"
    origin "text"
  ]
  node [
    id 94
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 95
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 96
    label "telewizja"
    origin "text"
  ]
  node [
    id 97
    label "breaktv"
    origin "text"
  ]
  node [
    id 98
    label "relacjonowa&#263;"
    origin "text"
  ]
  node [
    id 99
    label "specjalny"
    origin "text"
  ]
  node [
    id 100
    label "ostatnie"
    origin "text"
  ]
  node [
    id 101
    label "trzy"
    origin "text"
  ]
  node [
    id 102
    label "dni"
    origin "text"
  ]
  node [
    id 103
    label "matczysko"
  ]
  node [
    id 104
    label "macierz"
  ]
  node [
    id 105
    label "przodkini"
  ]
  node [
    id 106
    label "Matka_Boska"
  ]
  node [
    id 107
    label "macocha"
  ]
  node [
    id 108
    label "matka_zast&#281;pcza"
  ]
  node [
    id 109
    label "stara"
  ]
  node [
    id 110
    label "rodzice"
  ]
  node [
    id 111
    label "rodzic"
  ]
  node [
    id 112
    label "s&#261;d"
  ]
  node [
    id 113
    label "plac"
  ]
  node [
    id 114
    label "miejsce"
  ]
  node [
    id 115
    label "grupa_dyskusyjna"
  ]
  node [
    id 116
    label "grupa"
  ]
  node [
    id 117
    label "przestrze&#324;"
  ]
  node [
    id 118
    label "agora"
  ]
  node [
    id 119
    label "bazylika"
  ]
  node [
    id 120
    label "konferencja"
  ]
  node [
    id 121
    label "portal"
  ]
  node [
    id 122
    label "gryzipi&#243;rek"
  ]
  node [
    id 123
    label "approach"
  ]
  node [
    id 124
    label "range"
  ]
  node [
    id 125
    label "dotrze&#263;"
  ]
  node [
    id 126
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 127
    label "przybiec"
  ]
  node [
    id 128
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 129
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 130
    label "supervene"
  ]
  node [
    id 131
    label "d&#378;wi&#281;k"
  ]
  node [
    id 132
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 133
    label "overwhelm"
  ]
  node [
    id 134
    label "defenestracja"
  ]
  node [
    id 135
    label "szereg"
  ]
  node [
    id 136
    label "dzia&#322;anie"
  ]
  node [
    id 137
    label "ostatnie_podrygi"
  ]
  node [
    id 138
    label "kres"
  ]
  node [
    id 139
    label "agonia"
  ]
  node [
    id 140
    label "visitation"
  ]
  node [
    id 141
    label "szeol"
  ]
  node [
    id 142
    label "mogi&#322;a"
  ]
  node [
    id 143
    label "chwila"
  ]
  node [
    id 144
    label "wydarzenie"
  ]
  node [
    id 145
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 146
    label "pogrzebanie"
  ]
  node [
    id 147
    label "punkt"
  ]
  node [
    id 148
    label "&#380;a&#322;oba"
  ]
  node [
    id 149
    label "zabicie"
  ]
  node [
    id 150
    label "kres_&#380;ycia"
  ]
  node [
    id 151
    label "ostygn&#261;&#263;"
  ]
  node [
    id 152
    label "afekt"
  ]
  node [
    id 153
    label "stan"
  ]
  node [
    id 154
    label "iskrzy&#263;"
  ]
  node [
    id 155
    label "wpa&#347;&#263;"
  ]
  node [
    id 156
    label "wpada&#263;"
  ]
  node [
    id 157
    label "d&#322;awi&#263;"
  ]
  node [
    id 158
    label "ogrom"
  ]
  node [
    id 159
    label "stygn&#261;&#263;"
  ]
  node [
    id 160
    label "temperatura"
  ]
  node [
    id 161
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 162
    label "redaction"
  ]
  node [
    id 163
    label "obr&#243;bka"
  ]
  node [
    id 164
    label "radio"
  ]
  node [
    id 165
    label "wydawnictwo"
  ]
  node [
    id 166
    label "siedziba"
  ]
  node [
    id 167
    label "composition"
  ]
  node [
    id 168
    label "zostawa&#263;"
  ]
  node [
    id 169
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 170
    label "return"
  ]
  node [
    id 171
    label "godzi&#263;_si&#281;"
  ]
  node [
    id 172
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 173
    label "przybywa&#263;"
  ]
  node [
    id 174
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 175
    label "przychodzi&#263;"
  ]
  node [
    id 176
    label "zaczyna&#263;"
  ]
  node [
    id 177
    label "tax_return"
  ]
  node [
    id 178
    label "nawi&#261;zywa&#263;"
  ]
  node [
    id 179
    label "recur"
  ]
  node [
    id 180
    label "powodowa&#263;"
  ]
  node [
    id 181
    label "lokomotywa"
  ]
  node [
    id 182
    label "pojazd_kolejowy"
  ]
  node [
    id 183
    label "kolej"
  ]
  node [
    id 184
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 185
    label "tender"
  ]
  node [
    id 186
    label "cug"
  ]
  node [
    id 187
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 188
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 189
    label "wagon"
  ]
  node [
    id 190
    label "samoch&#243;d"
  ]
  node [
    id 191
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 192
    label "Mickiewicz"
  ]
  node [
    id 193
    label "czas"
  ]
  node [
    id 194
    label "szkolenie"
  ]
  node [
    id 195
    label "przepisa&#263;"
  ]
  node [
    id 196
    label "lesson"
  ]
  node [
    id 197
    label "praktyka"
  ]
  node [
    id 198
    label "metoda"
  ]
  node [
    id 199
    label "niepokalanki"
  ]
  node [
    id 200
    label "kara"
  ]
  node [
    id 201
    label "zda&#263;"
  ]
  node [
    id 202
    label "form"
  ]
  node [
    id 203
    label "kwalifikacje"
  ]
  node [
    id 204
    label "system"
  ]
  node [
    id 205
    label "przepisanie"
  ]
  node [
    id 206
    label "sztuba"
  ]
  node [
    id 207
    label "wiedza"
  ]
  node [
    id 208
    label "stopek"
  ]
  node [
    id 209
    label "school"
  ]
  node [
    id 210
    label "absolwent"
  ]
  node [
    id 211
    label "urszulanki"
  ]
  node [
    id 212
    label "gabinet"
  ]
  node [
    id 213
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 214
    label "ideologia"
  ]
  node [
    id 215
    label "lekcja"
  ]
  node [
    id 216
    label "muzyka"
  ]
  node [
    id 217
    label "podr&#281;cznik"
  ]
  node [
    id 218
    label "zdanie"
  ]
  node [
    id 219
    label "sekretariat"
  ]
  node [
    id 220
    label "nauka"
  ]
  node [
    id 221
    label "do&#347;wiadczenie"
  ]
  node [
    id 222
    label "tablica"
  ]
  node [
    id 223
    label "teren_szko&#322;y"
  ]
  node [
    id 224
    label "instytucja"
  ]
  node [
    id 225
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 226
    label "skolaryzacja"
  ]
  node [
    id 227
    label "&#322;awa_szkolna"
  ]
  node [
    id 228
    label "klasa"
  ]
  node [
    id 229
    label "si&#281;ga&#263;"
  ]
  node [
    id 230
    label "trwa&#263;"
  ]
  node [
    id 231
    label "obecno&#347;&#263;"
  ]
  node [
    id 232
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 233
    label "stand"
  ]
  node [
    id 234
    label "mie&#263;_miejsce"
  ]
  node [
    id 235
    label "uczestniczy&#263;"
  ]
  node [
    id 236
    label "chodzi&#263;"
  ]
  node [
    id 237
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 238
    label "equal"
  ]
  node [
    id 239
    label "przedstawienie"
  ]
  node [
    id 240
    label "pokazywa&#263;"
  ]
  node [
    id 241
    label "zapoznawa&#263;"
  ]
  node [
    id 242
    label "typify"
  ]
  node [
    id 243
    label "opisywa&#263;"
  ]
  node [
    id 244
    label "teatr"
  ]
  node [
    id 245
    label "podawa&#263;"
  ]
  node [
    id 246
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 247
    label "demonstrowa&#263;"
  ]
  node [
    id 248
    label "represent"
  ]
  node [
    id 249
    label "attest"
  ]
  node [
    id 250
    label "exhibit"
  ]
  node [
    id 251
    label "stanowi&#263;"
  ]
  node [
    id 252
    label "zg&#322;asza&#263;"
  ]
  node [
    id 253
    label "display"
  ]
  node [
    id 254
    label "wypowied&#378;"
  ]
  node [
    id 255
    label "message"
  ]
  node [
    id 256
    label "podzbi&#243;r"
  ]
  node [
    id 257
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 258
    label "ustosunkowywa&#263;"
  ]
  node [
    id 259
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 260
    label "bratnia_dusza"
  ]
  node [
    id 261
    label "zwi&#261;zanie"
  ]
  node [
    id 262
    label "ustosunkowanie"
  ]
  node [
    id 263
    label "ustosunkowywanie"
  ]
  node [
    id 264
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 265
    label "zwi&#261;za&#263;"
  ]
  node [
    id 266
    label "ustosunkowa&#263;"
  ]
  node [
    id 267
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 268
    label "korespondent"
  ]
  node [
    id 269
    label "marriage"
  ]
  node [
    id 270
    label "wi&#261;zanie"
  ]
  node [
    id 271
    label "trasa"
  ]
  node [
    id 272
    label "zwi&#261;zek"
  ]
  node [
    id 273
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 274
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 275
    label "sprawko"
  ]
  node [
    id 276
    label "doros&#322;y"
  ]
  node [
    id 277
    label "wiele"
  ]
  node [
    id 278
    label "dorodny"
  ]
  node [
    id 279
    label "znaczny"
  ]
  node [
    id 280
    label "du&#380;o"
  ]
  node [
    id 281
    label "prawdziwy"
  ]
  node [
    id 282
    label "niema&#322;o"
  ]
  node [
    id 283
    label "wa&#380;ny"
  ]
  node [
    id 284
    label "rozwini&#281;ty"
  ]
  node [
    id 285
    label "og&#243;lnopolsko"
  ]
  node [
    id 286
    label "og&#243;lnokrajowy"
  ]
  node [
    id 287
    label "eliminacje"
  ]
  node [
    id 288
    label "Interwizja"
  ]
  node [
    id 289
    label "emulation"
  ]
  node [
    id 290
    label "impreza"
  ]
  node [
    id 291
    label "casting"
  ]
  node [
    id 292
    label "Eurowizja"
  ]
  node [
    id 293
    label "nab&#243;r"
  ]
  node [
    id 294
    label "czasopismo"
  ]
  node [
    id 295
    label "szkolnie"
  ]
  node [
    id 296
    label "szkoleniowy"
  ]
  node [
    id 297
    label "podstawowy"
  ]
  node [
    id 298
    label "prosty"
  ]
  node [
    id 299
    label "typ"
  ]
  node [
    id 300
    label "przyczyna"
  ]
  node [
    id 301
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 302
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 303
    label "zaokr&#261;glenie"
  ]
  node [
    id 304
    label "event"
  ]
  node [
    id 305
    label "pisa&#263;"
  ]
  node [
    id 306
    label "odmianka"
  ]
  node [
    id 307
    label "opu&#347;ci&#263;"
  ]
  node [
    id 308
    label "wytw&#243;r"
  ]
  node [
    id 309
    label "koniektura"
  ]
  node [
    id 310
    label "preparacja"
  ]
  node [
    id 311
    label "ekscerpcja"
  ]
  node [
    id 312
    label "j&#281;zykowo"
  ]
  node [
    id 313
    label "obelga"
  ]
  node [
    id 314
    label "dzie&#322;o"
  ]
  node [
    id 315
    label "pomini&#281;cie"
  ]
  node [
    id 316
    label "da&#263;"
  ]
  node [
    id 317
    label "przyzna&#263;"
  ]
  node [
    id 318
    label "recompense"
  ]
  node [
    id 319
    label "nadgrodzi&#263;"
  ]
  node [
    id 320
    label "pay"
  ]
  node [
    id 321
    label "forma"
  ]
  node [
    id 322
    label "type"
  ]
  node [
    id 323
    label "teoria"
  ]
  node [
    id 324
    label "zbi&#243;r"
  ]
  node [
    id 325
    label "poj&#281;cie"
  ]
  node [
    id 326
    label "swoisty"
  ]
  node [
    id 327
    label "indywidualnie"
  ]
  node [
    id 328
    label "osobny"
  ]
  node [
    id 329
    label "uprawi&#263;"
  ]
  node [
    id 330
    label "gotowy"
  ]
  node [
    id 331
    label "might"
  ]
  node [
    id 332
    label "pozna&#263;"
  ]
  node [
    id 333
    label "wywr&#243;&#380;y&#263;"
  ]
  node [
    id 334
    label "przetworzy&#263;"
  ]
  node [
    id 335
    label "read"
  ]
  node [
    id 336
    label "zaobserwowa&#263;"
  ]
  node [
    id 337
    label "odczyta&#263;"
  ]
  node [
    id 338
    label "znak_j&#281;zykowy"
  ]
  node [
    id 339
    label "sygna&#322;"
  ]
  node [
    id 340
    label "gest"
  ]
  node [
    id 341
    label "jawny"
  ]
  node [
    id 342
    label "upublicznienie"
  ]
  node [
    id 343
    label "upublicznianie"
  ]
  node [
    id 344
    label "publicznie"
  ]
  node [
    id 345
    label "szko&#322;a_ponadpodstawowa"
  ]
  node [
    id 346
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 347
    label "whole"
  ]
  node [
    id 348
    label "odm&#322;adza&#263;"
  ]
  node [
    id 349
    label "zabudowania"
  ]
  node [
    id 350
    label "odm&#322;odzenie"
  ]
  node [
    id 351
    label "zespolik"
  ]
  node [
    id 352
    label "skupienie"
  ]
  node [
    id 353
    label "schorzenie"
  ]
  node [
    id 354
    label "Depeche_Mode"
  ]
  node [
    id 355
    label "Mazowsze"
  ]
  node [
    id 356
    label "ro&#347;lina"
  ]
  node [
    id 357
    label "The_Beatles"
  ]
  node [
    id 358
    label "group"
  ]
  node [
    id 359
    label "&#346;wietliki"
  ]
  node [
    id 360
    label "odm&#322;adzanie"
  ]
  node [
    id 361
    label "batch"
  ]
  node [
    id 362
    label "przechodzi&#263;"
  ]
  node [
    id 363
    label "hold"
  ]
  node [
    id 364
    label "conclusion"
  ]
  node [
    id 365
    label "coating"
  ]
  node [
    id 366
    label "runda"
  ]
  node [
    id 367
    label "ostateczny"
  ]
  node [
    id 368
    label "finalnie"
  ]
  node [
    id 369
    label "ko&#324;cowy"
  ]
  node [
    id 370
    label "jab&#322;ko"
  ]
  node [
    id 371
    label "ceremonia"
  ]
  node [
    id 372
    label "jab&#322;o&#324;_domowa"
  ]
  node [
    id 373
    label "&#347;ledziowate"
  ]
  node [
    id 374
    label "ryba"
  ]
  node [
    id 375
    label "summer"
  ]
  node [
    id 376
    label "sprawno&#347;&#263;"
  ]
  node [
    id 377
    label "spotkanie"
  ]
  node [
    id 378
    label "wyposa&#380;enie"
  ]
  node [
    id 379
    label "st&#243;&#322;_roboczy"
  ]
  node [
    id 380
    label "pracownia"
  ]
  node [
    id 381
    label "zawodowy"
  ]
  node [
    id 382
    label "tre&#347;ciwy"
  ]
  node [
    id 383
    label "rzetelny"
  ]
  node [
    id 384
    label "po_dziennikarsku"
  ]
  node [
    id 385
    label "dziennikarsko"
  ]
  node [
    id 386
    label "obiektywny"
  ]
  node [
    id 387
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 388
    label "wzorowy"
  ]
  node [
    id 389
    label "typowy"
  ]
  node [
    id 390
    label "stulecie"
  ]
  node [
    id 391
    label "kalendarz"
  ]
  node [
    id 392
    label "pora_roku"
  ]
  node [
    id 393
    label "cykl_astronomiczny"
  ]
  node [
    id 394
    label "p&#243;&#322;rocze"
  ]
  node [
    id 395
    label "kwarta&#322;"
  ]
  node [
    id 396
    label "kurs"
  ]
  node [
    id 397
    label "jubileusz"
  ]
  node [
    id 398
    label "miesi&#261;c"
  ]
  node [
    id 399
    label "martwy_sezon"
  ]
  node [
    id 400
    label "dawny"
  ]
  node [
    id 401
    label "rozw&#243;d"
  ]
  node [
    id 402
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 403
    label "eksprezydent"
  ]
  node [
    id 404
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 405
    label "partner"
  ]
  node [
    id 406
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 407
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 408
    label "wcze&#347;niejszy"
  ]
  node [
    id 409
    label "uderzenie"
  ]
  node [
    id 410
    label "cios"
  ]
  node [
    id 411
    label "time"
  ]
  node [
    id 412
    label "najwa&#380;niejszy"
  ]
  node [
    id 413
    label "pocz&#261;tkowy"
  ]
  node [
    id 414
    label "dobry"
  ]
  node [
    id 415
    label "ch&#281;tny"
  ]
  node [
    id 416
    label "dzie&#324;"
  ]
  node [
    id 417
    label "pr&#281;dki"
  ]
  node [
    id 418
    label "po_belfersku"
  ]
  node [
    id 419
    label "sztab"
  ]
  node [
    id 420
    label "przyw&#243;dztwo"
  ]
  node [
    id 421
    label "zaci&#281;cie"
  ]
  node [
    id 422
    label "command"
  ]
  node [
    id 423
    label "zacinanie"
  ]
  node [
    id 424
    label "organ"
  ]
  node [
    id 425
    label "dow&#243;dca"
  ]
  node [
    id 426
    label "zaci&#261;&#263;"
  ]
  node [
    id 427
    label "uzda"
  ]
  node [
    id 428
    label "zacina&#263;"
  ]
  node [
    id 429
    label "potomstwo"
  ]
  node [
    id 430
    label "organizm"
  ]
  node [
    id 431
    label "m&#322;odziak"
  ]
  node [
    id 432
    label "zwierz&#281;"
  ]
  node [
    id 433
    label "fledgling"
  ]
  node [
    id 434
    label "cz&#322;owiek"
  ]
  node [
    id 435
    label "bran&#380;owiec"
  ]
  node [
    id 436
    label "edytor"
  ]
  node [
    id 437
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 438
    label "paleograf"
  ]
  node [
    id 439
    label "list"
  ]
  node [
    id 440
    label "egzemplarz"
  ]
  node [
    id 441
    label "komunikacja"
  ]
  node [
    id 442
    label "psychotest"
  ]
  node [
    id 443
    label "ortografia"
  ]
  node [
    id 444
    label "handwriting"
  ]
  node [
    id 445
    label "grafia"
  ]
  node [
    id 446
    label "prasa"
  ]
  node [
    id 447
    label "j&#281;zyk"
  ]
  node [
    id 448
    label "adres"
  ]
  node [
    id 449
    label "script"
  ]
  node [
    id 450
    label "dzia&#322;"
  ]
  node [
    id 451
    label "paleografia"
  ]
  node [
    id 452
    label "Zwrotnica"
  ]
  node [
    id 453
    label "wk&#322;ad"
  ]
  node [
    id 454
    label "cecha"
  ]
  node [
    id 455
    label "przekaz"
  ]
  node [
    id 456
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 457
    label "interpunkcja"
  ]
  node [
    id 458
    label "communication"
  ]
  node [
    id 459
    label "dokument"
  ]
  node [
    id 460
    label "ok&#322;adka"
  ]
  node [
    id 461
    label "letter"
  ]
  node [
    id 462
    label "zajawka"
  ]
  node [
    id 463
    label "m&#322;odzie&#380;owo"
  ]
  node [
    id 464
    label "poprowadzi&#263;"
  ]
  node [
    id 465
    label "spowodowa&#263;"
  ]
  node [
    id 466
    label "pos&#322;a&#263;"
  ]
  node [
    id 467
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 468
    label "wykona&#263;"
  ]
  node [
    id 469
    label "wzbudzi&#263;"
  ]
  node [
    id 470
    label "wprowadzi&#263;"
  ]
  node [
    id 471
    label "set"
  ]
  node [
    id 472
    label "take"
  ]
  node [
    id 473
    label "carry"
  ]
  node [
    id 474
    label "uniesienie_si&#281;"
  ]
  node [
    id 475
    label "geneza"
  ]
  node [
    id 476
    label "chmielnicczyzna"
  ]
  node [
    id 477
    label "beginning"
  ]
  node [
    id 478
    label "orgy"
  ]
  node [
    id 479
    label "Ko&#347;ciuszko"
  ]
  node [
    id 480
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 481
    label "utworzenie"
  ]
  node [
    id 482
    label "powstanie_listopadowe"
  ]
  node [
    id 483
    label "potworzenie_si&#281;"
  ]
  node [
    id 484
    label "powstanie_warszawskie"
  ]
  node [
    id 485
    label "kl&#281;czenie"
  ]
  node [
    id 486
    label "zbuntowanie_si&#281;"
  ]
  node [
    id 487
    label "siedzenie"
  ]
  node [
    id 488
    label "stworzenie"
  ]
  node [
    id 489
    label "walka"
  ]
  node [
    id 490
    label "zaistnienie"
  ]
  node [
    id 491
    label "odbudowanie_si&#281;"
  ]
  node [
    id 492
    label "pierwocina"
  ]
  node [
    id 493
    label "origin"
  ]
  node [
    id 494
    label "koliszczyzna"
  ]
  node [
    id 495
    label "powstanie_tambowskie"
  ]
  node [
    id 496
    label "&#380;akieria"
  ]
  node [
    id 497
    label "le&#380;enie"
  ]
  node [
    id 498
    label "minuta"
  ]
  node [
    id 499
    label "doba"
  ]
  node [
    id 500
    label "p&#243;&#322;godzina"
  ]
  node [
    id 501
    label "kwadrans"
  ]
  node [
    id 502
    label "jednostka_czasu"
  ]
  node [
    id 503
    label "profesor"
  ]
  node [
    id 504
    label "kszta&#322;ciciel"
  ]
  node [
    id 505
    label "szkolnik"
  ]
  node [
    id 506
    label "preceptor"
  ]
  node [
    id 507
    label "pedagog"
  ]
  node [
    id 508
    label "popularyzator"
  ]
  node [
    id 509
    label "belfer"
  ]
  node [
    id 510
    label "funkcjonariusz"
  ]
  node [
    id 511
    label "nadzorca"
  ]
  node [
    id 512
    label "upubliczni&#263;"
  ]
  node [
    id 513
    label "picture"
  ]
  node [
    id 514
    label "dystyngowany"
  ]
  node [
    id 515
    label "ksi&#261;&#380;&#281;co"
  ]
  node [
    id 516
    label "kneziowski"
  ]
  node [
    id 517
    label "invite"
  ]
  node [
    id 518
    label "ask"
  ]
  node [
    id 519
    label "oferowa&#263;"
  ]
  node [
    id 520
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 521
    label "wyczytywanie"
  ]
  node [
    id 522
    label "doczytywanie"
  ]
  node [
    id 523
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 524
    label "oczytywanie_si&#281;"
  ]
  node [
    id 525
    label "zaczytanie_si&#281;"
  ]
  node [
    id 526
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 527
    label "czytywanie"
  ]
  node [
    id 528
    label "recitation"
  ]
  node [
    id 529
    label "poznawanie"
  ]
  node [
    id 530
    label "poczytanie"
  ]
  node [
    id 531
    label "wczytywanie_si&#281;"
  ]
  node [
    id 532
    label "tytu&#322;"
  ]
  node [
    id 533
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 534
    label "examine"
  ]
  node [
    id 535
    label "szuka&#263;"
  ]
  node [
    id 536
    label "&#322;owiectwo"
  ]
  node [
    id 537
    label "chase"
  ]
  node [
    id 538
    label "robi&#263;"
  ]
  node [
    id 539
    label "szperacz"
  ]
  node [
    id 540
    label "skr&#281;canie"
  ]
  node [
    id 541
    label "voice"
  ]
  node [
    id 542
    label "internet"
  ]
  node [
    id 543
    label "skr&#281;ci&#263;"
  ]
  node [
    id 544
    label "kartka"
  ]
  node [
    id 545
    label "orientowa&#263;"
  ]
  node [
    id 546
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 547
    label "powierzchnia"
  ]
  node [
    id 548
    label "plik"
  ]
  node [
    id 549
    label "bok"
  ]
  node [
    id 550
    label "pagina"
  ]
  node [
    id 551
    label "orientowanie"
  ]
  node [
    id 552
    label "fragment"
  ]
  node [
    id 553
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 554
    label "skr&#281;ca&#263;"
  ]
  node [
    id 555
    label "g&#243;ra"
  ]
  node [
    id 556
    label "serwis_internetowy"
  ]
  node [
    id 557
    label "orientacja"
  ]
  node [
    id 558
    label "linia"
  ]
  node [
    id 559
    label "skr&#281;cenie"
  ]
  node [
    id 560
    label "layout"
  ]
  node [
    id 561
    label "zorientowa&#263;"
  ]
  node [
    id 562
    label "zorientowanie"
  ]
  node [
    id 563
    label "obiekt"
  ]
  node [
    id 564
    label "podmiot"
  ]
  node [
    id 565
    label "ty&#322;"
  ]
  node [
    id 566
    label "logowanie"
  ]
  node [
    id 567
    label "adres_internetowy"
  ]
  node [
    id 568
    label "uj&#281;cie"
  ]
  node [
    id 569
    label "prz&#243;d"
  ]
  node [
    id 570
    label "posta&#263;"
  ]
  node [
    id 571
    label "unwrap"
  ]
  node [
    id 572
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 573
    label "inny"
  ]
  node [
    id 574
    label "nast&#281;pnie"
  ]
  node [
    id 575
    label "kt&#243;ry&#347;"
  ]
  node [
    id 576
    label "kolejno"
  ]
  node [
    id 577
    label "nastopny"
  ]
  node [
    id 578
    label "control"
  ]
  node [
    id 579
    label "placard"
  ]
  node [
    id 580
    label "m&#243;wi&#263;"
  ]
  node [
    id 581
    label "charge"
  ]
  node [
    id 582
    label "zadawa&#263;"
  ]
  node [
    id 583
    label "ordynowa&#263;"
  ]
  node [
    id 584
    label "powierza&#263;"
  ]
  node [
    id 585
    label "wydawa&#263;"
  ]
  node [
    id 586
    label "doradza&#263;"
  ]
  node [
    id 587
    label "notice"
  ]
  node [
    id 588
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 589
    label "styka&#263;_si&#281;"
  ]
  node [
    id 590
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 591
    label "Polsat"
  ]
  node [
    id 592
    label "paj&#281;czarz"
  ]
  node [
    id 593
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 594
    label "programowiec"
  ]
  node [
    id 595
    label "technologia"
  ]
  node [
    id 596
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 597
    label "BBC"
  ]
  node [
    id 598
    label "ekran"
  ]
  node [
    id 599
    label "media"
  ]
  node [
    id 600
    label "odbieranie"
  ]
  node [
    id 601
    label "odbiera&#263;"
  ]
  node [
    id 602
    label "odbiornik"
  ]
  node [
    id 603
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 604
    label "studio"
  ]
  node [
    id 605
    label "telekomunikacja"
  ]
  node [
    id 606
    label "muza"
  ]
  node [
    id 607
    label "relate"
  ]
  node [
    id 608
    label "specjalnie"
  ]
  node [
    id 609
    label "nieetatowy"
  ]
  node [
    id 610
    label "intencjonalny"
  ]
  node [
    id 611
    label "szczeg&#243;lny"
  ]
  node [
    id 612
    label "odpowiedni"
  ]
  node [
    id 613
    label "niedorozw&#243;j"
  ]
  node [
    id 614
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 615
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 616
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 617
    label "nienormalny"
  ]
  node [
    id 618
    label "umy&#347;lnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 200
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 203
  ]
  edge [
    source 12
    target 204
  ]
  edge [
    source 12
    target 205
  ]
  edge [
    source 12
    target 206
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 208
  ]
  edge [
    source 12
    target 209
  ]
  edge [
    source 12
    target 210
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 212
  ]
  edge [
    source 12
    target 213
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 215
  ]
  edge [
    source 12
    target 216
  ]
  edge [
    source 12
    target 217
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 221
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 224
  ]
  edge [
    source 12
    target 225
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 48
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 41
  ]
  edge [
    source 15
    target 52
  ]
  edge [
    source 15
    target 53
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 17
    target 91
  ]
  edge [
    source 17
    target 229
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 232
  ]
  edge [
    source 17
    target 233
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 237
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 41
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 19
    target 40
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 239
  ]
  edge [
    source 20
    target 240
  ]
  edge [
    source 20
    target 241
  ]
  edge [
    source 20
    target 242
  ]
  edge [
    source 20
    target 243
  ]
  edge [
    source 20
    target 244
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 20
    target 91
  ]
  edge [
    source 20
    target 249
  ]
  edge [
    source 20
    target 250
  ]
  edge [
    source 20
    target 251
  ]
  edge [
    source 20
    target 252
  ]
  edge [
    source 20
    target 253
  ]
  edge [
    source 20
    target 98
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 75
  ]
  edge [
    source 21
    target 98
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 92
  ]
  edge [
    source 22
    target 93
  ]
  edge [
    source 22
    target 254
  ]
  edge [
    source 22
    target 255
  ]
  edge [
    source 22
    target 256
  ]
  edge [
    source 22
    target 257
  ]
  edge [
    source 22
    target 258
  ]
  edge [
    source 22
    target 259
  ]
  edge [
    source 22
    target 260
  ]
  edge [
    source 22
    target 261
  ]
  edge [
    source 22
    target 262
  ]
  edge [
    source 22
    target 263
  ]
  edge [
    source 22
    target 264
  ]
  edge [
    source 22
    target 265
  ]
  edge [
    source 22
    target 266
  ]
  edge [
    source 22
    target 267
  ]
  edge [
    source 22
    target 268
  ]
  edge [
    source 22
    target 269
  ]
  edge [
    source 22
    target 270
  ]
  edge [
    source 22
    target 271
  ]
  edge [
    source 22
    target 272
  ]
  edge [
    source 22
    target 273
  ]
  edge [
    source 22
    target 274
  ]
  edge [
    source 22
    target 275
  ]
  edge [
    source 22
    target 44
  ]
  edge [
    source 22
    target 52
  ]
  edge [
    source 22
    target 53
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 23
    target 281
  ]
  edge [
    source 23
    target 282
  ]
  edge [
    source 23
    target 283
  ]
  edge [
    source 23
    target 284
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 285
  ]
  edge [
    source 24
    target 286
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 287
  ]
  edge [
    source 25
    target 288
  ]
  edge [
    source 25
    target 289
  ]
  edge [
    source 25
    target 290
  ]
  edge [
    source 25
    target 291
  ]
  edge [
    source 25
    target 292
  ]
  edge [
    source 25
    target 293
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 59
  ]
  edge [
    source 26
    target 60
  ]
  edge [
    source 26
    target 294
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 26
    target 78
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 96
  ]
  edge [
    source 27
    target 97
  ]
  edge [
    source 27
    target 295
  ]
  edge [
    source 27
    target 296
  ]
  edge [
    source 27
    target 297
  ]
  edge [
    source 27
    target 298
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 27
    target 95
  ]
  edge [
    source 28
    target 299
  ]
  edge [
    source 28
    target 136
  ]
  edge [
    source 28
    target 300
  ]
  edge [
    source 28
    target 301
  ]
  edge [
    source 28
    target 302
  ]
  edge [
    source 28
    target 303
  ]
  edge [
    source 28
    target 304
  ]
  edge [
    source 28
    target 53
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 68
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 87
  ]
  edge [
    source 30
    target 88
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 308
  ]
  edge [
    source 30
    target 309
  ]
  edge [
    source 30
    target 310
  ]
  edge [
    source 30
    target 311
  ]
  edge [
    source 30
    target 312
  ]
  edge [
    source 30
    target 313
  ]
  edge [
    source 30
    target 314
  ]
  edge [
    source 30
    target 315
  ]
  edge [
    source 30
    target 86
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 86
  ]
  edge [
    source 31
    target 87
  ]
  edge [
    source 31
    target 316
  ]
  edge [
    source 31
    target 317
  ]
  edge [
    source 31
    target 318
  ]
  edge [
    source 31
    target 319
  ]
  edge [
    source 31
    target 320
  ]
  edge [
    source 31
    target 78
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 32
    target 308
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 32
    target 323
  ]
  edge [
    source 32
    target 324
  ]
  edge [
    source 32
    target 325
  ]
  edge [
    source 32
    target 228
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 33
    target 328
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 329
  ]
  edge [
    source 34
    target 330
  ]
  edge [
    source 34
    target 331
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 332
  ]
  edge [
    source 35
    target 333
  ]
  edge [
    source 35
    target 334
  ]
  edge [
    source 35
    target 335
  ]
  edge [
    source 35
    target 336
  ]
  edge [
    source 35
    target 337
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 89
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 338
  ]
  edge [
    source 37
    target 339
  ]
  edge [
    source 37
    target 340
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 341
  ]
  edge [
    source 38
    target 342
  ]
  edge [
    source 38
    target 343
  ]
  edge [
    source 38
    target 344
  ]
  edge [
    source 39
    target 114
  ]
  edge [
    source 39
    target 345
  ]
  edge [
    source 40
    target 346
  ]
  edge [
    source 40
    target 347
  ]
  edge [
    source 40
    target 348
  ]
  edge [
    source 40
    target 349
  ]
  edge [
    source 40
    target 350
  ]
  edge [
    source 40
    target 351
  ]
  edge [
    source 40
    target 352
  ]
  edge [
    source 40
    target 353
  ]
  edge [
    source 40
    target 116
  ]
  edge [
    source 40
    target 354
  ]
  edge [
    source 40
    target 355
  ]
  edge [
    source 40
    target 356
  ]
  edge [
    source 40
    target 324
  ]
  edge [
    source 40
    target 357
  ]
  edge [
    source 40
    target 358
  ]
  edge [
    source 40
    target 359
  ]
  edge [
    source 40
    target 360
  ]
  edge [
    source 40
    target 361
  ]
  edge [
    source 40
    target 94
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 49
  ]
  edge [
    source 41
    target 235
  ]
  edge [
    source 41
    target 362
  ]
  edge [
    source 41
    target 363
  ]
  edge [
    source 41
    target 67
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 50
  ]
  edge [
    source 42
    target 91
  ]
  edge [
    source 42
    target 92
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 81
  ]
  edge [
    source 43
    target 364
  ]
  edge [
    source 43
    target 365
  ]
  edge [
    source 43
    target 366
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 367
  ]
  edge [
    source 45
    target 368
  ]
  edge [
    source 45
    target 369
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 370
  ]
  edge [
    source 46
    target 371
  ]
  edge [
    source 46
    target 372
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 373
  ]
  edge [
    source 48
    target 374
  ]
  edge [
    source 49
    target 375
  ]
  edge [
    source 49
    target 193
  ]
  edge [
    source 49
    target 55
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 114
  ]
  edge [
    source 51
    target 376
  ]
  edge [
    source 51
    target 377
  ]
  edge [
    source 51
    target 378
  ]
  edge [
    source 51
    target 379
  ]
  edge [
    source 51
    target 380
  ]
  edge [
    source 51
    target 63
  ]
  edge [
    source 52
    target 381
  ]
  edge [
    source 52
    target 382
  ]
  edge [
    source 52
    target 383
  ]
  edge [
    source 52
    target 384
  ]
  edge [
    source 52
    target 385
  ]
  edge [
    source 52
    target 386
  ]
  edge [
    source 52
    target 387
  ]
  edge [
    source 52
    target 388
  ]
  edge [
    source 52
    target 389
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 136
  ]
  edge [
    source 53
    target 299
  ]
  edge [
    source 53
    target 300
  ]
  edge [
    source 53
    target 304
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 390
  ]
  edge [
    source 55
    target 391
  ]
  edge [
    source 55
    target 193
  ]
  edge [
    source 55
    target 392
  ]
  edge [
    source 55
    target 393
  ]
  edge [
    source 55
    target 394
  ]
  edge [
    source 55
    target 116
  ]
  edge [
    source 55
    target 395
  ]
  edge [
    source 55
    target 396
  ]
  edge [
    source 55
    target 397
  ]
  edge [
    source 55
    target 398
  ]
  edge [
    source 55
    target 399
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 400
  ]
  edge [
    source 56
    target 401
  ]
  edge [
    source 56
    target 402
  ]
  edge [
    source 56
    target 403
  ]
  edge [
    source 56
    target 404
  ]
  edge [
    source 56
    target 405
  ]
  edge [
    source 56
    target 406
  ]
  edge [
    source 56
    target 407
  ]
  edge [
    source 56
    target 408
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 143
  ]
  edge [
    source 58
    target 409
  ]
  edge [
    source 58
    target 410
  ]
  edge [
    source 58
    target 411
  ]
  edge [
    source 59
    target 412
  ]
  edge [
    source 59
    target 413
  ]
  edge [
    source 59
    target 414
  ]
  edge [
    source 59
    target 415
  ]
  edge [
    source 59
    target 416
  ]
  edge [
    source 59
    target 417
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 387
  ]
  edge [
    source 60
    target 389
  ]
  edge [
    source 60
    target 418
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 419
  ]
  edge [
    source 63
    target 420
  ]
  edge [
    source 63
    target 421
  ]
  edge [
    source 63
    target 422
  ]
  edge [
    source 63
    target 423
  ]
  edge [
    source 63
    target 424
  ]
  edge [
    source 63
    target 425
  ]
  edge [
    source 63
    target 426
  ]
  edge [
    source 63
    target 427
  ]
  edge [
    source 63
    target 428
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 429
  ]
  edge [
    source 68
    target 430
  ]
  edge [
    source 68
    target 431
  ]
  edge [
    source 68
    target 432
  ]
  edge [
    source 68
    target 433
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 434
  ]
  edge [
    source 69
    target 435
  ]
  edge [
    source 69
    target 165
  ]
  edge [
    source 69
    target 436
  ]
  edge [
    source 69
    target 437
  ]
  edge [
    source 69
    target 73
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 438
  ]
  edge [
    source 70
    target 439
  ]
  edge [
    source 70
    target 440
  ]
  edge [
    source 70
    target 441
  ]
  edge [
    source 70
    target 442
  ]
  edge [
    source 70
    target 443
  ]
  edge [
    source 70
    target 444
  ]
  edge [
    source 70
    target 445
  ]
  edge [
    source 70
    target 446
  ]
  edge [
    source 70
    target 447
  ]
  edge [
    source 70
    target 448
  ]
  edge [
    source 70
    target 449
  ]
  edge [
    source 70
    target 450
  ]
  edge [
    source 70
    target 451
  ]
  edge [
    source 70
    target 452
  ]
  edge [
    source 70
    target 453
  ]
  edge [
    source 70
    target 454
  ]
  edge [
    source 70
    target 455
  ]
  edge [
    source 70
    target 456
  ]
  edge [
    source 70
    target 457
  ]
  edge [
    source 70
    target 458
  ]
  edge [
    source 70
    target 459
  ]
  edge [
    source 70
    target 460
  ]
  edge [
    source 70
    target 314
  ]
  edge [
    source 70
    target 294
  ]
  edge [
    source 70
    target 461
  ]
  edge [
    source 70
    target 462
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 463
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 464
  ]
  edge [
    source 73
    target 465
  ]
  edge [
    source 73
    target 466
  ]
  edge [
    source 73
    target 467
  ]
  edge [
    source 73
    target 468
  ]
  edge [
    source 73
    target 469
  ]
  edge [
    source 73
    target 470
  ]
  edge [
    source 73
    target 471
  ]
  edge [
    source 73
    target 472
  ]
  edge [
    source 73
    target 473
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 474
  ]
  edge [
    source 74
    target 475
  ]
  edge [
    source 74
    target 476
  ]
  edge [
    source 74
    target 477
  ]
  edge [
    source 74
    target 478
  ]
  edge [
    source 74
    target 479
  ]
  edge [
    source 74
    target 480
  ]
  edge [
    source 74
    target 481
  ]
  edge [
    source 74
    target 482
  ]
  edge [
    source 74
    target 483
  ]
  edge [
    source 74
    target 484
  ]
  edge [
    source 74
    target 485
  ]
  edge [
    source 74
    target 486
  ]
  edge [
    source 74
    target 487
  ]
  edge [
    source 74
    target 488
  ]
  edge [
    source 74
    target 489
  ]
  edge [
    source 74
    target 490
  ]
  edge [
    source 74
    target 491
  ]
  edge [
    source 74
    target 492
  ]
  edge [
    source 74
    target 493
  ]
  edge [
    source 74
    target 494
  ]
  edge [
    source 74
    target 495
  ]
  edge [
    source 74
    target 496
  ]
  edge [
    source 74
    target 497
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 498
  ]
  edge [
    source 75
    target 499
  ]
  edge [
    source 75
    target 193
  ]
  edge [
    source 75
    target 500
  ]
  edge [
    source 75
    target 501
  ]
  edge [
    source 75
    target 411
  ]
  edge [
    source 75
    target 502
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 503
  ]
  edge [
    source 79
    target 504
  ]
  edge [
    source 79
    target 505
  ]
  edge [
    source 79
    target 506
  ]
  edge [
    source 79
    target 437
  ]
  edge [
    source 79
    target 507
  ]
  edge [
    source 79
    target 508
  ]
  edge [
    source 79
    target 509
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 510
  ]
  edge [
    source 80
    target 434
  ]
  edge [
    source 80
    target 511
  ]
  edge [
    source 81
    target 93
  ]
  edge [
    source 82
    target 512
  ]
  edge [
    source 82
    target 165
  ]
  edge [
    source 82
    target 470
  ]
  edge [
    source 82
    target 513
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 514
  ]
  edge [
    source 84
    target 515
  ]
  edge [
    source 84
    target 516
  ]
  edge [
    source 85
    target 517
  ]
  edge [
    source 85
    target 518
  ]
  edge [
    source 85
    target 519
  ]
  edge [
    source 85
    target 520
  ]
  edge [
    source 86
    target 521
  ]
  edge [
    source 86
    target 522
  ]
  edge [
    source 86
    target 523
  ]
  edge [
    source 86
    target 524
  ]
  edge [
    source 86
    target 525
  ]
  edge [
    source 86
    target 526
  ]
  edge [
    source 86
    target 527
  ]
  edge [
    source 86
    target 528
  ]
  edge [
    source 86
    target 529
  ]
  edge [
    source 86
    target 530
  ]
  edge [
    source 86
    target 531
  ]
  edge [
    source 87
    target 446
  ]
  edge [
    source 87
    target 532
  ]
  edge [
    source 87
    target 533
  ]
  edge [
    source 87
    target 294
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 534
  ]
  edge [
    source 88
    target 535
  ]
  edge [
    source 88
    target 536
  ]
  edge [
    source 88
    target 537
  ]
  edge [
    source 88
    target 538
  ]
  edge [
    source 88
    target 539
  ]
  edge [
    source 89
    target 99
  ]
  edge [
    source 89
    target 100
  ]
  edge [
    source 89
    target 540
  ]
  edge [
    source 89
    target 541
  ]
  edge [
    source 89
    target 321
  ]
  edge [
    source 89
    target 542
  ]
  edge [
    source 89
    target 543
  ]
  edge [
    source 89
    target 544
  ]
  edge [
    source 89
    target 545
  ]
  edge [
    source 89
    target 546
  ]
  edge [
    source 89
    target 547
  ]
  edge [
    source 89
    target 548
  ]
  edge [
    source 89
    target 549
  ]
  edge [
    source 89
    target 550
  ]
  edge [
    source 89
    target 551
  ]
  edge [
    source 89
    target 552
  ]
  edge [
    source 89
    target 553
  ]
  edge [
    source 89
    target 112
  ]
  edge [
    source 89
    target 554
  ]
  edge [
    source 89
    target 555
  ]
  edge [
    source 89
    target 556
  ]
  edge [
    source 89
    target 557
  ]
  edge [
    source 89
    target 558
  ]
  edge [
    source 89
    target 559
  ]
  edge [
    source 89
    target 560
  ]
  edge [
    source 89
    target 561
  ]
  edge [
    source 89
    target 562
  ]
  edge [
    source 89
    target 563
  ]
  edge [
    source 89
    target 564
  ]
  edge [
    source 89
    target 565
  ]
  edge [
    source 89
    target 145
  ]
  edge [
    source 89
    target 566
  ]
  edge [
    source 89
    target 567
  ]
  edge [
    source 89
    target 568
  ]
  edge [
    source 89
    target 569
  ]
  edge [
    source 89
    target 570
  ]
  edge [
    source 91
    target 571
  ]
  edge [
    source 91
    target 240
  ]
  edge [
    source 91
    target 572
  ]
  edge [
    source 92
    target 573
  ]
  edge [
    source 92
    target 574
  ]
  edge [
    source 92
    target 575
  ]
  edge [
    source 92
    target 576
  ]
  edge [
    source 92
    target 577
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 578
  ]
  edge [
    source 93
    target 579
  ]
  edge [
    source 93
    target 580
  ]
  edge [
    source 93
    target 581
  ]
  edge [
    source 93
    target 582
  ]
  edge [
    source 93
    target 583
  ]
  edge [
    source 93
    target 584
  ]
  edge [
    source 93
    target 585
  ]
  edge [
    source 93
    target 586
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 587
  ]
  edge [
    source 95
    target 588
  ]
  edge [
    source 95
    target 589
  ]
  edge [
    source 95
    target 590
  ]
  edge [
    source 96
    target 591
  ]
  edge [
    source 96
    target 592
  ]
  edge [
    source 96
    target 593
  ]
  edge [
    source 96
    target 594
  ]
  edge [
    source 96
    target 595
  ]
  edge [
    source 96
    target 596
  ]
  edge [
    source 96
    target 288
  ]
  edge [
    source 96
    target 597
  ]
  edge [
    source 96
    target 598
  ]
  edge [
    source 96
    target 599
  ]
  edge [
    source 96
    target 600
  ]
  edge [
    source 96
    target 601
  ]
  edge [
    source 96
    target 602
  ]
  edge [
    source 96
    target 224
  ]
  edge [
    source 96
    target 603
  ]
  edge [
    source 96
    target 604
  ]
  edge [
    source 96
    target 605
  ]
  edge [
    source 96
    target 606
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 607
  ]
  edge [
    source 99
    target 608
  ]
  edge [
    source 99
    target 609
  ]
  edge [
    source 99
    target 610
  ]
  edge [
    source 99
    target 611
  ]
  edge [
    source 99
    target 612
  ]
  edge [
    source 99
    target 613
  ]
  edge [
    source 99
    target 614
  ]
  edge [
    source 99
    target 615
  ]
  edge [
    source 99
    target 616
  ]
  edge [
    source 99
    target 617
  ]
  edge [
    source 99
    target 618
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 102
    target 193
  ]
]
