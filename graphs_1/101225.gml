graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.7037037037037037
  density 0.06552706552706553
  graphCliqueNumber 2
  node [
    id 0
    label "sto&#380;ek"
    origin "text"
  ]
  node [
    id 1
    label "ma&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "beskid"
    origin "text"
  ]
  node [
    id 3
    label "&#347;l&#261;sk"
    origin "text"
  ]
  node [
    id 4
    label "cone"
  ]
  node [
    id 5
    label "sto&#380;ek_&#347;ci&#281;ty"
  ]
  node [
    id 6
    label "sterta"
  ]
  node [
    id 7
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 8
    label "marny"
  ]
  node [
    id 9
    label "nieznaczny"
  ]
  node [
    id 10
    label "s&#322;aby"
  ]
  node [
    id 11
    label "ch&#322;opiec"
  ]
  node [
    id 12
    label "ma&#322;o"
  ]
  node [
    id 13
    label "n&#281;dznie"
  ]
  node [
    id 14
    label "niewa&#380;ny"
  ]
  node [
    id 15
    label "przeci&#281;tny"
  ]
  node [
    id 16
    label "nieliczny"
  ]
  node [
    id 17
    label "wstydliwy"
  ]
  node [
    id 18
    label "szybki"
  ]
  node [
    id 19
    label "m&#322;ody"
  ]
  node [
    id 20
    label "Beskid"
  ]
  node [
    id 21
    label "&#347;l&#261;ski"
  ]
  node [
    id 22
    label "wielki"
  ]
  node [
    id 23
    label "pasmo"
  ]
  node [
    id 24
    label "Czantorii"
  ]
  node [
    id 25
    label "Mal&#253;"
  ]
  node [
    id 26
    label "Sto&#382;ek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
]
