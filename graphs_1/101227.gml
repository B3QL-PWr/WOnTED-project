graph [
  maxDegree 6
  minDegree 1
  meanDegree 2.787878787878788
  density 0.08712121212121213
  graphCliqueNumber 6
  node [
    id 0
    label "aleksander"
    origin "text"
  ]
  node [
    id 1
    label "mackiewicz"
    origin "text"
  ]
  node [
    id 2
    label "Aleksandra"
  ]
  node [
    id 3
    label "Mackiewicz"
  ]
  node [
    id 4
    label "stronnictwo"
  ]
  node [
    id 5
    label "demokratyczny"
  ]
  node [
    id 6
    label "wydzia&#322;"
  ]
  node [
    id 7
    label "ekonomia"
  ]
  node [
    id 8
    label "uniwersytet"
  ]
  node [
    id 9
    label "warszawski"
  ]
  node [
    id 10
    label "dom"
  ]
  node [
    id 11
    label "s&#322;owo"
  ]
  node [
    id 12
    label "polskie"
  ]
  node [
    id 13
    label "centralny"
  ]
  node [
    id 14
    label "o&#347;rodek"
  ]
  node [
    id 15
    label "planowa&#263;"
  ]
  node [
    id 16
    label "organizacja"
  ]
  node [
    id 17
    label "i"
  ]
  node [
    id 18
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 19
    label "polski"
  ]
  node [
    id 20
    label "poczta"
  ]
  node [
    id 21
    label "&#8211;"
  ]
  node [
    id 22
    label "telefon"
  ]
  node [
    id 23
    label "telegraf"
  ]
  node [
    id 24
    label "sp&#243;&#322;dzielczy"
  ]
  node [
    id 25
    label "zjednoczy&#263;"
  ]
  node [
    id 26
    label "przemys&#322;owy"
  ]
  node [
    id 27
    label "Ars"
  ]
  node [
    id 28
    label "Christian"
  ]
  node [
    id 29
    label "Tadeusz"
  ]
  node [
    id 30
    label "mazowiecki"
  ]
  node [
    id 31
    label "trybuna&#322;"
  ]
  node [
    id 32
    label "stan"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 21
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
]
