graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.0434782608695654
  density 0.011166547873604181
  graphCliqueNumber 3
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "lipcowy"
    origin "text"
  ]
  node [
    id 2
    label "spotkanie"
    origin "text"
  ]
  node [
    id 3
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pod"
    origin "text"
  ]
  node [
    id 5
    label "znak"
    origin "text"
  ]
  node [
    id 6
    label "bezlito&#347;nie"
    origin "text"
  ]
  node [
    id 7
    label "pra&#380;y&#263;"
    origin "text"
  ]
  node [
    id 8
    label "s&#322;o&#324;ce"
    origin "text"
  ]
  node [
    id 9
    label "niezb&#281;dny"
    origin "text"
  ]
  node [
    id 10
    label "by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "ci&#261;g&#322;y"
    origin "text"
  ]
  node [
    id 12
    label "polewa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "tor"
    origin "text"
  ]
  node [
    id 14
    label "okazyjny"
    origin "text"
  ]
  node [
    id 15
    label "kierowca"
    origin "text"
  ]
  node [
    id 16
    label "pomimo"
    origin "text"
  ]
  node [
    id 17
    label "tak"
    origin "text"
  ]
  node [
    id 18
    label "ekstremalny"
    origin "text"
  ]
  node [
    id 19
    label "warunek"
    origin "text"
  ]
  node [
    id 20
    label "je&#378;dzi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 21
    label "przez"
    origin "text"
  ]
  node [
    id 22
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 23
    label "dzienia"
    origin "text"
  ]
  node [
    id 24
    label "si&#281;"
    origin "text"
  ]
  node [
    id 25
    label "przy"
    origin "text"
  ]
  node [
    id 26
    label "tym"
    origin "text"
  ]
  node [
    id 27
    label "doskonale"
    origin "text"
  ]
  node [
    id 28
    label "inny"
  ]
  node [
    id 29
    label "nast&#281;pnie"
  ]
  node [
    id 30
    label "kt&#243;ry&#347;"
  ]
  node [
    id 31
    label "kolejno"
  ]
  node [
    id 32
    label "nastopny"
  ]
  node [
    id 33
    label "letni"
  ]
  node [
    id 34
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 35
    label "po&#380;egnanie"
  ]
  node [
    id 36
    label "spowodowanie"
  ]
  node [
    id 37
    label "znalezienie"
  ]
  node [
    id 38
    label "znajomy"
  ]
  node [
    id 39
    label "doznanie"
  ]
  node [
    id 40
    label "employment"
  ]
  node [
    id 41
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 42
    label "gather"
  ]
  node [
    id 43
    label "powitanie"
  ]
  node [
    id 44
    label "spotykanie"
  ]
  node [
    id 45
    label "wydarzenie"
  ]
  node [
    id 46
    label "gathering"
  ]
  node [
    id 47
    label "spotkanie_si&#281;"
  ]
  node [
    id 48
    label "zdarzenie_si&#281;"
  ]
  node [
    id 49
    label "match"
  ]
  node [
    id 50
    label "zawarcie"
  ]
  node [
    id 51
    label "pozostawa&#263;"
  ]
  node [
    id 52
    label "trwa&#263;"
  ]
  node [
    id 53
    label "wystarcza&#263;"
  ]
  node [
    id 54
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 55
    label "czeka&#263;"
  ]
  node [
    id 56
    label "stand"
  ]
  node [
    id 57
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 58
    label "mieszka&#263;"
  ]
  node [
    id 59
    label "wystarczy&#263;"
  ]
  node [
    id 60
    label "sprawowa&#263;"
  ]
  node [
    id 61
    label "przebywa&#263;"
  ]
  node [
    id 62
    label "kosztowa&#263;"
  ]
  node [
    id 63
    label "undertaking"
  ]
  node [
    id 64
    label "wystawa&#263;"
  ]
  node [
    id 65
    label "base"
  ]
  node [
    id 66
    label "digest"
  ]
  node [
    id 67
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 68
    label "postawi&#263;"
  ]
  node [
    id 69
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 70
    label "wytw&#243;r"
  ]
  node [
    id 71
    label "implikowa&#263;"
  ]
  node [
    id 72
    label "stawia&#263;"
  ]
  node [
    id 73
    label "mark"
  ]
  node [
    id 74
    label "kodzik"
  ]
  node [
    id 75
    label "attribute"
  ]
  node [
    id 76
    label "dow&#243;d"
  ]
  node [
    id 77
    label "herb"
  ]
  node [
    id 78
    label "fakt"
  ]
  node [
    id 79
    label "oznakowanie"
  ]
  node [
    id 80
    label "point"
  ]
  node [
    id 81
    label "bezlitosny"
  ]
  node [
    id 82
    label "przykro"
  ]
  node [
    id 83
    label "&#378;le"
  ]
  node [
    id 84
    label "okrutny"
  ]
  node [
    id 85
    label "heinously"
  ]
  node [
    id 86
    label "przypieka&#263;"
  ]
  node [
    id 87
    label "pociska&#263;"
  ]
  node [
    id 88
    label "beat_down"
  ]
  node [
    id 89
    label "ridicule"
  ]
  node [
    id 90
    label "burn"
  ]
  node [
    id 91
    label "pali&#263;"
  ]
  node [
    id 92
    label "grza&#263;"
  ]
  node [
    id 93
    label "sieka&#263;"
  ]
  node [
    id 94
    label "przygrzewa&#263;"
  ]
  node [
    id 95
    label "S&#322;o&#324;ce"
  ]
  node [
    id 96
    label "zach&#243;d"
  ]
  node [
    id 97
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 98
    label "&#347;wiat&#322;o"
  ]
  node [
    id 99
    label "sunlight"
  ]
  node [
    id 100
    label "wsch&#243;d"
  ]
  node [
    id 101
    label "kochanie"
  ]
  node [
    id 102
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 103
    label "pogoda"
  ]
  node [
    id 104
    label "dzie&#324;"
  ]
  node [
    id 105
    label "niezb&#281;dnie"
  ]
  node [
    id 106
    label "si&#281;ga&#263;"
  ]
  node [
    id 107
    label "obecno&#347;&#263;"
  ]
  node [
    id 108
    label "stan"
  ]
  node [
    id 109
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 110
    label "mie&#263;_miejsce"
  ]
  node [
    id 111
    label "uczestniczy&#263;"
  ]
  node [
    id 112
    label "chodzi&#263;"
  ]
  node [
    id 113
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 114
    label "equal"
  ]
  node [
    id 115
    label "nieprzerwany"
  ]
  node [
    id 116
    label "ci&#261;gle"
  ]
  node [
    id 117
    label "nieustanny"
  ]
  node [
    id 118
    label "sta&#322;y"
  ]
  node [
    id 119
    label "la&#263;"
  ]
  node [
    id 120
    label "glaze"
  ]
  node [
    id 121
    label "wlewa&#263;"
  ]
  node [
    id 122
    label "moczy&#263;"
  ]
  node [
    id 123
    label "na&#347;miewa&#263;_si&#281;"
  ]
  node [
    id 124
    label "powleka&#263;"
  ]
  node [
    id 125
    label "pour"
  ]
  node [
    id 126
    label "torowisko"
  ]
  node [
    id 127
    label "droga"
  ]
  node [
    id 128
    label "szyna"
  ]
  node [
    id 129
    label "rozstaw_przyl&#261;dkowy"
  ]
  node [
    id 130
    label "balastowanie"
  ]
  node [
    id 131
    label "aktynowiec"
  ]
  node [
    id 132
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 133
    label "spos&#243;b"
  ]
  node [
    id 134
    label "nawierzchnia_kolejowa"
  ]
  node [
    id 135
    label "rozstaw_bo&#347;niacki"
  ]
  node [
    id 136
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 137
    label "linia_kolejowa"
  ]
  node [
    id 138
    label "przeorientowywa&#263;"
  ]
  node [
    id 139
    label "bearing"
  ]
  node [
    id 140
    label "trasa"
  ]
  node [
    id 141
    label "kszta&#322;t"
  ]
  node [
    id 142
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 143
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 144
    label "miejsce"
  ]
  node [
    id 145
    label "kolej"
  ]
  node [
    id 146
    label "podbijarka_torowa"
  ]
  node [
    id 147
    label "przeorientowa&#263;"
  ]
  node [
    id 148
    label "rozstaw_tr&#243;jstopowy"
  ]
  node [
    id 149
    label "przeorientowanie"
  ]
  node [
    id 150
    label "podk&#322;ad"
  ]
  node [
    id 151
    label "przeorientowywanie"
  ]
  node [
    id 152
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 153
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 154
    label "lane"
  ]
  node [
    id 155
    label "korzystny"
  ]
  node [
    id 156
    label "okazyjnie"
  ]
  node [
    id 157
    label "cz&#322;owiek"
  ]
  node [
    id 158
    label "transportowiec"
  ]
  node [
    id 159
    label "ryzykowny"
  ]
  node [
    id 160
    label "ekstremalnie"
  ]
  node [
    id 161
    label "extremalny"
  ]
  node [
    id 162
    label "skrajnie"
  ]
  node [
    id 163
    label "za&#322;o&#380;enie"
  ]
  node [
    id 164
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 165
    label "umowa"
  ]
  node [
    id 166
    label "agent"
  ]
  node [
    id 167
    label "condition"
  ]
  node [
    id 168
    label "ekspozycja"
  ]
  node [
    id 169
    label "faktor"
  ]
  node [
    id 170
    label "du&#380;y"
  ]
  node [
    id 171
    label "jedyny"
  ]
  node [
    id 172
    label "kompletny"
  ]
  node [
    id 173
    label "zdr&#243;w"
  ]
  node [
    id 174
    label "&#380;ywy"
  ]
  node [
    id 175
    label "ca&#322;o"
  ]
  node [
    id 176
    label "pe&#322;ny"
  ]
  node [
    id 177
    label "calu&#347;ko"
  ]
  node [
    id 178
    label "podobny"
  ]
  node [
    id 179
    label "&#347;wietnie"
  ]
  node [
    id 180
    label "wspaniale"
  ]
  node [
    id 181
    label "doskona&#322;y"
  ]
  node [
    id 182
    label "kompletnie"
  ]
  node [
    id 183
    label "w&#322;a&#347;ciwie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 22
    target 174
  ]
  edge [
    source 22
    target 175
  ]
  edge [
    source 22
    target 176
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 22
    target 178
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 179
  ]
  edge [
    source 27
    target 180
  ]
  edge [
    source 27
    target 181
  ]
  edge [
    source 27
    target 182
  ]
  edge [
    source 27
    target 183
  ]
]
