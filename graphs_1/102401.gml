graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.0454545454545454
  density 0.023510971786833857
  graphCliqueNumber 3
  node [
    id 0
    label "okazja"
    origin "text"
  ]
  node [
    id 1
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 2
    label "wielki"
    origin "text"
  ]
  node [
    id 3
    label "orkiestra"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wi&#261;teczny"
    origin "text"
  ]
  node [
    id 5
    label "pomoc"
    origin "text"
  ]
  node [
    id 6
    label "ukaza&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "specjalny"
    origin "text"
  ]
  node [
    id 9
    label "numer"
    origin "text"
  ]
  node [
    id 10
    label "magazyn"
    origin "text"
  ]
  node [
    id 11
    label "outro"
    origin "text"
  ]
  node [
    id 12
    label "atrakcyjny"
  ]
  node [
    id 13
    label "oferta"
  ]
  node [
    id 14
    label "adeptness"
  ]
  node [
    id 15
    label "okazka"
  ]
  node [
    id 16
    label "wydarzenie"
  ]
  node [
    id 17
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 18
    label "podw&#243;zka"
  ]
  node [
    id 19
    label "autostop"
  ]
  node [
    id 20
    label "sytuacja"
  ]
  node [
    id 21
    label "koniec"
  ]
  node [
    id 22
    label "conclusion"
  ]
  node [
    id 23
    label "coating"
  ]
  node [
    id 24
    label "runda"
  ]
  node [
    id 25
    label "dupny"
  ]
  node [
    id 26
    label "wysoce"
  ]
  node [
    id 27
    label "wyj&#261;tkowy"
  ]
  node [
    id 28
    label "wybitny"
  ]
  node [
    id 29
    label "znaczny"
  ]
  node [
    id 30
    label "prawdziwy"
  ]
  node [
    id 31
    label "wa&#380;ny"
  ]
  node [
    id 32
    label "nieprzeci&#281;tny"
  ]
  node [
    id 33
    label "ch&#243;r"
  ]
  node [
    id 34
    label "zesp&#243;&#322;"
  ]
  node [
    id 35
    label "sekcja"
  ]
  node [
    id 36
    label "dzie&#324;_wolny"
  ]
  node [
    id 37
    label "&#347;wi&#281;tny"
  ]
  node [
    id 38
    label "&#347;wi&#261;tecznie"
  ]
  node [
    id 39
    label "od&#347;wi&#281;tny"
  ]
  node [
    id 40
    label "od&#347;wi&#281;tnie"
  ]
  node [
    id 41
    label "obrz&#281;dowy"
  ]
  node [
    id 42
    label "uroczysty"
  ]
  node [
    id 43
    label "zgodzi&#263;"
  ]
  node [
    id 44
    label "pomocnik"
  ]
  node [
    id 45
    label "doch&#243;d"
  ]
  node [
    id 46
    label "property"
  ]
  node [
    id 47
    label "przedmiot"
  ]
  node [
    id 48
    label "grupa"
  ]
  node [
    id 49
    label "telefon_zaufania"
  ]
  node [
    id 50
    label "darowizna"
  ]
  node [
    id 51
    label "&#347;rodek"
  ]
  node [
    id 52
    label "liga"
  ]
  node [
    id 53
    label "pokaza&#263;"
  ]
  node [
    id 54
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 55
    label "unwrap"
  ]
  node [
    id 56
    label "specjalnie"
  ]
  node [
    id 57
    label "nieetatowy"
  ]
  node [
    id 58
    label "intencjonalny"
  ]
  node [
    id 59
    label "szczeg&#243;lny"
  ]
  node [
    id 60
    label "odpowiedni"
  ]
  node [
    id 61
    label "niedorozw&#243;j"
  ]
  node [
    id 62
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 63
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 64
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 65
    label "nienormalny"
  ]
  node [
    id 66
    label "umy&#347;lnie"
  ]
  node [
    id 67
    label "manewr"
  ]
  node [
    id 68
    label "sztos"
  ]
  node [
    id 69
    label "pok&#243;j"
  ]
  node [
    id 70
    label "facet"
  ]
  node [
    id 71
    label "wyst&#281;p"
  ]
  node [
    id 72
    label "turn"
  ]
  node [
    id 73
    label "impression"
  ]
  node [
    id 74
    label "hotel"
  ]
  node [
    id 75
    label "liczba"
  ]
  node [
    id 76
    label "punkt"
  ]
  node [
    id 77
    label "czasopismo"
  ]
  node [
    id 78
    label "&#380;art"
  ]
  node [
    id 79
    label "orygina&#322;"
  ]
  node [
    id 80
    label "oznaczenie"
  ]
  node [
    id 81
    label "zi&#243;&#322;ko"
  ]
  node [
    id 82
    label "akt_p&#322;ciowy"
  ]
  node [
    id 83
    label "publikacja"
  ]
  node [
    id 84
    label "hurtownia"
  ]
  node [
    id 85
    label "fabryka"
  ]
  node [
    id 86
    label "tytu&#322;"
  ]
  node [
    id 87
    label "pomieszczenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 87
  ]
]
