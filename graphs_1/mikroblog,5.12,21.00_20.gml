graph [
  maxDegree 4
  minDegree 0
  meanDegree 3.3333333333333335
  density 0.6666666666666666
  graphCliqueNumber 5
  node [
    id 0
    label "dzienia"
    origin "text"
  ]
  node [
    id 1
    label "suchar"
  ]
  node [
    id 2
    label "bonusowy"
  ]
  node [
    id 3
    label "nades&#322;a&#263;"
  ]
  node [
    id 4
    label "przez"
  ]
  node [
    id 5
    label "Majster12"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
]
