graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.76
  density 0.07333333333333333
  graphCliqueNumber 3
  node [
    id 0
    label "plan"
    origin "text"
  ]
  node [
    id 1
    label "czteroletni"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "device"
  ]
  node [
    id 4
    label "model"
  ]
  node [
    id 5
    label "wytw&#243;r"
  ]
  node [
    id 6
    label "obraz"
  ]
  node [
    id 7
    label "przestrze&#324;"
  ]
  node [
    id 8
    label "dekoracja"
  ]
  node [
    id 9
    label "intencja"
  ]
  node [
    id 10
    label "agreement"
  ]
  node [
    id 11
    label "pomys&#322;"
  ]
  node [
    id 12
    label "punkt"
  ]
  node [
    id 13
    label "miejsce_pracy"
  ]
  node [
    id 14
    label "perspektywa"
  ]
  node [
    id 15
    label "rysunek"
  ]
  node [
    id 16
    label "reprezentacja"
  ]
  node [
    id 17
    label "kilkuletni"
  ]
  node [
    id 18
    label "Eugeniusz"
  ]
  node [
    id 19
    label "Kwiatkowski"
  ]
  node [
    id 20
    label "ii"
  ]
  node [
    id 21
    label "RP"
  ]
  node [
    id 22
    label "warszawski"
  ]
  node [
    id 23
    label "okr&#281;g"
  ]
  node [
    id 24
    label "przemys&#322;owy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
]
