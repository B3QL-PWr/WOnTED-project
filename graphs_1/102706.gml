graph [
  maxDegree 35
  minDegree 1
  meanDegree 2.242424242424242
  density 0.022881880024737167
  graphCliqueNumber 3
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 3
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 4
    label "filler"
    origin "text"
  ]
  node [
    id 5
    label "aleja"
    origin "text"
  ]
  node [
    id 6
    label "jerozolimski"
    origin "text"
  ]
  node [
    id 7
    label "przy"
    origin "text"
  ]
  node [
    id 8
    label "centralne"
    origin "text"
  ]
  node [
    id 9
    label "ten"
    origin "text"
  ]
  node [
    id 10
    label "pi&#281;&#263;set"
    origin "text"
  ]
  node [
    id 11
    label "co&#347;"
    origin "text"
  ]
  node [
    id 12
    label "chyba"
    origin "text"
  ]
  node [
    id 13
    label "si&#281;"
    origin "text"
  ]
  node [
    id 14
    label "zdawa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "by&#263;"
    origin "text"
  ]
  node [
    id 16
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 17
    label "jeszcze"
    origin "text"
  ]
  node [
    id 18
    label "jecha&#263;"
    origin "text"
  ]
  node [
    id 19
    label "dziewi&#281;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "proceed"
  ]
  node [
    id 21
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 22
    label "bangla&#263;"
  ]
  node [
    id 23
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 24
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 25
    label "run"
  ]
  node [
    id 26
    label "tryb"
  ]
  node [
    id 27
    label "p&#322;ywa&#263;"
  ]
  node [
    id 28
    label "continue"
  ]
  node [
    id 29
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 30
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 31
    label "przebiega&#263;"
  ]
  node [
    id 32
    label "mie&#263;_miejsce"
  ]
  node [
    id 33
    label "wk&#322;ada&#263;"
  ]
  node [
    id 34
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 35
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 36
    label "para"
  ]
  node [
    id 37
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 38
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 39
    label "krok"
  ]
  node [
    id 40
    label "str&#243;j"
  ]
  node [
    id 41
    label "bywa&#263;"
  ]
  node [
    id 42
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 43
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 44
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 45
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 46
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 47
    label "dziama&#263;"
  ]
  node [
    id 48
    label "stara&#263;_si&#281;"
  ]
  node [
    id 49
    label "carry"
  ]
  node [
    id 50
    label "dok&#322;adnie"
  ]
  node [
    id 51
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 52
    label "drzewo_owocowe"
  ]
  node [
    id 53
    label "chodnik"
  ]
  node [
    id 54
    label "ulica"
  ]
  node [
    id 55
    label "deptak"
  ]
  node [
    id 56
    label "piec"
  ]
  node [
    id 57
    label "central_heating"
  ]
  node [
    id 58
    label "miarkownik_ci&#261;gu"
  ]
  node [
    id 59
    label "ogrzewanie"
  ]
  node [
    id 60
    label "okre&#347;lony"
  ]
  node [
    id 61
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 62
    label "thing"
  ]
  node [
    id 63
    label "cosik"
  ]
  node [
    id 64
    label "zalicza&#263;"
  ]
  node [
    id 65
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 66
    label "opowiada&#263;"
  ]
  node [
    id 67
    label "render"
  ]
  node [
    id 68
    label "impart"
  ]
  node [
    id 69
    label "oddawa&#263;"
  ]
  node [
    id 70
    label "zostawia&#263;"
  ]
  node [
    id 71
    label "sk&#322;ada&#263;"
  ]
  node [
    id 72
    label "convey"
  ]
  node [
    id 73
    label "powierza&#263;"
  ]
  node [
    id 74
    label "bequeath"
  ]
  node [
    id 75
    label "si&#281;ga&#263;"
  ]
  node [
    id 76
    label "trwa&#263;"
  ]
  node [
    id 77
    label "obecno&#347;&#263;"
  ]
  node [
    id 78
    label "stan"
  ]
  node [
    id 79
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 80
    label "stand"
  ]
  node [
    id 81
    label "uczestniczy&#263;"
  ]
  node [
    id 82
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 83
    label "equal"
  ]
  node [
    id 84
    label "ci&#261;gle"
  ]
  node [
    id 85
    label "napada&#263;"
  ]
  node [
    id 86
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 87
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 88
    label "wykonywa&#263;"
  ]
  node [
    id 89
    label "czu&#263;"
  ]
  node [
    id 90
    label "overdrive"
  ]
  node [
    id 91
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 92
    label "ride"
  ]
  node [
    id 93
    label "korzysta&#263;"
  ]
  node [
    id 94
    label "go"
  ]
  node [
    id 95
    label "prowadzi&#263;"
  ]
  node [
    id 96
    label "drive"
  ]
  node [
    id 97
    label "kontynuowa&#263;"
  ]
  node [
    id 98
    label "odbywa&#263;"
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 65
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 14
    target 67
  ]
  edge [
    source 14
    target 68
  ]
  edge [
    source 14
    target 69
  ]
  edge [
    source 14
    target 70
  ]
  edge [
    source 14
    target 71
  ]
  edge [
    source 14
    target 72
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 15
    target 32
  ]
  edge [
    source 15
    target 81
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 85
  ]
  edge [
    source 18
    target 86
  ]
  edge [
    source 18
    target 87
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 89
  ]
  edge [
    source 18
    target 90
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 92
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 94
  ]
  edge [
    source 18
    target 95
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 30
  ]
  edge [
    source 18
    target 96
  ]
  edge [
    source 18
    target 97
  ]
  edge [
    source 18
    target 38
  ]
  edge [
    source 18
    target 98
  ]
  edge [
    source 18
    target 45
  ]
  edge [
    source 18
    target 49
  ]
]
