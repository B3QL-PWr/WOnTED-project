graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.0129032258064514
  density 0.013070800167574361
  graphCliqueNumber 2
  node [
    id 0
    label "dokument"
    origin "text"
  ]
  node [
    id 1
    label "trafi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "teraz"
    origin "text"
  ]
  node [
    id 3
    label "parlament"
    origin "text"
  ]
  node [
    id 4
    label "europejski"
    origin "text"
  ]
  node [
    id 5
    label "gdzie"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "opracowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ostateczny"
    origin "text"
  ]
  node [
    id 9
    label "kszta&#322;t"
    origin "text"
  ]
  node [
    id 10
    label "reguluj&#261;cy"
    origin "text"
  ]
  node [
    id 11
    label "zasada"
    origin "text"
  ]
  node [
    id 12
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "transport"
    origin "text"
  ]
  node [
    id 14
    label "drogowe"
    origin "text"
  ]
  node [
    id 15
    label "rok"
    origin "text"
  ]
  node [
    id 16
    label "record"
  ]
  node [
    id 17
    label "wytw&#243;r"
  ]
  node [
    id 18
    label "&#347;wiadectwo"
  ]
  node [
    id 19
    label "zapis"
  ]
  node [
    id 20
    label "fascyku&#322;"
  ]
  node [
    id 21
    label "raport&#243;wka"
  ]
  node [
    id 22
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 23
    label "artyku&#322;"
  ]
  node [
    id 24
    label "plik"
  ]
  node [
    id 25
    label "writing"
  ]
  node [
    id 26
    label "utw&#243;r"
  ]
  node [
    id 27
    label "dokumentacja"
  ]
  node [
    id 28
    label "parafa"
  ]
  node [
    id 29
    label "sygnatariusz"
  ]
  node [
    id 30
    label "registratura"
  ]
  node [
    id 31
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 32
    label "przypasowa&#263;"
  ]
  node [
    id 33
    label "wpa&#347;&#263;"
  ]
  node [
    id 34
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 35
    label "spotka&#263;"
  ]
  node [
    id 36
    label "dotrze&#263;"
  ]
  node [
    id 37
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 38
    label "happen"
  ]
  node [
    id 39
    label "znale&#378;&#263;"
  ]
  node [
    id 40
    label "hit"
  ]
  node [
    id 41
    label "pocisk"
  ]
  node [
    id 42
    label "stumble"
  ]
  node [
    id 43
    label "dolecie&#263;"
  ]
  node [
    id 44
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 45
    label "chwila"
  ]
  node [
    id 46
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 47
    label "grupa"
  ]
  node [
    id 48
    label "plankton_polityczny"
  ]
  node [
    id 49
    label "ustawodawca"
  ]
  node [
    id 50
    label "urz&#261;d"
  ]
  node [
    id 51
    label "europarlament"
  ]
  node [
    id 52
    label "grupa_bilateralna"
  ]
  node [
    id 53
    label "European"
  ]
  node [
    id 54
    label "po_europejsku"
  ]
  node [
    id 55
    label "charakterystyczny"
  ]
  node [
    id 56
    label "europejsko"
  ]
  node [
    id 57
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 58
    label "typowy"
  ]
  node [
    id 59
    label "si&#281;ga&#263;"
  ]
  node [
    id 60
    label "trwa&#263;"
  ]
  node [
    id 61
    label "obecno&#347;&#263;"
  ]
  node [
    id 62
    label "stan"
  ]
  node [
    id 63
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 64
    label "stand"
  ]
  node [
    id 65
    label "mie&#263;_miejsce"
  ]
  node [
    id 66
    label "uczestniczy&#263;"
  ]
  node [
    id 67
    label "chodzi&#263;"
  ]
  node [
    id 68
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 69
    label "equal"
  ]
  node [
    id 70
    label "invent"
  ]
  node [
    id 71
    label "przygotowa&#263;"
  ]
  node [
    id 72
    label "konieczny"
  ]
  node [
    id 73
    label "skrajny"
  ]
  node [
    id 74
    label "zupe&#322;ny"
  ]
  node [
    id 75
    label "ostatecznie"
  ]
  node [
    id 76
    label "wygl&#261;d"
  ]
  node [
    id 77
    label "comeliness"
  ]
  node [
    id 78
    label "blaszka"
  ]
  node [
    id 79
    label "linearno&#347;&#263;"
  ]
  node [
    id 80
    label "obiekt"
  ]
  node [
    id 81
    label "p&#281;tla"
  ]
  node [
    id 82
    label "p&#322;at"
  ]
  node [
    id 83
    label "gwiazda"
  ]
  node [
    id 84
    label "formacja"
  ]
  node [
    id 85
    label "cecha"
  ]
  node [
    id 86
    label "g&#322;owa"
  ]
  node [
    id 87
    label "punkt_widzenia"
  ]
  node [
    id 88
    label "kielich"
  ]
  node [
    id 89
    label "miniatura"
  ]
  node [
    id 90
    label "spirala"
  ]
  node [
    id 91
    label "charakter"
  ]
  node [
    id 92
    label "pasmo"
  ]
  node [
    id 93
    label "face"
  ]
  node [
    id 94
    label "obserwacja"
  ]
  node [
    id 95
    label "moralno&#347;&#263;"
  ]
  node [
    id 96
    label "podstawa"
  ]
  node [
    id 97
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 98
    label "umowa"
  ]
  node [
    id 99
    label "dominion"
  ]
  node [
    id 100
    label "qualification"
  ]
  node [
    id 101
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 102
    label "opis"
  ]
  node [
    id 103
    label "regu&#322;a_Allena"
  ]
  node [
    id 104
    label "normalizacja"
  ]
  node [
    id 105
    label "regu&#322;a_Glogera"
  ]
  node [
    id 106
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 107
    label "standard"
  ]
  node [
    id 108
    label "base"
  ]
  node [
    id 109
    label "substancja"
  ]
  node [
    id 110
    label "spos&#243;b"
  ]
  node [
    id 111
    label "prawid&#322;o"
  ]
  node [
    id 112
    label "prawo_Mendla"
  ]
  node [
    id 113
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 114
    label "criterion"
  ]
  node [
    id 115
    label "twierdzenie"
  ]
  node [
    id 116
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 117
    label "prawo"
  ]
  node [
    id 118
    label "occupation"
  ]
  node [
    id 119
    label "zasada_d'Alemberta"
  ]
  node [
    id 120
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 121
    label "work"
  ]
  node [
    id 122
    label "robi&#263;"
  ]
  node [
    id 123
    label "muzyka"
  ]
  node [
    id 124
    label "rola"
  ]
  node [
    id 125
    label "create"
  ]
  node [
    id 126
    label "wytwarza&#263;"
  ]
  node [
    id 127
    label "praca"
  ]
  node [
    id 128
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 129
    label "zawarto&#347;&#263;"
  ]
  node [
    id 130
    label "unos"
  ]
  node [
    id 131
    label "traffic"
  ]
  node [
    id 132
    label "za&#322;adunek"
  ]
  node [
    id 133
    label "gospodarka"
  ]
  node [
    id 134
    label "roz&#322;adunek"
  ]
  node [
    id 135
    label "sprz&#281;t"
  ]
  node [
    id 136
    label "jednoszynowy"
  ]
  node [
    id 137
    label "cedu&#322;a"
  ]
  node [
    id 138
    label "tyfon"
  ]
  node [
    id 139
    label "us&#322;uga"
  ]
  node [
    id 140
    label "prze&#322;adunek"
  ]
  node [
    id 141
    label "komunikacja"
  ]
  node [
    id 142
    label "towar"
  ]
  node [
    id 143
    label "stulecie"
  ]
  node [
    id 144
    label "kalendarz"
  ]
  node [
    id 145
    label "czas"
  ]
  node [
    id 146
    label "pora_roku"
  ]
  node [
    id 147
    label "cykl_astronomiczny"
  ]
  node [
    id 148
    label "p&#243;&#322;rocze"
  ]
  node [
    id 149
    label "kwarta&#322;"
  ]
  node [
    id 150
    label "kurs"
  ]
  node [
    id 151
    label "jubileusz"
  ]
  node [
    id 152
    label "miesi&#261;c"
  ]
  node [
    id 153
    label "lata"
  ]
  node [
    id 154
    label "martwy_sezon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 47
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 47
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
]
