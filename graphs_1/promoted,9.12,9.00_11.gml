graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9375
  density 0.030753968253968252
  graphCliqueNumber 2
  node [
    id 0
    label "napi&#281;cie"
    origin "text"
  ]
  node [
    id 1
    label "niepok&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "przed"
    origin "text"
  ]
  node [
    id 3
    label "zapowiedzie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sobota"
    origin "text"
  ]
  node [
    id 5
    label "kolejny"
    origin "text"
  ]
  node [
    id 6
    label "protest"
    origin "text"
  ]
  node [
    id 7
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 8
    label "kamizelka"
    origin "text"
  ]
  node [
    id 9
    label "francja"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;ga&#263;"
    origin "text"
  ]
  node [
    id 11
    label "zenit"
    origin "text"
  ]
  node [
    id 12
    label "usztywnienie"
  ]
  node [
    id 13
    label "napr&#281;&#380;enie"
  ]
  node [
    id 14
    label "striving"
  ]
  node [
    id 15
    label "nastr&#243;j"
  ]
  node [
    id 16
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 17
    label "tension"
  ]
  node [
    id 18
    label "akatyzja"
  ]
  node [
    id 19
    label "niespokojno&#347;&#263;"
  ]
  node [
    id 20
    label "emocja"
  ]
  node [
    id 21
    label "w&#261;tpliwo&#347;&#263;"
  ]
  node [
    id 22
    label "motion"
  ]
  node [
    id 23
    label "nerwowo&#347;&#263;"
  ]
  node [
    id 24
    label "announce"
  ]
  node [
    id 25
    label "spowodowa&#263;"
  ]
  node [
    id 26
    label "przestrzec"
  ]
  node [
    id 27
    label "sign"
  ]
  node [
    id 28
    label "poinformowa&#263;"
  ]
  node [
    id 29
    label "og&#322;osi&#263;"
  ]
  node [
    id 30
    label "weekend"
  ]
  node [
    id 31
    label "dzie&#324;_powszedni"
  ]
  node [
    id 32
    label "Wielka_Sobota"
  ]
  node [
    id 33
    label "inny"
  ]
  node [
    id 34
    label "nast&#281;pnie"
  ]
  node [
    id 35
    label "kt&#243;ry&#347;"
  ]
  node [
    id 36
    label "kolejno"
  ]
  node [
    id 37
    label "nastopny"
  ]
  node [
    id 38
    label "reakcja"
  ]
  node [
    id 39
    label "protestacja"
  ]
  node [
    id 40
    label "czerwona_kartka"
  ]
  node [
    id 41
    label "jasny"
  ]
  node [
    id 42
    label "typ_mongoloidalny"
  ]
  node [
    id 43
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 44
    label "kolorowy"
  ]
  node [
    id 45
    label "ciep&#322;y"
  ]
  node [
    id 46
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 47
    label "westa"
  ]
  node [
    id 48
    label "g&#243;ra"
  ]
  node [
    id 49
    label "get"
  ]
  node [
    id 50
    label "mierzy&#263;"
  ]
  node [
    id 51
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 52
    label "appreciation"
  ]
  node [
    id 53
    label "osi&#261;ga&#263;"
  ]
  node [
    id 54
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 55
    label "dociera&#263;"
  ]
  node [
    id 56
    label "korzysta&#263;"
  ]
  node [
    id 57
    label "u&#380;ywa&#263;"
  ]
  node [
    id 58
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 59
    label "compass"
  ]
  node [
    id 60
    label "exsert"
  ]
  node [
    id 61
    label "poziom"
  ]
  node [
    id 62
    label "Benjamin"
  ]
  node [
    id 63
    label "Cauchy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 62
    target 63
  ]
]
