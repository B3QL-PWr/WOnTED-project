graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.96
  density 0.04
  graphCliqueNumber 2
  node [
    id 0
    label "pantuniesta&#322;"
    origin "text"
  ]
  node [
    id 1
    label "dowiedzie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "projekt"
    origin "text"
  ]
  node [
    id 4
    label "myspace"
    origin "text"
  ]
  node [
    id 5
    label "czyli"
    origin "text"
  ]
  node [
    id 6
    label "rewitalizacja"
    origin "text"
  ]
  node [
    id 7
    label "stara"
    origin "text"
  ]
  node [
    id 8
    label "gablota"
    origin "text"
  ]
  node [
    id 9
    label "reklamowy"
    origin "text"
  ]
  node [
    id 10
    label "okolica"
    origin "text"
  ]
  node [
    id 11
    label "poprzez"
    origin "text"
  ]
  node [
    id 12
    label "prezentowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "dokument"
  ]
  node [
    id 14
    label "device"
  ]
  node [
    id 15
    label "program_u&#380;ytkowy"
  ]
  node [
    id 16
    label "intencja"
  ]
  node [
    id 17
    label "agreement"
  ]
  node [
    id 18
    label "pomys&#322;"
  ]
  node [
    id 19
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 20
    label "plan"
  ]
  node [
    id 21
    label "dokumentacja"
  ]
  node [
    id 22
    label "odnowa"
  ]
  node [
    id 23
    label "odbudowa"
  ]
  node [
    id 24
    label "matka"
  ]
  node [
    id 25
    label "kobieta"
  ]
  node [
    id 26
    label "partnerka"
  ]
  node [
    id 27
    label "&#380;ona"
  ]
  node [
    id 28
    label "starzy"
  ]
  node [
    id 29
    label "fura"
  ]
  node [
    id 30
    label "szafka"
  ]
  node [
    id 31
    label "miejsce"
  ]
  node [
    id 32
    label "obszar"
  ]
  node [
    id 33
    label "krajobraz"
  ]
  node [
    id 34
    label "grupa"
  ]
  node [
    id 35
    label "organ"
  ]
  node [
    id 36
    label "przyroda"
  ]
  node [
    id 37
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 38
    label "po_s&#261;siedzku"
  ]
  node [
    id 39
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 40
    label "zapoznawa&#263;"
  ]
  node [
    id 41
    label "przedstawia&#263;"
  ]
  node [
    id 42
    label "present"
  ]
  node [
    id 43
    label "gra&#263;"
  ]
  node [
    id 44
    label "uprzedza&#263;"
  ]
  node [
    id 45
    label "represent"
  ]
  node [
    id 46
    label "program"
  ]
  node [
    id 47
    label "wyra&#380;a&#263;"
  ]
  node [
    id 48
    label "attest"
  ]
  node [
    id 49
    label "display"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 31
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 12
    target 41
  ]
  edge [
    source 12
    target 42
  ]
  edge [
    source 12
    target 43
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 45
  ]
  edge [
    source 12
    target 46
  ]
  edge [
    source 12
    target 47
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 49
  ]
]
