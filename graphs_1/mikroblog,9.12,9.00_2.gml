graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9285714285714286
  density 0.07142857142857142
  graphCliqueNumber 2
  node [
    id 0
    label "te&#380;"
    origin "text"
  ]
  node [
    id 1
    label "znale&#378;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "jeden"
    origin "text"
  ]
  node [
    id 3
    label "ten"
    origin "text"
  ]
  node [
    id 4
    label "s&#322;ynny"
    origin "text"
  ]
  node [
    id 5
    label "kawalerka"
    origin "text"
  ]
  node [
    id 6
    label "kieliszek"
  ]
  node [
    id 7
    label "shot"
  ]
  node [
    id 8
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 9
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 10
    label "jaki&#347;"
  ]
  node [
    id 11
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 12
    label "jednolicie"
  ]
  node [
    id 13
    label "w&#243;dka"
  ]
  node [
    id 14
    label "ujednolicenie"
  ]
  node [
    id 15
    label "jednakowy"
  ]
  node [
    id 16
    label "okre&#347;lony"
  ]
  node [
    id 17
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 18
    label "ws&#322;awianie_si&#281;"
  ]
  node [
    id 19
    label "ws&#322;awienie"
  ]
  node [
    id 20
    label "ws&#322;awienie_si&#281;"
  ]
  node [
    id 21
    label "wielki"
  ]
  node [
    id 22
    label "znany"
  ]
  node [
    id 23
    label "os&#322;awiony"
  ]
  node [
    id 24
    label "ws&#322;awianie"
  ]
  node [
    id 25
    label "m&#322;odzie&#380;"
  ]
  node [
    id 26
    label "mieszkanie"
  ]
  node [
    id 27
    label "garsoniera"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
]
