graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.0363382250174703
  graphCliqueNumber 3
  node [
    id 0
    label "s&#322;ynny"
    origin "text"
  ]
  node [
    id 1
    label "emil"
    origin "text"
  ]
  node [
    id 2
    label "radar"
    origin "text"
  ]
  node [
    id 3
    label "mandat"
    origin "text"
  ]
  node [
    id 4
    label "znienawidzi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "przez"
    origin "text"
  ]
  node [
    id 6
    label "stra&#380;"
    origin "text"
  ]
  node [
    id 7
    label "miejski"
    origin "text"
  ]
  node [
    id 8
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "polska"
    origin "text"
  ]
  node [
    id 10
    label "ws&#322;awianie_si&#281;"
  ]
  node [
    id 11
    label "ws&#322;awienie"
  ]
  node [
    id 12
    label "ws&#322;awienie_si&#281;"
  ]
  node [
    id 13
    label "wielki"
  ]
  node [
    id 14
    label "znany"
  ]
  node [
    id 15
    label "os&#322;awiony"
  ]
  node [
    id 16
    label "ws&#322;awianie"
  ]
  node [
    id 17
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 18
    label "pelengowanie"
  ]
  node [
    id 19
    label "namierza&#263;"
  ]
  node [
    id 20
    label "urz&#261;dzenie"
  ]
  node [
    id 21
    label "dokument"
  ]
  node [
    id 22
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 23
    label "kara_pieni&#281;&#380;na"
  ]
  node [
    id 24
    label "commission"
  ]
  node [
    id 25
    label "hate"
  ]
  node [
    id 26
    label "zacz&#261;&#263;"
  ]
  node [
    id 27
    label "ochrona"
  ]
  node [
    id 28
    label "s&#322;u&#380;ba_publiczna"
  ]
  node [
    id 29
    label "rota"
  ]
  node [
    id 30
    label "wedeta"
  ]
  node [
    id 31
    label "posterunek"
  ]
  node [
    id 32
    label "s&#322;u&#380;ba"
  ]
  node [
    id 33
    label "stra&#380;_ogniowa"
  ]
  node [
    id 34
    label "miejsko"
  ]
  node [
    id 35
    label "miastowy"
  ]
  node [
    id 36
    label "typowy"
  ]
  node [
    id 37
    label "publiczny"
  ]
  node [
    id 38
    label "du&#380;y"
  ]
  node [
    id 39
    label "jedyny"
  ]
  node [
    id 40
    label "kompletny"
  ]
  node [
    id 41
    label "zdr&#243;w"
  ]
  node [
    id 42
    label "&#380;ywy"
  ]
  node [
    id 43
    label "ca&#322;o"
  ]
  node [
    id 44
    label "pe&#322;ny"
  ]
  node [
    id 45
    label "calu&#347;ko"
  ]
  node [
    id 46
    label "podobny"
  ]
  node [
    id 47
    label "Emil"
  ]
  node [
    id 48
    label "Piotr"
  ]
  node [
    id 49
    label "Lisiewicza"
  ]
  node [
    id 50
    label "Rau"
  ]
  node [
    id 51
    label "wywiad"
  ]
  node [
    id 52
    label "z"
  ]
  node [
    id 53
    label "chuligan"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 52
    target 53
  ]
]
