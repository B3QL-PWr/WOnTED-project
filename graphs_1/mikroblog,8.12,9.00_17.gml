graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9047619047619047
  density 0.09523809523809523
  graphCliqueNumber 2
  node [
    id 0
    label "tam"
    origin "text"
  ]
  node [
    id 1
    label "&#347;mieszek"
    origin "text"
  ]
  node [
    id 2
    label "warto"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "tu"
  ]
  node [
    id 5
    label "weso&#322;ek"
  ]
  node [
    id 6
    label "przysparza&#263;"
  ]
  node [
    id 7
    label "give"
  ]
  node [
    id 8
    label "kali&#263;_si&#281;"
  ]
  node [
    id 9
    label "bonanza"
  ]
  node [
    id 10
    label "si&#281;ga&#263;"
  ]
  node [
    id 11
    label "trwa&#263;"
  ]
  node [
    id 12
    label "obecno&#347;&#263;"
  ]
  node [
    id 13
    label "stan"
  ]
  node [
    id 14
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 15
    label "stand"
  ]
  node [
    id 16
    label "mie&#263;_miejsce"
  ]
  node [
    id 17
    label "uczestniczy&#263;"
  ]
  node [
    id 18
    label "chodzi&#263;"
  ]
  node [
    id 19
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 20
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
]
