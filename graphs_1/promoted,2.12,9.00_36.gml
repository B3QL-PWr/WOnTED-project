graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.5714285714285716
  density 0.05357142857142857
  graphCliqueNumber 7
  node [
    id 0
    label "&#347;miertelna"
    origin "text"
  ]
  node [
    id 1
    label "potr&#261;cenie"
    origin "text"
  ]
  node [
    id 2
    label "latko"
    origin "text"
  ]
  node [
    id 3
    label "droga"
    origin "text"
  ]
  node [
    id 4
    label "krajowy"
    origin "text"
  ]
  node [
    id 5
    label "warmi&#324;sko"
    origin "text"
  ]
  node [
    id 6
    label "mazurski"
    origin "text"
  ]
  node [
    id 7
    label "wytr&#261;cenie"
  ]
  node [
    id 8
    label "movement"
  ]
  node [
    id 9
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 10
    label "jab"
  ]
  node [
    id 11
    label "uderzenie"
  ]
  node [
    id 12
    label "odliczenie"
  ]
  node [
    id 13
    label "jog"
  ]
  node [
    id 14
    label "kwota"
  ]
  node [
    id 15
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 16
    label "journey"
  ]
  node [
    id 17
    label "podbieg"
  ]
  node [
    id 18
    label "bezsilnikowy"
  ]
  node [
    id 19
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 20
    label "wylot"
  ]
  node [
    id 21
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 22
    label "drogowskaz"
  ]
  node [
    id 23
    label "nawierzchnia"
  ]
  node [
    id 24
    label "turystyka"
  ]
  node [
    id 25
    label "budowla"
  ]
  node [
    id 26
    label "spos&#243;b"
  ]
  node [
    id 27
    label "passage"
  ]
  node [
    id 28
    label "marszrutyzacja"
  ]
  node [
    id 29
    label "zbior&#243;wka"
  ]
  node [
    id 30
    label "ekskursja"
  ]
  node [
    id 31
    label "rajza"
  ]
  node [
    id 32
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 33
    label "ruch"
  ]
  node [
    id 34
    label "trasa"
  ]
  node [
    id 35
    label "wyb&#243;j"
  ]
  node [
    id 36
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 37
    label "ekwipunek"
  ]
  node [
    id 38
    label "korona_drogi"
  ]
  node [
    id 39
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 40
    label "pobocze"
  ]
  node [
    id 41
    label "rodzimy"
  ]
  node [
    id 42
    label "po_mazursku"
  ]
  node [
    id 43
    label "polski"
  ]
  node [
    id 44
    label "gwara"
  ]
  node [
    id 45
    label "regionalny"
  ]
  node [
    id 46
    label "nr"
  ]
  node [
    id 47
    label "16"
  ]
  node [
    id 48
    label "w"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
]
