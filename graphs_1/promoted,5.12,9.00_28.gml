graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9672131147540983
  density 0.03278688524590164
  graphCliqueNumber 2
  node [
    id 0
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 1
    label "czas"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "w&#322;ama&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 6
    label "jako&#347;"
  ]
  node [
    id 7
    label "charakterystyczny"
  ]
  node [
    id 8
    label "ciekawy"
  ]
  node [
    id 9
    label "jako_tako"
  ]
  node [
    id 10
    label "dziwny"
  ]
  node [
    id 11
    label "niez&#322;y"
  ]
  node [
    id 12
    label "przyzwoity"
  ]
  node [
    id 13
    label "czasokres"
  ]
  node [
    id 14
    label "trawienie"
  ]
  node [
    id 15
    label "kategoria_gramatyczna"
  ]
  node [
    id 16
    label "period"
  ]
  node [
    id 17
    label "odczyt"
  ]
  node [
    id 18
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 19
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 20
    label "chwila"
  ]
  node [
    id 21
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 22
    label "poprzedzenie"
  ]
  node [
    id 23
    label "koniugacja"
  ]
  node [
    id 24
    label "dzieje"
  ]
  node [
    id 25
    label "poprzedzi&#263;"
  ]
  node [
    id 26
    label "przep&#322;ywanie"
  ]
  node [
    id 27
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 28
    label "odwlekanie_si&#281;"
  ]
  node [
    id 29
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 30
    label "Zeitgeist"
  ]
  node [
    id 31
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 32
    label "okres_czasu"
  ]
  node [
    id 33
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 34
    label "pochodzi&#263;"
  ]
  node [
    id 35
    label "schy&#322;ek"
  ]
  node [
    id 36
    label "czwarty_wymiar"
  ]
  node [
    id 37
    label "chronometria"
  ]
  node [
    id 38
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 39
    label "poprzedzanie"
  ]
  node [
    id 40
    label "pogoda"
  ]
  node [
    id 41
    label "zegar"
  ]
  node [
    id 42
    label "pochodzenie"
  ]
  node [
    id 43
    label "poprzedza&#263;"
  ]
  node [
    id 44
    label "trawi&#263;"
  ]
  node [
    id 45
    label "time_period"
  ]
  node [
    id 46
    label "rachuba_czasu"
  ]
  node [
    id 47
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 48
    label "czasoprzestrze&#324;"
  ]
  node [
    id 49
    label "laba"
  ]
  node [
    id 50
    label "stanie"
  ]
  node [
    id 51
    label "przebywanie"
  ]
  node [
    id 52
    label "panowanie"
  ]
  node [
    id 53
    label "zajmowanie"
  ]
  node [
    id 54
    label "pomieszkanie"
  ]
  node [
    id 55
    label "adjustment"
  ]
  node [
    id 56
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 57
    label "lokal"
  ]
  node [
    id 58
    label "kwadrat"
  ]
  node [
    id 59
    label "animation"
  ]
  node [
    id 60
    label "dom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
]
