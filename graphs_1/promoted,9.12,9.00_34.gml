graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9574468085106382
  density 0.0425531914893617
  graphCliqueNumber 2
  node [
    id 0
    label "kart"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 3
    label "kupi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "bilet"
    origin "text"
  ]
  node [
    id 5
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 6
    label "automat"
    origin "text"
  ]
  node [
    id 7
    label "wy&#347;cig&#243;wka"
  ]
  node [
    id 8
    label "si&#281;ga&#263;"
  ]
  node [
    id 9
    label "trwa&#263;"
  ]
  node [
    id 10
    label "obecno&#347;&#263;"
  ]
  node [
    id 11
    label "stan"
  ]
  node [
    id 12
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "stand"
  ]
  node [
    id 14
    label "mie&#263;_miejsce"
  ]
  node [
    id 15
    label "uczestniczy&#263;"
  ]
  node [
    id 16
    label "chodzi&#263;"
  ]
  node [
    id 17
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 18
    label "equal"
  ]
  node [
    id 19
    label "free"
  ]
  node [
    id 20
    label "get"
  ]
  node [
    id 21
    label "wzi&#261;&#263;"
  ]
  node [
    id 22
    label "catch"
  ]
  node [
    id 23
    label "przyj&#261;&#263;"
  ]
  node [
    id 24
    label "beget"
  ]
  node [
    id 25
    label "pozyska&#263;"
  ]
  node [
    id 26
    label "ustawi&#263;"
  ]
  node [
    id 27
    label "uzna&#263;"
  ]
  node [
    id 28
    label "zagra&#263;"
  ]
  node [
    id 29
    label "uwierzy&#263;"
  ]
  node [
    id 30
    label "karta_wst&#281;pu"
  ]
  node [
    id 31
    label "cedu&#322;a"
  ]
  node [
    id 32
    label "passe-partout"
  ]
  node [
    id 33
    label "konik"
  ]
  node [
    id 34
    label "nijaki"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "dehumanizacja"
  ]
  node [
    id 37
    label "automatyczna_skrzynia_bieg&#243;w"
  ]
  node [
    id 38
    label "pralka"
  ]
  node [
    id 39
    label "telefon"
  ]
  node [
    id 40
    label "pistolet"
  ]
  node [
    id 41
    label "lody_w&#322;oskie"
  ]
  node [
    id 42
    label "samoch&#243;d"
  ]
  node [
    id 43
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 44
    label "urz&#261;dzenie"
  ]
  node [
    id 45
    label "bro&#324;_samoczynno-samopowtarzalna"
  ]
  node [
    id 46
    label "wrzutnik_monet"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
]
