graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 0.2
  graphCliqueNumber 2
  node [
    id 0
    label "emden"
    origin "text"
  ]
  node [
    id 1
    label "au&#223;enhafen"
    origin "text"
  ]
  node [
    id 2
    label "Emden"
  ]
  node [
    id 3
    label "Au&#223;enhafen"
  ]
  node [
    id 4
    label "dolny"
  ]
  node [
    id 5
    label "Saksonia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
]
