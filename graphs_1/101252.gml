graph [
  maxDegree 2
  minDegree 0
  meanDegree 1.3333333333333333
  density 0.26666666666666666
  graphCliqueNumber 3
  node [
    id 0
    label "tolmezzo"
    origin "text"
  ]
  node [
    id 1
    label "Friuli"
  ]
  node [
    id 2
    label "Wenecja"
  ]
  node [
    id 3
    label "julijski"
  ]
  node [
    id 4
    label "Andrea"
  ]
  node [
    id 5
    label "Morassi"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
]
