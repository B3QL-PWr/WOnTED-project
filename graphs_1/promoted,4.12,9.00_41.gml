graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.0185185185185186
  density 0.01886465905157494
  graphCliqueNumber 2
  node [
    id 0
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "lato"
    origin "text"
  ]
  node [
    id 2
    label "nowonarodzon&#261;"
    origin "text"
  ]
  node [
    id 3
    label "c&#243;rka"
    origin "text"
  ]
  node [
    id 4
    label "plan"
    origin "text"
  ]
  node [
    id 5
    label "wesprze&#263;"
    origin "text"
  ]
  node [
    id 6
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 7
    label "studiowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "prawo"
    origin "text"
  ]
  node [
    id 9
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 10
    label "czu&#263;"
  ]
  node [
    id 11
    label "need"
  ]
  node [
    id 12
    label "hide"
  ]
  node [
    id 13
    label "support"
  ]
  node [
    id 14
    label "pora_roku"
  ]
  node [
    id 15
    label "cz&#322;owiek"
  ]
  node [
    id 16
    label "dziewka"
  ]
  node [
    id 17
    label "potomkini"
  ]
  node [
    id 18
    label "dziewoja"
  ]
  node [
    id 19
    label "dziecko"
  ]
  node [
    id 20
    label "siksa"
  ]
  node [
    id 21
    label "dziewczynina"
  ]
  node [
    id 22
    label "dziunia"
  ]
  node [
    id 23
    label "dziewcz&#281;"
  ]
  node [
    id 24
    label "kora"
  ]
  node [
    id 25
    label "m&#322;&#243;dka"
  ]
  node [
    id 26
    label "dziecina"
  ]
  node [
    id 27
    label "sikorka"
  ]
  node [
    id 28
    label "device"
  ]
  node [
    id 29
    label "model"
  ]
  node [
    id 30
    label "wytw&#243;r"
  ]
  node [
    id 31
    label "obraz"
  ]
  node [
    id 32
    label "przestrze&#324;"
  ]
  node [
    id 33
    label "dekoracja"
  ]
  node [
    id 34
    label "intencja"
  ]
  node [
    id 35
    label "agreement"
  ]
  node [
    id 36
    label "pomys&#322;"
  ]
  node [
    id 37
    label "punkt"
  ]
  node [
    id 38
    label "miejsce_pracy"
  ]
  node [
    id 39
    label "perspektywa"
  ]
  node [
    id 40
    label "rysunek"
  ]
  node [
    id 41
    label "reprezentacja"
  ]
  node [
    id 42
    label "oprze&#263;"
  ]
  node [
    id 43
    label "help"
  ]
  node [
    id 44
    label "lean"
  ]
  node [
    id 45
    label "u&#322;atwi&#263;"
  ]
  node [
    id 46
    label "pocieszy&#263;"
  ]
  node [
    id 47
    label "pan_domu"
  ]
  node [
    id 48
    label "ch&#322;op"
  ]
  node [
    id 49
    label "ma&#322;&#380;onek"
  ]
  node [
    id 50
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 51
    label "stary"
  ]
  node [
    id 52
    label "&#347;lubny"
  ]
  node [
    id 53
    label "m&#243;j"
  ]
  node [
    id 54
    label "pan_i_w&#322;adca"
  ]
  node [
    id 55
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 56
    label "pan_m&#322;ody"
  ]
  node [
    id 57
    label "by&#263;"
  ]
  node [
    id 58
    label "album"
  ]
  node [
    id 59
    label "ogl&#261;da&#263;"
  ]
  node [
    id 60
    label "kszta&#322;ci&#263;_si&#281;"
  ]
  node [
    id 61
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 62
    label "read"
  ]
  node [
    id 63
    label "obserwacja"
  ]
  node [
    id 64
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 65
    label "nauka_prawa"
  ]
  node [
    id 66
    label "dominion"
  ]
  node [
    id 67
    label "normatywizm"
  ]
  node [
    id 68
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 69
    label "qualification"
  ]
  node [
    id 70
    label "opis"
  ]
  node [
    id 71
    label "regu&#322;a_Allena"
  ]
  node [
    id 72
    label "normalizacja"
  ]
  node [
    id 73
    label "kazuistyka"
  ]
  node [
    id 74
    label "regu&#322;a_Glogera"
  ]
  node [
    id 75
    label "kultura_duchowa"
  ]
  node [
    id 76
    label "prawo_karne"
  ]
  node [
    id 77
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 78
    label "standard"
  ]
  node [
    id 79
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 80
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 81
    label "struktura"
  ]
  node [
    id 82
    label "szko&#322;a"
  ]
  node [
    id 83
    label "prawo_karne_procesowe"
  ]
  node [
    id 84
    label "prawo_Mendla"
  ]
  node [
    id 85
    label "przepis"
  ]
  node [
    id 86
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 87
    label "criterion"
  ]
  node [
    id 88
    label "kanonistyka"
  ]
  node [
    id 89
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 90
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 91
    label "wykonawczy"
  ]
  node [
    id 92
    label "twierdzenie"
  ]
  node [
    id 93
    label "judykatura"
  ]
  node [
    id 94
    label "legislacyjnie"
  ]
  node [
    id 95
    label "umocowa&#263;"
  ]
  node [
    id 96
    label "podmiot"
  ]
  node [
    id 97
    label "procesualistyka"
  ]
  node [
    id 98
    label "kierunek"
  ]
  node [
    id 99
    label "kryminologia"
  ]
  node [
    id 100
    label "kryminalistyka"
  ]
  node [
    id 101
    label "cywilistyka"
  ]
  node [
    id 102
    label "law"
  ]
  node [
    id 103
    label "zasada_d'Alemberta"
  ]
  node [
    id 104
    label "jurisprudence"
  ]
  node [
    id 105
    label "zasada"
  ]
  node [
    id 106
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 107
    label "ga&#322;&#261;&#378;_prawa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
]
