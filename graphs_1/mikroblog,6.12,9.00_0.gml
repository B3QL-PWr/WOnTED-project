graph [
  maxDegree 23
  minDegree 1
  meanDegree 2
  density 0.025974025974025976
  graphCliqueNumber 2
  node [
    id 0
    label "dobra"
    origin "text"
  ]
  node [
    id 1
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 2
    label "dziewcz&#281;"
    origin "text"
  ]
  node [
    id 3
    label "racja"
    origin "text"
  ]
  node [
    id 4
    label "zaraz"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "miko&#322;ajek"
    origin "text"
  ]
  node [
    id 7
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 9
    label "rozdajo"
    origin "text"
  ]
  node [
    id 10
    label "frymark"
  ]
  node [
    id 11
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 12
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 13
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 14
    label "commodity"
  ]
  node [
    id 15
    label "mienie"
  ]
  node [
    id 16
    label "Wilko"
  ]
  node [
    id 17
    label "jednostka_monetarna"
  ]
  node [
    id 18
    label "centym"
  ]
  node [
    id 19
    label "cz&#322;owiek"
  ]
  node [
    id 20
    label "pomocnik"
  ]
  node [
    id 21
    label "g&#243;wniarz"
  ]
  node [
    id 22
    label "&#347;l&#261;ski"
  ]
  node [
    id 23
    label "m&#322;odzieniec"
  ]
  node [
    id 24
    label "kajtek"
  ]
  node [
    id 25
    label "kawaler"
  ]
  node [
    id 26
    label "usynawianie"
  ]
  node [
    id 27
    label "dziecko"
  ]
  node [
    id 28
    label "okrzos"
  ]
  node [
    id 29
    label "usynowienie"
  ]
  node [
    id 30
    label "sympatia"
  ]
  node [
    id 31
    label "pederasta"
  ]
  node [
    id 32
    label "synek"
  ]
  node [
    id 33
    label "boyfriend"
  ]
  node [
    id 34
    label "dziewczyna"
  ]
  node [
    id 35
    label "s&#261;d"
  ]
  node [
    id 36
    label "argument"
  ]
  node [
    id 37
    label "przyczyna"
  ]
  node [
    id 38
    label "porcja"
  ]
  node [
    id 39
    label "prawda"
  ]
  node [
    id 40
    label "blisko"
  ]
  node [
    id 41
    label "zara"
  ]
  node [
    id 42
    label "nied&#322;ugo"
  ]
  node [
    id 43
    label "si&#281;ga&#263;"
  ]
  node [
    id 44
    label "trwa&#263;"
  ]
  node [
    id 45
    label "obecno&#347;&#263;"
  ]
  node [
    id 46
    label "stan"
  ]
  node [
    id 47
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 48
    label "stand"
  ]
  node [
    id 49
    label "mie&#263;_miejsce"
  ]
  node [
    id 50
    label "uczestniczy&#263;"
  ]
  node [
    id 51
    label "chodzi&#263;"
  ]
  node [
    id 52
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 53
    label "equal"
  ]
  node [
    id 54
    label "bylina"
  ]
  node [
    id 55
    label "selerowate"
  ]
  node [
    id 56
    label "tentegowa&#263;"
  ]
  node [
    id 57
    label "urz&#261;dza&#263;"
  ]
  node [
    id 58
    label "give"
  ]
  node [
    id 59
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 60
    label "czyni&#263;"
  ]
  node [
    id 61
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 62
    label "post&#281;powa&#263;"
  ]
  node [
    id 63
    label "wydala&#263;"
  ]
  node [
    id 64
    label "oszukiwa&#263;"
  ]
  node [
    id 65
    label "organizowa&#263;"
  ]
  node [
    id 66
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 67
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 68
    label "work"
  ]
  node [
    id 69
    label "przerabia&#263;"
  ]
  node [
    id 70
    label "stylizowa&#263;"
  ]
  node [
    id 71
    label "falowa&#263;"
  ]
  node [
    id 72
    label "act"
  ]
  node [
    id 73
    label "peddle"
  ]
  node [
    id 74
    label "ukazywa&#263;"
  ]
  node [
    id 75
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 76
    label "praca"
  ]
  node [
    id 77
    label "dziewczynka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 34
  ]
]
