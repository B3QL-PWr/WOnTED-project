graph [
  maxDegree 44
  minDegree 1
  meanDegree 2
  density 0.015873015873015872
  graphCliqueNumber 2
  node [
    id 0
    label "suwa&#322;ki"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "organ"
    origin "text"
  ]
  node [
    id 4
    label "prowadz&#261;ca"
    origin "text"
  ]
  node [
    id 5
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 6
    label "jednostka"
    origin "text"
  ]
  node [
    id 7
    label "kultura"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;ga&#263;"
  ]
  node [
    id 9
    label "trwa&#263;"
  ]
  node [
    id 10
    label "obecno&#347;&#263;"
  ]
  node [
    id 11
    label "stan"
  ]
  node [
    id 12
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "stand"
  ]
  node [
    id 14
    label "mie&#263;_miejsce"
  ]
  node [
    id 15
    label "uczestniczy&#263;"
  ]
  node [
    id 16
    label "chodzi&#263;"
  ]
  node [
    id 17
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 18
    label "equal"
  ]
  node [
    id 19
    label "stulecie"
  ]
  node [
    id 20
    label "kalendarz"
  ]
  node [
    id 21
    label "czas"
  ]
  node [
    id 22
    label "pora_roku"
  ]
  node [
    id 23
    label "cykl_astronomiczny"
  ]
  node [
    id 24
    label "p&#243;&#322;rocze"
  ]
  node [
    id 25
    label "grupa"
  ]
  node [
    id 26
    label "kwarta&#322;"
  ]
  node [
    id 27
    label "kurs"
  ]
  node [
    id 28
    label "jubileusz"
  ]
  node [
    id 29
    label "miesi&#261;c"
  ]
  node [
    id 30
    label "lata"
  ]
  node [
    id 31
    label "martwy_sezon"
  ]
  node [
    id 32
    label "uk&#322;ad"
  ]
  node [
    id 33
    label "organogeneza"
  ]
  node [
    id 34
    label "Komitet_Region&#243;w"
  ]
  node [
    id 35
    label "Izba_Konsyliarska"
  ]
  node [
    id 36
    label "budowa"
  ]
  node [
    id 37
    label "okolica"
  ]
  node [
    id 38
    label "zesp&#243;&#322;"
  ]
  node [
    id 39
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 40
    label "jednostka_organizacyjna"
  ]
  node [
    id 41
    label "dekortykacja"
  ]
  node [
    id 42
    label "struktura_anatomiczna"
  ]
  node [
    id 43
    label "tkanka"
  ]
  node [
    id 44
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 45
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 46
    label "stomia"
  ]
  node [
    id 47
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 48
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 49
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 50
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 51
    label "tw&#243;r"
  ]
  node [
    id 52
    label "okre&#347;lony"
  ]
  node [
    id 53
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 54
    label "infimum"
  ]
  node [
    id 55
    label "ewoluowanie"
  ]
  node [
    id 56
    label "przyswoi&#263;"
  ]
  node [
    id 57
    label "reakcja"
  ]
  node [
    id 58
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 59
    label "wyewoluowanie"
  ]
  node [
    id 60
    label "individual"
  ]
  node [
    id 61
    label "profanum"
  ]
  node [
    id 62
    label "starzenie_si&#281;"
  ]
  node [
    id 63
    label "homo_sapiens"
  ]
  node [
    id 64
    label "skala"
  ]
  node [
    id 65
    label "supremum"
  ]
  node [
    id 66
    label "przyswaja&#263;"
  ]
  node [
    id 67
    label "ludzko&#347;&#263;"
  ]
  node [
    id 68
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 69
    label "one"
  ]
  node [
    id 70
    label "funkcja"
  ]
  node [
    id 71
    label "przeliczenie"
  ]
  node [
    id 72
    label "przeliczanie"
  ]
  node [
    id 73
    label "mikrokosmos"
  ]
  node [
    id 74
    label "rzut"
  ]
  node [
    id 75
    label "portrecista"
  ]
  node [
    id 76
    label "przelicza&#263;"
  ]
  node [
    id 77
    label "przyswajanie"
  ]
  node [
    id 78
    label "duch"
  ]
  node [
    id 79
    label "wyewoluowa&#263;"
  ]
  node [
    id 80
    label "ewoluowa&#263;"
  ]
  node [
    id 81
    label "oddzia&#322;ywanie"
  ]
  node [
    id 82
    label "g&#322;owa"
  ]
  node [
    id 83
    label "liczba_naturalna"
  ]
  node [
    id 84
    label "poj&#281;cie"
  ]
  node [
    id 85
    label "osoba"
  ]
  node [
    id 86
    label "figura"
  ]
  node [
    id 87
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 88
    label "obiekt"
  ]
  node [
    id 89
    label "matematyka"
  ]
  node [
    id 90
    label "przyswojenie"
  ]
  node [
    id 91
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 92
    label "czynnik_biotyczny"
  ]
  node [
    id 93
    label "przeliczy&#263;"
  ]
  node [
    id 94
    label "antropochoria"
  ]
  node [
    id 95
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 96
    label "przedmiot"
  ]
  node [
    id 97
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 98
    label "Wsch&#243;d"
  ]
  node [
    id 99
    label "rzecz"
  ]
  node [
    id 100
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 101
    label "sztuka"
  ]
  node [
    id 102
    label "religia"
  ]
  node [
    id 103
    label "przejmowa&#263;"
  ]
  node [
    id 104
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 105
    label "makrokosmos"
  ]
  node [
    id 106
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 107
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 108
    label "zjawisko"
  ]
  node [
    id 109
    label "praca_rolnicza"
  ]
  node [
    id 110
    label "tradycja"
  ]
  node [
    id 111
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 112
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 113
    label "przejmowanie"
  ]
  node [
    id 114
    label "cecha"
  ]
  node [
    id 115
    label "asymilowanie_si&#281;"
  ]
  node [
    id 116
    label "przej&#261;&#263;"
  ]
  node [
    id 117
    label "hodowla"
  ]
  node [
    id 118
    label "brzoskwiniarnia"
  ]
  node [
    id 119
    label "populace"
  ]
  node [
    id 120
    label "konwencja"
  ]
  node [
    id 121
    label "propriety"
  ]
  node [
    id 122
    label "jako&#347;&#263;"
  ]
  node [
    id 123
    label "kuchnia"
  ]
  node [
    id 124
    label "zwyczaj"
  ]
  node [
    id 125
    label "przej&#281;cie"
  ]
  node [
    id 126
    label "ro&#347;linno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
]
