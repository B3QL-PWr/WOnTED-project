graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.1503759398496243
  density 0.016290726817042606
  graphCliqueNumber 3
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "m&#322;ode"
    origin "text"
  ]
  node [
    id 2
    label "tw&#243;rca"
    origin "text"
  ]
  node [
    id 3
    label "czytelnia"
    origin "text"
  ]
  node [
    id 4
    label "miejski"
    origin "text"
  ]
  node [
    id 5
    label "biblioteka"
    origin "text"
  ]
  node [
    id 6
    label "publiczny"
    origin "text"
  ]
  node [
    id 7
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 8
    label "przy"
    origin "text"
  ]
  node [
    id 9
    label "ula"
    origin "text"
  ]
  node [
    id 10
    label "listopadowy"
    origin "text"
  ]
  node [
    id 11
    label "&#347;roda"
    origin "text"
  ]
  node [
    id 12
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 13
    label "rocznik"
    origin "text"
  ]
  node [
    id 14
    label "godz"
    origin "text"
  ]
  node [
    id 15
    label "by&#263;"
    origin "text"
  ]
  node [
    id 16
    label "rozmawia&#263;"
    origin "text"
  ]
  node [
    id 17
    label "wiersz"
    origin "text"
  ]
  node [
    id 18
    label "chwali&#263;"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 21
    label "dorobek"
    origin "text"
  ]
  node [
    id 22
    label "poetycki"
    origin "text"
  ]
  node [
    id 23
    label "dyskutowa&#263;"
    origin "text"
  ]
  node [
    id 24
    label "tym"
    origin "text"
  ]
  node [
    id 25
    label "podoba"
    origin "text"
  ]
  node [
    id 26
    label "przed"
    origin "text"
  ]
  node [
    id 27
    label "wszyscy"
    origin "text"
  ]
  node [
    id 28
    label "dobrze"
    origin "text"
  ]
  node [
    id 29
    label "bawi&#263;"
    origin "text"
  ]
  node [
    id 30
    label "invite"
  ]
  node [
    id 31
    label "ask"
  ]
  node [
    id 32
    label "oferowa&#263;"
  ]
  node [
    id 33
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 34
    label "potomstwo"
  ]
  node [
    id 35
    label "organizm"
  ]
  node [
    id 36
    label "m&#322;odziak"
  ]
  node [
    id 37
    label "zwierz&#281;"
  ]
  node [
    id 38
    label "fledgling"
  ]
  node [
    id 39
    label "pomys&#322;odawca"
  ]
  node [
    id 40
    label "kszta&#322;ciciel"
  ]
  node [
    id 41
    label "tworzyciel"
  ]
  node [
    id 42
    label "&#347;w"
  ]
  node [
    id 43
    label "wykonawca"
  ]
  node [
    id 44
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 45
    label "pomieszczenie"
  ]
  node [
    id 46
    label "informatorium"
  ]
  node [
    id 47
    label "miejsko"
  ]
  node [
    id 48
    label "miastowy"
  ]
  node [
    id 49
    label "typowy"
  ]
  node [
    id 50
    label "pok&#243;j"
  ]
  node [
    id 51
    label "rewers"
  ]
  node [
    id 52
    label "kolekcja"
  ]
  node [
    id 53
    label "czytelnik"
  ]
  node [
    id 54
    label "budynek"
  ]
  node [
    id 55
    label "zbi&#243;r"
  ]
  node [
    id 56
    label "programowanie"
  ]
  node [
    id 57
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 58
    label "library"
  ]
  node [
    id 59
    label "instytucja"
  ]
  node [
    id 60
    label "jawny"
  ]
  node [
    id 61
    label "upublicznienie"
  ]
  node [
    id 62
    label "upublicznianie"
  ]
  node [
    id 63
    label "publicznie"
  ]
  node [
    id 64
    label "Popielec"
  ]
  node [
    id 65
    label "dzie&#324;_powszedni"
  ]
  node [
    id 66
    label "tydzie&#324;"
  ]
  node [
    id 67
    label "Nowy_Rok"
  ]
  node [
    id 68
    label "miesi&#261;c"
  ]
  node [
    id 69
    label "formacja"
  ]
  node [
    id 70
    label "kronika"
  ]
  node [
    id 71
    label "czasopismo"
  ]
  node [
    id 72
    label "yearbook"
  ]
  node [
    id 73
    label "si&#281;ga&#263;"
  ]
  node [
    id 74
    label "trwa&#263;"
  ]
  node [
    id 75
    label "obecno&#347;&#263;"
  ]
  node [
    id 76
    label "stan"
  ]
  node [
    id 77
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 78
    label "stand"
  ]
  node [
    id 79
    label "mie&#263;_miejsce"
  ]
  node [
    id 80
    label "uczestniczy&#263;"
  ]
  node [
    id 81
    label "chodzi&#263;"
  ]
  node [
    id 82
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 83
    label "equal"
  ]
  node [
    id 84
    label "talk"
  ]
  node [
    id 85
    label "gaworzy&#263;"
  ]
  node [
    id 86
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 87
    label "refren"
  ]
  node [
    id 88
    label "wypowied&#378;"
  ]
  node [
    id 89
    label "figura_stylistyczna"
  ]
  node [
    id 90
    label "podmiot_liryczny"
  ]
  node [
    id 91
    label "zwrotka"
  ]
  node [
    id 92
    label "dzie&#322;o_poetyckie"
  ]
  node [
    id 93
    label "cezura"
  ]
  node [
    id 94
    label "metr"
  ]
  node [
    id 95
    label "strofoida"
  ]
  node [
    id 96
    label "tekst"
  ]
  node [
    id 97
    label "fragment"
  ]
  node [
    id 98
    label "wys&#322;awia&#263;"
  ]
  node [
    id 99
    label "wywy&#380;sza&#263;"
  ]
  node [
    id 100
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 101
    label "bless"
  ]
  node [
    id 102
    label "patrze&#263;_jak_w_&#347;wi&#281;ty_obraz"
  ]
  node [
    id 103
    label "nagradza&#263;"
  ]
  node [
    id 104
    label "wielmo&#380;y&#263;"
  ]
  node [
    id 105
    label "glorify"
  ]
  node [
    id 106
    label "konto"
  ]
  node [
    id 107
    label "wypracowa&#263;"
  ]
  node [
    id 108
    label "mienie"
  ]
  node [
    id 109
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 110
    label "rymotw&#243;rczy"
  ]
  node [
    id 111
    label "poetycko"
  ]
  node [
    id 112
    label "literacki"
  ]
  node [
    id 113
    label "argue"
  ]
  node [
    id 114
    label "moralnie"
  ]
  node [
    id 115
    label "wiele"
  ]
  node [
    id 116
    label "lepiej"
  ]
  node [
    id 117
    label "korzystnie"
  ]
  node [
    id 118
    label "pomy&#347;lnie"
  ]
  node [
    id 119
    label "pozytywnie"
  ]
  node [
    id 120
    label "dobry"
  ]
  node [
    id 121
    label "dobroczynnie"
  ]
  node [
    id 122
    label "odpowiednio"
  ]
  node [
    id 123
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 124
    label "skutecznie"
  ]
  node [
    id 125
    label "ubawia&#263;"
  ]
  node [
    id 126
    label "amuse"
  ]
  node [
    id 127
    label "zajmowa&#263;"
  ]
  node [
    id 128
    label "wzbudza&#263;"
  ]
  node [
    id 129
    label "przebywa&#263;"
  ]
  node [
    id 130
    label "sprawia&#263;"
  ]
  node [
    id 131
    label "zabawia&#263;"
  ]
  node [
    id 132
    label "play"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 73
  ]
  edge [
    source 15
    target 74
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 15
    target 81
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 86
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 17
    target 91
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 17
    target 93
  ]
  edge [
    source 17
    target 94
  ]
  edge [
    source 17
    target 95
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 97
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 98
  ]
  edge [
    source 18
    target 99
  ]
  edge [
    source 18
    target 100
  ]
  edge [
    source 18
    target 101
  ]
  edge [
    source 18
    target 102
  ]
  edge [
    source 18
    target 103
  ]
  edge [
    source 18
    target 104
  ]
  edge [
    source 18
    target 105
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 106
  ]
  edge [
    source 21
    target 107
  ]
  edge [
    source 21
    target 108
  ]
  edge [
    source 21
    target 109
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 22
    target 110
  ]
  edge [
    source 22
    target 111
  ]
  edge [
    source 22
    target 112
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 113
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 114
  ]
  edge [
    source 28
    target 115
  ]
  edge [
    source 28
    target 116
  ]
  edge [
    source 28
    target 117
  ]
  edge [
    source 28
    target 118
  ]
  edge [
    source 28
    target 119
  ]
  edge [
    source 28
    target 120
  ]
  edge [
    source 28
    target 121
  ]
  edge [
    source 28
    target 122
  ]
  edge [
    source 28
    target 123
  ]
  edge [
    source 28
    target 124
  ]
  edge [
    source 29
    target 125
  ]
  edge [
    source 29
    target 126
  ]
  edge [
    source 29
    target 127
  ]
  edge [
    source 29
    target 128
  ]
  edge [
    source 29
    target 129
  ]
  edge [
    source 29
    target 130
  ]
  edge [
    source 29
    target 131
  ]
  edge [
    source 29
    target 132
  ]
]
