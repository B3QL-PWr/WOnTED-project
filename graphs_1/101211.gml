graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.3333333333333335
  density 0.08045977011494253
  graphCliqueNumber 5
  node [
    id 0
    label "rekord"
    origin "text"
  ]
  node [
    id 1
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 2
    label "ameryka"
    origin "text"
  ]
  node [
    id 3
    label "po&#322;udniowy"
    origin "text"
  ]
  node [
    id 4
    label "lekkoatletyka"
    origin "text"
  ]
  node [
    id 5
    label "record"
  ]
  node [
    id 6
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 7
    label "baza_danych"
  ]
  node [
    id 8
    label "dane"
  ]
  node [
    id 9
    label "szczyt"
  ]
  node [
    id 10
    label "championship"
  ]
  node [
    id 11
    label "Formu&#322;a_1"
  ]
  node [
    id 12
    label "zawody"
  ]
  node [
    id 13
    label "gor&#261;cy"
  ]
  node [
    id 14
    label "s&#322;oneczny"
  ]
  node [
    id 15
    label "po&#322;udniowo"
  ]
  node [
    id 16
    label "sport"
  ]
  node [
    id 17
    label "tr&#243;jskok"
  ]
  node [
    id 18
    label "skok_o_tyczce"
  ]
  node [
    id 19
    label "dziesi&#281;ciob&#243;j"
  ]
  node [
    id 20
    label "skok_wzwy&#380;"
  ]
  node [
    id 21
    label "rzut_oszczepem"
  ]
  node [
    id 22
    label "bieg"
  ]
  node [
    id 23
    label "skok_w_dal"
  ]
  node [
    id 24
    label "ch&#243;d"
  ]
  node [
    id 25
    label "pchni&#281;cie_kul&#261;"
  ]
  node [
    id 26
    label "rzut_m&#322;otem"
  ]
  node [
    id 27
    label "ameryk"
  ]
  node [
    id 28
    label "mistrzostwo"
  ]
  node [
    id 29
    label "wyspa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 28
    target 29
  ]
]
