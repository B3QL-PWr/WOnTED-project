graph [
  maxDegree 8
  minDegree 1
  meanDegree 3.4
  density 0.37777777777777777
  graphCliqueNumber 5
  node [
    id 0
    label "adriaan"
    origin "text"
  ]
  node [
    id 1
    label "van"
    origin "text"
  ]
  node [
    id 2
    label "dera"
    origin "text"
  ]
  node [
    id 3
    label "hoop"
    origin "text"
  ]
  node [
    id 4
    label "samoch&#243;d"
  ]
  node [
    id 5
    label "nadwozie"
  ]
  node [
    id 6
    label "Adriaan"
  ]
  node [
    id 7
    label "Hoop"
  ]
  node [
    id 8
    label "Joan"
  ]
  node [
    id 9
    label "Cornelis"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
]
