graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.945945945945946
  density 0.05405405405405406
  graphCliqueNumber 2
  node [
    id 0
    label "pyta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "tutaj"
    origin "text"
  ]
  node [
    id 2
    label "elektroda"
    origin "text"
  ]
  node [
    id 3
    label "pewnie"
    origin "text"
  ]
  node [
    id 4
    label "zbanowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "inspect"
  ]
  node [
    id 7
    label "przes&#322;uchiwa&#263;"
  ]
  node [
    id 8
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 9
    label "ankieter"
  ]
  node [
    id 10
    label "sprawdza&#263;"
  ]
  node [
    id 11
    label "question"
  ]
  node [
    id 12
    label "tam"
  ]
  node [
    id 13
    label "ogniwo_galwaniczne"
  ]
  node [
    id 14
    label "elektrolizer"
  ]
  node [
    id 15
    label "electrode"
  ]
  node [
    id 16
    label "przedmiot"
  ]
  node [
    id 17
    label "zwinnie"
  ]
  node [
    id 18
    label "bezpiecznie"
  ]
  node [
    id 19
    label "wiarygodnie"
  ]
  node [
    id 20
    label "pewniej"
  ]
  node [
    id 21
    label "pewny"
  ]
  node [
    id 22
    label "mocno"
  ]
  node [
    id 23
    label "najpewniej"
  ]
  node [
    id 24
    label "stulecie"
  ]
  node [
    id 25
    label "kalendarz"
  ]
  node [
    id 26
    label "czas"
  ]
  node [
    id 27
    label "pora_roku"
  ]
  node [
    id 28
    label "cykl_astronomiczny"
  ]
  node [
    id 29
    label "p&#243;&#322;rocze"
  ]
  node [
    id 30
    label "grupa"
  ]
  node [
    id 31
    label "kwarta&#322;"
  ]
  node [
    id 32
    label "kurs"
  ]
  node [
    id 33
    label "jubileusz"
  ]
  node [
    id 34
    label "miesi&#261;c"
  ]
  node [
    id 35
    label "lata"
  ]
  node [
    id 36
    label "martwy_sezon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
]
