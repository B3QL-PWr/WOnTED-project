graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.01980198019802
  density 0.010048766070636914
  graphCliqueNumber 3
  node [
    id 0
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 1
    label "serwis"
    origin "text"
  ]
  node [
    id 2
    label "spo&#322;eczno&#347;ciowy"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 4
    label "przyzna&#263;"
    origin "text"
  ]
  node [
    id 5
    label "taki"
    origin "text"
  ]
  node [
    id 6
    label "firma"
    origin "text"
  ]
  node [
    id 7
    label "airbnb"
    origin "text"
  ]
  node [
    id 8
    label "netflix"
    origin "text"
  ]
  node [
    id 9
    label "lyft"
    origin "text"
  ]
  node [
    id 10
    label "uprzywilejowany"
    origin "text"
  ]
  node [
    id 11
    label "dost&#281;p"
    origin "text"
  ]
  node [
    id 12
    label "dana"
    origin "text"
  ]
  node [
    id 13
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 14
    label "wewn&#281;trzny"
    origin "text"
  ]
  node [
    id 15
    label "mail"
    origin "text"
  ]
  node [
    id 16
    label "facebook"
    origin "text"
  ]
  node [
    id 17
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 19
    label "przez"
    origin "text"
  ]
  node [
    id 20
    label "brytyjski"
    origin "text"
  ]
  node [
    id 21
    label "parlament"
    origin "text"
  ]
  node [
    id 22
    label "doros&#322;y"
  ]
  node [
    id 23
    label "wiele"
  ]
  node [
    id 24
    label "dorodny"
  ]
  node [
    id 25
    label "znaczny"
  ]
  node [
    id 26
    label "du&#380;o"
  ]
  node [
    id 27
    label "prawdziwy"
  ]
  node [
    id 28
    label "niema&#322;o"
  ]
  node [
    id 29
    label "wa&#380;ny"
  ]
  node [
    id 30
    label "rozwini&#281;ty"
  ]
  node [
    id 31
    label "mecz"
  ]
  node [
    id 32
    label "service"
  ]
  node [
    id 33
    label "wytw&#243;r"
  ]
  node [
    id 34
    label "zak&#322;ad"
  ]
  node [
    id 35
    label "us&#322;uga"
  ]
  node [
    id 36
    label "uderzenie"
  ]
  node [
    id 37
    label "doniesienie"
  ]
  node [
    id 38
    label "zastawa"
  ]
  node [
    id 39
    label "YouTube"
  ]
  node [
    id 40
    label "punkt"
  ]
  node [
    id 41
    label "porcja"
  ]
  node [
    id 42
    label "strona"
  ]
  node [
    id 43
    label "medialny"
  ]
  node [
    id 44
    label "publiczny"
  ]
  node [
    id 45
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 46
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 47
    label "obszar"
  ]
  node [
    id 48
    label "obiekt_naturalny"
  ]
  node [
    id 49
    label "przedmiot"
  ]
  node [
    id 50
    label "biosfera"
  ]
  node [
    id 51
    label "grupa"
  ]
  node [
    id 52
    label "stw&#243;r"
  ]
  node [
    id 53
    label "Stary_&#346;wiat"
  ]
  node [
    id 54
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 55
    label "rzecz"
  ]
  node [
    id 56
    label "magnetosfera"
  ]
  node [
    id 57
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 58
    label "environment"
  ]
  node [
    id 59
    label "Nowy_&#346;wiat"
  ]
  node [
    id 60
    label "geosfera"
  ]
  node [
    id 61
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 62
    label "planeta"
  ]
  node [
    id 63
    label "przejmowa&#263;"
  ]
  node [
    id 64
    label "litosfera"
  ]
  node [
    id 65
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 66
    label "makrokosmos"
  ]
  node [
    id 67
    label "barysfera"
  ]
  node [
    id 68
    label "biota"
  ]
  node [
    id 69
    label "p&#243;&#322;noc"
  ]
  node [
    id 70
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 71
    label "fauna"
  ]
  node [
    id 72
    label "wszechstworzenie"
  ]
  node [
    id 73
    label "geotermia"
  ]
  node [
    id 74
    label "biegun"
  ]
  node [
    id 75
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 76
    label "ekosystem"
  ]
  node [
    id 77
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 78
    label "teren"
  ]
  node [
    id 79
    label "zjawisko"
  ]
  node [
    id 80
    label "p&#243;&#322;kula"
  ]
  node [
    id 81
    label "atmosfera"
  ]
  node [
    id 82
    label "mikrokosmos"
  ]
  node [
    id 83
    label "class"
  ]
  node [
    id 84
    label "po&#322;udnie"
  ]
  node [
    id 85
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 86
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 87
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 88
    label "przejmowanie"
  ]
  node [
    id 89
    label "przestrze&#324;"
  ]
  node [
    id 90
    label "asymilowanie_si&#281;"
  ]
  node [
    id 91
    label "przej&#261;&#263;"
  ]
  node [
    id 92
    label "ekosfera"
  ]
  node [
    id 93
    label "przyroda"
  ]
  node [
    id 94
    label "ciemna_materia"
  ]
  node [
    id 95
    label "geoida"
  ]
  node [
    id 96
    label "Wsch&#243;d"
  ]
  node [
    id 97
    label "populace"
  ]
  node [
    id 98
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 99
    label "huczek"
  ]
  node [
    id 100
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 101
    label "Ziemia"
  ]
  node [
    id 102
    label "universe"
  ]
  node [
    id 103
    label "ozonosfera"
  ]
  node [
    id 104
    label "rze&#378;ba"
  ]
  node [
    id 105
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 106
    label "zagranica"
  ]
  node [
    id 107
    label "hydrosfera"
  ]
  node [
    id 108
    label "woda"
  ]
  node [
    id 109
    label "kuchnia"
  ]
  node [
    id 110
    label "przej&#281;cie"
  ]
  node [
    id 111
    label "czarna_dziura"
  ]
  node [
    id 112
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 113
    label "morze"
  ]
  node [
    id 114
    label "da&#263;"
  ]
  node [
    id 115
    label "nada&#263;"
  ]
  node [
    id 116
    label "pozwoli&#263;"
  ]
  node [
    id 117
    label "give"
  ]
  node [
    id 118
    label "stwierdzi&#263;"
  ]
  node [
    id 119
    label "okre&#347;lony"
  ]
  node [
    id 120
    label "jaki&#347;"
  ]
  node [
    id 121
    label "cz&#322;owiek"
  ]
  node [
    id 122
    label "Hortex"
  ]
  node [
    id 123
    label "MAC"
  ]
  node [
    id 124
    label "reengineering"
  ]
  node [
    id 125
    label "nazwa_w&#322;asna"
  ]
  node [
    id 126
    label "podmiot_gospodarczy"
  ]
  node [
    id 127
    label "Google"
  ]
  node [
    id 128
    label "zaufanie"
  ]
  node [
    id 129
    label "biurowiec"
  ]
  node [
    id 130
    label "networking"
  ]
  node [
    id 131
    label "zasoby_ludzkie"
  ]
  node [
    id 132
    label "interes"
  ]
  node [
    id 133
    label "paczkarnia"
  ]
  node [
    id 134
    label "Canon"
  ]
  node [
    id 135
    label "HP"
  ]
  node [
    id 136
    label "Baltona"
  ]
  node [
    id 137
    label "Pewex"
  ]
  node [
    id 138
    label "MAN_SE"
  ]
  node [
    id 139
    label "Apeks"
  ]
  node [
    id 140
    label "zasoby"
  ]
  node [
    id 141
    label "Orbis"
  ]
  node [
    id 142
    label "miejsce_pracy"
  ]
  node [
    id 143
    label "siedziba"
  ]
  node [
    id 144
    label "Spo&#322;em"
  ]
  node [
    id 145
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 146
    label "Orlen"
  ]
  node [
    id 147
    label "klasa"
  ]
  node [
    id 148
    label "szczeg&#243;lny"
  ]
  node [
    id 149
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 150
    label "lekki"
  ]
  node [
    id 151
    label "miejsce"
  ]
  node [
    id 152
    label "konto"
  ]
  node [
    id 153
    label "informatyka"
  ]
  node [
    id 154
    label "has&#322;o"
  ]
  node [
    id 155
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 156
    label "operacja"
  ]
  node [
    id 157
    label "dar"
  ]
  node [
    id 158
    label "cnota"
  ]
  node [
    id 159
    label "buddyzm"
  ]
  node [
    id 160
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 161
    label "rise"
  ]
  node [
    id 162
    label "appear"
  ]
  node [
    id 163
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 164
    label "wewn&#281;trznie"
  ]
  node [
    id 165
    label "wn&#281;trzny"
  ]
  node [
    id 166
    label "psychiczny"
  ]
  node [
    id 167
    label "numer"
  ]
  node [
    id 168
    label "oddzia&#322;"
  ]
  node [
    id 169
    label "us&#322;uga_internetowa"
  ]
  node [
    id 170
    label "identyfikator"
  ]
  node [
    id 171
    label "mailowanie"
  ]
  node [
    id 172
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 173
    label "skrzynka_mailowa"
  ]
  node [
    id 174
    label "skrzynka_odbiorcza"
  ]
  node [
    id 175
    label "poczta_elektroniczna"
  ]
  node [
    id 176
    label "mailowa&#263;"
  ]
  node [
    id 177
    label "wall"
  ]
  node [
    id 178
    label "upubliczni&#263;"
  ]
  node [
    id 179
    label "wydawnictwo"
  ]
  node [
    id 180
    label "wprowadzi&#263;"
  ]
  node [
    id 181
    label "picture"
  ]
  node [
    id 182
    label "doba"
  ]
  node [
    id 183
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 184
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 185
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 186
    label "angielsko"
  ]
  node [
    id 187
    label "po_brytyjsku"
  ]
  node [
    id 188
    label "europejski"
  ]
  node [
    id 189
    label "j&#281;zyk_celtycki"
  ]
  node [
    id 190
    label "j&#281;zyk_martwy"
  ]
  node [
    id 191
    label "j&#281;zyk_angielski"
  ]
  node [
    id 192
    label "zachodnioeuropejski"
  ]
  node [
    id 193
    label "morris"
  ]
  node [
    id 194
    label "angielski"
  ]
  node [
    id 195
    label "anglosaski"
  ]
  node [
    id 196
    label "brytyjsko"
  ]
  node [
    id 197
    label "plankton_polityczny"
  ]
  node [
    id 198
    label "ustawodawca"
  ]
  node [
    id 199
    label "urz&#261;d"
  ]
  node [
    id 200
    label "europarlament"
  ]
  node [
    id 201
    label "grupa_bilateralna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 21
    target 51
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 201
  ]
]
