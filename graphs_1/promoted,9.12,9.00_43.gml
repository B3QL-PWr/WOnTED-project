graph [
  maxDegree 21
  minDegree 1
  meanDegree 2.0210526315789474
  density 0.021500559910414333
  graphCliqueNumber 2
  node [
    id 0
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;osek"
    origin "text"
  ]
  node [
    id 2
    label "miesi&#281;cznik"
    origin "text"
  ]
  node [
    id 3
    label "katolicki"
    origin "text"
  ]
  node [
    id 4
    label "namawia&#263;"
    origin "text"
  ]
  node [
    id 5
    label "m&#322;oda"
    origin "text"
  ]
  node [
    id 6
    label "chrze&#347;cijanin"
    origin "text"
  ]
  node [
    id 7
    label "aby"
    origin "text"
  ]
  node [
    id 8
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 9
    label "wiara"
    origin "text"
  ]
  node [
    id 10
    label "oferowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "stypendium"
    origin "text"
  ]
  node [
    id 12
    label "tani"
    origin "text"
  ]
  node [
    id 13
    label "dobry"
    origin "text"
  ]
  node [
    id 14
    label "stabilny"
    origin "text"
  ]
  node [
    id 15
    label "tenis"
  ]
  node [
    id 16
    label "cover"
  ]
  node [
    id 17
    label "siatk&#243;wka"
  ]
  node [
    id 18
    label "dawa&#263;"
  ]
  node [
    id 19
    label "faszerowa&#263;"
  ]
  node [
    id 20
    label "informowa&#263;"
  ]
  node [
    id 21
    label "introduce"
  ]
  node [
    id 22
    label "jedzenie"
  ]
  node [
    id 23
    label "tender"
  ]
  node [
    id 24
    label "deal"
  ]
  node [
    id 25
    label "kelner"
  ]
  node [
    id 26
    label "serwowa&#263;"
  ]
  node [
    id 27
    label "rozgrywa&#263;"
  ]
  node [
    id 28
    label "stawia&#263;"
  ]
  node [
    id 29
    label "obiekt_naturalny"
  ]
  node [
    id 30
    label "w&#322;os"
  ]
  node [
    id 31
    label "Supe&#322;ek"
  ]
  node [
    id 32
    label "czasopismo"
  ]
  node [
    id 33
    label "pn&#261;cze"
  ]
  node [
    id 34
    label "miesi&#281;cznikowate"
  ]
  node [
    id 35
    label "po_katolicku"
  ]
  node [
    id 36
    label "zgodny"
  ]
  node [
    id 37
    label "chrze&#347;cija&#324;ski"
  ]
  node [
    id 38
    label "wyznaniowy"
  ]
  node [
    id 39
    label "powszechny"
  ]
  node [
    id 40
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 41
    label "typowy"
  ]
  node [
    id 42
    label "przekonywa&#263;"
  ]
  node [
    id 43
    label "prompt"
  ]
  node [
    id 44
    label "cz&#322;owiek"
  ]
  node [
    id 45
    label "kobieta"
  ]
  node [
    id 46
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 47
    label "&#380;ona"
  ]
  node [
    id 48
    label "suknia_&#347;lubna"
  ]
  node [
    id 49
    label "niezam&#281;&#380;na"
  ]
  node [
    id 50
    label "krze&#347;cijanin"
  ]
  node [
    id 51
    label "&#347;wiadek_Jehowy"
  ]
  node [
    id 52
    label "monoteista"
  ]
  node [
    id 53
    label "wyznawca"
  ]
  node [
    id 54
    label "troch&#281;"
  ]
  node [
    id 55
    label "bli&#378;ni"
  ]
  node [
    id 56
    label "odpowiedni"
  ]
  node [
    id 57
    label "swojak"
  ]
  node [
    id 58
    label "samodzielny"
  ]
  node [
    id 59
    label "przes&#261;dny"
  ]
  node [
    id 60
    label "konwikcja"
  ]
  node [
    id 61
    label "belief"
  ]
  node [
    id 62
    label "pogl&#261;d"
  ]
  node [
    id 63
    label "faith"
  ]
  node [
    id 64
    label "postawa"
  ]
  node [
    id 65
    label "volunteer"
  ]
  node [
    id 66
    label "zach&#281;ca&#263;"
  ]
  node [
    id 67
    label "&#347;wiadczenie"
  ]
  node [
    id 68
    label "niedrogo"
  ]
  node [
    id 69
    label "tanio"
  ]
  node [
    id 70
    label "tandetny"
  ]
  node [
    id 71
    label "pomy&#347;lny"
  ]
  node [
    id 72
    label "skuteczny"
  ]
  node [
    id 73
    label "moralny"
  ]
  node [
    id 74
    label "korzystny"
  ]
  node [
    id 75
    label "zwrot"
  ]
  node [
    id 76
    label "dobrze"
  ]
  node [
    id 77
    label "pozytywny"
  ]
  node [
    id 78
    label "grzeczny"
  ]
  node [
    id 79
    label "powitanie"
  ]
  node [
    id 80
    label "mi&#322;y"
  ]
  node [
    id 81
    label "dobroczynny"
  ]
  node [
    id 82
    label "pos&#322;uszny"
  ]
  node [
    id 83
    label "ca&#322;y"
  ]
  node [
    id 84
    label "czw&#243;rka"
  ]
  node [
    id 85
    label "spokojny"
  ]
  node [
    id 86
    label "&#347;mieszny"
  ]
  node [
    id 87
    label "drogi"
  ]
  node [
    id 88
    label "stabilnie"
  ]
  node [
    id 89
    label "pewny"
  ]
  node [
    id 90
    label "porz&#261;dny"
  ]
  node [
    id 91
    label "sta&#322;y"
  ]
  node [
    id 92
    label "trwa&#322;y"
  ]
  node [
    id 93
    label "Arabia"
  ]
  node [
    id 94
    label "saudyjski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 56
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 40
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 93
    target 94
  ]
]
