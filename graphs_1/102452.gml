graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.049689440993789
  density 0.01281055900621118
  graphCliqueNumber 2
  node [
    id 0
    label "artur"
    origin "text"
  ]
  node [
    id 1
    label "wyrzykowski"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "lata"
    origin "text"
  ]
  node [
    id 4
    label "studiowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "warszawski"
    origin "text"
  ]
  node [
    id 6
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 7
    label "filmowy"
    origin "text"
  ]
  node [
    id 8
    label "wsp&#243;&#322;tworzy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "tw&#243;rczy"
    origin "text"
  ]
  node [
    id 10
    label "stowarzyszenie"
    origin "text"
  ]
  node [
    id 11
    label "kompilacja"
    origin "text"
  ]
  node [
    id 12
    label "chcie&#263;by"
    origin "text"
  ]
  node [
    id 13
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "film"
    origin "text"
  ]
  node [
    id 15
    label "dyplomowy"
    origin "text"
  ]
  node [
    id 16
    label "nie"
    origin "text"
  ]
  node [
    id 17
    label "wystarczaj&#261;cy"
    origin "text"
  ]
  node [
    id 18
    label "fundusz"
    origin "text"
  ]
  node [
    id 19
    label "czyj&#347;"
  ]
  node [
    id 20
    label "m&#261;&#380;"
  ]
  node [
    id 21
    label "summer"
  ]
  node [
    id 22
    label "czas"
  ]
  node [
    id 23
    label "by&#263;"
  ]
  node [
    id 24
    label "album"
  ]
  node [
    id 25
    label "ogl&#261;da&#263;"
  ]
  node [
    id 26
    label "kszta&#322;ci&#263;_si&#281;"
  ]
  node [
    id 27
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 28
    label "read"
  ]
  node [
    id 29
    label "po_warszawsku"
  ]
  node [
    id 30
    label "marmuzela"
  ]
  node [
    id 31
    label "mazowiecki"
  ]
  node [
    id 32
    label "Mickiewicz"
  ]
  node [
    id 33
    label "szkolenie"
  ]
  node [
    id 34
    label "przepisa&#263;"
  ]
  node [
    id 35
    label "lesson"
  ]
  node [
    id 36
    label "grupa"
  ]
  node [
    id 37
    label "praktyka"
  ]
  node [
    id 38
    label "metoda"
  ]
  node [
    id 39
    label "niepokalanki"
  ]
  node [
    id 40
    label "kara"
  ]
  node [
    id 41
    label "zda&#263;"
  ]
  node [
    id 42
    label "form"
  ]
  node [
    id 43
    label "kwalifikacje"
  ]
  node [
    id 44
    label "system"
  ]
  node [
    id 45
    label "przepisanie"
  ]
  node [
    id 46
    label "sztuba"
  ]
  node [
    id 47
    label "wiedza"
  ]
  node [
    id 48
    label "stopek"
  ]
  node [
    id 49
    label "school"
  ]
  node [
    id 50
    label "absolwent"
  ]
  node [
    id 51
    label "urszulanki"
  ]
  node [
    id 52
    label "gabinet"
  ]
  node [
    id 53
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 54
    label "ideologia"
  ]
  node [
    id 55
    label "lekcja"
  ]
  node [
    id 56
    label "muzyka"
  ]
  node [
    id 57
    label "podr&#281;cznik"
  ]
  node [
    id 58
    label "zdanie"
  ]
  node [
    id 59
    label "siedziba"
  ]
  node [
    id 60
    label "sekretariat"
  ]
  node [
    id 61
    label "nauka"
  ]
  node [
    id 62
    label "do&#347;wiadczenie"
  ]
  node [
    id 63
    label "tablica"
  ]
  node [
    id 64
    label "teren_szko&#322;y"
  ]
  node [
    id 65
    label "instytucja"
  ]
  node [
    id 66
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 67
    label "skolaryzacja"
  ]
  node [
    id 68
    label "&#322;awa_szkolna"
  ]
  node [
    id 69
    label "klasa"
  ]
  node [
    id 70
    label "cinematic"
  ]
  node [
    id 71
    label "filmowo"
  ]
  node [
    id 72
    label "stanowi&#263;"
  ]
  node [
    id 73
    label "consist"
  ]
  node [
    id 74
    label "tworzy&#263;"
  ]
  node [
    id 75
    label "tworzycielski"
  ]
  node [
    id 76
    label "pracowity"
  ]
  node [
    id 77
    label "oryginalny"
  ]
  node [
    id 78
    label "kreatywny"
  ]
  node [
    id 79
    label "sprzyjaj&#261;cy"
  ]
  node [
    id 80
    label "tw&#243;rczo"
  ]
  node [
    id 81
    label "inspiruj&#261;cy"
  ]
  node [
    id 82
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 83
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 84
    label "organizacja"
  ]
  node [
    id 85
    label "Eleusis"
  ]
  node [
    id 86
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 87
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 88
    label "fabianie"
  ]
  node [
    id 89
    label "Chewra_Kadisza"
  ]
  node [
    id 90
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 91
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 92
    label "Rotary_International"
  ]
  node [
    id 93
    label "Monar"
  ]
  node [
    id 94
    label "zamiana"
  ]
  node [
    id 95
    label "mieszanka"
  ]
  node [
    id 96
    label "compilation"
  ]
  node [
    id 97
    label "utw&#243;r"
  ]
  node [
    id 98
    label "zorganizowa&#263;"
  ]
  node [
    id 99
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 100
    label "przerobi&#263;"
  ]
  node [
    id 101
    label "wystylizowa&#263;"
  ]
  node [
    id 102
    label "cause"
  ]
  node [
    id 103
    label "wydali&#263;"
  ]
  node [
    id 104
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 105
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 106
    label "post&#261;pi&#263;"
  ]
  node [
    id 107
    label "appoint"
  ]
  node [
    id 108
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 109
    label "nabra&#263;"
  ]
  node [
    id 110
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 111
    label "make"
  ]
  node [
    id 112
    label "rozbieg&#243;wka"
  ]
  node [
    id 113
    label "block"
  ]
  node [
    id 114
    label "blik"
  ]
  node [
    id 115
    label "odczula&#263;"
  ]
  node [
    id 116
    label "rola"
  ]
  node [
    id 117
    label "trawiarnia"
  ]
  node [
    id 118
    label "b&#322;ona"
  ]
  node [
    id 119
    label "filmoteka"
  ]
  node [
    id 120
    label "sztuka"
  ]
  node [
    id 121
    label "muza"
  ]
  node [
    id 122
    label "odczuli&#263;"
  ]
  node [
    id 123
    label "klatka"
  ]
  node [
    id 124
    label "odczulenie"
  ]
  node [
    id 125
    label "emulsja_fotograficzna"
  ]
  node [
    id 126
    label "animatronika"
  ]
  node [
    id 127
    label "dorobek"
  ]
  node [
    id 128
    label "odczulanie"
  ]
  node [
    id 129
    label "scena"
  ]
  node [
    id 130
    label "czo&#322;&#243;wka"
  ]
  node [
    id 131
    label "ty&#322;&#243;wka"
  ]
  node [
    id 132
    label "napisy"
  ]
  node [
    id 133
    label "photograph"
  ]
  node [
    id 134
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 135
    label "postprodukcja"
  ]
  node [
    id 136
    label "sklejarka"
  ]
  node [
    id 137
    label "anamorfoza"
  ]
  node [
    id 138
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 139
    label "ta&#347;ma"
  ]
  node [
    id 140
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 141
    label "uj&#281;cie"
  ]
  node [
    id 142
    label "sprzeciw"
  ]
  node [
    id 143
    label "odpowiedni"
  ]
  node [
    id 144
    label "niez&#322;y"
  ]
  node [
    id 145
    label "wystarczaj&#261;co"
  ]
  node [
    id 146
    label "uruchomienie"
  ]
  node [
    id 147
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 148
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 149
    label "supernadz&#243;r"
  ]
  node [
    id 150
    label "absolutorium"
  ]
  node [
    id 151
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 152
    label "podupada&#263;"
  ]
  node [
    id 153
    label "nap&#322;ywanie"
  ]
  node [
    id 154
    label "podupadanie"
  ]
  node [
    id 155
    label "kwestor"
  ]
  node [
    id 156
    label "uruchamia&#263;"
  ]
  node [
    id 157
    label "mienie"
  ]
  node [
    id 158
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 159
    label "uruchamianie"
  ]
  node [
    id 160
    label "czynnik_produkcji"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 65
  ]
  edge [
    source 18
    target 160
  ]
]
