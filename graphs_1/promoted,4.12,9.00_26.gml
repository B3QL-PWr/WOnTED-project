graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.0256410256410255
  density 0.026307026307026308
  graphCliqueNumber 2
  node [
    id 0
    label "funkcjonariusz"
    origin "text"
  ]
  node [
    id 1
    label "brytyjski"
    origin "text"
  ]
  node [
    id 2
    label "policja"
    origin "text"
  ]
  node [
    id 3
    label "metropolitalny"
    origin "text"
  ]
  node [
    id 4
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 5
    label "skazany"
    origin "text"
  ]
  node [
    id 6
    label "gwa&#322;ci&#263;"
    origin "text"
  ]
  node [
    id 7
    label "letni"
    origin "text"
  ]
  node [
    id 8
    label "dziewczynka"
    origin "text"
  ]
  node [
    id 9
    label "las"
    origin "text"
  ]
  node [
    id 10
    label "pracownik"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "angielsko"
  ]
  node [
    id 13
    label "po_brytyjsku"
  ]
  node [
    id 14
    label "europejski"
  ]
  node [
    id 15
    label "j&#281;zyk_martwy"
  ]
  node [
    id 16
    label "j&#281;zyk_celtycki"
  ]
  node [
    id 17
    label "j&#281;zyk_angielski"
  ]
  node [
    id 18
    label "zachodnioeuropejski"
  ]
  node [
    id 19
    label "morris"
  ]
  node [
    id 20
    label "angielski"
  ]
  node [
    id 21
    label "anglosaski"
  ]
  node [
    id 22
    label "brytyjsko"
  ]
  node [
    id 23
    label "komisariat"
  ]
  node [
    id 24
    label "psiarnia"
  ]
  node [
    id 25
    label "posterunek"
  ]
  node [
    id 26
    label "grupa"
  ]
  node [
    id 27
    label "organ"
  ]
  node [
    id 28
    label "s&#322;u&#380;ba"
  ]
  node [
    id 29
    label "proceed"
  ]
  node [
    id 30
    label "catch"
  ]
  node [
    id 31
    label "pozosta&#263;"
  ]
  node [
    id 32
    label "osta&#263;_si&#281;"
  ]
  node [
    id 33
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 34
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 35
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 36
    label "change"
  ]
  node [
    id 37
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 38
    label "s&#261;d"
  ]
  node [
    id 39
    label "dysponowa&#263;"
  ]
  node [
    id 40
    label "dysponowanie"
  ]
  node [
    id 41
    label "zmusza&#263;"
  ]
  node [
    id 42
    label "conflict"
  ]
  node [
    id 43
    label "krzywdzi&#263;"
  ]
  node [
    id 44
    label "narusza&#263;"
  ]
  node [
    id 45
    label "nijaki"
  ]
  node [
    id 46
    label "sezonowy"
  ]
  node [
    id 47
    label "letnio"
  ]
  node [
    id 48
    label "s&#322;oneczny"
  ]
  node [
    id 49
    label "weso&#322;y"
  ]
  node [
    id 50
    label "oboj&#281;tny"
  ]
  node [
    id 51
    label "latowy"
  ]
  node [
    id 52
    label "ciep&#322;y"
  ]
  node [
    id 53
    label "typowy"
  ]
  node [
    id 54
    label "potomkini"
  ]
  node [
    id 55
    label "prostytutka"
  ]
  node [
    id 56
    label "dziecko"
  ]
  node [
    id 57
    label "chody"
  ]
  node [
    id 58
    label "dno_lasu"
  ]
  node [
    id 59
    label "obr&#281;b"
  ]
  node [
    id 60
    label "&#347;ci&#243;&#322;ka"
  ]
  node [
    id 61
    label "podszyt"
  ]
  node [
    id 62
    label "rewir"
  ]
  node [
    id 63
    label "podrost"
  ]
  node [
    id 64
    label "teren"
  ]
  node [
    id 65
    label "le&#347;nictwo"
  ]
  node [
    id 66
    label "wykarczowanie"
  ]
  node [
    id 67
    label "runo"
  ]
  node [
    id 68
    label "teren_le&#347;ny"
  ]
  node [
    id 69
    label "wykarczowa&#263;"
  ]
  node [
    id 70
    label "mn&#243;stwo"
  ]
  node [
    id 71
    label "nadle&#347;nictwo"
  ]
  node [
    id 72
    label "formacja_ro&#347;linna"
  ]
  node [
    id 73
    label "zalesienie"
  ]
  node [
    id 74
    label "karczowa&#263;"
  ]
  node [
    id 75
    label "wiatro&#322;om"
  ]
  node [
    id 76
    label "karczowanie"
  ]
  node [
    id 77
    label "driada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
]
