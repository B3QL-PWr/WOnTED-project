graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.1877022653721685
  density 0.00710292943302652
  graphCliqueNumber 5
  node [
    id 0
    label "dwa"
    origin "text"
  ]
  node [
    id 1
    label "lata"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "&#322;am"
    origin "text"
  ]
  node [
    id 4
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 5
    label "warszawa"
    origin "text"
  ]
  node [
    id 6
    label "tomasz"
    origin "text"
  ]
  node [
    id 7
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "puchatek"
    origin "text"
  ]
  node [
    id 10
    label "nauczy&#263;by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 12
    label "patologicznie"
    origin "text"
  ]
  node [
    id 13
    label "&#322;akomy"
    origin "text"
  ]
  node [
    id 14
    label "idiota"
    origin "text"
  ]
  node [
    id 15
    label "mimo"
    origin "text"
  ]
  node [
    id 16
    label "poeta"
    origin "text"
  ]
  node [
    id 17
    label "by&#263;"
    origin "text"
  ]
  node [
    id 18
    label "tch&#243;rz"
    origin "text"
  ]
  node [
    id 19
    label "przyjaciel"
    origin "text"
  ]
  node [
    id 20
    label "mitoman"
    origin "text"
  ]
  node [
    id 21
    label "kole&#347;"
    origin "text"
  ]
  node [
    id 22
    label "idealny"
    origin "text"
  ]
  node [
    id 23
    label "impreza"
    origin "text"
  ]
  node [
    id 24
    label "wa&#380;niak"
    origin "text"
  ]
  node [
    id 25
    label "jako&#347;"
    origin "text"
  ]
  node [
    id 26
    label "&#380;y&#263;"
    origin "text"
  ]
  node [
    id 27
    label "malkontent"
    origin "text"
  ]
  node [
    id 28
    label "nie"
    origin "text"
  ]
  node [
    id 29
    label "wytrzymanie"
    origin "text"
  ]
  node [
    id 30
    label "&#347;piewaj&#261;co"
    origin "text"
  ]
  node [
    id 31
    label "gdybyby&#263;"
    origin "text"
  ]
  node [
    id 32
    label "czyta&#263;"
    origin "text"
  ]
  node [
    id 33
    label "disneyowskie"
    origin "text"
  ]
  node [
    id 34
    label "ksi&#261;&#380;eczka"
    origin "text"
  ]
  node [
    id 35
    label "tylko"
    origin "text"
  ]
  node [
    id 36
    label "intrygant"
    origin "text"
  ]
  node [
    id 37
    label "cham"
    origin "text"
  ]
  node [
    id 38
    label "par"
    origin "text"
  ]
  node [
    id 39
    label "homoseksualista"
    origin "text"
  ]
  node [
    id 40
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 41
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 42
    label "nadwaga"
    origin "text"
  ]
  node [
    id 43
    label "drugi"
    origin "text"
  ]
  node [
    id 44
    label "boja"
    origin "text"
  ]
  node [
    id 45
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 46
    label "ryj"
    origin "text"
  ]
  node [
    id 47
    label "summer"
  ]
  node [
    id 48
    label "czas"
  ]
  node [
    id 49
    label "kolumna"
  ]
  node [
    id 50
    label "energy"
  ]
  node [
    id 51
    label "bycie"
  ]
  node [
    id 52
    label "zegar_biologiczny"
  ]
  node [
    id 53
    label "okres_noworodkowy"
  ]
  node [
    id 54
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 55
    label "entity"
  ]
  node [
    id 56
    label "prze&#380;ywanie"
  ]
  node [
    id 57
    label "prze&#380;ycie"
  ]
  node [
    id 58
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 59
    label "wiek_matuzalemowy"
  ]
  node [
    id 60
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 61
    label "dzieci&#324;stwo"
  ]
  node [
    id 62
    label "power"
  ]
  node [
    id 63
    label "szwung"
  ]
  node [
    id 64
    label "menopauza"
  ]
  node [
    id 65
    label "umarcie"
  ]
  node [
    id 66
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 67
    label "life"
  ]
  node [
    id 68
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 69
    label "&#380;ywy"
  ]
  node [
    id 70
    label "rozw&#243;j"
  ]
  node [
    id 71
    label "po&#322;&#243;g"
  ]
  node [
    id 72
    label "byt"
  ]
  node [
    id 73
    label "przebywanie"
  ]
  node [
    id 74
    label "subsistence"
  ]
  node [
    id 75
    label "koleje_losu"
  ]
  node [
    id 76
    label "raj_utracony"
  ]
  node [
    id 77
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 78
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 79
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 80
    label "andropauza"
  ]
  node [
    id 81
    label "warunki"
  ]
  node [
    id 82
    label "do&#380;ywanie"
  ]
  node [
    id 83
    label "niemowl&#281;ctwo"
  ]
  node [
    id 84
    label "umieranie"
  ]
  node [
    id 85
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 86
    label "staro&#347;&#263;"
  ]
  node [
    id 87
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 88
    label "&#347;mier&#263;"
  ]
  node [
    id 89
    label "Warszawa"
  ]
  node [
    id 90
    label "samoch&#243;d"
  ]
  node [
    id 91
    label "fastback"
  ]
  node [
    id 92
    label "dzie&#324;_powszedni"
  ]
  node [
    id 93
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 94
    label "tydzie&#324;"
  ]
  node [
    id 95
    label "remark"
  ]
  node [
    id 96
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 97
    label "u&#380;ywa&#263;"
  ]
  node [
    id 98
    label "okre&#347;la&#263;"
  ]
  node [
    id 99
    label "j&#281;zyk"
  ]
  node [
    id 100
    label "say"
  ]
  node [
    id 101
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 102
    label "formu&#322;owa&#263;"
  ]
  node [
    id 103
    label "talk"
  ]
  node [
    id 104
    label "powiada&#263;"
  ]
  node [
    id 105
    label "informowa&#263;"
  ]
  node [
    id 106
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 107
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 108
    label "wydobywa&#263;"
  ]
  node [
    id 109
    label "express"
  ]
  node [
    id 110
    label "chew_the_fat"
  ]
  node [
    id 111
    label "dysfonia"
  ]
  node [
    id 112
    label "umie&#263;"
  ]
  node [
    id 113
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 114
    label "tell"
  ]
  node [
    id 115
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 116
    label "wyra&#380;a&#263;"
  ]
  node [
    id 117
    label "gaworzy&#263;"
  ]
  node [
    id 118
    label "rozmawia&#263;"
  ]
  node [
    id 119
    label "dziama&#263;"
  ]
  node [
    id 120
    label "prawi&#263;"
  ]
  node [
    id 121
    label "free"
  ]
  node [
    id 122
    label "chorobliwie"
  ]
  node [
    id 123
    label "patologiczny"
  ]
  node [
    id 124
    label "chorobliwy"
  ]
  node [
    id 125
    label "strasznie"
  ]
  node [
    id 126
    label "choro"
  ]
  node [
    id 127
    label "k&#322;opotliwie"
  ]
  node [
    id 128
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 129
    label "nienormalnie"
  ]
  node [
    id 130
    label "&#322;apczywy"
  ]
  node [
    id 131
    label "ch&#281;tny"
  ]
  node [
    id 132
    label "niezaspokojony"
  ]
  node [
    id 133
    label "g&#322;odnie"
  ]
  node [
    id 134
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 135
    label "g&#322;upek"
  ]
  node [
    id 136
    label "Zagajewski"
  ]
  node [
    id 137
    label "Osjan"
  ]
  node [
    id 138
    label "Horacy"
  ]
  node [
    id 139
    label "Mi&#322;osz"
  ]
  node [
    id 140
    label "Schiller"
  ]
  node [
    id 141
    label "Rej"
  ]
  node [
    id 142
    label "Byron"
  ]
  node [
    id 143
    label "Jan_Czeczot"
  ]
  node [
    id 144
    label "Peiper"
  ]
  node [
    id 145
    label "Bara&#324;czak"
  ]
  node [
    id 146
    label "Asnyk"
  ]
  node [
    id 147
    label "Bia&#322;oszewski"
  ]
  node [
    id 148
    label "Herbert"
  ]
  node [
    id 149
    label "Dante"
  ]
  node [
    id 150
    label "Puszkin"
  ]
  node [
    id 151
    label "Le&#347;mian"
  ]
  node [
    id 152
    label "R&#243;&#380;ewicz"
  ]
  node [
    id 153
    label "Jesienin"
  ]
  node [
    id 154
    label "pisarz"
  ]
  node [
    id 155
    label "wierszopis"
  ]
  node [
    id 156
    label "Tuwim"
  ]
  node [
    id 157
    label "Norwid"
  ]
  node [
    id 158
    label "Homer"
  ]
  node [
    id 159
    label "Pindar"
  ]
  node [
    id 160
    label "Dawid"
  ]
  node [
    id 161
    label "si&#281;ga&#263;"
  ]
  node [
    id 162
    label "trwa&#263;"
  ]
  node [
    id 163
    label "obecno&#347;&#263;"
  ]
  node [
    id 164
    label "stan"
  ]
  node [
    id 165
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 166
    label "stand"
  ]
  node [
    id 167
    label "mie&#263;_miejsce"
  ]
  node [
    id 168
    label "uczestniczy&#263;"
  ]
  node [
    id 169
    label "chodzi&#263;"
  ]
  node [
    id 170
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 171
    label "equal"
  ]
  node [
    id 172
    label "&#322;asice_w&#322;a&#347;ciwe"
  ]
  node [
    id 173
    label "zwierz&#281;_&#322;owne"
  ]
  node [
    id 174
    label "zwierz&#281;_futerkowe"
  ]
  node [
    id 175
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 176
    label "zwierzyna_drobna"
  ]
  node [
    id 177
    label "strachoput"
  ]
  node [
    id 178
    label "strachaj&#322;o"
  ]
  node [
    id 179
    label "kochanek"
  ]
  node [
    id 180
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 181
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 182
    label "kum"
  ]
  node [
    id 183
    label "sympatyk"
  ]
  node [
    id 184
    label "bratnia_dusza"
  ]
  node [
    id 185
    label "amikus"
  ]
  node [
    id 186
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 187
    label "pobratymiec"
  ]
  node [
    id 188
    label "drogi"
  ]
  node [
    id 189
    label "k&#322;amca"
  ]
  node [
    id 190
    label "chory_psychicznie"
  ]
  node [
    id 191
    label "cz&#322;owiek"
  ]
  node [
    id 192
    label "bratek"
  ]
  node [
    id 193
    label "kolega"
  ]
  node [
    id 194
    label "wznios&#322;y"
  ]
  node [
    id 195
    label "czysty"
  ]
  node [
    id 196
    label "idealistyczny"
  ]
  node [
    id 197
    label "wspania&#322;y"
  ]
  node [
    id 198
    label "pe&#322;ny"
  ]
  node [
    id 199
    label "abstrakcyjny"
  ]
  node [
    id 200
    label "doskonale"
  ]
  node [
    id 201
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 202
    label "skuteczny"
  ]
  node [
    id 203
    label "idealnie"
  ]
  node [
    id 204
    label "party"
  ]
  node [
    id 205
    label "rozrywka"
  ]
  node [
    id 206
    label "przyj&#281;cie"
  ]
  node [
    id 207
    label "okazja"
  ]
  node [
    id 208
    label "impra"
  ]
  node [
    id 209
    label "gracz"
  ]
  node [
    id 210
    label "fisza"
  ]
  node [
    id 211
    label "przem&#261;drzalec"
  ]
  node [
    id 212
    label "kto&#347;"
  ]
  node [
    id 213
    label "dziwnie"
  ]
  node [
    id 214
    label "w_miar&#281;"
  ]
  node [
    id 215
    label "przyzwoicie"
  ]
  node [
    id 216
    label "jako_taki"
  ]
  node [
    id 217
    label "nie&#378;le"
  ]
  node [
    id 218
    label "jaki&#347;"
  ]
  node [
    id 219
    label "pause"
  ]
  node [
    id 220
    label "stay"
  ]
  node [
    id 221
    label "consist"
  ]
  node [
    id 222
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 223
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 224
    label "istnie&#263;"
  ]
  node [
    id 225
    label "istota_&#380;ywa"
  ]
  node [
    id 226
    label "sprzeciw"
  ]
  node [
    id 227
    label "clasp"
  ]
  node [
    id 228
    label "oczekiwanie"
  ]
  node [
    id 229
    label "pozostanie"
  ]
  node [
    id 230
    label "experience"
  ]
  node [
    id 231
    label "zmuszenie"
  ]
  node [
    id 232
    label "d&#378;wi&#281;cznie"
  ]
  node [
    id 233
    label "koncertowo"
  ]
  node [
    id 234
    label "&#347;piewny"
  ]
  node [
    id 235
    label "udanie"
  ]
  node [
    id 236
    label "wspaniale"
  ]
  node [
    id 237
    label "&#347;piewaj&#261;cy"
  ]
  node [
    id 238
    label "melodyjnie"
  ]
  node [
    id 239
    label "g&#322;adko"
  ]
  node [
    id 240
    label "dysleksja"
  ]
  node [
    id 241
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 242
    label "przetwarza&#263;"
  ]
  node [
    id 243
    label "read"
  ]
  node [
    id 244
    label "poznawa&#263;"
  ]
  node [
    id 245
    label "obserwowa&#263;"
  ]
  node [
    id 246
    label "odczytywa&#263;"
  ]
  node [
    id 247
    label "dokument"
  ]
  node [
    id 248
    label "booklet"
  ]
  node [
    id 249
    label "dow&#243;d_to&#380;samo&#347;ci"
  ]
  node [
    id 250
    label "oszcz&#281;dno&#347;ci"
  ]
  node [
    id 251
    label "szkodnik"
  ]
  node [
    id 252
    label "chamstwo"
  ]
  node [
    id 253
    label "prostak"
  ]
  node [
    id 254
    label "ch&#322;op"
  ]
  node [
    id 255
    label "chamski"
  ]
  node [
    id 256
    label "Izba_Par&#243;w"
  ]
  node [
    id 257
    label "lord"
  ]
  node [
    id 258
    label "Izba_Lord&#243;w"
  ]
  node [
    id 259
    label "parlamentarzysta"
  ]
  node [
    id 260
    label "lennik"
  ]
  node [
    id 261
    label "homo&#347;"
  ]
  node [
    id 262
    label "czyj&#347;"
  ]
  node [
    id 263
    label "m&#261;&#380;"
  ]
  node [
    id 264
    label "wygl&#261;d"
  ]
  node [
    id 265
    label "fleshiness"
  ]
  node [
    id 266
    label "cecha"
  ]
  node [
    id 267
    label "zaburzenie"
  ]
  node [
    id 268
    label "inny"
  ]
  node [
    id 269
    label "kolejny"
  ]
  node [
    id 270
    label "przeciwny"
  ]
  node [
    id 271
    label "sw&#243;j"
  ]
  node [
    id 272
    label "odwrotnie"
  ]
  node [
    id 273
    label "dzie&#324;"
  ]
  node [
    id 274
    label "podobny"
  ]
  node [
    id 275
    label "wt&#243;ry"
  ]
  node [
    id 276
    label "float"
  ]
  node [
    id 277
    label "znak_nawigacyjny"
  ]
  node [
    id 278
    label "p&#322;ywak"
  ]
  node [
    id 279
    label "get"
  ]
  node [
    id 280
    label "doczeka&#263;"
  ]
  node [
    id 281
    label "zwiastun"
  ]
  node [
    id 282
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 283
    label "develop"
  ]
  node [
    id 284
    label "catch"
  ]
  node [
    id 285
    label "uzyska&#263;"
  ]
  node [
    id 286
    label "kupi&#263;"
  ]
  node [
    id 287
    label "wzi&#261;&#263;"
  ]
  node [
    id 288
    label "naby&#263;"
  ]
  node [
    id 289
    label "nabawienie_si&#281;"
  ]
  node [
    id 290
    label "obskoczy&#263;"
  ]
  node [
    id 291
    label "zapanowa&#263;"
  ]
  node [
    id 292
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 293
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 294
    label "zrobi&#263;"
  ]
  node [
    id 295
    label "nabawianie_si&#281;"
  ]
  node [
    id 296
    label "range"
  ]
  node [
    id 297
    label "schorzenie"
  ]
  node [
    id 298
    label "wystarczy&#263;"
  ]
  node [
    id 299
    label "wysta&#263;"
  ]
  node [
    id 300
    label "twarz"
  ]
  node [
    id 301
    label "dzi&#243;b"
  ]
  node [
    id 302
    label "morda"
  ]
  node [
    id 303
    label "&#347;winia"
  ]
  node [
    id 304
    label "usta"
  ]
  node [
    id 305
    label "Tomasz"
  ]
  node [
    id 306
    label "pi&#261;tka"
  ]
  node [
    id 307
    label "chatka"
  ]
  node [
    id 308
    label "Puchatek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 11
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 34
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 16
    target 39
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 17
    target 27
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 39
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 34
  ]
  edge [
    source 22
    target 40
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 39
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 219
  ]
  edge [
    source 26
    target 220
  ]
  edge [
    source 26
    target 221
  ]
  edge [
    source 26
    target 222
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 26
    target 224
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 29
    target 227
  ]
  edge [
    source 29
    target 228
  ]
  edge [
    source 29
    target 229
  ]
  edge [
    source 29
    target 230
  ]
  edge [
    source 29
    target 231
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 232
  ]
  edge [
    source 30
    target 233
  ]
  edge [
    source 30
    target 234
  ]
  edge [
    source 30
    target 235
  ]
  edge [
    source 30
    target 236
  ]
  edge [
    source 30
    target 237
  ]
  edge [
    source 30
    target 238
  ]
  edge [
    source 30
    target 239
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 32
    target 112
  ]
  edge [
    source 32
    target 241
  ]
  edge [
    source 32
    target 242
  ]
  edge [
    source 32
    target 243
  ]
  edge [
    source 32
    target 244
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 246
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 34
    target 247
  ]
  edge [
    source 34
    target 248
  ]
  edge [
    source 34
    target 249
  ]
  edge [
    source 34
    target 250
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 251
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 252
  ]
  edge [
    source 37
    target 253
  ]
  edge [
    source 37
    target 254
  ]
  edge [
    source 37
    target 255
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 256
  ]
  edge [
    source 38
    target 257
  ]
  edge [
    source 38
    target 258
  ]
  edge [
    source 38
    target 259
  ]
  edge [
    source 38
    target 260
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 191
  ]
  edge [
    source 39
    target 261
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 262
  ]
  edge [
    source 41
    target 263
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 264
  ]
  edge [
    source 42
    target 265
  ]
  edge [
    source 42
    target 266
  ]
  edge [
    source 42
    target 267
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 191
  ]
  edge [
    source 43
    target 268
  ]
  edge [
    source 43
    target 269
  ]
  edge [
    source 43
    target 270
  ]
  edge [
    source 43
    target 271
  ]
  edge [
    source 43
    target 272
  ]
  edge [
    source 43
    target 273
  ]
  edge [
    source 43
    target 274
  ]
  edge [
    source 43
    target 275
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 276
  ]
  edge [
    source 44
    target 277
  ]
  edge [
    source 44
    target 278
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 279
  ]
  edge [
    source 45
    target 280
  ]
  edge [
    source 45
    target 281
  ]
  edge [
    source 45
    target 282
  ]
  edge [
    source 45
    target 283
  ]
  edge [
    source 45
    target 284
  ]
  edge [
    source 45
    target 285
  ]
  edge [
    source 45
    target 286
  ]
  edge [
    source 45
    target 287
  ]
  edge [
    source 45
    target 288
  ]
  edge [
    source 45
    target 289
  ]
  edge [
    source 45
    target 290
  ]
  edge [
    source 45
    target 291
  ]
  edge [
    source 45
    target 292
  ]
  edge [
    source 45
    target 293
  ]
  edge [
    source 45
    target 294
  ]
  edge [
    source 45
    target 295
  ]
  edge [
    source 45
    target 296
  ]
  edge [
    source 45
    target 297
  ]
  edge [
    source 45
    target 298
  ]
  edge [
    source 45
    target 299
  ]
  edge [
    source 46
    target 300
  ]
  edge [
    source 46
    target 301
  ]
  edge [
    source 46
    target 302
  ]
  edge [
    source 46
    target 303
  ]
  edge [
    source 46
    target 304
  ]
  edge [
    source 305
    target 306
  ]
  edge [
    source 307
    target 308
  ]
]
