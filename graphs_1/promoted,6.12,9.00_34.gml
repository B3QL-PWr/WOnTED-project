graph [
  maxDegree 32
  minDegree 1
  meanDegree 1.967741935483871
  density 0.03225806451612903
  graphCliqueNumber 2
  node [
    id 0
    label "obejrze&#263;"
    origin "text"
  ]
  node [
    id 1
    label "film"
    origin "text"
  ]
  node [
    id 2
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mistrz"
    origin "text"
  ]
  node [
    id 4
    label "kafelkowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 6
    label "visualize"
  ]
  node [
    id 7
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 8
    label "rozbieg&#243;wka"
  ]
  node [
    id 9
    label "block"
  ]
  node [
    id 10
    label "blik"
  ]
  node [
    id 11
    label "odczula&#263;"
  ]
  node [
    id 12
    label "rola"
  ]
  node [
    id 13
    label "trawiarnia"
  ]
  node [
    id 14
    label "b&#322;ona"
  ]
  node [
    id 15
    label "filmoteka"
  ]
  node [
    id 16
    label "sztuka"
  ]
  node [
    id 17
    label "muza"
  ]
  node [
    id 18
    label "odczuli&#263;"
  ]
  node [
    id 19
    label "klatka"
  ]
  node [
    id 20
    label "odczulenie"
  ]
  node [
    id 21
    label "emulsja_fotograficzna"
  ]
  node [
    id 22
    label "animatronika"
  ]
  node [
    id 23
    label "dorobek"
  ]
  node [
    id 24
    label "odczulanie"
  ]
  node [
    id 25
    label "scena"
  ]
  node [
    id 26
    label "czo&#322;&#243;wka"
  ]
  node [
    id 27
    label "ty&#322;&#243;wka"
  ]
  node [
    id 28
    label "napisy"
  ]
  node [
    id 29
    label "photograph"
  ]
  node [
    id 30
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 31
    label "postprodukcja"
  ]
  node [
    id 32
    label "sklejarka"
  ]
  node [
    id 33
    label "anamorfoza"
  ]
  node [
    id 34
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 35
    label "ta&#347;ma"
  ]
  node [
    id 36
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 37
    label "uj&#281;cie"
  ]
  node [
    id 38
    label "proceed"
  ]
  node [
    id 39
    label "catch"
  ]
  node [
    id 40
    label "pozosta&#263;"
  ]
  node [
    id 41
    label "osta&#263;_si&#281;"
  ]
  node [
    id 42
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 43
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 44
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 45
    label "change"
  ]
  node [
    id 46
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 47
    label "miszczu"
  ]
  node [
    id 48
    label "rzemie&#347;lnik"
  ]
  node [
    id 49
    label "znakomito&#347;&#263;"
  ]
  node [
    id 50
    label "tytu&#322;"
  ]
  node [
    id 51
    label "doradca"
  ]
  node [
    id 52
    label "werkmistrz"
  ]
  node [
    id 53
    label "majstersztyk"
  ]
  node [
    id 54
    label "zwyci&#281;zca"
  ]
  node [
    id 55
    label "agent"
  ]
  node [
    id 56
    label "kozak"
  ]
  node [
    id 57
    label "autorytet"
  ]
  node [
    id 58
    label "zwierzchnik"
  ]
  node [
    id 59
    label "Towia&#324;ski"
  ]
  node [
    id 60
    label "uk&#322;ada&#263;"
  ]
  node [
    id 61
    label "k&#322;a&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
]
