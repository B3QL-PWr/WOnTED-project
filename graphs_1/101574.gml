graph [
  maxDegree 35
  minDegree 1
  meanDegree 2.0307692307692307
  density 0.010467882632831086
  graphCliqueNumber 2
  node [
    id 0
    label "gdyby"
    origin "text"
  ]
  node [
    id 1
    label "krytyk"
    origin "text"
  ]
  node [
    id 2
    label "jahus&#243;w"
    origin "text"
  ]
  node [
    id 3
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 4
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 5
    label "interesowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "mie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zupe&#322;ny"
    origin "text"
  ]
  node [
    id 8
    label "s&#322;uszno&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wiele"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "u&#380;ala&#263;"
    origin "text"
  ]
  node [
    id 12
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 13
    label "tyle"
    origin "text"
  ]
  node [
    id 14
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 15
    label "bezczelny"
    origin "text"
  ]
  node [
    id 16
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 17
    label "utrzymywa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "moja"
    origin "text"
  ]
  node [
    id 19
    label "by&#263;"
    origin "text"
  ]
  node [
    id 20
    label "czysta"
    origin "text"
  ]
  node [
    id 21
    label "bajka"
    origin "text"
  ]
  node [
    id 22
    label "m&#243;zg"
    origin "text"
  ]
  node [
    id 23
    label "moi"
    origin "text"
  ]
  node [
    id 24
    label "wyleg&#322;y"
    origin "text"
  ]
  node [
    id 25
    label "tak"
    origin "text"
  ]
  node [
    id 26
    label "dalece"
    origin "text"
  ]
  node [
    id 27
    label "zuchwa&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 28
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 29
    label "posun&#261;&#263;"
    origin "text"
  ]
  node [
    id 30
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 31
    label "nie"
    origin "text"
  ]
  node [
    id 32
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 33
    label "jak"
    origin "text"
  ]
  node [
    id 34
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 35
    label "przeciwnik"
  ]
  node [
    id 36
    label "publicysta"
  ]
  node [
    id 37
    label "krytyka"
  ]
  node [
    id 38
    label "rozciekawia&#263;"
  ]
  node [
    id 39
    label "sake"
  ]
  node [
    id 40
    label "w_pizdu"
  ]
  node [
    id 41
    label "&#322;&#261;czny"
  ]
  node [
    id 42
    label "og&#243;lnie"
  ]
  node [
    id 43
    label "ca&#322;y"
  ]
  node [
    id 44
    label "pe&#322;ny"
  ]
  node [
    id 45
    label "zupe&#322;nie"
  ]
  node [
    id 46
    label "kompletnie"
  ]
  node [
    id 47
    label "prawda"
  ]
  node [
    id 48
    label "prawdziwo&#347;&#263;"
  ]
  node [
    id 49
    label "zasadno&#347;&#263;"
  ]
  node [
    id 50
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 51
    label "wiela"
  ]
  node [
    id 52
    label "du&#380;y"
  ]
  node [
    id 53
    label "konkretnie"
  ]
  node [
    id 54
    label "nieznacznie"
  ]
  node [
    id 55
    label "dawny"
  ]
  node [
    id 56
    label "rozw&#243;d"
  ]
  node [
    id 57
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 58
    label "eksprezydent"
  ]
  node [
    id 59
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 60
    label "partner"
  ]
  node [
    id 61
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 62
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 63
    label "wcze&#347;niejszy"
  ]
  node [
    id 64
    label "rozbestwienie_si&#281;"
  ]
  node [
    id 65
    label "rozpanoszenie_si&#281;"
  ]
  node [
    id 66
    label "niepokorny"
  ]
  node [
    id 67
    label "harny"
  ]
  node [
    id 68
    label "rozzuchwalanie"
  ]
  node [
    id 69
    label "rozzuchwalanie_si&#281;"
  ]
  node [
    id 70
    label "rozzuchwalenie_si&#281;"
  ]
  node [
    id 71
    label "zuchwale"
  ]
  node [
    id 72
    label "pyszny"
  ]
  node [
    id 73
    label "odwa&#380;ny"
  ]
  node [
    id 74
    label "rozzuchwalenie"
  ]
  node [
    id 75
    label "czelny"
  ]
  node [
    id 76
    label "zuchwalczy"
  ]
  node [
    id 77
    label "rozbestwianie_si&#281;"
  ]
  node [
    id 78
    label "zapewnia&#263;"
  ]
  node [
    id 79
    label "manewr"
  ]
  node [
    id 80
    label "byt"
  ]
  node [
    id 81
    label "cope"
  ]
  node [
    id 82
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 83
    label "zachowywa&#263;"
  ]
  node [
    id 84
    label "twierdzi&#263;"
  ]
  node [
    id 85
    label "trzyma&#263;"
  ]
  node [
    id 86
    label "corroborate"
  ]
  node [
    id 87
    label "sprawowa&#263;"
  ]
  node [
    id 88
    label "s&#261;dzi&#263;"
  ]
  node [
    id 89
    label "podtrzymywa&#263;"
  ]
  node [
    id 90
    label "defy"
  ]
  node [
    id 91
    label "panowa&#263;"
  ]
  node [
    id 92
    label "argue"
  ]
  node [
    id 93
    label "broni&#263;"
  ]
  node [
    id 94
    label "si&#281;ga&#263;"
  ]
  node [
    id 95
    label "trwa&#263;"
  ]
  node [
    id 96
    label "obecno&#347;&#263;"
  ]
  node [
    id 97
    label "stan"
  ]
  node [
    id 98
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 99
    label "stand"
  ]
  node [
    id 100
    label "mie&#263;_miejsce"
  ]
  node [
    id 101
    label "uczestniczy&#263;"
  ]
  node [
    id 102
    label "chodzi&#263;"
  ]
  node [
    id 103
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 104
    label "equal"
  ]
  node [
    id 105
    label "w&#243;dka"
  ]
  node [
    id 106
    label "czy&#347;ciocha"
  ]
  node [
    id 107
    label "apolog"
  ]
  node [
    id 108
    label "morfing"
  ]
  node [
    id 109
    label "film"
  ]
  node [
    id 110
    label "mora&#322;"
  ]
  node [
    id 111
    label "opowie&#347;&#263;"
  ]
  node [
    id 112
    label "narrative"
  ]
  node [
    id 113
    label "g&#322;upstwo"
  ]
  node [
    id 114
    label "utw&#243;r"
  ]
  node [
    id 115
    label "Pok&#233;mon"
  ]
  node [
    id 116
    label "epika"
  ]
  node [
    id 117
    label "komfort"
  ]
  node [
    id 118
    label "sytuacja"
  ]
  node [
    id 119
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 120
    label "elektroencefalogram"
  ]
  node [
    id 121
    label "substancja_szara"
  ]
  node [
    id 122
    label "przodom&#243;zgowie"
  ]
  node [
    id 123
    label "bruzda"
  ]
  node [
    id 124
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 125
    label "wzg&#243;rze"
  ]
  node [
    id 126
    label "umys&#322;"
  ]
  node [
    id 127
    label "zw&#243;j"
  ]
  node [
    id 128
    label "kresom&#243;zgowie"
  ]
  node [
    id 129
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 130
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 131
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 132
    label "przysadka"
  ]
  node [
    id 133
    label "wiedza"
  ]
  node [
    id 134
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 135
    label "przedmurze"
  ]
  node [
    id 136
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 137
    label "projektodawca"
  ]
  node [
    id 138
    label "noosfera"
  ]
  node [
    id 139
    label "cecha"
  ]
  node [
    id 140
    label "g&#322;owa"
  ]
  node [
    id 141
    label "organ"
  ]
  node [
    id 142
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 143
    label "most"
  ]
  node [
    id 144
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 145
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 146
    label "encefalografia"
  ]
  node [
    id 147
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 148
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 149
    label "kora_m&#243;zgowa"
  ]
  node [
    id 150
    label "podwzg&#243;rze"
  ]
  node [
    id 151
    label "poduszka"
  ]
  node [
    id 152
    label "zniszczony"
  ]
  node [
    id 153
    label "znacznie"
  ]
  node [
    id 154
    label "du&#380;o"
  ]
  node [
    id 155
    label "charakterek"
  ]
  node [
    id 156
    label "heart"
  ]
  node [
    id 157
    label "niepokorno&#347;&#263;"
  ]
  node [
    id 158
    label "chojractwo"
  ]
  node [
    id 159
    label "wynios&#322;o&#347;&#263;"
  ]
  node [
    id 160
    label "pewno&#347;&#263;_siebie"
  ]
  node [
    id 161
    label "niegrzeczno&#347;&#263;"
  ]
  node [
    id 162
    label "cz&#322;owiek"
  ]
  node [
    id 163
    label "bli&#378;ni"
  ]
  node [
    id 164
    label "odpowiedni"
  ]
  node [
    id 165
    label "swojak"
  ]
  node [
    id 166
    label "samodzielny"
  ]
  node [
    id 167
    label "shift"
  ]
  node [
    id 168
    label "advance"
  ]
  node [
    id 169
    label "przyspieszy&#263;"
  ]
  node [
    id 170
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 171
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 172
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 173
    label "express"
  ]
  node [
    id 174
    label "rzekn&#261;&#263;"
  ]
  node [
    id 175
    label "okre&#347;li&#263;"
  ]
  node [
    id 176
    label "wyrazi&#263;"
  ]
  node [
    id 177
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 178
    label "unwrap"
  ]
  node [
    id 179
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 180
    label "convey"
  ]
  node [
    id 181
    label "discover"
  ]
  node [
    id 182
    label "wydoby&#263;"
  ]
  node [
    id 183
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 184
    label "poda&#263;"
  ]
  node [
    id 185
    label "sprzeciw"
  ]
  node [
    id 186
    label "czyj&#347;"
  ]
  node [
    id 187
    label "m&#261;&#380;"
  ]
  node [
    id 188
    label "byd&#322;o"
  ]
  node [
    id 189
    label "zobo"
  ]
  node [
    id 190
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 191
    label "yakalo"
  ]
  node [
    id 192
    label "dzo"
  ]
  node [
    id 193
    label "ludno&#347;&#263;"
  ]
  node [
    id 194
    label "zwierz&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 29
  ]
  edge [
    source 10
    target 30
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 53
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 55
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 57
  ]
  edge [
    source 14
    target 58
  ]
  edge [
    source 14
    target 59
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 61
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 64
  ]
  edge [
    source 15
    target 65
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 67
  ]
  edge [
    source 15
    target 68
  ]
  edge [
    source 15
    target 69
  ]
  edge [
    source 15
    target 70
  ]
  edge [
    source 15
    target 71
  ]
  edge [
    source 15
    target 72
  ]
  edge [
    source 15
    target 73
  ]
  edge [
    source 15
    target 74
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 78
  ]
  edge [
    source 17
    target 79
  ]
  edge [
    source 17
    target 80
  ]
  edge [
    source 17
    target 81
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 17
    target 85
  ]
  edge [
    source 17
    target 86
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 17
    target 91
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 17
    target 93
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 94
  ]
  edge [
    source 19
    target 95
  ]
  edge [
    source 19
    target 96
  ]
  edge [
    source 19
    target 97
  ]
  edge [
    source 19
    target 98
  ]
  edge [
    source 19
    target 99
  ]
  edge [
    source 19
    target 100
  ]
  edge [
    source 19
    target 101
  ]
  edge [
    source 19
    target 102
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 105
  ]
  edge [
    source 20
    target 106
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 107
  ]
  edge [
    source 21
    target 108
  ]
  edge [
    source 21
    target 109
  ]
  edge [
    source 21
    target 110
  ]
  edge [
    source 21
    target 111
  ]
  edge [
    source 21
    target 112
  ]
  edge [
    source 21
    target 113
  ]
  edge [
    source 21
    target 114
  ]
  edge [
    source 21
    target 115
  ]
  edge [
    source 21
    target 116
  ]
  edge [
    source 21
    target 117
  ]
  edge [
    source 21
    target 118
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 119
  ]
  edge [
    source 22
    target 120
  ]
  edge [
    source 22
    target 121
  ]
  edge [
    source 22
    target 122
  ]
  edge [
    source 22
    target 123
  ]
  edge [
    source 22
    target 124
  ]
  edge [
    source 22
    target 125
  ]
  edge [
    source 22
    target 126
  ]
  edge [
    source 22
    target 127
  ]
  edge [
    source 22
    target 128
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 130
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 22
    target 133
  ]
  edge [
    source 22
    target 134
  ]
  edge [
    source 22
    target 135
  ]
  edge [
    source 22
    target 136
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 138
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 22
    target 140
  ]
  edge [
    source 22
    target 141
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 22
    target 143
  ]
  edge [
    source 22
    target 144
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 147
  ]
  edge [
    source 22
    target 148
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 22
    target 150
  ]
  edge [
    source 22
    target 151
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 152
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 153
  ]
  edge [
    source 26
    target 154
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 155
  ]
  edge [
    source 27
    target 156
  ]
  edge [
    source 27
    target 157
  ]
  edge [
    source 27
    target 158
  ]
  edge [
    source 27
    target 159
  ]
  edge [
    source 27
    target 160
  ]
  edge [
    source 27
    target 161
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 162
  ]
  edge [
    source 28
    target 163
  ]
  edge [
    source 28
    target 164
  ]
  edge [
    source 28
    target 165
  ]
  edge [
    source 28
    target 166
  ]
  edge [
    source 29
    target 167
  ]
  edge [
    source 29
    target 168
  ]
  edge [
    source 29
    target 169
  ]
  edge [
    source 29
    target 170
  ]
  edge [
    source 29
    target 171
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 172
  ]
  edge [
    source 30
    target 173
  ]
  edge [
    source 30
    target 174
  ]
  edge [
    source 30
    target 175
  ]
  edge [
    source 30
    target 176
  ]
  edge [
    source 30
    target 177
  ]
  edge [
    source 30
    target 178
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 30
    target 181
  ]
  edge [
    source 30
    target 182
  ]
  edge [
    source 30
    target 183
  ]
  edge [
    source 30
    target 184
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 185
  ]
  edge [
    source 32
    target 186
  ]
  edge [
    source 32
    target 187
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 188
  ]
  edge [
    source 33
    target 189
  ]
  edge [
    source 33
    target 190
  ]
  edge [
    source 33
    target 191
  ]
  edge [
    source 33
    target 192
  ]
  edge [
    source 34
    target 162
  ]
  edge [
    source 34
    target 193
  ]
  edge [
    source 34
    target 194
  ]
]
