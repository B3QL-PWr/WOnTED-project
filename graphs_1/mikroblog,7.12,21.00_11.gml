graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9722222222222223
  density 0.027777777777777776
  graphCliqueNumber 2
  node [
    id 0
    label "mirka"
    origin "text"
  ]
  node [
    id 1
    label "jak"
    origin "text"
  ]
  node [
    id 2
    label "poz&#243;r"
    origin "text"
  ]
  node [
    id 3
    label "pierdo&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "wygl&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "fizyczny"
    origin "text"
  ]
  node [
    id 6
    label "lasek"
    origin "text"
  ]
  node [
    id 7
    label "powodowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "staj"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "dla"
    origin "text"
  ]
  node [
    id 11
    label "ca&#322;kowicie"
    origin "text"
  ]
  node [
    id 12
    label "askeusalnym"
    origin "text"
  ]
  node [
    id 13
    label "tw&#243;r"
    origin "text"
  ]
  node [
    id 14
    label "byd&#322;o"
  ]
  node [
    id 15
    label "zobo"
  ]
  node [
    id 16
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 17
    label "yakalo"
  ]
  node [
    id 18
    label "dzo"
  ]
  node [
    id 19
    label "semblance"
  ]
  node [
    id 20
    label "nieprawda"
  ]
  node [
    id 21
    label "banalny"
  ]
  node [
    id 22
    label "ramol"
  ]
  node [
    id 23
    label "szczeg&#243;&#322;"
  ]
  node [
    id 24
    label "furda"
  ]
  node [
    id 25
    label "g&#243;wno"
  ]
  node [
    id 26
    label "sofcik"
  ]
  node [
    id 27
    label "oferma"
  ]
  node [
    id 28
    label "nudziarz"
  ]
  node [
    id 29
    label "nadawanie"
  ]
  node [
    id 30
    label "ubarwienie"
  ]
  node [
    id 31
    label "brzydota"
  ]
  node [
    id 32
    label "widok"
  ]
  node [
    id 33
    label "postarzanie"
  ]
  node [
    id 34
    label "cecha"
  ]
  node [
    id 35
    label "prostota"
  ]
  node [
    id 36
    label "shape"
  ]
  node [
    id 37
    label "postarzenie"
  ]
  node [
    id 38
    label "kszta&#322;t"
  ]
  node [
    id 39
    label "postarzy&#263;"
  ]
  node [
    id 40
    label "postarza&#263;"
  ]
  node [
    id 41
    label "portrecista"
  ]
  node [
    id 42
    label "fizykalnie"
  ]
  node [
    id 43
    label "fizycznie"
  ]
  node [
    id 44
    label "materializowanie"
  ]
  node [
    id 45
    label "pracownik"
  ]
  node [
    id 46
    label "gimnastyczny"
  ]
  node [
    id 47
    label "widoczny"
  ]
  node [
    id 48
    label "namacalny"
  ]
  node [
    id 49
    label "zmaterializowanie"
  ]
  node [
    id 50
    label "organiczny"
  ]
  node [
    id 51
    label "materjalny"
  ]
  node [
    id 52
    label "motywowa&#263;"
  ]
  node [
    id 53
    label "mie&#263;_miejsce"
  ]
  node [
    id 54
    label "act"
  ]
  node [
    id 55
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 56
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 57
    label "kompletny"
  ]
  node [
    id 58
    label "wniwecz"
  ]
  node [
    id 59
    label "zupe&#322;ny"
  ]
  node [
    id 60
    label "organizm"
  ]
  node [
    id 61
    label "cia&#322;o"
  ]
  node [
    id 62
    label "istota"
  ]
  node [
    id 63
    label "part"
  ]
  node [
    id 64
    label "work"
  ]
  node [
    id 65
    label "organ"
  ]
  node [
    id 66
    label "rzecz"
  ]
  node [
    id 67
    label "przyroda"
  ]
  node [
    id 68
    label "substance"
  ]
  node [
    id 69
    label "rezultat"
  ]
  node [
    id 70
    label "p&#322;&#243;d"
  ]
  node [
    id 71
    label "element_anatomiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 60
  ]
  edge [
    source 13
    target 61
  ]
  edge [
    source 13
    target 62
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 64
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
]
