graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.826086956521739
  density 0.08300395256916997
  graphCliqueNumber 2
  node [
    id 0
    label "zwyci&#281;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 1
    label "jeden"
    origin "text"
  ]
  node [
    id 2
    label "rule"
  ]
  node [
    id 3
    label "score"
  ]
  node [
    id 4
    label "zdecydowa&#263;"
  ]
  node [
    id 5
    label "zwojowa&#263;"
  ]
  node [
    id 6
    label "znie&#347;&#263;"
  ]
  node [
    id 7
    label "poradzi&#263;_sobie"
  ]
  node [
    id 8
    label "overwhelm"
  ]
  node [
    id 9
    label "zrobi&#263;"
  ]
  node [
    id 10
    label "kieliszek"
  ]
  node [
    id 11
    label "shot"
  ]
  node [
    id 12
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 13
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 14
    label "jaki&#347;"
  ]
  node [
    id 15
    label "jednolicie"
  ]
  node [
    id 16
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 17
    label "w&#243;dka"
  ]
  node [
    id 18
    label "ten"
  ]
  node [
    id 19
    label "ujednolicenie"
  ]
  node [
    id 20
    label "jednakowy"
  ]
  node [
    id 21
    label "Fejdolosa"
  ]
  node [
    id 22
    label "Koryntianin"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 21
    target 22
  ]
]
