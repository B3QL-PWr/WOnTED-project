graph [
  maxDegree 10
  minDegree 1
  meanDegree 2.4186046511627906
  density 0.05758582502768549
  graphCliqueNumber 6
  node [
    id 0
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 1
    label "przyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 3
    label "miko&#322;aj"
    origin "text"
  ]
  node [
    id 4
    label "sam"
    origin "text"
  ]
  node [
    id 5
    label "zadba&#263;"
    origin "text"
  ]
  node [
    id 6
    label "&#347;wi&#261;teczny"
    origin "text"
  ]
  node [
    id 7
    label "prezent"
    origin "text"
  ]
  node [
    id 8
    label "sta&#263;_si&#281;"
  ]
  node [
    id 9
    label "zaistnie&#263;"
  ]
  node [
    id 10
    label "czas"
  ]
  node [
    id 11
    label "doj&#347;&#263;"
  ]
  node [
    id 12
    label "become"
  ]
  node [
    id 13
    label "line_up"
  ]
  node [
    id 14
    label "przyby&#263;"
  ]
  node [
    id 15
    label "doba"
  ]
  node [
    id 16
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 17
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 18
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 19
    label "sklep"
  ]
  node [
    id 20
    label "zatroszczy&#263;_si&#281;"
  ]
  node [
    id 21
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 22
    label "postara&#263;_si&#281;"
  ]
  node [
    id 23
    label "dzie&#324;_wolny"
  ]
  node [
    id 24
    label "&#347;wi&#281;tny"
  ]
  node [
    id 25
    label "&#347;wi&#261;tecznie"
  ]
  node [
    id 26
    label "wyj&#261;tkowy"
  ]
  node [
    id 27
    label "od&#347;wi&#281;tny"
  ]
  node [
    id 28
    label "od&#347;wi&#281;tnie"
  ]
  node [
    id 29
    label "obrz&#281;dowy"
  ]
  node [
    id 30
    label "uroczysty"
  ]
  node [
    id 31
    label "dar"
  ]
  node [
    id 32
    label "presentation"
  ]
  node [
    id 33
    label "lo&#380;a"
  ]
  node [
    id 34
    label "szyderca"
  ]
  node [
    id 35
    label "White"
  ]
  node [
    id 36
    label "spos&#243;b"
  ]
  node [
    id 37
    label "nagroda"
  ]
  node [
    id 38
    label "1"
  ]
  node [
    id 39
    label "na"
  ]
  node [
    id 40
    label "wygrana"
  ]
  node [
    id 41
    label "2"
  ]
  node [
    id 42
    label "Black"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 33
    target 42
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 34
    target 42
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 39
  ]
  edge [
    source 35
    target 40
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 36
    target 42
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 41
  ]
  edge [
    source 39
    target 40
  ]
]
