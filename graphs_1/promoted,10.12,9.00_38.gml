graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.962962962962963
  density 0.037037037037037035
  graphCliqueNumber 2
  node [
    id 0
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 2
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "poczucie"
    origin "text"
  ]
  node [
    id 4
    label "humor"
    origin "text"
  ]
  node [
    id 5
    label "wykop"
    origin "text"
  ]
  node [
    id 6
    label "odzyska&#263;"
  ]
  node [
    id 7
    label "devise"
  ]
  node [
    id 8
    label "oceni&#263;"
  ]
  node [
    id 9
    label "znaj&#347;&#263;"
  ]
  node [
    id 10
    label "wymy&#347;li&#263;"
  ]
  node [
    id 11
    label "invent"
  ]
  node [
    id 12
    label "pozyska&#263;"
  ]
  node [
    id 13
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 14
    label "wykry&#263;"
  ]
  node [
    id 15
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 16
    label "dozna&#263;"
  ]
  node [
    id 17
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 18
    label "czu&#263;"
  ]
  node [
    id 19
    label "need"
  ]
  node [
    id 20
    label "hide"
  ]
  node [
    id 21
    label "support"
  ]
  node [
    id 22
    label "zareagowanie"
  ]
  node [
    id 23
    label "opanowanie"
  ]
  node [
    id 24
    label "doznanie"
  ]
  node [
    id 25
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 26
    label "intuition"
  ]
  node [
    id 27
    label "wiedza"
  ]
  node [
    id 28
    label "ekstraspekcja"
  ]
  node [
    id 29
    label "os&#322;upienie"
  ]
  node [
    id 30
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 31
    label "smell"
  ]
  node [
    id 32
    label "zdarzenie_si&#281;"
  ]
  node [
    id 33
    label "feeling"
  ]
  node [
    id 34
    label "stan"
  ]
  node [
    id 35
    label "temper"
  ]
  node [
    id 36
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 37
    label "mechanizm_obronny"
  ]
  node [
    id 38
    label "nastr&#243;j"
  ]
  node [
    id 39
    label "state"
  ]
  node [
    id 40
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 41
    label "samopoczucie"
  ]
  node [
    id 42
    label "fondness"
  ]
  node [
    id 43
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 44
    label "odwa&#322;"
  ]
  node [
    id 45
    label "chody"
  ]
  node [
    id 46
    label "grodzisko"
  ]
  node [
    id 47
    label "budowa"
  ]
  node [
    id 48
    label "kopniak"
  ]
  node [
    id 49
    label "wyrobisko"
  ]
  node [
    id 50
    label "zrzutowy"
  ]
  node [
    id 51
    label "szaniec"
  ]
  node [
    id 52
    label "odk&#322;ad"
  ]
  node [
    id 53
    label "wg&#322;&#281;bienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
]
