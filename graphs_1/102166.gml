graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.076923076923077
  density 0.04072398190045249
  graphCliqueNumber 3
  node [
    id 0
    label "centrum"
    origin "text"
  ]
  node [
    id 1
    label "zarz&#261;dzanie"
    origin "text"
  ]
  node [
    id 2
    label "kryzysowy"
    origin "text"
  ]
  node [
    id 3
    label "urz&#261;d"
    origin "text"
  ]
  node [
    id 4
    label "miejski"
    origin "text"
  ]
  node [
    id 5
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 6
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "miejsce"
  ]
  node [
    id 8
    label "centroprawica"
  ]
  node [
    id 9
    label "core"
  ]
  node [
    id 10
    label "Hollywood"
  ]
  node [
    id 11
    label "centrolew"
  ]
  node [
    id 12
    label "blok"
  ]
  node [
    id 13
    label "sejm"
  ]
  node [
    id 14
    label "punkt"
  ]
  node [
    id 15
    label "o&#347;rodek"
  ]
  node [
    id 16
    label "nauka_ekonomiczna"
  ]
  node [
    id 17
    label "polecanie"
  ]
  node [
    id 18
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 19
    label "dominance"
  ]
  node [
    id 20
    label "trzymanie"
  ]
  node [
    id 21
    label "dawanie"
  ]
  node [
    id 22
    label "nauka_humanistyczna"
  ]
  node [
    id 23
    label "commission"
  ]
  node [
    id 24
    label "pozarz&#261;dzanie"
  ]
  node [
    id 25
    label "dysponowanie"
  ]
  node [
    id 26
    label "z&#322;y"
  ]
  node [
    id 27
    label "trudny"
  ]
  node [
    id 28
    label "organ"
  ]
  node [
    id 29
    label "w&#322;adza"
  ]
  node [
    id 30
    label "instytucja"
  ]
  node [
    id 31
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 32
    label "mianowaniec"
  ]
  node [
    id 33
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 34
    label "stanowisko"
  ]
  node [
    id 35
    label "position"
  ]
  node [
    id 36
    label "dzia&#322;"
  ]
  node [
    id 37
    label "siedziba"
  ]
  node [
    id 38
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 39
    label "okienko"
  ]
  node [
    id 40
    label "miejsko"
  ]
  node [
    id 41
    label "miastowy"
  ]
  node [
    id 42
    label "typowy"
  ]
  node [
    id 43
    label "publiczny"
  ]
  node [
    id 44
    label "komunikowa&#263;"
  ]
  node [
    id 45
    label "powiada&#263;"
  ]
  node [
    id 46
    label "inform"
  ]
  node [
    id 47
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 48
    label "Wroc&#322;aw"
  ]
  node [
    id 49
    label "biuro"
  ]
  node [
    id 50
    label "prognoza"
  ]
  node [
    id 51
    label "meteorologiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 50
    target 51
  ]
]
