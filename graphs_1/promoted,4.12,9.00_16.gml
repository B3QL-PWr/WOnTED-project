graph [
  maxDegree 30
  minDegree 1
  meanDegree 2.026315789473684
  density 0.027017543859649124
  graphCliqueNumber 3
  node [
    id 0
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 1
    label "cenny"
    origin "text"
  ]
  node [
    id 2
    label "pami&#261;tka"
    origin "text"
  ]
  node [
    id 3
    label "kalendarz"
    origin "text"
  ]
  node [
    id 4
    label "wyda&#263;"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "krak"
    origin "text"
  ]
  node [
    id 7
    label "prawdopodobnie"
    origin "text"
  ]
  node [
    id 8
    label "przez"
    origin "text"
  ]
  node [
    id 9
    label "przedstawienie"
  ]
  node [
    id 10
    label "pokazywa&#263;"
  ]
  node [
    id 11
    label "zapoznawa&#263;"
  ]
  node [
    id 12
    label "typify"
  ]
  node [
    id 13
    label "opisywa&#263;"
  ]
  node [
    id 14
    label "teatr"
  ]
  node [
    id 15
    label "podawa&#263;"
  ]
  node [
    id 16
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 17
    label "demonstrowa&#263;"
  ]
  node [
    id 18
    label "represent"
  ]
  node [
    id 19
    label "ukazywa&#263;"
  ]
  node [
    id 20
    label "attest"
  ]
  node [
    id 21
    label "exhibit"
  ]
  node [
    id 22
    label "stanowi&#263;"
  ]
  node [
    id 23
    label "zg&#322;asza&#263;"
  ]
  node [
    id 24
    label "display"
  ]
  node [
    id 25
    label "warto&#347;ciowy"
  ]
  node [
    id 26
    label "cennie"
  ]
  node [
    id 27
    label "drogi"
  ]
  node [
    id 28
    label "wa&#380;ny"
  ]
  node [
    id 29
    label "&#347;wiadectwo"
  ]
  node [
    id 30
    label "przedmiot"
  ]
  node [
    id 31
    label "almanac"
  ]
  node [
    id 32
    label "wydawnictwo"
  ]
  node [
    id 33
    label "rozk&#322;ad"
  ]
  node [
    id 34
    label "rachuba_czasu"
  ]
  node [
    id 35
    label "Juliusz_Cezar"
  ]
  node [
    id 36
    label "impart"
  ]
  node [
    id 37
    label "panna_na_wydaniu"
  ]
  node [
    id 38
    label "translate"
  ]
  node [
    id 39
    label "give"
  ]
  node [
    id 40
    label "pieni&#261;dze"
  ]
  node [
    id 41
    label "supply"
  ]
  node [
    id 42
    label "wprowadzi&#263;"
  ]
  node [
    id 43
    label "da&#263;"
  ]
  node [
    id 44
    label "zapach"
  ]
  node [
    id 45
    label "powierzy&#263;"
  ]
  node [
    id 46
    label "produkcja"
  ]
  node [
    id 47
    label "poda&#263;"
  ]
  node [
    id 48
    label "skojarzy&#263;"
  ]
  node [
    id 49
    label "dress"
  ]
  node [
    id 50
    label "plon"
  ]
  node [
    id 51
    label "ujawni&#263;"
  ]
  node [
    id 52
    label "reszta"
  ]
  node [
    id 53
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 54
    label "zadenuncjowa&#263;"
  ]
  node [
    id 55
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 56
    label "zrobi&#263;"
  ]
  node [
    id 57
    label "tajemnica"
  ]
  node [
    id 58
    label "wiano"
  ]
  node [
    id 59
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 60
    label "wytworzy&#263;"
  ]
  node [
    id 61
    label "d&#378;wi&#281;k"
  ]
  node [
    id 62
    label "picture"
  ]
  node [
    id 63
    label "stulecie"
  ]
  node [
    id 64
    label "czas"
  ]
  node [
    id 65
    label "pora_roku"
  ]
  node [
    id 66
    label "cykl_astronomiczny"
  ]
  node [
    id 67
    label "p&#243;&#322;rocze"
  ]
  node [
    id 68
    label "grupa"
  ]
  node [
    id 69
    label "kwarta&#322;"
  ]
  node [
    id 70
    label "kurs"
  ]
  node [
    id 71
    label "jubileusz"
  ]
  node [
    id 72
    label "miesi&#261;c"
  ]
  node [
    id 73
    label "lata"
  ]
  node [
    id 74
    label "martwy_sezon"
  ]
  node [
    id 75
    label "realnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
]
