graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "bon"
    origin "text"
  ]
  node [
    id 1
    label "komunalny"
    origin "text"
  ]
  node [
    id 2
    label "dokument"
  ]
  node [
    id 3
    label "voucher"
  ]
  node [
    id 4
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 5
    label "komunalizowanie"
  ]
  node [
    id 6
    label "miejski"
  ]
  node [
    id 7
    label "skomunalizowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
]
