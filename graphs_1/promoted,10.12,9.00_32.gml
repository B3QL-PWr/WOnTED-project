graph [
  maxDegree 16
  minDegree 1
  meanDegree 2
  density 0.03389830508474576
  graphCliqueNumber 2
  node [
    id 0
    label "kupa"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "normalna"
    origin "text"
  ]
  node [
    id 3
    label "krok"
    origin "text"
  ]
  node [
    id 4
    label "zdrowy"
    origin "text"
  ]
  node [
    id 5
    label "wypr&#243;&#380;nia&#263;"
    origin "text"
  ]
  node [
    id 6
    label "balas"
  ]
  node [
    id 7
    label "stool"
  ]
  node [
    id 8
    label "g&#243;wno"
  ]
  node [
    id 9
    label "tragedia"
  ]
  node [
    id 10
    label "koprofilia"
  ]
  node [
    id 11
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 12
    label "wydalina"
  ]
  node [
    id 13
    label "fekalia"
  ]
  node [
    id 14
    label "mn&#243;stwo"
  ]
  node [
    id 15
    label "odchody"
  ]
  node [
    id 16
    label "kszta&#322;t"
  ]
  node [
    id 17
    label "knoll"
  ]
  node [
    id 18
    label "si&#281;ga&#263;"
  ]
  node [
    id 19
    label "trwa&#263;"
  ]
  node [
    id 20
    label "obecno&#347;&#263;"
  ]
  node [
    id 21
    label "stan"
  ]
  node [
    id 22
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 23
    label "stand"
  ]
  node [
    id 24
    label "mie&#263;_miejsce"
  ]
  node [
    id 25
    label "uczestniczy&#263;"
  ]
  node [
    id 26
    label "chodzi&#263;"
  ]
  node [
    id 27
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 28
    label "equal"
  ]
  node [
    id 29
    label "prosta"
  ]
  node [
    id 30
    label "pace"
  ]
  node [
    id 31
    label "czyn"
  ]
  node [
    id 32
    label "passus"
  ]
  node [
    id 33
    label "measurement"
  ]
  node [
    id 34
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 35
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 36
    label "ruch"
  ]
  node [
    id 37
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 38
    label "action"
  ]
  node [
    id 39
    label "tu&#322;&#243;w"
  ]
  node [
    id 40
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 41
    label "skejt"
  ]
  node [
    id 42
    label "step"
  ]
  node [
    id 43
    label "zdrowo"
  ]
  node [
    id 44
    label "cz&#322;owiek"
  ]
  node [
    id 45
    label "wyzdrowienie"
  ]
  node [
    id 46
    label "uzdrowienie"
  ]
  node [
    id 47
    label "solidny"
  ]
  node [
    id 48
    label "silny"
  ]
  node [
    id 49
    label "korzystny"
  ]
  node [
    id 50
    label "rozs&#261;dny"
  ]
  node [
    id 51
    label "zdrowienie"
  ]
  node [
    id 52
    label "dobry"
  ]
  node [
    id 53
    label "wyleczenie_si&#281;"
  ]
  node [
    id 54
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 55
    label "uzdrawianie"
  ]
  node [
    id 56
    label "normalny"
  ]
  node [
    id 57
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 58
    label "wyjmowa&#263;"
  ]
  node [
    id 59
    label "authorize"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
]
