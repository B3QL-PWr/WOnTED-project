graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.130434782608696
  density 0.023411371237458192
  graphCliqueNumber 5
  node [
    id 0
    label "za&#322;oga"
    origin "text"
  ]
  node [
    id 1
    label "kopalnia"
    origin "text"
  ]
  node [
    id 2
    label "w&#281;giel"
    origin "text"
  ]
  node [
    id 3
    label "kamienny"
    origin "text"
  ]
  node [
    id 4
    label "brzeszcze"
    origin "text"
  ]
  node [
    id 5
    label "protestowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przeciwko"
    origin "text"
  ]
  node [
    id 7
    label "obni&#380;enie"
    origin "text"
  ]
  node [
    id 8
    label "nagroda"
    origin "text"
  ]
  node [
    id 9
    label "okazja"
    origin "text"
  ]
  node [
    id 10
    label "barb&#243;rka"
    origin "text"
  ]
  node [
    id 11
    label "dublet"
  ]
  node [
    id 12
    label "persona&#322;"
  ]
  node [
    id 13
    label "wachta"
  ]
  node [
    id 14
    label "grupa"
  ]
  node [
    id 15
    label "zesp&#243;&#322;"
  ]
  node [
    id 16
    label "force"
  ]
  node [
    id 17
    label "za&#322;ogant"
  ]
  node [
    id 18
    label "brygada"
  ]
  node [
    id 19
    label "hala"
  ]
  node [
    id 20
    label "klatka"
  ]
  node [
    id 21
    label "g&#243;rnik"
  ]
  node [
    id 22
    label "gmina_g&#243;rnicza"
  ]
  node [
    id 23
    label "zag&#322;&#281;bie"
  ]
  node [
    id 24
    label "bicie"
  ]
  node [
    id 25
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 26
    label "lutnioci&#261;g"
  ]
  node [
    id 27
    label "w&#281;giel_kopalny"
  ]
  node [
    id 28
    label "cechownia"
  ]
  node [
    id 29
    label "miejsce_pracy"
  ]
  node [
    id 30
    label "mina"
  ]
  node [
    id 31
    label "za&#322;adownia"
  ]
  node [
    id 32
    label "ucinka"
  ]
  node [
    id 33
    label "wyrobisko"
  ]
  node [
    id 34
    label "surowiec_energetyczny"
  ]
  node [
    id 35
    label "w&#281;glowiec"
  ]
  node [
    id 36
    label "w&#281;glowodan"
  ]
  node [
    id 37
    label "kopalina_podstawowa"
  ]
  node [
    id 38
    label "coal"
  ]
  node [
    id 39
    label "przybory_do_pisania"
  ]
  node [
    id 40
    label "makroelement"
  ]
  node [
    id 41
    label "niemetal"
  ]
  node [
    id 42
    label "zsypnik"
  ]
  node [
    id 43
    label "w&#281;glarka"
  ]
  node [
    id 44
    label "zw&#281;glenie_si&#281;"
  ]
  node [
    id 45
    label "zw&#281;glanie_si&#281;"
  ]
  node [
    id 46
    label "coil"
  ]
  node [
    id 47
    label "bry&#322;a"
  ]
  node [
    id 48
    label "ska&#322;a"
  ]
  node [
    id 49
    label "carbon"
  ]
  node [
    id 50
    label "fulleren"
  ]
  node [
    id 51
    label "rysunek"
  ]
  node [
    id 52
    label "niewzruszony"
  ]
  node [
    id 53
    label "twardy"
  ]
  node [
    id 54
    label "kamiennie"
  ]
  node [
    id 55
    label "naturalny"
  ]
  node [
    id 56
    label "g&#322;&#281;boki"
  ]
  node [
    id 57
    label "mineralny"
  ]
  node [
    id 58
    label "ch&#322;odny"
  ]
  node [
    id 59
    label "ghaty"
  ]
  node [
    id 60
    label "napastowa&#263;"
  ]
  node [
    id 61
    label "post&#281;powa&#263;"
  ]
  node [
    id 62
    label "anticipate"
  ]
  node [
    id 63
    label "ni&#380;szy"
  ]
  node [
    id 64
    label "czynno&#347;&#263;"
  ]
  node [
    id 65
    label "spowodowanie"
  ]
  node [
    id 66
    label "zabrzmienie"
  ]
  node [
    id 67
    label "miejsce"
  ]
  node [
    id 68
    label "zmniejszenie"
  ]
  node [
    id 69
    label "suspension"
  ]
  node [
    id 70
    label "pad&#243;&#322;"
  ]
  node [
    id 71
    label "niski"
  ]
  node [
    id 72
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 73
    label "snub"
  ]
  node [
    id 74
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 75
    label "kszta&#322;t"
  ]
  node [
    id 76
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 77
    label "return"
  ]
  node [
    id 78
    label "konsekwencja"
  ]
  node [
    id 79
    label "oskar"
  ]
  node [
    id 80
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 81
    label "atrakcyjny"
  ]
  node [
    id 82
    label "oferta"
  ]
  node [
    id 83
    label "adeptness"
  ]
  node [
    id 84
    label "okazka"
  ]
  node [
    id 85
    label "wydarzenie"
  ]
  node [
    id 86
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 87
    label "podw&#243;zka"
  ]
  node [
    id 88
    label "autostop"
  ]
  node [
    id 89
    label "sytuacja"
  ]
  node [
    id 90
    label "impreza"
  ]
  node [
    id 91
    label "W&#281;giel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 10
    target 90
  ]
]
