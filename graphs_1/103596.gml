graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.112676056338028
  density 0.004971002485501243
  graphCliqueNumber 3
  node [
    id 0
    label "dzia&#322;o"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "niemal"
    origin "text"
  ]
  node [
    id 3
    label "dok&#322;adnie"
    origin "text"
  ]
  node [
    id 4
    label "rok"
    origin "text"
  ]
  node [
    id 5
    label "temu"
    origin "text"
  ]
  node [
    id 6
    label "podczas"
    origin "text"
  ]
  node [
    id 7
    label "msza"
    origin "text"
  ]
  node [
    id 8
    label "rezurekcyjny"
    origin "text"
  ]
  node [
    id 9
    label "kazan"
    origin "text"
  ]
  node [
    id 10
    label "wyg&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 12
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 13
    label "przygotowywa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "bierzmowanie"
    origin "text"
  ]
  node [
    id 15
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 16
    label "bardzo"
    origin "text"
  ]
  node [
    id 17
    label "dobre"
    origin "text"
  ]
  node [
    id 18
    label "zdanie"
    origin "text"
  ]
  node [
    id 19
    label "potrafi&#263;"
    origin "text"
  ]
  node [
    id 20
    label "rozmawia&#263;"
    origin "text"
  ]
  node [
    id 21
    label "inteligentnie"
    origin "text"
  ]
  node [
    id 22
    label "m&#322;ode"
    origin "text"
  ]
  node [
    id 23
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 24
    label "poczucie"
    origin "text"
  ]
  node [
    id 25
    label "humor"
    origin "text"
  ]
  node [
    id 26
    label "bez"
    origin "text"
  ]
  node [
    id 27
    label "zb&#281;dny"
    origin "text"
  ]
  node [
    id 28
    label "patos"
    origin "text"
  ]
  node [
    id 29
    label "tymczasem"
    origin "text"
  ]
  node [
    id 30
    label "okaza&#263;"
    origin "text"
  ]
  node [
    id 31
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 32
    label "te&#380;"
    origin "text"
  ]
  node [
    id 33
    label "druga"
    origin "text"
  ]
  node [
    id 34
    label "oblicze"
    origin "text"
  ]
  node [
    id 35
    label "pierwsza"
    origin "text"
  ]
  node [
    id 36
    label "by&#263;"
    origin "text"
  ]
  node [
    id 37
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 38
    label "niepodobny"
    origin "text"
  ]
  node [
    id 39
    label "ale"
    origin "text"
  ]
  node [
    id 40
    label "z&#322;y"
    origin "text"
  ]
  node [
    id 41
    label "przerwa"
    origin "text"
  ]
  node [
    id 42
    label "uderza&#263;"
    origin "text"
  ]
  node [
    id 43
    label "moherowy"
    origin "text"
  ]
  node [
    id 44
    label "ton"
    origin "text"
  ]
  node [
    id 45
    label "niestety"
    origin "text"
  ]
  node [
    id 46
    label "wtedy"
    origin "text"
  ]
  node [
    id 47
    label "jeszcze"
    origin "text"
  ]
  node [
    id 48
    label "my&#347;le&#263;by&#263;"
    origin "text"
  ]
  node [
    id 49
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 50
    label "blog"
    origin "text"
  ]
  node [
    id 51
    label "wszystko"
    origin "text"
  ]
  node [
    id 52
    label "zapami&#281;ta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 53
    label "tylko"
    origin "text"
  ]
  node [
    id 54
    label "zszokowa&#263;"
    origin "text"
  ]
  node [
    id 55
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 56
    label "co&#347;"
    origin "text"
  ]
  node [
    id 57
    label "taki"
    origin "text"
  ]
  node [
    id 58
    label "bateria"
  ]
  node [
    id 59
    label "laweta"
  ]
  node [
    id 60
    label "bro&#324;_artyleryjska"
  ]
  node [
    id 61
    label "bro&#324;"
  ]
  node [
    id 62
    label "oporopowrotnik"
  ]
  node [
    id 63
    label "przedmuchiwacz"
  ]
  node [
    id 64
    label "artyleria"
  ]
  node [
    id 65
    label "waln&#261;&#263;"
  ]
  node [
    id 66
    label "bateria_artylerii"
  ]
  node [
    id 67
    label "cannon"
  ]
  node [
    id 68
    label "punctiliously"
  ]
  node [
    id 69
    label "dok&#322;adny"
  ]
  node [
    id 70
    label "meticulously"
  ]
  node [
    id 71
    label "precyzyjnie"
  ]
  node [
    id 72
    label "rzetelnie"
  ]
  node [
    id 73
    label "stulecie"
  ]
  node [
    id 74
    label "kalendarz"
  ]
  node [
    id 75
    label "czas"
  ]
  node [
    id 76
    label "pora_roku"
  ]
  node [
    id 77
    label "cykl_astronomiczny"
  ]
  node [
    id 78
    label "p&#243;&#322;rocze"
  ]
  node [
    id 79
    label "grupa"
  ]
  node [
    id 80
    label "kwarta&#322;"
  ]
  node [
    id 81
    label "kurs"
  ]
  node [
    id 82
    label "jubileusz"
  ]
  node [
    id 83
    label "miesi&#261;c"
  ]
  node [
    id 84
    label "lata"
  ]
  node [
    id 85
    label "martwy_sezon"
  ]
  node [
    id 86
    label "credo"
  ]
  node [
    id 87
    label "episto&#322;a"
  ]
  node [
    id 88
    label "prezbiter"
  ]
  node [
    id 89
    label "nabo&#380;e&#324;stwo"
  ]
  node [
    id 90
    label "kazanie"
  ]
  node [
    id 91
    label "kanon"
  ]
  node [
    id 92
    label "utw&#243;r"
  ]
  node [
    id 93
    label "sekreta"
  ]
  node [
    id 94
    label "przeistoczenie"
  ]
  node [
    id 95
    label "prefacja"
  ]
  node [
    id 96
    label "ofiarowanie"
  ]
  node [
    id 97
    label "katolicyzm"
  ]
  node [
    id 98
    label "prawos&#322;awie"
  ]
  node [
    id 99
    label "podniesienie"
  ]
  node [
    id 100
    label "confiteor"
  ]
  node [
    id 101
    label "ofertorium"
  ]
  node [
    id 102
    label "czytanie"
  ]
  node [
    id 103
    label "dzie&#322;o"
  ]
  node [
    id 104
    label "kolekta"
  ]
  node [
    id 105
    label "komunia"
  ]
  node [
    id 106
    label "gloria"
  ]
  node [
    id 107
    label "ewangelia"
  ]
  node [
    id 108
    label "Mass"
  ]
  node [
    id 109
    label "talk"
  ]
  node [
    id 110
    label "wypowiada&#263;"
  ]
  node [
    id 111
    label "klecha"
  ]
  node [
    id 112
    label "eklezjasta"
  ]
  node [
    id 113
    label "rozgrzeszanie"
  ]
  node [
    id 114
    label "duszpasterstwo"
  ]
  node [
    id 115
    label "rozgrzesza&#263;"
  ]
  node [
    id 116
    label "kap&#322;an"
  ]
  node [
    id 117
    label "ksi&#281;&#380;a"
  ]
  node [
    id 118
    label "duchowny"
  ]
  node [
    id 119
    label "kol&#281;da"
  ]
  node [
    id 120
    label "seminarzysta"
  ]
  node [
    id 121
    label "pasterz"
  ]
  node [
    id 122
    label "wykonywa&#263;"
  ]
  node [
    id 123
    label "sposobi&#263;"
  ]
  node [
    id 124
    label "arrange"
  ]
  node [
    id 125
    label "pryczy&#263;"
  ]
  node [
    id 126
    label "train"
  ]
  node [
    id 127
    label "robi&#263;"
  ]
  node [
    id 128
    label "wytwarza&#263;"
  ]
  node [
    id 129
    label "szkoli&#263;"
  ]
  node [
    id 130
    label "usposabia&#263;"
  ]
  node [
    id 131
    label "udzielanie"
  ]
  node [
    id 132
    label "robienie"
  ]
  node [
    id 133
    label "czynno&#347;&#263;"
  ]
  node [
    id 134
    label "konfirmacja"
  ]
  node [
    id 135
    label "ratification"
  ]
  node [
    id 136
    label "inicjacja"
  ]
  node [
    id 137
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 138
    label "sakrament"
  ]
  node [
    id 139
    label "confirmation"
  ]
  node [
    id 140
    label "proszek"
  ]
  node [
    id 141
    label "w_chuj"
  ]
  node [
    id 142
    label "attitude"
  ]
  node [
    id 143
    label "system"
  ]
  node [
    id 144
    label "przedstawienie"
  ]
  node [
    id 145
    label "fraza"
  ]
  node [
    id 146
    label "prison_term"
  ]
  node [
    id 147
    label "adjudication"
  ]
  node [
    id 148
    label "przekazanie"
  ]
  node [
    id 149
    label "pass"
  ]
  node [
    id 150
    label "wyra&#380;enie"
  ]
  node [
    id 151
    label "okres"
  ]
  node [
    id 152
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 153
    label "wypowiedzenie"
  ]
  node [
    id 154
    label "konektyw"
  ]
  node [
    id 155
    label "zaliczenie"
  ]
  node [
    id 156
    label "stanowisko"
  ]
  node [
    id 157
    label "powierzenie"
  ]
  node [
    id 158
    label "antylogizm"
  ]
  node [
    id 159
    label "zmuszenie"
  ]
  node [
    id 160
    label "szko&#322;a"
  ]
  node [
    id 161
    label "umie&#263;"
  ]
  node [
    id 162
    label "cope"
  ]
  node [
    id 163
    label "potrafia&#263;"
  ]
  node [
    id 164
    label "posuwa&#263;_si&#281;"
  ]
  node [
    id 165
    label "posun&#261;&#263;_si&#281;"
  ]
  node [
    id 166
    label "can"
  ]
  node [
    id 167
    label "gaworzy&#263;"
  ]
  node [
    id 168
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 169
    label "m&#261;drze"
  ]
  node [
    id 170
    label "dobrze"
  ]
  node [
    id 171
    label "inteligentny"
  ]
  node [
    id 172
    label "zmy&#347;lnie"
  ]
  node [
    id 173
    label "potomstwo"
  ]
  node [
    id 174
    label "organizm"
  ]
  node [
    id 175
    label "m&#322;odziak"
  ]
  node [
    id 176
    label "zwierz&#281;"
  ]
  node [
    id 177
    label "fledgling"
  ]
  node [
    id 178
    label "asymilowa&#263;"
  ]
  node [
    id 179
    label "wapniak"
  ]
  node [
    id 180
    label "dwun&#243;g"
  ]
  node [
    id 181
    label "polifag"
  ]
  node [
    id 182
    label "wz&#243;r"
  ]
  node [
    id 183
    label "profanum"
  ]
  node [
    id 184
    label "hominid"
  ]
  node [
    id 185
    label "homo_sapiens"
  ]
  node [
    id 186
    label "nasada"
  ]
  node [
    id 187
    label "podw&#322;adny"
  ]
  node [
    id 188
    label "ludzko&#347;&#263;"
  ]
  node [
    id 189
    label "os&#322;abianie"
  ]
  node [
    id 190
    label "mikrokosmos"
  ]
  node [
    id 191
    label "portrecista"
  ]
  node [
    id 192
    label "duch"
  ]
  node [
    id 193
    label "oddzia&#322;ywanie"
  ]
  node [
    id 194
    label "g&#322;owa"
  ]
  node [
    id 195
    label "asymilowanie"
  ]
  node [
    id 196
    label "osoba"
  ]
  node [
    id 197
    label "os&#322;abia&#263;"
  ]
  node [
    id 198
    label "figura"
  ]
  node [
    id 199
    label "Adam"
  ]
  node [
    id 200
    label "senior"
  ]
  node [
    id 201
    label "antropochoria"
  ]
  node [
    id 202
    label "posta&#263;"
  ]
  node [
    id 203
    label "zareagowanie"
  ]
  node [
    id 204
    label "opanowanie"
  ]
  node [
    id 205
    label "doznanie"
  ]
  node [
    id 206
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 207
    label "intuition"
  ]
  node [
    id 208
    label "wiedza"
  ]
  node [
    id 209
    label "ekstraspekcja"
  ]
  node [
    id 210
    label "os&#322;upienie"
  ]
  node [
    id 211
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 212
    label "smell"
  ]
  node [
    id 213
    label "zdarzenie_si&#281;"
  ]
  node [
    id 214
    label "feeling"
  ]
  node [
    id 215
    label "stan"
  ]
  node [
    id 216
    label "temper"
  ]
  node [
    id 217
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 218
    label "mechanizm_obronny"
  ]
  node [
    id 219
    label "nastr&#243;j"
  ]
  node [
    id 220
    label "state"
  ]
  node [
    id 221
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 222
    label "samopoczucie"
  ]
  node [
    id 223
    label "fondness"
  ]
  node [
    id 224
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 225
    label "ki&#347;&#263;"
  ]
  node [
    id 226
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 227
    label "krzew"
  ]
  node [
    id 228
    label "pi&#380;maczkowate"
  ]
  node [
    id 229
    label "pestkowiec"
  ]
  node [
    id 230
    label "kwiat"
  ]
  node [
    id 231
    label "owoc"
  ]
  node [
    id 232
    label "oliwkowate"
  ]
  node [
    id 233
    label "ro&#347;lina"
  ]
  node [
    id 234
    label "hy&#263;ka"
  ]
  node [
    id 235
    label "lilac"
  ]
  node [
    id 236
    label "delfinidyna"
  ]
  node [
    id 237
    label "odchodzenie"
  ]
  node [
    id 238
    label "odej&#347;cie"
  ]
  node [
    id 239
    label "nadmiarowy"
  ]
  node [
    id 240
    label "zb&#281;dnie"
  ]
  node [
    id 241
    label "deklamowanie"
  ]
  node [
    id 242
    label "sztuczno&#347;&#263;"
  ]
  node [
    id 243
    label "cecha"
  ]
  node [
    id 244
    label "deklamowa&#263;"
  ]
  node [
    id 245
    label "pathos"
  ]
  node [
    id 246
    label "pompatyczno&#347;&#263;"
  ]
  node [
    id 247
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 248
    label "podnios&#322;o&#347;&#263;"
  ]
  node [
    id 249
    label "styl"
  ]
  node [
    id 250
    label "czasowo"
  ]
  node [
    id 251
    label "pokaza&#263;"
  ]
  node [
    id 252
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 253
    label "testify"
  ]
  node [
    id 254
    label "give"
  ]
  node [
    id 255
    label "czyj&#347;"
  ]
  node [
    id 256
    label "m&#261;&#380;"
  ]
  node [
    id 257
    label "godzina"
  ]
  node [
    id 258
    label "wygl&#261;d"
  ]
  node [
    id 259
    label "comeliness"
  ]
  node [
    id 260
    label "twarz"
  ]
  node [
    id 261
    label "facjata"
  ]
  node [
    id 262
    label "charakter"
  ]
  node [
    id 263
    label "face"
  ]
  node [
    id 264
    label "si&#281;ga&#263;"
  ]
  node [
    id 265
    label "trwa&#263;"
  ]
  node [
    id 266
    label "obecno&#347;&#263;"
  ]
  node [
    id 267
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 268
    label "stand"
  ]
  node [
    id 269
    label "mie&#263;_miejsce"
  ]
  node [
    id 270
    label "uczestniczy&#263;"
  ]
  node [
    id 271
    label "chodzi&#263;"
  ]
  node [
    id 272
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 273
    label "equal"
  ]
  node [
    id 274
    label "daleki"
  ]
  node [
    id 275
    label "d&#322;ugo"
  ]
  node [
    id 276
    label "ruch"
  ]
  node [
    id 277
    label "r&#243;&#380;ny"
  ]
  node [
    id 278
    label "nietypowy"
  ]
  node [
    id 279
    label "niepodobnie"
  ]
  node [
    id 280
    label "piwo"
  ]
  node [
    id 281
    label "zdenerwowany"
  ]
  node [
    id 282
    label "zez&#322;oszczenie"
  ]
  node [
    id 283
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 284
    label "gniewanie"
  ]
  node [
    id 285
    label "niekorzystny"
  ]
  node [
    id 286
    label "niemoralny"
  ]
  node [
    id 287
    label "niegrzeczny"
  ]
  node [
    id 288
    label "pieski"
  ]
  node [
    id 289
    label "negatywny"
  ]
  node [
    id 290
    label "&#378;le"
  ]
  node [
    id 291
    label "syf"
  ]
  node [
    id 292
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 293
    label "sierdzisty"
  ]
  node [
    id 294
    label "z&#322;oszczenie"
  ]
  node [
    id 295
    label "rozgniewanie"
  ]
  node [
    id 296
    label "niepomy&#347;lny"
  ]
  node [
    id 297
    label "pauza"
  ]
  node [
    id 298
    label "miejsce"
  ]
  node [
    id 299
    label "przedzia&#322;"
  ]
  node [
    id 300
    label "dotyka&#263;"
  ]
  node [
    id 301
    label "funkcjonowa&#263;"
  ]
  node [
    id 302
    label "napada&#263;"
  ]
  node [
    id 303
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 304
    label "nast&#281;powa&#263;"
  ]
  node [
    id 305
    label "blend"
  ]
  node [
    id 306
    label "strike"
  ]
  node [
    id 307
    label "woo"
  ]
  node [
    id 308
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 309
    label "ofensywny"
  ]
  node [
    id 310
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 311
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 312
    label "break"
  ]
  node [
    id 313
    label "walczy&#263;"
  ]
  node [
    id 314
    label "go"
  ]
  node [
    id 315
    label "take"
  ]
  node [
    id 316
    label "wstrzeliwa&#263;_si&#281;"
  ]
  node [
    id 317
    label "hopka&#263;"
  ]
  node [
    id 318
    label "startowa&#263;"
  ]
  node [
    id 319
    label "zadawa&#263;"
  ]
  node [
    id 320
    label "uderza&#263;_do_panny"
  ]
  node [
    id 321
    label "rani&#263;"
  ]
  node [
    id 322
    label "konkurowa&#263;"
  ]
  node [
    id 323
    label "sztacha&#263;"
  ]
  node [
    id 324
    label "napierdziela&#263;"
  ]
  node [
    id 325
    label "przypieprza&#263;"
  ]
  node [
    id 326
    label "chop"
  ]
  node [
    id 327
    label "rwa&#263;"
  ]
  node [
    id 328
    label "krytykowa&#263;"
  ]
  node [
    id 329
    label "powodowa&#263;"
  ]
  node [
    id 330
    label "stara&#263;_si&#281;"
  ]
  node [
    id 331
    label "we&#322;niany"
  ]
  node [
    id 332
    label "seria"
  ]
  node [
    id 333
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 334
    label "d&#378;wi&#281;k"
  ]
  node [
    id 335
    label "formality"
  ]
  node [
    id 336
    label "heksachord"
  ]
  node [
    id 337
    label "note"
  ]
  node [
    id 338
    label "akcent"
  ]
  node [
    id 339
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 340
    label "ubarwienie"
  ]
  node [
    id 341
    label "tone"
  ]
  node [
    id 342
    label "rejestr"
  ]
  node [
    id 343
    label "neoproterozoik"
  ]
  node [
    id 344
    label "interwa&#322;"
  ]
  node [
    id 345
    label "wieloton"
  ]
  node [
    id 346
    label "r&#243;&#380;nica"
  ]
  node [
    id 347
    label "kolorystyka"
  ]
  node [
    id 348
    label "modalizm"
  ]
  node [
    id 349
    label "glinka"
  ]
  node [
    id 350
    label "zabarwienie"
  ]
  node [
    id 351
    label "sound"
  ]
  node [
    id 352
    label "repetycja"
  ]
  node [
    id 353
    label "zwyczaj"
  ]
  node [
    id 354
    label "tu&#324;czyk"
  ]
  node [
    id 355
    label "solmizacja"
  ]
  node [
    id 356
    label "jednostka"
  ]
  node [
    id 357
    label "kiedy&#347;"
  ]
  node [
    id 358
    label "ci&#261;gle"
  ]
  node [
    id 359
    label "przywodzenie"
  ]
  node [
    id 360
    label "prowadzanie"
  ]
  node [
    id 361
    label "ukierunkowywanie"
  ]
  node [
    id 362
    label "kszta&#322;towanie"
  ]
  node [
    id 363
    label "poprowadzenie"
  ]
  node [
    id 364
    label "wprowadzanie"
  ]
  node [
    id 365
    label "dysponowanie"
  ]
  node [
    id 366
    label "przeci&#261;ganie"
  ]
  node [
    id 367
    label "doprowadzanie"
  ]
  node [
    id 368
    label "wprowadzenie"
  ]
  node [
    id 369
    label "eksponowanie"
  ]
  node [
    id 370
    label "oprowadzenie"
  ]
  node [
    id 371
    label "trzymanie"
  ]
  node [
    id 372
    label "ta&#324;czenie"
  ]
  node [
    id 373
    label "przeci&#281;cie"
  ]
  node [
    id 374
    label "przewy&#380;szanie"
  ]
  node [
    id 375
    label "prowadzi&#263;"
  ]
  node [
    id 376
    label "aim"
  ]
  node [
    id 377
    label "zwracanie"
  ]
  node [
    id 378
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 379
    label "przecinanie"
  ]
  node [
    id 380
    label "sterowanie"
  ]
  node [
    id 381
    label "drive"
  ]
  node [
    id 382
    label "kre&#347;lenie"
  ]
  node [
    id 383
    label "management"
  ]
  node [
    id 384
    label "dawanie"
  ]
  node [
    id 385
    label "oprowadzanie"
  ]
  node [
    id 386
    label "pozarz&#261;dzanie"
  ]
  node [
    id 387
    label "g&#243;rowanie"
  ]
  node [
    id 388
    label "linia_melodyczna"
  ]
  node [
    id 389
    label "granie"
  ]
  node [
    id 390
    label "doprowadzenie"
  ]
  node [
    id 391
    label "kierowanie"
  ]
  node [
    id 392
    label "zaprowadzanie"
  ]
  node [
    id 393
    label "lead"
  ]
  node [
    id 394
    label "powodowanie"
  ]
  node [
    id 395
    label "krzywa"
  ]
  node [
    id 396
    label "komcio"
  ]
  node [
    id 397
    label "strona"
  ]
  node [
    id 398
    label "blogosfera"
  ]
  node [
    id 399
    label "pami&#281;tnik"
  ]
  node [
    id 400
    label "lock"
  ]
  node [
    id 401
    label "absolut"
  ]
  node [
    id 402
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 403
    label "oburzy&#263;"
  ]
  node [
    id 404
    label "przerazi&#263;"
  ]
  node [
    id 405
    label "zdziwi&#263;"
  ]
  node [
    id 406
    label "wzbudzi&#263;"
  ]
  node [
    id 407
    label "shock"
  ]
  node [
    id 408
    label "poruszy&#263;"
  ]
  node [
    id 409
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 410
    label "express"
  ]
  node [
    id 411
    label "rzekn&#261;&#263;"
  ]
  node [
    id 412
    label "okre&#347;li&#263;"
  ]
  node [
    id 413
    label "wyrazi&#263;"
  ]
  node [
    id 414
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 415
    label "unwrap"
  ]
  node [
    id 416
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 417
    label "convey"
  ]
  node [
    id 418
    label "discover"
  ]
  node [
    id 419
    label "wydoby&#263;"
  ]
  node [
    id 420
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 421
    label "poda&#263;"
  ]
  node [
    id 422
    label "thing"
  ]
  node [
    id 423
    label "cosik"
  ]
  node [
    id 424
    label "okre&#347;lony"
  ]
  node [
    id 425
    label "jaki&#347;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 36
  ]
  edge [
    source 16
    target 37
  ]
  edge [
    source 16
    target 53
  ]
  edge [
    source 16
    target 54
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 109
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 48
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 22
    target 174
  ]
  edge [
    source 22
    target 175
  ]
  edge [
    source 22
    target 176
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 178
  ]
  edge [
    source 23
    target 179
  ]
  edge [
    source 23
    target 180
  ]
  edge [
    source 23
    target 181
  ]
  edge [
    source 23
    target 182
  ]
  edge [
    source 23
    target 183
  ]
  edge [
    source 23
    target 184
  ]
  edge [
    source 23
    target 185
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 23
    target 187
  ]
  edge [
    source 23
    target 188
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 34
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 41
  ]
  edge [
    source 26
    target 225
  ]
  edge [
    source 26
    target 226
  ]
  edge [
    source 26
    target 227
  ]
  edge [
    source 26
    target 228
  ]
  edge [
    source 26
    target 229
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 237
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 239
  ]
  edge [
    source 27
    target 240
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 241
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 250
  ]
  edge [
    source 29
    target 46
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 255
  ]
  edge [
    source 31
    target 256
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 257
  ]
  edge [
    source 34
    target 258
  ]
  edge [
    source 34
    target 259
  ]
  edge [
    source 34
    target 260
  ]
  edge [
    source 34
    target 261
  ]
  edge [
    source 34
    target 262
  ]
  edge [
    source 34
    target 263
  ]
  edge [
    source 34
    target 54
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 257
  ]
  edge [
    source 36
    target 264
  ]
  edge [
    source 36
    target 265
  ]
  edge [
    source 36
    target 266
  ]
  edge [
    source 36
    target 215
  ]
  edge [
    source 36
    target 267
  ]
  edge [
    source 36
    target 268
  ]
  edge [
    source 36
    target 269
  ]
  edge [
    source 36
    target 270
  ]
  edge [
    source 36
    target 271
  ]
  edge [
    source 36
    target 272
  ]
  edge [
    source 36
    target 273
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 274
  ]
  edge [
    source 37
    target 275
  ]
  edge [
    source 37
    target 276
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 277
  ]
  edge [
    source 38
    target 278
  ]
  edge [
    source 38
    target 279
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 280
  ]
  edge [
    source 40
    target 281
  ]
  edge [
    source 40
    target 282
  ]
  edge [
    source 40
    target 283
  ]
  edge [
    source 40
    target 284
  ]
  edge [
    source 40
    target 285
  ]
  edge [
    source 40
    target 286
  ]
  edge [
    source 40
    target 287
  ]
  edge [
    source 40
    target 288
  ]
  edge [
    source 40
    target 289
  ]
  edge [
    source 40
    target 290
  ]
  edge [
    source 40
    target 291
  ]
  edge [
    source 40
    target 292
  ]
  edge [
    source 40
    target 293
  ]
  edge [
    source 40
    target 294
  ]
  edge [
    source 40
    target 295
  ]
  edge [
    source 40
    target 296
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 297
  ]
  edge [
    source 41
    target 298
  ]
  edge [
    source 41
    target 299
  ]
  edge [
    source 41
    target 75
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 300
  ]
  edge [
    source 42
    target 301
  ]
  edge [
    source 42
    target 302
  ]
  edge [
    source 42
    target 303
  ]
  edge [
    source 42
    target 304
  ]
  edge [
    source 42
    target 305
  ]
  edge [
    source 42
    target 306
  ]
  edge [
    source 42
    target 307
  ]
  edge [
    source 42
    target 308
  ]
  edge [
    source 42
    target 309
  ]
  edge [
    source 42
    target 310
  ]
  edge [
    source 42
    target 311
  ]
  edge [
    source 42
    target 312
  ]
  edge [
    source 42
    target 313
  ]
  edge [
    source 42
    target 314
  ]
  edge [
    source 42
    target 315
  ]
  edge [
    source 42
    target 316
  ]
  edge [
    source 42
    target 317
  ]
  edge [
    source 42
    target 318
  ]
  edge [
    source 42
    target 269
  ]
  edge [
    source 42
    target 319
  ]
  edge [
    source 42
    target 320
  ]
  edge [
    source 42
    target 127
  ]
  edge [
    source 42
    target 321
  ]
  edge [
    source 42
    target 322
  ]
  edge [
    source 42
    target 323
  ]
  edge [
    source 42
    target 324
  ]
  edge [
    source 42
    target 325
  ]
  edge [
    source 42
    target 326
  ]
  edge [
    source 42
    target 327
  ]
  edge [
    source 42
    target 328
  ]
  edge [
    source 42
    target 329
  ]
  edge [
    source 42
    target 330
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 331
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 332
  ]
  edge [
    source 44
    target 333
  ]
  edge [
    source 44
    target 334
  ]
  edge [
    source 44
    target 335
  ]
  edge [
    source 44
    target 336
  ]
  edge [
    source 44
    target 337
  ]
  edge [
    source 44
    target 338
  ]
  edge [
    source 44
    target 339
  ]
  edge [
    source 44
    target 340
  ]
  edge [
    source 44
    target 341
  ]
  edge [
    source 44
    target 342
  ]
  edge [
    source 44
    target 343
  ]
  edge [
    source 44
    target 344
  ]
  edge [
    source 44
    target 345
  ]
  edge [
    source 44
    target 243
  ]
  edge [
    source 44
    target 346
  ]
  edge [
    source 44
    target 347
  ]
  edge [
    source 44
    target 348
  ]
  edge [
    source 44
    target 349
  ]
  edge [
    source 44
    target 350
  ]
  edge [
    source 44
    target 351
  ]
  edge [
    source 44
    target 352
  ]
  edge [
    source 44
    target 353
  ]
  edge [
    source 44
    target 354
  ]
  edge [
    source 44
    target 355
  ]
  edge [
    source 44
    target 356
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 357
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 358
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 132
  ]
  edge [
    source 49
    target 359
  ]
  edge [
    source 49
    target 360
  ]
  edge [
    source 49
    target 361
  ]
  edge [
    source 49
    target 362
  ]
  edge [
    source 49
    target 363
  ]
  edge [
    source 49
    target 364
  ]
  edge [
    source 49
    target 365
  ]
  edge [
    source 49
    target 366
  ]
  edge [
    source 49
    target 367
  ]
  edge [
    source 49
    target 368
  ]
  edge [
    source 49
    target 369
  ]
  edge [
    source 49
    target 370
  ]
  edge [
    source 49
    target 371
  ]
  edge [
    source 49
    target 372
  ]
  edge [
    source 49
    target 373
  ]
  edge [
    source 49
    target 374
  ]
  edge [
    source 49
    target 375
  ]
  edge [
    source 49
    target 376
  ]
  edge [
    source 49
    target 133
  ]
  edge [
    source 49
    target 377
  ]
  edge [
    source 49
    target 378
  ]
  edge [
    source 49
    target 379
  ]
  edge [
    source 49
    target 380
  ]
  edge [
    source 49
    target 381
  ]
  edge [
    source 49
    target 382
  ]
  edge [
    source 49
    target 383
  ]
  edge [
    source 49
    target 384
  ]
  edge [
    source 49
    target 385
  ]
  edge [
    source 49
    target 386
  ]
  edge [
    source 49
    target 387
  ]
  edge [
    source 49
    target 388
  ]
  edge [
    source 49
    target 389
  ]
  edge [
    source 49
    target 390
  ]
  edge [
    source 49
    target 391
  ]
  edge [
    source 49
    target 392
  ]
  edge [
    source 49
    target 393
  ]
  edge [
    source 49
    target 394
  ]
  edge [
    source 49
    target 395
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 396
  ]
  edge [
    source 50
    target 397
  ]
  edge [
    source 50
    target 398
  ]
  edge [
    source 50
    target 399
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 400
  ]
  edge [
    source 51
    target 401
  ]
  edge [
    source 51
    target 402
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 403
  ]
  edge [
    source 54
    target 404
  ]
  edge [
    source 54
    target 405
  ]
  edge [
    source 54
    target 406
  ]
  edge [
    source 54
    target 407
  ]
  edge [
    source 54
    target 408
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 409
  ]
  edge [
    source 55
    target 410
  ]
  edge [
    source 55
    target 411
  ]
  edge [
    source 55
    target 412
  ]
  edge [
    source 55
    target 413
  ]
  edge [
    source 55
    target 414
  ]
  edge [
    source 55
    target 415
  ]
  edge [
    source 55
    target 416
  ]
  edge [
    source 55
    target 417
  ]
  edge [
    source 55
    target 418
  ]
  edge [
    source 55
    target 419
  ]
  edge [
    source 55
    target 420
  ]
  edge [
    source 55
    target 421
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 422
  ]
  edge [
    source 56
    target 423
  ]
  edge [
    source 57
    target 424
  ]
  edge [
    source 57
    target 425
  ]
]
