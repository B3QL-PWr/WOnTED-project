graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.6521739130434783
  density 0.07509881422924901
  graphCliqueNumber 3
  node [
    id 0
    label "&#322;azar"
    origin "text"
  ]
  node [
    id 1
    label "kaganowicz"
    origin "text"
  ]
  node [
    id 2
    label "&#321;azar"
  ]
  node [
    id 3
    label "Kaganowicz"
  ]
  node [
    id 4
    label "Mojsiejewicz"
  ]
  node [
    id 5
    label "&#1051;&#1072;&#1079;&#1072;&#1088;&#1100;"
  ]
  node [
    id 6
    label "&#1052;&#1086;&#1080;&#1089;&#1077;&#1077;&#1074;&#1080;&#1095;"
  ]
  node [
    id 7
    label "&#1050;&#1072;&#1075;&#1072;&#1085;&#1086;&#1074;&#1080;&#1095;"
  ]
  node [
    id 8
    label "komitet"
  ]
  node [
    id 9
    label "centralny"
  ]
  node [
    id 10
    label "biuro"
  ]
  node [
    id 11
    label "polityczny"
  ]
  node [
    id 12
    label "komunistyczny"
  ]
  node [
    id 13
    label "partia"
  ]
  node [
    id 14
    label "Rosja"
  ]
  node [
    id 15
    label "cerkiew"
  ]
  node [
    id 16
    label "Chrystus"
  ]
  node [
    id 17
    label "zbawiciel"
  ]
  node [
    id 18
    label "&#1061;&#1088;&#1072;&#1084;"
  ]
  node [
    id 19
    label "&#1061;&#1088;&#1080;&#1089;&#1090;&#1072;"
  ]
  node [
    id 20
    label "&#1057;&#1087;&#1072;&#1089;&#1080;&#1090;&#1077;&#1083;&#1103;"
  ]
  node [
    id 21
    label "zbrodnia"
  ]
  node [
    id 22
    label "katy&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
]
