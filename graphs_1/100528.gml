graph [
  maxDegree 9
  minDegree 1
  meanDegree 2
  density 0.11764705882352941
  graphCliqueNumber 4
  node [
    id 0
    label "biberach"
    origin "text"
  ]
  node [
    id 1
    label "ri&#223;"
    origin "text"
  ]
  node [
    id 2
    label "stacja"
    origin "text"
  ]
  node [
    id 3
    label "kolejowy"
    origin "text"
  ]
  node [
    id 4
    label "miejsce"
  ]
  node [
    id 5
    label "instytucja"
  ]
  node [
    id 6
    label "droga_krzy&#380;owa"
  ]
  node [
    id 7
    label "punkt"
  ]
  node [
    id 8
    label "urz&#261;dzenie"
  ]
  node [
    id 9
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 10
    label "siedziba"
  ]
  node [
    id 11
    label "komunikacyjny"
  ]
  node [
    id 12
    label "Biberach"
  ]
  node [
    id 13
    label "Ri&#223;"
  ]
  node [
    id 14
    label "Badenia"
  ]
  node [
    id 15
    label "Wirtembergia"
  ]
  node [
    id 16
    label "an"
  ]
  node [
    id 17
    label "dera"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
]
