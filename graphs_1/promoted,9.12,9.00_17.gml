graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9591836734693877
  density 0.04081632653061224
  graphCliqueNumber 2
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "pot&#281;&#380;ny"
    origin "text"
  ]
  node [
    id 2
    label "warownia"
    origin "text"
  ]
  node [
    id 3
    label "kresowy"
    origin "text"
  ]
  node [
    id 4
    label "rzeczpospolita"
    origin "text"
  ]
  node [
    id 5
    label "kamieniec"
    origin "text"
  ]
  node [
    id 6
    label "podolski"
    origin "text"
  ]
  node [
    id 7
    label "ogromny"
  ]
  node [
    id 8
    label "pot&#281;&#380;nie"
  ]
  node [
    id 9
    label "konkretny"
  ]
  node [
    id 10
    label "ogromnie"
  ]
  node [
    id 11
    label "wielki"
  ]
  node [
    id 12
    label "okaza&#322;y"
  ]
  node [
    id 13
    label "ros&#322;y"
  ]
  node [
    id 14
    label "mocny"
  ]
  node [
    id 15
    label "wielow&#322;adny"
  ]
  node [
    id 16
    label "twierdza"
  ]
  node [
    id 17
    label "wschodni"
  ]
  node [
    id 18
    label "blin"
  ]
  node [
    id 19
    label "warenik"
  ]
  node [
    id 20
    label "Buriacja"
  ]
  node [
    id 21
    label "Abchazja"
  ]
  node [
    id 22
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 23
    label "Inguszetia"
  ]
  node [
    id 24
    label "Nachiczewan"
  ]
  node [
    id 25
    label "Karaka&#322;pacja"
  ]
  node [
    id 26
    label "Jakucja"
  ]
  node [
    id 27
    label "Singapur"
  ]
  node [
    id 28
    label "Karelia"
  ]
  node [
    id 29
    label "Komi"
  ]
  node [
    id 30
    label "Tatarstan"
  ]
  node [
    id 31
    label "Chakasja"
  ]
  node [
    id 32
    label "Dagestan"
  ]
  node [
    id 33
    label "Mordowia"
  ]
  node [
    id 34
    label "Ka&#322;mucja"
  ]
  node [
    id 35
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 36
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 37
    label "Baszkiria"
  ]
  node [
    id 38
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 39
    label "Mari_El"
  ]
  node [
    id 40
    label "Ad&#380;aria"
  ]
  node [
    id 41
    label "Czuwaszja"
  ]
  node [
    id 42
    label "Tuwa"
  ]
  node [
    id 43
    label "Czeczenia"
  ]
  node [
    id 44
    label "Udmurcja"
  ]
  node [
    id 45
    label "pa&#324;stwo"
  ]
  node [
    id 46
    label "zwa&#322;"
  ]
  node [
    id 47
    label "ukrai&#324;ski"
  ]
  node [
    id 48
    label "po_podolsku"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
]
