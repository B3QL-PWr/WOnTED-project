graph [
  maxDegree 16
  minDegree 1
  meanDegree 2.072289156626506
  density 0.025271818983250073
  graphCliqueNumber 4
  node [
    id 0
    label "d&#261;b"
    origin "text"
  ]
  node [
    id 1
    label "j&#243;zef"
    origin "text"
  ]
  node [
    id 2
    label "wi&#347;niowy"
    origin "text"
  ]
  node [
    id 3
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 4
    label "podkarpacki"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "rewers"
    origin "text"
  ]
  node [
    id 9
    label "banknot"
    origin "text"
  ]
  node [
    id 10
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 11
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 12
    label "bukowate"
  ]
  node [
    id 13
    label "owoc_rzekomy"
  ]
  node [
    id 14
    label "drewno"
  ]
  node [
    id 15
    label "gr&#261;d"
  ]
  node [
    id 16
    label "d&#281;bina"
  ]
  node [
    id 17
    label "&#380;o&#322;&#261;d&#378;"
  ]
  node [
    id 18
    label "owocowy"
  ]
  node [
    id 19
    label "ciemnoczerwony"
  ]
  node [
    id 20
    label "drewniany"
  ]
  node [
    id 21
    label "aromatyczny"
  ]
  node [
    id 22
    label "kriek"
  ]
  node [
    id 23
    label "wi&#347;niowo"
  ]
  node [
    id 24
    label "jednostka_administracyjna"
  ]
  node [
    id 25
    label "makroregion"
  ]
  node [
    id 26
    label "powiat"
  ]
  node [
    id 27
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 28
    label "mikroregion"
  ]
  node [
    id 29
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 30
    label "pa&#324;stwo"
  ]
  node [
    id 31
    label "polski"
  ]
  node [
    id 32
    label "po_podkarpacku"
  ]
  node [
    id 33
    label "bina"
  ]
  node [
    id 34
    label "regionalny"
  ]
  node [
    id 35
    label "stulecie"
  ]
  node [
    id 36
    label "kalendarz"
  ]
  node [
    id 37
    label "czas"
  ]
  node [
    id 38
    label "pora_roku"
  ]
  node [
    id 39
    label "cykl_astronomiczny"
  ]
  node [
    id 40
    label "p&#243;&#322;rocze"
  ]
  node [
    id 41
    label "grupa"
  ]
  node [
    id 42
    label "kwarta&#322;"
  ]
  node [
    id 43
    label "kurs"
  ]
  node [
    id 44
    label "jubileusz"
  ]
  node [
    id 45
    label "miesi&#261;c"
  ]
  node [
    id 46
    label "lata"
  ]
  node [
    id 47
    label "martwy_sezon"
  ]
  node [
    id 48
    label "odzyska&#263;"
  ]
  node [
    id 49
    label "devise"
  ]
  node [
    id 50
    label "oceni&#263;"
  ]
  node [
    id 51
    label "znaj&#347;&#263;"
  ]
  node [
    id 52
    label "wymy&#347;li&#263;"
  ]
  node [
    id 53
    label "invent"
  ]
  node [
    id 54
    label "pozyska&#263;"
  ]
  node [
    id 55
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 56
    label "wykry&#263;"
  ]
  node [
    id 57
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 58
    label "dozna&#263;"
  ]
  node [
    id 59
    label "odwrotna_strona"
  ]
  node [
    id 60
    label "biblioteka"
  ]
  node [
    id 61
    label "formularz"
  ]
  node [
    id 62
    label "pokwitowanie"
  ]
  node [
    id 63
    label "reszka"
  ]
  node [
    id 64
    label "pieni&#261;dz"
  ]
  node [
    id 65
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 66
    label "szlachetny"
  ]
  node [
    id 67
    label "metaliczny"
  ]
  node [
    id 68
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 69
    label "z&#322;ocenie"
  ]
  node [
    id 70
    label "grosz"
  ]
  node [
    id 71
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 72
    label "utytu&#322;owany"
  ]
  node [
    id 73
    label "poz&#322;ocenie"
  ]
  node [
    id 74
    label "Polska"
  ]
  node [
    id 75
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 76
    label "wspania&#322;y"
  ]
  node [
    id 77
    label "doskona&#322;y"
  ]
  node [
    id 78
    label "kochany"
  ]
  node [
    id 79
    label "jednostka_monetarna"
  ]
  node [
    id 80
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 81
    label "J&#243;zef"
  ]
  node [
    id 82
    label "z"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 81
    target 82
  ]
]
