graph [
  maxDegree 25
  minDegree 1
  meanDegree 2.020408163265306
  density 0.020828950136755734
  graphCliqueNumber 3
  node [
    id 0
    label "belgijski"
    origin "text"
  ]
  node [
    id 1
    label "my&#347;liwiec"
    origin "text"
  ]
  node [
    id 2
    label "przechwyci&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rosyjski"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "lecie&#263;"
    origin "text"
  ]
  node [
    id 6
    label "nad"
    origin "text"
  ]
  node [
    id 7
    label "morze"
    origin "text"
  ]
  node [
    id 8
    label "ba&#322;tycki"
    origin "text"
  ]
  node [
    id 9
    label "bez"
    origin "text"
  ]
  node [
    id 10
    label "zezwolenie"
    origin "text"
  ]
  node [
    id 11
    label "europejski"
  ]
  node [
    id 12
    label "zachodnioeuropejski"
  ]
  node [
    id 13
    label "po_belgijsku"
  ]
  node [
    id 14
    label "eskadra_my&#347;liwska"
  ]
  node [
    id 15
    label "dywizjon_my&#347;liwski"
  ]
  node [
    id 16
    label "samolot_bojowy"
  ]
  node [
    id 17
    label "bang"
  ]
  node [
    id 18
    label "wzi&#261;&#263;"
  ]
  node [
    id 19
    label "catch"
  ]
  node [
    id 20
    label "wykry&#263;"
  ]
  node [
    id 21
    label "po_rosyjsku"
  ]
  node [
    id 22
    label "j&#281;zyk"
  ]
  node [
    id 23
    label "wielkoruski"
  ]
  node [
    id 24
    label "kacapski"
  ]
  node [
    id 25
    label "Russian"
  ]
  node [
    id 26
    label "rusek"
  ]
  node [
    id 27
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 28
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 29
    label "lata&#263;"
  ]
  node [
    id 30
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 31
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 32
    label "fly"
  ]
  node [
    id 33
    label "omdlewa&#263;"
  ]
  node [
    id 34
    label "mie&#263;_miejsce"
  ]
  node [
    id 35
    label "rush"
  ]
  node [
    id 36
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 37
    label "po&#380;&#261;da&#263;"
  ]
  node [
    id 38
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 39
    label "robi&#263;"
  ]
  node [
    id 40
    label "spada&#263;"
  ]
  node [
    id 41
    label "biega&#263;"
  ]
  node [
    id 42
    label "mija&#263;"
  ]
  node [
    id 43
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 44
    label "sterowa&#263;"
  ]
  node [
    id 45
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 46
    label "i&#347;&#263;"
  ]
  node [
    id 47
    label "odchodzi&#263;"
  ]
  node [
    id 48
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 49
    label "Neptun"
  ]
  node [
    id 50
    label "Morze_Bia&#322;e"
  ]
  node [
    id 51
    label "reda"
  ]
  node [
    id 52
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 53
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 54
    label "paliszcze"
  ]
  node [
    id 55
    label "okeanida"
  ]
  node [
    id 56
    label "latarnia_morska"
  ]
  node [
    id 57
    label "zbiornik_wodny"
  ]
  node [
    id 58
    label "Morze_Czerwone"
  ]
  node [
    id 59
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 60
    label "laguna"
  ]
  node [
    id 61
    label "marina"
  ]
  node [
    id 62
    label "talasoterapia"
  ]
  node [
    id 63
    label "Morze_Adriatyckie"
  ]
  node [
    id 64
    label "bezmiar"
  ]
  node [
    id 65
    label "pe&#322;ne_morze"
  ]
  node [
    id 66
    label "Morze_Czarne"
  ]
  node [
    id 67
    label "nereida"
  ]
  node [
    id 68
    label "Ziemia"
  ]
  node [
    id 69
    label "przymorze"
  ]
  node [
    id 70
    label "Morze_Egejskie"
  ]
  node [
    id 71
    label "ki&#347;&#263;"
  ]
  node [
    id 72
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 73
    label "krzew"
  ]
  node [
    id 74
    label "pi&#380;maczkowate"
  ]
  node [
    id 75
    label "pestkowiec"
  ]
  node [
    id 76
    label "kwiat"
  ]
  node [
    id 77
    label "owoc"
  ]
  node [
    id 78
    label "oliwkowate"
  ]
  node [
    id 79
    label "ro&#347;lina"
  ]
  node [
    id 80
    label "hy&#263;ka"
  ]
  node [
    id 81
    label "lilac"
  ]
  node [
    id 82
    label "delfinidyna"
  ]
  node [
    id 83
    label "dokument"
  ]
  node [
    id 84
    label "zrobienie"
  ]
  node [
    id 85
    label "zwolnienie_si&#281;"
  ]
  node [
    id 86
    label "zwalnianie_si&#281;"
  ]
  node [
    id 87
    label "odpowied&#378;"
  ]
  node [
    id 88
    label "wiedza"
  ]
  node [
    id 89
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 90
    label "pozwole&#324;stwo"
  ]
  node [
    id 91
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 92
    label "pofolgowanie"
  ]
  node [
    id 93
    label "decyzja"
  ]
  node [
    id 94
    label "license"
  ]
  node [
    id 95
    label "odwieszenie"
  ]
  node [
    id 96
    label "uznanie"
  ]
  node [
    id 97
    label "authorization"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
]
