graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9672131147540983
  density 0.03278688524590164
  graphCliqueNumber 2
  node [
    id 0
    label "przez"
    origin "text"
  ]
  node [
    id 1
    label "wiele"
    origin "text"
  ]
  node [
    id 2
    label "godzina"
    origin "text"
  ]
  node [
    id 3
    label "poci&#261;g"
    origin "text"
  ]
  node [
    id 4
    label "krak"
    origin "text"
  ]
  node [
    id 5
    label "sta&#322;y"
    origin "text"
  ]
  node [
    id 6
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "peron"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 9
    label "wjecha&#263;"
    origin "text"
  ]
  node [
    id 10
    label "wiela"
  ]
  node [
    id 11
    label "du&#380;y"
  ]
  node [
    id 12
    label "minuta"
  ]
  node [
    id 13
    label "doba"
  ]
  node [
    id 14
    label "czas"
  ]
  node [
    id 15
    label "p&#243;&#322;godzina"
  ]
  node [
    id 16
    label "kwadrans"
  ]
  node [
    id 17
    label "time"
  ]
  node [
    id 18
    label "jednostka_czasu"
  ]
  node [
    id 19
    label "lokomotywa"
  ]
  node [
    id 20
    label "pojazd_kolejowy"
  ]
  node [
    id 21
    label "kolej"
  ]
  node [
    id 22
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 23
    label "tender"
  ]
  node [
    id 24
    label "cug"
  ]
  node [
    id 25
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 26
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 27
    label "wagon"
  ]
  node [
    id 28
    label "zwyk&#322;y"
  ]
  node [
    id 29
    label "jednakowy"
  ]
  node [
    id 30
    label "stale"
  ]
  node [
    id 31
    label "regularny"
  ]
  node [
    id 32
    label "whole"
  ]
  node [
    id 33
    label "Rzym_Zachodni"
  ]
  node [
    id 34
    label "element"
  ]
  node [
    id 35
    label "ilo&#347;&#263;"
  ]
  node [
    id 36
    label "urz&#261;dzenie"
  ]
  node [
    id 37
    label "Rzym_Wschodni"
  ]
  node [
    id 38
    label "stanowisko"
  ]
  node [
    id 39
    label "dworzec"
  ]
  node [
    id 40
    label "by&#263;"
  ]
  node [
    id 41
    label "uprawi&#263;"
  ]
  node [
    id 42
    label "gotowy"
  ]
  node [
    id 43
    label "might"
  ]
  node [
    id 44
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 45
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 46
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 47
    label "wkroczy&#263;"
  ]
  node [
    id 48
    label "zacz&#261;&#263;"
  ]
  node [
    id 49
    label "wsun&#261;&#263;_si&#281;"
  ]
  node [
    id 50
    label "skrytykowa&#263;"
  ]
  node [
    id 51
    label "wpa&#347;&#263;"
  ]
  node [
    id 52
    label "powiedzie&#263;"
  ]
  node [
    id 53
    label "doj&#347;&#263;"
  ]
  node [
    id 54
    label "move"
  ]
  node [
    id 55
    label "wsun&#261;&#263;"
  ]
  node [
    id 56
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 57
    label "spell"
  ]
  node [
    id 58
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 59
    label "przekroczy&#263;"
  ]
  node [
    id 60
    label "intervene"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
]
