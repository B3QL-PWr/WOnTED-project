graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.1444866920152093
  density 0.008185063709981713
  graphCliqueNumber 5
  node [
    id 0
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 1
    label "tym"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "drugi"
    origin "text"
  ]
  node [
    id 4
    label "czytanie"
    origin "text"
  ]
  node [
    id 5
    label "zg&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przed&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 7
    label "projekt"
    origin "text"
  ]
  node [
    id 8
    label "ustawa"
    origin "text"
  ]
  node [
    id 9
    label "poprawka"
    origin "text"
  ]
  node [
    id 10
    label "proponowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "aby"
    origin "text"
  ]
  node [
    id 12
    label "sejm"
    origin "text"
  ]
  node [
    id 13
    label "ponownie"
    origin "text"
  ]
  node [
    id 14
    label "skierowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "ten"
    origin "text"
  ]
  node [
    id 16
    label "komisja"
    origin "text"
  ]
  node [
    id 17
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "prawy"
    origin "text"
  ]
  node [
    id 19
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 20
    label "cela"
    origin "text"
  ]
  node [
    id 21
    label "przedstawienie"
    origin "text"
  ]
  node [
    id 22
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 23
    label "odwodnienie"
  ]
  node [
    id 24
    label "konstytucja"
  ]
  node [
    id 25
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 26
    label "substancja_chemiczna"
  ]
  node [
    id 27
    label "bratnia_dusza"
  ]
  node [
    id 28
    label "zwi&#261;zanie"
  ]
  node [
    id 29
    label "lokant"
  ]
  node [
    id 30
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 31
    label "zwi&#261;za&#263;"
  ]
  node [
    id 32
    label "organizacja"
  ]
  node [
    id 33
    label "odwadnia&#263;"
  ]
  node [
    id 34
    label "marriage"
  ]
  node [
    id 35
    label "marketing_afiliacyjny"
  ]
  node [
    id 36
    label "bearing"
  ]
  node [
    id 37
    label "wi&#261;zanie"
  ]
  node [
    id 38
    label "odwadnianie"
  ]
  node [
    id 39
    label "koligacja"
  ]
  node [
    id 40
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 41
    label "odwodni&#263;"
  ]
  node [
    id 42
    label "azeotrop"
  ]
  node [
    id 43
    label "powi&#261;zanie"
  ]
  node [
    id 44
    label "czasokres"
  ]
  node [
    id 45
    label "trawienie"
  ]
  node [
    id 46
    label "kategoria_gramatyczna"
  ]
  node [
    id 47
    label "period"
  ]
  node [
    id 48
    label "odczyt"
  ]
  node [
    id 49
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 50
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 51
    label "chwila"
  ]
  node [
    id 52
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 53
    label "poprzedzenie"
  ]
  node [
    id 54
    label "koniugacja"
  ]
  node [
    id 55
    label "dzieje"
  ]
  node [
    id 56
    label "poprzedzi&#263;"
  ]
  node [
    id 57
    label "przep&#322;ywanie"
  ]
  node [
    id 58
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 59
    label "odwlekanie_si&#281;"
  ]
  node [
    id 60
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 61
    label "Zeitgeist"
  ]
  node [
    id 62
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 63
    label "okres_czasu"
  ]
  node [
    id 64
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 65
    label "pochodzi&#263;"
  ]
  node [
    id 66
    label "schy&#322;ek"
  ]
  node [
    id 67
    label "czwarty_wymiar"
  ]
  node [
    id 68
    label "chronometria"
  ]
  node [
    id 69
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 70
    label "poprzedzanie"
  ]
  node [
    id 71
    label "pogoda"
  ]
  node [
    id 72
    label "zegar"
  ]
  node [
    id 73
    label "pochodzenie"
  ]
  node [
    id 74
    label "poprzedza&#263;"
  ]
  node [
    id 75
    label "trawi&#263;"
  ]
  node [
    id 76
    label "time_period"
  ]
  node [
    id 77
    label "rachuba_czasu"
  ]
  node [
    id 78
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 79
    label "czasoprzestrze&#324;"
  ]
  node [
    id 80
    label "laba"
  ]
  node [
    id 81
    label "inny"
  ]
  node [
    id 82
    label "kolejny"
  ]
  node [
    id 83
    label "przeciwny"
  ]
  node [
    id 84
    label "sw&#243;j"
  ]
  node [
    id 85
    label "odwrotnie"
  ]
  node [
    id 86
    label "dzie&#324;"
  ]
  node [
    id 87
    label "podobny"
  ]
  node [
    id 88
    label "wt&#243;ry"
  ]
  node [
    id 89
    label "dysleksja"
  ]
  node [
    id 90
    label "wyczytywanie"
  ]
  node [
    id 91
    label "doczytywanie"
  ]
  node [
    id 92
    label "lektor"
  ]
  node [
    id 93
    label "przepowiadanie"
  ]
  node [
    id 94
    label "wczytywanie_si&#281;"
  ]
  node [
    id 95
    label "oczytywanie_si&#281;"
  ]
  node [
    id 96
    label "zaczytanie_si&#281;"
  ]
  node [
    id 97
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 98
    label "wczytywanie"
  ]
  node [
    id 99
    label "obrz&#261;dek"
  ]
  node [
    id 100
    label "czytywanie"
  ]
  node [
    id 101
    label "bycie_w_stanie"
  ]
  node [
    id 102
    label "pokazywanie"
  ]
  node [
    id 103
    label "poznawanie"
  ]
  node [
    id 104
    label "poczytanie"
  ]
  node [
    id 105
    label "Biblia"
  ]
  node [
    id 106
    label "reading"
  ]
  node [
    id 107
    label "recitation"
  ]
  node [
    id 108
    label "report"
  ]
  node [
    id 109
    label "announce"
  ]
  node [
    id 110
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 111
    label "write"
  ]
  node [
    id 112
    label "poinformowa&#263;"
  ]
  node [
    id 113
    label "express"
  ]
  node [
    id 114
    label "translate"
  ]
  node [
    id 115
    label "zaproponowa&#263;"
  ]
  node [
    id 116
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 117
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 118
    label "dokument"
  ]
  node [
    id 119
    label "device"
  ]
  node [
    id 120
    label "program_u&#380;ytkowy"
  ]
  node [
    id 121
    label "intencja"
  ]
  node [
    id 122
    label "agreement"
  ]
  node [
    id 123
    label "pomys&#322;"
  ]
  node [
    id 124
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 125
    label "plan"
  ]
  node [
    id 126
    label "dokumentacja"
  ]
  node [
    id 127
    label "Karta_Nauczyciela"
  ]
  node [
    id 128
    label "marc&#243;wka"
  ]
  node [
    id 129
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 130
    label "akt"
  ]
  node [
    id 131
    label "przej&#347;&#263;"
  ]
  node [
    id 132
    label "charter"
  ]
  node [
    id 133
    label "przej&#347;cie"
  ]
  node [
    id 134
    label "poprawa"
  ]
  node [
    id 135
    label "warto&#347;&#263;"
  ]
  node [
    id 136
    label "alternation"
  ]
  node [
    id 137
    label "modyfikacja"
  ]
  node [
    id 138
    label "egzamin"
  ]
  node [
    id 139
    label "volunteer"
  ]
  node [
    id 140
    label "informowa&#263;"
  ]
  node [
    id 141
    label "zach&#281;ca&#263;"
  ]
  node [
    id 142
    label "wnioskowa&#263;"
  ]
  node [
    id 143
    label "suggest"
  ]
  node [
    id 144
    label "kandydatura"
  ]
  node [
    id 145
    label "troch&#281;"
  ]
  node [
    id 146
    label "parlament"
  ]
  node [
    id 147
    label "obrady"
  ]
  node [
    id 148
    label "grupa"
  ]
  node [
    id 149
    label "lewica"
  ]
  node [
    id 150
    label "zgromadzenie"
  ]
  node [
    id 151
    label "prawica"
  ]
  node [
    id 152
    label "centrum"
  ]
  node [
    id 153
    label "izba_ni&#380;sza"
  ]
  node [
    id 154
    label "siedziba"
  ]
  node [
    id 155
    label "parliament"
  ]
  node [
    id 156
    label "znowu"
  ]
  node [
    id 157
    label "ponowny"
  ]
  node [
    id 158
    label "return"
  ]
  node [
    id 159
    label "podpowiedzie&#263;"
  ]
  node [
    id 160
    label "direct"
  ]
  node [
    id 161
    label "dispatch"
  ]
  node [
    id 162
    label "przeznaczy&#263;"
  ]
  node [
    id 163
    label "ustawi&#263;"
  ]
  node [
    id 164
    label "wys&#322;a&#263;"
  ]
  node [
    id 165
    label "precede"
  ]
  node [
    id 166
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 167
    label "set"
  ]
  node [
    id 168
    label "okre&#347;lony"
  ]
  node [
    id 169
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 170
    label "zesp&#243;&#322;"
  ]
  node [
    id 171
    label "organ"
  ]
  node [
    id 172
    label "Komisja_Europejska"
  ]
  node [
    id 173
    label "podkomisja"
  ]
  node [
    id 174
    label "konsekwencja"
  ]
  node [
    id 175
    label "punishment"
  ]
  node [
    id 176
    label "cecha"
  ]
  node [
    id 177
    label "roboty_przymusowe"
  ]
  node [
    id 178
    label "nemezis"
  ]
  node [
    id 179
    label "righteousness"
  ]
  node [
    id 180
    label "z_prawa"
  ]
  node [
    id 181
    label "cnotliwy"
  ]
  node [
    id 182
    label "moralny"
  ]
  node [
    id 183
    label "zgodnie_z_prawem"
  ]
  node [
    id 184
    label "naturalny"
  ]
  node [
    id 185
    label "legalny"
  ]
  node [
    id 186
    label "na_prawo"
  ]
  node [
    id 187
    label "s&#322;uszny"
  ]
  node [
    id 188
    label "w_prawo"
  ]
  node [
    id 189
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 190
    label "prawicowy"
  ]
  node [
    id 191
    label "chwalebny"
  ]
  node [
    id 192
    label "zacny"
  ]
  node [
    id 193
    label "asymilowa&#263;"
  ]
  node [
    id 194
    label "wapniak"
  ]
  node [
    id 195
    label "dwun&#243;g"
  ]
  node [
    id 196
    label "polifag"
  ]
  node [
    id 197
    label "wz&#243;r"
  ]
  node [
    id 198
    label "profanum"
  ]
  node [
    id 199
    label "hominid"
  ]
  node [
    id 200
    label "homo_sapiens"
  ]
  node [
    id 201
    label "nasada"
  ]
  node [
    id 202
    label "podw&#322;adny"
  ]
  node [
    id 203
    label "ludzko&#347;&#263;"
  ]
  node [
    id 204
    label "os&#322;abianie"
  ]
  node [
    id 205
    label "mikrokosmos"
  ]
  node [
    id 206
    label "portrecista"
  ]
  node [
    id 207
    label "duch"
  ]
  node [
    id 208
    label "oddzia&#322;ywanie"
  ]
  node [
    id 209
    label "g&#322;owa"
  ]
  node [
    id 210
    label "asymilowanie"
  ]
  node [
    id 211
    label "osoba"
  ]
  node [
    id 212
    label "os&#322;abia&#263;"
  ]
  node [
    id 213
    label "figura"
  ]
  node [
    id 214
    label "Adam"
  ]
  node [
    id 215
    label "senior"
  ]
  node [
    id 216
    label "antropochoria"
  ]
  node [
    id 217
    label "posta&#263;"
  ]
  node [
    id 218
    label "pomieszczenie"
  ]
  node [
    id 219
    label "klasztor"
  ]
  node [
    id 220
    label "theatrical_performance"
  ]
  node [
    id 221
    label "podanie"
  ]
  node [
    id 222
    label "ods&#322;ona"
  ]
  node [
    id 223
    label "malarstwo"
  ]
  node [
    id 224
    label "narration"
  ]
  node [
    id 225
    label "rola"
  ]
  node [
    id 226
    label "exhibit"
  ]
  node [
    id 227
    label "pokazanie"
  ]
  node [
    id 228
    label "realizacja"
  ]
  node [
    id 229
    label "wytw&#243;r"
  ]
  node [
    id 230
    label "scenografia"
  ]
  node [
    id 231
    label "zapoznanie"
  ]
  node [
    id 232
    label "obgadanie"
  ]
  node [
    id 233
    label "pokaz"
  ]
  node [
    id 234
    label "spos&#243;b"
  ]
  node [
    id 235
    label "przedstawia&#263;"
  ]
  node [
    id 236
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 237
    label "pr&#243;bowanie"
  ]
  node [
    id 238
    label "teatr"
  ]
  node [
    id 239
    label "scena"
  ]
  node [
    id 240
    label "przedstawianie"
  ]
  node [
    id 241
    label "wyst&#261;pienie"
  ]
  node [
    id 242
    label "zademonstrowanie"
  ]
  node [
    id 243
    label "cyrk"
  ]
  node [
    id 244
    label "opisanie"
  ]
  node [
    id 245
    label "ukazanie"
  ]
  node [
    id 246
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 247
    label "przedstawi&#263;"
  ]
  node [
    id 248
    label "message"
  ]
  node [
    id 249
    label "korespondent"
  ]
  node [
    id 250
    label "wypowied&#378;"
  ]
  node [
    id 251
    label "sprawko"
  ]
  node [
    id 252
    label "i"
  ]
  node [
    id 253
    label "ojciec"
  ]
  node [
    id 254
    label "sejmowy"
  ]
  node [
    id 255
    label "&#347;ledczy"
  ]
  node [
    id 256
    label "Roberto"
  ]
  node [
    id 257
    label "w&#281;grzyn"
  ]
  node [
    id 258
    label "Roberta"
  ]
  node [
    id 259
    label "biuro"
  ]
  node [
    id 260
    label "analiza"
  ]
  node [
    id 261
    label "Jaros&#322;awa"
  ]
  node [
    id 262
    label "Kalinowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 252
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 253
  ]
  edge [
    source 16
    target 254
  ]
  edge [
    source 16
    target 255
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 252
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 252
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 252
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 108
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 223
  ]
  edge [
    source 21
    target 224
  ]
  edge [
    source 21
    target 225
  ]
  edge [
    source 21
    target 226
  ]
  edge [
    source 21
    target 227
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 21
    target 229
  ]
  edge [
    source 21
    target 230
  ]
  edge [
    source 21
    target 231
  ]
  edge [
    source 21
    target 232
  ]
  edge [
    source 21
    target 233
  ]
  edge [
    source 21
    target 234
  ]
  edge [
    source 21
    target 235
  ]
  edge [
    source 21
    target 236
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 21
    target 239
  ]
  edge [
    source 21
    target 240
  ]
  edge [
    source 21
    target 241
  ]
  edge [
    source 21
    target 242
  ]
  edge [
    source 21
    target 243
  ]
  edge [
    source 21
    target 244
  ]
  edge [
    source 21
    target 245
  ]
  edge [
    source 21
    target 246
  ]
  edge [
    source 21
    target 247
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 22
    target 248
  ]
  edge [
    source 22
    target 249
  ]
  edge [
    source 22
    target 250
  ]
  edge [
    source 22
    target 251
  ]
  edge [
    source 253
    target 254
  ]
  edge [
    source 253
    target 255
  ]
  edge [
    source 254
    target 255
  ]
  edge [
    source 254
    target 259
  ]
  edge [
    source 254
    target 260
  ]
  edge [
    source 256
    target 257
  ]
  edge [
    source 257
    target 258
  ]
  edge [
    source 259
    target 260
  ]
  edge [
    source 261
    target 262
  ]
]
