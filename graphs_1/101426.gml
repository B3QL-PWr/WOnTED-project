graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.05714285714285714
  graphCliqueNumber 6
  node [
    id 0
    label "radoslav"
    origin "text"
  ]
  node [
    id 1
    label "zabavn&#237;k"
    origin "text"
  ]
  node [
    id 2
    label "Radoslav"
  ]
  node [
    id 3
    label "Zabavn&#237;k"
  ]
  node [
    id 4
    label "1"
  ]
  node [
    id 5
    label "FC"
  ]
  node [
    id 6
    label "Ko&#353;ice"
  ]
  node [
    id 7
    label "M&#352;K"
  ]
  node [
    id 8
    label "&#381;ilina"
  ]
  node [
    id 9
    label "CSKA"
  ]
  node [
    id 10
    label "Sofia"
  ]
  node [
    id 11
    label "grupa"
  ]
  node [
    id 12
    label "albo"
  ]
  node [
    id 13
    label "zeprze&#263;"
  ]
  node [
    id 14
    label "Praga"
  ]
  node [
    id 15
    label "puchar"
  ]
  node [
    id 16
    label "Czechy"
  ]
  node [
    id 17
    label "premiera"
  ]
  node [
    id 18
    label "liga"
  ]
  node [
    id 19
    label "Tereka"
  ]
  node [
    id 20
    label "Grozny"
  ]
  node [
    id 21
    label "Krylj&#261;"
  ]
  node [
    id 22
    label "Sowietow"
  ]
  node [
    id 23
    label "samara"
  ]
  node [
    id 24
    label "FSV"
  ]
  node [
    id 25
    label "Mainz"
  ]
  node [
    id 26
    label "05"
  ]
  node [
    id 27
    label "Borussi&#261;"
  ]
  node [
    id 28
    label "M&#246;nchengladbach"
  ]
  node [
    id 29
    label "mistrzostwo"
  ]
  node [
    id 30
    label "&#347;wiat"
  ]
  node [
    id 31
    label "wyspa"
  ]
  node [
    id 32
    label "pi&#322;ka"
  ]
  node [
    id 33
    label "no&#380;ny"
  ]
  node [
    id 34
    label "2010"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 33
    target 34
  ]
]
