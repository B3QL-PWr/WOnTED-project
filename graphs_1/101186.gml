graph [
  maxDegree 9
  minDegree 1
  meanDegree 2.646153846153846
  density 0.041346153846153845
  graphCliqueNumber 6
  node [
    id 0
    label "leszek"
    origin "text"
  ]
  node [
    id 1
    label "pe&#322;szyk"
    origin "text"
  ]
  node [
    id 2
    label "Leszek"
  ]
  node [
    id 3
    label "Pe&#322;szyk"
  ]
  node [
    id 4
    label "Miracle"
  ]
  node [
    id 5
    label "inny"
  ]
  node [
    id 6
    label "5"
  ]
  node [
    id 7
    label "sp&#243;&#322;ka"
  ]
  node [
    id 8
    label "zeszyt"
  ]
  node [
    id 9
    label "ojciec"
  ]
  node [
    id 10
    label "Zuza"
  ]
  node [
    id 11
    label "Pictures"
  ]
  node [
    id 12
    label "Artur"
  ]
  node [
    id 13
    label "wydzia&#322;"
  ]
  node [
    id 14
    label "radio"
  ]
  node [
    id 15
    label "i"
  ]
  node [
    id 16
    label "telewizja"
  ]
  node [
    id 17
    label "uniwersytet"
  ]
  node [
    id 18
    label "&#347;l&#261;ski"
  ]
  node [
    id 19
    label "budowa"
  ]
  node [
    id 20
    label "maszyna"
  ]
  node [
    id 21
    label "lotnictwo"
  ]
  node [
    id 22
    label "politechnika"
  ]
  node [
    id 23
    label "rzeszowski"
  ]
  node [
    id 24
    label "Freemantle"
  ]
  node [
    id 25
    label "medium"
  ]
  node [
    id 26
    label "Pearson"
  ]
  node [
    id 27
    label "Television"
  ]
  node [
    id 28
    label "biga"
  ]
  node [
    id 29
    label "Brother"
  ]
  node [
    id 30
    label "SNAP"
  ]
  node [
    id 31
    label "tv"
  ]
  node [
    id 32
    label "Rinke"
  ]
  node [
    id 33
    label "Rooyensem"
  ]
  node [
    id 34
    label "Clare"
  ]
  node [
    id 35
    label "Frances"
  ]
  node [
    id 36
    label "Downer"
  ]
  node [
    id 37
    label "jaka"
  ]
  node [
    id 38
    label "IKEA"
  ]
  node [
    id 39
    label "domowy"
  ]
  node [
    id 40
    label "pe&#322;ny"
  ]
  node [
    id 41
    label "pomys&#322;"
  ]
  node [
    id 42
    label "kto"
  ]
  node [
    id 43
    label "wy"
  ]
  node [
    id 44
    label "taka"
  ]
  node [
    id 45
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 46
    label "TVP"
  ]
  node [
    id 47
    label "2"
  ]
  node [
    id 48
    label "si&#322;acz"
  ]
  node [
    id 49
    label "TVN"
  ]
  node [
    id 50
    label "Szymona"
  ]
  node [
    id 51
    label "Majewski"
  ]
  node [
    id 52
    label "na"
  ]
  node [
    id 53
    label "ka&#380;dy"
  ]
  node [
    id 54
    label "temat"
  ]
  node [
    id 55
    label "pod"
  ]
  node [
    id 56
    label "napi&#281;cie"
  ]
  node [
    id 57
    label "show"
  ]
  node [
    id 58
    label "dla"
  ]
  node [
    id 59
    label "ty"
  ]
  node [
    id 60
    label "wszystko"
  ]
  node [
    id 61
    label "XXXI"
  ]
  node [
    id 62
    label "mi&#281;dzynarodowy"
  ]
  node [
    id 63
    label "konferencja"
  ]
  node [
    id 64
    label "PIKE"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 37
    target 41
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 61
    target 64
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 63
    target 64
  ]
]
