graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9402985074626866
  density 0.02939846223428313
  graphCliqueNumber 2
  node [
    id 0
    label "pomnik"
    origin "text"
  ]
  node [
    id 1
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 2
    label "henryk"
    origin "text"
  ]
  node [
    id 3
    label "jankowski"
    origin "text"
  ]
  node [
    id 4
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "symbol"
    origin "text"
  ]
  node [
    id 7
    label "ob&#322;uda"
    origin "text"
  ]
  node [
    id 8
    label "k&#322;amstwo"
    origin "text"
  ]
  node [
    id 9
    label "krzywda"
    origin "text"
  ]
  node [
    id 10
    label "cok&#243;&#322;"
  ]
  node [
    id 11
    label "&#347;wiadectwo"
  ]
  node [
    id 12
    label "p&#322;yta"
  ]
  node [
    id 13
    label "dzie&#322;o"
  ]
  node [
    id 14
    label "rzecz"
  ]
  node [
    id 15
    label "dow&#243;d"
  ]
  node [
    id 16
    label "gr&#243;b"
  ]
  node [
    id 17
    label "klecha"
  ]
  node [
    id 18
    label "eklezjasta"
  ]
  node [
    id 19
    label "rozgrzeszanie"
  ]
  node [
    id 20
    label "duszpasterstwo"
  ]
  node [
    id 21
    label "rozgrzesza&#263;"
  ]
  node [
    id 22
    label "duchowny"
  ]
  node [
    id 23
    label "ksi&#281;&#380;a"
  ]
  node [
    id 24
    label "kap&#322;an"
  ]
  node [
    id 25
    label "kol&#281;da"
  ]
  node [
    id 26
    label "seminarzysta"
  ]
  node [
    id 27
    label "pasterz"
  ]
  node [
    id 28
    label "pozostawa&#263;"
  ]
  node [
    id 29
    label "trwa&#263;"
  ]
  node [
    id 30
    label "by&#263;"
  ]
  node [
    id 31
    label "wystarcza&#263;"
  ]
  node [
    id 32
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 33
    label "czeka&#263;"
  ]
  node [
    id 34
    label "stand"
  ]
  node [
    id 35
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 36
    label "mieszka&#263;"
  ]
  node [
    id 37
    label "wystarczy&#263;"
  ]
  node [
    id 38
    label "sprawowa&#263;"
  ]
  node [
    id 39
    label "przebywa&#263;"
  ]
  node [
    id 40
    label "kosztowa&#263;"
  ]
  node [
    id 41
    label "undertaking"
  ]
  node [
    id 42
    label "wystawa&#263;"
  ]
  node [
    id 43
    label "base"
  ]
  node [
    id 44
    label "digest"
  ]
  node [
    id 45
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 46
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 47
    label "symbolizowanie"
  ]
  node [
    id 48
    label "wcielenie"
  ]
  node [
    id 49
    label "notacja"
  ]
  node [
    id 50
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 51
    label "znak_pisarski"
  ]
  node [
    id 52
    label "znak"
  ]
  node [
    id 53
    label "character"
  ]
  node [
    id 54
    label "nieuczciwo&#347;&#263;"
  ]
  node [
    id 55
    label "lipa"
  ]
  node [
    id 56
    label "oszustwo"
  ]
  node [
    id 57
    label "wymys&#322;"
  ]
  node [
    id 58
    label "lie"
  ]
  node [
    id 59
    label "blef"
  ]
  node [
    id 60
    label "nieprawda"
  ]
  node [
    id 61
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 62
    label "bias"
  ]
  node [
    id 63
    label "obelga"
  ]
  node [
    id 64
    label "strata"
  ]
  node [
    id 65
    label "Henryk"
  ]
  node [
    id 66
    label "Jankowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 65
    target 66
  ]
]
