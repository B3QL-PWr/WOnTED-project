graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.042105263157895
  density 0.010804789752158173
  graphCliqueNumber 2
  node [
    id 0
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "nagranie"
    origin "text"
  ]
  node [
    id 4
    label "wideo"
    origin "text"
  ]
  node [
    id 5
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "wida&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dziesi&#261;tek"
    origin "text"
  ]
  node [
    id 9
    label "zatrzymana"
    origin "text"
  ]
  node [
    id 10
    label "ucze&#324;"
    origin "text"
  ]
  node [
    id 11
    label "kl&#281;cz&#261;cy"
    origin "text"
  ]
  node [
    id 12
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 13
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 14
    label "sku&#263;"
    origin "text"
  ]
  node [
    id 15
    label "kajdanki"
    origin "text"
  ]
  node [
    id 16
    label "trzyma&#263;"
    origin "text"
  ]
  node [
    id 17
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 18
    label "hipertekst"
  ]
  node [
    id 19
    label "gauze"
  ]
  node [
    id 20
    label "nitka"
  ]
  node [
    id 21
    label "mesh"
  ]
  node [
    id 22
    label "e-hazard"
  ]
  node [
    id 23
    label "netbook"
  ]
  node [
    id 24
    label "cyberprzestrze&#324;"
  ]
  node [
    id 25
    label "biznes_elektroniczny"
  ]
  node [
    id 26
    label "snu&#263;"
  ]
  node [
    id 27
    label "organization"
  ]
  node [
    id 28
    label "zasadzka"
  ]
  node [
    id 29
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 30
    label "web"
  ]
  node [
    id 31
    label "provider"
  ]
  node [
    id 32
    label "struktura"
  ]
  node [
    id 33
    label "us&#322;uga_internetowa"
  ]
  node [
    id 34
    label "punkt_dost&#281;pu"
  ]
  node [
    id 35
    label "organizacja"
  ]
  node [
    id 36
    label "mem"
  ]
  node [
    id 37
    label "vane"
  ]
  node [
    id 38
    label "podcast"
  ]
  node [
    id 39
    label "grooming"
  ]
  node [
    id 40
    label "kszta&#322;t"
  ]
  node [
    id 41
    label "strona"
  ]
  node [
    id 42
    label "obiekt"
  ]
  node [
    id 43
    label "wysnu&#263;"
  ]
  node [
    id 44
    label "gra_sieciowa"
  ]
  node [
    id 45
    label "instalacja"
  ]
  node [
    id 46
    label "sie&#263;_komputerowa"
  ]
  node [
    id 47
    label "net"
  ]
  node [
    id 48
    label "plecionka"
  ]
  node [
    id 49
    label "media"
  ]
  node [
    id 50
    label "rozmieszczenie"
  ]
  node [
    id 51
    label "wys&#322;uchanie"
  ]
  node [
    id 52
    label "wytw&#243;r"
  ]
  node [
    id 53
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 54
    label "recording"
  ]
  node [
    id 55
    label "utrwalenie"
  ]
  node [
    id 56
    label "odtwarzacz"
  ]
  node [
    id 57
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 58
    label "film"
  ]
  node [
    id 59
    label "technika"
  ]
  node [
    id 60
    label "wideokaseta"
  ]
  node [
    id 61
    label "dark_lantern"
  ]
  node [
    id 62
    label "zbi&#243;r"
  ]
  node [
    id 63
    label "kopa"
  ]
  node [
    id 64
    label "cz&#322;owiek"
  ]
  node [
    id 65
    label "zwolennik"
  ]
  node [
    id 66
    label "tarcza"
  ]
  node [
    id 67
    label "czeladnik"
  ]
  node [
    id 68
    label "elew"
  ]
  node [
    id 69
    label "rzemie&#347;lnik"
  ]
  node [
    id 70
    label "kontynuator"
  ]
  node [
    id 71
    label "klasa"
  ]
  node [
    id 72
    label "wyprawka"
  ]
  node [
    id 73
    label "mundurek"
  ]
  node [
    id 74
    label "absolwent"
  ]
  node [
    id 75
    label "szko&#322;a"
  ]
  node [
    id 76
    label "praktykant"
  ]
  node [
    id 77
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 78
    label "kategoria"
  ]
  node [
    id 79
    label "egzekutywa"
  ]
  node [
    id 80
    label "gabinet_cieni"
  ]
  node [
    id 81
    label "gromada"
  ]
  node [
    id 82
    label "premier"
  ]
  node [
    id 83
    label "Londyn"
  ]
  node [
    id 84
    label "Konsulat"
  ]
  node [
    id 85
    label "uporz&#261;dkowanie"
  ]
  node [
    id 86
    label "jednostka_systematyczna"
  ]
  node [
    id 87
    label "szpaler"
  ]
  node [
    id 88
    label "przybli&#380;enie"
  ]
  node [
    id 89
    label "tract"
  ]
  node [
    id 90
    label "number"
  ]
  node [
    id 91
    label "lon&#380;a"
  ]
  node [
    id 92
    label "w&#322;adza"
  ]
  node [
    id 93
    label "instytucja"
  ]
  node [
    id 94
    label "krzy&#380;"
  ]
  node [
    id 95
    label "paw"
  ]
  node [
    id 96
    label "rami&#281;"
  ]
  node [
    id 97
    label "gestykulowanie"
  ]
  node [
    id 98
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 99
    label "pracownik"
  ]
  node [
    id 100
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 101
    label "bramkarz"
  ]
  node [
    id 102
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 103
    label "handwriting"
  ]
  node [
    id 104
    label "hasta"
  ]
  node [
    id 105
    label "pi&#322;ka"
  ]
  node [
    id 106
    label "&#322;okie&#263;"
  ]
  node [
    id 107
    label "spos&#243;b"
  ]
  node [
    id 108
    label "zagrywka"
  ]
  node [
    id 109
    label "obietnica"
  ]
  node [
    id 110
    label "przedrami&#281;"
  ]
  node [
    id 111
    label "chwyta&#263;"
  ]
  node [
    id 112
    label "r&#261;czyna"
  ]
  node [
    id 113
    label "cecha"
  ]
  node [
    id 114
    label "wykroczenie"
  ]
  node [
    id 115
    label "kroki"
  ]
  node [
    id 116
    label "palec"
  ]
  node [
    id 117
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 118
    label "graba"
  ]
  node [
    id 119
    label "hand"
  ]
  node [
    id 120
    label "nadgarstek"
  ]
  node [
    id 121
    label "pomocnik"
  ]
  node [
    id 122
    label "k&#322;&#261;b"
  ]
  node [
    id 123
    label "hazena"
  ]
  node [
    id 124
    label "gestykulowa&#263;"
  ]
  node [
    id 125
    label "cmoknonsens"
  ]
  node [
    id 126
    label "d&#322;o&#324;"
  ]
  node [
    id 127
    label "chwytanie"
  ]
  node [
    id 128
    label "czerwona_kartka"
  ]
  node [
    id 129
    label "spowodowa&#263;"
  ]
  node [
    id 130
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 131
    label "scali&#263;"
  ]
  node [
    id 132
    label "usun&#261;&#263;"
  ]
  node [
    id 133
    label "fetter"
  ]
  node [
    id 134
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 135
    label "narz&#281;dzie_wymierzania_kary"
  ]
  node [
    id 136
    label "handcuff"
  ]
  node [
    id 137
    label "continue"
  ]
  node [
    id 138
    label "zmusza&#263;"
  ]
  node [
    id 139
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 140
    label "sympatyzowa&#263;"
  ]
  node [
    id 141
    label "pozostawa&#263;"
  ]
  node [
    id 142
    label "zachowywa&#263;"
  ]
  node [
    id 143
    label "utrzymywa&#263;"
  ]
  node [
    id 144
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 145
    label "treat"
  ]
  node [
    id 146
    label "przetrzymywa&#263;"
  ]
  node [
    id 147
    label "sprawowa&#263;"
  ]
  node [
    id 148
    label "administrowa&#263;"
  ]
  node [
    id 149
    label "robi&#263;"
  ]
  node [
    id 150
    label "adhere"
  ]
  node [
    id 151
    label "dzier&#380;y&#263;"
  ]
  node [
    id 152
    label "podtrzymywa&#263;"
  ]
  node [
    id 153
    label "argue"
  ]
  node [
    id 154
    label "hodowa&#263;"
  ]
  node [
    id 155
    label "wychowywa&#263;"
  ]
  node [
    id 156
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 157
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 158
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 159
    label "ucho"
  ]
  node [
    id 160
    label "makrocefalia"
  ]
  node [
    id 161
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 162
    label "m&#243;zg"
  ]
  node [
    id 163
    label "kierownictwo"
  ]
  node [
    id 164
    label "czaszka"
  ]
  node [
    id 165
    label "dekiel"
  ]
  node [
    id 166
    label "umys&#322;"
  ]
  node [
    id 167
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 168
    label "&#347;ci&#281;cie"
  ]
  node [
    id 169
    label "sztuka"
  ]
  node [
    id 170
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 171
    label "g&#243;ra"
  ]
  node [
    id 172
    label "byd&#322;o"
  ]
  node [
    id 173
    label "alkohol"
  ]
  node [
    id 174
    label "wiedza"
  ]
  node [
    id 175
    label "ro&#347;lina"
  ]
  node [
    id 176
    label "&#347;ci&#281;gno"
  ]
  node [
    id 177
    label "&#380;ycie"
  ]
  node [
    id 178
    label "pryncypa&#322;"
  ]
  node [
    id 179
    label "fryzura"
  ]
  node [
    id 180
    label "noosfera"
  ]
  node [
    id 181
    label "kierowa&#263;"
  ]
  node [
    id 182
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 183
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 184
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 185
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 186
    label "zdolno&#347;&#263;"
  ]
  node [
    id 187
    label "cz&#322;onek"
  ]
  node [
    id 188
    label "cia&#322;o"
  ]
  node [
    id 189
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 17
    target 64
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 42
  ]
  edge [
    source 17
    target 189
  ]
]
