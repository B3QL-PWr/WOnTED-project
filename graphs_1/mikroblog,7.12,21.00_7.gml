graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.0210526315789474
  density 0.021500559910414333
  graphCliqueNumber 3
  node [
    id 0
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 1
    label "swoje"
    origin "text"
  ]
  node [
    id 2
    label "urodziny"
    origin "text"
  ]
  node [
    id 3
    label "obchodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "kto"
    origin "text"
  ]
  node [
    id 5
    label "inny"
    origin "text"
  ]
  node [
    id 6
    label "pan"
    origin "text"
  ]
  node [
    id 7
    label "kierowca"
    origin "text"
  ]
  node [
    id 8
    label "wszystko"
    origin "text"
  ]
  node [
    id 9
    label "dobry"
    origin "text"
  ]
  node [
    id 10
    label "doba"
  ]
  node [
    id 11
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 12
    label "dzi&#347;"
  ]
  node [
    id 13
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 14
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 15
    label "impreza"
  ]
  node [
    id 16
    label "jubileusz"
  ]
  node [
    id 17
    label "&#347;wi&#281;to"
  ]
  node [
    id 18
    label "pocz&#261;tek"
  ]
  node [
    id 19
    label "wykorzystywa&#263;"
  ]
  node [
    id 20
    label "omija&#263;"
  ]
  node [
    id 21
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 22
    label "sp&#281;dza&#263;"
  ]
  node [
    id 23
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 24
    label "interesowa&#263;"
  ]
  node [
    id 25
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 26
    label "post&#281;powa&#263;"
  ]
  node [
    id 27
    label "feast"
  ]
  node [
    id 28
    label "chodzi&#263;"
  ]
  node [
    id 29
    label "wymija&#263;"
  ]
  node [
    id 30
    label "odwiedza&#263;"
  ]
  node [
    id 31
    label "prowadzi&#263;"
  ]
  node [
    id 32
    label "kolejny"
  ]
  node [
    id 33
    label "inaczej"
  ]
  node [
    id 34
    label "r&#243;&#380;ny"
  ]
  node [
    id 35
    label "inszy"
  ]
  node [
    id 36
    label "osobno"
  ]
  node [
    id 37
    label "cz&#322;owiek"
  ]
  node [
    id 38
    label "profesor"
  ]
  node [
    id 39
    label "kszta&#322;ciciel"
  ]
  node [
    id 40
    label "jegomo&#347;&#263;"
  ]
  node [
    id 41
    label "zwrot"
  ]
  node [
    id 42
    label "pracodawca"
  ]
  node [
    id 43
    label "rz&#261;dzenie"
  ]
  node [
    id 44
    label "m&#261;&#380;"
  ]
  node [
    id 45
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 46
    label "ch&#322;opina"
  ]
  node [
    id 47
    label "bratek"
  ]
  node [
    id 48
    label "opiekun"
  ]
  node [
    id 49
    label "doros&#322;y"
  ]
  node [
    id 50
    label "preceptor"
  ]
  node [
    id 51
    label "Midas"
  ]
  node [
    id 52
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 53
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 54
    label "murza"
  ]
  node [
    id 55
    label "ojciec"
  ]
  node [
    id 56
    label "androlog"
  ]
  node [
    id 57
    label "pupil"
  ]
  node [
    id 58
    label "efendi"
  ]
  node [
    id 59
    label "nabab"
  ]
  node [
    id 60
    label "w&#322;odarz"
  ]
  node [
    id 61
    label "szkolnik"
  ]
  node [
    id 62
    label "pedagog"
  ]
  node [
    id 63
    label "popularyzator"
  ]
  node [
    id 64
    label "andropauza"
  ]
  node [
    id 65
    label "gra_w_karty"
  ]
  node [
    id 66
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 67
    label "Mieszko_I"
  ]
  node [
    id 68
    label "bogaty"
  ]
  node [
    id 69
    label "samiec"
  ]
  node [
    id 70
    label "przyw&#243;dca"
  ]
  node [
    id 71
    label "pa&#324;stwo"
  ]
  node [
    id 72
    label "belfer"
  ]
  node [
    id 73
    label "transportowiec"
  ]
  node [
    id 74
    label "lock"
  ]
  node [
    id 75
    label "absolut"
  ]
  node [
    id 76
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 77
    label "pomy&#347;lny"
  ]
  node [
    id 78
    label "skuteczny"
  ]
  node [
    id 79
    label "moralny"
  ]
  node [
    id 80
    label "korzystny"
  ]
  node [
    id 81
    label "odpowiedni"
  ]
  node [
    id 82
    label "dobrze"
  ]
  node [
    id 83
    label "pozytywny"
  ]
  node [
    id 84
    label "grzeczny"
  ]
  node [
    id 85
    label "powitanie"
  ]
  node [
    id 86
    label "mi&#322;y"
  ]
  node [
    id 87
    label "dobroczynny"
  ]
  node [
    id 88
    label "pos&#322;uszny"
  ]
  node [
    id 89
    label "ca&#322;y"
  ]
  node [
    id 90
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 91
    label "czw&#243;rka"
  ]
  node [
    id 92
    label "spokojny"
  ]
  node [
    id 93
    label "&#347;mieszny"
  ]
  node [
    id 94
    label "drogi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
]
