graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.4666666666666666
  density 0.10476190476190476
  graphCliqueNumber 2
  node [
    id 0
    label "nowa"
    origin "text"
  ]
  node [
    id 1
    label "wiesia"
    origin "text"
  ]
  node [
    id 2
    label "powiat"
    origin "text"
  ]
  node [
    id 3
    label "w&#261;growiecki"
    origin "text"
  ]
  node [
    id 4
    label "gwiazda"
  ]
  node [
    id 5
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 6
    label "gmina"
  ]
  node [
    id 7
    label "jednostka_administracyjna"
  ]
  node [
    id 8
    label "wojew&#243;dztwo"
  ]
  node [
    id 9
    label "nowy"
  ]
  node [
    id 10
    label "wie&#347;"
  ]
  node [
    id 11
    label "janowiec"
  ]
  node [
    id 12
    label "wielkopolski"
  ]
  node [
    id 13
    label "RSP"
  ]
  node [
    id 14
    label "&#322;azisko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
]
