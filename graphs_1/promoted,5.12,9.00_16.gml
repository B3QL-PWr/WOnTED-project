graph [
  maxDegree 20
  minDegree 1
  meanDegree 2
  density 0.02631578947368421
  graphCliqueNumber 3
  node [
    id 0
    label "polak"
    origin "text"
  ]
  node [
    id 1
    label "dop&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 2
    label "g&#243;rnictwo"
    origin "text"
  ]
  node [
    id 3
    label "energetyka"
    origin "text"
  ]
  node [
    id 4
    label "oprze&#263;"
    origin "text"
  ]
  node [
    id 5
    label "w&#281;giel"
    origin "text"
  ]
  node [
    id 6
    label "lato"
    origin "text"
  ]
  node [
    id 7
    label "blisko"
    origin "text"
  ]
  node [
    id 8
    label "dwa"
    origin "text"
  ]
  node [
    id 9
    label "bilion"
    origin "text"
  ]
  node [
    id 10
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 11
    label "polski"
  ]
  node [
    id 12
    label "zap&#322;aci&#263;"
  ]
  node [
    id 13
    label "przemys&#322;_ci&#281;&#380;ki"
  ]
  node [
    id 14
    label "wydobycie"
  ]
  node [
    id 15
    label "wiertnictwo"
  ]
  node [
    id 16
    label "wydobywanie"
  ]
  node [
    id 17
    label "krzeska"
  ]
  node [
    id 18
    label "solnictwo"
  ]
  node [
    id 19
    label "obrywak"
  ]
  node [
    id 20
    label "zgarniacz"
  ]
  node [
    id 21
    label "aerologia_g&#243;rnicza"
  ]
  node [
    id 22
    label "przesyp"
  ]
  node [
    id 23
    label "wydobywa&#263;"
  ]
  node [
    id 24
    label "rozpierak"
  ]
  node [
    id 25
    label "&#322;adownik"
  ]
  node [
    id 26
    label "wydoby&#263;"
  ]
  node [
    id 27
    label "odstrzeliwa&#263;"
  ]
  node [
    id 28
    label "odstrzeliwanie"
  ]
  node [
    id 29
    label "wcinka"
  ]
  node [
    id 30
    label "nauka"
  ]
  node [
    id 31
    label "przemys&#322;"
  ]
  node [
    id 32
    label "establish"
  ]
  node [
    id 33
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 34
    label "recline"
  ]
  node [
    id 35
    label "podstawa"
  ]
  node [
    id 36
    label "ustawi&#263;"
  ]
  node [
    id 37
    label "osnowa&#263;"
  ]
  node [
    id 38
    label "surowiec_energetyczny"
  ]
  node [
    id 39
    label "w&#281;glowiec"
  ]
  node [
    id 40
    label "w&#281;glowodan"
  ]
  node [
    id 41
    label "kopalina_podstawowa"
  ]
  node [
    id 42
    label "coal"
  ]
  node [
    id 43
    label "przybory_do_pisania"
  ]
  node [
    id 44
    label "makroelement"
  ]
  node [
    id 45
    label "niemetal"
  ]
  node [
    id 46
    label "zsypnik"
  ]
  node [
    id 47
    label "w&#281;glarka"
  ]
  node [
    id 48
    label "zw&#281;glenie_si&#281;"
  ]
  node [
    id 49
    label "zw&#281;glanie_si&#281;"
  ]
  node [
    id 50
    label "coil"
  ]
  node [
    id 51
    label "bry&#322;a"
  ]
  node [
    id 52
    label "ska&#322;a"
  ]
  node [
    id 53
    label "carbon"
  ]
  node [
    id 54
    label "fulleren"
  ]
  node [
    id 55
    label "rysunek"
  ]
  node [
    id 56
    label "pora_roku"
  ]
  node [
    id 57
    label "dok&#322;adnie"
  ]
  node [
    id 58
    label "bliski"
  ]
  node [
    id 59
    label "silnie"
  ]
  node [
    id 60
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 61
    label "liczba"
  ]
  node [
    id 62
    label "szlachetny"
  ]
  node [
    id 63
    label "metaliczny"
  ]
  node [
    id 64
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 65
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 66
    label "grosz"
  ]
  node [
    id 67
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 68
    label "utytu&#322;owany"
  ]
  node [
    id 69
    label "poz&#322;ocenie"
  ]
  node [
    id 70
    label "Polska"
  ]
  node [
    id 71
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 72
    label "wspania&#322;y"
  ]
  node [
    id 73
    label "doskona&#322;y"
  ]
  node [
    id 74
    label "kochany"
  ]
  node [
    id 75
    label "jednostka_monetarna"
  ]
  node [
    id 76
    label "z&#322;ocenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
]
