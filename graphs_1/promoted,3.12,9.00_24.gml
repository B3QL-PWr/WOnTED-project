graph [
  maxDegree 18
  minDegree 1
  meanDegree 2.235294117647059
  density 0.04470588235294118
  graphCliqueNumber 5
  node [
    id 0
    label "zbudowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "naukowiec"
    origin "text"
  ]
  node [
    id 2
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "narodowy"
    origin "text"
  ]
  node [
    id 4
    label "instytut"
    origin "text"
  ]
  node [
    id 5
    label "standaryzacja"
    origin "text"
  ]
  node [
    id 6
    label "technologia"
    origin "text"
  ]
  node [
    id 7
    label "nist"
    origin "text"
  ]
  node [
    id 8
    label "zaplanowa&#263;"
  ]
  node [
    id 9
    label "establish"
  ]
  node [
    id 10
    label "stworzy&#263;"
  ]
  node [
    id 11
    label "wytworzy&#263;"
  ]
  node [
    id 12
    label "evolve"
  ]
  node [
    id 13
    label "budowla"
  ]
  node [
    id 14
    label "Miczurin"
  ]
  node [
    id 15
    label "&#347;ledziciel"
  ]
  node [
    id 16
    label "uczony"
  ]
  node [
    id 17
    label "nowoczesny"
  ]
  node [
    id 18
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 19
    label "boston"
  ]
  node [
    id 20
    label "po_ameryka&#324;sku"
  ]
  node [
    id 21
    label "cake-walk"
  ]
  node [
    id 22
    label "charakterystyczny"
  ]
  node [
    id 23
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 24
    label "fajny"
  ]
  node [
    id 25
    label "j&#281;zyk_angielski"
  ]
  node [
    id 26
    label "Princeton"
  ]
  node [
    id 27
    label "pepperoni"
  ]
  node [
    id 28
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 29
    label "zachodni"
  ]
  node [
    id 30
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 31
    label "anglosaski"
  ]
  node [
    id 32
    label "typowy"
  ]
  node [
    id 33
    label "nacjonalistyczny"
  ]
  node [
    id 34
    label "narodowo"
  ]
  node [
    id 35
    label "wa&#380;ny"
  ]
  node [
    id 36
    label "plac&#243;wka"
  ]
  node [
    id 37
    label "institute"
  ]
  node [
    id 38
    label "&#379;ydowski_Instytut_Historyczny"
  ]
  node [
    id 39
    label "Ossolineum"
  ]
  node [
    id 40
    label "instytucja"
  ]
  node [
    id 41
    label "plac&#243;wka_naukowa"
  ]
  node [
    id 42
    label "normalizacja"
  ]
  node [
    id 43
    label "calibration"
  ]
  node [
    id 44
    label "engineering"
  ]
  node [
    id 45
    label "technika"
  ]
  node [
    id 46
    label "mikrotechnologia"
  ]
  node [
    id 47
    label "technologia_nieorganiczna"
  ]
  node [
    id 48
    label "biotechnologia"
  ]
  node [
    id 49
    label "spos&#243;b"
  ]
  node [
    id 50
    label "i"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
]
