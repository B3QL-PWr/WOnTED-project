graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.974025974025974
  density 0.025974025974025976
  graphCliqueNumber 2
  node [
    id 0
    label "dziewi&#281;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wilk"
    origin "text"
  ]
  node [
    id 2
    label "przechodzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "przed"
    origin "text"
  ]
  node [
    id 4
    label "kamera"
    origin "text"
  ]
  node [
    id 5
    label "t&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "wschodz&#261;cy"
    origin "text"
  ]
  node [
    id 7
    label "slonca"
    origin "text"
  ]
  node [
    id 8
    label "pies_ratowniczy_lawinowy"
  ]
  node [
    id 9
    label "wataha"
  ]
  node [
    id 10
    label "owczarek"
  ]
  node [
    id 11
    label "wy&#263;"
  ]
  node [
    id 12
    label "wiecha"
  ]
  node [
    id 13
    label "p&#281;cherz_moczowy"
  ]
  node [
    id 14
    label "pies_policyjny"
  ]
  node [
    id 15
    label "masarnia"
  ]
  node [
    id 16
    label "pies"
  ]
  node [
    id 17
    label "zaka&#380;enie"
  ]
  node [
    id 18
    label "gruby_zwierz"
  ]
  node [
    id 19
    label "tycznia"
  ]
  node [
    id 20
    label "p&#281;d"
  ]
  node [
    id 21
    label "srom"
  ]
  node [
    id 22
    label "k&#322;a&#324;ce"
  ]
  node [
    id 23
    label "schorzenie"
  ]
  node [
    id 24
    label "futro"
  ]
  node [
    id 25
    label "tocze&#324;"
  ]
  node [
    id 26
    label "lampy"
  ]
  node [
    id 27
    label "maszyna"
  ]
  node [
    id 28
    label "zawy&#263;"
  ]
  node [
    id 29
    label "podlega&#263;"
  ]
  node [
    id 30
    label "zmienia&#263;"
  ]
  node [
    id 31
    label "proceed"
  ]
  node [
    id 32
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 33
    label "saturate"
  ]
  node [
    id 34
    label "pass"
  ]
  node [
    id 35
    label "doznawa&#263;"
  ]
  node [
    id 36
    label "test"
  ]
  node [
    id 37
    label "zalicza&#263;"
  ]
  node [
    id 38
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 39
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 40
    label "conflict"
  ]
  node [
    id 41
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 42
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 43
    label "go"
  ]
  node [
    id 44
    label "continue"
  ]
  node [
    id 45
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 46
    label "mie&#263;_miejsce"
  ]
  node [
    id 47
    label "przestawa&#263;"
  ]
  node [
    id 48
    label "przerabia&#263;"
  ]
  node [
    id 49
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 50
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 51
    label "move"
  ]
  node [
    id 52
    label "przebywa&#263;"
  ]
  node [
    id 53
    label "mija&#263;"
  ]
  node [
    id 54
    label "zaczyna&#263;"
  ]
  node [
    id 55
    label "i&#347;&#263;"
  ]
  node [
    id 56
    label "krosownica"
  ]
  node [
    id 57
    label "przyrz&#261;d"
  ]
  node [
    id 58
    label "wideotelefon"
  ]
  node [
    id 59
    label "melodia"
  ]
  node [
    id 60
    label "partia"
  ]
  node [
    id 61
    label "&#347;rodowisko"
  ]
  node [
    id 62
    label "obiekt"
  ]
  node [
    id 63
    label "ubarwienie"
  ]
  node [
    id 64
    label "lingwistyka_kognitywna"
  ]
  node [
    id 65
    label "gestaltyzm"
  ]
  node [
    id 66
    label "obraz"
  ]
  node [
    id 67
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 68
    label "pod&#322;o&#380;e"
  ]
  node [
    id 69
    label "informacja"
  ]
  node [
    id 70
    label "causal_agent"
  ]
  node [
    id 71
    label "dalszoplanowy"
  ]
  node [
    id 72
    label "layer"
  ]
  node [
    id 73
    label "plan"
  ]
  node [
    id 74
    label "warunki"
  ]
  node [
    id 75
    label "background"
  ]
  node [
    id 76
    label "p&#322;aszczyzna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 6
    target 7
  ]
]
