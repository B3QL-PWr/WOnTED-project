graph [
  maxDegree 593
  minDegree 1
  meanDegree 2.0897226753670473
  density 0.0017058960615241201
  graphCliqueNumber 3
  node [
    id 0
    label "punkt"
    origin "text"
  ]
  node [
    id 1
    label "zwrotny"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "tet"
    origin "text"
  ]
  node [
    id 5
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 6
    label "nowy"
    origin "text"
  ]
  node [
    id 7
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 8
    label "kalendarz"
    origin "text"
  ]
  node [
    id 9
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 10
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 11
    label "komunistyczny"
    origin "text"
  ]
  node [
    id 12
    label "z&#322;ama&#263;"
    origin "text"
  ]
  node [
    id 13
    label "niepisany"
    origin "text"
  ]
  node [
    id 14
    label "zawieszenie"
    origin "text"
  ]
  node [
    id 15
    label "bronia"
    origin "text"
  ]
  node [
    id 16
    label "atakowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 18
    label "baza"
    origin "text"
  ]
  node [
    id 19
    label "d&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 20
    label "przeniesienie"
    origin "text"
  ]
  node [
    id 21
    label "walka"
    origin "text"
  ]
  node [
    id 22
    label "obj&#261;&#263;"
    origin "text"
  ]
  node [
    id 23
    label "sajgon"
    origin "text"
  ]
  node [
    id 24
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 25
    label "inny"
    origin "text"
  ]
  node [
    id 26
    label "ci&#281;&#380;ki"
    origin "text"
  ]
  node [
    id 27
    label "toczy&#263;"
    origin "text"
  ]
  node [
    id 28
    label "si&#281;"
    origin "text"
  ]
  node [
    id 29
    label "cesarski"
    origin "text"
  ]
  node [
    id 30
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 31
    label "piechota"
    origin "text"
  ]
  node [
    id 32
    label "morski"
    origin "text"
  ]
  node [
    id 33
    label "armia"
    origin "text"
  ]
  node [
    id 34
    label "republika"
    origin "text"
  ]
  node [
    id 35
    label "wietnam"
    origin "text"
  ]
  node [
    id 36
    label "opora"
    origin "text"
  ]
  node [
    id 37
    label "komunista"
    origin "text"
  ]
  node [
    id 38
    label "zdobycie"
    origin "text"
  ]
  node [
    id 39
    label "miasto"
    origin "text"
  ]
  node [
    id 40
    label "vietcongu"
    origin "text"
  ]
  node [
    id 41
    label "p&#243;&#322;nocny"
    origin "text"
  ]
  node [
    id 42
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 43
    label "masakra"
    origin "text"
  ]
  node [
    id 44
    label "ludno&#347;&#263;"
    origin "text"
  ]
  node [
    id 45
    label "cywilny"
    origin "text"
  ]
  node [
    id 46
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 47
    label "osoba"
    origin "text"
  ]
  node [
    id 48
    label "wysoki"
    origin "text"
  ]
  node [
    id 49
    label "wykszta&#322;cenie"
    origin "text"
  ]
  node [
    id 50
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 51
    label "wszyscy"
    origin "text"
  ]
  node [
    id 52
    label "pracownik"
    origin "text"
  ]
  node [
    id 53
    label "administracja"
    origin "text"
  ]
  node [
    id 54
    label "oblicze"
    origin "text"
  ]
  node [
    id 55
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 56
    label "dni"
    origin "text"
  ]
  node [
    id 57
    label "sprawowanie"
    origin "text"
  ]
  node [
    id 58
    label "kontrola"
    origin "text"
  ]
  node [
    id 59
    label "wymordowa&#263;"
    origin "text"
  ]
  node [
    id 60
    label "tys"
    origin "text"
  ]
  node [
    id 61
    label "cywil"
    origin "text"
  ]
  node [
    id 62
    label "pomi&#281;dzy"
    origin "text"
  ]
  node [
    id 63
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 64
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 65
    label "obywatel"
    origin "text"
  ]
  node [
    id 66
    label "francuski"
    origin "text"
  ]
  node [
    id 67
    label "zachodnioniemiecki"
    origin "text"
  ]
  node [
    id 68
    label "prosta"
  ]
  node [
    id 69
    label "po&#322;o&#380;enie"
  ]
  node [
    id 70
    label "chwila"
  ]
  node [
    id 71
    label "ust&#281;p"
  ]
  node [
    id 72
    label "problemat"
  ]
  node [
    id 73
    label "kres"
  ]
  node [
    id 74
    label "mark"
  ]
  node [
    id 75
    label "pozycja"
  ]
  node [
    id 76
    label "point"
  ]
  node [
    id 77
    label "stopie&#324;_pisma"
  ]
  node [
    id 78
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 79
    label "przestrze&#324;"
  ]
  node [
    id 80
    label "wojsko"
  ]
  node [
    id 81
    label "problematyka"
  ]
  node [
    id 82
    label "zapunktowa&#263;"
  ]
  node [
    id 83
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 84
    label "obiekt_matematyczny"
  ]
  node [
    id 85
    label "sprawa"
  ]
  node [
    id 86
    label "plamka"
  ]
  node [
    id 87
    label "miejsce"
  ]
  node [
    id 88
    label "obiekt"
  ]
  node [
    id 89
    label "plan"
  ]
  node [
    id 90
    label "podpunkt"
  ]
  node [
    id 91
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 92
    label "jednostka"
  ]
  node [
    id 93
    label "zmienny"
  ]
  node [
    id 94
    label "prze&#322;omowy"
  ]
  node [
    id 95
    label "zwinny"
  ]
  node [
    id 96
    label "kluczowy"
  ]
  node [
    id 97
    label "zwrotnie"
  ]
  node [
    id 98
    label "si&#281;ga&#263;"
  ]
  node [
    id 99
    label "trwa&#263;"
  ]
  node [
    id 100
    label "obecno&#347;&#263;"
  ]
  node [
    id 101
    label "stan"
  ]
  node [
    id 102
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 103
    label "stand"
  ]
  node [
    id 104
    label "mie&#263;_miejsce"
  ]
  node [
    id 105
    label "uczestniczy&#263;"
  ]
  node [
    id 106
    label "chodzi&#263;"
  ]
  node [
    id 107
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 108
    label "equal"
  ]
  node [
    id 109
    label "stulecie"
  ]
  node [
    id 110
    label "czas"
  ]
  node [
    id 111
    label "pora_roku"
  ]
  node [
    id 112
    label "cykl_astronomiczny"
  ]
  node [
    id 113
    label "p&#243;&#322;rocze"
  ]
  node [
    id 114
    label "grupa"
  ]
  node [
    id 115
    label "kwarta&#322;"
  ]
  node [
    id 116
    label "kurs"
  ]
  node [
    id 117
    label "jubileusz"
  ]
  node [
    id 118
    label "miesi&#261;c"
  ]
  node [
    id 119
    label "lata"
  ]
  node [
    id 120
    label "martwy_sezon"
  ]
  node [
    id 121
    label "Nowy_Rok"
  ]
  node [
    id 122
    label "cz&#322;owiek"
  ]
  node [
    id 123
    label "nowotny"
  ]
  node [
    id 124
    label "drugi"
  ]
  node [
    id 125
    label "kolejny"
  ]
  node [
    id 126
    label "bie&#380;&#261;cy"
  ]
  node [
    id 127
    label "nowo"
  ]
  node [
    id 128
    label "narybek"
  ]
  node [
    id 129
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 130
    label "obcy"
  ]
  node [
    id 131
    label "almanac"
  ]
  node [
    id 132
    label "wydawnictwo"
  ]
  node [
    id 133
    label "rozk&#322;ad"
  ]
  node [
    id 134
    label "rachuba_czasu"
  ]
  node [
    id 135
    label "Juliusz_Cezar"
  ]
  node [
    id 136
    label "kitajski"
  ]
  node [
    id 137
    label "j&#281;zyk"
  ]
  node [
    id 138
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 139
    label "dziwaczny"
  ]
  node [
    id 140
    label "tandetny"
  ]
  node [
    id 141
    label "makroj&#281;zyk"
  ]
  node [
    id 142
    label "chi&#324;sko"
  ]
  node [
    id 143
    label "po_chi&#324;sku"
  ]
  node [
    id 144
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 145
    label "azjatycki"
  ]
  node [
    id 146
    label "lipny"
  ]
  node [
    id 147
    label "go"
  ]
  node [
    id 148
    label "niedrogi"
  ]
  node [
    id 149
    label "dalekowschodni"
  ]
  node [
    id 150
    label "magnitude"
  ]
  node [
    id 151
    label "energia"
  ]
  node [
    id 152
    label "capacity"
  ]
  node [
    id 153
    label "wuchta"
  ]
  node [
    id 154
    label "cecha"
  ]
  node [
    id 155
    label "parametr"
  ]
  node [
    id 156
    label "moment_si&#322;y"
  ]
  node [
    id 157
    label "przemoc"
  ]
  node [
    id 158
    label "zdolno&#347;&#263;"
  ]
  node [
    id 159
    label "mn&#243;stwo"
  ]
  node [
    id 160
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 161
    label "rozwi&#261;zanie"
  ]
  node [
    id 162
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 163
    label "potencja"
  ]
  node [
    id 164
    label "zjawisko"
  ]
  node [
    id 165
    label "zaleta"
  ]
  node [
    id 166
    label "lewicowy"
  ]
  node [
    id 167
    label "skomunizowanie"
  ]
  node [
    id 168
    label "czerwono"
  ]
  node [
    id 169
    label "komunizowanie"
  ]
  node [
    id 170
    label "transgress"
  ]
  node [
    id 171
    label "przygn&#281;bi&#263;"
  ]
  node [
    id 172
    label "spowodowa&#263;"
  ]
  node [
    id 173
    label "podzieli&#263;"
  ]
  node [
    id 174
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 175
    label "break"
  ]
  node [
    id 176
    label "cut"
  ]
  node [
    id 177
    label "zrobi&#263;"
  ]
  node [
    id 178
    label "crush"
  ]
  node [
    id 179
    label "kolor"
  ]
  node [
    id 180
    label "wygra&#263;"
  ]
  node [
    id 181
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 182
    label "nieoficjalny"
  ]
  node [
    id 183
    label "hang"
  ]
  node [
    id 184
    label "przymocowanie"
  ]
  node [
    id 185
    label "stop"
  ]
  node [
    id 186
    label "odwieszanie"
  ]
  node [
    id 187
    label "reprieve"
  ]
  node [
    id 188
    label "pozawieszanie"
  ]
  node [
    id 189
    label "kara"
  ]
  node [
    id 190
    label "suspension"
  ]
  node [
    id 191
    label "skomunikowanie"
  ]
  node [
    id 192
    label "obwieszenie"
  ]
  node [
    id 193
    label "powieszenie"
  ]
  node [
    id 194
    label "odwieszenie"
  ]
  node [
    id 195
    label "zako&#324;czenie"
  ]
  node [
    id 196
    label "wstrzymanie"
  ]
  node [
    id 197
    label "oduczenie"
  ]
  node [
    id 198
    label "disavowal"
  ]
  node [
    id 199
    label "resor"
  ]
  node [
    id 200
    label "zabranie"
  ]
  node [
    id 201
    label "podwozie"
  ]
  node [
    id 202
    label "amortyzator"
  ]
  node [
    id 203
    label "umieszczenie"
  ]
  node [
    id 204
    label "napada&#263;"
  ]
  node [
    id 205
    label "dzia&#322;a&#263;"
  ]
  node [
    id 206
    label "nast&#281;powa&#263;"
  ]
  node [
    id 207
    label "strike"
  ]
  node [
    id 208
    label "trouble_oneself"
  ]
  node [
    id 209
    label "m&#243;wi&#263;"
  ]
  node [
    id 210
    label "walczy&#263;"
  ]
  node [
    id 211
    label "usi&#322;owa&#263;"
  ]
  node [
    id 212
    label "rozgrywa&#263;"
  ]
  node [
    id 213
    label "aim"
  ]
  node [
    id 214
    label "sport"
  ]
  node [
    id 215
    label "przewaga"
  ]
  node [
    id 216
    label "attack"
  ]
  node [
    id 217
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 218
    label "robi&#263;"
  ]
  node [
    id 219
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 220
    label "schorzenie"
  ]
  node [
    id 221
    label "epidemia"
  ]
  node [
    id 222
    label "krytykowa&#263;"
  ]
  node [
    id 223
    label "ofensywny"
  ]
  node [
    id 224
    label "silny"
  ]
  node [
    id 225
    label "wa&#380;nie"
  ]
  node [
    id 226
    label "eksponowany"
  ]
  node [
    id 227
    label "istotnie"
  ]
  node [
    id 228
    label "znaczny"
  ]
  node [
    id 229
    label "dobry"
  ]
  node [
    id 230
    label "wynios&#322;y"
  ]
  node [
    id 231
    label "dono&#347;ny"
  ]
  node [
    id 232
    label "pole"
  ]
  node [
    id 233
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 234
    label "za&#322;o&#380;enie"
  ]
  node [
    id 235
    label "podstawa"
  ]
  node [
    id 236
    label "rekord"
  ]
  node [
    id 237
    label "system_bazy_danych"
  ]
  node [
    id 238
    label "kosmetyk"
  ]
  node [
    id 239
    label "zasadzenie"
  ]
  node [
    id 240
    label "kolumna"
  ]
  node [
    id 241
    label "documentation"
  ]
  node [
    id 242
    label "base"
  ]
  node [
    id 243
    label "zasadzi&#263;"
  ]
  node [
    id 244
    label "baseball"
  ]
  node [
    id 245
    label "stacjonowanie"
  ]
  node [
    id 246
    label "informatyka"
  ]
  node [
    id 247
    label "punkt_odniesienia"
  ]
  node [
    id 248
    label "poj&#281;cie"
  ]
  node [
    id 249
    label "boisko"
  ]
  node [
    id 250
    label "podstawowy"
  ]
  node [
    id 251
    label "zbi&#243;r"
  ]
  node [
    id 252
    label "post&#281;powa&#263;"
  ]
  node [
    id 253
    label "try"
  ]
  node [
    id 254
    label "przemieszczenie"
  ]
  node [
    id 255
    label "poprzesuwanie"
  ]
  node [
    id 256
    label "zmienienie"
  ]
  node [
    id 257
    label "move"
  ]
  node [
    id 258
    label "rozpowszechnienie"
  ]
  node [
    id 259
    label "assignment"
  ]
  node [
    id 260
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 261
    label "przesadzenie"
  ]
  node [
    id 262
    label "transfer"
  ]
  node [
    id 263
    label "pocisk"
  ]
  node [
    id 264
    label "mechanizm_obronny"
  ]
  node [
    id 265
    label "przelecenie"
  ]
  node [
    id 266
    label "skopiowanie"
  ]
  node [
    id 267
    label "dostosowanie"
  ]
  node [
    id 268
    label "strzelenie"
  ]
  node [
    id 269
    label "czyn"
  ]
  node [
    id 270
    label "trudno&#347;&#263;"
  ]
  node [
    id 271
    label "zaatakowanie"
  ]
  node [
    id 272
    label "obrona"
  ]
  node [
    id 273
    label "konfrontacyjny"
  ]
  node [
    id 274
    label "military_action"
  ]
  node [
    id 275
    label "wrestle"
  ]
  node [
    id 276
    label "action"
  ]
  node [
    id 277
    label "wydarzenie"
  ]
  node [
    id 278
    label "rywalizacja"
  ]
  node [
    id 279
    label "sambo"
  ]
  node [
    id 280
    label "contest"
  ]
  node [
    id 281
    label "sp&#243;r"
  ]
  node [
    id 282
    label "obejmowa&#263;"
  ]
  node [
    id 283
    label "zacz&#261;&#263;"
  ]
  node [
    id 284
    label "embrace"
  ]
  node [
    id 285
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 286
    label "obj&#281;cie"
  ]
  node [
    id 287
    label "skuma&#263;"
  ]
  node [
    id 288
    label "manipulate"
  ]
  node [
    id 289
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 290
    label "podj&#261;&#263;"
  ]
  node [
    id 291
    label "do"
  ]
  node [
    id 292
    label "dotkn&#261;&#263;"
  ]
  node [
    id 293
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 294
    label "assume"
  ]
  node [
    id 295
    label "involve"
  ]
  node [
    id 296
    label "zagarn&#261;&#263;"
  ]
  node [
    id 297
    label "nieporz&#261;dek"
  ]
  node [
    id 298
    label "rowdiness"
  ]
  node [
    id 299
    label "kipisz"
  ]
  node [
    id 300
    label "meksyk"
  ]
  node [
    id 301
    label "inaczej"
  ]
  node [
    id 302
    label "r&#243;&#380;ny"
  ]
  node [
    id 303
    label "inszy"
  ]
  node [
    id 304
    label "osobno"
  ]
  node [
    id 305
    label "nieprzejrzysty"
  ]
  node [
    id 306
    label "wolny"
  ]
  node [
    id 307
    label "grubo"
  ]
  node [
    id 308
    label "przyswajalny"
  ]
  node [
    id 309
    label "masywny"
  ]
  node [
    id 310
    label "zbrojny"
  ]
  node [
    id 311
    label "gro&#378;ny"
  ]
  node [
    id 312
    label "trudny"
  ]
  node [
    id 313
    label "wymagaj&#261;cy"
  ]
  node [
    id 314
    label "ambitny"
  ]
  node [
    id 315
    label "monumentalny"
  ]
  node [
    id 316
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 317
    label "niezgrabny"
  ]
  node [
    id 318
    label "charakterystyczny"
  ]
  node [
    id 319
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 320
    label "k&#322;opotliwy"
  ]
  node [
    id 321
    label "dotkliwy"
  ]
  node [
    id 322
    label "nieudany"
  ]
  node [
    id 323
    label "mocny"
  ]
  node [
    id 324
    label "bojowy"
  ]
  node [
    id 325
    label "ci&#281;&#380;ko"
  ]
  node [
    id 326
    label "kompletny"
  ]
  node [
    id 327
    label "intensywny"
  ]
  node [
    id 328
    label "wielki"
  ]
  node [
    id 329
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 330
    label "liczny"
  ]
  node [
    id 331
    label "niedelikatny"
  ]
  node [
    id 332
    label "niszczy&#263;"
  ]
  node [
    id 333
    label "obrabia&#263;"
  ]
  node [
    id 334
    label "wypuszcza&#263;"
  ]
  node [
    id 335
    label "przemieszcza&#263;"
  ]
  node [
    id 336
    label "zabiera&#263;"
  ]
  node [
    id 337
    label "prowadzi&#263;"
  ]
  node [
    id 338
    label "p&#281;dzi&#263;"
  ]
  node [
    id 339
    label "grind"
  ]
  node [
    id 340
    label "force"
  ]
  node [
    id 341
    label "przesuwa&#263;"
  ]
  node [
    id 342
    label "tacza&#263;"
  ]
  node [
    id 343
    label "&#380;y&#263;"
  ]
  node [
    id 344
    label "carry"
  ]
  node [
    id 345
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 346
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 347
    label "rusza&#263;"
  ]
  node [
    id 348
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 349
    label "po_kr&#243;lewsku"
  ]
  node [
    id 350
    label "wspania&#322;y"
  ]
  node [
    id 351
    label "po_cesarsku"
  ]
  node [
    id 352
    label "nowoczesny"
  ]
  node [
    id 353
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 354
    label "po_ameryka&#324;sku"
  ]
  node [
    id 355
    label "boston"
  ]
  node [
    id 356
    label "cake-walk"
  ]
  node [
    id 357
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 358
    label "fajny"
  ]
  node [
    id 359
    label "j&#281;zyk_angielski"
  ]
  node [
    id 360
    label "Princeton"
  ]
  node [
    id 361
    label "pepperoni"
  ]
  node [
    id 362
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 363
    label "zachodni"
  ]
  node [
    id 364
    label "anglosaski"
  ]
  node [
    id 365
    label "typowy"
  ]
  node [
    id 366
    label "kompania_honorowa"
  ]
  node [
    id 367
    label "formacja"
  ]
  node [
    id 368
    label "falanga"
  ]
  node [
    id 369
    label "infantry"
  ]
  node [
    id 370
    label "specjalny"
  ]
  node [
    id 371
    label "niebieski"
  ]
  node [
    id 372
    label "nadmorski"
  ]
  node [
    id 373
    label "morsko"
  ]
  node [
    id 374
    label "wodny"
  ]
  node [
    id 375
    label "s&#322;ony"
  ]
  node [
    id 376
    label "zielony"
  ]
  node [
    id 377
    label "przypominaj&#261;cy"
  ]
  node [
    id 378
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 379
    label "bateria"
  ]
  node [
    id 380
    label "zdemobilizowanie"
  ]
  node [
    id 381
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 382
    label "korpus"
  ]
  node [
    id 383
    label "zrejterowanie"
  ]
  node [
    id 384
    label "milicja"
  ]
  node [
    id 385
    label "legia"
  ]
  node [
    id 386
    label "werbowanie_si&#281;"
  ]
  node [
    id 387
    label "zmobilizowa&#263;"
  ]
  node [
    id 388
    label "artyleria"
  ]
  node [
    id 389
    label "struktura"
  ]
  node [
    id 390
    label "zdemobilizowa&#263;"
  ]
  node [
    id 391
    label "mobilizowa&#263;"
  ]
  node [
    id 392
    label "oddzia&#322;"
  ]
  node [
    id 393
    label "rzut"
  ]
  node [
    id 394
    label "Armia_Krajowa"
  ]
  node [
    id 395
    label "mobilizowanie"
  ]
  node [
    id 396
    label "linia"
  ]
  node [
    id 397
    label "kawaleria_powietrzna"
  ]
  node [
    id 398
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 399
    label "pospolite_ruszenie"
  ]
  node [
    id 400
    label "Armia_Czerwona"
  ]
  node [
    id 401
    label "t&#322;um"
  ]
  node [
    id 402
    label "zwi&#261;zek_operacyjny"
  ]
  node [
    id 403
    label "Eurokorpus"
  ]
  node [
    id 404
    label "wojska_pancerne"
  ]
  node [
    id 405
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 406
    label "rejterowanie"
  ]
  node [
    id 407
    label "cofni&#281;cie"
  ]
  node [
    id 408
    label "tabor"
  ]
  node [
    id 409
    label "military"
  ]
  node [
    id 410
    label "szlak_bojowy"
  ]
  node [
    id 411
    label "dywizjon_artylerii"
  ]
  node [
    id 412
    label "brygada"
  ]
  node [
    id 413
    label "zrejterowa&#263;"
  ]
  node [
    id 414
    label "zmobilizowanie"
  ]
  node [
    id 415
    label "rejterowa&#263;"
  ]
  node [
    id 416
    label "Czerwona_Gwardia"
  ]
  node [
    id 417
    label "Legia_Cudzoziemska"
  ]
  node [
    id 418
    label "wermacht"
  ]
  node [
    id 419
    label "soldateska"
  ]
  node [
    id 420
    label "oddzia&#322;_karny"
  ]
  node [
    id 421
    label "rezerwa"
  ]
  node [
    id 422
    label "or&#281;&#380;"
  ]
  node [
    id 423
    label "dywizjon_kawaleryjski"
  ]
  node [
    id 424
    label "Buriacja"
  ]
  node [
    id 425
    label "Abchazja"
  ]
  node [
    id 426
    label "Inguszetia"
  ]
  node [
    id 427
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 428
    label "Nachiczewan"
  ]
  node [
    id 429
    label "Karaka&#322;pacja"
  ]
  node [
    id 430
    label "Jakucja"
  ]
  node [
    id 431
    label "Singapur"
  ]
  node [
    id 432
    label "Komi"
  ]
  node [
    id 433
    label "Karelia"
  ]
  node [
    id 434
    label "Tatarstan"
  ]
  node [
    id 435
    label "Chakasja"
  ]
  node [
    id 436
    label "Dagestan"
  ]
  node [
    id 437
    label "Mordowia"
  ]
  node [
    id 438
    label "Ka&#322;mucja"
  ]
  node [
    id 439
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 440
    label "ustr&#243;j"
  ]
  node [
    id 441
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 442
    label "Baszkiria"
  ]
  node [
    id 443
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 444
    label "Mari_El"
  ]
  node [
    id 445
    label "Ad&#380;aria"
  ]
  node [
    id 446
    label "Czuwaszja"
  ]
  node [
    id 447
    label "Tuwa"
  ]
  node [
    id 448
    label "Czeczenia"
  ]
  node [
    id 449
    label "Udmurcja"
  ]
  node [
    id 450
    label "pa&#324;stwo"
  ]
  node [
    id 451
    label "utrudnienie"
  ]
  node [
    id 452
    label "anchor"
  ]
  node [
    id 453
    label "Stalin"
  ]
  node [
    id 454
    label "lewicowiec"
  ]
  node [
    id 455
    label "Fidel_Castro"
  ]
  node [
    id 456
    label "komuszek"
  ]
  node [
    id 457
    label "Tito"
  ]
  node [
    id 458
    label "lewactwo"
  ]
  node [
    id 459
    label "Gomu&#322;ka"
  ]
  node [
    id 460
    label "Chruszczow"
  ]
  node [
    id 461
    label "Mao"
  ]
  node [
    id 462
    label "Bre&#380;niew"
  ]
  node [
    id 463
    label "Gierek"
  ]
  node [
    id 464
    label "Bierut"
  ]
  node [
    id 465
    label "zdobywanie"
  ]
  node [
    id 466
    label "control"
  ]
  node [
    id 467
    label "return"
  ]
  node [
    id 468
    label "granie"
  ]
  node [
    id 469
    label "dostanie"
  ]
  node [
    id 470
    label "podporz&#261;dkowanie"
  ]
  node [
    id 471
    label "uzyskanie"
  ]
  node [
    id 472
    label "bycie_w_posiadaniu"
  ]
  node [
    id 473
    label "Brac&#322;aw"
  ]
  node [
    id 474
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 475
    label "G&#322;uch&#243;w"
  ]
  node [
    id 476
    label "Hallstatt"
  ]
  node [
    id 477
    label "Zbara&#380;"
  ]
  node [
    id 478
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 479
    label "Suworow"
  ]
  node [
    id 480
    label "Halicz"
  ]
  node [
    id 481
    label "Gandawa"
  ]
  node [
    id 482
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 483
    label "Wismar"
  ]
  node [
    id 484
    label "Norymberga"
  ]
  node [
    id 485
    label "Ruciane-Nida"
  ]
  node [
    id 486
    label "Wia&#378;ma"
  ]
  node [
    id 487
    label "Sewilla"
  ]
  node [
    id 488
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 489
    label "Kobry&#324;"
  ]
  node [
    id 490
    label "Brno"
  ]
  node [
    id 491
    label "Tomsk"
  ]
  node [
    id 492
    label "Poniatowa"
  ]
  node [
    id 493
    label "Hadziacz"
  ]
  node [
    id 494
    label "Tiume&#324;"
  ]
  node [
    id 495
    label "Karlsbad"
  ]
  node [
    id 496
    label "Drohobycz"
  ]
  node [
    id 497
    label "Lyon"
  ]
  node [
    id 498
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 499
    label "K&#322;odawa"
  ]
  node [
    id 500
    label "Solikamsk"
  ]
  node [
    id 501
    label "Wolgast"
  ]
  node [
    id 502
    label "Saloniki"
  ]
  node [
    id 503
    label "Lw&#243;w"
  ]
  node [
    id 504
    label "Al-Kufa"
  ]
  node [
    id 505
    label "Hamburg"
  ]
  node [
    id 506
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 507
    label "Nampula"
  ]
  node [
    id 508
    label "burmistrz"
  ]
  node [
    id 509
    label "D&#252;sseldorf"
  ]
  node [
    id 510
    label "Nowy_Orlean"
  ]
  node [
    id 511
    label "Bamberg"
  ]
  node [
    id 512
    label "Osaka"
  ]
  node [
    id 513
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 514
    label "Michalovce"
  ]
  node [
    id 515
    label "Fryburg"
  ]
  node [
    id 516
    label "Trabzon"
  ]
  node [
    id 517
    label "Wersal"
  ]
  node [
    id 518
    label "Swatowe"
  ]
  node [
    id 519
    label "Ka&#322;uga"
  ]
  node [
    id 520
    label "Dijon"
  ]
  node [
    id 521
    label "Cannes"
  ]
  node [
    id 522
    label "Borowsk"
  ]
  node [
    id 523
    label "Kursk"
  ]
  node [
    id 524
    label "Tyberiada"
  ]
  node [
    id 525
    label "Boden"
  ]
  node [
    id 526
    label "Dodona"
  ]
  node [
    id 527
    label "Vukovar"
  ]
  node [
    id 528
    label "Soleczniki"
  ]
  node [
    id 529
    label "Barcelona"
  ]
  node [
    id 530
    label "Oszmiana"
  ]
  node [
    id 531
    label "Stuttgart"
  ]
  node [
    id 532
    label "Nerczy&#324;sk"
  ]
  node [
    id 533
    label "Bijsk"
  ]
  node [
    id 534
    label "Essen"
  ]
  node [
    id 535
    label "Luboml"
  ]
  node [
    id 536
    label "Gr&#243;dek"
  ]
  node [
    id 537
    label "Orany"
  ]
  node [
    id 538
    label "Siedliszcze"
  ]
  node [
    id 539
    label "P&#322;owdiw"
  ]
  node [
    id 540
    label "A&#322;apajewsk"
  ]
  node [
    id 541
    label "Liverpool"
  ]
  node [
    id 542
    label "Ostrawa"
  ]
  node [
    id 543
    label "Penza"
  ]
  node [
    id 544
    label "Rudki"
  ]
  node [
    id 545
    label "Aktobe"
  ]
  node [
    id 546
    label "I&#322;awka"
  ]
  node [
    id 547
    label "Tolkmicko"
  ]
  node [
    id 548
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 549
    label "Sajgon"
  ]
  node [
    id 550
    label "Windawa"
  ]
  node [
    id 551
    label "Weimar"
  ]
  node [
    id 552
    label "Jekaterynburg"
  ]
  node [
    id 553
    label "Lejda"
  ]
  node [
    id 554
    label "Cremona"
  ]
  node [
    id 555
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 556
    label "Kordoba"
  ]
  node [
    id 557
    label "urz&#261;d"
  ]
  node [
    id 558
    label "&#321;ohojsk"
  ]
  node [
    id 559
    label "Kalmar"
  ]
  node [
    id 560
    label "Akerman"
  ]
  node [
    id 561
    label "Locarno"
  ]
  node [
    id 562
    label "Bych&#243;w"
  ]
  node [
    id 563
    label "Toledo"
  ]
  node [
    id 564
    label "Minusi&#324;sk"
  ]
  node [
    id 565
    label "Szk&#322;&#243;w"
  ]
  node [
    id 566
    label "Wenecja"
  ]
  node [
    id 567
    label "Bazylea"
  ]
  node [
    id 568
    label "Peszt"
  ]
  node [
    id 569
    label "Piza"
  ]
  node [
    id 570
    label "Tanger"
  ]
  node [
    id 571
    label "Krzywi&#324;"
  ]
  node [
    id 572
    label "Eger"
  ]
  node [
    id 573
    label "Bogus&#322;aw"
  ]
  node [
    id 574
    label "Taganrog"
  ]
  node [
    id 575
    label "Oksford"
  ]
  node [
    id 576
    label "Gwardiejsk"
  ]
  node [
    id 577
    label "Tyraspol"
  ]
  node [
    id 578
    label "Kleczew"
  ]
  node [
    id 579
    label "Nowa_D&#281;ba"
  ]
  node [
    id 580
    label "Wilejka"
  ]
  node [
    id 581
    label "Modena"
  ]
  node [
    id 582
    label "Demmin"
  ]
  node [
    id 583
    label "Houston"
  ]
  node [
    id 584
    label "Rydu&#322;towy"
  ]
  node [
    id 585
    label "Bordeaux"
  ]
  node [
    id 586
    label "Schmalkalden"
  ]
  node [
    id 587
    label "O&#322;omuniec"
  ]
  node [
    id 588
    label "Tuluza"
  ]
  node [
    id 589
    label "tramwaj"
  ]
  node [
    id 590
    label "Nantes"
  ]
  node [
    id 591
    label "Debreczyn"
  ]
  node [
    id 592
    label "Kowel"
  ]
  node [
    id 593
    label "Witnica"
  ]
  node [
    id 594
    label "Stalingrad"
  ]
  node [
    id 595
    label "Drezno"
  ]
  node [
    id 596
    label "Perejas&#322;aw"
  ]
  node [
    id 597
    label "Luksor"
  ]
  node [
    id 598
    label "Ostaszk&#243;w"
  ]
  node [
    id 599
    label "Gettysburg"
  ]
  node [
    id 600
    label "Trydent"
  ]
  node [
    id 601
    label "Poczdam"
  ]
  node [
    id 602
    label "Mesyna"
  ]
  node [
    id 603
    label "Krasnogorsk"
  ]
  node [
    id 604
    label "Kars"
  ]
  node [
    id 605
    label "Darmstadt"
  ]
  node [
    id 606
    label "Rzg&#243;w"
  ]
  node [
    id 607
    label "Kar&#322;owice"
  ]
  node [
    id 608
    label "Czeskie_Budziejowice"
  ]
  node [
    id 609
    label "Buda"
  ]
  node [
    id 610
    label "Monako"
  ]
  node [
    id 611
    label "Pardubice"
  ]
  node [
    id 612
    label "Pas&#322;&#281;k"
  ]
  node [
    id 613
    label "Fatima"
  ]
  node [
    id 614
    label "Bir&#380;e"
  ]
  node [
    id 615
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 616
    label "Wi&#322;komierz"
  ]
  node [
    id 617
    label "Opawa"
  ]
  node [
    id 618
    label "Mantua"
  ]
  node [
    id 619
    label "ulica"
  ]
  node [
    id 620
    label "Tarragona"
  ]
  node [
    id 621
    label "Antwerpia"
  ]
  node [
    id 622
    label "Asuan"
  ]
  node [
    id 623
    label "Korynt"
  ]
  node [
    id 624
    label "Armenia"
  ]
  node [
    id 625
    label "Budionnowsk"
  ]
  node [
    id 626
    label "Lengyel"
  ]
  node [
    id 627
    label "Betlejem"
  ]
  node [
    id 628
    label "Asy&#380;"
  ]
  node [
    id 629
    label "Batumi"
  ]
  node [
    id 630
    label "Paczk&#243;w"
  ]
  node [
    id 631
    label "Grenada"
  ]
  node [
    id 632
    label "Suczawa"
  ]
  node [
    id 633
    label "Nowogard"
  ]
  node [
    id 634
    label "Tyr"
  ]
  node [
    id 635
    label "Bria&#324;sk"
  ]
  node [
    id 636
    label "Bar"
  ]
  node [
    id 637
    label "Czerkiesk"
  ]
  node [
    id 638
    label "Ja&#322;ta"
  ]
  node [
    id 639
    label "Mo&#347;ciska"
  ]
  node [
    id 640
    label "Medyna"
  ]
  node [
    id 641
    label "Tartu"
  ]
  node [
    id 642
    label "Pemba"
  ]
  node [
    id 643
    label "Lipawa"
  ]
  node [
    id 644
    label "Tyl&#380;a"
  ]
  node [
    id 645
    label "Lipsk"
  ]
  node [
    id 646
    label "Dayton"
  ]
  node [
    id 647
    label "Rohatyn"
  ]
  node [
    id 648
    label "Peszawar"
  ]
  node [
    id 649
    label "Azow"
  ]
  node [
    id 650
    label "Adrianopol"
  ]
  node [
    id 651
    label "Iwano-Frankowsk"
  ]
  node [
    id 652
    label "Czarnobyl"
  ]
  node [
    id 653
    label "Rakoniewice"
  ]
  node [
    id 654
    label "Obuch&#243;w"
  ]
  node [
    id 655
    label "Orneta"
  ]
  node [
    id 656
    label "Koszyce"
  ]
  node [
    id 657
    label "Czeski_Cieszyn"
  ]
  node [
    id 658
    label "Zagorsk"
  ]
  node [
    id 659
    label "Nieder_Selters"
  ]
  node [
    id 660
    label "Ko&#322;omna"
  ]
  node [
    id 661
    label "Rost&#243;w"
  ]
  node [
    id 662
    label "Bolonia"
  ]
  node [
    id 663
    label "Rajgr&#243;d"
  ]
  node [
    id 664
    label "L&#252;neburg"
  ]
  node [
    id 665
    label "Brack"
  ]
  node [
    id 666
    label "Konstancja"
  ]
  node [
    id 667
    label "Koluszki"
  ]
  node [
    id 668
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 669
    label "Suez"
  ]
  node [
    id 670
    label "Mrocza"
  ]
  node [
    id 671
    label "Triest"
  ]
  node [
    id 672
    label "Murma&#324;sk"
  ]
  node [
    id 673
    label "Tu&#322;a"
  ]
  node [
    id 674
    label "Tarnogr&#243;d"
  ]
  node [
    id 675
    label "Radziech&#243;w"
  ]
  node [
    id 676
    label "Kokand"
  ]
  node [
    id 677
    label "Kircholm"
  ]
  node [
    id 678
    label "Nowa_Ruda"
  ]
  node [
    id 679
    label "Huma&#324;"
  ]
  node [
    id 680
    label "Turkiestan"
  ]
  node [
    id 681
    label "Kani&#243;w"
  ]
  node [
    id 682
    label "Pilzno"
  ]
  node [
    id 683
    label "Dubno"
  ]
  node [
    id 684
    label "Bras&#322;aw"
  ]
  node [
    id 685
    label "Korfant&#243;w"
  ]
  node [
    id 686
    label "Choroszcz"
  ]
  node [
    id 687
    label "Nowogr&#243;d"
  ]
  node [
    id 688
    label "Konotop"
  ]
  node [
    id 689
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 690
    label "Jastarnia"
  ]
  node [
    id 691
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 692
    label "Omsk"
  ]
  node [
    id 693
    label "Troick"
  ]
  node [
    id 694
    label "Koper"
  ]
  node [
    id 695
    label "Jenisejsk"
  ]
  node [
    id 696
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 697
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 698
    label "Trenczyn"
  ]
  node [
    id 699
    label "Wormacja"
  ]
  node [
    id 700
    label "Wagram"
  ]
  node [
    id 701
    label "Lubeka"
  ]
  node [
    id 702
    label "Genewa"
  ]
  node [
    id 703
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 704
    label "Kleck"
  ]
  node [
    id 705
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 706
    label "Struga"
  ]
  node [
    id 707
    label "Izmir"
  ]
  node [
    id 708
    label "Dortmund"
  ]
  node [
    id 709
    label "Izbica_Kujawska"
  ]
  node [
    id 710
    label "Stalinogorsk"
  ]
  node [
    id 711
    label "Workuta"
  ]
  node [
    id 712
    label "Jerycho"
  ]
  node [
    id 713
    label "Brunszwik"
  ]
  node [
    id 714
    label "Aleksandria"
  ]
  node [
    id 715
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 716
    label "Borys&#322;aw"
  ]
  node [
    id 717
    label "Zaleszczyki"
  ]
  node [
    id 718
    label "Z&#322;oczew"
  ]
  node [
    id 719
    label "Piast&#243;w"
  ]
  node [
    id 720
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 721
    label "Bor"
  ]
  node [
    id 722
    label "Nazaret"
  ]
  node [
    id 723
    label "Sarat&#243;w"
  ]
  node [
    id 724
    label "Brasz&#243;w"
  ]
  node [
    id 725
    label "Malin"
  ]
  node [
    id 726
    label "Parma"
  ]
  node [
    id 727
    label "Wierchoja&#324;sk"
  ]
  node [
    id 728
    label "Tarent"
  ]
  node [
    id 729
    label "Mariampol"
  ]
  node [
    id 730
    label "Wuhan"
  ]
  node [
    id 731
    label "Split"
  ]
  node [
    id 732
    label "Baranowicze"
  ]
  node [
    id 733
    label "Marki"
  ]
  node [
    id 734
    label "Adana"
  ]
  node [
    id 735
    label "B&#322;aszki"
  ]
  node [
    id 736
    label "Lubecz"
  ]
  node [
    id 737
    label "Sulech&#243;w"
  ]
  node [
    id 738
    label "Borys&#243;w"
  ]
  node [
    id 739
    label "Homel"
  ]
  node [
    id 740
    label "Tours"
  ]
  node [
    id 741
    label "Kapsztad"
  ]
  node [
    id 742
    label "Edam"
  ]
  node [
    id 743
    label "Zaporo&#380;e"
  ]
  node [
    id 744
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 745
    label "Kamieniec_Podolski"
  ]
  node [
    id 746
    label "Chocim"
  ]
  node [
    id 747
    label "Mohylew"
  ]
  node [
    id 748
    label "Merseburg"
  ]
  node [
    id 749
    label "Konstantynopol"
  ]
  node [
    id 750
    label "Sambor"
  ]
  node [
    id 751
    label "Manchester"
  ]
  node [
    id 752
    label "Pi&#324;sk"
  ]
  node [
    id 753
    label "Ochryda"
  ]
  node [
    id 754
    label "Rybi&#324;sk"
  ]
  node [
    id 755
    label "Czadca"
  ]
  node [
    id 756
    label "Orenburg"
  ]
  node [
    id 757
    label "Krajowa"
  ]
  node [
    id 758
    label "Eleusis"
  ]
  node [
    id 759
    label "Awinion"
  ]
  node [
    id 760
    label "Rzeczyca"
  ]
  node [
    id 761
    label "Barczewo"
  ]
  node [
    id 762
    label "Lozanna"
  ]
  node [
    id 763
    label "&#379;migr&#243;d"
  ]
  node [
    id 764
    label "Chabarowsk"
  ]
  node [
    id 765
    label "Jena"
  ]
  node [
    id 766
    label "Xai-Xai"
  ]
  node [
    id 767
    label "Radk&#243;w"
  ]
  node [
    id 768
    label "Syrakuzy"
  ]
  node [
    id 769
    label "Zas&#322;aw"
  ]
  node [
    id 770
    label "Getynga"
  ]
  node [
    id 771
    label "Windsor"
  ]
  node [
    id 772
    label "Carrara"
  ]
  node [
    id 773
    label "Madras"
  ]
  node [
    id 774
    label "Nitra"
  ]
  node [
    id 775
    label "Kilonia"
  ]
  node [
    id 776
    label "Rawenna"
  ]
  node [
    id 777
    label "Stawropol"
  ]
  node [
    id 778
    label "Warna"
  ]
  node [
    id 779
    label "Ba&#322;tijsk"
  ]
  node [
    id 780
    label "Cumana"
  ]
  node [
    id 781
    label "Kostroma"
  ]
  node [
    id 782
    label "Bajonna"
  ]
  node [
    id 783
    label "Magadan"
  ]
  node [
    id 784
    label "Kercz"
  ]
  node [
    id 785
    label "Harbin"
  ]
  node [
    id 786
    label "Sankt_Florian"
  ]
  node [
    id 787
    label "Norak"
  ]
  node [
    id 788
    label "Wo&#322;kowysk"
  ]
  node [
    id 789
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 790
    label "S&#232;vres"
  ]
  node [
    id 791
    label "Barwice"
  ]
  node [
    id 792
    label "Jutrosin"
  ]
  node [
    id 793
    label "Sumy"
  ]
  node [
    id 794
    label "Canterbury"
  ]
  node [
    id 795
    label "Czerkasy"
  ]
  node [
    id 796
    label "Troki"
  ]
  node [
    id 797
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 798
    label "Turka"
  ]
  node [
    id 799
    label "Budziszyn"
  ]
  node [
    id 800
    label "A&#322;czewsk"
  ]
  node [
    id 801
    label "Chark&#243;w"
  ]
  node [
    id 802
    label "Go&#347;cino"
  ]
  node [
    id 803
    label "Ku&#378;nieck"
  ]
  node [
    id 804
    label "Wotki&#324;sk"
  ]
  node [
    id 805
    label "Symferopol"
  ]
  node [
    id 806
    label "Dmitrow"
  ]
  node [
    id 807
    label "Cherso&#324;"
  ]
  node [
    id 808
    label "zabudowa"
  ]
  node [
    id 809
    label "Nowogr&#243;dek"
  ]
  node [
    id 810
    label "Orlean"
  ]
  node [
    id 811
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 812
    label "Berdia&#324;sk"
  ]
  node [
    id 813
    label "Szumsk"
  ]
  node [
    id 814
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 815
    label "Orsza"
  ]
  node [
    id 816
    label "Cluny"
  ]
  node [
    id 817
    label "Aralsk"
  ]
  node [
    id 818
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 819
    label "Bogumin"
  ]
  node [
    id 820
    label "Antiochia"
  ]
  node [
    id 821
    label "Inhambane"
  ]
  node [
    id 822
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 823
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 824
    label "Trewir"
  ]
  node [
    id 825
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 826
    label "Siewieromorsk"
  ]
  node [
    id 827
    label "Calais"
  ]
  node [
    id 828
    label "&#379;ytawa"
  ]
  node [
    id 829
    label "Eupatoria"
  ]
  node [
    id 830
    label "Twer"
  ]
  node [
    id 831
    label "Stara_Zagora"
  ]
  node [
    id 832
    label "Jastrowie"
  ]
  node [
    id 833
    label "Piatigorsk"
  ]
  node [
    id 834
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 835
    label "Le&#324;sk"
  ]
  node [
    id 836
    label "Johannesburg"
  ]
  node [
    id 837
    label "Kaszyn"
  ]
  node [
    id 838
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 839
    label "&#379;ylina"
  ]
  node [
    id 840
    label "Sewastopol"
  ]
  node [
    id 841
    label "Pietrozawodsk"
  ]
  node [
    id 842
    label "Bobolice"
  ]
  node [
    id 843
    label "Mosty"
  ]
  node [
    id 844
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 845
    label "Karaganda"
  ]
  node [
    id 846
    label "Marsylia"
  ]
  node [
    id 847
    label "Buchara"
  ]
  node [
    id 848
    label "Dubrownik"
  ]
  node [
    id 849
    label "Be&#322;z"
  ]
  node [
    id 850
    label "Oran"
  ]
  node [
    id 851
    label "Regensburg"
  ]
  node [
    id 852
    label "Rotterdam"
  ]
  node [
    id 853
    label "Trembowla"
  ]
  node [
    id 854
    label "Woskriesiensk"
  ]
  node [
    id 855
    label "Po&#322;ock"
  ]
  node [
    id 856
    label "Poprad"
  ]
  node [
    id 857
    label "Los_Angeles"
  ]
  node [
    id 858
    label "Kronsztad"
  ]
  node [
    id 859
    label "U&#322;an_Ude"
  ]
  node [
    id 860
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 861
    label "W&#322;adywostok"
  ]
  node [
    id 862
    label "Kandahar"
  ]
  node [
    id 863
    label "Tobolsk"
  ]
  node [
    id 864
    label "Boston"
  ]
  node [
    id 865
    label "Hawana"
  ]
  node [
    id 866
    label "Kis&#322;owodzk"
  ]
  node [
    id 867
    label "Tulon"
  ]
  node [
    id 868
    label "Utrecht"
  ]
  node [
    id 869
    label "Oleszyce"
  ]
  node [
    id 870
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 871
    label "Katania"
  ]
  node [
    id 872
    label "Teby"
  ]
  node [
    id 873
    label "Paw&#322;owo"
  ]
  node [
    id 874
    label "W&#252;rzburg"
  ]
  node [
    id 875
    label "Podiebrady"
  ]
  node [
    id 876
    label "Uppsala"
  ]
  node [
    id 877
    label "Poniewie&#380;"
  ]
  node [
    id 878
    label "Berezyna"
  ]
  node [
    id 879
    label "Aczy&#324;sk"
  ]
  node [
    id 880
    label "Niko&#322;ajewsk"
  ]
  node [
    id 881
    label "Ostr&#243;g"
  ]
  node [
    id 882
    label "Brze&#347;&#263;"
  ]
  node [
    id 883
    label "Stryj"
  ]
  node [
    id 884
    label "Lancaster"
  ]
  node [
    id 885
    label "Kozielsk"
  ]
  node [
    id 886
    label "Loreto"
  ]
  node [
    id 887
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 888
    label "Hebron"
  ]
  node [
    id 889
    label "Kaspijsk"
  ]
  node [
    id 890
    label "Peczora"
  ]
  node [
    id 891
    label "Isfahan"
  ]
  node [
    id 892
    label "Chimoio"
  ]
  node [
    id 893
    label "Mory&#324;"
  ]
  node [
    id 894
    label "Kowno"
  ]
  node [
    id 895
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 896
    label "Opalenica"
  ]
  node [
    id 897
    label "Kolonia"
  ]
  node [
    id 898
    label "Stary_Sambor"
  ]
  node [
    id 899
    label "Kolkata"
  ]
  node [
    id 900
    label "Turkmenbaszy"
  ]
  node [
    id 901
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 902
    label "Nankin"
  ]
  node [
    id 903
    label "Krzanowice"
  ]
  node [
    id 904
    label "Efez"
  ]
  node [
    id 905
    label "Dobrodzie&#324;"
  ]
  node [
    id 906
    label "Neapol"
  ]
  node [
    id 907
    label "S&#322;uck"
  ]
  node [
    id 908
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 909
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 910
    label "Frydek-Mistek"
  ]
  node [
    id 911
    label "Korsze"
  ]
  node [
    id 912
    label "T&#322;uszcz"
  ]
  node [
    id 913
    label "Soligorsk"
  ]
  node [
    id 914
    label "Kie&#380;mark"
  ]
  node [
    id 915
    label "Mannheim"
  ]
  node [
    id 916
    label "Ulm"
  ]
  node [
    id 917
    label "Podhajce"
  ]
  node [
    id 918
    label "Dniepropetrowsk"
  ]
  node [
    id 919
    label "Szamocin"
  ]
  node [
    id 920
    label "Ko&#322;omyja"
  ]
  node [
    id 921
    label "Buczacz"
  ]
  node [
    id 922
    label "M&#252;nster"
  ]
  node [
    id 923
    label "Brema"
  ]
  node [
    id 924
    label "Delhi"
  ]
  node [
    id 925
    label "Nicea"
  ]
  node [
    id 926
    label "&#346;niatyn"
  ]
  node [
    id 927
    label "Szawle"
  ]
  node [
    id 928
    label "Czerniowce"
  ]
  node [
    id 929
    label "Mi&#347;nia"
  ]
  node [
    id 930
    label "Sydney"
  ]
  node [
    id 931
    label "Moguncja"
  ]
  node [
    id 932
    label "Narbona"
  ]
  node [
    id 933
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 934
    label "Wittenberga"
  ]
  node [
    id 935
    label "Uljanowsk"
  ]
  node [
    id 936
    label "Wyborg"
  ]
  node [
    id 937
    label "&#321;uga&#324;sk"
  ]
  node [
    id 938
    label "Trojan"
  ]
  node [
    id 939
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 940
    label "Brandenburg"
  ]
  node [
    id 941
    label "Kemerowo"
  ]
  node [
    id 942
    label "Kaszgar"
  ]
  node [
    id 943
    label "Lenzen"
  ]
  node [
    id 944
    label "Nanning"
  ]
  node [
    id 945
    label "Gotha"
  ]
  node [
    id 946
    label "Zurych"
  ]
  node [
    id 947
    label "Baltimore"
  ]
  node [
    id 948
    label "&#321;uck"
  ]
  node [
    id 949
    label "Bristol"
  ]
  node [
    id 950
    label "Ferrara"
  ]
  node [
    id 951
    label "Mariupol"
  ]
  node [
    id 952
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 953
    label "Filadelfia"
  ]
  node [
    id 954
    label "Czerniejewo"
  ]
  node [
    id 955
    label "Milan&#243;wek"
  ]
  node [
    id 956
    label "Lhasa"
  ]
  node [
    id 957
    label "Kanton"
  ]
  node [
    id 958
    label "Perwomajsk"
  ]
  node [
    id 959
    label "Nieftiegorsk"
  ]
  node [
    id 960
    label "Greifswald"
  ]
  node [
    id 961
    label "Pittsburgh"
  ]
  node [
    id 962
    label "Akwileja"
  ]
  node [
    id 963
    label "Norfolk"
  ]
  node [
    id 964
    label "Perm"
  ]
  node [
    id 965
    label "Fergana"
  ]
  node [
    id 966
    label "Detroit"
  ]
  node [
    id 967
    label "Starobielsk"
  ]
  node [
    id 968
    label "Wielsk"
  ]
  node [
    id 969
    label "Zaklik&#243;w"
  ]
  node [
    id 970
    label "Majsur"
  ]
  node [
    id 971
    label "Narwa"
  ]
  node [
    id 972
    label "Chicago"
  ]
  node [
    id 973
    label "Byczyna"
  ]
  node [
    id 974
    label "Mozyrz"
  ]
  node [
    id 975
    label "Konstantyn&#243;wka"
  ]
  node [
    id 976
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 977
    label "Megara"
  ]
  node [
    id 978
    label "Stralsund"
  ]
  node [
    id 979
    label "Wo&#322;gograd"
  ]
  node [
    id 980
    label "Lichinga"
  ]
  node [
    id 981
    label "Haga"
  ]
  node [
    id 982
    label "Tarnopol"
  ]
  node [
    id 983
    label "Nowomoskowsk"
  ]
  node [
    id 984
    label "K&#322;ajpeda"
  ]
  node [
    id 985
    label "Ussuryjsk"
  ]
  node [
    id 986
    label "Brugia"
  ]
  node [
    id 987
    label "Natal"
  ]
  node [
    id 988
    label "Kro&#347;niewice"
  ]
  node [
    id 989
    label "Edynburg"
  ]
  node [
    id 990
    label "Marburg"
  ]
  node [
    id 991
    label "Dalton"
  ]
  node [
    id 992
    label "S&#322;onim"
  ]
  node [
    id 993
    label "&#346;wiebodzice"
  ]
  node [
    id 994
    label "Smorgonie"
  ]
  node [
    id 995
    label "Orze&#322;"
  ]
  node [
    id 996
    label "Nowoku&#378;nieck"
  ]
  node [
    id 997
    label "Zadar"
  ]
  node [
    id 998
    label "Koprzywnica"
  ]
  node [
    id 999
    label "Angarsk"
  ]
  node [
    id 1000
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 1001
    label "Mo&#380;ajsk"
  ]
  node [
    id 1002
    label "Norylsk"
  ]
  node [
    id 1003
    label "Akwizgran"
  ]
  node [
    id 1004
    label "Jawor&#243;w"
  ]
  node [
    id 1005
    label "weduta"
  ]
  node [
    id 1006
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 1007
    label "Suzdal"
  ]
  node [
    id 1008
    label "W&#322;odzimierz"
  ]
  node [
    id 1009
    label "Bujnaksk"
  ]
  node [
    id 1010
    label "Beresteczko"
  ]
  node [
    id 1011
    label "Strzelno"
  ]
  node [
    id 1012
    label "Siewsk"
  ]
  node [
    id 1013
    label "Cymlansk"
  ]
  node [
    id 1014
    label "Trzyniec"
  ]
  node [
    id 1015
    label "Hanower"
  ]
  node [
    id 1016
    label "Wuppertal"
  ]
  node [
    id 1017
    label "Sura&#380;"
  ]
  node [
    id 1018
    label "Samara"
  ]
  node [
    id 1019
    label "Winchester"
  ]
  node [
    id 1020
    label "Krasnodar"
  ]
  node [
    id 1021
    label "Sydon"
  ]
  node [
    id 1022
    label "Worone&#380;"
  ]
  node [
    id 1023
    label "Paw&#322;odar"
  ]
  node [
    id 1024
    label "Czelabi&#324;sk"
  ]
  node [
    id 1025
    label "Reda"
  ]
  node [
    id 1026
    label "Karwina"
  ]
  node [
    id 1027
    label "Wyszehrad"
  ]
  node [
    id 1028
    label "Sara&#324;sk"
  ]
  node [
    id 1029
    label "Koby&#322;ka"
  ]
  node [
    id 1030
    label "Tambow"
  ]
  node [
    id 1031
    label "Pyskowice"
  ]
  node [
    id 1032
    label "Winnica"
  ]
  node [
    id 1033
    label "Heidelberg"
  ]
  node [
    id 1034
    label "Maribor"
  ]
  node [
    id 1035
    label "Werona"
  ]
  node [
    id 1036
    label "G&#322;uszyca"
  ]
  node [
    id 1037
    label "Rostock"
  ]
  node [
    id 1038
    label "Mekka"
  ]
  node [
    id 1039
    label "Liberec"
  ]
  node [
    id 1040
    label "Bie&#322;gorod"
  ]
  node [
    id 1041
    label "Berdycz&#243;w"
  ]
  node [
    id 1042
    label "Sierdobsk"
  ]
  node [
    id 1043
    label "Bobrujsk"
  ]
  node [
    id 1044
    label "Padwa"
  ]
  node [
    id 1045
    label "Chanty-Mansyjsk"
  ]
  node [
    id 1046
    label "Pasawa"
  ]
  node [
    id 1047
    label "Poczaj&#243;w"
  ]
  node [
    id 1048
    label "&#379;ar&#243;w"
  ]
  node [
    id 1049
    label "Barabi&#324;sk"
  ]
  node [
    id 1050
    label "Gorycja"
  ]
  node [
    id 1051
    label "Haarlem"
  ]
  node [
    id 1052
    label "Kiejdany"
  ]
  node [
    id 1053
    label "Chmielnicki"
  ]
  node [
    id 1054
    label "Siena"
  ]
  node [
    id 1055
    label "Burgas"
  ]
  node [
    id 1056
    label "Magnitogorsk"
  ]
  node [
    id 1057
    label "Korzec"
  ]
  node [
    id 1058
    label "Bonn"
  ]
  node [
    id 1059
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 1060
    label "Walencja"
  ]
  node [
    id 1061
    label "Mosina"
  ]
  node [
    id 1062
    label "zimowy"
  ]
  node [
    id 1063
    label "nocny"
  ]
  node [
    id 1064
    label "&#347;nie&#380;ny"
  ]
  node [
    id 1065
    label "mro&#378;ny"
  ]
  node [
    id 1066
    label "przesta&#263;"
  ]
  node [
    id 1067
    label "cause"
  ]
  node [
    id 1068
    label "communicate"
  ]
  node [
    id 1069
    label "zbrodnia"
  ]
  node [
    id 1070
    label "genocide"
  ]
  node [
    id 1071
    label "zag&#322;ada"
  ]
  node [
    id 1072
    label "ch&#322;opstwo"
  ]
  node [
    id 1073
    label "innowierstwo"
  ]
  node [
    id 1074
    label "cywilnie"
  ]
  node [
    id 1075
    label "majority"
  ]
  node [
    id 1076
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1077
    label "Zgredek"
  ]
  node [
    id 1078
    label "kategoria_gramatyczna"
  ]
  node [
    id 1079
    label "Casanova"
  ]
  node [
    id 1080
    label "Don_Juan"
  ]
  node [
    id 1081
    label "Gargantua"
  ]
  node [
    id 1082
    label "Faust"
  ]
  node [
    id 1083
    label "profanum"
  ]
  node [
    id 1084
    label "Chocho&#322;"
  ]
  node [
    id 1085
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 1086
    label "koniugacja"
  ]
  node [
    id 1087
    label "Winnetou"
  ]
  node [
    id 1088
    label "Dwukwiat"
  ]
  node [
    id 1089
    label "homo_sapiens"
  ]
  node [
    id 1090
    label "Edyp"
  ]
  node [
    id 1091
    label "Herkules_Poirot"
  ]
  node [
    id 1092
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1093
    label "mikrokosmos"
  ]
  node [
    id 1094
    label "person"
  ]
  node [
    id 1095
    label "Szwejk"
  ]
  node [
    id 1096
    label "portrecista"
  ]
  node [
    id 1097
    label "Sherlock_Holmes"
  ]
  node [
    id 1098
    label "Hamlet"
  ]
  node [
    id 1099
    label "duch"
  ]
  node [
    id 1100
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1101
    label "g&#322;owa"
  ]
  node [
    id 1102
    label "Quasimodo"
  ]
  node [
    id 1103
    label "Dulcynea"
  ]
  node [
    id 1104
    label "Wallenrod"
  ]
  node [
    id 1105
    label "Don_Kiszot"
  ]
  node [
    id 1106
    label "Plastu&#347;"
  ]
  node [
    id 1107
    label "Harry_Potter"
  ]
  node [
    id 1108
    label "figura"
  ]
  node [
    id 1109
    label "parali&#380;owa&#263;"
  ]
  node [
    id 1110
    label "istota"
  ]
  node [
    id 1111
    label "Werter"
  ]
  node [
    id 1112
    label "antropochoria"
  ]
  node [
    id 1113
    label "posta&#263;"
  ]
  node [
    id 1114
    label "warto&#347;ciowy"
  ]
  node [
    id 1115
    label "du&#380;y"
  ]
  node [
    id 1116
    label "wysoce"
  ]
  node [
    id 1117
    label "daleki"
  ]
  node [
    id 1118
    label "wysoko"
  ]
  node [
    id 1119
    label "szczytnie"
  ]
  node [
    id 1120
    label "wznios&#322;y"
  ]
  node [
    id 1121
    label "wyrafinowany"
  ]
  node [
    id 1122
    label "z_wysoka"
  ]
  node [
    id 1123
    label "chwalebny"
  ]
  node [
    id 1124
    label "uprzywilejowany"
  ]
  node [
    id 1125
    label "niepo&#347;ledni"
  ]
  node [
    id 1126
    label "kwalifikacje"
  ]
  node [
    id 1127
    label "niepokalanki"
  ]
  node [
    id 1128
    label "sophistication"
  ]
  node [
    id 1129
    label "form"
  ]
  node [
    id 1130
    label "training"
  ]
  node [
    id 1131
    label "pomo&#380;enie"
  ]
  node [
    id 1132
    label "wiedza"
  ]
  node [
    id 1133
    label "zapoznanie"
  ]
  node [
    id 1134
    label "rozwini&#281;cie"
  ]
  node [
    id 1135
    label "wys&#322;anie"
  ]
  node [
    id 1136
    label "o&#347;wiecenie"
  ]
  node [
    id 1137
    label "skolaryzacja"
  ]
  node [
    id 1138
    label "udoskonalenie"
  ]
  node [
    id 1139
    label "urszulanki"
  ]
  node [
    id 1140
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 1141
    label "delegowa&#263;"
  ]
  node [
    id 1142
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 1143
    label "pracu&#347;"
  ]
  node [
    id 1144
    label "delegowanie"
  ]
  node [
    id 1145
    label "r&#281;ka"
  ]
  node [
    id 1146
    label "salariat"
  ]
  node [
    id 1147
    label "zarz&#261;d"
  ]
  node [
    id 1148
    label "gospodarka"
  ]
  node [
    id 1149
    label "petent"
  ]
  node [
    id 1150
    label "biuro"
  ]
  node [
    id 1151
    label "dziekanat"
  ]
  node [
    id 1152
    label "siedziba"
  ]
  node [
    id 1153
    label "wygl&#261;d"
  ]
  node [
    id 1154
    label "comeliness"
  ]
  node [
    id 1155
    label "twarz"
  ]
  node [
    id 1156
    label "facjata"
  ]
  node [
    id 1157
    label "charakter"
  ]
  node [
    id 1158
    label "face"
  ]
  node [
    id 1159
    label "lina"
  ]
  node [
    id 1160
    label "way"
  ]
  node [
    id 1161
    label "cable"
  ]
  node [
    id 1162
    label "przebieg"
  ]
  node [
    id 1163
    label "ch&#243;d"
  ]
  node [
    id 1164
    label "trasa"
  ]
  node [
    id 1165
    label "rz&#261;d"
  ]
  node [
    id 1166
    label "k&#322;us"
  ]
  node [
    id 1167
    label "progression"
  ]
  node [
    id 1168
    label "current"
  ]
  node [
    id 1169
    label "pr&#261;d"
  ]
  node [
    id 1170
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1171
    label "lot"
  ]
  node [
    id 1172
    label "pracowanie"
  ]
  node [
    id 1173
    label "zachowanie"
  ]
  node [
    id 1174
    label "behavior"
  ]
  node [
    id 1175
    label "czynno&#347;&#263;"
  ]
  node [
    id 1176
    label "examination"
  ]
  node [
    id 1177
    label "legalizacja_pierwotna"
  ]
  node [
    id 1178
    label "w&#322;adza"
  ]
  node [
    id 1179
    label "perlustracja"
  ]
  node [
    id 1180
    label "instytucja"
  ]
  node [
    id 1181
    label "legalizacja_ponowna"
  ]
  node [
    id 1182
    label "pomordowa&#263;"
  ]
  node [
    id 1183
    label "wybi&#263;"
  ]
  node [
    id 1184
    label "killing"
  ]
  node [
    id 1185
    label "&#347;lub"
  ]
  node [
    id 1186
    label "doznawa&#263;"
  ]
  node [
    id 1187
    label "znachodzi&#263;"
  ]
  node [
    id 1188
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 1189
    label "pozyskiwa&#263;"
  ]
  node [
    id 1190
    label "odzyskiwa&#263;"
  ]
  node [
    id 1191
    label "os&#261;dza&#263;"
  ]
  node [
    id 1192
    label "wykrywa&#263;"
  ]
  node [
    id 1193
    label "unwrap"
  ]
  node [
    id 1194
    label "detect"
  ]
  node [
    id 1195
    label "wymy&#347;la&#263;"
  ]
  node [
    id 1196
    label "powodowa&#263;"
  ]
  node [
    id 1197
    label "przedstawiciel"
  ]
  node [
    id 1198
    label "miastowy"
  ]
  node [
    id 1199
    label "mieszkaniec"
  ]
  node [
    id 1200
    label "kurant"
  ]
  node [
    id 1201
    label "menuet"
  ]
  node [
    id 1202
    label "nami&#281;tny"
  ]
  node [
    id 1203
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1204
    label "europejski"
  ]
  node [
    id 1205
    label "po_francusku"
  ]
  node [
    id 1206
    label "chrancuski"
  ]
  node [
    id 1207
    label "francuz"
  ]
  node [
    id 1208
    label "verlan"
  ]
  node [
    id 1209
    label "zachodnioeuropejski"
  ]
  node [
    id 1210
    label "bourr&#233;e"
  ]
  node [
    id 1211
    label "French"
  ]
  node [
    id 1212
    label "j&#281;zyk_roma&#324;ski"
  ]
  node [
    id 1213
    label "frankofonia"
  ]
  node [
    id 1214
    label "farandola"
  ]
  node [
    id 1215
    label "ofensywa"
  ]
  node [
    id 1216
    label "Tet"
  ]
  node [
    id 1217
    label "Wietnam"
  ]
  node [
    id 1218
    label "mig"
  ]
  node [
    id 1219
    label "17"
  ]
  node [
    id 1220
    label "tom"
  ]
  node [
    id 1221
    label "55"
  ]
  node [
    id 1222
    label "AK"
  ]
  node [
    id 1223
    label "47"
  ]
  node [
    id 1224
    label "21"
  ]
  node [
    id 1225
    label "ludowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 1225
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 204
  ]
  edge [
    source 16
    target 205
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 207
  ]
  edge [
    source 16
    target 208
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 16
    target 213
  ]
  edge [
    source 16
    target 214
  ]
  edge [
    source 16
    target 215
  ]
  edge [
    source 16
    target 216
  ]
  edge [
    source 16
    target 217
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 47
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 229
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 18
    target 233
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 18
    target 235
  ]
  edge [
    source 18
    target 236
  ]
  edge [
    source 18
    target 237
  ]
  edge [
    source 18
    target 238
  ]
  edge [
    source 18
    target 239
  ]
  edge [
    source 18
    target 240
  ]
  edge [
    source 18
    target 241
  ]
  edge [
    source 18
    target 242
  ]
  edge [
    source 18
    target 243
  ]
  edge [
    source 18
    target 244
  ]
  edge [
    source 18
    target 245
  ]
  edge [
    source 18
    target 246
  ]
  edge [
    source 18
    target 247
  ]
  edge [
    source 18
    target 248
  ]
  edge [
    source 18
    target 83
  ]
  edge [
    source 18
    target 249
  ]
  edge [
    source 18
    target 87
  ]
  edge [
    source 18
    target 250
  ]
  edge [
    source 18
    target 251
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 252
  ]
  edge [
    source 19
    target 253
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 254
  ]
  edge [
    source 20
    target 255
  ]
  edge [
    source 20
    target 256
  ]
  edge [
    source 20
    target 257
  ]
  edge [
    source 20
    target 258
  ]
  edge [
    source 20
    target 259
  ]
  edge [
    source 20
    target 260
  ]
  edge [
    source 20
    target 261
  ]
  edge [
    source 20
    target 262
  ]
  edge [
    source 20
    target 263
  ]
  edge [
    source 20
    target 264
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 266
  ]
  edge [
    source 20
    target 267
  ]
  edge [
    source 20
    target 268
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 269
  ]
  edge [
    source 21
    target 270
  ]
  edge [
    source 21
    target 271
  ]
  edge [
    source 21
    target 272
  ]
  edge [
    source 21
    target 273
  ]
  edge [
    source 21
    target 274
  ]
  edge [
    source 21
    target 275
  ]
  edge [
    source 21
    target 276
  ]
  edge [
    source 21
    target 277
  ]
  edge [
    source 21
    target 278
  ]
  edge [
    source 21
    target 279
  ]
  edge [
    source 21
    target 280
  ]
  edge [
    source 21
    target 281
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 282
  ]
  edge [
    source 22
    target 283
  ]
  edge [
    source 22
    target 284
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 285
  ]
  edge [
    source 22
    target 286
  ]
  edge [
    source 22
    target 287
  ]
  edge [
    source 22
    target 288
  ]
  edge [
    source 22
    target 289
  ]
  edge [
    source 22
    target 290
  ]
  edge [
    source 22
    target 291
  ]
  edge [
    source 22
    target 292
  ]
  edge [
    source 22
    target 293
  ]
  edge [
    source 22
    target 294
  ]
  edge [
    source 22
    target 295
  ]
  edge [
    source 22
    target 296
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 297
  ]
  edge [
    source 23
    target 298
  ]
  edge [
    source 23
    target 299
  ]
  edge [
    source 23
    target 300
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 125
  ]
  edge [
    source 25
    target 301
  ]
  edge [
    source 25
    target 302
  ]
  edge [
    source 25
    target 303
  ]
  edge [
    source 25
    target 304
  ]
  edge [
    source 26
    target 305
  ]
  edge [
    source 26
    target 306
  ]
  edge [
    source 26
    target 307
  ]
  edge [
    source 26
    target 308
  ]
  edge [
    source 26
    target 309
  ]
  edge [
    source 26
    target 310
  ]
  edge [
    source 26
    target 311
  ]
  edge [
    source 26
    target 312
  ]
  edge [
    source 26
    target 313
  ]
  edge [
    source 26
    target 314
  ]
  edge [
    source 26
    target 315
  ]
  edge [
    source 26
    target 316
  ]
  edge [
    source 26
    target 317
  ]
  edge [
    source 26
    target 318
  ]
  edge [
    source 26
    target 319
  ]
  edge [
    source 26
    target 320
  ]
  edge [
    source 26
    target 321
  ]
  edge [
    source 26
    target 322
  ]
  edge [
    source 26
    target 323
  ]
  edge [
    source 26
    target 324
  ]
  edge [
    source 26
    target 325
  ]
  edge [
    source 26
    target 326
  ]
  edge [
    source 26
    target 327
  ]
  edge [
    source 26
    target 328
  ]
  edge [
    source 26
    target 329
  ]
  edge [
    source 26
    target 330
  ]
  edge [
    source 26
    target 331
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 332
  ]
  edge [
    source 27
    target 333
  ]
  edge [
    source 27
    target 334
  ]
  edge [
    source 27
    target 335
  ]
  edge [
    source 27
    target 336
  ]
  edge [
    source 27
    target 337
  ]
  edge [
    source 27
    target 338
  ]
  edge [
    source 27
    target 339
  ]
  edge [
    source 27
    target 340
  ]
  edge [
    source 27
    target 288
  ]
  edge [
    source 27
    target 341
  ]
  edge [
    source 27
    target 342
  ]
  edge [
    source 27
    target 343
  ]
  edge [
    source 27
    target 344
  ]
  edge [
    source 27
    target 345
  ]
  edge [
    source 27
    target 346
  ]
  edge [
    source 27
    target 347
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 54
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 28
    target 64
  ]
  edge [
    source 28
    target 50
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 348
  ]
  edge [
    source 29
    target 349
  ]
  edge [
    source 29
    target 350
  ]
  edge [
    source 29
    target 351
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 66
  ]
  edge [
    source 30
    target 67
  ]
  edge [
    source 30
    target 352
  ]
  edge [
    source 30
    target 353
  ]
  edge [
    source 30
    target 354
  ]
  edge [
    source 30
    target 355
  ]
  edge [
    source 30
    target 356
  ]
  edge [
    source 30
    target 318
  ]
  edge [
    source 30
    target 357
  ]
  edge [
    source 30
    target 358
  ]
  edge [
    source 30
    target 359
  ]
  edge [
    source 30
    target 360
  ]
  edge [
    source 30
    target 361
  ]
  edge [
    source 30
    target 362
  ]
  edge [
    source 30
    target 363
  ]
  edge [
    source 30
    target 348
  ]
  edge [
    source 30
    target 364
  ]
  edge [
    source 30
    target 365
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 366
  ]
  edge [
    source 31
    target 367
  ]
  edge [
    source 31
    target 368
  ]
  edge [
    source 31
    target 369
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 370
  ]
  edge [
    source 32
    target 371
  ]
  edge [
    source 32
    target 372
  ]
  edge [
    source 32
    target 373
  ]
  edge [
    source 32
    target 374
  ]
  edge [
    source 32
    target 375
  ]
  edge [
    source 32
    target 376
  ]
  edge [
    source 32
    target 377
  ]
  edge [
    source 32
    target 365
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 378
  ]
  edge [
    source 33
    target 379
  ]
  edge [
    source 33
    target 380
  ]
  edge [
    source 33
    target 381
  ]
  edge [
    source 33
    target 382
  ]
  edge [
    source 33
    target 383
  ]
  edge [
    source 33
    target 384
  ]
  edge [
    source 33
    target 385
  ]
  edge [
    source 33
    target 386
  ]
  edge [
    source 33
    target 387
  ]
  edge [
    source 33
    target 388
  ]
  edge [
    source 33
    target 389
  ]
  edge [
    source 33
    target 390
  ]
  edge [
    source 33
    target 391
  ]
  edge [
    source 33
    target 392
  ]
  edge [
    source 33
    target 393
  ]
  edge [
    source 33
    target 394
  ]
  edge [
    source 33
    target 75
  ]
  edge [
    source 33
    target 395
  ]
  edge [
    source 33
    target 396
  ]
  edge [
    source 33
    target 397
  ]
  edge [
    source 33
    target 272
  ]
  edge [
    source 33
    target 398
  ]
  edge [
    source 33
    target 399
  ]
  edge [
    source 33
    target 400
  ]
  edge [
    source 33
    target 401
  ]
  edge [
    source 33
    target 402
  ]
  edge [
    source 33
    target 403
  ]
  edge [
    source 33
    target 404
  ]
  edge [
    source 33
    target 405
  ]
  edge [
    source 33
    target 406
  ]
  edge [
    source 33
    target 407
  ]
  edge [
    source 33
    target 408
  ]
  edge [
    source 33
    target 409
  ]
  edge [
    source 33
    target 410
  ]
  edge [
    source 33
    target 411
  ]
  edge [
    source 33
    target 412
  ]
  edge [
    source 33
    target 413
  ]
  edge [
    source 33
    target 414
  ]
  edge [
    source 33
    target 415
  ]
  edge [
    source 33
    target 416
  ]
  edge [
    source 33
    target 80
  ]
  edge [
    source 33
    target 417
  ]
  edge [
    source 33
    target 418
  ]
  edge [
    source 33
    target 419
  ]
  edge [
    source 33
    target 420
  ]
  edge [
    source 33
    target 421
  ]
  edge [
    source 33
    target 422
  ]
  edge [
    source 33
    target 163
  ]
  edge [
    source 33
    target 423
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 424
  ]
  edge [
    source 34
    target 425
  ]
  edge [
    source 34
    target 426
  ]
  edge [
    source 34
    target 427
  ]
  edge [
    source 34
    target 428
  ]
  edge [
    source 34
    target 429
  ]
  edge [
    source 34
    target 430
  ]
  edge [
    source 34
    target 431
  ]
  edge [
    source 34
    target 432
  ]
  edge [
    source 34
    target 433
  ]
  edge [
    source 34
    target 434
  ]
  edge [
    source 34
    target 435
  ]
  edge [
    source 34
    target 436
  ]
  edge [
    source 34
    target 437
  ]
  edge [
    source 34
    target 438
  ]
  edge [
    source 34
    target 439
  ]
  edge [
    source 34
    target 440
  ]
  edge [
    source 34
    target 441
  ]
  edge [
    source 34
    target 442
  ]
  edge [
    source 34
    target 443
  ]
  edge [
    source 34
    target 444
  ]
  edge [
    source 34
    target 445
  ]
  edge [
    source 34
    target 446
  ]
  edge [
    source 34
    target 447
  ]
  edge [
    source 34
    target 448
  ]
  edge [
    source 34
    target 449
  ]
  edge [
    source 34
    target 450
  ]
  edge [
    source 34
    target 1217
  ]
  edge [
    source 34
    target 1225
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 41
  ]
  edge [
    source 35
    target 40
  ]
  edge [
    source 35
    target 49
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 451
  ]
  edge [
    source 36
    target 452
  ]
  edge [
    source 36
    target 235
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 453
  ]
  edge [
    source 37
    target 454
  ]
  edge [
    source 37
    target 455
  ]
  edge [
    source 37
    target 456
  ]
  edge [
    source 37
    target 457
  ]
  edge [
    source 37
    target 458
  ]
  edge [
    source 37
    target 459
  ]
  edge [
    source 37
    target 460
  ]
  edge [
    source 37
    target 461
  ]
  edge [
    source 37
    target 462
  ]
  edge [
    source 37
    target 463
  ]
  edge [
    source 37
    target 464
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 465
  ]
  edge [
    source 38
    target 466
  ]
  edge [
    source 38
    target 467
  ]
  edge [
    source 38
    target 468
  ]
  edge [
    source 38
    target 469
  ]
  edge [
    source 38
    target 470
  ]
  edge [
    source 38
    target 471
  ]
  edge [
    source 38
    target 472
  ]
  edge [
    source 39
    target 473
  ]
  edge [
    source 39
    target 474
  ]
  edge [
    source 39
    target 475
  ]
  edge [
    source 39
    target 476
  ]
  edge [
    source 39
    target 477
  ]
  edge [
    source 39
    target 478
  ]
  edge [
    source 39
    target 428
  ]
  edge [
    source 39
    target 479
  ]
  edge [
    source 39
    target 480
  ]
  edge [
    source 39
    target 481
  ]
  edge [
    source 39
    target 482
  ]
  edge [
    source 39
    target 483
  ]
  edge [
    source 39
    target 484
  ]
  edge [
    source 39
    target 485
  ]
  edge [
    source 39
    target 486
  ]
  edge [
    source 39
    target 487
  ]
  edge [
    source 39
    target 488
  ]
  edge [
    source 39
    target 489
  ]
  edge [
    source 39
    target 490
  ]
  edge [
    source 39
    target 491
  ]
  edge [
    source 39
    target 492
  ]
  edge [
    source 39
    target 493
  ]
  edge [
    source 39
    target 494
  ]
  edge [
    source 39
    target 495
  ]
  edge [
    source 39
    target 496
  ]
  edge [
    source 39
    target 497
  ]
  edge [
    source 39
    target 498
  ]
  edge [
    source 39
    target 499
  ]
  edge [
    source 39
    target 500
  ]
  edge [
    source 39
    target 501
  ]
  edge [
    source 39
    target 502
  ]
  edge [
    source 39
    target 503
  ]
  edge [
    source 39
    target 504
  ]
  edge [
    source 39
    target 505
  ]
  edge [
    source 39
    target 506
  ]
  edge [
    source 39
    target 507
  ]
  edge [
    source 39
    target 508
  ]
  edge [
    source 39
    target 509
  ]
  edge [
    source 39
    target 510
  ]
  edge [
    source 39
    target 511
  ]
  edge [
    source 39
    target 512
  ]
  edge [
    source 39
    target 513
  ]
  edge [
    source 39
    target 514
  ]
  edge [
    source 39
    target 515
  ]
  edge [
    source 39
    target 516
  ]
  edge [
    source 39
    target 517
  ]
  edge [
    source 39
    target 518
  ]
  edge [
    source 39
    target 519
  ]
  edge [
    source 39
    target 520
  ]
  edge [
    source 39
    target 521
  ]
  edge [
    source 39
    target 522
  ]
  edge [
    source 39
    target 523
  ]
  edge [
    source 39
    target 524
  ]
  edge [
    source 39
    target 525
  ]
  edge [
    source 39
    target 526
  ]
  edge [
    source 39
    target 527
  ]
  edge [
    source 39
    target 528
  ]
  edge [
    source 39
    target 529
  ]
  edge [
    source 39
    target 530
  ]
  edge [
    source 39
    target 531
  ]
  edge [
    source 39
    target 532
  ]
  edge [
    source 39
    target 533
  ]
  edge [
    source 39
    target 534
  ]
  edge [
    source 39
    target 535
  ]
  edge [
    source 39
    target 536
  ]
  edge [
    source 39
    target 537
  ]
  edge [
    source 39
    target 538
  ]
  edge [
    source 39
    target 539
  ]
  edge [
    source 39
    target 540
  ]
  edge [
    source 39
    target 541
  ]
  edge [
    source 39
    target 542
  ]
  edge [
    source 39
    target 543
  ]
  edge [
    source 39
    target 544
  ]
  edge [
    source 39
    target 545
  ]
  edge [
    source 39
    target 546
  ]
  edge [
    source 39
    target 547
  ]
  edge [
    source 39
    target 548
  ]
  edge [
    source 39
    target 549
  ]
  edge [
    source 39
    target 550
  ]
  edge [
    source 39
    target 551
  ]
  edge [
    source 39
    target 552
  ]
  edge [
    source 39
    target 553
  ]
  edge [
    source 39
    target 554
  ]
  edge [
    source 39
    target 555
  ]
  edge [
    source 39
    target 556
  ]
  edge [
    source 39
    target 557
  ]
  edge [
    source 39
    target 558
  ]
  edge [
    source 39
    target 559
  ]
  edge [
    source 39
    target 560
  ]
  edge [
    source 39
    target 561
  ]
  edge [
    source 39
    target 562
  ]
  edge [
    source 39
    target 563
  ]
  edge [
    source 39
    target 564
  ]
  edge [
    source 39
    target 565
  ]
  edge [
    source 39
    target 566
  ]
  edge [
    source 39
    target 567
  ]
  edge [
    source 39
    target 568
  ]
  edge [
    source 39
    target 569
  ]
  edge [
    source 39
    target 570
  ]
  edge [
    source 39
    target 571
  ]
  edge [
    source 39
    target 572
  ]
  edge [
    source 39
    target 573
  ]
  edge [
    source 39
    target 574
  ]
  edge [
    source 39
    target 575
  ]
  edge [
    source 39
    target 576
  ]
  edge [
    source 39
    target 577
  ]
  edge [
    source 39
    target 578
  ]
  edge [
    source 39
    target 579
  ]
  edge [
    source 39
    target 580
  ]
  edge [
    source 39
    target 581
  ]
  edge [
    source 39
    target 582
  ]
  edge [
    source 39
    target 583
  ]
  edge [
    source 39
    target 584
  ]
  edge [
    source 39
    target 585
  ]
  edge [
    source 39
    target 586
  ]
  edge [
    source 39
    target 587
  ]
  edge [
    source 39
    target 588
  ]
  edge [
    source 39
    target 589
  ]
  edge [
    source 39
    target 590
  ]
  edge [
    source 39
    target 591
  ]
  edge [
    source 39
    target 592
  ]
  edge [
    source 39
    target 593
  ]
  edge [
    source 39
    target 594
  ]
  edge [
    source 39
    target 595
  ]
  edge [
    source 39
    target 596
  ]
  edge [
    source 39
    target 597
  ]
  edge [
    source 39
    target 598
  ]
  edge [
    source 39
    target 599
  ]
  edge [
    source 39
    target 600
  ]
  edge [
    source 39
    target 601
  ]
  edge [
    source 39
    target 602
  ]
  edge [
    source 39
    target 603
  ]
  edge [
    source 39
    target 604
  ]
  edge [
    source 39
    target 605
  ]
  edge [
    source 39
    target 606
  ]
  edge [
    source 39
    target 607
  ]
  edge [
    source 39
    target 608
  ]
  edge [
    source 39
    target 609
  ]
  edge [
    source 39
    target 610
  ]
  edge [
    source 39
    target 611
  ]
  edge [
    source 39
    target 612
  ]
  edge [
    source 39
    target 613
  ]
  edge [
    source 39
    target 614
  ]
  edge [
    source 39
    target 615
  ]
  edge [
    source 39
    target 616
  ]
  edge [
    source 39
    target 617
  ]
  edge [
    source 39
    target 618
  ]
  edge [
    source 39
    target 619
  ]
  edge [
    source 39
    target 620
  ]
  edge [
    source 39
    target 621
  ]
  edge [
    source 39
    target 622
  ]
  edge [
    source 39
    target 623
  ]
  edge [
    source 39
    target 624
  ]
  edge [
    source 39
    target 625
  ]
  edge [
    source 39
    target 626
  ]
  edge [
    source 39
    target 627
  ]
  edge [
    source 39
    target 628
  ]
  edge [
    source 39
    target 629
  ]
  edge [
    source 39
    target 630
  ]
  edge [
    source 39
    target 631
  ]
  edge [
    source 39
    target 632
  ]
  edge [
    source 39
    target 633
  ]
  edge [
    source 39
    target 634
  ]
  edge [
    source 39
    target 635
  ]
  edge [
    source 39
    target 636
  ]
  edge [
    source 39
    target 637
  ]
  edge [
    source 39
    target 638
  ]
  edge [
    source 39
    target 639
  ]
  edge [
    source 39
    target 640
  ]
  edge [
    source 39
    target 641
  ]
  edge [
    source 39
    target 642
  ]
  edge [
    source 39
    target 643
  ]
  edge [
    source 39
    target 644
  ]
  edge [
    source 39
    target 645
  ]
  edge [
    source 39
    target 646
  ]
  edge [
    source 39
    target 647
  ]
  edge [
    source 39
    target 648
  ]
  edge [
    source 39
    target 649
  ]
  edge [
    source 39
    target 650
  ]
  edge [
    source 39
    target 651
  ]
  edge [
    source 39
    target 652
  ]
  edge [
    source 39
    target 653
  ]
  edge [
    source 39
    target 654
  ]
  edge [
    source 39
    target 655
  ]
  edge [
    source 39
    target 656
  ]
  edge [
    source 39
    target 657
  ]
  edge [
    source 39
    target 658
  ]
  edge [
    source 39
    target 659
  ]
  edge [
    source 39
    target 660
  ]
  edge [
    source 39
    target 661
  ]
  edge [
    source 39
    target 662
  ]
  edge [
    source 39
    target 663
  ]
  edge [
    source 39
    target 664
  ]
  edge [
    source 39
    target 665
  ]
  edge [
    source 39
    target 666
  ]
  edge [
    source 39
    target 667
  ]
  edge [
    source 39
    target 668
  ]
  edge [
    source 39
    target 669
  ]
  edge [
    source 39
    target 670
  ]
  edge [
    source 39
    target 671
  ]
  edge [
    source 39
    target 672
  ]
  edge [
    source 39
    target 673
  ]
  edge [
    source 39
    target 674
  ]
  edge [
    source 39
    target 675
  ]
  edge [
    source 39
    target 676
  ]
  edge [
    source 39
    target 677
  ]
  edge [
    source 39
    target 678
  ]
  edge [
    source 39
    target 679
  ]
  edge [
    source 39
    target 680
  ]
  edge [
    source 39
    target 681
  ]
  edge [
    source 39
    target 682
  ]
  edge [
    source 39
    target 683
  ]
  edge [
    source 39
    target 684
  ]
  edge [
    source 39
    target 685
  ]
  edge [
    source 39
    target 686
  ]
  edge [
    source 39
    target 687
  ]
  edge [
    source 39
    target 688
  ]
  edge [
    source 39
    target 689
  ]
  edge [
    source 39
    target 690
  ]
  edge [
    source 39
    target 691
  ]
  edge [
    source 39
    target 692
  ]
  edge [
    source 39
    target 693
  ]
  edge [
    source 39
    target 694
  ]
  edge [
    source 39
    target 695
  ]
  edge [
    source 39
    target 696
  ]
  edge [
    source 39
    target 697
  ]
  edge [
    source 39
    target 698
  ]
  edge [
    source 39
    target 699
  ]
  edge [
    source 39
    target 700
  ]
  edge [
    source 39
    target 701
  ]
  edge [
    source 39
    target 702
  ]
  edge [
    source 39
    target 703
  ]
  edge [
    source 39
    target 704
  ]
  edge [
    source 39
    target 705
  ]
  edge [
    source 39
    target 706
  ]
  edge [
    source 39
    target 707
  ]
  edge [
    source 39
    target 708
  ]
  edge [
    source 39
    target 709
  ]
  edge [
    source 39
    target 710
  ]
  edge [
    source 39
    target 711
  ]
  edge [
    source 39
    target 712
  ]
  edge [
    source 39
    target 713
  ]
  edge [
    source 39
    target 714
  ]
  edge [
    source 39
    target 715
  ]
  edge [
    source 39
    target 716
  ]
  edge [
    source 39
    target 717
  ]
  edge [
    source 39
    target 718
  ]
  edge [
    source 39
    target 719
  ]
  edge [
    source 39
    target 720
  ]
  edge [
    source 39
    target 721
  ]
  edge [
    source 39
    target 722
  ]
  edge [
    source 39
    target 723
  ]
  edge [
    source 39
    target 724
  ]
  edge [
    source 39
    target 725
  ]
  edge [
    source 39
    target 726
  ]
  edge [
    source 39
    target 727
  ]
  edge [
    source 39
    target 728
  ]
  edge [
    source 39
    target 729
  ]
  edge [
    source 39
    target 730
  ]
  edge [
    source 39
    target 731
  ]
  edge [
    source 39
    target 732
  ]
  edge [
    source 39
    target 733
  ]
  edge [
    source 39
    target 734
  ]
  edge [
    source 39
    target 735
  ]
  edge [
    source 39
    target 736
  ]
  edge [
    source 39
    target 737
  ]
  edge [
    source 39
    target 738
  ]
  edge [
    source 39
    target 739
  ]
  edge [
    source 39
    target 740
  ]
  edge [
    source 39
    target 741
  ]
  edge [
    source 39
    target 742
  ]
  edge [
    source 39
    target 743
  ]
  edge [
    source 39
    target 744
  ]
  edge [
    source 39
    target 745
  ]
  edge [
    source 39
    target 746
  ]
  edge [
    source 39
    target 747
  ]
  edge [
    source 39
    target 748
  ]
  edge [
    source 39
    target 749
  ]
  edge [
    source 39
    target 750
  ]
  edge [
    source 39
    target 751
  ]
  edge [
    source 39
    target 752
  ]
  edge [
    source 39
    target 753
  ]
  edge [
    source 39
    target 754
  ]
  edge [
    source 39
    target 755
  ]
  edge [
    source 39
    target 756
  ]
  edge [
    source 39
    target 757
  ]
  edge [
    source 39
    target 758
  ]
  edge [
    source 39
    target 759
  ]
  edge [
    source 39
    target 760
  ]
  edge [
    source 39
    target 761
  ]
  edge [
    source 39
    target 762
  ]
  edge [
    source 39
    target 763
  ]
  edge [
    source 39
    target 764
  ]
  edge [
    source 39
    target 765
  ]
  edge [
    source 39
    target 766
  ]
  edge [
    source 39
    target 767
  ]
  edge [
    source 39
    target 768
  ]
  edge [
    source 39
    target 769
  ]
  edge [
    source 39
    target 770
  ]
  edge [
    source 39
    target 771
  ]
  edge [
    source 39
    target 772
  ]
  edge [
    source 39
    target 773
  ]
  edge [
    source 39
    target 774
  ]
  edge [
    source 39
    target 775
  ]
  edge [
    source 39
    target 776
  ]
  edge [
    source 39
    target 777
  ]
  edge [
    source 39
    target 778
  ]
  edge [
    source 39
    target 779
  ]
  edge [
    source 39
    target 780
  ]
  edge [
    source 39
    target 781
  ]
  edge [
    source 39
    target 782
  ]
  edge [
    source 39
    target 783
  ]
  edge [
    source 39
    target 784
  ]
  edge [
    source 39
    target 785
  ]
  edge [
    source 39
    target 786
  ]
  edge [
    source 39
    target 787
  ]
  edge [
    source 39
    target 788
  ]
  edge [
    source 39
    target 789
  ]
  edge [
    source 39
    target 790
  ]
  edge [
    source 39
    target 791
  ]
  edge [
    source 39
    target 792
  ]
  edge [
    source 39
    target 793
  ]
  edge [
    source 39
    target 794
  ]
  edge [
    source 39
    target 795
  ]
  edge [
    source 39
    target 796
  ]
  edge [
    source 39
    target 797
  ]
  edge [
    source 39
    target 798
  ]
  edge [
    source 39
    target 799
  ]
  edge [
    source 39
    target 800
  ]
  edge [
    source 39
    target 801
  ]
  edge [
    source 39
    target 802
  ]
  edge [
    source 39
    target 803
  ]
  edge [
    source 39
    target 804
  ]
  edge [
    source 39
    target 805
  ]
  edge [
    source 39
    target 806
  ]
  edge [
    source 39
    target 807
  ]
  edge [
    source 39
    target 808
  ]
  edge [
    source 39
    target 809
  ]
  edge [
    source 39
    target 810
  ]
  edge [
    source 39
    target 811
  ]
  edge [
    source 39
    target 812
  ]
  edge [
    source 39
    target 813
  ]
  edge [
    source 39
    target 814
  ]
  edge [
    source 39
    target 815
  ]
  edge [
    source 39
    target 816
  ]
  edge [
    source 39
    target 817
  ]
  edge [
    source 39
    target 818
  ]
  edge [
    source 39
    target 819
  ]
  edge [
    source 39
    target 820
  ]
  edge [
    source 39
    target 114
  ]
  edge [
    source 39
    target 821
  ]
  edge [
    source 39
    target 822
  ]
  edge [
    source 39
    target 823
  ]
  edge [
    source 39
    target 824
  ]
  edge [
    source 39
    target 825
  ]
  edge [
    source 39
    target 826
  ]
  edge [
    source 39
    target 827
  ]
  edge [
    source 39
    target 828
  ]
  edge [
    source 39
    target 829
  ]
  edge [
    source 39
    target 830
  ]
  edge [
    source 39
    target 831
  ]
  edge [
    source 39
    target 832
  ]
  edge [
    source 39
    target 833
  ]
  edge [
    source 39
    target 834
  ]
  edge [
    source 39
    target 835
  ]
  edge [
    source 39
    target 836
  ]
  edge [
    source 39
    target 837
  ]
  edge [
    source 39
    target 838
  ]
  edge [
    source 39
    target 839
  ]
  edge [
    source 39
    target 840
  ]
  edge [
    source 39
    target 841
  ]
  edge [
    source 39
    target 842
  ]
  edge [
    source 39
    target 843
  ]
  edge [
    source 39
    target 844
  ]
  edge [
    source 39
    target 845
  ]
  edge [
    source 39
    target 846
  ]
  edge [
    source 39
    target 847
  ]
  edge [
    source 39
    target 848
  ]
  edge [
    source 39
    target 849
  ]
  edge [
    source 39
    target 850
  ]
  edge [
    source 39
    target 851
  ]
  edge [
    source 39
    target 852
  ]
  edge [
    source 39
    target 853
  ]
  edge [
    source 39
    target 854
  ]
  edge [
    source 39
    target 855
  ]
  edge [
    source 39
    target 856
  ]
  edge [
    source 39
    target 857
  ]
  edge [
    source 39
    target 858
  ]
  edge [
    source 39
    target 859
  ]
  edge [
    source 39
    target 860
  ]
  edge [
    source 39
    target 861
  ]
  edge [
    source 39
    target 862
  ]
  edge [
    source 39
    target 863
  ]
  edge [
    source 39
    target 864
  ]
  edge [
    source 39
    target 865
  ]
  edge [
    source 39
    target 866
  ]
  edge [
    source 39
    target 867
  ]
  edge [
    source 39
    target 868
  ]
  edge [
    source 39
    target 869
  ]
  edge [
    source 39
    target 870
  ]
  edge [
    source 39
    target 871
  ]
  edge [
    source 39
    target 872
  ]
  edge [
    source 39
    target 873
  ]
  edge [
    source 39
    target 874
  ]
  edge [
    source 39
    target 875
  ]
  edge [
    source 39
    target 876
  ]
  edge [
    source 39
    target 877
  ]
  edge [
    source 39
    target 878
  ]
  edge [
    source 39
    target 879
  ]
  edge [
    source 39
    target 880
  ]
  edge [
    source 39
    target 881
  ]
  edge [
    source 39
    target 882
  ]
  edge [
    source 39
    target 883
  ]
  edge [
    source 39
    target 884
  ]
  edge [
    source 39
    target 885
  ]
  edge [
    source 39
    target 886
  ]
  edge [
    source 39
    target 887
  ]
  edge [
    source 39
    target 888
  ]
  edge [
    source 39
    target 889
  ]
  edge [
    source 39
    target 890
  ]
  edge [
    source 39
    target 891
  ]
  edge [
    source 39
    target 892
  ]
  edge [
    source 39
    target 893
  ]
  edge [
    source 39
    target 894
  ]
  edge [
    source 39
    target 895
  ]
  edge [
    source 39
    target 896
  ]
  edge [
    source 39
    target 897
  ]
  edge [
    source 39
    target 898
  ]
  edge [
    source 39
    target 899
  ]
  edge [
    source 39
    target 900
  ]
  edge [
    source 39
    target 901
  ]
  edge [
    source 39
    target 902
  ]
  edge [
    source 39
    target 903
  ]
  edge [
    source 39
    target 904
  ]
  edge [
    source 39
    target 905
  ]
  edge [
    source 39
    target 906
  ]
  edge [
    source 39
    target 907
  ]
  edge [
    source 39
    target 908
  ]
  edge [
    source 39
    target 909
  ]
  edge [
    source 39
    target 910
  ]
  edge [
    source 39
    target 911
  ]
  edge [
    source 39
    target 912
  ]
  edge [
    source 39
    target 913
  ]
  edge [
    source 39
    target 914
  ]
  edge [
    source 39
    target 915
  ]
  edge [
    source 39
    target 916
  ]
  edge [
    source 39
    target 917
  ]
  edge [
    source 39
    target 918
  ]
  edge [
    source 39
    target 919
  ]
  edge [
    source 39
    target 920
  ]
  edge [
    source 39
    target 921
  ]
  edge [
    source 39
    target 922
  ]
  edge [
    source 39
    target 923
  ]
  edge [
    source 39
    target 924
  ]
  edge [
    source 39
    target 925
  ]
  edge [
    source 39
    target 926
  ]
  edge [
    source 39
    target 927
  ]
  edge [
    source 39
    target 928
  ]
  edge [
    source 39
    target 929
  ]
  edge [
    source 39
    target 930
  ]
  edge [
    source 39
    target 931
  ]
  edge [
    source 39
    target 932
  ]
  edge [
    source 39
    target 933
  ]
  edge [
    source 39
    target 934
  ]
  edge [
    source 39
    target 935
  ]
  edge [
    source 39
    target 936
  ]
  edge [
    source 39
    target 937
  ]
  edge [
    source 39
    target 938
  ]
  edge [
    source 39
    target 939
  ]
  edge [
    source 39
    target 940
  ]
  edge [
    source 39
    target 941
  ]
  edge [
    source 39
    target 942
  ]
  edge [
    source 39
    target 943
  ]
  edge [
    source 39
    target 944
  ]
  edge [
    source 39
    target 945
  ]
  edge [
    source 39
    target 946
  ]
  edge [
    source 39
    target 947
  ]
  edge [
    source 39
    target 948
  ]
  edge [
    source 39
    target 949
  ]
  edge [
    source 39
    target 950
  ]
  edge [
    source 39
    target 951
  ]
  edge [
    source 39
    target 952
  ]
  edge [
    source 39
    target 953
  ]
  edge [
    source 39
    target 954
  ]
  edge [
    source 39
    target 955
  ]
  edge [
    source 39
    target 956
  ]
  edge [
    source 39
    target 957
  ]
  edge [
    source 39
    target 958
  ]
  edge [
    source 39
    target 959
  ]
  edge [
    source 39
    target 960
  ]
  edge [
    source 39
    target 961
  ]
  edge [
    source 39
    target 962
  ]
  edge [
    source 39
    target 963
  ]
  edge [
    source 39
    target 964
  ]
  edge [
    source 39
    target 965
  ]
  edge [
    source 39
    target 966
  ]
  edge [
    source 39
    target 967
  ]
  edge [
    source 39
    target 968
  ]
  edge [
    source 39
    target 969
  ]
  edge [
    source 39
    target 970
  ]
  edge [
    source 39
    target 971
  ]
  edge [
    source 39
    target 972
  ]
  edge [
    source 39
    target 973
  ]
  edge [
    source 39
    target 974
  ]
  edge [
    source 39
    target 975
  ]
  edge [
    source 39
    target 976
  ]
  edge [
    source 39
    target 977
  ]
  edge [
    source 39
    target 978
  ]
  edge [
    source 39
    target 979
  ]
  edge [
    source 39
    target 980
  ]
  edge [
    source 39
    target 981
  ]
  edge [
    source 39
    target 982
  ]
  edge [
    source 39
    target 983
  ]
  edge [
    source 39
    target 984
  ]
  edge [
    source 39
    target 985
  ]
  edge [
    source 39
    target 986
  ]
  edge [
    source 39
    target 987
  ]
  edge [
    source 39
    target 988
  ]
  edge [
    source 39
    target 989
  ]
  edge [
    source 39
    target 990
  ]
  edge [
    source 39
    target 991
  ]
  edge [
    source 39
    target 992
  ]
  edge [
    source 39
    target 993
  ]
  edge [
    source 39
    target 994
  ]
  edge [
    source 39
    target 995
  ]
  edge [
    source 39
    target 996
  ]
  edge [
    source 39
    target 997
  ]
  edge [
    source 39
    target 998
  ]
  edge [
    source 39
    target 999
  ]
  edge [
    source 39
    target 1000
  ]
  edge [
    source 39
    target 1001
  ]
  edge [
    source 39
    target 1002
  ]
  edge [
    source 39
    target 1003
  ]
  edge [
    source 39
    target 1004
  ]
  edge [
    source 39
    target 1005
  ]
  edge [
    source 39
    target 1006
  ]
  edge [
    source 39
    target 1007
  ]
  edge [
    source 39
    target 1008
  ]
  edge [
    source 39
    target 1009
  ]
  edge [
    source 39
    target 1010
  ]
  edge [
    source 39
    target 1011
  ]
  edge [
    source 39
    target 1012
  ]
  edge [
    source 39
    target 1013
  ]
  edge [
    source 39
    target 1014
  ]
  edge [
    source 39
    target 1015
  ]
  edge [
    source 39
    target 1016
  ]
  edge [
    source 39
    target 1017
  ]
  edge [
    source 39
    target 1018
  ]
  edge [
    source 39
    target 1019
  ]
  edge [
    source 39
    target 1020
  ]
  edge [
    source 39
    target 1021
  ]
  edge [
    source 39
    target 1022
  ]
  edge [
    source 39
    target 1023
  ]
  edge [
    source 39
    target 1024
  ]
  edge [
    source 39
    target 1025
  ]
  edge [
    source 39
    target 1026
  ]
  edge [
    source 39
    target 1027
  ]
  edge [
    source 39
    target 1028
  ]
  edge [
    source 39
    target 1029
  ]
  edge [
    source 39
    target 1030
  ]
  edge [
    source 39
    target 1031
  ]
  edge [
    source 39
    target 1032
  ]
  edge [
    source 39
    target 1033
  ]
  edge [
    source 39
    target 1034
  ]
  edge [
    source 39
    target 1035
  ]
  edge [
    source 39
    target 1036
  ]
  edge [
    source 39
    target 1037
  ]
  edge [
    source 39
    target 1038
  ]
  edge [
    source 39
    target 1039
  ]
  edge [
    source 39
    target 1040
  ]
  edge [
    source 39
    target 1041
  ]
  edge [
    source 39
    target 1042
  ]
  edge [
    source 39
    target 1043
  ]
  edge [
    source 39
    target 1044
  ]
  edge [
    source 39
    target 1045
  ]
  edge [
    source 39
    target 1046
  ]
  edge [
    source 39
    target 1047
  ]
  edge [
    source 39
    target 1048
  ]
  edge [
    source 39
    target 1049
  ]
  edge [
    source 39
    target 1050
  ]
  edge [
    source 39
    target 1051
  ]
  edge [
    source 39
    target 1052
  ]
  edge [
    source 39
    target 1053
  ]
  edge [
    source 39
    target 1054
  ]
  edge [
    source 39
    target 1055
  ]
  edge [
    source 39
    target 1056
  ]
  edge [
    source 39
    target 1057
  ]
  edge [
    source 39
    target 1058
  ]
  edge [
    source 39
    target 1059
  ]
  edge [
    source 39
    target 1060
  ]
  edge [
    source 39
    target 1061
  ]
  edge [
    source 40
    target 49
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1062
  ]
  edge [
    source 41
    target 1063
  ]
  edge [
    source 41
    target 1064
  ]
  edge [
    source 41
    target 1065
  ]
  edge [
    source 41
    target 1217
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1066
  ]
  edge [
    source 42
    target 177
  ]
  edge [
    source 42
    target 1067
  ]
  edge [
    source 42
    target 1068
  ]
  edge [
    source 42
    target 50
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1069
  ]
  edge [
    source 43
    target 1070
  ]
  edge [
    source 43
    target 1071
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 162
  ]
  edge [
    source 44
    target 1072
  ]
  edge [
    source 44
    target 1073
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 52
  ]
  edge [
    source 45
    target 53
  ]
  edge [
    source 45
    target 1074
  ]
  edge [
    source 45
    target 182
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1075
  ]
  edge [
    source 46
    target 1076
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 1077
  ]
  edge [
    source 47
    target 1078
  ]
  edge [
    source 47
    target 1079
  ]
  edge [
    source 47
    target 1080
  ]
  edge [
    source 47
    target 1081
  ]
  edge [
    source 47
    target 1082
  ]
  edge [
    source 47
    target 1083
  ]
  edge [
    source 47
    target 1084
  ]
  edge [
    source 47
    target 1085
  ]
  edge [
    source 47
    target 1086
  ]
  edge [
    source 47
    target 1087
  ]
  edge [
    source 47
    target 1088
  ]
  edge [
    source 47
    target 1089
  ]
  edge [
    source 47
    target 1090
  ]
  edge [
    source 47
    target 1091
  ]
  edge [
    source 47
    target 1092
  ]
  edge [
    source 47
    target 1093
  ]
  edge [
    source 47
    target 1094
  ]
  edge [
    source 47
    target 1095
  ]
  edge [
    source 47
    target 1096
  ]
  edge [
    source 47
    target 1097
  ]
  edge [
    source 47
    target 1098
  ]
  edge [
    source 47
    target 1099
  ]
  edge [
    source 47
    target 1100
  ]
  edge [
    source 47
    target 1101
  ]
  edge [
    source 47
    target 1102
  ]
  edge [
    source 47
    target 1103
  ]
  edge [
    source 47
    target 1104
  ]
  edge [
    source 47
    target 1105
  ]
  edge [
    source 47
    target 1106
  ]
  edge [
    source 47
    target 1107
  ]
  edge [
    source 47
    target 1108
  ]
  edge [
    source 47
    target 1109
  ]
  edge [
    source 47
    target 1110
  ]
  edge [
    source 47
    target 1111
  ]
  edge [
    source 47
    target 1112
  ]
  edge [
    source 47
    target 1113
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 1114
  ]
  edge [
    source 48
    target 1115
  ]
  edge [
    source 48
    target 1116
  ]
  edge [
    source 48
    target 1117
  ]
  edge [
    source 48
    target 228
  ]
  edge [
    source 48
    target 1118
  ]
  edge [
    source 48
    target 1119
  ]
  edge [
    source 48
    target 1120
  ]
  edge [
    source 48
    target 1121
  ]
  edge [
    source 48
    target 1122
  ]
  edge [
    source 48
    target 1123
  ]
  edge [
    source 48
    target 1124
  ]
  edge [
    source 48
    target 1125
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 1126
  ]
  edge [
    source 49
    target 1127
  ]
  edge [
    source 49
    target 1128
  ]
  edge [
    source 49
    target 1129
  ]
  edge [
    source 49
    target 1130
  ]
  edge [
    source 49
    target 1131
  ]
  edge [
    source 49
    target 1132
  ]
  edge [
    source 49
    target 1133
  ]
  edge [
    source 49
    target 1134
  ]
  edge [
    source 49
    target 1135
  ]
  edge [
    source 49
    target 1136
  ]
  edge [
    source 49
    target 1137
  ]
  edge [
    source 49
    target 1138
  ]
  edge [
    source 49
    target 1139
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 65
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 52
    target 1140
  ]
  edge [
    source 52
    target 122
  ]
  edge [
    source 52
    target 1141
  ]
  edge [
    source 52
    target 1142
  ]
  edge [
    source 52
    target 1143
  ]
  edge [
    source 52
    target 1144
  ]
  edge [
    source 52
    target 1145
  ]
  edge [
    source 52
    target 1146
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 1147
  ]
  edge [
    source 53
    target 1148
  ]
  edge [
    source 53
    target 1149
  ]
  edge [
    source 53
    target 1150
  ]
  edge [
    source 53
    target 1151
  ]
  edge [
    source 53
    target 389
  ]
  edge [
    source 53
    target 1152
  ]
  edge [
    source 54
    target 1153
  ]
  edge [
    source 54
    target 122
  ]
  edge [
    source 54
    target 1154
  ]
  edge [
    source 54
    target 1155
  ]
  edge [
    source 54
    target 1156
  ]
  edge [
    source 54
    target 1157
  ]
  edge [
    source 54
    target 1158
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 101
  ]
  edge [
    source 55
    target 1159
  ]
  edge [
    source 55
    target 1160
  ]
  edge [
    source 55
    target 1161
  ]
  edge [
    source 55
    target 1162
  ]
  edge [
    source 55
    target 251
  ]
  edge [
    source 55
    target 1163
  ]
  edge [
    source 55
    target 1164
  ]
  edge [
    source 55
    target 1165
  ]
  edge [
    source 55
    target 1166
  ]
  edge [
    source 55
    target 1167
  ]
  edge [
    source 55
    target 1168
  ]
  edge [
    source 55
    target 1169
  ]
  edge [
    source 55
    target 1170
  ]
  edge [
    source 55
    target 277
  ]
  edge [
    source 55
    target 1171
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 110
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 1172
  ]
  edge [
    source 57
    target 1173
  ]
  edge [
    source 57
    target 1174
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 1175
  ]
  edge [
    source 58
    target 1176
  ]
  edge [
    source 58
    target 1177
  ]
  edge [
    source 58
    target 1178
  ]
  edge [
    source 58
    target 1179
  ]
  edge [
    source 58
    target 1180
  ]
  edge [
    source 58
    target 1181
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 1182
  ]
  edge [
    source 59
    target 1183
  ]
  edge [
    source 59
    target 1184
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 1185
  ]
  edge [
    source 61
    target 101
  ]
  edge [
    source 61
    target 65
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 64
    target 1186
  ]
  edge [
    source 64
    target 1187
  ]
  edge [
    source 64
    target 1188
  ]
  edge [
    source 64
    target 1189
  ]
  edge [
    source 64
    target 1190
  ]
  edge [
    source 64
    target 1191
  ]
  edge [
    source 64
    target 1192
  ]
  edge [
    source 64
    target 1193
  ]
  edge [
    source 64
    target 1194
  ]
  edge [
    source 64
    target 1195
  ]
  edge [
    source 64
    target 1196
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 122
  ]
  edge [
    source 65
    target 1197
  ]
  edge [
    source 65
    target 1198
  ]
  edge [
    source 65
    target 1199
  ]
  edge [
    source 65
    target 450
  ]
  edge [
    source 66
    target 1200
  ]
  edge [
    source 66
    target 1201
  ]
  edge [
    source 66
    target 1202
  ]
  edge [
    source 66
    target 1203
  ]
  edge [
    source 66
    target 137
  ]
  edge [
    source 66
    target 1204
  ]
  edge [
    source 66
    target 1205
  ]
  edge [
    source 66
    target 1206
  ]
  edge [
    source 66
    target 1207
  ]
  edge [
    source 66
    target 1208
  ]
  edge [
    source 66
    target 1209
  ]
  edge [
    source 66
    target 1210
  ]
  edge [
    source 66
    target 1211
  ]
  edge [
    source 66
    target 1212
  ]
  edge [
    source 66
    target 1213
  ]
  edge [
    source 66
    target 1214
  ]
  edge [
    source 1215
    target 1216
  ]
  edge [
    source 1218
    target 1219
  ]
  edge [
    source 1218
    target 1224
  ]
  edge [
    source 1220
    target 1221
  ]
  edge [
    source 1222
    target 1223
  ]
]
