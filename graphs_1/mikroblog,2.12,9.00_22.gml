graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.2
  density 0.3
  graphCliqueNumber 2
  node [
    id 0
    label "postanowi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "przys&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "Tinder"
  ]
  node [
    id 4
    label "Golda"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
]
