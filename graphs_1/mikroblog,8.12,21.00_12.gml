graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9655172413793103
  density 0.034482758620689655
  graphCliqueNumber 2
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "atrapa"
    origin "text"
  ]
  node [
    id 2
    label "radiow&#243;z"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "sta&#322;y"
    origin "text"
  ]
  node [
    id 5
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 6
    label "przy"
    origin "text"
  ]
  node [
    id 7
    label "droga"
    origin "text"
  ]
  node [
    id 8
    label "krajowy"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;ga&#263;"
  ]
  node [
    id 10
    label "zna&#263;"
  ]
  node [
    id 11
    label "troska&#263;_si&#281;"
  ]
  node [
    id 12
    label "zachowywa&#263;"
  ]
  node [
    id 13
    label "chowa&#263;"
  ]
  node [
    id 14
    label "think"
  ]
  node [
    id 15
    label "pilnowa&#263;"
  ]
  node [
    id 16
    label "robi&#263;"
  ]
  node [
    id 17
    label "recall"
  ]
  node [
    id 18
    label "echo"
  ]
  node [
    id 19
    label "take_care"
  ]
  node [
    id 20
    label "poz&#243;r"
  ]
  node [
    id 21
    label "mock-up"
  ]
  node [
    id 22
    label "os&#322;ona"
  ]
  node [
    id 23
    label "imitacja"
  ]
  node [
    id 24
    label "radiator"
  ]
  node [
    id 25
    label "samoch&#243;d"
  ]
  node [
    id 26
    label "misiow&#243;z"
  ]
  node [
    id 27
    label "zwyk&#322;y"
  ]
  node [
    id 28
    label "jednakowy"
  ]
  node [
    id 29
    label "stale"
  ]
  node [
    id 30
    label "regularny"
  ]
  node [
    id 31
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 32
    label "journey"
  ]
  node [
    id 33
    label "podbieg"
  ]
  node [
    id 34
    label "bezsilnikowy"
  ]
  node [
    id 35
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 36
    label "wylot"
  ]
  node [
    id 37
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 38
    label "drogowskaz"
  ]
  node [
    id 39
    label "nawierzchnia"
  ]
  node [
    id 40
    label "turystyka"
  ]
  node [
    id 41
    label "budowla"
  ]
  node [
    id 42
    label "spos&#243;b"
  ]
  node [
    id 43
    label "passage"
  ]
  node [
    id 44
    label "marszrutyzacja"
  ]
  node [
    id 45
    label "zbior&#243;wka"
  ]
  node [
    id 46
    label "rajza"
  ]
  node [
    id 47
    label "ekskursja"
  ]
  node [
    id 48
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 49
    label "ruch"
  ]
  node [
    id 50
    label "trasa"
  ]
  node [
    id 51
    label "wyb&#243;j"
  ]
  node [
    id 52
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 53
    label "ekwipunek"
  ]
  node [
    id 54
    label "korona_drogi"
  ]
  node [
    id 55
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 56
    label "pobocze"
  ]
  node [
    id 57
    label "rodzimy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 57
  ]
]
