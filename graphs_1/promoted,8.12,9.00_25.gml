graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.22
  density 0.022424242424242423
  graphCliqueNumber 3
  node [
    id 0
    label "wszystek"
    origin "text"
  ]
  node [
    id 1
    label "osoba"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "rozpoznawa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 5
    label "proszone"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "kontakt"
    origin "text"
  ]
  node [
    id 8
    label "policja"
    origin "text"
  ]
  node [
    id 9
    label "ca&#322;y"
  ]
  node [
    id 10
    label "Zgredek"
  ]
  node [
    id 11
    label "kategoria_gramatyczna"
  ]
  node [
    id 12
    label "Casanova"
  ]
  node [
    id 13
    label "Don_Juan"
  ]
  node [
    id 14
    label "Gargantua"
  ]
  node [
    id 15
    label "Faust"
  ]
  node [
    id 16
    label "profanum"
  ]
  node [
    id 17
    label "Chocho&#322;"
  ]
  node [
    id 18
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 19
    label "koniugacja"
  ]
  node [
    id 20
    label "Winnetou"
  ]
  node [
    id 21
    label "Dwukwiat"
  ]
  node [
    id 22
    label "homo_sapiens"
  ]
  node [
    id 23
    label "Edyp"
  ]
  node [
    id 24
    label "Herkules_Poirot"
  ]
  node [
    id 25
    label "ludzko&#347;&#263;"
  ]
  node [
    id 26
    label "mikrokosmos"
  ]
  node [
    id 27
    label "person"
  ]
  node [
    id 28
    label "Szwejk"
  ]
  node [
    id 29
    label "portrecista"
  ]
  node [
    id 30
    label "Sherlock_Holmes"
  ]
  node [
    id 31
    label "Hamlet"
  ]
  node [
    id 32
    label "duch"
  ]
  node [
    id 33
    label "oddzia&#322;ywanie"
  ]
  node [
    id 34
    label "g&#322;owa"
  ]
  node [
    id 35
    label "Quasimodo"
  ]
  node [
    id 36
    label "Dulcynea"
  ]
  node [
    id 37
    label "Wallenrod"
  ]
  node [
    id 38
    label "Don_Kiszot"
  ]
  node [
    id 39
    label "Plastu&#347;"
  ]
  node [
    id 40
    label "Harry_Potter"
  ]
  node [
    id 41
    label "figura"
  ]
  node [
    id 42
    label "parali&#380;owa&#263;"
  ]
  node [
    id 43
    label "istota"
  ]
  node [
    id 44
    label "Werter"
  ]
  node [
    id 45
    label "antropochoria"
  ]
  node [
    id 46
    label "posta&#263;"
  ]
  node [
    id 47
    label "orientowa&#263;_si&#281;"
  ]
  node [
    id 48
    label "detect"
  ]
  node [
    id 49
    label "accredit"
  ]
  node [
    id 50
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 51
    label "rozpatrywa&#263;"
  ]
  node [
    id 52
    label "asymilowa&#263;"
  ]
  node [
    id 53
    label "wapniak"
  ]
  node [
    id 54
    label "dwun&#243;g"
  ]
  node [
    id 55
    label "polifag"
  ]
  node [
    id 56
    label "wz&#243;r"
  ]
  node [
    id 57
    label "hominid"
  ]
  node [
    id 58
    label "nasada"
  ]
  node [
    id 59
    label "podw&#322;adny"
  ]
  node [
    id 60
    label "os&#322;abianie"
  ]
  node [
    id 61
    label "asymilowanie"
  ]
  node [
    id 62
    label "os&#322;abia&#263;"
  ]
  node [
    id 63
    label "Adam"
  ]
  node [
    id 64
    label "senior"
  ]
  node [
    id 65
    label "si&#281;ga&#263;"
  ]
  node [
    id 66
    label "trwa&#263;"
  ]
  node [
    id 67
    label "obecno&#347;&#263;"
  ]
  node [
    id 68
    label "stan"
  ]
  node [
    id 69
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 70
    label "stand"
  ]
  node [
    id 71
    label "mie&#263;_miejsce"
  ]
  node [
    id 72
    label "uczestniczy&#263;"
  ]
  node [
    id 73
    label "chodzi&#263;"
  ]
  node [
    id 74
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 75
    label "equal"
  ]
  node [
    id 76
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 77
    label "formacja_geologiczna"
  ]
  node [
    id 78
    label "zwi&#261;zek"
  ]
  node [
    id 79
    label "soczewka"
  ]
  node [
    id 80
    label "contact"
  ]
  node [
    id 81
    label "linkage"
  ]
  node [
    id 82
    label "katalizator"
  ]
  node [
    id 83
    label "z&#322;&#261;czenie"
  ]
  node [
    id 84
    label "&#322;&#261;cznik_mechanizmowy"
  ]
  node [
    id 85
    label "regulator"
  ]
  node [
    id 86
    label "styk"
  ]
  node [
    id 87
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 88
    label "wydarzenie"
  ]
  node [
    id 89
    label "communication"
  ]
  node [
    id 90
    label "instalacja_elektryczna"
  ]
  node [
    id 91
    label "&#322;&#261;cznik"
  ]
  node [
    id 92
    label "socket"
  ]
  node [
    id 93
    label "association"
  ]
  node [
    id 94
    label "komisariat"
  ]
  node [
    id 95
    label "psiarnia"
  ]
  node [
    id 96
    label "posterunek"
  ]
  node [
    id 97
    label "grupa"
  ]
  node [
    id 98
    label "organ"
  ]
  node [
    id 99
    label "s&#322;u&#380;ba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
]
