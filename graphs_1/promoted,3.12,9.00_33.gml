graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.0135135135135136
  density 0.013697370840227983
  graphCliqueNumber 2
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "badanie"
    origin "text"
  ]
  node [
    id 2
    label "wskazywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "starsza"
    origin "text"
  ]
  node [
    id 4
    label "osoba"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "k&#322;ama&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 8
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 9
    label "minuta"
    origin "text"
  ]
  node [
    id 10
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 11
    label "przyj&#261;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "k&#322;amstwo"
    origin "text"
  ]
  node [
    id 13
    label "prawda"
    origin "text"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "nowotny"
  ]
  node [
    id 16
    label "drugi"
  ]
  node [
    id 17
    label "kolejny"
  ]
  node [
    id 18
    label "bie&#380;&#261;cy"
  ]
  node [
    id 19
    label "nowo"
  ]
  node [
    id 20
    label "narybek"
  ]
  node [
    id 21
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 22
    label "obcy"
  ]
  node [
    id 23
    label "usi&#322;owanie"
  ]
  node [
    id 24
    label "examination"
  ]
  node [
    id 25
    label "investigation"
  ]
  node [
    id 26
    label "ustalenie"
  ]
  node [
    id 27
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 28
    label "ustalanie"
  ]
  node [
    id 29
    label "bia&#322;a_niedziela"
  ]
  node [
    id 30
    label "analysis"
  ]
  node [
    id 31
    label "rozpatrywanie"
  ]
  node [
    id 32
    label "wziernikowanie"
  ]
  node [
    id 33
    label "obserwowanie"
  ]
  node [
    id 34
    label "omawianie"
  ]
  node [
    id 35
    label "sprawdzanie"
  ]
  node [
    id 36
    label "udowadnianie"
  ]
  node [
    id 37
    label "diagnostyka"
  ]
  node [
    id 38
    label "czynno&#347;&#263;"
  ]
  node [
    id 39
    label "macanie"
  ]
  node [
    id 40
    label "rektalny"
  ]
  node [
    id 41
    label "penetrowanie"
  ]
  node [
    id 42
    label "krytykowanie"
  ]
  node [
    id 43
    label "kontrola"
  ]
  node [
    id 44
    label "dociekanie"
  ]
  node [
    id 45
    label "zrecenzowanie"
  ]
  node [
    id 46
    label "praca"
  ]
  node [
    id 47
    label "rezultat"
  ]
  node [
    id 48
    label "by&#263;"
  ]
  node [
    id 49
    label "pokazywa&#263;"
  ]
  node [
    id 50
    label "warto&#347;&#263;"
  ]
  node [
    id 51
    label "set"
  ]
  node [
    id 52
    label "represent"
  ]
  node [
    id 53
    label "indicate"
  ]
  node [
    id 54
    label "wyraz"
  ]
  node [
    id 55
    label "wybiera&#263;"
  ]
  node [
    id 56
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 57
    label "podawa&#263;"
  ]
  node [
    id 58
    label "signify"
  ]
  node [
    id 59
    label "podkre&#347;la&#263;"
  ]
  node [
    id 60
    label "matka"
  ]
  node [
    id 61
    label "starzy"
  ]
  node [
    id 62
    label "Zgredek"
  ]
  node [
    id 63
    label "kategoria_gramatyczna"
  ]
  node [
    id 64
    label "Casanova"
  ]
  node [
    id 65
    label "Don_Juan"
  ]
  node [
    id 66
    label "Gargantua"
  ]
  node [
    id 67
    label "Faust"
  ]
  node [
    id 68
    label "profanum"
  ]
  node [
    id 69
    label "Chocho&#322;"
  ]
  node [
    id 70
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 71
    label "koniugacja"
  ]
  node [
    id 72
    label "Winnetou"
  ]
  node [
    id 73
    label "Dwukwiat"
  ]
  node [
    id 74
    label "homo_sapiens"
  ]
  node [
    id 75
    label "Edyp"
  ]
  node [
    id 76
    label "Herkules_Poirot"
  ]
  node [
    id 77
    label "ludzko&#347;&#263;"
  ]
  node [
    id 78
    label "mikrokosmos"
  ]
  node [
    id 79
    label "person"
  ]
  node [
    id 80
    label "Sherlock_Holmes"
  ]
  node [
    id 81
    label "portrecista"
  ]
  node [
    id 82
    label "Szwejk"
  ]
  node [
    id 83
    label "Hamlet"
  ]
  node [
    id 84
    label "duch"
  ]
  node [
    id 85
    label "g&#322;owa"
  ]
  node [
    id 86
    label "oddzia&#322;ywanie"
  ]
  node [
    id 87
    label "Quasimodo"
  ]
  node [
    id 88
    label "Dulcynea"
  ]
  node [
    id 89
    label "Don_Kiszot"
  ]
  node [
    id 90
    label "Wallenrod"
  ]
  node [
    id 91
    label "Plastu&#347;"
  ]
  node [
    id 92
    label "Harry_Potter"
  ]
  node [
    id 93
    label "figura"
  ]
  node [
    id 94
    label "parali&#380;owa&#263;"
  ]
  node [
    id 95
    label "istota"
  ]
  node [
    id 96
    label "Werter"
  ]
  node [
    id 97
    label "antropochoria"
  ]
  node [
    id 98
    label "posta&#263;"
  ]
  node [
    id 99
    label "m&#243;wi&#263;"
  ]
  node [
    id 100
    label "lie"
  ]
  node [
    id 101
    label "mitomania"
  ]
  node [
    id 102
    label "pitoli&#263;"
  ]
  node [
    id 103
    label "oszukiwa&#263;"
  ]
  node [
    id 104
    label "zapis"
  ]
  node [
    id 105
    label "godzina"
  ]
  node [
    id 106
    label "sekunda"
  ]
  node [
    id 107
    label "kwadrans"
  ]
  node [
    id 108
    label "stopie&#324;"
  ]
  node [
    id 109
    label "design"
  ]
  node [
    id 110
    label "time"
  ]
  node [
    id 111
    label "jednostka"
  ]
  node [
    id 112
    label "uprawi&#263;"
  ]
  node [
    id 113
    label "gotowy"
  ]
  node [
    id 114
    label "might"
  ]
  node [
    id 115
    label "dostarczy&#263;"
  ]
  node [
    id 116
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 117
    label "strike"
  ]
  node [
    id 118
    label "przybra&#263;"
  ]
  node [
    id 119
    label "swallow"
  ]
  node [
    id 120
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 121
    label "odebra&#263;"
  ]
  node [
    id 122
    label "umie&#347;ci&#263;"
  ]
  node [
    id 123
    label "obra&#263;"
  ]
  node [
    id 124
    label "fall"
  ]
  node [
    id 125
    label "wzi&#261;&#263;"
  ]
  node [
    id 126
    label "undertake"
  ]
  node [
    id 127
    label "absorb"
  ]
  node [
    id 128
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 129
    label "receive"
  ]
  node [
    id 130
    label "draw"
  ]
  node [
    id 131
    label "zrobi&#263;"
  ]
  node [
    id 132
    label "przyj&#281;cie"
  ]
  node [
    id 133
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 134
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 135
    label "uzna&#263;"
  ]
  node [
    id 136
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 137
    label "lipa"
  ]
  node [
    id 138
    label "oszustwo"
  ]
  node [
    id 139
    label "wymys&#322;"
  ]
  node [
    id 140
    label "blef"
  ]
  node [
    id 141
    label "nieprawda"
  ]
  node [
    id 142
    label "s&#261;d"
  ]
  node [
    id 143
    label "truth"
  ]
  node [
    id 144
    label "nieprawdziwy"
  ]
  node [
    id 145
    label "za&#322;o&#380;enie"
  ]
  node [
    id 146
    label "prawdziwy"
  ]
  node [
    id 147
    label "realia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
]
