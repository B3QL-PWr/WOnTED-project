graph [
  maxDegree 5
  minDegree 1
  meanDegree 2
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "ulubiony"
    origin "text"
  ]
  node [
    id 1
    label "tost"
    origin "text"
  ]
  node [
    id 2
    label "heheszki"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "memy"
    origin "text"
  ]
  node [
    id 5
    label "wyj&#261;tkowy"
  ]
  node [
    id 6
    label "faworytny"
  ]
  node [
    id 7
    label "kromka"
  ]
  node [
    id 8
    label "przypiec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
]
