graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.0259740259740258
  density 0.026657552973342446
  graphCliqueNumber 3
  node [
    id 0
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 1
    label "nasz"
    origin "text"
  ]
  node [
    id 2
    label "c&#243;rka"
    origin "text"
  ]
  node [
    id 3
    label "ko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 6
    label "doba"
  ]
  node [
    id 7
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 8
    label "dzi&#347;"
  ]
  node [
    id 9
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 10
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 11
    label "czyj&#347;"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "dziewka"
  ]
  node [
    id 14
    label "potomkini"
  ]
  node [
    id 15
    label "dziewoja"
  ]
  node [
    id 16
    label "dziunia"
  ]
  node [
    id 17
    label "dziecko"
  ]
  node [
    id 18
    label "dziewczynina"
  ]
  node [
    id 19
    label "siksa"
  ]
  node [
    id 20
    label "dziewcz&#281;"
  ]
  node [
    id 21
    label "kora"
  ]
  node [
    id 22
    label "m&#322;&#243;dka"
  ]
  node [
    id 23
    label "dziecina"
  ]
  node [
    id 24
    label "sikorka"
  ]
  node [
    id 25
    label "zako&#324;cza&#263;"
  ]
  node [
    id 26
    label "przestawa&#263;"
  ]
  node [
    id 27
    label "robi&#263;"
  ]
  node [
    id 28
    label "satisfy"
  ]
  node [
    id 29
    label "close"
  ]
  node [
    id 30
    label "determine"
  ]
  node [
    id 31
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 32
    label "stanowi&#263;"
  ]
  node [
    id 33
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 34
    label "czas"
  ]
  node [
    id 35
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 36
    label "weekend"
  ]
  node [
    id 37
    label "miesi&#261;c"
  ]
  node [
    id 38
    label "energy"
  ]
  node [
    id 39
    label "bycie"
  ]
  node [
    id 40
    label "zegar_biologiczny"
  ]
  node [
    id 41
    label "okres_noworodkowy"
  ]
  node [
    id 42
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 43
    label "entity"
  ]
  node [
    id 44
    label "prze&#380;ywanie"
  ]
  node [
    id 45
    label "prze&#380;ycie"
  ]
  node [
    id 46
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 47
    label "wiek_matuzalemowy"
  ]
  node [
    id 48
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 49
    label "dzieci&#324;stwo"
  ]
  node [
    id 50
    label "power"
  ]
  node [
    id 51
    label "szwung"
  ]
  node [
    id 52
    label "menopauza"
  ]
  node [
    id 53
    label "umarcie"
  ]
  node [
    id 54
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 55
    label "life"
  ]
  node [
    id 56
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 57
    label "&#380;ywy"
  ]
  node [
    id 58
    label "rozw&#243;j"
  ]
  node [
    id 59
    label "po&#322;&#243;g"
  ]
  node [
    id 60
    label "byt"
  ]
  node [
    id 61
    label "przebywanie"
  ]
  node [
    id 62
    label "subsistence"
  ]
  node [
    id 63
    label "koleje_losu"
  ]
  node [
    id 64
    label "raj_utracony"
  ]
  node [
    id 65
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 66
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 67
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 68
    label "andropauza"
  ]
  node [
    id 69
    label "warunki"
  ]
  node [
    id 70
    label "do&#380;ywanie"
  ]
  node [
    id 71
    label "niemowl&#281;ctwo"
  ]
  node [
    id 72
    label "umieranie"
  ]
  node [
    id 73
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 74
    label "staro&#347;&#263;"
  ]
  node [
    id 75
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 76
    label "&#347;mier&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
]
