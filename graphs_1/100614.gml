graph [
  maxDegree 218
  minDegree 1
  meanDegree 2.536812674743709
  density 0.0011826632516287688
  graphCliqueNumber 11
  node [
    id 0
    label "&#347;rodowisko"
    origin "text"
  ]
  node [
    id 1
    label "polski"
    origin "text"
  ]
  node [
    id 2
    label "historyk"
    origin "text"
  ]
  node [
    id 3
    label "panowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "opinia"
    origin "text"
  ]
  node [
    id 5
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 6
    label "siedemnasta"
    origin "text"
  ]
  node [
    id 7
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 8
    label "kampania"
    origin "text"
  ]
  node [
    id 9
    label "wrze&#347;niowy"
    origin "text"
  ]
  node [
    id 10
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "ostatecznie"
    origin "text"
  ]
  node [
    id 12
    label "przegrana"
    origin "text"
  ]
  node [
    id 13
    label "trudno"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;"
    origin "text"
  ]
  node [
    id 15
    label "tym"
    origin "text"
  ]
  node [
    id 16
    label "zgodzi&#263;"
    origin "text"
  ]
  node [
    id 17
    label "agresor"
    origin "text"
  ]
  node [
    id 18
    label "zach&#243;d"
    origin "text"
  ]
  node [
    id 19
    label "dysponowa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "prawie"
    origin "text"
  ]
  node [
    id 21
    label "milion"
    origin "text"
  ]
  node [
    id 22
    label "&#380;o&#322;nierz"
    origin "text"
  ]
  node [
    id 23
    label "tys"
    origin "text"
  ]
  node [
    id 24
    label "dzia&#322;"
    origin "text"
  ]
  node [
    id 25
    label "czo&#322;g"
    origin "text"
  ]
  node [
    id 26
    label "samolot"
    origin "text"
  ]
  node [
    id 27
    label "do&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 28
    label "nieprzyjaciel"
    origin "text"
  ]
  node [
    id 29
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 30
    label "rzuci&#263;"
    origin "text"
  ]
  node [
    id 31
    label "walka"
    origin "text"
  ]
  node [
    id 32
    label "ponad"
    origin "text"
  ]
  node [
    id 33
    label "tysi&#261;c"
    origin "text"
  ]
  node [
    id 34
    label "wojsko"
    origin "text"
  ]
  node [
    id 35
    label "powa&#380;nie"
    origin "text"
  ]
  node [
    id 36
    label "os&#322;abi&#263;"
    origin "text"
  ]
  node [
    id 37
    label "walek"
    origin "text"
  ]
  node [
    id 38
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 39
    label "fizyczny"
    origin "text"
  ]
  node [
    id 40
    label "szansa"
    origin "text"
  ]
  node [
    id 41
    label "powstrzyma&#263;"
    origin "text"
  ]
  node [
    id 42
    label "taki"
    origin "text"
  ]
  node [
    id 43
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 44
    label "rzeczywi&#347;cie"
    origin "text"
  ]
  node [
    id 45
    label "tak"
    origin "text"
  ]
  node [
    id 46
    label "by&#263;"
    origin "text"
  ]
  node [
    id 47
    label "wsch&#243;d"
    origin "text"
  ]
  node [
    id 48
    label "naprawd&#281;"
    origin "text"
  ]
  node [
    id 49
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 50
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 51
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 52
    label "stawi&#263;"
    origin "text"
  ]
  node [
    id 53
    label "twardy"
    origin "text"
  ]
  node [
    id 54
    label "opora"
    origin "text"
  ]
  node [
    id 55
    label "armia"
    origin "text"
  ]
  node [
    id 56
    label "czerwona"
    origin "text"
  ]
  node [
    id 57
    label "dzisiejsi"
    origin "text"
  ]
  node [
    id 58
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 59
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 60
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 61
    label "wojna"
    origin "text"
  ]
  node [
    id 62
    label "zanika&#263;"
    origin "text"
  ]
  node [
    id 63
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 64
    label "demokracja"
    origin "text"
  ]
  node [
    id 65
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 66
    label "rok"
    origin "text"
  ]
  node [
    id 67
    label "liczba"
    origin "text"
  ]
  node [
    id 68
    label "konflikt"
    origin "text"
  ]
  node [
    id 69
    label "male&#263;"
    origin "text"
  ]
  node [
    id 70
    label "daleko"
    origin "text"
  ]
  node [
    id 71
    label "trwa&#263;"
    origin "text"
  ]
  node [
    id 72
    label "dwa"
    origin "text"
  ]
  node [
    id 73
    label "wielki"
    origin "text"
  ]
  node [
    id 74
    label "starcie"
    origin "text"
  ]
  node [
    id 75
    label "pierwsza"
    origin "text"
  ]
  node [
    id 76
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 77
    label "dzienia"
    origin "text"
  ]
  node [
    id 78
    label "telewizja"
    origin "text"
  ]
  node [
    id 79
    label "gdy"
    origin "text"
  ]
  node [
    id 80
    label "dowiadywa&#263;"
    origin "text"
  ]
  node [
    id 81
    label "kolejny"
    origin "text"
  ]
  node [
    id 82
    label "anonimowy"
    origin "text"
  ]
  node [
    id 83
    label "ofiara"
    origin "text"
  ]
  node [
    id 84
    label "irak"
    origin "text"
  ]
  node [
    id 85
    label "terroryzm"
    origin "text"
  ]
  node [
    id 86
    label "nag&#322;o&#347;ni&#263;"
    origin "text"
  ]
  node [
    id 87
    label "przez"
    origin "text"
  ]
  node [
    id 88
    label "media"
    origin "text"
  ]
  node [
    id 89
    label "wr&#243;g"
    origin "text"
  ]
  node [
    id 90
    label "znany"
    origin "text"
  ]
  node [
    id 91
    label "zwyci&#281;stwo"
    origin "text"
  ]
  node [
    id 92
    label "zdawa&#263;"
    origin "text"
  ]
  node [
    id 93
    label "zasi&#261;g"
    origin "text"
  ]
  node [
    id 94
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 95
    label "ale"
    origin "text"
  ]
  node [
    id 96
    label "opr&#243;cz"
    origin "text"
  ]
  node [
    id 97
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 98
    label "jeszcze"
    origin "text"
  ]
  node [
    id 99
    label "jeden"
    origin "text"
  ]
  node [
    id 100
    label "raz"
    origin "text"
  ]
  node [
    id 101
    label "zauwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 102
    label "r&#243;&#380;nica"
    origin "text"
  ]
  node [
    id 103
    label "stosunek"
    origin "text"
  ]
  node [
    id 104
    label "&#347;wiatowy"
    origin "text"
  ]
  node [
    id 105
    label "ukry&#263;"
    origin "text"
  ]
  node [
    id 106
    label "toczy&#263;"
    origin "text"
  ]
  node [
    id 107
    label "codziennie"
    origin "text"
  ]
  node [
    id 108
    label "ulica"
    origin "text"
  ]
  node [
    id 109
    label "nasi"
    origin "text"
  ]
  node [
    id 110
    label "jaki"
    origin "text"
  ]
  node [
    id 111
    label "zbiera&#263;"
    origin "text"
  ]
  node [
    id 112
    label "&#380;niwo"
    origin "text"
  ]
  node [
    id 113
    label "hekatomba"
    origin "text"
  ]
  node [
    id 114
    label "cywilny"
    origin "text"
  ]
  node [
    id 115
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 116
    label "s&#322;ucha&#263;"
    origin "text"
  ]
  node [
    id 117
    label "radio"
    origin "text"
  ]
  node [
    id 118
    label "czyta&#263;"
    origin "text"
  ]
  node [
    id 119
    label "gazeta"
    origin "text"
  ]
  node [
    id 120
    label "mama"
    origin "text"
  ]
  node [
    id 121
    label "okazja"
    origin "text"
  ]
  node [
    id 122
    label "pozna&#263;"
    origin "text"
  ]
  node [
    id 123
    label "przekonywa&#263;"
    origin "text"
  ]
  node [
    id 124
    label "nasa"
    origin "text"
  ]
  node [
    id 125
    label "jak"
    origin "text"
  ]
  node [
    id 126
    label "bardzo"
    origin "text"
  ]
  node [
    id 127
    label "z&#322;o"
    origin "text"
  ]
  node [
    id 128
    label "beznadziejny"
    origin "text"
  ]
  node [
    id 129
    label "sytuacja"
    origin "text"
  ]
  node [
    id 130
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 131
    label "narzeka&#263;"
    origin "text"
  ]
  node [
    id 132
    label "przygotowywa&#263;"
    origin "text"
  ]
  node [
    id 133
    label "doros&#322;y"
    origin "text"
  ]
  node [
    id 134
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 135
    label "samodzielny"
    origin "text"
  ]
  node [
    id 136
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 137
    label "decyzja"
    origin "text"
  ]
  node [
    id 138
    label "kreatywny"
    origin "text"
  ]
  node [
    id 139
    label "my&#347;lenie"
    origin "text"
  ]
  node [
    id 140
    label "pewnie"
    origin "text"
  ]
  node [
    id 141
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 142
    label "racja"
    origin "text"
  ]
  node [
    id 143
    label "wszystek"
    origin "text"
  ]
  node [
    id 144
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 145
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 146
    label "sko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 147
    label "kilkana&#347;cie"
    origin "text"
  ]
  node [
    id 148
    label "lata"
    origin "text"
  ]
  node [
    id 149
    label "temu"
    origin "text"
  ]
  node [
    id 150
    label "dobry"
    origin "text"
  ]
  node [
    id 151
    label "wypadek"
    origin "text"
  ]
  node [
    id 152
    label "kompletnie"
    origin "text"
  ]
  node [
    id 153
    label "inny"
    origin "text"
  ]
  node [
    id 154
    label "system"
    origin "text"
  ]
  node [
    id 155
    label "kilka"
    origin "text"
  ]
  node [
    id 156
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 157
    label "plant"
    origin "text"
  ]
  node [
    id 158
    label "wok&#243;&#322;"
    origin "text"
  ]
  node [
    id 159
    label "rynek"
    origin "text"
  ]
  node [
    id 160
    label "p&#322;achta"
    origin "text"
  ]
  node [
    id 161
    label "widnie&#263;"
    origin "text"
  ]
  node [
    id 162
    label "satyryczny"
    origin "text"
  ]
  node [
    id 163
    label "rysunek"
    origin "text"
  ]
  node [
    id 164
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 165
    label "rysownik"
    origin "text"
  ]
  node [
    id 166
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 167
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 168
    label "przy"
    origin "text"
  ]
  node [
    id 169
    label "ula"
    origin "text"
  ]
  node [
    id 170
    label "podwale"
    origin "text"
  ]
  node [
    id 171
    label "ko&#322;o"
    origin "text"
  ]
  node [
    id 172
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 173
    label "prosty"
    origin "text"
  ]
  node [
    id 174
    label "centrum"
    origin "text"
  ]
  node [
    id 175
    label "posta&#263;"
    origin "text"
  ]
  node [
    id 176
    label "tradycyjny"
    origin "text"
  ]
  node [
    id 177
    label "krakowskie"
    origin "text"
  ]
  node [
    id 178
    label "str&#243;j"
    origin "text"
  ]
  node [
    id 179
    label "twarz"
    origin "text"
  ]
  node [
    id 180
    label "wida&#263;"
    origin "text"
  ]
  node [
    id 181
    label "rozpacz"
    origin "text"
  ]
  node [
    id 182
    label "przera&#380;enie"
    origin "text"
  ]
  node [
    id 183
    label "nad"
    origin "text"
  ]
  node [
    id 184
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 185
    label "napis"
    origin "text"
  ]
  node [
    id 186
    label "pan"
    origin "text"
  ]
  node [
    id 187
    label "olska"
    origin "text"
  ]
  node [
    id 188
    label "europa"
    origin "text"
  ]
  node [
    id 189
    label "&#347;rodkowa"
    origin "text"
  ]
  node [
    id 190
    label "nie"
    origin "text"
  ]
  node [
    id 191
    label "problem"
    origin "text"
  ]
  node [
    id 192
    label "silny"
    origin "text"
  ]
  node [
    id 193
    label "wszystko"
    origin "text"
  ]
  node [
    id 194
    label "polska"
    origin "text"
  ]
  node [
    id 195
    label "nadchodzi&#263;"
    origin "text"
  ]
  node [
    id 196
    label "kryzys"
    origin "text"
  ]
  node [
    id 197
    label "demograficzny"
    origin "text"
  ]
  node [
    id 198
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 199
    label "przekre&#347;li&#263;"
    origin "text"
  ]
  node [
    id 200
    label "wysi&#322;ek"
    origin "text"
  ]
  node [
    id 201
    label "budowa"
    origin "text"
  ]
  node [
    id 202
    label "stabilny"
    origin "text"
  ]
  node [
    id 203
    label "gospodarka"
    origin "text"
  ]
  node [
    id 204
    label "zagra&#380;a&#263;"
    origin "text"
  ]
  node [
    id 205
    label "byt"
    origin "text"
  ]
  node [
    id 206
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 207
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 208
    label "przys&#322;owiowy"
    origin "text"
  ]
  node [
    id 209
    label "najwy&#380;szy"
    origin "text"
  ]
  node [
    id 210
    label "implozja"
    origin "text"
  ]
  node [
    id 211
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 212
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 213
    label "wisie&#263;"
    origin "text"
  ]
  node [
    id 214
    label "raj"
    origin "text"
  ]
  node [
    id 215
    label "niczym"
    origin "text"
  ]
  node [
    id 216
    label "top&#243;r"
    origin "text"
  ]
  node [
    id 217
    label "skazany"
    origin "text"
  ]
  node [
    id 218
    label "czas"
    origin "text"
  ]
  node [
    id 219
    label "aby"
    origin "text"
  ]
  node [
    id 220
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 221
    label "siebie"
    origin "text"
  ]
  node [
    id 222
    label "prawda"
    origin "text"
  ]
  node [
    id 223
    label "temat"
    origin "text"
  ]
  node [
    id 224
    label "zjawisko"
    origin "text"
  ]
  node [
    id 225
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 226
    label "cela"
    origin "text"
  ]
  node [
    id 227
    label "w&#322;a&#347;ciwie"
    origin "text"
  ]
  node [
    id 228
    label "tylko"
    origin "text"
  ]
  node [
    id 229
    label "odnie&#347;&#263;"
    origin "text"
  ]
  node [
    id 230
    label "wsp&#243;lny"
    origin "text"
  ]
  node [
    id 231
    label "d&#322;ugoletni"
    origin "text"
  ]
  node [
    id 232
    label "sukces"
    origin "text"
  ]
  node [
    id 233
    label "cykl"
    origin "text"
  ]
  node [
    id 234
    label "geograf"
    origin "text"
  ]
  node [
    id 235
    label "historia"
    origin "text"
  ]
  node [
    id 236
    label "bieda"
    origin "text"
  ]
  node [
    id 237
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 238
    label "obiekt_naturalny"
  ]
  node [
    id 239
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 240
    label "grupa"
  ]
  node [
    id 241
    label "stw&#243;r"
  ]
  node [
    id 242
    label "rzecz"
  ]
  node [
    id 243
    label "environment"
  ]
  node [
    id 244
    label "biota"
  ]
  node [
    id 245
    label "wszechstworzenie"
  ]
  node [
    id 246
    label "otoczenie"
  ]
  node [
    id 247
    label "fauna"
  ]
  node [
    id 248
    label "ekosystem"
  ]
  node [
    id 249
    label "teren"
  ]
  node [
    id 250
    label "mikrokosmos"
  ]
  node [
    id 251
    label "class"
  ]
  node [
    id 252
    label "zesp&#243;&#322;"
  ]
  node [
    id 253
    label "warunki"
  ]
  node [
    id 254
    label "huczek"
  ]
  node [
    id 255
    label "Ziemia"
  ]
  node [
    id 256
    label "woda"
  ]
  node [
    id 257
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 258
    label "lacki"
  ]
  node [
    id 259
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 260
    label "przedmiot"
  ]
  node [
    id 261
    label "sztajer"
  ]
  node [
    id 262
    label "drabant"
  ]
  node [
    id 263
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 264
    label "polak"
  ]
  node [
    id 265
    label "pierogi_ruskie"
  ]
  node [
    id 266
    label "krakowiak"
  ]
  node [
    id 267
    label "Polish"
  ]
  node [
    id 268
    label "j&#281;zyk"
  ]
  node [
    id 269
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 270
    label "oberek"
  ]
  node [
    id 271
    label "po_polsku"
  ]
  node [
    id 272
    label "mazur"
  ]
  node [
    id 273
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 274
    label "chodzony"
  ]
  node [
    id 275
    label "skoczny"
  ]
  node [
    id 276
    label "ryba_po_grecku"
  ]
  node [
    id 277
    label "goniony"
  ]
  node [
    id 278
    label "polsko"
  ]
  node [
    id 279
    label "nauczyciel"
  ]
  node [
    id 280
    label "Voltaire"
  ]
  node [
    id 281
    label "Michnik"
  ]
  node [
    id 282
    label "humanista"
  ]
  node [
    id 283
    label "przewa&#380;a&#263;"
  ]
  node [
    id 284
    label "kontrolowa&#263;"
  ]
  node [
    id 285
    label "control"
  ]
  node [
    id 286
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 287
    label "kierowa&#263;"
  ]
  node [
    id 288
    label "manipulate"
  ]
  node [
    id 289
    label "dominowa&#263;"
  ]
  node [
    id 290
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 291
    label "dokument"
  ]
  node [
    id 292
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 293
    label "reputacja"
  ]
  node [
    id 294
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 295
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 296
    label "cecha"
  ]
  node [
    id 297
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 298
    label "informacja"
  ]
  node [
    id 299
    label "sofcik"
  ]
  node [
    id 300
    label "appraisal"
  ]
  node [
    id 301
    label "ekspertyza"
  ]
  node [
    id 302
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 303
    label "pogl&#261;d"
  ]
  node [
    id 304
    label "kryterium"
  ]
  node [
    id 305
    label "wielko&#347;&#263;"
  ]
  node [
    id 306
    label "s&#322;o&#324;ce"
  ]
  node [
    id 307
    label "czynienie_si&#281;"
  ]
  node [
    id 308
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 309
    label "long_time"
  ]
  node [
    id 310
    label "przedpo&#322;udnie"
  ]
  node [
    id 311
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 312
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 313
    label "tydzie&#324;"
  ]
  node [
    id 314
    label "godzina"
  ]
  node [
    id 315
    label "t&#322;usty_czwartek"
  ]
  node [
    id 316
    label "wsta&#263;"
  ]
  node [
    id 317
    label "day"
  ]
  node [
    id 318
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 319
    label "przedwiecz&#243;r"
  ]
  node [
    id 320
    label "Sylwester"
  ]
  node [
    id 321
    label "po&#322;udnie"
  ]
  node [
    id 322
    label "wzej&#347;cie"
  ]
  node [
    id 323
    label "podwiecz&#243;r"
  ]
  node [
    id 324
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 325
    label "rano"
  ]
  node [
    id 326
    label "termin"
  ]
  node [
    id 327
    label "ranek"
  ]
  node [
    id 328
    label "doba"
  ]
  node [
    id 329
    label "wiecz&#243;r"
  ]
  node [
    id 330
    label "walentynki"
  ]
  node [
    id 331
    label "popo&#322;udnie"
  ]
  node [
    id 332
    label "noc"
  ]
  node [
    id 333
    label "wstanie"
  ]
  node [
    id 334
    label "dzia&#322;anie"
  ]
  node [
    id 335
    label "campaign"
  ]
  node [
    id 336
    label "wydarzenie"
  ]
  node [
    id 337
    label "akcja"
  ]
  node [
    id 338
    label "Kampania_Wrze&#347;niowa"
  ]
  node [
    id 339
    label "proceed"
  ]
  node [
    id 340
    label "catch"
  ]
  node [
    id 341
    label "pozosta&#263;"
  ]
  node [
    id 342
    label "osta&#263;_si&#281;"
  ]
  node [
    id 343
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 344
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 345
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 346
    label "change"
  ]
  node [
    id 347
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 348
    label "ostateczny"
  ]
  node [
    id 349
    label "finalnie"
  ]
  node [
    id 350
    label "zupe&#322;nie"
  ]
  node [
    id 351
    label "gorzka_pigu&#322;ka"
  ]
  node [
    id 352
    label "przegra"
  ]
  node [
    id 353
    label "wysiadka"
  ]
  node [
    id 354
    label "passa"
  ]
  node [
    id 355
    label "niefortunno&#347;&#263;"
  ]
  node [
    id 356
    label "po&#322;o&#380;enie"
  ]
  node [
    id 357
    label "niepowodzenie"
  ]
  node [
    id 358
    label "strata"
  ]
  node [
    id 359
    label "reverse"
  ]
  node [
    id 360
    label "rezultat"
  ]
  node [
    id 361
    label "kwota"
  ]
  node [
    id 362
    label "k&#322;adzenie"
  ]
  node [
    id 363
    label "trudny"
  ]
  node [
    id 364
    label "hard"
  ]
  node [
    id 365
    label "zatrudni&#263;"
  ]
  node [
    id 366
    label "zgodzenie"
  ]
  node [
    id 367
    label "pomoc"
  ]
  node [
    id 368
    label "zgadza&#263;"
  ]
  node [
    id 369
    label "brutal"
  ]
  node [
    id 370
    label "agresywny"
  ]
  node [
    id 371
    label "inicjator"
  ]
  node [
    id 372
    label "z&#322;oczy&#324;ca"
  ]
  node [
    id 373
    label "usi&#322;owanie"
  ]
  node [
    id 374
    label "trud"
  ]
  node [
    id 375
    label "obszar"
  ]
  node [
    id 376
    label "sunset"
  ]
  node [
    id 377
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 378
    label "strona_&#347;wiata"
  ]
  node [
    id 379
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 380
    label "pora"
  ]
  node [
    id 381
    label "szar&#243;wka"
  ]
  node [
    id 382
    label "namaszczenie_chorych"
  ]
  node [
    id 383
    label "ekonomia"
  ]
  node [
    id 384
    label "command"
  ]
  node [
    id 385
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 386
    label "rozporz&#261;dza&#263;"
  ]
  node [
    id 387
    label "dispose"
  ]
  node [
    id 388
    label "miljon"
  ]
  node [
    id 389
    label "ba&#324;ka"
  ]
  node [
    id 390
    label "demobilizowa&#263;"
  ]
  node [
    id 391
    label "rota"
  ]
  node [
    id 392
    label "walcz&#261;cy"
  ]
  node [
    id 393
    label "demobilizowanie"
  ]
  node [
    id 394
    label "harcap"
  ]
  node [
    id 395
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 396
    label "&#380;o&#322;dowy"
  ]
  node [
    id 397
    label "zdemobilizowanie"
  ]
  node [
    id 398
    label "elew"
  ]
  node [
    id 399
    label "mundurowy"
  ]
  node [
    id 400
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 401
    label "so&#322;dat"
  ]
  node [
    id 402
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 403
    label "zdemobilizowa&#263;"
  ]
  node [
    id 404
    label "Gurkha"
  ]
  node [
    id 405
    label "zakres"
  ]
  node [
    id 406
    label "whole"
  ]
  node [
    id 407
    label "wytw&#243;r"
  ]
  node [
    id 408
    label "column"
  ]
  node [
    id 409
    label "distribution"
  ]
  node [
    id 410
    label "bezdro&#380;e"
  ]
  node [
    id 411
    label "competence"
  ]
  node [
    id 412
    label "urz&#261;d"
  ]
  node [
    id 413
    label "jednostka_organizacyjna"
  ]
  node [
    id 414
    label "stopie&#324;"
  ]
  node [
    id 415
    label "insourcing"
  ]
  node [
    id 416
    label "sfera"
  ]
  node [
    id 417
    label "miejsce_pracy"
  ]
  node [
    id 418
    label "poddzia&#322;"
  ]
  node [
    id 419
    label "ci&#281;&#380;ki_sprz&#281;t"
  ]
  node [
    id 420
    label "opancerzony_pojazd_bojowy"
  ]
  node [
    id 421
    label "armata"
  ]
  node [
    id 422
    label "ekran_przeciwkumulacyjny"
  ]
  node [
    id 423
    label "tra&#322;"
  ]
  node [
    id 424
    label "pojazd_g&#261;sienicowy"
  ]
  node [
    id 425
    label "luk"
  ]
  node [
    id 426
    label "wojska_pancerne"
  ]
  node [
    id 427
    label "bro&#324;"
  ]
  node [
    id 428
    label "peryskop"
  ]
  node [
    id 429
    label "bro&#324;_pancerna"
  ]
  node [
    id 430
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 431
    label "p&#322;atowiec"
  ]
  node [
    id 432
    label "lecenie"
  ]
  node [
    id 433
    label "gondola"
  ]
  node [
    id 434
    label "wylecie&#263;"
  ]
  node [
    id 435
    label "kapotowanie"
  ]
  node [
    id 436
    label "wylatywa&#263;"
  ]
  node [
    id 437
    label "katapulta"
  ]
  node [
    id 438
    label "dzi&#243;b"
  ]
  node [
    id 439
    label "sterownica"
  ]
  node [
    id 440
    label "kad&#322;ub"
  ]
  node [
    id 441
    label "kapotowa&#263;"
  ]
  node [
    id 442
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 443
    label "fotel_lotniczy"
  ]
  node [
    id 444
    label "kabina"
  ]
  node [
    id 445
    label "wylatywanie"
  ]
  node [
    id 446
    label "pilot_automatyczny"
  ]
  node [
    id 447
    label "inhalator_tlenowy"
  ]
  node [
    id 448
    label "kapota&#380;"
  ]
  node [
    id 449
    label "pok&#322;ad"
  ]
  node [
    id 450
    label "&#380;yroskop"
  ]
  node [
    id 451
    label "sta&#322;op&#322;at"
  ]
  node [
    id 452
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 453
    label "wy&#347;lizg"
  ]
  node [
    id 454
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 455
    label "skrzyd&#322;o"
  ]
  node [
    id 456
    label "wiatrochron"
  ]
  node [
    id 457
    label "spalin&#243;wka"
  ]
  node [
    id 458
    label "czarna_skrzynka"
  ]
  node [
    id 459
    label "kapot"
  ]
  node [
    id 460
    label "wylecenie"
  ]
  node [
    id 461
    label "kabinka"
  ]
  node [
    id 462
    label "spowodowa&#263;"
  ]
  node [
    id 463
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 464
    label "zrobi&#263;"
  ]
  node [
    id 465
    label "articulation"
  ]
  node [
    id 466
    label "dokoptowa&#263;"
  ]
  node [
    id 467
    label "przeciwnik"
  ]
  node [
    id 468
    label "wyzwanie"
  ]
  node [
    id 469
    label "majdn&#261;&#263;"
  ]
  node [
    id 470
    label "opu&#347;ci&#263;"
  ]
  node [
    id 471
    label "cie&#324;"
  ]
  node [
    id 472
    label "konwulsja"
  ]
  node [
    id 473
    label "podejrzenie"
  ]
  node [
    id 474
    label "wywo&#322;a&#263;"
  ]
  node [
    id 475
    label "ruszy&#263;"
  ]
  node [
    id 476
    label "odej&#347;&#263;"
  ]
  node [
    id 477
    label "project"
  ]
  node [
    id 478
    label "da&#263;"
  ]
  node [
    id 479
    label "czar"
  ]
  node [
    id 480
    label "atak"
  ]
  node [
    id 481
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 482
    label "zdecydowa&#263;"
  ]
  node [
    id 483
    label "rush"
  ]
  node [
    id 484
    label "bewilder"
  ]
  node [
    id 485
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 486
    label "sygn&#261;&#263;"
  ]
  node [
    id 487
    label "zmieni&#263;"
  ]
  node [
    id 488
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 489
    label "poruszy&#263;"
  ]
  node [
    id 490
    label "&#347;wiat&#322;o"
  ]
  node [
    id 491
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 492
    label "most"
  ]
  node [
    id 493
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 494
    label "frame"
  ]
  node [
    id 495
    label "przeznaczenie"
  ]
  node [
    id 496
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 497
    label "peddle"
  ]
  node [
    id 498
    label "skonstruowa&#263;"
  ]
  node [
    id 499
    label "towar"
  ]
  node [
    id 500
    label "czyn"
  ]
  node [
    id 501
    label "trudno&#347;&#263;"
  ]
  node [
    id 502
    label "zaatakowanie"
  ]
  node [
    id 503
    label "obrona"
  ]
  node [
    id 504
    label "konfrontacyjny"
  ]
  node [
    id 505
    label "military_action"
  ]
  node [
    id 506
    label "wrestle"
  ]
  node [
    id 507
    label "action"
  ]
  node [
    id 508
    label "rywalizacja"
  ]
  node [
    id 509
    label "sambo"
  ]
  node [
    id 510
    label "contest"
  ]
  node [
    id 511
    label "sp&#243;r"
  ]
  node [
    id 512
    label "tauzen"
  ]
  node [
    id 513
    label "musik"
  ]
  node [
    id 514
    label "molarity"
  ]
  node [
    id 515
    label "licytacja"
  ]
  node [
    id 516
    label "patyk"
  ]
  node [
    id 517
    label "gra_w_karty"
  ]
  node [
    id 518
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 519
    label "dryl"
  ]
  node [
    id 520
    label "ods&#322;ugiwanie"
  ]
  node [
    id 521
    label "korpus"
  ]
  node [
    id 522
    label "s&#322;u&#380;ba"
  ]
  node [
    id 523
    label "wojo"
  ]
  node [
    id 524
    label "zrejterowanie"
  ]
  node [
    id 525
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 526
    label "werbowanie_si&#281;"
  ]
  node [
    id 527
    label "zmobilizowa&#263;"
  ]
  node [
    id 528
    label "struktura"
  ]
  node [
    id 529
    label "oddzia&#322;"
  ]
  node [
    id 530
    label "mobilizowa&#263;"
  ]
  node [
    id 531
    label "Armia_Krajowa"
  ]
  node [
    id 532
    label "pozycja"
  ]
  node [
    id 533
    label "mobilizowanie"
  ]
  node [
    id 534
    label "fala"
  ]
  node [
    id 535
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 536
    label "pospolite_ruszenie"
  ]
  node [
    id 537
    label "Armia_Czerwona"
  ]
  node [
    id 538
    label "rejterowanie"
  ]
  node [
    id 539
    label "cofni&#281;cie"
  ]
  node [
    id 540
    label "Eurokorpus"
  ]
  node [
    id 541
    label "tabor"
  ]
  node [
    id 542
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 543
    label "petarda"
  ]
  node [
    id 544
    label "zrejterowa&#263;"
  ]
  node [
    id 545
    label "zmobilizowanie"
  ]
  node [
    id 546
    label "rejterowa&#263;"
  ]
  node [
    id 547
    label "Czerwona_Gwardia"
  ]
  node [
    id 548
    label "Legia_Cudzoziemska"
  ]
  node [
    id 549
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 550
    label "wermacht"
  ]
  node [
    id 551
    label "soldateska"
  ]
  node [
    id 552
    label "oddzia&#322;_karny"
  ]
  node [
    id 553
    label "rezerwa"
  ]
  node [
    id 554
    label "or&#281;&#380;"
  ]
  node [
    id 555
    label "dezerter"
  ]
  node [
    id 556
    label "potencja"
  ]
  node [
    id 557
    label "sztabslekarz"
  ]
  node [
    id 558
    label "powa&#380;ny"
  ]
  node [
    id 559
    label "gro&#378;nie"
  ]
  node [
    id 560
    label "niema&#322;o"
  ]
  node [
    id 561
    label "ci&#281;&#380;ko"
  ]
  node [
    id 562
    label "ci&#281;&#380;ki"
  ]
  node [
    id 563
    label "os&#322;abia&#263;"
  ]
  node [
    id 564
    label "zmniejszy&#263;"
  ]
  node [
    id 565
    label "cushion"
  ]
  node [
    id 566
    label "zdrowie"
  ]
  node [
    id 567
    label "reduce"
  ]
  node [
    id 568
    label "kondycja_fizyczna"
  ]
  node [
    id 569
    label "os&#322;abienie"
  ]
  node [
    id 570
    label "os&#322;abianie"
  ]
  node [
    id 571
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 572
    label "czu&#263;"
  ]
  node [
    id 573
    label "need"
  ]
  node [
    id 574
    label "hide"
  ]
  node [
    id 575
    label "support"
  ]
  node [
    id 576
    label "fizykalnie"
  ]
  node [
    id 577
    label "fizycznie"
  ]
  node [
    id 578
    label "materializowanie"
  ]
  node [
    id 579
    label "pracownik"
  ]
  node [
    id 580
    label "gimnastyczny"
  ]
  node [
    id 581
    label "widoczny"
  ]
  node [
    id 582
    label "namacalny"
  ]
  node [
    id 583
    label "zmaterializowanie"
  ]
  node [
    id 584
    label "organiczny"
  ]
  node [
    id 585
    label "materjalny"
  ]
  node [
    id 586
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 587
    label "continue"
  ]
  node [
    id 588
    label "zreflektowa&#263;"
  ]
  node [
    id 589
    label "zaczepi&#263;"
  ]
  node [
    id 590
    label "anticipate"
  ]
  node [
    id 591
    label "okre&#347;lony"
  ]
  node [
    id 592
    label "magnitude"
  ]
  node [
    id 593
    label "energia"
  ]
  node [
    id 594
    label "capacity"
  ]
  node [
    id 595
    label "wuchta"
  ]
  node [
    id 596
    label "parametr"
  ]
  node [
    id 597
    label "moment_si&#322;y"
  ]
  node [
    id 598
    label "przemoc"
  ]
  node [
    id 599
    label "zdolno&#347;&#263;"
  ]
  node [
    id 600
    label "mn&#243;stwo"
  ]
  node [
    id 601
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 602
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 603
    label "zaleta"
  ]
  node [
    id 604
    label "realny"
  ]
  node [
    id 605
    label "si&#281;ga&#263;"
  ]
  node [
    id 606
    label "obecno&#347;&#263;"
  ]
  node [
    id 607
    label "stan"
  ]
  node [
    id 608
    label "stand"
  ]
  node [
    id 609
    label "mie&#263;_miejsce"
  ]
  node [
    id 610
    label "uczestniczy&#263;"
  ]
  node [
    id 611
    label "chodzi&#263;"
  ]
  node [
    id 612
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 613
    label "equal"
  ]
  node [
    id 614
    label "szabas"
  ]
  node [
    id 615
    label "pocz&#261;tek"
  ]
  node [
    id 616
    label "brzask"
  ]
  node [
    id 617
    label "prawdziwy"
  ]
  node [
    id 618
    label "uprawi&#263;"
  ]
  node [
    id 619
    label "gotowy"
  ]
  node [
    id 620
    label "might"
  ]
  node [
    id 621
    label "obstawi&#263;"
  ]
  node [
    id 622
    label "usztywnienie"
  ]
  node [
    id 623
    label "usztywnianie"
  ]
  node [
    id 624
    label "wytrzyma&#322;y"
  ]
  node [
    id 625
    label "nieugi&#281;ty"
  ]
  node [
    id 626
    label "zesztywnienie"
  ]
  node [
    id 627
    label "konkretny"
  ]
  node [
    id 628
    label "zdeterminowany"
  ]
  node [
    id 629
    label "niewra&#380;liwy"
  ]
  node [
    id 630
    label "sta&#322;y"
  ]
  node [
    id 631
    label "twardo"
  ]
  node [
    id 632
    label "mocny"
  ]
  node [
    id 633
    label "sztywnienie"
  ]
  node [
    id 634
    label "utrudnienie"
  ]
  node [
    id 635
    label "anchor"
  ]
  node [
    id 636
    label "podstawa"
  ]
  node [
    id 637
    label "bateria"
  ]
  node [
    id 638
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 639
    label "legia"
  ]
  node [
    id 640
    label "milicja"
  ]
  node [
    id 641
    label "artyleria"
  ]
  node [
    id 642
    label "rzut"
  ]
  node [
    id 643
    label "linia"
  ]
  node [
    id 644
    label "kawaleria_powietrzna"
  ]
  node [
    id 645
    label "t&#322;um"
  ]
  node [
    id 646
    label "zwi&#261;zek_operacyjny"
  ]
  node [
    id 647
    label "military"
  ]
  node [
    id 648
    label "szlak_bojowy"
  ]
  node [
    id 649
    label "dywizjon_artylerii"
  ]
  node [
    id 650
    label "brygada"
  ]
  node [
    id 651
    label "piechota"
  ]
  node [
    id 652
    label "dywizjon_kawaleryjski"
  ]
  node [
    id 653
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 654
    label "biosfera"
  ]
  node [
    id 655
    label "Stary_&#346;wiat"
  ]
  node [
    id 656
    label "magnetosfera"
  ]
  node [
    id 657
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 658
    label "Nowy_&#346;wiat"
  ]
  node [
    id 659
    label "geosfera"
  ]
  node [
    id 660
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 661
    label "planeta"
  ]
  node [
    id 662
    label "przejmowa&#263;"
  ]
  node [
    id 663
    label "litosfera"
  ]
  node [
    id 664
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 665
    label "makrokosmos"
  ]
  node [
    id 666
    label "barysfera"
  ]
  node [
    id 667
    label "p&#243;&#322;noc"
  ]
  node [
    id 668
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 669
    label "geotermia"
  ]
  node [
    id 670
    label "biegun"
  ]
  node [
    id 671
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 672
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 673
    label "p&#243;&#322;kula"
  ]
  node [
    id 674
    label "atmosfera"
  ]
  node [
    id 675
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 676
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 677
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 678
    label "przejmowanie"
  ]
  node [
    id 679
    label "przestrze&#324;"
  ]
  node [
    id 680
    label "asymilowanie_si&#281;"
  ]
  node [
    id 681
    label "przej&#261;&#263;"
  ]
  node [
    id 682
    label "ekosfera"
  ]
  node [
    id 683
    label "przyroda"
  ]
  node [
    id 684
    label "ciemna_materia"
  ]
  node [
    id 685
    label "geoida"
  ]
  node [
    id 686
    label "Wsch&#243;d"
  ]
  node [
    id 687
    label "populace"
  ]
  node [
    id 688
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 689
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 690
    label "universe"
  ]
  node [
    id 691
    label "ozonosfera"
  ]
  node [
    id 692
    label "rze&#378;ba"
  ]
  node [
    id 693
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 694
    label "zagranica"
  ]
  node [
    id 695
    label "hydrosfera"
  ]
  node [
    id 696
    label "kuchnia"
  ]
  node [
    id 697
    label "przej&#281;cie"
  ]
  node [
    id 698
    label "czarna_dziura"
  ]
  node [
    id 699
    label "morze"
  ]
  node [
    id 700
    label "impart"
  ]
  node [
    id 701
    label "panna_na_wydaniu"
  ]
  node [
    id 702
    label "surrender"
  ]
  node [
    id 703
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 704
    label "train"
  ]
  node [
    id 705
    label "give"
  ]
  node [
    id 706
    label "wytwarza&#263;"
  ]
  node [
    id 707
    label "dawa&#263;"
  ]
  node [
    id 708
    label "zapach"
  ]
  node [
    id 709
    label "wprowadza&#263;"
  ]
  node [
    id 710
    label "ujawnia&#263;"
  ]
  node [
    id 711
    label "wydawnictwo"
  ]
  node [
    id 712
    label "powierza&#263;"
  ]
  node [
    id 713
    label "produkcja"
  ]
  node [
    id 714
    label "denuncjowa&#263;"
  ]
  node [
    id 715
    label "plon"
  ]
  node [
    id 716
    label "reszta"
  ]
  node [
    id 717
    label "robi&#263;"
  ]
  node [
    id 718
    label "placard"
  ]
  node [
    id 719
    label "tajemnica"
  ]
  node [
    id 720
    label "wiano"
  ]
  node [
    id 721
    label "kojarzy&#263;"
  ]
  node [
    id 722
    label "d&#378;wi&#281;k"
  ]
  node [
    id 723
    label "podawa&#263;"
  ]
  node [
    id 724
    label "zimna_wojna"
  ]
  node [
    id 725
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 726
    label "angaria"
  ]
  node [
    id 727
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 728
    label "war"
  ]
  node [
    id 729
    label "wojna_stuletnia"
  ]
  node [
    id 730
    label "burza"
  ]
  node [
    id 731
    label "zbrodnia_wojenna"
  ]
  node [
    id 732
    label "fall"
  ]
  node [
    id 733
    label "przestawa&#263;"
  ]
  node [
    id 734
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 735
    label "shrink"
  ]
  node [
    id 736
    label "pluralizm"
  ]
  node [
    id 737
    label "partia"
  ]
  node [
    id 738
    label "ustr&#243;j"
  ]
  node [
    id 739
    label "demokratyzm"
  ]
  node [
    id 740
    label "stulecie"
  ]
  node [
    id 741
    label "kalendarz"
  ]
  node [
    id 742
    label "pora_roku"
  ]
  node [
    id 743
    label "cykl_astronomiczny"
  ]
  node [
    id 744
    label "p&#243;&#322;rocze"
  ]
  node [
    id 745
    label "kwarta&#322;"
  ]
  node [
    id 746
    label "kurs"
  ]
  node [
    id 747
    label "jubileusz"
  ]
  node [
    id 748
    label "martwy_sezon"
  ]
  node [
    id 749
    label "kategoria"
  ]
  node [
    id 750
    label "kategoria_gramatyczna"
  ]
  node [
    id 751
    label "kwadrat_magiczny"
  ]
  node [
    id 752
    label "wyra&#380;enie"
  ]
  node [
    id 753
    label "pierwiastek"
  ]
  node [
    id 754
    label "rozmiar"
  ]
  node [
    id 755
    label "number"
  ]
  node [
    id 756
    label "poj&#281;cie"
  ]
  node [
    id 757
    label "koniugacja"
  ]
  node [
    id 758
    label "clash"
  ]
  node [
    id 759
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 760
    label "relax"
  ]
  node [
    id 761
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 762
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 763
    label "slack"
  ]
  node [
    id 764
    label "zmniejsza&#263;_si&#281;"
  ]
  node [
    id 765
    label "dawno"
  ]
  node [
    id 766
    label "nisko"
  ]
  node [
    id 767
    label "nieobecnie"
  ]
  node [
    id 768
    label "daleki"
  ]
  node [
    id 769
    label "het"
  ]
  node [
    id 770
    label "wysoko"
  ]
  node [
    id 771
    label "du&#380;o"
  ]
  node [
    id 772
    label "znacznie"
  ]
  node [
    id 773
    label "g&#322;&#281;boko"
  ]
  node [
    id 774
    label "zostawa&#263;"
  ]
  node [
    id 775
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 776
    label "pozostawa&#263;"
  ]
  node [
    id 777
    label "adhere"
  ]
  node [
    id 778
    label "dupny"
  ]
  node [
    id 779
    label "wysoce"
  ]
  node [
    id 780
    label "wyj&#261;tkowy"
  ]
  node [
    id 781
    label "wybitny"
  ]
  node [
    id 782
    label "znaczny"
  ]
  node [
    id 783
    label "wa&#380;ny"
  ]
  node [
    id 784
    label "nieprzeci&#281;tny"
  ]
  node [
    id 785
    label "zranienie"
  ]
  node [
    id 786
    label "usuni&#281;cie"
  ]
  node [
    id 787
    label "trafienie"
  ]
  node [
    id 788
    label "oczyszczenie"
  ]
  node [
    id 789
    label "euroliga"
  ]
  node [
    id 790
    label "interliga"
  ]
  node [
    id 791
    label "runda"
  ]
  node [
    id 792
    label "discord"
  ]
  node [
    id 793
    label "zagrywka"
  ]
  node [
    id 794
    label "expunction"
  ]
  node [
    id 795
    label "rub"
  ]
  node [
    id 796
    label "rewan&#380;owy"
  ]
  node [
    id 797
    label "otarcie"
  ]
  node [
    id 798
    label "faza"
  ]
  node [
    id 799
    label "rozdrobnienie"
  ]
  node [
    id 800
    label "k&#322;&#243;tnia"
  ]
  node [
    id 801
    label "scramble"
  ]
  node [
    id 802
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 803
    label "perceive"
  ]
  node [
    id 804
    label "reagowa&#263;"
  ]
  node [
    id 805
    label "zmale&#263;"
  ]
  node [
    id 806
    label "spotka&#263;"
  ]
  node [
    id 807
    label "go_steady"
  ]
  node [
    id 808
    label "dostrzega&#263;"
  ]
  node [
    id 809
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 810
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 811
    label "os&#261;dza&#263;"
  ]
  node [
    id 812
    label "aprobowa&#263;"
  ]
  node [
    id 813
    label "punkt_widzenia"
  ]
  node [
    id 814
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 815
    label "wzrok"
  ]
  node [
    id 816
    label "postrzega&#263;"
  ]
  node [
    id 817
    label "notice"
  ]
  node [
    id 818
    label "Polsat"
  ]
  node [
    id 819
    label "paj&#281;czarz"
  ]
  node [
    id 820
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 821
    label "programowiec"
  ]
  node [
    id 822
    label "technologia"
  ]
  node [
    id 823
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 824
    label "Interwizja"
  ]
  node [
    id 825
    label "BBC"
  ]
  node [
    id 826
    label "ekran"
  ]
  node [
    id 827
    label "redakcja"
  ]
  node [
    id 828
    label "odbieranie"
  ]
  node [
    id 829
    label "odbiera&#263;"
  ]
  node [
    id 830
    label "odbiornik"
  ]
  node [
    id 831
    label "instytucja"
  ]
  node [
    id 832
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 833
    label "studio"
  ]
  node [
    id 834
    label "telekomunikacja"
  ]
  node [
    id 835
    label "muza"
  ]
  node [
    id 836
    label "nast&#281;pnie"
  ]
  node [
    id 837
    label "kt&#243;ry&#347;"
  ]
  node [
    id 838
    label "kolejno"
  ]
  node [
    id 839
    label "nastopny"
  ]
  node [
    id 840
    label "bezimienny"
  ]
  node [
    id 841
    label "bezimiennie"
  ]
  node [
    id 842
    label "anonimowo"
  ]
  node [
    id 843
    label "gamo&#324;"
  ]
  node [
    id 844
    label "istota_&#380;ywa"
  ]
  node [
    id 845
    label "dupa_wo&#322;owa"
  ]
  node [
    id 846
    label "pastwa"
  ]
  node [
    id 847
    label "zbi&#243;rka"
  ]
  node [
    id 848
    label "dar"
  ]
  node [
    id 849
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 850
    label "nastawienie"
  ]
  node [
    id 851
    label "crack"
  ]
  node [
    id 852
    label "zwierz&#281;"
  ]
  node [
    id 853
    label "ko&#347;cielny"
  ]
  node [
    id 854
    label "po&#347;miewisko"
  ]
  node [
    id 855
    label "terrorism"
  ]
  node [
    id 856
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 857
    label "zainstalowa&#263;"
  ]
  node [
    id 858
    label "rozpowszechni&#263;"
  ]
  node [
    id 859
    label "nada&#263;"
  ]
  node [
    id 860
    label "przekazior"
  ]
  node [
    id 861
    label "mass-media"
  ]
  node [
    id 862
    label "uzbrajanie"
  ]
  node [
    id 863
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 864
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 865
    label "medium"
  ]
  node [
    id 866
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 867
    label "czynnik"
  ]
  node [
    id 868
    label "rozpowszechnianie"
  ]
  node [
    id 869
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 870
    label "puchar"
  ]
  node [
    id 871
    label "beat"
  ]
  node [
    id 872
    label "conquest"
  ]
  node [
    id 873
    label "poradzenie_sobie"
  ]
  node [
    id 874
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 875
    label "zalicza&#263;"
  ]
  node [
    id 876
    label "opowiada&#263;"
  ]
  node [
    id 877
    label "render"
  ]
  node [
    id 878
    label "oddawa&#263;"
  ]
  node [
    id 879
    label "zostawia&#263;"
  ]
  node [
    id 880
    label "sk&#322;ada&#263;"
  ]
  node [
    id 881
    label "convey"
  ]
  node [
    id 882
    label "bequeath"
  ]
  node [
    id 883
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 884
    label "krzy&#380;"
  ]
  node [
    id 885
    label "paw"
  ]
  node [
    id 886
    label "rami&#281;"
  ]
  node [
    id 887
    label "gestykulowanie"
  ]
  node [
    id 888
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 889
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 890
    label "bramkarz"
  ]
  node [
    id 891
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 892
    label "handwriting"
  ]
  node [
    id 893
    label "hasta"
  ]
  node [
    id 894
    label "pi&#322;ka"
  ]
  node [
    id 895
    label "&#322;okie&#263;"
  ]
  node [
    id 896
    label "spos&#243;b"
  ]
  node [
    id 897
    label "obietnica"
  ]
  node [
    id 898
    label "przedrami&#281;"
  ]
  node [
    id 899
    label "chwyta&#263;"
  ]
  node [
    id 900
    label "r&#261;czyna"
  ]
  node [
    id 901
    label "wykroczenie"
  ]
  node [
    id 902
    label "kroki"
  ]
  node [
    id 903
    label "palec"
  ]
  node [
    id 904
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 905
    label "graba"
  ]
  node [
    id 906
    label "hand"
  ]
  node [
    id 907
    label "nadgarstek"
  ]
  node [
    id 908
    label "pomocnik"
  ]
  node [
    id 909
    label "k&#322;&#261;b"
  ]
  node [
    id 910
    label "hazena"
  ]
  node [
    id 911
    label "gestykulowa&#263;"
  ]
  node [
    id 912
    label "cmoknonsens"
  ]
  node [
    id 913
    label "d&#322;o&#324;"
  ]
  node [
    id 914
    label "chwytanie"
  ]
  node [
    id 915
    label "czerwona_kartka"
  ]
  node [
    id 916
    label "piwo"
  ]
  node [
    id 917
    label "ci&#261;gle"
  ]
  node [
    id 918
    label "kieliszek"
  ]
  node [
    id 919
    label "shot"
  ]
  node [
    id 920
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 921
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 922
    label "jednolicie"
  ]
  node [
    id 923
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 924
    label "w&#243;dka"
  ]
  node [
    id 925
    label "ten"
  ]
  node [
    id 926
    label "ujednolicenie"
  ]
  node [
    id 927
    label "jednakowy"
  ]
  node [
    id 928
    label "chwila"
  ]
  node [
    id 929
    label "uderzenie"
  ]
  node [
    id 930
    label "cios"
  ]
  node [
    id 931
    label "time"
  ]
  node [
    id 932
    label "obacza&#263;"
  ]
  node [
    id 933
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 934
    label "wynik"
  ]
  node [
    id 935
    label "r&#243;&#380;nienie"
  ]
  node [
    id 936
    label "kontrastowy"
  ]
  node [
    id 937
    label "erotyka"
  ]
  node [
    id 938
    label "podniecanie"
  ]
  node [
    id 939
    label "wzw&#243;d"
  ]
  node [
    id 940
    label "rozmna&#380;anie"
  ]
  node [
    id 941
    label "po&#380;&#261;danie"
  ]
  node [
    id 942
    label "imisja"
  ]
  node [
    id 943
    label "po&#380;ycie"
  ]
  node [
    id 944
    label "pozycja_misjonarska"
  ]
  node [
    id 945
    label "podnieci&#263;"
  ]
  node [
    id 946
    label "podnieca&#263;"
  ]
  node [
    id 947
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 948
    label "iloraz"
  ]
  node [
    id 949
    label "czynno&#347;&#263;"
  ]
  node [
    id 950
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 951
    label "gra_wst&#281;pna"
  ]
  node [
    id 952
    label "podej&#347;cie"
  ]
  node [
    id 953
    label "wyraz_skrajny"
  ]
  node [
    id 954
    label "numer"
  ]
  node [
    id 955
    label "ruch_frykcyjny"
  ]
  node [
    id 956
    label "baraszki"
  ]
  node [
    id 957
    label "powaga"
  ]
  node [
    id 958
    label "na_pieska"
  ]
  node [
    id 959
    label "z&#322;&#261;czenie"
  ]
  node [
    id 960
    label "relacja"
  ]
  node [
    id 961
    label "seks"
  ]
  node [
    id 962
    label "podniecenie"
  ]
  node [
    id 963
    label "kulturalny"
  ]
  node [
    id 964
    label "&#347;wiatowo"
  ]
  node [
    id 965
    label "generalny"
  ]
  node [
    id 966
    label "zachowa&#263;"
  ]
  node [
    id 967
    label "ensconce"
  ]
  node [
    id 968
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 969
    label "umie&#347;ci&#263;"
  ]
  node [
    id 970
    label "przytai&#263;"
  ]
  node [
    id 971
    label "niszczy&#263;"
  ]
  node [
    id 972
    label "obrabia&#263;"
  ]
  node [
    id 973
    label "wypuszcza&#263;"
  ]
  node [
    id 974
    label "przemieszcza&#263;"
  ]
  node [
    id 975
    label "zabiera&#263;"
  ]
  node [
    id 976
    label "prowadzi&#263;"
  ]
  node [
    id 977
    label "p&#281;dzi&#263;"
  ]
  node [
    id 978
    label "grind"
  ]
  node [
    id 979
    label "force"
  ]
  node [
    id 980
    label "przesuwa&#263;"
  ]
  node [
    id 981
    label "tacza&#263;"
  ]
  node [
    id 982
    label "&#380;y&#263;"
  ]
  node [
    id 983
    label "carry"
  ]
  node [
    id 984
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 985
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 986
    label "rusza&#263;"
  ]
  node [
    id 987
    label "daily"
  ]
  node [
    id 988
    label "cz&#281;sto"
  ]
  node [
    id 989
    label "codzienny"
  ]
  node [
    id 990
    label "prozaicznie"
  ]
  node [
    id 991
    label "stale"
  ]
  node [
    id 992
    label "regularnie"
  ]
  node [
    id 993
    label "pospolicie"
  ]
  node [
    id 994
    label "miasteczko"
  ]
  node [
    id 995
    label "streetball"
  ]
  node [
    id 996
    label "pierzeja"
  ]
  node [
    id 997
    label "pas_ruchu"
  ]
  node [
    id 998
    label "pas_rozdzielczy"
  ]
  node [
    id 999
    label "jezdnia"
  ]
  node [
    id 1000
    label "droga"
  ]
  node [
    id 1001
    label "korona_drogi"
  ]
  node [
    id 1002
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 1003
    label "chodnik"
  ]
  node [
    id 1004
    label "arteria"
  ]
  node [
    id 1005
    label "Broadway"
  ]
  node [
    id 1006
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 1007
    label "wysepka"
  ]
  node [
    id 1008
    label "autostrada"
  ]
  node [
    id 1009
    label "consolidate"
  ]
  node [
    id 1010
    label "dostawa&#263;"
  ]
  node [
    id 1011
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 1012
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 1013
    label "pozyskiwa&#263;"
  ]
  node [
    id 1014
    label "meet"
  ]
  node [
    id 1015
    label "congregate"
  ]
  node [
    id 1016
    label "poci&#261;ga&#263;"
  ]
  node [
    id 1017
    label "uk&#322;ada&#263;"
  ]
  node [
    id 1018
    label "umieszcza&#263;"
  ]
  node [
    id 1019
    label "wzbiera&#263;"
  ]
  node [
    id 1020
    label "gromadzi&#263;"
  ]
  node [
    id 1021
    label "powodowa&#263;"
  ]
  node [
    id 1022
    label "bra&#263;"
  ]
  node [
    id 1023
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 1024
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 1025
    label "zag&#322;ada"
  ]
  node [
    id 1026
    label "cywilnie"
  ]
  node [
    id 1027
    label "nieoficjalny"
  ]
  node [
    id 1028
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 1029
    label "styka&#263;_si&#281;"
  ]
  node [
    id 1030
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1031
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 1032
    label "lubi&#263;"
  ]
  node [
    id 1033
    label "wybiera&#263;"
  ]
  node [
    id 1034
    label "odtwarza&#263;"
  ]
  node [
    id 1035
    label "uk&#322;ad"
  ]
  node [
    id 1036
    label "fala_radiowa"
  ]
  node [
    id 1037
    label "spot"
  ]
  node [
    id 1038
    label "eliminator"
  ]
  node [
    id 1039
    label "radiola"
  ]
  node [
    id 1040
    label "dyskryminator"
  ]
  node [
    id 1041
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 1042
    label "stacja"
  ]
  node [
    id 1043
    label "radiolinia"
  ]
  node [
    id 1044
    label "radiofonia"
  ]
  node [
    id 1045
    label "dysleksja"
  ]
  node [
    id 1046
    label "umie&#263;"
  ]
  node [
    id 1047
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 1048
    label "przetwarza&#263;"
  ]
  node [
    id 1049
    label "read"
  ]
  node [
    id 1050
    label "poznawa&#263;"
  ]
  node [
    id 1051
    label "obserwowa&#263;"
  ]
  node [
    id 1052
    label "odczytywa&#263;"
  ]
  node [
    id 1053
    label "prasa"
  ]
  node [
    id 1054
    label "tytu&#322;"
  ]
  node [
    id 1055
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 1056
    label "czasopismo"
  ]
  node [
    id 1057
    label "matczysko"
  ]
  node [
    id 1058
    label "macierz"
  ]
  node [
    id 1059
    label "przodkini"
  ]
  node [
    id 1060
    label "Matka_Boska"
  ]
  node [
    id 1061
    label "macocha"
  ]
  node [
    id 1062
    label "matka_zast&#281;pcza"
  ]
  node [
    id 1063
    label "stara"
  ]
  node [
    id 1064
    label "rodzice"
  ]
  node [
    id 1065
    label "rodzic"
  ]
  node [
    id 1066
    label "atrakcyjny"
  ]
  node [
    id 1067
    label "oferta"
  ]
  node [
    id 1068
    label "adeptness"
  ]
  node [
    id 1069
    label "okazka"
  ]
  node [
    id 1070
    label "podw&#243;zka"
  ]
  node [
    id 1071
    label "autostop"
  ]
  node [
    id 1072
    label "przyswoi&#263;"
  ]
  node [
    id 1073
    label "feel"
  ]
  node [
    id 1074
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 1075
    label "teach"
  ]
  node [
    id 1076
    label "zrozumie&#263;"
  ]
  node [
    id 1077
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1078
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 1079
    label "topographic_point"
  ]
  node [
    id 1080
    label "experience"
  ]
  node [
    id 1081
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 1082
    label "visualize"
  ]
  node [
    id 1083
    label "argue"
  ]
  node [
    id 1084
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1085
    label "nak&#322;ania&#263;"
  ]
  node [
    id 1086
    label "byd&#322;o"
  ]
  node [
    id 1087
    label "zobo"
  ]
  node [
    id 1088
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 1089
    label "yakalo"
  ]
  node [
    id 1090
    label "dzo"
  ]
  node [
    id 1091
    label "w_chuj"
  ]
  node [
    id 1092
    label "cholerstwo"
  ]
  node [
    id 1093
    label "negatywno&#347;&#263;"
  ]
  node [
    id 1094
    label "ailment"
  ]
  node [
    id 1095
    label "z&#322;y"
  ]
  node [
    id 1096
    label "g&#243;wniany"
  ]
  node [
    id 1097
    label "beznadziejnie"
  ]
  node [
    id 1098
    label "kijowy"
  ]
  node [
    id 1099
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1100
    label "motyw"
  ]
  node [
    id 1101
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1102
    label "state"
  ]
  node [
    id 1103
    label "realia"
  ]
  node [
    id 1104
    label "Mickiewicz"
  ]
  node [
    id 1105
    label "szkolenie"
  ]
  node [
    id 1106
    label "przepisa&#263;"
  ]
  node [
    id 1107
    label "lesson"
  ]
  node [
    id 1108
    label "praktyka"
  ]
  node [
    id 1109
    label "metoda"
  ]
  node [
    id 1110
    label "niepokalanki"
  ]
  node [
    id 1111
    label "kara"
  ]
  node [
    id 1112
    label "zda&#263;"
  ]
  node [
    id 1113
    label "form"
  ]
  node [
    id 1114
    label "kwalifikacje"
  ]
  node [
    id 1115
    label "przepisanie"
  ]
  node [
    id 1116
    label "sztuba"
  ]
  node [
    id 1117
    label "wiedza"
  ]
  node [
    id 1118
    label "stopek"
  ]
  node [
    id 1119
    label "school"
  ]
  node [
    id 1120
    label "absolwent"
  ]
  node [
    id 1121
    label "urszulanki"
  ]
  node [
    id 1122
    label "gabinet"
  ]
  node [
    id 1123
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 1124
    label "ideologia"
  ]
  node [
    id 1125
    label "lekcja"
  ]
  node [
    id 1126
    label "muzyka"
  ]
  node [
    id 1127
    label "podr&#281;cznik"
  ]
  node [
    id 1128
    label "zdanie"
  ]
  node [
    id 1129
    label "siedziba"
  ]
  node [
    id 1130
    label "sekretariat"
  ]
  node [
    id 1131
    label "nauka"
  ]
  node [
    id 1132
    label "do&#347;wiadczenie"
  ]
  node [
    id 1133
    label "tablica"
  ]
  node [
    id 1134
    label "teren_szko&#322;y"
  ]
  node [
    id 1135
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 1136
    label "skolaryzacja"
  ]
  node [
    id 1137
    label "&#322;awa_szkolna"
  ]
  node [
    id 1138
    label "klasa"
  ]
  node [
    id 1139
    label "niezadowolenie"
  ]
  node [
    id 1140
    label "m&#243;wi&#263;"
  ]
  node [
    id 1141
    label "snivel"
  ]
  node [
    id 1142
    label "swarzy&#263;"
  ]
  node [
    id 1143
    label "wyrzeka&#263;"
  ]
  node [
    id 1144
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1145
    label "wykonywa&#263;"
  ]
  node [
    id 1146
    label "sposobi&#263;"
  ]
  node [
    id 1147
    label "arrange"
  ]
  node [
    id 1148
    label "pryczy&#263;"
  ]
  node [
    id 1149
    label "szkoli&#263;"
  ]
  node [
    id 1150
    label "usposabia&#263;"
  ]
  node [
    id 1151
    label "&#378;ra&#322;y"
  ]
  node [
    id 1152
    label "doletni"
  ]
  node [
    id 1153
    label "du&#380;y"
  ]
  node [
    id 1154
    label "wapniak"
  ]
  node [
    id 1155
    label "doro&#347;le"
  ]
  node [
    id 1156
    label "wydoro&#347;lenie"
  ]
  node [
    id 1157
    label "dojrzale"
  ]
  node [
    id 1158
    label "dojrza&#322;y"
  ]
  node [
    id 1159
    label "m&#261;dry"
  ]
  node [
    id 1160
    label "senior"
  ]
  node [
    id 1161
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1162
    label "doro&#347;lenie"
  ]
  node [
    id 1163
    label "energy"
  ]
  node [
    id 1164
    label "bycie"
  ]
  node [
    id 1165
    label "zegar_biologiczny"
  ]
  node [
    id 1166
    label "okres_noworodkowy"
  ]
  node [
    id 1167
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1168
    label "entity"
  ]
  node [
    id 1169
    label "prze&#380;ywanie"
  ]
  node [
    id 1170
    label "prze&#380;ycie"
  ]
  node [
    id 1171
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 1172
    label "wiek_matuzalemowy"
  ]
  node [
    id 1173
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 1174
    label "dzieci&#324;stwo"
  ]
  node [
    id 1175
    label "power"
  ]
  node [
    id 1176
    label "szwung"
  ]
  node [
    id 1177
    label "menopauza"
  ]
  node [
    id 1178
    label "umarcie"
  ]
  node [
    id 1179
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 1180
    label "life"
  ]
  node [
    id 1181
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 1182
    label "&#380;ywy"
  ]
  node [
    id 1183
    label "rozw&#243;j"
  ]
  node [
    id 1184
    label "po&#322;&#243;g"
  ]
  node [
    id 1185
    label "przebywanie"
  ]
  node [
    id 1186
    label "subsistence"
  ]
  node [
    id 1187
    label "koleje_losu"
  ]
  node [
    id 1188
    label "raj_utracony"
  ]
  node [
    id 1189
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1190
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 1191
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1192
    label "andropauza"
  ]
  node [
    id 1193
    label "do&#380;ywanie"
  ]
  node [
    id 1194
    label "niemowl&#281;ctwo"
  ]
  node [
    id 1195
    label "umieranie"
  ]
  node [
    id 1196
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 1197
    label "staro&#347;&#263;"
  ]
  node [
    id 1198
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 1199
    label "&#347;mier&#263;"
  ]
  node [
    id 1200
    label "czyj&#347;"
  ]
  node [
    id 1201
    label "sobieradzki"
  ]
  node [
    id 1202
    label "autonomicznie"
  ]
  node [
    id 1203
    label "osobny"
  ]
  node [
    id 1204
    label "indywidualny"
  ]
  node [
    id 1205
    label "niepodleg&#322;y"
  ]
  node [
    id 1206
    label "sw&#243;j"
  ]
  node [
    id 1207
    label "samodzielnie"
  ]
  node [
    id 1208
    label "odr&#281;bny"
  ]
  node [
    id 1209
    label "w&#322;asny"
  ]
  node [
    id 1210
    label "zmienia&#263;"
  ]
  node [
    id 1211
    label "rise"
  ]
  node [
    id 1212
    label "admit"
  ]
  node [
    id 1213
    label "drive"
  ]
  node [
    id 1214
    label "draw"
  ]
  node [
    id 1215
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 1216
    label "podnosi&#263;"
  ]
  node [
    id 1217
    label "resolution"
  ]
  node [
    id 1218
    label "zdecydowanie"
  ]
  node [
    id 1219
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 1220
    label "management"
  ]
  node [
    id 1221
    label "kreatywnie"
  ]
  node [
    id 1222
    label "zmienny"
  ]
  node [
    id 1223
    label "oryginalny"
  ]
  node [
    id 1224
    label "pomys&#322;owy"
  ]
  node [
    id 1225
    label "sprzyjaj&#261;cy"
  ]
  node [
    id 1226
    label "tw&#243;rczo"
  ]
  node [
    id 1227
    label "inspiruj&#261;cy"
  ]
  node [
    id 1228
    label "zinterpretowanie"
  ]
  node [
    id 1229
    label "pilnowanie"
  ]
  node [
    id 1230
    label "robienie"
  ]
  node [
    id 1231
    label "wnioskowanie"
  ]
  node [
    id 1232
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1233
    label "troskanie_si&#281;"
  ]
  node [
    id 1234
    label "skupianie_si&#281;"
  ]
  node [
    id 1235
    label "s&#261;dzenie"
  ]
  node [
    id 1236
    label "judgment"
  ]
  node [
    id 1237
    label "walczy&#263;"
  ]
  node [
    id 1238
    label "reflection"
  ]
  node [
    id 1239
    label "wzlecie&#263;"
  ]
  node [
    id 1240
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 1241
    label "proces_my&#347;lowy"
  ]
  node [
    id 1242
    label "wzlecenie"
  ]
  node [
    id 1243
    label "walczenie"
  ]
  node [
    id 1244
    label "treatment"
  ]
  node [
    id 1245
    label "domy&#347;lanie_si&#281;"
  ]
  node [
    id 1246
    label "proces"
  ]
  node [
    id 1247
    label "zwinnie"
  ]
  node [
    id 1248
    label "bezpiecznie"
  ]
  node [
    id 1249
    label "wiarygodnie"
  ]
  node [
    id 1250
    label "pewniej"
  ]
  node [
    id 1251
    label "pewny"
  ]
  node [
    id 1252
    label "mocno"
  ]
  node [
    id 1253
    label "najpewniej"
  ]
  node [
    id 1254
    label "argument"
  ]
  node [
    id 1255
    label "przyczyna"
  ]
  node [
    id 1256
    label "porcja"
  ]
  node [
    id 1257
    label "ca&#322;y"
  ]
  node [
    id 1258
    label "procesowicz"
  ]
  node [
    id 1259
    label "wypowied&#378;"
  ]
  node [
    id 1260
    label "pods&#261;dny"
  ]
  node [
    id 1261
    label "podejrzany"
  ]
  node [
    id 1262
    label "broni&#263;"
  ]
  node [
    id 1263
    label "bronienie"
  ]
  node [
    id 1264
    label "my&#347;l"
  ]
  node [
    id 1265
    label "konektyw"
  ]
  node [
    id 1266
    label "court"
  ]
  node [
    id 1267
    label "s&#261;downictwo"
  ]
  node [
    id 1268
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1269
    label "forum"
  ]
  node [
    id 1270
    label "post&#281;powanie"
  ]
  node [
    id 1271
    label "&#347;wiadek"
  ]
  node [
    id 1272
    label "antylogizm"
  ]
  node [
    id 1273
    label "strona"
  ]
  node [
    id 1274
    label "oskar&#380;yciel"
  ]
  node [
    id 1275
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1276
    label "biuro"
  ]
  node [
    id 1277
    label "asymilowa&#263;"
  ]
  node [
    id 1278
    label "dwun&#243;g"
  ]
  node [
    id 1279
    label "polifag"
  ]
  node [
    id 1280
    label "wz&#243;r"
  ]
  node [
    id 1281
    label "profanum"
  ]
  node [
    id 1282
    label "hominid"
  ]
  node [
    id 1283
    label "homo_sapiens"
  ]
  node [
    id 1284
    label "nasada"
  ]
  node [
    id 1285
    label "podw&#322;adny"
  ]
  node [
    id 1286
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1287
    label "portrecista"
  ]
  node [
    id 1288
    label "duch"
  ]
  node [
    id 1289
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1290
    label "asymilowanie"
  ]
  node [
    id 1291
    label "osoba"
  ]
  node [
    id 1292
    label "figura"
  ]
  node [
    id 1293
    label "Adam"
  ]
  node [
    id 1294
    label "antropochoria"
  ]
  node [
    id 1295
    label "communicate"
  ]
  node [
    id 1296
    label "zako&#324;czy&#263;"
  ]
  node [
    id 1297
    label "przesta&#263;"
  ]
  node [
    id 1298
    label "end"
  ]
  node [
    id 1299
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 1300
    label "summer"
  ]
  node [
    id 1301
    label "pomy&#347;lny"
  ]
  node [
    id 1302
    label "skuteczny"
  ]
  node [
    id 1303
    label "moralny"
  ]
  node [
    id 1304
    label "korzystny"
  ]
  node [
    id 1305
    label "odpowiedni"
  ]
  node [
    id 1306
    label "zwrot"
  ]
  node [
    id 1307
    label "dobrze"
  ]
  node [
    id 1308
    label "pozytywny"
  ]
  node [
    id 1309
    label "grzeczny"
  ]
  node [
    id 1310
    label "powitanie"
  ]
  node [
    id 1311
    label "mi&#322;y"
  ]
  node [
    id 1312
    label "dobroczynny"
  ]
  node [
    id 1313
    label "pos&#322;uszny"
  ]
  node [
    id 1314
    label "czw&#243;rka"
  ]
  node [
    id 1315
    label "spokojny"
  ]
  node [
    id 1316
    label "&#347;mieszny"
  ]
  node [
    id 1317
    label "drogi"
  ]
  node [
    id 1318
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1319
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 1320
    label "fabu&#322;a"
  ]
  node [
    id 1321
    label "przebiec"
  ]
  node [
    id 1322
    label "happening"
  ]
  node [
    id 1323
    label "przebiegni&#281;cie"
  ]
  node [
    id 1324
    label "event"
  ]
  node [
    id 1325
    label "charakter"
  ]
  node [
    id 1326
    label "kompletny"
  ]
  node [
    id 1327
    label "inaczej"
  ]
  node [
    id 1328
    label "r&#243;&#380;ny"
  ]
  node [
    id 1329
    label "inszy"
  ]
  node [
    id 1330
    label "osobno"
  ]
  node [
    id 1331
    label "model"
  ]
  node [
    id 1332
    label "sk&#322;ad"
  ]
  node [
    id 1333
    label "zachowanie"
  ]
  node [
    id 1334
    label "porz&#261;dek"
  ]
  node [
    id 1335
    label "Android"
  ]
  node [
    id 1336
    label "przyn&#281;ta"
  ]
  node [
    id 1337
    label "jednostka_geologiczna"
  ]
  node [
    id 1338
    label "podsystem"
  ]
  node [
    id 1339
    label "p&#322;&#243;d"
  ]
  node [
    id 1340
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1341
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1342
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1343
    label "j&#261;dro"
  ]
  node [
    id 1344
    label "eratem"
  ]
  node [
    id 1345
    label "ryba"
  ]
  node [
    id 1346
    label "pulpit"
  ]
  node [
    id 1347
    label "usenet"
  ]
  node [
    id 1348
    label "o&#347;"
  ]
  node [
    id 1349
    label "oprogramowanie"
  ]
  node [
    id 1350
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1351
    label "w&#281;dkarstwo"
  ]
  node [
    id 1352
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1353
    label "Leopard"
  ]
  node [
    id 1354
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1355
    label "systemik"
  ]
  node [
    id 1356
    label "rozprz&#261;c"
  ]
  node [
    id 1357
    label "cybernetyk"
  ]
  node [
    id 1358
    label "konstelacja"
  ]
  node [
    id 1359
    label "doktryna"
  ]
  node [
    id 1360
    label "net"
  ]
  node [
    id 1361
    label "zbi&#243;r"
  ]
  node [
    id 1362
    label "method"
  ]
  node [
    id 1363
    label "systemat"
  ]
  node [
    id 1364
    label "&#347;ledziowate"
  ]
  node [
    id 1365
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1366
    label "miech"
  ]
  node [
    id 1367
    label "kalendy"
  ]
  node [
    id 1368
    label "blisko"
  ]
  node [
    id 1369
    label "wsz&#281;dzie"
  ]
  node [
    id 1370
    label "stoisko"
  ]
  node [
    id 1371
    label "plac"
  ]
  node [
    id 1372
    label "emitowanie"
  ]
  node [
    id 1373
    label "targowica"
  ]
  node [
    id 1374
    label "emitowa&#263;"
  ]
  node [
    id 1375
    label "wprowadzanie"
  ]
  node [
    id 1376
    label "wprowadzi&#263;"
  ]
  node [
    id 1377
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 1378
    label "rynek_wt&#243;rny"
  ]
  node [
    id 1379
    label "wprowadzenie"
  ]
  node [
    id 1380
    label "kram"
  ]
  node [
    id 1381
    label "pojawienie_si&#281;"
  ]
  node [
    id 1382
    label "rynek_podstawowy"
  ]
  node [
    id 1383
    label "biznes"
  ]
  node [
    id 1384
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1385
    label "obiekt_handlowy"
  ]
  node [
    id 1386
    label "konsument"
  ]
  node [
    id 1387
    label "wytw&#243;rca"
  ]
  node [
    id 1388
    label "segment_rynku"
  ]
  node [
    id 1389
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 1390
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 1391
    label "kawa&#322;ek"
  ]
  node [
    id 1392
    label "p&#322;achetka_zwyczajna"
  ]
  node [
    id 1393
    label "rozja&#347;nia&#263;_si&#281;"
  ]
  node [
    id 1394
    label "dawn"
  ]
  node [
    id 1395
    label "krytyczny"
  ]
  node [
    id 1396
    label "prze&#347;miewczy"
  ]
  node [
    id 1397
    label "satyrycznie"
  ]
  node [
    id 1398
    label "photograph"
  ]
  node [
    id 1399
    label "kreska"
  ]
  node [
    id 1400
    label "ilustracja"
  ]
  node [
    id 1401
    label "teka"
  ]
  node [
    id 1402
    label "plastyka"
  ]
  node [
    id 1403
    label "shape"
  ]
  node [
    id 1404
    label "kszta&#322;t"
  ]
  node [
    id 1405
    label "grafika"
  ]
  node [
    id 1406
    label "picture"
  ]
  node [
    id 1407
    label "jako&#347;"
  ]
  node [
    id 1408
    label "charakterystyczny"
  ]
  node [
    id 1409
    label "ciekawy"
  ]
  node [
    id 1410
    label "jako_tako"
  ]
  node [
    id 1411
    label "dziwny"
  ]
  node [
    id 1412
    label "niez&#322;y"
  ]
  node [
    id 1413
    label "przyzwoity"
  ]
  node [
    id 1414
    label "Rembrandt"
  ]
  node [
    id 1415
    label "Roland_Topor"
  ]
  node [
    id 1416
    label "Mro&#380;ek"
  ]
  node [
    id 1417
    label "grafik"
  ]
  node [
    id 1418
    label "zna&#263;"
  ]
  node [
    id 1419
    label "troska&#263;_si&#281;"
  ]
  node [
    id 1420
    label "zachowywa&#263;"
  ]
  node [
    id 1421
    label "chowa&#263;"
  ]
  node [
    id 1422
    label "think"
  ]
  node [
    id 1423
    label "pilnowa&#263;"
  ]
  node [
    id 1424
    label "recall"
  ]
  node [
    id 1425
    label "echo"
  ]
  node [
    id 1426
    label "take_care"
  ]
  node [
    id 1427
    label "doznawa&#263;"
  ]
  node [
    id 1428
    label "znachodzi&#263;"
  ]
  node [
    id 1429
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 1430
    label "odzyskiwa&#263;"
  ]
  node [
    id 1431
    label "wykrywa&#263;"
  ]
  node [
    id 1432
    label "unwrap"
  ]
  node [
    id 1433
    label "detect"
  ]
  node [
    id 1434
    label "wymy&#347;la&#263;"
  ]
  node [
    id 1435
    label "miejsce"
  ]
  node [
    id 1436
    label "odcinek_ko&#322;a"
  ]
  node [
    id 1437
    label "whip"
  ]
  node [
    id 1438
    label "zabawa"
  ]
  node [
    id 1439
    label "gang"
  ]
  node [
    id 1440
    label "obr&#281;cz"
  ]
  node [
    id 1441
    label "pi"
  ]
  node [
    id 1442
    label "zwolnica"
  ]
  node [
    id 1443
    label "piasta"
  ]
  node [
    id 1444
    label "pojazd"
  ]
  node [
    id 1445
    label "&#322;amanie"
  ]
  node [
    id 1446
    label "okr&#261;g"
  ]
  node [
    id 1447
    label "figura_ograniczona"
  ]
  node [
    id 1448
    label "stowarzyszenie"
  ]
  node [
    id 1449
    label "figura_geometryczna"
  ]
  node [
    id 1450
    label "kolokwium"
  ]
  node [
    id 1451
    label "sphere"
  ]
  node [
    id 1452
    label "p&#243;&#322;kole"
  ]
  node [
    id 1453
    label "lap"
  ]
  node [
    id 1454
    label "podwozie"
  ]
  node [
    id 1455
    label "sejmik"
  ]
  node [
    id 1456
    label "&#322;ama&#263;"
  ]
  node [
    id 1457
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 1458
    label "&#322;atwy"
  ]
  node [
    id 1459
    label "prostowanie_si&#281;"
  ]
  node [
    id 1460
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 1461
    label "rozprostowanie"
  ]
  node [
    id 1462
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 1463
    label "prostoduszny"
  ]
  node [
    id 1464
    label "naturalny"
  ]
  node [
    id 1465
    label "naiwny"
  ]
  node [
    id 1466
    label "prostowanie"
  ]
  node [
    id 1467
    label "niepozorny"
  ]
  node [
    id 1468
    label "zwyk&#322;y"
  ]
  node [
    id 1469
    label "prosto"
  ]
  node [
    id 1470
    label "po_prostu"
  ]
  node [
    id 1471
    label "skromny"
  ]
  node [
    id 1472
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 1473
    label "centroprawica"
  ]
  node [
    id 1474
    label "core"
  ]
  node [
    id 1475
    label "Hollywood"
  ]
  node [
    id 1476
    label "centrolew"
  ]
  node [
    id 1477
    label "blok"
  ]
  node [
    id 1478
    label "sejm"
  ]
  node [
    id 1479
    label "punkt"
  ]
  node [
    id 1480
    label "o&#347;rodek"
  ]
  node [
    id 1481
    label "Aspazja"
  ]
  node [
    id 1482
    label "charakterystyka"
  ]
  node [
    id 1483
    label "poby&#263;"
  ]
  node [
    id 1484
    label "kompleksja"
  ]
  node [
    id 1485
    label "Osjan"
  ]
  node [
    id 1486
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1487
    label "formacja"
  ]
  node [
    id 1488
    label "point"
  ]
  node [
    id 1489
    label "zaistnie&#263;"
  ]
  node [
    id 1490
    label "go&#347;&#263;"
  ]
  node [
    id 1491
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1492
    label "trim"
  ]
  node [
    id 1493
    label "wygl&#261;d"
  ]
  node [
    id 1494
    label "przedstawienie"
  ]
  node [
    id 1495
    label "wytrzyma&#263;"
  ]
  node [
    id 1496
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1497
    label "kto&#347;"
  ]
  node [
    id 1498
    label "nienowoczesny"
  ]
  node [
    id 1499
    label "zachowawczy"
  ]
  node [
    id 1500
    label "zwyczajowy"
  ]
  node [
    id 1501
    label "modelowy"
  ]
  node [
    id 1502
    label "przyj&#281;ty"
  ]
  node [
    id 1503
    label "wierny"
  ]
  node [
    id 1504
    label "tradycyjnie"
  ]
  node [
    id 1505
    label "surowy"
  ]
  node [
    id 1506
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 1507
    label "zrzucenie"
  ]
  node [
    id 1508
    label "odziewek"
  ]
  node [
    id 1509
    label "garderoba"
  ]
  node [
    id 1510
    label "kr&#243;j"
  ]
  node [
    id 1511
    label "pasmanteria"
  ]
  node [
    id 1512
    label "pochodzi&#263;"
  ]
  node [
    id 1513
    label "ubranie_si&#281;"
  ]
  node [
    id 1514
    label "wyko&#324;czenie"
  ]
  node [
    id 1515
    label "znosi&#263;"
  ]
  node [
    id 1516
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1517
    label "znoszenie"
  ]
  node [
    id 1518
    label "w&#322;o&#380;enie"
  ]
  node [
    id 1519
    label "pochodzenie"
  ]
  node [
    id 1520
    label "nosi&#263;"
  ]
  node [
    id 1521
    label "odzie&#380;"
  ]
  node [
    id 1522
    label "gorset"
  ]
  node [
    id 1523
    label "zrzuci&#263;"
  ]
  node [
    id 1524
    label "zasada"
  ]
  node [
    id 1525
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1526
    label "profil"
  ]
  node [
    id 1527
    label "ucho"
  ]
  node [
    id 1528
    label "policzek"
  ]
  node [
    id 1529
    label "czo&#322;o"
  ]
  node [
    id 1530
    label "usta"
  ]
  node [
    id 1531
    label "micha"
  ]
  node [
    id 1532
    label "powieka"
  ]
  node [
    id 1533
    label "podbr&#243;dek"
  ]
  node [
    id 1534
    label "p&#243;&#322;profil"
  ]
  node [
    id 1535
    label "wyraz_twarzy"
  ]
  node [
    id 1536
    label "liczko"
  ]
  node [
    id 1537
    label "rys"
  ]
  node [
    id 1538
    label "zas&#322;ona"
  ]
  node [
    id 1539
    label "twarzyczka"
  ]
  node [
    id 1540
    label "nos"
  ]
  node [
    id 1541
    label "pysk"
  ]
  node [
    id 1542
    label "cera"
  ]
  node [
    id 1543
    label "p&#322;e&#263;"
  ]
  node [
    id 1544
    label "maskowato&#347;&#263;"
  ]
  node [
    id 1545
    label "przedstawiciel"
  ]
  node [
    id 1546
    label "brew"
  ]
  node [
    id 1547
    label "uj&#281;cie"
  ]
  node [
    id 1548
    label "prz&#243;d"
  ]
  node [
    id 1549
    label "oko"
  ]
  node [
    id 1550
    label "emocja"
  ]
  node [
    id 1551
    label "dezolacja"
  ]
  node [
    id 1552
    label "perturbation"
  ]
  node [
    id 1553
    label "l&#281;k"
  ]
  node [
    id 1554
    label "przera&#380;enie_si&#281;"
  ]
  node [
    id 1555
    label "przestraszenie"
  ]
  node [
    id 1556
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1557
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1558
    label "makrocefalia"
  ]
  node [
    id 1559
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1560
    label "m&#243;zg"
  ]
  node [
    id 1561
    label "kierownictwo"
  ]
  node [
    id 1562
    label "czaszka"
  ]
  node [
    id 1563
    label "dekiel"
  ]
  node [
    id 1564
    label "umys&#322;"
  ]
  node [
    id 1565
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1566
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1567
    label "sztuka"
  ]
  node [
    id 1568
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1569
    label "g&#243;ra"
  ]
  node [
    id 1570
    label "alkohol"
  ]
  node [
    id 1571
    label "ro&#347;lina"
  ]
  node [
    id 1572
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1573
    label "pryncypa&#322;"
  ]
  node [
    id 1574
    label "fryzura"
  ]
  node [
    id 1575
    label "noosfera"
  ]
  node [
    id 1576
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1577
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1578
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1579
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1580
    label "cz&#322;onek"
  ]
  node [
    id 1581
    label "cia&#322;o"
  ]
  node [
    id 1582
    label "obiekt"
  ]
  node [
    id 1583
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1584
    label "expressive_style"
  ]
  node [
    id 1585
    label "autografia"
  ]
  node [
    id 1586
    label "tekst"
  ]
  node [
    id 1587
    label "profesor"
  ]
  node [
    id 1588
    label "kszta&#322;ciciel"
  ]
  node [
    id 1589
    label "jegomo&#347;&#263;"
  ]
  node [
    id 1590
    label "pracodawca"
  ]
  node [
    id 1591
    label "rz&#261;dzenie"
  ]
  node [
    id 1592
    label "m&#261;&#380;"
  ]
  node [
    id 1593
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 1594
    label "ch&#322;opina"
  ]
  node [
    id 1595
    label "bratek"
  ]
  node [
    id 1596
    label "opiekun"
  ]
  node [
    id 1597
    label "preceptor"
  ]
  node [
    id 1598
    label "Midas"
  ]
  node [
    id 1599
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 1600
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 1601
    label "murza"
  ]
  node [
    id 1602
    label "ojciec"
  ]
  node [
    id 1603
    label "androlog"
  ]
  node [
    id 1604
    label "pupil"
  ]
  node [
    id 1605
    label "efendi"
  ]
  node [
    id 1606
    label "nabab"
  ]
  node [
    id 1607
    label "w&#322;odarz"
  ]
  node [
    id 1608
    label "szkolnik"
  ]
  node [
    id 1609
    label "pedagog"
  ]
  node [
    id 1610
    label "popularyzator"
  ]
  node [
    id 1611
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 1612
    label "Mieszko_I"
  ]
  node [
    id 1613
    label "bogaty"
  ]
  node [
    id 1614
    label "samiec"
  ]
  node [
    id 1615
    label "przyw&#243;dca"
  ]
  node [
    id 1616
    label "belfer"
  ]
  node [
    id 1617
    label "sprzeciw"
  ]
  node [
    id 1618
    label "sprawa"
  ]
  node [
    id 1619
    label "ambaras"
  ]
  node [
    id 1620
    label "problemat"
  ]
  node [
    id 1621
    label "pierepa&#322;ka"
  ]
  node [
    id 1622
    label "obstruction"
  ]
  node [
    id 1623
    label "problematyka"
  ]
  node [
    id 1624
    label "jajko_Kolumba"
  ]
  node [
    id 1625
    label "subiekcja"
  ]
  node [
    id 1626
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1627
    label "przekonuj&#261;cy"
  ]
  node [
    id 1628
    label "zdecydowany"
  ]
  node [
    id 1629
    label "niepodwa&#380;alny"
  ]
  node [
    id 1630
    label "zdrowy"
  ]
  node [
    id 1631
    label "silnie"
  ]
  node [
    id 1632
    label "&#380;ywotny"
  ]
  node [
    id 1633
    label "intensywny"
  ]
  node [
    id 1634
    label "krzepienie"
  ]
  node [
    id 1635
    label "meflochina"
  ]
  node [
    id 1636
    label "zajebisty"
  ]
  node [
    id 1637
    label "pokrzepienie"
  ]
  node [
    id 1638
    label "lock"
  ]
  node [
    id 1639
    label "absolut"
  ]
  node [
    id 1640
    label "set_about"
  ]
  node [
    id 1641
    label "boost"
  ]
  node [
    id 1642
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1643
    label "schorzenie"
  ]
  node [
    id 1644
    label "Marzec_'68"
  ]
  node [
    id 1645
    label "cykl_koniunkturalny"
  ]
  node [
    id 1646
    label "k&#322;opot"
  ]
  node [
    id 1647
    label "head"
  ]
  node [
    id 1648
    label "July"
  ]
  node [
    id 1649
    label "pogorszenie"
  ]
  node [
    id 1650
    label "zaprzepa&#347;ci&#263;"
  ]
  node [
    id 1651
    label "scribble"
  ]
  node [
    id 1652
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 1653
    label "usun&#261;&#263;"
  ]
  node [
    id 1654
    label "authorize"
  ]
  node [
    id 1655
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 1656
    label "trudzenie"
  ]
  node [
    id 1657
    label "przytoczenie_si&#281;"
  ]
  node [
    id 1658
    label "trudzi&#263;"
  ]
  node [
    id 1659
    label "wleczenie_si&#281;"
  ]
  node [
    id 1660
    label "przytaczanie_si&#281;"
  ]
  node [
    id 1661
    label "wlec_si&#281;"
  ]
  node [
    id 1662
    label "wjazd"
  ]
  node [
    id 1663
    label "konstrukcja"
  ]
  node [
    id 1664
    label "r&#243;w"
  ]
  node [
    id 1665
    label "kreacja"
  ]
  node [
    id 1666
    label "posesja"
  ]
  node [
    id 1667
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 1668
    label "organ"
  ]
  node [
    id 1669
    label "mechanika"
  ]
  node [
    id 1670
    label "praca"
  ]
  node [
    id 1671
    label "constitution"
  ]
  node [
    id 1672
    label "stabilnie"
  ]
  node [
    id 1673
    label "porz&#261;dny"
  ]
  node [
    id 1674
    label "trwa&#322;y"
  ]
  node [
    id 1675
    label "pole"
  ]
  node [
    id 1676
    label "szkolnictwo"
  ]
  node [
    id 1677
    label "przemys&#322;"
  ]
  node [
    id 1678
    label "gospodarka_wodna"
  ]
  node [
    id 1679
    label "fabryka"
  ]
  node [
    id 1680
    label "rolnictwo"
  ]
  node [
    id 1681
    label "gospodarka_le&#347;na"
  ]
  node [
    id 1682
    label "gospodarowa&#263;"
  ]
  node [
    id 1683
    label "sektor_prywatny"
  ]
  node [
    id 1684
    label "obronno&#347;&#263;"
  ]
  node [
    id 1685
    label "obora"
  ]
  node [
    id 1686
    label "mieszkalnictwo"
  ]
  node [
    id 1687
    label "sektor_publiczny"
  ]
  node [
    id 1688
    label "czerwona_strefa"
  ]
  node [
    id 1689
    label "stodo&#322;a"
  ]
  node [
    id 1690
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 1691
    label "produkowanie"
  ]
  node [
    id 1692
    label "gospodarowanie"
  ]
  node [
    id 1693
    label "agregat_ekonomiczny"
  ]
  node [
    id 1694
    label "sch&#322;adza&#263;"
  ]
  node [
    id 1695
    label "spichlerz"
  ]
  node [
    id 1696
    label "inwentarz"
  ]
  node [
    id 1697
    label "transport"
  ]
  node [
    id 1698
    label "sch&#322;odzenie"
  ]
  node [
    id 1699
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 1700
    label "wytw&#243;rnia"
  ]
  node [
    id 1701
    label "farmaceutyka"
  ]
  node [
    id 1702
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 1703
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 1704
    label "administracja"
  ]
  node [
    id 1705
    label "sch&#322;adzanie"
  ]
  node [
    id 1706
    label "bankowo&#347;&#263;"
  ]
  node [
    id 1707
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 1708
    label "regulacja_cen"
  ]
  node [
    id 1709
    label "hazard"
  ]
  node [
    id 1710
    label "ontologicznie"
  ]
  node [
    id 1711
    label "utrzymywanie"
  ]
  node [
    id 1712
    label "utrzymywa&#263;"
  ]
  node [
    id 1713
    label "wy&#380;ywienie"
  ]
  node [
    id 1714
    label "egzystencja"
  ]
  node [
    id 1715
    label "utrzymanie"
  ]
  node [
    id 1716
    label "utrzyma&#263;"
  ]
  node [
    id 1717
    label "subsystencja"
  ]
  node [
    id 1718
    label "Rwanda"
  ]
  node [
    id 1719
    label "Filipiny"
  ]
  node [
    id 1720
    label "Monako"
  ]
  node [
    id 1721
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1722
    label "Korea"
  ]
  node [
    id 1723
    label "Czarnog&#243;ra"
  ]
  node [
    id 1724
    label "Ghana"
  ]
  node [
    id 1725
    label "Malawi"
  ]
  node [
    id 1726
    label "Indonezja"
  ]
  node [
    id 1727
    label "Bu&#322;garia"
  ]
  node [
    id 1728
    label "Nauru"
  ]
  node [
    id 1729
    label "Kenia"
  ]
  node [
    id 1730
    label "Kambod&#380;a"
  ]
  node [
    id 1731
    label "Mali"
  ]
  node [
    id 1732
    label "Austria"
  ]
  node [
    id 1733
    label "interior"
  ]
  node [
    id 1734
    label "Armenia"
  ]
  node [
    id 1735
    label "Fid&#380;i"
  ]
  node [
    id 1736
    label "Tuwalu"
  ]
  node [
    id 1737
    label "Etiopia"
  ]
  node [
    id 1738
    label "Malezja"
  ]
  node [
    id 1739
    label "Malta"
  ]
  node [
    id 1740
    label "Tad&#380;ykistan"
  ]
  node [
    id 1741
    label "Grenada"
  ]
  node [
    id 1742
    label "Wehrlen"
  ]
  node [
    id 1743
    label "para"
  ]
  node [
    id 1744
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1745
    label "Rumunia"
  ]
  node [
    id 1746
    label "Maroko"
  ]
  node [
    id 1747
    label "Bhutan"
  ]
  node [
    id 1748
    label "S&#322;owacja"
  ]
  node [
    id 1749
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1750
    label "Seszele"
  ]
  node [
    id 1751
    label "Kuwejt"
  ]
  node [
    id 1752
    label "Arabia_Saudyjska"
  ]
  node [
    id 1753
    label "Kanada"
  ]
  node [
    id 1754
    label "Ekwador"
  ]
  node [
    id 1755
    label "Japonia"
  ]
  node [
    id 1756
    label "ziemia"
  ]
  node [
    id 1757
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1758
    label "Hiszpania"
  ]
  node [
    id 1759
    label "Wyspy_Marshalla"
  ]
  node [
    id 1760
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1761
    label "D&#380;ibuti"
  ]
  node [
    id 1762
    label "Botswana"
  ]
  node [
    id 1763
    label "Wietnam"
  ]
  node [
    id 1764
    label "Egipt"
  ]
  node [
    id 1765
    label "Burkina_Faso"
  ]
  node [
    id 1766
    label "Niemcy"
  ]
  node [
    id 1767
    label "Khitai"
  ]
  node [
    id 1768
    label "Macedonia"
  ]
  node [
    id 1769
    label "Albania"
  ]
  node [
    id 1770
    label "Madagaskar"
  ]
  node [
    id 1771
    label "Bahrajn"
  ]
  node [
    id 1772
    label "Jemen"
  ]
  node [
    id 1773
    label "Lesoto"
  ]
  node [
    id 1774
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1775
    label "Samoa"
  ]
  node [
    id 1776
    label "Andora"
  ]
  node [
    id 1777
    label "Chiny"
  ]
  node [
    id 1778
    label "Cypr"
  ]
  node [
    id 1779
    label "Wielka_Brytania"
  ]
  node [
    id 1780
    label "Ukraina"
  ]
  node [
    id 1781
    label "Paragwaj"
  ]
  node [
    id 1782
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1783
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1784
    label "Libia"
  ]
  node [
    id 1785
    label "Surinam"
  ]
  node [
    id 1786
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1787
    label "Nigeria"
  ]
  node [
    id 1788
    label "Australia"
  ]
  node [
    id 1789
    label "Honduras"
  ]
  node [
    id 1790
    label "Peru"
  ]
  node [
    id 1791
    label "USA"
  ]
  node [
    id 1792
    label "Bangladesz"
  ]
  node [
    id 1793
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1794
    label "Kazachstan"
  ]
  node [
    id 1795
    label "holoarktyka"
  ]
  node [
    id 1796
    label "Nepal"
  ]
  node [
    id 1797
    label "Sudan"
  ]
  node [
    id 1798
    label "Irak"
  ]
  node [
    id 1799
    label "San_Marino"
  ]
  node [
    id 1800
    label "Burundi"
  ]
  node [
    id 1801
    label "Dominikana"
  ]
  node [
    id 1802
    label "Komory"
  ]
  node [
    id 1803
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1804
    label "Gwatemala"
  ]
  node [
    id 1805
    label "Antarktis"
  ]
  node [
    id 1806
    label "Brunei"
  ]
  node [
    id 1807
    label "Iran"
  ]
  node [
    id 1808
    label "Zimbabwe"
  ]
  node [
    id 1809
    label "Namibia"
  ]
  node [
    id 1810
    label "Meksyk"
  ]
  node [
    id 1811
    label "Kamerun"
  ]
  node [
    id 1812
    label "Somalia"
  ]
  node [
    id 1813
    label "Angola"
  ]
  node [
    id 1814
    label "Gabon"
  ]
  node [
    id 1815
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1816
    label "Nowa_Zelandia"
  ]
  node [
    id 1817
    label "Mozambik"
  ]
  node [
    id 1818
    label "Tunezja"
  ]
  node [
    id 1819
    label "Tajwan"
  ]
  node [
    id 1820
    label "Liban"
  ]
  node [
    id 1821
    label "Jordania"
  ]
  node [
    id 1822
    label "Tonga"
  ]
  node [
    id 1823
    label "Czad"
  ]
  node [
    id 1824
    label "Gwinea"
  ]
  node [
    id 1825
    label "Liberia"
  ]
  node [
    id 1826
    label "Belize"
  ]
  node [
    id 1827
    label "Benin"
  ]
  node [
    id 1828
    label "&#321;otwa"
  ]
  node [
    id 1829
    label "Syria"
  ]
  node [
    id 1830
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1831
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1832
    label "Dominika"
  ]
  node [
    id 1833
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1834
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1835
    label "Hanower"
  ]
  node [
    id 1836
    label "Afganistan"
  ]
  node [
    id 1837
    label "W&#322;ochy"
  ]
  node [
    id 1838
    label "Kiribati"
  ]
  node [
    id 1839
    label "Szwajcaria"
  ]
  node [
    id 1840
    label "Chorwacja"
  ]
  node [
    id 1841
    label "Sahara_Zachodnia"
  ]
  node [
    id 1842
    label "Tajlandia"
  ]
  node [
    id 1843
    label "Salwador"
  ]
  node [
    id 1844
    label "Bahamy"
  ]
  node [
    id 1845
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1846
    label "S&#322;owenia"
  ]
  node [
    id 1847
    label "Gambia"
  ]
  node [
    id 1848
    label "Urugwaj"
  ]
  node [
    id 1849
    label "Zair"
  ]
  node [
    id 1850
    label "Erytrea"
  ]
  node [
    id 1851
    label "Rosja"
  ]
  node [
    id 1852
    label "Mauritius"
  ]
  node [
    id 1853
    label "Niger"
  ]
  node [
    id 1854
    label "Uganda"
  ]
  node [
    id 1855
    label "Turkmenistan"
  ]
  node [
    id 1856
    label "Turcja"
  ]
  node [
    id 1857
    label "Irlandia"
  ]
  node [
    id 1858
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1859
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1860
    label "Gwinea_Bissau"
  ]
  node [
    id 1861
    label "Belgia"
  ]
  node [
    id 1862
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1863
    label "Palau"
  ]
  node [
    id 1864
    label "Barbados"
  ]
  node [
    id 1865
    label "Wenezuela"
  ]
  node [
    id 1866
    label "W&#281;gry"
  ]
  node [
    id 1867
    label "Chile"
  ]
  node [
    id 1868
    label "Argentyna"
  ]
  node [
    id 1869
    label "Kolumbia"
  ]
  node [
    id 1870
    label "Sierra_Leone"
  ]
  node [
    id 1871
    label "Azerbejd&#380;an"
  ]
  node [
    id 1872
    label "Kongo"
  ]
  node [
    id 1873
    label "Pakistan"
  ]
  node [
    id 1874
    label "Liechtenstein"
  ]
  node [
    id 1875
    label "Nikaragua"
  ]
  node [
    id 1876
    label "Senegal"
  ]
  node [
    id 1877
    label "Indie"
  ]
  node [
    id 1878
    label "Suazi"
  ]
  node [
    id 1879
    label "Polska"
  ]
  node [
    id 1880
    label "Algieria"
  ]
  node [
    id 1881
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1882
    label "terytorium"
  ]
  node [
    id 1883
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1884
    label "Jamajka"
  ]
  node [
    id 1885
    label "Kostaryka"
  ]
  node [
    id 1886
    label "Timor_Wschodni"
  ]
  node [
    id 1887
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1888
    label "Kuba"
  ]
  node [
    id 1889
    label "Mauretania"
  ]
  node [
    id 1890
    label "Portoryko"
  ]
  node [
    id 1891
    label "Brazylia"
  ]
  node [
    id 1892
    label "Mo&#322;dawia"
  ]
  node [
    id 1893
    label "organizacja"
  ]
  node [
    id 1894
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1895
    label "Litwa"
  ]
  node [
    id 1896
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1897
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1898
    label "Izrael"
  ]
  node [
    id 1899
    label "Grecja"
  ]
  node [
    id 1900
    label "Kirgistan"
  ]
  node [
    id 1901
    label "Holandia"
  ]
  node [
    id 1902
    label "Sri_Lanka"
  ]
  node [
    id 1903
    label "Katar"
  ]
  node [
    id 1904
    label "Mikronezja"
  ]
  node [
    id 1905
    label "Laos"
  ]
  node [
    id 1906
    label "Mongolia"
  ]
  node [
    id 1907
    label "Malediwy"
  ]
  node [
    id 1908
    label "Zambia"
  ]
  node [
    id 1909
    label "Tanzania"
  ]
  node [
    id 1910
    label "Gujana"
  ]
  node [
    id 1911
    label "Uzbekistan"
  ]
  node [
    id 1912
    label "Panama"
  ]
  node [
    id 1913
    label "Czechy"
  ]
  node [
    id 1914
    label "Gruzja"
  ]
  node [
    id 1915
    label "Serbia"
  ]
  node [
    id 1916
    label "Francja"
  ]
  node [
    id 1917
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1918
    label "Togo"
  ]
  node [
    id 1919
    label "Estonia"
  ]
  node [
    id 1920
    label "Boliwia"
  ]
  node [
    id 1921
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1922
    label "Oman"
  ]
  node [
    id 1923
    label "Wyspy_Salomona"
  ]
  node [
    id 1924
    label "Haiti"
  ]
  node [
    id 1925
    label "Luksemburg"
  ]
  node [
    id 1926
    label "Portugalia"
  ]
  node [
    id 1927
    label "Birma"
  ]
  node [
    id 1928
    label "Rodezja"
  ]
  node [
    id 1929
    label "Samojedzi"
  ]
  node [
    id 1930
    label "nacja"
  ]
  node [
    id 1931
    label "Aztekowie"
  ]
  node [
    id 1932
    label "Irokezi"
  ]
  node [
    id 1933
    label "Buriaci"
  ]
  node [
    id 1934
    label "Komancze"
  ]
  node [
    id 1935
    label "ludno&#347;&#263;"
  ]
  node [
    id 1936
    label "lud"
  ]
  node [
    id 1937
    label "Siuksowie"
  ]
  node [
    id 1938
    label "Czejenowie"
  ]
  node [
    id 1939
    label "Wotiacy"
  ]
  node [
    id 1940
    label "Baszkirzy"
  ]
  node [
    id 1941
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 1942
    label "Mohikanie"
  ]
  node [
    id 1943
    label "Apacze"
  ]
  node [
    id 1944
    label "Syngalezi"
  ]
  node [
    id 1945
    label "przys&#322;owiowo"
  ]
  node [
    id 1946
    label "znajomy"
  ]
  node [
    id 1947
    label "deformacja"
  ]
  node [
    id 1948
    label "spadek"
  ]
  node [
    id 1949
    label "wymowa"
  ]
  node [
    id 1950
    label "zgniot"
  ]
  node [
    id 1951
    label "cause"
  ]
  node [
    id 1952
    label "introduce"
  ]
  node [
    id 1953
    label "begin"
  ]
  node [
    id 1954
    label "odj&#261;&#263;"
  ]
  node [
    id 1955
    label "post&#261;pi&#263;"
  ]
  node [
    id 1956
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 1957
    label "do"
  ]
  node [
    id 1958
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 1959
    label "wyj&#347;cie"
  ]
  node [
    id 1960
    label "spe&#322;nienie"
  ]
  node [
    id 1961
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 1962
    label "po&#322;o&#380;na"
  ]
  node [
    id 1963
    label "proces_fizjologiczny"
  ]
  node [
    id 1964
    label "przestanie"
  ]
  node [
    id 1965
    label "marc&#243;wka"
  ]
  node [
    id 1966
    label "uniewa&#380;nienie"
  ]
  node [
    id 1967
    label "pomys&#322;"
  ]
  node [
    id 1968
    label "birth"
  ]
  node [
    id 1969
    label "wymy&#347;lenie"
  ]
  node [
    id 1970
    label "szok_poporodowy"
  ]
  node [
    id 1971
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1972
    label "dula"
  ]
  node [
    id 1973
    label "bent"
  ]
  node [
    id 1974
    label "dynda&#263;"
  ]
  node [
    id 1975
    label "m&#281;czy&#263;"
  ]
  node [
    id 1976
    label "ci&#261;&#380;y&#263;"
  ]
  node [
    id 1977
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 1978
    label "idea&#322;"
  ]
  node [
    id 1979
    label "Eden"
  ]
  node [
    id 1980
    label "ogr&#243;d"
  ]
  node [
    id 1981
    label "Wyraj"
  ]
  node [
    id 1982
    label "niebo"
  ]
  node [
    id 1983
    label "Pola_Elizejskie"
  ]
  node [
    id 1984
    label "siekiera"
  ]
  node [
    id 1985
    label "ostrze"
  ]
  node [
    id 1986
    label "tomahawk"
  ]
  node [
    id 1987
    label "bro&#324;_obuchowa"
  ]
  node [
    id 1988
    label "dysponowanie"
  ]
  node [
    id 1989
    label "czasokres"
  ]
  node [
    id 1990
    label "trawienie"
  ]
  node [
    id 1991
    label "period"
  ]
  node [
    id 1992
    label "odczyt"
  ]
  node [
    id 1993
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1994
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1995
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1996
    label "poprzedzenie"
  ]
  node [
    id 1997
    label "dzieje"
  ]
  node [
    id 1998
    label "poprzedzi&#263;"
  ]
  node [
    id 1999
    label "przep&#322;ywanie"
  ]
  node [
    id 2000
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 2001
    label "odwlekanie_si&#281;"
  ]
  node [
    id 2002
    label "Zeitgeist"
  ]
  node [
    id 2003
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 2004
    label "okres_czasu"
  ]
  node [
    id 2005
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 2006
    label "schy&#322;ek"
  ]
  node [
    id 2007
    label "czwarty_wymiar"
  ]
  node [
    id 2008
    label "chronometria"
  ]
  node [
    id 2009
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 2010
    label "poprzedzanie"
  ]
  node [
    id 2011
    label "pogoda"
  ]
  node [
    id 2012
    label "zegar"
  ]
  node [
    id 2013
    label "poprzedza&#263;"
  ]
  node [
    id 2014
    label "trawi&#263;"
  ]
  node [
    id 2015
    label "time_period"
  ]
  node [
    id 2016
    label "rachuba_czasu"
  ]
  node [
    id 2017
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 2018
    label "czasoprzestrze&#324;"
  ]
  node [
    id 2019
    label "laba"
  ]
  node [
    id 2020
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 2021
    label "express"
  ]
  node [
    id 2022
    label "rzekn&#261;&#263;"
  ]
  node [
    id 2023
    label "okre&#347;li&#263;"
  ]
  node [
    id 2024
    label "wyrazi&#263;"
  ]
  node [
    id 2025
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 2026
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 2027
    label "discover"
  ]
  node [
    id 2028
    label "wydoby&#263;"
  ]
  node [
    id 2029
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 2030
    label "poda&#263;"
  ]
  node [
    id 2031
    label "truth"
  ]
  node [
    id 2032
    label "nieprawdziwy"
  ]
  node [
    id 2033
    label "za&#322;o&#380;enie"
  ]
  node [
    id 2034
    label "fraza"
  ]
  node [
    id 2035
    label "forma"
  ]
  node [
    id 2036
    label "melodia"
  ]
  node [
    id 2037
    label "zbacza&#263;"
  ]
  node [
    id 2038
    label "omawia&#263;"
  ]
  node [
    id 2039
    label "topik"
  ]
  node [
    id 2040
    label "wyraz_pochodny"
  ]
  node [
    id 2041
    label "om&#243;wi&#263;"
  ]
  node [
    id 2042
    label "omawianie"
  ]
  node [
    id 2043
    label "w&#261;tek"
  ]
  node [
    id 2044
    label "zboczenie"
  ]
  node [
    id 2045
    label "zbaczanie"
  ]
  node [
    id 2046
    label "tre&#347;&#263;"
  ]
  node [
    id 2047
    label "tematyka"
  ]
  node [
    id 2048
    label "istota"
  ]
  node [
    id 2049
    label "otoczka"
  ]
  node [
    id 2050
    label "zboczy&#263;"
  ]
  node [
    id 2051
    label "om&#243;wienie"
  ]
  node [
    id 2052
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 2053
    label "krajobraz"
  ]
  node [
    id 2054
    label "przywidzenie"
  ]
  node [
    id 2055
    label "boski"
  ]
  node [
    id 2056
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 2057
    label "presence"
  ]
  node [
    id 2058
    label "work"
  ]
  node [
    id 2059
    label "reakcja_chemiczna"
  ]
  node [
    id 2060
    label "function"
  ]
  node [
    id 2061
    label "commit"
  ]
  node [
    id 2062
    label "bangla&#263;"
  ]
  node [
    id 2063
    label "determine"
  ]
  node [
    id 2064
    label "tryb"
  ]
  node [
    id 2065
    label "dziama&#263;"
  ]
  node [
    id 2066
    label "pomieszczenie"
  ]
  node [
    id 2067
    label "klasztor"
  ]
  node [
    id 2068
    label "stosowny"
  ]
  node [
    id 2069
    label "nale&#380;ycie"
  ]
  node [
    id 2070
    label "charakterystycznie"
  ]
  node [
    id 2071
    label "prawdziwie"
  ]
  node [
    id 2072
    label "nale&#380;nie"
  ]
  node [
    id 2073
    label "dostarczy&#263;"
  ]
  node [
    id 2074
    label "odda&#263;"
  ]
  node [
    id 2075
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 2076
    label "powi&#261;za&#263;"
  ]
  node [
    id 2077
    label "tax_return"
  ]
  node [
    id 2078
    label "dozna&#263;"
  ]
  node [
    id 2079
    label "spolny"
  ]
  node [
    id 2080
    label "sp&#243;lny"
  ]
  node [
    id 2081
    label "wsp&#243;lnie"
  ]
  node [
    id 2082
    label "uwsp&#243;lnianie"
  ]
  node [
    id 2083
    label "uwsp&#243;lnienie"
  ]
  node [
    id 2084
    label "stary"
  ]
  node [
    id 2085
    label "d&#322;ugi"
  ]
  node [
    id 2086
    label "wieloletni"
  ]
  node [
    id 2087
    label "success"
  ]
  node [
    id 2088
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 2089
    label "kobieta_sukcesu"
  ]
  node [
    id 2090
    label "sekwencja"
  ]
  node [
    id 2091
    label "edycja"
  ]
  node [
    id 2092
    label "przebieg"
  ]
  node [
    id 2093
    label "okres"
  ]
  node [
    id 2094
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 2095
    label "cycle"
  ]
  node [
    id 2096
    label "owulacja"
  ]
  node [
    id 2097
    label "miesi&#261;czka"
  ]
  node [
    id 2098
    label "set"
  ]
  node [
    id 2099
    label "Ptolemeusz"
  ]
  node [
    id 2100
    label "naukowiec"
  ]
  node [
    id 2101
    label "report"
  ]
  node [
    id 2102
    label "neografia"
  ]
  node [
    id 2103
    label "papirologia"
  ]
  node [
    id 2104
    label "historia_gospodarcza"
  ]
  node [
    id 2105
    label "hista"
  ]
  node [
    id 2106
    label "nauka_humanistyczna"
  ]
  node [
    id 2107
    label "filigranistyka"
  ]
  node [
    id 2108
    label "dyplomatyka"
  ]
  node [
    id 2109
    label "annalistyka"
  ]
  node [
    id 2110
    label "historyka"
  ]
  node [
    id 2111
    label "heraldyka"
  ]
  node [
    id 2112
    label "muzealnictwo"
  ]
  node [
    id 2113
    label "varsavianistyka"
  ]
  node [
    id 2114
    label "mediewistyka"
  ]
  node [
    id 2115
    label "prezentyzm"
  ]
  node [
    id 2116
    label "paleografia"
  ]
  node [
    id 2117
    label "genealogia"
  ]
  node [
    id 2118
    label "prozopografia"
  ]
  node [
    id 2119
    label "nautologia"
  ]
  node [
    id 2120
    label "epoka"
  ]
  node [
    id 2121
    label "numizmatyka"
  ]
  node [
    id 2122
    label "ruralistyka"
  ]
  node [
    id 2123
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 2124
    label "epigrafika"
  ]
  node [
    id 2125
    label "historiografia"
  ]
  node [
    id 2126
    label "bizantynistyka"
  ]
  node [
    id 2127
    label "weksylologia"
  ]
  node [
    id 2128
    label "kierunek"
  ]
  node [
    id 2129
    label "ikonografia"
  ]
  node [
    id 2130
    label "chronologia"
  ]
  node [
    id 2131
    label "archiwistyka"
  ]
  node [
    id 2132
    label "sfragistyka"
  ]
  node [
    id 2133
    label "zabytkoznawstwo"
  ]
  node [
    id 2134
    label "historia_sztuki"
  ]
  node [
    id 2135
    label "wstyd"
  ]
  node [
    id 2136
    label "mortus"
  ]
  node [
    id 2137
    label "zbiednienie"
  ]
  node [
    id 2138
    label "pojazd_niemechaniczny"
  ]
  node [
    id 2139
    label "polskie"
  ]
  node [
    id 2140
    label "czerwony"
  ]
  node [
    id 2141
    label "zeszyt"
  ]
  node [
    id 2142
    label "narkotykowy"
  ]
  node [
    id 2143
    label "p&#243;&#378;no"
  ]
  node [
    id 2144
    label "europ"
  ]
  node [
    id 2145
    label "&#347;rodkowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 59
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 46
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 46
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 365
  ]
  edge [
    source 16
    target 366
  ]
  edge [
    source 16
    target 367
  ]
  edge [
    source 16
    target 368
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 216
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 369
  ]
  edge [
    source 17
    target 370
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 371
  ]
  edge [
    source 17
    target 372
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 37
  ]
  edge [
    source 18
    target 38
  ]
  edge [
    source 18
    target 306
  ]
  edge [
    source 18
    target 373
  ]
  edge [
    source 18
    target 374
  ]
  edge [
    source 18
    target 375
  ]
  edge [
    source 18
    target 376
  ]
  edge [
    source 18
    target 377
  ]
  edge [
    source 18
    target 329
  ]
  edge [
    source 18
    target 378
  ]
  edge [
    source 18
    target 379
  ]
  edge [
    source 18
    target 380
  ]
  edge [
    source 18
    target 224
  ]
  edge [
    source 18
    target 381
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 382
  ]
  edge [
    source 19
    target 285
  ]
  edge [
    source 19
    target 383
  ]
  edge [
    source 19
    target 384
  ]
  edge [
    source 19
    target 385
  ]
  edge [
    source 19
    target 386
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 387
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 50
  ]
  edge [
    source 20
    target 59
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 67
  ]
  edge [
    source 21
    target 388
  ]
  edge [
    source 21
    target 389
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 390
  ]
  edge [
    source 22
    target 391
  ]
  edge [
    source 22
    target 392
  ]
  edge [
    source 22
    target 393
  ]
  edge [
    source 22
    target 394
  ]
  edge [
    source 22
    target 395
  ]
  edge [
    source 22
    target 396
  ]
  edge [
    source 22
    target 397
  ]
  edge [
    source 22
    target 398
  ]
  edge [
    source 22
    target 399
  ]
  edge [
    source 22
    target 400
  ]
  edge [
    source 22
    target 401
  ]
  edge [
    source 22
    target 34
  ]
  edge [
    source 22
    target 402
  ]
  edge [
    source 22
    target 403
  ]
  edge [
    source 22
    target 404
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 32
  ]
  edge [
    source 24
    target 237
  ]
  edge [
    source 24
    target 405
  ]
  edge [
    source 24
    target 406
  ]
  edge [
    source 24
    target 407
  ]
  edge [
    source 24
    target 408
  ]
  edge [
    source 24
    target 409
  ]
  edge [
    source 24
    target 410
  ]
  edge [
    source 24
    target 411
  ]
  edge [
    source 24
    target 252
  ]
  edge [
    source 24
    target 412
  ]
  edge [
    source 24
    target 413
  ]
  edge [
    source 24
    target 414
  ]
  edge [
    source 24
    target 415
  ]
  edge [
    source 24
    target 379
  ]
  edge [
    source 24
    target 416
  ]
  edge [
    source 24
    target 417
  ]
  edge [
    source 24
    target 418
  ]
  edge [
    source 24
    target 39
  ]
  edge [
    source 24
    target 98
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 419
  ]
  edge [
    source 25
    target 420
  ]
  edge [
    source 25
    target 421
  ]
  edge [
    source 25
    target 422
  ]
  edge [
    source 25
    target 423
  ]
  edge [
    source 25
    target 424
  ]
  edge [
    source 25
    target 425
  ]
  edge [
    source 25
    target 426
  ]
  edge [
    source 25
    target 427
  ]
  edge [
    source 25
    target 428
  ]
  edge [
    source 25
    target 429
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 430
  ]
  edge [
    source 26
    target 431
  ]
  edge [
    source 26
    target 432
  ]
  edge [
    source 26
    target 433
  ]
  edge [
    source 26
    target 434
  ]
  edge [
    source 26
    target 435
  ]
  edge [
    source 26
    target 436
  ]
  edge [
    source 26
    target 437
  ]
  edge [
    source 26
    target 438
  ]
  edge [
    source 26
    target 439
  ]
  edge [
    source 26
    target 440
  ]
  edge [
    source 26
    target 441
  ]
  edge [
    source 26
    target 442
  ]
  edge [
    source 26
    target 443
  ]
  edge [
    source 26
    target 444
  ]
  edge [
    source 26
    target 445
  ]
  edge [
    source 26
    target 446
  ]
  edge [
    source 26
    target 447
  ]
  edge [
    source 26
    target 448
  ]
  edge [
    source 26
    target 449
  ]
  edge [
    source 26
    target 450
  ]
  edge [
    source 26
    target 451
  ]
  edge [
    source 26
    target 452
  ]
  edge [
    source 26
    target 453
  ]
  edge [
    source 26
    target 454
  ]
  edge [
    source 26
    target 455
  ]
  edge [
    source 26
    target 456
  ]
  edge [
    source 26
    target 457
  ]
  edge [
    source 26
    target 458
  ]
  edge [
    source 26
    target 459
  ]
  edge [
    source 26
    target 460
  ]
  edge [
    source 26
    target 461
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 26
    target 37
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 340
  ]
  edge [
    source 27
    target 462
  ]
  edge [
    source 27
    target 463
  ]
  edge [
    source 27
    target 464
  ]
  edge [
    source 27
    target 465
  ]
  edge [
    source 27
    target 466
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 467
  ]
  edge [
    source 28
    target 175
  ]
  edge [
    source 28
    target 61
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 28
    target 37
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 145
  ]
  edge [
    source 29
    target 130
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 468
  ]
  edge [
    source 30
    target 469
  ]
  edge [
    source 30
    target 470
  ]
  edge [
    source 30
    target 471
  ]
  edge [
    source 30
    target 472
  ]
  edge [
    source 30
    target 473
  ]
  edge [
    source 30
    target 474
  ]
  edge [
    source 30
    target 475
  ]
  edge [
    source 30
    target 476
  ]
  edge [
    source 30
    target 477
  ]
  edge [
    source 30
    target 478
  ]
  edge [
    source 30
    target 479
  ]
  edge [
    source 30
    target 480
  ]
  edge [
    source 30
    target 481
  ]
  edge [
    source 30
    target 482
  ]
  edge [
    source 30
    target 483
  ]
  edge [
    source 30
    target 484
  ]
  edge [
    source 30
    target 485
  ]
  edge [
    source 30
    target 486
  ]
  edge [
    source 30
    target 487
  ]
  edge [
    source 30
    target 488
  ]
  edge [
    source 30
    target 489
  ]
  edge [
    source 30
    target 490
  ]
  edge [
    source 30
    target 491
  ]
  edge [
    source 30
    target 492
  ]
  edge [
    source 30
    target 493
  ]
  edge [
    source 30
    target 462
  ]
  edge [
    source 30
    target 494
  ]
  edge [
    source 30
    target 220
  ]
  edge [
    source 30
    target 495
  ]
  edge [
    source 30
    target 496
  ]
  edge [
    source 30
    target 497
  ]
  edge [
    source 30
    target 498
  ]
  edge [
    source 30
    target 499
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 105
  ]
  edge [
    source 31
    target 106
  ]
  edge [
    source 31
    target 500
  ]
  edge [
    source 31
    target 501
  ]
  edge [
    source 31
    target 502
  ]
  edge [
    source 31
    target 503
  ]
  edge [
    source 31
    target 504
  ]
  edge [
    source 31
    target 505
  ]
  edge [
    source 31
    target 506
  ]
  edge [
    source 31
    target 507
  ]
  edge [
    source 31
    target 336
  ]
  edge [
    source 31
    target 508
  ]
  edge [
    source 31
    target 509
  ]
  edge [
    source 31
    target 510
  ]
  edge [
    source 31
    target 511
  ]
  edge [
    source 31
    target 61
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 31
    target 80
  ]
  edge [
    source 33
    target 512
  ]
  edge [
    source 33
    target 513
  ]
  edge [
    source 33
    target 514
  ]
  edge [
    source 33
    target 515
  ]
  edge [
    source 33
    target 516
  ]
  edge [
    source 33
    target 67
  ]
  edge [
    source 33
    target 517
  ]
  edge [
    source 33
    target 361
  ]
  edge [
    source 33
    target 137
  ]
  edge [
    source 34
    target 46
  ]
  edge [
    source 34
    target 49
  ]
  edge [
    source 34
    target 518
  ]
  edge [
    source 34
    target 519
  ]
  edge [
    source 34
    target 260
  ]
  edge [
    source 34
    target 397
  ]
  edge [
    source 34
    target 520
  ]
  edge [
    source 34
    target 521
  ]
  edge [
    source 34
    target 522
  ]
  edge [
    source 34
    target 523
  ]
  edge [
    source 34
    target 524
  ]
  edge [
    source 34
    target 525
  ]
  edge [
    source 34
    target 526
  ]
  edge [
    source 34
    target 527
  ]
  edge [
    source 34
    target 528
  ]
  edge [
    source 34
    target 403
  ]
  edge [
    source 34
    target 130
  ]
  edge [
    source 34
    target 529
  ]
  edge [
    source 34
    target 530
  ]
  edge [
    source 34
    target 531
  ]
  edge [
    source 34
    target 532
  ]
  edge [
    source 34
    target 533
  ]
  edge [
    source 34
    target 43
  ]
  edge [
    source 34
    target 534
  ]
  edge [
    source 34
    target 503
  ]
  edge [
    source 34
    target 535
  ]
  edge [
    source 34
    target 536
  ]
  edge [
    source 34
    target 537
  ]
  edge [
    source 34
    target 538
  ]
  edge [
    source 34
    target 539
  ]
  edge [
    source 34
    target 540
  ]
  edge [
    source 34
    target 541
  ]
  edge [
    source 34
    target 542
  ]
  edge [
    source 34
    target 543
  ]
  edge [
    source 34
    target 544
  ]
  edge [
    source 34
    target 545
  ]
  edge [
    source 34
    target 546
  ]
  edge [
    source 34
    target 547
  ]
  edge [
    source 34
    target 548
  ]
  edge [
    source 34
    target 549
  ]
  edge [
    source 34
    target 550
  ]
  edge [
    source 34
    target 551
  ]
  edge [
    source 34
    target 552
  ]
  edge [
    source 34
    target 553
  ]
  edge [
    source 34
    target 554
  ]
  edge [
    source 34
    target 555
  ]
  edge [
    source 34
    target 556
  ]
  edge [
    source 34
    target 557
  ]
  edge [
    source 34
    target 55
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 2139
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 558
  ]
  edge [
    source 35
    target 126
  ]
  edge [
    source 35
    target 559
  ]
  edge [
    source 35
    target 560
  ]
  edge [
    source 35
    target 561
  ]
  edge [
    source 35
    target 562
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 563
  ]
  edge [
    source 36
    target 462
  ]
  edge [
    source 36
    target 564
  ]
  edge [
    source 36
    target 565
  ]
  edge [
    source 36
    target 566
  ]
  edge [
    source 36
    target 567
  ]
  edge [
    source 36
    target 568
  ]
  edge [
    source 36
    target 569
  ]
  edge [
    source 36
    target 570
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 571
  ]
  edge [
    source 38
    target 572
  ]
  edge [
    source 38
    target 573
  ]
  edge [
    source 38
    target 574
  ]
  edge [
    source 38
    target 575
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 576
  ]
  edge [
    source 39
    target 577
  ]
  edge [
    source 39
    target 578
  ]
  edge [
    source 39
    target 579
  ]
  edge [
    source 39
    target 580
  ]
  edge [
    source 39
    target 581
  ]
  edge [
    source 39
    target 582
  ]
  edge [
    source 39
    target 583
  ]
  edge [
    source 39
    target 584
  ]
  edge [
    source 39
    target 585
  ]
  edge [
    source 39
    target 193
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 586
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 50
  ]
  edge [
    source 41
    target 51
  ]
  edge [
    source 41
    target 226
  ]
  edge [
    source 41
    target 225
  ]
  edge [
    source 41
    target 587
  ]
  edge [
    source 41
    target 588
  ]
  edge [
    source 41
    target 462
  ]
  edge [
    source 41
    target 488
  ]
  edge [
    source 41
    target 589
  ]
  edge [
    source 41
    target 590
  ]
  edge [
    source 41
    target 62
  ]
  edge [
    source 41
    target 215
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 591
  ]
  edge [
    source 42
    target 164
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 592
  ]
  edge [
    source 43
    target 593
  ]
  edge [
    source 43
    target 594
  ]
  edge [
    source 43
    target 595
  ]
  edge [
    source 43
    target 296
  ]
  edge [
    source 43
    target 596
  ]
  edge [
    source 43
    target 597
  ]
  edge [
    source 43
    target 598
  ]
  edge [
    source 43
    target 599
  ]
  edge [
    source 43
    target 600
  ]
  edge [
    source 43
    target 601
  ]
  edge [
    source 43
    target 212
  ]
  edge [
    source 43
    target 602
  ]
  edge [
    source 43
    target 556
  ]
  edge [
    source 43
    target 224
  ]
  edge [
    source 43
    target 603
  ]
  edge [
    source 43
    target 55
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 604
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 44
    target 166
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 228
  ]
  edge [
    source 45
    target 49
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 85
  ]
  edge [
    source 46
    target 86
  ]
  edge [
    source 46
    target 89
  ]
  edge [
    source 46
    target 90
  ]
  edge [
    source 46
    target 93
  ]
  edge [
    source 46
    target 105
  ]
  edge [
    source 46
    target 140
  ]
  edge [
    source 46
    target 60
  ]
  edge [
    source 46
    target 87
  ]
  edge [
    source 46
    target 163
  ]
  edge [
    source 46
    target 172
  ]
  edge [
    source 46
    target 180
  ]
  edge [
    source 46
    target 181
  ]
  edge [
    source 46
    target 605
  ]
  edge [
    source 46
    target 71
  ]
  edge [
    source 46
    target 606
  ]
  edge [
    source 46
    target 607
  ]
  edge [
    source 46
    target 571
  ]
  edge [
    source 46
    target 608
  ]
  edge [
    source 46
    target 609
  ]
  edge [
    source 46
    target 610
  ]
  edge [
    source 46
    target 611
  ]
  edge [
    source 46
    target 612
  ]
  edge [
    source 46
    target 613
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 46
    target 213
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 306
  ]
  edge [
    source 47
    target 614
  ]
  edge [
    source 47
    target 375
  ]
  edge [
    source 47
    target 378
  ]
  edge [
    source 47
    target 379
  ]
  edge [
    source 47
    target 380
  ]
  edge [
    source 47
    target 615
  ]
  edge [
    source 47
    target 616
  ]
  edge [
    source 47
    target 325
  ]
  edge [
    source 47
    target 224
  ]
  edge [
    source 48
    target 617
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 229
  ]
  edge [
    source 49
    target 618
  ]
  edge [
    source 49
    target 619
  ]
  edge [
    source 49
    target 620
  ]
  edge [
    source 50
    target 59
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 621
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 363
  ]
  edge [
    source 53
    target 622
  ]
  edge [
    source 53
    target 623
  ]
  edge [
    source 53
    target 624
  ]
  edge [
    source 53
    target 192
  ]
  edge [
    source 53
    target 625
  ]
  edge [
    source 53
    target 626
  ]
  edge [
    source 53
    target 627
  ]
  edge [
    source 53
    target 628
  ]
  edge [
    source 53
    target 629
  ]
  edge [
    source 53
    target 630
  ]
  edge [
    source 53
    target 631
  ]
  edge [
    source 53
    target 632
  ]
  edge [
    source 53
    target 633
  ]
  edge [
    source 53
    target 65
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 634
  ]
  edge [
    source 54
    target 635
  ]
  edge [
    source 54
    target 636
  ]
  edge [
    source 54
    target 143
  ]
  edge [
    source 54
    target 199
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 518
  ]
  edge [
    source 55
    target 637
  ]
  edge [
    source 55
    target 397
  ]
  edge [
    source 55
    target 638
  ]
  edge [
    source 55
    target 521
  ]
  edge [
    source 55
    target 524
  ]
  edge [
    source 55
    target 639
  ]
  edge [
    source 55
    target 640
  ]
  edge [
    source 55
    target 527
  ]
  edge [
    source 55
    target 526
  ]
  edge [
    source 55
    target 641
  ]
  edge [
    source 55
    target 528
  ]
  edge [
    source 55
    target 530
  ]
  edge [
    source 55
    target 403
  ]
  edge [
    source 55
    target 529
  ]
  edge [
    source 55
    target 642
  ]
  edge [
    source 55
    target 531
  ]
  edge [
    source 55
    target 533
  ]
  edge [
    source 55
    target 532
  ]
  edge [
    source 55
    target 643
  ]
  edge [
    source 55
    target 644
  ]
  edge [
    source 55
    target 503
  ]
  edge [
    source 55
    target 535
  ]
  edge [
    source 55
    target 536
  ]
  edge [
    source 55
    target 537
  ]
  edge [
    source 55
    target 645
  ]
  edge [
    source 55
    target 646
  ]
  edge [
    source 55
    target 540
  ]
  edge [
    source 55
    target 426
  ]
  edge [
    source 55
    target 542
  ]
  edge [
    source 55
    target 539
  ]
  edge [
    source 55
    target 538
  ]
  edge [
    source 55
    target 541
  ]
  edge [
    source 55
    target 647
  ]
  edge [
    source 55
    target 648
  ]
  edge [
    source 55
    target 649
  ]
  edge [
    source 55
    target 650
  ]
  edge [
    source 55
    target 544
  ]
  edge [
    source 55
    target 545
  ]
  edge [
    source 55
    target 546
  ]
  edge [
    source 55
    target 547
  ]
  edge [
    source 55
    target 651
  ]
  edge [
    source 55
    target 548
  ]
  edge [
    source 55
    target 550
  ]
  edge [
    source 55
    target 551
  ]
  edge [
    source 55
    target 552
  ]
  edge [
    source 55
    target 553
  ]
  edge [
    source 55
    target 554
  ]
  edge [
    source 55
    target 556
  ]
  edge [
    source 55
    target 652
  ]
  edge [
    source 55
    target 2140
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 60
  ]
  edge [
    source 56
    target 67
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 162
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 237
  ]
  edge [
    source 58
    target 653
  ]
  edge [
    source 58
    target 375
  ]
  edge [
    source 58
    target 238
  ]
  edge [
    source 58
    target 260
  ]
  edge [
    source 58
    target 654
  ]
  edge [
    source 58
    target 240
  ]
  edge [
    source 58
    target 241
  ]
  edge [
    source 58
    target 655
  ]
  edge [
    source 58
    target 239
  ]
  edge [
    source 58
    target 242
  ]
  edge [
    source 58
    target 656
  ]
  edge [
    source 58
    target 657
  ]
  edge [
    source 58
    target 243
  ]
  edge [
    source 58
    target 658
  ]
  edge [
    source 58
    target 659
  ]
  edge [
    source 58
    target 660
  ]
  edge [
    source 58
    target 661
  ]
  edge [
    source 58
    target 662
  ]
  edge [
    source 58
    target 663
  ]
  edge [
    source 58
    target 664
  ]
  edge [
    source 58
    target 665
  ]
  edge [
    source 58
    target 666
  ]
  edge [
    source 58
    target 244
  ]
  edge [
    source 58
    target 667
  ]
  edge [
    source 58
    target 668
  ]
  edge [
    source 58
    target 247
  ]
  edge [
    source 58
    target 245
  ]
  edge [
    source 58
    target 669
  ]
  edge [
    source 58
    target 670
  ]
  edge [
    source 58
    target 671
  ]
  edge [
    source 58
    target 248
  ]
  edge [
    source 58
    target 672
  ]
  edge [
    source 58
    target 249
  ]
  edge [
    source 58
    target 224
  ]
  edge [
    source 58
    target 673
  ]
  edge [
    source 58
    target 674
  ]
  edge [
    source 58
    target 250
  ]
  edge [
    source 58
    target 251
  ]
  edge [
    source 58
    target 321
  ]
  edge [
    source 58
    target 675
  ]
  edge [
    source 58
    target 676
  ]
  edge [
    source 58
    target 677
  ]
  edge [
    source 58
    target 678
  ]
  edge [
    source 58
    target 679
  ]
  edge [
    source 58
    target 680
  ]
  edge [
    source 58
    target 681
  ]
  edge [
    source 58
    target 682
  ]
  edge [
    source 58
    target 683
  ]
  edge [
    source 58
    target 684
  ]
  edge [
    source 58
    target 685
  ]
  edge [
    source 58
    target 686
  ]
  edge [
    source 58
    target 687
  ]
  edge [
    source 58
    target 688
  ]
  edge [
    source 58
    target 254
  ]
  edge [
    source 58
    target 689
  ]
  edge [
    source 58
    target 255
  ]
  edge [
    source 58
    target 690
  ]
  edge [
    source 58
    target 691
  ]
  edge [
    source 58
    target 692
  ]
  edge [
    source 58
    target 693
  ]
  edge [
    source 58
    target 694
  ]
  edge [
    source 58
    target 695
  ]
  edge [
    source 58
    target 256
  ]
  edge [
    source 58
    target 696
  ]
  edge [
    source 58
    target 697
  ]
  edge [
    source 58
    target 698
  ]
  edge [
    source 58
    target 257
  ]
  edge [
    source 58
    target 699
  ]
  edge [
    source 58
    target 61
  ]
  edge [
    source 58
    target 107
  ]
  edge [
    source 58
    target 114
  ]
  edge [
    source 58
    target 116
  ]
  edge [
    source 58
    target 135
  ]
  edge [
    source 58
    target 179
  ]
  edge [
    source 58
    target 187
  ]
  edge [
    source 58
    target 223
  ]
  edge [
    source 58
    target 234
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 144
  ]
  edge [
    source 60
    target 700
  ]
  edge [
    source 60
    target 701
  ]
  edge [
    source 60
    target 702
  ]
  edge [
    source 60
    target 703
  ]
  edge [
    source 60
    target 704
  ]
  edge [
    source 60
    target 705
  ]
  edge [
    source 60
    target 706
  ]
  edge [
    source 60
    target 707
  ]
  edge [
    source 60
    target 708
  ]
  edge [
    source 60
    target 709
  ]
  edge [
    source 60
    target 710
  ]
  edge [
    source 60
    target 711
  ]
  edge [
    source 60
    target 712
  ]
  edge [
    source 60
    target 713
  ]
  edge [
    source 60
    target 714
  ]
  edge [
    source 60
    target 609
  ]
  edge [
    source 60
    target 715
  ]
  edge [
    source 60
    target 716
  ]
  edge [
    source 60
    target 717
  ]
  edge [
    source 60
    target 718
  ]
  edge [
    source 60
    target 719
  ]
  edge [
    source 60
    target 720
  ]
  edge [
    source 60
    target 721
  ]
  edge [
    source 60
    target 722
  ]
  edge [
    source 60
    target 723
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 84
  ]
  edge [
    source 61
    target 85
  ]
  edge [
    source 61
    target 724
  ]
  edge [
    source 61
    target 725
  ]
  edge [
    source 61
    target 726
  ]
  edge [
    source 61
    target 89
  ]
  edge [
    source 61
    target 727
  ]
  edge [
    source 61
    target 728
  ]
  edge [
    source 61
    target 68
  ]
  edge [
    source 61
    target 729
  ]
  edge [
    source 61
    target 730
  ]
  edge [
    source 61
    target 731
  ]
  edge [
    source 61
    target 517
  ]
  edge [
    source 61
    target 511
  ]
  edge [
    source 61
    target 107
  ]
  edge [
    source 61
    target 114
  ]
  edge [
    source 61
    target 116
  ]
  edge [
    source 61
    target 135
  ]
  edge [
    source 61
    target 179
  ]
  edge [
    source 61
    target 187
  ]
  edge [
    source 61
    target 223
  ]
  edge [
    source 61
    target 234
  ]
  edge [
    source 61
    target 2141
  ]
  edge [
    source 61
    target 2142
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 732
  ]
  edge [
    source 62
    target 733
  ]
  edge [
    source 62
    target 734
  ]
  edge [
    source 62
    target 735
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 736
  ]
  edge [
    source 64
    target 737
  ]
  edge [
    source 64
    target 738
  ]
  edge [
    source 64
    target 739
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 164
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 207
  ]
  edge [
    source 66
    target 208
  ]
  edge [
    source 66
    target 740
  ]
  edge [
    source 66
    target 741
  ]
  edge [
    source 66
    target 218
  ]
  edge [
    source 66
    target 742
  ]
  edge [
    source 66
    target 743
  ]
  edge [
    source 66
    target 744
  ]
  edge [
    source 66
    target 240
  ]
  edge [
    source 66
    target 745
  ]
  edge [
    source 66
    target 746
  ]
  edge [
    source 66
    target 747
  ]
  edge [
    source 66
    target 156
  ]
  edge [
    source 66
    target 148
  ]
  edge [
    source 66
    target 748
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 749
  ]
  edge [
    source 67
    target 750
  ]
  edge [
    source 67
    target 751
  ]
  edge [
    source 67
    target 240
  ]
  edge [
    source 67
    target 296
  ]
  edge [
    source 67
    target 752
  ]
  edge [
    source 67
    target 753
  ]
  edge [
    source 67
    target 754
  ]
  edge [
    source 67
    target 755
  ]
  edge [
    source 67
    target 756
  ]
  edge [
    source 67
    target 757
  ]
  edge [
    source 67
    target 119
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 99
  ]
  edge [
    source 68
    target 100
  ]
  edge [
    source 68
    target 758
  ]
  edge [
    source 68
    target 336
  ]
  edge [
    source 68
    target 759
  ]
  edge [
    source 68
    target 150
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 76
  ]
  edge [
    source 69
    target 760
  ]
  edge [
    source 69
    target 761
  ]
  edge [
    source 69
    target 762
  ]
  edge [
    source 69
    target 763
  ]
  edge [
    source 69
    target 764
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 765
  ]
  edge [
    source 70
    target 766
  ]
  edge [
    source 70
    target 767
  ]
  edge [
    source 70
    target 768
  ]
  edge [
    source 70
    target 769
  ]
  edge [
    source 70
    target 770
  ]
  edge [
    source 70
    target 771
  ]
  edge [
    source 70
    target 772
  ]
  edge [
    source 70
    target 773
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 774
  ]
  edge [
    source 71
    target 775
  ]
  edge [
    source 71
    target 776
  ]
  edge [
    source 71
    target 608
  ]
  edge [
    source 71
    target 777
  ]
  edge [
    source 71
    target 97
  ]
  edge [
    source 71
    target 213
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 778
  ]
  edge [
    source 73
    target 779
  ]
  edge [
    source 73
    target 780
  ]
  edge [
    source 73
    target 781
  ]
  edge [
    source 73
    target 782
  ]
  edge [
    source 73
    target 617
  ]
  edge [
    source 73
    target 783
  ]
  edge [
    source 73
    target 784
  ]
  edge [
    source 73
    target 90
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 103
  ]
  edge [
    source 74
    target 104
  ]
  edge [
    source 74
    target 785
  ]
  edge [
    source 74
    target 504
  ]
  edge [
    source 74
    target 786
  ]
  edge [
    source 74
    target 505
  ]
  edge [
    source 74
    target 787
  ]
  edge [
    source 74
    target 788
  ]
  edge [
    source 74
    target 509
  ]
  edge [
    source 74
    target 789
  ]
  edge [
    source 74
    target 502
  ]
  edge [
    source 74
    target 790
  ]
  edge [
    source 74
    target 791
  ]
  edge [
    source 74
    target 510
  ]
  edge [
    source 74
    target 792
  ]
  edge [
    source 74
    target 793
  ]
  edge [
    source 74
    target 503
  ]
  edge [
    source 74
    target 794
  ]
  edge [
    source 74
    target 795
  ]
  edge [
    source 74
    target 336
  ]
  edge [
    source 74
    target 796
  ]
  edge [
    source 74
    target 797
  ]
  edge [
    source 74
    target 798
  ]
  edge [
    source 74
    target 799
  ]
  edge [
    source 74
    target 800
  ]
  edge [
    source 74
    target 801
  ]
  edge [
    source 74
    target 128
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 314
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 802
  ]
  edge [
    source 76
    target 803
  ]
  edge [
    source 76
    target 804
  ]
  edge [
    source 76
    target 462
  ]
  edge [
    source 76
    target 805
  ]
  edge [
    source 76
    target 806
  ]
  edge [
    source 76
    target 807
  ]
  edge [
    source 76
    target 808
  ]
  edge [
    source 76
    target 809
  ]
  edge [
    source 76
    target 810
  ]
  edge [
    source 76
    target 115
  ]
  edge [
    source 76
    target 811
  ]
  edge [
    source 76
    target 812
  ]
  edge [
    source 76
    target 813
  ]
  edge [
    source 76
    target 814
  ]
  edge [
    source 76
    target 815
  ]
  edge [
    source 76
    target 816
  ]
  edge [
    source 76
    target 817
  ]
  edge [
    source 76
    target 101
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 103
  ]
  edge [
    source 77
    target 139
  ]
  edge [
    source 77
    target 216
  ]
  edge [
    source 77
    target 225
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 115
  ]
  edge [
    source 78
    target 116
  ]
  edge [
    source 78
    target 818
  ]
  edge [
    source 78
    target 819
  ]
  edge [
    source 78
    target 820
  ]
  edge [
    source 78
    target 821
  ]
  edge [
    source 78
    target 822
  ]
  edge [
    source 78
    target 823
  ]
  edge [
    source 78
    target 824
  ]
  edge [
    source 78
    target 825
  ]
  edge [
    source 78
    target 826
  ]
  edge [
    source 78
    target 827
  ]
  edge [
    source 78
    target 88
  ]
  edge [
    source 78
    target 828
  ]
  edge [
    source 78
    target 829
  ]
  edge [
    source 78
    target 830
  ]
  edge [
    source 78
    target 831
  ]
  edge [
    source 78
    target 832
  ]
  edge [
    source 78
    target 833
  ]
  edge [
    source 78
    target 834
  ]
  edge [
    source 78
    target 835
  ]
  edge [
    source 78
    target 185
  ]
  edge [
    source 78
    target 103
  ]
  edge [
    source 78
    target 139
  ]
  edge [
    source 78
    target 216
  ]
  edge [
    source 78
    target 225
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 153
  ]
  edge [
    source 81
    target 836
  ]
  edge [
    source 81
    target 837
  ]
  edge [
    source 81
    target 838
  ]
  edge [
    source 81
    target 839
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 840
  ]
  edge [
    source 82
    target 841
  ]
  edge [
    source 82
    target 842
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 114
  ]
  edge [
    source 83
    target 107
  ]
  edge [
    source 83
    target 145
  ]
  edge [
    source 83
    target 843
  ]
  edge [
    source 83
    target 844
  ]
  edge [
    source 83
    target 845
  ]
  edge [
    source 83
    target 260
  ]
  edge [
    source 83
    target 846
  ]
  edge [
    source 83
    target 242
  ]
  edge [
    source 83
    target 847
  ]
  edge [
    source 83
    target 848
  ]
  edge [
    source 83
    target 849
  ]
  edge [
    source 83
    target 850
  ]
  edge [
    source 83
    target 851
  ]
  edge [
    source 83
    target 852
  ]
  edge [
    source 83
    target 853
  ]
  edge [
    source 83
    target 854
  ]
  edge [
    source 83
    target 113
  ]
  edge [
    source 83
    target 211
  ]
  edge [
    source 85
    target 104
  ]
  edge [
    source 85
    target 89
  ]
  edge [
    source 85
    target 855
  ]
  edge [
    source 85
    target 856
  ]
  edge [
    source 85
    target 130
  ]
  edge [
    source 85
    target 2141
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 857
  ]
  edge [
    source 86
    target 858
  ]
  edge [
    source 86
    target 859
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 145
  ]
  edge [
    source 87
    target 126
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 860
  ]
  edge [
    source 88
    target 861
  ]
  edge [
    source 88
    target 862
  ]
  edge [
    source 88
    target 863
  ]
  edge [
    source 88
    target 864
  ]
  edge [
    source 88
    target 865
  ]
  edge [
    source 88
    target 866
  ]
  edge [
    source 88
    target 117
  ]
  edge [
    source 89
    target 867
  ]
  edge [
    source 89
    target 467
  ]
  edge [
    source 89
    target 175
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 164
  ]
  edge [
    source 90
    target 165
  ]
  edge [
    source 90
    target 868
  ]
  edge [
    source 90
    target 869
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 870
  ]
  edge [
    source 91
    target 871
  ]
  edge [
    source 91
    target 232
  ]
  edge [
    source 91
    target 872
  ]
  edge [
    source 91
    target 873
  ]
  edge [
    source 92
    target 874
  ]
  edge [
    source 92
    target 875
  ]
  edge [
    source 92
    target 876
  ]
  edge [
    source 92
    target 877
  ]
  edge [
    source 92
    target 700
  ]
  edge [
    source 92
    target 878
  ]
  edge [
    source 92
    target 879
  ]
  edge [
    source 92
    target 880
  ]
  edge [
    source 92
    target 881
  ]
  edge [
    source 92
    target 712
  ]
  edge [
    source 92
    target 882
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 883
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 884
  ]
  edge [
    source 94
    target 885
  ]
  edge [
    source 94
    target 886
  ]
  edge [
    source 94
    target 887
  ]
  edge [
    source 94
    target 888
  ]
  edge [
    source 94
    target 579
  ]
  edge [
    source 94
    target 889
  ]
  edge [
    source 94
    target 890
  ]
  edge [
    source 94
    target 891
  ]
  edge [
    source 94
    target 892
  ]
  edge [
    source 94
    target 893
  ]
  edge [
    source 94
    target 894
  ]
  edge [
    source 94
    target 895
  ]
  edge [
    source 94
    target 896
  ]
  edge [
    source 94
    target 793
  ]
  edge [
    source 94
    target 897
  ]
  edge [
    source 94
    target 898
  ]
  edge [
    source 94
    target 899
  ]
  edge [
    source 94
    target 900
  ]
  edge [
    source 94
    target 296
  ]
  edge [
    source 94
    target 901
  ]
  edge [
    source 94
    target 902
  ]
  edge [
    source 94
    target 903
  ]
  edge [
    source 94
    target 904
  ]
  edge [
    source 94
    target 905
  ]
  edge [
    source 94
    target 906
  ]
  edge [
    source 94
    target 907
  ]
  edge [
    source 94
    target 908
  ]
  edge [
    source 94
    target 909
  ]
  edge [
    source 94
    target 910
  ]
  edge [
    source 94
    target 911
  ]
  edge [
    source 94
    target 912
  ]
  edge [
    source 94
    target 913
  ]
  edge [
    source 94
    target 914
  ]
  edge [
    source 94
    target 915
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 916
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 131
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 608
  ]
  edge [
    source 97
    target 225
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 190
  ]
  edge [
    source 98
    target 917
  ]
  edge [
    source 99
    target 166
  ]
  edge [
    source 99
    target 167
  ]
  edge [
    source 99
    target 918
  ]
  edge [
    source 99
    target 919
  ]
  edge [
    source 99
    target 920
  ]
  edge [
    source 99
    target 921
  ]
  edge [
    source 99
    target 164
  ]
  edge [
    source 99
    target 922
  ]
  edge [
    source 99
    target 923
  ]
  edge [
    source 99
    target 924
  ]
  edge [
    source 99
    target 925
  ]
  edge [
    source 99
    target 926
  ]
  edge [
    source 99
    target 927
  ]
  edge [
    source 99
    target 230
  ]
  edge [
    source 99
    target 191
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 928
  ]
  edge [
    source 100
    target 929
  ]
  edge [
    source 100
    target 930
  ]
  edge [
    source 100
    target 931
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 803
  ]
  edge [
    source 101
    target 932
  ]
  edge [
    source 101
    target 933
  ]
  edge [
    source 101
    target 817
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 934
  ]
  edge [
    source 102
    target 935
  ]
  edge [
    source 102
    target 296
  ]
  edge [
    source 102
    target 936
  ]
  edge [
    source 102
    target 792
  ]
  edge [
    source 103
    target 937
  ]
  edge [
    source 103
    target 938
  ]
  edge [
    source 103
    target 939
  ]
  edge [
    source 103
    target 940
  ]
  edge [
    source 103
    target 941
  ]
  edge [
    source 103
    target 942
  ]
  edge [
    source 103
    target 943
  ]
  edge [
    source 103
    target 944
  ]
  edge [
    source 103
    target 945
  ]
  edge [
    source 103
    target 946
  ]
  edge [
    source 103
    target 947
  ]
  edge [
    source 103
    target 948
  ]
  edge [
    source 103
    target 949
  ]
  edge [
    source 103
    target 950
  ]
  edge [
    source 103
    target 951
  ]
  edge [
    source 103
    target 952
  ]
  edge [
    source 103
    target 296
  ]
  edge [
    source 103
    target 953
  ]
  edge [
    source 103
    target 954
  ]
  edge [
    source 103
    target 955
  ]
  edge [
    source 103
    target 956
  ]
  edge [
    source 103
    target 957
  ]
  edge [
    source 103
    target 958
  ]
  edge [
    source 103
    target 959
  ]
  edge [
    source 103
    target 960
  ]
  edge [
    source 103
    target 961
  ]
  edge [
    source 103
    target 962
  ]
  edge [
    source 103
    target 139
  ]
  edge [
    source 103
    target 216
  ]
  edge [
    source 103
    target 225
  ]
  edge [
    source 104
    target 963
  ]
  edge [
    source 104
    target 964
  ]
  edge [
    source 104
    target 965
  ]
  edge [
    source 105
    target 966
  ]
  edge [
    source 105
    target 967
  ]
  edge [
    source 105
    target 574
  ]
  edge [
    source 105
    target 968
  ]
  edge [
    source 105
    target 969
  ]
  edge [
    source 105
    target 970
  ]
  edge [
    source 106
    target 971
  ]
  edge [
    source 106
    target 972
  ]
  edge [
    source 106
    target 973
  ]
  edge [
    source 106
    target 974
  ]
  edge [
    source 106
    target 975
  ]
  edge [
    source 106
    target 976
  ]
  edge [
    source 106
    target 977
  ]
  edge [
    source 106
    target 978
  ]
  edge [
    source 106
    target 979
  ]
  edge [
    source 106
    target 288
  ]
  edge [
    source 106
    target 980
  ]
  edge [
    source 106
    target 981
  ]
  edge [
    source 106
    target 982
  ]
  edge [
    source 106
    target 983
  ]
  edge [
    source 106
    target 984
  ]
  edge [
    source 106
    target 985
  ]
  edge [
    source 106
    target 986
  ]
  edge [
    source 106
    target 133
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 115
  ]
  edge [
    source 107
    target 987
  ]
  edge [
    source 107
    target 988
  ]
  edge [
    source 107
    target 989
  ]
  edge [
    source 107
    target 990
  ]
  edge [
    source 107
    target 991
  ]
  edge [
    source 107
    target 992
  ]
  edge [
    source 107
    target 993
  ]
  edge [
    source 107
    target 114
  ]
  edge [
    source 107
    target 116
  ]
  edge [
    source 107
    target 135
  ]
  edge [
    source 107
    target 179
  ]
  edge [
    source 107
    target 187
  ]
  edge [
    source 107
    target 223
  ]
  edge [
    source 107
    target 234
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 994
  ]
  edge [
    source 108
    target 995
  ]
  edge [
    source 108
    target 996
  ]
  edge [
    source 108
    target 240
  ]
  edge [
    source 108
    target 997
  ]
  edge [
    source 108
    target 998
  ]
  edge [
    source 108
    target 999
  ]
  edge [
    source 108
    target 1000
  ]
  edge [
    source 108
    target 1001
  ]
  edge [
    source 108
    target 1002
  ]
  edge [
    source 108
    target 1003
  ]
  edge [
    source 108
    target 1004
  ]
  edge [
    source 108
    target 1005
  ]
  edge [
    source 108
    target 1006
  ]
  edge [
    source 108
    target 1007
  ]
  edge [
    source 108
    target 1008
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 183
  ]
  edge [
    source 109
    target 214
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 1009
  ]
  edge [
    source 111
    target 662
  ]
  edge [
    source 111
    target 1010
  ]
  edge [
    source 111
    target 609
  ]
  edge [
    source 111
    target 1011
  ]
  edge [
    source 111
    target 1012
  ]
  edge [
    source 111
    target 1013
  ]
  edge [
    source 111
    target 1014
  ]
  edge [
    source 111
    target 1015
  ]
  edge [
    source 111
    target 1016
  ]
  edge [
    source 111
    target 717
  ]
  edge [
    source 111
    target 1017
  ]
  edge [
    source 111
    target 1018
  ]
  edge [
    source 111
    target 1019
  ]
  edge [
    source 111
    target 1020
  ]
  edge [
    source 111
    target 1021
  ]
  edge [
    source 111
    target 1022
  ]
  edge [
    source 111
    target 1023
  ]
  edge [
    source 111
    target 188
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 360
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 1024
  ]
  edge [
    source 113
    target 1025
  ]
  edge [
    source 113
    target 129
  ]
  edge [
    source 113
    target 134
  ]
  edge [
    source 114
    target 1026
  ]
  edge [
    source 114
    target 1027
  ]
  edge [
    source 114
    target 116
  ]
  edge [
    source 114
    target 135
  ]
  edge [
    source 114
    target 179
  ]
  edge [
    source 114
    target 187
  ]
  edge [
    source 114
    target 223
  ]
  edge [
    source 114
    target 234
  ]
  edge [
    source 115
    target 817
  ]
  edge [
    source 115
    target 1028
  ]
  edge [
    source 115
    target 1029
  ]
  edge [
    source 115
    target 1030
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 587
  ]
  edge [
    source 116
    target 1031
  ]
  edge [
    source 116
    target 1032
  ]
  edge [
    source 116
    target 829
  ]
  edge [
    source 116
    target 1033
  ]
  edge [
    source 116
    target 1034
  ]
  edge [
    source 116
    target 1023
  ]
  edge [
    source 116
    target 135
  ]
  edge [
    source 116
    target 179
  ]
  edge [
    source 116
    target 187
  ]
  edge [
    source 116
    target 223
  ]
  edge [
    source 116
    target 234
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 1035
  ]
  edge [
    source 117
    target 819
  ]
  edge [
    source 117
    target 1036
  ]
  edge [
    source 117
    target 1037
  ]
  edge [
    source 117
    target 821
  ]
  edge [
    source 117
    target 823
  ]
  edge [
    source 117
    target 1038
  ]
  edge [
    source 117
    target 833
  ]
  edge [
    source 117
    target 1039
  ]
  edge [
    source 117
    target 827
  ]
  edge [
    source 117
    target 828
  ]
  edge [
    source 117
    target 1040
  ]
  edge [
    source 117
    target 829
  ]
  edge [
    source 117
    target 830
  ]
  edge [
    source 117
    target 1041
  ]
  edge [
    source 117
    target 1042
  ]
  edge [
    source 117
    target 1043
  ]
  edge [
    source 117
    target 1044
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 1045
  ]
  edge [
    source 118
    target 1046
  ]
  edge [
    source 118
    target 1047
  ]
  edge [
    source 118
    target 1048
  ]
  edge [
    source 118
    target 1049
  ]
  edge [
    source 118
    target 1050
  ]
  edge [
    source 118
    target 1051
  ]
  edge [
    source 118
    target 1052
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 1053
  ]
  edge [
    source 119
    target 827
  ]
  edge [
    source 119
    target 1054
  ]
  edge [
    source 119
    target 1055
  ]
  edge [
    source 119
    target 1056
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 1057
  ]
  edge [
    source 120
    target 1058
  ]
  edge [
    source 120
    target 1059
  ]
  edge [
    source 120
    target 1060
  ]
  edge [
    source 120
    target 1061
  ]
  edge [
    source 120
    target 1062
  ]
  edge [
    source 120
    target 1063
  ]
  edge [
    source 120
    target 1064
  ]
  edge [
    source 120
    target 1065
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 1066
  ]
  edge [
    source 121
    target 1067
  ]
  edge [
    source 121
    target 1068
  ]
  edge [
    source 121
    target 1069
  ]
  edge [
    source 121
    target 336
  ]
  edge [
    source 121
    target 586
  ]
  edge [
    source 121
    target 1070
  ]
  edge [
    source 121
    target 1071
  ]
  edge [
    source 121
    target 129
  ]
  edge [
    source 122
    target 1072
  ]
  edge [
    source 122
    target 1073
  ]
  edge [
    source 122
    target 1074
  ]
  edge [
    source 122
    target 1075
  ]
  edge [
    source 122
    target 1076
  ]
  edge [
    source 122
    target 1077
  ]
  edge [
    source 122
    target 1078
  ]
  edge [
    source 122
    target 1079
  ]
  edge [
    source 122
    target 1080
  ]
  edge [
    source 122
    target 345
  ]
  edge [
    source 122
    target 1081
  ]
  edge [
    source 122
    target 1082
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 1083
  ]
  edge [
    source 123
    target 1084
  ]
  edge [
    source 123
    target 1085
  ]
  edge [
    source 123
    target 213
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 1086
  ]
  edge [
    source 125
    target 1087
  ]
  edge [
    source 125
    target 1088
  ]
  edge [
    source 125
    target 1089
  ]
  edge [
    source 125
    target 1090
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 1091
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 500
  ]
  edge [
    source 127
    target 1092
  ]
  edge [
    source 127
    target 1093
  ]
  edge [
    source 127
    target 1094
  ]
  edge [
    source 127
    target 242
  ]
  edge [
    source 127
    target 507
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 1095
  ]
  edge [
    source 128
    target 1096
  ]
  edge [
    source 128
    target 1097
  ]
  edge [
    source 128
    target 1098
  ]
  edge [
    source 129
    target 1099
  ]
  edge [
    source 129
    target 1100
  ]
  edge [
    source 129
    target 1101
  ]
  edge [
    source 129
    target 1102
  ]
  edge [
    source 129
    target 1103
  ]
  edge [
    source 129
    target 253
  ]
  edge [
    source 129
    target 196
  ]
  edge [
    source 129
    target 236
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 130
    target 146
  ]
  edge [
    source 130
    target 1104
  ]
  edge [
    source 130
    target 218
  ]
  edge [
    source 130
    target 1105
  ]
  edge [
    source 130
    target 1106
  ]
  edge [
    source 130
    target 1107
  ]
  edge [
    source 130
    target 240
  ]
  edge [
    source 130
    target 1108
  ]
  edge [
    source 130
    target 1109
  ]
  edge [
    source 130
    target 1110
  ]
  edge [
    source 130
    target 1111
  ]
  edge [
    source 130
    target 1112
  ]
  edge [
    source 130
    target 1113
  ]
  edge [
    source 130
    target 1114
  ]
  edge [
    source 130
    target 154
  ]
  edge [
    source 130
    target 1115
  ]
  edge [
    source 130
    target 1116
  ]
  edge [
    source 130
    target 1117
  ]
  edge [
    source 130
    target 1118
  ]
  edge [
    source 130
    target 1119
  ]
  edge [
    source 130
    target 1120
  ]
  edge [
    source 130
    target 1121
  ]
  edge [
    source 130
    target 1122
  ]
  edge [
    source 130
    target 1123
  ]
  edge [
    source 130
    target 1124
  ]
  edge [
    source 130
    target 1125
  ]
  edge [
    source 130
    target 1126
  ]
  edge [
    source 130
    target 1127
  ]
  edge [
    source 130
    target 1128
  ]
  edge [
    source 130
    target 1129
  ]
  edge [
    source 130
    target 1130
  ]
  edge [
    source 130
    target 1131
  ]
  edge [
    source 130
    target 1132
  ]
  edge [
    source 130
    target 1133
  ]
  edge [
    source 130
    target 1134
  ]
  edge [
    source 130
    target 831
  ]
  edge [
    source 130
    target 1135
  ]
  edge [
    source 130
    target 1136
  ]
  edge [
    source 130
    target 1137
  ]
  edge [
    source 130
    target 1138
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 1139
  ]
  edge [
    source 131
    target 1140
  ]
  edge [
    source 131
    target 1141
  ]
  edge [
    source 131
    target 1142
  ]
  edge [
    source 131
    target 1143
  ]
  edge [
    source 131
    target 1144
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 132
    target 1145
  ]
  edge [
    source 132
    target 1146
  ]
  edge [
    source 132
    target 1147
  ]
  edge [
    source 132
    target 1148
  ]
  edge [
    source 132
    target 704
  ]
  edge [
    source 132
    target 717
  ]
  edge [
    source 132
    target 706
  ]
  edge [
    source 132
    target 1149
  ]
  edge [
    source 132
    target 1150
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 133
    target 145
  ]
  edge [
    source 133
    target 1151
  ]
  edge [
    source 133
    target 1152
  ]
  edge [
    source 133
    target 1153
  ]
  edge [
    source 133
    target 1154
  ]
  edge [
    source 133
    target 1155
  ]
  edge [
    source 133
    target 1156
  ]
  edge [
    source 133
    target 1157
  ]
  edge [
    source 133
    target 1158
  ]
  edge [
    source 133
    target 1159
  ]
  edge [
    source 133
    target 1160
  ]
  edge [
    source 133
    target 1161
  ]
  edge [
    source 133
    target 1162
  ]
  edge [
    source 133
    target 186
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 134
    target 1163
  ]
  edge [
    source 134
    target 218
  ]
  edge [
    source 134
    target 1164
  ]
  edge [
    source 134
    target 1165
  ]
  edge [
    source 134
    target 1166
  ]
  edge [
    source 134
    target 1167
  ]
  edge [
    source 134
    target 1168
  ]
  edge [
    source 134
    target 1169
  ]
  edge [
    source 134
    target 1170
  ]
  edge [
    source 134
    target 1171
  ]
  edge [
    source 134
    target 1172
  ]
  edge [
    source 134
    target 1173
  ]
  edge [
    source 134
    target 1174
  ]
  edge [
    source 134
    target 1175
  ]
  edge [
    source 134
    target 1176
  ]
  edge [
    source 134
    target 1177
  ]
  edge [
    source 134
    target 1178
  ]
  edge [
    source 134
    target 1179
  ]
  edge [
    source 134
    target 1180
  ]
  edge [
    source 134
    target 1181
  ]
  edge [
    source 134
    target 1182
  ]
  edge [
    source 134
    target 1183
  ]
  edge [
    source 134
    target 1184
  ]
  edge [
    source 134
    target 205
  ]
  edge [
    source 134
    target 1185
  ]
  edge [
    source 134
    target 1186
  ]
  edge [
    source 134
    target 1187
  ]
  edge [
    source 134
    target 1188
  ]
  edge [
    source 134
    target 1189
  ]
  edge [
    source 134
    target 1190
  ]
  edge [
    source 134
    target 1191
  ]
  edge [
    source 134
    target 1192
  ]
  edge [
    source 134
    target 253
  ]
  edge [
    source 134
    target 1193
  ]
  edge [
    source 134
    target 1194
  ]
  edge [
    source 134
    target 1195
  ]
  edge [
    source 134
    target 1196
  ]
  edge [
    source 134
    target 1197
  ]
  edge [
    source 134
    target 1198
  ]
  edge [
    source 134
    target 1199
  ]
  edge [
    source 134
    target 184
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 135
    target 1200
  ]
  edge [
    source 135
    target 1201
  ]
  edge [
    source 135
    target 1202
  ]
  edge [
    source 135
    target 1203
  ]
  edge [
    source 135
    target 1204
  ]
  edge [
    source 135
    target 1205
  ]
  edge [
    source 135
    target 1206
  ]
  edge [
    source 135
    target 1207
  ]
  edge [
    source 135
    target 1208
  ]
  edge [
    source 135
    target 1209
  ]
  edge [
    source 135
    target 179
  ]
  edge [
    source 135
    target 187
  ]
  edge [
    source 135
    target 223
  ]
  edge [
    source 135
    target 234
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 136
    target 1210
  ]
  edge [
    source 136
    target 804
  ]
  edge [
    source 136
    target 1211
  ]
  edge [
    source 136
    target 1212
  ]
  edge [
    source 136
    target 1213
  ]
  edge [
    source 136
    target 717
  ]
  edge [
    source 136
    target 1214
  ]
  edge [
    source 136
    target 1215
  ]
  edge [
    source 136
    target 1216
  ]
  edge [
    source 137
    target 138
  ]
  edge [
    source 137
    target 291
  ]
  edge [
    source 137
    target 1217
  ]
  edge [
    source 137
    target 1218
  ]
  edge [
    source 137
    target 407
  ]
  edge [
    source 137
    target 1219
  ]
  edge [
    source 137
    target 1220
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 138
    target 1221
  ]
  edge [
    source 138
    target 1222
  ]
  edge [
    source 138
    target 1223
  ]
  edge [
    source 138
    target 1224
  ]
  edge [
    source 138
    target 1225
  ]
  edge [
    source 138
    target 1226
  ]
  edge [
    source 138
    target 1227
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 139
    target 1228
  ]
  edge [
    source 139
    target 1229
  ]
  edge [
    source 139
    target 1230
  ]
  edge [
    source 139
    target 1231
  ]
  edge [
    source 139
    target 1232
  ]
  edge [
    source 139
    target 1233
  ]
  edge [
    source 139
    target 1234
  ]
  edge [
    source 139
    target 1235
  ]
  edge [
    source 139
    target 1236
  ]
  edge [
    source 139
    target 1237
  ]
  edge [
    source 139
    target 224
  ]
  edge [
    source 139
    target 949
  ]
  edge [
    source 139
    target 1238
  ]
  edge [
    source 139
    target 1239
  ]
  edge [
    source 139
    target 1240
  ]
  edge [
    source 139
    target 1241
  ]
  edge [
    source 139
    target 1242
  ]
  edge [
    source 139
    target 1243
  ]
  edge [
    source 139
    target 1244
  ]
  edge [
    source 139
    target 1245
  ]
  edge [
    source 139
    target 1246
  ]
  edge [
    source 139
    target 216
  ]
  edge [
    source 139
    target 225
  ]
  edge [
    source 140
    target 1247
  ]
  edge [
    source 140
    target 1248
  ]
  edge [
    source 140
    target 1249
  ]
  edge [
    source 140
    target 1250
  ]
  edge [
    source 140
    target 1251
  ]
  edge [
    source 140
    target 1252
  ]
  edge [
    source 140
    target 1253
  ]
  edge [
    source 140
    target 146
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 141
    target 219
  ]
  edge [
    source 142
    target 143
  ]
  edge [
    source 142
    target 144
  ]
  edge [
    source 142
    target 1254
  ]
  edge [
    source 142
    target 1255
  ]
  edge [
    source 142
    target 1256
  ]
  edge [
    source 142
    target 222
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 143
    target 1257
  ]
  edge [
    source 144
    target 1258
  ]
  edge [
    source 144
    target 1259
  ]
  edge [
    source 144
    target 1260
  ]
  edge [
    source 144
    target 1261
  ]
  edge [
    source 144
    target 1262
  ]
  edge [
    source 144
    target 1263
  ]
  edge [
    source 144
    target 154
  ]
  edge [
    source 144
    target 1264
  ]
  edge [
    source 144
    target 407
  ]
  edge [
    source 144
    target 412
  ]
  edge [
    source 144
    target 1265
  ]
  edge [
    source 144
    target 1266
  ]
  edge [
    source 144
    target 503
  ]
  edge [
    source 144
    target 1267
  ]
  edge [
    source 144
    target 1268
  ]
  edge [
    source 144
    target 1269
  ]
  edge [
    source 144
    target 252
  ]
  edge [
    source 144
    target 1270
  ]
  edge [
    source 144
    target 217
  ]
  edge [
    source 144
    target 336
  ]
  edge [
    source 144
    target 1271
  ]
  edge [
    source 144
    target 1272
  ]
  edge [
    source 144
    target 1273
  ]
  edge [
    source 144
    target 1274
  ]
  edge [
    source 144
    target 1275
  ]
  edge [
    source 144
    target 1276
  ]
  edge [
    source 144
    target 831
  ]
  edge [
    source 144
    target 222
  ]
  edge [
    source 145
    target 1277
  ]
  edge [
    source 145
    target 1154
  ]
  edge [
    source 145
    target 1278
  ]
  edge [
    source 145
    target 1279
  ]
  edge [
    source 145
    target 1280
  ]
  edge [
    source 145
    target 1281
  ]
  edge [
    source 145
    target 1282
  ]
  edge [
    source 145
    target 1283
  ]
  edge [
    source 145
    target 1284
  ]
  edge [
    source 145
    target 1285
  ]
  edge [
    source 145
    target 1286
  ]
  edge [
    source 145
    target 570
  ]
  edge [
    source 145
    target 250
  ]
  edge [
    source 145
    target 1287
  ]
  edge [
    source 145
    target 1288
  ]
  edge [
    source 145
    target 184
  ]
  edge [
    source 145
    target 1289
  ]
  edge [
    source 145
    target 1290
  ]
  edge [
    source 145
    target 1291
  ]
  edge [
    source 145
    target 563
  ]
  edge [
    source 145
    target 1292
  ]
  edge [
    source 145
    target 1293
  ]
  edge [
    source 145
    target 1160
  ]
  edge [
    source 145
    target 1294
  ]
  edge [
    source 145
    target 175
  ]
  edge [
    source 145
    target 179
  ]
  edge [
    source 145
    target 186
  ]
  edge [
    source 145
    target 217
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 146
    target 1295
  ]
  edge [
    source 146
    target 1296
  ]
  edge [
    source 146
    target 1297
  ]
  edge [
    source 146
    target 1298
  ]
  edge [
    source 146
    target 1299
  ]
  edge [
    source 146
    target 464
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 148
    target 1300
  ]
  edge [
    source 148
    target 218
  ]
  edge [
    source 149
    target 150
  ]
  edge [
    source 149
    target 156
  ]
  edge [
    source 149
    target 157
  ]
  edge [
    source 150
    target 151
  ]
  edge [
    source 150
    target 1301
  ]
  edge [
    source 150
    target 1302
  ]
  edge [
    source 150
    target 1303
  ]
  edge [
    source 150
    target 1304
  ]
  edge [
    source 150
    target 1305
  ]
  edge [
    source 150
    target 1306
  ]
  edge [
    source 150
    target 1307
  ]
  edge [
    source 150
    target 1308
  ]
  edge [
    source 150
    target 1309
  ]
  edge [
    source 150
    target 1310
  ]
  edge [
    source 150
    target 1311
  ]
  edge [
    source 150
    target 1312
  ]
  edge [
    source 150
    target 1313
  ]
  edge [
    source 150
    target 1257
  ]
  edge [
    source 150
    target 1161
  ]
  edge [
    source 150
    target 1314
  ]
  edge [
    source 150
    target 1315
  ]
  edge [
    source 150
    target 1316
  ]
  edge [
    source 150
    target 1317
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 151
    target 949
  ]
  edge [
    source 151
    target 1318
  ]
  edge [
    source 151
    target 1100
  ]
  edge [
    source 151
    target 1319
  ]
  edge [
    source 151
    target 1320
  ]
  edge [
    source 151
    target 1321
  ]
  edge [
    source 151
    target 336
  ]
  edge [
    source 151
    target 1322
  ]
  edge [
    source 151
    target 1323
  ]
  edge [
    source 151
    target 1324
  ]
  edge [
    source 151
    target 1325
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 152
    target 1326
  ]
  edge [
    source 152
    target 350
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 153
    target 1327
  ]
  edge [
    source 153
    target 1328
  ]
  edge [
    source 153
    target 1329
  ]
  edge [
    source 153
    target 1330
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 154
    target 1331
  ]
  edge [
    source 154
    target 1332
  ]
  edge [
    source 154
    target 1333
  ]
  edge [
    source 154
    target 636
  ]
  edge [
    source 154
    target 1334
  ]
  edge [
    source 154
    target 1335
  ]
  edge [
    source 154
    target 1336
  ]
  edge [
    source 154
    target 1337
  ]
  edge [
    source 154
    target 1109
  ]
  edge [
    source 154
    target 1338
  ]
  edge [
    source 154
    target 1339
  ]
  edge [
    source 154
    target 1340
  ]
  edge [
    source 154
    target 1341
  ]
  edge [
    source 154
    target 1342
  ]
  edge [
    source 154
    target 671
  ]
  edge [
    source 154
    target 1343
  ]
  edge [
    source 154
    target 1344
  ]
  edge [
    source 154
    target 1345
  ]
  edge [
    source 154
    target 1346
  ]
  edge [
    source 154
    target 528
  ]
  edge [
    source 154
    target 896
  ]
  edge [
    source 154
    target 529
  ]
  edge [
    source 154
    target 1347
  ]
  edge [
    source 154
    target 1348
  ]
  edge [
    source 154
    target 1349
  ]
  edge [
    source 154
    target 1350
  ]
  edge [
    source 154
    target 756
  ]
  edge [
    source 154
    target 1351
  ]
  edge [
    source 154
    target 1352
  ]
  edge [
    source 154
    target 1353
  ]
  edge [
    source 154
    target 1354
  ]
  edge [
    source 154
    target 1355
  ]
  edge [
    source 154
    target 1356
  ]
  edge [
    source 154
    target 1357
  ]
  edge [
    source 154
    target 1358
  ]
  edge [
    source 154
    target 1359
  ]
  edge [
    source 154
    target 1360
  ]
  edge [
    source 154
    target 1361
  ]
  edge [
    source 154
    target 1362
  ]
  edge [
    source 154
    target 1363
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 155
    target 1364
  ]
  edge [
    source 155
    target 1345
  ]
  edge [
    source 156
    target 218
  ]
  edge [
    source 156
    target 1365
  ]
  edge [
    source 156
    target 1366
  ]
  edge [
    source 156
    target 1367
  ]
  edge [
    source 156
    target 313
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 158
    target 1368
  ]
  edge [
    source 158
    target 1369
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 159
    target 1370
  ]
  edge [
    source 159
    target 1371
  ]
  edge [
    source 159
    target 1372
  ]
  edge [
    source 159
    target 1373
  ]
  edge [
    source 159
    target 1374
  ]
  edge [
    source 159
    target 1375
  ]
  edge [
    source 159
    target 1376
  ]
  edge [
    source 159
    target 1377
  ]
  edge [
    source 159
    target 1378
  ]
  edge [
    source 159
    target 1379
  ]
  edge [
    source 159
    target 1380
  ]
  edge [
    source 159
    target 709
  ]
  edge [
    source 159
    target 1381
  ]
  edge [
    source 159
    target 1382
  ]
  edge [
    source 159
    target 1383
  ]
  edge [
    source 159
    target 203
  ]
  edge [
    source 159
    target 1384
  ]
  edge [
    source 159
    target 1385
  ]
  edge [
    source 159
    target 1386
  ]
  edge [
    source 159
    target 1387
  ]
  edge [
    source 159
    target 1388
  ]
  edge [
    source 159
    target 1389
  ]
  edge [
    source 159
    target 1390
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 160
    target 1391
  ]
  edge [
    source 160
    target 1392
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 161
    target 184
  ]
  edge [
    source 161
    target 185
  ]
  edge [
    source 161
    target 180
  ]
  edge [
    source 161
    target 1393
  ]
  edge [
    source 161
    target 1394
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 162
    target 1395
  ]
  edge [
    source 162
    target 1396
  ]
  edge [
    source 162
    target 1397
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 163
    target 171
  ]
  edge [
    source 163
    target 1398
  ]
  edge [
    source 163
    target 1399
  ]
  edge [
    source 163
    target 1400
  ]
  edge [
    source 163
    target 1401
  ]
  edge [
    source 163
    target 1402
  ]
  edge [
    source 163
    target 1403
  ]
  edge [
    source 163
    target 1404
  ]
  edge [
    source 163
    target 1405
  ]
  edge [
    source 163
    target 1406
  ]
  edge [
    source 164
    target 1407
  ]
  edge [
    source 164
    target 1408
  ]
  edge [
    source 164
    target 1409
  ]
  edge [
    source 164
    target 1410
  ]
  edge [
    source 164
    target 1411
  ]
  edge [
    source 164
    target 1412
  ]
  edge [
    source 164
    target 1413
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 165
    target 1414
  ]
  edge [
    source 165
    target 1415
  ]
  edge [
    source 165
    target 1416
  ]
  edge [
    source 165
    target 1417
  ]
  edge [
    source 166
    target 605
  ]
  edge [
    source 166
    target 1418
  ]
  edge [
    source 166
    target 1419
  ]
  edge [
    source 166
    target 1420
  ]
  edge [
    source 166
    target 1421
  ]
  edge [
    source 166
    target 1422
  ]
  edge [
    source 166
    target 1423
  ]
  edge [
    source 166
    target 717
  ]
  edge [
    source 166
    target 1424
  ]
  edge [
    source 166
    target 1425
  ]
  edge [
    source 166
    target 1426
  ]
  edge [
    source 167
    target 174
  ]
  edge [
    source 167
    target 1427
  ]
  edge [
    source 167
    target 1428
  ]
  edge [
    source 167
    target 1429
  ]
  edge [
    source 167
    target 1013
  ]
  edge [
    source 167
    target 1430
  ]
  edge [
    source 167
    target 811
  ]
  edge [
    source 167
    target 1431
  ]
  edge [
    source 167
    target 1432
  ]
  edge [
    source 167
    target 1433
  ]
  edge [
    source 167
    target 1434
  ]
  edge [
    source 167
    target 1021
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 170
    target 171
  ]
  edge [
    source 170
    target 1435
  ]
  edge [
    source 171
    target 1436
  ]
  edge [
    source 171
    target 1437
  ]
  edge [
    source 171
    target 260
  ]
  edge [
    source 171
    target 240
  ]
  edge [
    source 171
    target 1438
  ]
  edge [
    source 171
    target 1439
  ]
  edge [
    source 171
    target 1440
  ]
  edge [
    source 171
    target 1441
  ]
  edge [
    source 171
    target 1442
  ]
  edge [
    source 171
    target 1443
  ]
  edge [
    source 171
    target 1444
  ]
  edge [
    source 171
    target 1445
  ]
  edge [
    source 171
    target 1348
  ]
  edge [
    source 171
    target 1446
  ]
  edge [
    source 171
    target 1447
  ]
  edge [
    source 171
    target 1448
  ]
  edge [
    source 171
    target 1449
  ]
  edge [
    source 171
    target 1450
  ]
  edge [
    source 171
    target 1451
  ]
  edge [
    source 171
    target 1452
  ]
  edge [
    source 171
    target 1453
  ]
  edge [
    source 171
    target 1454
  ]
  edge [
    source 171
    target 1455
  ]
  edge [
    source 171
    target 379
  ]
  edge [
    source 171
    target 1456
  ]
  edge [
    source 171
    target 1457
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 173
    target 174
  ]
  edge [
    source 173
    target 1458
  ]
  edge [
    source 173
    target 1459
  ]
  edge [
    source 173
    target 1460
  ]
  edge [
    source 173
    target 1461
  ]
  edge [
    source 173
    target 1462
  ]
  edge [
    source 173
    target 1463
  ]
  edge [
    source 173
    target 1464
  ]
  edge [
    source 173
    target 1465
  ]
  edge [
    source 173
    target 930
  ]
  edge [
    source 173
    target 1466
  ]
  edge [
    source 173
    target 1467
  ]
  edge [
    source 173
    target 1468
  ]
  edge [
    source 173
    target 1469
  ]
  edge [
    source 173
    target 1470
  ]
  edge [
    source 173
    target 1471
  ]
  edge [
    source 173
    target 1472
  ]
  edge [
    source 174
    target 1435
  ]
  edge [
    source 174
    target 1473
  ]
  edge [
    source 174
    target 1474
  ]
  edge [
    source 174
    target 1475
  ]
  edge [
    source 174
    target 1476
  ]
  edge [
    source 174
    target 1477
  ]
  edge [
    source 174
    target 1478
  ]
  edge [
    source 174
    target 1479
  ]
  edge [
    source 174
    target 1480
  ]
  edge [
    source 175
    target 176
  ]
  edge [
    source 175
    target 1481
  ]
  edge [
    source 175
    target 1482
  ]
  edge [
    source 175
    target 813
  ]
  edge [
    source 175
    target 1483
  ]
  edge [
    source 175
    target 1484
  ]
  edge [
    source 175
    target 1485
  ]
  edge [
    source 175
    target 407
  ]
  edge [
    source 175
    target 201
  ]
  edge [
    source 175
    target 1486
  ]
  edge [
    source 175
    target 1487
  ]
  edge [
    source 175
    target 341
  ]
  edge [
    source 175
    target 1488
  ]
  edge [
    source 175
    target 1489
  ]
  edge [
    source 175
    target 1490
  ]
  edge [
    source 175
    target 296
  ]
  edge [
    source 175
    target 1491
  ]
  edge [
    source 175
    target 1492
  ]
  edge [
    source 175
    target 1493
  ]
  edge [
    source 175
    target 1494
  ]
  edge [
    source 175
    target 1495
  ]
  edge [
    source 175
    target 1496
  ]
  edge [
    source 175
    target 1497
  ]
  edge [
    source 175
    target 179
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 176
    target 1498
  ]
  edge [
    source 176
    target 1499
  ]
  edge [
    source 176
    target 1468
  ]
  edge [
    source 176
    target 1500
  ]
  edge [
    source 176
    target 1501
  ]
  edge [
    source 176
    target 1502
  ]
  edge [
    source 176
    target 1503
  ]
  edge [
    source 176
    target 1504
  ]
  edge [
    source 176
    target 1505
  ]
  edge [
    source 177
    target 178
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 178
    target 1506
  ]
  edge [
    source 178
    target 1507
  ]
  edge [
    source 178
    target 1508
  ]
  edge [
    source 178
    target 1509
  ]
  edge [
    source 178
    target 528
  ]
  edge [
    source 178
    target 1510
  ]
  edge [
    source 178
    target 1511
  ]
  edge [
    source 178
    target 1512
  ]
  edge [
    source 178
    target 1513
  ]
  edge [
    source 178
    target 1514
  ]
  edge [
    source 178
    target 1515
  ]
  edge [
    source 178
    target 1516
  ]
  edge [
    source 178
    target 1517
  ]
  edge [
    source 178
    target 1518
  ]
  edge [
    source 178
    target 1519
  ]
  edge [
    source 178
    target 1520
  ]
  edge [
    source 178
    target 1521
  ]
  edge [
    source 178
    target 1522
  ]
  edge [
    source 178
    target 1523
  ]
  edge [
    source 178
    target 1524
  ]
  edge [
    source 178
    target 1525
  ]
  edge [
    source 179
    target 180
  ]
  edge [
    source 179
    target 295
  ]
  edge [
    source 179
    target 1526
  ]
  edge [
    source 179
    target 1527
  ]
  edge [
    source 179
    target 1528
  ]
  edge [
    source 179
    target 1529
  ]
  edge [
    source 179
    target 1530
  ]
  edge [
    source 179
    target 1531
  ]
  edge [
    source 179
    target 1532
  ]
  edge [
    source 179
    target 1533
  ]
  edge [
    source 179
    target 1534
  ]
  edge [
    source 179
    target 1535
  ]
  edge [
    source 179
    target 1536
  ]
  edge [
    source 179
    target 438
  ]
  edge [
    source 179
    target 1537
  ]
  edge [
    source 179
    target 1538
  ]
  edge [
    source 179
    target 1539
  ]
  edge [
    source 179
    target 292
  ]
  edge [
    source 179
    target 1540
  ]
  edge [
    source 179
    target 293
  ]
  edge [
    source 179
    target 1541
  ]
  edge [
    source 179
    target 1542
  ]
  edge [
    source 179
    target 297
  ]
  edge [
    source 179
    target 1543
  ]
  edge [
    source 179
    target 302
  ]
  edge [
    source 179
    target 1544
  ]
  edge [
    source 179
    target 294
  ]
  edge [
    source 179
    target 1545
  ]
  edge [
    source 179
    target 1546
  ]
  edge [
    source 179
    target 1547
  ]
  edge [
    source 179
    target 1548
  ]
  edge [
    source 179
    target 305
  ]
  edge [
    source 179
    target 1549
  ]
  edge [
    source 179
    target 187
  ]
  edge [
    source 179
    target 223
  ]
  edge [
    source 179
    target 234
  ]
  edge [
    source 181
    target 182
  ]
  edge [
    source 181
    target 1550
  ]
  edge [
    source 181
    target 1551
  ]
  edge [
    source 182
    target 183
  ]
  edge [
    source 182
    target 1552
  ]
  edge [
    source 182
    target 1553
  ]
  edge [
    source 182
    target 1554
  ]
  edge [
    source 182
    target 1555
  ]
  edge [
    source 183
    target 184
  ]
  edge [
    source 183
    target 212
  ]
  edge [
    source 183
    target 213
  ]
  edge [
    source 183
    target 216
  ]
  edge [
    source 184
    target 217
  ]
  edge [
    source 184
    target 1556
  ]
  edge [
    source 184
    target 1557
  ]
  edge [
    source 184
    target 1527
  ]
  edge [
    source 184
    target 1558
  ]
  edge [
    source 184
    target 1559
  ]
  edge [
    source 184
    target 1560
  ]
  edge [
    source 184
    target 1561
  ]
  edge [
    source 184
    target 1562
  ]
  edge [
    source 184
    target 1563
  ]
  edge [
    source 184
    target 1564
  ]
  edge [
    source 184
    target 1565
  ]
  edge [
    source 184
    target 1566
  ]
  edge [
    source 184
    target 1567
  ]
  edge [
    source 184
    target 1568
  ]
  edge [
    source 184
    target 1569
  ]
  edge [
    source 184
    target 1086
  ]
  edge [
    source 184
    target 1570
  ]
  edge [
    source 184
    target 1117
  ]
  edge [
    source 184
    target 1571
  ]
  edge [
    source 184
    target 1572
  ]
  edge [
    source 184
    target 1573
  ]
  edge [
    source 184
    target 1574
  ]
  edge [
    source 184
    target 1575
  ]
  edge [
    source 184
    target 287
  ]
  edge [
    source 184
    target 1576
  ]
  edge [
    source 184
    target 1577
  ]
  edge [
    source 184
    target 1578
  ]
  edge [
    source 184
    target 296
  ]
  edge [
    source 184
    target 1579
  ]
  edge [
    source 184
    target 599
  ]
  edge [
    source 184
    target 1404
  ]
  edge [
    source 184
    target 1580
  ]
  edge [
    source 184
    target 1581
  ]
  edge [
    source 184
    target 1582
  ]
  edge [
    source 184
    target 1583
  ]
  edge [
    source 185
    target 186
  ]
  edge [
    source 185
    target 1584
  ]
  edge [
    source 185
    target 1585
  ]
  edge [
    source 185
    target 1586
  ]
  edge [
    source 185
    target 298
  ]
  edge [
    source 186
    target 187
  ]
  edge [
    source 186
    target 1587
  ]
  edge [
    source 186
    target 1588
  ]
  edge [
    source 186
    target 1589
  ]
  edge [
    source 186
    target 1306
  ]
  edge [
    source 186
    target 1590
  ]
  edge [
    source 186
    target 1591
  ]
  edge [
    source 186
    target 1592
  ]
  edge [
    source 186
    target 1593
  ]
  edge [
    source 186
    target 1594
  ]
  edge [
    source 186
    target 1595
  ]
  edge [
    source 186
    target 1596
  ]
  edge [
    source 186
    target 1597
  ]
  edge [
    source 186
    target 1598
  ]
  edge [
    source 186
    target 1599
  ]
  edge [
    source 186
    target 1600
  ]
  edge [
    source 186
    target 1601
  ]
  edge [
    source 186
    target 1602
  ]
  edge [
    source 186
    target 1603
  ]
  edge [
    source 186
    target 1604
  ]
  edge [
    source 186
    target 1605
  ]
  edge [
    source 186
    target 1606
  ]
  edge [
    source 186
    target 1607
  ]
  edge [
    source 186
    target 1608
  ]
  edge [
    source 186
    target 1609
  ]
  edge [
    source 186
    target 1610
  ]
  edge [
    source 186
    target 1192
  ]
  edge [
    source 186
    target 517
  ]
  edge [
    source 186
    target 1611
  ]
  edge [
    source 186
    target 1612
  ]
  edge [
    source 186
    target 1613
  ]
  edge [
    source 186
    target 1614
  ]
  edge [
    source 186
    target 1615
  ]
  edge [
    source 186
    target 206
  ]
  edge [
    source 186
    target 1616
  ]
  edge [
    source 187
    target 188
  ]
  edge [
    source 187
    target 223
  ]
  edge [
    source 187
    target 234
  ]
  edge [
    source 187
    target 2143
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 189
    target 190
  ]
  edge [
    source 190
    target 1617
  ]
  edge [
    source 191
    target 192
  ]
  edge [
    source 191
    target 209
  ]
  edge [
    source 191
    target 210
  ]
  edge [
    source 191
    target 212
  ]
  edge [
    source 191
    target 213
  ]
  edge [
    source 191
    target 501
  ]
  edge [
    source 191
    target 1618
  ]
  edge [
    source 191
    target 1619
  ]
  edge [
    source 191
    target 1620
  ]
  edge [
    source 191
    target 1621
  ]
  edge [
    source 191
    target 1622
  ]
  edge [
    source 191
    target 1623
  ]
  edge [
    source 191
    target 1624
  ]
  edge [
    source 191
    target 1625
  ]
  edge [
    source 191
    target 1626
  ]
  edge [
    source 191
    target 236
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 192
    target 194
  ]
  edge [
    source 192
    target 1627
  ]
  edge [
    source 192
    target 1153
  ]
  edge [
    source 192
    target 1628
  ]
  edge [
    source 192
    target 1629
  ]
  edge [
    source 192
    target 624
  ]
  edge [
    source 192
    target 1630
  ]
  edge [
    source 192
    target 1631
  ]
  edge [
    source 192
    target 1632
  ]
  edge [
    source 192
    target 627
  ]
  edge [
    source 192
    target 1633
  ]
  edge [
    source 192
    target 1634
  ]
  edge [
    source 192
    target 1635
  ]
  edge [
    source 192
    target 1636
  ]
  edge [
    source 192
    target 1252
  ]
  edge [
    source 192
    target 1637
  ]
  edge [
    source 192
    target 632
  ]
  edge [
    source 193
    target 1638
  ]
  edge [
    source 193
    target 1639
  ]
  edge [
    source 193
    target 671
  ]
  edge [
    source 193
    target 201
  ]
  edge [
    source 194
    target 195
  ]
  edge [
    source 194
    target 203
  ]
  edge [
    source 195
    target 196
  ]
  edge [
    source 195
    target 218
  ]
  edge [
    source 195
    target 1640
  ]
  edge [
    source 195
    target 1641
  ]
  edge [
    source 195
    target 762
  ]
  edge [
    source 195
    target 1642
  ]
  edge [
    source 196
    target 197
  ]
  edge [
    source 196
    target 1643
  ]
  edge [
    source 196
    target 1644
  ]
  edge [
    source 196
    target 1306
  ]
  edge [
    source 196
    target 1645
  ]
  edge [
    source 196
    target 1646
  ]
  edge [
    source 196
    target 1647
  ]
  edge [
    source 196
    target 1648
  ]
  edge [
    source 196
    target 1649
  ]
  edge [
    source 197
    target 198
  ]
  edge [
    source 197
    target 210
  ]
  edge [
    source 197
    target 211
  ]
  edge [
    source 198
    target 199
  ]
  edge [
    source 199
    target 200
  ]
  edge [
    source 199
    target 1650
  ]
  edge [
    source 199
    target 1651
  ]
  edge [
    source 199
    target 1652
  ]
  edge [
    source 199
    target 1653
  ]
  edge [
    source 199
    target 1654
  ]
  edge [
    source 199
    target 1655
  ]
  edge [
    source 200
    target 201
  ]
  edge [
    source 200
    target 1656
  ]
  edge [
    source 200
    target 1657
  ]
  edge [
    source 200
    target 1658
  ]
  edge [
    source 200
    target 296
  ]
  edge [
    source 200
    target 1659
  ]
  edge [
    source 200
    target 1660
  ]
  edge [
    source 200
    target 1661
  ]
  edge [
    source 201
    target 202
  ]
  edge [
    source 201
    target 1292
  ]
  edge [
    source 201
    target 1662
  ]
  edge [
    source 201
    target 528
  ]
  edge [
    source 201
    target 1663
  ]
  edge [
    source 201
    target 1664
  ]
  edge [
    source 201
    target 1665
  ]
  edge [
    source 201
    target 1666
  ]
  edge [
    source 201
    target 296
  ]
  edge [
    source 201
    target 1667
  ]
  edge [
    source 201
    target 1668
  ]
  edge [
    source 201
    target 1669
  ]
  edge [
    source 201
    target 852
  ]
  edge [
    source 201
    target 417
  ]
  edge [
    source 201
    target 1670
  ]
  edge [
    source 201
    target 1671
  ]
  edge [
    source 202
    target 203
  ]
  edge [
    source 202
    target 1672
  ]
  edge [
    source 202
    target 1251
  ]
  edge [
    source 202
    target 1673
  ]
  edge [
    source 202
    target 630
  ]
  edge [
    source 202
    target 1674
  ]
  edge [
    source 203
    target 204
  ]
  edge [
    source 203
    target 1675
  ]
  edge [
    source 203
    target 1676
  ]
  edge [
    source 203
    target 1677
  ]
  edge [
    source 203
    target 1678
  ]
  edge [
    source 203
    target 1679
  ]
  edge [
    source 203
    target 1680
  ]
  edge [
    source 203
    target 1681
  ]
  edge [
    source 203
    target 1682
  ]
  edge [
    source 203
    target 1683
  ]
  edge [
    source 203
    target 1684
  ]
  edge [
    source 203
    target 1685
  ]
  edge [
    source 203
    target 1686
  ]
  edge [
    source 203
    target 1687
  ]
  edge [
    source 203
    target 1688
  ]
  edge [
    source 203
    target 528
  ]
  edge [
    source 203
    target 1689
  ]
  edge [
    source 203
    target 1690
  ]
  edge [
    source 203
    target 1691
  ]
  edge [
    source 203
    target 1692
  ]
  edge [
    source 203
    target 1693
  ]
  edge [
    source 203
    target 1694
  ]
  edge [
    source 203
    target 1695
  ]
  edge [
    source 203
    target 1696
  ]
  edge [
    source 203
    target 1697
  ]
  edge [
    source 203
    target 1698
  ]
  edge [
    source 203
    target 1699
  ]
  edge [
    source 203
    target 417
  ]
  edge [
    source 203
    target 1700
  ]
  edge [
    source 203
    target 1701
  ]
  edge [
    source 203
    target 1702
  ]
  edge [
    source 203
    target 1703
  ]
  edge [
    source 203
    target 1704
  ]
  edge [
    source 203
    target 1705
  ]
  edge [
    source 203
    target 1706
  ]
  edge [
    source 203
    target 1524
  ]
  edge [
    source 203
    target 1707
  ]
  edge [
    source 203
    target 1708
  ]
  edge [
    source 204
    target 205
  ]
  edge [
    source 204
    target 1709
  ]
  edge [
    source 204
    target 213
  ]
  edge [
    source 205
    target 206
  ]
  edge [
    source 205
    target 1710
  ]
  edge [
    source 205
    target 1711
  ]
  edge [
    source 205
    target 1164
  ]
  edge [
    source 205
    target 1712
  ]
  edge [
    source 205
    target 671
  ]
  edge [
    source 205
    target 1168
  ]
  edge [
    source 205
    target 1713
  ]
  edge [
    source 205
    target 1714
  ]
  edge [
    source 205
    target 1715
  ]
  edge [
    source 205
    target 556
  ]
  edge [
    source 205
    target 1716
  ]
  edge [
    source 205
    target 1717
  ]
  edge [
    source 206
    target 207
  ]
  edge [
    source 206
    target 1718
  ]
  edge [
    source 206
    target 1719
  ]
  edge [
    source 206
    target 1720
  ]
  edge [
    source 206
    target 1721
  ]
  edge [
    source 206
    target 1722
  ]
  edge [
    source 206
    target 1723
  ]
  edge [
    source 206
    target 1724
  ]
  edge [
    source 206
    target 1725
  ]
  edge [
    source 206
    target 1726
  ]
  edge [
    source 206
    target 1727
  ]
  edge [
    source 206
    target 1728
  ]
  edge [
    source 206
    target 1729
  ]
  edge [
    source 206
    target 1730
  ]
  edge [
    source 206
    target 1731
  ]
  edge [
    source 206
    target 1732
  ]
  edge [
    source 206
    target 1733
  ]
  edge [
    source 206
    target 1734
  ]
  edge [
    source 206
    target 1735
  ]
  edge [
    source 206
    target 1736
  ]
  edge [
    source 206
    target 1737
  ]
  edge [
    source 206
    target 1738
  ]
  edge [
    source 206
    target 1739
  ]
  edge [
    source 206
    target 1740
  ]
  edge [
    source 206
    target 1741
  ]
  edge [
    source 206
    target 1742
  ]
  edge [
    source 206
    target 1743
  ]
  edge [
    source 206
    target 1744
  ]
  edge [
    source 206
    target 1745
  ]
  edge [
    source 206
    target 1746
  ]
  edge [
    source 206
    target 1747
  ]
  edge [
    source 206
    target 1748
  ]
  edge [
    source 206
    target 1749
  ]
  edge [
    source 206
    target 1750
  ]
  edge [
    source 206
    target 1751
  ]
  edge [
    source 206
    target 1752
  ]
  edge [
    source 206
    target 1753
  ]
  edge [
    source 206
    target 1754
  ]
  edge [
    source 206
    target 1755
  ]
  edge [
    source 206
    target 1756
  ]
  edge [
    source 206
    target 1757
  ]
  edge [
    source 206
    target 1758
  ]
  edge [
    source 206
    target 1759
  ]
  edge [
    source 206
    target 1760
  ]
  edge [
    source 206
    target 1761
  ]
  edge [
    source 206
    target 1762
  ]
  edge [
    source 206
    target 240
  ]
  edge [
    source 206
    target 1763
  ]
  edge [
    source 206
    target 1764
  ]
  edge [
    source 206
    target 1765
  ]
  edge [
    source 206
    target 1766
  ]
  edge [
    source 206
    target 1767
  ]
  edge [
    source 206
    target 1768
  ]
  edge [
    source 206
    target 1769
  ]
  edge [
    source 206
    target 1770
  ]
  edge [
    source 206
    target 1771
  ]
  edge [
    source 206
    target 1772
  ]
  edge [
    source 206
    target 1773
  ]
  edge [
    source 206
    target 1774
  ]
  edge [
    source 206
    target 1775
  ]
  edge [
    source 206
    target 1776
  ]
  edge [
    source 206
    target 1777
  ]
  edge [
    source 206
    target 1778
  ]
  edge [
    source 206
    target 1779
  ]
  edge [
    source 206
    target 1780
  ]
  edge [
    source 206
    target 1781
  ]
  edge [
    source 206
    target 1782
  ]
  edge [
    source 206
    target 1783
  ]
  edge [
    source 206
    target 1784
  ]
  edge [
    source 206
    target 1785
  ]
  edge [
    source 206
    target 1786
  ]
  edge [
    source 206
    target 1787
  ]
  edge [
    source 206
    target 1788
  ]
  edge [
    source 206
    target 1789
  ]
  edge [
    source 206
    target 1790
  ]
  edge [
    source 206
    target 1791
  ]
  edge [
    source 206
    target 1792
  ]
  edge [
    source 206
    target 1793
  ]
  edge [
    source 206
    target 1794
  ]
  edge [
    source 206
    target 1795
  ]
  edge [
    source 206
    target 1796
  ]
  edge [
    source 206
    target 1797
  ]
  edge [
    source 206
    target 1798
  ]
  edge [
    source 206
    target 1799
  ]
  edge [
    source 206
    target 1800
  ]
  edge [
    source 206
    target 1801
  ]
  edge [
    source 206
    target 1802
  ]
  edge [
    source 206
    target 1803
  ]
  edge [
    source 206
    target 1804
  ]
  edge [
    source 206
    target 1805
  ]
  edge [
    source 206
    target 257
  ]
  edge [
    source 206
    target 1806
  ]
  edge [
    source 206
    target 1807
  ]
  edge [
    source 206
    target 1808
  ]
  edge [
    source 206
    target 1809
  ]
  edge [
    source 206
    target 1810
  ]
  edge [
    source 206
    target 1811
  ]
  edge [
    source 206
    target 1306
  ]
  edge [
    source 206
    target 1812
  ]
  edge [
    source 206
    target 1813
  ]
  edge [
    source 206
    target 1814
  ]
  edge [
    source 206
    target 1815
  ]
  edge [
    source 206
    target 1816
  ]
  edge [
    source 206
    target 1817
  ]
  edge [
    source 206
    target 1818
  ]
  edge [
    source 206
    target 1819
  ]
  edge [
    source 206
    target 1820
  ]
  edge [
    source 206
    target 1821
  ]
  edge [
    source 206
    target 1822
  ]
  edge [
    source 206
    target 1823
  ]
  edge [
    source 206
    target 1824
  ]
  edge [
    source 206
    target 1825
  ]
  edge [
    source 206
    target 1826
  ]
  edge [
    source 206
    target 1827
  ]
  edge [
    source 206
    target 1828
  ]
  edge [
    source 206
    target 1829
  ]
  edge [
    source 206
    target 1830
  ]
  edge [
    source 206
    target 1831
  ]
  edge [
    source 206
    target 1832
  ]
  edge [
    source 206
    target 1833
  ]
  edge [
    source 206
    target 1834
  ]
  edge [
    source 206
    target 1835
  ]
  edge [
    source 206
    target 737
  ]
  edge [
    source 206
    target 1836
  ]
  edge [
    source 206
    target 1837
  ]
  edge [
    source 206
    target 1838
  ]
  edge [
    source 206
    target 1839
  ]
  edge [
    source 206
    target 1840
  ]
  edge [
    source 206
    target 1841
  ]
  edge [
    source 206
    target 1842
  ]
  edge [
    source 206
    target 1843
  ]
  edge [
    source 206
    target 1844
  ]
  edge [
    source 206
    target 1845
  ]
  edge [
    source 206
    target 1846
  ]
  edge [
    source 206
    target 1847
  ]
  edge [
    source 206
    target 1848
  ]
  edge [
    source 206
    target 1849
  ]
  edge [
    source 206
    target 1850
  ]
  edge [
    source 206
    target 1851
  ]
  edge [
    source 206
    target 1852
  ]
  edge [
    source 206
    target 1853
  ]
  edge [
    source 206
    target 1854
  ]
  edge [
    source 206
    target 1855
  ]
  edge [
    source 206
    target 1856
  ]
  edge [
    source 206
    target 1857
  ]
  edge [
    source 206
    target 1858
  ]
  edge [
    source 206
    target 1859
  ]
  edge [
    source 206
    target 1860
  ]
  edge [
    source 206
    target 1861
  ]
  edge [
    source 206
    target 1862
  ]
  edge [
    source 206
    target 1863
  ]
  edge [
    source 206
    target 1864
  ]
  edge [
    source 206
    target 1865
  ]
  edge [
    source 206
    target 1866
  ]
  edge [
    source 206
    target 1867
  ]
  edge [
    source 206
    target 1868
  ]
  edge [
    source 206
    target 1869
  ]
  edge [
    source 206
    target 1870
  ]
  edge [
    source 206
    target 1871
  ]
  edge [
    source 206
    target 1872
  ]
  edge [
    source 206
    target 1873
  ]
  edge [
    source 206
    target 1874
  ]
  edge [
    source 206
    target 1875
  ]
  edge [
    source 206
    target 1876
  ]
  edge [
    source 206
    target 1877
  ]
  edge [
    source 206
    target 1878
  ]
  edge [
    source 206
    target 1879
  ]
  edge [
    source 206
    target 1880
  ]
  edge [
    source 206
    target 1881
  ]
  edge [
    source 206
    target 1882
  ]
  edge [
    source 206
    target 1883
  ]
  edge [
    source 206
    target 1884
  ]
  edge [
    source 206
    target 1885
  ]
  edge [
    source 206
    target 1886
  ]
  edge [
    source 206
    target 1887
  ]
  edge [
    source 206
    target 1888
  ]
  edge [
    source 206
    target 1889
  ]
  edge [
    source 206
    target 1890
  ]
  edge [
    source 206
    target 1891
  ]
  edge [
    source 206
    target 1892
  ]
  edge [
    source 206
    target 1893
  ]
  edge [
    source 206
    target 1894
  ]
  edge [
    source 206
    target 1895
  ]
  edge [
    source 206
    target 1896
  ]
  edge [
    source 206
    target 1897
  ]
  edge [
    source 206
    target 1898
  ]
  edge [
    source 206
    target 1899
  ]
  edge [
    source 206
    target 1900
  ]
  edge [
    source 206
    target 1901
  ]
  edge [
    source 206
    target 1902
  ]
  edge [
    source 206
    target 1903
  ]
  edge [
    source 206
    target 1904
  ]
  edge [
    source 206
    target 1905
  ]
  edge [
    source 206
    target 1906
  ]
  edge [
    source 206
    target 1907
  ]
  edge [
    source 206
    target 1908
  ]
  edge [
    source 206
    target 1909
  ]
  edge [
    source 206
    target 1910
  ]
  edge [
    source 206
    target 1911
  ]
  edge [
    source 206
    target 1912
  ]
  edge [
    source 206
    target 1913
  ]
  edge [
    source 206
    target 1914
  ]
  edge [
    source 206
    target 1915
  ]
  edge [
    source 206
    target 1916
  ]
  edge [
    source 206
    target 1917
  ]
  edge [
    source 206
    target 1918
  ]
  edge [
    source 206
    target 1919
  ]
  edge [
    source 206
    target 1920
  ]
  edge [
    source 206
    target 1921
  ]
  edge [
    source 206
    target 1922
  ]
  edge [
    source 206
    target 1923
  ]
  edge [
    source 206
    target 1924
  ]
  edge [
    source 206
    target 1925
  ]
  edge [
    source 206
    target 1926
  ]
  edge [
    source 206
    target 1927
  ]
  edge [
    source 206
    target 1928
  ]
  edge [
    source 207
    target 1929
  ]
  edge [
    source 207
    target 1930
  ]
  edge [
    source 207
    target 1931
  ]
  edge [
    source 207
    target 1932
  ]
  edge [
    source 207
    target 1933
  ]
  edge [
    source 207
    target 1934
  ]
  edge [
    source 207
    target 645
  ]
  edge [
    source 207
    target 1935
  ]
  edge [
    source 207
    target 1936
  ]
  edge [
    source 207
    target 1937
  ]
  edge [
    source 207
    target 1938
  ]
  edge [
    source 207
    target 1939
  ]
  edge [
    source 207
    target 1940
  ]
  edge [
    source 207
    target 1941
  ]
  edge [
    source 207
    target 1942
  ]
  edge [
    source 207
    target 1943
  ]
  edge [
    source 207
    target 1944
  ]
  edge [
    source 208
    target 209
  ]
  edge [
    source 208
    target 1945
  ]
  edge [
    source 208
    target 1946
  ]
  edge [
    source 210
    target 1947
  ]
  edge [
    source 210
    target 1948
  ]
  edge [
    source 210
    target 1949
  ]
  edge [
    source 210
    target 1950
  ]
  edge [
    source 210
    target 222
  ]
  edge [
    source 211
    target 224
  ]
  edge [
    source 211
    target 225
  ]
  edge [
    source 211
    target 1951
  ]
  edge [
    source 211
    target 1952
  ]
  edge [
    source 211
    target 1953
  ]
  edge [
    source 211
    target 1954
  ]
  edge [
    source 211
    target 1955
  ]
  edge [
    source 211
    target 1956
  ]
  edge [
    source 211
    target 1957
  ]
  edge [
    source 211
    target 1958
  ]
  edge [
    source 211
    target 464
  ]
  edge [
    source 212
    target 934
  ]
  edge [
    source 212
    target 1959
  ]
  edge [
    source 212
    target 1960
  ]
  edge [
    source 212
    target 1961
  ]
  edge [
    source 212
    target 1962
  ]
  edge [
    source 212
    target 1963
  ]
  edge [
    source 212
    target 1964
  ]
  edge [
    source 212
    target 1965
  ]
  edge [
    source 212
    target 786
  ]
  edge [
    source 212
    target 1966
  ]
  edge [
    source 212
    target 1967
  ]
  edge [
    source 212
    target 1968
  ]
  edge [
    source 212
    target 1969
  ]
  edge [
    source 212
    target 1184
  ]
  edge [
    source 212
    target 1970
  ]
  edge [
    source 212
    target 1324
  ]
  edge [
    source 212
    target 1190
  ]
  edge [
    source 212
    target 896
  ]
  edge [
    source 212
    target 1971
  ]
  edge [
    source 212
    target 1972
  ]
  edge [
    source 213
    target 1973
  ]
  edge [
    source 213
    target 1974
  ]
  edge [
    source 213
    target 1975
  ]
  edge [
    source 213
    target 1976
  ]
  edge [
    source 213
    target 1977
  ]
  edge [
    source 214
    target 215
  ]
  edge [
    source 214
    target 1435
  ]
  edge [
    source 214
    target 1978
  ]
  edge [
    source 214
    target 1979
  ]
  edge [
    source 214
    target 1980
  ]
  edge [
    source 214
    target 1981
  ]
  edge [
    source 214
    target 1982
  ]
  edge [
    source 214
    target 1983
  ]
  edge [
    source 215
    target 216
  ]
  edge [
    source 216
    target 1984
  ]
  edge [
    source 216
    target 427
  ]
  edge [
    source 216
    target 1985
  ]
  edge [
    source 216
    target 1986
  ]
  edge [
    source 216
    target 1987
  ]
  edge [
    source 216
    target 225
  ]
  edge [
    source 217
    target 218
  ]
  edge [
    source 217
    target 1988
  ]
  edge [
    source 218
    target 219
  ]
  edge [
    source 218
    target 1989
  ]
  edge [
    source 218
    target 1990
  ]
  edge [
    source 218
    target 750
  ]
  edge [
    source 218
    target 1991
  ]
  edge [
    source 218
    target 1992
  ]
  edge [
    source 218
    target 1993
  ]
  edge [
    source 218
    target 1994
  ]
  edge [
    source 218
    target 928
  ]
  edge [
    source 218
    target 1995
  ]
  edge [
    source 218
    target 1996
  ]
  edge [
    source 218
    target 757
  ]
  edge [
    source 218
    target 1997
  ]
  edge [
    source 218
    target 1998
  ]
  edge [
    source 218
    target 1999
  ]
  edge [
    source 218
    target 2000
  ]
  edge [
    source 218
    target 2001
  ]
  edge [
    source 218
    target 1642
  ]
  edge [
    source 218
    target 2002
  ]
  edge [
    source 218
    target 2003
  ]
  edge [
    source 218
    target 2004
  ]
  edge [
    source 218
    target 2005
  ]
  edge [
    source 218
    target 1512
  ]
  edge [
    source 218
    target 2006
  ]
  edge [
    source 218
    target 2007
  ]
  edge [
    source 218
    target 2008
  ]
  edge [
    source 218
    target 2009
  ]
  edge [
    source 218
    target 2010
  ]
  edge [
    source 218
    target 2011
  ]
  edge [
    source 218
    target 2012
  ]
  edge [
    source 218
    target 1519
  ]
  edge [
    source 218
    target 2013
  ]
  edge [
    source 218
    target 2014
  ]
  edge [
    source 218
    target 2015
  ]
  edge [
    source 218
    target 2016
  ]
  edge [
    source 218
    target 2017
  ]
  edge [
    source 218
    target 2018
  ]
  edge [
    source 218
    target 2019
  ]
  edge [
    source 218
    target 233
  ]
  edge [
    source 219
    target 220
  ]
  edge [
    source 220
    target 221
  ]
  edge [
    source 220
    target 2020
  ]
  edge [
    source 220
    target 2021
  ]
  edge [
    source 220
    target 2022
  ]
  edge [
    source 220
    target 2023
  ]
  edge [
    source 220
    target 2024
  ]
  edge [
    source 220
    target 2025
  ]
  edge [
    source 220
    target 1432
  ]
  edge [
    source 220
    target 2026
  ]
  edge [
    source 220
    target 881
  ]
  edge [
    source 220
    target 2027
  ]
  edge [
    source 220
    target 2028
  ]
  edge [
    source 220
    target 2029
  ]
  edge [
    source 220
    target 2030
  ]
  edge [
    source 221
    target 222
  ]
  edge [
    source 222
    target 223
  ]
  edge [
    source 222
    target 2031
  ]
  edge [
    source 222
    target 2032
  ]
  edge [
    source 222
    target 2033
  ]
  edge [
    source 222
    target 617
  ]
  edge [
    source 222
    target 1103
  ]
  edge [
    source 223
    target 224
  ]
  edge [
    source 223
    target 2034
  ]
  edge [
    source 223
    target 2035
  ]
  edge [
    source 223
    target 2036
  ]
  edge [
    source 223
    target 242
  ]
  edge [
    source 223
    target 2037
  ]
  edge [
    source 223
    target 1168
  ]
  edge [
    source 223
    target 2038
  ]
  edge [
    source 223
    target 2039
  ]
  edge [
    source 223
    target 2040
  ]
  edge [
    source 223
    target 2041
  ]
  edge [
    source 223
    target 2042
  ]
  edge [
    source 223
    target 2043
  ]
  edge [
    source 223
    target 1269
  ]
  edge [
    source 223
    target 296
  ]
  edge [
    source 223
    target 2044
  ]
  edge [
    source 223
    target 2045
  ]
  edge [
    source 223
    target 2046
  ]
  edge [
    source 223
    target 2047
  ]
  edge [
    source 223
    target 1618
  ]
  edge [
    source 223
    target 2048
  ]
  edge [
    source 223
    target 2049
  ]
  edge [
    source 223
    target 2050
  ]
  edge [
    source 223
    target 2051
  ]
  edge [
    source 223
    target 234
  ]
  edge [
    source 224
    target 2052
  ]
  edge [
    source 224
    target 2053
  ]
  edge [
    source 224
    target 2054
  ]
  edge [
    source 224
    target 693
  ]
  edge [
    source 224
    target 2055
  ]
  edge [
    source 224
    target 2056
  ]
  edge [
    source 224
    target 1246
  ]
  edge [
    source 224
    target 2057
  ]
  edge [
    source 224
    target 1325
  ]
  edge [
    source 225
    target 226
  ]
  edge [
    source 225
    target 227
  ]
  edge [
    source 225
    target 609
  ]
  edge [
    source 225
    target 2058
  ]
  edge [
    source 225
    target 2059
  ]
  edge [
    source 225
    target 2060
  ]
  edge [
    source 225
    target 2061
  ]
  edge [
    source 225
    target 2062
  ]
  edge [
    source 225
    target 717
  ]
  edge [
    source 225
    target 2063
  ]
  edge [
    source 225
    target 2064
  ]
  edge [
    source 225
    target 1021
  ]
  edge [
    source 225
    target 2065
  ]
  edge [
    source 226
    target 2066
  ]
  edge [
    source 226
    target 2067
  ]
  edge [
    source 227
    target 228
  ]
  edge [
    source 227
    target 1307
  ]
  edge [
    source 227
    target 2068
  ]
  edge [
    source 227
    target 2069
  ]
  edge [
    source 227
    target 2070
  ]
  edge [
    source 227
    target 2071
  ]
  edge [
    source 227
    target 1161
  ]
  edge [
    source 227
    target 2072
  ]
  edge [
    source 229
    target 230
  ]
  edge [
    source 229
    target 877
  ]
  edge [
    source 229
    target 2073
  ]
  edge [
    source 229
    target 2074
  ]
  edge [
    source 229
    target 340
  ]
  edge [
    source 229
    target 2075
  ]
  edge [
    source 229
    target 2076
  ]
  edge [
    source 229
    target 2077
  ]
  edge [
    source 229
    target 2078
  ]
  edge [
    source 229
    target 983
  ]
  edge [
    source 230
    target 231
  ]
  edge [
    source 230
    target 2079
  ]
  edge [
    source 230
    target 2080
  ]
  edge [
    source 230
    target 2081
  ]
  edge [
    source 230
    target 2082
  ]
  edge [
    source 230
    target 2083
  ]
  edge [
    source 231
    target 232
  ]
  edge [
    source 231
    target 2084
  ]
  edge [
    source 231
    target 768
  ]
  edge [
    source 231
    target 2085
  ]
  edge [
    source 231
    target 2086
  ]
  edge [
    source 232
    target 233
  ]
  edge [
    source 232
    target 2087
  ]
  edge [
    source 232
    target 354
  ]
  edge [
    source 232
    target 2088
  ]
  edge [
    source 232
    target 2089
  ]
  edge [
    source 232
    target 360
  ]
  edge [
    source 233
    target 234
  ]
  edge [
    source 233
    target 2090
  ]
  edge [
    source 233
    target 2091
  ]
  edge [
    source 233
    target 2092
  ]
  edge [
    source 233
    target 671
  ]
  edge [
    source 233
    target 2093
  ]
  edge [
    source 233
    target 2094
  ]
  edge [
    source 233
    target 2095
  ]
  edge [
    source 233
    target 2096
  ]
  edge [
    source 233
    target 2097
  ]
  edge [
    source 233
    target 2098
  ]
  edge [
    source 234
    target 235
  ]
  edge [
    source 234
    target 2099
  ]
  edge [
    source 234
    target 2100
  ]
  edge [
    source 234
    target 279
  ]
  edge [
    source 234
    target 1549
  ]
  edge [
    source 235
    target 2101
  ]
  edge [
    source 235
    target 1318
  ]
  edge [
    source 235
    target 1259
  ]
  edge [
    source 235
    target 2102
  ]
  edge [
    source 235
    target 260
  ]
  edge [
    source 235
    target 2103
  ]
  edge [
    source 235
    target 2104
  ]
  edge [
    source 235
    target 1321
  ]
  edge [
    source 235
    target 2105
  ]
  edge [
    source 235
    target 2106
  ]
  edge [
    source 235
    target 2107
  ]
  edge [
    source 235
    target 2108
  ]
  edge [
    source 235
    target 2109
  ]
  edge [
    source 235
    target 2110
  ]
  edge [
    source 235
    target 2111
  ]
  edge [
    source 235
    target 1320
  ]
  edge [
    source 235
    target 2112
  ]
  edge [
    source 235
    target 2113
  ]
  edge [
    source 235
    target 2114
  ]
  edge [
    source 235
    target 2115
  ]
  edge [
    source 235
    target 1323
  ]
  edge [
    source 235
    target 1325
  ]
  edge [
    source 235
    target 2116
  ]
  edge [
    source 235
    target 2117
  ]
  edge [
    source 235
    target 949
  ]
  edge [
    source 235
    target 2118
  ]
  edge [
    source 235
    target 1100
  ]
  edge [
    source 235
    target 2119
  ]
  edge [
    source 235
    target 1189
  ]
  edge [
    source 235
    target 2120
  ]
  edge [
    source 235
    target 2121
  ]
  edge [
    source 235
    target 2122
  ]
  edge [
    source 235
    target 2123
  ]
  edge [
    source 235
    target 2124
  ]
  edge [
    source 235
    target 2125
  ]
  edge [
    source 235
    target 2126
  ]
  edge [
    source 235
    target 2127
  ]
  edge [
    source 235
    target 2128
  ]
  edge [
    source 235
    target 2129
  ]
  edge [
    source 235
    target 2130
  ]
  edge [
    source 235
    target 2131
  ]
  edge [
    source 235
    target 2132
  ]
  edge [
    source 235
    target 2133
  ]
  edge [
    source 235
    target 2134
  ]
  edge [
    source 236
    target 2135
  ]
  edge [
    source 236
    target 2136
  ]
  edge [
    source 236
    target 296
  ]
  edge [
    source 236
    target 2137
  ]
  edge [
    source 236
    target 2138
  ]
  edge [
    source 2144
    target 2145
  ]
]
