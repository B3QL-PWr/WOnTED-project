graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.935483870967742
  density 0.06451612903225806
  graphCliqueNumber 2
  node [
    id 0
    label "taormina"
    origin "text"
  ]
  node [
    id 1
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 2
    label "poleca&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zamawia&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pizza"
    origin "text"
  ]
  node [
    id 5
    label "tam"
    origin "text"
  ]
  node [
    id 6
    label "control"
  ]
  node [
    id 7
    label "placard"
  ]
  node [
    id 8
    label "m&#243;wi&#263;"
  ]
  node [
    id 9
    label "charge"
  ]
  node [
    id 10
    label "zadawa&#263;"
  ]
  node [
    id 11
    label "ordynowa&#263;"
  ]
  node [
    id 12
    label "powierza&#263;"
  ]
  node [
    id 13
    label "wydawa&#263;"
  ]
  node [
    id 14
    label "doradza&#263;"
  ]
  node [
    id 15
    label "zamawianie"
  ]
  node [
    id 16
    label "szeptucha"
  ]
  node [
    id 17
    label "umawia&#263;_si&#281;"
  ]
  node [
    id 18
    label "czarowa&#263;"
  ]
  node [
    id 19
    label "order"
  ]
  node [
    id 20
    label "bespeak"
  ]
  node [
    id 21
    label "zam&#243;wienie"
  ]
  node [
    id 22
    label "rezerwowa&#263;"
  ]
  node [
    id 23
    label "zleca&#263;"
  ]
  node [
    id 24
    label "zam&#243;wi&#263;"
  ]
  node [
    id 25
    label "zg&#322;asza&#263;"
  ]
  node [
    id 26
    label "indenture"
  ]
  node [
    id 27
    label "wypiek"
  ]
  node [
    id 28
    label "fast_food"
  ]
  node [
    id 29
    label "sp&#243;d"
  ]
  node [
    id 30
    label "tu"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 30
  ]
]
