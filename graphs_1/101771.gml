graph [
  maxDegree 12
  minDegree 1
  meanDegree 2
  density 0.03508771929824561
  graphCliqueNumber 3
  node [
    id 0
    label "&#347;nieg"
    origin "text"
  ]
  node [
    id 1
    label "mr&#243;z"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "spacer"
    origin "text"
  ]
  node [
    id 4
    label "ale"
    origin "text"
  ]
  node [
    id 5
    label "nie"
    origin "text"
  ]
  node [
    id 6
    label "oznaka"
    origin "text"
  ]
  node [
    id 7
    label "wiosna"
    origin "text"
  ]
  node [
    id 8
    label "musza"
    origin "text"
  ]
  node [
    id 9
    label "uzbroi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "cierpliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "pozdrawia&#263;"
    origin "text"
  ]
  node [
    id 13
    label "cieplutko"
    origin "text"
  ]
  node [
    id 14
    label "sypni&#281;cie"
  ]
  node [
    id 15
    label "opad"
  ]
  node [
    id 16
    label "pow&#322;oka"
  ]
  node [
    id 17
    label "sypn&#261;&#263;"
  ]
  node [
    id 18
    label "rakieta"
  ]
  node [
    id 19
    label "kokaina"
  ]
  node [
    id 20
    label "zimno"
  ]
  node [
    id 21
    label "niepogoda"
  ]
  node [
    id 22
    label "mro&#380;enie"
  ]
  node [
    id 23
    label "mrozi&#263;"
  ]
  node [
    id 24
    label "jednostopniowy"
  ]
  node [
    id 25
    label "temperatura"
  ]
  node [
    id 26
    label "k&#261;sa&#263;"
  ]
  node [
    id 27
    label "k&#261;sanie"
  ]
  node [
    id 28
    label "zjawisko"
  ]
  node [
    id 29
    label "przymrozek"
  ]
  node [
    id 30
    label "prezentacja"
  ]
  node [
    id 31
    label "natural_process"
  ]
  node [
    id 32
    label "czynno&#347;&#263;"
  ]
  node [
    id 33
    label "ruch"
  ]
  node [
    id 34
    label "piwo"
  ]
  node [
    id 35
    label "sprzeciw"
  ]
  node [
    id 36
    label "fakt"
  ]
  node [
    id 37
    label "signal"
  ]
  node [
    id 38
    label "implikowa&#263;"
  ]
  node [
    id 39
    label "symbol"
  ]
  node [
    id 40
    label "nowalijka"
  ]
  node [
    id 41
    label "pora_roku"
  ]
  node [
    id 42
    label "przedn&#243;wek"
  ]
  node [
    id 43
    label "rok"
  ]
  node [
    id 44
    label "pocz&#261;tek"
  ]
  node [
    id 45
    label "zwiesna"
  ]
  node [
    id 46
    label "zainstalowa&#263;"
  ]
  node [
    id 47
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 48
    label "prime"
  ]
  node [
    id 49
    label "wytrwa&#322;o&#347;&#263;"
  ]
  node [
    id 50
    label "pacjencja"
  ]
  node [
    id 51
    label "patience"
  ]
  node [
    id 52
    label "robi&#263;"
  ]
  node [
    id 53
    label "greet"
  ]
  node [
    id 54
    label "cieplutki"
  ]
  node [
    id 55
    label "milutko"
  ]
  node [
    id 56
    label "heartily"
  ]
  node [
    id 57
    label "przyjemnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 13
    target 55
  ]
  edge [
    source 13
    target 56
  ]
  edge [
    source 13
    target 57
  ]
]
