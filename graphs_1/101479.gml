graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "dalmierz"
    origin "text"
  ]
  node [
    id 1
    label "tachymetria"
  ]
  node [
    id 2
    label "miernik"
  ]
  node [
    id 3
    label "aparat_dalmierzowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
]
