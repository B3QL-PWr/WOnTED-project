graph [
  maxDegree 24
  minDegree 1
  meanDegree 2.030456852791878
  density 0.010359473738734073
  graphCliqueNumber 3
  node [
    id 0
    label "nawet"
    origin "text"
  ]
  node [
    id 1
    label "tysi&#261;c"
    origin "text"
  ]
  node [
    id 2
    label "podpis"
    origin "text"
  ]
  node [
    id 3
    label "pod"
    origin "text"
  ]
  node [
    id 4
    label "projekt"
    origin "text"
  ]
  node [
    id 5
    label "ustawa"
    origin "text"
  ]
  node [
    id 6
    label "wprowadza&#263;"
    origin "text"
  ]
  node [
    id 7
    label "obowi&#261;zkowy"
    origin "text"
  ]
  node [
    id 8
    label "szczepienie"
    origin "text"
  ]
  node [
    id 9
    label "kryterium"
    origin "text"
  ]
  node [
    id 10
    label "przyjmowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "dziecko"
    origin "text"
  ]
  node [
    id 12
    label "&#380;&#322;obek"
    origin "text"
  ]
  node [
    id 13
    label "przedszkole"
    origin "text"
  ]
  node [
    id 14
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 15
    label "zebra&#263;"
    origin "text"
  ]
  node [
    id 16
    label "wroc&#322;awianin"
    origin "text"
  ]
  node [
    id 17
    label "rama"
    origin "text"
  ]
  node [
    id 18
    label "akcja"
    origin "text"
  ]
  node [
    id 19
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 20
    label "tauzen"
  ]
  node [
    id 21
    label "musik"
  ]
  node [
    id 22
    label "molarity"
  ]
  node [
    id 23
    label "licytacja"
  ]
  node [
    id 24
    label "patyk"
  ]
  node [
    id 25
    label "liczba"
  ]
  node [
    id 26
    label "gra_w_karty"
  ]
  node [
    id 27
    label "kwota"
  ]
  node [
    id 28
    label "w&#322;asnor&#281;czny"
  ]
  node [
    id 29
    label "napis"
  ]
  node [
    id 30
    label "obja&#347;nienie"
  ]
  node [
    id 31
    label "sign"
  ]
  node [
    id 32
    label "potwierdzenie"
  ]
  node [
    id 33
    label "signal"
  ]
  node [
    id 34
    label "znak"
  ]
  node [
    id 35
    label "dokument"
  ]
  node [
    id 36
    label "device"
  ]
  node [
    id 37
    label "program_u&#380;ytkowy"
  ]
  node [
    id 38
    label "intencja"
  ]
  node [
    id 39
    label "agreement"
  ]
  node [
    id 40
    label "pomys&#322;"
  ]
  node [
    id 41
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 42
    label "plan"
  ]
  node [
    id 43
    label "dokumentacja"
  ]
  node [
    id 44
    label "Karta_Nauczyciela"
  ]
  node [
    id 45
    label "marc&#243;wka"
  ]
  node [
    id 46
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 47
    label "akt"
  ]
  node [
    id 48
    label "przej&#347;&#263;"
  ]
  node [
    id 49
    label "charter"
  ]
  node [
    id 50
    label "przej&#347;cie"
  ]
  node [
    id 51
    label "rynek"
  ]
  node [
    id 52
    label "zapoznawa&#263;"
  ]
  node [
    id 53
    label "take"
  ]
  node [
    id 54
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 55
    label "wchodzi&#263;"
  ]
  node [
    id 56
    label "begin"
  ]
  node [
    id 57
    label "wprawia&#263;"
  ]
  node [
    id 58
    label "schodzi&#263;"
  ]
  node [
    id 59
    label "wpisywa&#263;"
  ]
  node [
    id 60
    label "robi&#263;"
  ]
  node [
    id 61
    label "umieszcza&#263;"
  ]
  node [
    id 62
    label "induct"
  ]
  node [
    id 63
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 64
    label "zaczyna&#263;"
  ]
  node [
    id 65
    label "doprowadza&#263;"
  ]
  node [
    id 66
    label "inflict"
  ]
  node [
    id 67
    label "powodowa&#263;"
  ]
  node [
    id 68
    label "konieczny"
  ]
  node [
    id 69
    label "porz&#261;dny"
  ]
  node [
    id 70
    label "obligatoryjnie"
  ]
  node [
    id 71
    label "obbligato"
  ]
  node [
    id 72
    label "kursowy"
  ]
  node [
    id 73
    label "wariolacja"
  ]
  node [
    id 74
    label "immunizacja"
  ]
  node [
    id 75
    label "zabieg"
  ]
  node [
    id 76
    label "uodpornianie"
  ]
  node [
    id 77
    label "inoculation"
  ]
  node [
    id 78
    label "uszlachetnianie"
  ]
  node [
    id 79
    label "uprawianie"
  ]
  node [
    id 80
    label "metoda"
  ]
  node [
    id 81
    label "immunizacja_czynna"
  ]
  node [
    id 82
    label "ocena"
  ]
  node [
    id 83
    label "czynnik"
  ]
  node [
    id 84
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 85
    label "poch&#322;ania&#263;"
  ]
  node [
    id 86
    label "dostarcza&#263;"
  ]
  node [
    id 87
    label "uznawa&#263;"
  ]
  node [
    id 88
    label "swallow"
  ]
  node [
    id 89
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 90
    label "admit"
  ]
  node [
    id 91
    label "fall"
  ]
  node [
    id 92
    label "undertake"
  ]
  node [
    id 93
    label "dopuszcza&#263;"
  ]
  node [
    id 94
    label "wyprawia&#263;"
  ]
  node [
    id 95
    label "wpuszcza&#263;"
  ]
  node [
    id 96
    label "close"
  ]
  node [
    id 97
    label "przyjmowanie"
  ]
  node [
    id 98
    label "obiera&#263;"
  ]
  node [
    id 99
    label "pracowa&#263;"
  ]
  node [
    id 100
    label "bra&#263;"
  ]
  node [
    id 101
    label "zgadza&#263;_si&#281;"
  ]
  node [
    id 102
    label "odbiera&#263;"
  ]
  node [
    id 103
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 104
    label "cz&#322;owiek"
  ]
  node [
    id 105
    label "potomstwo"
  ]
  node [
    id 106
    label "organizm"
  ]
  node [
    id 107
    label "sraluch"
  ]
  node [
    id 108
    label "utulanie"
  ]
  node [
    id 109
    label "pediatra"
  ]
  node [
    id 110
    label "dzieciarnia"
  ]
  node [
    id 111
    label "m&#322;odziak"
  ]
  node [
    id 112
    label "dzieciak"
  ]
  node [
    id 113
    label "utula&#263;"
  ]
  node [
    id 114
    label "potomek"
  ]
  node [
    id 115
    label "entliczek-pentliczek"
  ]
  node [
    id 116
    label "pedofil"
  ]
  node [
    id 117
    label "m&#322;odzik"
  ]
  node [
    id 118
    label "cz&#322;owieczek"
  ]
  node [
    id 119
    label "zwierz&#281;"
  ]
  node [
    id 120
    label "niepe&#322;noletni"
  ]
  node [
    id 121
    label "fledgling"
  ]
  node [
    id 122
    label "utuli&#263;"
  ]
  node [
    id 123
    label "utulenie"
  ]
  node [
    id 124
    label "izba_wytrze&#378;wie&#324;"
  ]
  node [
    id 125
    label "wci&#281;cie"
  ]
  node [
    id 126
    label "plac&#243;wka_opieku&#324;czo-wychowawcza"
  ]
  node [
    id 127
    label "farsa"
  ]
  node [
    id 128
    label "cyrk"
  ]
  node [
    id 129
    label "plac_zabaw"
  ]
  node [
    id 130
    label "stopek"
  ]
  node [
    id 131
    label "grupa"
  ]
  node [
    id 132
    label "siedziba"
  ]
  node [
    id 133
    label "dziecinada"
  ]
  node [
    id 134
    label "warunki"
  ]
  node [
    id 135
    label "czu&#263;"
  ]
  node [
    id 136
    label "desire"
  ]
  node [
    id 137
    label "kcie&#263;"
  ]
  node [
    id 138
    label "wzi&#261;&#263;"
  ]
  node [
    id 139
    label "spowodowa&#263;"
  ]
  node [
    id 140
    label "sprz&#261;tn&#261;&#263;"
  ]
  node [
    id 141
    label "przyci&#261;gn&#261;&#263;"
  ]
  node [
    id 142
    label "wezbra&#263;"
  ]
  node [
    id 143
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 144
    label "congregate"
  ]
  node [
    id 145
    label "oszcz&#281;dzi&#263;"
  ]
  node [
    id 146
    label "dosta&#263;"
  ]
  node [
    id 147
    label "pozyska&#263;"
  ]
  node [
    id 148
    label "zgromadzi&#263;"
  ]
  node [
    id 149
    label "przej&#261;&#263;"
  ]
  node [
    id 150
    label "umie&#347;ci&#263;"
  ]
  node [
    id 151
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 152
    label "raise"
  ]
  node [
    id 153
    label "skupi&#263;"
  ]
  node [
    id 154
    label "nat&#281;&#380;y&#263;"
  ]
  node [
    id 155
    label "plane"
  ]
  node [
    id 156
    label "Dolno&#347;l&#261;zak"
  ]
  node [
    id 157
    label "mieszkaniec"
  ]
  node [
    id 158
    label "zakres"
  ]
  node [
    id 159
    label "dodatek"
  ]
  node [
    id 160
    label "struktura"
  ]
  node [
    id 161
    label "stela&#380;"
  ]
  node [
    id 162
    label "za&#322;o&#380;enie"
  ]
  node [
    id 163
    label "human_body"
  ]
  node [
    id 164
    label "szablon"
  ]
  node [
    id 165
    label "oprawa"
  ]
  node [
    id 166
    label "paczka"
  ]
  node [
    id 167
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 168
    label "obramowanie"
  ]
  node [
    id 169
    label "pojazd"
  ]
  node [
    id 170
    label "postawa"
  ]
  node [
    id 171
    label "element_konstrukcyjny"
  ]
  node [
    id 172
    label "zagrywka"
  ]
  node [
    id 173
    label "czyn"
  ]
  node [
    id 174
    label "czynno&#347;&#263;"
  ]
  node [
    id 175
    label "wysoko&#347;&#263;"
  ]
  node [
    id 176
    label "stock"
  ]
  node [
    id 177
    label "gra"
  ]
  node [
    id 178
    label "w&#281;ze&#322;"
  ]
  node [
    id 179
    label "instrument_strunowy"
  ]
  node [
    id 180
    label "dywidenda"
  ]
  node [
    id 181
    label "przebieg"
  ]
  node [
    id 182
    label "udzia&#322;"
  ]
  node [
    id 183
    label "occupation"
  ]
  node [
    id 184
    label "jazda"
  ]
  node [
    id 185
    label "wydarzenie"
  ]
  node [
    id 186
    label "commotion"
  ]
  node [
    id 187
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 188
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 189
    label "operacja"
  ]
  node [
    id 190
    label "take_care"
  ]
  node [
    id 191
    label "troska&#263;_si&#281;"
  ]
  node [
    id 192
    label "zamierza&#263;"
  ]
  node [
    id 193
    label "os&#261;dza&#263;"
  ]
  node [
    id 194
    label "argue"
  ]
  node [
    id 195
    label "rozpatrywa&#263;"
  ]
  node [
    id 196
    label "deliver"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 60
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
]
