graph [
  maxDegree 48
  minDegree 1
  meanDegree 2.358974358974359
  density 0.020335985853227233
  graphCliqueNumber 7
  node [
    id 0
    label "dni"
    origin "text"
  ]
  node [
    id 1
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 3
    label "pinia"
    origin "text"
  ]
  node [
    id 4
    label "sokolnik"
    origin "text"
  ]
  node [
    id 5
    label "le&#347;"
    origin "text"
  ]
  node [
    id 6
    label "ula"
    origin "text"
  ]
  node [
    id 7
    label "sokolnicki"
    origin "text"
  ]
  node [
    id 8
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 11
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 12
    label "okr&#261;g"
    origin "text"
  ]
  node [
    id 13
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 14
    label "bryd&#380;"
    origin "text"
  ]
  node [
    id 15
    label "sportowy"
    origin "text"
  ]
  node [
    id 16
    label "czas"
  ]
  node [
    id 17
    label "miesi&#261;c"
  ]
  node [
    id 18
    label "miejsce"
  ]
  node [
    id 19
    label "Hollywood"
  ]
  node [
    id 20
    label "zal&#261;&#380;ek"
  ]
  node [
    id 21
    label "otoczenie"
  ]
  node [
    id 22
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 23
    label "&#347;rodek"
  ]
  node [
    id 24
    label "center"
  ]
  node [
    id 25
    label "instytucja"
  ]
  node [
    id 26
    label "skupisko"
  ]
  node [
    id 27
    label "warunki"
  ]
  node [
    id 28
    label "orzeszek_piniowy"
  ]
  node [
    id 29
    label "sosna"
  ]
  node [
    id 30
    label "stone_pine"
  ]
  node [
    id 31
    label "iglak"
  ]
  node [
    id 32
    label "drzewo"
  ]
  node [
    id 33
    label "treser"
  ]
  node [
    id 34
    label "hodowca"
  ]
  node [
    id 35
    label "my&#347;liwy"
  ]
  node [
    id 36
    label "reserve"
  ]
  node [
    id 37
    label "przej&#347;&#263;"
  ]
  node [
    id 38
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 39
    label "championship"
  ]
  node [
    id 40
    label "Formu&#322;a_1"
  ]
  node [
    id 41
    label "zawody"
  ]
  node [
    id 42
    label "Mickiewicz"
  ]
  node [
    id 43
    label "szkolenie"
  ]
  node [
    id 44
    label "przepisa&#263;"
  ]
  node [
    id 45
    label "lesson"
  ]
  node [
    id 46
    label "grupa"
  ]
  node [
    id 47
    label "praktyka"
  ]
  node [
    id 48
    label "metoda"
  ]
  node [
    id 49
    label "niepokalanki"
  ]
  node [
    id 50
    label "kara"
  ]
  node [
    id 51
    label "zda&#263;"
  ]
  node [
    id 52
    label "form"
  ]
  node [
    id 53
    label "kwalifikacje"
  ]
  node [
    id 54
    label "system"
  ]
  node [
    id 55
    label "przepisanie"
  ]
  node [
    id 56
    label "sztuba"
  ]
  node [
    id 57
    label "wiedza"
  ]
  node [
    id 58
    label "stopek"
  ]
  node [
    id 59
    label "school"
  ]
  node [
    id 60
    label "absolwent"
  ]
  node [
    id 61
    label "urszulanki"
  ]
  node [
    id 62
    label "gabinet"
  ]
  node [
    id 63
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 64
    label "ideologia"
  ]
  node [
    id 65
    label "lekcja"
  ]
  node [
    id 66
    label "muzyka"
  ]
  node [
    id 67
    label "podr&#281;cznik"
  ]
  node [
    id 68
    label "zdanie"
  ]
  node [
    id 69
    label "siedziba"
  ]
  node [
    id 70
    label "sekretariat"
  ]
  node [
    id 71
    label "nauka"
  ]
  node [
    id 72
    label "do&#347;wiadczenie"
  ]
  node [
    id 73
    label "tablica"
  ]
  node [
    id 74
    label "teren_szko&#322;y"
  ]
  node [
    id 75
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 76
    label "skolaryzacja"
  ]
  node [
    id 77
    label "&#322;awa_szkolna"
  ]
  node [
    id 78
    label "klasa"
  ]
  node [
    id 79
    label "&#322;uk"
  ]
  node [
    id 80
    label "circumference"
  ]
  node [
    id 81
    label "figura_p&#322;aska"
  ]
  node [
    id 82
    label "circle"
  ]
  node [
    id 83
    label "figura_geometryczna"
  ]
  node [
    id 84
    label "ko&#322;o"
  ]
  node [
    id 85
    label "sport"
  ]
  node [
    id 86
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 87
    label "longer"
  ]
  node [
    id 88
    label "odwrotka"
  ]
  node [
    id 89
    label "kontrakt"
  ]
  node [
    id 90
    label "licytacja"
  ]
  node [
    id 91
    label "odzywanie_si&#281;"
  ]
  node [
    id 92
    label "odezwanie_si&#281;"
  ]
  node [
    id 93
    label "rober"
  ]
  node [
    id 94
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 95
    label "korona"
  ]
  node [
    id 96
    label "inwit"
  ]
  node [
    id 97
    label "gra_w_karty"
  ]
  node [
    id 98
    label "rekontra"
  ]
  node [
    id 99
    label "specjalny"
  ]
  node [
    id 100
    label "na_sportowo"
  ]
  node [
    id 101
    label "uczciwy"
  ]
  node [
    id 102
    label "wygodny"
  ]
  node [
    id 103
    label "pe&#322;ny"
  ]
  node [
    id 104
    label "sportowo"
  ]
  node [
    id 105
    label "las"
  ]
  node [
    id 106
    label "mistrzostwo"
  ]
  node [
    id 107
    label "okr&#281;g"
  ]
  node [
    id 108
    label "&#322;&#243;dzki"
  ]
  node [
    id 109
    label "wyspa"
  ]
  node [
    id 110
    label "powiat"
  ]
  node [
    id 111
    label "zgierski"
  ]
  node [
    id 112
    label "zwi&#261;zka"
  ]
  node [
    id 113
    label "wojew&#243;dzki"
  ]
  node [
    id 114
    label "zwi&#261;zek"
  ]
  node [
    id 115
    label "W&#322;odzimierz"
  ]
  node [
    id 116
    label "Choinkowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 42
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 11
    target 44
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 108
  ]
  edge [
    source 106
    target 109
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 109
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 112
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 115
    target 116
  ]
]
