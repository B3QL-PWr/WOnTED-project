graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9333333333333333
  density 0.06666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "dlaczego"
    origin "text"
  ]
  node [
    id 1
    label "pszczelarz"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 3
    label "&#380;y&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zgoda"
    origin "text"
  ]
  node [
    id 5
    label "hodowca"
  ]
  node [
    id 6
    label "by&#263;"
  ]
  node [
    id 7
    label "uprawi&#263;"
  ]
  node [
    id 8
    label "gotowy"
  ]
  node [
    id 9
    label "might"
  ]
  node [
    id 10
    label "pause"
  ]
  node [
    id 11
    label "stay"
  ]
  node [
    id 12
    label "consist"
  ]
  node [
    id 13
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 14
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 15
    label "istnie&#263;"
  ]
  node [
    id 16
    label "zwolnienie_si&#281;"
  ]
  node [
    id 17
    label "spok&#243;j"
  ]
  node [
    id 18
    label "zwalnianie_si&#281;"
  ]
  node [
    id 19
    label "odpowied&#378;"
  ]
  node [
    id 20
    label "wiedza"
  ]
  node [
    id 21
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 22
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 23
    label "entity"
  ]
  node [
    id 24
    label "agreement"
  ]
  node [
    id 25
    label "consensus"
  ]
  node [
    id 26
    label "decyzja"
  ]
  node [
    id 27
    label "jednomy&#347;lno&#347;&#263;"
  ]
  node [
    id 28
    label "license"
  ]
  node [
    id 29
    label "pozwole&#324;stwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
]
