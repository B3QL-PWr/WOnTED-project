graph [
  maxDegree 593
  minDegree 1
  meanDegree 2.0232288037166084
  density 0.0023525916322286147
  graphCliqueNumber 3
  node [
    id 0
    label "prezydent"
    origin "text"
  ]
  node [
    id 1
    label "miasto"
    origin "text"
  ]
  node [
    id 2
    label "gdynia"
    origin "text"
  ]
  node [
    id 3
    label "og&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "otwarty"
    origin "text"
  ]
  node [
    id 5
    label "konkurs"
    origin "text"
  ]
  node [
    id 6
    label "oferta"
    origin "text"
  ]
  node [
    id 7
    label "mai&#263;"
    origin "text"
  ]
  node [
    id 8
    label "cela"
    origin "text"
  ]
  node [
    id 9
    label "przyznanie"
    origin "text"
  ]
  node [
    id 10
    label "dotacja"
    origin "text"
  ]
  node [
    id 11
    label "podmiot"
    origin "text"
  ]
  node [
    id 12
    label "prowadz&#261;ca"
    origin "text"
  ]
  node [
    id 13
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 14
    label "po&#380;ytek"
    origin "text"
  ]
  node [
    id 15
    label "publiczny"
    origin "text"
  ]
  node [
    id 16
    label "wsparcie"
    origin "text"
  ]
  node [
    id 17
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 18
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 19
    label "hipoterapeutycznego"
    origin "text"
  ]
  node [
    id 20
    label "mie&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 21
    label "si&#281;"
    origin "text"
  ]
  node [
    id 22
    label "przy"
    origin "text"
  ]
  node [
    id 23
    label "ulica"
    origin "text"
  ]
  node [
    id 24
    label "sp&#243;&#322;dzielczy"
    origin "text"
  ]
  node [
    id 25
    label "okres"
    origin "text"
  ]
  node [
    id 26
    label "lipiec"
    origin "text"
  ]
  node [
    id 27
    label "rok"
    origin "text"
  ]
  node [
    id 28
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 29
    label "Jelcyn"
  ]
  node [
    id 30
    label "Roosevelt"
  ]
  node [
    id 31
    label "Clinton"
  ]
  node [
    id 32
    label "dostojnik"
  ]
  node [
    id 33
    label "Nixon"
  ]
  node [
    id 34
    label "Tito"
  ]
  node [
    id 35
    label "de_Gaulle"
  ]
  node [
    id 36
    label "gruba_ryba"
  ]
  node [
    id 37
    label "Gorbaczow"
  ]
  node [
    id 38
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 39
    label "Putin"
  ]
  node [
    id 40
    label "Naser"
  ]
  node [
    id 41
    label "samorz&#261;dowiec"
  ]
  node [
    id 42
    label "Kemal"
  ]
  node [
    id 43
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 44
    label "zwierzchnik"
  ]
  node [
    id 45
    label "Bierut"
  ]
  node [
    id 46
    label "Brac&#322;aw"
  ]
  node [
    id 47
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 48
    label "G&#322;uch&#243;w"
  ]
  node [
    id 49
    label "Hallstatt"
  ]
  node [
    id 50
    label "Zbara&#380;"
  ]
  node [
    id 51
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 52
    label "Nachiczewan"
  ]
  node [
    id 53
    label "Suworow"
  ]
  node [
    id 54
    label "Halicz"
  ]
  node [
    id 55
    label "Gandawa"
  ]
  node [
    id 56
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 57
    label "Wismar"
  ]
  node [
    id 58
    label "Norymberga"
  ]
  node [
    id 59
    label "Ruciane-Nida"
  ]
  node [
    id 60
    label "Wia&#378;ma"
  ]
  node [
    id 61
    label "Sewilla"
  ]
  node [
    id 62
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 63
    label "Kobry&#324;"
  ]
  node [
    id 64
    label "Brno"
  ]
  node [
    id 65
    label "Tomsk"
  ]
  node [
    id 66
    label "Poniatowa"
  ]
  node [
    id 67
    label "Hadziacz"
  ]
  node [
    id 68
    label "Tiume&#324;"
  ]
  node [
    id 69
    label "Karlsbad"
  ]
  node [
    id 70
    label "Drohobycz"
  ]
  node [
    id 71
    label "Lyon"
  ]
  node [
    id 72
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 73
    label "K&#322;odawa"
  ]
  node [
    id 74
    label "Solikamsk"
  ]
  node [
    id 75
    label "Wolgast"
  ]
  node [
    id 76
    label "Saloniki"
  ]
  node [
    id 77
    label "Lw&#243;w"
  ]
  node [
    id 78
    label "Al-Kufa"
  ]
  node [
    id 79
    label "Hamburg"
  ]
  node [
    id 80
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 81
    label "Nampula"
  ]
  node [
    id 82
    label "burmistrz"
  ]
  node [
    id 83
    label "D&#252;sseldorf"
  ]
  node [
    id 84
    label "Nowy_Orlean"
  ]
  node [
    id 85
    label "Bamberg"
  ]
  node [
    id 86
    label "Osaka"
  ]
  node [
    id 87
    label "Michalovce"
  ]
  node [
    id 88
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 89
    label "Fryburg"
  ]
  node [
    id 90
    label "Trabzon"
  ]
  node [
    id 91
    label "Wersal"
  ]
  node [
    id 92
    label "Swatowe"
  ]
  node [
    id 93
    label "Ka&#322;uga"
  ]
  node [
    id 94
    label "Dijon"
  ]
  node [
    id 95
    label "Cannes"
  ]
  node [
    id 96
    label "Borowsk"
  ]
  node [
    id 97
    label "Kursk"
  ]
  node [
    id 98
    label "Tyberiada"
  ]
  node [
    id 99
    label "Boden"
  ]
  node [
    id 100
    label "Dodona"
  ]
  node [
    id 101
    label "Vukovar"
  ]
  node [
    id 102
    label "Soleczniki"
  ]
  node [
    id 103
    label "Barcelona"
  ]
  node [
    id 104
    label "Oszmiana"
  ]
  node [
    id 105
    label "Stuttgart"
  ]
  node [
    id 106
    label "Nerczy&#324;sk"
  ]
  node [
    id 107
    label "Essen"
  ]
  node [
    id 108
    label "Bijsk"
  ]
  node [
    id 109
    label "Luboml"
  ]
  node [
    id 110
    label "Gr&#243;dek"
  ]
  node [
    id 111
    label "Orany"
  ]
  node [
    id 112
    label "Siedliszcze"
  ]
  node [
    id 113
    label "P&#322;owdiw"
  ]
  node [
    id 114
    label "A&#322;apajewsk"
  ]
  node [
    id 115
    label "Liverpool"
  ]
  node [
    id 116
    label "Ostrawa"
  ]
  node [
    id 117
    label "Penza"
  ]
  node [
    id 118
    label "Rudki"
  ]
  node [
    id 119
    label "Aktobe"
  ]
  node [
    id 120
    label "I&#322;awka"
  ]
  node [
    id 121
    label "Tolkmicko"
  ]
  node [
    id 122
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 123
    label "Sajgon"
  ]
  node [
    id 124
    label "Windawa"
  ]
  node [
    id 125
    label "Weimar"
  ]
  node [
    id 126
    label "Jekaterynburg"
  ]
  node [
    id 127
    label "Lejda"
  ]
  node [
    id 128
    label "Cremona"
  ]
  node [
    id 129
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 130
    label "Kordoba"
  ]
  node [
    id 131
    label "urz&#261;d"
  ]
  node [
    id 132
    label "&#321;ohojsk"
  ]
  node [
    id 133
    label "Kalmar"
  ]
  node [
    id 134
    label "Akerman"
  ]
  node [
    id 135
    label "Locarno"
  ]
  node [
    id 136
    label "Bych&#243;w"
  ]
  node [
    id 137
    label "Toledo"
  ]
  node [
    id 138
    label "Minusi&#324;sk"
  ]
  node [
    id 139
    label "Szk&#322;&#243;w"
  ]
  node [
    id 140
    label "Wenecja"
  ]
  node [
    id 141
    label "Bazylea"
  ]
  node [
    id 142
    label "Peszt"
  ]
  node [
    id 143
    label "Piza"
  ]
  node [
    id 144
    label "Tanger"
  ]
  node [
    id 145
    label "Krzywi&#324;"
  ]
  node [
    id 146
    label "Eger"
  ]
  node [
    id 147
    label "Bogus&#322;aw"
  ]
  node [
    id 148
    label "Taganrog"
  ]
  node [
    id 149
    label "Oksford"
  ]
  node [
    id 150
    label "Gwardiejsk"
  ]
  node [
    id 151
    label "Tyraspol"
  ]
  node [
    id 152
    label "Kleczew"
  ]
  node [
    id 153
    label "Nowa_D&#281;ba"
  ]
  node [
    id 154
    label "Wilejka"
  ]
  node [
    id 155
    label "Modena"
  ]
  node [
    id 156
    label "Demmin"
  ]
  node [
    id 157
    label "Houston"
  ]
  node [
    id 158
    label "Rydu&#322;towy"
  ]
  node [
    id 159
    label "Bordeaux"
  ]
  node [
    id 160
    label "Schmalkalden"
  ]
  node [
    id 161
    label "O&#322;omuniec"
  ]
  node [
    id 162
    label "Tuluza"
  ]
  node [
    id 163
    label "tramwaj"
  ]
  node [
    id 164
    label "Nantes"
  ]
  node [
    id 165
    label "Debreczyn"
  ]
  node [
    id 166
    label "Kowel"
  ]
  node [
    id 167
    label "Witnica"
  ]
  node [
    id 168
    label "Stalingrad"
  ]
  node [
    id 169
    label "Drezno"
  ]
  node [
    id 170
    label "Perejas&#322;aw"
  ]
  node [
    id 171
    label "Luksor"
  ]
  node [
    id 172
    label "Ostaszk&#243;w"
  ]
  node [
    id 173
    label "Gettysburg"
  ]
  node [
    id 174
    label "Trydent"
  ]
  node [
    id 175
    label "Poczdam"
  ]
  node [
    id 176
    label "Mesyna"
  ]
  node [
    id 177
    label "Krasnogorsk"
  ]
  node [
    id 178
    label "Kars"
  ]
  node [
    id 179
    label "Darmstadt"
  ]
  node [
    id 180
    label "Rzg&#243;w"
  ]
  node [
    id 181
    label "Kar&#322;owice"
  ]
  node [
    id 182
    label "Czeskie_Budziejowice"
  ]
  node [
    id 183
    label "Buda"
  ]
  node [
    id 184
    label "Monako"
  ]
  node [
    id 185
    label "Pardubice"
  ]
  node [
    id 186
    label "Pas&#322;&#281;k"
  ]
  node [
    id 187
    label "Fatima"
  ]
  node [
    id 188
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 189
    label "Bir&#380;e"
  ]
  node [
    id 190
    label "Wi&#322;komierz"
  ]
  node [
    id 191
    label "Opawa"
  ]
  node [
    id 192
    label "Mantua"
  ]
  node [
    id 193
    label "Tarragona"
  ]
  node [
    id 194
    label "Antwerpia"
  ]
  node [
    id 195
    label "Asuan"
  ]
  node [
    id 196
    label "Korynt"
  ]
  node [
    id 197
    label "Armenia"
  ]
  node [
    id 198
    label "Budionnowsk"
  ]
  node [
    id 199
    label "Lengyel"
  ]
  node [
    id 200
    label "Betlejem"
  ]
  node [
    id 201
    label "Asy&#380;"
  ]
  node [
    id 202
    label "Batumi"
  ]
  node [
    id 203
    label "Paczk&#243;w"
  ]
  node [
    id 204
    label "Grenada"
  ]
  node [
    id 205
    label "Suczawa"
  ]
  node [
    id 206
    label "Nowogard"
  ]
  node [
    id 207
    label "Tyr"
  ]
  node [
    id 208
    label "Bria&#324;sk"
  ]
  node [
    id 209
    label "Bar"
  ]
  node [
    id 210
    label "Czerkiesk"
  ]
  node [
    id 211
    label "Ja&#322;ta"
  ]
  node [
    id 212
    label "Mo&#347;ciska"
  ]
  node [
    id 213
    label "Medyna"
  ]
  node [
    id 214
    label "Tartu"
  ]
  node [
    id 215
    label "Pemba"
  ]
  node [
    id 216
    label "Lipawa"
  ]
  node [
    id 217
    label "Tyl&#380;a"
  ]
  node [
    id 218
    label "Dayton"
  ]
  node [
    id 219
    label "Lipsk"
  ]
  node [
    id 220
    label "Rohatyn"
  ]
  node [
    id 221
    label "Peszawar"
  ]
  node [
    id 222
    label "Adrianopol"
  ]
  node [
    id 223
    label "Azow"
  ]
  node [
    id 224
    label "Iwano-Frankowsk"
  ]
  node [
    id 225
    label "Czarnobyl"
  ]
  node [
    id 226
    label "Rakoniewice"
  ]
  node [
    id 227
    label "Obuch&#243;w"
  ]
  node [
    id 228
    label "Orneta"
  ]
  node [
    id 229
    label "Koszyce"
  ]
  node [
    id 230
    label "Czeski_Cieszyn"
  ]
  node [
    id 231
    label "Zagorsk"
  ]
  node [
    id 232
    label "Nieder_Selters"
  ]
  node [
    id 233
    label "Ko&#322;omna"
  ]
  node [
    id 234
    label "Rost&#243;w"
  ]
  node [
    id 235
    label "Bolonia"
  ]
  node [
    id 236
    label "Rajgr&#243;d"
  ]
  node [
    id 237
    label "L&#252;neburg"
  ]
  node [
    id 238
    label "Brack"
  ]
  node [
    id 239
    label "Konstancja"
  ]
  node [
    id 240
    label "Koluszki"
  ]
  node [
    id 241
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 242
    label "Suez"
  ]
  node [
    id 243
    label "Mrocza"
  ]
  node [
    id 244
    label "Triest"
  ]
  node [
    id 245
    label "Murma&#324;sk"
  ]
  node [
    id 246
    label "Tu&#322;a"
  ]
  node [
    id 247
    label "Tarnogr&#243;d"
  ]
  node [
    id 248
    label "Radziech&#243;w"
  ]
  node [
    id 249
    label "Kokand"
  ]
  node [
    id 250
    label "Kircholm"
  ]
  node [
    id 251
    label "Nowa_Ruda"
  ]
  node [
    id 252
    label "Huma&#324;"
  ]
  node [
    id 253
    label "Turkiestan"
  ]
  node [
    id 254
    label "Kani&#243;w"
  ]
  node [
    id 255
    label "Pilzno"
  ]
  node [
    id 256
    label "Korfant&#243;w"
  ]
  node [
    id 257
    label "Dubno"
  ]
  node [
    id 258
    label "Bras&#322;aw"
  ]
  node [
    id 259
    label "Choroszcz"
  ]
  node [
    id 260
    label "Nowogr&#243;d"
  ]
  node [
    id 261
    label "Konotop"
  ]
  node [
    id 262
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 263
    label "Jastarnia"
  ]
  node [
    id 264
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 265
    label "Omsk"
  ]
  node [
    id 266
    label "Troick"
  ]
  node [
    id 267
    label "Koper"
  ]
  node [
    id 268
    label "Jenisejsk"
  ]
  node [
    id 269
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 270
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 271
    label "Trenczyn"
  ]
  node [
    id 272
    label "Wormacja"
  ]
  node [
    id 273
    label "Wagram"
  ]
  node [
    id 274
    label "Lubeka"
  ]
  node [
    id 275
    label "Genewa"
  ]
  node [
    id 276
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 277
    label "Kleck"
  ]
  node [
    id 278
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 279
    label "Struga"
  ]
  node [
    id 280
    label "Izbica_Kujawska"
  ]
  node [
    id 281
    label "Stalinogorsk"
  ]
  node [
    id 282
    label "Izmir"
  ]
  node [
    id 283
    label "Dortmund"
  ]
  node [
    id 284
    label "Workuta"
  ]
  node [
    id 285
    label "Jerycho"
  ]
  node [
    id 286
    label "Brunszwik"
  ]
  node [
    id 287
    label "Aleksandria"
  ]
  node [
    id 288
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 289
    label "Borys&#322;aw"
  ]
  node [
    id 290
    label "Zaleszczyki"
  ]
  node [
    id 291
    label "Z&#322;oczew"
  ]
  node [
    id 292
    label "Piast&#243;w"
  ]
  node [
    id 293
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 294
    label "Bor"
  ]
  node [
    id 295
    label "Nazaret"
  ]
  node [
    id 296
    label "Sarat&#243;w"
  ]
  node [
    id 297
    label "Brasz&#243;w"
  ]
  node [
    id 298
    label "Malin"
  ]
  node [
    id 299
    label "Parma"
  ]
  node [
    id 300
    label "Wierchoja&#324;sk"
  ]
  node [
    id 301
    label "Tarent"
  ]
  node [
    id 302
    label "Mariampol"
  ]
  node [
    id 303
    label "Wuhan"
  ]
  node [
    id 304
    label "Split"
  ]
  node [
    id 305
    label "Baranowicze"
  ]
  node [
    id 306
    label "Marki"
  ]
  node [
    id 307
    label "Adana"
  ]
  node [
    id 308
    label "B&#322;aszki"
  ]
  node [
    id 309
    label "Lubecz"
  ]
  node [
    id 310
    label "Sulech&#243;w"
  ]
  node [
    id 311
    label "Borys&#243;w"
  ]
  node [
    id 312
    label "Homel"
  ]
  node [
    id 313
    label "Tours"
  ]
  node [
    id 314
    label "Zaporo&#380;e"
  ]
  node [
    id 315
    label "Edam"
  ]
  node [
    id 316
    label "Kamieniec_Podolski"
  ]
  node [
    id 317
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 318
    label "Konstantynopol"
  ]
  node [
    id 319
    label "Chocim"
  ]
  node [
    id 320
    label "Mohylew"
  ]
  node [
    id 321
    label "Merseburg"
  ]
  node [
    id 322
    label "Kapsztad"
  ]
  node [
    id 323
    label "Sambor"
  ]
  node [
    id 324
    label "Manchester"
  ]
  node [
    id 325
    label "Pi&#324;sk"
  ]
  node [
    id 326
    label "Ochryda"
  ]
  node [
    id 327
    label "Rybi&#324;sk"
  ]
  node [
    id 328
    label "Czadca"
  ]
  node [
    id 329
    label "Orenburg"
  ]
  node [
    id 330
    label "Krajowa"
  ]
  node [
    id 331
    label "Eleusis"
  ]
  node [
    id 332
    label "Awinion"
  ]
  node [
    id 333
    label "Rzeczyca"
  ]
  node [
    id 334
    label "Lozanna"
  ]
  node [
    id 335
    label "Barczewo"
  ]
  node [
    id 336
    label "&#379;migr&#243;d"
  ]
  node [
    id 337
    label "Chabarowsk"
  ]
  node [
    id 338
    label "Jena"
  ]
  node [
    id 339
    label "Xai-Xai"
  ]
  node [
    id 340
    label "Radk&#243;w"
  ]
  node [
    id 341
    label "Syrakuzy"
  ]
  node [
    id 342
    label "Zas&#322;aw"
  ]
  node [
    id 343
    label "Windsor"
  ]
  node [
    id 344
    label "Getynga"
  ]
  node [
    id 345
    label "Carrara"
  ]
  node [
    id 346
    label "Madras"
  ]
  node [
    id 347
    label "Nitra"
  ]
  node [
    id 348
    label "Kilonia"
  ]
  node [
    id 349
    label "Rawenna"
  ]
  node [
    id 350
    label "Stawropol"
  ]
  node [
    id 351
    label "Warna"
  ]
  node [
    id 352
    label "Ba&#322;tijsk"
  ]
  node [
    id 353
    label "Cumana"
  ]
  node [
    id 354
    label "Kostroma"
  ]
  node [
    id 355
    label "Bajonna"
  ]
  node [
    id 356
    label "Magadan"
  ]
  node [
    id 357
    label "Kercz"
  ]
  node [
    id 358
    label "Harbin"
  ]
  node [
    id 359
    label "Sankt_Florian"
  ]
  node [
    id 360
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 361
    label "Wo&#322;kowysk"
  ]
  node [
    id 362
    label "Norak"
  ]
  node [
    id 363
    label "S&#232;vres"
  ]
  node [
    id 364
    label "Barwice"
  ]
  node [
    id 365
    label "Sumy"
  ]
  node [
    id 366
    label "Jutrosin"
  ]
  node [
    id 367
    label "Canterbury"
  ]
  node [
    id 368
    label "Czerkasy"
  ]
  node [
    id 369
    label "Troki"
  ]
  node [
    id 370
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 371
    label "Turka"
  ]
  node [
    id 372
    label "Budziszyn"
  ]
  node [
    id 373
    label "A&#322;czewsk"
  ]
  node [
    id 374
    label "Chark&#243;w"
  ]
  node [
    id 375
    label "Go&#347;cino"
  ]
  node [
    id 376
    label "Ku&#378;nieck"
  ]
  node [
    id 377
    label "Wotki&#324;sk"
  ]
  node [
    id 378
    label "Symferopol"
  ]
  node [
    id 379
    label "Dmitrow"
  ]
  node [
    id 380
    label "Cherso&#324;"
  ]
  node [
    id 381
    label "zabudowa"
  ]
  node [
    id 382
    label "Orlean"
  ]
  node [
    id 383
    label "Nowogr&#243;dek"
  ]
  node [
    id 384
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 385
    label "Berdia&#324;sk"
  ]
  node [
    id 386
    label "Szumsk"
  ]
  node [
    id 387
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 388
    label "Orsza"
  ]
  node [
    id 389
    label "Cluny"
  ]
  node [
    id 390
    label "Aralsk"
  ]
  node [
    id 391
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 392
    label "Bogumin"
  ]
  node [
    id 393
    label "Antiochia"
  ]
  node [
    id 394
    label "grupa"
  ]
  node [
    id 395
    label "Inhambane"
  ]
  node [
    id 396
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 397
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 398
    label "Trewir"
  ]
  node [
    id 399
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 400
    label "Siewieromorsk"
  ]
  node [
    id 401
    label "Calais"
  ]
  node [
    id 402
    label "Twer"
  ]
  node [
    id 403
    label "&#379;ytawa"
  ]
  node [
    id 404
    label "Eupatoria"
  ]
  node [
    id 405
    label "Stara_Zagora"
  ]
  node [
    id 406
    label "Jastrowie"
  ]
  node [
    id 407
    label "Piatigorsk"
  ]
  node [
    id 408
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 409
    label "Le&#324;sk"
  ]
  node [
    id 410
    label "Johannesburg"
  ]
  node [
    id 411
    label "Kaszyn"
  ]
  node [
    id 412
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 413
    label "&#379;ylina"
  ]
  node [
    id 414
    label "Sewastopol"
  ]
  node [
    id 415
    label "Pietrozawodsk"
  ]
  node [
    id 416
    label "Bobolice"
  ]
  node [
    id 417
    label "Mosty"
  ]
  node [
    id 418
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 419
    label "Karaganda"
  ]
  node [
    id 420
    label "Marsylia"
  ]
  node [
    id 421
    label "Buchara"
  ]
  node [
    id 422
    label "Dubrownik"
  ]
  node [
    id 423
    label "Be&#322;z"
  ]
  node [
    id 424
    label "Oran"
  ]
  node [
    id 425
    label "Regensburg"
  ]
  node [
    id 426
    label "Rotterdam"
  ]
  node [
    id 427
    label "Trembowla"
  ]
  node [
    id 428
    label "Woskriesiensk"
  ]
  node [
    id 429
    label "Po&#322;ock"
  ]
  node [
    id 430
    label "Poprad"
  ]
  node [
    id 431
    label "Kronsztad"
  ]
  node [
    id 432
    label "Los_Angeles"
  ]
  node [
    id 433
    label "U&#322;an_Ude"
  ]
  node [
    id 434
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 435
    label "W&#322;adywostok"
  ]
  node [
    id 436
    label "Kandahar"
  ]
  node [
    id 437
    label "Tobolsk"
  ]
  node [
    id 438
    label "Boston"
  ]
  node [
    id 439
    label "Hawana"
  ]
  node [
    id 440
    label "Kis&#322;owodzk"
  ]
  node [
    id 441
    label "Tulon"
  ]
  node [
    id 442
    label "Utrecht"
  ]
  node [
    id 443
    label "Oleszyce"
  ]
  node [
    id 444
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 445
    label "Katania"
  ]
  node [
    id 446
    label "Teby"
  ]
  node [
    id 447
    label "Paw&#322;owo"
  ]
  node [
    id 448
    label "W&#252;rzburg"
  ]
  node [
    id 449
    label "Podiebrady"
  ]
  node [
    id 450
    label "Uppsala"
  ]
  node [
    id 451
    label "Poniewie&#380;"
  ]
  node [
    id 452
    label "Niko&#322;ajewsk"
  ]
  node [
    id 453
    label "Aczy&#324;sk"
  ]
  node [
    id 454
    label "Berezyna"
  ]
  node [
    id 455
    label "Ostr&#243;g"
  ]
  node [
    id 456
    label "Brze&#347;&#263;"
  ]
  node [
    id 457
    label "Lancaster"
  ]
  node [
    id 458
    label "Stryj"
  ]
  node [
    id 459
    label "Kozielsk"
  ]
  node [
    id 460
    label "Loreto"
  ]
  node [
    id 461
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 462
    label "Hebron"
  ]
  node [
    id 463
    label "Kaspijsk"
  ]
  node [
    id 464
    label "Peczora"
  ]
  node [
    id 465
    label "Isfahan"
  ]
  node [
    id 466
    label "Chimoio"
  ]
  node [
    id 467
    label "Mory&#324;"
  ]
  node [
    id 468
    label "Kowno"
  ]
  node [
    id 469
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 470
    label "Opalenica"
  ]
  node [
    id 471
    label "Kolonia"
  ]
  node [
    id 472
    label "Stary_Sambor"
  ]
  node [
    id 473
    label "Kolkata"
  ]
  node [
    id 474
    label "Turkmenbaszy"
  ]
  node [
    id 475
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 476
    label "Nankin"
  ]
  node [
    id 477
    label "Krzanowice"
  ]
  node [
    id 478
    label "Efez"
  ]
  node [
    id 479
    label "Dobrodzie&#324;"
  ]
  node [
    id 480
    label "Neapol"
  ]
  node [
    id 481
    label "S&#322;uck"
  ]
  node [
    id 482
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 483
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 484
    label "Frydek-Mistek"
  ]
  node [
    id 485
    label "Korsze"
  ]
  node [
    id 486
    label "T&#322;uszcz"
  ]
  node [
    id 487
    label "Soligorsk"
  ]
  node [
    id 488
    label "Kie&#380;mark"
  ]
  node [
    id 489
    label "Mannheim"
  ]
  node [
    id 490
    label "Ulm"
  ]
  node [
    id 491
    label "Podhajce"
  ]
  node [
    id 492
    label "Dniepropetrowsk"
  ]
  node [
    id 493
    label "Szamocin"
  ]
  node [
    id 494
    label "Ko&#322;omyja"
  ]
  node [
    id 495
    label "Buczacz"
  ]
  node [
    id 496
    label "M&#252;nster"
  ]
  node [
    id 497
    label "Brema"
  ]
  node [
    id 498
    label "Delhi"
  ]
  node [
    id 499
    label "&#346;niatyn"
  ]
  node [
    id 500
    label "Nicea"
  ]
  node [
    id 501
    label "Szawle"
  ]
  node [
    id 502
    label "Czerniowce"
  ]
  node [
    id 503
    label "Mi&#347;nia"
  ]
  node [
    id 504
    label "Sydney"
  ]
  node [
    id 505
    label "Moguncja"
  ]
  node [
    id 506
    label "Narbona"
  ]
  node [
    id 507
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 508
    label "Wittenberga"
  ]
  node [
    id 509
    label "Uljanowsk"
  ]
  node [
    id 510
    label "&#321;uga&#324;sk"
  ]
  node [
    id 511
    label "Wyborg"
  ]
  node [
    id 512
    label "Trojan"
  ]
  node [
    id 513
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 514
    label "Brandenburg"
  ]
  node [
    id 515
    label "Kemerowo"
  ]
  node [
    id 516
    label "Kaszgar"
  ]
  node [
    id 517
    label "Lenzen"
  ]
  node [
    id 518
    label "Nanning"
  ]
  node [
    id 519
    label "Gotha"
  ]
  node [
    id 520
    label "Zurych"
  ]
  node [
    id 521
    label "Baltimore"
  ]
  node [
    id 522
    label "&#321;uck"
  ]
  node [
    id 523
    label "Bristol"
  ]
  node [
    id 524
    label "Ferrara"
  ]
  node [
    id 525
    label "Mariupol"
  ]
  node [
    id 526
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 527
    label "Lhasa"
  ]
  node [
    id 528
    label "Czerniejewo"
  ]
  node [
    id 529
    label "Filadelfia"
  ]
  node [
    id 530
    label "Kanton"
  ]
  node [
    id 531
    label "Milan&#243;wek"
  ]
  node [
    id 532
    label "Perwomajsk"
  ]
  node [
    id 533
    label "Nieftiegorsk"
  ]
  node [
    id 534
    label "Pittsburgh"
  ]
  node [
    id 535
    label "Greifswald"
  ]
  node [
    id 536
    label "Akwileja"
  ]
  node [
    id 537
    label "Norfolk"
  ]
  node [
    id 538
    label "Perm"
  ]
  node [
    id 539
    label "Detroit"
  ]
  node [
    id 540
    label "Fergana"
  ]
  node [
    id 541
    label "Starobielsk"
  ]
  node [
    id 542
    label "Wielsk"
  ]
  node [
    id 543
    label "Zaklik&#243;w"
  ]
  node [
    id 544
    label "Majsur"
  ]
  node [
    id 545
    label "Narwa"
  ]
  node [
    id 546
    label "Chicago"
  ]
  node [
    id 547
    label "Byczyna"
  ]
  node [
    id 548
    label "Mozyrz"
  ]
  node [
    id 549
    label "Konstantyn&#243;wka"
  ]
  node [
    id 550
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 551
    label "Megara"
  ]
  node [
    id 552
    label "Stralsund"
  ]
  node [
    id 553
    label "Wo&#322;gograd"
  ]
  node [
    id 554
    label "Lichinga"
  ]
  node [
    id 555
    label "Haga"
  ]
  node [
    id 556
    label "Tarnopol"
  ]
  node [
    id 557
    label "K&#322;ajpeda"
  ]
  node [
    id 558
    label "Nowomoskowsk"
  ]
  node [
    id 559
    label "Ussuryjsk"
  ]
  node [
    id 560
    label "Brugia"
  ]
  node [
    id 561
    label "Natal"
  ]
  node [
    id 562
    label "Kro&#347;niewice"
  ]
  node [
    id 563
    label "Edynburg"
  ]
  node [
    id 564
    label "Marburg"
  ]
  node [
    id 565
    label "&#346;wiebodzice"
  ]
  node [
    id 566
    label "S&#322;onim"
  ]
  node [
    id 567
    label "Dalton"
  ]
  node [
    id 568
    label "Smorgonie"
  ]
  node [
    id 569
    label "Orze&#322;"
  ]
  node [
    id 570
    label "Nowoku&#378;nieck"
  ]
  node [
    id 571
    label "Zadar"
  ]
  node [
    id 572
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 573
    label "Koprzywnica"
  ]
  node [
    id 574
    label "Angarsk"
  ]
  node [
    id 575
    label "Mo&#380;ajsk"
  ]
  node [
    id 576
    label "Akwizgran"
  ]
  node [
    id 577
    label "Norylsk"
  ]
  node [
    id 578
    label "Jawor&#243;w"
  ]
  node [
    id 579
    label "weduta"
  ]
  node [
    id 580
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 581
    label "Suzdal"
  ]
  node [
    id 582
    label "W&#322;odzimierz"
  ]
  node [
    id 583
    label "Bujnaksk"
  ]
  node [
    id 584
    label "Beresteczko"
  ]
  node [
    id 585
    label "Strzelno"
  ]
  node [
    id 586
    label "Siewsk"
  ]
  node [
    id 587
    label "Cymlansk"
  ]
  node [
    id 588
    label "Trzyniec"
  ]
  node [
    id 589
    label "Hanower"
  ]
  node [
    id 590
    label "Wuppertal"
  ]
  node [
    id 591
    label "Sura&#380;"
  ]
  node [
    id 592
    label "Winchester"
  ]
  node [
    id 593
    label "Samara"
  ]
  node [
    id 594
    label "Sydon"
  ]
  node [
    id 595
    label "Krasnodar"
  ]
  node [
    id 596
    label "Worone&#380;"
  ]
  node [
    id 597
    label "Paw&#322;odar"
  ]
  node [
    id 598
    label "Czelabi&#324;sk"
  ]
  node [
    id 599
    label "Reda"
  ]
  node [
    id 600
    label "Karwina"
  ]
  node [
    id 601
    label "Wyszehrad"
  ]
  node [
    id 602
    label "Sara&#324;sk"
  ]
  node [
    id 603
    label "Koby&#322;ka"
  ]
  node [
    id 604
    label "Winnica"
  ]
  node [
    id 605
    label "Tambow"
  ]
  node [
    id 606
    label "Pyskowice"
  ]
  node [
    id 607
    label "Heidelberg"
  ]
  node [
    id 608
    label "Maribor"
  ]
  node [
    id 609
    label "Werona"
  ]
  node [
    id 610
    label "G&#322;uszyca"
  ]
  node [
    id 611
    label "Rostock"
  ]
  node [
    id 612
    label "Mekka"
  ]
  node [
    id 613
    label "Liberec"
  ]
  node [
    id 614
    label "Bie&#322;gorod"
  ]
  node [
    id 615
    label "Berdycz&#243;w"
  ]
  node [
    id 616
    label "Sierdobsk"
  ]
  node [
    id 617
    label "Bobrujsk"
  ]
  node [
    id 618
    label "Padwa"
  ]
  node [
    id 619
    label "Pasawa"
  ]
  node [
    id 620
    label "Chanty-Mansyjsk"
  ]
  node [
    id 621
    label "&#379;ar&#243;w"
  ]
  node [
    id 622
    label "Poczaj&#243;w"
  ]
  node [
    id 623
    label "Barabi&#324;sk"
  ]
  node [
    id 624
    label "Gorycja"
  ]
  node [
    id 625
    label "Haarlem"
  ]
  node [
    id 626
    label "Kiejdany"
  ]
  node [
    id 627
    label "Chmielnicki"
  ]
  node [
    id 628
    label "Magnitogorsk"
  ]
  node [
    id 629
    label "Burgas"
  ]
  node [
    id 630
    label "Siena"
  ]
  node [
    id 631
    label "Korzec"
  ]
  node [
    id 632
    label "Bonn"
  ]
  node [
    id 633
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 634
    label "Walencja"
  ]
  node [
    id 635
    label "Mosina"
  ]
  node [
    id 636
    label "announce"
  ]
  node [
    id 637
    label "publikowa&#263;"
  ]
  node [
    id 638
    label "obwo&#322;ywa&#263;"
  ]
  node [
    id 639
    label "post"
  ]
  node [
    id 640
    label "podawa&#263;"
  ]
  node [
    id 641
    label "ewidentny"
  ]
  node [
    id 642
    label "bezpo&#347;redni"
  ]
  node [
    id 643
    label "otwarcie"
  ]
  node [
    id 644
    label "nieograniczony"
  ]
  node [
    id 645
    label "zdecydowany"
  ]
  node [
    id 646
    label "gotowy"
  ]
  node [
    id 647
    label "aktualny"
  ]
  node [
    id 648
    label "prostoduszny"
  ]
  node [
    id 649
    label "jawnie"
  ]
  node [
    id 650
    label "otworzysty"
  ]
  node [
    id 651
    label "dost&#281;pny"
  ]
  node [
    id 652
    label "aktywny"
  ]
  node [
    id 653
    label "kontaktowy"
  ]
  node [
    id 654
    label "eliminacje"
  ]
  node [
    id 655
    label "Interwizja"
  ]
  node [
    id 656
    label "emulation"
  ]
  node [
    id 657
    label "impreza"
  ]
  node [
    id 658
    label "casting"
  ]
  node [
    id 659
    label "Eurowizja"
  ]
  node [
    id 660
    label "nab&#243;r"
  ]
  node [
    id 661
    label "propozycja"
  ]
  node [
    id 662
    label "offer"
  ]
  node [
    id 663
    label "ozdabia&#263;"
  ]
  node [
    id 664
    label "pomieszczenie"
  ]
  node [
    id 665
    label "klasztor"
  ]
  node [
    id 666
    label "recognition"
  ]
  node [
    id 667
    label "danie"
  ]
  node [
    id 668
    label "stwierdzenie"
  ]
  node [
    id 669
    label "confession"
  ]
  node [
    id 670
    label "oznajmienie"
  ]
  node [
    id 671
    label "dop&#322;ata"
  ]
  node [
    id 672
    label "cz&#322;owiek"
  ]
  node [
    id 673
    label "byt"
  ]
  node [
    id 674
    label "organizacja"
  ]
  node [
    id 675
    label "prawo"
  ]
  node [
    id 676
    label "nauka_prawa"
  ]
  node [
    id 677
    label "osobowo&#347;&#263;"
  ]
  node [
    id 678
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 679
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 680
    label "dzia&#322;anie"
  ]
  node [
    id 681
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 682
    label "absolutorium"
  ]
  node [
    id 683
    label "activity"
  ]
  node [
    id 684
    label "py&#322;ek"
  ]
  node [
    id 685
    label "korzy&#347;&#263;"
  ]
  node [
    id 686
    label "surowiec"
  ]
  node [
    id 687
    label "spad&#378;"
  ]
  node [
    id 688
    label "dobro"
  ]
  node [
    id 689
    label "zaleta"
  ]
  node [
    id 690
    label "nektar"
  ]
  node [
    id 691
    label "jawny"
  ]
  node [
    id 692
    label "upublicznienie"
  ]
  node [
    id 693
    label "upublicznianie"
  ]
  node [
    id 694
    label "publicznie"
  ]
  node [
    id 695
    label "comfort"
  ]
  node [
    id 696
    label "u&#322;atwienie"
  ]
  node [
    id 697
    label "doch&#243;d"
  ]
  node [
    id 698
    label "oparcie"
  ]
  node [
    id 699
    label "telefon_zaufania"
  ]
  node [
    id 700
    label "dar"
  ]
  node [
    id 701
    label "zapomoga"
  ]
  node [
    id 702
    label "pocieszenie"
  ]
  node [
    id 703
    label "darowizna"
  ]
  node [
    id 704
    label "pomoc"
  ]
  node [
    id 705
    label "&#347;rodek"
  ]
  node [
    id 706
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 707
    label "support"
  ]
  node [
    id 708
    label "robienie"
  ]
  node [
    id 709
    label "przywodzenie"
  ]
  node [
    id 710
    label "prowadzanie"
  ]
  node [
    id 711
    label "ukierunkowywanie"
  ]
  node [
    id 712
    label "kszta&#322;towanie"
  ]
  node [
    id 713
    label "poprowadzenie"
  ]
  node [
    id 714
    label "wprowadzanie"
  ]
  node [
    id 715
    label "dysponowanie"
  ]
  node [
    id 716
    label "przeci&#261;ganie"
  ]
  node [
    id 717
    label "doprowadzanie"
  ]
  node [
    id 718
    label "wprowadzenie"
  ]
  node [
    id 719
    label "eksponowanie"
  ]
  node [
    id 720
    label "oprowadzenie"
  ]
  node [
    id 721
    label "trzymanie"
  ]
  node [
    id 722
    label "ta&#324;czenie"
  ]
  node [
    id 723
    label "przeci&#281;cie"
  ]
  node [
    id 724
    label "przewy&#380;szanie"
  ]
  node [
    id 725
    label "prowadzi&#263;"
  ]
  node [
    id 726
    label "aim"
  ]
  node [
    id 727
    label "czynno&#347;&#263;"
  ]
  node [
    id 728
    label "zwracanie"
  ]
  node [
    id 729
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 730
    label "przecinanie"
  ]
  node [
    id 731
    label "sterowanie"
  ]
  node [
    id 732
    label "drive"
  ]
  node [
    id 733
    label "kre&#347;lenie"
  ]
  node [
    id 734
    label "management"
  ]
  node [
    id 735
    label "dawanie"
  ]
  node [
    id 736
    label "oprowadzanie"
  ]
  node [
    id 737
    label "pozarz&#261;dzanie"
  ]
  node [
    id 738
    label "g&#243;rowanie"
  ]
  node [
    id 739
    label "linia_melodyczna"
  ]
  node [
    id 740
    label "granie"
  ]
  node [
    id 741
    label "doprowadzenie"
  ]
  node [
    id 742
    label "kierowanie"
  ]
  node [
    id 743
    label "zaprowadzanie"
  ]
  node [
    id 744
    label "lead"
  ]
  node [
    id 745
    label "powodowanie"
  ]
  node [
    id 746
    label "krzywa"
  ]
  node [
    id 747
    label "miejsce"
  ]
  node [
    id 748
    label "Hollywood"
  ]
  node [
    id 749
    label "zal&#261;&#380;ek"
  ]
  node [
    id 750
    label "otoczenie"
  ]
  node [
    id 751
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 752
    label "center"
  ]
  node [
    id 753
    label "instytucja"
  ]
  node [
    id 754
    label "skupisko"
  ]
  node [
    id 755
    label "warunki"
  ]
  node [
    id 756
    label "mie&#263;"
  ]
  node [
    id 757
    label "m&#243;c"
  ]
  node [
    id 758
    label "zawiera&#263;"
  ]
  node [
    id 759
    label "fold"
  ]
  node [
    id 760
    label "lock"
  ]
  node [
    id 761
    label "&#347;rodowisko"
  ]
  node [
    id 762
    label "miasteczko"
  ]
  node [
    id 763
    label "streetball"
  ]
  node [
    id 764
    label "pierzeja"
  ]
  node [
    id 765
    label "pas_ruchu"
  ]
  node [
    id 766
    label "jezdnia"
  ]
  node [
    id 767
    label "pas_rozdzielczy"
  ]
  node [
    id 768
    label "droga"
  ]
  node [
    id 769
    label "korona_drogi"
  ]
  node [
    id 770
    label "chodnik"
  ]
  node [
    id 771
    label "arteria"
  ]
  node [
    id 772
    label "Broadway"
  ]
  node [
    id 773
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 774
    label "wysepka"
  ]
  node [
    id 775
    label "autostrada"
  ]
  node [
    id 776
    label "uspo&#322;ecznianie"
  ]
  node [
    id 777
    label "uspo&#322;ecznienie"
  ]
  node [
    id 778
    label "paleogen"
  ]
  node [
    id 779
    label "spell"
  ]
  node [
    id 780
    label "czas"
  ]
  node [
    id 781
    label "period"
  ]
  node [
    id 782
    label "prekambr"
  ]
  node [
    id 783
    label "jura"
  ]
  node [
    id 784
    label "interstadia&#322;"
  ]
  node [
    id 785
    label "jednostka_geologiczna"
  ]
  node [
    id 786
    label "izochronizm"
  ]
  node [
    id 787
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 788
    label "okres_noachijski"
  ]
  node [
    id 789
    label "orosir"
  ]
  node [
    id 790
    label "kreda"
  ]
  node [
    id 791
    label "sten"
  ]
  node [
    id 792
    label "drugorz&#281;d"
  ]
  node [
    id 793
    label "semester"
  ]
  node [
    id 794
    label "trzeciorz&#281;d"
  ]
  node [
    id 795
    label "ton"
  ]
  node [
    id 796
    label "dzieje"
  ]
  node [
    id 797
    label "poprzednik"
  ]
  node [
    id 798
    label "ordowik"
  ]
  node [
    id 799
    label "karbon"
  ]
  node [
    id 800
    label "trias"
  ]
  node [
    id 801
    label "kalim"
  ]
  node [
    id 802
    label "stater"
  ]
  node [
    id 803
    label "era"
  ]
  node [
    id 804
    label "cykl"
  ]
  node [
    id 805
    label "p&#243;&#322;okres"
  ]
  node [
    id 806
    label "czwartorz&#281;d"
  ]
  node [
    id 807
    label "pulsacja"
  ]
  node [
    id 808
    label "okres_amazo&#324;ski"
  ]
  node [
    id 809
    label "kambr"
  ]
  node [
    id 810
    label "Zeitgeist"
  ]
  node [
    id 811
    label "nast&#281;pnik"
  ]
  node [
    id 812
    label "kriogen"
  ]
  node [
    id 813
    label "glacja&#322;"
  ]
  node [
    id 814
    label "fala"
  ]
  node [
    id 815
    label "okres_czasu"
  ]
  node [
    id 816
    label "riak"
  ]
  node [
    id 817
    label "schy&#322;ek"
  ]
  node [
    id 818
    label "okres_hesperyjski"
  ]
  node [
    id 819
    label "sylur"
  ]
  node [
    id 820
    label "dewon"
  ]
  node [
    id 821
    label "ciota"
  ]
  node [
    id 822
    label "epoka"
  ]
  node [
    id 823
    label "pierwszorz&#281;d"
  ]
  node [
    id 824
    label "okres_halsztacki"
  ]
  node [
    id 825
    label "ektas"
  ]
  node [
    id 826
    label "zdanie"
  ]
  node [
    id 827
    label "condition"
  ]
  node [
    id 828
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 829
    label "rok_akademicki"
  ]
  node [
    id 830
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 831
    label "postglacja&#322;"
  ]
  node [
    id 832
    label "faza"
  ]
  node [
    id 833
    label "proces_fizjologiczny"
  ]
  node [
    id 834
    label "ediakar"
  ]
  node [
    id 835
    label "time_period"
  ]
  node [
    id 836
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 837
    label "perm"
  ]
  node [
    id 838
    label "rok_szkolny"
  ]
  node [
    id 839
    label "neogen"
  ]
  node [
    id 840
    label "sider"
  ]
  node [
    id 841
    label "flow"
  ]
  node [
    id 842
    label "podokres"
  ]
  node [
    id 843
    label "preglacja&#322;"
  ]
  node [
    id 844
    label "retoryka"
  ]
  node [
    id 845
    label "choroba_przyrodzona"
  ]
  node [
    id 846
    label "miesi&#261;c"
  ]
  node [
    id 847
    label "stulecie"
  ]
  node [
    id 848
    label "kalendarz"
  ]
  node [
    id 849
    label "pora_roku"
  ]
  node [
    id 850
    label "cykl_astronomiczny"
  ]
  node [
    id 851
    label "p&#243;&#322;rocze"
  ]
  node [
    id 852
    label "kwarta&#322;"
  ]
  node [
    id 853
    label "kurs"
  ]
  node [
    id 854
    label "jubileusz"
  ]
  node [
    id 855
    label "lata"
  ]
  node [
    id 856
    label "martwy_sezon"
  ]
  node [
    id 857
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 858
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 859
    label "Barb&#243;rka"
  ]
  node [
    id 860
    label "Sylwester"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 1
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 1
    target 413
  ]
  edge [
    source 1
    target 414
  ]
  edge [
    source 1
    target 415
  ]
  edge [
    source 1
    target 416
  ]
  edge [
    source 1
    target 417
  ]
  edge [
    source 1
    target 418
  ]
  edge [
    source 1
    target 419
  ]
  edge [
    source 1
    target 420
  ]
  edge [
    source 1
    target 421
  ]
  edge [
    source 1
    target 422
  ]
  edge [
    source 1
    target 423
  ]
  edge [
    source 1
    target 424
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 426
  ]
  edge [
    source 1
    target 427
  ]
  edge [
    source 1
    target 428
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 1
    target 438
  ]
  edge [
    source 1
    target 439
  ]
  edge [
    source 1
    target 440
  ]
  edge [
    source 1
    target 441
  ]
  edge [
    source 1
    target 442
  ]
  edge [
    source 1
    target 443
  ]
  edge [
    source 1
    target 444
  ]
  edge [
    source 1
    target 445
  ]
  edge [
    source 1
    target 446
  ]
  edge [
    source 1
    target 447
  ]
  edge [
    source 1
    target 448
  ]
  edge [
    source 1
    target 449
  ]
  edge [
    source 1
    target 450
  ]
  edge [
    source 1
    target 451
  ]
  edge [
    source 1
    target 452
  ]
  edge [
    source 1
    target 453
  ]
  edge [
    source 1
    target 454
  ]
  edge [
    source 1
    target 455
  ]
  edge [
    source 1
    target 456
  ]
  edge [
    source 1
    target 457
  ]
  edge [
    source 1
    target 458
  ]
  edge [
    source 1
    target 459
  ]
  edge [
    source 1
    target 460
  ]
  edge [
    source 1
    target 461
  ]
  edge [
    source 1
    target 462
  ]
  edge [
    source 1
    target 463
  ]
  edge [
    source 1
    target 464
  ]
  edge [
    source 1
    target 465
  ]
  edge [
    source 1
    target 466
  ]
  edge [
    source 1
    target 467
  ]
  edge [
    source 1
    target 468
  ]
  edge [
    source 1
    target 469
  ]
  edge [
    source 1
    target 470
  ]
  edge [
    source 1
    target 471
  ]
  edge [
    source 1
    target 472
  ]
  edge [
    source 1
    target 473
  ]
  edge [
    source 1
    target 474
  ]
  edge [
    source 1
    target 475
  ]
  edge [
    source 1
    target 476
  ]
  edge [
    source 1
    target 477
  ]
  edge [
    source 1
    target 478
  ]
  edge [
    source 1
    target 479
  ]
  edge [
    source 1
    target 480
  ]
  edge [
    source 1
    target 481
  ]
  edge [
    source 1
    target 482
  ]
  edge [
    source 1
    target 483
  ]
  edge [
    source 1
    target 484
  ]
  edge [
    source 1
    target 485
  ]
  edge [
    source 1
    target 486
  ]
  edge [
    source 1
    target 487
  ]
  edge [
    source 1
    target 488
  ]
  edge [
    source 1
    target 489
  ]
  edge [
    source 1
    target 490
  ]
  edge [
    source 1
    target 491
  ]
  edge [
    source 1
    target 492
  ]
  edge [
    source 1
    target 493
  ]
  edge [
    source 1
    target 494
  ]
  edge [
    source 1
    target 495
  ]
  edge [
    source 1
    target 496
  ]
  edge [
    source 1
    target 497
  ]
  edge [
    source 1
    target 498
  ]
  edge [
    source 1
    target 499
  ]
  edge [
    source 1
    target 500
  ]
  edge [
    source 1
    target 501
  ]
  edge [
    source 1
    target 502
  ]
  edge [
    source 1
    target 503
  ]
  edge [
    source 1
    target 504
  ]
  edge [
    source 1
    target 505
  ]
  edge [
    source 1
    target 506
  ]
  edge [
    source 1
    target 507
  ]
  edge [
    source 1
    target 508
  ]
  edge [
    source 1
    target 509
  ]
  edge [
    source 1
    target 510
  ]
  edge [
    source 1
    target 511
  ]
  edge [
    source 1
    target 512
  ]
  edge [
    source 1
    target 513
  ]
  edge [
    source 1
    target 514
  ]
  edge [
    source 1
    target 515
  ]
  edge [
    source 1
    target 516
  ]
  edge [
    source 1
    target 517
  ]
  edge [
    source 1
    target 518
  ]
  edge [
    source 1
    target 519
  ]
  edge [
    source 1
    target 520
  ]
  edge [
    source 1
    target 521
  ]
  edge [
    source 1
    target 522
  ]
  edge [
    source 1
    target 523
  ]
  edge [
    source 1
    target 524
  ]
  edge [
    source 1
    target 525
  ]
  edge [
    source 1
    target 526
  ]
  edge [
    source 1
    target 527
  ]
  edge [
    source 1
    target 528
  ]
  edge [
    source 1
    target 529
  ]
  edge [
    source 1
    target 530
  ]
  edge [
    source 1
    target 531
  ]
  edge [
    source 1
    target 532
  ]
  edge [
    source 1
    target 533
  ]
  edge [
    source 1
    target 534
  ]
  edge [
    source 1
    target 535
  ]
  edge [
    source 1
    target 536
  ]
  edge [
    source 1
    target 537
  ]
  edge [
    source 1
    target 538
  ]
  edge [
    source 1
    target 539
  ]
  edge [
    source 1
    target 540
  ]
  edge [
    source 1
    target 541
  ]
  edge [
    source 1
    target 542
  ]
  edge [
    source 1
    target 543
  ]
  edge [
    source 1
    target 544
  ]
  edge [
    source 1
    target 545
  ]
  edge [
    source 1
    target 546
  ]
  edge [
    source 1
    target 547
  ]
  edge [
    source 1
    target 548
  ]
  edge [
    source 1
    target 549
  ]
  edge [
    source 1
    target 550
  ]
  edge [
    source 1
    target 551
  ]
  edge [
    source 1
    target 552
  ]
  edge [
    source 1
    target 553
  ]
  edge [
    source 1
    target 554
  ]
  edge [
    source 1
    target 555
  ]
  edge [
    source 1
    target 556
  ]
  edge [
    source 1
    target 557
  ]
  edge [
    source 1
    target 558
  ]
  edge [
    source 1
    target 559
  ]
  edge [
    source 1
    target 560
  ]
  edge [
    source 1
    target 561
  ]
  edge [
    source 1
    target 562
  ]
  edge [
    source 1
    target 563
  ]
  edge [
    source 1
    target 564
  ]
  edge [
    source 1
    target 565
  ]
  edge [
    source 1
    target 566
  ]
  edge [
    source 1
    target 567
  ]
  edge [
    source 1
    target 568
  ]
  edge [
    source 1
    target 569
  ]
  edge [
    source 1
    target 570
  ]
  edge [
    source 1
    target 571
  ]
  edge [
    source 1
    target 572
  ]
  edge [
    source 1
    target 573
  ]
  edge [
    source 1
    target 574
  ]
  edge [
    source 1
    target 575
  ]
  edge [
    source 1
    target 576
  ]
  edge [
    source 1
    target 577
  ]
  edge [
    source 1
    target 578
  ]
  edge [
    source 1
    target 579
  ]
  edge [
    source 1
    target 580
  ]
  edge [
    source 1
    target 581
  ]
  edge [
    source 1
    target 582
  ]
  edge [
    source 1
    target 583
  ]
  edge [
    source 1
    target 584
  ]
  edge [
    source 1
    target 585
  ]
  edge [
    source 1
    target 586
  ]
  edge [
    source 1
    target 587
  ]
  edge [
    source 1
    target 588
  ]
  edge [
    source 1
    target 589
  ]
  edge [
    source 1
    target 590
  ]
  edge [
    source 1
    target 591
  ]
  edge [
    source 1
    target 592
  ]
  edge [
    source 1
    target 593
  ]
  edge [
    source 1
    target 594
  ]
  edge [
    source 1
    target 595
  ]
  edge [
    source 1
    target 596
  ]
  edge [
    source 1
    target 597
  ]
  edge [
    source 1
    target 598
  ]
  edge [
    source 1
    target 599
  ]
  edge [
    source 1
    target 600
  ]
  edge [
    source 1
    target 601
  ]
  edge [
    source 1
    target 602
  ]
  edge [
    source 1
    target 603
  ]
  edge [
    source 1
    target 604
  ]
  edge [
    source 1
    target 605
  ]
  edge [
    source 1
    target 606
  ]
  edge [
    source 1
    target 607
  ]
  edge [
    source 1
    target 608
  ]
  edge [
    source 1
    target 609
  ]
  edge [
    source 1
    target 610
  ]
  edge [
    source 1
    target 611
  ]
  edge [
    source 1
    target 612
  ]
  edge [
    source 1
    target 613
  ]
  edge [
    source 1
    target 614
  ]
  edge [
    source 1
    target 615
  ]
  edge [
    source 1
    target 616
  ]
  edge [
    source 1
    target 617
  ]
  edge [
    source 1
    target 618
  ]
  edge [
    source 1
    target 619
  ]
  edge [
    source 1
    target 620
  ]
  edge [
    source 1
    target 621
  ]
  edge [
    source 1
    target 622
  ]
  edge [
    source 1
    target 623
  ]
  edge [
    source 1
    target 624
  ]
  edge [
    source 1
    target 625
  ]
  edge [
    source 1
    target 626
  ]
  edge [
    source 1
    target 627
  ]
  edge [
    source 1
    target 628
  ]
  edge [
    source 1
    target 629
  ]
  edge [
    source 1
    target 630
  ]
  edge [
    source 1
    target 631
  ]
  edge [
    source 1
    target 632
  ]
  edge [
    source 1
    target 633
  ]
  edge [
    source 1
    target 634
  ]
  edge [
    source 1
    target 635
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 636
  ]
  edge [
    source 3
    target 637
  ]
  edge [
    source 3
    target 638
  ]
  edge [
    source 3
    target 639
  ]
  edge [
    source 3
    target 640
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 654
  ]
  edge [
    source 5
    target 655
  ]
  edge [
    source 5
    target 656
  ]
  edge [
    source 5
    target 657
  ]
  edge [
    source 5
    target 658
  ]
  edge [
    source 5
    target 659
  ]
  edge [
    source 5
    target 660
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 708
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 710
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 714
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 750
  ]
  edge [
    source 18
    target 751
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 756
  ]
  edge [
    source 20
    target 757
  ]
  edge [
    source 20
    target 758
  ]
  edge [
    source 20
    target 759
  ]
  edge [
    source 20
    target 760
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 761
  ]
  edge [
    source 23
    target 762
  ]
  edge [
    source 23
    target 763
  ]
  edge [
    source 23
    target 764
  ]
  edge [
    source 23
    target 394
  ]
  edge [
    source 23
    target 765
  ]
  edge [
    source 23
    target 766
  ]
  edge [
    source 23
    target 767
  ]
  edge [
    source 23
    target 768
  ]
  edge [
    source 23
    target 769
  ]
  edge [
    source 23
    target 122
  ]
  edge [
    source 23
    target 770
  ]
  edge [
    source 23
    target 771
  ]
  edge [
    source 23
    target 772
  ]
  edge [
    source 23
    target 773
  ]
  edge [
    source 23
    target 774
  ]
  edge [
    source 23
    target 775
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 776
  ]
  edge [
    source 24
    target 777
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 778
  ]
  edge [
    source 25
    target 779
  ]
  edge [
    source 25
    target 780
  ]
  edge [
    source 25
    target 781
  ]
  edge [
    source 25
    target 782
  ]
  edge [
    source 25
    target 783
  ]
  edge [
    source 25
    target 784
  ]
  edge [
    source 25
    target 785
  ]
  edge [
    source 25
    target 786
  ]
  edge [
    source 25
    target 787
  ]
  edge [
    source 25
    target 788
  ]
  edge [
    source 25
    target 789
  ]
  edge [
    source 25
    target 790
  ]
  edge [
    source 25
    target 791
  ]
  edge [
    source 25
    target 792
  ]
  edge [
    source 25
    target 793
  ]
  edge [
    source 25
    target 794
  ]
  edge [
    source 25
    target 795
  ]
  edge [
    source 25
    target 796
  ]
  edge [
    source 25
    target 797
  ]
  edge [
    source 25
    target 798
  ]
  edge [
    source 25
    target 799
  ]
  edge [
    source 25
    target 800
  ]
  edge [
    source 25
    target 801
  ]
  edge [
    source 25
    target 802
  ]
  edge [
    source 25
    target 803
  ]
  edge [
    source 25
    target 804
  ]
  edge [
    source 25
    target 805
  ]
  edge [
    source 25
    target 806
  ]
  edge [
    source 25
    target 807
  ]
  edge [
    source 25
    target 808
  ]
  edge [
    source 25
    target 809
  ]
  edge [
    source 25
    target 810
  ]
  edge [
    source 25
    target 811
  ]
  edge [
    source 25
    target 812
  ]
  edge [
    source 25
    target 813
  ]
  edge [
    source 25
    target 814
  ]
  edge [
    source 25
    target 815
  ]
  edge [
    source 25
    target 816
  ]
  edge [
    source 25
    target 817
  ]
  edge [
    source 25
    target 818
  ]
  edge [
    source 25
    target 819
  ]
  edge [
    source 25
    target 820
  ]
  edge [
    source 25
    target 821
  ]
  edge [
    source 25
    target 822
  ]
  edge [
    source 25
    target 823
  ]
  edge [
    source 25
    target 824
  ]
  edge [
    source 25
    target 825
  ]
  edge [
    source 25
    target 826
  ]
  edge [
    source 25
    target 827
  ]
  edge [
    source 25
    target 828
  ]
  edge [
    source 25
    target 829
  ]
  edge [
    source 25
    target 830
  ]
  edge [
    source 25
    target 831
  ]
  edge [
    source 25
    target 832
  ]
  edge [
    source 25
    target 833
  ]
  edge [
    source 25
    target 834
  ]
  edge [
    source 25
    target 835
  ]
  edge [
    source 25
    target 836
  ]
  edge [
    source 25
    target 837
  ]
  edge [
    source 25
    target 838
  ]
  edge [
    source 25
    target 839
  ]
  edge [
    source 25
    target 840
  ]
  edge [
    source 25
    target 841
  ]
  edge [
    source 25
    target 842
  ]
  edge [
    source 25
    target 843
  ]
  edge [
    source 25
    target 844
  ]
  edge [
    source 25
    target 845
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 846
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 847
  ]
  edge [
    source 27
    target 848
  ]
  edge [
    source 27
    target 780
  ]
  edge [
    source 27
    target 849
  ]
  edge [
    source 27
    target 850
  ]
  edge [
    source 27
    target 851
  ]
  edge [
    source 27
    target 394
  ]
  edge [
    source 27
    target 852
  ]
  edge [
    source 27
    target 853
  ]
  edge [
    source 27
    target 854
  ]
  edge [
    source 27
    target 846
  ]
  edge [
    source 27
    target 855
  ]
  edge [
    source 27
    target 856
  ]
  edge [
    source 28
    target 857
  ]
  edge [
    source 28
    target 846
  ]
  edge [
    source 28
    target 858
  ]
  edge [
    source 28
    target 859
  ]
  edge [
    source 28
    target 860
  ]
]
