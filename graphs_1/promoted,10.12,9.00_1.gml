graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.894736842105263
  density 0.10526315789473684
  graphCliqueNumber 2
  node [
    id 0
    label "arabski"
    origin "text"
  ]
  node [
    id 1
    label "ulotka"
    origin "text"
  ]
  node [
    id 2
    label "druz"
  ]
  node [
    id 3
    label "j&#281;zyk_semicki"
  ]
  node [
    id 4
    label "taniec_brzucha"
  ]
  node [
    id 5
    label "Arabic"
  ]
  node [
    id 6
    label "j&#281;zyk"
  ]
  node [
    id 7
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 8
    label "arabsko"
  ]
  node [
    id 9
    label "agal"
  ]
  node [
    id 10
    label "harira"
  ]
  node [
    id 11
    label "abaja"
  ]
  node [
    id 12
    label "po_arabsku"
  ]
  node [
    id 13
    label "d&#380;ellaba"
  ]
  node [
    id 14
    label "typowy"
  ]
  node [
    id 15
    label "&#380;uaw"
  ]
  node [
    id 16
    label "booklet"
  ]
  node [
    id 17
    label "druk_ulotny"
  ]
  node [
    id 18
    label "reklama"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
]
