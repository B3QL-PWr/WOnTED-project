graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "antoni"
    origin "text"
  ]
  node [
    id 1
    label "go&#347;cia&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "Antoni"
  ]
  node [
    id 3
    label "Go&#347;cia&#324;ski"
  ]
  node [
    id 4
    label "Stanis&#322;awa"
  ]
  node [
    id 5
    label "zalewski"
  ]
  node [
    id 6
    label "CWKS"
  ]
  node [
    id 7
    label "warszawa"
  ]
  node [
    id 8
    label "igrzysko"
  ]
  node [
    id 9
    label "olimpijski"
  ]
  node [
    id 10
    label "mistrzostwo"
  ]
  node [
    id 11
    label "europ"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
]
