graph [
  maxDegree 64
  minDegree 1
  meanDegree 2.0123456790123457
  density 0.012499041484548732
  graphCliqueNumber 3
  node [
    id 0
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 1
    label "godzina"
    origin "text"
  ]
  node [
    id 2
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wiadek"
    origin "text"
  ]
  node [
    id 4
    label "j&#243;zef"
    origin "text"
  ]
  node [
    id 5
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 6
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 7
    label "ma&#322;&#380;e&#324;ski"
    origin "text"
  ]
  node [
    id 8
    label "wst&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "burmistrz"
    origin "text"
  ]
  node [
    id 10
    label "piotr"
    origin "text"
  ]
  node [
    id 11
    label "gizi&#324;ski"
    origin "text"
  ]
  node [
    id 12
    label "wybranka"
    origin "text"
  ]
  node [
    id 13
    label "serce"
    origin "text"
  ]
  node [
    id 14
    label "by&#263;"
    origin "text"
  ]
  node [
    id 15
    label "ozorkowianka"
    origin "text"
  ]
  node [
    id 16
    label "edyta"
    origin "text"
  ]
  node [
    id 17
    label "graczyk"
    origin "text"
  ]
  node [
    id 18
    label "doba"
  ]
  node [
    id 19
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 20
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 21
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 22
    label "minuta"
  ]
  node [
    id 23
    label "czas"
  ]
  node [
    id 24
    label "p&#243;&#322;godzina"
  ]
  node [
    id 25
    label "kwadrans"
  ]
  node [
    id 26
    label "time"
  ]
  node [
    id 27
    label "jednostka_czasu"
  ]
  node [
    id 28
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 29
    label "zakrystia"
  ]
  node [
    id 30
    label "organizacja_religijna"
  ]
  node [
    id 31
    label "nawa"
  ]
  node [
    id 32
    label "nerwica_eklezjogenna"
  ]
  node [
    id 33
    label "kropielnica"
  ]
  node [
    id 34
    label "prezbiterium"
  ]
  node [
    id 35
    label "wsp&#243;lnota"
  ]
  node [
    id 36
    label "church"
  ]
  node [
    id 37
    label "kruchta"
  ]
  node [
    id 38
    label "Ska&#322;ka"
  ]
  node [
    id 39
    label "kult"
  ]
  node [
    id 40
    label "ub&#322;agalnia"
  ]
  node [
    id 41
    label "dom"
  ]
  node [
    id 42
    label "cz&#322;owiek"
  ]
  node [
    id 43
    label "s&#261;d"
  ]
  node [
    id 44
    label "osoba_fizyczna"
  ]
  node [
    id 45
    label "uczestnik"
  ]
  node [
    id 46
    label "obserwator"
  ]
  node [
    id 47
    label "dru&#380;ba"
  ]
  node [
    id 48
    label "odwodnienie"
  ]
  node [
    id 49
    label "konstytucja"
  ]
  node [
    id 50
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 51
    label "substancja_chemiczna"
  ]
  node [
    id 52
    label "bratnia_dusza"
  ]
  node [
    id 53
    label "zwi&#261;zanie"
  ]
  node [
    id 54
    label "lokant"
  ]
  node [
    id 55
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 56
    label "zwi&#261;za&#263;"
  ]
  node [
    id 57
    label "organizacja"
  ]
  node [
    id 58
    label "odwadnia&#263;"
  ]
  node [
    id 59
    label "marriage"
  ]
  node [
    id 60
    label "marketing_afiliacyjny"
  ]
  node [
    id 61
    label "bearing"
  ]
  node [
    id 62
    label "wi&#261;zanie"
  ]
  node [
    id 63
    label "odwadnianie"
  ]
  node [
    id 64
    label "koligacja"
  ]
  node [
    id 65
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 66
    label "odwodni&#263;"
  ]
  node [
    id 67
    label "azeotrop"
  ]
  node [
    id 68
    label "powi&#261;zanie"
  ]
  node [
    id 69
    label "specjalny"
  ]
  node [
    id 70
    label "po_ma&#322;&#380;e&#324;sku"
  ]
  node [
    id 71
    label "typowy"
  ]
  node [
    id 72
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 73
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 74
    label "wpada&#263;"
  ]
  node [
    id 75
    label "wchodzi&#263;"
  ]
  node [
    id 76
    label "&#322;oi&#263;"
  ]
  node [
    id 77
    label "osi&#261;ga&#263;"
  ]
  node [
    id 78
    label "submit"
  ]
  node [
    id 79
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 80
    label "nachodzi&#263;"
  ]
  node [
    id 81
    label "intervene"
  ]
  node [
    id 82
    label "scale"
  ]
  node [
    id 83
    label "ceklarz"
  ]
  node [
    id 84
    label "miasto"
  ]
  node [
    id 85
    label "burmistrzyna"
  ]
  node [
    id 86
    label "samorz&#261;dowiec"
  ]
  node [
    id 87
    label "mi&#322;a"
  ]
  node [
    id 88
    label "partia"
  ]
  node [
    id 89
    label "jedyna"
  ]
  node [
    id 90
    label "courage"
  ]
  node [
    id 91
    label "mi&#281;sie&#324;"
  ]
  node [
    id 92
    label "kompleks"
  ]
  node [
    id 93
    label "sfera_afektywna"
  ]
  node [
    id 94
    label "heart"
  ]
  node [
    id 95
    label "sumienie"
  ]
  node [
    id 96
    label "przedsionek"
  ]
  node [
    id 97
    label "entity"
  ]
  node [
    id 98
    label "nastawienie"
  ]
  node [
    id 99
    label "punkt"
  ]
  node [
    id 100
    label "kompleksja"
  ]
  node [
    id 101
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 102
    label "kier"
  ]
  node [
    id 103
    label "systol"
  ]
  node [
    id 104
    label "pikawa"
  ]
  node [
    id 105
    label "power"
  ]
  node [
    id 106
    label "zastawka"
  ]
  node [
    id 107
    label "psychika"
  ]
  node [
    id 108
    label "wola"
  ]
  node [
    id 109
    label "komora"
  ]
  node [
    id 110
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 111
    label "zapalno&#347;&#263;"
  ]
  node [
    id 112
    label "wsierdzie"
  ]
  node [
    id 113
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 114
    label "podekscytowanie"
  ]
  node [
    id 115
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 116
    label "strunowiec"
  ]
  node [
    id 117
    label "favor"
  ]
  node [
    id 118
    label "dusza"
  ]
  node [
    id 119
    label "fizjonomia"
  ]
  node [
    id 120
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 121
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 122
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 123
    label "charakter"
  ]
  node [
    id 124
    label "mikrokosmos"
  ]
  node [
    id 125
    label "dzwon"
  ]
  node [
    id 126
    label "koniuszek_serca"
  ]
  node [
    id 127
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 128
    label "passion"
  ]
  node [
    id 129
    label "cecha"
  ]
  node [
    id 130
    label "pulsowa&#263;"
  ]
  node [
    id 131
    label "ego"
  ]
  node [
    id 132
    label "organ"
  ]
  node [
    id 133
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 134
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 135
    label "kardiografia"
  ]
  node [
    id 136
    label "osobowo&#347;&#263;"
  ]
  node [
    id 137
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 138
    label "kszta&#322;t"
  ]
  node [
    id 139
    label "karta"
  ]
  node [
    id 140
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 141
    label "dobro&#263;"
  ]
  node [
    id 142
    label "podroby"
  ]
  node [
    id 143
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 144
    label "pulsowanie"
  ]
  node [
    id 145
    label "deformowa&#263;"
  ]
  node [
    id 146
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 147
    label "seksualno&#347;&#263;"
  ]
  node [
    id 148
    label "deformowanie"
  ]
  node [
    id 149
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 150
    label "elektrokardiografia"
  ]
  node [
    id 151
    label "si&#281;ga&#263;"
  ]
  node [
    id 152
    label "trwa&#263;"
  ]
  node [
    id 153
    label "obecno&#347;&#263;"
  ]
  node [
    id 154
    label "stan"
  ]
  node [
    id 155
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 156
    label "stand"
  ]
  node [
    id 157
    label "mie&#263;_miejsce"
  ]
  node [
    id 158
    label "uczestniczy&#263;"
  ]
  node [
    id 159
    label "chodzi&#263;"
  ]
  node [
    id 160
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 161
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 42
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
]
