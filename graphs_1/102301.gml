graph [
  maxDegree 47
  minDegree 1
  meanDegree 2.0208333333333335
  density 0.010580279232111692
  graphCliqueNumber 2
  node [
    id 0
    label "bardzo"
    origin "text"
  ]
  node [
    id 1
    label "dobre"
    origin "text"
  ]
  node [
    id 2
    label "miejsce"
    origin "text"
  ]
  node [
    id 3
    label "przerwa"
    origin "text"
  ]
  node [
    id 4
    label "ci&#281;&#380;ki"
    origin "text"
  ]
  node [
    id 5
    label "rozwa&#380;anie"
    origin "text"
  ]
  node [
    id 6
    label "teoretyczny"
    origin "text"
  ]
  node [
    id 7
    label "ciekawy"
    origin "text"
  ]
  node [
    id 8
    label "inspirowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "rozdzia&#322;"
    origin "text"
  ]
  node [
    id 10
    label "przed"
    origin "text"
  ]
  node [
    id 11
    label "bogactwo"
    origin "text"
  ]
  node [
    id 12
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 14
    label "nasa"
    origin "text"
  ]
  node [
    id 15
    label "wszyscy"
    origin "text"
  ]
  node [
    id 16
    label "chyba"
    origin "text"
  ]
  node [
    id 17
    label "prawo"
    origin "text"
  ]
  node [
    id 18
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 19
    label "inaczej"
    origin "text"
  ]
  node [
    id 20
    label "w_chuj"
  ]
  node [
    id 21
    label "cia&#322;o"
  ]
  node [
    id 22
    label "plac"
  ]
  node [
    id 23
    label "cecha"
  ]
  node [
    id 24
    label "uwaga"
  ]
  node [
    id 25
    label "przestrze&#324;"
  ]
  node [
    id 26
    label "status"
  ]
  node [
    id 27
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 28
    label "chwila"
  ]
  node [
    id 29
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 30
    label "rz&#261;d"
  ]
  node [
    id 31
    label "praca"
  ]
  node [
    id 32
    label "location"
  ]
  node [
    id 33
    label "warunek_lokalowy"
  ]
  node [
    id 34
    label "pauza"
  ]
  node [
    id 35
    label "przedzia&#322;"
  ]
  node [
    id 36
    label "czas"
  ]
  node [
    id 37
    label "nieprzejrzysty"
  ]
  node [
    id 38
    label "wolny"
  ]
  node [
    id 39
    label "grubo"
  ]
  node [
    id 40
    label "przyswajalny"
  ]
  node [
    id 41
    label "masywny"
  ]
  node [
    id 42
    label "zbrojny"
  ]
  node [
    id 43
    label "gro&#378;ny"
  ]
  node [
    id 44
    label "trudny"
  ]
  node [
    id 45
    label "wymagaj&#261;cy"
  ]
  node [
    id 46
    label "ambitny"
  ]
  node [
    id 47
    label "monumentalny"
  ]
  node [
    id 48
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 49
    label "niezgrabny"
  ]
  node [
    id 50
    label "charakterystyczny"
  ]
  node [
    id 51
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 52
    label "k&#322;opotliwy"
  ]
  node [
    id 53
    label "dotkliwy"
  ]
  node [
    id 54
    label "nieudany"
  ]
  node [
    id 55
    label "mocny"
  ]
  node [
    id 56
    label "bojowy"
  ]
  node [
    id 57
    label "ci&#281;&#380;ko"
  ]
  node [
    id 58
    label "kompletny"
  ]
  node [
    id 59
    label "intensywny"
  ]
  node [
    id 60
    label "wielki"
  ]
  node [
    id 61
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 62
    label "liczny"
  ]
  node [
    id 63
    label "niedelikatny"
  ]
  node [
    id 64
    label "wytw&#243;r"
  ]
  node [
    id 65
    label "examination"
  ]
  node [
    id 66
    label "contemplation"
  ]
  node [
    id 67
    label "porcjowanie"
  ]
  node [
    id 68
    label "przemy&#347;liwanie"
  ]
  node [
    id 69
    label "teoretycznie"
  ]
  node [
    id 70
    label "nierealny"
  ]
  node [
    id 71
    label "swoisty"
  ]
  node [
    id 72
    label "cz&#322;owiek"
  ]
  node [
    id 73
    label "interesowanie"
  ]
  node [
    id 74
    label "nietuzinkowy"
  ]
  node [
    id 75
    label "ciekawie"
  ]
  node [
    id 76
    label "indagator"
  ]
  node [
    id 77
    label "interesuj&#261;cy"
  ]
  node [
    id 78
    label "dziwny"
  ]
  node [
    id 79
    label "intryguj&#261;cy"
  ]
  node [
    id 80
    label "ch&#281;tny"
  ]
  node [
    id 81
    label "kopiowa&#263;"
  ]
  node [
    id 82
    label "wk&#322;ada&#263;"
  ]
  node [
    id 83
    label "motywowa&#263;"
  ]
  node [
    id 84
    label "tug"
  ]
  node [
    id 85
    label "faza"
  ]
  node [
    id 86
    label "interruption"
  ]
  node [
    id 87
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 88
    label "podzia&#322;"
  ]
  node [
    id 89
    label "podrozdzia&#322;"
  ]
  node [
    id 90
    label "wydarzenie"
  ]
  node [
    id 91
    label "fragment"
  ]
  node [
    id 92
    label "podostatek"
  ]
  node [
    id 93
    label "fortune"
  ]
  node [
    id 94
    label "wysyp"
  ]
  node [
    id 95
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 96
    label "mienie"
  ]
  node [
    id 97
    label "fullness"
  ]
  node [
    id 98
    label "ilo&#347;&#263;"
  ]
  node [
    id 99
    label "sytuacja"
  ]
  node [
    id 100
    label "z&#322;ote_czasy"
  ]
  node [
    id 101
    label "hipertekst"
  ]
  node [
    id 102
    label "gauze"
  ]
  node [
    id 103
    label "nitka"
  ]
  node [
    id 104
    label "mesh"
  ]
  node [
    id 105
    label "e-hazard"
  ]
  node [
    id 106
    label "netbook"
  ]
  node [
    id 107
    label "cyberprzestrze&#324;"
  ]
  node [
    id 108
    label "biznes_elektroniczny"
  ]
  node [
    id 109
    label "snu&#263;"
  ]
  node [
    id 110
    label "organization"
  ]
  node [
    id 111
    label "zasadzka"
  ]
  node [
    id 112
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 113
    label "web"
  ]
  node [
    id 114
    label "provider"
  ]
  node [
    id 115
    label "struktura"
  ]
  node [
    id 116
    label "us&#322;uga_internetowa"
  ]
  node [
    id 117
    label "punkt_dost&#281;pu"
  ]
  node [
    id 118
    label "organizacja"
  ]
  node [
    id 119
    label "mem"
  ]
  node [
    id 120
    label "vane"
  ]
  node [
    id 121
    label "podcast"
  ]
  node [
    id 122
    label "grooming"
  ]
  node [
    id 123
    label "kszta&#322;t"
  ]
  node [
    id 124
    label "strona"
  ]
  node [
    id 125
    label "obiekt"
  ]
  node [
    id 126
    label "wysnu&#263;"
  ]
  node [
    id 127
    label "gra_sieciowa"
  ]
  node [
    id 128
    label "instalacja"
  ]
  node [
    id 129
    label "sie&#263;_komputerowa"
  ]
  node [
    id 130
    label "net"
  ]
  node [
    id 131
    label "plecionka"
  ]
  node [
    id 132
    label "media"
  ]
  node [
    id 133
    label "rozmieszczenie"
  ]
  node [
    id 134
    label "by&#263;"
  ]
  node [
    id 135
    label "hold"
  ]
  node [
    id 136
    label "sp&#281;dza&#263;"
  ]
  node [
    id 137
    label "look"
  ]
  node [
    id 138
    label "decydowa&#263;"
  ]
  node [
    id 139
    label "oczekiwa&#263;"
  ]
  node [
    id 140
    label "pauzowa&#263;"
  ]
  node [
    id 141
    label "anticipate"
  ]
  node [
    id 142
    label "obserwacja"
  ]
  node [
    id 143
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 144
    label "nauka_prawa"
  ]
  node [
    id 145
    label "dominion"
  ]
  node [
    id 146
    label "normatywizm"
  ]
  node [
    id 147
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 148
    label "qualification"
  ]
  node [
    id 149
    label "opis"
  ]
  node [
    id 150
    label "regu&#322;a_Allena"
  ]
  node [
    id 151
    label "normalizacja"
  ]
  node [
    id 152
    label "kazuistyka"
  ]
  node [
    id 153
    label "regu&#322;a_Glogera"
  ]
  node [
    id 154
    label "kultura_duchowa"
  ]
  node [
    id 155
    label "prawo_karne"
  ]
  node [
    id 156
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 157
    label "standard"
  ]
  node [
    id 158
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 159
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 160
    label "szko&#322;a"
  ]
  node [
    id 161
    label "prawo_karne_procesowe"
  ]
  node [
    id 162
    label "prawo_Mendla"
  ]
  node [
    id 163
    label "przepis"
  ]
  node [
    id 164
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 165
    label "criterion"
  ]
  node [
    id 166
    label "kanonistyka"
  ]
  node [
    id 167
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 168
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 169
    label "wykonawczy"
  ]
  node [
    id 170
    label "twierdzenie"
  ]
  node [
    id 171
    label "judykatura"
  ]
  node [
    id 172
    label "legislacyjnie"
  ]
  node [
    id 173
    label "umocowa&#263;"
  ]
  node [
    id 174
    label "podmiot"
  ]
  node [
    id 175
    label "procesualistyka"
  ]
  node [
    id 176
    label "kierunek"
  ]
  node [
    id 177
    label "kryminologia"
  ]
  node [
    id 178
    label "kryminalistyka"
  ]
  node [
    id 179
    label "cywilistyka"
  ]
  node [
    id 180
    label "law"
  ]
  node [
    id 181
    label "zasada_d'Alemberta"
  ]
  node [
    id 182
    label "jurisprudence"
  ]
  node [
    id 183
    label "zasada"
  ]
  node [
    id 184
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 185
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 186
    label "sta&#263;_si&#281;"
  ]
  node [
    id 187
    label "zrobi&#263;"
  ]
  node [
    id 188
    label "podj&#261;&#263;"
  ]
  node [
    id 189
    label "determine"
  ]
  node [
    id 190
    label "niestandardowo"
  ]
  node [
    id 191
    label "inny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
]
