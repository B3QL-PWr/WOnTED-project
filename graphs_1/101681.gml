graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.251101321585903
  density 0.004969318590697358
  graphCliqueNumber 4
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "wysoki"
    origin "text"
  ]
  node [
    id 3
    label "izba"
    origin "text"
  ]
  node [
    id 4
    label "minister"
    origin "text"
  ]
  node [
    id 5
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "tak"
    origin "text"
  ]
  node [
    id 7
    label "jak"
    origin "text"
  ]
  node [
    id 8
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "moi"
    origin "text"
  ]
  node [
    id 10
    label "przedm&#243;wca"
    origin "text"
  ]
  node [
    id 11
    label "wielki"
    origin "text"
  ]
  node [
    id 12
    label "podzi&#281;kowanie"
    origin "text"
  ]
  node [
    id 13
    label "dla"
    origin "text"
  ]
  node [
    id 14
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 15
    label "ten"
    origin "text"
  ]
  node [
    id 16
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 17
    label "inicjatywa"
    origin "text"
  ]
  node [
    id 18
    label "szczeg&#243;lnie"
    origin "text"
  ]
  node [
    id 19
    label "jakub"
    origin "text"
  ]
  node [
    id 20
    label "p&#322;a&#380;y&#324;skiego"
    origin "text"
  ]
  node [
    id 21
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 22
    label "dobrze"
    origin "text"
  ]
  node [
    id 23
    label "by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "polska"
    origin "text"
  ]
  node [
    id 25
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 26
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 27
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 28
    label "osoba"
    origin "text"
  ]
  node [
    id 29
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 30
    label "ponad"
    origin "text"
  ]
  node [
    id 31
    label "lata"
    origin "text"
  ]
  node [
    id 32
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 33
    label "wype&#322;nienie"
    origin "text"
  ]
  node [
    id 34
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 35
    label "dziejowy"
    origin "text"
  ]
  node [
    id 36
    label "mo&#380;liwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 37
    label "powr&#243;t"
    origin "text"
  ]
  node [
    id 38
    label "ojczyzna"
    origin "text"
  ]
  node [
    id 39
    label "wiele"
    origin "text"
  ]
  node [
    id 40
    label "inny"
    origin "text"
  ]
  node [
    id 41
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 42
    label "narodowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 43
    label "dawno"
    origin "text"
  ]
  node [
    id 44
    label "wr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 45
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 46
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 47
    label "nasa"
    origin "text"
  ]
  node [
    id 48
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 49
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 50
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 51
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 52
    label "czas"
    origin "text"
  ]
  node [
    id 53
    label "realizowa&#263;"
    origin "text"
  ]
  node [
    id 54
    label "albo"
    origin "text"
  ]
  node [
    id 55
    label "po&#347;rednictwo"
    origin "text"
  ]
  node [
    id 56
    label "samorz&#261;d"
    origin "text"
  ]
  node [
    id 57
    label "profesor"
  ]
  node [
    id 58
    label "kszta&#322;ciciel"
  ]
  node [
    id 59
    label "jegomo&#347;&#263;"
  ]
  node [
    id 60
    label "zwrot"
  ]
  node [
    id 61
    label "pracodawca"
  ]
  node [
    id 62
    label "rz&#261;dzenie"
  ]
  node [
    id 63
    label "m&#261;&#380;"
  ]
  node [
    id 64
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 65
    label "ch&#322;opina"
  ]
  node [
    id 66
    label "bratek"
  ]
  node [
    id 67
    label "opiekun"
  ]
  node [
    id 68
    label "doros&#322;y"
  ]
  node [
    id 69
    label "preceptor"
  ]
  node [
    id 70
    label "Midas"
  ]
  node [
    id 71
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 72
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 73
    label "murza"
  ]
  node [
    id 74
    label "ojciec"
  ]
  node [
    id 75
    label "androlog"
  ]
  node [
    id 76
    label "pupil"
  ]
  node [
    id 77
    label "efendi"
  ]
  node [
    id 78
    label "nabab"
  ]
  node [
    id 79
    label "w&#322;odarz"
  ]
  node [
    id 80
    label "szkolnik"
  ]
  node [
    id 81
    label "pedagog"
  ]
  node [
    id 82
    label "popularyzator"
  ]
  node [
    id 83
    label "andropauza"
  ]
  node [
    id 84
    label "gra_w_karty"
  ]
  node [
    id 85
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 86
    label "Mieszko_I"
  ]
  node [
    id 87
    label "bogaty"
  ]
  node [
    id 88
    label "samiec"
  ]
  node [
    id 89
    label "przyw&#243;dca"
  ]
  node [
    id 90
    label "pa&#324;stwo"
  ]
  node [
    id 91
    label "belfer"
  ]
  node [
    id 92
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 93
    label "dostojnik"
  ]
  node [
    id 94
    label "oficer"
  ]
  node [
    id 95
    label "parlamentarzysta"
  ]
  node [
    id 96
    label "Pi&#322;sudski"
  ]
  node [
    id 97
    label "warto&#347;ciowy"
  ]
  node [
    id 98
    label "du&#380;y"
  ]
  node [
    id 99
    label "wysoce"
  ]
  node [
    id 100
    label "daleki"
  ]
  node [
    id 101
    label "znaczny"
  ]
  node [
    id 102
    label "wysoko"
  ]
  node [
    id 103
    label "szczytnie"
  ]
  node [
    id 104
    label "wznios&#322;y"
  ]
  node [
    id 105
    label "wyrafinowany"
  ]
  node [
    id 106
    label "z_wysoka"
  ]
  node [
    id 107
    label "chwalebny"
  ]
  node [
    id 108
    label "uprzywilejowany"
  ]
  node [
    id 109
    label "niepo&#347;ledni"
  ]
  node [
    id 110
    label "pok&#243;j"
  ]
  node [
    id 111
    label "parlament"
  ]
  node [
    id 112
    label "zwi&#261;zek"
  ]
  node [
    id 113
    label "NIK"
  ]
  node [
    id 114
    label "urz&#261;d"
  ]
  node [
    id 115
    label "organ"
  ]
  node [
    id 116
    label "pomieszczenie"
  ]
  node [
    id 117
    label "Goebbels"
  ]
  node [
    id 118
    label "Sto&#322;ypin"
  ]
  node [
    id 119
    label "rz&#261;d"
  ]
  node [
    id 120
    label "cause"
  ]
  node [
    id 121
    label "introduce"
  ]
  node [
    id 122
    label "begin"
  ]
  node [
    id 123
    label "odj&#261;&#263;"
  ]
  node [
    id 124
    label "post&#261;pi&#263;"
  ]
  node [
    id 125
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 126
    label "do"
  ]
  node [
    id 127
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 128
    label "zrobi&#263;"
  ]
  node [
    id 129
    label "byd&#322;o"
  ]
  node [
    id 130
    label "zobo"
  ]
  node [
    id 131
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 132
    label "yakalo"
  ]
  node [
    id 133
    label "dzo"
  ]
  node [
    id 134
    label "whole"
  ]
  node [
    id 135
    label "Rzym_Zachodni"
  ]
  node [
    id 136
    label "element"
  ]
  node [
    id 137
    label "ilo&#347;&#263;"
  ]
  node [
    id 138
    label "urz&#261;dzenie"
  ]
  node [
    id 139
    label "Rzym_Wschodni"
  ]
  node [
    id 140
    label "m&#243;wca"
  ]
  node [
    id 141
    label "rozm&#243;wca"
  ]
  node [
    id 142
    label "poprzednik"
  ]
  node [
    id 143
    label "dupny"
  ]
  node [
    id 144
    label "wyj&#261;tkowy"
  ]
  node [
    id 145
    label "wybitny"
  ]
  node [
    id 146
    label "prawdziwy"
  ]
  node [
    id 147
    label "nieprzeci&#281;tny"
  ]
  node [
    id 148
    label "wypowied&#378;"
  ]
  node [
    id 149
    label "z&#322;o&#380;enie"
  ]
  node [
    id 150
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 151
    label "denial"
  ]
  node [
    id 152
    label "notyfikowanie"
  ]
  node [
    id 153
    label "notyfikowa&#263;"
  ]
  node [
    id 154
    label "cz&#322;onek"
  ]
  node [
    id 155
    label "substytuowanie"
  ]
  node [
    id 156
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 157
    label "przyk&#322;ad"
  ]
  node [
    id 158
    label "zast&#281;pca"
  ]
  node [
    id 159
    label "substytuowa&#263;"
  ]
  node [
    id 160
    label "okre&#347;lony"
  ]
  node [
    id 161
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 162
    label "silny"
  ]
  node [
    id 163
    label "wa&#380;nie"
  ]
  node [
    id 164
    label "eksponowany"
  ]
  node [
    id 165
    label "istotnie"
  ]
  node [
    id 166
    label "dobry"
  ]
  node [
    id 167
    label "wynios&#322;y"
  ]
  node [
    id 168
    label "dono&#347;ny"
  ]
  node [
    id 169
    label "plan"
  ]
  node [
    id 170
    label "propozycja"
  ]
  node [
    id 171
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 172
    label "szczeg&#243;lny"
  ]
  node [
    id 173
    label "osobnie"
  ]
  node [
    id 174
    label "wyj&#261;tkowo"
  ]
  node [
    id 175
    label "specially"
  ]
  node [
    id 176
    label "ma&#347;lak_pstry"
  ]
  node [
    id 177
    label "take_care"
  ]
  node [
    id 178
    label "troska&#263;_si&#281;"
  ]
  node [
    id 179
    label "zamierza&#263;"
  ]
  node [
    id 180
    label "os&#261;dza&#263;"
  ]
  node [
    id 181
    label "robi&#263;"
  ]
  node [
    id 182
    label "argue"
  ]
  node [
    id 183
    label "rozpatrywa&#263;"
  ]
  node [
    id 184
    label "deliver"
  ]
  node [
    id 185
    label "moralnie"
  ]
  node [
    id 186
    label "lepiej"
  ]
  node [
    id 187
    label "korzystnie"
  ]
  node [
    id 188
    label "pomy&#347;lnie"
  ]
  node [
    id 189
    label "pozytywnie"
  ]
  node [
    id 190
    label "dobroczynnie"
  ]
  node [
    id 191
    label "odpowiednio"
  ]
  node [
    id 192
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 193
    label "skutecznie"
  ]
  node [
    id 194
    label "si&#281;ga&#263;"
  ]
  node [
    id 195
    label "trwa&#263;"
  ]
  node [
    id 196
    label "obecno&#347;&#263;"
  ]
  node [
    id 197
    label "stan"
  ]
  node [
    id 198
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 199
    label "stand"
  ]
  node [
    id 200
    label "mie&#263;_miejsce"
  ]
  node [
    id 201
    label "uczestniczy&#263;"
  ]
  node [
    id 202
    label "chodzi&#263;"
  ]
  node [
    id 203
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 204
    label "equal"
  ]
  node [
    id 205
    label "asymilowa&#263;"
  ]
  node [
    id 206
    label "wapniak"
  ]
  node [
    id 207
    label "dwun&#243;g"
  ]
  node [
    id 208
    label "polifag"
  ]
  node [
    id 209
    label "wz&#243;r"
  ]
  node [
    id 210
    label "profanum"
  ]
  node [
    id 211
    label "hominid"
  ]
  node [
    id 212
    label "homo_sapiens"
  ]
  node [
    id 213
    label "nasada"
  ]
  node [
    id 214
    label "podw&#322;adny"
  ]
  node [
    id 215
    label "ludzko&#347;&#263;"
  ]
  node [
    id 216
    label "os&#322;abianie"
  ]
  node [
    id 217
    label "mikrokosmos"
  ]
  node [
    id 218
    label "portrecista"
  ]
  node [
    id 219
    label "duch"
  ]
  node [
    id 220
    label "g&#322;owa"
  ]
  node [
    id 221
    label "oddzia&#322;ywanie"
  ]
  node [
    id 222
    label "asymilowanie"
  ]
  node [
    id 223
    label "os&#322;abia&#263;"
  ]
  node [
    id 224
    label "figura"
  ]
  node [
    id 225
    label "Adam"
  ]
  node [
    id 226
    label "senior"
  ]
  node [
    id 227
    label "antropochoria"
  ]
  node [
    id 228
    label "posta&#263;"
  ]
  node [
    id 229
    label "zna&#263;"
  ]
  node [
    id 230
    label "zachowywa&#263;"
  ]
  node [
    id 231
    label "chowa&#263;"
  ]
  node [
    id 232
    label "think"
  ]
  node [
    id 233
    label "pilnowa&#263;"
  ]
  node [
    id 234
    label "recall"
  ]
  node [
    id 235
    label "echo"
  ]
  node [
    id 236
    label "Zgredek"
  ]
  node [
    id 237
    label "kategoria_gramatyczna"
  ]
  node [
    id 238
    label "Casanova"
  ]
  node [
    id 239
    label "Don_Juan"
  ]
  node [
    id 240
    label "Gargantua"
  ]
  node [
    id 241
    label "Faust"
  ]
  node [
    id 242
    label "Chocho&#322;"
  ]
  node [
    id 243
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 244
    label "koniugacja"
  ]
  node [
    id 245
    label "Winnetou"
  ]
  node [
    id 246
    label "Dwukwiat"
  ]
  node [
    id 247
    label "Edyp"
  ]
  node [
    id 248
    label "Herkules_Poirot"
  ]
  node [
    id 249
    label "person"
  ]
  node [
    id 250
    label "Sherlock_Holmes"
  ]
  node [
    id 251
    label "Szwejk"
  ]
  node [
    id 252
    label "Hamlet"
  ]
  node [
    id 253
    label "Quasimodo"
  ]
  node [
    id 254
    label "Dulcynea"
  ]
  node [
    id 255
    label "Don_Kiszot"
  ]
  node [
    id 256
    label "Wallenrod"
  ]
  node [
    id 257
    label "Plastu&#347;"
  ]
  node [
    id 258
    label "Harry_Potter"
  ]
  node [
    id 259
    label "parali&#380;owa&#263;"
  ]
  node [
    id 260
    label "istota"
  ]
  node [
    id 261
    label "Werter"
  ]
  node [
    id 262
    label "summer"
  ]
  node [
    id 263
    label "hold"
  ]
  node [
    id 264
    label "sp&#281;dza&#263;"
  ]
  node [
    id 265
    label "look"
  ]
  node [
    id 266
    label "decydowa&#263;"
  ]
  node [
    id 267
    label "oczekiwa&#263;"
  ]
  node [
    id 268
    label "pauzowa&#263;"
  ]
  node [
    id 269
    label "anticipate"
  ]
  node [
    id 270
    label "zrobienie"
  ]
  node [
    id 271
    label "control"
  ]
  node [
    id 272
    label "spowodowanie"
  ]
  node [
    id 273
    label "completion"
  ]
  node [
    id 274
    label "bash"
  ]
  node [
    id 275
    label "znalezienie_si&#281;"
  ]
  node [
    id 276
    label "activity"
  ]
  node [
    id 277
    label "ziszczenie_si&#281;"
  ]
  node [
    id 278
    label "nasilenie_si&#281;"
  ]
  node [
    id 279
    label "rubryka"
  ]
  node [
    id 280
    label "woof"
  ]
  node [
    id 281
    label "poczucie"
  ]
  node [
    id 282
    label "pe&#322;ny"
  ]
  node [
    id 283
    label "zdarzenie_si&#281;"
  ]
  node [
    id 284
    label "uzupe&#322;nienie"
  ]
  node [
    id 285
    label "performance"
  ]
  node [
    id 286
    label "umieszczenie"
  ]
  node [
    id 287
    label "konsekwencja"
  ]
  node [
    id 288
    label "punishment"
  ]
  node [
    id 289
    label "cecha"
  ]
  node [
    id 290
    label "roboty_przymusowe"
  ]
  node [
    id 291
    label "nemezis"
  ]
  node [
    id 292
    label "righteousness"
  ]
  node [
    id 293
    label "epokowy"
  ]
  node [
    id 294
    label "dziejowo"
  ]
  node [
    id 295
    label "ability"
  ]
  node [
    id 296
    label "wyb&#243;r"
  ]
  node [
    id 297
    label "prospect"
  ]
  node [
    id 298
    label "egzekutywa"
  ]
  node [
    id 299
    label "alternatywa"
  ]
  node [
    id 300
    label "potencja&#322;"
  ]
  node [
    id 301
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 302
    label "obliczeniowo"
  ]
  node [
    id 303
    label "wydarzenie"
  ]
  node [
    id 304
    label "operator_modalny"
  ]
  node [
    id 305
    label "posiada&#263;"
  ]
  node [
    id 306
    label "return"
  ]
  node [
    id 307
    label "odyseja"
  ]
  node [
    id 308
    label "para"
  ]
  node [
    id 309
    label "rektyfikacja"
  ]
  node [
    id 310
    label "matuszka"
  ]
  node [
    id 311
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 312
    label "patriota"
  ]
  node [
    id 313
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 314
    label "wiela"
  ]
  node [
    id 315
    label "kolejny"
  ]
  node [
    id 316
    label "inaczej"
  ]
  node [
    id 317
    label "r&#243;&#380;ny"
  ]
  node [
    id 318
    label "inszy"
  ]
  node [
    id 319
    label "osobno"
  ]
  node [
    id 320
    label "Samojedzi"
  ]
  node [
    id 321
    label "nacja"
  ]
  node [
    id 322
    label "Aztekowie"
  ]
  node [
    id 323
    label "Irokezi"
  ]
  node [
    id 324
    label "Buriaci"
  ]
  node [
    id 325
    label "Komancze"
  ]
  node [
    id 326
    label "t&#322;um"
  ]
  node [
    id 327
    label "ludno&#347;&#263;"
  ]
  node [
    id 328
    label "lud"
  ]
  node [
    id 329
    label "Siuksowie"
  ]
  node [
    id 330
    label "Czejenowie"
  ]
  node [
    id 331
    label "Wotiacy"
  ]
  node [
    id 332
    label "Baszkirzy"
  ]
  node [
    id 333
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 334
    label "Mohikanie"
  ]
  node [
    id 335
    label "Apacze"
  ]
  node [
    id 336
    label "Syngalezi"
  ]
  node [
    id 337
    label "etnogeneza"
  ]
  node [
    id 338
    label "nationality"
  ]
  node [
    id 339
    label "dawny"
  ]
  node [
    id 340
    label "ongi&#347;"
  ]
  node [
    id 341
    label "dawnie"
  ]
  node [
    id 342
    label "wcze&#347;niej"
  ]
  node [
    id 343
    label "d&#322;ugotrwale"
  ]
  node [
    id 344
    label "render"
  ]
  node [
    id 345
    label "zosta&#263;"
  ]
  node [
    id 346
    label "spowodowa&#263;"
  ]
  node [
    id 347
    label "przyj&#347;&#263;"
  ]
  node [
    id 348
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 349
    label "revive"
  ]
  node [
    id 350
    label "pojedna&#263;_si&#281;"
  ]
  node [
    id 351
    label "podj&#261;&#263;"
  ]
  node [
    id 352
    label "nawi&#261;za&#263;"
  ]
  node [
    id 353
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 354
    label "przyby&#263;"
  ]
  node [
    id 355
    label "recur"
  ]
  node [
    id 356
    label "miejsce"
  ]
  node [
    id 357
    label "faza"
  ]
  node [
    id 358
    label "upgrade"
  ]
  node [
    id 359
    label "pierworodztwo"
  ]
  node [
    id 360
    label "nast&#281;pstwo"
  ]
  node [
    id 361
    label "remark"
  ]
  node [
    id 362
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 363
    label "u&#380;ywa&#263;"
  ]
  node [
    id 364
    label "okre&#347;la&#263;"
  ]
  node [
    id 365
    label "j&#281;zyk"
  ]
  node [
    id 366
    label "say"
  ]
  node [
    id 367
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 368
    label "formu&#322;owa&#263;"
  ]
  node [
    id 369
    label "talk"
  ]
  node [
    id 370
    label "powiada&#263;"
  ]
  node [
    id 371
    label "informowa&#263;"
  ]
  node [
    id 372
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 373
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 374
    label "wydobywa&#263;"
  ]
  node [
    id 375
    label "express"
  ]
  node [
    id 376
    label "chew_the_fat"
  ]
  node [
    id 377
    label "dysfonia"
  ]
  node [
    id 378
    label "umie&#263;"
  ]
  node [
    id 379
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 380
    label "tell"
  ]
  node [
    id 381
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 382
    label "wyra&#380;a&#263;"
  ]
  node [
    id 383
    label "gaworzy&#263;"
  ]
  node [
    id 384
    label "rozmawia&#263;"
  ]
  node [
    id 385
    label "dziama&#263;"
  ]
  node [
    id 386
    label "prawi&#263;"
  ]
  node [
    id 387
    label "pozostawa&#263;"
  ]
  node [
    id 388
    label "wystarcza&#263;"
  ]
  node [
    id 389
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 390
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 391
    label "mieszka&#263;"
  ]
  node [
    id 392
    label "wystarczy&#263;"
  ]
  node [
    id 393
    label "sprawowa&#263;"
  ]
  node [
    id 394
    label "przebywa&#263;"
  ]
  node [
    id 395
    label "kosztowa&#263;"
  ]
  node [
    id 396
    label "undertaking"
  ]
  node [
    id 397
    label "wystawa&#263;"
  ]
  node [
    id 398
    label "base"
  ]
  node [
    id 399
    label "digest"
  ]
  node [
    id 400
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 401
    label "free"
  ]
  node [
    id 402
    label "jako&#347;"
  ]
  node [
    id 403
    label "charakterystyczny"
  ]
  node [
    id 404
    label "ciekawy"
  ]
  node [
    id 405
    label "jako_tako"
  ]
  node [
    id 406
    label "dziwny"
  ]
  node [
    id 407
    label "niez&#322;y"
  ]
  node [
    id 408
    label "przyzwoity"
  ]
  node [
    id 409
    label "czasokres"
  ]
  node [
    id 410
    label "trawienie"
  ]
  node [
    id 411
    label "period"
  ]
  node [
    id 412
    label "odczyt"
  ]
  node [
    id 413
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 414
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 415
    label "chwila"
  ]
  node [
    id 416
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 417
    label "poprzedzenie"
  ]
  node [
    id 418
    label "dzieje"
  ]
  node [
    id 419
    label "poprzedzi&#263;"
  ]
  node [
    id 420
    label "przep&#322;ywanie"
  ]
  node [
    id 421
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 422
    label "odwlekanie_si&#281;"
  ]
  node [
    id 423
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 424
    label "Zeitgeist"
  ]
  node [
    id 425
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 426
    label "okres_czasu"
  ]
  node [
    id 427
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 428
    label "pochodzi&#263;"
  ]
  node [
    id 429
    label "schy&#322;ek"
  ]
  node [
    id 430
    label "czwarty_wymiar"
  ]
  node [
    id 431
    label "chronometria"
  ]
  node [
    id 432
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 433
    label "poprzedzanie"
  ]
  node [
    id 434
    label "pogoda"
  ]
  node [
    id 435
    label "zegar"
  ]
  node [
    id 436
    label "pochodzenie"
  ]
  node [
    id 437
    label "poprzedza&#263;"
  ]
  node [
    id 438
    label "trawi&#263;"
  ]
  node [
    id 439
    label "time_period"
  ]
  node [
    id 440
    label "rachuba_czasu"
  ]
  node [
    id 441
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 442
    label "czasoprzestrze&#324;"
  ]
  node [
    id 443
    label "laba"
  ]
  node [
    id 444
    label "wykorzystywa&#263;"
  ]
  node [
    id 445
    label "przeprowadza&#263;"
  ]
  node [
    id 446
    label "spieni&#281;&#380;a&#263;"
  ]
  node [
    id 447
    label "prosecute"
  ]
  node [
    id 448
    label "create"
  ]
  node [
    id 449
    label "prawdzi&#263;"
  ]
  node [
    id 450
    label "tworzy&#263;"
  ]
  node [
    id 451
    label "metier"
  ]
  node [
    id 452
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 453
    label "autonomy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 25
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 39
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 52
  ]
  edge [
    source 23
    target 53
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 32
  ]
  edge [
    source 23
    target 48
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 228
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 194
  ]
  edge [
    source 27
    target 229
  ]
  edge [
    source 27
    target 178
  ]
  edge [
    source 27
    target 230
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 232
  ]
  edge [
    source 27
    target 233
  ]
  edge [
    source 27
    target 181
  ]
  edge [
    source 27
    target 234
  ]
  edge [
    source 27
    target 235
  ]
  edge [
    source 27
    target 177
  ]
  edge [
    source 28
    target 236
  ]
  edge [
    source 28
    target 237
  ]
  edge [
    source 28
    target 238
  ]
  edge [
    source 28
    target 239
  ]
  edge [
    source 28
    target 240
  ]
  edge [
    source 28
    target 241
  ]
  edge [
    source 28
    target 210
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 212
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 215
  ]
  edge [
    source 28
    target 217
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 218
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 219
  ]
  edge [
    source 28
    target 220
  ]
  edge [
    source 28
    target 221
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 224
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 227
  ]
  edge [
    source 28
    target 228
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 42
  ]
  edge [
    source 29
    target 43
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 45
  ]
  edge [
    source 31
    target 46
  ]
  edge [
    source 31
    target 262
  ]
  edge [
    source 31
    target 52
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 263
  ]
  edge [
    source 32
    target 264
  ]
  edge [
    source 32
    target 265
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 32
    target 48
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 270
  ]
  edge [
    source 33
    target 271
  ]
  edge [
    source 33
    target 272
  ]
  edge [
    source 33
    target 273
  ]
  edge [
    source 33
    target 274
  ]
  edge [
    source 33
    target 275
  ]
  edge [
    source 33
    target 276
  ]
  edge [
    source 33
    target 277
  ]
  edge [
    source 33
    target 278
  ]
  edge [
    source 33
    target 279
  ]
  edge [
    source 33
    target 280
  ]
  edge [
    source 33
    target 281
  ]
  edge [
    source 33
    target 136
  ]
  edge [
    source 33
    target 282
  ]
  edge [
    source 33
    target 283
  ]
  edge [
    source 33
    target 284
  ]
  edge [
    source 33
    target 285
  ]
  edge [
    source 33
    target 286
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 287
  ]
  edge [
    source 34
    target 288
  ]
  edge [
    source 34
    target 289
  ]
  edge [
    source 34
    target 290
  ]
  edge [
    source 34
    target 291
  ]
  edge [
    source 34
    target 292
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 293
  ]
  edge [
    source 35
    target 294
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 295
  ]
  edge [
    source 36
    target 296
  ]
  edge [
    source 36
    target 297
  ]
  edge [
    source 36
    target 298
  ]
  edge [
    source 36
    target 299
  ]
  edge [
    source 36
    target 300
  ]
  edge [
    source 36
    target 289
  ]
  edge [
    source 36
    target 301
  ]
  edge [
    source 36
    target 302
  ]
  edge [
    source 36
    target 303
  ]
  edge [
    source 36
    target 304
  ]
  edge [
    source 36
    target 305
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 306
  ]
  edge [
    source 37
    target 307
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 303
  ]
  edge [
    source 37
    target 309
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 310
  ]
  edge [
    source 38
    target 311
  ]
  edge [
    source 38
    target 312
  ]
  edge [
    source 38
    target 313
  ]
  edge [
    source 38
    target 90
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 314
  ]
  edge [
    source 39
    target 98
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 315
  ]
  edge [
    source 40
    target 316
  ]
  edge [
    source 40
    target 317
  ]
  edge [
    source 40
    target 318
  ]
  edge [
    source 40
    target 319
  ]
  edge [
    source 41
    target 320
  ]
  edge [
    source 41
    target 321
  ]
  edge [
    source 41
    target 322
  ]
  edge [
    source 41
    target 323
  ]
  edge [
    source 41
    target 324
  ]
  edge [
    source 41
    target 325
  ]
  edge [
    source 41
    target 326
  ]
  edge [
    source 41
    target 327
  ]
  edge [
    source 41
    target 328
  ]
  edge [
    source 41
    target 329
  ]
  edge [
    source 41
    target 330
  ]
  edge [
    source 41
    target 331
  ]
  edge [
    source 41
    target 332
  ]
  edge [
    source 41
    target 333
  ]
  edge [
    source 41
    target 334
  ]
  edge [
    source 41
    target 335
  ]
  edge [
    source 41
    target 336
  ]
  edge [
    source 42
    target 321
  ]
  edge [
    source 42
    target 337
  ]
  edge [
    source 42
    target 338
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 339
  ]
  edge [
    source 43
    target 340
  ]
  edge [
    source 43
    target 341
  ]
  edge [
    source 43
    target 342
  ]
  edge [
    source 43
    target 343
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 344
  ]
  edge [
    source 44
    target 306
  ]
  edge [
    source 44
    target 345
  ]
  edge [
    source 44
    target 346
  ]
  edge [
    source 44
    target 347
  ]
  edge [
    source 44
    target 348
  ]
  edge [
    source 44
    target 349
  ]
  edge [
    source 44
    target 350
  ]
  edge [
    source 44
    target 351
  ]
  edge [
    source 44
    target 352
  ]
  edge [
    source 44
    target 353
  ]
  edge [
    source 44
    target 354
  ]
  edge [
    source 44
    target 355
  ]
  edge [
    source 45
    target 356
  ]
  edge [
    source 45
    target 357
  ]
  edge [
    source 45
    target 358
  ]
  edge [
    source 45
    target 359
  ]
  edge [
    source 45
    target 360
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 361
  ]
  edge [
    source 46
    target 362
  ]
  edge [
    source 46
    target 363
  ]
  edge [
    source 46
    target 364
  ]
  edge [
    source 46
    target 365
  ]
  edge [
    source 46
    target 366
  ]
  edge [
    source 46
    target 367
  ]
  edge [
    source 46
    target 368
  ]
  edge [
    source 46
    target 369
  ]
  edge [
    source 46
    target 370
  ]
  edge [
    source 46
    target 371
  ]
  edge [
    source 46
    target 372
  ]
  edge [
    source 46
    target 373
  ]
  edge [
    source 46
    target 374
  ]
  edge [
    source 46
    target 375
  ]
  edge [
    source 46
    target 376
  ]
  edge [
    source 46
    target 377
  ]
  edge [
    source 46
    target 378
  ]
  edge [
    source 46
    target 379
  ]
  edge [
    source 46
    target 380
  ]
  edge [
    source 46
    target 381
  ]
  edge [
    source 46
    target 382
  ]
  edge [
    source 46
    target 383
  ]
  edge [
    source 46
    target 384
  ]
  edge [
    source 46
    target 385
  ]
  edge [
    source 46
    target 386
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 387
  ]
  edge [
    source 48
    target 195
  ]
  edge [
    source 48
    target 388
  ]
  edge [
    source 48
    target 389
  ]
  edge [
    source 48
    target 199
  ]
  edge [
    source 48
    target 390
  ]
  edge [
    source 48
    target 391
  ]
  edge [
    source 48
    target 392
  ]
  edge [
    source 48
    target 393
  ]
  edge [
    source 48
    target 394
  ]
  edge [
    source 48
    target 395
  ]
  edge [
    source 48
    target 396
  ]
  edge [
    source 48
    target 397
  ]
  edge [
    source 48
    target 398
  ]
  edge [
    source 48
    target 399
  ]
  edge [
    source 48
    target 400
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 401
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 402
  ]
  edge [
    source 51
    target 403
  ]
  edge [
    source 51
    target 404
  ]
  edge [
    source 51
    target 405
  ]
  edge [
    source 51
    target 406
  ]
  edge [
    source 51
    target 407
  ]
  edge [
    source 51
    target 408
  ]
  edge [
    source 52
    target 409
  ]
  edge [
    source 52
    target 410
  ]
  edge [
    source 52
    target 237
  ]
  edge [
    source 52
    target 411
  ]
  edge [
    source 52
    target 412
  ]
  edge [
    source 52
    target 413
  ]
  edge [
    source 52
    target 414
  ]
  edge [
    source 52
    target 415
  ]
  edge [
    source 52
    target 416
  ]
  edge [
    source 52
    target 417
  ]
  edge [
    source 52
    target 244
  ]
  edge [
    source 52
    target 418
  ]
  edge [
    source 52
    target 419
  ]
  edge [
    source 52
    target 420
  ]
  edge [
    source 52
    target 421
  ]
  edge [
    source 52
    target 422
  ]
  edge [
    source 52
    target 423
  ]
  edge [
    source 52
    target 424
  ]
  edge [
    source 52
    target 425
  ]
  edge [
    source 52
    target 426
  ]
  edge [
    source 52
    target 427
  ]
  edge [
    source 52
    target 428
  ]
  edge [
    source 52
    target 429
  ]
  edge [
    source 52
    target 430
  ]
  edge [
    source 52
    target 431
  ]
  edge [
    source 52
    target 432
  ]
  edge [
    source 52
    target 433
  ]
  edge [
    source 52
    target 434
  ]
  edge [
    source 52
    target 435
  ]
  edge [
    source 52
    target 436
  ]
  edge [
    source 52
    target 437
  ]
  edge [
    source 52
    target 438
  ]
  edge [
    source 52
    target 439
  ]
  edge [
    source 52
    target 440
  ]
  edge [
    source 52
    target 441
  ]
  edge [
    source 52
    target 442
  ]
  edge [
    source 52
    target 443
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 444
  ]
  edge [
    source 53
    target 445
  ]
  edge [
    source 53
    target 446
  ]
  edge [
    source 53
    target 447
  ]
  edge [
    source 53
    target 448
  ]
  edge [
    source 53
    target 449
  ]
  edge [
    source 53
    target 450
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 451
  ]
  edge [
    source 55
    target 452
  ]
  edge [
    source 56
    target 453
  ]
  edge [
    source 56
    target 115
  ]
]
