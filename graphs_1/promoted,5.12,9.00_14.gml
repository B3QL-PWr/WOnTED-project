graph [
  maxDegree 26
  minDegree 1
  meanDegree 1.9701492537313432
  density 0.029850746268656716
  graphCliqueNumber 2
  node [
    id 0
    label "zanim"
    origin "text"
  ]
  node [
    id 1
    label "iwan"
    origin "text"
  ]
  node [
    id 2
    label "gro&#378;ny"
    origin "text"
  ]
  node [
    id 3
    label "urodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "wiele"
    origin "text"
  ]
  node [
    id 6
    label "przepowiednia"
    origin "text"
  ]
  node [
    id 7
    label "zwiastowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "przyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "niezwyk&#322;y"
    origin "text"
  ]
  node [
    id 10
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 11
    label "nad&#261;sany"
  ]
  node [
    id 12
    label "niebezpieczny"
  ]
  node [
    id 13
    label "gro&#378;nie"
  ]
  node [
    id 14
    label "narodzi&#263;"
  ]
  node [
    id 15
    label "zlec"
  ]
  node [
    id 16
    label "engender"
  ]
  node [
    id 17
    label "powi&#263;"
  ]
  node [
    id 18
    label "zrobi&#263;"
  ]
  node [
    id 19
    label "porodzi&#263;"
  ]
  node [
    id 20
    label "wiela"
  ]
  node [
    id 21
    label "du&#380;y"
  ]
  node [
    id 22
    label "prediction"
  ]
  node [
    id 23
    label "wr&#243;&#380;biarstwo"
  ]
  node [
    id 24
    label "intuicja"
  ]
  node [
    id 25
    label "przewidywanie"
  ]
  node [
    id 26
    label "przewidywa&#263;"
  ]
  node [
    id 27
    label "oznajmia&#263;"
  ]
  node [
    id 28
    label "przewidzie&#263;"
  ]
  node [
    id 29
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 30
    label "harbinger"
  ]
  node [
    id 31
    label "oznajmi&#263;"
  ]
  node [
    id 32
    label "sta&#263;_si&#281;"
  ]
  node [
    id 33
    label "zaistnie&#263;"
  ]
  node [
    id 34
    label "czas"
  ]
  node [
    id 35
    label "doj&#347;&#263;"
  ]
  node [
    id 36
    label "become"
  ]
  node [
    id 37
    label "line_up"
  ]
  node [
    id 38
    label "przyby&#263;"
  ]
  node [
    id 39
    label "inny"
  ]
  node [
    id 40
    label "niezwykle"
  ]
  node [
    id 41
    label "asymilowa&#263;"
  ]
  node [
    id 42
    label "wapniak"
  ]
  node [
    id 43
    label "dwun&#243;g"
  ]
  node [
    id 44
    label "polifag"
  ]
  node [
    id 45
    label "wz&#243;r"
  ]
  node [
    id 46
    label "profanum"
  ]
  node [
    id 47
    label "hominid"
  ]
  node [
    id 48
    label "homo_sapiens"
  ]
  node [
    id 49
    label "nasada"
  ]
  node [
    id 50
    label "podw&#322;adny"
  ]
  node [
    id 51
    label "ludzko&#347;&#263;"
  ]
  node [
    id 52
    label "os&#322;abianie"
  ]
  node [
    id 53
    label "mikrokosmos"
  ]
  node [
    id 54
    label "portrecista"
  ]
  node [
    id 55
    label "duch"
  ]
  node [
    id 56
    label "oddzia&#322;ywanie"
  ]
  node [
    id 57
    label "g&#322;owa"
  ]
  node [
    id 58
    label "asymilowanie"
  ]
  node [
    id 59
    label "osoba"
  ]
  node [
    id 60
    label "os&#322;abia&#263;"
  ]
  node [
    id 61
    label "figura"
  ]
  node [
    id 62
    label "Adam"
  ]
  node [
    id 63
    label "senior"
  ]
  node [
    id 64
    label "antropochoria"
  ]
  node [
    id 65
    label "posta&#263;"
  ]
  node [
    id 66
    label "Iwan"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
]
