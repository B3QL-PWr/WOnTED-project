graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9
  density 0.1
  graphCliqueNumber 2
  node [
    id 0
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 1
    label "zem&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "ojciec"
    origin "text"
  ]
  node [
    id 4
    label "pomys&#322;odawca"
  ]
  node [
    id 5
    label "kszta&#322;ciciel"
  ]
  node [
    id 6
    label "tworzyciel"
  ]
  node [
    id 7
    label "ojczym"
  ]
  node [
    id 8
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 9
    label "stary"
  ]
  node [
    id 10
    label "samiec"
  ]
  node [
    id 11
    label "papa"
  ]
  node [
    id 12
    label "&#347;w"
  ]
  node [
    id 13
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 14
    label "zakonnik"
  ]
  node [
    id 15
    label "kuwada"
  ]
  node [
    id 16
    label "przodek"
  ]
  node [
    id 17
    label "wykonawca"
  ]
  node [
    id 18
    label "rodzice"
  ]
  node [
    id 19
    label "rodzic"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
]
