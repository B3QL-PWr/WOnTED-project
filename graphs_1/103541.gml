graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.123989218328841
  density 0.005740511400888759
  graphCliqueNumber 3
  node [
    id 0
    label "karina"
    origin "text"
  ]
  node [
    id 1
    label "naiwny"
    origin "text"
  ]
  node [
    id 2
    label "kochanek"
    origin "text"
  ]
  node [
    id 3
    label "prosty"
    origin "text"
  ]
  node [
    id 4
    label "wierzy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zostawi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "&#380;ona"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "wys&#322;uchiwa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "opowie&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "nie"
    origin "text"
  ]
  node [
    id 11
    label "dawny"
    origin "text"
  ]
  node [
    id 12
    label "siebie"
    origin "text"
  ]
  node [
    id 13
    label "j&#281;dza"
    origin "text"
  ]
  node [
    id 14
    label "zimno"
    origin "text"
  ]
  node [
    id 15
    label "sopel"
    origin "text"
  ]
  node [
    id 16
    label "l&#243;d"
    origin "text"
  ]
  node [
    id 17
    label "tym"
    origin "text"
  ]
  node [
    id 18
    label "bajeczka"
    origin "text"
  ]
  node [
    id 19
    label "ci&#261;gle"
    origin "text"
  ]
  node [
    id 20
    label "powtarza&#263;"
    origin "text"
  ]
  node [
    id 21
    label "koniec"
    origin "text"
  ]
  node [
    id 22
    label "bra&#263;"
    origin "text"
  ]
  node [
    id 23
    label "rozw&#243;d"
    origin "text"
  ]
  node [
    id 24
    label "ale"
    origin "text"
  ]
  node [
    id 25
    label "mija&#263;"
    origin "text"
  ]
  node [
    id 26
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 27
    label "nawet"
    origin "text"
  ]
  node [
    id 28
    label "lata"
    origin "text"
  ]
  node [
    id 29
    label "coraz"
    origin "text"
  ]
  node [
    id 30
    label "nowa"
    origin "text"
  ]
  node [
    id 31
    label "przeszkoda"
    origin "text"
  ]
  node [
    id 32
    label "staja"
    origin "text"
  ]
  node [
    id 33
    label "droga"
    origin "text"
  ]
  node [
    id 34
    label "zasadniczo"
    origin "text"
  ]
  node [
    id 35
    label "rzuca&#263;"
    origin "text"
  ]
  node [
    id 36
    label "albo"
    origin "text"
  ]
  node [
    id 37
    label "si&#281;"
    origin "text"
  ]
  node [
    id 38
    label "znudzi&#263;"
    origin "text"
  ]
  node [
    id 39
    label "bardzo"
    origin "text"
  ]
  node [
    id 40
    label "naciska&#263;"
    origin "text"
  ]
  node [
    id 41
    label "wreszcie"
    origin "text"
  ]
  node [
    id 42
    label "ostro"
    origin "text"
  ]
  node [
    id 43
    label "wkroczy&#263;"
    origin "text"
  ]
  node [
    id 44
    label "akcja"
    origin "text"
  ]
  node [
    id 45
    label "naiwnie"
  ]
  node [
    id 46
    label "g&#322;upi"
  ]
  node [
    id 47
    label "prostoduszny"
  ]
  node [
    id 48
    label "poczciwy"
  ]
  node [
    id 49
    label "bratek"
  ]
  node [
    id 50
    label "kocha&#347;"
  ]
  node [
    id 51
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 52
    label "zwrot"
  ]
  node [
    id 53
    label "przyjaciel"
  ]
  node [
    id 54
    label "fagas"
  ]
  node [
    id 55
    label "partner"
  ]
  node [
    id 56
    label "mi&#322;y"
  ]
  node [
    id 57
    label "&#322;atwy"
  ]
  node [
    id 58
    label "prostowanie_si&#281;"
  ]
  node [
    id 59
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 60
    label "rozprostowanie"
  ]
  node [
    id 61
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 62
    label "naturalny"
  ]
  node [
    id 63
    label "cios"
  ]
  node [
    id 64
    label "prostowanie"
  ]
  node [
    id 65
    label "niepozorny"
  ]
  node [
    id 66
    label "zwyk&#322;y"
  ]
  node [
    id 67
    label "prosto"
  ]
  node [
    id 68
    label "po_prostu"
  ]
  node [
    id 69
    label "skromny"
  ]
  node [
    id 70
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 71
    label "czu&#263;"
  ]
  node [
    id 72
    label "chowa&#263;"
  ]
  node [
    id 73
    label "wierza&#263;"
  ]
  node [
    id 74
    label "powierzy&#263;"
  ]
  node [
    id 75
    label "powierza&#263;"
  ]
  node [
    id 76
    label "faith"
  ]
  node [
    id 77
    label "uznawa&#263;"
  ]
  node [
    id 78
    label "trust"
  ]
  node [
    id 79
    label "wyznawa&#263;"
  ]
  node [
    id 80
    label "nadzieja"
  ]
  node [
    id 81
    label "skrzywdzi&#263;"
  ]
  node [
    id 82
    label "impart"
  ]
  node [
    id 83
    label "liszy&#263;"
  ]
  node [
    id 84
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 85
    label "doprowadzi&#263;"
  ]
  node [
    id 86
    label "da&#263;"
  ]
  node [
    id 87
    label "zachowa&#263;"
  ]
  node [
    id 88
    label "stworzy&#263;"
  ]
  node [
    id 89
    label "overhaul"
  ]
  node [
    id 90
    label "permit"
  ]
  node [
    id 91
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 92
    label "przekaza&#263;"
  ]
  node [
    id 93
    label "wyda&#263;"
  ]
  node [
    id 94
    label "wyznaczy&#263;"
  ]
  node [
    id 95
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 96
    label "zrobi&#263;"
  ]
  node [
    id 97
    label "zerwa&#263;"
  ]
  node [
    id 98
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 99
    label "zaplanowa&#263;"
  ]
  node [
    id 100
    label "zabra&#263;"
  ]
  node [
    id 101
    label "shove"
  ]
  node [
    id 102
    label "spowodowa&#263;"
  ]
  node [
    id 103
    label "zrezygnowa&#263;"
  ]
  node [
    id 104
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 105
    label "drop"
  ]
  node [
    id 106
    label "release"
  ]
  node [
    id 107
    label "shelve"
  ]
  node [
    id 108
    label "ma&#322;&#380;onek"
  ]
  node [
    id 109
    label "panna_m&#322;oda"
  ]
  node [
    id 110
    label "partnerka"
  ]
  node [
    id 111
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 112
    label "&#347;lubna"
  ]
  node [
    id 113
    label "kobita"
  ]
  node [
    id 114
    label "si&#281;ga&#263;"
  ]
  node [
    id 115
    label "trwa&#263;"
  ]
  node [
    id 116
    label "obecno&#347;&#263;"
  ]
  node [
    id 117
    label "stan"
  ]
  node [
    id 118
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 119
    label "stand"
  ]
  node [
    id 120
    label "mie&#263;_miejsce"
  ]
  node [
    id 121
    label "uczestniczy&#263;"
  ]
  node [
    id 122
    label "chodzi&#263;"
  ]
  node [
    id 123
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 124
    label "equal"
  ]
  node [
    id 125
    label "s&#322;ucha&#263;"
  ]
  node [
    id 126
    label "report"
  ]
  node [
    id 127
    label "fabu&#322;a"
  ]
  node [
    id 128
    label "opowiadanie"
  ]
  node [
    id 129
    label "wypowied&#378;"
  ]
  node [
    id 130
    label "sprzeciw"
  ]
  node [
    id 131
    label "przesz&#322;y"
  ]
  node [
    id 132
    label "dawno"
  ]
  node [
    id 133
    label "dawniej"
  ]
  node [
    id 134
    label "kombatant"
  ]
  node [
    id 135
    label "stary"
  ]
  node [
    id 136
    label "odleg&#322;y"
  ]
  node [
    id 137
    label "anachroniczny"
  ]
  node [
    id 138
    label "przestarza&#322;y"
  ]
  node [
    id 139
    label "od_dawna"
  ]
  node [
    id 140
    label "poprzedni"
  ]
  node [
    id 141
    label "d&#322;ugoletni"
  ]
  node [
    id 142
    label "wcze&#347;niejszy"
  ]
  node [
    id 143
    label "niegdysiejszy"
  ]
  node [
    id 144
    label "cz&#322;owiek"
  ]
  node [
    id 145
    label "istota_&#380;ywa"
  ]
  node [
    id 146
    label "kobieta"
  ]
  node [
    id 147
    label "czarownica"
  ]
  node [
    id 148
    label "wstr&#281;ciucha"
  ]
  node [
    id 149
    label "franca"
  ]
  node [
    id 150
    label "zimny"
  ]
  node [
    id 151
    label "coldness"
  ]
  node [
    id 152
    label "opryszczkowe_zapalenie_opon_m&#243;zgowych_i_m&#243;zgu"
  ]
  node [
    id 153
    label "spokojnie"
  ]
  node [
    id 154
    label "temperatura"
  ]
  node [
    id 155
    label "p&#281;cherz"
  ]
  node [
    id 156
    label "wirus_opryszczki_pospolitej"
  ]
  node [
    id 157
    label "ch&#322;odny"
  ]
  node [
    id 158
    label "kriofil"
  ]
  node [
    id 159
    label "choroba_wirusowa"
  ]
  node [
    id 160
    label "nieczule"
  ]
  node [
    id 161
    label "bry&#322;a"
  ]
  node [
    id 162
    label "lody"
  ]
  node [
    id 163
    label "g&#322;ad&#378;"
  ]
  node [
    id 164
    label "zlodowacenie"
  ]
  node [
    id 165
    label "woda"
  ]
  node [
    id 166
    label "cia&#322;o_sta&#322;e"
  ]
  node [
    id 167
    label "kostkarka"
  ]
  node [
    id 168
    label "lodowacenie"
  ]
  node [
    id 169
    label "narrative"
  ]
  node [
    id 170
    label "g&#322;upstwo"
  ]
  node [
    id 171
    label "nieprzerwanie"
  ]
  node [
    id 172
    label "ci&#261;g&#322;y"
  ]
  node [
    id 173
    label "stale"
  ]
  node [
    id 174
    label "repeat"
  ]
  node [
    id 175
    label "robi&#263;"
  ]
  node [
    id 176
    label "podszkala&#263;_si&#281;"
  ]
  node [
    id 177
    label "podawa&#263;"
  ]
  node [
    id 178
    label "defenestracja"
  ]
  node [
    id 179
    label "szereg"
  ]
  node [
    id 180
    label "dzia&#322;anie"
  ]
  node [
    id 181
    label "miejsce"
  ]
  node [
    id 182
    label "ostatnie_podrygi"
  ]
  node [
    id 183
    label "kres"
  ]
  node [
    id 184
    label "agonia"
  ]
  node [
    id 185
    label "visitation"
  ]
  node [
    id 186
    label "szeol"
  ]
  node [
    id 187
    label "mogi&#322;a"
  ]
  node [
    id 188
    label "chwila"
  ]
  node [
    id 189
    label "wydarzenie"
  ]
  node [
    id 190
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 191
    label "pogrzebanie"
  ]
  node [
    id 192
    label "punkt"
  ]
  node [
    id 193
    label "&#380;a&#322;oba"
  ]
  node [
    id 194
    label "zabicie"
  ]
  node [
    id 195
    label "kres_&#380;ycia"
  ]
  node [
    id 196
    label "get"
  ]
  node [
    id 197
    label "przewa&#380;a&#263;"
  ]
  node [
    id 198
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 199
    label "poczytywa&#263;"
  ]
  node [
    id 200
    label "levy"
  ]
  node [
    id 201
    label "pokonywa&#263;"
  ]
  node [
    id 202
    label "u&#380;ywa&#263;"
  ]
  node [
    id 203
    label "rusza&#263;"
  ]
  node [
    id 204
    label "zalicza&#263;"
  ]
  node [
    id 205
    label "wygrywa&#263;"
  ]
  node [
    id 206
    label "open"
  ]
  node [
    id 207
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 208
    label "branie"
  ]
  node [
    id 209
    label "korzysta&#263;"
  ]
  node [
    id 210
    label "&#263;pa&#263;"
  ]
  node [
    id 211
    label "wch&#322;ania&#263;"
  ]
  node [
    id 212
    label "interpretowa&#263;"
  ]
  node [
    id 213
    label "atakowa&#263;"
  ]
  node [
    id 214
    label "prowadzi&#263;"
  ]
  node [
    id 215
    label "rucha&#263;"
  ]
  node [
    id 216
    label "take"
  ]
  node [
    id 217
    label "dostawa&#263;"
  ]
  node [
    id 218
    label "wzi&#261;&#263;"
  ]
  node [
    id 219
    label "wk&#322;ada&#263;"
  ]
  node [
    id 220
    label "chwyta&#263;"
  ]
  node [
    id 221
    label "arise"
  ]
  node [
    id 222
    label "za&#380;ywa&#263;"
  ]
  node [
    id 223
    label "uprawia&#263;_seks"
  ]
  node [
    id 224
    label "porywa&#263;"
  ]
  node [
    id 225
    label "grza&#263;"
  ]
  node [
    id 226
    label "abstract"
  ]
  node [
    id 227
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 228
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 229
    label "towarzystwo"
  ]
  node [
    id 230
    label "otrzymywa&#263;"
  ]
  node [
    id 231
    label "przyjmowa&#263;"
  ]
  node [
    id 232
    label "wchodzi&#263;"
  ]
  node [
    id 233
    label "ucieka&#263;"
  ]
  node [
    id 234
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 235
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 236
    label "&#322;apa&#263;"
  ]
  node [
    id 237
    label "raise"
  ]
  node [
    id 238
    label "rozbita_rodzina"
  ]
  node [
    id 239
    label "ekspartner"
  ]
  node [
    id 240
    label "uniewa&#380;nienie"
  ]
  node [
    id 241
    label "separation"
  ]
  node [
    id 242
    label "rozstanie"
  ]
  node [
    id 243
    label "piwo"
  ]
  node [
    id 244
    label "omija&#263;"
  ]
  node [
    id 245
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 246
    label "proceed"
  ]
  node [
    id 247
    label "przestawa&#263;"
  ]
  node [
    id 248
    label "przechodzi&#263;"
  ]
  node [
    id 249
    label "base_on_balls"
  ]
  node [
    id 250
    label "go"
  ]
  node [
    id 251
    label "czas"
  ]
  node [
    id 252
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 253
    label "rok"
  ]
  node [
    id 254
    label "miech"
  ]
  node [
    id 255
    label "kalendy"
  ]
  node [
    id 256
    label "tydzie&#324;"
  ]
  node [
    id 257
    label "summer"
  ]
  node [
    id 258
    label "gwiazda"
  ]
  node [
    id 259
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 260
    label "trudno&#347;&#263;"
  ]
  node [
    id 261
    label "je&#378;dziectwo"
  ]
  node [
    id 262
    label "bieg_prze&#322;ajowy"
  ]
  node [
    id 263
    label "dzielenie"
  ]
  node [
    id 264
    label "obstruction"
  ]
  node [
    id 265
    label "podzielenie"
  ]
  node [
    id 266
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 267
    label "journey"
  ]
  node [
    id 268
    label "podbieg"
  ]
  node [
    id 269
    label "bezsilnikowy"
  ]
  node [
    id 270
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 271
    label "wylot"
  ]
  node [
    id 272
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 273
    label "drogowskaz"
  ]
  node [
    id 274
    label "nawierzchnia"
  ]
  node [
    id 275
    label "turystyka"
  ]
  node [
    id 276
    label "budowla"
  ]
  node [
    id 277
    label "spos&#243;b"
  ]
  node [
    id 278
    label "passage"
  ]
  node [
    id 279
    label "marszrutyzacja"
  ]
  node [
    id 280
    label "zbior&#243;wka"
  ]
  node [
    id 281
    label "rajza"
  ]
  node [
    id 282
    label "ekskursja"
  ]
  node [
    id 283
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 284
    label "ruch"
  ]
  node [
    id 285
    label "trasa"
  ]
  node [
    id 286
    label "wyb&#243;j"
  ]
  node [
    id 287
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 288
    label "ekwipunek"
  ]
  node [
    id 289
    label "korona_drogi"
  ]
  node [
    id 290
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 291
    label "pobocze"
  ]
  node [
    id 292
    label "g&#322;&#243;wnie"
  ]
  node [
    id 293
    label "pryncypalnie"
  ]
  node [
    id 294
    label "og&#243;lnie"
  ]
  node [
    id 295
    label "surowo"
  ]
  node [
    id 296
    label "zasadniczy"
  ]
  node [
    id 297
    label "wyzwanie"
  ]
  node [
    id 298
    label "zmienia&#263;"
  ]
  node [
    id 299
    label "spring"
  ]
  node [
    id 300
    label "cie&#324;"
  ]
  node [
    id 301
    label "syga&#263;"
  ]
  node [
    id 302
    label "podejrzenie"
  ]
  node [
    id 303
    label "flip"
  ]
  node [
    id 304
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 305
    label "czar"
  ]
  node [
    id 306
    label "m&#243;wi&#263;"
  ]
  node [
    id 307
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 308
    label "rush"
  ]
  node [
    id 309
    label "konstruowa&#263;"
  ]
  node [
    id 310
    label "porusza&#263;"
  ]
  node [
    id 311
    label "bequeath"
  ]
  node [
    id 312
    label "&#347;wiat&#322;o"
  ]
  node [
    id 313
    label "przemieszcza&#263;"
  ]
  node [
    id 314
    label "przewraca&#263;"
  ]
  node [
    id 315
    label "most"
  ]
  node [
    id 316
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 317
    label "odchodzi&#263;"
  ]
  node [
    id 318
    label "opuszcza&#263;"
  ]
  node [
    id 319
    label "grzmoci&#263;"
  ]
  node [
    id 320
    label "unwrap"
  ]
  node [
    id 321
    label "tug"
  ]
  node [
    id 322
    label "towar"
  ]
  node [
    id 323
    label "wzbudzi&#263;"
  ]
  node [
    id 324
    label "tidal_bore"
  ]
  node [
    id 325
    label "w_chuj"
  ]
  node [
    id 326
    label "crowd"
  ]
  node [
    id 327
    label "napierdziela&#263;"
  ]
  node [
    id 328
    label "force"
  ]
  node [
    id 329
    label "przekonywa&#263;"
  ]
  node [
    id 330
    label "w&#380;dy"
  ]
  node [
    id 331
    label "niemile"
  ]
  node [
    id 332
    label "zdecydowanie"
  ]
  node [
    id 333
    label "jednoznacznie"
  ]
  node [
    id 334
    label "raptownie"
  ]
  node [
    id 335
    label "widocznie"
  ]
  node [
    id 336
    label "wyra&#378;nie"
  ]
  node [
    id 337
    label "gryz&#261;co"
  ]
  node [
    id 338
    label "energicznie"
  ]
  node [
    id 339
    label "szybko"
  ]
  node [
    id 340
    label "podniecaj&#261;co"
  ]
  node [
    id 341
    label "intensywnie"
  ]
  node [
    id 342
    label "dziko"
  ]
  node [
    id 343
    label "ostry"
  ]
  node [
    id 344
    label "ci&#281;&#380;ko"
  ]
  node [
    id 345
    label "nieneutralnie"
  ]
  node [
    id 346
    label "wej&#347;&#263;"
  ]
  node [
    id 347
    label "zaj&#261;&#263;"
  ]
  node [
    id 348
    label "zacz&#261;&#263;"
  ]
  node [
    id 349
    label "doj&#347;&#263;"
  ]
  node [
    id 350
    label "move"
  ]
  node [
    id 351
    label "induct"
  ]
  node [
    id 352
    label "motivate"
  ]
  node [
    id 353
    label "poruszy&#263;"
  ]
  node [
    id 354
    label "zagrywka"
  ]
  node [
    id 355
    label "czyn"
  ]
  node [
    id 356
    label "czynno&#347;&#263;"
  ]
  node [
    id 357
    label "wysoko&#347;&#263;"
  ]
  node [
    id 358
    label "stock"
  ]
  node [
    id 359
    label "gra"
  ]
  node [
    id 360
    label "w&#281;ze&#322;"
  ]
  node [
    id 361
    label "instrument_strunowy"
  ]
  node [
    id 362
    label "dywidenda"
  ]
  node [
    id 363
    label "przebieg"
  ]
  node [
    id 364
    label "udzia&#322;"
  ]
  node [
    id 365
    label "occupation"
  ]
  node [
    id 366
    label "jazda"
  ]
  node [
    id 367
    label "commotion"
  ]
  node [
    id 368
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 369
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 370
    label "operacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 20
    target 82
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 205
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 22
    target 208
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 217
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 219
  ]
  edge [
    source 22
    target 220
  ]
  edge [
    source 22
    target 221
  ]
  edge [
    source 22
    target 222
  ]
  edge [
    source 22
    target 223
  ]
  edge [
    source 22
    target 224
  ]
  edge [
    source 22
    target 175
  ]
  edge [
    source 22
    target 225
  ]
  edge [
    source 22
    target 226
  ]
  edge [
    source 22
    target 227
  ]
  edge [
    source 22
    target 228
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 22
    target 231
  ]
  edge [
    source 22
    target 232
  ]
  edge [
    source 22
    target 233
  ]
  edge [
    source 22
    target 234
  ]
  edge [
    source 22
    target 235
  ]
  edge [
    source 22
    target 236
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 33
  ]
  edge [
    source 23
    target 34
  ]
  edge [
    source 23
    target 238
  ]
  edge [
    source 23
    target 239
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 243
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 244
  ]
  edge [
    source 25
    target 115
  ]
  edge [
    source 25
    target 245
  ]
  edge [
    source 25
    target 246
  ]
  edge [
    source 25
    target 247
  ]
  edge [
    source 25
    target 248
  ]
  edge [
    source 25
    target 249
  ]
  edge [
    source 25
    target 250
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 251
  ]
  edge [
    source 26
    target 252
  ]
  edge [
    source 26
    target 253
  ]
  edge [
    source 26
    target 254
  ]
  edge [
    source 26
    target 255
  ]
  edge [
    source 26
    target 256
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 259
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 260
  ]
  edge [
    source 31
    target 261
  ]
  edge [
    source 31
    target 262
  ]
  edge [
    source 31
    target 263
  ]
  edge [
    source 31
    target 264
  ]
  edge [
    source 31
    target 265
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 266
  ]
  edge [
    source 33
    target 267
  ]
  edge [
    source 33
    target 268
  ]
  edge [
    source 33
    target 269
  ]
  edge [
    source 33
    target 270
  ]
  edge [
    source 33
    target 271
  ]
  edge [
    source 33
    target 272
  ]
  edge [
    source 33
    target 273
  ]
  edge [
    source 33
    target 274
  ]
  edge [
    source 33
    target 275
  ]
  edge [
    source 33
    target 276
  ]
  edge [
    source 33
    target 277
  ]
  edge [
    source 33
    target 278
  ]
  edge [
    source 33
    target 279
  ]
  edge [
    source 33
    target 280
  ]
  edge [
    source 33
    target 281
  ]
  edge [
    source 33
    target 282
  ]
  edge [
    source 33
    target 283
  ]
  edge [
    source 33
    target 284
  ]
  edge [
    source 33
    target 285
  ]
  edge [
    source 33
    target 286
  ]
  edge [
    source 33
    target 287
  ]
  edge [
    source 33
    target 288
  ]
  edge [
    source 33
    target 289
  ]
  edge [
    source 33
    target 290
  ]
  edge [
    source 33
    target 291
  ]
  edge [
    source 34
    target 292
  ]
  edge [
    source 34
    target 293
  ]
  edge [
    source 34
    target 294
  ]
  edge [
    source 34
    target 295
  ]
  edge [
    source 34
    target 296
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 297
  ]
  edge [
    source 35
    target 298
  ]
  edge [
    source 35
    target 299
  ]
  edge [
    source 35
    target 300
  ]
  edge [
    source 35
    target 301
  ]
  edge [
    source 35
    target 302
  ]
  edge [
    source 35
    target 303
  ]
  edge [
    source 35
    target 304
  ]
  edge [
    source 35
    target 203
  ]
  edge [
    source 35
    target 305
  ]
  edge [
    source 35
    target 306
  ]
  edge [
    source 35
    target 307
  ]
  edge [
    source 35
    target 308
  ]
  edge [
    source 35
    target 309
  ]
  edge [
    source 35
    target 310
  ]
  edge [
    source 35
    target 311
  ]
  edge [
    source 35
    target 312
  ]
  edge [
    source 35
    target 313
  ]
  edge [
    source 35
    target 247
  ]
  edge [
    source 35
    target 314
  ]
  edge [
    source 35
    target 315
  ]
  edge [
    source 35
    target 316
  ]
  edge [
    source 35
    target 317
  ]
  edge [
    source 35
    target 318
  ]
  edge [
    source 35
    target 319
  ]
  edge [
    source 35
    target 320
  ]
  edge [
    source 35
    target 321
  ]
  edge [
    source 35
    target 322
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 323
  ]
  edge [
    source 38
    target 324
  ]
  edge [
    source 39
    target 325
  ]
  edge [
    source 40
    target 326
  ]
  edge [
    source 40
    target 327
  ]
  edge [
    source 40
    target 219
  ]
  edge [
    source 40
    target 308
  ]
  edge [
    source 40
    target 328
  ]
  edge [
    source 40
    target 316
  ]
  edge [
    source 40
    target 329
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 330
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 331
  ]
  edge [
    source 42
    target 332
  ]
  edge [
    source 42
    target 333
  ]
  edge [
    source 42
    target 334
  ]
  edge [
    source 42
    target 335
  ]
  edge [
    source 42
    target 336
  ]
  edge [
    source 42
    target 337
  ]
  edge [
    source 42
    target 338
  ]
  edge [
    source 42
    target 339
  ]
  edge [
    source 42
    target 340
  ]
  edge [
    source 42
    target 341
  ]
  edge [
    source 42
    target 342
  ]
  edge [
    source 42
    target 343
  ]
  edge [
    source 42
    target 344
  ]
  edge [
    source 42
    target 345
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 346
  ]
  edge [
    source 43
    target 347
  ]
  edge [
    source 43
    target 348
  ]
  edge [
    source 43
    target 349
  ]
  edge [
    source 43
    target 350
  ]
  edge [
    source 43
    target 351
  ]
  edge [
    source 43
    target 352
  ]
  edge [
    source 43
    target 353
  ]
  edge [
    source 44
    target 354
  ]
  edge [
    source 44
    target 355
  ]
  edge [
    source 44
    target 356
  ]
  edge [
    source 44
    target 357
  ]
  edge [
    source 44
    target 358
  ]
  edge [
    source 44
    target 359
  ]
  edge [
    source 44
    target 360
  ]
  edge [
    source 44
    target 361
  ]
  edge [
    source 44
    target 362
  ]
  edge [
    source 44
    target 363
  ]
  edge [
    source 44
    target 364
  ]
  edge [
    source 44
    target 365
  ]
  edge [
    source 44
    target 366
  ]
  edge [
    source 44
    target 189
  ]
  edge [
    source 44
    target 367
  ]
  edge [
    source 44
    target 368
  ]
  edge [
    source 44
    target 369
  ]
  edge [
    source 44
    target 370
  ]
]
