graph [
  maxDegree 6
  minDegree 1
  meanDegree 2
  density 0.07692307692307693
  graphCliqueNumber 4
  node [
    id 0
    label "herman"
    origin "text"
  ]
  node [
    id 1
    label "moralin"
    origin "text"
  ]
  node [
    id 2
    label "lewo"
  ]
  node [
    id 3
    label "Moralin"
  ]
  node [
    id 4
    label "nowo"
  ]
  node [
    id 5
    label "Jazykowie"
  ]
  node [
    id 6
    label "rosyjski"
  ]
  node [
    id 7
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 8
    label "prawos&#322;awny"
  ]
  node [
    id 9
    label "sob&#243;r"
  ]
  node [
    id 10
    label "za&#347;ni&#281;cie"
  ]
  node [
    id 11
    label "matka"
  ]
  node [
    id 12
    label "bo&#380;y"
  ]
  node [
    id 13
    label "parafia"
  ]
  node [
    id 14
    label "narodzi&#263;"
  ]
  node [
    id 15
    label "eparchia"
  ]
  node [
    id 16
    label "w&#322;odzimierski"
  ]
  node [
    id 17
    label "Likin"
  ]
  node [
    id 18
    label "Sudugodski"
  ]
  node [
    id 19
    label "opieka"
  ]
  node [
    id 20
    label "&#347;wi&#281;ty"
  ]
  node [
    id 21
    label "Nikita"
  ]
  node [
    id 22
    label "Juriewie"
  ]
  node [
    id 23
    label "polskie"
  ]
  node [
    id 24
    label "juriew"
  ]
  node [
    id 25
    label "objawi&#263;"
  ]
  node [
    id 26
    label "pa&#324;skie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
]
