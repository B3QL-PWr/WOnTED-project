graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.11764705882352941
  graphCliqueNumber 3
  node [
    id 0
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 1
    label "dokument"
  ]
  node [
    id 2
    label "towar"
  ]
  node [
    id 3
    label "nag&#322;&#243;wek"
  ]
  node [
    id 4
    label "znak_j&#281;zykowy"
  ]
  node [
    id 5
    label "wyr&#243;b"
  ]
  node [
    id 6
    label "blok"
  ]
  node [
    id 7
    label "line"
  ]
  node [
    id 8
    label "paragraf"
  ]
  node [
    id 9
    label "rodzajnik"
  ]
  node [
    id 10
    label "prawda"
  ]
  node [
    id 11
    label "szkic"
  ]
  node [
    id 12
    label "tekst"
  ]
  node [
    id 13
    label "fragment"
  ]
  node [
    id 14
    label "europejski"
  ]
  node [
    id 15
    label "agencja"
  ]
  node [
    id 16
    label "obrona"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
]
