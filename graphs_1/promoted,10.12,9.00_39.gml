graph [
  maxDegree 34
  minDegree 1
  meanDegree 1.9772727272727273
  density 0.022727272727272728
  graphCliqueNumber 3
  node [
    id 0
    label "jaki"
    origin "text"
  ]
  node [
    id 1
    label "zadanie"
    origin "text"
  ]
  node [
    id 2
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 3
    label "spe&#322;nia&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ile"
    origin "text"
  ]
  node [
    id 5
    label "kosztowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "samolot"
    origin "text"
  ]
  node [
    id 8
    label "technologia"
    origin "text"
  ]
  node [
    id 9
    label "stealth"
    origin "text"
  ]
  node [
    id 10
    label "yield"
  ]
  node [
    id 11
    label "czynno&#347;&#263;"
  ]
  node [
    id 12
    label "problem"
  ]
  node [
    id 13
    label "przepisanie"
  ]
  node [
    id 14
    label "przepisa&#263;"
  ]
  node [
    id 15
    label "za&#322;o&#380;enie"
  ]
  node [
    id 16
    label "work"
  ]
  node [
    id 17
    label "nakarmienie"
  ]
  node [
    id 18
    label "duty"
  ]
  node [
    id 19
    label "zbi&#243;r"
  ]
  node [
    id 20
    label "powierzanie"
  ]
  node [
    id 21
    label "zaszkodzenie"
  ]
  node [
    id 22
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 23
    label "zaj&#281;cie"
  ]
  node [
    id 24
    label "zobowi&#261;zanie"
  ]
  node [
    id 25
    label "proszek"
  ]
  node [
    id 26
    label "robi&#263;"
  ]
  node [
    id 27
    label "close"
  ]
  node [
    id 28
    label "perform"
  ]
  node [
    id 29
    label "urzeczywistnia&#263;"
  ]
  node [
    id 30
    label "cena"
  ]
  node [
    id 31
    label "try"
  ]
  node [
    id 32
    label "essay"
  ]
  node [
    id 33
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 34
    label "doznawa&#263;"
  ]
  node [
    id 35
    label "savor"
  ]
  node [
    id 36
    label "si&#281;ga&#263;"
  ]
  node [
    id 37
    label "trwa&#263;"
  ]
  node [
    id 38
    label "obecno&#347;&#263;"
  ]
  node [
    id 39
    label "stan"
  ]
  node [
    id 40
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 41
    label "stand"
  ]
  node [
    id 42
    label "mie&#263;_miejsce"
  ]
  node [
    id 43
    label "uczestniczy&#263;"
  ]
  node [
    id 44
    label "chodzi&#263;"
  ]
  node [
    id 45
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 46
    label "equal"
  ]
  node [
    id 47
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 48
    label "p&#322;atowiec"
  ]
  node [
    id 49
    label "lecenie"
  ]
  node [
    id 50
    label "gondola"
  ]
  node [
    id 51
    label "wylecie&#263;"
  ]
  node [
    id 52
    label "kapotowanie"
  ]
  node [
    id 53
    label "wylatywa&#263;"
  ]
  node [
    id 54
    label "katapulta"
  ]
  node [
    id 55
    label "dzi&#243;b"
  ]
  node [
    id 56
    label "sterownica"
  ]
  node [
    id 57
    label "kad&#322;ub"
  ]
  node [
    id 58
    label "kapotowa&#263;"
  ]
  node [
    id 59
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 60
    label "fotel_lotniczy"
  ]
  node [
    id 61
    label "kabina"
  ]
  node [
    id 62
    label "wylatywanie"
  ]
  node [
    id 63
    label "pilot_automatyczny"
  ]
  node [
    id 64
    label "inhalator_tlenowy"
  ]
  node [
    id 65
    label "kapota&#380;"
  ]
  node [
    id 66
    label "pok&#322;ad"
  ]
  node [
    id 67
    label "sta&#322;op&#322;at"
  ]
  node [
    id 68
    label "&#380;yroskop"
  ]
  node [
    id 69
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 70
    label "wy&#347;lizg"
  ]
  node [
    id 71
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 72
    label "skrzyd&#322;o"
  ]
  node [
    id 73
    label "wiatrochron"
  ]
  node [
    id 74
    label "spalin&#243;wka"
  ]
  node [
    id 75
    label "czarna_skrzynka"
  ]
  node [
    id 76
    label "kapot"
  ]
  node [
    id 77
    label "wylecenie"
  ]
  node [
    id 78
    label "kabinka"
  ]
  node [
    id 79
    label "engineering"
  ]
  node [
    id 80
    label "technika"
  ]
  node [
    id 81
    label "mikrotechnologia"
  ]
  node [
    id 82
    label "technologia_nieorganiczna"
  ]
  node [
    id 83
    label "biotechnologia"
  ]
  node [
    id 84
    label "spos&#243;b"
  ]
  node [
    id 85
    label "PZL"
  ]
  node [
    id 86
    label "230"
  ]
  node [
    id 87
    label "Skorpion"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 87
  ]
  edge [
    source 86
    target 87
  ]
]
