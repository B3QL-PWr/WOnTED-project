graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "juz"
    origin "text"
  ]
  node [
    id 1
    label "ponad"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "stulecie"
  ]
  node [
    id 4
    label "kalendarz"
  ]
  node [
    id 5
    label "czas"
  ]
  node [
    id 6
    label "pora_roku"
  ]
  node [
    id 7
    label "cykl_astronomiczny"
  ]
  node [
    id 8
    label "p&#243;&#322;rocze"
  ]
  node [
    id 9
    label "grupa"
  ]
  node [
    id 10
    label "kwarta&#322;"
  ]
  node [
    id 11
    label "kurs"
  ]
  node [
    id 12
    label "jubileusz"
  ]
  node [
    id 13
    label "miesi&#261;c"
  ]
  node [
    id 14
    label "lata"
  ]
  node [
    id 15
    label "martwy_sezon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
]
