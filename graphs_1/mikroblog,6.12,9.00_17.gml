graph [
  maxDegree 17
  minDegree 1
  meanDegree 2.074074074074074
  density 0.039133473095737246
  graphCliqueNumber 2
  node [
    id 0
    label "nigdy"
    origin "text"
  ]
  node [
    id 1
    label "rozumie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "potrzeba"
    origin "text"
  ]
  node [
    id 3
    label "seksualny"
    origin "text"
  ]
  node [
    id 4
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 5
    label "tak"
    origin "text"
  ]
  node [
    id 6
    label "bardzo"
    origin "text"
  ]
  node [
    id 7
    label "odbiega&#263;"
    origin "text"
  ]
  node [
    id 8
    label "potrzeb"
    origin "text"
  ]
  node [
    id 9
    label "kobieta"
    origin "text"
  ]
  node [
    id 10
    label "kompletnie"
  ]
  node [
    id 11
    label "need"
  ]
  node [
    id 12
    label "wym&#243;g"
  ]
  node [
    id 13
    label "necessity"
  ]
  node [
    id 14
    label "pragnienie"
  ]
  node [
    id 15
    label "sytuacja"
  ]
  node [
    id 16
    label "p&#322;ciowo"
  ]
  node [
    id 17
    label "seksualnie"
  ]
  node [
    id 18
    label "seksowny"
  ]
  node [
    id 19
    label "erotyczny"
  ]
  node [
    id 20
    label "ch&#322;opina"
  ]
  node [
    id 21
    label "cz&#322;owiek"
  ]
  node [
    id 22
    label "jegomo&#347;&#263;"
  ]
  node [
    id 23
    label "bratek"
  ]
  node [
    id 24
    label "doros&#322;y"
  ]
  node [
    id 25
    label "samiec"
  ]
  node [
    id 26
    label "ojciec"
  ]
  node [
    id 27
    label "twardziel"
  ]
  node [
    id 28
    label "androlog"
  ]
  node [
    id 29
    label "pa&#324;stwo"
  ]
  node [
    id 30
    label "m&#261;&#380;"
  ]
  node [
    id 31
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 32
    label "andropauza"
  ]
  node [
    id 33
    label "w_chuj"
  ]
  node [
    id 34
    label "r&#243;&#380;ni&#263;_si&#281;"
  ]
  node [
    id 35
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 36
    label "biec"
  ]
  node [
    id 37
    label "digress"
  ]
  node [
    id 38
    label "odchodzi&#263;"
  ]
  node [
    id 39
    label "przekwitanie"
  ]
  node [
    id 40
    label "m&#281;&#380;yna"
  ]
  node [
    id 41
    label "babka"
  ]
  node [
    id 42
    label "samica"
  ]
  node [
    id 43
    label "ulec"
  ]
  node [
    id 44
    label "uleganie"
  ]
  node [
    id 45
    label "partnerka"
  ]
  node [
    id 46
    label "&#380;ona"
  ]
  node [
    id 47
    label "ulega&#263;"
  ]
  node [
    id 48
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 49
    label "ulegni&#281;cie"
  ]
  node [
    id 50
    label "menopauza"
  ]
  node [
    id 51
    label "&#322;ono"
  ]
  node [
    id 52
    label "centrum"
  ]
  node [
    id 53
    label "handlowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 52
    target 53
  ]
]
