graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.078602620087336
  density 0.00911667815827779
  graphCliqueNumber 3
  node [
    id 0
    label "stowarzyszenie"
    origin "text"
  ]
  node [
    id 1
    label "upowa&#380;ni&#263;"
    origin "text"
  ]
  node [
    id 2
    label "kom&#243;rka"
    origin "text"
  ]
  node [
    id 3
    label "organizacyjny"
    origin "text"
  ]
  node [
    id 4
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 5
    label "obrona"
    origin "text"
  ]
  node [
    id 6
    label "narodowy"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "uzgadnia&#263;"
    origin "text"
  ]
  node [
    id 9
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 10
    label "czerwiec"
    origin "text"
  ]
  node [
    id 11
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 12
    label "rok"
    origin "text"
  ]
  node [
    id 13
    label "plan"
    origin "text"
  ]
  node [
    id 14
    label "wsp&#243;lny"
    origin "text"
  ]
  node [
    id 15
    label "przedsi&#281;wzi&#281;cie"
    origin "text"
  ]
  node [
    id 16
    label "rzecz"
    origin "text"
  ]
  node [
    id 17
    label "obronno&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "nast&#281;pny"
    origin "text"
  ]
  node [
    id 19
    label "kalendarzowy"
    origin "text"
  ]
  node [
    id 20
    label "corocznie"
    origin "text"
  ]
  node [
    id 21
    label "dokonywa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "ocena"
    origin "text"
  ]
  node [
    id 23
    label "efekt"
    origin "text"
  ]
  node [
    id 24
    label "wsp&#243;&#322;dzia&#322;anie"
    origin "text"
  ]
  node [
    id 25
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 26
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 27
    label "organizacja"
  ]
  node [
    id 28
    label "Eleusis"
  ]
  node [
    id 29
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 30
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 31
    label "grupa"
  ]
  node [
    id 32
    label "fabianie"
  ]
  node [
    id 33
    label "Chewra_Kadisza"
  ]
  node [
    id 34
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 35
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 36
    label "Rotary_International"
  ]
  node [
    id 37
    label "Monar"
  ]
  node [
    id 38
    label "authorize"
  ]
  node [
    id 39
    label "spowodowa&#263;"
  ]
  node [
    id 40
    label "pole"
  ]
  node [
    id 41
    label "telefon"
  ]
  node [
    id 42
    label "embrioblast"
  ]
  node [
    id 43
    label "obszar"
  ]
  node [
    id 44
    label "b&#322;ona_kom&#243;rkowa"
  ]
  node [
    id 45
    label "cell"
  ]
  node [
    id 46
    label "cytochemia"
  ]
  node [
    id 47
    label "pomieszczenie"
  ]
  node [
    id 48
    label "b&#322;ona_podstawna"
  ]
  node [
    id 49
    label "organellum"
  ]
  node [
    id 50
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 51
    label "wy&#347;wietlacz"
  ]
  node [
    id 52
    label "mikrosom"
  ]
  node [
    id 53
    label "burza"
  ]
  node [
    id 54
    label "filia"
  ]
  node [
    id 55
    label "cytoplazma"
  ]
  node [
    id 56
    label "hipoderma"
  ]
  node [
    id 57
    label "oddychanie_kom&#243;rkowe"
  ]
  node [
    id 58
    label "tkanka"
  ]
  node [
    id 59
    label "wakuom"
  ]
  node [
    id 60
    label "biomembrana"
  ]
  node [
    id 61
    label "plaster"
  ]
  node [
    id 62
    label "struktura_anatomiczna"
  ]
  node [
    id 63
    label "osocze_krwi"
  ]
  node [
    id 64
    label "genotyp"
  ]
  node [
    id 65
    label "urz&#261;dzenie"
  ]
  node [
    id 66
    label "p&#281;cherzyk"
  ]
  node [
    id 67
    label "tabela"
  ]
  node [
    id 68
    label "akantoliza"
  ]
  node [
    id 69
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 70
    label "ministerium"
  ]
  node [
    id 71
    label "resort"
  ]
  node [
    id 72
    label "urz&#261;d"
  ]
  node [
    id 73
    label "MSW"
  ]
  node [
    id 74
    label "departament"
  ]
  node [
    id 75
    label "NKWD"
  ]
  node [
    id 76
    label "manewr"
  ]
  node [
    id 77
    label "reakcja"
  ]
  node [
    id 78
    label "auspices"
  ]
  node [
    id 79
    label "mecz"
  ]
  node [
    id 80
    label "poparcie"
  ]
  node [
    id 81
    label "ochrona"
  ]
  node [
    id 82
    label "s&#261;d"
  ]
  node [
    id 83
    label "defensive_structure"
  ]
  node [
    id 84
    label "liga"
  ]
  node [
    id 85
    label "egzamin"
  ]
  node [
    id 86
    label "gracz"
  ]
  node [
    id 87
    label "defense"
  ]
  node [
    id 88
    label "walka"
  ]
  node [
    id 89
    label "post&#281;powanie"
  ]
  node [
    id 90
    label "wojsko"
  ]
  node [
    id 91
    label "protection"
  ]
  node [
    id 92
    label "poj&#281;cie"
  ]
  node [
    id 93
    label "guard_duty"
  ]
  node [
    id 94
    label "strona"
  ]
  node [
    id 95
    label "sp&#243;r"
  ]
  node [
    id 96
    label "gra"
  ]
  node [
    id 97
    label "nacjonalistyczny"
  ]
  node [
    id 98
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 99
    label "narodowo"
  ]
  node [
    id 100
    label "wa&#380;ny"
  ]
  node [
    id 101
    label "si&#281;ga&#263;"
  ]
  node [
    id 102
    label "trwa&#263;"
  ]
  node [
    id 103
    label "obecno&#347;&#263;"
  ]
  node [
    id 104
    label "stan"
  ]
  node [
    id 105
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 106
    label "stand"
  ]
  node [
    id 107
    label "mie&#263;_miejsce"
  ]
  node [
    id 108
    label "uczestniczy&#263;"
  ]
  node [
    id 109
    label "chodzi&#263;"
  ]
  node [
    id 110
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 111
    label "equal"
  ]
  node [
    id 112
    label "ujednostajnia&#263;"
  ]
  node [
    id 113
    label "porozumiewa&#263;_si&#281;"
  ]
  node [
    id 114
    label "ask"
  ]
  node [
    id 115
    label "uwsp&#243;lnia&#263;"
  ]
  node [
    id 116
    label "s&#322;o&#324;ce"
  ]
  node [
    id 117
    label "czynienie_si&#281;"
  ]
  node [
    id 118
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 119
    label "czas"
  ]
  node [
    id 120
    label "long_time"
  ]
  node [
    id 121
    label "przedpo&#322;udnie"
  ]
  node [
    id 122
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 123
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 124
    label "tydzie&#324;"
  ]
  node [
    id 125
    label "godzina"
  ]
  node [
    id 126
    label "t&#322;usty_czwartek"
  ]
  node [
    id 127
    label "wsta&#263;"
  ]
  node [
    id 128
    label "day"
  ]
  node [
    id 129
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 130
    label "przedwiecz&#243;r"
  ]
  node [
    id 131
    label "Sylwester"
  ]
  node [
    id 132
    label "po&#322;udnie"
  ]
  node [
    id 133
    label "wzej&#347;cie"
  ]
  node [
    id 134
    label "podwiecz&#243;r"
  ]
  node [
    id 135
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 136
    label "rano"
  ]
  node [
    id 137
    label "termin"
  ]
  node [
    id 138
    label "ranek"
  ]
  node [
    id 139
    label "doba"
  ]
  node [
    id 140
    label "wiecz&#243;r"
  ]
  node [
    id 141
    label "walentynki"
  ]
  node [
    id 142
    label "popo&#322;udnie"
  ]
  node [
    id 143
    label "noc"
  ]
  node [
    id 144
    label "wstanie"
  ]
  node [
    id 145
    label "ro&#347;lina_zielna"
  ]
  node [
    id 146
    label "go&#378;dzikowate"
  ]
  node [
    id 147
    label "miesi&#261;c"
  ]
  node [
    id 148
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 149
    label "jaki&#347;"
  ]
  node [
    id 150
    label "stulecie"
  ]
  node [
    id 151
    label "kalendarz"
  ]
  node [
    id 152
    label "pora_roku"
  ]
  node [
    id 153
    label "cykl_astronomiczny"
  ]
  node [
    id 154
    label "p&#243;&#322;rocze"
  ]
  node [
    id 155
    label "kwarta&#322;"
  ]
  node [
    id 156
    label "kurs"
  ]
  node [
    id 157
    label "jubileusz"
  ]
  node [
    id 158
    label "lata"
  ]
  node [
    id 159
    label "martwy_sezon"
  ]
  node [
    id 160
    label "device"
  ]
  node [
    id 161
    label "model"
  ]
  node [
    id 162
    label "wytw&#243;r"
  ]
  node [
    id 163
    label "obraz"
  ]
  node [
    id 164
    label "przestrze&#324;"
  ]
  node [
    id 165
    label "dekoracja"
  ]
  node [
    id 166
    label "intencja"
  ]
  node [
    id 167
    label "agreement"
  ]
  node [
    id 168
    label "pomys&#322;"
  ]
  node [
    id 169
    label "punkt"
  ]
  node [
    id 170
    label "miejsce_pracy"
  ]
  node [
    id 171
    label "perspektywa"
  ]
  node [
    id 172
    label "rysunek"
  ]
  node [
    id 173
    label "reprezentacja"
  ]
  node [
    id 174
    label "spolny"
  ]
  node [
    id 175
    label "jeden"
  ]
  node [
    id 176
    label "sp&#243;lny"
  ]
  node [
    id 177
    label "wsp&#243;lnie"
  ]
  node [
    id 178
    label "uwsp&#243;lnianie"
  ]
  node [
    id 179
    label "uwsp&#243;lnienie"
  ]
  node [
    id 180
    label "zrobienie"
  ]
  node [
    id 181
    label "consumption"
  ]
  node [
    id 182
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 183
    label "zacz&#281;cie"
  ]
  node [
    id 184
    label "startup"
  ]
  node [
    id 185
    label "obiekt"
  ]
  node [
    id 186
    label "temat"
  ]
  node [
    id 187
    label "istota"
  ]
  node [
    id 188
    label "wpa&#347;&#263;"
  ]
  node [
    id 189
    label "wpadanie"
  ]
  node [
    id 190
    label "przedmiot"
  ]
  node [
    id 191
    label "wpada&#263;"
  ]
  node [
    id 192
    label "kultura"
  ]
  node [
    id 193
    label "przyroda"
  ]
  node [
    id 194
    label "mienie"
  ]
  node [
    id 195
    label "object"
  ]
  node [
    id 196
    label "wpadni&#281;cie"
  ]
  node [
    id 197
    label "gospodarka"
  ]
  node [
    id 198
    label "nast&#281;pnie"
  ]
  node [
    id 199
    label "kolejno"
  ]
  node [
    id 200
    label "nastopny"
  ]
  node [
    id 201
    label "coroczny"
  ]
  node [
    id 202
    label "cyklicznie"
  ]
  node [
    id 203
    label "rocznie"
  ]
  node [
    id 204
    label "robi&#263;"
  ]
  node [
    id 205
    label "determine"
  ]
  node [
    id 206
    label "przestawa&#263;"
  ]
  node [
    id 207
    label "make"
  ]
  node [
    id 208
    label "informacja"
  ]
  node [
    id 209
    label "sofcik"
  ]
  node [
    id 210
    label "appraisal"
  ]
  node [
    id 211
    label "decyzja"
  ]
  node [
    id 212
    label "pogl&#261;d"
  ]
  node [
    id 213
    label "kryterium"
  ]
  node [
    id 214
    label "dzia&#322;anie"
  ]
  node [
    id 215
    label "typ"
  ]
  node [
    id 216
    label "impression"
  ]
  node [
    id 217
    label "robienie_wra&#380;enia"
  ]
  node [
    id 218
    label "wra&#380;enie"
  ]
  node [
    id 219
    label "przyczyna"
  ]
  node [
    id 220
    label "zrobienie_wra&#380;enia"
  ]
  node [
    id 221
    label "robi&#263;_wra&#380;enie"
  ]
  node [
    id 222
    label "&#347;rodek"
  ]
  node [
    id 223
    label "event"
  ]
  node [
    id 224
    label "zrobi&#263;_wra&#380;enie"
  ]
  node [
    id 225
    label "rezultat"
  ]
  node [
    id 226
    label "struktura_sieciowa"
  ]
  node [
    id 227
    label "synergy"
  ]
  node [
    id 228
    label "partnerski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 21
    target 206
  ]
  edge [
    source 21
    target 207
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 208
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 23
    target 218
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 226
  ]
  edge [
    source 24
    target 227
  ]
  edge [
    source 24
    target 228
  ]
]
