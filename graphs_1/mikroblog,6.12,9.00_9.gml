graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9487179487179487
  density 0.05128205128205128
  graphCliqueNumber 2
  node [
    id 0
    label "mireczka"
    origin "text"
  ]
  node [
    id 1
    label "chyba"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 3
    label "ko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 6
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 7
    label "dok&#322;adnie"
  ]
  node [
    id 8
    label "zako&#324;cza&#263;"
  ]
  node [
    id 9
    label "przestawa&#263;"
  ]
  node [
    id 10
    label "robi&#263;"
  ]
  node [
    id 11
    label "satisfy"
  ]
  node [
    id 12
    label "close"
  ]
  node [
    id 13
    label "determine"
  ]
  node [
    id 14
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 15
    label "stanowi&#263;"
  ]
  node [
    id 16
    label "czyj&#347;"
  ]
  node [
    id 17
    label "m&#261;&#380;"
  ]
  node [
    id 18
    label "odwodnienie"
  ]
  node [
    id 19
    label "konstytucja"
  ]
  node [
    id 20
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 21
    label "substancja_chemiczna"
  ]
  node [
    id 22
    label "bratnia_dusza"
  ]
  node [
    id 23
    label "zwi&#261;zanie"
  ]
  node [
    id 24
    label "lokant"
  ]
  node [
    id 25
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 26
    label "zwi&#261;za&#263;"
  ]
  node [
    id 27
    label "organizacja"
  ]
  node [
    id 28
    label "odwadnia&#263;"
  ]
  node [
    id 29
    label "marriage"
  ]
  node [
    id 30
    label "marketing_afiliacyjny"
  ]
  node [
    id 31
    label "bearing"
  ]
  node [
    id 32
    label "wi&#261;zanie"
  ]
  node [
    id 33
    label "odwadnianie"
  ]
  node [
    id 34
    label "koligacja"
  ]
  node [
    id 35
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 36
    label "odwodni&#263;"
  ]
  node [
    id 37
    label "azeotrop"
  ]
  node [
    id 38
    label "powi&#261;zanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
]
