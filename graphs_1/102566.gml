graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9649122807017543
  density 0.03508771929824561
  graphCliqueNumber 2
  node [
    id 0
    label "zakres"
    origin "text"
  ]
  node [
    id 1
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 2
    label "gabinet"
    origin "text"
  ]
  node [
    id 3
    label "prezydent"
    origin "text"
  ]
  node [
    id 4
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 5
    label "szczeg&#243;lno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 7
    label "granica"
  ]
  node [
    id 8
    label "circle"
  ]
  node [
    id 9
    label "podzakres"
  ]
  node [
    id 10
    label "zbi&#243;r"
  ]
  node [
    id 11
    label "dziedzina"
  ]
  node [
    id 12
    label "desygnat"
  ]
  node [
    id 13
    label "sfera"
  ]
  node [
    id 14
    label "wielko&#347;&#263;"
  ]
  node [
    id 15
    label "strategia"
  ]
  node [
    id 16
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 17
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 18
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 19
    label "pok&#243;j"
  ]
  node [
    id 20
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 21
    label "s&#261;d"
  ]
  node [
    id 22
    label "egzekutywa"
  ]
  node [
    id 23
    label "biurko"
  ]
  node [
    id 24
    label "gabinet_cieni"
  ]
  node [
    id 25
    label "palestra"
  ]
  node [
    id 26
    label "premier"
  ]
  node [
    id 27
    label "Londyn"
  ]
  node [
    id 28
    label "Konsulat"
  ]
  node [
    id 29
    label "pracownia"
  ]
  node [
    id 30
    label "instytucja"
  ]
  node [
    id 31
    label "boks"
  ]
  node [
    id 32
    label "pomieszczenie"
  ]
  node [
    id 33
    label "szko&#322;a"
  ]
  node [
    id 34
    label "Jelcyn"
  ]
  node [
    id 35
    label "Roosevelt"
  ]
  node [
    id 36
    label "Clinton"
  ]
  node [
    id 37
    label "dostojnik"
  ]
  node [
    id 38
    label "Tito"
  ]
  node [
    id 39
    label "de_Gaulle"
  ]
  node [
    id 40
    label "Nixon"
  ]
  node [
    id 41
    label "gruba_ryba"
  ]
  node [
    id 42
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 43
    label "Putin"
  ]
  node [
    id 44
    label "Gorbaczow"
  ]
  node [
    id 45
    label "Naser"
  ]
  node [
    id 46
    label "samorz&#261;dowiec"
  ]
  node [
    id 47
    label "Kemal"
  ]
  node [
    id 48
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 49
    label "zwierzchnik"
  ]
  node [
    id 50
    label "Bierut"
  ]
  node [
    id 51
    label "trza"
  ]
  node [
    id 52
    label "uczestniczy&#263;"
  ]
  node [
    id 53
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 54
    label "para"
  ]
  node [
    id 55
    label "necessity"
  ]
  node [
    id 56
    label "wyj&#261;tkowo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 5
    target 56
  ]
]
