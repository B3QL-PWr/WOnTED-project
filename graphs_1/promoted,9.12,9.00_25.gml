graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.95
  density 0.05
  graphCliqueNumber 2
  node [
    id 0
    label "kampania"
    origin "text"
  ]
  node [
    id 1
    label "zdrowotny"
    origin "text"
  ]
  node [
    id 2
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 3
    label "zdrowie"
    origin "text"
  ]
  node [
    id 4
    label "przeciw"
    origin "text"
  ]
  node [
    id 5
    label "oty&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "dzia&#322;anie"
  ]
  node [
    id 7
    label "campaign"
  ]
  node [
    id 8
    label "wydarzenie"
  ]
  node [
    id 9
    label "akcja"
  ]
  node [
    id 10
    label "Kampania_Wrze&#347;niowa"
  ]
  node [
    id 11
    label "zdrowy"
  ]
  node [
    id 12
    label "dobry"
  ]
  node [
    id 13
    label "prozdrowotny"
  ]
  node [
    id 14
    label "zdrowotnie"
  ]
  node [
    id 15
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 16
    label "ministerium"
  ]
  node [
    id 17
    label "resort"
  ]
  node [
    id 18
    label "urz&#261;d"
  ]
  node [
    id 19
    label "MSW"
  ]
  node [
    id 20
    label "departament"
  ]
  node [
    id 21
    label "NKWD"
  ]
  node [
    id 22
    label "os&#322;abia&#263;"
  ]
  node [
    id 23
    label "niszczy&#263;"
  ]
  node [
    id 24
    label "zniszczy&#263;"
  ]
  node [
    id 25
    label "stan"
  ]
  node [
    id 26
    label "firmness"
  ]
  node [
    id 27
    label "kondycja"
  ]
  node [
    id 28
    label "zniszczenie"
  ]
  node [
    id 29
    label "rozsypanie_si&#281;"
  ]
  node [
    id 30
    label "os&#322;abi&#263;"
  ]
  node [
    id 31
    label "cecha"
  ]
  node [
    id 32
    label "zdarcie"
  ]
  node [
    id 33
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 34
    label "zedrze&#263;"
  ]
  node [
    id 35
    label "niszczenie"
  ]
  node [
    id 36
    label "os&#322;abienie"
  ]
  node [
    id 37
    label "soundness"
  ]
  node [
    id 38
    label "os&#322;abianie"
  ]
  node [
    id 39
    label "grubo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 39
  ]
]
