graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.96
  density 0.04
  graphCliqueNumber 2
  node [
    id 0
    label "budowa"
    origin "text"
  ]
  node [
    id 1
    label "dom"
    origin "text"
  ]
  node [
    id 2
    label "prefabrykat"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "bardzo"
    origin "text"
  ]
  node [
    id 5
    label "szybka"
    origin "text"
  ]
  node [
    id 6
    label "figura"
  ]
  node [
    id 7
    label "wjazd"
  ]
  node [
    id 8
    label "struktura"
  ]
  node [
    id 9
    label "konstrukcja"
  ]
  node [
    id 10
    label "r&#243;w"
  ]
  node [
    id 11
    label "kreacja"
  ]
  node [
    id 12
    label "posesja"
  ]
  node [
    id 13
    label "cecha"
  ]
  node [
    id 14
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 15
    label "organ"
  ]
  node [
    id 16
    label "mechanika"
  ]
  node [
    id 17
    label "zwierz&#281;"
  ]
  node [
    id 18
    label "miejsce_pracy"
  ]
  node [
    id 19
    label "praca"
  ]
  node [
    id 20
    label "constitution"
  ]
  node [
    id 21
    label "garderoba"
  ]
  node [
    id 22
    label "wiecha"
  ]
  node [
    id 23
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 24
    label "grupa"
  ]
  node [
    id 25
    label "budynek"
  ]
  node [
    id 26
    label "fratria"
  ]
  node [
    id 27
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 28
    label "poj&#281;cie"
  ]
  node [
    id 29
    label "rodzina"
  ]
  node [
    id 30
    label "substancja_mieszkaniowa"
  ]
  node [
    id 31
    label "instytucja"
  ]
  node [
    id 32
    label "dom_rodzinny"
  ]
  node [
    id 33
    label "stead"
  ]
  node [
    id 34
    label "siedziba"
  ]
  node [
    id 35
    label "materia&#322;_budowlany"
  ]
  node [
    id 36
    label "si&#281;ga&#263;"
  ]
  node [
    id 37
    label "trwa&#263;"
  ]
  node [
    id 38
    label "obecno&#347;&#263;"
  ]
  node [
    id 39
    label "stan"
  ]
  node [
    id 40
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 41
    label "stand"
  ]
  node [
    id 42
    label "mie&#263;_miejsce"
  ]
  node [
    id 43
    label "uczestniczy&#263;"
  ]
  node [
    id 44
    label "chodzi&#263;"
  ]
  node [
    id 45
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 46
    label "equal"
  ]
  node [
    id 47
    label "w_chuj"
  ]
  node [
    id 48
    label "szk&#322;o"
  ]
  node [
    id 49
    label "quartz_glass"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
]
