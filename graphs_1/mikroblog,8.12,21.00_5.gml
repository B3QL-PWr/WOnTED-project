graph [
  maxDegree 11
  minDegree 1
  meanDegree 2
  density 0.05714285714285714
  graphCliqueNumber 3
  node [
    id 0
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 1
    label "wuj"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "mieszek"
    origin "text"
  ]
  node [
    id 4
    label "sta&#322;a"
    origin "text"
  ]
  node [
    id 5
    label "norwegia"
    origin "text"
  ]
  node [
    id 6
    label "opowiedzie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 8
    label "anegdota"
    origin "text"
  ]
  node [
    id 9
    label "odno&#347;nie"
    origin "text"
  ]
  node [
    id 10
    label "piekarnia"
    origin "text"
  ]
  node [
    id 11
    label "czyj&#347;"
  ]
  node [
    id 12
    label "m&#261;&#380;"
  ]
  node [
    id 13
    label "krewny"
  ]
  node [
    id 14
    label "wujo"
  ]
  node [
    id 15
    label "woreczek"
  ]
  node [
    id 16
    label "przedmiot"
  ]
  node [
    id 17
    label "czujnik"
  ]
  node [
    id 18
    label "owoc"
  ]
  node [
    id 19
    label "kapsa"
  ]
  node [
    id 20
    label "element"
  ]
  node [
    id 21
    label "aparat_fotograficzny"
  ]
  node [
    id 22
    label "urz&#261;dzenie"
  ]
  node [
    id 23
    label "piter"
  ]
  node [
    id 24
    label "wielko&#347;&#263;"
  ]
  node [
    id 25
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 26
    label "constant"
  ]
  node [
    id 27
    label "describe"
  ]
  node [
    id 28
    label "przedstawi&#263;"
  ]
  node [
    id 29
    label "opowiadanie"
  ]
  node [
    id 30
    label "tre&#347;&#263;"
  ]
  node [
    id 31
    label "anecdote"
  ]
  node [
    id 32
    label "raptularz"
  ]
  node [
    id 33
    label "pomieszczenie"
  ]
  node [
    id 34
    label "sklep"
  ]
  node [
    id 35
    label "wytw&#243;rnia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 35
  ]
]
