graph [
  maxDegree 46
  minDegree 1
  meanDegree 1.9615384615384615
  density 0.038461538461538464
  graphCliqueNumber 2
  node [
    id 0
    label "lato"
    origin "text"
  ]
  node [
    id 1
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 2
    label "historia"
    origin "text"
  ]
  node [
    id 3
    label "ciekawostka"
    origin "text"
  ]
  node [
    id 4
    label "pora_roku"
  ]
  node [
    id 5
    label "p&#243;&#378;ny"
  ]
  node [
    id 6
    label "report"
  ]
  node [
    id 7
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 8
    label "wypowied&#378;"
  ]
  node [
    id 9
    label "neografia"
  ]
  node [
    id 10
    label "przedmiot"
  ]
  node [
    id 11
    label "papirologia"
  ]
  node [
    id 12
    label "historia_gospodarcza"
  ]
  node [
    id 13
    label "przebiec"
  ]
  node [
    id 14
    label "hista"
  ]
  node [
    id 15
    label "nauka_humanistyczna"
  ]
  node [
    id 16
    label "filigranistyka"
  ]
  node [
    id 17
    label "dyplomatyka"
  ]
  node [
    id 18
    label "annalistyka"
  ]
  node [
    id 19
    label "historyka"
  ]
  node [
    id 20
    label "heraldyka"
  ]
  node [
    id 21
    label "fabu&#322;a"
  ]
  node [
    id 22
    label "muzealnictwo"
  ]
  node [
    id 23
    label "varsavianistyka"
  ]
  node [
    id 24
    label "prezentyzm"
  ]
  node [
    id 25
    label "mediewistyka"
  ]
  node [
    id 26
    label "przebiegni&#281;cie"
  ]
  node [
    id 27
    label "charakter"
  ]
  node [
    id 28
    label "paleografia"
  ]
  node [
    id 29
    label "genealogia"
  ]
  node [
    id 30
    label "czynno&#347;&#263;"
  ]
  node [
    id 31
    label "prozopografia"
  ]
  node [
    id 32
    label "motyw"
  ]
  node [
    id 33
    label "nautologia"
  ]
  node [
    id 34
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 35
    label "epoka"
  ]
  node [
    id 36
    label "numizmatyka"
  ]
  node [
    id 37
    label "ruralistyka"
  ]
  node [
    id 38
    label "epigrafika"
  ]
  node [
    id 39
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 40
    label "historiografia"
  ]
  node [
    id 41
    label "bizantynistyka"
  ]
  node [
    id 42
    label "weksylologia"
  ]
  node [
    id 43
    label "kierunek"
  ]
  node [
    id 44
    label "ikonografia"
  ]
  node [
    id 45
    label "chronologia"
  ]
  node [
    id 46
    label "archiwistyka"
  ]
  node [
    id 47
    label "sfragistyka"
  ]
  node [
    id 48
    label "zabytkoznawstwo"
  ]
  node [
    id 49
    label "historia_sztuki"
  ]
  node [
    id 50
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 51
    label "informacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
]
