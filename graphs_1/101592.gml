graph [
  maxDegree 8
  minDegree 1
  meanDegree 2.72
  density 0.11333333333333333
  graphCliqueNumber 7
  node [
    id 0
    label "wicemarsza&#322;ek"
    origin "text"
  ]
  node [
    id 1
    label "krzysztof"
    origin "text"
  ]
  node [
    id 2
    label "putra"
    origin "text"
  ]
  node [
    id 3
    label "marsza&#322;ek"
  ]
  node [
    id 4
    label "zast&#281;pca"
  ]
  node [
    id 5
    label "Krzysztofa"
  ]
  node [
    id 6
    label "Putra"
  ]
  node [
    id 7
    label "Wojciecha"
  ]
  node [
    id 8
    label "Kossakowski"
  ]
  node [
    id 9
    label "prawo"
  ]
  node [
    id 10
    label "i"
  ]
  node [
    id 11
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 12
    label "celny"
  ]
  node [
    id 13
    label "Jaros&#322;awa"
  ]
  node [
    id 14
    label "Matwiejuk"
  ]
  node [
    id 15
    label "ustawa"
  ]
  node [
    id 16
    label "ojciec"
  ]
  node [
    id 17
    label "gwarancja"
  ]
  node [
    id 18
    label "wolno&#347;&#263;"
  ]
  node [
    id 19
    label "sumienie"
  ]
  node [
    id 20
    label "wyzna&#263;"
  ]
  node [
    id 21
    label "ministerstwo"
  ]
  node [
    id 22
    label "finanse"
  ]
  node [
    id 23
    label "Jacek"
  ]
  node [
    id 24
    label "kapica"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
]
