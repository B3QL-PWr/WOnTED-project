graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "malina"
    origin "text"
  ]
  node [
    id 1
    label "dzbanek"
    origin "text"
  ]
  node [
    id 2
    label "wielopestkowiec"
  ]
  node [
    id 3
    label "pelargonidyna"
  ]
  node [
    id 4
    label "owoc"
  ]
  node [
    id 5
    label "je&#380;yna"
  ]
  node [
    id 6
    label "zawarto&#347;&#263;"
  ]
  node [
    id 7
    label "naczynie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
]
