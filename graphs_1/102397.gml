graph [
  maxDegree 13
  minDegree 1
  meanDegree 2.0615384615384613
  density 0.03221153846153846
  graphCliqueNumber 3
  node [
    id 0
    label "przygotowywa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "jeden"
    origin "text"
  ]
  node [
    id 2
    label "kielich"
    origin "text"
  ]
  node [
    id 3
    label "jedno"
    origin "text"
  ]
  node [
    id 4
    label "&#322;o&#380;ysko"
    origin "text"
  ]
  node [
    id 5
    label "obudowa"
    origin "text"
  ]
  node [
    id 6
    label "kubek"
    origin "text"
  ]
  node [
    id 7
    label "dyferencja&#322;"
    origin "text"
  ]
  node [
    id 8
    label "wykonywa&#263;"
  ]
  node [
    id 9
    label "sposobi&#263;"
  ]
  node [
    id 10
    label "arrange"
  ]
  node [
    id 11
    label "pryczy&#263;"
  ]
  node [
    id 12
    label "train"
  ]
  node [
    id 13
    label "robi&#263;"
  ]
  node [
    id 14
    label "wytwarza&#263;"
  ]
  node [
    id 15
    label "szkoli&#263;"
  ]
  node [
    id 16
    label "usposabia&#263;"
  ]
  node [
    id 17
    label "kieliszek"
  ]
  node [
    id 18
    label "shot"
  ]
  node [
    id 19
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 20
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 21
    label "jaki&#347;"
  ]
  node [
    id 22
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 23
    label "jednolicie"
  ]
  node [
    id 24
    label "w&#243;dka"
  ]
  node [
    id 25
    label "ten"
  ]
  node [
    id 26
    label "ujednolicenie"
  ]
  node [
    id 27
    label "jednakowy"
  ]
  node [
    id 28
    label "zawarto&#347;&#263;"
  ]
  node [
    id 29
    label "obiekt"
  ]
  node [
    id 30
    label "roztruchan"
  ]
  node [
    id 31
    label "kwiat"
  ]
  node [
    id 32
    label "naczynie"
  ]
  node [
    id 33
    label "puch_kielichowy"
  ]
  node [
    id 34
    label "Graal"
  ]
  node [
    id 35
    label "kszta&#322;t"
  ]
  node [
    id 36
    label "dzia&#322;ka"
  ]
  node [
    id 37
    label "katarakta"
  ]
  node [
    id 38
    label "trofoblast"
  ]
  node [
    id 39
    label "placenta"
  ]
  node [
    id 40
    label "organ"
  ]
  node [
    id 41
    label "dolina"
  ]
  node [
    id 42
    label "zal&#261;&#380;nia"
  ]
  node [
    id 43
    label "layer"
  ]
  node [
    id 44
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 45
    label "panew"
  ]
  node [
    id 46
    label "bariera_&#322;o&#380;yskowa"
  ]
  node [
    id 47
    label "ochrona"
  ]
  node [
    id 48
    label "enclosure"
  ]
  node [
    id 49
    label "konstrukcja"
  ]
  node [
    id 50
    label "wyposa&#380;enie_wn&#281;trz"
  ]
  node [
    id 51
    label "os&#322;ona"
  ]
  node [
    id 52
    label "zabudowa"
  ]
  node [
    id 53
    label "box"
  ]
  node [
    id 54
    label "wyrobisko"
  ]
  node [
    id 55
    label "pieczarkowiec"
  ]
  node [
    id 56
    label "pieczarkowate"
  ]
  node [
    id 57
    label "ucho"
  ]
  node [
    id 58
    label "pojemnik"
  ]
  node [
    id 59
    label "przyrz&#261;d_asekuracyjny"
  ]
  node [
    id 60
    label "beaker"
  ]
  node [
    id 61
    label "opakowanie"
  ]
  node [
    id 62
    label "przek&#322;adnia_z&#281;bata"
  ]
  node [
    id 63
    label "differential_gear"
  ]
  node [
    id 64
    label "mechanizm"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
]
