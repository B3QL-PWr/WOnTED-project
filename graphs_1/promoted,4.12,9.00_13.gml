graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9682539682539681
  density 0.031746031746031744
  graphCliqueNumber 2
  node [
    id 0
    label "policjantka"
    origin "text"
  ]
  node [
    id 1
    label "legnica"
    origin "text"
  ]
  node [
    id 2
    label "dolny"
    origin "text"
  ]
  node [
    id 3
    label "&#347;l&#261;ski"
    origin "text"
  ]
  node [
    id 4
    label "zapa&#322;"
    origin "text"
  ]
  node [
    id 5
    label "&#347;ciga&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przest&#281;pca"
    origin "text"
  ]
  node [
    id 7
    label "dostawa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "nawet"
    origin "text"
  ]
  node [
    id 9
    label "nagroda"
    origin "text"
  ]
  node [
    id 10
    label "nisko"
  ]
  node [
    id 11
    label "zminimalizowanie"
  ]
  node [
    id 12
    label "minimalnie"
  ]
  node [
    id 13
    label "graniczny"
  ]
  node [
    id 14
    label "minimalizowanie"
  ]
  node [
    id 15
    label "buchta"
  ]
  node [
    id 16
    label "szl&#261;ski"
  ]
  node [
    id 17
    label "waloszek"
  ]
  node [
    id 18
    label "szpajza"
  ]
  node [
    id 19
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 20
    label "ch&#322;opiec"
  ]
  node [
    id 21
    label "cug"
  ]
  node [
    id 22
    label "francuz"
  ]
  node [
    id 23
    label "regionalny"
  ]
  node [
    id 24
    label "&#347;lonski"
  ]
  node [
    id 25
    label "mietlorz"
  ]
  node [
    id 26
    label "polski"
  ]
  node [
    id 27
    label "halba"
  ]
  node [
    id 28
    label "sza&#322;ot"
  ]
  node [
    id 29
    label "czarne_kluski"
  ]
  node [
    id 30
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 31
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 32
    label "krepel"
  ]
  node [
    id 33
    label "etnolekt"
  ]
  node [
    id 34
    label "passion"
  ]
  node [
    id 35
    label "power"
  ]
  node [
    id 36
    label "zapalno&#347;&#263;"
  ]
  node [
    id 37
    label "podekscytowanie"
  ]
  node [
    id 38
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 39
    label "straszy&#263;"
  ]
  node [
    id 40
    label "prosecute"
  ]
  node [
    id 41
    label "kara&#263;"
  ]
  node [
    id 42
    label "usi&#322;owa&#263;"
  ]
  node [
    id 43
    label "poszukiwa&#263;"
  ]
  node [
    id 44
    label "z&#322;oczy&#324;ca"
  ]
  node [
    id 45
    label "pogwa&#322;ciciel"
  ]
  node [
    id 46
    label "si&#281;ga&#263;"
  ]
  node [
    id 47
    label "by&#263;"
  ]
  node [
    id 48
    label "uzyskiwa&#263;"
  ]
  node [
    id 49
    label "wystarcza&#263;"
  ]
  node [
    id 50
    label "range"
  ]
  node [
    id 51
    label "winnings"
  ]
  node [
    id 52
    label "otrzymywa&#263;"
  ]
  node [
    id 53
    label "mie&#263;_miejsce"
  ]
  node [
    id 54
    label "opanowywa&#263;"
  ]
  node [
    id 55
    label "kupowa&#263;"
  ]
  node [
    id 56
    label "nabywa&#263;"
  ]
  node [
    id 57
    label "bra&#263;"
  ]
  node [
    id 58
    label "obskakiwa&#263;"
  ]
  node [
    id 59
    label "return"
  ]
  node [
    id 60
    label "konsekwencja"
  ]
  node [
    id 61
    label "oskar"
  ]
  node [
    id 62
    label "wyr&#243;&#380;nienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
]
