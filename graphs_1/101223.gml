graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.0943396226415096
  density 0.009925780202092461
  graphCliqueNumber 3
  node [
    id 0
    label "inny"
    origin "text"
  ]
  node [
    id 1
    label "zabytek"
    origin "text"
  ]
  node [
    id 2
    label "wsi"
    origin "text"
  ]
  node [
    id 3
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 4
    label "klasycystyczny"
    origin "text"
  ]
  node [
    id 5
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 6
    label "pa&#322;acowo"
    origin "text"
  ]
  node [
    id 7
    label "parkowy"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "dawno"
    origin "text"
  ]
  node [
    id 10
    label "siedziba"
    origin "text"
  ]
  node [
    id 11
    label "jeden"
    origin "text"
  ]
  node [
    id 12
    label "ga&#322;&#261;&#378;"
    origin "text"
  ]
  node [
    id 13
    label "hrabiowski"
    origin "text"
  ]
  node [
    id 14
    label "rod"
    origin "text"
  ]
  node [
    id 15
    label "plater"
    origin "text"
  ]
  node [
    id 16
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 17
    label "wywodzi&#263;"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;"
    origin "text"
  ]
  node [
    id 19
    label "metr"
    origin "text"
  ]
  node [
    id 20
    label "emilia"
    origin "text"
  ]
  node [
    id 21
    label "teren"
    origin "text"
  ]
  node [
    id 22
    label "pa&#322;acowy"
    origin "text"
  ]
  node [
    id 23
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 24
    label "odnowi&#263;"
    origin "text"
  ]
  node [
    id 25
    label "lato"
    origin "text"
  ]
  node [
    id 26
    label "obecnie"
    origin "text"
  ]
  node [
    id 27
    label "mie&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 28
    label "tam"
    origin "text"
  ]
  node [
    id 29
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 30
    label "szkoleniowy"
    origin "text"
  ]
  node [
    id 31
    label "zus"
    origin "text"
  ]
  node [
    id 32
    label "dost&#281;pny"
    origin "text"
  ]
  node [
    id 33
    label "dla"
    origin "text"
  ]
  node [
    id 34
    label "zwiedzaj&#261;ca"
    origin "text"
  ]
  node [
    id 35
    label "kolejny"
  ]
  node [
    id 36
    label "inaczej"
  ]
  node [
    id 37
    label "r&#243;&#380;ny"
  ]
  node [
    id 38
    label "inszy"
  ]
  node [
    id 39
    label "osobno"
  ]
  node [
    id 40
    label "&#347;wiadectwo"
  ]
  node [
    id 41
    label "przedmiot"
  ]
  node [
    id 42
    label "starzyzna"
  ]
  node [
    id 43
    label "keepsake"
  ]
  node [
    id 44
    label "trza"
  ]
  node [
    id 45
    label "uczestniczy&#263;"
  ]
  node [
    id 46
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 47
    label "para"
  ]
  node [
    id 48
    label "necessity"
  ]
  node [
    id 49
    label "klasyczny"
  ]
  node [
    id 50
    label "klasycystycznie"
  ]
  node [
    id 51
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 52
    label "whole"
  ]
  node [
    id 53
    label "odm&#322;adza&#263;"
  ]
  node [
    id 54
    label "zabudowania"
  ]
  node [
    id 55
    label "odm&#322;odzenie"
  ]
  node [
    id 56
    label "zespolik"
  ]
  node [
    id 57
    label "skupienie"
  ]
  node [
    id 58
    label "schorzenie"
  ]
  node [
    id 59
    label "grupa"
  ]
  node [
    id 60
    label "Depeche_Mode"
  ]
  node [
    id 61
    label "Mazowsze"
  ]
  node [
    id 62
    label "ro&#347;lina"
  ]
  node [
    id 63
    label "zbi&#243;r"
  ]
  node [
    id 64
    label "The_Beatles"
  ]
  node [
    id 65
    label "group"
  ]
  node [
    id 66
    label "&#346;wietliki"
  ]
  node [
    id 67
    label "odm&#322;adzanie"
  ]
  node [
    id 68
    label "batch"
  ]
  node [
    id 69
    label "cie&#263;"
  ]
  node [
    id 70
    label "si&#281;ga&#263;"
  ]
  node [
    id 71
    label "trwa&#263;"
  ]
  node [
    id 72
    label "obecno&#347;&#263;"
  ]
  node [
    id 73
    label "stan"
  ]
  node [
    id 74
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 75
    label "stand"
  ]
  node [
    id 76
    label "mie&#263;_miejsce"
  ]
  node [
    id 77
    label "chodzi&#263;"
  ]
  node [
    id 78
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 79
    label "equal"
  ]
  node [
    id 80
    label "dawny"
  ]
  node [
    id 81
    label "ongi&#347;"
  ]
  node [
    id 82
    label "dawnie"
  ]
  node [
    id 83
    label "wcze&#347;niej"
  ]
  node [
    id 84
    label "d&#322;ugotrwale"
  ]
  node [
    id 85
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 86
    label "Kreml"
  ]
  node [
    id 87
    label "miejsce"
  ]
  node [
    id 88
    label "budynek"
  ]
  node [
    id 89
    label "Bia&#322;y_Dom"
  ]
  node [
    id 90
    label "&#321;ubianka"
  ]
  node [
    id 91
    label "sadowisko"
  ]
  node [
    id 92
    label "dzia&#322;_personalny"
  ]
  node [
    id 93
    label "miejsce_pracy"
  ]
  node [
    id 94
    label "kieliszek"
  ]
  node [
    id 95
    label "shot"
  ]
  node [
    id 96
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 97
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 98
    label "jaki&#347;"
  ]
  node [
    id 99
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 100
    label "jednolicie"
  ]
  node [
    id 101
    label "w&#243;dka"
  ]
  node [
    id 102
    label "ten"
  ]
  node [
    id 103
    label "ujednolicenie"
  ]
  node [
    id 104
    label "jednakowy"
  ]
  node [
    id 105
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 106
    label "zakres"
  ]
  node [
    id 107
    label "linia"
  ]
  node [
    id 108
    label "krzew"
  ]
  node [
    id 109
    label "p&#322;awina"
  ]
  node [
    id 110
    label "poddzia&#322;"
  ]
  node [
    id 111
    label "bezdro&#380;e"
  ]
  node [
    id 112
    label "oki&#347;&#263;"
  ]
  node [
    id 113
    label "korona"
  ]
  node [
    id 114
    label "drzewko"
  ]
  node [
    id 115
    label "p&#281;d"
  ]
  node [
    id 116
    label "odnoga"
  ]
  node [
    id 117
    label "sfera"
  ]
  node [
    id 118
    label "oskrzele"
  ]
  node [
    id 119
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 120
    label "platynowiec"
  ]
  node [
    id 121
    label "rhesus_factor"
  ]
  node [
    id 122
    label "kobaltowiec"
  ]
  node [
    id 123
    label "sztuciec"
  ]
  node [
    id 124
    label "pow&#322;oka"
  ]
  node [
    id 125
    label "uzasadnia&#263;"
  ]
  node [
    id 126
    label "wodzi&#263;"
  ]
  node [
    id 127
    label "wnioskowa&#263;"
  ]
  node [
    id 128
    label "zabiera&#263;"
  ]
  node [
    id 129
    label "stwierdza&#263;"
  ]
  node [
    id 130
    label "gaworzy&#263;"
  ]
  node [
    id 131
    label "chant"
  ]
  node [
    id 132
    label "powodowa&#263;"
  ]
  node [
    id 133
    label "condescend"
  ]
  node [
    id 134
    label "meter"
  ]
  node [
    id 135
    label "decymetr"
  ]
  node [
    id 136
    label "megabyte"
  ]
  node [
    id 137
    label "plon"
  ]
  node [
    id 138
    label "metrum"
  ]
  node [
    id 139
    label "dekametr"
  ]
  node [
    id 140
    label "jednostka_powierzchni"
  ]
  node [
    id 141
    label "uk&#322;ad_SI"
  ]
  node [
    id 142
    label "literaturoznawstwo"
  ]
  node [
    id 143
    label "wiersz"
  ]
  node [
    id 144
    label "gigametr"
  ]
  node [
    id 145
    label "miara"
  ]
  node [
    id 146
    label "nauczyciel"
  ]
  node [
    id 147
    label "kilometr_kwadratowy"
  ]
  node [
    id 148
    label "jednostka_metryczna"
  ]
  node [
    id 149
    label "jednostka_masy"
  ]
  node [
    id 150
    label "centymetr_kwadratowy"
  ]
  node [
    id 151
    label "kontekst"
  ]
  node [
    id 152
    label "wymiar"
  ]
  node [
    id 153
    label "obszar"
  ]
  node [
    id 154
    label "krajobraz"
  ]
  node [
    id 155
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 156
    label "w&#322;adza"
  ]
  node [
    id 157
    label "nation"
  ]
  node [
    id 158
    label "przyroda"
  ]
  node [
    id 159
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 160
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 161
    label "proceed"
  ]
  node [
    id 162
    label "catch"
  ]
  node [
    id 163
    label "pozosta&#263;"
  ]
  node [
    id 164
    label "osta&#263;_si&#281;"
  ]
  node [
    id 165
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 166
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 167
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 168
    label "change"
  ]
  node [
    id 169
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 170
    label "sum_up"
  ]
  node [
    id 171
    label "ulepszy&#263;"
  ]
  node [
    id 172
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 173
    label "pora_roku"
  ]
  node [
    id 174
    label "ninie"
  ]
  node [
    id 175
    label "aktualny"
  ]
  node [
    id 176
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 177
    label "mie&#263;"
  ]
  node [
    id 178
    label "m&#243;c"
  ]
  node [
    id 179
    label "zawiera&#263;"
  ]
  node [
    id 180
    label "fold"
  ]
  node [
    id 181
    label "lock"
  ]
  node [
    id 182
    label "tu"
  ]
  node [
    id 183
    label "Hollywood"
  ]
  node [
    id 184
    label "zal&#261;&#380;ek"
  ]
  node [
    id 185
    label "otoczenie"
  ]
  node [
    id 186
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 187
    label "&#347;rodek"
  ]
  node [
    id 188
    label "center"
  ]
  node [
    id 189
    label "instytucja"
  ]
  node [
    id 190
    label "skupisko"
  ]
  node [
    id 191
    label "warunki"
  ]
  node [
    id 192
    label "naukowy"
  ]
  node [
    id 193
    label "sk&#322;adka"
  ]
  node [
    id 194
    label "&#322;atwy"
  ]
  node [
    id 195
    label "mo&#380;liwy"
  ]
  node [
    id 196
    label "dost&#281;pnie"
  ]
  node [
    id 197
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 198
    label "przyst&#281;pnie"
  ]
  node [
    id 199
    label "zrozumia&#322;y"
  ]
  node [
    id 200
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 201
    label "odblokowanie_si&#281;"
  ]
  node [
    id 202
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 203
    label "Emilia"
  ]
  node [
    id 204
    label "gr&#261;d"
  ]
  node [
    id 205
    label "osuchowski"
  ]
  node [
    id 206
    label "Jan"
  ]
  node [
    id 207
    label "d&#322;ugosz"
  ]
  node [
    id 208
    label "W&#322;adys&#322;awa"
  ]
  node [
    id 209
    label "Jagie&#322;&#322;o"
  ]
  node [
    id 210
    label "Feliksa"
  ]
  node [
    id 211
    label "wo&#322;owski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 30
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 27
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 27
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 19
    target 135
  ]
  edge [
    source 19
    target 136
  ]
  edge [
    source 19
    target 137
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 106
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 93
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 161
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 23
    target 164
  ]
  edge [
    source 23
    target 165
  ]
  edge [
    source 23
    target 166
  ]
  edge [
    source 23
    target 167
  ]
  edge [
    source 23
    target 168
  ]
  edge [
    source 23
    target 169
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 170
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 174
  ]
  edge [
    source 26
    target 175
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 27
    target 177
  ]
  edge [
    source 27
    target 178
  ]
  edge [
    source 27
    target 179
  ]
  edge [
    source 27
    target 180
  ]
  edge [
    source 27
    target 181
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 182
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 87
  ]
  edge [
    source 29
    target 183
  ]
  edge [
    source 29
    target 184
  ]
  edge [
    source 29
    target 185
  ]
  edge [
    source 29
    target 186
  ]
  edge [
    source 29
    target 187
  ]
  edge [
    source 29
    target 188
  ]
  edge [
    source 29
    target 189
  ]
  edge [
    source 29
    target 190
  ]
  edge [
    source 29
    target 191
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 192
  ]
  edge [
    source 31
    target 193
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 194
  ]
  edge [
    source 32
    target 195
  ]
  edge [
    source 32
    target 196
  ]
  edge [
    source 32
    target 197
  ]
  edge [
    source 32
    target 198
  ]
  edge [
    source 32
    target 199
  ]
  edge [
    source 32
    target 200
  ]
  edge [
    source 32
    target 201
  ]
  edge [
    source 32
    target 202
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 204
    target 205
  ]
  edge [
    source 206
    target 207
  ]
  edge [
    source 208
    target 209
  ]
  edge [
    source 210
    target 211
  ]
]
