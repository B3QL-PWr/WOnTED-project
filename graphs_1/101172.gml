graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.951219512195122
  density 0.04878048780487805
  graphCliqueNumber 3
  node [
    id 0
    label "teatr"
    origin "text"
  ]
  node [
    id 1
    label "wojna"
    origin "text"
  ]
  node [
    id 2
    label "przedstawienie"
  ]
  node [
    id 3
    label "deski"
  ]
  node [
    id 4
    label "przedstawia&#263;"
  ]
  node [
    id 5
    label "gra"
  ]
  node [
    id 6
    label "modelatornia"
  ]
  node [
    id 7
    label "widzownia"
  ]
  node [
    id 8
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 9
    label "budynek"
  ]
  node [
    id 10
    label "literatura"
  ]
  node [
    id 11
    label "teren"
  ]
  node [
    id 12
    label "przedstawianie"
  ]
  node [
    id 13
    label "antyteatr"
  ]
  node [
    id 14
    label "dekoratornia"
  ]
  node [
    id 15
    label "instytucja"
  ]
  node [
    id 16
    label "sala"
  ]
  node [
    id 17
    label "play"
  ]
  node [
    id 18
    label "sztuka"
  ]
  node [
    id 19
    label "zimna_wojna"
  ]
  node [
    id 20
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 21
    label "angaria"
  ]
  node [
    id 22
    label "wr&#243;g"
  ]
  node [
    id 23
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 24
    label "walka"
  ]
  node [
    id 25
    label "war"
  ]
  node [
    id 26
    label "konflikt"
  ]
  node [
    id 27
    label "wojna_stuletnia"
  ]
  node [
    id 28
    label "burza"
  ]
  node [
    id 29
    label "zbrodnia_wojenna"
  ]
  node [
    id 30
    label "gra_w_karty"
  ]
  node [
    id 31
    label "sp&#243;r"
  ]
  node [
    id 32
    label "ii"
  ]
  node [
    id 33
    label "&#347;wiatowy"
  ]
  node [
    id 34
    label "ocean"
  ]
  node [
    id 35
    label "atlantycki"
  ]
  node [
    id 36
    label "spokojny"
  ]
  node [
    id 37
    label "Azja"
  ]
  node [
    id 38
    label "dalekowschodni"
  ]
  node [
    id 39
    label "europejski"
  ]
  node [
    id 40
    label "P&#243;&#322;nocnoafryka&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 37
    target 38
  ]
]
