graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "prawdziwy"
    origin "text"
  ]
  node [
    id 1
    label "plaga"
    origin "text"
  ]
  node [
    id 2
    label "szczery"
  ]
  node [
    id 3
    label "naprawd&#281;"
  ]
  node [
    id 4
    label "zgodny"
  ]
  node [
    id 5
    label "naturalny"
  ]
  node [
    id 6
    label "realnie"
  ]
  node [
    id 7
    label "prawdziwie"
  ]
  node [
    id 8
    label "m&#261;dry"
  ]
  node [
    id 9
    label "&#380;ywny"
  ]
  node [
    id 10
    label "podobny"
  ]
  node [
    id 11
    label "wydarzenie"
  ]
  node [
    id 12
    label "trouble"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
]
