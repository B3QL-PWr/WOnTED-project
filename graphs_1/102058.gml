graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.0701754385964914
  density 0.01217750257997936
  graphCliqueNumber 2
  node [
    id 0
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 1
    label "serwis"
    origin "text"
  ]
  node [
    id 2
    label "mam"
    origin "text"
  ]
  node [
    id 3
    label "media"
    origin "text"
  ]
  node [
    id 4
    label "ukaza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "jura"
    origin "text"
  ]
  node [
    id 6
    label "qmam"
    origin "text"
  ]
  node [
    id 7
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 8
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 9
    label "dobry"
    origin "text"
  ]
  node [
    id 10
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 11
    label "lista"
    origin "text"
  ]
  node [
    id 12
    label "laureat"
    origin "text"
  ]
  node [
    id 13
    label "dwa"
    origin "text"
  ]
  node [
    id 14
    label "kategoria"
    origin "text"
  ]
  node [
    id 15
    label "cykliczny"
    origin "text"
  ]
  node [
    id 16
    label "warsztatowy"
    origin "text"
  ]
  node [
    id 17
    label "krzyk"
    origin "text"
  ]
  node [
    id 18
    label "qlt"
    origin "text"
  ]
  node [
    id 19
    label "mazak"
    origin "text"
  ]
  node [
    id 20
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 21
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 22
    label "blondyn"
    origin "text"
  ]
  node [
    id 23
    label "nasz"
    origin "text"
  ]
  node [
    id 24
    label "mix"
    origin "text"
  ]
  node [
    id 25
    label "gratulowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "Nowy_Rok"
  ]
  node [
    id 27
    label "miesi&#261;c"
  ]
  node [
    id 28
    label "mecz"
  ]
  node [
    id 29
    label "service"
  ]
  node [
    id 30
    label "wytw&#243;r"
  ]
  node [
    id 31
    label "zak&#322;ad"
  ]
  node [
    id 32
    label "us&#322;uga"
  ]
  node [
    id 33
    label "uderzenie"
  ]
  node [
    id 34
    label "doniesienie"
  ]
  node [
    id 35
    label "zastawa"
  ]
  node [
    id 36
    label "YouTube"
  ]
  node [
    id 37
    label "punkt"
  ]
  node [
    id 38
    label "porcja"
  ]
  node [
    id 39
    label "strona"
  ]
  node [
    id 40
    label "przekazior"
  ]
  node [
    id 41
    label "mass-media"
  ]
  node [
    id 42
    label "uzbrajanie"
  ]
  node [
    id 43
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 44
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 45
    label "medium"
  ]
  node [
    id 46
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 47
    label "pokaza&#263;"
  ]
  node [
    id 48
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 49
    label "unwrap"
  ]
  node [
    id 50
    label "formacja_geologiczna"
  ]
  node [
    id 51
    label "jura_&#347;rodkowa"
  ]
  node [
    id 52
    label "dogger"
  ]
  node [
    id 53
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 54
    label "era_mezozoiczna"
  ]
  node [
    id 55
    label "plezjozaur"
  ]
  node [
    id 56
    label "euoplocefal"
  ]
  node [
    id 57
    label "jura_wczesna"
  ]
  node [
    id 58
    label "przesta&#263;"
  ]
  node [
    id 59
    label "zrobi&#263;"
  ]
  node [
    id 60
    label "cause"
  ]
  node [
    id 61
    label "communicate"
  ]
  node [
    id 62
    label "czynno&#347;&#263;"
  ]
  node [
    id 63
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 64
    label "decyzja"
  ]
  node [
    id 65
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 66
    label "pick"
  ]
  node [
    id 67
    label "pomy&#347;lny"
  ]
  node [
    id 68
    label "skuteczny"
  ]
  node [
    id 69
    label "moralny"
  ]
  node [
    id 70
    label "korzystny"
  ]
  node [
    id 71
    label "odpowiedni"
  ]
  node [
    id 72
    label "zwrot"
  ]
  node [
    id 73
    label "dobrze"
  ]
  node [
    id 74
    label "pozytywny"
  ]
  node [
    id 75
    label "grzeczny"
  ]
  node [
    id 76
    label "powitanie"
  ]
  node [
    id 77
    label "mi&#322;y"
  ]
  node [
    id 78
    label "dobroczynny"
  ]
  node [
    id 79
    label "pos&#322;uszny"
  ]
  node [
    id 80
    label "ca&#322;y"
  ]
  node [
    id 81
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 82
    label "czw&#243;rka"
  ]
  node [
    id 83
    label "spokojny"
  ]
  node [
    id 84
    label "&#347;mieszny"
  ]
  node [
    id 85
    label "drogi"
  ]
  node [
    id 86
    label "przedstawienie"
  ]
  node [
    id 87
    label "pokazywa&#263;"
  ]
  node [
    id 88
    label "zapoznawa&#263;"
  ]
  node [
    id 89
    label "typify"
  ]
  node [
    id 90
    label "opisywa&#263;"
  ]
  node [
    id 91
    label "teatr"
  ]
  node [
    id 92
    label "podawa&#263;"
  ]
  node [
    id 93
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 94
    label "demonstrowa&#263;"
  ]
  node [
    id 95
    label "represent"
  ]
  node [
    id 96
    label "ukazywa&#263;"
  ]
  node [
    id 97
    label "attest"
  ]
  node [
    id 98
    label "exhibit"
  ]
  node [
    id 99
    label "stanowi&#263;"
  ]
  node [
    id 100
    label "zg&#322;asza&#263;"
  ]
  node [
    id 101
    label "display"
  ]
  node [
    id 102
    label "wyliczanka"
  ]
  node [
    id 103
    label "catalog"
  ]
  node [
    id 104
    label "stock"
  ]
  node [
    id 105
    label "figurowa&#263;"
  ]
  node [
    id 106
    label "zbi&#243;r"
  ]
  node [
    id 107
    label "book"
  ]
  node [
    id 108
    label "pozycja"
  ]
  node [
    id 109
    label "tekst"
  ]
  node [
    id 110
    label "sumariusz"
  ]
  node [
    id 111
    label "zdobywca"
  ]
  node [
    id 112
    label "forma"
  ]
  node [
    id 113
    label "type"
  ]
  node [
    id 114
    label "teoria"
  ]
  node [
    id 115
    label "poj&#281;cie"
  ]
  node [
    id 116
    label "klasa"
  ]
  node [
    id 117
    label "tyrocydyna"
  ]
  node [
    id 118
    label "zwi&#261;zek_cykliczny"
  ]
  node [
    id 119
    label "zwi&#261;zek_heterocykliczny"
  ]
  node [
    id 120
    label "cyklicznie"
  ]
  node [
    id 121
    label "regularny"
  ]
  node [
    id 122
    label "technicznie"
  ]
  node [
    id 123
    label "mowa"
  ]
  node [
    id 124
    label "g&#322;os"
  ]
  node [
    id 125
    label "tusz"
  ]
  node [
    id 126
    label "artyku&#322;"
  ]
  node [
    id 127
    label "przybory_do_pisania"
  ]
  node [
    id 128
    label "sztyft"
  ]
  node [
    id 129
    label "Mickiewicz"
  ]
  node [
    id 130
    label "czas"
  ]
  node [
    id 131
    label "szkolenie"
  ]
  node [
    id 132
    label "przepisa&#263;"
  ]
  node [
    id 133
    label "lesson"
  ]
  node [
    id 134
    label "grupa"
  ]
  node [
    id 135
    label "praktyka"
  ]
  node [
    id 136
    label "metoda"
  ]
  node [
    id 137
    label "niepokalanki"
  ]
  node [
    id 138
    label "kara"
  ]
  node [
    id 139
    label "zda&#263;"
  ]
  node [
    id 140
    label "form"
  ]
  node [
    id 141
    label "kwalifikacje"
  ]
  node [
    id 142
    label "system"
  ]
  node [
    id 143
    label "przepisanie"
  ]
  node [
    id 144
    label "sztuba"
  ]
  node [
    id 145
    label "wiedza"
  ]
  node [
    id 146
    label "stopek"
  ]
  node [
    id 147
    label "school"
  ]
  node [
    id 148
    label "absolwent"
  ]
  node [
    id 149
    label "urszulanki"
  ]
  node [
    id 150
    label "gabinet"
  ]
  node [
    id 151
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 152
    label "ideologia"
  ]
  node [
    id 153
    label "lekcja"
  ]
  node [
    id 154
    label "muzyka"
  ]
  node [
    id 155
    label "podr&#281;cznik"
  ]
  node [
    id 156
    label "zdanie"
  ]
  node [
    id 157
    label "siedziba"
  ]
  node [
    id 158
    label "sekretariat"
  ]
  node [
    id 159
    label "nauka"
  ]
  node [
    id 160
    label "do&#347;wiadczenie"
  ]
  node [
    id 161
    label "tablica"
  ]
  node [
    id 162
    label "teren_szko&#322;y"
  ]
  node [
    id 163
    label "instytucja"
  ]
  node [
    id 164
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 165
    label "skolaryzacja"
  ]
  node [
    id 166
    label "&#322;awa_szkolna"
  ]
  node [
    id 167
    label "cz&#322;owiek"
  ]
  node [
    id 168
    label "czyj&#347;"
  ]
  node [
    id 169
    label "compliment"
  ]
  node [
    id 170
    label "kierowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 30
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 168
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 170
  ]
]
