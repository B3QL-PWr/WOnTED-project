graph [
  maxDegree 6
  minDegree 1
  meanDegree 2.347826086956522
  density 0.1067193675889328
  graphCliqueNumber 5
  node [
    id 0
    label "wicemarsza&#322;ek"
    origin "text"
  ]
  node [
    id 1
    label "krzysztof"
    origin "text"
  ]
  node [
    id 2
    label "putra"
    origin "text"
  ]
  node [
    id 3
    label "marsza&#322;ek"
  ]
  node [
    id 4
    label "zast&#281;pca"
  ]
  node [
    id 5
    label "Krzysztofa"
  ]
  node [
    id 6
    label "Putra"
  ]
  node [
    id 7
    label "Czes&#322;awa"
  ]
  node [
    id 8
    label "Hoca"
  ]
  node [
    id 9
    label "prawo"
  ]
  node [
    id 10
    label "i"
  ]
  node [
    id 11
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 12
    label "&#321;ukasz"
  ]
  node [
    id 13
    label "Zbonikowski"
  ]
  node [
    id 14
    label "hoc"
  ]
  node [
    id 15
    label "mi&#281;dzynarodowy"
  ]
  node [
    id 16
    label "komisja"
  ]
  node [
    id 17
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 18
    label "morze"
  ]
  node [
    id 19
    label "ba&#322;tycki"
  ]
  node [
    id 20
    label "rada"
  ]
  node [
    id 21
    label "badanie"
  ]
  node [
    id 22
    label "europejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 20
    target 21
  ]
]
