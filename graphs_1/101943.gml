graph [
  maxDegree 9
  minDegree 1
  meanDegree 2.6666666666666665
  density 0.08333333333333333
  graphCliqueNumber 5
  node [
    id 0
    label "art"
    origin "text"
  ]
  node [
    id 1
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "brzmienie"
    origin "text"
  ]
  node [
    id 3
    label "wytwarza&#263;"
  ]
  node [
    id 4
    label "take"
  ]
  node [
    id 5
    label "dostawa&#263;"
  ]
  node [
    id 6
    label "return"
  ]
  node [
    id 7
    label "wyra&#380;anie"
  ]
  node [
    id 8
    label "cecha"
  ]
  node [
    id 9
    label "sound"
  ]
  node [
    id 10
    label "tone"
  ]
  node [
    id 11
    label "kolorystyka"
  ]
  node [
    id 12
    label "rejestr"
  ]
  node [
    id 13
    label "spirit"
  ]
  node [
    id 14
    label "wydawanie"
  ]
  node [
    id 15
    label "generalny"
  ]
  node [
    id 16
    label "inspektor"
  ]
  node [
    id 17
    label "ochrona"
  ]
  node [
    id 18
    label "da&#263;"
  ]
  node [
    id 19
    label "osobowy"
  ]
  node [
    id 20
    label "europejski"
  ]
  node [
    id 21
    label "parlament"
  ]
  node [
    id 22
    label "system"
  ]
  node [
    id 23
    label "informacyjny"
  ]
  node [
    id 24
    label "Schengen"
  ]
  node [
    id 25
    label "wizowy"
  ]
  node [
    id 26
    label "systema"
  ]
  node [
    id 27
    label "krajowy"
  ]
  node [
    id 28
    label "informatyczny"
  ]
  node [
    id 29
    label "rzeczpospolita"
  ]
  node [
    id 30
    label "polski"
  ]
  node [
    id 31
    label "urz&#261;d"
  ]
  node [
    id 32
    label "policja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 31
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
]
