graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.0476190476190474
  density 0.01638095238095238
  graphCliqueNumber 3
  node [
    id 0
    label "spad&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "cena"
    origin "text"
  ]
  node [
    id 2
    label "akcja"
    origin "text"
  ]
  node [
    id 3
    label "google"
    origin "text"
  ]
  node [
    id 4
    label "microsoft"
    origin "text"
  ]
  node [
    id 5
    label "rozmin&#261;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "prognoza"
    origin "text"
  ]
  node [
    id 8
    label "rzekomo"
    origin "text"
  ]
  node [
    id 9
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 10
    label "nadmierny"
    origin "text"
  ]
  node [
    id 11
    label "wzrost"
    origin "text"
  ]
  node [
    id 12
    label "zatrudnienie"
    origin "text"
  ]
  node [
    id 13
    label "radzi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "siebie"
    origin "text"
  ]
  node [
    id 15
    label "dobrze"
    origin "text"
  ]
  node [
    id 16
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 17
    label "zapowiedzie&#263;"
    origin "text"
  ]
  node [
    id 18
    label "ale"
    origin "text"
  ]
  node [
    id 19
    label "pop&#322;yn&#261;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "razem"
    origin "text"
  ]
  node [
    id 21
    label "googlem"
    origin "text"
  ]
  node [
    id 22
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 23
    label "warto&#347;&#263;"
  ]
  node [
    id 24
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 25
    label "dyskryminacja_cenowa"
  ]
  node [
    id 26
    label "inflacja"
  ]
  node [
    id 27
    label "kupowanie"
  ]
  node [
    id 28
    label "kosztowa&#263;"
  ]
  node [
    id 29
    label "kosztowanie"
  ]
  node [
    id 30
    label "worth"
  ]
  node [
    id 31
    label "wyceni&#263;"
  ]
  node [
    id 32
    label "wycenienie"
  ]
  node [
    id 33
    label "zagrywka"
  ]
  node [
    id 34
    label "czyn"
  ]
  node [
    id 35
    label "czynno&#347;&#263;"
  ]
  node [
    id 36
    label "wysoko&#347;&#263;"
  ]
  node [
    id 37
    label "stock"
  ]
  node [
    id 38
    label "gra"
  ]
  node [
    id 39
    label "w&#281;ze&#322;"
  ]
  node [
    id 40
    label "instrument_strunowy"
  ]
  node [
    id 41
    label "dywidenda"
  ]
  node [
    id 42
    label "przebieg"
  ]
  node [
    id 43
    label "udzia&#322;"
  ]
  node [
    id 44
    label "occupation"
  ]
  node [
    id 45
    label "jazda"
  ]
  node [
    id 46
    label "wydarzenie"
  ]
  node [
    id 47
    label "commotion"
  ]
  node [
    id 48
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 49
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 50
    label "operacja"
  ]
  node [
    id 51
    label "diagnoza"
  ]
  node [
    id 52
    label "perspektywa"
  ]
  node [
    id 53
    label "przewidywanie"
  ]
  node [
    id 54
    label "pozornie"
  ]
  node [
    id 55
    label "apparently"
  ]
  node [
    id 56
    label "strona"
  ]
  node [
    id 57
    label "przyczyna"
  ]
  node [
    id 58
    label "matuszka"
  ]
  node [
    id 59
    label "geneza"
  ]
  node [
    id 60
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 61
    label "czynnik"
  ]
  node [
    id 62
    label "poci&#261;ganie"
  ]
  node [
    id 63
    label "rezultat"
  ]
  node [
    id 64
    label "uprz&#261;&#380;"
  ]
  node [
    id 65
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 66
    label "subject"
  ]
  node [
    id 67
    label "nadmiernie"
  ]
  node [
    id 68
    label "wegetacja"
  ]
  node [
    id 69
    label "increase"
  ]
  node [
    id 70
    label "rozw&#243;j"
  ]
  node [
    id 71
    label "zmiana"
  ]
  node [
    id 72
    label "wzi&#281;cie"
  ]
  node [
    id 73
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 74
    label "stosunek_pracy"
  ]
  node [
    id 75
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 76
    label "zatrudni&#263;"
  ]
  node [
    id 77
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 78
    label "agregat_ekonomiczny"
  ]
  node [
    id 79
    label "function"
  ]
  node [
    id 80
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 81
    label "pracowanie"
  ]
  node [
    id 82
    label "najem"
  ]
  node [
    id 83
    label "zaw&#243;d"
  ]
  node [
    id 84
    label "kierownictwo"
  ]
  node [
    id 85
    label "pracowa&#263;"
  ]
  node [
    id 86
    label "zobowi&#261;zanie"
  ]
  node [
    id 87
    label "poda&#380;_pracy"
  ]
  node [
    id 88
    label "affair"
  ]
  node [
    id 89
    label "rede"
  ]
  node [
    id 90
    label "m&#243;wi&#263;"
  ]
  node [
    id 91
    label "udziela&#263;"
  ]
  node [
    id 92
    label "rozmawia&#263;"
  ]
  node [
    id 93
    label "argue"
  ]
  node [
    id 94
    label "moralnie"
  ]
  node [
    id 95
    label "wiele"
  ]
  node [
    id 96
    label "lepiej"
  ]
  node [
    id 97
    label "korzystnie"
  ]
  node [
    id 98
    label "pomy&#347;lnie"
  ]
  node [
    id 99
    label "pozytywnie"
  ]
  node [
    id 100
    label "dobry"
  ]
  node [
    id 101
    label "dobroczynnie"
  ]
  node [
    id 102
    label "odpowiednio"
  ]
  node [
    id 103
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 104
    label "skutecznie"
  ]
  node [
    id 105
    label "poziom"
  ]
  node [
    id 106
    label "faza"
  ]
  node [
    id 107
    label "depression"
  ]
  node [
    id 108
    label "zjawisko"
  ]
  node [
    id 109
    label "nizina"
  ]
  node [
    id 110
    label "announce"
  ]
  node [
    id 111
    label "spowodowa&#263;"
  ]
  node [
    id 112
    label "przestrzec"
  ]
  node [
    id 113
    label "sign"
  ]
  node [
    id 114
    label "poinformowa&#263;"
  ]
  node [
    id 115
    label "og&#322;osi&#263;"
  ]
  node [
    id 116
    label "piwo"
  ]
  node [
    id 117
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 118
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 119
    label "sail"
  ]
  node [
    id 120
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 121
    label "cruise"
  ]
  node [
    id 122
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 123
    label "odej&#347;&#263;"
  ]
  node [
    id 124
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 125
    label "&#322;&#261;cznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 125
  ]
]
