graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "mecz"
    origin "text"
  ]
  node [
    id 1
    label "obrona"
  ]
  node [
    id 2
    label "gra"
  ]
  node [
    id 3
    label "dwumecz"
  ]
  node [
    id 4
    label "game"
  ]
  node [
    id 5
    label "serw"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
]
