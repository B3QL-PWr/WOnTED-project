graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9130434782608696
  density 0.08695652173913043
  graphCliqueNumber 2
  node [
    id 0
    label "poznan"
    origin "text"
  ]
  node [
    id 1
    label "fajny"
    origin "text"
  ]
  node [
    id 2
    label "mat"
    origin "text"
  ]
  node [
    id 3
    label "lotnisko"
    origin "text"
  ]
  node [
    id 4
    label "fajnie"
  ]
  node [
    id 5
    label "dobry"
  ]
  node [
    id 6
    label "byczy"
  ]
  node [
    id 7
    label "klawy"
  ]
  node [
    id 8
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 9
    label "ruch"
  ]
  node [
    id 10
    label "podoficer_marynarki"
  ]
  node [
    id 11
    label "szach"
  ]
  node [
    id 12
    label "szachy"
  ]
  node [
    id 13
    label "hala"
  ]
  node [
    id 14
    label "droga_ko&#322;owania"
  ]
  node [
    id 15
    label "baza"
  ]
  node [
    id 16
    label "p&#322;yta_postojowa"
  ]
  node [
    id 17
    label "wie&#380;a_kontroli_lot&#243;w"
  ]
  node [
    id 18
    label "betonka"
  ]
  node [
    id 19
    label "budowla"
  ]
  node [
    id 20
    label "aerodrom"
  ]
  node [
    id 21
    label "pas_startowy"
  ]
  node [
    id 22
    label "terminal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
]
