graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.967741935483871
  density 0.03225806451612903
  graphCliqueNumber 2
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "skutecznie"
    origin "text"
  ]
  node [
    id 2
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "projekt"
    origin "text"
  ]
  node [
    id 4
    label "typ"
    origin "text"
  ]
  node [
    id 5
    label "byd&#322;o"
  ]
  node [
    id 6
    label "zobo"
  ]
  node [
    id 7
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 8
    label "yakalo"
  ]
  node [
    id 9
    label "dzo"
  ]
  node [
    id 10
    label "dobrze"
  ]
  node [
    id 11
    label "skuteczny"
  ]
  node [
    id 12
    label "control"
  ]
  node [
    id 13
    label "eksponowa&#263;"
  ]
  node [
    id 14
    label "kre&#347;li&#263;"
  ]
  node [
    id 15
    label "g&#243;rowa&#263;"
  ]
  node [
    id 16
    label "message"
  ]
  node [
    id 17
    label "partner"
  ]
  node [
    id 18
    label "string"
  ]
  node [
    id 19
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 20
    label "przesuwa&#263;"
  ]
  node [
    id 21
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 22
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 23
    label "powodowa&#263;"
  ]
  node [
    id 24
    label "kierowa&#263;"
  ]
  node [
    id 25
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 26
    label "robi&#263;"
  ]
  node [
    id 27
    label "manipulate"
  ]
  node [
    id 28
    label "&#380;y&#263;"
  ]
  node [
    id 29
    label "navigate"
  ]
  node [
    id 30
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 31
    label "ukierunkowywa&#263;"
  ]
  node [
    id 32
    label "linia_melodyczna"
  ]
  node [
    id 33
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 34
    label "prowadzenie"
  ]
  node [
    id 35
    label "tworzy&#263;"
  ]
  node [
    id 36
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 37
    label "sterowa&#263;"
  ]
  node [
    id 38
    label "krzywa"
  ]
  node [
    id 39
    label "dokument"
  ]
  node [
    id 40
    label "device"
  ]
  node [
    id 41
    label "program_u&#380;ytkowy"
  ]
  node [
    id 42
    label "intencja"
  ]
  node [
    id 43
    label "agreement"
  ]
  node [
    id 44
    label "pomys&#322;"
  ]
  node [
    id 45
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 46
    label "plan"
  ]
  node [
    id 47
    label "dokumentacja"
  ]
  node [
    id 48
    label "cz&#322;owiek"
  ]
  node [
    id 49
    label "gromada"
  ]
  node [
    id 50
    label "autorament"
  ]
  node [
    id 51
    label "przypuszczenie"
  ]
  node [
    id 52
    label "cynk"
  ]
  node [
    id 53
    label "rezultat"
  ]
  node [
    id 54
    label "jednostka_systematyczna"
  ]
  node [
    id 55
    label "kr&#243;lestwo"
  ]
  node [
    id 56
    label "obstawia&#263;"
  ]
  node [
    id 57
    label "design"
  ]
  node [
    id 58
    label "facet"
  ]
  node [
    id 59
    label "variety"
  ]
  node [
    id 60
    label "sztuka"
  ]
  node [
    id 61
    label "antycypacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
]
