graph [
  maxDegree 72
  minDegree 1
  meanDegree 2.118811881188119
  density 0.004203991827754204
  graphCliqueNumber 3
  node [
    id 0
    label "dodawa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "tutaj"
    origin "text"
  ]
  node [
    id 2
    label "para"
    origin "text"
  ]
  node [
    id 3
    label "daleki"
    origin "text"
  ]
  node [
    id 4
    label "informacja"
    origin "text"
  ]
  node [
    id 5
    label "blog"
    origin "text"
  ]
  node [
    id 6
    label "posta&#263;"
    origin "text"
  ]
  node [
    id 7
    label "&#322;atwy"
    origin "text"
  ]
  node [
    id 8
    label "czytanie"
    origin "text"
  ]
  node [
    id 9
    label "ankieta"
    origin "text"
  ]
  node [
    id 10
    label "pytanie"
    origin "text"
  ]
  node [
    id 11
    label "odpowied&#378;"
    origin "text"
  ]
  node [
    id 12
    label "kima"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "autorek"
    origin "text"
  ]
  node [
    id 15
    label "gdzie"
    origin "text"
  ]
  node [
    id 16
    label "mieszko"
    origin "text"
  ]
  node [
    id 17
    label "dlaczego"
    origin "text"
  ]
  node [
    id 18
    label "kto"
    origin "text"
  ]
  node [
    id 19
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 20
    label "mieszka&#263;"
    origin "text"
  ]
  node [
    id 21
    label "plejada"
    origin "text"
  ]
  node [
    id 22
    label "w&#322;&#243;czy&#263;"
    origin "text"
  ]
  node [
    id 23
    label "si&#281;"
    origin "text"
  ]
  node [
    id 24
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 25
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 26
    label "blisko"
    origin "text"
  ]
  node [
    id 27
    label "albo"
    origin "text"
  ]
  node [
    id 28
    label "daleko"
    origin "text"
  ]
  node [
    id 29
    label "zale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 30
    label "jak"
    origin "text"
  ]
  node [
    id 31
    label "praca"
    origin "text"
  ]
  node [
    id 32
    label "pogo&#324;"
    origin "text"
  ]
  node [
    id 33
    label "wolno"
    origin "text"
  ]
  node [
    id 34
    label "nim"
    origin "text"
  ]
  node [
    id 35
    label "jecha&#263;"
    origin "text"
  ]
  node [
    id 36
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 37
    label "zdrowie"
    origin "text"
  ]
  node [
    id 38
    label "lata"
    origin "text"
  ]
  node [
    id 39
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 40
    label "moja"
    origin "text"
  ]
  node [
    id 41
    label "raczej"
    origin "text"
  ]
  node [
    id 42
    label "sta&#322;a"
    origin "text"
  ]
  node [
    id 43
    label "miejsce"
    origin "text"
  ]
  node [
    id 44
    label "pobyt"
    origin "text"
  ]
  node [
    id 45
    label "poza"
    origin "text"
  ]
  node [
    id 46
    label "polska"
    origin "text"
  ]
  node [
    id 47
    label "tym"
    origin "text"
  ]
  node [
    id 48
    label "bardzo"
    origin "text"
  ]
  node [
    id 49
    label "ceni&#263;"
    origin "text"
  ]
  node [
    id 50
    label "ochrona"
    origin "text"
  ]
  node [
    id 51
    label "sfera"
    origin "text"
  ]
  node [
    id 52
    label "prywatny"
    origin "text"
  ]
  node [
    id 53
    label "czas"
    origin "text"
  ]
  node [
    id 54
    label "internetowo"
    origin "text"
  ]
  node [
    id 55
    label "multimedialny"
    origin "text"
  ]
  node [
    id 56
    label "prawie"
    origin "text"
  ]
  node [
    id 57
    label "niemo&#380;liwy"
    origin "text"
  ]
  node [
    id 58
    label "ale"
    origin "text"
  ]
  node [
    id 59
    label "mimo"
    origin "text"
  ]
  node [
    id 60
    label "wszystko"
    origin "text"
  ]
  node [
    id 61
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 62
    label "dawa&#263;"
  ]
  node [
    id 63
    label "uzupe&#322;nia&#263;"
  ]
  node [
    id 64
    label "bind"
  ]
  node [
    id 65
    label "suma"
  ]
  node [
    id 66
    label "liczy&#263;"
  ]
  node [
    id 67
    label "nadawa&#263;"
  ]
  node [
    id 68
    label "tam"
  ]
  node [
    id 69
    label "gaz_cieplarniany"
  ]
  node [
    id 70
    label "grupa"
  ]
  node [
    id 71
    label "smoke"
  ]
  node [
    id 72
    label "pair"
  ]
  node [
    id 73
    label "sztuka"
  ]
  node [
    id 74
    label "Albania"
  ]
  node [
    id 75
    label "dodatek"
  ]
  node [
    id 76
    label "odparowanie"
  ]
  node [
    id 77
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 78
    label "odparowywa&#263;"
  ]
  node [
    id 79
    label "nale&#380;e&#263;"
  ]
  node [
    id 80
    label "wyparowanie"
  ]
  node [
    id 81
    label "zesp&#243;&#322;"
  ]
  node [
    id 82
    label "parowanie"
  ]
  node [
    id 83
    label "damp"
  ]
  node [
    id 84
    label "odparowywanie"
  ]
  node [
    id 85
    label "poker"
  ]
  node [
    id 86
    label "moneta"
  ]
  node [
    id 87
    label "odparowa&#263;"
  ]
  node [
    id 88
    label "jednostka_monetarna"
  ]
  node [
    id 89
    label "uk&#322;ad"
  ]
  node [
    id 90
    label "gaz"
  ]
  node [
    id 91
    label "chodzi&#263;"
  ]
  node [
    id 92
    label "zbi&#243;r"
  ]
  node [
    id 93
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 94
    label "dawny"
  ]
  node [
    id 95
    label "du&#380;y"
  ]
  node [
    id 96
    label "s&#322;aby"
  ]
  node [
    id 97
    label "oddalony"
  ]
  node [
    id 98
    label "przysz&#322;y"
  ]
  node [
    id 99
    label "ogl&#281;dny"
  ]
  node [
    id 100
    label "r&#243;&#380;ny"
  ]
  node [
    id 101
    label "g&#322;&#281;boki"
  ]
  node [
    id 102
    label "odlegle"
  ]
  node [
    id 103
    label "nieobecny"
  ]
  node [
    id 104
    label "odleg&#322;y"
  ]
  node [
    id 105
    label "d&#322;ugi"
  ]
  node [
    id 106
    label "zwi&#261;zany"
  ]
  node [
    id 107
    label "obcy"
  ]
  node [
    id 108
    label "doj&#347;cie"
  ]
  node [
    id 109
    label "doj&#347;&#263;"
  ]
  node [
    id 110
    label "powzi&#261;&#263;"
  ]
  node [
    id 111
    label "wiedza"
  ]
  node [
    id 112
    label "sygna&#322;"
  ]
  node [
    id 113
    label "obiegni&#281;cie"
  ]
  node [
    id 114
    label "obieganie"
  ]
  node [
    id 115
    label "obiec"
  ]
  node [
    id 116
    label "dane"
  ]
  node [
    id 117
    label "obiega&#263;"
  ]
  node [
    id 118
    label "punkt"
  ]
  node [
    id 119
    label "publikacja"
  ]
  node [
    id 120
    label "powzi&#281;cie"
  ]
  node [
    id 121
    label "komcio"
  ]
  node [
    id 122
    label "strona"
  ]
  node [
    id 123
    label "blogosfera"
  ]
  node [
    id 124
    label "pami&#281;tnik"
  ]
  node [
    id 125
    label "cz&#322;owiek"
  ]
  node [
    id 126
    label "Aspazja"
  ]
  node [
    id 127
    label "charakterystyka"
  ]
  node [
    id 128
    label "punkt_widzenia"
  ]
  node [
    id 129
    label "poby&#263;"
  ]
  node [
    id 130
    label "kompleksja"
  ]
  node [
    id 131
    label "Osjan"
  ]
  node [
    id 132
    label "wytw&#243;r"
  ]
  node [
    id 133
    label "budowa"
  ]
  node [
    id 134
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 135
    label "formacja"
  ]
  node [
    id 136
    label "pozosta&#263;"
  ]
  node [
    id 137
    label "point"
  ]
  node [
    id 138
    label "zaistnie&#263;"
  ]
  node [
    id 139
    label "go&#347;&#263;"
  ]
  node [
    id 140
    label "cecha"
  ]
  node [
    id 141
    label "osobowo&#347;&#263;"
  ]
  node [
    id 142
    label "trim"
  ]
  node [
    id 143
    label "wygl&#261;d"
  ]
  node [
    id 144
    label "przedstawienie"
  ]
  node [
    id 145
    label "wytrzyma&#263;"
  ]
  node [
    id 146
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 147
    label "kto&#347;"
  ]
  node [
    id 148
    label "letki"
  ]
  node [
    id 149
    label "przyjemny"
  ]
  node [
    id 150
    label "snadny"
  ]
  node [
    id 151
    label "prosty"
  ]
  node [
    id 152
    label "&#322;atwo"
  ]
  node [
    id 153
    label "&#322;acny"
  ]
  node [
    id 154
    label "dysleksja"
  ]
  node [
    id 155
    label "wyczytywanie"
  ]
  node [
    id 156
    label "doczytywanie"
  ]
  node [
    id 157
    label "lektor"
  ]
  node [
    id 158
    label "przepowiadanie"
  ]
  node [
    id 159
    label "wczytywanie_si&#281;"
  ]
  node [
    id 160
    label "oczytywanie_si&#281;"
  ]
  node [
    id 161
    label "zaczytanie_si&#281;"
  ]
  node [
    id 162
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 163
    label "wczytywanie"
  ]
  node [
    id 164
    label "obrz&#261;dek"
  ]
  node [
    id 165
    label "czytywanie"
  ]
  node [
    id 166
    label "bycie_w_stanie"
  ]
  node [
    id 167
    label "pokazywanie"
  ]
  node [
    id 168
    label "poznawanie"
  ]
  node [
    id 169
    label "poczytanie"
  ]
  node [
    id 170
    label "Biblia"
  ]
  node [
    id 171
    label "reading"
  ]
  node [
    id 172
    label "recitation"
  ]
  node [
    id 173
    label "kafeteria"
  ]
  node [
    id 174
    label "formularz"
  ]
  node [
    id 175
    label "badanie"
  ]
  node [
    id 176
    label "sprawa"
  ]
  node [
    id 177
    label "zadanie"
  ]
  node [
    id 178
    label "wypowied&#378;"
  ]
  node [
    id 179
    label "problemat"
  ]
  node [
    id 180
    label "rozpytywanie"
  ]
  node [
    id 181
    label "sprawdzian"
  ]
  node [
    id 182
    label "przes&#322;uchiwanie"
  ]
  node [
    id 183
    label "wypytanie"
  ]
  node [
    id 184
    label "zwracanie_si&#281;"
  ]
  node [
    id 185
    label "wypowiedzenie"
  ]
  node [
    id 186
    label "wywo&#322;ywanie"
  ]
  node [
    id 187
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 188
    label "problematyka"
  ]
  node [
    id 189
    label "question"
  ]
  node [
    id 190
    label "sprawdzanie"
  ]
  node [
    id 191
    label "odpowiadanie"
  ]
  node [
    id 192
    label "survey"
  ]
  node [
    id 193
    label "odpowiada&#263;"
  ]
  node [
    id 194
    label "egzaminowanie"
  ]
  node [
    id 195
    label "dokument"
  ]
  node [
    id 196
    label "rozmowa"
  ]
  node [
    id 197
    label "reakcja"
  ]
  node [
    id 198
    label "wyj&#347;cie"
  ]
  node [
    id 199
    label "react"
  ]
  node [
    id 200
    label "respondent"
  ]
  node [
    id 201
    label "replica"
  ]
  node [
    id 202
    label "sen"
  ]
  node [
    id 203
    label "kszta&#322;townik"
  ]
  node [
    id 204
    label "si&#281;ga&#263;"
  ]
  node [
    id 205
    label "trwa&#263;"
  ]
  node [
    id 206
    label "obecno&#347;&#263;"
  ]
  node [
    id 207
    label "stan"
  ]
  node [
    id 208
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 209
    label "stand"
  ]
  node [
    id 210
    label "mie&#263;_miejsce"
  ]
  node [
    id 211
    label "uczestniczy&#263;"
  ]
  node [
    id 212
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 213
    label "equal"
  ]
  node [
    id 214
    label "cognizance"
  ]
  node [
    id 215
    label "zajmowa&#263;"
  ]
  node [
    id 216
    label "sta&#263;"
  ]
  node [
    id 217
    label "przebywa&#263;"
  ]
  node [
    id 218
    label "room"
  ]
  node [
    id 219
    label "panowa&#263;"
  ]
  node [
    id 220
    label "fall"
  ]
  node [
    id 221
    label "Elektra"
  ]
  node [
    id 222
    label "elita"
  ]
  node [
    id 223
    label "atlantyda"
  ]
  node [
    id 224
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 225
    label "obszar"
  ]
  node [
    id 226
    label "obiekt_naturalny"
  ]
  node [
    id 227
    label "przedmiot"
  ]
  node [
    id 228
    label "Stary_&#346;wiat"
  ]
  node [
    id 229
    label "stw&#243;r"
  ]
  node [
    id 230
    label "biosfera"
  ]
  node [
    id 231
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 232
    label "rzecz"
  ]
  node [
    id 233
    label "magnetosfera"
  ]
  node [
    id 234
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 235
    label "environment"
  ]
  node [
    id 236
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 237
    label "geosfera"
  ]
  node [
    id 238
    label "Nowy_&#346;wiat"
  ]
  node [
    id 239
    label "planeta"
  ]
  node [
    id 240
    label "przejmowa&#263;"
  ]
  node [
    id 241
    label "litosfera"
  ]
  node [
    id 242
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 243
    label "makrokosmos"
  ]
  node [
    id 244
    label "barysfera"
  ]
  node [
    id 245
    label "biota"
  ]
  node [
    id 246
    label "p&#243;&#322;noc"
  ]
  node [
    id 247
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 248
    label "fauna"
  ]
  node [
    id 249
    label "wszechstworzenie"
  ]
  node [
    id 250
    label "geotermia"
  ]
  node [
    id 251
    label "biegun"
  ]
  node [
    id 252
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 253
    label "ekosystem"
  ]
  node [
    id 254
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 255
    label "teren"
  ]
  node [
    id 256
    label "zjawisko"
  ]
  node [
    id 257
    label "p&#243;&#322;kula"
  ]
  node [
    id 258
    label "atmosfera"
  ]
  node [
    id 259
    label "mikrokosmos"
  ]
  node [
    id 260
    label "class"
  ]
  node [
    id 261
    label "po&#322;udnie"
  ]
  node [
    id 262
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 263
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 264
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 265
    label "przejmowanie"
  ]
  node [
    id 266
    label "przestrze&#324;"
  ]
  node [
    id 267
    label "asymilowanie_si&#281;"
  ]
  node [
    id 268
    label "przej&#261;&#263;"
  ]
  node [
    id 269
    label "ekosfera"
  ]
  node [
    id 270
    label "przyroda"
  ]
  node [
    id 271
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 272
    label "ciemna_materia"
  ]
  node [
    id 273
    label "geoida"
  ]
  node [
    id 274
    label "Wsch&#243;d"
  ]
  node [
    id 275
    label "populace"
  ]
  node [
    id 276
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 277
    label "huczek"
  ]
  node [
    id 278
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 279
    label "Ziemia"
  ]
  node [
    id 280
    label "universe"
  ]
  node [
    id 281
    label "ozonosfera"
  ]
  node [
    id 282
    label "rze&#378;ba"
  ]
  node [
    id 283
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 284
    label "zagranica"
  ]
  node [
    id 285
    label "hydrosfera"
  ]
  node [
    id 286
    label "woda"
  ]
  node [
    id 287
    label "kuchnia"
  ]
  node [
    id 288
    label "przej&#281;cie"
  ]
  node [
    id 289
    label "czarna_dziura"
  ]
  node [
    id 290
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 291
    label "morze"
  ]
  node [
    id 292
    label "pan_domu"
  ]
  node [
    id 293
    label "ch&#322;op"
  ]
  node [
    id 294
    label "ma&#322;&#380;onek"
  ]
  node [
    id 295
    label "stary"
  ]
  node [
    id 296
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 297
    label "&#347;lubny"
  ]
  node [
    id 298
    label "m&#243;j"
  ]
  node [
    id 299
    label "pan_i_w&#322;adca"
  ]
  node [
    id 300
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 301
    label "pan_m&#322;ody"
  ]
  node [
    id 302
    label "dok&#322;adnie"
  ]
  node [
    id 303
    label "bliski"
  ]
  node [
    id 304
    label "silnie"
  ]
  node [
    id 305
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 306
    label "dawno"
  ]
  node [
    id 307
    label "nisko"
  ]
  node [
    id 308
    label "nieobecnie"
  ]
  node [
    id 309
    label "het"
  ]
  node [
    id 310
    label "wysoko"
  ]
  node [
    id 311
    label "du&#380;o"
  ]
  node [
    id 312
    label "znacznie"
  ]
  node [
    id 313
    label "g&#322;&#281;boko"
  ]
  node [
    id 314
    label "potrzebowa&#263;"
  ]
  node [
    id 315
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 316
    label "lie"
  ]
  node [
    id 317
    label "byd&#322;o"
  ]
  node [
    id 318
    label "zobo"
  ]
  node [
    id 319
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 320
    label "yakalo"
  ]
  node [
    id 321
    label "dzo"
  ]
  node [
    id 322
    label "stosunek_pracy"
  ]
  node [
    id 323
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 324
    label "benedykty&#324;ski"
  ]
  node [
    id 325
    label "pracowanie"
  ]
  node [
    id 326
    label "zaw&#243;d"
  ]
  node [
    id 327
    label "kierownictwo"
  ]
  node [
    id 328
    label "zmiana"
  ]
  node [
    id 329
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 330
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 331
    label "tynkarski"
  ]
  node [
    id 332
    label "czynnik_produkcji"
  ]
  node [
    id 333
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 334
    label "zobowi&#261;zanie"
  ]
  node [
    id 335
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 336
    label "czynno&#347;&#263;"
  ]
  node [
    id 337
    label "tyrka"
  ]
  node [
    id 338
    label "pracowa&#263;"
  ]
  node [
    id 339
    label "siedziba"
  ]
  node [
    id 340
    label "poda&#380;_pracy"
  ]
  node [
    id 341
    label "zak&#322;ad"
  ]
  node [
    id 342
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 343
    label "najem"
  ]
  node [
    id 344
    label "bieg"
  ]
  node [
    id 345
    label "wolny"
  ]
  node [
    id 346
    label "lu&#378;no"
  ]
  node [
    id 347
    label "wolniej"
  ]
  node [
    id 348
    label "thinly"
  ]
  node [
    id 349
    label "swobodny"
  ]
  node [
    id 350
    label "wolnie"
  ]
  node [
    id 351
    label "niespiesznie"
  ]
  node [
    id 352
    label "lu&#378;ny"
  ]
  node [
    id 353
    label "free"
  ]
  node [
    id 354
    label "gra_planszowa"
  ]
  node [
    id 355
    label "proceed"
  ]
  node [
    id 356
    label "napada&#263;"
  ]
  node [
    id 357
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 358
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 359
    label "wykonywa&#263;"
  ]
  node [
    id 360
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 361
    label "czu&#263;"
  ]
  node [
    id 362
    label "overdrive"
  ]
  node [
    id 363
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 364
    label "ride"
  ]
  node [
    id 365
    label "korzysta&#263;"
  ]
  node [
    id 366
    label "go"
  ]
  node [
    id 367
    label "prowadzi&#263;"
  ]
  node [
    id 368
    label "continue"
  ]
  node [
    id 369
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 370
    label "drive"
  ]
  node [
    id 371
    label "kontynuowa&#263;"
  ]
  node [
    id 372
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 373
    label "odbywa&#263;"
  ]
  node [
    id 374
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 375
    label "carry"
  ]
  node [
    id 376
    label "przyczyna"
  ]
  node [
    id 377
    label "uwaga"
  ]
  node [
    id 378
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 379
    label "os&#322;abia&#263;"
  ]
  node [
    id 380
    label "niszczy&#263;"
  ]
  node [
    id 381
    label "zniszczy&#263;"
  ]
  node [
    id 382
    label "firmness"
  ]
  node [
    id 383
    label "kondycja"
  ]
  node [
    id 384
    label "zniszczenie"
  ]
  node [
    id 385
    label "rozsypanie_si&#281;"
  ]
  node [
    id 386
    label "os&#322;abi&#263;"
  ]
  node [
    id 387
    label "zdarcie"
  ]
  node [
    id 388
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 389
    label "zedrze&#263;"
  ]
  node [
    id 390
    label "niszczenie"
  ]
  node [
    id 391
    label "os&#322;abienie"
  ]
  node [
    id 392
    label "soundness"
  ]
  node [
    id 393
    label "os&#322;abianie"
  ]
  node [
    id 394
    label "summer"
  ]
  node [
    id 395
    label "wielko&#347;&#263;"
  ]
  node [
    id 396
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 397
    label "element"
  ]
  node [
    id 398
    label "constant"
  ]
  node [
    id 399
    label "cia&#322;o"
  ]
  node [
    id 400
    label "plac"
  ]
  node [
    id 401
    label "status"
  ]
  node [
    id 402
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 403
    label "chwila"
  ]
  node [
    id 404
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 405
    label "rz&#261;d"
  ]
  node [
    id 406
    label "location"
  ]
  node [
    id 407
    label "warunek_lokalowy"
  ]
  node [
    id 408
    label "mode"
  ]
  node [
    id 409
    label "gra"
  ]
  node [
    id 410
    label "przesada"
  ]
  node [
    id 411
    label "ustawienie"
  ]
  node [
    id 412
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 413
    label "w_chuj"
  ]
  node [
    id 414
    label "szanowa&#263;"
  ]
  node [
    id 415
    label "prize"
  ]
  node [
    id 416
    label "appreciate"
  ]
  node [
    id 417
    label "uznawa&#263;"
  ]
  node [
    id 418
    label "ustala&#263;"
  ]
  node [
    id 419
    label "chemical_bond"
  ]
  node [
    id 420
    label "tarcza"
  ]
  node [
    id 421
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 422
    label "obiekt"
  ]
  node [
    id 423
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 424
    label "borowiec"
  ]
  node [
    id 425
    label "obstawienie"
  ]
  node [
    id 426
    label "ubezpieczenie"
  ]
  node [
    id 427
    label "obstawia&#263;"
  ]
  node [
    id 428
    label "obstawianie"
  ]
  node [
    id 429
    label "transportacja"
  ]
  node [
    id 430
    label "zakres"
  ]
  node [
    id 431
    label "wymiar"
  ]
  node [
    id 432
    label "strefa"
  ]
  node [
    id 433
    label "powierzchnia"
  ]
  node [
    id 434
    label "kolur"
  ]
  node [
    id 435
    label "kula"
  ]
  node [
    id 436
    label "sector"
  ]
  node [
    id 437
    label "p&#243;&#322;sfera"
  ]
  node [
    id 438
    label "czyj&#347;"
  ]
  node [
    id 439
    label "nieformalny"
  ]
  node [
    id 440
    label "personalny"
  ]
  node [
    id 441
    label "w&#322;asny"
  ]
  node [
    id 442
    label "prywatnie"
  ]
  node [
    id 443
    label "niepubliczny"
  ]
  node [
    id 444
    label "czasokres"
  ]
  node [
    id 445
    label "trawienie"
  ]
  node [
    id 446
    label "kategoria_gramatyczna"
  ]
  node [
    id 447
    label "period"
  ]
  node [
    id 448
    label "odczyt"
  ]
  node [
    id 449
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 450
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 451
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 452
    label "poprzedzenie"
  ]
  node [
    id 453
    label "koniugacja"
  ]
  node [
    id 454
    label "dzieje"
  ]
  node [
    id 455
    label "poprzedzi&#263;"
  ]
  node [
    id 456
    label "przep&#322;ywanie"
  ]
  node [
    id 457
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 458
    label "odwlekanie_si&#281;"
  ]
  node [
    id 459
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 460
    label "Zeitgeist"
  ]
  node [
    id 461
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 462
    label "okres_czasu"
  ]
  node [
    id 463
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 464
    label "pochodzi&#263;"
  ]
  node [
    id 465
    label "schy&#322;ek"
  ]
  node [
    id 466
    label "czwarty_wymiar"
  ]
  node [
    id 467
    label "chronometria"
  ]
  node [
    id 468
    label "poprzedzanie"
  ]
  node [
    id 469
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 470
    label "pogoda"
  ]
  node [
    id 471
    label "zegar"
  ]
  node [
    id 472
    label "trawi&#263;"
  ]
  node [
    id 473
    label "pochodzenie"
  ]
  node [
    id 474
    label "poprzedza&#263;"
  ]
  node [
    id 475
    label "time_period"
  ]
  node [
    id 476
    label "rachuba_czasu"
  ]
  node [
    id 477
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 478
    label "czasoprzestrze&#324;"
  ]
  node [
    id 479
    label "laba"
  ]
  node [
    id 480
    label "elektronicznie"
  ]
  node [
    id 481
    label "internetowy"
  ]
  node [
    id 482
    label "multimedialnie"
  ]
  node [
    id 483
    label "niezno&#347;ny"
  ]
  node [
    id 484
    label "utrapiony"
  ]
  node [
    id 485
    label "niemo&#380;liwie"
  ]
  node [
    id 486
    label "piwo"
  ]
  node [
    id 487
    label "lock"
  ]
  node [
    id 488
    label "absolut"
  ]
  node [
    id 489
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 490
    label "feel"
  ]
  node [
    id 491
    label "try"
  ]
  node [
    id 492
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 493
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 494
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 495
    label "sprawdza&#263;"
  ]
  node [
    id 496
    label "stara&#263;_si&#281;"
  ]
  node [
    id 497
    label "kosztowa&#263;"
  ]
  node [
    id 498
    label "Creative"
  ]
  node [
    id 499
    label "Commons"
  ]
  node [
    id 500
    label "Google"
  ]
  node [
    id 501
    label "obraza"
  ]
  node [
    id 502
    label "kuchenka"
  ]
  node [
    id 503
    label "kuchenny"
  ]
  node [
    id 504
    label "bajka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 200
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 203
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 206
  ]
  edge [
    source 13
    target 207
  ]
  edge [
    source 13
    target 208
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 13
    target 210
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 212
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 52
  ]
  edge [
    source 19
    target 53
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 92
  ]
  edge [
    source 21
    target 223
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 24
    target 225
  ]
  edge [
    source 24
    target 226
  ]
  edge [
    source 24
    target 227
  ]
  edge [
    source 24
    target 228
  ]
  edge [
    source 24
    target 70
  ]
  edge [
    source 24
    target 229
  ]
  edge [
    source 24
    target 230
  ]
  edge [
    source 24
    target 231
  ]
  edge [
    source 24
    target 232
  ]
  edge [
    source 24
    target 233
  ]
  edge [
    source 24
    target 234
  ]
  edge [
    source 24
    target 235
  ]
  edge [
    source 24
    target 236
  ]
  edge [
    source 24
    target 237
  ]
  edge [
    source 24
    target 238
  ]
  edge [
    source 24
    target 239
  ]
  edge [
    source 24
    target 240
  ]
  edge [
    source 24
    target 241
  ]
  edge [
    source 24
    target 242
  ]
  edge [
    source 24
    target 243
  ]
  edge [
    source 24
    target 244
  ]
  edge [
    source 24
    target 245
  ]
  edge [
    source 24
    target 246
  ]
  edge [
    source 24
    target 247
  ]
  edge [
    source 24
    target 248
  ]
  edge [
    source 24
    target 249
  ]
  edge [
    source 24
    target 250
  ]
  edge [
    source 24
    target 251
  ]
  edge [
    source 24
    target 252
  ]
  edge [
    source 24
    target 253
  ]
  edge [
    source 24
    target 254
  ]
  edge [
    source 24
    target 255
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 24
    target 257
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 259
  ]
  edge [
    source 24
    target 260
  ]
  edge [
    source 24
    target 261
  ]
  edge [
    source 24
    target 262
  ]
  edge [
    source 24
    target 263
  ]
  edge [
    source 24
    target 264
  ]
  edge [
    source 24
    target 265
  ]
  edge [
    source 24
    target 266
  ]
  edge [
    source 24
    target 267
  ]
  edge [
    source 24
    target 268
  ]
  edge [
    source 24
    target 269
  ]
  edge [
    source 24
    target 270
  ]
  edge [
    source 24
    target 271
  ]
  edge [
    source 24
    target 272
  ]
  edge [
    source 24
    target 273
  ]
  edge [
    source 24
    target 274
  ]
  edge [
    source 24
    target 275
  ]
  edge [
    source 24
    target 276
  ]
  edge [
    source 24
    target 277
  ]
  edge [
    source 24
    target 278
  ]
  edge [
    source 24
    target 279
  ]
  edge [
    source 24
    target 280
  ]
  edge [
    source 24
    target 281
  ]
  edge [
    source 24
    target 282
  ]
  edge [
    source 24
    target 283
  ]
  edge [
    source 24
    target 284
  ]
  edge [
    source 24
    target 285
  ]
  edge [
    source 24
    target 286
  ]
  edge [
    source 24
    target 287
  ]
  edge [
    source 24
    target 288
  ]
  edge [
    source 24
    target 289
  ]
  edge [
    source 24
    target 290
  ]
  edge [
    source 24
    target 291
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 292
  ]
  edge [
    source 25
    target 125
  ]
  edge [
    source 25
    target 293
  ]
  edge [
    source 25
    target 294
  ]
  edge [
    source 25
    target 295
  ]
  edge [
    source 25
    target 296
  ]
  edge [
    source 25
    target 297
  ]
  edge [
    source 25
    target 298
  ]
  edge [
    source 25
    target 299
  ]
  edge [
    source 25
    target 300
  ]
  edge [
    source 25
    target 301
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 302
  ]
  edge [
    source 26
    target 303
  ]
  edge [
    source 26
    target 304
  ]
  edge [
    source 26
    target 305
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 306
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 28
    target 308
  ]
  edge [
    source 28
    target 309
  ]
  edge [
    source 28
    target 310
  ]
  edge [
    source 28
    target 311
  ]
  edge [
    source 28
    target 312
  ]
  edge [
    source 28
    target 313
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 314
  ]
  edge [
    source 29
    target 315
  ]
  edge [
    source 29
    target 316
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 317
  ]
  edge [
    source 30
    target 318
  ]
  edge [
    source 30
    target 319
  ]
  edge [
    source 30
    target 320
  ]
  edge [
    source 30
    target 321
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 322
  ]
  edge [
    source 31
    target 323
  ]
  edge [
    source 31
    target 324
  ]
  edge [
    source 31
    target 325
  ]
  edge [
    source 31
    target 326
  ]
  edge [
    source 31
    target 327
  ]
  edge [
    source 31
    target 328
  ]
  edge [
    source 31
    target 329
  ]
  edge [
    source 31
    target 132
  ]
  edge [
    source 31
    target 330
  ]
  edge [
    source 31
    target 331
  ]
  edge [
    source 31
    target 332
  ]
  edge [
    source 31
    target 333
  ]
  edge [
    source 31
    target 334
  ]
  edge [
    source 31
    target 335
  ]
  edge [
    source 31
    target 336
  ]
  edge [
    source 31
    target 337
  ]
  edge [
    source 31
    target 338
  ]
  edge [
    source 31
    target 339
  ]
  edge [
    source 31
    target 340
  ]
  edge [
    source 31
    target 43
  ]
  edge [
    source 31
    target 341
  ]
  edge [
    source 31
    target 342
  ]
  edge [
    source 31
    target 343
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 344
  ]
  edge [
    source 32
    target 70
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 345
  ]
  edge [
    source 33
    target 346
  ]
  edge [
    source 33
    target 347
  ]
  edge [
    source 33
    target 348
  ]
  edge [
    source 33
    target 349
  ]
  edge [
    source 33
    target 350
  ]
  edge [
    source 33
    target 351
  ]
  edge [
    source 33
    target 352
  ]
  edge [
    source 33
    target 353
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 354
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 355
  ]
  edge [
    source 35
    target 356
  ]
  edge [
    source 35
    target 357
  ]
  edge [
    source 35
    target 358
  ]
  edge [
    source 35
    target 359
  ]
  edge [
    source 35
    target 360
  ]
  edge [
    source 35
    target 361
  ]
  edge [
    source 35
    target 362
  ]
  edge [
    source 35
    target 363
  ]
  edge [
    source 35
    target 364
  ]
  edge [
    source 35
    target 365
  ]
  edge [
    source 35
    target 366
  ]
  edge [
    source 35
    target 367
  ]
  edge [
    source 35
    target 368
  ]
  edge [
    source 35
    target 369
  ]
  edge [
    source 35
    target 370
  ]
  edge [
    source 35
    target 371
  ]
  edge [
    source 35
    target 372
  ]
  edge [
    source 35
    target 373
  ]
  edge [
    source 35
    target 374
  ]
  edge [
    source 35
    target 375
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 376
  ]
  edge [
    source 36
    target 377
  ]
  edge [
    source 36
    target 378
  ]
  edge [
    source 36
    target 128
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 379
  ]
  edge [
    source 37
    target 380
  ]
  edge [
    source 37
    target 381
  ]
  edge [
    source 37
    target 207
  ]
  edge [
    source 37
    target 382
  ]
  edge [
    source 37
    target 383
  ]
  edge [
    source 37
    target 384
  ]
  edge [
    source 37
    target 385
  ]
  edge [
    source 37
    target 386
  ]
  edge [
    source 37
    target 140
  ]
  edge [
    source 37
    target 387
  ]
  edge [
    source 37
    target 388
  ]
  edge [
    source 37
    target 389
  ]
  edge [
    source 37
    target 390
  ]
  edge [
    source 37
    target 391
  ]
  edge [
    source 37
    target 392
  ]
  edge [
    source 37
    target 393
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 394
  ]
  edge [
    source 38
    target 53
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 395
  ]
  edge [
    source 42
    target 396
  ]
  edge [
    source 42
    target 397
  ]
  edge [
    source 42
    target 398
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 399
  ]
  edge [
    source 43
    target 400
  ]
  edge [
    source 43
    target 140
  ]
  edge [
    source 43
    target 377
  ]
  edge [
    source 43
    target 266
  ]
  edge [
    source 43
    target 401
  ]
  edge [
    source 43
    target 402
  ]
  edge [
    source 43
    target 403
  ]
  edge [
    source 43
    target 404
  ]
  edge [
    source 43
    target 405
  ]
  edge [
    source 43
    target 406
  ]
  edge [
    source 43
    target 407
  ]
  edge [
    source 44
    target 206
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 408
  ]
  edge [
    source 45
    target 409
  ]
  edge [
    source 45
    target 410
  ]
  edge [
    source 45
    target 411
  ]
  edge [
    source 45
    target 412
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 413
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 414
  ]
  edge [
    source 49
    target 415
  ]
  edge [
    source 49
    target 416
  ]
  edge [
    source 49
    target 417
  ]
  edge [
    source 49
    target 66
  ]
  edge [
    source 49
    target 418
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 419
  ]
  edge [
    source 50
    target 420
  ]
  edge [
    source 50
    target 421
  ]
  edge [
    source 50
    target 422
  ]
  edge [
    source 50
    target 423
  ]
  edge [
    source 50
    target 424
  ]
  edge [
    source 50
    target 425
  ]
  edge [
    source 50
    target 135
  ]
  edge [
    source 50
    target 426
  ]
  edge [
    source 50
    target 427
  ]
  edge [
    source 50
    target 428
  ]
  edge [
    source 50
    target 429
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 224
  ]
  edge [
    source 51
    target 430
  ]
  edge [
    source 51
    target 277
  ]
  edge [
    source 51
    target 431
  ]
  edge [
    source 51
    target 432
  ]
  edge [
    source 51
    target 70
  ]
  edge [
    source 51
    target 266
  ]
  edge [
    source 51
    target 252
  ]
  edge [
    source 51
    target 433
  ]
  edge [
    source 51
    target 434
  ]
  edge [
    source 51
    target 435
  ]
  edge [
    source 51
    target 436
  ]
  edge [
    source 51
    target 437
  ]
  edge [
    source 51
    target 257
  ]
  edge [
    source 51
    target 260
  ]
  edge [
    source 52
    target 438
  ]
  edge [
    source 52
    target 439
  ]
  edge [
    source 52
    target 440
  ]
  edge [
    source 52
    target 441
  ]
  edge [
    source 52
    target 442
  ]
  edge [
    source 52
    target 443
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 444
  ]
  edge [
    source 53
    target 445
  ]
  edge [
    source 53
    target 446
  ]
  edge [
    source 53
    target 447
  ]
  edge [
    source 53
    target 448
  ]
  edge [
    source 53
    target 449
  ]
  edge [
    source 53
    target 450
  ]
  edge [
    source 53
    target 403
  ]
  edge [
    source 53
    target 451
  ]
  edge [
    source 53
    target 452
  ]
  edge [
    source 53
    target 453
  ]
  edge [
    source 53
    target 454
  ]
  edge [
    source 53
    target 455
  ]
  edge [
    source 53
    target 456
  ]
  edge [
    source 53
    target 457
  ]
  edge [
    source 53
    target 458
  ]
  edge [
    source 53
    target 459
  ]
  edge [
    source 53
    target 460
  ]
  edge [
    source 53
    target 461
  ]
  edge [
    source 53
    target 462
  ]
  edge [
    source 53
    target 463
  ]
  edge [
    source 53
    target 464
  ]
  edge [
    source 53
    target 465
  ]
  edge [
    source 53
    target 466
  ]
  edge [
    source 53
    target 467
  ]
  edge [
    source 53
    target 468
  ]
  edge [
    source 53
    target 469
  ]
  edge [
    source 53
    target 470
  ]
  edge [
    source 53
    target 471
  ]
  edge [
    source 53
    target 472
  ]
  edge [
    source 53
    target 473
  ]
  edge [
    source 53
    target 474
  ]
  edge [
    source 53
    target 475
  ]
  edge [
    source 53
    target 476
  ]
  edge [
    source 53
    target 477
  ]
  edge [
    source 53
    target 478
  ]
  edge [
    source 53
    target 479
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 480
  ]
  edge [
    source 54
    target 481
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 482
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 483
  ]
  edge [
    source 57
    target 484
  ]
  edge [
    source 57
    target 485
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 486
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 487
  ]
  edge [
    source 60
    target 488
  ]
  edge [
    source 60
    target 252
  ]
  edge [
    source 61
    target 489
  ]
  edge [
    source 61
    target 490
  ]
  edge [
    source 61
    target 144
  ]
  edge [
    source 61
    target 491
  ]
  edge [
    source 61
    target 492
  ]
  edge [
    source 61
    target 493
  ]
  edge [
    source 61
    target 494
  ]
  edge [
    source 61
    target 495
  ]
  edge [
    source 61
    target 496
  ]
  edge [
    source 61
    target 497
  ]
  edge [
    source 481
    target 502
  ]
  edge [
    source 498
    target 499
  ]
  edge [
    source 500
    target 501
  ]
  edge [
    source 503
    target 504
  ]
]
