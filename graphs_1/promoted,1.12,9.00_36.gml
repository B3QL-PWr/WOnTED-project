graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.019607843137255
  density 0.019996117258784703
  graphCliqueNumber 2
  node [
    id 0
    label "traci&#263;"
    origin "text"
  ]
  node [
    id 1
    label "redford"
    origin "text"
  ]
  node [
    id 2
    label "mama"
    origin "text"
  ]
  node [
    id 3
    label "pi&#281;cioletni"
    origin "text"
  ]
  node [
    id 4
    label "abcde"
    origin "text"
  ]
  node [
    id 5
    label "wymawia&#263;"
    origin "text"
  ]
  node [
    id 6
    label "ksi&#281;ga_abdiasza"
    origin "text"
  ]
  node [
    id 7
    label "city"
    origin "text"
  ]
  node [
    id 8
    label "umie&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 9
    label "media"
    origin "text"
  ]
  node [
    id 10
    label "spo&#322;eczno&#347;ciowy"
    origin "text"
  ]
  node [
    id 11
    label "wpis"
    origin "text"
  ]
  node [
    id 12
    label "przykry"
    origin "text"
  ]
  node [
    id 13
    label "zaj&#347;cie"
    origin "text"
  ]
  node [
    id 14
    label "lotnisko"
    origin "text"
  ]
  node [
    id 15
    label "omija&#263;"
  ]
  node [
    id 16
    label "mie&#263;_miejsce"
  ]
  node [
    id 17
    label "forfeit"
  ]
  node [
    id 18
    label "przegrywa&#263;"
  ]
  node [
    id 19
    label "execute"
  ]
  node [
    id 20
    label "zabija&#263;"
  ]
  node [
    id 21
    label "wytraca&#263;"
  ]
  node [
    id 22
    label "pogarsza&#263;_si&#281;"
  ]
  node [
    id 23
    label "szasta&#263;"
  ]
  node [
    id 24
    label "appear"
  ]
  node [
    id 25
    label "matczysko"
  ]
  node [
    id 26
    label "macierz"
  ]
  node [
    id 27
    label "przodkini"
  ]
  node [
    id 28
    label "Matka_Boska"
  ]
  node [
    id 29
    label "macocha"
  ]
  node [
    id 30
    label "matka_zast&#281;pcza"
  ]
  node [
    id 31
    label "stara"
  ]
  node [
    id 32
    label "rodzice"
  ]
  node [
    id 33
    label "rodzic"
  ]
  node [
    id 34
    label "kilkuletni"
  ]
  node [
    id 35
    label "pami&#281;ta&#263;"
  ]
  node [
    id 36
    label "typify"
  ]
  node [
    id 37
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 38
    label "say"
  ]
  node [
    id 39
    label "zastrzega&#263;"
  ]
  node [
    id 40
    label "werbalizowa&#263;"
  ]
  node [
    id 41
    label "oskar&#380;a&#263;"
  ]
  node [
    id 42
    label "wydobywa&#263;"
  ]
  node [
    id 43
    label "express"
  ]
  node [
    id 44
    label "denounce"
  ]
  node [
    id 45
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 46
    label "zrobi&#263;"
  ]
  node [
    id 47
    label "okre&#347;li&#263;"
  ]
  node [
    id 48
    label "uplasowa&#263;"
  ]
  node [
    id 49
    label "umieszcza&#263;"
  ]
  node [
    id 50
    label "wpierniczy&#263;"
  ]
  node [
    id 51
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 52
    label "zmieni&#263;"
  ]
  node [
    id 53
    label "put"
  ]
  node [
    id 54
    label "set"
  ]
  node [
    id 55
    label "przekazior"
  ]
  node [
    id 56
    label "mass-media"
  ]
  node [
    id 57
    label "uzbrajanie"
  ]
  node [
    id 58
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 59
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 60
    label "medium"
  ]
  node [
    id 61
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 62
    label "medialny"
  ]
  node [
    id 63
    label "publiczny"
  ]
  node [
    id 64
    label "czynno&#347;&#263;"
  ]
  node [
    id 65
    label "entrance"
  ]
  node [
    id 66
    label "inscription"
  ]
  node [
    id 67
    label "akt"
  ]
  node [
    id 68
    label "op&#322;ata"
  ]
  node [
    id 69
    label "tekst"
  ]
  node [
    id 70
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 71
    label "dokuczliwy"
  ]
  node [
    id 72
    label "niemi&#322;y"
  ]
  node [
    id 73
    label "niegrzeczny"
  ]
  node [
    id 74
    label "przykro"
  ]
  node [
    id 75
    label "nieprzyjemny"
  ]
  node [
    id 76
    label "porobienie_si&#281;"
  ]
  node [
    id 77
    label "doj&#347;cie"
  ]
  node [
    id 78
    label "przestanie"
  ]
  node [
    id 79
    label "skrycie_si&#281;"
  ]
  node [
    id 80
    label "krajobraz"
  ]
  node [
    id 81
    label "podej&#347;cie"
  ]
  node [
    id 82
    label "zakrycie"
  ]
  node [
    id 83
    label "ploy"
  ]
  node [
    id 84
    label "przyobleczenie_si&#281;"
  ]
  node [
    id 85
    label "zaniesienie"
  ]
  node [
    id 86
    label "wydarzenie"
  ]
  node [
    id 87
    label "happening"
  ]
  node [
    id 88
    label "event"
  ]
  node [
    id 89
    label "stanie_si&#281;"
  ]
  node [
    id 90
    label "odwiedzenie"
  ]
  node [
    id 91
    label "hala"
  ]
  node [
    id 92
    label "droga_ko&#322;owania"
  ]
  node [
    id 93
    label "baza"
  ]
  node [
    id 94
    label "p&#322;yta_postojowa"
  ]
  node [
    id 95
    label "wie&#380;a_kontroli_lot&#243;w"
  ]
  node [
    id 96
    label "betonka"
  ]
  node [
    id 97
    label "budowla"
  ]
  node [
    id 98
    label "aerodrom"
  ]
  node [
    id 99
    label "pas_startowy"
  ]
  node [
    id 100
    label "terminal"
  ]
  node [
    id 101
    label "Redford"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
]
