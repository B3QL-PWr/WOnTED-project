graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "powiat"
    origin "text"
  ]
  node [
    id 1
    label "gmina"
  ]
  node [
    id 2
    label "jednostka_administracyjna"
  ]
  node [
    id 3
    label "wojew&#243;dztwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
]
