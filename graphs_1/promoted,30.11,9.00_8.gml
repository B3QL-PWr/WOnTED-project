graph [
  maxDegree 30
  minDegree 1
  meanDegree 2
  density 0.03636363636363636
  graphCliqueNumber 2
  node [
    id 0
    label "zestawienie"
    origin "text"
  ]
  node [
    id 1
    label "kilka"
    origin "text"
  ]
  node [
    id 2
    label "bardzo"
    origin "text"
  ]
  node [
    id 3
    label "trudny"
    origin "text"
  ]
  node [
    id 4
    label "utw&#243;r"
    origin "text"
  ]
  node [
    id 5
    label "wraz"
    origin "text"
  ]
  node [
    id 6
    label "wizualizacja"
    origin "text"
  ]
  node [
    id 7
    label "figurowa&#263;"
  ]
  node [
    id 8
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 9
    label "set"
  ]
  node [
    id 10
    label "tekst"
  ]
  node [
    id 11
    label "sumariusz"
  ]
  node [
    id 12
    label "obrot&#243;wka"
  ]
  node [
    id 13
    label "z&#322;o&#380;enie"
  ]
  node [
    id 14
    label "strata"
  ]
  node [
    id 15
    label "pozycja"
  ]
  node [
    id 16
    label "wyliczanka"
  ]
  node [
    id 17
    label "stock"
  ]
  node [
    id 18
    label "deficyt"
  ]
  node [
    id 19
    label "informacja"
  ]
  node [
    id 20
    label "zanalizowanie"
  ]
  node [
    id 21
    label "ustawienie"
  ]
  node [
    id 22
    label "przedstawienie"
  ]
  node [
    id 23
    label "sprawozdanie_finansowe"
  ]
  node [
    id 24
    label "catalog"
  ]
  node [
    id 25
    label "z&#322;&#261;czenie"
  ]
  node [
    id 26
    label "z&#322;amanie"
  ]
  node [
    id 27
    label "kompozycja"
  ]
  node [
    id 28
    label "wyra&#380;enie"
  ]
  node [
    id 29
    label "count"
  ]
  node [
    id 30
    label "zbi&#243;r"
  ]
  node [
    id 31
    label "comparison"
  ]
  node [
    id 32
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 33
    label "analiza"
  ]
  node [
    id 34
    label "composition"
  ]
  node [
    id 35
    label "book"
  ]
  node [
    id 36
    label "&#347;ledziowate"
  ]
  node [
    id 37
    label "ryba"
  ]
  node [
    id 38
    label "w_chuj"
  ]
  node [
    id 39
    label "wymagaj&#261;cy"
  ]
  node [
    id 40
    label "skomplikowany"
  ]
  node [
    id 41
    label "k&#322;opotliwy"
  ]
  node [
    id 42
    label "ci&#281;&#380;ko"
  ]
  node [
    id 43
    label "tre&#347;&#263;"
  ]
  node [
    id 44
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 45
    label "obrazowanie"
  ]
  node [
    id 46
    label "part"
  ]
  node [
    id 47
    label "organ"
  ]
  node [
    id 48
    label "komunikat"
  ]
  node [
    id 49
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 50
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 51
    label "element_anatomiczny"
  ]
  node [
    id 52
    label "obraz"
  ]
  node [
    id 53
    label "czynno&#347;&#263;"
  ]
  node [
    id 54
    label "proces"
  ]
  node [
    id 55
    label "reprezentacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
]
