graph [
  maxDegree 32
  minDegree 1
  meanDegree 2
  density 0.011299435028248588
  graphCliqueNumber 2
  node [
    id 0
    label "nowa"
    origin "text"
  ]
  node [
    id 1
    label "po&#322;&#261;czenie"
    origin "text"
  ]
  node [
    id 2
    label "trzy"
    origin "text"
  ]
  node [
    id 3
    label "lek"
    origin "text"
  ]
  node [
    id 4
    label "wspomagaj&#261;cy"
    origin "text"
  ]
  node [
    id 5
    label "uk&#322;ad"
    origin "text"
  ]
  node [
    id 6
    label "odporno&#347;ciowy"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "bezpieczny"
    origin "text"
  ]
  node [
    id 9
    label "zabija&#263;"
    origin "text"
  ]
  node [
    id 10
    label "te&#380;"
    origin "text"
  ]
  node [
    id 11
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "kom&#243;rka"
    origin "text"
  ]
  node [
    id 13
    label "nowotworowy"
    origin "text"
  ]
  node [
    id 14
    label "pacjent"
    origin "text"
  ]
  node [
    id 15
    label "nawraca&#263;"
    origin "text"
  ]
  node [
    id 16
    label "ch&#322;oniak"
    origin "text"
  ]
  node [
    id 17
    label "hodgkina"
    origin "text"
  ]
  node [
    id 18
    label "gwiazda"
  ]
  node [
    id 19
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 20
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 21
    label "mention"
  ]
  node [
    id 22
    label "zestawienie"
  ]
  node [
    id 23
    label "zgrzeina"
  ]
  node [
    id 24
    label "coalescence"
  ]
  node [
    id 25
    label "element"
  ]
  node [
    id 26
    label "rzucenie"
  ]
  node [
    id 27
    label "komunikacja"
  ]
  node [
    id 28
    label "akt_p&#322;ciowy"
  ]
  node [
    id 29
    label "umo&#380;liwienie"
  ]
  node [
    id 30
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 31
    label "phreaker"
  ]
  node [
    id 32
    label "pomy&#347;lenie"
  ]
  node [
    id 33
    label "zjednoczenie"
  ]
  node [
    id 34
    label "kontakt"
  ]
  node [
    id 35
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 36
    label "czynno&#347;&#263;"
  ]
  node [
    id 37
    label "stworzenie"
  ]
  node [
    id 38
    label "dressing"
  ]
  node [
    id 39
    label "zwi&#261;zany"
  ]
  node [
    id 40
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 41
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 42
    label "spowodowanie"
  ]
  node [
    id 43
    label "zespolenie"
  ]
  node [
    id 44
    label "billing"
  ]
  node [
    id 45
    label "port"
  ]
  node [
    id 46
    label "alliance"
  ]
  node [
    id 47
    label "joining"
  ]
  node [
    id 48
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 49
    label "naszprycowa&#263;"
  ]
  node [
    id 50
    label "Albania"
  ]
  node [
    id 51
    label "tonizowa&#263;"
  ]
  node [
    id 52
    label "medicine"
  ]
  node [
    id 53
    label "szprycowa&#263;"
  ]
  node [
    id 54
    label "przepisanie"
  ]
  node [
    id 55
    label "przepisa&#263;"
  ]
  node [
    id 56
    label "tonizowanie"
  ]
  node [
    id 57
    label "szprycowanie"
  ]
  node [
    id 58
    label "naszprycowanie"
  ]
  node [
    id 59
    label "jednostka_monetarna"
  ]
  node [
    id 60
    label "apteczka"
  ]
  node [
    id 61
    label "substancja"
  ]
  node [
    id 62
    label "spos&#243;b"
  ]
  node [
    id 63
    label "quindarka"
  ]
  node [
    id 64
    label "wspomagaj&#261;co"
  ]
  node [
    id 65
    label "sk&#322;ad"
  ]
  node [
    id 66
    label "zachowanie"
  ]
  node [
    id 67
    label "umowa"
  ]
  node [
    id 68
    label "podsystem"
  ]
  node [
    id 69
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 70
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 71
    label "system"
  ]
  node [
    id 72
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 73
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 74
    label "struktura"
  ]
  node [
    id 75
    label "wi&#281;&#378;"
  ]
  node [
    id 76
    label "zawarcie"
  ]
  node [
    id 77
    label "systemat"
  ]
  node [
    id 78
    label "usenet"
  ]
  node [
    id 79
    label "ONZ"
  ]
  node [
    id 80
    label "o&#347;"
  ]
  node [
    id 81
    label "organ"
  ]
  node [
    id 82
    label "przestawi&#263;"
  ]
  node [
    id 83
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 84
    label "traktat_wersalski"
  ]
  node [
    id 85
    label "rozprz&#261;c"
  ]
  node [
    id 86
    label "cybernetyk"
  ]
  node [
    id 87
    label "cia&#322;o"
  ]
  node [
    id 88
    label "zawrze&#263;"
  ]
  node [
    id 89
    label "konstelacja"
  ]
  node [
    id 90
    label "zbi&#243;r"
  ]
  node [
    id 91
    label "NATO"
  ]
  node [
    id 92
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 93
    label "treaty"
  ]
  node [
    id 94
    label "si&#281;ga&#263;"
  ]
  node [
    id 95
    label "trwa&#263;"
  ]
  node [
    id 96
    label "obecno&#347;&#263;"
  ]
  node [
    id 97
    label "stan"
  ]
  node [
    id 98
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 99
    label "stand"
  ]
  node [
    id 100
    label "mie&#263;_miejsce"
  ]
  node [
    id 101
    label "uczestniczy&#263;"
  ]
  node [
    id 102
    label "chodzi&#263;"
  ]
  node [
    id 103
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 104
    label "equal"
  ]
  node [
    id 105
    label "schronienie"
  ]
  node [
    id 106
    label "&#322;atwy"
  ]
  node [
    id 107
    label "bezpiecznie"
  ]
  node [
    id 108
    label "niszczy&#263;"
  ]
  node [
    id 109
    label "krzywdzi&#263;"
  ]
  node [
    id 110
    label "rzuca&#263;_na_kolana"
  ]
  node [
    id 111
    label "os&#322;ania&#263;"
  ]
  node [
    id 112
    label "kill"
  ]
  node [
    id 113
    label "beat"
  ]
  node [
    id 114
    label "zako&#324;cza&#263;"
  ]
  node [
    id 115
    label "dispatch"
  ]
  node [
    id 116
    label "morzy&#263;"
  ]
  node [
    id 117
    label "bi&#263;"
  ]
  node [
    id 118
    label "mordowa&#263;"
  ]
  node [
    id 119
    label "powodowa&#263;_&#347;mier&#263;"
  ]
  node [
    id 120
    label "karci&#263;"
  ]
  node [
    id 121
    label "zakrywa&#263;"
  ]
  node [
    id 122
    label "rozbraja&#263;"
  ]
  node [
    id 123
    label "przybija&#263;"
  ]
  node [
    id 124
    label "zwalcza&#263;"
  ]
  node [
    id 125
    label "majority"
  ]
  node [
    id 126
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 127
    label "pole"
  ]
  node [
    id 128
    label "telefon"
  ]
  node [
    id 129
    label "embrioblast"
  ]
  node [
    id 130
    label "obszar"
  ]
  node [
    id 131
    label "b&#322;ona_kom&#243;rkowa"
  ]
  node [
    id 132
    label "cell"
  ]
  node [
    id 133
    label "cytochemia"
  ]
  node [
    id 134
    label "pomieszczenie"
  ]
  node [
    id 135
    label "b&#322;ona_podstawna"
  ]
  node [
    id 136
    label "organellum"
  ]
  node [
    id 137
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 138
    label "wy&#347;wietlacz"
  ]
  node [
    id 139
    label "mikrosom"
  ]
  node [
    id 140
    label "burza"
  ]
  node [
    id 141
    label "filia"
  ]
  node [
    id 142
    label "cytoplazma"
  ]
  node [
    id 143
    label "hipoderma"
  ]
  node [
    id 144
    label "oddychanie_kom&#243;rkowe"
  ]
  node [
    id 145
    label "tkanka"
  ]
  node [
    id 146
    label "wakuom"
  ]
  node [
    id 147
    label "biomembrana"
  ]
  node [
    id 148
    label "plaster"
  ]
  node [
    id 149
    label "struktura_anatomiczna"
  ]
  node [
    id 150
    label "osocze_krwi"
  ]
  node [
    id 151
    label "genotyp"
  ]
  node [
    id 152
    label "urz&#261;dzenie"
  ]
  node [
    id 153
    label "p&#281;cherzyk"
  ]
  node [
    id 154
    label "tabela"
  ]
  node [
    id 155
    label "akantoliza"
  ]
  node [
    id 156
    label "nowotworowo"
  ]
  node [
    id 157
    label "chorobowy"
  ]
  node [
    id 158
    label "charakterystyczny"
  ]
  node [
    id 159
    label "patologiczny"
  ]
  node [
    id 160
    label "od&#322;&#261;czenie"
  ]
  node [
    id 161
    label "cz&#322;owiek"
  ]
  node [
    id 162
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 163
    label "od&#322;&#261;czanie"
  ]
  node [
    id 164
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 165
    label "klient"
  ]
  node [
    id 166
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 167
    label "szpitalnik"
  ]
  node [
    id 168
    label "przypadek"
  ]
  node [
    id 169
    label "chory"
  ]
  node [
    id 170
    label "piel&#281;gniarz"
  ]
  node [
    id 171
    label "pogl&#261;dy"
  ]
  node [
    id 172
    label "religia"
  ]
  node [
    id 173
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 174
    label "return"
  ]
  node [
    id 175
    label "nak&#322;ania&#263;"
  ]
  node [
    id 176
    label "wirus_ludzkiej_bia&#322;aczki_z_kom&#243;rek_T"
  ]
  node [
    id 177
    label "guz_z&#322;o&#347;liwy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
]
