graph [
  maxDegree 4
  minDegree 1
  meanDegree 2
  density 0.1
  graphCliqueNumber 3
  node [
    id 0
    label "mariusz"
    origin "text"
  ]
  node [
    id 1
    label "grabowski"
    origin "text"
  ]
  node [
    id 2
    label "Mariusz"
  ]
  node [
    id 3
    label "wydzia&#322;"
  ]
  node [
    id 4
    label "prawniczy"
  ]
  node [
    id 5
    label "katolicki"
  ]
  node [
    id 6
    label "uniwersytet"
  ]
  node [
    id 7
    label "lubelski"
  ]
  node [
    id 8
    label "zjednoczy&#263;"
  ]
  node [
    id 9
    label "chrze&#347;cija&#324;sko"
  ]
  node [
    id 10
    label "narodowy"
  ]
  node [
    id 11
    label "wyborczy"
  ]
  node [
    id 12
    label "akcja"
  ]
  node [
    id 13
    label "liga"
  ]
  node [
    id 14
    label "polskie"
  ]
  node [
    id 15
    label "rodzina"
  ]
  node [
    id 16
    label "solidarno&#347;&#263;"
  ]
  node [
    id 17
    label "porozumie&#263;"
  ]
  node [
    id 18
    label "katedra"
  ]
  node [
    id 19
    label "prawy"
  ]
  node [
    id 20
    label "cywilny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
]
