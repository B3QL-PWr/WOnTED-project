graph [
  maxDegree 35
  minDegree 1
  meanDegree 2.2829268292682925
  density 0.011190817790530846
  graphCliqueNumber 3
  node [
    id 0
    label "wiosna"
    origin "text"
  ]
  node [
    id 1
    label "firma"
    origin "text"
  ]
  node [
    id 2
    label "schumacher"
    origin "text"
  ]
  node [
    id 3
    label "zaplanowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "premiera"
    origin "text"
  ]
  node [
    id 5
    label "nowy"
    origin "text"
  ]
  node [
    id 6
    label "buggy"
    origin "text"
  ]
  node [
    id 7
    label "nazwa"
    origin "text"
  ]
  node [
    id 8
    label "cougar"
    origin "text"
  ]
  node [
    id 9
    label "model"
    origin "text"
  ]
  node [
    id 10
    label "wiadomo"
    origin "text"
  ]
  node [
    id 11
    label "raz"
    origin "text"
  ]
  node [
    id 12
    label "beda"
    origin "text"
  ]
  node [
    id 13
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 14
    label "konstrukcja"
    origin "text"
  ]
  node [
    id 15
    label "typ"
    origin "text"
  ]
  node [
    id 16
    label "mid"
    origin "text"
  ]
  node [
    id 17
    label "motor"
    origin "text"
  ]
  node [
    id 18
    label "silnik"
    origin "text"
  ]
  node [
    id 19
    label "by&#263;"
    origin "text"
  ]
  node [
    id 20
    label "umie&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 21
    label "przed"
    origin "text"
  ]
  node [
    id 22
    label "tylny"
    origin "text"
  ]
  node [
    id 23
    label "nowa"
    origin "text"
  ]
  node [
    id 24
    label "wersja"
    origin "text"
  ]
  node [
    id 25
    label "elektryczny"
    origin "text"
  ]
  node [
    id 26
    label "team"
    origin "text"
  ]
  node [
    id 27
    label "associated"
    origin "text"
  ]
  node [
    id 28
    label "nowalijka"
  ]
  node [
    id 29
    label "pora_roku"
  ]
  node [
    id 30
    label "przedn&#243;wek"
  ]
  node [
    id 31
    label "rok"
  ]
  node [
    id 32
    label "pocz&#261;tek"
  ]
  node [
    id 33
    label "zwiesna"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "MAC"
  ]
  node [
    id 36
    label "Hortex"
  ]
  node [
    id 37
    label "reengineering"
  ]
  node [
    id 38
    label "nazwa_w&#322;asna"
  ]
  node [
    id 39
    label "podmiot_gospodarczy"
  ]
  node [
    id 40
    label "Google"
  ]
  node [
    id 41
    label "zaufanie"
  ]
  node [
    id 42
    label "biurowiec"
  ]
  node [
    id 43
    label "interes"
  ]
  node [
    id 44
    label "zasoby_ludzkie"
  ]
  node [
    id 45
    label "networking"
  ]
  node [
    id 46
    label "paczkarnia"
  ]
  node [
    id 47
    label "Canon"
  ]
  node [
    id 48
    label "HP"
  ]
  node [
    id 49
    label "Baltona"
  ]
  node [
    id 50
    label "Pewex"
  ]
  node [
    id 51
    label "MAN_SE"
  ]
  node [
    id 52
    label "Apeks"
  ]
  node [
    id 53
    label "zasoby"
  ]
  node [
    id 54
    label "Orbis"
  ]
  node [
    id 55
    label "miejsce_pracy"
  ]
  node [
    id 56
    label "siedziba"
  ]
  node [
    id 57
    label "Spo&#322;em"
  ]
  node [
    id 58
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 59
    label "Orlen"
  ]
  node [
    id 60
    label "klasa"
  ]
  node [
    id 61
    label "pomy&#347;le&#263;"
  ]
  node [
    id 62
    label "opracowa&#263;"
  ]
  node [
    id 63
    label "line_up"
  ]
  node [
    id 64
    label "zrobi&#263;"
  ]
  node [
    id 65
    label "przemy&#347;le&#263;"
  ]
  node [
    id 66
    label "map"
  ]
  node [
    id 67
    label "przedstawienie"
  ]
  node [
    id 68
    label "premiere"
  ]
  node [
    id 69
    label "nowotny"
  ]
  node [
    id 70
    label "drugi"
  ]
  node [
    id 71
    label "kolejny"
  ]
  node [
    id 72
    label "bie&#380;&#261;cy"
  ]
  node [
    id 73
    label "nowo"
  ]
  node [
    id 74
    label "narybek"
  ]
  node [
    id 75
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 76
    label "obcy"
  ]
  node [
    id 77
    label "samoch&#243;d"
  ]
  node [
    id 78
    label "term"
  ]
  node [
    id 79
    label "wezwanie"
  ]
  node [
    id 80
    label "leksem"
  ]
  node [
    id 81
    label "patron"
  ]
  node [
    id 82
    label "pozowa&#263;"
  ]
  node [
    id 83
    label "ideal"
  ]
  node [
    id 84
    label "matryca"
  ]
  node [
    id 85
    label "imitacja"
  ]
  node [
    id 86
    label "ruch"
  ]
  node [
    id 87
    label "motif"
  ]
  node [
    id 88
    label "pozowanie"
  ]
  node [
    id 89
    label "wz&#243;r"
  ]
  node [
    id 90
    label "miniatura"
  ]
  node [
    id 91
    label "prezenter"
  ]
  node [
    id 92
    label "facet"
  ]
  node [
    id 93
    label "orygina&#322;"
  ]
  node [
    id 94
    label "mildew"
  ]
  node [
    id 95
    label "spos&#243;b"
  ]
  node [
    id 96
    label "zi&#243;&#322;ko"
  ]
  node [
    id 97
    label "adaptation"
  ]
  node [
    id 98
    label "chwila"
  ]
  node [
    id 99
    label "uderzenie"
  ]
  node [
    id 100
    label "cios"
  ]
  node [
    id 101
    label "time"
  ]
  node [
    id 102
    label "proszek"
  ]
  node [
    id 103
    label "wytw&#243;r"
  ]
  node [
    id 104
    label "struktura"
  ]
  node [
    id 105
    label "practice"
  ]
  node [
    id 106
    label "budowa"
  ]
  node [
    id 107
    label "rzecz"
  ]
  node [
    id 108
    label "wykre&#347;lanie"
  ]
  node [
    id 109
    label "element_konstrukcyjny"
  ]
  node [
    id 110
    label "gromada"
  ]
  node [
    id 111
    label "autorament"
  ]
  node [
    id 112
    label "przypuszczenie"
  ]
  node [
    id 113
    label "cynk"
  ]
  node [
    id 114
    label "rezultat"
  ]
  node [
    id 115
    label "jednostka_systematyczna"
  ]
  node [
    id 116
    label "kr&#243;lestwo"
  ]
  node [
    id 117
    label "obstawia&#263;"
  ]
  node [
    id 118
    label "design"
  ]
  node [
    id 119
    label "variety"
  ]
  node [
    id 120
    label "sztuka"
  ]
  node [
    id 121
    label "antycypacja"
  ]
  node [
    id 122
    label "kosz"
  ]
  node [
    id 123
    label "dotarcie"
  ]
  node [
    id 124
    label "wyci&#261;garka"
  ]
  node [
    id 125
    label "biblioteka"
  ]
  node [
    id 126
    label "aerosanie"
  ]
  node [
    id 127
    label "podgrzewacz"
  ]
  node [
    id 128
    label "bombowiec"
  ]
  node [
    id 129
    label "engine"
  ]
  node [
    id 130
    label "dociera&#263;"
  ]
  node [
    id 131
    label "gniazdo_zaworowe"
  ]
  node [
    id 132
    label "kierownica"
  ]
  node [
    id 133
    label "pojazd_drogowy"
  ]
  node [
    id 134
    label "nap&#281;d"
  ]
  node [
    id 135
    label "motor&#243;wka"
  ]
  node [
    id 136
    label "perpetuum_mobile"
  ]
  node [
    id 137
    label "dwuko&#322;owiec"
  ]
  node [
    id 138
    label "rz&#281;&#380;enie"
  ]
  node [
    id 139
    label "mechanizm"
  ]
  node [
    id 140
    label "czynnik"
  ]
  node [
    id 141
    label "gondola_silnikowa"
  ]
  node [
    id 142
    label "docieranie"
  ]
  node [
    id 143
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 144
    label "rz&#281;zi&#263;"
  ]
  node [
    id 145
    label "motoszybowiec"
  ]
  node [
    id 146
    label "motogodzina"
  ]
  node [
    id 147
    label "wiatrochron"
  ]
  node [
    id 148
    label "&#322;a&#324;cuch"
  ]
  node [
    id 149
    label "dotrze&#263;"
  ]
  node [
    id 150
    label "radiator"
  ]
  node [
    id 151
    label "program"
  ]
  node [
    id 152
    label "wirnik"
  ]
  node [
    id 153
    label "si&#281;ga&#263;"
  ]
  node [
    id 154
    label "trwa&#263;"
  ]
  node [
    id 155
    label "obecno&#347;&#263;"
  ]
  node [
    id 156
    label "stan"
  ]
  node [
    id 157
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 158
    label "stand"
  ]
  node [
    id 159
    label "mie&#263;_miejsce"
  ]
  node [
    id 160
    label "uczestniczy&#263;"
  ]
  node [
    id 161
    label "chodzi&#263;"
  ]
  node [
    id 162
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 163
    label "equal"
  ]
  node [
    id 164
    label "okre&#347;li&#263;"
  ]
  node [
    id 165
    label "uplasowa&#263;"
  ]
  node [
    id 166
    label "umieszcza&#263;"
  ]
  node [
    id 167
    label "wpierniczy&#263;"
  ]
  node [
    id 168
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 169
    label "zmieni&#263;"
  ]
  node [
    id 170
    label "put"
  ]
  node [
    id 171
    label "set"
  ]
  node [
    id 172
    label "gwiazda"
  ]
  node [
    id 173
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 174
    label "posta&#263;"
  ]
  node [
    id 175
    label "elektrycznie"
  ]
  node [
    id 176
    label "dublet"
  ]
  node [
    id 177
    label "zesp&#243;&#322;"
  ]
  node [
    id 178
    label "force"
  ]
  node [
    id 179
    label "Cougar"
  ]
  node [
    id 180
    label "SV"
  ]
  node [
    id 181
    label "Associated"
  ]
  node [
    id 182
    label "RC10B4"
  ]
  node [
    id 183
    label "1"
  ]
  node [
    id 184
    label "RC10T4"
  ]
  node [
    id 185
    label "XP"
  ]
  node [
    id 186
    label "SC450"
  ]
  node [
    id 187
    label "BL"
  ]
  node [
    id 188
    label "Kyosho"
  ]
  node [
    id 189
    label "Inferno"
  ]
  node [
    id 190
    label "MP9e"
  ]
  node [
    id 191
    label "MP9"
  ]
  node [
    id 192
    label "Hyper"
  ]
  node [
    id 193
    label "10sc"
  ]
  node [
    id 194
    label "reda"
  ]
  node [
    id 195
    label "RC"
  ]
  node [
    id 196
    label "mambo"
  ]
  node [
    id 197
    label "monstera"
  ]
  node [
    id 198
    label "Castle"
  ]
  node [
    id 199
    label "Creations"
  ]
  node [
    id 200
    label "Ultima"
  ]
  node [
    id 201
    label "SC"
  ]
  node [
    id 202
    label "Tokyo"
  ]
  node [
    id 203
    label "hobby"
  ]
  node [
    id 204
    label "show"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 34
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 24
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 77
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 77
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 64
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 23
    target 173
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 177
  ]
  edge [
    source 26
    target 178
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 179
    target 180
  ]
  edge [
    source 182
    target 183
  ]
  edge [
    source 183
    target 184
  ]
  edge [
    source 185
    target 186
  ]
  edge [
    source 185
    target 187
  ]
  edge [
    source 186
    target 187
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 188
    target 190
  ]
  edge [
    source 188
    target 191
  ]
  edge [
    source 188
    target 200
  ]
  edge [
    source 188
    target 201
  ]
  edge [
    source 189
    target 190
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 194
    target 195
  ]
  edge [
    source 196
    target 197
  ]
  edge [
    source 198
    target 199
  ]
  edge [
    source 200
    target 201
  ]
  edge [
    source 202
    target 203
  ]
  edge [
    source 202
    target 204
  ]
  edge [
    source 203
    target 204
  ]
]
