graph [
  maxDegree 220
  minDegree 1
  meanDegree 2.211167512690355
  density 0.0022471214559861333
  graphCliqueNumber 6
  node [
    id 0
    label "hiszpania"
    origin "text"
  ]
  node [
    id 1
    label "brytania"
    origin "text"
  ]
  node [
    id 2
    label "tak"
    origin "text"
  ]
  node [
    id 3
    label "nie"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 7
    label "podkomisja"
    origin "text"
  ]
  node [
    id 8
    label "komisja"
    origin "text"
  ]
  node [
    id 9
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "bardzo"
    origin "text"
  ]
  node [
    id 12
    label "merytoryczny"
    origin "text"
  ]
  node [
    id 13
    label "ale"
    origin "text"
  ]
  node [
    id 14
    label "d&#322;ugotrwa&#322;y"
    origin "text"
  ]
  node [
    id 15
    label "dyskusja"
    origin "text"
  ]
  node [
    id 16
    label "ten"
    origin "text"
  ]
  node [
    id 17
    label "temat"
    origin "text"
  ]
  node [
    id 18
    label "powodowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 20
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 21
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 22
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 23
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 25
    label "opinia"
    origin "text"
  ]
  node [
    id 26
    label "instytut"
    origin "text"
  ]
  node [
    id 27
    label "medycyna"
    origin "text"
  ]
  node [
    id 28
    label "praca"
    origin "text"
  ]
  node [
    id 29
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 30
    label "sosnowiec"
    origin "text"
  ]
  node [
    id 31
    label "rad"
    origin "text"
  ]
  node [
    id 32
    label "ochrona"
    origin "text"
  ]
  node [
    id 33
    label "przy"
    origin "text"
  ]
  node [
    id 34
    label "sejm"
    origin "text"
  ]
  node [
    id 35
    label "rzeczpospolita"
    origin "text"
  ]
  node [
    id 36
    label "polski"
    origin "text"
  ]
  node [
    id 37
    label "krajowy"
    origin "text"
  ]
  node [
    id 38
    label "centrum"
    origin "text"
  ]
  node [
    id 39
    label "radiologiczny"
    origin "text"
  ]
  node [
    id 40
    label "zdrowie"
    origin "text"
  ]
  node [
    id 41
    label "g&#322;&#243;wny"
    origin "text"
  ]
  node [
    id 42
    label "inspektorat"
    origin "text"
  ]
  node [
    id 43
    label "sanitarny"
    origin "text"
  ]
  node [
    id 44
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 45
    label "nigdzie"
    origin "text"
  ]
  node [
    id 46
    label "przekroczy&#263;"
    origin "text"
  ]
  node [
    id 47
    label "norma"
    origin "text"
  ]
  node [
    id 48
    label "dopuszczalny"
    origin "text"
  ]
  node [
    id 49
    label "praktycznie"
    origin "text"
  ]
  node [
    id 50
    label "rzecz"
    origin "text"
  ]
  node [
    id 51
    label "bra&#263;"
    origin "text"
  ]
  node [
    id 52
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 53
    label "zdefiniowa&#263;"
    origin "text"
  ]
  node [
    id 54
    label "prawie"
    origin "text"
  ]
  node [
    id 55
    label "unijny"
    origin "text"
  ]
  node [
    id 56
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 57
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 58
    label "zagro&#380;enie"
    origin "text"
  ]
  node [
    id 59
    label "jaki"
    origin "text"
  ]
  node [
    id 60
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 61
    label "rzeczywi&#347;cie"
    origin "text"
  ]
  node [
    id 62
    label "przesz&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 63
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 64
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 65
    label "choroba"
    origin "text"
  ]
  node [
    id 66
    label "zawodowy"
    origin "text"
  ]
  node [
    id 67
    label "skutek"
    origin "text"
  ]
  node [
    id 68
    label "promieniowanie"
    origin "text"
  ]
  node [
    id 69
    label "jonizowa&#263;"
    origin "text"
  ]
  node [
    id 70
    label "inspektor"
    origin "text"
  ]
  node [
    id 71
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 72
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 73
    label "czas"
    origin "text"
  ]
  node [
    id 74
    label "monitorowa&#263;"
    origin "text"
  ]
  node [
    id 75
    label "jako&#347;&#263;"
    origin "text"
  ]
  node [
    id 76
    label "aparatura"
    origin "text"
  ]
  node [
    id 77
    label "wcale"
    origin "text"
  ]
  node [
    id 78
    label "wiek"
    origin "text"
  ]
  node [
    id 79
    label "&#347;wiadczy&#263;"
    origin "text"
  ]
  node [
    id 80
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 81
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 82
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 83
    label "przekroczenie"
    origin "text"
  ]
  node [
    id 84
    label "tyle"
    origin "text"
  ]
  node [
    id 85
    label "tutaj"
    origin "text"
  ]
  node [
    id 86
    label "odnosi&#263;"
    origin "text"
  ]
  node [
    id 87
    label "merytorycznie"
    origin "text"
  ]
  node [
    id 88
    label "taka"
    origin "text"
  ]
  node [
    id 89
    label "nasz"
    origin "text"
  ]
  node [
    id 90
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 91
    label "oklask"
    origin "text"
  ]
  node [
    id 92
    label "sprzeciw"
  ]
  node [
    id 93
    label "si&#281;ga&#263;"
  ]
  node [
    id 94
    label "trwa&#263;"
  ]
  node [
    id 95
    label "obecno&#347;&#263;"
  ]
  node [
    id 96
    label "stan"
  ]
  node [
    id 97
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 98
    label "stand"
  ]
  node [
    id 99
    label "mie&#263;_miejsce"
  ]
  node [
    id 100
    label "uczestniczy&#263;"
  ]
  node [
    id 101
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 102
    label "equal"
  ]
  node [
    id 103
    label "du&#380;y"
  ]
  node [
    id 104
    label "cz&#281;sto"
  ]
  node [
    id 105
    label "mocno"
  ]
  node [
    id 106
    label "wiela"
  ]
  node [
    id 107
    label "subcommittee"
  ]
  node [
    id 108
    label "obrady"
  ]
  node [
    id 109
    label "zesp&#243;&#322;"
  ]
  node [
    id 110
    label "organ"
  ]
  node [
    id 111
    label "Komisja_Europejska"
  ]
  node [
    id 112
    label "reserve"
  ]
  node [
    id 113
    label "przej&#347;&#263;"
  ]
  node [
    id 114
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 115
    label "w_chuj"
  ]
  node [
    id 116
    label "rzeczowy"
  ]
  node [
    id 117
    label "piwo"
  ]
  node [
    id 118
    label "d&#322;ugotrwale"
  ]
  node [
    id 119
    label "d&#322;ugi"
  ]
  node [
    id 120
    label "rozmowa"
  ]
  node [
    id 121
    label "sympozjon"
  ]
  node [
    id 122
    label "conference"
  ]
  node [
    id 123
    label "okre&#347;lony"
  ]
  node [
    id 124
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 125
    label "fraza"
  ]
  node [
    id 126
    label "forma"
  ]
  node [
    id 127
    label "melodia"
  ]
  node [
    id 128
    label "zbacza&#263;"
  ]
  node [
    id 129
    label "entity"
  ]
  node [
    id 130
    label "omawia&#263;"
  ]
  node [
    id 131
    label "topik"
  ]
  node [
    id 132
    label "wyraz_pochodny"
  ]
  node [
    id 133
    label "om&#243;wi&#263;"
  ]
  node [
    id 134
    label "omawianie"
  ]
  node [
    id 135
    label "w&#261;tek"
  ]
  node [
    id 136
    label "forum"
  ]
  node [
    id 137
    label "cecha"
  ]
  node [
    id 138
    label "zboczenie"
  ]
  node [
    id 139
    label "zbaczanie"
  ]
  node [
    id 140
    label "tre&#347;&#263;"
  ]
  node [
    id 141
    label "tematyka"
  ]
  node [
    id 142
    label "sprawa"
  ]
  node [
    id 143
    label "istota"
  ]
  node [
    id 144
    label "otoczka"
  ]
  node [
    id 145
    label "zboczy&#263;"
  ]
  node [
    id 146
    label "om&#243;wienie"
  ]
  node [
    id 147
    label "motywowa&#263;"
  ]
  node [
    id 148
    label "act"
  ]
  node [
    id 149
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 150
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 151
    label "czu&#263;"
  ]
  node [
    id 152
    label "desire"
  ]
  node [
    id 153
    label "kcie&#263;"
  ]
  node [
    id 154
    label "zorganizowa&#263;"
  ]
  node [
    id 155
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 156
    label "przerobi&#263;"
  ]
  node [
    id 157
    label "wystylizowa&#263;"
  ]
  node [
    id 158
    label "cause"
  ]
  node [
    id 159
    label "wydali&#263;"
  ]
  node [
    id 160
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 161
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 162
    label "post&#261;pi&#263;"
  ]
  node [
    id 163
    label "appoint"
  ]
  node [
    id 164
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 165
    label "nabra&#263;"
  ]
  node [
    id 166
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 167
    label "make"
  ]
  node [
    id 168
    label "dawny"
  ]
  node [
    id 169
    label "rozw&#243;d"
  ]
  node [
    id 170
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 171
    label "eksprezydent"
  ]
  node [
    id 172
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 173
    label "partner"
  ]
  node [
    id 174
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 175
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 176
    label "wcze&#347;niejszy"
  ]
  node [
    id 177
    label "przedstawienie"
  ]
  node [
    id 178
    label "pokazywa&#263;"
  ]
  node [
    id 179
    label "zapoznawa&#263;"
  ]
  node [
    id 180
    label "typify"
  ]
  node [
    id 181
    label "opisywa&#263;"
  ]
  node [
    id 182
    label "teatr"
  ]
  node [
    id 183
    label "podawa&#263;"
  ]
  node [
    id 184
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 185
    label "demonstrowa&#263;"
  ]
  node [
    id 186
    label "represent"
  ]
  node [
    id 187
    label "ukazywa&#263;"
  ]
  node [
    id 188
    label "attest"
  ]
  node [
    id 189
    label "exhibit"
  ]
  node [
    id 190
    label "stanowi&#263;"
  ]
  node [
    id 191
    label "zg&#322;asza&#263;"
  ]
  node [
    id 192
    label "display"
  ]
  node [
    id 193
    label "zezwala&#263;"
  ]
  node [
    id 194
    label "ask"
  ]
  node [
    id 195
    label "invite"
  ]
  node [
    id 196
    label "zach&#281;ca&#263;"
  ]
  node [
    id 197
    label "preach"
  ]
  node [
    id 198
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 199
    label "pies"
  ]
  node [
    id 200
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 201
    label "poleca&#263;"
  ]
  node [
    id 202
    label "zaprasza&#263;"
  ]
  node [
    id 203
    label "suffice"
  ]
  node [
    id 204
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 205
    label "Filipiny"
  ]
  node [
    id 206
    label "Rwanda"
  ]
  node [
    id 207
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 208
    label "Monako"
  ]
  node [
    id 209
    label "Korea"
  ]
  node [
    id 210
    label "Ghana"
  ]
  node [
    id 211
    label "Czarnog&#243;ra"
  ]
  node [
    id 212
    label "Malawi"
  ]
  node [
    id 213
    label "Indonezja"
  ]
  node [
    id 214
    label "Bu&#322;garia"
  ]
  node [
    id 215
    label "Nauru"
  ]
  node [
    id 216
    label "Kenia"
  ]
  node [
    id 217
    label "Kambod&#380;a"
  ]
  node [
    id 218
    label "Mali"
  ]
  node [
    id 219
    label "Austria"
  ]
  node [
    id 220
    label "interior"
  ]
  node [
    id 221
    label "Armenia"
  ]
  node [
    id 222
    label "Fid&#380;i"
  ]
  node [
    id 223
    label "Tuwalu"
  ]
  node [
    id 224
    label "Etiopia"
  ]
  node [
    id 225
    label "Malta"
  ]
  node [
    id 226
    label "Malezja"
  ]
  node [
    id 227
    label "Grenada"
  ]
  node [
    id 228
    label "Tad&#380;ykistan"
  ]
  node [
    id 229
    label "Wehrlen"
  ]
  node [
    id 230
    label "para"
  ]
  node [
    id 231
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 232
    label "Rumunia"
  ]
  node [
    id 233
    label "Maroko"
  ]
  node [
    id 234
    label "Bhutan"
  ]
  node [
    id 235
    label "S&#322;owacja"
  ]
  node [
    id 236
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 237
    label "Seszele"
  ]
  node [
    id 238
    label "Kuwejt"
  ]
  node [
    id 239
    label "Arabia_Saudyjska"
  ]
  node [
    id 240
    label "Ekwador"
  ]
  node [
    id 241
    label "Kanada"
  ]
  node [
    id 242
    label "Japonia"
  ]
  node [
    id 243
    label "ziemia"
  ]
  node [
    id 244
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 245
    label "Hiszpania"
  ]
  node [
    id 246
    label "Wyspy_Marshalla"
  ]
  node [
    id 247
    label "Botswana"
  ]
  node [
    id 248
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 249
    label "D&#380;ibuti"
  ]
  node [
    id 250
    label "grupa"
  ]
  node [
    id 251
    label "Wietnam"
  ]
  node [
    id 252
    label "Egipt"
  ]
  node [
    id 253
    label "Burkina_Faso"
  ]
  node [
    id 254
    label "Niemcy"
  ]
  node [
    id 255
    label "Khitai"
  ]
  node [
    id 256
    label "Macedonia"
  ]
  node [
    id 257
    label "Albania"
  ]
  node [
    id 258
    label "Madagaskar"
  ]
  node [
    id 259
    label "Bahrajn"
  ]
  node [
    id 260
    label "Jemen"
  ]
  node [
    id 261
    label "Lesoto"
  ]
  node [
    id 262
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 263
    label "Samoa"
  ]
  node [
    id 264
    label "Andora"
  ]
  node [
    id 265
    label "Chiny"
  ]
  node [
    id 266
    label "Cypr"
  ]
  node [
    id 267
    label "Wielka_Brytania"
  ]
  node [
    id 268
    label "Ukraina"
  ]
  node [
    id 269
    label "Paragwaj"
  ]
  node [
    id 270
    label "Trynidad_i_Tobago"
  ]
  node [
    id 271
    label "Libia"
  ]
  node [
    id 272
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 273
    label "Surinam"
  ]
  node [
    id 274
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 275
    label "Australia"
  ]
  node [
    id 276
    label "Nigeria"
  ]
  node [
    id 277
    label "Honduras"
  ]
  node [
    id 278
    label "Bangladesz"
  ]
  node [
    id 279
    label "Peru"
  ]
  node [
    id 280
    label "Kazachstan"
  ]
  node [
    id 281
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 282
    label "Irak"
  ]
  node [
    id 283
    label "holoarktyka"
  ]
  node [
    id 284
    label "USA"
  ]
  node [
    id 285
    label "Sudan"
  ]
  node [
    id 286
    label "Nepal"
  ]
  node [
    id 287
    label "San_Marino"
  ]
  node [
    id 288
    label "Burundi"
  ]
  node [
    id 289
    label "Dominikana"
  ]
  node [
    id 290
    label "Komory"
  ]
  node [
    id 291
    label "granica_pa&#324;stwa"
  ]
  node [
    id 292
    label "Gwatemala"
  ]
  node [
    id 293
    label "Antarktis"
  ]
  node [
    id 294
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 295
    label "Brunei"
  ]
  node [
    id 296
    label "Iran"
  ]
  node [
    id 297
    label "Zimbabwe"
  ]
  node [
    id 298
    label "Namibia"
  ]
  node [
    id 299
    label "Meksyk"
  ]
  node [
    id 300
    label "Kamerun"
  ]
  node [
    id 301
    label "zwrot"
  ]
  node [
    id 302
    label "Somalia"
  ]
  node [
    id 303
    label "Angola"
  ]
  node [
    id 304
    label "Gabon"
  ]
  node [
    id 305
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 306
    label "Mozambik"
  ]
  node [
    id 307
    label "Tajwan"
  ]
  node [
    id 308
    label "Tunezja"
  ]
  node [
    id 309
    label "Nowa_Zelandia"
  ]
  node [
    id 310
    label "Liban"
  ]
  node [
    id 311
    label "Jordania"
  ]
  node [
    id 312
    label "Tonga"
  ]
  node [
    id 313
    label "Czad"
  ]
  node [
    id 314
    label "Liberia"
  ]
  node [
    id 315
    label "Gwinea"
  ]
  node [
    id 316
    label "Belize"
  ]
  node [
    id 317
    label "&#321;otwa"
  ]
  node [
    id 318
    label "Syria"
  ]
  node [
    id 319
    label "Benin"
  ]
  node [
    id 320
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 321
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 322
    label "Dominika"
  ]
  node [
    id 323
    label "Antigua_i_Barbuda"
  ]
  node [
    id 324
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 325
    label "Hanower"
  ]
  node [
    id 326
    label "partia"
  ]
  node [
    id 327
    label "Afganistan"
  ]
  node [
    id 328
    label "Kiribati"
  ]
  node [
    id 329
    label "W&#322;ochy"
  ]
  node [
    id 330
    label "Szwajcaria"
  ]
  node [
    id 331
    label "Sahara_Zachodnia"
  ]
  node [
    id 332
    label "Chorwacja"
  ]
  node [
    id 333
    label "Tajlandia"
  ]
  node [
    id 334
    label "Salwador"
  ]
  node [
    id 335
    label "Bahamy"
  ]
  node [
    id 336
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 337
    label "S&#322;owenia"
  ]
  node [
    id 338
    label "Gambia"
  ]
  node [
    id 339
    label "Urugwaj"
  ]
  node [
    id 340
    label "Zair"
  ]
  node [
    id 341
    label "Erytrea"
  ]
  node [
    id 342
    label "Rosja"
  ]
  node [
    id 343
    label "Uganda"
  ]
  node [
    id 344
    label "Niger"
  ]
  node [
    id 345
    label "Mauritius"
  ]
  node [
    id 346
    label "Turkmenistan"
  ]
  node [
    id 347
    label "Turcja"
  ]
  node [
    id 348
    label "Irlandia"
  ]
  node [
    id 349
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 350
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 351
    label "Gwinea_Bissau"
  ]
  node [
    id 352
    label "Belgia"
  ]
  node [
    id 353
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 354
    label "Palau"
  ]
  node [
    id 355
    label "Barbados"
  ]
  node [
    id 356
    label "Chile"
  ]
  node [
    id 357
    label "Wenezuela"
  ]
  node [
    id 358
    label "W&#281;gry"
  ]
  node [
    id 359
    label "Argentyna"
  ]
  node [
    id 360
    label "Kolumbia"
  ]
  node [
    id 361
    label "Sierra_Leone"
  ]
  node [
    id 362
    label "Azerbejd&#380;an"
  ]
  node [
    id 363
    label "Kongo"
  ]
  node [
    id 364
    label "Pakistan"
  ]
  node [
    id 365
    label "Liechtenstein"
  ]
  node [
    id 366
    label "Nikaragua"
  ]
  node [
    id 367
    label "Senegal"
  ]
  node [
    id 368
    label "Indie"
  ]
  node [
    id 369
    label "Suazi"
  ]
  node [
    id 370
    label "Polska"
  ]
  node [
    id 371
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 372
    label "Algieria"
  ]
  node [
    id 373
    label "terytorium"
  ]
  node [
    id 374
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 375
    label "Jamajka"
  ]
  node [
    id 376
    label "Kostaryka"
  ]
  node [
    id 377
    label "Timor_Wschodni"
  ]
  node [
    id 378
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 379
    label "Kuba"
  ]
  node [
    id 380
    label "Mauretania"
  ]
  node [
    id 381
    label "Portoryko"
  ]
  node [
    id 382
    label "Brazylia"
  ]
  node [
    id 383
    label "Mo&#322;dawia"
  ]
  node [
    id 384
    label "organizacja"
  ]
  node [
    id 385
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 386
    label "Litwa"
  ]
  node [
    id 387
    label "Kirgistan"
  ]
  node [
    id 388
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 389
    label "Izrael"
  ]
  node [
    id 390
    label "Grecja"
  ]
  node [
    id 391
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 392
    label "Holandia"
  ]
  node [
    id 393
    label "Sri_Lanka"
  ]
  node [
    id 394
    label "Katar"
  ]
  node [
    id 395
    label "Mikronezja"
  ]
  node [
    id 396
    label "Mongolia"
  ]
  node [
    id 397
    label "Laos"
  ]
  node [
    id 398
    label "Malediwy"
  ]
  node [
    id 399
    label "Zambia"
  ]
  node [
    id 400
    label "Tanzania"
  ]
  node [
    id 401
    label "Gujana"
  ]
  node [
    id 402
    label "Czechy"
  ]
  node [
    id 403
    label "Panama"
  ]
  node [
    id 404
    label "Uzbekistan"
  ]
  node [
    id 405
    label "Gruzja"
  ]
  node [
    id 406
    label "Serbia"
  ]
  node [
    id 407
    label "Francja"
  ]
  node [
    id 408
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 409
    label "Togo"
  ]
  node [
    id 410
    label "Estonia"
  ]
  node [
    id 411
    label "Oman"
  ]
  node [
    id 412
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 413
    label "Portugalia"
  ]
  node [
    id 414
    label "Boliwia"
  ]
  node [
    id 415
    label "Luksemburg"
  ]
  node [
    id 416
    label "Haiti"
  ]
  node [
    id 417
    label "Wyspy_Salomona"
  ]
  node [
    id 418
    label "Birma"
  ]
  node [
    id 419
    label "Rodezja"
  ]
  node [
    id 420
    label "dokument"
  ]
  node [
    id 421
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 422
    label "reputacja"
  ]
  node [
    id 423
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 424
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 425
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 426
    label "informacja"
  ]
  node [
    id 427
    label "sofcik"
  ]
  node [
    id 428
    label "appraisal"
  ]
  node [
    id 429
    label "ekspertyza"
  ]
  node [
    id 430
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 431
    label "pogl&#261;d"
  ]
  node [
    id 432
    label "kryterium"
  ]
  node [
    id 433
    label "wielko&#347;&#263;"
  ]
  node [
    id 434
    label "plac&#243;wka"
  ]
  node [
    id 435
    label "institute"
  ]
  node [
    id 436
    label "&#379;ydowski_Instytut_Historyczny"
  ]
  node [
    id 437
    label "Ossolineum"
  ]
  node [
    id 438
    label "instytucja"
  ]
  node [
    id 439
    label "plac&#243;wka_naukowa"
  ]
  node [
    id 440
    label "hepatologia"
  ]
  node [
    id 441
    label "hemodynamika"
  ]
  node [
    id 442
    label "proktologia"
  ]
  node [
    id 443
    label "geriatria"
  ]
  node [
    id 444
    label "po&#322;o&#380;nictwo"
  ]
  node [
    id 445
    label "medycyna_lotnicza"
  ]
  node [
    id 446
    label "patologia"
  ]
  node [
    id 447
    label "protetyka"
  ]
  node [
    id 448
    label "balneologia"
  ]
  node [
    id 449
    label "stomatologia"
  ]
  node [
    id 450
    label "pulmonologia"
  ]
  node [
    id 451
    label "medycyna_tropikalna"
  ]
  node [
    id 452
    label "psychosomatyka"
  ]
  node [
    id 453
    label "elektromiografia"
  ]
  node [
    id 454
    label "ginekologia"
  ]
  node [
    id 455
    label "elektromedycyna"
  ]
  node [
    id 456
    label "medycyna_kosmiczna"
  ]
  node [
    id 457
    label "nozologia"
  ]
  node [
    id 458
    label "urologia"
  ]
  node [
    id 459
    label "p&#322;yn_fizjologiczny"
  ]
  node [
    id 460
    label "transplantologia"
  ]
  node [
    id 461
    label "symptomatologia"
  ]
  node [
    id 462
    label "higiena"
  ]
  node [
    id 463
    label "medycyna_nuklearna"
  ]
  node [
    id 464
    label "ftyzjologia"
  ]
  node [
    id 465
    label "audiologia"
  ]
  node [
    id 466
    label "seksuologia"
  ]
  node [
    id 467
    label "psychiatria"
  ]
  node [
    id 468
    label "torakotomia"
  ]
  node [
    id 469
    label "sztuka_leczenia"
  ]
  node [
    id 470
    label "toksykologia"
  ]
  node [
    id 471
    label "etiologia"
  ]
  node [
    id 472
    label "medycyna_weterynaryjna"
  ]
  node [
    id 473
    label "medycyna_s&#261;dowa"
  ]
  node [
    id 474
    label "anestezjologia"
  ]
  node [
    id 475
    label "hipertensjologia"
  ]
  node [
    id 476
    label "gerontologia"
  ]
  node [
    id 477
    label "reumatologia"
  ]
  node [
    id 478
    label "zio&#322;olecznictwo"
  ]
  node [
    id 479
    label "wenerologia"
  ]
  node [
    id 480
    label "neurofizjologia"
  ]
  node [
    id 481
    label "transfuzjologia"
  ]
  node [
    id 482
    label "onkologia"
  ]
  node [
    id 483
    label "diagnostyka"
  ]
  node [
    id 484
    label "neuropsychiatria"
  ]
  node [
    id 485
    label "otorynolaryngologia"
  ]
  node [
    id 486
    label "epidemiologia"
  ]
  node [
    id 487
    label "antropotomia"
  ]
  node [
    id 488
    label "biomedycyna"
  ]
  node [
    id 489
    label "cytologia"
  ]
  node [
    id 490
    label "ortopedia"
  ]
  node [
    id 491
    label "polisomnografia"
  ]
  node [
    id 492
    label "dermatologia"
  ]
  node [
    id 493
    label "osmologia"
  ]
  node [
    id 494
    label "psychofizjologia"
  ]
  node [
    id 495
    label "pediatria"
  ]
  node [
    id 496
    label "nefrologia"
  ]
  node [
    id 497
    label "alergologia"
  ]
  node [
    id 498
    label "radiologia"
  ]
  node [
    id 499
    label "gastroenterologia"
  ]
  node [
    id 500
    label "medycyna_wewn&#281;trzna"
  ]
  node [
    id 501
    label "andrologia"
  ]
  node [
    id 502
    label "medycyna_zapobiegawcza"
  ]
  node [
    id 503
    label "immunologia"
  ]
  node [
    id 504
    label "medycyna_ratunkowa"
  ]
  node [
    id 505
    label "kierunek"
  ]
  node [
    id 506
    label "endokrynologia"
  ]
  node [
    id 507
    label "podologia"
  ]
  node [
    id 508
    label "psychoonkologia"
  ]
  node [
    id 509
    label "bariatria"
  ]
  node [
    id 510
    label "chirurgia"
  ]
  node [
    id 511
    label "serologia"
  ]
  node [
    id 512
    label "diabetologia"
  ]
  node [
    id 513
    label "kardiologia"
  ]
  node [
    id 514
    label "medycyna_sportowa"
  ]
  node [
    id 515
    label "neurologia"
  ]
  node [
    id 516
    label "hematologia"
  ]
  node [
    id 517
    label "laryngologia"
  ]
  node [
    id 518
    label "okulistyka"
  ]
  node [
    id 519
    label "stosunek_pracy"
  ]
  node [
    id 520
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 521
    label "benedykty&#324;ski"
  ]
  node [
    id 522
    label "pracowanie"
  ]
  node [
    id 523
    label "zaw&#243;d"
  ]
  node [
    id 524
    label "kierownictwo"
  ]
  node [
    id 525
    label "zmiana"
  ]
  node [
    id 526
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 527
    label "wytw&#243;r"
  ]
  node [
    id 528
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 529
    label "tynkarski"
  ]
  node [
    id 530
    label "czynnik_produkcji"
  ]
  node [
    id 531
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 532
    label "zobowi&#261;zanie"
  ]
  node [
    id 533
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 534
    label "czynno&#347;&#263;"
  ]
  node [
    id 535
    label "tyrka"
  ]
  node [
    id 536
    label "pracowa&#263;"
  ]
  node [
    id 537
    label "siedziba"
  ]
  node [
    id 538
    label "poda&#380;_pracy"
  ]
  node [
    id 539
    label "miejsce"
  ]
  node [
    id 540
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 541
    label "najem"
  ]
  node [
    id 542
    label "regaty"
  ]
  node [
    id 543
    label "statek"
  ]
  node [
    id 544
    label "spalin&#243;wka"
  ]
  node [
    id 545
    label "pok&#322;ad"
  ]
  node [
    id 546
    label "ster"
  ]
  node [
    id 547
    label "kratownica"
  ]
  node [
    id 548
    label "pojazd_niemechaniczny"
  ]
  node [
    id 549
    label "drzewce"
  ]
  node [
    id 550
    label "berylowiec"
  ]
  node [
    id 551
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 552
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 553
    label "mikroradian"
  ]
  node [
    id 554
    label "zadowolenie_si&#281;"
  ]
  node [
    id 555
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 556
    label "content"
  ]
  node [
    id 557
    label "jednostka_promieniowania"
  ]
  node [
    id 558
    label "miliradian"
  ]
  node [
    id 559
    label "jednostka"
  ]
  node [
    id 560
    label "chemical_bond"
  ]
  node [
    id 561
    label "tarcza"
  ]
  node [
    id 562
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 563
    label "obiekt"
  ]
  node [
    id 564
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 565
    label "borowiec"
  ]
  node [
    id 566
    label "obstawienie"
  ]
  node [
    id 567
    label "formacja"
  ]
  node [
    id 568
    label "ubezpieczenie"
  ]
  node [
    id 569
    label "obstawia&#263;"
  ]
  node [
    id 570
    label "obstawianie"
  ]
  node [
    id 571
    label "transportacja"
  ]
  node [
    id 572
    label "parlament"
  ]
  node [
    id 573
    label "lewica"
  ]
  node [
    id 574
    label "zgromadzenie"
  ]
  node [
    id 575
    label "prawica"
  ]
  node [
    id 576
    label "izba_ni&#380;sza"
  ]
  node [
    id 577
    label "parliament"
  ]
  node [
    id 578
    label "Buriacja"
  ]
  node [
    id 579
    label "Abchazja"
  ]
  node [
    id 580
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 581
    label "Inguszetia"
  ]
  node [
    id 582
    label "Nachiczewan"
  ]
  node [
    id 583
    label "Karaka&#322;pacja"
  ]
  node [
    id 584
    label "Jakucja"
  ]
  node [
    id 585
    label "Singapur"
  ]
  node [
    id 586
    label "Karelia"
  ]
  node [
    id 587
    label "Komi"
  ]
  node [
    id 588
    label "Tatarstan"
  ]
  node [
    id 589
    label "Chakasja"
  ]
  node [
    id 590
    label "Dagestan"
  ]
  node [
    id 591
    label "Mordowia"
  ]
  node [
    id 592
    label "Ka&#322;mucja"
  ]
  node [
    id 593
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 594
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 595
    label "Baszkiria"
  ]
  node [
    id 596
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 597
    label "Mari_El"
  ]
  node [
    id 598
    label "Ad&#380;aria"
  ]
  node [
    id 599
    label "Czuwaszja"
  ]
  node [
    id 600
    label "Tuwa"
  ]
  node [
    id 601
    label "Czeczenia"
  ]
  node [
    id 602
    label "Udmurcja"
  ]
  node [
    id 603
    label "lacki"
  ]
  node [
    id 604
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 605
    label "przedmiot"
  ]
  node [
    id 606
    label "sztajer"
  ]
  node [
    id 607
    label "drabant"
  ]
  node [
    id 608
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 609
    label "polak"
  ]
  node [
    id 610
    label "pierogi_ruskie"
  ]
  node [
    id 611
    label "krakowiak"
  ]
  node [
    id 612
    label "Polish"
  ]
  node [
    id 613
    label "j&#281;zyk"
  ]
  node [
    id 614
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 615
    label "oberek"
  ]
  node [
    id 616
    label "po_polsku"
  ]
  node [
    id 617
    label "mazur"
  ]
  node [
    id 618
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 619
    label "chodzony"
  ]
  node [
    id 620
    label "skoczny"
  ]
  node [
    id 621
    label "ryba_po_grecku"
  ]
  node [
    id 622
    label "goniony"
  ]
  node [
    id 623
    label "polsko"
  ]
  node [
    id 624
    label "rodzimy"
  ]
  node [
    id 625
    label "centroprawica"
  ]
  node [
    id 626
    label "core"
  ]
  node [
    id 627
    label "Hollywood"
  ]
  node [
    id 628
    label "centrolew"
  ]
  node [
    id 629
    label "blok"
  ]
  node [
    id 630
    label "punkt"
  ]
  node [
    id 631
    label "o&#347;rodek"
  ]
  node [
    id 632
    label "os&#322;abia&#263;"
  ]
  node [
    id 633
    label "niszczy&#263;"
  ]
  node [
    id 634
    label "zniszczy&#263;"
  ]
  node [
    id 635
    label "firmness"
  ]
  node [
    id 636
    label "kondycja"
  ]
  node [
    id 637
    label "os&#322;abi&#263;"
  ]
  node [
    id 638
    label "rozsypanie_si&#281;"
  ]
  node [
    id 639
    label "zdarcie"
  ]
  node [
    id 640
    label "zniszczenie"
  ]
  node [
    id 641
    label "zedrze&#263;"
  ]
  node [
    id 642
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 643
    label "niszczenie"
  ]
  node [
    id 644
    label "os&#322;abienie"
  ]
  node [
    id 645
    label "soundness"
  ]
  node [
    id 646
    label "os&#322;abianie"
  ]
  node [
    id 647
    label "najwa&#380;niejszy"
  ]
  node [
    id 648
    label "g&#322;&#243;wnie"
  ]
  node [
    id 649
    label "inspectorate"
  ]
  node [
    id 650
    label "urz&#261;d"
  ]
  node [
    id 651
    label "medyczny"
  ]
  node [
    id 652
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 653
    label "express"
  ]
  node [
    id 654
    label "rzekn&#261;&#263;"
  ]
  node [
    id 655
    label "okre&#347;li&#263;"
  ]
  node [
    id 656
    label "wyrazi&#263;"
  ]
  node [
    id 657
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 658
    label "unwrap"
  ]
  node [
    id 659
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 660
    label "convey"
  ]
  node [
    id 661
    label "discover"
  ]
  node [
    id 662
    label "wydoby&#263;"
  ]
  node [
    id 663
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 664
    label "poda&#263;"
  ]
  node [
    id 665
    label "open"
  ]
  node [
    id 666
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 667
    label "min&#261;&#263;"
  ]
  node [
    id 668
    label "cut"
  ]
  node [
    id 669
    label "ograniczenie"
  ]
  node [
    id 670
    label "przeby&#263;"
  ]
  node [
    id 671
    label "pique"
  ]
  node [
    id 672
    label "pace"
  ]
  node [
    id 673
    label "moralno&#347;&#263;"
  ]
  node [
    id 674
    label "powszednio&#347;&#263;"
  ]
  node [
    id 675
    label "konstrukt"
  ]
  node [
    id 676
    label "prawid&#322;o"
  ]
  node [
    id 677
    label "criterion"
  ]
  node [
    id 678
    label "rozmiar"
  ]
  node [
    id 679
    label "standard"
  ]
  node [
    id 680
    label "mo&#380;liwy"
  ]
  node [
    id 681
    label "dopuszczalnie"
  ]
  node [
    id 682
    label "u&#380;ytecznie"
  ]
  node [
    id 683
    label "praktyczny"
  ]
  node [
    id 684
    label "racjonalnie"
  ]
  node [
    id 685
    label "wpa&#347;&#263;"
  ]
  node [
    id 686
    label "wpadanie"
  ]
  node [
    id 687
    label "wpada&#263;"
  ]
  node [
    id 688
    label "kultura"
  ]
  node [
    id 689
    label "przyroda"
  ]
  node [
    id 690
    label "mienie"
  ]
  node [
    id 691
    label "object"
  ]
  node [
    id 692
    label "wpadni&#281;cie"
  ]
  node [
    id 693
    label "get"
  ]
  node [
    id 694
    label "przewa&#380;a&#263;"
  ]
  node [
    id 695
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 696
    label "poczytywa&#263;"
  ]
  node [
    id 697
    label "levy"
  ]
  node [
    id 698
    label "pokonywa&#263;"
  ]
  node [
    id 699
    label "u&#380;ywa&#263;"
  ]
  node [
    id 700
    label "rusza&#263;"
  ]
  node [
    id 701
    label "zalicza&#263;"
  ]
  node [
    id 702
    label "wygrywa&#263;"
  ]
  node [
    id 703
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 704
    label "branie"
  ]
  node [
    id 705
    label "korzysta&#263;"
  ]
  node [
    id 706
    label "&#263;pa&#263;"
  ]
  node [
    id 707
    label "wch&#322;ania&#263;"
  ]
  node [
    id 708
    label "interpretowa&#263;"
  ]
  node [
    id 709
    label "atakowa&#263;"
  ]
  node [
    id 710
    label "prowadzi&#263;"
  ]
  node [
    id 711
    label "rucha&#263;"
  ]
  node [
    id 712
    label "take"
  ]
  node [
    id 713
    label "dostawa&#263;"
  ]
  node [
    id 714
    label "wzi&#261;&#263;"
  ]
  node [
    id 715
    label "wk&#322;ada&#263;"
  ]
  node [
    id 716
    label "chwyta&#263;"
  ]
  node [
    id 717
    label "arise"
  ]
  node [
    id 718
    label "za&#380;ywa&#263;"
  ]
  node [
    id 719
    label "uprawia&#263;_seks"
  ]
  node [
    id 720
    label "porywa&#263;"
  ]
  node [
    id 721
    label "robi&#263;"
  ]
  node [
    id 722
    label "grza&#263;"
  ]
  node [
    id 723
    label "abstract"
  ]
  node [
    id 724
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 725
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 726
    label "towarzystwo"
  ]
  node [
    id 727
    label "otrzymywa&#263;"
  ]
  node [
    id 728
    label "przyjmowa&#263;"
  ]
  node [
    id 729
    label "wchodzi&#263;"
  ]
  node [
    id 730
    label "ucieka&#263;"
  ]
  node [
    id 731
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 732
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 733
    label "&#322;apa&#263;"
  ]
  node [
    id 734
    label "raise"
  ]
  node [
    id 735
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 736
    label "odsuwa&#263;"
  ]
  node [
    id 737
    label "zanosi&#263;"
  ]
  node [
    id 738
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 739
    label "rozpowszechnia&#263;"
  ]
  node [
    id 740
    label "ujawnia&#263;"
  ]
  node [
    id 741
    label "kra&#347;&#263;"
  ]
  node [
    id 742
    label "kwota"
  ]
  node [
    id 743
    label "liczy&#263;"
  ]
  node [
    id 744
    label "podnosi&#263;"
  ]
  node [
    id 745
    label "czyj&#347;"
  ]
  node [
    id 746
    label "m&#261;&#380;"
  ]
  node [
    id 747
    label "uprzedzenie"
  ]
  node [
    id 748
    label "zawisa&#263;"
  ]
  node [
    id 749
    label "zawisanie"
  ]
  node [
    id 750
    label "szachowanie"
  ]
  node [
    id 751
    label "zaszachowanie"
  ]
  node [
    id 752
    label "emergency"
  ]
  node [
    id 753
    label "zaistnienie"
  ]
  node [
    id 754
    label "czarny_punkt"
  ]
  node [
    id 755
    label "zagrozi&#263;"
  ]
  node [
    id 756
    label "realny"
  ]
  node [
    id 757
    label "naprawd&#281;"
  ]
  node [
    id 758
    label "&#380;ycie"
  ]
  node [
    id 759
    label "koleje_losu"
  ]
  node [
    id 760
    label "proceed"
  ]
  node [
    id 761
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 762
    label "bangla&#263;"
  ]
  node [
    id 763
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 764
    label "run"
  ]
  node [
    id 765
    label "tryb"
  ]
  node [
    id 766
    label "p&#322;ywa&#263;"
  ]
  node [
    id 767
    label "continue"
  ]
  node [
    id 768
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 769
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 770
    label "przebiega&#263;"
  ]
  node [
    id 771
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 772
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 773
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 774
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 775
    label "krok"
  ]
  node [
    id 776
    label "str&#243;j"
  ]
  node [
    id 777
    label "bywa&#263;"
  ]
  node [
    id 778
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 779
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 780
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 781
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 782
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 783
    label "dziama&#263;"
  ]
  node [
    id 784
    label "stara&#263;_si&#281;"
  ]
  node [
    id 785
    label "carry"
  ]
  node [
    id 786
    label "ognisko"
  ]
  node [
    id 787
    label "odezwanie_si&#281;"
  ]
  node [
    id 788
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 789
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 790
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 791
    label "przypadek"
  ]
  node [
    id 792
    label "zajmowa&#263;"
  ]
  node [
    id 793
    label "zajmowanie"
  ]
  node [
    id 794
    label "badanie_histopatologiczne"
  ]
  node [
    id 795
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 796
    label "atakowanie"
  ]
  node [
    id 797
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 798
    label "bol&#261;czka"
  ]
  node [
    id 799
    label "remisja"
  ]
  node [
    id 800
    label "grupa_ryzyka"
  ]
  node [
    id 801
    label "kryzys"
  ]
  node [
    id 802
    label "nabawienie_si&#281;"
  ]
  node [
    id 803
    label "chor&#243;bka"
  ]
  node [
    id 804
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 805
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 806
    label "inkubacja"
  ]
  node [
    id 807
    label "powalenie"
  ]
  node [
    id 808
    label "cholera"
  ]
  node [
    id 809
    label "nabawianie_si&#281;"
  ]
  node [
    id 810
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 811
    label "odzywanie_si&#281;"
  ]
  node [
    id 812
    label "diagnoza"
  ]
  node [
    id 813
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 814
    label "powali&#263;"
  ]
  node [
    id 815
    label "zaburzenie"
  ]
  node [
    id 816
    label "zawodowo"
  ]
  node [
    id 817
    label "zawo&#322;any"
  ]
  node [
    id 818
    label "profesjonalny"
  ]
  node [
    id 819
    label "klawy"
  ]
  node [
    id 820
    label "czadowy"
  ]
  node [
    id 821
    label "fajny"
  ]
  node [
    id 822
    label "fachowy"
  ]
  node [
    id 823
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 824
    label "formalny"
  ]
  node [
    id 825
    label "rezultat"
  ]
  node [
    id 826
    label "radiation"
  ]
  node [
    id 827
    label "wysy&#322;anie"
  ]
  node [
    id 828
    label "energia"
  ]
  node [
    id 829
    label "radiesteta"
  ]
  node [
    id 830
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 831
    label "oddzia&#322;ywanie"
  ]
  node [
    id 832
    label "aktynometr"
  ]
  node [
    id 833
    label "radiance"
  ]
  node [
    id 834
    label "emanowanie"
  ]
  node [
    id 835
    label "irradiacja"
  ]
  node [
    id 836
    label "narz&#261;d_krytyczny"
  ]
  node [
    id 837
    label "emission"
  ]
  node [
    id 838
    label "radiotherapy"
  ]
  node [
    id 839
    label "zjawisko"
  ]
  node [
    id 840
    label "wypromieniowywanie"
  ]
  node [
    id 841
    label "zmienia&#263;"
  ]
  node [
    id 842
    label "nadawa&#263;"
  ]
  node [
    id 843
    label "kontroler"
  ]
  node [
    id 844
    label "urz&#281;dnik"
  ]
  node [
    id 845
    label "oficer_policji"
  ]
  node [
    id 846
    label "jedyny"
  ]
  node [
    id 847
    label "kompletny"
  ]
  node [
    id 848
    label "zdr&#243;w"
  ]
  node [
    id 849
    label "&#380;ywy"
  ]
  node [
    id 850
    label "ca&#322;o"
  ]
  node [
    id 851
    label "pe&#322;ny"
  ]
  node [
    id 852
    label "calu&#347;ko"
  ]
  node [
    id 853
    label "podobny"
  ]
  node [
    id 854
    label "czasokres"
  ]
  node [
    id 855
    label "trawienie"
  ]
  node [
    id 856
    label "kategoria_gramatyczna"
  ]
  node [
    id 857
    label "period"
  ]
  node [
    id 858
    label "odczyt"
  ]
  node [
    id 859
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 860
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 861
    label "chwila"
  ]
  node [
    id 862
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 863
    label "poprzedzenie"
  ]
  node [
    id 864
    label "koniugacja"
  ]
  node [
    id 865
    label "dzieje"
  ]
  node [
    id 866
    label "poprzedzi&#263;"
  ]
  node [
    id 867
    label "przep&#322;ywanie"
  ]
  node [
    id 868
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 869
    label "odwlekanie_si&#281;"
  ]
  node [
    id 870
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 871
    label "Zeitgeist"
  ]
  node [
    id 872
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 873
    label "okres_czasu"
  ]
  node [
    id 874
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 875
    label "pochodzi&#263;"
  ]
  node [
    id 876
    label "schy&#322;ek"
  ]
  node [
    id 877
    label "czwarty_wymiar"
  ]
  node [
    id 878
    label "chronometria"
  ]
  node [
    id 879
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 880
    label "poprzedzanie"
  ]
  node [
    id 881
    label "pogoda"
  ]
  node [
    id 882
    label "zegar"
  ]
  node [
    id 883
    label "pochodzenie"
  ]
  node [
    id 884
    label "poprzedza&#263;"
  ]
  node [
    id 885
    label "trawi&#263;"
  ]
  node [
    id 886
    label "time_period"
  ]
  node [
    id 887
    label "rachuba_czasu"
  ]
  node [
    id 888
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 889
    label "czasoprzestrze&#324;"
  ]
  node [
    id 890
    label "laba"
  ]
  node [
    id 891
    label "obserwowa&#263;"
  ]
  node [
    id 892
    label "kontrolowa&#263;"
  ]
  node [
    id 893
    label "warto&#347;&#263;"
  ]
  node [
    id 894
    label "co&#347;"
  ]
  node [
    id 895
    label "syf"
  ]
  node [
    id 896
    label "state"
  ]
  node [
    id 897
    label "quality"
  ]
  node [
    id 898
    label "pod&#322;&#261;czy&#263;"
  ]
  node [
    id 899
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 900
    label "urz&#261;dzenie"
  ]
  node [
    id 901
    label "pod&#322;&#261;cza&#263;"
  ]
  node [
    id 902
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 903
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 904
    label "ni_chuja"
  ]
  node [
    id 905
    label "ca&#322;kiem"
  ]
  node [
    id 906
    label "zupe&#322;nie"
  ]
  node [
    id 907
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 908
    label "rok"
  ]
  node [
    id 909
    label "long_time"
  ]
  node [
    id 910
    label "choroba_wieku"
  ]
  node [
    id 911
    label "jednostka_geologiczna"
  ]
  node [
    id 912
    label "chron"
  ]
  node [
    id 913
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 914
    label "opowiada&#263;"
  ]
  node [
    id 915
    label "informowa&#263;"
  ]
  node [
    id 916
    label "czyni&#263;_dobro"
  ]
  node [
    id 917
    label "us&#322;uga"
  ]
  node [
    id 918
    label "sk&#322;ada&#263;"
  ]
  node [
    id 919
    label "bespeak"
  ]
  node [
    id 920
    label "op&#322;aca&#263;"
  ]
  node [
    id 921
    label "testify"
  ]
  node [
    id 922
    label "wyraz"
  ]
  node [
    id 923
    label "supply"
  ]
  node [
    id 924
    label "uzna&#263;"
  ]
  node [
    id 925
    label "oznajmi&#263;"
  ]
  node [
    id 926
    label "declare"
  ]
  node [
    id 927
    label "nijaki"
  ]
  node [
    id 928
    label "czyn"
  ]
  node [
    id 929
    label "company"
  ]
  node [
    id 930
    label "zak&#322;adka"
  ]
  node [
    id 931
    label "firma"
  ]
  node [
    id 932
    label "wyko&#324;czenie"
  ]
  node [
    id 933
    label "jednostka_organizacyjna"
  ]
  node [
    id 934
    label "umowa"
  ]
  node [
    id 935
    label "miejsce_pracy"
  ]
  node [
    id 936
    label "transgression"
  ]
  node [
    id 937
    label "zrobienie"
  ]
  node [
    id 938
    label "transgresja"
  ]
  node [
    id 939
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 940
    label "offense"
  ]
  node [
    id 941
    label "przebycie"
  ]
  node [
    id 942
    label "mini&#281;cie"
  ]
  node [
    id 943
    label "przepuszczenie"
  ]
  node [
    id 944
    label "discourtesy"
  ]
  node [
    id 945
    label "emergence"
  ]
  node [
    id 946
    label "wiele"
  ]
  node [
    id 947
    label "konkretnie"
  ]
  node [
    id 948
    label "nieznacznie"
  ]
  node [
    id 949
    label "tam"
  ]
  node [
    id 950
    label "catch"
  ]
  node [
    id 951
    label "oddawa&#263;"
  ]
  node [
    id 952
    label "dostarcza&#263;"
  ]
  node [
    id 953
    label "osi&#261;ga&#263;"
  ]
  node [
    id 954
    label "doznawa&#263;"
  ]
  node [
    id 955
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 956
    label "rzeczowo"
  ]
  node [
    id 957
    label "jednostka_monetarna"
  ]
  node [
    id 958
    label "odmawia&#263;"
  ]
  node [
    id 959
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 960
    label "thank"
  ]
  node [
    id 961
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 962
    label "wyra&#380;a&#263;"
  ]
  node [
    id 963
    label "etykieta"
  ]
  node [
    id 964
    label "oklaski"
  ]
  node [
    id 965
    label "kla&#347;ni&#281;cie"
  ]
  node [
    id 966
    label "wielki"
  ]
  node [
    id 967
    label "rada"
  ]
  node [
    id 968
    label "wyspa"
  ]
  node [
    id 969
    label "Stefan"
  ]
  node [
    id 970
    label "Niesio&#322;owski"
  ]
  node [
    id 971
    label "Damian"
  ]
  node [
    id 972
    label "Raczkowski"
  ]
  node [
    id 973
    label "ustawa"
  ]
  node [
    id 974
    label "ojciec"
  ]
  node [
    id 975
    label "pracownik"
  ]
  node [
    id 976
    label "opieka"
  ]
  node [
    id 977
    label "zdrowotny"
  ]
  node [
    id 978
    label "Krystyna"
  ]
  node [
    id 979
    label "Skowro&#324;ska"
  ]
  node [
    id 980
    label "Boles&#322;awa"
  ]
  node [
    id 981
    label "Grzegorz"
  ]
  node [
    id 982
    label "Piecha"
  ]
  node [
    id 983
    label "Anna"
  ]
  node [
    id 984
    label "paluch"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 966
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 50
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 99
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 43
  ]
  edge [
    source 19
    target 44
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 46
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 174
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 22
    target 178
  ]
  edge [
    source 22
    target 179
  ]
  edge [
    source 22
    target 180
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 84
  ]
  edge [
    source 23
    target 94
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 53
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 24
    target 225
  ]
  edge [
    source 24
    target 226
  ]
  edge [
    source 24
    target 227
  ]
  edge [
    source 24
    target 228
  ]
  edge [
    source 24
    target 229
  ]
  edge [
    source 24
    target 230
  ]
  edge [
    source 24
    target 231
  ]
  edge [
    source 24
    target 232
  ]
  edge [
    source 24
    target 233
  ]
  edge [
    source 24
    target 234
  ]
  edge [
    source 24
    target 235
  ]
  edge [
    source 24
    target 236
  ]
  edge [
    source 24
    target 237
  ]
  edge [
    source 24
    target 238
  ]
  edge [
    source 24
    target 239
  ]
  edge [
    source 24
    target 240
  ]
  edge [
    source 24
    target 241
  ]
  edge [
    source 24
    target 242
  ]
  edge [
    source 24
    target 243
  ]
  edge [
    source 24
    target 244
  ]
  edge [
    source 24
    target 245
  ]
  edge [
    source 24
    target 246
  ]
  edge [
    source 24
    target 247
  ]
  edge [
    source 24
    target 248
  ]
  edge [
    source 24
    target 249
  ]
  edge [
    source 24
    target 250
  ]
  edge [
    source 24
    target 251
  ]
  edge [
    source 24
    target 252
  ]
  edge [
    source 24
    target 253
  ]
  edge [
    source 24
    target 254
  ]
  edge [
    source 24
    target 255
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 24
    target 257
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 259
  ]
  edge [
    source 24
    target 260
  ]
  edge [
    source 24
    target 261
  ]
  edge [
    source 24
    target 262
  ]
  edge [
    source 24
    target 263
  ]
  edge [
    source 24
    target 264
  ]
  edge [
    source 24
    target 265
  ]
  edge [
    source 24
    target 266
  ]
  edge [
    source 24
    target 267
  ]
  edge [
    source 24
    target 268
  ]
  edge [
    source 24
    target 269
  ]
  edge [
    source 24
    target 270
  ]
  edge [
    source 24
    target 271
  ]
  edge [
    source 24
    target 272
  ]
  edge [
    source 24
    target 273
  ]
  edge [
    source 24
    target 274
  ]
  edge [
    source 24
    target 275
  ]
  edge [
    source 24
    target 276
  ]
  edge [
    source 24
    target 277
  ]
  edge [
    source 24
    target 278
  ]
  edge [
    source 24
    target 279
  ]
  edge [
    source 24
    target 280
  ]
  edge [
    source 24
    target 281
  ]
  edge [
    source 24
    target 282
  ]
  edge [
    source 24
    target 283
  ]
  edge [
    source 24
    target 284
  ]
  edge [
    source 24
    target 285
  ]
  edge [
    source 24
    target 286
  ]
  edge [
    source 24
    target 287
  ]
  edge [
    source 24
    target 288
  ]
  edge [
    source 24
    target 289
  ]
  edge [
    source 24
    target 290
  ]
  edge [
    source 24
    target 291
  ]
  edge [
    source 24
    target 292
  ]
  edge [
    source 24
    target 293
  ]
  edge [
    source 24
    target 294
  ]
  edge [
    source 24
    target 295
  ]
  edge [
    source 24
    target 296
  ]
  edge [
    source 24
    target 297
  ]
  edge [
    source 24
    target 298
  ]
  edge [
    source 24
    target 299
  ]
  edge [
    source 24
    target 300
  ]
  edge [
    source 24
    target 301
  ]
  edge [
    source 24
    target 302
  ]
  edge [
    source 24
    target 303
  ]
  edge [
    source 24
    target 304
  ]
  edge [
    source 24
    target 305
  ]
  edge [
    source 24
    target 306
  ]
  edge [
    source 24
    target 307
  ]
  edge [
    source 24
    target 308
  ]
  edge [
    source 24
    target 309
  ]
  edge [
    source 24
    target 310
  ]
  edge [
    source 24
    target 311
  ]
  edge [
    source 24
    target 312
  ]
  edge [
    source 24
    target 313
  ]
  edge [
    source 24
    target 314
  ]
  edge [
    source 24
    target 315
  ]
  edge [
    source 24
    target 316
  ]
  edge [
    source 24
    target 317
  ]
  edge [
    source 24
    target 318
  ]
  edge [
    source 24
    target 319
  ]
  edge [
    source 24
    target 320
  ]
  edge [
    source 24
    target 321
  ]
  edge [
    source 24
    target 322
  ]
  edge [
    source 24
    target 323
  ]
  edge [
    source 24
    target 324
  ]
  edge [
    source 24
    target 325
  ]
  edge [
    source 24
    target 326
  ]
  edge [
    source 24
    target 327
  ]
  edge [
    source 24
    target 328
  ]
  edge [
    source 24
    target 329
  ]
  edge [
    source 24
    target 330
  ]
  edge [
    source 24
    target 331
  ]
  edge [
    source 24
    target 332
  ]
  edge [
    source 24
    target 333
  ]
  edge [
    source 24
    target 334
  ]
  edge [
    source 24
    target 335
  ]
  edge [
    source 24
    target 336
  ]
  edge [
    source 24
    target 337
  ]
  edge [
    source 24
    target 338
  ]
  edge [
    source 24
    target 339
  ]
  edge [
    source 24
    target 340
  ]
  edge [
    source 24
    target 341
  ]
  edge [
    source 24
    target 342
  ]
  edge [
    source 24
    target 343
  ]
  edge [
    source 24
    target 344
  ]
  edge [
    source 24
    target 345
  ]
  edge [
    source 24
    target 346
  ]
  edge [
    source 24
    target 347
  ]
  edge [
    source 24
    target 348
  ]
  edge [
    source 24
    target 349
  ]
  edge [
    source 24
    target 350
  ]
  edge [
    source 24
    target 351
  ]
  edge [
    source 24
    target 352
  ]
  edge [
    source 24
    target 353
  ]
  edge [
    source 24
    target 354
  ]
  edge [
    source 24
    target 355
  ]
  edge [
    source 24
    target 356
  ]
  edge [
    source 24
    target 357
  ]
  edge [
    source 24
    target 358
  ]
  edge [
    source 24
    target 359
  ]
  edge [
    source 24
    target 360
  ]
  edge [
    source 24
    target 361
  ]
  edge [
    source 24
    target 362
  ]
  edge [
    source 24
    target 363
  ]
  edge [
    source 24
    target 364
  ]
  edge [
    source 24
    target 365
  ]
  edge [
    source 24
    target 366
  ]
  edge [
    source 24
    target 367
  ]
  edge [
    source 24
    target 368
  ]
  edge [
    source 24
    target 369
  ]
  edge [
    source 24
    target 370
  ]
  edge [
    source 24
    target 371
  ]
  edge [
    source 24
    target 372
  ]
  edge [
    source 24
    target 373
  ]
  edge [
    source 24
    target 374
  ]
  edge [
    source 24
    target 375
  ]
  edge [
    source 24
    target 376
  ]
  edge [
    source 24
    target 377
  ]
  edge [
    source 24
    target 378
  ]
  edge [
    source 24
    target 379
  ]
  edge [
    source 24
    target 380
  ]
  edge [
    source 24
    target 381
  ]
  edge [
    source 24
    target 382
  ]
  edge [
    source 24
    target 383
  ]
  edge [
    source 24
    target 384
  ]
  edge [
    source 24
    target 385
  ]
  edge [
    source 24
    target 386
  ]
  edge [
    source 24
    target 387
  ]
  edge [
    source 24
    target 388
  ]
  edge [
    source 24
    target 389
  ]
  edge [
    source 24
    target 390
  ]
  edge [
    source 24
    target 391
  ]
  edge [
    source 24
    target 392
  ]
  edge [
    source 24
    target 393
  ]
  edge [
    source 24
    target 394
  ]
  edge [
    source 24
    target 395
  ]
  edge [
    source 24
    target 396
  ]
  edge [
    source 24
    target 397
  ]
  edge [
    source 24
    target 398
  ]
  edge [
    source 24
    target 399
  ]
  edge [
    source 24
    target 400
  ]
  edge [
    source 24
    target 401
  ]
  edge [
    source 24
    target 402
  ]
  edge [
    source 24
    target 403
  ]
  edge [
    source 24
    target 404
  ]
  edge [
    source 24
    target 405
  ]
  edge [
    source 24
    target 406
  ]
  edge [
    source 24
    target 407
  ]
  edge [
    source 24
    target 408
  ]
  edge [
    source 24
    target 409
  ]
  edge [
    source 24
    target 410
  ]
  edge [
    source 24
    target 411
  ]
  edge [
    source 24
    target 412
  ]
  edge [
    source 24
    target 413
  ]
  edge [
    source 24
    target 414
  ]
  edge [
    source 24
    target 415
  ]
  edge [
    source 24
    target 416
  ]
  edge [
    source 24
    target 417
  ]
  edge [
    source 24
    target 418
  ]
  edge [
    source 24
    target 419
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 54
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 89
  ]
  edge [
    source 25
    target 90
  ]
  edge [
    source 25
    target 420
  ]
  edge [
    source 25
    target 421
  ]
  edge [
    source 25
    target 422
  ]
  edge [
    source 25
    target 423
  ]
  edge [
    source 25
    target 424
  ]
  edge [
    source 25
    target 137
  ]
  edge [
    source 25
    target 425
  ]
  edge [
    source 25
    target 426
  ]
  edge [
    source 25
    target 427
  ]
  edge [
    source 25
    target 428
  ]
  edge [
    source 25
    target 429
  ]
  edge [
    source 25
    target 430
  ]
  edge [
    source 25
    target 431
  ]
  edge [
    source 25
    target 432
  ]
  edge [
    source 25
    target 433
  ]
  edge [
    source 25
    target 76
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 434
  ]
  edge [
    source 26
    target 435
  ]
  edge [
    source 26
    target 436
  ]
  edge [
    source 26
    target 437
  ]
  edge [
    source 26
    target 438
  ]
  edge [
    source 26
    target 439
  ]
  edge [
    source 26
    target 82
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 440
  ]
  edge [
    source 27
    target 441
  ]
  edge [
    source 27
    target 442
  ]
  edge [
    source 27
    target 443
  ]
  edge [
    source 27
    target 444
  ]
  edge [
    source 27
    target 445
  ]
  edge [
    source 27
    target 446
  ]
  edge [
    source 27
    target 447
  ]
  edge [
    source 27
    target 448
  ]
  edge [
    source 27
    target 449
  ]
  edge [
    source 27
    target 450
  ]
  edge [
    source 27
    target 451
  ]
  edge [
    source 27
    target 452
  ]
  edge [
    source 27
    target 453
  ]
  edge [
    source 27
    target 454
  ]
  edge [
    source 27
    target 455
  ]
  edge [
    source 27
    target 456
  ]
  edge [
    source 27
    target 457
  ]
  edge [
    source 27
    target 458
  ]
  edge [
    source 27
    target 459
  ]
  edge [
    source 27
    target 460
  ]
  edge [
    source 27
    target 461
  ]
  edge [
    source 27
    target 462
  ]
  edge [
    source 27
    target 463
  ]
  edge [
    source 27
    target 464
  ]
  edge [
    source 27
    target 465
  ]
  edge [
    source 27
    target 466
  ]
  edge [
    source 27
    target 467
  ]
  edge [
    source 27
    target 468
  ]
  edge [
    source 27
    target 469
  ]
  edge [
    source 27
    target 470
  ]
  edge [
    source 27
    target 471
  ]
  edge [
    source 27
    target 472
  ]
  edge [
    source 27
    target 473
  ]
  edge [
    source 27
    target 474
  ]
  edge [
    source 27
    target 475
  ]
  edge [
    source 27
    target 476
  ]
  edge [
    source 27
    target 477
  ]
  edge [
    source 27
    target 478
  ]
  edge [
    source 27
    target 479
  ]
  edge [
    source 27
    target 480
  ]
  edge [
    source 27
    target 481
  ]
  edge [
    source 27
    target 482
  ]
  edge [
    source 27
    target 483
  ]
  edge [
    source 27
    target 484
  ]
  edge [
    source 27
    target 485
  ]
  edge [
    source 27
    target 486
  ]
  edge [
    source 27
    target 487
  ]
  edge [
    source 27
    target 488
  ]
  edge [
    source 27
    target 489
  ]
  edge [
    source 27
    target 490
  ]
  edge [
    source 27
    target 491
  ]
  edge [
    source 27
    target 492
  ]
  edge [
    source 27
    target 493
  ]
  edge [
    source 27
    target 494
  ]
  edge [
    source 27
    target 495
  ]
  edge [
    source 27
    target 496
  ]
  edge [
    source 27
    target 497
  ]
  edge [
    source 27
    target 498
  ]
  edge [
    source 27
    target 499
  ]
  edge [
    source 27
    target 500
  ]
  edge [
    source 27
    target 501
  ]
  edge [
    source 27
    target 502
  ]
  edge [
    source 27
    target 503
  ]
  edge [
    source 27
    target 504
  ]
  edge [
    source 27
    target 505
  ]
  edge [
    source 27
    target 506
  ]
  edge [
    source 27
    target 507
  ]
  edge [
    source 27
    target 508
  ]
  edge [
    source 27
    target 509
  ]
  edge [
    source 27
    target 510
  ]
  edge [
    source 27
    target 511
  ]
  edge [
    source 27
    target 512
  ]
  edge [
    source 27
    target 513
  ]
  edge [
    source 27
    target 514
  ]
  edge [
    source 27
    target 515
  ]
  edge [
    source 27
    target 516
  ]
  edge [
    source 27
    target 517
  ]
  edge [
    source 27
    target 518
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 82
  ]
  edge [
    source 28
    target 56
  ]
  edge [
    source 28
    target 519
  ]
  edge [
    source 28
    target 520
  ]
  edge [
    source 28
    target 521
  ]
  edge [
    source 28
    target 522
  ]
  edge [
    source 28
    target 523
  ]
  edge [
    source 28
    target 524
  ]
  edge [
    source 28
    target 525
  ]
  edge [
    source 28
    target 526
  ]
  edge [
    source 28
    target 527
  ]
  edge [
    source 28
    target 528
  ]
  edge [
    source 28
    target 529
  ]
  edge [
    source 28
    target 530
  ]
  edge [
    source 28
    target 531
  ]
  edge [
    source 28
    target 532
  ]
  edge [
    source 28
    target 533
  ]
  edge [
    source 28
    target 534
  ]
  edge [
    source 28
    target 535
  ]
  edge [
    source 28
    target 536
  ]
  edge [
    source 28
    target 537
  ]
  edge [
    source 28
    target 538
  ]
  edge [
    source 28
    target 539
  ]
  edge [
    source 28
    target 540
  ]
  edge [
    source 28
    target 541
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 28
    target 967
  ]
  edge [
    source 29
    target 542
  ]
  edge [
    source 29
    target 543
  ]
  edge [
    source 29
    target 544
  ]
  edge [
    source 29
    target 545
  ]
  edge [
    source 29
    target 546
  ]
  edge [
    source 29
    target 547
  ]
  edge [
    source 29
    target 548
  ]
  edge [
    source 29
    target 549
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 550
  ]
  edge [
    source 31
    target 551
  ]
  edge [
    source 31
    target 552
  ]
  edge [
    source 31
    target 553
  ]
  edge [
    source 31
    target 554
  ]
  edge [
    source 31
    target 555
  ]
  edge [
    source 31
    target 556
  ]
  edge [
    source 31
    target 557
  ]
  edge [
    source 31
    target 558
  ]
  edge [
    source 31
    target 559
  ]
  edge [
    source 32
    target 38
  ]
  edge [
    source 32
    target 39
  ]
  edge [
    source 32
    target 40
  ]
  edge [
    source 32
    target 560
  ]
  edge [
    source 32
    target 561
  ]
  edge [
    source 32
    target 562
  ]
  edge [
    source 32
    target 563
  ]
  edge [
    source 32
    target 564
  ]
  edge [
    source 32
    target 565
  ]
  edge [
    source 32
    target 566
  ]
  edge [
    source 32
    target 567
  ]
  edge [
    source 32
    target 568
  ]
  edge [
    source 32
    target 569
  ]
  edge [
    source 32
    target 570
  ]
  edge [
    source 32
    target 571
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 32
    target 43
  ]
  edge [
    source 32
    target 967
  ]
  edge [
    source 32
    target 968
  ]
  edge [
    source 32
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 572
  ]
  edge [
    source 34
    target 108
  ]
  edge [
    source 34
    target 250
  ]
  edge [
    source 34
    target 573
  ]
  edge [
    source 34
    target 574
  ]
  edge [
    source 34
    target 575
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 34
    target 576
  ]
  edge [
    source 34
    target 537
  ]
  edge [
    source 34
    target 577
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 578
  ]
  edge [
    source 35
    target 579
  ]
  edge [
    source 35
    target 580
  ]
  edge [
    source 35
    target 581
  ]
  edge [
    source 35
    target 582
  ]
  edge [
    source 35
    target 583
  ]
  edge [
    source 35
    target 584
  ]
  edge [
    source 35
    target 585
  ]
  edge [
    source 35
    target 586
  ]
  edge [
    source 35
    target 587
  ]
  edge [
    source 35
    target 588
  ]
  edge [
    source 35
    target 589
  ]
  edge [
    source 35
    target 590
  ]
  edge [
    source 35
    target 591
  ]
  edge [
    source 35
    target 592
  ]
  edge [
    source 35
    target 593
  ]
  edge [
    source 35
    target 594
  ]
  edge [
    source 35
    target 595
  ]
  edge [
    source 35
    target 596
  ]
  edge [
    source 35
    target 597
  ]
  edge [
    source 35
    target 598
  ]
  edge [
    source 35
    target 599
  ]
  edge [
    source 35
    target 600
  ]
  edge [
    source 35
    target 601
  ]
  edge [
    source 35
    target 602
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 603
  ]
  edge [
    source 36
    target 604
  ]
  edge [
    source 36
    target 605
  ]
  edge [
    source 36
    target 606
  ]
  edge [
    source 36
    target 607
  ]
  edge [
    source 36
    target 608
  ]
  edge [
    source 36
    target 609
  ]
  edge [
    source 36
    target 610
  ]
  edge [
    source 36
    target 611
  ]
  edge [
    source 36
    target 612
  ]
  edge [
    source 36
    target 613
  ]
  edge [
    source 36
    target 614
  ]
  edge [
    source 36
    target 615
  ]
  edge [
    source 36
    target 616
  ]
  edge [
    source 36
    target 617
  ]
  edge [
    source 36
    target 618
  ]
  edge [
    source 36
    target 619
  ]
  edge [
    source 36
    target 620
  ]
  edge [
    source 36
    target 621
  ]
  edge [
    source 36
    target 622
  ]
  edge [
    source 36
    target 623
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 624
  ]
  edge [
    source 37
    target 43
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 968
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 38
    target 539
  ]
  edge [
    source 38
    target 625
  ]
  edge [
    source 38
    target 626
  ]
  edge [
    source 38
    target 627
  ]
  edge [
    source 38
    target 628
  ]
  edge [
    source 38
    target 629
  ]
  edge [
    source 38
    target 630
  ]
  edge [
    source 38
    target 631
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 968
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 39
    target 968
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 632
  ]
  edge [
    source 40
    target 633
  ]
  edge [
    source 40
    target 634
  ]
  edge [
    source 40
    target 96
  ]
  edge [
    source 40
    target 635
  ]
  edge [
    source 40
    target 636
  ]
  edge [
    source 40
    target 637
  ]
  edge [
    source 40
    target 638
  ]
  edge [
    source 40
    target 639
  ]
  edge [
    source 40
    target 137
  ]
  edge [
    source 40
    target 640
  ]
  edge [
    source 40
    target 641
  ]
  edge [
    source 40
    target 642
  ]
  edge [
    source 40
    target 643
  ]
  edge [
    source 40
    target 644
  ]
  edge [
    source 40
    target 645
  ]
  edge [
    source 40
    target 646
  ]
  edge [
    source 40
    target 968
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 69
  ]
  edge [
    source 41
    target 70
  ]
  edge [
    source 41
    target 647
  ]
  edge [
    source 41
    target 648
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 649
  ]
  edge [
    source 42
    target 650
  ]
  edge [
    source 42
    target 49
  ]
  edge [
    source 42
    target 52
  ]
  edge [
    source 43
    target 70
  ]
  edge [
    source 43
    target 71
  ]
  edge [
    source 43
    target 651
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 652
  ]
  edge [
    source 44
    target 653
  ]
  edge [
    source 44
    target 654
  ]
  edge [
    source 44
    target 655
  ]
  edge [
    source 44
    target 656
  ]
  edge [
    source 44
    target 657
  ]
  edge [
    source 44
    target 658
  ]
  edge [
    source 44
    target 659
  ]
  edge [
    source 44
    target 660
  ]
  edge [
    source 44
    target 661
  ]
  edge [
    source 44
    target 662
  ]
  edge [
    source 44
    target 663
  ]
  edge [
    source 44
    target 664
  ]
  edge [
    source 44
    target 80
  ]
  edge [
    source 45
    target 61
  ]
  edge [
    source 45
    target 81
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 665
  ]
  edge [
    source 46
    target 666
  ]
  edge [
    source 46
    target 667
  ]
  edge [
    source 46
    target 668
  ]
  edge [
    source 46
    target 669
  ]
  edge [
    source 46
    target 670
  ]
  edge [
    source 46
    target 671
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 53
  ]
  edge [
    source 47
    target 84
  ]
  edge [
    source 47
    target 420
  ]
  edge [
    source 47
    target 672
  ]
  edge [
    source 47
    target 673
  ]
  edge [
    source 47
    target 674
  ]
  edge [
    source 47
    target 675
  ]
  edge [
    source 47
    target 676
  ]
  edge [
    source 47
    target 677
  ]
  edge [
    source 47
    target 678
  ]
  edge [
    source 47
    target 679
  ]
  edge [
    source 47
    target 47
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 48
    target 83
  ]
  edge [
    source 48
    target 680
  ]
  edge [
    source 48
    target 681
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 682
  ]
  edge [
    source 49
    target 683
  ]
  edge [
    source 49
    target 684
  ]
  edge [
    source 49
    target 52
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 563
  ]
  edge [
    source 50
    target 143
  ]
  edge [
    source 50
    target 685
  ]
  edge [
    source 50
    target 686
  ]
  edge [
    source 50
    target 605
  ]
  edge [
    source 50
    target 687
  ]
  edge [
    source 50
    target 688
  ]
  edge [
    source 50
    target 689
  ]
  edge [
    source 50
    target 690
  ]
  edge [
    source 50
    target 691
  ]
  edge [
    source 50
    target 692
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 693
  ]
  edge [
    source 51
    target 694
  ]
  edge [
    source 51
    target 695
  ]
  edge [
    source 51
    target 696
  ]
  edge [
    source 51
    target 697
  ]
  edge [
    source 51
    target 698
  ]
  edge [
    source 51
    target 699
  ]
  edge [
    source 51
    target 700
  ]
  edge [
    source 51
    target 701
  ]
  edge [
    source 51
    target 702
  ]
  edge [
    source 51
    target 665
  ]
  edge [
    source 51
    target 703
  ]
  edge [
    source 51
    target 704
  ]
  edge [
    source 51
    target 705
  ]
  edge [
    source 51
    target 706
  ]
  edge [
    source 51
    target 707
  ]
  edge [
    source 51
    target 708
  ]
  edge [
    source 51
    target 709
  ]
  edge [
    source 51
    target 710
  ]
  edge [
    source 51
    target 711
  ]
  edge [
    source 51
    target 712
  ]
  edge [
    source 51
    target 713
  ]
  edge [
    source 51
    target 714
  ]
  edge [
    source 51
    target 715
  ]
  edge [
    source 51
    target 716
  ]
  edge [
    source 51
    target 717
  ]
  edge [
    source 51
    target 718
  ]
  edge [
    source 51
    target 719
  ]
  edge [
    source 51
    target 720
  ]
  edge [
    source 51
    target 721
  ]
  edge [
    source 51
    target 722
  ]
  edge [
    source 51
    target 723
  ]
  edge [
    source 51
    target 724
  ]
  edge [
    source 51
    target 725
  ]
  edge [
    source 51
    target 726
  ]
  edge [
    source 51
    target 727
  ]
  edge [
    source 51
    target 728
  ]
  edge [
    source 51
    target 729
  ]
  edge [
    source 51
    target 730
  ]
  edge [
    source 51
    target 731
  ]
  edge [
    source 51
    target 732
  ]
  edge [
    source 51
    target 733
  ]
  edge [
    source 51
    target 734
  ]
  edge [
    source 52
    target 735
  ]
  edge [
    source 52
    target 736
  ]
  edge [
    source 52
    target 737
  ]
  edge [
    source 52
    target 738
  ]
  edge [
    source 52
    target 727
  ]
  edge [
    source 52
    target 739
  ]
  edge [
    source 52
    target 740
  ]
  edge [
    source 52
    target 741
  ]
  edge [
    source 52
    target 742
  ]
  edge [
    source 52
    target 743
  ]
  edge [
    source 52
    target 734
  ]
  edge [
    source 52
    target 744
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 655
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 83
  ]
  edge [
    source 56
    target 745
  ]
  edge [
    source 56
    target 746
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 747
  ]
  edge [
    source 58
    target 748
  ]
  edge [
    source 58
    target 749
  ]
  edge [
    source 58
    target 750
  ]
  edge [
    source 58
    target 137
  ]
  edge [
    source 58
    target 751
  ]
  edge [
    source 58
    target 752
  ]
  edge [
    source 58
    target 753
  ]
  edge [
    source 58
    target 754
  ]
  edge [
    source 58
    target 755
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 98
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 80
  ]
  edge [
    source 61
    target 756
  ]
  edge [
    source 61
    target 757
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 758
  ]
  edge [
    source 62
    target 759
  ]
  edge [
    source 62
    target 73
  ]
  edge [
    source 62
    target 69
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 68
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 760
  ]
  edge [
    source 64
    target 761
  ]
  edge [
    source 64
    target 762
  ]
  edge [
    source 64
    target 763
  ]
  edge [
    source 64
    target 703
  ]
  edge [
    source 64
    target 764
  ]
  edge [
    source 64
    target 765
  ]
  edge [
    source 64
    target 766
  ]
  edge [
    source 64
    target 767
  ]
  edge [
    source 64
    target 768
  ]
  edge [
    source 64
    target 769
  ]
  edge [
    source 64
    target 770
  ]
  edge [
    source 64
    target 99
  ]
  edge [
    source 64
    target 715
  ]
  edge [
    source 64
    target 771
  ]
  edge [
    source 64
    target 772
  ]
  edge [
    source 64
    target 230
  ]
  edge [
    source 64
    target 773
  ]
  edge [
    source 64
    target 774
  ]
  edge [
    source 64
    target 775
  ]
  edge [
    source 64
    target 776
  ]
  edge [
    source 64
    target 777
  ]
  edge [
    source 64
    target 778
  ]
  edge [
    source 64
    target 779
  ]
  edge [
    source 64
    target 780
  ]
  edge [
    source 64
    target 781
  ]
  edge [
    source 64
    target 782
  ]
  edge [
    source 64
    target 783
  ]
  edge [
    source 64
    target 784
  ]
  edge [
    source 64
    target 785
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 786
  ]
  edge [
    source 65
    target 787
  ]
  edge [
    source 65
    target 788
  ]
  edge [
    source 65
    target 789
  ]
  edge [
    source 65
    target 790
  ]
  edge [
    source 65
    target 791
  ]
  edge [
    source 65
    target 792
  ]
  edge [
    source 65
    target 793
  ]
  edge [
    source 65
    target 794
  ]
  edge [
    source 65
    target 795
  ]
  edge [
    source 65
    target 796
  ]
  edge [
    source 65
    target 797
  ]
  edge [
    source 65
    target 798
  ]
  edge [
    source 65
    target 799
  ]
  edge [
    source 65
    target 800
  ]
  edge [
    source 65
    target 709
  ]
  edge [
    source 65
    target 801
  ]
  edge [
    source 65
    target 802
  ]
  edge [
    source 65
    target 803
  ]
  edge [
    source 65
    target 804
  ]
  edge [
    source 65
    target 805
  ]
  edge [
    source 65
    target 806
  ]
  edge [
    source 65
    target 807
  ]
  edge [
    source 65
    target 808
  ]
  edge [
    source 65
    target 809
  ]
  edge [
    source 65
    target 810
  ]
  edge [
    source 65
    target 811
  ]
  edge [
    source 65
    target 812
  ]
  edge [
    source 65
    target 813
  ]
  edge [
    source 65
    target 814
  ]
  edge [
    source 65
    target 815
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 816
  ]
  edge [
    source 66
    target 817
  ]
  edge [
    source 66
    target 818
  ]
  edge [
    source 66
    target 819
  ]
  edge [
    source 66
    target 820
  ]
  edge [
    source 66
    target 821
  ]
  edge [
    source 66
    target 822
  ]
  edge [
    source 66
    target 823
  ]
  edge [
    source 66
    target 824
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 825
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 826
  ]
  edge [
    source 68
    target 827
  ]
  edge [
    source 68
    target 828
  ]
  edge [
    source 68
    target 829
  ]
  edge [
    source 68
    target 830
  ]
  edge [
    source 68
    target 831
  ]
  edge [
    source 68
    target 832
  ]
  edge [
    source 68
    target 833
  ]
  edge [
    source 68
    target 834
  ]
  edge [
    source 68
    target 835
  ]
  edge [
    source 68
    target 836
  ]
  edge [
    source 68
    target 837
  ]
  edge [
    source 68
    target 838
  ]
  edge [
    source 68
    target 839
  ]
  edge [
    source 68
    target 840
  ]
  edge [
    source 69
    target 841
  ]
  edge [
    source 69
    target 842
  ]
  edge [
    source 70
    target 843
  ]
  edge [
    source 70
    target 844
  ]
  edge [
    source 70
    target 845
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 103
  ]
  edge [
    source 72
    target 846
  ]
  edge [
    source 72
    target 847
  ]
  edge [
    source 72
    target 848
  ]
  edge [
    source 72
    target 849
  ]
  edge [
    source 72
    target 850
  ]
  edge [
    source 72
    target 851
  ]
  edge [
    source 72
    target 852
  ]
  edge [
    source 72
    target 853
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 854
  ]
  edge [
    source 73
    target 855
  ]
  edge [
    source 73
    target 856
  ]
  edge [
    source 73
    target 857
  ]
  edge [
    source 73
    target 858
  ]
  edge [
    source 73
    target 859
  ]
  edge [
    source 73
    target 860
  ]
  edge [
    source 73
    target 861
  ]
  edge [
    source 73
    target 862
  ]
  edge [
    source 73
    target 863
  ]
  edge [
    source 73
    target 864
  ]
  edge [
    source 73
    target 865
  ]
  edge [
    source 73
    target 866
  ]
  edge [
    source 73
    target 867
  ]
  edge [
    source 73
    target 868
  ]
  edge [
    source 73
    target 869
  ]
  edge [
    source 73
    target 870
  ]
  edge [
    source 73
    target 871
  ]
  edge [
    source 73
    target 872
  ]
  edge [
    source 73
    target 873
  ]
  edge [
    source 73
    target 874
  ]
  edge [
    source 73
    target 875
  ]
  edge [
    source 73
    target 876
  ]
  edge [
    source 73
    target 877
  ]
  edge [
    source 73
    target 878
  ]
  edge [
    source 73
    target 879
  ]
  edge [
    source 73
    target 880
  ]
  edge [
    source 73
    target 881
  ]
  edge [
    source 73
    target 882
  ]
  edge [
    source 73
    target 883
  ]
  edge [
    source 73
    target 884
  ]
  edge [
    source 73
    target 885
  ]
  edge [
    source 73
    target 886
  ]
  edge [
    source 73
    target 887
  ]
  edge [
    source 73
    target 888
  ]
  edge [
    source 73
    target 889
  ]
  edge [
    source 73
    target 890
  ]
  edge [
    source 73
    target 78
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 891
  ]
  edge [
    source 74
    target 892
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 893
  ]
  edge [
    source 75
    target 894
  ]
  edge [
    source 75
    target 895
  ]
  edge [
    source 75
    target 896
  ]
  edge [
    source 75
    target 897
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 898
  ]
  edge [
    source 76
    target 899
  ]
  edge [
    source 76
    target 900
  ]
  edge [
    source 76
    target 901
  ]
  edge [
    source 76
    target 902
  ]
  edge [
    source 76
    target 903
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 904
  ]
  edge [
    source 77
    target 905
  ]
  edge [
    source 77
    target 906
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 907
  ]
  edge [
    source 78
    target 857
  ]
  edge [
    source 78
    target 137
  ]
  edge [
    source 78
    target 908
  ]
  edge [
    source 78
    target 909
  ]
  edge [
    source 78
    target 910
  ]
  edge [
    source 78
    target 911
  ]
  edge [
    source 78
    target 912
  ]
  edge [
    source 78
    target 913
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 914
  ]
  edge [
    source 79
    target 915
  ]
  edge [
    source 79
    target 916
  ]
  edge [
    source 79
    target 917
  ]
  edge [
    source 79
    target 918
  ]
  edge [
    source 79
    target 919
  ]
  edge [
    source 79
    target 920
  ]
  edge [
    source 79
    target 186
  ]
  edge [
    source 79
    target 921
  ]
  edge [
    source 79
    target 922
  ]
  edge [
    source 79
    target 188
  ]
  edge [
    source 79
    target 536
  ]
  edge [
    source 79
    target 923
  ]
  edge [
    source 80
    target 921
  ]
  edge [
    source 80
    target 924
  ]
  edge [
    source 80
    target 925
  ]
  edge [
    source 80
    target 926
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 927
  ]
  edge [
    source 82
    target 928
  ]
  edge [
    source 82
    target 929
  ]
  edge [
    source 82
    target 930
  ]
  edge [
    source 82
    target 931
  ]
  edge [
    source 82
    target 932
  ]
  edge [
    source 82
    target 933
  ]
  edge [
    source 82
    target 934
  ]
  edge [
    source 82
    target 438
  ]
  edge [
    source 82
    target 935
  ]
  edge [
    source 82
    target 973
  ]
  edge [
    source 82
    target 974
  ]
  edge [
    source 82
    target 975
  ]
  edge [
    source 82
    target 976
  ]
  edge [
    source 82
    target 977
  ]
  edge [
    source 83
    target 936
  ]
  edge [
    source 83
    target 937
  ]
  edge [
    source 83
    target 938
  ]
  edge [
    source 83
    target 939
  ]
  edge [
    source 83
    target 940
  ]
  edge [
    source 83
    target 941
  ]
  edge [
    source 83
    target 942
  ]
  edge [
    source 83
    target 943
  ]
  edge [
    source 83
    target 944
  ]
  edge [
    source 83
    target 669
  ]
  edge [
    source 83
    target 945
  ]
  edge [
    source 84
    target 946
  ]
  edge [
    source 84
    target 947
  ]
  edge [
    source 84
    target 948
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 949
  ]
  edge [
    source 86
    target 693
  ]
  edge [
    source 86
    target 950
  ]
  edge [
    source 86
    target 951
  ]
  edge [
    source 86
    target 952
  ]
  edge [
    source 86
    target 953
  ]
  edge [
    source 86
    target 954
  ]
  edge [
    source 86
    target 955
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 956
  ]
  edge [
    source 88
    target 278
  ]
  edge [
    source 88
    target 957
  ]
  edge [
    source 89
    target 745
  ]
  edge [
    source 90
    target 958
  ]
  edge [
    source 90
    target 959
  ]
  edge [
    source 90
    target 918
  ]
  edge [
    source 90
    target 960
  ]
  edge [
    source 90
    target 961
  ]
  edge [
    source 90
    target 962
  ]
  edge [
    source 90
    target 963
  ]
  edge [
    source 91
    target 964
  ]
  edge [
    source 91
    target 965
  ]
  edge [
    source 969
    target 970
  ]
  edge [
    source 971
    target 972
  ]
  edge [
    source 973
    target 974
  ]
  edge [
    source 973
    target 975
  ]
  edge [
    source 973
    target 976
  ]
  edge [
    source 973
    target 977
  ]
  edge [
    source 974
    target 975
  ]
  edge [
    source 974
    target 976
  ]
  edge [
    source 974
    target 977
  ]
  edge [
    source 975
    target 976
  ]
  edge [
    source 975
    target 977
  ]
  edge [
    source 976
    target 977
  ]
  edge [
    source 978
    target 979
  ]
  edge [
    source 980
    target 981
  ]
  edge [
    source 980
    target 982
  ]
  edge [
    source 981
    target 982
  ]
  edge [
    source 983
    target 984
  ]
]
