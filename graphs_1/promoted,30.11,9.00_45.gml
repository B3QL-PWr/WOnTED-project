graph [
  maxDegree 20
  minDegree 1
  meanDegree 2
  density 0.017241379310344827
  graphCliqueNumber 3
  node [
    id 0
    label "pkn"
    origin "text"
  ]
  node [
    id 1
    label "orlen"
    origin "text"
  ]
  node [
    id 2
    label "potwierdzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "oficjalny"
    origin "text"
  ]
  node [
    id 5
    label "partner"
    origin "text"
  ]
  node [
    id 6
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 7
    label "formu&#322;a"
    origin "text"
  ]
  node [
    id 8
    label "williams"
    origin "text"
  ]
  node [
    id 9
    label "martini"
    origin "text"
  ]
  node [
    id 10
    label "racing"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 13
    label "rok"
    origin "text"
  ]
  node [
    id 14
    label "by&#263;"
    origin "text"
  ]
  node [
    id 15
    label "si&#281;"
    origin "text"
  ]
  node [
    id 16
    label "&#347;ciga&#263;"
    origin "text"
  ]
  node [
    id 17
    label "robert"
    origin "text"
  ]
  node [
    id 18
    label "kubica"
    origin "text"
  ]
  node [
    id 19
    label "stwierdzi&#263;"
  ]
  node [
    id 20
    label "acknowledge"
  ]
  node [
    id 21
    label "przy&#347;wiadczy&#263;"
  ]
  node [
    id 22
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 23
    label "attest"
  ]
  node [
    id 24
    label "proceed"
  ]
  node [
    id 25
    label "catch"
  ]
  node [
    id 26
    label "pozosta&#263;"
  ]
  node [
    id 27
    label "osta&#263;_si&#281;"
  ]
  node [
    id 28
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 29
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 30
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 31
    label "change"
  ]
  node [
    id 32
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 33
    label "jawny"
  ]
  node [
    id 34
    label "oficjalnie"
  ]
  node [
    id 35
    label "legalny"
  ]
  node [
    id 36
    label "sformalizowanie"
  ]
  node [
    id 37
    label "formalizowanie"
  ]
  node [
    id 38
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 39
    label "formalnie"
  ]
  node [
    id 40
    label "sp&#243;lnik"
  ]
  node [
    id 41
    label "cz&#322;owiek"
  ]
  node [
    id 42
    label "uczestniczenie"
  ]
  node [
    id 43
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 44
    label "przedsi&#281;biorca"
  ]
  node [
    id 45
    label "prowadzi&#263;"
  ]
  node [
    id 46
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 47
    label "kolaborator"
  ]
  node [
    id 48
    label "pracownik"
  ]
  node [
    id 49
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 50
    label "aktor"
  ]
  node [
    id 51
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 52
    label "whole"
  ]
  node [
    id 53
    label "odm&#322;adza&#263;"
  ]
  node [
    id 54
    label "zabudowania"
  ]
  node [
    id 55
    label "odm&#322;odzenie"
  ]
  node [
    id 56
    label "zespolik"
  ]
  node [
    id 57
    label "skupienie"
  ]
  node [
    id 58
    label "schorzenie"
  ]
  node [
    id 59
    label "grupa"
  ]
  node [
    id 60
    label "Depeche_Mode"
  ]
  node [
    id 61
    label "Mazowsze"
  ]
  node [
    id 62
    label "ro&#347;lina"
  ]
  node [
    id 63
    label "zbi&#243;r"
  ]
  node [
    id 64
    label "The_Beatles"
  ]
  node [
    id 65
    label "group"
  ]
  node [
    id 66
    label "&#346;wietliki"
  ]
  node [
    id 67
    label "odm&#322;adzanie"
  ]
  node [
    id 68
    label "batch"
  ]
  node [
    id 69
    label "rule"
  ]
  node [
    id 70
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 71
    label "zapis"
  ]
  node [
    id 72
    label "formularz"
  ]
  node [
    id 73
    label "sformu&#322;owanie"
  ]
  node [
    id 74
    label "kultura"
  ]
  node [
    id 75
    label "kultura_duchowa"
  ]
  node [
    id 76
    label "ceremony"
  ]
  node [
    id 77
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 78
    label "short_drink"
  ]
  node [
    id 79
    label "wermut"
  ]
  node [
    id 80
    label "kolejny"
  ]
  node [
    id 81
    label "stulecie"
  ]
  node [
    id 82
    label "kalendarz"
  ]
  node [
    id 83
    label "czas"
  ]
  node [
    id 84
    label "pora_roku"
  ]
  node [
    id 85
    label "cykl_astronomiczny"
  ]
  node [
    id 86
    label "p&#243;&#322;rocze"
  ]
  node [
    id 87
    label "kwarta&#322;"
  ]
  node [
    id 88
    label "kurs"
  ]
  node [
    id 89
    label "jubileusz"
  ]
  node [
    id 90
    label "miesi&#261;c"
  ]
  node [
    id 91
    label "lata"
  ]
  node [
    id 92
    label "martwy_sezon"
  ]
  node [
    id 93
    label "si&#281;ga&#263;"
  ]
  node [
    id 94
    label "trwa&#263;"
  ]
  node [
    id 95
    label "obecno&#347;&#263;"
  ]
  node [
    id 96
    label "stan"
  ]
  node [
    id 97
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 98
    label "stand"
  ]
  node [
    id 99
    label "mie&#263;_miejsce"
  ]
  node [
    id 100
    label "uczestniczy&#263;"
  ]
  node [
    id 101
    label "chodzi&#263;"
  ]
  node [
    id 102
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 103
    label "equal"
  ]
  node [
    id 104
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 105
    label "straszy&#263;"
  ]
  node [
    id 106
    label "prosecute"
  ]
  node [
    id 107
    label "kara&#263;"
  ]
  node [
    id 108
    label "usi&#322;owa&#263;"
  ]
  node [
    id 109
    label "poszukiwa&#263;"
  ]
  node [
    id 110
    label "PKN"
  ]
  node [
    id 111
    label "Orlen"
  ]
  node [
    id 112
    label "Williams"
  ]
  node [
    id 113
    label "Racing"
  ]
  node [
    id 114
    label "Robert"
  ]
  node [
    id 115
    label "Kubica"
  ]
  node [
    id 116
    label "1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 59
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 114
    target 115
  ]
]
