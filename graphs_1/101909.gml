graph [
  maxDegree 43
  minDegree 1
  meanDegree 1.9764705882352942
  density 0.023529411764705882
  graphCliqueNumber 2
  node [
    id 0
    label "czynno&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "faza"
    origin "text"
  ]
  node [
    id 3
    label "piel&#281;gnacja"
    origin "text"
  ]
  node [
    id 4
    label "system"
    origin "text"
  ]
  node [
    id 5
    label "informatyczny"
    origin "text"
  ]
  node [
    id 6
    label "activity"
  ]
  node [
    id 7
    label "wydarzenie"
  ]
  node [
    id 8
    label "bezproblemowy"
  ]
  node [
    id 9
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 10
    label "work"
  ]
  node [
    id 11
    label "robi&#263;"
  ]
  node [
    id 12
    label "muzyka"
  ]
  node [
    id 13
    label "rola"
  ]
  node [
    id 14
    label "create"
  ]
  node [
    id 15
    label "wytwarza&#263;"
  ]
  node [
    id 16
    label "praca"
  ]
  node [
    id 17
    label "czas"
  ]
  node [
    id 18
    label "dw&#243;jnik"
  ]
  node [
    id 19
    label "fotoelement"
  ]
  node [
    id 20
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 21
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 22
    label "obsesja"
  ]
  node [
    id 23
    label "komutowa&#263;"
  ]
  node [
    id 24
    label "degree"
  ]
  node [
    id 25
    label "cykl_astronomiczny"
  ]
  node [
    id 26
    label "komutowanie"
  ]
  node [
    id 27
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 28
    label "coil"
  ]
  node [
    id 29
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 30
    label "obw&#243;d"
  ]
  node [
    id 31
    label "zjawisko"
  ]
  node [
    id 32
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 33
    label "stan_skupienia"
  ]
  node [
    id 34
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 35
    label "nastr&#243;j"
  ]
  node [
    id 36
    label "przew&#243;d"
  ]
  node [
    id 37
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 38
    label "kraw&#281;d&#378;"
  ]
  node [
    id 39
    label "okres"
  ]
  node [
    id 40
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 41
    label "przerywacz"
  ]
  node [
    id 42
    label "opieka"
  ]
  node [
    id 43
    label "sustenance"
  ]
  node [
    id 44
    label "model"
  ]
  node [
    id 45
    label "sk&#322;ad"
  ]
  node [
    id 46
    label "zachowanie"
  ]
  node [
    id 47
    label "podstawa"
  ]
  node [
    id 48
    label "porz&#261;dek"
  ]
  node [
    id 49
    label "Android"
  ]
  node [
    id 50
    label "przyn&#281;ta"
  ]
  node [
    id 51
    label "jednostka_geologiczna"
  ]
  node [
    id 52
    label "metoda"
  ]
  node [
    id 53
    label "podsystem"
  ]
  node [
    id 54
    label "p&#322;&#243;d"
  ]
  node [
    id 55
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 56
    label "s&#261;d"
  ]
  node [
    id 57
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 58
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 59
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 60
    label "j&#261;dro"
  ]
  node [
    id 61
    label "eratem"
  ]
  node [
    id 62
    label "ryba"
  ]
  node [
    id 63
    label "pulpit"
  ]
  node [
    id 64
    label "struktura"
  ]
  node [
    id 65
    label "spos&#243;b"
  ]
  node [
    id 66
    label "oddzia&#322;"
  ]
  node [
    id 67
    label "usenet"
  ]
  node [
    id 68
    label "o&#347;"
  ]
  node [
    id 69
    label "oprogramowanie"
  ]
  node [
    id 70
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 71
    label "poj&#281;cie"
  ]
  node [
    id 72
    label "w&#281;dkarstwo"
  ]
  node [
    id 73
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 74
    label "Leopard"
  ]
  node [
    id 75
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 76
    label "systemik"
  ]
  node [
    id 77
    label "rozprz&#261;c"
  ]
  node [
    id 78
    label "cybernetyk"
  ]
  node [
    id 79
    label "konstelacja"
  ]
  node [
    id 80
    label "doktryna"
  ]
  node [
    id 81
    label "net"
  ]
  node [
    id 82
    label "zbi&#243;r"
  ]
  node [
    id 83
    label "method"
  ]
  node [
    id 84
    label "systemat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
]
