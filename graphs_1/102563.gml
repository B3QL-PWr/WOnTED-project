graph [
  maxDegree 44
  minDegree 1
  meanDegree 2
  density 0.018518518518518517
  graphCliqueNumber 2
  node [
    id 0
    label "informacja"
    origin "text"
  ]
  node [
    id 1
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "samorz&#261;dowy"
    origin "text"
  ]
  node [
    id 3
    label "jednostka"
    origin "text"
  ]
  node [
    id 4
    label "kultura"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "doj&#347;cie"
  ]
  node [
    id 7
    label "doj&#347;&#263;"
  ]
  node [
    id 8
    label "powzi&#261;&#263;"
  ]
  node [
    id 9
    label "wiedza"
  ]
  node [
    id 10
    label "sygna&#322;"
  ]
  node [
    id 11
    label "obiegni&#281;cie"
  ]
  node [
    id 12
    label "obieganie"
  ]
  node [
    id 13
    label "obiec"
  ]
  node [
    id 14
    label "dane"
  ]
  node [
    id 15
    label "obiega&#263;"
  ]
  node [
    id 16
    label "punkt"
  ]
  node [
    id 17
    label "publikacja"
  ]
  node [
    id 18
    label "powzi&#281;cie"
  ]
  node [
    id 19
    label "dzia&#322;anie"
  ]
  node [
    id 20
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 21
    label "absolutorium"
  ]
  node [
    id 22
    label "activity"
  ]
  node [
    id 23
    label "infimum"
  ]
  node [
    id 24
    label "ewoluowanie"
  ]
  node [
    id 25
    label "przyswoi&#263;"
  ]
  node [
    id 26
    label "reakcja"
  ]
  node [
    id 27
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 28
    label "wyewoluowanie"
  ]
  node [
    id 29
    label "individual"
  ]
  node [
    id 30
    label "profanum"
  ]
  node [
    id 31
    label "starzenie_si&#281;"
  ]
  node [
    id 32
    label "homo_sapiens"
  ]
  node [
    id 33
    label "skala"
  ]
  node [
    id 34
    label "supremum"
  ]
  node [
    id 35
    label "przyswaja&#263;"
  ]
  node [
    id 36
    label "ludzko&#347;&#263;"
  ]
  node [
    id 37
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 38
    label "one"
  ]
  node [
    id 39
    label "funkcja"
  ]
  node [
    id 40
    label "przeliczenie"
  ]
  node [
    id 41
    label "przeliczanie"
  ]
  node [
    id 42
    label "mikrokosmos"
  ]
  node [
    id 43
    label "rzut"
  ]
  node [
    id 44
    label "portrecista"
  ]
  node [
    id 45
    label "przelicza&#263;"
  ]
  node [
    id 46
    label "przyswajanie"
  ]
  node [
    id 47
    label "duch"
  ]
  node [
    id 48
    label "wyewoluowa&#263;"
  ]
  node [
    id 49
    label "ewoluowa&#263;"
  ]
  node [
    id 50
    label "oddzia&#322;ywanie"
  ]
  node [
    id 51
    label "g&#322;owa"
  ]
  node [
    id 52
    label "liczba_naturalna"
  ]
  node [
    id 53
    label "poj&#281;cie"
  ]
  node [
    id 54
    label "osoba"
  ]
  node [
    id 55
    label "figura"
  ]
  node [
    id 56
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 57
    label "obiekt"
  ]
  node [
    id 58
    label "matematyka"
  ]
  node [
    id 59
    label "przyswojenie"
  ]
  node [
    id 60
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 61
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 62
    label "czynnik_biotyczny"
  ]
  node [
    id 63
    label "przeliczy&#263;"
  ]
  node [
    id 64
    label "antropochoria"
  ]
  node [
    id 65
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 66
    label "przedmiot"
  ]
  node [
    id 67
    label "Wsch&#243;d"
  ]
  node [
    id 68
    label "rzecz"
  ]
  node [
    id 69
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 70
    label "sztuka"
  ]
  node [
    id 71
    label "religia"
  ]
  node [
    id 72
    label "przejmowa&#263;"
  ]
  node [
    id 73
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 74
    label "makrokosmos"
  ]
  node [
    id 75
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 76
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 77
    label "zjawisko"
  ]
  node [
    id 78
    label "praca_rolnicza"
  ]
  node [
    id 79
    label "tradycja"
  ]
  node [
    id 80
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 81
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 82
    label "przejmowanie"
  ]
  node [
    id 83
    label "cecha"
  ]
  node [
    id 84
    label "asymilowanie_si&#281;"
  ]
  node [
    id 85
    label "przej&#261;&#263;"
  ]
  node [
    id 86
    label "hodowla"
  ]
  node [
    id 87
    label "brzoskwiniarnia"
  ]
  node [
    id 88
    label "populace"
  ]
  node [
    id 89
    label "konwencja"
  ]
  node [
    id 90
    label "propriety"
  ]
  node [
    id 91
    label "jako&#347;&#263;"
  ]
  node [
    id 92
    label "kuchnia"
  ]
  node [
    id 93
    label "zwyczaj"
  ]
  node [
    id 94
    label "przej&#281;cie"
  ]
  node [
    id 95
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 96
    label "stulecie"
  ]
  node [
    id 97
    label "kalendarz"
  ]
  node [
    id 98
    label "czas"
  ]
  node [
    id 99
    label "pora_roku"
  ]
  node [
    id 100
    label "cykl_astronomiczny"
  ]
  node [
    id 101
    label "p&#243;&#322;rocze"
  ]
  node [
    id 102
    label "grupa"
  ]
  node [
    id 103
    label "kwarta&#322;"
  ]
  node [
    id 104
    label "kurs"
  ]
  node [
    id 105
    label "jubileusz"
  ]
  node [
    id 106
    label "miesi&#261;c"
  ]
  node [
    id 107
    label "lata"
  ]
  node [
    id 108
    label "martwy_sezon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
]
