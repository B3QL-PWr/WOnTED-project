graph [
  maxDegree 52
  minDegree 1
  meanDegree 2.3275862068965516
  density 0.0050271840321739775
  graphCliqueNumber 4
  node [
    id 0
    label "przygotowywa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ucze&#324;"
    origin "text"
  ]
  node [
    id 2
    label "wykorzystanie"
    origin "text"
  ]
  node [
    id 3
    label "wiedza"
    origin "text"
  ]
  node [
    id 4
    label "matematyczny"
    origin "text"
  ]
  node [
    id 5
    label "rozwi&#261;zywa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "problem"
    origin "text"
  ]
  node [
    id 7
    label "zakres"
    origin "text"
  ]
  node [
    id 8
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 9
    label "dziedzina"
    origin "text"
  ]
  node [
    id 10
    label "kszta&#322;cenie"
    origin "text"
  ]
  node [
    id 11
    label "szkolny"
    origin "text"
  ]
  node [
    id 12
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 13
    label "codzienny"
    origin "text"
  ]
  node [
    id 14
    label "budowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "model"
    origin "text"
  ]
  node [
    id 16
    label "dla"
    origin "text"
  ]
  node [
    id 17
    label "konkretny"
    origin "text"
  ]
  node [
    id 18
    label "sytuacja"
    origin "text"
  ]
  node [
    id 19
    label "przyswaja&#263;"
    origin "text"
  ]
  node [
    id 20
    label "przez"
    origin "text"
  ]
  node [
    id 21
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 22
    label "matematyka"
    origin "text"
  ]
  node [
    id 23
    label "dostrzega&#263;"
    origin "text"
  ]
  node [
    id 24
    label "formu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "dyskutowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "kszta&#322;towa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "umiej&#281;tno&#347;&#263;"
    origin "text"
  ]
  node [
    id 28
    label "my&#347;lenie"
    origin "text"
  ]
  node [
    id 29
    label "jasny"
    origin "text"
  ]
  node [
    id 30
    label "wypowied&#378;"
    origin "text"
  ]
  node [
    id 31
    label "rozwija&#263;"
    origin "text"
  ]
  node [
    id 32
    label "rozumienie"
    origin "text"
  ]
  node [
    id 33
    label "tekst"
    origin "text"
  ]
  node [
    id 34
    label "sformu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 35
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 36
    label "prosta"
    origin "text"
  ]
  node [
    id 37
    label "u&#322;atwia&#263;"
    origin "text"
  ]
  node [
    id 38
    label "badanie"
    origin "text"
  ]
  node [
    id 39
    label "przypadek"
    origin "text"
  ]
  node [
    id 40
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 41
    label "rozumowanie"
    origin "text"
  ]
  node [
    id 42
    label "wykonywa&#263;"
  ]
  node [
    id 43
    label "sposobi&#263;"
  ]
  node [
    id 44
    label "arrange"
  ]
  node [
    id 45
    label "pryczy&#263;"
  ]
  node [
    id 46
    label "train"
  ]
  node [
    id 47
    label "robi&#263;"
  ]
  node [
    id 48
    label "wytwarza&#263;"
  ]
  node [
    id 49
    label "szkoli&#263;"
  ]
  node [
    id 50
    label "usposabia&#263;"
  ]
  node [
    id 51
    label "cz&#322;owiek"
  ]
  node [
    id 52
    label "zwolennik"
  ]
  node [
    id 53
    label "tarcza"
  ]
  node [
    id 54
    label "czeladnik"
  ]
  node [
    id 55
    label "elew"
  ]
  node [
    id 56
    label "rzemie&#347;lnik"
  ]
  node [
    id 57
    label "kontynuator"
  ]
  node [
    id 58
    label "klasa"
  ]
  node [
    id 59
    label "wyprawka"
  ]
  node [
    id 60
    label "mundurek"
  ]
  node [
    id 61
    label "absolwent"
  ]
  node [
    id 62
    label "szko&#322;a"
  ]
  node [
    id 63
    label "praktykant"
  ]
  node [
    id 64
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 65
    label "zrobienie"
  ]
  node [
    id 66
    label "use"
  ]
  node [
    id 67
    label "u&#380;ycie"
  ]
  node [
    id 68
    label "stosowanie"
  ]
  node [
    id 69
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 70
    label "u&#380;yteczny"
  ]
  node [
    id 71
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 72
    label "exploitation"
  ]
  node [
    id 73
    label "pozwolenie"
  ]
  node [
    id 74
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 75
    label "wykszta&#322;cenie"
  ]
  node [
    id 76
    label "zaawansowanie"
  ]
  node [
    id 77
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 78
    label "intelekt"
  ]
  node [
    id 79
    label "cognition"
  ]
  node [
    id 80
    label "dok&#322;adny"
  ]
  node [
    id 81
    label "matematycznie"
  ]
  node [
    id 82
    label "cope"
  ]
  node [
    id 83
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 84
    label "odkrywa&#263;"
  ]
  node [
    id 85
    label "przestawa&#263;"
  ]
  node [
    id 86
    label "urzeczywistnia&#263;"
  ]
  node [
    id 87
    label "usuwa&#263;"
  ]
  node [
    id 88
    label "undo"
  ]
  node [
    id 89
    label "trudno&#347;&#263;"
  ]
  node [
    id 90
    label "sprawa"
  ]
  node [
    id 91
    label "ambaras"
  ]
  node [
    id 92
    label "problemat"
  ]
  node [
    id 93
    label "pierepa&#322;ka"
  ]
  node [
    id 94
    label "obstruction"
  ]
  node [
    id 95
    label "problematyka"
  ]
  node [
    id 96
    label "jajko_Kolumba"
  ]
  node [
    id 97
    label "subiekcja"
  ]
  node [
    id 98
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 99
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 100
    label "granica"
  ]
  node [
    id 101
    label "circle"
  ]
  node [
    id 102
    label "podzakres"
  ]
  node [
    id 103
    label "zbi&#243;r"
  ]
  node [
    id 104
    label "desygnat"
  ]
  node [
    id 105
    label "sfera"
  ]
  node [
    id 106
    label "wielko&#347;&#263;"
  ]
  node [
    id 107
    label "r&#243;&#380;nie"
  ]
  node [
    id 108
    label "inny"
  ]
  node [
    id 109
    label "jaki&#347;"
  ]
  node [
    id 110
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 111
    label "bezdro&#380;e"
  ]
  node [
    id 112
    label "funkcja"
  ]
  node [
    id 113
    label "poddzia&#322;"
  ]
  node [
    id 114
    label "zapoznawanie"
  ]
  node [
    id 115
    label "wysy&#322;anie"
  ]
  node [
    id 116
    label "formation"
  ]
  node [
    id 117
    label "rozwijanie"
  ]
  node [
    id 118
    label "training"
  ]
  node [
    id 119
    label "pomaganie"
  ]
  node [
    id 120
    label "o&#347;wiecanie"
  ]
  node [
    id 121
    label "kliker"
  ]
  node [
    id 122
    label "heureza"
  ]
  node [
    id 123
    label "pouczenie"
  ]
  node [
    id 124
    label "nauka"
  ]
  node [
    id 125
    label "szkolnie"
  ]
  node [
    id 126
    label "szkoleniowy"
  ]
  node [
    id 127
    label "podstawowy"
  ]
  node [
    id 128
    label "prosty"
  ]
  node [
    id 129
    label "energy"
  ]
  node [
    id 130
    label "czas"
  ]
  node [
    id 131
    label "bycie"
  ]
  node [
    id 132
    label "zegar_biologiczny"
  ]
  node [
    id 133
    label "okres_noworodkowy"
  ]
  node [
    id 134
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 135
    label "entity"
  ]
  node [
    id 136
    label "prze&#380;ywanie"
  ]
  node [
    id 137
    label "prze&#380;ycie"
  ]
  node [
    id 138
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 139
    label "wiek_matuzalemowy"
  ]
  node [
    id 140
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 141
    label "dzieci&#324;stwo"
  ]
  node [
    id 142
    label "power"
  ]
  node [
    id 143
    label "szwung"
  ]
  node [
    id 144
    label "menopauza"
  ]
  node [
    id 145
    label "umarcie"
  ]
  node [
    id 146
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 147
    label "life"
  ]
  node [
    id 148
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 149
    label "&#380;ywy"
  ]
  node [
    id 150
    label "rozw&#243;j"
  ]
  node [
    id 151
    label "po&#322;&#243;g"
  ]
  node [
    id 152
    label "byt"
  ]
  node [
    id 153
    label "przebywanie"
  ]
  node [
    id 154
    label "subsistence"
  ]
  node [
    id 155
    label "koleje_losu"
  ]
  node [
    id 156
    label "raj_utracony"
  ]
  node [
    id 157
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 158
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 159
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 160
    label "andropauza"
  ]
  node [
    id 161
    label "warunki"
  ]
  node [
    id 162
    label "do&#380;ywanie"
  ]
  node [
    id 163
    label "niemowl&#281;ctwo"
  ]
  node [
    id 164
    label "umieranie"
  ]
  node [
    id 165
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 166
    label "staro&#347;&#263;"
  ]
  node [
    id 167
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 168
    label "&#347;mier&#263;"
  ]
  node [
    id 169
    label "cykliczny"
  ]
  node [
    id 170
    label "codziennie"
  ]
  node [
    id 171
    label "powszedny"
  ]
  node [
    id 172
    label "prozaiczny"
  ]
  node [
    id 173
    label "cz&#281;sty"
  ]
  node [
    id 174
    label "pospolity"
  ]
  node [
    id 175
    label "sta&#322;y"
  ]
  node [
    id 176
    label "regularny"
  ]
  node [
    id 177
    label "planowa&#263;"
  ]
  node [
    id 178
    label "dodawa&#263;_otuchy"
  ]
  node [
    id 179
    label "consist"
  ]
  node [
    id 180
    label "tworzy&#263;"
  ]
  node [
    id 181
    label "raise"
  ]
  node [
    id 182
    label "stanowi&#263;"
  ]
  node [
    id 183
    label "typ"
  ]
  node [
    id 184
    label "pozowa&#263;"
  ]
  node [
    id 185
    label "ideal"
  ]
  node [
    id 186
    label "matryca"
  ]
  node [
    id 187
    label "imitacja"
  ]
  node [
    id 188
    label "ruch"
  ]
  node [
    id 189
    label "motif"
  ]
  node [
    id 190
    label "pozowanie"
  ]
  node [
    id 191
    label "wz&#243;r"
  ]
  node [
    id 192
    label "miniatura"
  ]
  node [
    id 193
    label "prezenter"
  ]
  node [
    id 194
    label "facet"
  ]
  node [
    id 195
    label "orygina&#322;"
  ]
  node [
    id 196
    label "mildew"
  ]
  node [
    id 197
    label "spos&#243;b"
  ]
  node [
    id 198
    label "zi&#243;&#322;ko"
  ]
  node [
    id 199
    label "adaptation"
  ]
  node [
    id 200
    label "tre&#347;ciwy"
  ]
  node [
    id 201
    label "&#322;adny"
  ]
  node [
    id 202
    label "okre&#347;lony"
  ]
  node [
    id 203
    label "skupiony"
  ]
  node [
    id 204
    label "po&#380;ywny"
  ]
  node [
    id 205
    label "ogarni&#281;ty"
  ]
  node [
    id 206
    label "konkretnie"
  ]
  node [
    id 207
    label "posilny"
  ]
  node [
    id 208
    label "abstrakcyjny"
  ]
  node [
    id 209
    label "solidnie"
  ]
  node [
    id 210
    label "niez&#322;y"
  ]
  node [
    id 211
    label "szczeg&#243;&#322;"
  ]
  node [
    id 212
    label "motyw"
  ]
  node [
    id 213
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 214
    label "state"
  ]
  node [
    id 215
    label "realia"
  ]
  node [
    id 216
    label "rede"
  ]
  node [
    id 217
    label "organizm"
  ]
  node [
    id 218
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 219
    label "translate"
  ]
  node [
    id 220
    label "treat"
  ]
  node [
    id 221
    label "pobiera&#263;"
  ]
  node [
    id 222
    label "kultura"
  ]
  node [
    id 223
    label "czerpa&#263;"
  ]
  node [
    id 224
    label "pisa&#263;"
  ]
  node [
    id 225
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 226
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 227
    label "ssanie"
  ]
  node [
    id 228
    label "po_koroniarsku"
  ]
  node [
    id 229
    label "przedmiot"
  ]
  node [
    id 230
    label "but"
  ]
  node [
    id 231
    label "m&#243;wienie"
  ]
  node [
    id 232
    label "rozumie&#263;"
  ]
  node [
    id 233
    label "formacja_geologiczna"
  ]
  node [
    id 234
    label "m&#243;wi&#263;"
  ]
  node [
    id 235
    label "gramatyka"
  ]
  node [
    id 236
    label "pype&#263;"
  ]
  node [
    id 237
    label "makroglosja"
  ]
  node [
    id 238
    label "kawa&#322;ek"
  ]
  node [
    id 239
    label "artykulator"
  ]
  node [
    id 240
    label "kultura_duchowa"
  ]
  node [
    id 241
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 242
    label "jama_ustna"
  ]
  node [
    id 243
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 244
    label "przet&#322;umaczenie"
  ]
  node [
    id 245
    label "t&#322;umaczenie"
  ]
  node [
    id 246
    label "language"
  ]
  node [
    id 247
    label "jeniec"
  ]
  node [
    id 248
    label "organ"
  ]
  node [
    id 249
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 250
    label "pismo"
  ]
  node [
    id 251
    label "formalizowanie"
  ]
  node [
    id 252
    label "fonetyka"
  ]
  node [
    id 253
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 254
    label "wokalizm"
  ]
  node [
    id 255
    label "liza&#263;"
  ]
  node [
    id 256
    label "s&#322;ownictwo"
  ]
  node [
    id 257
    label "napisa&#263;"
  ]
  node [
    id 258
    label "formalizowa&#263;"
  ]
  node [
    id 259
    label "natural_language"
  ]
  node [
    id 260
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 261
    label "stylik"
  ]
  node [
    id 262
    label "konsonantyzm"
  ]
  node [
    id 263
    label "urz&#261;dzenie"
  ]
  node [
    id 264
    label "ssa&#263;"
  ]
  node [
    id 265
    label "kod"
  ]
  node [
    id 266
    label "lizanie"
  ]
  node [
    id 267
    label "infimum"
  ]
  node [
    id 268
    label "matematyka_czysta"
  ]
  node [
    id 269
    label "fizyka_matematyczna"
  ]
  node [
    id 270
    label "matma"
  ]
  node [
    id 271
    label "rachunki"
  ]
  node [
    id 272
    label "kryptologia"
  ]
  node [
    id 273
    label "supremum"
  ]
  node [
    id 274
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 275
    label "rachunek_operatorowy"
  ]
  node [
    id 276
    label "rzut"
  ]
  node [
    id 277
    label "forsing"
  ]
  node [
    id 278
    label "teoria_graf&#243;w"
  ]
  node [
    id 279
    label "logicyzm"
  ]
  node [
    id 280
    label "logika"
  ]
  node [
    id 281
    label "topologia_algebraiczna"
  ]
  node [
    id 282
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 283
    label "matematyka_stosowana"
  ]
  node [
    id 284
    label "kierunek"
  ]
  node [
    id 285
    label "modelowanie_matematyczne"
  ]
  node [
    id 286
    label "teoria_katastrof"
  ]
  node [
    id 287
    label "jednostka"
  ]
  node [
    id 288
    label "widzie&#263;"
  ]
  node [
    id 289
    label "perceive"
  ]
  node [
    id 290
    label "orientowa&#263;_si&#281;"
  ]
  node [
    id 291
    label "obacza&#263;"
  ]
  node [
    id 292
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 293
    label "notice"
  ]
  node [
    id 294
    label "komunikowa&#263;"
  ]
  node [
    id 295
    label "convey"
  ]
  node [
    id 296
    label "rozmawia&#263;"
  ]
  node [
    id 297
    label "argue"
  ]
  node [
    id 298
    label "shape"
  ]
  node [
    id 299
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 300
    label "dostosowywa&#263;"
  ]
  node [
    id 301
    label "nadawa&#263;"
  ]
  node [
    id 302
    label "zdolno&#347;&#263;"
  ]
  node [
    id 303
    label "cecha"
  ]
  node [
    id 304
    label "zinterpretowanie"
  ]
  node [
    id 305
    label "pilnowanie"
  ]
  node [
    id 306
    label "robienie"
  ]
  node [
    id 307
    label "wnioskowanie"
  ]
  node [
    id 308
    label "troskanie_si&#281;"
  ]
  node [
    id 309
    label "skupianie_si&#281;"
  ]
  node [
    id 310
    label "s&#261;dzenie"
  ]
  node [
    id 311
    label "judgment"
  ]
  node [
    id 312
    label "walczy&#263;"
  ]
  node [
    id 313
    label "zjawisko"
  ]
  node [
    id 314
    label "czynno&#347;&#263;"
  ]
  node [
    id 315
    label "reflection"
  ]
  node [
    id 316
    label "wzlecie&#263;"
  ]
  node [
    id 317
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 318
    label "proces_my&#347;lowy"
  ]
  node [
    id 319
    label "wzlecenie"
  ]
  node [
    id 320
    label "walczenie"
  ]
  node [
    id 321
    label "treatment"
  ]
  node [
    id 322
    label "domy&#347;lanie_si&#281;"
  ]
  node [
    id 323
    label "proces"
  ]
  node [
    id 324
    label "szczery"
  ]
  node [
    id 325
    label "o&#347;wietlenie"
  ]
  node [
    id 326
    label "klarowny"
  ]
  node [
    id 327
    label "przytomny"
  ]
  node [
    id 328
    label "jednoznaczny"
  ]
  node [
    id 329
    label "pogodny"
  ]
  node [
    id 330
    label "o&#347;wietlanie"
  ]
  node [
    id 331
    label "bia&#322;y"
  ]
  node [
    id 332
    label "niezm&#261;cony"
  ]
  node [
    id 333
    label "zrozumia&#322;y"
  ]
  node [
    id 334
    label "dobry"
  ]
  node [
    id 335
    label "jasno"
  ]
  node [
    id 336
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 337
    label "parafrazowa&#263;"
  ]
  node [
    id 338
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 339
    label "sparafrazowa&#263;"
  ]
  node [
    id 340
    label "s&#261;d"
  ]
  node [
    id 341
    label "trawestowanie"
  ]
  node [
    id 342
    label "sparafrazowanie"
  ]
  node [
    id 343
    label "strawestowa&#263;"
  ]
  node [
    id 344
    label "sformu&#322;owanie"
  ]
  node [
    id 345
    label "strawestowanie"
  ]
  node [
    id 346
    label "komunikat"
  ]
  node [
    id 347
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 348
    label "delimitacja"
  ]
  node [
    id 349
    label "trawestowa&#263;"
  ]
  node [
    id 350
    label "parafrazowanie"
  ]
  node [
    id 351
    label "stylizacja"
  ]
  node [
    id 352
    label "ozdobnik"
  ]
  node [
    id 353
    label "pos&#322;uchanie"
  ]
  node [
    id 354
    label "rezultat"
  ]
  node [
    id 355
    label "rozstawia&#263;"
  ]
  node [
    id 356
    label "puszcza&#263;"
  ]
  node [
    id 357
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 358
    label "dopowiada&#263;"
  ]
  node [
    id 359
    label "omawia&#263;"
  ]
  node [
    id 360
    label "rozpakowywa&#263;"
  ]
  node [
    id 361
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 362
    label "inflate"
  ]
  node [
    id 363
    label "stawia&#263;"
  ]
  node [
    id 364
    label "czucie"
  ]
  node [
    id 365
    label "wytw&#243;r"
  ]
  node [
    id 366
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 367
    label "kontekst"
  ]
  node [
    id 368
    label "apprehension"
  ]
  node [
    id 369
    label "kumanie"
  ]
  node [
    id 370
    label "obja&#347;nienie"
  ]
  node [
    id 371
    label "hermeneutyka"
  ]
  node [
    id 372
    label "interpretation"
  ]
  node [
    id 373
    label "realization"
  ]
  node [
    id 374
    label "odmianka"
  ]
  node [
    id 375
    label "opu&#347;ci&#263;"
  ]
  node [
    id 376
    label "koniektura"
  ]
  node [
    id 377
    label "preparacja"
  ]
  node [
    id 378
    label "ekscerpcja"
  ]
  node [
    id 379
    label "j&#281;zykowo"
  ]
  node [
    id 380
    label "obelga"
  ]
  node [
    id 381
    label "dzie&#322;o"
  ]
  node [
    id 382
    label "redakcja"
  ]
  node [
    id 383
    label "pomini&#281;cie"
  ]
  node [
    id 384
    label "testify"
  ]
  node [
    id 385
    label "zakomunikowa&#263;"
  ]
  node [
    id 386
    label "zapoznawa&#263;"
  ]
  node [
    id 387
    label "represent"
  ]
  node [
    id 388
    label "odcinek"
  ]
  node [
    id 389
    label "proste_sko&#347;ne"
  ]
  node [
    id 390
    label "straight_line"
  ]
  node [
    id 391
    label "punkt"
  ]
  node [
    id 392
    label "trasa"
  ]
  node [
    id 393
    label "krzywa"
  ]
  node [
    id 394
    label "ease"
  ]
  node [
    id 395
    label "powodowa&#263;"
  ]
  node [
    id 396
    label "&#322;atwi&#263;"
  ]
  node [
    id 397
    label "usi&#322;owanie"
  ]
  node [
    id 398
    label "examination"
  ]
  node [
    id 399
    label "investigation"
  ]
  node [
    id 400
    label "ustalenie"
  ]
  node [
    id 401
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 402
    label "ustalanie"
  ]
  node [
    id 403
    label "bia&#322;a_niedziela"
  ]
  node [
    id 404
    label "analysis"
  ]
  node [
    id 405
    label "rozpatrywanie"
  ]
  node [
    id 406
    label "wziernikowanie"
  ]
  node [
    id 407
    label "obserwowanie"
  ]
  node [
    id 408
    label "omawianie"
  ]
  node [
    id 409
    label "sprawdzanie"
  ]
  node [
    id 410
    label "udowadnianie"
  ]
  node [
    id 411
    label "diagnostyka"
  ]
  node [
    id 412
    label "macanie"
  ]
  node [
    id 413
    label "rektalny"
  ]
  node [
    id 414
    label "penetrowanie"
  ]
  node [
    id 415
    label "krytykowanie"
  ]
  node [
    id 416
    label "kontrola"
  ]
  node [
    id 417
    label "dociekanie"
  ]
  node [
    id 418
    label "zrecenzowanie"
  ]
  node [
    id 419
    label "praca"
  ]
  node [
    id 420
    label "pacjent"
  ]
  node [
    id 421
    label "kategoria_gramatyczna"
  ]
  node [
    id 422
    label "schorzenie"
  ]
  node [
    id 423
    label "przeznaczenie"
  ]
  node [
    id 424
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 425
    label "wydarzenie"
  ]
  node [
    id 426
    label "happening"
  ]
  node [
    id 427
    label "przyk&#322;ad"
  ]
  node [
    id 428
    label "przywodzenie"
  ]
  node [
    id 429
    label "prowadzanie"
  ]
  node [
    id 430
    label "ukierunkowywanie"
  ]
  node [
    id 431
    label "kszta&#322;towanie"
  ]
  node [
    id 432
    label "poprowadzenie"
  ]
  node [
    id 433
    label "wprowadzanie"
  ]
  node [
    id 434
    label "dysponowanie"
  ]
  node [
    id 435
    label "przeci&#261;ganie"
  ]
  node [
    id 436
    label "doprowadzanie"
  ]
  node [
    id 437
    label "wprowadzenie"
  ]
  node [
    id 438
    label "eksponowanie"
  ]
  node [
    id 439
    label "oprowadzenie"
  ]
  node [
    id 440
    label "trzymanie"
  ]
  node [
    id 441
    label "ta&#324;czenie"
  ]
  node [
    id 442
    label "przeci&#281;cie"
  ]
  node [
    id 443
    label "przewy&#380;szanie"
  ]
  node [
    id 444
    label "prowadzi&#263;"
  ]
  node [
    id 445
    label "aim"
  ]
  node [
    id 446
    label "zwracanie"
  ]
  node [
    id 447
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 448
    label "przecinanie"
  ]
  node [
    id 449
    label "sterowanie"
  ]
  node [
    id 450
    label "drive"
  ]
  node [
    id 451
    label "kre&#347;lenie"
  ]
  node [
    id 452
    label "management"
  ]
  node [
    id 453
    label "dawanie"
  ]
  node [
    id 454
    label "oprowadzanie"
  ]
  node [
    id 455
    label "pozarz&#261;dzanie"
  ]
  node [
    id 456
    label "g&#243;rowanie"
  ]
  node [
    id 457
    label "linia_melodyczna"
  ]
  node [
    id 458
    label "granie"
  ]
  node [
    id 459
    label "doprowadzenie"
  ]
  node [
    id 460
    label "kierowanie"
  ]
  node [
    id 461
    label "zaprowadzanie"
  ]
  node [
    id 462
    label "lead"
  ]
  node [
    id 463
    label "powodowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 46
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 48
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 197
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 38
  ]
  edge [
    source 17
    target 39
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 201
  ]
  edge [
    source 17
    target 29
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 207
  ]
  edge [
    source 17
    target 208
  ]
  edge [
    source 17
    target 209
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 37
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 18
    target 213
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 39
  ]
  edge [
    source 18
    target 41
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 223
  ]
  edge [
    source 19
    target 37
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 224
  ]
  edge [
    source 21
    target 225
  ]
  edge [
    source 21
    target 226
  ]
  edge [
    source 21
    target 227
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 21
    target 229
  ]
  edge [
    source 21
    target 230
  ]
  edge [
    source 21
    target 231
  ]
  edge [
    source 21
    target 232
  ]
  edge [
    source 21
    target 233
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 234
  ]
  edge [
    source 21
    target 235
  ]
  edge [
    source 21
    target 236
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 21
    target 239
  ]
  edge [
    source 21
    target 240
  ]
  edge [
    source 21
    target 241
  ]
  edge [
    source 21
    target 242
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 243
  ]
  edge [
    source 21
    target 244
  ]
  edge [
    source 21
    target 245
  ]
  edge [
    source 21
    target 246
  ]
  edge [
    source 21
    target 247
  ]
  edge [
    source 21
    target 248
  ]
  edge [
    source 21
    target 249
  ]
  edge [
    source 21
    target 250
  ]
  edge [
    source 21
    target 251
  ]
  edge [
    source 21
    target 252
  ]
  edge [
    source 21
    target 253
  ]
  edge [
    source 21
    target 254
  ]
  edge [
    source 21
    target 255
  ]
  edge [
    source 21
    target 256
  ]
  edge [
    source 21
    target 257
  ]
  edge [
    source 21
    target 258
  ]
  edge [
    source 21
    target 259
  ]
  edge [
    source 21
    target 260
  ]
  edge [
    source 21
    target 261
  ]
  edge [
    source 21
    target 262
  ]
  edge [
    source 21
    target 263
  ]
  edge [
    source 21
    target 264
  ]
  edge [
    source 21
    target 265
  ]
  edge [
    source 21
    target 266
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 31
  ]
  edge [
    source 22
    target 36
  ]
  edge [
    source 22
    target 267
  ]
  edge [
    source 22
    target 268
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 269
  ]
  edge [
    source 22
    target 270
  ]
  edge [
    source 22
    target 271
  ]
  edge [
    source 22
    target 272
  ]
  edge [
    source 22
    target 273
  ]
  edge [
    source 22
    target 274
  ]
  edge [
    source 22
    target 275
  ]
  edge [
    source 22
    target 112
  ]
  edge [
    source 22
    target 276
  ]
  edge [
    source 22
    target 277
  ]
  edge [
    source 22
    target 278
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 22
    target 280
  ]
  edge [
    source 22
    target 281
  ]
  edge [
    source 22
    target 282
  ]
  edge [
    source 22
    target 283
  ]
  edge [
    source 22
    target 284
  ]
  edge [
    source 22
    target 285
  ]
  edge [
    source 22
    target 286
  ]
  edge [
    source 22
    target 287
  ]
  edge [
    source 22
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 23
    target 288
  ]
  edge [
    source 23
    target 289
  ]
  edge [
    source 23
    target 290
  ]
  edge [
    source 23
    target 291
  ]
  edge [
    source 23
    target 292
  ]
  edge [
    source 23
    target 293
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 294
  ]
  edge [
    source 24
    target 295
  ]
  edge [
    source 25
    target 296
  ]
  edge [
    source 25
    target 297
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 298
  ]
  edge [
    source 26
    target 299
  ]
  edge [
    source 26
    target 300
  ]
  edge [
    source 26
    target 301
  ]
  edge [
    source 26
    target 38
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 27
    target 302
  ]
  edge [
    source 27
    target 303
  ]
  edge [
    source 27
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 304
  ]
  edge [
    source 28
    target 305
  ]
  edge [
    source 28
    target 306
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 28
    target 308
  ]
  edge [
    source 28
    target 309
  ]
  edge [
    source 28
    target 310
  ]
  edge [
    source 28
    target 311
  ]
  edge [
    source 28
    target 312
  ]
  edge [
    source 28
    target 313
  ]
  edge [
    source 28
    target 314
  ]
  edge [
    source 28
    target 315
  ]
  edge [
    source 28
    target 316
  ]
  edge [
    source 28
    target 317
  ]
  edge [
    source 28
    target 318
  ]
  edge [
    source 28
    target 319
  ]
  edge [
    source 28
    target 320
  ]
  edge [
    source 28
    target 321
  ]
  edge [
    source 28
    target 322
  ]
  edge [
    source 28
    target 323
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 29
    target 324
  ]
  edge [
    source 29
    target 325
  ]
  edge [
    source 29
    target 326
  ]
  edge [
    source 29
    target 327
  ]
  edge [
    source 29
    target 328
  ]
  edge [
    source 29
    target 329
  ]
  edge [
    source 29
    target 330
  ]
  edge [
    source 29
    target 331
  ]
  edge [
    source 29
    target 332
  ]
  edge [
    source 29
    target 333
  ]
  edge [
    source 29
    target 334
  ]
  edge [
    source 29
    target 335
  ]
  edge [
    source 29
    target 336
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 337
  ]
  edge [
    source 30
    target 338
  ]
  edge [
    source 30
    target 339
  ]
  edge [
    source 30
    target 340
  ]
  edge [
    source 30
    target 341
  ]
  edge [
    source 30
    target 342
  ]
  edge [
    source 30
    target 343
  ]
  edge [
    source 30
    target 344
  ]
  edge [
    source 30
    target 345
  ]
  edge [
    source 30
    target 346
  ]
  edge [
    source 30
    target 347
  ]
  edge [
    source 30
    target 348
  ]
  edge [
    source 30
    target 349
  ]
  edge [
    source 30
    target 350
  ]
  edge [
    source 30
    target 351
  ]
  edge [
    source 30
    target 352
  ]
  edge [
    source 30
    target 353
  ]
  edge [
    source 30
    target 354
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 31
    target 355
  ]
  edge [
    source 31
    target 356
  ]
  edge [
    source 31
    target 46
  ]
  edge [
    source 31
    target 357
  ]
  edge [
    source 31
    target 358
  ]
  edge [
    source 31
    target 299
  ]
  edge [
    source 31
    target 359
  ]
  edge [
    source 31
    target 360
  ]
  edge [
    source 31
    target 361
  ]
  edge [
    source 31
    target 362
  ]
  edge [
    source 31
    target 363
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 306
  ]
  edge [
    source 32
    target 314
  ]
  edge [
    source 32
    target 364
  ]
  edge [
    source 32
    target 365
  ]
  edge [
    source 32
    target 307
  ]
  edge [
    source 32
    target 131
  ]
  edge [
    source 32
    target 366
  ]
  edge [
    source 32
    target 367
  ]
  edge [
    source 32
    target 368
  ]
  edge [
    source 32
    target 369
  ]
  edge [
    source 32
    target 370
  ]
  edge [
    source 32
    target 371
  ]
  edge [
    source 32
    target 372
  ]
  edge [
    source 32
    target 373
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 224
  ]
  edge [
    source 33
    target 374
  ]
  edge [
    source 33
    target 375
  ]
  edge [
    source 33
    target 365
  ]
  edge [
    source 33
    target 376
  ]
  edge [
    source 33
    target 377
  ]
  edge [
    source 33
    target 378
  ]
  edge [
    source 33
    target 379
  ]
  edge [
    source 33
    target 380
  ]
  edge [
    source 33
    target 381
  ]
  edge [
    source 33
    target 382
  ]
  edge [
    source 33
    target 383
  ]
  edge [
    source 34
    target 384
  ]
  edge [
    source 34
    target 385
  ]
  edge [
    source 35
    target 386
  ]
  edge [
    source 35
    target 387
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 36
    target 41
  ]
  edge [
    source 36
    target 130
  ]
  edge [
    source 36
    target 388
  ]
  edge [
    source 36
    target 389
  ]
  edge [
    source 36
    target 390
  ]
  edge [
    source 36
    target 391
  ]
  edge [
    source 36
    target 392
  ]
  edge [
    source 36
    target 393
  ]
  edge [
    source 37
    target 394
  ]
  edge [
    source 37
    target 47
  ]
  edge [
    source 37
    target 395
  ]
  edge [
    source 37
    target 396
  ]
  edge [
    source 38
    target 397
  ]
  edge [
    source 38
    target 398
  ]
  edge [
    source 38
    target 399
  ]
  edge [
    source 38
    target 400
  ]
  edge [
    source 38
    target 401
  ]
  edge [
    source 38
    target 402
  ]
  edge [
    source 38
    target 403
  ]
  edge [
    source 38
    target 404
  ]
  edge [
    source 38
    target 405
  ]
  edge [
    source 38
    target 406
  ]
  edge [
    source 38
    target 407
  ]
  edge [
    source 38
    target 408
  ]
  edge [
    source 38
    target 409
  ]
  edge [
    source 38
    target 410
  ]
  edge [
    source 38
    target 411
  ]
  edge [
    source 38
    target 314
  ]
  edge [
    source 38
    target 412
  ]
  edge [
    source 38
    target 413
  ]
  edge [
    source 38
    target 414
  ]
  edge [
    source 38
    target 415
  ]
  edge [
    source 38
    target 416
  ]
  edge [
    source 38
    target 417
  ]
  edge [
    source 38
    target 418
  ]
  edge [
    source 38
    target 419
  ]
  edge [
    source 38
    target 354
  ]
  edge [
    source 39
    target 420
  ]
  edge [
    source 39
    target 421
  ]
  edge [
    source 39
    target 422
  ]
  edge [
    source 39
    target 423
  ]
  edge [
    source 39
    target 424
  ]
  edge [
    source 39
    target 425
  ]
  edge [
    source 39
    target 426
  ]
  edge [
    source 39
    target 427
  ]
  edge [
    source 40
    target 306
  ]
  edge [
    source 40
    target 428
  ]
  edge [
    source 40
    target 429
  ]
  edge [
    source 40
    target 430
  ]
  edge [
    source 40
    target 431
  ]
  edge [
    source 40
    target 432
  ]
  edge [
    source 40
    target 433
  ]
  edge [
    source 40
    target 434
  ]
  edge [
    source 40
    target 435
  ]
  edge [
    source 40
    target 436
  ]
  edge [
    source 40
    target 437
  ]
  edge [
    source 40
    target 438
  ]
  edge [
    source 40
    target 439
  ]
  edge [
    source 40
    target 440
  ]
  edge [
    source 40
    target 441
  ]
  edge [
    source 40
    target 442
  ]
  edge [
    source 40
    target 443
  ]
  edge [
    source 40
    target 444
  ]
  edge [
    source 40
    target 445
  ]
  edge [
    source 40
    target 314
  ]
  edge [
    source 40
    target 446
  ]
  edge [
    source 40
    target 447
  ]
  edge [
    source 40
    target 448
  ]
  edge [
    source 40
    target 449
  ]
  edge [
    source 40
    target 450
  ]
  edge [
    source 40
    target 451
  ]
  edge [
    source 40
    target 452
  ]
  edge [
    source 40
    target 453
  ]
  edge [
    source 40
    target 454
  ]
  edge [
    source 40
    target 455
  ]
  edge [
    source 40
    target 456
  ]
  edge [
    source 40
    target 457
  ]
  edge [
    source 40
    target 458
  ]
  edge [
    source 40
    target 459
  ]
  edge [
    source 40
    target 460
  ]
  edge [
    source 40
    target 461
  ]
  edge [
    source 40
    target 462
  ]
  edge [
    source 40
    target 463
  ]
  edge [
    source 40
    target 393
  ]
  edge [
    source 41
    target 304
  ]
  edge [
    source 41
    target 306
  ]
  edge [
    source 41
    target 314
  ]
  edge [
    source 41
    target 307
  ]
  edge [
    source 41
    target 322
  ]
  edge [
    source 41
    target 309
  ]
  edge [
    source 41
    target 317
  ]
  edge [
    source 41
    target 311
  ]
  edge [
    source 41
    target 318
  ]
]
