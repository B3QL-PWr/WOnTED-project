graph [
  maxDegree 24
  minDegree 1
  meanDegree 2
  density 0.020833333333333332
  graphCliqueNumber 2
  node [
    id 0
    label "zwr&#243;ci&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "uwaga"
    origin "text"
  ]
  node [
    id 2
    label "gdy"
    origin "text"
  ]
  node [
    id 3
    label "d&#322;ug"
    origin "text"
  ]
  node [
    id 4
    label "sen"
    origin "text"
  ]
  node [
    id 5
    label "s&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 6
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 7
    label "wyczerpa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "wobec"
    origin "text"
  ]
  node [
    id 9
    label "atak"
    origin "text"
  ]
  node [
    id 10
    label "opinia"
    origin "text"
  ]
  node [
    id 11
    label "nagana"
  ]
  node [
    id 12
    label "wypowied&#378;"
  ]
  node [
    id 13
    label "stan"
  ]
  node [
    id 14
    label "dzienniczek"
  ]
  node [
    id 15
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 16
    label "wzgl&#261;d"
  ]
  node [
    id 17
    label "gossip"
  ]
  node [
    id 18
    label "upomnienie"
  ]
  node [
    id 19
    label "tekst"
  ]
  node [
    id 20
    label "nie&#347;ci&#261;galno&#347;&#263;"
  ]
  node [
    id 21
    label "konwersja"
  ]
  node [
    id 22
    label "indebtedness"
  ]
  node [
    id 23
    label "zobowi&#261;zanie"
  ]
  node [
    id 24
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 25
    label "kima"
  ]
  node [
    id 26
    label "wytw&#243;r"
  ]
  node [
    id 27
    label "proces_fizjologiczny"
  ]
  node [
    id 28
    label "relaxation"
  ]
  node [
    id 29
    label "marzenie_senne"
  ]
  node [
    id 30
    label "sen_wolnofalowy"
  ]
  node [
    id 31
    label "odpoczynek"
  ]
  node [
    id 32
    label "hipersomnia"
  ]
  node [
    id 33
    label "jen"
  ]
  node [
    id 34
    label "fun"
  ]
  node [
    id 35
    label "wymys&#322;"
  ]
  node [
    id 36
    label "nokturn"
  ]
  node [
    id 37
    label "sen_paradoksalny"
  ]
  node [
    id 38
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 39
    label "use"
  ]
  node [
    id 40
    label "trwa&#263;"
  ]
  node [
    id 41
    label "by&#263;"
  ]
  node [
    id 42
    label "&#380;o&#322;nierz"
  ]
  node [
    id 43
    label "pies"
  ]
  node [
    id 44
    label "robi&#263;"
  ]
  node [
    id 45
    label "wait"
  ]
  node [
    id 46
    label "pomaga&#263;"
  ]
  node [
    id 47
    label "cel"
  ]
  node [
    id 48
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 49
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 50
    label "pracowa&#263;"
  ]
  node [
    id 51
    label "suffice"
  ]
  node [
    id 52
    label "match"
  ]
  node [
    id 53
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 54
    label "zu&#380;y&#263;"
  ]
  node [
    id 55
    label "try"
  ]
  node [
    id 56
    label "scoop"
  ]
  node [
    id 57
    label "wybra&#263;"
  ]
  node [
    id 58
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 59
    label "opracowa&#263;"
  ]
  node [
    id 60
    label "zm&#281;czy&#263;"
  ]
  node [
    id 61
    label "manewr"
  ]
  node [
    id 62
    label "spasm"
  ]
  node [
    id 63
    label "&#380;&#261;danie"
  ]
  node [
    id 64
    label "rzucenie"
  ]
  node [
    id 65
    label "pogorszenie"
  ]
  node [
    id 66
    label "krytyka"
  ]
  node [
    id 67
    label "stroke"
  ]
  node [
    id 68
    label "liga"
  ]
  node [
    id 69
    label "pozycja"
  ]
  node [
    id 70
    label "zagrywka"
  ]
  node [
    id 71
    label "rzuci&#263;"
  ]
  node [
    id 72
    label "walka"
  ]
  node [
    id 73
    label "przemoc"
  ]
  node [
    id 74
    label "pogoda"
  ]
  node [
    id 75
    label "przyp&#322;yw"
  ]
  node [
    id 76
    label "fit"
  ]
  node [
    id 77
    label "ofensywa"
  ]
  node [
    id 78
    label "knock"
  ]
  node [
    id 79
    label "oznaka"
  ]
  node [
    id 80
    label "bat"
  ]
  node [
    id 81
    label "kaszel"
  ]
  node [
    id 82
    label "dokument"
  ]
  node [
    id 83
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 84
    label "reputacja"
  ]
  node [
    id 85
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 86
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 87
    label "cecha"
  ]
  node [
    id 88
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 89
    label "informacja"
  ]
  node [
    id 90
    label "sofcik"
  ]
  node [
    id 91
    label "appraisal"
  ]
  node [
    id 92
    label "ekspertyza"
  ]
  node [
    id 93
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 94
    label "pogl&#261;d"
  ]
  node [
    id 95
    label "kryterium"
  ]
  node [
    id 96
    label "wielko&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
]
