graph [
  maxDegree 44
  minDegree 1
  meanDegree 2.3902439024390243
  density 0.029509183980728697
  graphCliqueNumber 5
  node [
    id 0
    label "historia"
    origin "text"
  ]
  node [
    id 1
    label "report"
  ]
  node [
    id 2
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 3
    label "wypowied&#378;"
  ]
  node [
    id 4
    label "neografia"
  ]
  node [
    id 5
    label "przedmiot"
  ]
  node [
    id 6
    label "papirologia"
  ]
  node [
    id 7
    label "historia_gospodarcza"
  ]
  node [
    id 8
    label "przebiec"
  ]
  node [
    id 9
    label "hista"
  ]
  node [
    id 10
    label "nauka_humanistyczna"
  ]
  node [
    id 11
    label "filigranistyka"
  ]
  node [
    id 12
    label "dyplomatyka"
  ]
  node [
    id 13
    label "annalistyka"
  ]
  node [
    id 14
    label "historyka"
  ]
  node [
    id 15
    label "heraldyka"
  ]
  node [
    id 16
    label "fabu&#322;a"
  ]
  node [
    id 17
    label "muzealnictwo"
  ]
  node [
    id 18
    label "varsavianistyka"
  ]
  node [
    id 19
    label "mediewistyka"
  ]
  node [
    id 20
    label "prezentyzm"
  ]
  node [
    id 21
    label "przebiegni&#281;cie"
  ]
  node [
    id 22
    label "charakter"
  ]
  node [
    id 23
    label "paleografia"
  ]
  node [
    id 24
    label "genealogia"
  ]
  node [
    id 25
    label "czynno&#347;&#263;"
  ]
  node [
    id 26
    label "prozopografia"
  ]
  node [
    id 27
    label "motyw"
  ]
  node [
    id 28
    label "nautologia"
  ]
  node [
    id 29
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 30
    label "epoka"
  ]
  node [
    id 31
    label "numizmatyka"
  ]
  node [
    id 32
    label "ruralistyka"
  ]
  node [
    id 33
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 34
    label "epigrafika"
  ]
  node [
    id 35
    label "historiografia"
  ]
  node [
    id 36
    label "bizantynistyka"
  ]
  node [
    id 37
    label "weksylologia"
  ]
  node [
    id 38
    label "kierunek"
  ]
  node [
    id 39
    label "ikonografia"
  ]
  node [
    id 40
    label "chronologia"
  ]
  node [
    id 41
    label "archiwistyka"
  ]
  node [
    id 42
    label "sfragistyka"
  ]
  node [
    id 43
    label "zabytkoznawstwo"
  ]
  node [
    id 44
    label "historia_sztuki"
  ]
  node [
    id 45
    label "w&#322;oski"
  ]
  node [
    id 46
    label "republika"
  ]
  node [
    id 47
    label "socjalny"
  ]
  node [
    id 48
    label "Maria"
  ]
  node [
    id 49
    label "Carlonim"
  ]
  node [
    id 50
    label "4"
  ]
  node [
    id 51
    label "dywizja"
  ]
  node [
    id 52
    label "bersalier"
  ]
  node [
    id 53
    label "Italia"
  ]
  node [
    id 54
    label "m&#281;ski"
  ]
  node [
    id 55
    label "Carloni"
  ]
  node [
    id 56
    label "alpejski"
  ]
  node [
    id 57
    label "Monterosa"
  ]
  node [
    id 58
    label "1"
  ]
  node [
    id 59
    label "Guido"
  ]
  node [
    id 60
    label "Manardi"
  ]
  node [
    id 61
    label "linia"
  ]
  node [
    id 62
    label "got&#243;w"
  ]
  node [
    id 63
    label "ii"
  ]
  node [
    id 64
    label "batalion"
  ]
  node [
    id 65
    label "3"
  ]
  node [
    id 66
    label "pu&#322;k"
  ]
  node [
    id 67
    label "i"
  ]
  node [
    id 68
    label "artyleria"
  ]
  node [
    id 69
    label "Kampfgruppe"
  ]
  node [
    id 70
    label "Ferrario"
  ]
  node [
    id 71
    label "grupa"
  ]
  node [
    id 72
    label "artyleryjski"
  ]
  node [
    id 73
    label "Goffredo"
  ]
  node [
    id 74
    label "Mameli"
  ]
  node [
    id 75
    label "Bergamo"
  ]
  node [
    id 76
    label "2"
  ]
  node [
    id 77
    label "Cacciatori"
  ]
  node [
    id 78
    label "degli"
  ]
  node [
    id 79
    label "Appenninini"
  ]
  node [
    id 80
    label "148"
  ]
  node [
    id 81
    label "piechota"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 50
    target 56
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 50
    target 66
  ]
  edge [
    source 50
    target 68
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 56
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 51
    target 58
  ]
  edge [
    source 51
    target 80
  ]
  edge [
    source 51
    target 81
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 58
  ]
  edge [
    source 52
    target 65
  ]
  edge [
    source 52
    target 66
  ]
  edge [
    source 53
    target 58
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 71
  ]
  edge [
    source 56
    target 72
  ]
  edge [
    source 56
    target 75
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 71
  ]
  edge [
    source 63
    target 72
  ]
  edge [
    source 64
    target 67
  ]
  edge [
    source 64
    target 73
  ]
  edge [
    source 64
    target 74
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 66
    target 68
  ]
  edge [
    source 66
    target 76
  ]
  edge [
    source 66
    target 77
  ]
  edge [
    source 66
    target 78
  ]
  edge [
    source 66
    target 79
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 75
  ]
  edge [
    source 72
    target 75
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 78
  ]
  edge [
    source 76
    target 79
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 79
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 80
    target 81
  ]
]
