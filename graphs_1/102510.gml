graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9
  density 0.1
  graphCliqueNumber 2
  node [
    id 0
    label "spotkanie"
    origin "text"
  ]
  node [
    id 1
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 3
    label "po&#380;egnanie"
  ]
  node [
    id 4
    label "spowodowanie"
  ]
  node [
    id 5
    label "znalezienie"
  ]
  node [
    id 6
    label "znajomy"
  ]
  node [
    id 7
    label "doznanie"
  ]
  node [
    id 8
    label "employment"
  ]
  node [
    id 9
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 10
    label "gather"
  ]
  node [
    id 11
    label "powitanie"
  ]
  node [
    id 12
    label "spotykanie"
  ]
  node [
    id 13
    label "wydarzenie"
  ]
  node [
    id 14
    label "gathering"
  ]
  node [
    id 15
    label "spotkanie_si&#281;"
  ]
  node [
    id 16
    label "zdarzenie_si&#281;"
  ]
  node [
    id 17
    label "match"
  ]
  node [
    id 18
    label "zawarcie"
  ]
  node [
    id 19
    label "miesi&#261;c"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 19
  ]
]
